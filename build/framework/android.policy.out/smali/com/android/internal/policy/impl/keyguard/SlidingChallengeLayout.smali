.class public Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
.super Landroid/view/ViewGroup;
.source "SlidingChallengeLayout.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;,
        Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;
    }
.end annotation


# static fields
.field private static final CHALLENGE_FADE_IN_DURATION:I = 0xa0

.field private static final CHALLENGE_FADE_OUT_DURATION:I = 0x64

.field private static final DEBUG:Z = false

.field private static final DRAG_HANDLE_CLOSED_ABOVE:I = 0x8

.field private static final DRAG_HANDLE_CLOSED_BELOW:I = 0x0

.field private static final DRAG_HANDLE_OPEN_ABOVE:I = 0x8

.field private static final DRAG_HANDLE_OPEN_BELOW:I = 0x0

.field static final HANDLE_ALPHA:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final HANDLE_ANIMATE_DURATION:I = 0xfa

.field private static final INVALID_POINTER:I = -0x1

.field private static final MAX_SETTLE_DURATION:I = 0x258

.field public static final SCROLL_STATE_DRAGGING:I = 0x1

.field public static final SCROLL_STATE_FADING:I = 0x3

.field public static final SCROLL_STATE_IDLE:I = 0x0

.field public static final SCROLL_STATE_SETTLING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SlidingChallengeLayout"

.field private static final sHandleFadeInterpolator:Landroid/view/animation/Interpolator;

.field private static final sMotionInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mActivePointerId:I

.field private mBlockDrag:Z

.field private mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

.field private mChallengeBottomBound:I

.field private mChallengeInteractiveExternal:Z

.field private mChallengeInteractiveInternal:Z

.field private mChallengeOffset:F

.field private mChallengeShowing:Z

.field private mChallengeShowingTargetState:Z

.field private mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDragHandleClosedAbove:I

.field private mDragHandleClosedBelow:I

.field private mDragHandleEdgeSlop:I

.field private mDragHandleOpenAbove:I

.field private mDragHandleOpenBelow:I

.field private mDragging:Z

.field private mEdgeCaptured:Z

.field private final mEndScrollRunnable:Ljava/lang/Runnable;

.field private final mExpandChallengeClickListener:Landroid/view/View$OnClickListener;

.field private mExpandChallengeView:Landroid/view/View;

.field private mFader:Landroid/animation/ObjectAnimator;

.field mFrameAlpha:F

.field private mFrameAnimation:Landroid/animation/ObjectAnimator;

.field mFrameAnimationTarget:F

.field private mGestureStartChallengeBottom:I

.field private mGestureStartX:F

.field private mGestureStartY:F

.field mHandleAlpha:F

.field private mHandleAnimation:Landroid/animation/ObjectAnimator;

.field private mHasGlowpad:Z

.field private mHasLayout:Z

.field private mIsBouncing:Z

.field private mMaxVelocity:I

.field private mMinVelocity:I

.field private final mScrimClickListener:Landroid/view/View$OnClickListener;

.field private mScrimView:Landroid/view/View;

.field private mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

.field private mScrollState:I

.field private final mScroller:Landroid/widget/Scroller;

.field private mSecurityBlockDrag:Z

.field private mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

.field private mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

.field private mTouchSlop:I

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mWasChallengeShowing:Z

.field private mWidgetsView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 142
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$1;

    #@2
    const-string v1, "handleAlpha"

    #@4
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$1;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->HANDLE_ALPHA:Landroid/util/Property;

    #@9
    .line 159
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$2;

    #@b
    invoke-direct {v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$2;-><init>()V

    #@e
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->sMotionInterpolator:Landroid/view/animation/Interpolator;

    #@10
    .line 166
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$3;

    #@12
    invoke-direct {v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$3;-><init>()V

    #@15
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->sHandleFadeInterpolator:Landroid/view/animation/Interpolator;

    #@17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 235
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 236
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 239
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 240
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/high16 v9, 0x4100

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    const/high16 v6, 0x3f00

    #@6
    const/4 v5, 0x1

    #@7
    .line 243
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@a
    .line 77
    const/high16 v3, 0x3f80

    #@c
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@e
    .line 78
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@10
    .line 79
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowingTargetState:Z

    #@12
    .line 80
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWasChallengeShowing:Z

    #@14
    .line 81
    iput-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@16
    .line 100
    const/4 v3, -0x1

    #@17
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@19
    .line 128
    const/4 v3, 0x1

    #@1a
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFrameAnimationTarget:F

    #@1c
    .line 135
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveExternal:Z

    #@1e
    .line 136
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@20
    .line 140
    iput-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityBlockDrag:Z

    #@22
    .line 172
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$4;

    #@24
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$4;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)V

    #@27
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEndScrollRunnable:Ljava/lang/Runnable;

    #@29
    .line 178
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$5;

    #@2b
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$5;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)V

    #@2e
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimClickListener:Landroid/view/View$OnClickListener;

    #@30
    .line 185
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$6;

    #@32
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$6;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)V

    #@35
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeClickListener:Landroid/view/View$OnClickListener;

    #@37
    .line 245
    new-instance v3, Landroid/widget/Scroller;

    #@39
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->sMotionInterpolator:Landroid/view/animation/Interpolator;

    #@3b
    invoke-direct {v3, p1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    #@3e
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@40
    .line 247
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@43
    move-result-object v2

    #@44
    .line 248
    .local v2, vc:Landroid/view/ViewConfiguration;
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@47
    move-result v3

    #@48
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mMinVelocity:I

    #@4a
    .line 249
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@4d
    move-result v3

    #@4e
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mMaxVelocity:I

    #@50
    .line 251
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getResources()Landroid/content/res/Resources;

    #@53
    move-result-object v1

    #@54
    .line 253
    .local v1, res:Landroid/content/res/Resources;
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@56
    invoke-direct {v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;-><init>(Landroid/content/Context;)V

    #@59
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@5b
    .line 254
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@5d
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@60
    move-result-object v3

    #@61
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@63
    .line 256
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@65
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@67
    if-eq v3, v4, :cond_75

    #@69
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@6b
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@6d
    if-eq v3, v4, :cond_75

    #@6f
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@71
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@73
    if-ne v3, v4, :cond_be

    #@75
    .line 257
    :cond_75
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityBlockDrag:Z

    #@77
    .line 258
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleEdgeSlop:I

    #@79
    .line 263
    :goto_79
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@80
    move-result v3

    #@81
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mTouchSlop:I

    #@83
    .line 264
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mTouchSlop:I

    #@85
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mTouchSlop:I

    #@87
    mul-int/2addr v3, v4

    #@88
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mTouchSlopSquare:I

    #@8a
    .line 266
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@8d
    move-result-object v3

    #@8e
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@90
    .line 267
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@92
    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    #@94
    .line 270
    .local v0, density:F
    mul-float v3, v9, v0

    #@96
    add-float/2addr v3, v6

    #@97
    float-to-int v3, v3

    #@98
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleClosedAbove:I

    #@9a
    .line 271
    mul-float v3, v8, v0

    #@9c
    add-float/2addr v3, v6

    #@9d
    float-to-int v3, v3

    #@9e
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleClosedBelow:I

    #@a0
    .line 272
    mul-float v3, v9, v0

    #@a2
    add-float/2addr v3, v6

    #@a3
    float-to-int v3, v3

    #@a4
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleOpenAbove:I

    #@a6
    .line 273
    mul-float v3, v8, v0

    #@a8
    add-float/2addr v3, v6

    #@a9
    float-to-int v3, v3

    #@aa
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleOpenBelow:I

    #@ac
    .line 276
    const v3, 0x105006b

    #@af
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@b2
    move-result v3

    #@b3
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@b5
    .line 278
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setWillNotDraw(Z)V

    #@b8
    .line 279
    const/16 v3, 0x100

    #@ba
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setSystemUiVisibility(I)V

    #@bd
    .line 280
    return-void

    #@be
    .line 260
    .end local v0           #density:F
    :cond_be
    const v3, 0x1050076

    #@c1
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@c4
    move-result v3

    #@c5
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleEdgeSlop:I

    #@c7
    goto :goto_79
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->onFadeStart(Z)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->onFadeEnd(Z)V

    #@3
    return-void
.end method

.method private cancelTransitionsInProgress()V
    .registers 2

    #@0
    .prologue
    .line 1055
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    .line 1056
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@a
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    #@d
    .line 1057
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->completeChallengeScroll()V

    #@10
    .line 1059
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@12
    if-eqz v0, :cond_19

    #@14
    .line 1060
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@16
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    #@19
    .line 1062
    :cond_19
    return-void
.end method

.method private crossedDragHandle(FFF)Z
    .registers 10
    .parameter "x"
    .parameter "y"
    .parameter "initialY"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 775
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@4
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getTop()I

    #@7
    move-result v0

    #@8
    .line 776
    .local v0, challengeTop:I
    const/4 v5, 0x0

    #@9
    cmpl-float v5, p1, v5

    #@b
    if-ltz v5, :cond_36

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getWidth()I

    #@10
    move-result v5

    #@11
    int-to-float v5, v5

    #@12
    cmpg-float v5, p1, v5

    #@14
    if-gez v5, :cond_36

    #@16
    move v1, v3

    #@17
    .line 779
    .local v1, horizOk:Z
    :goto_17
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@19
    if-eqz v5, :cond_3a

    #@1b
    .line 780
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getDragHandleSizeAbove()I

    #@1e
    move-result v5

    #@1f
    sub-int v5, v0, v5

    #@21
    int-to-float v5, v5

    #@22
    cmpg-float v5, p3, v5

    #@24
    if-gez v5, :cond_38

    #@26
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getDragHandleSizeBelow()I

    #@29
    move-result v5

    #@2a
    add-int/2addr v5, v0

    #@2b
    int-to-float v5, v5

    #@2c
    cmpl-float v5, p2, v5

    #@2e
    if-lez v5, :cond_38

    #@30
    move v2, v3

    #@31
    .line 786
    .local v2, vertOk:Z
    :goto_31
    if-eqz v1, :cond_53

    #@33
    if-eqz v2, :cond_53

    #@35
    :goto_35
    return v3

    #@36
    .end local v1           #horizOk:Z
    .end local v2           #vertOk:Z
    :cond_36
    move v1, v4

    #@37
    .line 776
    goto :goto_17

    #@38
    .restart local v1       #horizOk:Z
    :cond_38
    move v2, v4

    #@39
    .line 780
    goto :goto_31

    #@3a
    .line 783
    :cond_3a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getDragHandleSizeBelow()I

    #@3d
    move-result v5

    #@3e
    add-int/2addr v5, v0

    #@3f
    int-to-float v5, v5

    #@40
    cmpl-float v5, p3, v5

    #@42
    if-lez v5, :cond_51

    #@44
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getDragHandleSizeAbove()I

    #@47
    move-result v5

    #@48
    sub-int v5, v0, v5

    #@4a
    int-to-float v5, v5

    #@4b
    cmpg-float v5, p2, v5

    #@4d
    if-gez v5, :cond_51

    #@4f
    move v2, v3

    #@50
    .restart local v2       #vertOk:Z
    :goto_50
    goto :goto_31

    #@51
    .end local v2           #vertOk:Z
    :cond_51
    move v2, v4

    #@52
    goto :goto_50

    #@53
    .restart local v2       #vertOk:Z
    :cond_53
    move v3, v4

    #@54
    .line 786
    goto :goto_35
.end method

.method private getChallengeAlpha()F
    .registers 4

    #@0
    .prologue
    const/high16 v2, 0x3f80

    #@2
    .line 548
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@4
    sub-float v0, v1, v2

    #@6
    .line 549
    .local v0, x:F
    mul-float v1, v0, v0

    #@8
    mul-float/2addr v1, v0

    #@9
    add-float/2addr v1, v2

    #@a
    return v1
.end method

.method private getChallengeBottom()I
    .registers 2

    #@0
    .prologue
    .line 1193
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 1195
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getBottom()I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method private getChallengeMargin(Z)I
    .registers 3
    .parameter "expanded"

    #@0
    .prologue
    .line 544
    if-eqz p1, :cond_8

    #@2
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasGlowpad:Z

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleEdgeSlop:I

    #@a
    goto :goto_7
.end method

.method private getDragHandleSizeAbove()I
    .registers 2

    #@0
    .prologue
    .line 751
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleOpenAbove:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleClosedAbove:I

    #@b
    goto :goto_8
.end method

.method private getDragHandleSizeBelow()I
    .registers 2

    #@0
    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleOpenBelow:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleClosedBelow:I

    #@b
    goto :goto_8
.end method

.method private getLayoutBottom()I
    .registers 5

    #@0
    .prologue
    .line 1182
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-nez v2, :cond_11

    #@4
    const/4 v0, 0x0

    #@5
    .line 1185
    .local v0, bottomMargin:I
    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMeasuredHeight()I

    #@8
    move-result v2

    #@9
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getPaddingBottom()I

    #@c
    move-result v3

    #@d
    sub-int/2addr v2, v3

    #@e
    sub-int v1, v2, v0

    #@10
    .line 1186
    .local v1, layoutBottom:I
    return v1

    #@11
    .line 1182
    .end local v0           #bottomMargin:I
    .end local v1           #layoutBottom:I
    :cond_11
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@13
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@19
    iget v0, v2, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->bottomMargin:I

    #@1b
    goto :goto_5
.end method

.method private getMaxChallengeBottom()I
    .registers 5

    #@0
    .prologue
    .line 1095
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-nez v2, :cond_6

    #@4
    const/4 v2, 0x0

    #@5
    .line 1099
    :goto_5
    return v2

    #@6
    .line 1096
    :cond_6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@9
    move-result v1

    #@a
    .line 1097
    .local v1, layoutBottom:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@c
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getMeasuredHeight()I

    #@f
    move-result v0

    #@10
    .line 1099
    .local v0, challengeHeight:I
    add-int v2, v1, v0

    #@12
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@14
    sub-int/2addr v2, v3

    #@15
    goto :goto_5
.end method

.method private getMinChallengeBottom()I
    .registers 2

    #@0
    .prologue
    .line 1103
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private isChallengeInteractionBlocked()Z
    .registers 2

    #@0
    .prologue
    .line 610
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveExternal:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@6
    if-nez v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isEdgeSwipeBeginEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "ev"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 738
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_8

    #@7
    .line 743
    :cond_7
    :goto_7
    return v1

    #@8
    .line 742
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@b
    move-result v0

    #@c
    .line 743
    .local v0, x:F
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleEdgeSlop:I

    #@e
    int-to-float v2, v2

    #@f
    cmpg-float v2, v0, v2

    #@11
    if-ltz v2, :cond_1f

    #@13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getWidth()I

    #@16
    move-result v2

    #@17
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragHandleEdgeSlop:I

    #@19
    sub-int/2addr v2, v3

    #@1a
    int-to-float v2, v2

    #@1b
    cmpl-float v2, v0, v2

    #@1d
    if-ltz v2, :cond_7

    #@1f
    :cond_1f
    const/4 v1, 0x1

    #@20
    goto :goto_7
.end method

.method private isInChallengeView(FF)Z
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 758
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isPointInView(FFLandroid/view/View;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private isInDragHandle(FF)Z
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@2
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isPointInView(FFLandroid/view/View;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private isPointInView(FFLandroid/view/View;)Z
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "view"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 766
    if-nez p3, :cond_4

    #@3
    .line 769
    :cond_3
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    #@7
    move-result v1

    #@8
    int-to-float v1, v1

    #@9
    cmpl-float v1, p1, v1

    #@b
    if-ltz v1, :cond_3

    #@d
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    #@10
    move-result v1

    #@11
    int-to-float v1, v1

    #@12
    cmpl-float v1, p2, v1

    #@14
    if-ltz v1, :cond_3

    #@16
    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    #@19
    move-result v1

    #@1a
    int-to-float v1, v1

    #@1b
    cmpg-float v1, p1, v1

    #@1d
    if-gez v1, :cond_3

    #@1f
    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    #@22
    move-result v1

    #@23
    int-to-float v1, v1

    #@24
    cmpg-float v1, p2, v1

    #@26
    if-gez v1, :cond_3

    #@28
    const/4 v0, 0x1

    #@29
    goto :goto_3
.end method

.method private makeChildMeasureSpec(II)I
    .registers 6
    .parameter "maxSize"
    .parameter "childDimen"

    #@0
    .prologue
    .line 792
    packed-switch p2, :pswitch_data_16

    #@3
    .line 802
    const/high16 v0, 0x4000

    #@5
    .line 803
    .local v0, mode:I
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    #@8
    move-result v1

    #@9
    .line 806
    .local v1, size:I
    :goto_9
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c
    move-result v2

    #@d
    return v2

    #@e
    .line 794
    .end local v0           #mode:I
    .end local v1           #size:I
    :pswitch_e
    const/high16 v0, -0x8000

    #@10
    .line 795
    .restart local v0       #mode:I
    move v1, p1

    #@11
    .line 796
    .restart local v1       #size:I
    goto :goto_9

    #@12
    .line 798
    .end local v0           #mode:I
    .end local v1           #size:I
    :pswitch_12
    const/high16 v0, 0x4000

    #@14
    .line 799
    .restart local v0       #mode:I
    move v1, p1

    #@15
    .line 800
    .restart local v1       #size:I
    goto :goto_9

    #@16
    .line 792
    :pswitch_data_16
    .packed-switch -0x2
        :pswitch_e
        :pswitch_12
    .end packed-switch
.end method

.method private moveChallengeTo(I)Z
    .registers 10
    .parameter "bottom"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1149
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@3
    if-eqz v4, :cond_9

    #@5
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@7
    if-nez v4, :cond_b

    #@9
    .line 1150
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 1174
    :goto_a
    return v3

    #@b
    .line 1153
    :cond_b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@e
    move-result v1

    #@f
    .line 1154
    .local v1, layoutBottom:I
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@11
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getHeight()I

    #@14
    move-result v0

    #@15
    .line 1156
    .local v0, challengeHeight:I
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMinChallengeBottom()I

    #@18
    move-result v4

    #@19
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMaxChallengeBottom()I

    #@1c
    move-result v5

    #@1d
    invoke-static {p1, v5}, Ljava/lang/Math;->min(II)I

    #@20
    move-result v5

    #@21
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@24
    move-result p1

    #@25
    .line 1159
    const/high16 v4, 0x3f80

    #@27
    sub-int v5, p1, v1

    #@29
    int-to-float v5, v5

    #@2a
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@2c
    sub-int v6, v0, v6

    #@2e
    int-to-float v6, v6

    #@2f
    div-float/2addr v5, v6

    #@30
    sub-float v2, v4, v5

    #@32
    .line 1161
    .local v2, offset:F
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@34
    .line 1162
    const/4 v4, 0x0

    #@35
    cmpl-float v4, v2, v4

    #@37
    if-lez v4, :cond_40

    #@39
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@3b
    if-nez v4, :cond_40

    #@3d
    .line 1163
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setChallengeShowing(Z)V

    #@40
    .line 1166
    :cond_40
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@42
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@44
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getLeft()I

    #@47
    move-result v5

    #@48
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@4a
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getHeight()I

    #@4d
    move-result v6

    #@4e
    sub-int v6, p1, v6

    #@50
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@52
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getRight()I

    #@55
    move-result v7

    #@56
    invoke-virtual {v4, v5, v6, v7, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->layout(IIII)V

    #@59
    .line 1169
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@5b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChallengeAlpha()F

    #@5e
    move-result v5

    #@5f
    invoke-virtual {v4, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setAlpha(F)V

    #@62
    .line 1170
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@64
    if-eqz v4, :cond_71

    #@66
    .line 1171
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@68
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6a
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getTop()I

    #@6d
    move-result v5

    #@6e
    invoke-interface {v4, v2, v5}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;->onScrollPositionChanged(FI)V

    #@71
    .line 1173
    :cond_71
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->postInvalidateOnAnimation()V

    #@74
    goto :goto_a
.end method

.method private onFadeEnd(Z)V
    .registers 5
    .parameter "show"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1119
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@5
    .line 1120
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setChallengeShowing(Z)V

    #@8
    .line 1122
    if-nez p1, :cond_11

    #@a
    .line 1123
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMaxChallengeBottom()I

    #@d
    move-result v0

    #@e
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->moveChallengeTo(I)Z

    #@11
    .line 1126
    :cond_11
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@13
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@16
    .line 1127
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@18
    .line 1128
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrollState(I)V

    #@1b
    .line 1129
    return-void
.end method

.method private onFadeStart(Z)V
    .registers 5
    .parameter "show"

    #@0
    .prologue
    .line 1108
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@3
    .line 1109
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@5
    const/4 v1, 0x2

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@a
    .line 1111
    if-eqz p1, :cond_13

    #@c
    .line 1112
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMinChallengeBottom()I

    #@f
    move-result v0

    #@10
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->moveChallengeTo(I)Z

    #@13
    .line 1115
    :cond_13
    const/4 v0, 0x3

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrollState(I)V

    #@17
    .line 1116
    return-void
.end method

.method private resetTouch()V
    .registers 2

    #@0
    .prologue
    .line 614
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@5
    .line 615
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@8
    .line 616
    const/4 v0, -0x1

    #@9
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@b
    .line 617
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@10
    .line 618
    return-void
.end method

.method private sendInitialListenerUpdates()V
    .registers 4

    #@0
    .prologue
    .line 311
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@2
    if-eqz v1, :cond_1c

    #@4
    .line 312
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6
    if-eqz v1, :cond_1d

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getTop()I

    #@d
    move-result v0

    #@e
    .line 313
    .local v0, challengeTop:I
    :goto_e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@10
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@12
    invoke-interface {v1, v2, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;->onScrollPositionChanged(FI)V

    #@15
    .line 314
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@17
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollState:I

    #@19
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;->onScrollStateChanged(I)V

    #@1c
    .line 316
    .end local v0           #challengeTop:I
    :cond_1c
    return-void

    #@1d
    .line 312
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_e
.end method

.method private setChallengeShowing(Z)V
    .registers 5
    .parameter "showChallenge"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    const/4 v1, 0x0

    #@2
    .line 439
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@4
    if-ne v0, p1, :cond_7

    #@6
    .line 468
    :cond_6
    :goto_6
    return-void

    #@7
    .line 442
    :cond_7
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@9
    .line 444
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@b
    if-eqz v0, :cond_6

    #@d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@f
    if-eqz v0, :cond_6

    #@11
    .line 451
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@13
    if-eqz v0, :cond_3f

    #@15
    .line 452
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@17
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@1a
    .line 453
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setVisibility(I)V

    #@1f
    .line 454
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@21
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_6

    #@2b
    .line 455
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2d
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->requestAccessibilityFocus()Z

    #@30
    .line 456
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@32
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@34
    const v2, 0x1040348

    #@37
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@3e
    goto :goto_6

    #@3f
    .line 460
    :cond_3f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@41
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@44
    .line 461
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@46
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setVisibility(I)V

    #@49
    .line 462
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@4b
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_6

    #@55
    .line 463
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@57
    invoke-virtual {v0}, Landroid/view/View;->requestAccessibilityFocus()Z

    #@5a
    .line 464
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@5c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@5e
    const v2, 0x1040349

    #@61
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@68
    goto :goto_6
.end method

.method private showChallenge(I)V
    .registers 7
    .parameter "velocity"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1212
    const/4 v0, 0x0

    #@3
    .line 1213
    .local v0, show:Z
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@6
    move-result v3

    #@7
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mMinVelocity:I

    #@9
    if-le v3, v4, :cond_14

    #@b
    .line 1214
    if-gez p1, :cond_12

    #@d
    move v0, v1

    #@e
    .line 1218
    :goto_e
    invoke-direct {p0, v0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(ZI)V

    #@11
    .line 1219
    return-void

    #@12
    :cond_12
    move v0, v2

    #@13
    .line 1214
    goto :goto_e

    #@14
    .line 1216
    :cond_14
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@16
    const/high16 v4, 0x3f00

    #@18
    cmpl-float v3, v3, v4

    #@1a
    if-ltz v3, :cond_1e

    #@1c
    move v0, v1

    #@1d
    :goto_1d
    goto :goto_e

    #@1e
    :cond_1e
    move v0, v2

    #@1f
    goto :goto_1d
.end method

.method private showChallenge(ZI)V
    .registers 6
    .parameter "show"
    .parameter "velocity"

    #@0
    .prologue
    .line 1222
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-nez v1, :cond_9

    #@4
    .line 1223
    const/4 v1, 0x0

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setChallengeShowing(Z)V

    #@8
    .line 1233
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1227
    :cond_9
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@b
    if-eqz v1, :cond_8

    #@d
    .line 1228
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowingTargetState:Z

    #@f
    .line 1229
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@12
    move-result v0

    #@13
    .line 1230
    .local v0, layoutBottom:I
    if-eqz p1, :cond_19

    #@15
    .end local v0           #layoutBottom:I
    :goto_15
    invoke-virtual {p0, v0, p2}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->animateChallengeTo(II)V

    #@18
    goto :goto_8

    #@19
    .restart local v0       #layoutBottom:I
    :cond_19
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@1b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getHeight()I

    #@1e
    move-result v1

    #@1f
    add-int/2addr v1, v0

    #@20
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@22
    sub-int v0, v1, v2

    #@24
    goto :goto_15
.end method


# virtual methods
.method animateChallengeTo(II)V
    .registers 16
    .parameter "y"
    .parameter "velocity"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    const/high16 v12, 0x3f80

    #@4
    .line 400
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6
    if-nez v0, :cond_9

    #@8
    .line 436
    :goto_8
    return-void

    #@9
    .line 405
    :cond_9
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->cancelTransitionsInProgress()V

    #@c
    .line 407
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@e
    .line 408
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v0, v11, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@14
    .line 409
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getBottom()I

    #@19
    move-result v2

    #@1a
    .line 410
    .local v2, sy:I
    sub-int v4, p1, v2

    #@1c
    .line 411
    .local v4, dy:I
    if-nez v4, :cond_22

    #@1e
    .line 412
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->completeChallengeScroll()V

    #@21
    goto :goto_8

    #@22
    .line 416
    :cond_22
    invoke-virtual {p0, v11}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrollState(I)V

    #@25
    .line 418
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@27
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getHeight()I

    #@2a
    move-result v7

    #@2b
    .line 419
    .local v7, childHeight:I
    div-int/lit8 v10, v7, 0x2

    #@2d
    .line 420
    .local v10, halfHeight:I
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@30
    move-result v0

    #@31
    int-to-float v0, v0

    #@32
    mul-float/2addr v0, v12

    #@33
    int-to-float v3, v7

    #@34
    div-float/2addr v0, v3

    #@35
    invoke-static {v12, v0}, Ljava/lang/Math;->min(FF)F

    #@38
    move-result v9

    #@39
    .line 421
    .local v9, distanceRatio:F
    int-to-float v0, v10

    #@3a
    int-to-float v3, v10

    #@3b
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->distanceInfluenceForSnapDuration(F)F

    #@3e
    move-result v11

    #@3f
    mul-float/2addr v3, v11

    #@40
    add-float v8, v0, v3

    #@42
    .line 424
    .local v8, distance:F
    const/4 v5, 0x0

    #@43
    .line 425
    .local v5, duration:I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@46
    move-result p2

    #@47
    .line 426
    if-lez p2, :cond_69

    #@49
    .line 427
    const/high16 v0, 0x447a

    #@4b
    int-to-float v3, p2

    #@4c
    div-float v3, v8, v3

    #@4e
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    #@51
    move-result v3

    #@52
    mul-float/2addr v0, v3

    #@53
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@56
    move-result v0

    #@57
    mul-int/lit8 v5, v0, 0x4

    #@59
    .line 432
    :goto_59
    const/16 v0, 0x258

    #@5b
    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    #@5e
    move-result v5

    #@5f
    .line 434
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@61
    move v3, v1

    #@62
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@65
    .line 435
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->postInvalidateOnAnimation()V

    #@68
    goto :goto_8

    #@69
    .line 429
    :cond_69
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@6c
    move-result v0

    #@6d
    int-to-float v0, v0

    #@6e
    int-to-float v3, v7

    #@6f
    div-float v6, v0, v3

    #@71
    .line 430
    .local v6, childDelta:F
    add-float v0, v6, v12

    #@73
    const/high16 v3, 0x42c8

    #@75
    mul-float/2addr v0, v3

    #@76
    float-to-int v5, v0

    #@77
    goto :goto_59
.end method

.method animateHandle(Z)V
    .registers 6
    .parameter "visible"

    #@0
    .prologue
    .line 296
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 297
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@6
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    #@9
    .line 298
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@c
    .line 300
    :cond_c
    if-eqz p1, :cond_17

    #@e
    const/high16 v0, 0x3f80

    #@10
    .line 301
    .local v0, targetAlpha:F
    :goto_10
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAlpha:F

    #@12
    cmpl-float v1, v0, v1

    #@14
    if-nez v1, :cond_19

    #@16
    .line 308
    :goto_16
    return-void

    #@17
    .line 300
    .end local v0           #targetAlpha:F
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_10

    #@19
    .line 304
    .restart local v0       #targetAlpha:F
    :cond_19
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->HANDLE_ALPHA:Landroid/util/Property;

    #@1b
    const/4 v2, 0x1

    #@1c
    new-array v2, v2, [F

    #@1e
    const/4 v3, 0x0

    #@1f
    aput v0, v2, v3

    #@21
    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@27
    .line 305
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@29
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->sHandleFadeInterpolator:Landroid/view/animation/Interpolator;

    #@2b
    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@2e
    .line 306
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@30
    const-wide/16 v2, 0xfa

    #@32
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@35
    .line 307
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAnimation:Landroid/animation/ObjectAnimator;

    #@37
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    #@3a
    goto :goto_16
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1254
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@2
    return v0
.end method

.method completeChallengeScroll()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 376
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowingTargetState:Z

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setChallengeShowing(Z)V

    #@6
    .line 377
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@8
    if-eqz v0, :cond_1b

    #@a
    const/high16 v0, 0x3f80

    #@c
    :goto_c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@e
    .line 378
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrollState(I)V

    #@11
    .line 379
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveInternal:Z

    #@14
    .line 380
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {v0, v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@1a
    .line 381
    return-void

    #@1b
    .line 377
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_c
.end method

.method public computeScroll()V
    .registers 3

    #@0
    .prologue
    .line 1035
    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    #@3
    .line 1037
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@5
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_1b

    #@b
    .line 1038
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@d
    if-nez v0, :cond_1c

    #@f
    .line 1040
    const-string v0, "SlidingChallengeLayout"

    #@11
    const-string v1, "Challenge view missing in computeScroll"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 1041
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@18
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    #@1b
    .line 1052
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1045
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@1e
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@21
    .line 1046
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@23
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    #@26
    move-result v0

    #@27
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->moveChallengeTo(I)Z

    #@2a
    .line 1048
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScroller:Landroid/widget/Scroller;

    #@2c
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_1b

    #@32
    .line 1049
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEndScrollRunnable:Ljava/lang/Runnable;

    #@34
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->post(Ljava/lang/Runnable;)Z

    #@37
    goto :goto_1b
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 709
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@4
    move-result v0

    #@5
    .line 710
    .local v0, action:I
    const/4 v1, 0x0

    #@6
    .line 711
    .local v1, handled:Z
    if-nez v0, :cond_a

    #@8
    .line 713
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@a
    .line 715
    :cond_a
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWidgetsView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_28

    #@e
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@10
    if-nez v2, :cond_28

    #@12
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@14
    if-nez v2, :cond_1c

    #@16
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isEdgeSwipeBeginEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_28

    #@1c
    .line 723
    :cond_1c
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@1e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWidgetsView:Landroid/view/View;

    #@20
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@23
    move-result v3

    #@24
    or-int v1, v2, v3

    #@26
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@28
    .line 726
    :cond_28
    if-nez v1, :cond_32

    #@2a
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@2c
    if-nez v2, :cond_32

    #@2e
    .line 727
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@31
    move-result v1

    #@32
    .line 730
    :cond_32
    const/4 v2, 0x1

    #@33
    if-eq v0, v2, :cond_38

    #@35
    const/4 v2, 0x3

    #@36
    if-ne v0, v2, :cond_3a

    #@38
    .line 731
    :cond_38
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEdgeCaptured:Z

    #@3a
    .line 734
    :cond_3a
    return v1
.end method

.method distanceInfluenceForSnapDuration(F)F
    .registers 6
    .parameter "f"

    #@0
    .prologue
    .line 359
    const/high16 v0, 0x3f00

    #@2
    sub-float/2addr p1, v0

    #@3
    .line 360
    float-to-double v0, p1

    #@4
    const-wide v2, 0x3fde28c7460698c7L

    #@9
    mul-double/2addr v0, v2

    #@a
    double-to-float p1, v0

    #@b
    .line 361
    float-to-double v0, p1

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-float v0, v0

    #@11
    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 1021
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    #@3
    .line 1032
    return-void
.end method

.method public fadeChallenge(Z)V
    .registers 8
    .parameter "show"

    #@0
    .prologue
    .line 1073
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-eqz v2, :cond_34

    #@4
    .line 1075
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->cancelTransitionsInProgress()V

    #@7
    .line 1076
    if-eqz p1, :cond_35

    #@9
    const/high16 v0, 0x3f80

    #@b
    .line 1077
    .local v0, alpha:F
    :goto_b
    if-eqz p1, :cond_37

    #@d
    const/16 v1, 0xa0

    #@f
    .line 1078
    .local v1, duration:I
    :goto_f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@11
    const-string v3, "alpha"

    #@13
    const/4 v4, 0x1

    #@14
    new-array v4, v4, [F

    #@16
    const/4 v5, 0x0

    #@17
    aput v0, v4, v5

    #@19
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@1c
    move-result-object v2

    #@1d
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@1f
    .line 1079
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@21
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$9;

    #@23
    invoke-direct {v3, p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$9;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;Z)V

    #@26
    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@29
    .line 1089
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@2b
    int-to-long v3, v1

    #@2c
    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@2f
    .line 1090
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mFader:Landroid/animation/ObjectAnimator;

    #@31
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    #@34
    .line 1092
    .end local v0           #alpha:F
    .end local v1           #duration:I
    :cond_34
    return-void

    #@35
    .line 1076
    :cond_35
    const/4 v0, 0x0

    #@36
    goto :goto_b

    #@37
    .line 1077
    .restart local v0       #alpha:F
    :cond_37
    const/16 v1, 0x64

    #@39
    goto :goto_f
.end method

.method public fadeInChallenge()V
    .registers 2

    #@0
    .prologue
    .line 1065
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->fadeChallenge(Z)V

    #@4
    .line 1066
    return-void
.end method

.method public fadeOutChallenge()V
    .registers 2

    #@0
    .prologue
    .line 1069
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->fadeChallenge(Z)V

    #@4
    .line 1070
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 1249
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@2
    invoke-direct {v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 1237
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1242
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@2
    if-eqz v0, :cond_c

    #@4
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@6
    check-cast p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@8
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;)V

    #@b
    :goto_b
    return-object v0

    #@c
    .restart local p1
    :cond_c
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@e
    if-eqz v0, :cond_18

    #@10
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@12
    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@14
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@17
    goto :goto_b

    #@18
    .restart local p1
    :cond_18
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@1a
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@1d
    goto :goto_b
.end method

.method public getBouncerAnimationDuration()I
    .registers 2

    #@0
    .prologue
    .line 489
    const/16 v0, 0xfa

    #@2
    return v0
.end method

.method public getMaxChallengeTop()I
    .registers 4

    #@0
    .prologue
    .line 1132
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2
    if-nez v2, :cond_6

    #@4
    const/4 v2, 0x0

    #@5
    .line 1136
    :goto_5
    return v2

    #@6
    .line 1134
    :cond_6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@9
    move-result v1

    #@a
    .line 1135
    .local v1, layoutBottom:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@c
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getMeasuredHeight()I

    #@f
    move-result v0

    #@10
    .line 1136
    .local v0, challengeHeight:I
    sub-int v2, v1, v0

    #@12
    goto :goto_5
.end method

.method public hideBouncer()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 520
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 541
    :cond_5
    :goto_5
    return-void

    #@6
    .line 521
    :cond_6
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWasChallengeShowing:Z

    #@8
    if-nez v1, :cond_d

    #@a
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(Z)V

    #@d
    .line 522
    :cond_d
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@f
    .line 524
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@11
    if-eqz v1, :cond_31

    #@13
    .line 525
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@15
    const-string v2, "alpha"

    #@17
    const/4 v3, 0x1

    #@18
    new-array v3, v3, [F

    #@1a
    const/4 v4, 0x0

    #@1b
    aput v4, v3, v5

    #@1d
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@20
    move-result-object v0

    #@21
    .line 526
    .local v0, anim:Landroid/animation/Animator;
    const-wide/16 v1, 0xfa

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@26
    .line 527
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$8;

    #@28
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$8;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)V

    #@2b
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@2e
    .line 533
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@31
    .line 535
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_31
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@33
    if-eqz v1, :cond_3c

    #@35
    .line 536
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@37
    const/16 v2, 0xfa

    #@39
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->hideBouncer(I)V

    #@3c
    .line 538
    :cond_3c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@3e
    if-eqz v1, :cond_5

    #@40
    .line 539
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@42
    invoke-interface {v1, v5}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;->onBouncerStateChanged(Z)V

    #@45
    goto :goto_5
.end method

.method public isBouncing()Z
    .registers 2

    #@0
    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@2
    return v0
.end method

.method public isChallengeOverlapping()Z
    .registers 2

    #@0
    .prologue
    .line 479
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@2
    return v0
.end method

.method public isChallengeShowing()Z
    .registers 2

    #@0
    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@2
    return v0
.end method

.method public onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 331
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    #@3
    .line 333
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@6
    .line 334
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 338
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    #@3
    .line 340
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mEndScrollRunnable:Ljava/lang/Runnable;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@8
    .line 341
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@b
    .line 342
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter "ev"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v7, -0x1

    #@4
    .line 561
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6
    if-nez v5, :cond_e

    #@8
    .line 562
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@b
    move-result-object v5

    #@c
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@e
    .line 564
    :cond_e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@10
    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@13
    .line 566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@16
    move-result v0

    #@17
    .line 567
    .local v0, action:I
    packed-switch v0, :pswitch_data_90

    #@1a
    .line 601
    :cond_1a
    :goto_1a
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@1c
    if-nez v5, :cond_24

    #@1e
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeInteractionBlocked()Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_28

    #@24
    .line 602
    :cond_24
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@26
    .line 603
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@28
    .line 606
    :cond_28
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@2a
    return v5

    #@2b
    .line 569
    :pswitch_2b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@2e
    move-result v5

    #@2f
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartX:F

    #@31
    .line 570
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@34
    move-result v5

    #@35
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@37
    .line 571
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@39
    goto :goto_1a

    #@3a
    .line 576
    :pswitch_3a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->resetTouch()V

    #@3d
    goto :goto_1a

    #@3e
    .line 580
    :pswitch_3e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@41
    move-result v1

    #@42
    .line 581
    .local v1, count:I
    const/4 v2, 0x0

    #@43
    .local v2, i:I
    :goto_43
    if-ge v2, v1, :cond_1a

    #@45
    .line 582
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    #@48
    move-result v3

    #@49
    .line 583
    .local v3, x:F
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    #@4c
    move-result v4

    #@4d
    .line 584
    .local v4, y:F
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@4f
    if-nez v5, :cond_82

    #@51
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@53
    if-ne v5, v7, :cond_82

    #@55
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@57
    invoke-direct {p0, v3, v4, v5}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->crossedDragHandle(FFF)Z

    #@5a
    move-result v5

    #@5b
    if-nez v5, :cond_67

    #@5d
    invoke-direct {p0, v3, v4}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isInChallengeView(FF)Z

    #@60
    move-result v5

    #@61
    if-eqz v5, :cond_82

    #@63
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollState:I

    #@65
    if-ne v5, v10, :cond_82

    #@67
    .line 588
    :cond_67
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@6a
    move-result v5

    #@6b
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@6d
    .line 589
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartX:F

    #@6f
    .line 590
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@71
    .line 591
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChallengeBottom()I

    #@74
    move-result v5

    #@75
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartChallengeBottom:I

    #@77
    .line 592
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@79
    .line 593
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@7b
    const/4 v6, 0x0

    #@7c
    invoke-virtual {v5, v10, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@7f
    .line 581
    :cond_7f
    :goto_7f
    add-int/lit8 v2, v2, 0x1

    #@81
    goto :goto_43

    #@82
    .line 594
    :cond_82
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@84
    if-eqz v5, :cond_7f

    #@86
    invoke-direct {p0, v3, v4}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isInChallengeView(FF)Z

    #@89
    move-result v5

    #@8a
    if-eqz v5, :cond_7f

    #@8c
    .line 595
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@8e
    goto :goto_7f

    #@8f
    .line 567
    nop

    #@90
    :pswitch_data_90
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_3a
        :pswitch_3e
        :pswitch_3a
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 32
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 969
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getPaddingLeft()I

    #@3
    move-result v16

    #@4
    .line 970
    .local v16, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getPaddingTop()I

    #@7
    move-result v18

    #@8
    .line 971
    .local v18, paddingTop:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getPaddingRight()I

    #@b
    move-result v17

    #@c
    .line 972
    .local v17, paddingRight:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getPaddingBottom()I

    #@f
    move-result v15

    #@10
    .line 973
    .local v15, paddingBottom:I
    sub-int v21, p4, p2

    #@12
    .line 974
    .local v21, width:I
    sub-int v10, p5, p3

    #@14
    .line 976
    .local v10, height:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChildCount()I

    #@17
    move-result v9

    #@18
    .line 977
    .local v9, count:I
    const/4 v11, 0x0

    #@19
    .local v11, i:I
    :goto_19
    if-ge v11, v9, :cond_ed

    #@1b
    .line 978
    move-object/from16 v0, p0

    #@1d
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v6

    #@21
    .line 980
    .local v6, child:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    #@24
    move-result v22

    #@25
    const/16 v23, 0x8

    #@27
    move/from16 v0, v22

    #@29
    move/from16 v1, v23

    #@2b
    if-ne v0, v1, :cond_30

    #@2d
    .line 977
    :goto_2d
    add-int/lit8 v11, v11, 0x1

    #@2f
    goto :goto_19

    #@30
    .line 982
    :cond_30
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@33
    move-result-object v14

    #@34
    check-cast v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@36
    .line 984
    .local v14, lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    iget v0, v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@38
    move/from16 v22, v0

    #@3a
    const/16 v23, 0x2

    #@3c
    move/from16 v0, v22

    #@3e
    move/from16 v1, v23

    #@40
    if-ne v0, v1, :cond_91

    #@42
    .line 987
    add-int v22, v16, v21

    #@44
    sub-int v22, v22, v17

    #@46
    div-int/lit8 v5, v22, 0x2

    #@48
    .line 988
    .local v5, center:I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    #@4b
    move-result v8

    #@4c
    .line 989
    .local v8, childWidth:I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    #@4f
    move-result v7

    #@50
    .line 990
    .local v7, childHeight:I
    div-int/lit8 v22, v8, 0x2

    #@52
    sub-int v13, v5, v22

    #@54
    .line 991
    .local v13, left:I
    sub-int v22, v10, v15

    #@56
    iget v0, v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->bottomMargin:I

    #@58
    move/from16 v23, v0

    #@5a
    sub-int v12, v22, v23

    #@5c
    .line 994
    .local v12, layoutBottom:I
    move-object/from16 v0, p0

    #@5e
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@60
    move/from16 v22, v0

    #@62
    sub-int v22, v7, v22

    #@64
    move/from16 v0, v22

    #@66
    int-to-float v0, v0

    #@67
    move/from16 v22, v0

    #@69
    const/high16 v23, 0x3f80

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeOffset:F

    #@6f
    move/from16 v24, v0

    #@71
    sub-float v23, v23, v24

    #@73
    mul-float v22, v22, v23

    #@75
    move/from16 v0, v22

    #@77
    float-to-int v0, v0

    #@78
    move/from16 v22, v0

    #@7a
    add-int v4, v12, v22

    #@7c
    .line 996
    .local v4, bottom:I
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChallengeAlpha()F

    #@7f
    move-result v22

    #@80
    move/from16 v0, v22

    #@82
    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    #@85
    .line 997
    sub-int v22, v4, v7

    #@87
    add-int v23, v13, v8

    #@89
    move/from16 v0, v22

    #@8b
    move/from16 v1, v23

    #@8d
    invoke-virtual {v6, v13, v0, v1, v4}, Landroid/view/View;->layout(IIII)V

    #@90
    goto :goto_2d

    #@91
    .line 998
    .end local v4           #bottom:I
    .end local v5           #center:I
    .end local v7           #childHeight:I
    .end local v8           #childWidth:I
    .end local v12           #layoutBottom:I
    .end local v13           #left:I
    :cond_91
    iget v0, v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@93
    move/from16 v22, v0

    #@95
    const/16 v23, 0x6

    #@97
    move/from16 v0, v22

    #@99
    move/from16 v1, v23

    #@9b
    if-ne v0, v1, :cond_c8

    #@9d
    .line 999
    add-int v22, v16, v21

    #@9f
    sub-int v22, v22, v17

    #@a1
    div-int/lit8 v5, v22, 0x2

    #@a3
    .line 1000
    .restart local v5       #center:I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    #@a6
    move-result v22

    #@a7
    div-int/lit8 v22, v22, 0x2

    #@a9
    sub-int v13, v5, v22

    #@ab
    .line 1001
    .restart local v13       #left:I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    #@ae
    move-result v22

    #@af
    add-int v19, v13, v22

    #@b1
    .line 1002
    .local v19, right:I
    sub-int v22, v10, v15

    #@b3
    iget v0, v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->bottomMargin:I

    #@b5
    move/from16 v23, v0

    #@b7
    sub-int v4, v22, v23

    #@b9
    .line 1003
    .restart local v4       #bottom:I
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    #@bc
    move-result v22

    #@bd
    sub-int v20, v4, v22

    #@bf
    .line 1004
    .local v20, top:I
    move/from16 v0, v20

    #@c1
    move/from16 v1, v19

    #@c3
    invoke-virtual {v6, v13, v0, v1, v4}, Landroid/view/View;->layout(IIII)V

    #@c6
    goto/16 :goto_2d

    #@c8
    .line 1007
    .end local v4           #bottom:I
    .end local v5           #center:I
    .end local v13           #left:I
    .end local v19           #right:I
    .end local v20           #top:I
    :cond_c8
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@ca
    move/from16 v22, v0

    #@cc
    add-int v22, v22, v16

    #@ce
    iget v0, v14, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->topMargin:I

    #@d0
    move/from16 v23, v0

    #@d2
    add-int v23, v23, v18

    #@d4
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    #@d7
    move-result v24

    #@d8
    add-int v24, v24, v16

    #@da
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    #@dd
    move-result v25

    #@de
    add-int v25, v25, v18

    #@e0
    move/from16 v0, v22

    #@e2
    move/from16 v1, v23

    #@e4
    move/from16 v2, v24

    #@e6
    move/from16 v3, v25

    #@e8
    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    #@eb
    goto/16 :goto_2d

    #@ed
    .line 1014
    .end local v6           #child:Landroid/view/View;
    .end local v14           #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    :cond_ed
    move-object/from16 v0, p0

    #@ef
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@f1
    move/from16 v22, v0

    #@f3
    if-nez v22, :cond_fd

    #@f5
    .line 1015
    const/16 v22, 0x1

    #@f7
    move/from16 v0, v22

    #@f9
    move-object/from16 v1, p0

    #@fb
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@fd
    .line 1017
    :cond_fd
    return-void
.end method

.method protected onMeasure(II)V
    .registers 35
    .parameter "widthSpec"
    .parameter "heightSpec"

    #@0
    .prologue
    .line 811
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v3

    #@4
    const/high16 v4, 0x4000

    #@6
    if-ne v3, v4, :cond_10

    #@8
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v3

    #@c
    const/high16 v4, 0x4000

    #@e
    if-eq v3, v4, :cond_18

    #@10
    .line 813
    :cond_10
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v4, "SlidingChallengeLayout must be measured with an exact size"

    #@14
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3

    #@18
    .line 817
    :cond_18
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1b
    move-result v29

    #@1c
    .line 818
    .local v29, width:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1f
    move-result v18

    #@20
    .line 819
    .local v18, height:I
    move-object/from16 v0, p0

    #@22
    move/from16 v1, v29

    #@24
    move/from16 v2, v18

    #@26
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setMeasuredDimension(II)V

    #@29
    .line 822
    move-object/from16 v0, p0

    #@2b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2d
    move-object/from16 v24, v0

    #@2f
    .line 823
    .local v24, oldChallengeView:Landroid/view/View;
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@33
    move-object/from16 v25, v0

    #@35
    .line 824
    .local v25, oldExpandChallengeView:Landroid/view/View;
    const/4 v3, 0x0

    #@36
    move-object/from16 v0, p0

    #@38
    iput-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@3a
    .line 825
    const/4 v3, 0x0

    #@3b
    move-object/from16 v0, p0

    #@3d
    iput-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@3f
    .line 826
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChildCount()I

    #@42
    move-result v15

    #@43
    .line 830
    .local v15, count:I
    const/16 v19, 0x0

    #@45
    .local v19, i:I
    :goto_45
    move/from16 v0, v19

    #@47
    if-ge v0, v15, :cond_12a

    #@49
    .line 831
    move-object/from16 v0, p0

    #@4b
    move/from16 v1, v19

    #@4d
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@50
    move-result-object v9

    #@51
    .line 832
    .local v9, child:Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@54
    move-result-object v21

    #@55
    check-cast v21, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@57
    .line 833
    .local v21, lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    move-object/from16 v0, v21

    #@59
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@5b
    const/4 v4, 0x2

    #@5c
    if-ne v3, v4, :cond_d4

    #@5e
    .line 834
    move-object/from16 v0, p0

    #@60
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@62
    if-eqz v3, :cond_6c

    #@64
    .line 835
    new-instance v3, Ljava/lang/IllegalStateException;

    #@66
    const-string v4, "There may only be one child with layout_isChallenge=\"true\""

    #@68
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v3

    #@6c
    .line 838
    :cond_6c
    instance-of v3, v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6e
    if-nez v3, :cond_78

    #@70
    .line 839
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@72
    const-string v4, "Challenge must be a KeyguardSecurityContainer"

    #@74
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@77
    throw v3

    #@78
    :cond_78
    move-object v3, v9

    #@79
    .line 842
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@7b
    move-object/from16 v0, p0

    #@7d
    iput-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@7f
    .line 843
    move-object/from16 v0, p0

    #@81
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@83
    move-object/from16 v0, v24

    #@85
    if-eq v3, v0, :cond_95

    #@87
    .line 844
    move-object/from16 v0, p0

    #@89
    iget-object v4, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@8f
    if-eqz v3, :cond_bf

    #@91
    const/4 v3, 0x0

    #@92
    :goto_92
    invoke-virtual {v4, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setVisibility(I)V

    #@95
    .line 847
    :cond_95
    move-object/from16 v0, p0

    #@97
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@99
    if-nez v3, :cond_bc

    #@9b
    .line 849
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@9d
    if-eqz v3, :cond_c3

    #@9f
    .line 850
    const v3, 0x20d005a

    #@a2
    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@a5
    move-result-object v3

    #@a6
    if-eqz v3, :cond_c1

    #@a8
    const/4 v3, 0x1

    #@a9
    :goto_a9
    move-object/from16 v0, p0

    #@ab
    iput-boolean v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasGlowpad:Z

    #@ad
    .line 854
    :goto_ad
    const/4 v3, 0x1

    #@ae
    move-object/from16 v0, p0

    #@b0
    invoke-direct {v0, v3}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChallengeMargin(Z)I

    #@b3
    move-result v3

    #@b4
    move-object/from16 v0, v21

    #@b6
    iput v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->rightMargin:I

    #@b8
    move-object/from16 v0, v21

    #@ba
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@bc
    .line 830
    :cond_bc
    :goto_bc
    add-int/lit8 v19, v19, 0x1

    #@be
    goto :goto_45

    #@bf
    .line 844
    :cond_bf
    const/4 v3, 0x4

    #@c0
    goto :goto_92

    #@c1
    .line 850
    :cond_c1
    const/4 v3, 0x0

    #@c2
    goto :goto_a9

    #@c3
    .line 852
    :cond_c3
    const v3, 0x102030b

    #@c6
    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@c9
    move-result-object v3

    #@ca
    if-eqz v3, :cond_d2

    #@cc
    const/4 v3, 0x1

    #@cd
    :goto_cd
    move-object/from16 v0, p0

    #@cf
    iput-boolean v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasGlowpad:Z

    #@d1
    goto :goto_ad

    #@d2
    :cond_d2
    const/4 v3, 0x0

    #@d3
    goto :goto_cd

    #@d4
    .line 856
    :cond_d4
    move-object/from16 v0, v21

    #@d6
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@d8
    const/4 v4, 0x6

    #@d9
    if-ne v3, v4, :cond_111

    #@db
    .line 857
    move-object/from16 v0, p0

    #@dd
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@df
    if-eqz v3, :cond_e9

    #@e1
    .line 858
    new-instance v3, Ljava/lang/IllegalStateException;

    #@e3
    const-string v4, "There may only be one child with layout_childType=\"expandChallengeHandle\""

    #@e5
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e8
    throw v3

    #@e9
    .line 862
    :cond_e9
    move-object/from16 v0, p0

    #@eb
    iput-object v9, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@ed
    .line 863
    move-object/from16 v0, p0

    #@ef
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@f1
    move-object/from16 v0, v25

    #@f3
    if-eq v3, v0, :cond_bc

    #@f5
    .line 864
    move-object/from16 v0, p0

    #@f7
    iget-object v4, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@fd
    if-eqz v3, :cond_10f

    #@ff
    const/4 v3, 0x4

    #@100
    :goto_100
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    #@103
    .line 865
    move-object/from16 v0, p0

    #@105
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@107
    move-object/from16 v0, p0

    #@109
    iget-object v4, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeClickListener:Landroid/view/View$OnClickListener;

    #@10b
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@10e
    goto :goto_bc

    #@10f
    .line 864
    :cond_10f
    const/4 v3, 0x0

    #@110
    goto :goto_100

    #@111
    .line 867
    :cond_111
    move-object/from16 v0, v21

    #@113
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@115
    const/4 v4, 0x4

    #@116
    if-ne v3, v4, :cond_11e

    #@118
    .line 868
    move-object/from16 v0, p0

    #@11a
    invoke-virtual {v0, v9}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrimView(Landroid/view/View;)V

    #@11d
    goto :goto_bc

    #@11e
    .line 869
    :cond_11e
    move-object/from16 v0, v21

    #@120
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@122
    const/4 v4, 0x5

    #@123
    if-ne v3, v4, :cond_bc

    #@125
    .line 870
    move-object/from16 v0, p0

    #@127
    iput-object v9, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWidgetsView:Landroid/view/View;

    #@129
    goto :goto_bc

    #@12a
    .line 877
    .end local v9           #child:Landroid/view/View;
    .end local v21           #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    :cond_12a
    move-object/from16 v0, p0

    #@12c
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@12e
    if-eqz v3, :cond_1fc

    #@130
    move-object/from16 v0, p0

    #@132
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@134
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getVisibility()I

    #@137
    move-result v3

    #@138
    const/16 v4, 0x8

    #@13a
    if-eq v3, v4, :cond_1fc

    #@13c
    .line 884
    move/from16 v7, p2

    #@13e
    .line 885
    .local v7, challengeHeightSpec:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getRootView()Landroid/view/View;

    #@141
    move-result-object v26

    #@142
    .line 886
    .local v26, root:Landroid/view/View;
    if-eqz v26, :cond_1ef

    #@144
    .line 887
    move-object/from16 v0, p0

    #@146
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@148
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@14b
    move-result-object v21

    #@14c
    check-cast v21, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@14e
    .line 888
    .restart local v21       #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@151
    move-result v27

    #@152
    .line 889
    .local v27, specSize:I
    move-object/from16 v0, p0

    #@154
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@156
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@158
    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingTop()I

    #@15b
    move-result v4

    #@15c
    sub-int v30, v3, v4

    #@15e
    .line 890
    .local v30, windowHeight:I
    sub-int v16, v30, v27

    #@160
    .line 893
    .local v16, diff:I
    move-object/from16 v0, p0

    #@162
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@164
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@166
    if-eq v3, v4, :cond_178

    #@168
    move-object/from16 v0, p0

    #@16a
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@16c
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@16e
    if-eq v3, v4, :cond_178

    #@170
    move-object/from16 v0, p0

    #@172
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@174
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@176
    if-ne v3, v4, :cond_23e

    #@178
    .line 895
    :cond_178
    const/4 v3, 0x0

    #@179
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17c
    move-result-object v14

    #@17d
    .line 896
    .local v14, configSWkey:Ljava/lang/Boolean;
    move-object/from16 v0, p0

    #@17f
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@181
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@184
    move-result-object v3

    #@185
    const v4, 0x105000c

    #@188
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@18b
    move-result v28

    #@18c
    .line 897
    .local v28, statusBarHeight:I
    move-object/from16 v0, p0

    #@18e
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@190
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@193
    move-result-object v3

    #@194
    const v4, 0x105000d

    #@197
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@19a
    move-result v23

    #@19b
    .line 899
    .local v23, naviBarHeight:I
    :try_start_19b
    move-object/from16 v0, p0

    #@19d
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mContext:Landroid/content/Context;

    #@19f
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1a2
    move-result-object v3

    #@1a3
    const v4, 0x111003c

    #@1a6
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1a9
    move-result v3

    #@1aa
    const/4 v4, 0x1

    #@1ab
    if-ne v3, v4, :cond_1b2

    #@1ad
    .line 900
    const/4 v3, 0x1

    #@1ae
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1b1
    .catch Ljava/lang/Exception; {:try_start_19b .. :try_end_1b1} :catch_215

    #@1b1
    move-result-object v14

    #@1b2
    .line 905
    :cond_1b2
    :goto_1b2
    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    #@1b5
    move-result v3

    #@1b6
    const/4 v4, 0x1

    #@1b7
    if-ne v3, v4, :cond_221

    #@1b9
    .line 906
    const/4 v3, 0x0

    #@1ba
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1bd
    move-result-object v20

    #@1be
    .line 907
    .local v20, isNavibarhidden:Ljava/lang/Boolean;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1c1
    move-result-object v3

    #@1c2
    if-eqz v3, :cond_1d6

    #@1c4
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1c7
    move-result-object v3

    #@1c8
    const-string v4, "config_feature_remove_navibar"

    #@1ca
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@1cd
    move-result v3

    #@1ce
    const/4 v4, 0x1

    #@1cf
    if-ne v3, v4, :cond_1d6

    #@1d1
    .line 908
    const/4 v3, 0x1

    #@1d2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1d5
    move-result-object v20

    #@1d6
    .line 910
    :cond_1d6
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    #@1d9
    move-result v3

    #@1da
    const/4 v4, 0x1

    #@1db
    if-ne v3, v4, :cond_21e

    #@1dd
    .line 911
    sub-int v3, v23, v28

    #@1df
    add-int v22, v30, v3

    #@1e1
    .line 929
    .end local v14           #configSWkey:Ljava/lang/Boolean;
    .end local v20           #isNavibarhidden:Ljava/lang/Boolean;
    .end local v23           #naviBarHeight:I
    .end local v28           #statusBarHeight:I
    .local v22, maxChallengeHeight:I
    :goto_1e1
    if-lez v22, :cond_1ef

    #@1e3
    .line 930
    move-object/from16 v0, v21

    #@1e5
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    move/from16 v1, v22

    #@1eb
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->makeChildMeasureSpec(II)I

    #@1ee
    move-result v7

    #@1ef
    .line 933
    .end local v16           #diff:I
    .end local v21           #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    .end local v22           #maxChallengeHeight:I
    .end local v27           #specSize:I
    .end local v30           #windowHeight:I
    :cond_1ef
    move-object/from16 v0, p0

    #@1f1
    iget-object v4, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@1f3
    const/4 v6, 0x0

    #@1f4
    const/4 v8, 0x0

    #@1f5
    move-object/from16 v3, p0

    #@1f7
    move/from16 v5, p1

    #@1f9
    invoke-virtual/range {v3 .. v8}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@1fc
    .line 937
    .end local v7           #challengeHeightSpec:I
    .end local v26           #root:Landroid/view/View;
    :cond_1fc
    const/16 v19, 0x0

    #@1fe
    :goto_1fe
    move/from16 v0, v19

    #@200
    if-ge v0, v15, :cond_28e

    #@202
    .line 938
    move-object/from16 v0, p0

    #@204
    move/from16 v1, v19

    #@206
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@209
    move-result-object v9

    #@20a
    .line 939
    .restart local v9       #child:Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    #@20d
    move-result v3

    #@20e
    const/16 v4, 0x8

    #@210
    if-ne v3, v4, :cond_245

    #@212
    .line 937
    :cond_212
    :goto_212
    add-int/lit8 v19, v19, 0x1

    #@214
    goto :goto_1fe

    #@215
    .line 902
    .end local v9           #child:Landroid/view/View;
    .restart local v7       #challengeHeightSpec:I
    .restart local v14       #configSWkey:Ljava/lang/Boolean;
    .restart local v16       #diff:I
    .restart local v21       #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    .restart local v23       #naviBarHeight:I
    .restart local v26       #root:Landroid/view/View;
    .restart local v27       #specSize:I
    .restart local v28       #statusBarHeight:I
    .restart local v30       #windowHeight:I
    :catch_215
    move-exception v17

    #@216
    .line 903
    .local v17, e:Ljava/lang/Exception;
    const-string v3, "SlidingChallengeLayout"

    #@218
    const-string v4, "fail to get resource R.bool.config_showNavigationBar"

    #@21a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21d
    goto :goto_1b2

    #@21e
    .line 913
    .end local v17           #e:Ljava/lang/Exception;
    .restart local v20       #isNavibarhidden:Ljava/lang/Boolean;
    :cond_21e
    move/from16 v22, v30

    #@220
    .restart local v22       #maxChallengeHeight:I
    goto :goto_1e1

    #@221
    .line 917
    .end local v20           #isNavibarhidden:Ljava/lang/Boolean;
    .end local v22           #maxChallengeHeight:I
    :cond_221
    move-object/from16 v0, p0

    #@223
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@225
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@227
    move/from16 v0, v30

    #@229
    if-ne v3, v0, :cond_22e

    #@22b
    .line 918
    sub-int v22, v30, v28

    #@22d
    .restart local v22       #maxChallengeHeight:I
    goto :goto_1e1

    #@22e
    .line 919
    .end local v22           #maxChallengeHeight:I
    :cond_22e
    move-object/from16 v0, p0

    #@230
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@232
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@234
    add-int v4, v30, v28

    #@236
    if-ne v3, v4, :cond_23b

    #@238
    .line 920
    move/from16 v22, v30

    #@23a
    .restart local v22       #maxChallengeHeight:I
    goto :goto_1e1

    #@23b
    .line 922
    .end local v22           #maxChallengeHeight:I
    :cond_23b
    move/from16 v22, v30

    #@23d
    .restart local v22       #maxChallengeHeight:I
    goto :goto_1e1

    #@23e
    .line 927
    .end local v14           #configSWkey:Ljava/lang/Boolean;
    .end local v22           #maxChallengeHeight:I
    .end local v23           #naviBarHeight:I
    .end local v28           #statusBarHeight:I
    :cond_23e
    move-object/from16 v0, v21

    #@240
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->maxHeight:I

    #@242
    sub-int v22, v3, v16

    #@244
    .restart local v22       #maxChallengeHeight:I
    goto :goto_1e1

    #@245
    .line 943
    .end local v7           #challengeHeightSpec:I
    .end local v16           #diff:I
    .end local v21           #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    .end local v22           #maxChallengeHeight:I
    .end local v26           #root:Landroid/view/View;
    .end local v27           #specSize:I
    .end local v30           #windowHeight:I
    .restart local v9       #child:Landroid/view/View;
    :cond_245
    move-object/from16 v0, p0

    #@247
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@249
    if-eq v9, v3, :cond_212

    #@24b
    .line 947
    move/from16 v10, p1

    #@24d
    .local v10, parentWidthSpec:I
    move/from16 v12, p2

    #@24f
    .line 948
    .local v12, parentHeightSpec:I
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@252
    move-result-object v21

    #@253
    check-cast v21, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;

    #@255
    .line 949
    .restart local v21       #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    move-object/from16 v0, v21

    #@257
    iget v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;->childType:I

    #@259
    const/4 v4, 0x5

    #@25a
    if-ne v3, v4, :cond_286

    #@25c
    .line 950
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getRootView()Landroid/view/View;

    #@25f
    move-result-object v26

    #@260
    .line 951
    .restart local v26       #root:Landroid/view/View;
    if-eqz v26, :cond_286

    #@262
    .line 955
    move-object/from16 v0, p0

    #@264
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@266
    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    #@268
    move/from16 v31, v0

    #@26a
    .line 956
    .local v31, windowWidth:I
    move-object/from16 v0, p0

    #@26c
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@26e
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@270
    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingTop()I

    #@273
    move-result v4

    #@274
    sub-int v30, v3, v4

    #@276
    .line 957
    .restart local v30       #windowHeight:I
    const/high16 v3, 0x4000

    #@278
    move/from16 v0, v31

    #@27a
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@27d
    move-result v10

    #@27e
    .line 959
    const/high16 v3, 0x4000

    #@280
    move/from16 v0, v30

    #@282
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@285
    move-result v12

    #@286
    .line 963
    .end local v26           #root:Landroid/view/View;
    .end local v30           #windowHeight:I
    .end local v31           #windowWidth:I
    :cond_286
    const/4 v11, 0x0

    #@287
    const/4 v13, 0x0

    #@288
    move-object/from16 v8, p0

    #@28a
    invoke-virtual/range {v8 .. v13}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@28d
    goto :goto_212

    #@28e
    .line 965
    .end local v9           #child:Landroid/view/View;
    .end local v10           #parentWidthSpec:I
    .end local v12           #parentHeightSpec:I
    .end local v21           #lp:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$LayoutParams;
    :cond_28e
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter "ev"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v10, 0x1

    #@3
    .line 622
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5
    if-nez v7, :cond_d

    #@7
    .line 623
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@a
    move-result-object v7

    #@b
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@d
    .line 625
    :cond_d
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@f
    invoke-virtual {v7, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@12
    .line 627
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@15
    move-result v0

    #@16
    .line 628
    .local v0, action:I
    packed-switch v0, :pswitch_data_fc

    #@19
    .line 697
    :cond_19
    :goto_19
    :pswitch_19
    return v10

    #@1a
    .line 630
    :pswitch_1a
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@1c
    .line 631
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@1f
    move-result v7

    #@20
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartX:F

    #@22
    .line 632
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@25
    move-result v7

    #@26
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@28
    goto :goto_19

    #@29
    .line 636
    :pswitch_29
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@2b
    if-eqz v7, :cond_36

    #@2d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeInteractionBlocked()Z

    #@30
    move-result v7

    #@31
    if-nez v7, :cond_36

    #@33
    .line 637
    invoke-direct {p0, v9}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(I)V

    #@36
    .line 639
    :cond_36
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->resetTouch()V

    #@39
    goto :goto_19

    #@3a
    .line 643
    :pswitch_3a
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@3c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@3f
    move-result v8

    #@40
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@43
    move-result v8

    #@44
    if-ne v7, v8, :cond_19

    #@46
    .line 647
    :pswitch_46
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@48
    if-eqz v7, :cond_66

    #@4a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeInteractionBlocked()Z

    #@4d
    move-result v7

    #@4e
    if-nez v7, :cond_66

    #@50
    .line 648
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@52
    const/16 v8, 0x3e8

    #@54
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mMaxVelocity:I

    #@56
    int-to-float v9, v9

    #@57
    invoke-virtual {v7, v8, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@5a
    .line 649
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5c
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@5e
    invoke-virtual {v7, v8}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@61
    move-result v7

    #@62
    float-to-int v7, v7

    #@63
    invoke-direct {p0, v7}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(I)V

    #@66
    .line 651
    :cond_66
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->resetTouch()V

    #@69
    goto :goto_19

    #@6a
    .line 655
    :pswitch_6a
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@6c
    if-nez v7, :cond_c4

    #@6e
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@70
    if-nez v7, :cond_c4

    #@72
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@74
    if-nez v7, :cond_c4

    #@76
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mSecurityBlockDrag:Z

    #@78
    if-nez v7, :cond_c4

    #@7a
    .line 656
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@7d
    move-result v1

    #@7e
    .line 657
    .local v1, count:I
    const/4 v2, 0x0

    #@7f
    .local v2, i:I
    :goto_7f
    if-ge v2, v1, :cond_c4

    #@81
    .line 658
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    #@84
    move-result v5

    #@85
    .line 659
    .local v5, x:F
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    #@88
    move-result v6

    #@89
    .line 661
    .local v6, y:F
    invoke-direct {p0, v5, v6}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isInDragHandle(FF)Z

    #@8c
    move-result v7

    #@8d
    if-nez v7, :cond_a1

    #@8f
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@91
    invoke-direct {p0, v5, v6, v7}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->crossedDragHandle(FFF)Z

    #@94
    move-result v7

    #@95
    if-nez v7, :cond_a1

    #@97
    invoke-direct {p0, v5, v6}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isInChallengeView(FF)Z

    #@9a
    move-result v7

    #@9b
    if-eqz v7, :cond_db

    #@9d
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollState:I

    #@9f
    if-ne v7, v11, :cond_db

    #@a1
    :cond_a1
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@a3
    const/4 v8, -0x1

    #@a4
    if-ne v7, v8, :cond_db

    #@a6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeInteractionBlocked()Z

    #@a9
    move-result v7

    #@aa
    if-nez v7, :cond_db

    #@ac
    .line 665
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartX:F

    #@ae
    .line 666
    iput v6, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@b0
    .line 667
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@b3
    move-result v7

    #@b4
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@b6
    .line 668
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getChallengeBottom()I

    #@b9
    move-result v7

    #@ba
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartChallengeBottom:I

    #@bc
    .line 669
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@be
    .line 670
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@c0
    const/4 v8, 0x0

    #@c1
    invoke-virtual {v7, v11, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->setLayerType(ILandroid/graphics/Paint;)V

    #@c4
    .line 676
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_c4
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mDragging:Z

    #@c6
    if-eqz v7, :cond_19

    #@c8
    .line 679
    invoke-virtual {p0, v10}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setScrollState(I)V

    #@cb
    .line 681
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mActivePointerId:I

    #@cd
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@d0
    move-result v3

    #@d1
    .line 682
    .local v3, index:I
    if-gez v3, :cond_de

    #@d3
    .line 685
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->resetTouch()V

    #@d6
    .line 686
    invoke-direct {p0, v9}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(I)V

    #@d9
    goto/16 :goto_19

    #@db
    .line 657
    .end local v3           #index:I
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v5       #x:F
    .restart local v6       #y:F
    :cond_db
    add-int/lit8 v2, v2, 0x1

    #@dd
    goto :goto_7f

    #@de
    .line 689
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v5           #x:F
    .end local v6           #y:F
    .restart local v3       #index:I
    :cond_de
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    #@e1
    move-result v6

    #@e2
    .line 690
    .restart local v6       #y:F
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartY:F

    #@e4
    sub-float v7, v6, v7

    #@e6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getLayoutBottom()I

    #@e9
    move-result v8

    #@ea
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeBottomBound:I

    #@ec
    sub-int/2addr v8, v9

    #@ed
    int-to-float v8, v8

    #@ee
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    #@f1
    move-result v4

    #@f2
    .line 693
    .local v4, pos:F
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mGestureStartChallengeBottom:I

    #@f4
    float-to-int v8, v4

    #@f5
    add-int/2addr v7, v8

    #@f6
    invoke-direct {p0, v7}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->moveChallengeTo(I)Z

    #@f9
    goto/16 :goto_19

    #@fb
    .line 628
    nop

    #@fc
    :pswitch_data_fc
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_46
        :pswitch_6a
        :pswitch_29
        :pswitch_19
        :pswitch_19
        :pswitch_3a
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 346
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6
    if-eq p1, v0, :cond_b

    #@8
    .line 349
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->hideBouncer()V

    #@b
    .line 351
    :cond_b
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@e
    .line 352
    return-void
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "allowIntercept"

    #@0
    .prologue
    .line 557
    return-void
.end method

.method public setChallengeInteractive(Z)V
    .registers 3
    .parameter "interactive"

    #@0
    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeInteractiveExternal:Z

    #@2
    .line 290
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 291
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@8
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    #@b
    .line 293
    :cond_b
    return-void
.end method

.method public setHandleAlpha(F)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 284
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mExpandChallengeView:Landroid/view/View;

    #@6
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    #@9
    .line 286
    :cond_9
    return-void
.end method

.method public setOnBouncerStateChangedListener(Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 326
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@2
    .line 327
    return-void
.end method

.method public setOnChallengeScrolledListener(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 319
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@2
    .line 320
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHasLayout:Z

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 321
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->sendInitialListenerUpdates()V

    #@9
    .line 323
    :cond_9
    return-void
.end method

.method setScrimView(Landroid/view/View;)V
    .registers 4
    .parameter "scrim"

    #@0
    .prologue
    .line 384
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 385
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@a
    .line 387
    :cond_a
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@c
    .line 388
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@e
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@10
    if-eqz v0, :cond_24

    #@12
    const/4 v0, 0x0

    #@13
    :goto_13
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    #@16
    .line 389
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@18
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    #@1c
    .line 390
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@1e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimClickListener:Landroid/view/View$OnClickListener;

    #@20
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@23
    .line 391
    return-void

    #@24
    .line 388
    :cond_24
    const/16 v0, 0x8

    #@26
    goto :goto_13
.end method

.method setScrollState(I)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 365
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollState:I

    #@2
    if-eq v0, p1, :cond_19

    #@4
    .line 366
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollState:I

    #@6
    .line 368
    if-nez p1, :cond_1a

    #@8
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@a
    if-nez v0, :cond_1a

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->animateHandle(Z)V

    #@10
    .line 369
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@12
    if-eqz v0, :cond_19

    #@14
    .line 370
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrollListener:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;

    #@16
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;->onScrollStateChanged(I)V

    #@19
    .line 373
    :cond_19
    return-void

    #@1a
    .line 368
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_d
.end method

.method public showBouncer()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 494
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 516
    :cond_5
    :goto_5
    return-void

    #@6
    .line 495
    :cond_6
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeShowing:Z

    #@8
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mWasChallengeShowing:Z

    #@a
    .line 496
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mIsBouncing:Z

    #@c
    .line 497
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(Z)V

    #@f
    .line 498
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@11
    if-eqz v1, :cond_32

    #@13
    .line 499
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mScrimView:Landroid/view/View;

    #@15
    const-string v2, "alpha"

    #@17
    new-array v3, v6, [F

    #@19
    const/4 v4, 0x0

    #@1a
    const/high16 v5, 0x3f80

    #@1c
    aput v5, v3, v4

    #@1e
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@21
    move-result-object v0

    #@22
    .line 500
    .local v0, anim:Landroid/animation/Animator;
    const-wide/16 v1, 0xfa

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@27
    .line 501
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$7;

    #@29
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$7;-><init>(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)V

    #@2c
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@2f
    .line 507
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@32
    .line 509
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_32
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@34
    if-eqz v1, :cond_3d

    #@36
    .line 510
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@38
    const/16 v2, 0xfa

    #@3a
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->showBouncer(I)V

    #@3d
    .line 513
    :cond_3d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@3f
    if-eqz v1, :cond_5

    #@41
    .line 514
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@43
    invoke-interface {v1, v6}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;->onBouncerStateChanged(Z)V

    #@46
    goto :goto_5
.end method

.method public showChallenge(Z)V
    .registers 3
    .parameter "show"

    #@0
    .prologue
    .line 1203
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(ZI)V

    #@4
    .line 1204
    if-nez p1, :cond_9

    #@6
    .line 1207
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mBlockDrag:Z

    #@9
    .line 1209
    :cond_9
    return-void
.end method
