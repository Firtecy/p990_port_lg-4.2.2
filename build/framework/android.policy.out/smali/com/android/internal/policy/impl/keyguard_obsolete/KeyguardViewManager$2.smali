.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

.field final synthetic val$lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 291
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->val$lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 293
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@2
    monitor-enter v1

    #@3
    .line 294
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->val$lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->cleanUp()V

    #@8
    .line 295
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@d
    move-result-object v0

    #@e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;->val$lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@10
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    #@13
    .line 296
    monitor-exit v1

    #@14
    .line 297
    return-void

    #@15
    .line 296
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method
