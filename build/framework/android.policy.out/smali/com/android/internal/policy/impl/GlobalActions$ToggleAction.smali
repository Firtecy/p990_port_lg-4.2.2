.class abstract Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;
.super Ljava/lang/Object;
.source "GlobalActions.java"

# interfaces
.implements Lcom/android/internal/policy/impl/GlobalActions$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/GlobalActions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ToggleAction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
    }
.end annotation


# instance fields
.field protected mDisabledIconResid:I

.field protected mDisabledStatusMessageResId:I

.field protected mEnabledIconResId:I

.field protected mEnabledStatusMessageResId:I

.field protected mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;


# direct methods
.method public constructor <init>(IIII)V
    .registers 6
    .parameter "enabledIconResId"
    .parameter "disabledIconResid"
    .parameter "enabledStatusMessageResId"
    .parameter "disabledStatusMessageResId"

    #@0
    .prologue
    .line 758
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 740
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@5
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@7
    .line 759
    iput p1, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mEnabledIconResId:I

    #@9
    .line 760
    iput p2, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mDisabledIconResid:I

    #@b
    .line 761
    iput p3, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mEnabledStatusMessageResId:I

    #@d
    .line 762
    iput p4, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mDisabledStatusMessageResId:I

    #@f
    .line 763
    return-void
.end method

.method private isVZWAdminDisabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 841
    const-string v0, "ro.build.target_operator"

    #@4
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string v1, "VZW"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    move v0, v8

    #@11
    .line 867
    :goto_11
    return v0

    #@12
    .line 844
    :cond_12
    const/4 v3, 0x0

    #@13
    .line 845
    .local v3, selection:Ljava/lang/String;
    const-string v0, "gsm.sim.operator.numeric"

    #@15
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    .line 847
    .local v7, operator:Ljava/lang/String;
    const-string v0, "311480"

    #@1b
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_23

    #@21
    move v0, v8

    #@22
    .line 848
    goto :goto_11

    #@23
    .line 850
    :cond_23
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v1, "numeric = \'"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, "\'"

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    const-string v1, " and type = \'admin\'"

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    .line 851
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, " and carrier_enabled = 1"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    .line 853
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5c
    move-result-object v0

    #@5d
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@5f
    move-object v4, v2

    #@60
    move-object v5, v2

    #@61
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@64
    move-result-object v6

    #@65
    .line 856
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_77

    #@67
    .line 857
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@6a
    move-result v0

    #@6b
    if-lez v0, :cond_72

    #@6d
    .line 858
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@70
    move v0, v8

    #@71
    .line 859
    goto :goto_11

    #@72
    .line 862
    :cond_72
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@75
    .line 863
    const/4 v0, 0x1

    #@76
    goto :goto_11

    #@77
    :cond_77
    move v0, v8

    #@78
    .line 867
    goto :goto_11
.end method


# virtual methods
.method protected changeStateFromPress(Z)V
    .registers 3
    .parameter "buttonOn"

    #@0
    .prologue
    .line 878
    if-eqz p1, :cond_7

    #@2
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@4
    :goto_4
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@6
    .line 879
    return-void

    #@7
    .line 878
    :cond_7
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@9
    goto :goto_4
.end method

.method public create(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 13
    .parameter "context"
    .parameter "convertView"
    .parameter "parent"
    .parameter "inflater"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 775
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->willCreate()V

    #@4
    .line 777
    const v5, 0x1090044

    #@7
    invoke-virtual {p4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@a
    move-result-object v4

    #@b
    .line 780
    .local v4, v:Landroid/view/View;
    const v5, 0x1020006

    #@e
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/widget/ImageView;

    #@14
    .line 781
    .local v1, icon:Landroid/widget/ImageView;
    const v5, 0x102000b

    #@17
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/widget/TextView;

    #@1d
    .line 782
    .local v2, messageView:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->isEnabled()Z

    #@20
    move-result v0

    #@21
    .line 784
    .local v0, enabled:Z
    iget-object v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@23
    sget-object v7, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@25
    if-eq v5, v7, :cond_2d

    #@27
    iget-object v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@29
    sget-object v7, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2b
    if-ne v5, v7, :cond_61

    #@2d
    :cond_2d
    const/4 v3, 0x1

    #@2e
    .line 785
    .local v3, on:Z
    :goto_2e
    if-eqz v1, :cond_42

    #@30
    .line 786
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@33
    move-result-object v7

    #@34
    if-eqz v3, :cond_63

    #@36
    iget v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mEnabledIconResId:I

    #@38
    :goto_38
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3f
    .line 788
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    #@42
    .line 790
    :cond_42
    const v5, 0x1020291

    #@45
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@48
    move-result-object v5

    #@49
    const/16 v7, 0x8

    #@4b
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    #@4e
    .line 791
    if-eqz v2, :cond_5d

    #@50
    .line 792
    if-eqz v3, :cond_66

    #@52
    iget v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mEnabledStatusMessageResId:I

    #@54
    :goto_54
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    #@57
    .line 793
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    #@5a
    .line 794
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    #@5d
    .line 796
    :cond_5d
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    #@60
    .line 798
    return-object v4

    #@61
    .end local v3           #on:Z
    :cond_61
    move v3, v6

    #@62
    .line 784
    goto :goto_2e

    #@63
    .line 786
    .restart local v3       #on:Z
    :cond_63
    iget v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mDisabledIconResid:I

    #@65
    goto :goto_38

    #@66
    .line 792
    :cond_66
    iget v5, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mDisabledStatusMessageResId:I

    #@68
    goto :goto_54
.end method

.method public isEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 828
    const-string v3, "ro.build.target_operator"

    #@4
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 829
    .local v0, operator:Ljava/lang/String;
    const-string v3, "VZW"

    #@a
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_1e

    #@10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->isVZWAdminDisabled()Z

    #@13
    move-result v3

    #@14
    if-ne v3, v1, :cond_1e

    #@16
    .line 831
    const-string v1, "GlobalActions"

    #@18
    const-string v3, "Will pass airplane activation because APN2 is Disabled"

    #@1a
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 834
    :goto_1d
    return v2

    #@1e
    :cond_1e
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@20
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->inTransition()Z

    #@23
    move-result v3

    #@24
    if-nez v3, :cond_28

    #@26
    :goto_26
    move v2, v1

    #@27
    goto :goto_1d

    #@28
    :cond_28
    move v1, v2

    #@29
    goto :goto_26
.end method

.method public onLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 823
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final onPress()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 802
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->inTransition()Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_11

    #@9
    .line 803
    const-string v2, "GlobalActions"

    #@b
    const-string v3, "shouldn\'t be able to toggle when in transition"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 820
    :goto_10
    return-void

    #@11
    .line 808
    :cond_11
    const-string v2, "ro.build.target_operator"

    #@13
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 810
    .local v1, operator:Ljava/lang/String;
    const-string v2, "VZW"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_2d

    #@1f
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->isVZWAdminDisabled()Z

    #@22
    move-result v2

    #@23
    if-ne v2, v0, :cond_2d

    #@25
    .line 812
    const-string v2, "GlobalActions"

    #@27
    const-string v3, "Will pass airplane activation because APN2 is Disabled"

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_10

    #@2d
    .line 817
    :cond_2d
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2f
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@31
    if-eq v2, v3, :cond_3a

    #@33
    .line 818
    .local v0, nowOn:Z
    :goto_33
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->onToggle(Z)V

    #@36
    .line 819
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->changeStateFromPress(Z)V

    #@39
    goto :goto_10

    #@3a
    .line 817
    .end local v0           #nowOn:Z
    :cond_3a
    const/4 v0, 0x0

    #@3b
    goto :goto_33
.end method

.method abstract onToggle(Z)V
.end method

.method public updateState(Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 884
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2
    .line 885
    return-void
.end method

.method willCreate()V
    .registers 1

    #@0
    .prologue
    .line 771
    return-void
.end method
