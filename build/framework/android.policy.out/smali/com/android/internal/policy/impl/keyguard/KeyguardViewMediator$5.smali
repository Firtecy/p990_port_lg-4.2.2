.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;
.super Landroid/os/Handler;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V
    .registers 5
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1856
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-direct {p0, p2, p3, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 1859
    iget v2, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_b4

    #@7
    .line 1933
    :goto_7
    :pswitch_7
    return-void

    #@8
    .line 1861
    :pswitch_8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v0, Landroid/os/Bundle;

    #@e
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@11
    goto :goto_7

    #@12
    .line 1864
    :pswitch_12
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@14
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@17
    goto :goto_7

    #@18
    .line 1868
    :pswitch_18
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@1a
    if-ne v1, v0, :cond_26

    #@1c
    .line 1869
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    check-cast v0, Landroid/os/Bundle;

    #@22
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@25
    goto :goto_7

    #@26
    .line 1871
    :cond_26
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@28
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a
    check-cast v0, Landroid/os/Bundle;

    #@2c
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@2f
    goto :goto_7

    #@30
    .line 1876
    :pswitch_30
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@32
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@35
    goto :goto_7

    #@36
    .line 1879
    :pswitch_36
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@38
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@3b
    goto :goto_7

    #@3c
    .line 1882
    :pswitch_3c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@3e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;

    #@42
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@45
    goto :goto_7

    #@46
    .line 1885
    :pswitch_46
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@48
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@4a
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;I)V

    #@4d
    goto :goto_7

    #@4e
    .line 1888
    :pswitch_4e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@50
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@52
    if-eqz v3, :cond_58

    #@54
    :goto_54
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V

    #@57
    goto :goto_7

    #@58
    :cond_58
    move v0, v1

    #@59
    goto :goto_54

    #@5a
    .line 1891
    :pswitch_5a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@5c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@5f
    goto :goto_7

    #@60
    .line 1894
    :pswitch_60
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@62
    invoke-virtual {v1, v0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@65
    goto :goto_7

    #@66
    .line 1897
    :pswitch_66
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@68
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@6a
    if-eqz v3, :cond_70

    #@6c
    :goto_6c
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V

    #@6f
    goto :goto_7

    #@70
    :cond_70
    move v0, v1

    #@71
    goto :goto_6c

    #@72
    .line 1900
    :pswitch_72
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@74
    monitor-enter v1

    #@75
    .line 1901
    :try_start_75
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@77
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@79
    check-cast v0, Landroid/os/Bundle;

    #@7b
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@7e
    .line 1902
    monitor-exit v1

    #@7f
    goto :goto_7

    #@80
    :catchall_80
    move-exception v0

    #@81
    monitor-exit v1
    :try_end_82
    .catchall {:try_start_75 .. :try_end_82} :catchall_80

    #@82
    throw v0

    #@83
    .line 1905
    :pswitch_83
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@85
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleShowAssistant()V

    #@88
    goto/16 :goto_7

    #@8a
    .line 1909
    :pswitch_8a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@8f
    goto/16 :goto_7

    #@91
    .line 1914
    :pswitch_91
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@93
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$4900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@96
    goto/16 :goto_7

    #@98
    .line 1919
    :pswitch_98
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@9a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$5000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@9d
    goto/16 :goto_7

    #@9f
    .line 1924
    :pswitch_9f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@a1
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a4
    move-result-object v0

    #@a5
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@a7
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setLockTimerState(I)V

    #@aa
    goto/16 :goto_7

    #@ac
    .line 1929
    :pswitch_ac
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@ae
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$5100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@b1
    goto/16 :goto_7

    #@b3
    .line 1859
    nop

    #@b4
    :pswitch_data_b4
    .packed-switch 0x2
        :pswitch_8
        :pswitch_12
        :pswitch_18
        :pswitch_30
        :pswitch_36
        :pswitch_3c
        :pswitch_46
        :pswitch_4e
        :pswitch_5a
        :pswitch_60
        :pswitch_66
        :pswitch_72
        :pswitch_83
        :pswitch_98
        :pswitch_ac
        :pswitch_8a
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_9f
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_91
    .end packed-switch
.end method
