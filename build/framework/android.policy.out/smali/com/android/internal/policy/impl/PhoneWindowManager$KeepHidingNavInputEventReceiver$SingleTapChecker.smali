.class Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SingleTapChecker"
.end annotation


# instance fields
.field downX:I

.field downY:I

.field isSingleTouchValid:Z

.field runnable:Ljava/lang/Runnable;

.field final synthetic this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

.field timeHandler:Landroid/os/Handler;

.field touchSlopSquare:I

.field waitingTouchUp:Z


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4301
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 4343
    new-instance v0, Landroid/os/Handler;

    #@8
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->timeHandler:Landroid/os/Handler;

    #@d
    .line 4344
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker$1;

    #@f
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->runnable:Ljava/lang/Runnable;

    #@14
    .line 4302
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@1a
    .line 4303
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@1c
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@1e
    mul-int/2addr v0, v1

    #@1f
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@21
    .line 4304
    const/4 v0, -0x1

    #@22
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downY:I

    #@24
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downX:I

    #@26
    .line 4305
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->isSingleTouchValid:Z

    #@28
    .line 4306
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->waitingTouchUp:Z

    #@2a
    .line 4307
    return-void
.end method


# virtual methods
.method public init(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "e"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4310
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_29

    #@7
    .line 4311
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@a
    move-result v0

    #@b
    float-to-int v0, v0

    #@c
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downX:I

    #@e
    .line 4312
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@11
    move-result v0

    #@12
    float-to-int v0, v0

    #@13
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downY:I

    #@15
    .line 4313
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->isSingleTouchValid:Z

    #@17
    .line 4314
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->waitingTouchUp:Z

    #@19
    .line 4315
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->timeHandler:Landroid/os/Handler;

    #@1b
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->runnable:Ljava/lang/Runnable;

    #@1d
    const-wide/16 v2, 0x1f4

    #@1f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@22
    .line 4316
    const-string v0, "WindowManager"

    #@24
    const-string v1, "init SingleTapChecker"

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 4318
    :cond_29
    return-void
.end method

.method public isSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter "e"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4321
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->waitingTouchUp:Z

    #@3
    if-nez v4, :cond_6

    #@5
    .line 4340
    :goto_5
    return v3

    #@6
    .line 4323
    :cond_6
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->waitingTouchUp:Z

    #@8
    .line 4325
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->isSingleTouchValid:Z

    #@a
    if-eqz v4, :cond_56

    #@c
    .line 4326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@f
    move-result v4

    #@10
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downX:I

    #@12
    int-to-float v5, v5

    #@13
    sub-float/2addr v4, v5

    #@14
    float-to-int v0, v4

    #@15
    .line 4327
    .local v0, deltaX:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@18
    move-result v4

    #@19
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->downY:I

    #@1b
    int-to-float v5, v5

    #@1c
    sub-float/2addr v4, v5

    #@1d
    float-to-int v1, v4

    #@1e
    .line 4328
    .local v1, deltaY:I
    mul-int v4, v0, v0

    #@20
    mul-int v5, v1, v1

    #@22
    add-int v2, v4, v5

    #@24
    .line 4329
    .local v2, distance:I
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@26
    if-gt v2, v4, :cond_31

    #@28
    .line 4330
    const-string v3, "WindowManager"

    #@2a
    const-string v4, "single tap up shows nav bar"

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 4331
    const/4 v3, 0x1

    #@30
    goto :goto_5

    #@31
    .line 4334
    :cond_31
    const-string v4, "WindowManager"

    #@33
    new-instance v5, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v6, "SingleTapChecker: distance exceeded = "

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    const-string v6, ", tolerance = "

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->touchSlopSquare:I

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_5

    #@56
    .line 4338
    .end local v0           #deltaX:I
    .end local v1           #deltaY:I
    .end local v2           #distance:I
    :cond_56
    const-string v4, "WindowManager"

    #@58
    const-string v5, "SingleTapChecker: ignore long click"

    #@5a
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_5
.end method
