.class public Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$KeyguardViewHost;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

.field private final mContext:Landroid/content/Context;

.field private mKeyguardHost:Landroid/widget/FrameLayout;

.field private mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

.field private final mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

.field private mNeedsInput:Z

.field private mScreenOn:Z

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

.field private final mViewManager:Landroid/view/ViewManager;

.field private mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 46
    const-string v0, "KeyguardViewManager"

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewManager;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V
    .registers 7
    .parameter "context"
    .parameter "viewManager"
    .parameter "callback"
    .parameter "keyguardViewProperties"
    .parameter "updateMonitor"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 56
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mNeedsInput:Z

    #@6
    .line 61
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mScreenOn:Z

    #@8
    .line 75
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@a
    .line 76
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@c
    .line 77
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@e
    .line 78
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

    #@10
    .line 80
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@12
    .line 81
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method


# virtual methods
.method public declared-synchronized hide()V
    .registers 6

    #@0
    .prologue
    .line 284
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@3
    if-eqz v1, :cond_21

    #@5
    .line 285
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@7
    const/16 v2, 0x8

    #@9
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@c
    .line 288
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@e
    if-eqz v1, :cond_21

    #@10
    .line 289
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@12
    .line 290
    .local v0, lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@15
    .line 291
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@17
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;

    #@19
    invoke-direct {v2, p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;)V

    #@1c
    const-wide/16 v3, 0x1f4

    #@1e
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    #@21
    .line 301
    .end local v0           #lastView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;
    :cond_21
    monitor-exit p0

    #@22
    return-void

    #@23
    .line 284
    :catchall_23
    move-exception v1

    #@24
    monitor-exit p0

    #@25
    throw v1
.end method

.method public declared-synchronized isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 307
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@3
    if-eqz v0, :cond_10

    #@5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@7
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_12

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_10

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    monitor-exit p0

    #@f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e

    #@12
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized onScreenTurnedOff()V
    .registers 2

    #@0
    .prologue
    .line 215
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mScreenOn:Z

    #@4
    .line 216
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 217
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->onScreenTurnedOff()V
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_f

    #@d
    .line 219
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 215
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method public declared-synchronized onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V
    .registers 4
    .parameter "showListener"

    #@0
    .prologue
    .line 224
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mScreenOn:Z

    #@4
    .line 225
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@6
    if-eqz v0, :cond_29

    #@8
    .line 226
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->onScreenTurnedOn()V

    #@d
    .line 230
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@f
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_21

    #@15
    .line 233
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@17
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$1;

    #@19
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V

    #@1c
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z
    :try_end_1f
    .catchall {:try_start_2 .. :try_end_1f} :catchall_26

    #@1f
    .line 248
    :goto_1f
    monitor-exit p0

    #@20
    return-void

    #@21
    .line 243
    :cond_21
    const/4 v0, 0x0

    #@22
    :try_start_22
    invoke-interface {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;->onShown(Landroid/os/IBinder;)V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_26

    #@25
    goto :goto_1f

    #@26
    .line 224
    :catchall_26
    move-exception v0

    #@27
    monitor-exit p0

    #@28
    throw v0

    #@29
    .line 246
    :cond_29
    const/4 v0, 0x0

    #@2a
    :try_start_2a
    invoke-interface {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;->onShown(Landroid/os/IBinder;)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_26

    #@2d
    goto :goto_1f
.end method

.method public declared-synchronized reset()V
    .registers 2

    #@0
    .prologue
    .line 208
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 209
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->reset()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 211
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 208
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setNeedsInput(Z)V
    .registers 5
    .parameter "needsInput"

    #@0
    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mNeedsInput:Z

    #@2
    .line 191
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@4
    if-eqz v0, :cond_1b

    #@6
    .line 192
    if-eqz p1, :cond_1c

    #@8
    .line 193
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@c
    const v2, -0x20001

    #@f
    and-int/2addr v1, v2

    #@10
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@12
    .line 199
    :goto_12
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@14
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@16
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@18
    invoke-interface {v0, v1, v2}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1b
    .line 201
    :cond_1b
    return-void

    #@1c
    .line 196
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@1e
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@20
    const/high16 v2, 0x2

    #@22
    or-int/2addr v1, v2

    #@23
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@25
    goto :goto_12
.end method

.method public declared-synchronized show()V
    .registers 12

    #@0
    .prologue
    const/high16 v10, 0x100

    #@2
    const/4 v6, 0x0

    #@3
    .line 108
    monitor-enter p0

    #@4
    :try_start_4
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v7

    #@a
    .line 109
    .local v7, res:Landroid/content/res/Resources;
    const-string v1, "lockscreen.rot_override"

    #@c
    const/4 v2, 0x0

    #@d
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_1c

    #@13
    const v1, 0x1110027

    #@16
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_1d

    #@1c
    :cond_1c
    const/4 v6, 0x1

    #@1d
    .line 112
    .local v6, enableScreenRotation:Z
    :cond_1d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@1f
    if-nez v1, :cond_77

    #@21
    .line 115
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$KeyguardViewHost;

    #@23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@25
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@27
    const/4 v5, 0x0

    #@28
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$KeyguardViewHost;-><init>(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$1;)V

    #@2b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@2d
    .line 117
    const/4 v8, -0x1

    #@2e
    .line 118
    .local v8, stretch:I
    const v4, 0x4100800

    #@31
    .line 123
    .local v4, flags:I
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mNeedsInput:Z

    #@33
    if-nez v1, :cond_38

    #@35
    .line 124
    const/high16 v1, 0x2

    #@37
    or-int/2addr v4, v1

    #@38
    .line 126
    :cond_38
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_3f

    #@3e
    .line 127
    or-int/2addr v4, v10

    #@3f
    .line 129
    :cond_3f
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@41
    const/4 v1, -0x1

    #@42
    const/4 v2, -0x1

    #@43
    const/16 v3, 0x7d4

    #@45
    const/4 v5, -0x3

    #@46
    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    #@49
    .line 132
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x10

    #@4b
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@4d
    .line 133
    const v1, 0x10301e2

    #@50
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@52
    .line 134
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@55
    move-result v1

    #@56
    if-eqz v1, :cond_63

    #@58
    .line 135
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@5a
    or-int/2addr v1, v10

    #@5b
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@5d
    .line 136
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@5f
    or-int/lit8 v1, v1, 0x2

    #@61
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@63
    .line 139
    :cond_63
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@65
    or-int/lit8 v1, v1, 0x8

    #@67
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@69
    .line 140
    const-string v1, "Keyguard"

    #@6b
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@6e
    .line 141
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@70
    .line 143
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@72
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@74
    invoke-interface {v1, v2, v0}, Landroid/view/ViewManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@77
    .line 146
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    .end local v4           #flags:I
    .end local v8           #stretch:I
    :cond_77
    if-eqz v6, :cond_f9

    #@79
    .line 148
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@7b
    const/4 v2, 0x2

    #@7c
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@7e
    .line 154
    :goto_7e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@80
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@82
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@84
    invoke-interface {v1, v2, v3}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@87
    .line 156
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@89
    if-nez v1, :cond_b8

    #@8b
    .line 158
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

    #@8d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@8f
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@91
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@93
    invoke-interface {v1, v2, v3, v5, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;->createKeyguardView(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@96
    move-result-object v1

    #@97
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@99
    .line 160
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@9b
    const v2, 0x102024f

    #@9e
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->setId(I)V

    #@a1
    .line 162
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@a3
    const/4 v1, -0x1

    #@a4
    const/4 v2, -0x1

    #@a5
    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@a8
    .line 166
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@aa
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@ac
    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@af
    .line 168
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mScreenOn:Z

    #@b1
    if-eqz v1, :cond_b8

    #@b3
    .line 169
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@b5
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->show()V

    #@b8
    .line 177
    .end local v0           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_b8
    const/high16 v9, 0x60

    #@ba
    .line 181
    .local v9, visFlags:I
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@bc
    new-instance v2, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v3, "KGVM: Set visibility on "

    #@c3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@c9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v2

    #@cd
    const-string v3, " to "

    #@cf
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v2

    #@d3
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v2

    #@d7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v2

    #@db
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 182
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@e0
    invoke-virtual {v1, v9}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    #@e3
    .line 184
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@e5
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@e7
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@e9
    invoke-interface {v1, v2, v3}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@ec
    .line 185
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@ee
    const/4 v2, 0x0

    #@ef
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@f2
    .line 186
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@f4
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->requestFocus()Z
    :try_end_f7
    .catchall {:try_start_4 .. :try_end_f7} :catchall_ff

    #@f7
    .line 187
    monitor-exit p0

    #@f8
    return-void

    #@f9
    .line 151
    .end local v9           #visFlags:I
    :cond_f9
    :try_start_f9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@fb
    const/4 v2, 0x5

    #@fc
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I
    :try_end_fe
    .catchall {:try_start_f9 .. :try_end_fe} :catchall_ff

    #@fe
    goto :goto_7e

    #@ff
    .line 108
    .end local v6           #enableScreenRotation:Z
    .end local v7           #res:Landroid/content/res/Resources;
    :catchall_ff
    move-exception v1

    #@100
    monitor-exit p0

    #@101
    throw v1
.end method

.method public declared-synchronized verifyUnlock()V
    .registers 2

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->show()V

    #@4
    .line 253
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->verifyUnlock()V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 254
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 252
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public wakeWhenReadyTq(I)Z
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 270
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->wakeWhenReadyTq(I)V

    #@9
    .line 271
    const/4 v0, 0x1

    #@a
    .line 274
    :goto_a
    return v0

    #@b
    .line 273
    :cond_b
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@d
    const-string v1, "mKeyguardView is null in wakeWhenReadyTq"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 274
    const/4 v0, 0x0

    #@13
    goto :goto_a
.end method
