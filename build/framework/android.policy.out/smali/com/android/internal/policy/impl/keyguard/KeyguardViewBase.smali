.class public abstract Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
.super Landroid/widget/FrameLayout;
.source "KeyguardViewBase.java"


# static fields
.field private static final BACKGROUND_COLOR:I = 0x70000000

.field private static final KEYGUARD_MANAGES_VOLUME:Z = true

.field private static final mBackgroundDrawable:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field protected mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field protected mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 62
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 53
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@6
    .line 88
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@8
    if-nez v0, :cond_d

    #@a
    .line 89
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->resetBackground()V

    #@d
    .line 91
    :cond_d
    return-void
.end method


# virtual methods
.method public abstract cleanUp()V
.end method

.method public abstract clearLockPattern()V
.end method

.method public abstract dismiss()V
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 154
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->interceptMediaKey(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 155
    const/4 v0, 0x1

    #@7
    .line 157
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method public dispatchSystemUiVisibilityChanged(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 266
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchSystemUiVisibilityChanged(I)V

    #@3
    .line 268
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@5
    instance-of v0, v0, Landroid/app/Activity;

    #@7
    if-nez v0, :cond_e

    #@9
    .line 269
    const/high16 v0, 0x40

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setSystemUiVisibility(I)V

    #@e
    .line 271
    :cond_e
    return-void
.end method

.method public abstract expirePasswordReset()V
.end method

.method public abstract getUserActivityTimeout()J
.end method

.method public abstract goToUserSwitcher()V
.end method

.method public abstract goToWidget(I)V
.end method

.method public abstract handleBackKey()Z
.end method

.method handleMediaKeyEvent(Landroid/view/KeyEvent;)V
    .registers 7
    .parameter "keyEvent"

    #@0
    .prologue
    .line 251
    const-string v2, "audio"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    .line 253
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_2a

    #@c
    .line 255
    :try_start_c
    invoke-interface {v0, p1}, Landroid/media/IAudioService;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_10

    #@f
    .line 262
    :goto_f
    return-void

    #@10
    .line 256
    :catch_10
    move-exception v1

    #@11
    .line 257
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "KeyguardViewBase"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "dispatchMediaKeyEvent threw exception "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_f

    #@2a
    .line 260
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_2a
    const-string v2, "KeyguardViewBase"

    #@2c
    const-string v3, "Unable to find IAudioService for media key event"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_f
.end method

.method public abstract handleMenuKey()Z
.end method

.method public abstract hideSKTLocked()V
.end method

.method interceptMediaKey(Landroid/view/KeyEvent;)Z
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/16 v6, 0x18

    #@2
    const/4 v3, -0x1

    #@3
    const/4 v2, 0x1

    #@4
    .line 169
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v0

    #@8
    .line 170
    .local v0, keyCode:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_73

    #@e
    .line 171
    sparse-switch v0, :sswitch_data_82

    #@11
    .line 247
    :cond_11
    :goto_11
    const/4 v2, 0x0

    #@12
    :cond_12
    :goto_12
    return v2

    #@13
    .line 177
    :sswitch_13
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@15
    if-nez v1, :cond_25

    #@17
    .line 178
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getContext()Landroid/content/Context;

    #@1a
    move-result-object v1

    #@1b
    const-string v3, "phone"

    #@1d
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@23
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@25
    .line 181
    :cond_25
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@27
    if-eqz v1, :cond_31

    #@29
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2b
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_12

    #@31
    .line 193
    :cond_31
    :sswitch_31
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    #@34
    goto :goto_12

    #@35
    .line 201
    :sswitch_35
    monitor-enter p0

    #@36
    .line 202
    :try_start_36
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@38
    if-nez v1, :cond_48

    #@3a
    .line 203
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getContext()Landroid/content/Context;

    #@3d
    move-result-object v1

    #@3e
    const-string v4, "audio"

    #@40
    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Landroid/media/AudioManager;

    #@46
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@48
    .line 206
    :cond_48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_36 .. :try_end_49} :catchall_5b

    #@49
    .line 208
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@4b
    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_60

    #@51
    .line 210
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@53
    const/4 v5, 0x3

    #@54
    if-ne v0, v6, :cond_5e

    #@56
    move v1, v2

    #@57
    :goto_57
    invoke-virtual {v4, v5, v1}, Landroid/media/AudioManager;->adjustLocalOrRemoteStreamVolume(II)V

    #@5a
    goto :goto_12

    #@5b
    .line 206
    :catchall_5b
    move-exception v1

    #@5c
    :try_start_5c
    monitor-exit p0
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_5b

    #@5d
    throw v1

    #@5e
    :cond_5e
    move v1, v3

    #@5f
    .line 210
    goto :goto_57

    #@60
    .line 215
    :cond_60
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@62
    invoke-virtual {v1}, Landroid/media/AudioManager;->isFMActive()Z

    #@65
    move-result v1

    #@66
    if-eqz v1, :cond_12

    #@68
    .line 216
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    #@6a
    const/16 v4, 0xa

    #@6c
    if-ne v0, v6, :cond_6f

    #@6e
    move v3, v2

    #@6f
    :cond_6f
    invoke-virtual {v1, v4, v3}, Landroid/media/AudioManager;->adjustLocalOrRemoteStreamVolume(II)V

    #@72
    goto :goto_12

    #@73
    .line 229
    :cond_73
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@76
    move-result v1

    #@77
    if-ne v1, v2, :cond_11

    #@79
    .line 230
    sparse-switch v0, :sswitch_data_bc

    #@7c
    goto :goto_11

    #@7d
    .line 242
    :sswitch_7d
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    #@80
    goto :goto_12

    #@81
    .line 171
    nop

    #@82
    :sswitch_data_82
    .sparse-switch
        0x18 -> :sswitch_35
        0x19 -> :sswitch_35
        0x4f -> :sswitch_31
        0x55 -> :sswitch_13
        0x56 -> :sswitch_31
        0x57 -> :sswitch_31
        0x58 -> :sswitch_31
        0x59 -> :sswitch_31
        0x5a -> :sswitch_31
        0x5b -> :sswitch_31
        0x7e -> :sswitch_13
        0x7f -> :sswitch_13
        0x82 -> :sswitch_31
        0xa4 -> :sswitch_35
    .end sparse-switch

    #@bc
    .line 230
    :sswitch_data_bc
    .sparse-switch
        0x4f -> :sswitch_7d
        0x55 -> :sswitch_7d
        0x56 -> :sswitch_7d
        0x57 -> :sswitch_7d
        0x58 -> :sswitch_7d
        0x59 -> :sswitch_7d
        0x5a -> :sswitch_7d
        0x5b -> :sswitch_7d
        0x7e -> :sswitch_7d
        0x7f -> :sswitch_7d
        0x82 -> :sswitch_7d
    .end sparse-switch
.end method

.method public needRecreateMe()Z
    .registers 2

    #@0
    .prologue
    .line 317
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public abstract onScreenTurnedOff()V
.end method

.method public abstract onScreenTurnedOn()V
.end method

.method public abstract reset()V
.end method

.method public resetBackground()V
    .registers 2

    #@0
    .prologue
    .line 94
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 95
    return-void
.end method

.method protected setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 2
    .parameter "utils"

    #@0
    .prologue
    .line 286
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    .line 287
    return-void
.end method

.method public setViewMediatorCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;)V
    .registers 2
    .parameter "viewMediatorCallback"

    #@0
    .prologue
    .line 275
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@2
    .line 276
    return-void
.end method

.method public abstract show()V
.end method

.method public abstract showAssistant()V
.end method

.method public abstract showNextSecurityScreenIfPresent()Z
.end method

.method public abstract showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract verifyUnlock()V
.end method

.method public abstract wakeWhenReadyTq(I)V
.end method
