.class Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.source "NextiKeyguardSelectorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 232
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDevicePolicyManagerStateChanged()V
    .registers 2

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$100(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@5
    .line 247
    return-void
.end method

.method public onRingerModeChanged(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 236
    const/4 v1, 0x2

    #@1
    if-eq v1, p1, :cond_17

    #@3
    const/4 v0, 0x1

    #@4
    .line 237
    .local v0, silent:Z
    :goto_4
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@6
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$000(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Z

    #@9
    move-result v1

    #@a
    if-eq v0, v1, :cond_16

    #@c
    .line 238
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@e
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$002(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Z)Z

    #@11
    .line 240
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@13
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateResources()V

    #@16
    .line 242
    :cond_16
    return-void

    #@17
    .line 236
    .end local v0           #silent:Z
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_4
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 3
    .parameter "simState"

    #@0
    .prologue
    .line 251
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$100(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@5
    .line 252
    return-void
.end method
