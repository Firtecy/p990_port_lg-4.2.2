.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;
.super Landroid/content/BroadcastReceiver;
.source "KeyguardSimPukView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 175
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 177
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 179
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    #@6
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_5b

    #@c
    .line 180
    const-string v2, "ss"

    #@e
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 182
    .local v1, stateExtra:Ljava/lang/String;
    const-string v2, "KeyguardSimPukView"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "[UICC] PUK unlock screen get ACTION_SIM_STATE_CHANGED: "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 184
    const-string v2, "SIM_REMOVED"

    #@2c
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_5b

    #@32
    .line 185
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@34
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)Landroid/content/Context;

    #@37
    move-result-object v2

    #@38
    if-eqz v2, :cond_5b

    #@3a
    .line 186
    const-string v2, "KeyguardSimPukView"

    #@3c
    const-string v3, "[UICC] PUK unlock screen disappeared"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 187
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@43
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@45
    if-eqz v2, :cond_4e

    #@47
    .line 188
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@49
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@4b
    invoke-virtual {v2}, Landroid/app/ProgressDialog;->hide()V

    #@4e
    .line 191
    :cond_4e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@50
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@52
    if-eqz v2, :cond_5b

    #@54
    .line 192
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@56
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@58
    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    #@5b
    .line 205
    .end local v1           #stateExtra:Ljava/lang/String;
    :cond_5b
    return-void
.end method
