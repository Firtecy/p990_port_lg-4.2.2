.class Lcom/android/internal/policy/impl/keyguard/PagedView$1;
.super Ljava/lang/Object;
.source "PagedView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->onTouchEvent(Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

.field final synthetic val$dragViewIndex:I

.field final synthetic val$pageUnderPointIndex:I


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1429
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@4
    iput p3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v14, 0x0

    #@3
    .line 1434
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@5
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@7
    iget v12, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@9
    invoke-virtual {v11, v12}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@c
    move-result v11

    #@d
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@f
    iget v13, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@11
    invoke-virtual {v12, v13}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@14
    move-result v12

    #@15
    sub-int/2addr v11, v12

    #@16
    int-to-float v11, v11

    #@17
    invoke-static {v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$002(Lcom/android/internal/policy/impl/keyguard/PagedView;F)F

    #@1a
    .line 1438
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1c
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@1e
    invoke-virtual {v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@21
    .line 1443
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@23
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@25
    if-ge v10, v11, :cond_9c

    #@27
    move v5, v8

    #@28
    .line 1444
    .local v5, shiftDelta:I
    :goto_28
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@2a
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@2c
    if-ge v10, v11, :cond_9e

    #@2e
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@30
    add-int/lit8 v2, v10, 0x1

    #@32
    .line 1446
    .local v2, lowerIndex:I
    :goto_32
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@34
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@36
    if-le v10, v11, :cond_a1

    #@38
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$dragViewIndex:I

    #@3a
    add-int/lit8 v6, v10, -0x1

    #@3c
    .line 1448
    .local v6, upperIndex:I
    :goto_3c
    move v1, v2

    #@3d
    .local v1, i:I
    :goto_3d
    if-gt v1, v6, :cond_a4

    #@3f
    .line 1449
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@41
    invoke-virtual {v10, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@44
    move-result-object v7

    #@45
    .line 1453
    .local v7, v:Landroid/view/View;
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@47
    invoke-virtual {v10}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@4a
    move-result v10

    #@4b
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@4d
    invoke-virtual {v11, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@50
    move-result v11

    #@51
    add-int v4, v10, v11

    #@53
    .line 1454
    .local v4, oldX:I
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@55
    invoke-virtual {v10}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@58
    move-result v10

    #@59
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@5b
    add-int v12, v1, v5

    #@5d
    invoke-virtual {v11, v12}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@60
    move-result v11

    #@61
    add-int v3, v10, v11

    #@63
    .line 1458
    .local v3, newX:I
    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Landroid/animation/AnimatorSet;

    #@69
    .line 1459
    .local v0, anim:Landroid/animation/AnimatorSet;
    if-eqz v0, :cond_6e

    #@6b
    .line 1460
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    #@6e
    .line 1463
    :cond_6e
    sub-int v10, v4, v3

    #@70
    int-to-float v10, v10

    #@71
    invoke-virtual {v7, v10}, Landroid/view/View;->setTranslationX(F)V

    #@74
    .line 1464
    new-instance v0, Landroid/animation/AnimatorSet;

    #@76
    .end local v0           #anim:Landroid/animation/AnimatorSet;
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    #@79
    .line 1465
    .restart local v0       #anim:Landroid/animation/AnimatorSet;
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@7b
    iget v10, v10, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_REORDER_REPOSITION_DURATION:I

    #@7d
    int-to-long v10, v10

    #@7e
    invoke-virtual {v0, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@81
    .line 1466
    new-array v10, v9, [Landroid/animation/Animator;

    #@83
    const-string v11, "translationX"

    #@85
    new-array v12, v9, [F

    #@87
    const/4 v13, 0x0

    #@88
    aput v13, v12, v14

    #@8a
    invoke-static {v7, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@8d
    move-result-object v11

    #@8e
    aput-object v11, v10, v14

    #@90
    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@93
    .line 1468
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    #@96
    .line 1469
    invoke-virtual {v7, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@99
    .line 1448
    add-int/lit8 v1, v1, 0x1

    #@9b
    goto :goto_3d

    #@9c
    .end local v0           #anim:Landroid/animation/AnimatorSet;
    .end local v1           #i:I
    .end local v2           #lowerIndex:I
    .end local v3           #newX:I
    .end local v4           #oldX:I
    .end local v5           #shiftDelta:I
    .end local v6           #upperIndex:I
    .end local v7           #v:Landroid/view/View;
    :cond_9c
    move v5, v9

    #@9d
    .line 1443
    goto :goto_28

    #@9e
    .line 1444
    .restart local v5       #shiftDelta:I
    :cond_9e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@a0
    goto :goto_32

    #@a1
    .line 1446
    .restart local v2       #lowerIndex:I
    :cond_a1
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@a3
    goto :goto_3c

    #@a4
    .line 1472
    .restart local v1       #i:I
    .restart local v6       #upperIndex:I
    :cond_a4
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@a6
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@a8
    iget-object v10, v10, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@aa
    invoke-virtual {v9, v10}, Lcom/android/internal/policy/impl/keyguard/PagedView;->removeView(Landroid/view/View;)V

    #@ad
    .line 1473
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@af
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@b1
    iget-object v10, v10, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@b3
    invoke-virtual {v9, v10, v14}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onRemoveView(Landroid/view/View;Z)V

    #@b6
    .line 1474
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@b8
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@ba
    iget-object v10, v10, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@bc
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@be
    invoke-virtual {v9, v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->addView(Landroid/view/View;I)V

    #@c1
    .line 1475
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@c3
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@c5
    iget-object v10, v10, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@c7
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->val$pageUnderPointIndex:I

    #@c9
    invoke-virtual {v9, v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onAddView(Landroid/view/View;I)V

    #@cc
    .line 1476
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@ce
    invoke-static {v9, v8}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$102(Lcom/android/internal/policy/impl/keyguard/PagedView;I)I

    #@d1
    .line 1477
    return-void
.end method
