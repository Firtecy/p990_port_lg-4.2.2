.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;
.source "KeyguardSimPukView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$CheckSimPuk;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;
    }
.end annotation


# static fields
.field private static final BUILD_TARGET_DCM:Ljava/lang/String; = "DCM"

.field private static final KEYPAD_CANCEL_FONT_SIZE_EN:F = 17.33f

.field private static final KEYPAD_CANCEL_FONT_SIZE_JA:F = 15.33f

.field private static final KEYPAD_NUMBER_FONT_SIZE:F = 23.33f

.field private static final KEYPAD_OK_FONT_SIZE:F = 17.33f

.field private static final LANGUAGE_EN:Ljava/lang/String; = "en"

.field private static final LANGUAGE_JA:Ljava/lang/String; = "ja"

.field private static final LOG_TAG:Ljava/lang/String; = "KeyguardSimPukView"

.field private static final TEXT_FONT_SIZE_LARGE:F = 23.33f

.field private static final TEXT_FONT_SIZE_MEDIUM:F = 17.33f

.field private static countrycode:Ljava/lang/String;

.field protected static mPinText:Ljava/lang/String;

.field protected static mPukText:Ljava/lang/String;

.field public static mTempString:Ljava/lang/String;

.field private static operator:Ljava/lang/String;

.field protected static state:I


# instance fields
.field private IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private currentLanguage:Ljava/lang/String;

.field private emergency_status:Z

.field protected volatile mCheckInProgress:Z

.field private mEmergencyCallButton:Landroid/widget/TextView;

.field private mEmergencyText:Landroid/widget/TextView;

.field private mHeaderText:Landroid/widget/TextView;

.field protected mPinEntry:Landroid/widget/TextView;

.field private mRetryText:Landroid/widget/TextView;

.field private mServiceState:Landroid/telephony/ServiceState;

.field protected mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

.field protected mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

.field protected mStatusBarManager:Landroid/app/StatusBarManager;

.field public mdialog:Landroid/app/AlertDialog;

.field private previousPuk1Remaining:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 166
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->countrycode:Ljava/lang/String;

    #@3
    .line 167
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->operator:Ljava/lang/String;

    #@5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 211
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 215
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 72
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@6
    .line 76
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@8
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@d
    .line 82
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@f
    .line 158
    const-string v0, ""

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->currentLanguage:Ljava/lang/String;

    #@13
    .line 165
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->emergency_status:Z

    #@16
    .line 175
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;

    #@18
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1d
    .line 216
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->previousPuk1Remaining:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->previousPuk1Remaining:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->setUIStringByOperator(II)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->setPopupStringByOperator(II)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 67
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->showPukDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private setPopupStringByOperator(II)V
    .registers 14
    .parameter "state"
    .parameter "retryCount"

    #@0
    .prologue
    const v10, 0x20902c5

    #@3
    const v9, 0x20902c4

    #@6
    const/4 v6, 0x2

    #@7
    const/4 v8, 0x0

    #@8
    const/4 v7, 0x1

    #@9
    .line 766
    const/4 v0, 0x0

    #@a
    .line 767
    .local v0, mStrTitleWrongPuk:Ljava/lang/String;
    const/4 v1, 0x0

    #@b
    .line 768
    .local v1, popupMsg:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getResources()Landroid/content/res/Resources;

    #@e
    move-result-object v2

    #@f
    .line 769
    .local v2, res:Landroid/content/res/Resources;
    const-string v3, "KeyguardSimPukView"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "[FCMANIA10] setPopupStringByOperator  state == "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 770
    packed-switch p1, :pswitch_data_2d2

    #@2a
    .line 791
    :pswitch_2a
    const-string v3, "KeyguardSimPukView"

    #@2c
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "[FCMANIA11] setPopupStringByOperator  retryCount == "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 792
    if-lez p2, :cond_241

    #@44
    .line 793
    if-ne p2, v7, :cond_bc

    #@46
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    const-string v4, "KR"

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v3

    #@50
    if-nez v3, :cond_bc

    #@52
    .line 794
    const-string v3, "KeyguardSimPukView"

    #@54
    const-string v4, "attemptsRemaining == 1"

    #@56
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 795
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@5b
    const v4, 0x2090122

    #@5e
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    .line 863
    :cond_62
    :goto_62
    if-eqz v1, :cond_75

    #@64
    .line 865
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    const-string v4, "DCM"

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v3

    #@6e
    if-eqz v3, :cond_2cc

    #@70
    if-nez p2, :cond_2cc

    #@72
    .line 866
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->showPukDialogDCM(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    .line 871
    :cond_75
    :goto_75
    return-void

    #@76
    .line 772
    :pswitch_76
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    const-string v4, "KR"

    #@7c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v3

    #@80
    if-eqz v3, :cond_8c

    #@82
    .line 773
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@84
    const v4, 0x20902cd

    #@87
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    goto :goto_62

    #@8c
    .line 775
    :cond_8c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@8e
    const v4, 0x2090113

    #@91
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@94
    move-result-object v1

    #@95
    .line 777
    goto :goto_62

    #@96
    .line 780
    :pswitch_96
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    const-string v4, "KR"

    #@9c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v3

    #@a0
    if-eqz v3, :cond_62

    #@a2
    .line 781
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@a5
    move-result-object v3

    #@a6
    const-string v4, "SKT"

    #@a8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v3

    #@ac
    if-eqz v3, :cond_b5

    #@ae
    .line 783
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@b0
    invoke-virtual {v3, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    goto :goto_62

    #@b5
    .line 786
    :cond_b5
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@b7
    invoke-virtual {v3, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@ba
    move-result-object v1

    #@bb
    goto :goto_62

    #@bc
    .line 797
    :cond_bc
    const/16 v3, 0xa

    #@be
    if-ne p2, v3, :cond_169

    #@c0
    .line 798
    const-string v3, "KeyguardSimPukView"

    #@c2
    const-string v4, "attemptsRemaining == 10"

    #@c4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 799
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@ca
    move-result-object v3

    #@cb
    const-string v4, "SKT"

    #@cd
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v3

    #@d1
    if-eqz v3, :cond_ee

    #@d3
    .line 801
    const-string v3, "KeyguardSimPukView"

    #@d5
    const-string v4, "[FCMANIA12] setPopupStringByOperator  SKT PUK 10 Times"

    #@d7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 802
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@dc
    const v4, 0x20902bd

    #@df
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e2
    move-result-object v0

    #@e3
    .line 803
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@e5
    const v4, 0x20902be

    #@e8
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@eb
    move-result-object v1

    #@ec
    goto/16 :goto_62

    #@ee
    .line 804
    :cond_ee
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f1
    move-result-object v3

    #@f2
    const-string v4, "KT"

    #@f4
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f7
    move-result v3

    #@f8
    if-eqz v3, :cond_105

    #@fa
    .line 806
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@fc
    const v4, 0x20902bf

    #@ff
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@102
    move-result-object v1

    #@103
    goto/16 :goto_62

    #@105
    .line 807
    :cond_105
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@108
    move-result-object v3

    #@109
    const-string v4, "LGU"

    #@10b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10e
    move-result v3

    #@10f
    if-eqz v3, :cond_11c

    #@111
    .line 809
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@113
    const v4, 0x20902c0

    #@116
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@119
    move-result-object v1

    #@11a
    goto/16 :goto_62

    #@11c
    .line 810
    :cond_11c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11f
    move-result-object v3

    #@120
    const-string v4, "DCM"

    #@122
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@125
    move-result v3

    #@126
    if-eqz v3, :cond_133

    #@128
    .line 811
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@12a
    const v4, 0x20902af

    #@12d
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@130
    move-result-object v1

    #@131
    goto/16 :goto_62

    #@133
    .line 812
    :cond_133
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@136
    move-result-object v3

    #@137
    const-string v4, "TMO"

    #@139
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13c
    move-result v3

    #@13d
    if-eqz v3, :cond_156

    #@13f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@142
    move-result-object v3

    #@143
    const-string v4, "US"

    #@145
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@148
    move-result v3

    #@149
    if-eqz v3, :cond_156

    #@14b
    .line 813
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@14d
    const v4, 0x20902d8

    #@150
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@153
    move-result-object v1

    #@154
    goto/16 :goto_62

    #@156
    .line 815
    :cond_156
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@158
    const v4, 0x2090123

    #@15b
    new-array v5, v7, [Ljava/lang/Object;

    #@15d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@160
    move-result-object v6

    #@161
    aput-object v6, v5, v8

    #@163
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@166
    move-result-object v1

    #@167
    goto/16 :goto_62

    #@169
    .line 818
    :cond_169
    const-string v3, "KeyguardSimPukView"

    #@16b
    const-string v4, "attemptsRemaining is not 1"

    #@16d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    .line 819
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@173
    move-result-object v3

    #@174
    const-string v4, "KR"

    #@176
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@179
    move-result v3

    #@17a
    if-eqz v3, :cond_1ef

    #@17c
    .line 820
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@17f
    move-result-object v3

    #@180
    const-string v4, "SKT"

    #@182
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@185
    move-result v3

    #@186
    if-eqz v3, :cond_1a9

    #@188
    .line 822
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@18a
    invoke-virtual {v3, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@18d
    move-result-object v0

    #@18e
    .line 823
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@190
    const v4, 0x20902c6

    #@193
    new-array v5, v6, [Ljava/lang/Object;

    #@195
    rsub-int/lit8 v6, p2, 0xa

    #@197
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19a
    move-result-object v6

    #@19b
    aput-object v6, v5, v8

    #@19d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a0
    move-result-object v6

    #@1a1
    aput-object v6, v5, v7

    #@1a3
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1a6
    move-result-object v1

    #@1a7
    goto/16 :goto_62

    #@1a9
    .line 824
    :cond_1a9
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1ac
    move-result-object v3

    #@1ad
    const-string v4, "KT"

    #@1af
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b2
    move-result v3

    #@1b3
    if-eqz v3, :cond_1c8

    #@1b5
    .line 826
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1b7
    const v4, 0x20902cf

    #@1ba
    new-array v5, v7, [Ljava/lang/Object;

    #@1bc
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bf
    move-result-object v6

    #@1c0
    aput-object v6, v5, v8

    #@1c2
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1c5
    move-result-object v1

    #@1c6
    goto/16 :goto_62

    #@1c8
    .line 827
    :cond_1c8
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1cb
    move-result-object v3

    #@1cc
    const-string v4, "LGU"

    #@1ce
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d1
    move-result v3

    #@1d2
    if-eqz v3, :cond_62

    #@1d4
    .line 829
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1d6
    const v4, 0x20902c7

    #@1d9
    new-array v5, v6, [Ljava/lang/Object;

    #@1db
    rsub-int/lit8 v6, p2, 0xa

    #@1dd
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e0
    move-result-object v6

    #@1e1
    aput-object v6, v5, v8

    #@1e3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e6
    move-result-object v6

    #@1e7
    aput-object v6, v5, v7

    #@1e9
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1ec
    move-result-object v1

    #@1ed
    goto/16 :goto_62

    #@1ef
    .line 832
    :cond_1ef
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1f2
    move-result-object v3

    #@1f3
    const-string v4, "DCM"

    #@1f5
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f8
    move-result v3

    #@1f9
    if-eqz v3, :cond_22e

    #@1fb
    .line 833
    new-instance v3, Ljava/lang/StringBuilder;

    #@1fd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@200
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@202
    const v5, 0x20902ce

    #@205
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@208
    move-result-object v4

    #@209
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v3

    #@20d
    const-string v4, "\n"

    #@20f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v3

    #@213
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@215
    const v5, 0x2090125

    #@218
    new-array v6, v7, [Ljava/lang/Object;

    #@21a
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21d
    move-result-object v7

    #@21e
    aput-object v7, v6, v8

    #@220
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@223
    move-result-object v4

    #@224
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v3

    #@228
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22b
    move-result-object v1

    #@22c
    goto/16 :goto_62

    #@22e
    .line 836
    :cond_22e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@230
    const v4, 0x2090110

    #@233
    new-array v5, v7, [Ljava/lang/Object;

    #@235
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@238
    move-result-object v6

    #@239
    aput-object v6, v5, v8

    #@23b
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@23e
    move-result-object v1

    #@23f
    goto/16 :goto_62

    #@241
    .line 840
    :cond_241
    if-nez p2, :cond_62

    #@243
    .line 841
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@246
    move-result-object v3

    #@247
    const-string v4, "KR"

    #@249
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24c
    move-result v3

    #@24d
    if-eqz v3, :cond_2aa

    #@24f
    .line 842
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@252
    move-result-object v3

    #@253
    const-string v4, "SKT"

    #@255
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@258
    move-result v3

    #@259
    if-eqz v3, :cond_26c

    #@25b
    .line 844
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@25d
    invoke-virtual {v3, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@260
    move-result-object v0

    #@261
    .line 845
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@263
    const v4, 0x20902c8

    #@266
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@269
    move-result-object v1

    #@26a
    goto/16 :goto_62

    #@26c
    .line 846
    :cond_26c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@26f
    move-result-object v3

    #@270
    const-string v4, "KT"

    #@272
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@275
    move-result v3

    #@276
    if-eqz v3, :cond_28b

    #@278
    .line 848
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@27a
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@27d
    move-result-object v3

    #@27e
    const v4, 0x20902c9

    #@281
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@284
    move-result-object v3

    #@285
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@288
    move-result-object v1

    #@289
    goto/16 :goto_62

    #@28b
    .line 849
    :cond_28b
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@28e
    move-result-object v3

    #@28f
    const-string v4, "LGU"

    #@291
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@294
    move-result v3

    #@295
    if-eqz v3, :cond_62

    #@297
    .line 851
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@299
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29c
    move-result-object v3

    #@29d
    const v4, 0x20902ca

    #@2a0
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@2a3
    move-result-object v3

    #@2a4
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2a7
    move-result-object v1

    #@2a8
    goto/16 :goto_62

    #@2aa
    .line 854
    :cond_2aa
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@2ad
    move-result-object v3

    #@2ae
    const-string v4, "DCM"

    #@2b0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b3
    move-result v3

    #@2b4
    if-eqz v3, :cond_2c1

    #@2b6
    .line 855
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2b8
    const v4, 0x20902b0

    #@2bb
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2be
    move-result-object v1

    #@2bf
    goto/16 :goto_62

    #@2c1
    .line 857
    :cond_2c1
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2c3
    const v4, 0x2090115

    #@2c6
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2c9
    move-result-object v1

    #@2ca
    goto/16 :goto_62

    #@2cc
    .line 868
    :cond_2cc
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->showPukDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@2cf
    goto/16 :goto_75

    #@2d1
    .line 770
    nop

    #@2d2
    :pswitch_data_2d2
    .packed-switch 0x1
        :pswitch_76
        :pswitch_2a
        :pswitch_96
    .end packed-switch
.end method

.method private setUIStringByOperator(II)V
    .registers 13
    .parameter "state"
    .parameter "retryCount"

    #@0
    .prologue
    const v9, 0x20902c3

    #@3
    const v5, 0x20902b8

    #@6
    const/4 v8, 0x1

    #@7
    const/4 v7, 0x0

    #@8
    const v6, 0x2090125

    #@b
    .line 675
    const/4 v0, 0x0

    #@c
    .line 676
    .local v0, mHeaderTextString:Ljava/lang/String;
    const/4 v1, 0x0

    #@d
    .line 677
    .local v1, mRetryTextString:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v2

    #@11
    .line 679
    .local v2, res:Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_214

    #@14
    .line 744
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    const-string v4, "KR"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_1f6

    #@20
    .line 745
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    const-string v4, "SKT"

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_1dd

    #@2c
    .line 747
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    .line 748
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@34
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@37
    move-result-object v3

    #@38
    const v4, 0x20902b9

    #@3b
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    .line 761
    :cond_43
    :goto_43
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mHeaderText:Landroid/widget/TextView;

    #@45
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@48
    .line 762
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mRetryText:Landroid/widget/TextView;

    #@4a
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4d
    .line 763
    return-void

    #@4e
    .line 682
    :pswitch_4e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    const-string v4, "KR"

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_cd

    #@5a
    .line 683
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    const-string v4, "SKT"

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_81

    #@66
    .line 685
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@68
    const v4, 0x20902c1

    #@6b
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    .line 686
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@71
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@74
    move-result-object v3

    #@75
    const v4, 0x20902c2

    #@78
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    goto :goto_43

    #@81
    .line 687
    :cond_81
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    const-string v4, "KT"

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v3

    #@8b
    if-eqz v3, :cond_a5

    #@8d
    .line 689
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@8f
    const v4, 0x209010f

    #@92
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@95
    move-result-object v0

    #@96
    .line 690
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@98
    new-array v4, v8, [Ljava/lang/Object;

    #@9a
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v5

    #@9e
    aput-object v5, v4, v7

    #@a0
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    goto :goto_43

    #@a5
    .line 691
    :cond_a5
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    const-string v4, "LGU"

    #@ab
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ae
    move-result v3

    #@af
    if-eqz v3, :cond_43

    #@b1
    .line 693
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@b3
    const v4, 0x20902c1

    #@b6
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b9
    move-result-object v0

    #@ba
    .line 694
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@bc
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@bf
    move-result-object v3

    #@c0
    const v4, 0x20902c2

    #@c3
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@c6
    move-result-object v3

    #@c7
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ca
    move-result-object v1

    #@cb
    goto/16 :goto_43

    #@cd
    .line 697
    :cond_cd
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@d0
    move-result-object v3

    #@d1
    const-string v4, "DCM"

    #@d3
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d6
    move-result v3

    #@d7
    if-eqz v3, :cond_118

    #@d9
    .line 698
    new-instance v3, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@e1
    move-result-object v4

    #@e2
    const v5, 0x20902ac

    #@e5
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e8
    move-result-object v4

    #@e9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    const-string v4, "\n"

    #@ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@f6
    move-result-object v4

    #@f7
    const v5, 0x20902ad

    #@fa
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@fd
    move-result-object v4

    #@fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v3

    #@102
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v0

    #@106
    .line 700
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@108
    new-array v4, v8, [Ljava/lang/Object;

    #@10a
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10d
    move-result-object v5

    #@10e
    aput-object v5, v4, v7

    #@110
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@113
    move-result-object v1

    #@114
    .line 706
    :goto_114
    if-nez p2, :cond_43

    #@116
    goto/16 :goto_43

    #@118
    .line 702
    :cond_118
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@11b
    move-result-object v3

    #@11c
    const v4, 0x209010f

    #@11f
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@122
    move-result-object v0

    #@123
    .line 703
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@125
    new-array v4, v8, [Ljava/lang/Object;

    #@127
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12a
    move-result-object v5

    #@12b
    aput-object v5, v4, v7

    #@12d
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@130
    move-result-object v1

    #@131
    goto :goto_114

    #@132
    .line 713
    :pswitch_132
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@135
    move-result-object v3

    #@136
    const-string v4, "KR"

    #@138
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13b
    move-result v3

    #@13c
    if-eqz v3, :cond_14c

    #@13e
    .line 714
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@140
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@143
    move-result-object v0

    #@144
    .line 715
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@146
    invoke-virtual {v3, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@149
    move-result-object v1

    #@14a
    goto/16 :goto_43

    #@14c
    .line 717
    :cond_14c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@14f
    move-result-object v3

    #@150
    const-string v4, "DCM"

    #@152
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@155
    move-result v3

    #@156
    if-eqz v3, :cond_173

    #@158
    .line 718
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@15b
    move-result-object v3

    #@15c
    const v4, 0x2090111

    #@15f
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@162
    move-result-object v0

    #@163
    .line 719
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@165
    new-array v4, v8, [Ljava/lang/Object;

    #@167
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16a
    move-result-object v5

    #@16b
    aput-object v5, v4, v7

    #@16d
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@170
    move-result-object v1

    #@171
    goto/16 :goto_43

    #@173
    .line 722
    :cond_173
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@176
    move-result-object v3

    #@177
    const v4, 0x2090111

    #@17a
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17d
    move-result-object v0

    #@17e
    .line 723
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@180
    invoke-virtual {v3, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@183
    move-result-object v1

    #@184
    .line 726
    goto/16 :goto_43

    #@186
    .line 729
    :pswitch_186
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@189
    move-result-object v3

    #@18a
    const-string v4, "KR"

    #@18c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18f
    move-result v3

    #@190
    if-eqz v3, :cond_1a3

    #@192
    .line 730
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@194
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@197
    move-result-object v0

    #@198
    .line 731
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@19a
    const v4, 0x20902cc

    #@19d
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1a0
    move-result-object v1

    #@1a1
    goto/16 :goto_43

    #@1a3
    .line 733
    :cond_1a3
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1a6
    move-result-object v3

    #@1a7
    const-string v4, "DCM"

    #@1a9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ac
    move-result v3

    #@1ad
    if-eqz v3, :cond_1ca

    #@1af
    .line 734
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@1b2
    move-result-object v3

    #@1b3
    const v4, 0x2090112

    #@1b6
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1b9
    move-result-object v0

    #@1ba
    .line 735
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1bc
    new-array v4, v8, [Ljava/lang/Object;

    #@1be
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c1
    move-result-object v5

    #@1c2
    aput-object v5, v4, v7

    #@1c4
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1c7
    move-result-object v1

    #@1c8
    goto/16 :goto_43

    #@1ca
    .line 738
    :cond_1ca
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@1cd
    move-result-object v3

    #@1ce
    const v4, 0x2090112

    #@1d1
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1d4
    move-result-object v0

    #@1d5
    .line 739
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1d7
    invoke-virtual {v3, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1da
    move-result-object v1

    #@1db
    .line 741
    goto/16 :goto_43

    #@1dd
    .line 751
    :cond_1dd
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1df
    const v4, 0x10402fc

    #@1e2
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1e5
    move-result-object v0

    #@1e6
    .line 752
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1e8
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1eb
    move-result-object v3

    #@1ec
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@1ef
    move-result-object v3

    #@1f0
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v1

    #@1f4
    goto/16 :goto_43

    #@1f6
    .line 756
    :cond_1f6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@1f9
    move-result-object v3

    #@1fa
    const v4, 0x10402fc

    #@1fd
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@200
    move-result-object v0

    #@201
    .line 757
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@204
    move-result-object v3

    #@205
    new-array v4, v8, [Ljava/lang/Object;

    #@207
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20a
    move-result-object v5

    #@20b
    aput-object v5, v4, v7

    #@20d
    invoke-virtual {v3, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@210
    move-result-object v1

    #@211
    goto/16 :goto_43

    #@213
    .line 679
    nop

    #@214
    :pswitch_data_214
    .packed-switch 0x0
        :pswitch_4e
        :pswitch_132
        :pswitch_186
    .end packed-switch
.end method

.method private showPukDialog(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 541
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@3
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@8
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@f
    move-result-object v0

    #@10
    const v1, 0x104000a

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@1d
    .line 546
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@1f
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@22
    move-result-object v0

    #@23
    const/16 v1, 0x7d9

    #@25
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@28
    .line 549
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@2a
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2d
    .line 550
    return-void
.end method

.method private showPukDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 573
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@e
    move-result-object v0

    #@f
    const v1, 0x104000a

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v0

    #@17
    const v1, 0x202026e

    #@1a
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@24
    .line 579
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@26
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@29
    move-result-object v0

    #@2a
    const/16 v1, 0x7d9

    #@2c
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@2f
    .line 580
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mdialog:Landroid/app/AlertDialog;

    #@31
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@34
    .line 582
    return-void
.end method

.method private showPukDialogDCM(Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 554
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@f
    move-result-object v0

    #@10
    const v1, 0x202026e

    #@13
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1a
    move-result-object v6

    #@1b
    .line 559
    .local v6, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@1e
    move-result-object v0

    #@1f
    const/16 v1, 0x7d9

    #@21
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@24
    .line 560
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    #@27
    .line 562
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$7;

    #@29
    const-wide/16 v2, 0xbb8

    #@2b
    const-wide/16 v4, 0x3e8

    #@2d
    move-object v1, p0

    #@2e
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$7;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;JJLandroid/app/AlertDialog;)V

    #@31
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$7;->start()Landroid/os/CountDownTimer;

    #@34
    move-result-object v7

    #@35
    .line 568
    .local v7, timer:Landroid/os/CountDownTimer;
    return-void
.end method


# virtual methods
.method protected checkPin()Z
    .registers 3

    #@0
    .prologue
    .line 595
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v0

    #@a
    .line 596
    .local v0, length:I
    const/4 v1, 0x4

    #@b
    if-lt v0, v1, :cond_1f

    #@d
    const/16 v1, 0x8

    #@f
    if-gt v0, v1, :cond_1f

    #@11
    .line 597
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@13
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@1d
    .line 598
    const/4 v1, 0x1

    #@1e
    .line 600
    :goto_1e
    return v1

    #@1f
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1e
.end method

.method protected checkPuk()Z
    .registers 3

    #@0
    .prologue
    .line 586
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v0

    #@a
    const/16 v1, 0x8

    #@c
    if-lt v0, v1, :cond_1c

    #@e
    .line 587
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@10
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPukText:Ljava/lang/String;

    #@1a
    .line 588
    const/4 v0, 0x1

    #@1b
    .line 590
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public confirmPin()Z
    .registers 3

    #@0
    .prologue
    .line 604
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@4
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method protected getPasswordTextViewId()I
    .registers 2

    #@0
    .prologue
    .line 226
    const v0, 0x10202d3

    #@3
    return v0
.end method

.method protected getSimUnlockProgressDialog()Landroid/app/Dialog;
    .registers 4

    #@0
    .prologue
    .line 513
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2
    if-nez v0, :cond_44

    #@4
    .line 514
    new-instance v0, Landroid/app/ProgressDialog;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@d
    .line 515
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    const-string v1, "KR"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_47

    #@19
    .line 516
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@1b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1d
    const v2, 0x20902b7

    #@20
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@27
    .line 530
    :goto_27
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@29
    const/4 v1, 0x1

    #@2a
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@2d
    .line 531
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@33
    .line 532
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@35
    instance-of v0, v0, Landroid/app/Activity;

    #@37
    if-nez v0, :cond_44

    #@39
    .line 533
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@3b
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@3e
    move-result-object v0

    #@3f
    const/16 v1, 0x7d9

    #@41
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@44
    .line 537
    :cond_44
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@46
    return-object v0

    #@47
    .line 518
    :cond_47
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "KDDI"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v0

    #@51
    if-eqz v0, :cond_62

    #@53
    .line 519
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@55
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@57
    const v2, 0x20902cb

    #@5a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@61
    goto :goto_27

    #@62
    .line 521
    :cond_62
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    const-string v1, "DCM"

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_7d

    #@6e
    .line 522
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@70
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@72
    const v2, 0x20902b1

    #@75
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@7c
    goto :goto_27

    #@7d
    .line 525
    :cond_7d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@7f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@81
    const v2, 0x104032d

    #@84
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@8b
    goto :goto_27
.end method

.method protected onFinishInflate()V
    .registers 14

    #@0
    .prologue
    .line 231
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onFinishInflate()V

    #@3
    .line 238
    const v9, 0x10202fb

    #@6
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v9

    #@a
    check-cast v9, Landroid/widget/TextView;

    #@c
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mHeaderText:Landroid/widget/TextView;

    #@e
    .line 239
    const v9, 0x20d0054

    #@11
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v9

    #@15
    check-cast v9, Landroid/widget/TextView;

    #@17
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mRetryText:Landroid/widget/TextView;

    #@19
    .line 240
    const/4 v1, 0x0

    #@1a
    .line 242
    .local v1, attemptsRemaining:I
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1d
    move-result-object v9

    #@1e
    const-string v10, "AU"

    #@20
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v9

    #@24
    if-eqz v9, :cond_44

    #@26
    .line 243
    const v9, 0x20d0055

    #@29
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@2c
    move-result-object v9

    #@2d
    check-cast v9, Landroid/widget/TextView;

    #@2f
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyText:Landroid/widget/TextView;

    #@31
    .line 244
    const v9, 0x10202b8

    #@34
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@37
    move-result-object v9

    #@38
    check-cast v9, Landroid/widget/TextView;

    #@3a
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@3c
    .line 245
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@3e
    const v10, 0x1040310

    #@41
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@44
    .line 249
    :cond_44
    const-string v9, "KeyguardSimPukView"

    #@46
    const-string v10, "[kjj] [UICC] Regist ICC Removed BroadcastReceiver"

    #@48
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 250
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@4e
    move-result-object v9

    #@4f
    const-string v10, "DCM"

    #@51
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v9

    #@55
    if-nez v9, :cond_65

    #@57
    .line 251
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@59
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@5b
    new-instance v11, Landroid/content/IntentFilter;

    #@5d
    const-string v12, "android.intent.action.SIM_STATE_CHANGED"

    #@5f
    invoke-direct {v11, v12}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@62
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@65
    .line 254
    :cond_65
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@68
    move-result-object v9

    #@69
    const-string v10, "KR"

    #@6b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v9

    #@6f
    if-nez v9, :cond_82

    #@71
    .line 255
    const v9, 0x10202d3

    #@74
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@77
    move-result-object v9

    #@78
    check-cast v9, Landroid/widget/TextView;

    #@7a
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinEntry:Landroid/widget/TextView;

    #@7c
    .line 256
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinEntry:Landroid/widget/TextView;

    #@7e
    const/4 v10, 0x3

    #@7f
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setGravity(I)V

    #@82
    .line 260
    :cond_82
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@84
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->get_state()I

    #@87
    move-result v0

    #@88
    .line 262
    .local v0, CurrentState:I
    :try_start_88
    const-string v9, "phone"

    #@8a
    invoke-static {v9}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8d
    move-result-object v9

    #@8e
    invoke-static {v9}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@91
    move-result-object v9

    #@92
    invoke-interface {v9}, Lcom/android/internal/telephony/ITelephony;->getIccPuk1RetryCount()I

    #@95
    move-result v1

    #@96
    .line 264
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->setUIStringByOperator(II)V

    #@99
    .line 265
    const-string v9, "KeyguardSimPukView"

    #@9b
    new-instance v10, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v11, "KeyguardSimPukView START "

    #@a2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v10

    #@a6
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v10

    #@aa
    const-string v11, " CurrentState "

    #@ac
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v10

    #@b0
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v10

    #@b8
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 266
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@bd
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c0
    move-result-object v9

    #@c1
    const-string v10, "sim_err_popup_msg"

    #@c3
    const/4 v11, 0x0

    #@c4
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c7
    move-result v9

    #@c8
    if-lez v9, :cond_f1

    #@ca
    .line 267
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@cc
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cf
    move-result-object v9

    #@d0
    const-string v10, "sim_err_popup_msg"

    #@d2
    const/4 v11, 0x0

    #@d3
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@d6
    .line 268
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@d9
    move-result-object v9

    #@da
    const-string v10, "BR"

    #@dc
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@df
    move-result v9

    #@e0
    if-nez v9, :cond_f1

    #@e2
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@e5
    move-result-object v9

    #@e6
    const-string v10, "TCL"

    #@e8
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@eb
    move-result v9

    #@ec
    if-nez v9, :cond_f1

    #@ee
    .line 269
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->setPopupStringByOperator(II)V
    :try_end_f1
    .catch Landroid/os/RemoteException; {:try_start_88 .. :try_end_f1} :catch_1e2

    #@f1
    .line 277
    :cond_f1
    :goto_f1
    const v9, 0x10202df

    #@f4
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@f7
    move-result-object v6

    #@f8
    check-cast v6, Landroid/widget/TextView;

    #@fa
    .line 278
    .local v6, ok:Landroid/widget/TextView;
    if-eqz v6, :cond_108

    #@fc
    .line 279
    const/4 v9, 0x0

    #@fd
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    #@100
    .line 280
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$2;

    #@102
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@105
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@108
    .line 291
    :cond_108
    const v9, 0x10202d4

    #@10b
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@10e
    move-result-object v7

    #@10f
    .line 292
    .local v7, pinDelete:Landroid/view/View;
    if-eqz v7, :cond_125

    #@111
    .line 293
    const/4 v9, 0x0

    #@112
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    #@115
    .line 294
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$3;

    #@117
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@11a
    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@11d
    .line 303
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$4;

    #@11f
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@122
    invoke-virtual {v7, v9}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@125
    .line 312
    :cond_125
    const v9, 0x1020283

    #@128
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@12b
    move-result-object v2

    #@12c
    check-cast v2, Landroid/widget/TextView;

    #@12e
    .line 313
    .local v2, cancel:Landroid/widget/TextView;
    if-eqz v2, :cond_13c

    #@130
    .line 314
    const/4 v9, 0x0

    #@131
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    #@134
    .line 315
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$5;

    #@136
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@139
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@13c
    .line 324
    :cond_13c
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@13e
    const-string v10, "phone"

    #@140
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@143
    move-result-object v8

    #@144
    check-cast v8, Landroid/telephony/TelephonyManager;

    #@146
    .line 325
    .local v8, telephonyManager:Landroid/telephony/TelephonyManager;
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;

    #@148
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@14b
    .line 338
    .local v5, listener:Landroid/telephony/PhoneStateListener;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@14e
    move-result-object v9

    #@14f
    const-string v10, "AU"

    #@151
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@154
    move-result v9

    #@155
    if-eqz v9, :cond_163

    #@157
    .line 339
    const/4 v9, 0x1

    #@158
    invoke-virtual {v8, v5, v9}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@15b
    .line 340
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyText:Landroid/widget/TextView;

    #@15d
    const v10, 0x1040325

    #@160
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@163
    .line 344
    :cond_163
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@165
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@168
    move-result-object v9

    #@169
    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@16c
    move-result-object v9

    #@16d
    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@16f
    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@172
    move-result-object v9

    #@173
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->currentLanguage:Ljava/lang/String;

    #@175
    .line 345
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@178
    move-result-object v9

    #@179
    const-string v10, "DCM"

    #@17b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17e
    move-result v9

    #@17f
    if-eqz v9, :cond_1ab

    #@181
    .line 346
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->currentLanguage:Ljava/lang/String;

    #@183
    const-string v10, "ja"

    #@185
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@188
    move-result v9

    #@189
    if-eqz v9, :cond_202

    #@18b
    .line 347
    const/4 v9, 0x1

    #@18c
    const v10, 0x418aa3d7

    #@18f
    invoke-virtual {v6, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    #@192
    .line 348
    const/4 v9, 0x1

    #@193
    const v10, 0x417547ae

    #@196
    invoke-virtual {v2, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    #@199
    .line 353
    :goto_199
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mRetryText:Landroid/widget/TextView;

    #@19b
    const/4 v10, 0x1

    #@19c
    const v11, 0x418aa3d7

    #@19f
    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    #@1a2
    .line 354
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mHeaderText:Landroid/widget/TextView;

    #@1a4
    const/4 v10, 0x1

    #@1a5
    const v11, 0x41baa3d7

    #@1a8
    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    #@1ab
    .line 359
    :cond_1ab
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@1ad
    if-nez v9, :cond_1bb

    #@1af
    .line 360
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1b1
    const-string v10, "statusbar"

    #@1b3
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b6
    move-result-object v9

    #@1b7
    check-cast v9, Landroid/app/StatusBarManager;

    #@1b9
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@1bb
    .line 363
    :cond_1bb
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@1bd
    if-nez v9, :cond_211

    #@1bf
    .line 364
    const-string v9, "KeyguardSimPukView"

    #@1c1
    const-string v10, "Could not get status bar manager"

    #@1c3
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c6
    .line 378
    :goto_1c6
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1c8
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    #@1cb
    move-result-object v10

    #@1cc
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@1cf
    .line 379
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1d1
    const/16 v10, 0x12

    #@1d3
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setInputType(I)V

    #@1d6
    .line 382
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1d8
    invoke-virtual {v9}, Landroid/widget/TextView;->requestFocus()Z

    #@1db
    .line 384
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@1dd
    const/4 v10, 0x0

    #@1de
    invoke-interface {v9, v10}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setTimeout(I)V

    #@1e1
    .line 385
    return-void

    #@1e2
    .line 272
    .end local v2           #cancel:Landroid/widget/TextView;
    .end local v5           #listener:Landroid/telephony/PhoneStateListener;
    .end local v6           #ok:Landroid/widget/TextView;
    .end local v7           #pinDelete:Landroid/view/View;
    .end local v8           #telephonyManager:Landroid/telephony/TelephonyManager;
    :catch_1e2
    move-exception v3

    #@1e3
    .line 273
    .local v3, ex:Landroid/os/RemoteException;
    const/4 v9, 0x0

    #@1e4
    const/4 v10, -0x1

    #@1e5
    invoke-direct {p0, v9, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->setUIStringByOperator(II)V

    #@1e8
    .line 274
    const-string v9, "KeyguardSimPukView"

    #@1ea
    new-instance v10, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    const-string v11, "KeyguardSimPukView START  catch "

    #@1f1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v10

    #@1f5
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v10

    #@1f9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fc
    move-result-object v10

    #@1fd
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@200
    goto/16 :goto_f1

    #@202
    .line 350
    .end local v3           #ex:Landroid/os/RemoteException;
    .restart local v2       #cancel:Landroid/widget/TextView;
    .restart local v5       #listener:Landroid/telephony/PhoneStateListener;
    .restart local v6       #ok:Landroid/widget/TextView;
    .restart local v7       #pinDelete:Landroid/view/View;
    .restart local v8       #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_202
    const/4 v9, 0x1

    #@203
    const v10, 0x418aa3d7

    #@206
    invoke-virtual {v6, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    #@209
    .line 351
    const/4 v9, 0x1

    #@20a
    const v10, 0x418aa3d7

    #@20d
    invoke-virtual {v2, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    #@210
    goto :goto_199

    #@211
    .line 369
    :cond_211
    const/4 v4, 0x0

    #@212
    .line 372
    .local v4, flags:I
    const/high16 v9, 0x1

    #@214
    or-int/2addr v4, v9

    #@215
    .line 373
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@217
    invoke-virtual {v9, v4}, Landroid/app/StatusBarManager;->disable(I)V

    #@21a
    .line 374
    const-string v9, "KeyguardSimPukView"

    #@21c
    const-string v10, "Disable the status bar touching !!!"

    #@21e
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@221
    goto :goto_1c6
.end method

.method public onPause()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 394
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 395
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@7
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@a
    .line 396
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@c
    .line 399
    :cond_c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "DCM"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_29

    #@18
    .line 400
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    if-eqz v0, :cond_29

    #@1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1e
    if-eqz v0, :cond_29

    #@20
    .line 401
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@24
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@27
    .line 402
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@29
    .line 406
    :cond_29
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 16
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/16 v9, 0x8

    #@3
    const/4 v8, 0x1

    #@4
    const/4 v7, 0x0

    #@5
    .line 428
    const v4, 0x10202df

    #@8
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v3

    #@c
    .line 429
    .local v3, ok:Landroid/view/View;
    const v4, 0x1020283

    #@f
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->findViewById(I)Landroid/view/View;

    #@12
    move-result-object v1

    #@13
    .line 431
    .local v1, cancel:Landroid/view/View;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    const-string v5, "AU"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_60

    #@1f
    .line 432
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@21
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    sput-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mTempString:Ljava/lang/String;

    #@2b
    .line 433
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@2e
    move-result v4

    #@2f
    const/4 v5, 0x3

    #@30
    if-ne v4, v5, :cond_8e

    #@32
    const-string v4, "112"

    #@34
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mTempString:Ljava/lang/String;

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_50

    #@3c
    const-string v4, "911"

    #@3e
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mTempString:Ljava/lang/String;

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v4

    #@44
    if-nez v4, :cond_50

    #@46
    const-string v4, "000"

    #@48
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mTempString:Ljava/lang/String;

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_8e

    #@50
    .line 434
    :cond_50
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@52
    const v5, 0x1080084

    #@55
    invoke-virtual {v4, v5, v7, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@58
    .line 437
    :goto_58
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@5a
    const v5, 0x1040310

    #@5d
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    #@60
    .line 440
    :cond_60
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@62
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->get_state()I

    #@65
    move-result v0

    #@66
    .line 441
    .local v0, CurrentState:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@69
    move-result v2

    #@6a
    .line 442
    .local v2, len:I
    const-string v4, "KeyguardSimPukView"

    #@6c
    new-instance v5, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v6, "[KYC] onTextChanged CurrentState="

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 443
    if-nez v2, :cond_97

    #@84
    .line 444
    invoke-virtual {v1, v7}, Landroid/view/View;->setEnabled(Z)V

    #@87
    .line 445
    invoke-virtual {v3, v7}, Landroid/view/View;->setEnabled(Z)V

    #@8a
    .line 450
    :goto_8a
    packed-switch v0, :pswitch_data_be

    #@8d
    .line 472
    :goto_8d
    return-void

    #@8e
    .line 436
    .end local v0           #CurrentState:I
    .end local v2           #len:I
    :cond_8e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@90
    const v5, 0x10802cf

    #@93
    invoke-virtual {v4, v5, v7, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@96
    goto :goto_58

    #@97
    .line 448
    .restart local v0       #CurrentState:I
    .restart local v2       #len:I
    :cond_97
    invoke-virtual {v1, v8}, Landroid/view/View;->setEnabled(Z)V

    #@9a
    goto :goto_8a

    #@9b
    .line 452
    :pswitch_9b
    if-lt v2, v9, :cond_a1

    #@9d
    .line 453
    invoke-virtual {v3, v8}, Landroid/view/View;->setEnabled(Z)V

    #@a0
    goto :goto_8d

    #@a1
    .line 455
    :cond_a1
    invoke-virtual {v3, v7}, Landroid/view/View;->setEnabled(Z)V

    #@a4
    goto :goto_8d

    #@a5
    .line 459
    :pswitch_a5
    if-lt v2, v10, :cond_ad

    #@a7
    if-gt v2, v9, :cond_ad

    #@a9
    .line 460
    invoke-virtual {v3, v8}, Landroid/view/View;->setEnabled(Z)V

    #@ac
    goto :goto_8d

    #@ad
    .line 462
    :cond_ad
    invoke-virtual {v3, v7}, Landroid/view/View;->setEnabled(Z)V

    #@b0
    goto :goto_8d

    #@b1
    .line 466
    :pswitch_b1
    if-lt v2, v10, :cond_b9

    #@b3
    if-gt v2, v9, :cond_b9

    #@b5
    .line 467
    invoke-virtual {v3, v8}, Landroid/view/View;->setEnabled(Z)V

    #@b8
    goto :goto_8d

    #@b9
    .line 469
    :cond_b9
    invoke-virtual {v3, v7}, Landroid/view/View;->setEnabled(Z)V

    #@bc
    goto :goto_8d

    #@bd
    .line 450
    nop

    #@be
    :pswitch_data_be
    .packed-switch 0x0
        :pswitch_9b
        :pswitch_a5
        :pswitch_b1
    .end packed-switch
.end method

.method public resetState()V
    .registers 3

    #@0
    .prologue
    .line 220
    const-string v0, "KeyguardSimPukView"

    #@2
    const-string v1, "[kjj]resetState()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 221
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@d
    .line 222
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 389
    return-void
.end method

.method public updateEmergencyText()V
    .registers 4

    #@0
    .prologue
    .line 412
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    if-eqz v0, :cond_26

    #@4
    .line 414
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mServiceState:Landroid/telephony/ServiceState;

    #@6
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    #@9
    move-result v0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->emergency_status:Z

    #@c
    .line 415
    const-string v0, "KeyguardSimPukView"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "[LGE] get the service status="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->emergency_status:Z

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 418
    :cond_26
    const-string v0, "KeyguardSimPukView"

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "Service state on PIN/PUK = "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->emergency_status:Z

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 419
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->emergency_status:Z

    #@42
    if-eqz v0, :cond_4d

    #@44
    .line 420
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyText:Landroid/widget/TextView;

    #@46
    const v1, 0x1040325

    #@49
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@4c
    .line 423
    :goto_4c
    return-void

    #@4d
    .line 422
    :cond_4d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mEmergencyText:Landroid/widget/TextView;

    #@4f
    const v1, 0x104030b

    #@52
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@55
    goto :goto_4c
.end method

.method protected updateSim()V
    .registers 4

    #@0
    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getSimUnlockProgressDialog()Landroid/app/Dialog;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@7
    .line 611
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mCheckInProgress:Z

    #@9
    if-nez v0, :cond_1a

    #@b
    .line 612
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mCheckInProgress:Z

    #@e
    .line 613
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@10
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPukText:Ljava/lang/String;

    #@12
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@14
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;Ljava/lang/String;Ljava/lang/String;)V

    #@17
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->start()V

    #@1a
    .line 665
    :cond_1a
    return-void
.end method

.method protected verifyPasswordAndUnlock()V
    .registers 2

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->next()V

    #@5
    .line 670
    return-void
.end method
