.class public Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;
.super Ljava/lang/Object;
.source "CheckLongPressHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;
    }
.end annotation


# instance fields
.field private mDownX:F

.field private mDownY:F

.field private mHasPerformedLongPress:Z

.field private mLongPressTimeout:I

.field private mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

.field private mScaledTouchSlop:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mScaledTouchSlop:I

    #@11
    .line 45
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@14
    move-result v0

    #@15
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mLongPressTimeout:I

    #@17
    .line 46
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mView:Landroid/view/View;

    #@19
    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mHasPerformedLongPress:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mHasPerformedLongPress:Z

    #@2
    return p1
.end method


# virtual methods
.method public cancelLongPress()V
    .registers 3

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mHasPerformedLongPress:Z

    #@3
    .line 73
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 74
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mView:Landroid/view/View;

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@b
    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@e
    .line 75
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@11
    .line 77
    :cond_11
    return-void
.end method

.method public hasPerformedLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mHasPerformedLongPress:Z

    #@2
    return v0
.end method

.method public onMove(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter "ev"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 61
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@5
    move-result v0

    #@6
    .line 62
    .local v0, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@9
    move-result v2

    #@a
    .line 63
    .local v2, y:F
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mDownX:F

    #@c
    sub-float/2addr v6, v0

    #@d
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@10
    move-result v6

    #@11
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mScaledTouchSlop:I

    #@13
    int-to-float v7, v7

    #@14
    cmpl-float v6, v6, v7

    #@16
    if-lez v6, :cond_30

    #@18
    move v1, v4

    #@19
    .line 64
    .local v1, xMoved:Z
    :goto_19
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mDownY:F

    #@1b
    sub-float/2addr v6, v2

    #@1c
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@1f
    move-result v6

    #@20
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mScaledTouchSlop:I

    #@22
    int-to-float v7, v7

    #@23
    cmpl-float v6, v6, v7

    #@25
    if-lez v6, :cond_32

    #@27
    move v3, v4

    #@28
    .line 66
    .local v3, yMoved:Z
    :goto_28
    if-nez v1, :cond_2c

    #@2a
    if-eqz v3, :cond_2f

    #@2c
    .line 67
    :cond_2c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->cancelLongPress()V

    #@2f
    .line 69
    :cond_2f
    return-void

    #@30
    .end local v1           #xMoved:Z
    .end local v3           #yMoved:Z
    :cond_30
    move v1, v5

    #@31
    .line 63
    goto :goto_19

    #@32
    .restart local v1       #xMoved:Z
    :cond_32
    move v3, v5

    #@33
    .line 64
    goto :goto_28
.end method

.method public postCheckForLongPress(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "ev"

    #@0
    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mDownX:F

    #@6
    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mDownY:F

    #@c
    .line 52
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mHasPerformedLongPress:Z

    #@f
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@11
    if-nez v0, :cond_1a

    #@13
    .line 55
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@15
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;-><init>(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)V

    #@18
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@1a
    .line 57
    :cond_1a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mView:Landroid/view/View;

    #@1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mPendingCheckForLongPress:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;

    #@1e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->mLongPressTimeout:I

    #@20
    int-to-long v2, v2

    #@21
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    #@24
    .line 58
    return-void
.end method
