.class public Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;
.super Landroid/widget/GridLayout;
.source "KeyguardStatusView.java"


# static fields
.field public static final ALARM_ICON:I = 0x108002e

.field public static final BATTERY_LOW_ICON:I = 0x0

.field public static final CHARGING_ICON:I = 0x0

.field private static final DEBUG:Z = true

.field public static final LOCK_ICON:I = 0x0

.field private static final TAG:Ljava/lang/String; = "KeyguardStatusView"


# instance fields
.field private mAlarmStatusView:Landroid/widget/TextView;

.field private mClockView:Lcom/android/internal/policy/impl/keyguard/ClockView;

.field private mDateFormatString:Ljava/lang/CharSequence;

.field private mDateView:Landroid/widget/TextView;

.field private mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 69
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 73
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 51
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@a
    .line 78
    return-void
.end method

.method private maybeSetUpperCaseText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "textView"
    .parameter "text"

    #@0
    .prologue
    .line 145
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->USE_UPPER_CASE:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    #@7
    move-result v0

    #@8
    const v1, 0x1020306

    #@b
    if-eq v0, v1, :cond_1d

    #@d
    .line 147
    if-eqz p2, :cond_1b

    #@f
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    :goto_17
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1a
    .line 151
    :goto_1a
    return-void

    #@1b
    .line 147
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_17

    #@1d
    .line 149
    :cond_1d
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@20
    goto :goto_1a
.end method


# virtual methods
.method public getAppWidgetId()I
    .registers 2

    #@0
    .prologue
    .line 141
    const/4 v0, -0x2

    #@1
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 130
    invoke-super {p0}, Landroid/widget/GridLayout;->onAttachedToWindow()V

    #@3
    .line 131
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 132
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 136
    invoke-super {p0}, Landroid/widget/GridLayout;->onDetachedFromWindow()V

    #@3
    .line 137
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 138
    return-void
.end method

.method protected onFinishInflate()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 82
    invoke-super {p0}, Landroid/widget/GridLayout;->onFinishInflate()V

    #@4
    .line 83
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    .line 84
    .local v2, res:Landroid/content/res/Resources;
    const v4, 0x104007f

    #@f
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@12
    move-result-object v4

    #@13
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateFormatString:Ljava/lang/CharSequence;

    #@15
    .line 86
    const v4, 0x1020063

    #@18
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->findViewById(I)Landroid/view/View;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Landroid/widget/TextView;

    #@1e
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateView:Landroid/widget/TextView;

    #@20
    .line 87
    const v4, 0x10202f4

    #@23
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v4

    #@27
    check-cast v4, Landroid/widget/TextView;

    #@29
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@2b
    .line 88
    const v4, 0x1020312

    #@2e
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v4

    #@32
    check-cast v4, Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@34
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mClockView:Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@36
    .line 89
    new-instance v4, Lcom/android/internal/widget/LockPatternUtils;

    #@38
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->getContext()Landroid/content/Context;

    #@3b
    move-result-object v5

    #@3c
    invoke-direct {v4, v5}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@3f
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@41
    .line 92
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateView:Landroid/widget/TextView;

    #@43
    sget-object v5, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@45
    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    #@48
    .line 95
    const/4 v4, 0x2

    #@49
    new-array v1, v4, [Landroid/view/View;

    #@4b
    const/4 v4, 0x0

    #@4c
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateView:Landroid/widget/TextView;

    #@4e
    aput-object v5, v1, v4

    #@50
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@52
    aput-object v4, v1, v6

    #@54
    .line 96
    .local v1, marqueeViews:[Landroid/view/View;
    const/4 v0, 0x0

    #@55
    .local v0, i:I
    :goto_55
    array-length v4, v1

    #@56
    if-ge v0, v4, :cond_7b

    #@58
    .line 97
    aget-object v3, v1, v0

    #@5a
    .line 98
    .local v3, v:Landroid/view/View;
    if-nez v3, :cond_75

    #@5c
    .line 99
    new-instance v4, Ljava/lang/RuntimeException;

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "Can\'t find widget at index "

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v5

    #@71
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@74
    throw v4

    #@75
    .line 101
    :cond_75
    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    #@78
    .line 96
    add-int/lit8 v0, v0, 0x1

    #@7a
    goto :goto_55

    #@7b
    .line 103
    .end local v3           #v:Landroid/view/View;
    :cond_7b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->refresh()V

    #@7e
    .line 104
    return-void
.end method

.method protected refresh()V
    .registers 2

    #@0
    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mClockView:Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/ClockView;->updateTime()V

    #@5
    .line 108
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->refreshDate()V

    #@8
    .line 109
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->refreshAlarmStatus()V

    #@b
    .line 110
    return-void
.end method

.method refreshAlarmStatus()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 114
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getNextAlarm()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 115
    .local v0, nextAlarm:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_20

    #@d
    .line 116
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@f
    invoke-direct {p0, v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->maybeSetUpperCaseText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    #@12
    .line 117
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@14
    const v2, 0x108002e

    #@17
    invoke-virtual {v1, v2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@1a
    .line 118
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@1f
    .line 122
    :goto_1f
    return-void

    #@20
    .line 120
    :cond_20
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mAlarmStatusView:Landroid/widget/TextView;

    #@22
    const/16 v2, 0x8

    #@24
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@27
    goto :goto_1f
.end method

.method refreshDate()V
    .registers 4

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateView:Landroid/widget/TextView;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->mDateFormatString:Ljava/lang/CharSequence;

    #@4
    new-instance v2, Ljava/util/Date;

    #@6
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    #@9
    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    #@c
    move-result-object v1

    #@d
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->maybeSetUpperCaseText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    #@10
    .line 126
    return-void
.end method
