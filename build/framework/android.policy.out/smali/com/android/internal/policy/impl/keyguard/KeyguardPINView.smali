.class public Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;
.source "KeyguardPINView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 42
    return-void
.end method


# virtual methods
.method protected getPasswordTextViewId()I
    .registers 2

    #@0
    .prologue
    .line 55
    const v0, 0x10202d3

    #@3
    return v0
.end method

.method public getWrongPasswordStringId()I
    .registers 2

    #@0
    .prologue
    .line 118
    const v0, 0x104054e

    #@3
    return v0
.end method

.method protected onFinishInflate()V
    .registers 5

    #@0
    .prologue
    .line 60
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onFinishInflate()V

    #@3
    .line 62
    const v2, 0x10202df

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 63
    .local v0, ok:Landroid/view/View;
    if-eqz v0, :cond_20

    #@c
    .line 64
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;

    #@e
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V

    #@11
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@14
    .line 73
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;

    #@16
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->getContext()Landroid/content/Context;

    #@19
    move-result-object v3

    #@1a
    invoke-direct {v2, v3}, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;-><init>(Landroid/content/Context;)V

    #@1d
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    #@20
    .line 78
    :cond_20
    const v2, 0x10202d4

    #@23
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v1

    #@27
    .line 79
    .local v1, pinDelete:Landroid/view/View;
    if-eqz v1, :cond_3d

    #@29
    .line 80
    const/4 v2, 0x0

    #@2a
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@2d
    .line 81
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;

    #@2f
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V

    #@32
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@35
    .line 93
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;

    #@37
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V

    #@3a
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@3d
    .line 105
    :cond_3d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@3f
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@46
    .line 106
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@48
    const/16 v3, 0x12

    #@4a
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setInputType(I)V

    #@4d
    .line 109
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@4f
    invoke-virtual {v2}, Landroid/widget/TextView;->requestFocus()Z

    #@52
    .line 110
    return-void
.end method

.method protected resetState()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 45
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getMaxBiometricUnlockAttemptsReached()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_1b

    #@d
    .line 46
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@f
    const v1, 0x1040315

    #@12
    invoke-interface {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(IZ)V

    #@15
    .line 50
    :goto_15
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@17
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    #@1a
    .line 51
    return-void

    #@1b
    .line 48
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@1d
    const v1, 0x1040552

    #@20
    const/4 v2, 0x0

    #@21
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(IZ)V

    #@24
    goto :goto_15
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 114
    return-void
.end method
