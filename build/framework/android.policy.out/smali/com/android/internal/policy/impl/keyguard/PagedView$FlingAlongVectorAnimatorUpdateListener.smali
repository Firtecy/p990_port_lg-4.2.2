.class Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;
.super Ljava/lang/Object;
.source "PagedView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FlingAlongVectorAnimatorUpdateListener"
.end annotation


# instance fields
.field private final mAlphaInterpolator:Landroid/animation/TimeInterpolator;

.field private mDragView:Landroid/view/View;

.field private mFriction:F

.field private mFrom:Landroid/graphics/Rect;

.field private mPrevTime:J

.field private mVelocity:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V
    .registers 9
    .parameter "dragView"
    .parameter "vel"
    .parameter "from"
    .parameter "startTime"
    .parameter "friction"

    #@0
    .prologue
    .line 2302
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2299
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    #@5
    const/high16 v1, 0x3f40

    #@7
    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@c
    .line 2303
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mDragView:Landroid/view/View;

    #@e
    .line 2304
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mVelocity:Landroid/graphics/PointF;

    #@10
    .line 2305
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFrom:Landroid/graphics/Rect;

    #@12
    .line 2306
    iput-wide p4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mPrevTime:J

    #@14
    .line 2307
    const/high16 v0, 0x3f80

    #@16
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mDragView:Landroid/view/View;

    #@18
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1f
    move-result-object v1

    #@20
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    #@22
    mul-float/2addr v1, p6

    #@23
    sub-float/2addr v0, v1

    #@24
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFriction:F

    #@26
    .line 2308
    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 11
    .parameter "animation"

    #@0
    .prologue
    const/high16 v8, 0x447a

    #@2
    .line 2312
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    check-cast v3, Ljava/lang/Float;

    #@8
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    #@b
    move-result v2

    #@c
    .line 2313
    .local v2, t:F
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@f
    move-result-wide v0

    #@10
    .line 2315
    .local v0, curTime:J
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFrom:Landroid/graphics/Rect;

    #@12
    iget v4, v3, Landroid/graphics/Rect;->left:I

    #@14
    int-to-float v4, v4

    #@15
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mVelocity:Landroid/graphics/PointF;

    #@17
    iget v5, v5, Landroid/graphics/PointF;->x:F

    #@19
    iget-wide v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mPrevTime:J

    #@1b
    sub-long v6, v0, v6

    #@1d
    long-to-float v6, v6

    #@1e
    mul-float/2addr v5, v6

    #@1f
    div-float/2addr v5, v8

    #@20
    add-float/2addr v4, v5

    #@21
    float-to-int v4, v4

    #@22
    iput v4, v3, Landroid/graphics/Rect;->left:I

    #@24
    .line 2316
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFrom:Landroid/graphics/Rect;

    #@26
    iget v4, v3, Landroid/graphics/Rect;->top:I

    #@28
    int-to-float v4, v4

    #@29
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mVelocity:Landroid/graphics/PointF;

    #@2b
    iget v5, v5, Landroid/graphics/PointF;->y:F

    #@2d
    iget-wide v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mPrevTime:J

    #@2f
    sub-long v6, v0, v6

    #@31
    long-to-float v6, v6

    #@32
    mul-float/2addr v5, v6

    #@33
    div-float/2addr v5, v8

    #@34
    add-float/2addr v4, v5

    #@35
    float-to-int v4, v4

    #@36
    iput v4, v3, Landroid/graphics/Rect;->top:I

    #@38
    .line 2318
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mDragView:Landroid/view/View;

    #@3a
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFrom:Landroid/graphics/Rect;

    #@3c
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@3e
    int-to-float v4, v4

    #@3f
    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationX(F)V

    #@42
    .line 2319
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mDragView:Landroid/view/View;

    #@44
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFrom:Landroid/graphics/Rect;

    #@46
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@48
    int-to-float v4, v4

    #@49
    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationY(F)V

    #@4c
    .line 2320
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mDragView:Landroid/view/View;

    #@4e
    const/high16 v4, 0x3f80

    #@50
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@52
    invoke-interface {v5, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@55
    move-result v5

    #@56
    sub-float/2addr v4, v5

    #@57
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    #@5a
    .line 2322
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mVelocity:Landroid/graphics/PointF;

    #@5c
    iget v4, v3, Landroid/graphics/PointF;->x:F

    #@5e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFriction:F

    #@60
    mul-float/2addr v4, v5

    #@61
    iput v4, v3, Landroid/graphics/PointF;->x:F

    #@63
    .line 2323
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mVelocity:Landroid/graphics/PointF;

    #@65
    iget v4, v3, Landroid/graphics/PointF;->y:F

    #@67
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mFriction:F

    #@69
    mul-float/2addr v4, v5

    #@6a
    iput v4, v3, Landroid/graphics/PointF;->y:F

    #@6c
    .line 2324
    iput-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;->mPrevTime:J

    #@6e
    .line 2325
    return-void
.end method
