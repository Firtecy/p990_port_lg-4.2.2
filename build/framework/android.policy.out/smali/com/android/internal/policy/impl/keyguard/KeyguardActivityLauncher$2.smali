.class Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;
.super Ljava/lang/Object;
.source "KeyguardActivityLauncher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->startActivityForCurrentUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$onStarted:Ljava/lang/Runnable;

.field final synthetic val$options:Landroid/os/Bundle;

.field final synthetic val$user:Landroid/os/UserHandle;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;Ljava/lang/Runnable;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 217
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$intent:Landroid/content/Intent;

    #@4
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$options:Landroid/os/Bundle;

    #@6
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$user:Landroid/os/UserHandle;

    #@8
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$onStarted:Ljava/lang/Runnable;

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    .line 221
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$intent:Landroid/content/Intent;

    #@7
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$intent:Landroid/content/Intent;

    #@9
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@b
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getContext()Landroid/content/Context;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v3, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    const/4 v4, 0x0

    #@18
    const/4 v5, 0x0

    #@19
    const/4 v6, 0x0

    #@1a
    const/high16 v7, 0x1000

    #@1c
    const/4 v8, 0x0

    #@1d
    const/4 v9, 0x0

    #@1e
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$options:Landroid/os/Bundle;

    #@20
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$user:Landroid/os/UserHandle;

    #@22
    invoke-virtual {v11}, Landroid/os/UserHandle;->getIdentifier()I

    #@25
    move-result v11

    #@26
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->startActivityAndWait(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)Landroid/app/IActivityManager$WaitResult;

    #@29
    move-result-object v13

    #@2a
    .line 233
    .local v13, result:Landroid/app/IActivityManager$WaitResult;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$200()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_6b

    #@30
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$300()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    const-string v1, "waitResult[%s,%s,%s,%s] at %s"

    #@36
    const/4 v2, 0x5

    #@37
    new-array v2, v2, [Ljava/lang/Object;

    #@39
    const/4 v3, 0x0

    #@3a
    iget v4, v13, Landroid/app/IActivityManager$WaitResult;->result:I

    #@3c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v4

    #@40
    aput-object v4, v2, v3

    #@42
    const/4 v3, 0x1

    #@43
    iget-wide v4, v13, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@45
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@48
    move-result-object v4

    #@49
    aput-object v4, v2, v3

    #@4b
    const/4 v3, 0x2

    #@4c
    iget-wide v4, v13, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@4e
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@51
    move-result-object v4

    #@52
    aput-object v4, v2, v3

    #@54
    const/4 v3, 0x3

    #@55
    iget-object v4, v13, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@57
    aput-object v4, v2, v3

    #@59
    const/4 v3, 0x4

    #@5a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5d
    move-result-wide v4

    #@5e
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@61
    move-result-object v4

    #@62
    aput-object v4, v2, v3

    #@64
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6b} :catch_71

    #@6b
    .line 241
    :cond_6b
    :try_start_6b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;->val$onStarted:Ljava/lang/Runnable;

    #@6d
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_70
    .catch Ljava/lang/Throwable; {:try_start_6b .. :try_end_70} :catch_7c

    #@70
    .line 245
    .end local v13           #result:Landroid/app/IActivityManager$WaitResult;
    :goto_70
    return-void

    #@71
    .line 236
    :catch_71
    move-exception v12

    #@72
    .line 237
    .local v12, e:Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$300()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    const-string v1, "Error starting activity"

    #@78
    invoke-static {v0, v1, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7b
    goto :goto_70

    #@7c
    .line 242
    .end local v12           #e:Landroid/os/RemoteException;
    .restart local v13       #result:Landroid/app/IActivityManager$WaitResult;
    :catch_7c
    move-exception v14

    #@7d
    .line 243
    .local v14, t:Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$300()Ljava/lang/String;

    #@80
    move-result-object v0

    #@81
    const-string v1, "Error running onStarted callback"

    #@83
    invoke-static {v0, v1, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@86
    goto :goto_70
.end method
