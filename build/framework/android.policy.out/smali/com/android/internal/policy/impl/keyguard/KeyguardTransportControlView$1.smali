.class Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;
.super Landroid/os/Handler;
.source "KeyguardTransportControlView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 89
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_a4

    #@5
    .line 125
    :cond_5
    :goto_5
    return-void

    #@6
    .line 91
    :pswitch_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@8
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)I

    #@b
    move-result v0

    #@c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@e
    if-ne v0, v1, :cond_5

    #@10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@12
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@14
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;I)V

    #@17
    goto :goto_5

    #@18
    .line 95
    :pswitch_18
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@1a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)I

    #@1d
    move-result v0

    #@1e
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@20
    if-ne v0, v1, :cond_5

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@24
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    check-cast v0, Landroid/os/Bundle;

    #@28
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;Landroid/os/Bundle;)V

    #@2b
    goto :goto_5

    #@2c
    .line 99
    :pswitch_2c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@2e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)I

    #@31
    move-result v0

    #@32
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@34
    if-ne v0, v1, :cond_5

    #@36
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@38
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@3a
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;I)V

    #@3d
    goto :goto_5

    #@3e
    .line 103
    :pswitch_3e
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@40
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)I

    #@43
    move-result v0

    #@44
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@46
    if-ne v0, v1, :cond_5

    #@48
    .line 104
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@4a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;

    #@4d
    move-result-object v0

    #@4e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@51
    move-result-object v0

    #@52
    if-eqz v0, :cond_61

    #@54
    .line 105
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@56
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;

    #@59
    move-result-object v0

    #@5a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@61
    .line 107
    :cond_61
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@63
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;

    #@66
    move-result-object v1

    #@67
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    check-cast v0, Landroid/graphics/Bitmap;

    #@6b
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;->access$502(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@6e
    .line 108
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@70
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)Landroid/widget/ImageView;

    #@73
    move-result-object v0

    #@74
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@76
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;

    #@79
    move-result-object v1

    #@7a
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@81
    goto :goto_5

    #@82
    .line 113
    :pswitch_82
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@84
    if-eqz v0, :cond_9d

    #@86
    .line 115
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@88
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->onListenerDetached()V

    #@8b
    .line 120
    :goto_8b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@8d
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@8f
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;I)I

    #@92
    .line 121
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@94
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@96
    check-cast v0, Landroid/app/PendingIntent;

    #@98
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$802(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    #@9b
    goto/16 :goto_5

    #@9d
    .line 117
    :cond_9d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@9f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;)V

    #@a2
    goto :goto_8b

    #@a3
    .line 89
    nop

    #@a4
    :pswitch_data_a4
    .packed-switch 0x64
        :pswitch_6
        :pswitch_18
        :pswitch_2c
        :pswitch_3e
        :pswitch_82
    .end packed-switch
.end method
