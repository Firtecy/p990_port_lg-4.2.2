.class Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;
.super Landroid/os/CountDownTimer;
.source "PasswordUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->handleAttemptLockout(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;JJ)V
    .registers 6
    .parameter
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@2
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    #@5
    return-void
.end method


# virtual methods
.method public onFinish()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 336
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@3
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Landroid/widget/EditText;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    #@a
    .line 337
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setEnabled(Z)V

    #@13
    .line 338
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@15
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->resetStatusInfo()V

    #@1c
    .line 339
    return-void
.end method

.method public onTick(J)V
    .registers 10
    .parameter "millisUntilFinished"

    #@0
    .prologue
    .line 327
    const-wide/16 v2, 0x3e8

    #@2
    div-long v2, p1, v2

    #@4
    long-to-int v1, v2

    #@5
    .line 328
    .local v1, secondsRemaining:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@7
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->getContext()Landroid/content/Context;

    #@a
    move-result-object v2

    #@b
    const v3, 0x1040334

    #@e
    const/4 v4, 0x1

    #@f
    new-array v4, v4, [Ljava/lang/Object;

    #@11
    const/4 v5, 0x0

    #@12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 331
    .local v0, instructions:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@1e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@25
    .line 332
    return-void
.end method
