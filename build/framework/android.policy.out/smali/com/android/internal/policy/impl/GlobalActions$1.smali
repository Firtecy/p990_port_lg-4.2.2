.class Lcom/android/internal/policy/impl/GlobalActions$1;
.super Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/GlobalActions;->createDialog()Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/GlobalActions;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/GlobalActions;IIII)V
    .registers 6
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 204
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;-><init>(IIII)V

    #@5
    return-void
.end method


# virtual methods
.method protected changeStateFromPress(Z)V
    .registers 4
    .parameter "buttonOn"

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/GlobalActions;->access$200(Lcom/android/internal/policy/impl/GlobalActions;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 243
    :cond_8
    :goto_8
    return-void

    #@9
    .line 232
    :cond_9
    if-eqz p1, :cond_1d

    #@b
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@d
    invoke-static {v0}, Lcom/android/internal/policy/impl/GlobalActions;->access$000(Lcom/android/internal/policy/impl/GlobalActions;)I

    #@10
    move-result v0

    #@11
    const/4 v1, 0x1

    #@12
    if-eq v0, v1, :cond_8

    #@14
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@16
    invoke-static {v0}, Lcom/android/internal/policy/impl/GlobalActions;->access$000(Lcom/android/internal/policy/impl/GlobalActions;)I

    #@19
    move-result v0

    #@1a
    const/4 v1, 0x2

    #@1b
    if-eq v0, v1, :cond_8

    #@1d
    .line 238
    :cond_1d
    const-string v0, "ril.cdma.inecmmode"

    #@1f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_8

    #@29
    .line 240
    if-eqz p1, :cond_37

    #@2b
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2d
    :goto_2d
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2f
    .line 241
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@31
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->mState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@33
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$502(Lcom/android/internal/policy/impl/GlobalActions;Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@36
    goto :goto_8

    #@37
    .line 240
    :cond_37
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOff:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@39
    goto :goto_2d
.end method

.method public create(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "convertView"
    .parameter "parent"
    .parameter "inflater"

    #@0
    .prologue
    .line 257
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->create(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 259
    .local v0, view:Landroid/view/View;
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 260
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v1, v0}, Lcom/lge/cappuccino/IMdm;->setAirplaneModeDisplayMenu(Landroid/view/View;)V

    #@f
    .line 262
    :cond_f
    return-object v0
.end method

.method public isEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 269
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7
    move-result-object v0

    #@8
    const/4 v1, 0x0

    #@9
    const-string v2, "LGMDMAirplaneModeUIAdpater"

    #@b
    invoke-interface {v0, v1, v2}, Lcom/lge/cappuccino/IMdm;->checkDisabledGlobalAction(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 272
    const-string v0, "GlobalActions"

    #@13
    const-string v1, "ToggleAction isEnabled false : on getAllowAirplaneModeOn"

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 273
    const/4 v0, 0x0

    #@19
    .line 275
    :goto_19
    return v0

    #@1a
    :cond_1a
    invoke-super {p0}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->isEnabled()Z

    #@1d
    move-result v0

    #@1e
    goto :goto_19
.end method

.method onToggle(Z)V
    .registers 7
    .parameter "on"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 208
    if-eqz p1, :cond_2f

    #@3
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@5
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$000(Lcom/android/internal/policy/impl/GlobalActions;)I

    #@8
    move-result v1

    #@9
    if-eq v1, v4, :cond_14

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@d
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$000(Lcom/android/internal/policy/impl/GlobalActions;)I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x2

    #@12
    if-ne v1, v2, :cond_2f

    #@14
    .line 210
    :cond_14
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@17
    move-result-object v1

    #@18
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v2

    #@20
    const v3, 0x2090166

    #@23
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@2e
    .line 226
    :goto_2e
    return-void

    #@2f
    .line 215
    :cond_2f
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@31
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$200(Lcom/android/internal/policy/impl/GlobalActions;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_5d

    #@37
    const-string v1, "ril.cdma.inecmmode"

    #@39
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@40
    move-result v1

    #@41
    if-eqz v1, :cond_5d

    #@43
    .line 217
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@45
    invoke-static {v1, v4}, Lcom/android/internal/policy/impl/GlobalActions;->access$302(Lcom/android/internal/policy/impl/GlobalActions;Z)Z

    #@48
    .line 219
    new-instance v0, Landroid/content/Intent;

    #@4a
    const-string v1, "android.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    #@4c
    const/4 v2, 0x0

    #@4d
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@50
    .line 221
    .local v0, ecmDialogIntent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@52
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@55
    .line 222
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@5c
    goto :goto_2e

    #@5d
    .line 224
    .end local v0           #ecmDialogIntent:Landroid/content/Intent;
    :cond_5d
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$1;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@5f
    invoke-static {v1, p1}, Lcom/android/internal/policy/impl/GlobalActions;->access$400(Lcom/android/internal/policy/impl/GlobalActions;Z)V

    #@62
    goto :goto_2e
.end method

.method public showBeforeProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 250
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showDuringKeyguard()Z
    .registers 2

    #@0
    .prologue
    .line 246
    const/4 v0, 0x1

    #@1
    return v0
.end method
