.class final enum Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
.super Ljava/lang/Enum;
.source "LockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

.field public static final enum LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

.field public static final enum UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 128
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@4
    const-string v1, "LockScreen"

    #@6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@b
    .line 129
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@d
    const-string v1, "UnlockScreen"

    #@f
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@14
    .line 127
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@17
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 127
    const-class v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    .registers 1

    #@0
    .prologue
    .line 127
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@8
    return-object v0
.end method
