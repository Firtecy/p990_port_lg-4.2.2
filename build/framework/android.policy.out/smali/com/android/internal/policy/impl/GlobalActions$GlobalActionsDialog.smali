.class final Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
.super Landroid/app/Dialog;
.source "GlobalActions.java"

# interfaces
.implements Landroid/content/DialogInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/GlobalActions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "GlobalActionsDialog"
.end annotation


# instance fields
.field private final mAlert:Lcom/android/internal/app/AlertController;

.field private mCancelOnUp:Z

.field private final mContext:Landroid/content/Context;

.field private mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

.field private mIntercepted:Z

.field private final mWindowTouchSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/app/AlertController$AlertParams;)V
    .registers 6
    .parameter "context"
    .parameter "params"

    #@0
    .prologue
    .line 1142
    invoke-static {p1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getDialogTheme(Landroid/content/Context;)I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@7
    .line 1143
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@9
    .line 1144
    new-instance v0, Lcom/android/internal/app/AlertController;

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@10
    move-result-object v2

    #@11
    invoke-direct {v0, v1, p0, v2}, Lcom/android/internal/app/AlertController;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@16
    .line 1145
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@18
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@1a
    const v2, 0x209025e

    #@1d
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    #@24
    .line 1146
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mWindowTouchSlop:I

    #@2e
    .line 1147
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@30
    invoke-virtual {p2, v0}, Lcom/android/internal/app/AlertController$AlertParams;->apply(Lcom/android/internal/app/AlertController;)V

    #@33
    .line 1148
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@35
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@37
    const v2, 0x1040102

    #@3a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    #@41
    .line 1149
    return-void
.end method

.method private static getDialogTheme(Landroid/content/Context;)I
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 1152
    new-instance v0, Landroid/util/TypedValue;

    #@2
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@5
    .line 1153
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@8
    move-result-object v1

    #@9
    const v2, 0x1010309

    #@c
    const/4 v3, 0x1

    #@d
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@10
    .line 1155
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    #@12
    return v1
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "event"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 1184
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@4
    if-eqz v2, :cond_6e

    #@6
    .line 1185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@9
    move-result v8

    #@a
    .line 1186
    .local v8, action:I
    if-nez v8, :cond_3c

    #@c
    .line 1187
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@13
    move-result-object v9

    #@14
    .line 1188
    .local v9, decor:Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@17
    move-result v2

    #@18
    float-to-int v10, v2

    #@19
    .line 1189
    .local v10, eventX:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1c
    move-result v2

    #@1d
    float-to-int v11, v2

    #@1e
    .line 1190
    .local v11, eventY:I
    iget v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mWindowTouchSlop:I

    #@20
    neg-int v2, v2

    #@21
    if-lt v10, v2, :cond_3a

    #@23
    iget v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mWindowTouchSlop:I

    #@25
    neg-int v2, v2

    #@26
    if-lt v11, v2, :cond_3a

    #@28
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    #@2b
    move-result v2

    #@2c
    iget v3, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mWindowTouchSlop:I

    #@2e
    add-int/2addr v2, v3

    #@2f
    if-ge v10, v2, :cond_3a

    #@31
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    #@34
    move-result v2

    #@35
    iget v3, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mWindowTouchSlop:I

    #@37
    add-int/2addr v2, v3

    #@38
    if-lt v11, v2, :cond_3c

    #@3a
    .line 1194
    :cond_3a
    iput-boolean v13, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@3c
    .line 1198
    .end local v9           #decor:Landroid/view/View;
    .end local v10           #eventX:I
    .end local v11           #eventY:I
    :cond_3c
    :try_start_3c
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@3e
    if-nez v2, :cond_73

    #@40
    .line 1199
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@42
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@45
    move-result v2

    #@46
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@48
    .line 1200
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@4a
    if-eqz v2, :cond_61

    #@4c
    .line 1201
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4f
    move-result-wide v0

    #@50
    .line 1202
    .local v0, now:J
    const/4 v4, 0x3

    #@51
    const/4 v5, 0x0

    #@52
    const/4 v6, 0x0

    #@53
    const/4 v7, 0x0

    #@54
    move-wide v2, v0

    #@55
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@58
    move-result-object p1

    #@59
    .line 1204
    const/16 v2, 0x1002

    #@5b
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setSource(I)V

    #@5e
    .line 1205
    const/4 v2, 0x1

    #@5f
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z
    :try_end_61
    .catchall {:try_start_3c .. :try_end_61} :catchall_87

    #@61
    .line 1211
    .end local v0           #now:J
    :cond_61
    if-ne v8, v13, :cond_6e

    #@63
    .line 1212
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@65
    if-eqz v2, :cond_6a

    #@67
    .line 1213
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->cancel()V

    #@6a
    .line 1215
    :cond_6a
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@6c
    .line 1216
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@6e
    .line 1220
    .end local v8           #action:I
    :cond_6e
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@71
    move-result v2

    #@72
    :cond_72
    :goto_72
    return v2

    #@73
    .line 1208
    .restart local v8       #action:I
    :cond_73
    :try_start_73
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@75
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_78
    .catchall {:try_start_73 .. :try_end_78} :catchall_87

    #@78
    move-result v2

    #@79
    .line 1211
    if-ne v8, v13, :cond_72

    #@7b
    .line 1212
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@7d
    if-eqz v3, :cond_82

    #@7f
    .line 1213
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->cancel()V

    #@82
    .line 1215
    :cond_82
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@84
    .line 1216
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@86
    goto :goto_72

    #@87
    .line 1211
    :catchall_87
    move-exception v2

    #@88
    if-ne v8, v13, :cond_95

    #@8a
    .line 1212
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@8c
    if-eqz v3, :cond_91

    #@8e
    .line 1213
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->cancel()V

    #@91
    .line 1215
    :cond_91
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mCancelOnUp:Z

    #@93
    .line 1216
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mIntercepted:Z

    #@95
    :cond_95
    throw v2
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 1224
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/app/AlertController;->getListView()Landroid/widget/ListView;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1229
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 1230
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/app/AlertController;->installContent()V

    #@8
    .line 1231
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1235
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/app/AlertController;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 1236
    const/4 v0, 0x1

    #@9
    .line 1238
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1243
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/app/AlertController;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 1244
    const/4 v0, 0x1

    #@9
    .line 1246
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method protected onStart()V
    .registers 3

    #@0
    .prologue
    .line 1164
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->canEnableAccessibilityViaGesture(Landroid/content/Context;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_19

    #@8
    .line 1165
    new-instance v0, Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@a
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mContext:Landroid/content/Context;

    #@c
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/EnableAccessibilityController;-><init>(Landroid/content/Context;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@11
    .line 1166
    const/4 v0, 0x0

    #@12
    invoke-super {p0, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    #@15
    .line 1171
    :goto_15
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    #@18
    .line 1172
    return-void

    #@19
    .line 1168
    :cond_19
    const/4 v0, 0x0

    #@1a
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@1c
    .line 1169
    const/4 v0, 0x1

    #@1d
    invoke-super {p0, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    #@20
    goto :goto_15
.end method

.method protected onStop()V
    .registers 2

    #@0
    .prologue
    .line 1176
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1177
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->mEnableAccessibilityController:Lcom/android/internal/policy/impl/EnableAccessibilityController;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->onDestroy()V

    #@9
    .line 1179
    :cond_9
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    #@c
    .line 1180
    return-void
.end method
