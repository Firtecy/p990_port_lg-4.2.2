.class Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2$1;
.super Ljava/lang/Object;
.source "KeyguardMultiUserSelectorView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "internal"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_3c

    #@10
    .line 144
    :try_start_10
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;

    #@16
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;->val$avatar:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@18
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->getUserInfo()Landroid/content/pm/UserInfo;

    #@1b
    move-result-object v2

    #@1c
    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    #@1e
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->switchUser(I)Z
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_21} :catch_22

    #@21
    .line 152
    :goto_21
    return-void

    #@22
    .line 146
    :catch_22
    move-exception v0

    #@23
    .line 147
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "KeyguardMultiUserSelectorView"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "Couldn\'t switch user "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_21

    #@3c
    .line 150
    .end local v0           #re:Landroid/os/RemoteException;
    :cond_3c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;

    #@3e
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;

    #@40
    const/4 v2, 0x1

    #@41
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;Z)V

    #@44
    goto :goto_21
.end method
