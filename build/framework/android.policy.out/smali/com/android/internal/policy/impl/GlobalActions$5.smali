.class Lcom/android/internal/policy/impl/GlobalActions$5;
.super Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/GlobalActions;->createDialog()Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/GlobalActions;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/GlobalActions;II)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 368
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$5;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    invoke-direct {p0, p2, p3}, Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;-><init>(II)V

    #@5
    return-void
.end method


# virtual methods
.method public onLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 399
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onPress()V
    .registers 5

    #@0
    .prologue
    .line 371
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@9
    .line 372
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x1040106

    #@c
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@f
    .line 373
    const v2, 0x1040107

    #@12
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@15
    .line 374
    const/high16 v2, 0x104

    #@17
    const/4 v3, 0x0

    #@18
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1b
    .line 375
    const v2, 0x1040404

    #@1e
    new-instance v3, Lcom/android/internal/policy/impl/GlobalActions$5$1;

    #@20
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/GlobalActions$5$1;-><init>(Lcom/android/internal/policy/impl/GlobalActions$5;)V

    #@23
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@26
    .line 393
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@29
    move-result-object v1

    #@2a
    .line 394
    .local v1, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@2d
    move-result-object v2

    #@2e
    const/16 v3, 0x7d9

    #@30
    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    #@33
    .line 395
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@36
    .line 396
    return-void
.end method

.method public showBeforeProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 407
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showDuringKeyguard()Z
    .registers 2

    #@0
    .prologue
    .line 403
    const/4 v0, 0x1

    #@1
    return v0
.end method
