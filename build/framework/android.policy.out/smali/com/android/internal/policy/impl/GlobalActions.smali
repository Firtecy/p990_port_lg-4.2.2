.class Lcom/android/internal/policy/impl/GlobalActions;
.super Ljava/lang/Object;
.source "GlobalActions.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;,
        Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;,
        Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;,
        Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;,
        Lcom/android/internal/policy/impl/GlobalActions$Action;,
        Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;
    }
.end annotation


# static fields
.field private static final DIALOG_DISMISS_DELAY:I = 0x12c

.field private static final MESSAGE_DISMISS:I = 0x0

.field private static final MESSAGE_REFRESH:I = 0x1

.field private static final MESSAGE_SHOW:I = 0x2

.field private static final SHOW_SILENT_TOGGLE:Z = true

.field private static final TAG:Ljava/lang/String; = "GlobalActions"

.field private static final VIBRATE_DURATION:I = 0xc8

.field private static mContext:Landroid/content/Context;

.field private static mRingerMode:I


# instance fields
.field private mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

.field private mAirplaneModeObserver:Landroid/database/ContentObserver;

.field private mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

.field private mAirplaneOnDialog:Landroid/app/AlertDialog;

.field private mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallState:I

.field private mDeviceProvisioned:Z

.field private mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

.field private final mDreamManager:Landroid/service/dreams/IDreamManager;

.field private mHandler:Landroid/os/Handler;

.field private mHasTelephony:Z

.field private mHasVibrator:Z

.field private mIsWaitingForEcmExit:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/policy/impl/GlobalActions$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyguardShowing:Z

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mRingerModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSilentModeAction:Lcom/android/internal/policy/impl/GlobalActions$Action;

.field private final mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 111
    const/4 v0, -0x1

    #@1
    sput v0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerMode:I

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V
    .registers 12
    .parameter "context"
    .parameter "windowManagerFuncs"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 104
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/GlobalActions;->mKeyguardShowing:Z

    #@7
    .line 105
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDeviceProvisioned:Z

    #@9
    .line 106
    sget-object v4, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@b
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@d
    .line 107
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/GlobalActions;->mIsWaitingForEcmExit:Z

    #@f
    .line 113
    const/4 v4, 0x0

    #@10
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneOnDialog:Landroid/app/AlertDialog;

    #@12
    .line 114
    iput v6, p0, Lcom/android/internal/policy/impl/GlobalActions;->mCallState:I

    #@14
    .line 978
    new-instance v4, Lcom/android/internal/policy/impl/GlobalActions$9;

    #@16
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/GlobalActions$9;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@19
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1b
    .line 999
    new-instance v4, Lcom/android/internal/policy/impl/GlobalActions$10;

    #@1d
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/GlobalActions$10;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@20
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@22
    .line 1016
    new-instance v4, Lcom/android/internal/policy/impl/GlobalActions$11;

    #@24
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/GlobalActions$11;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@27
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    #@29
    .line 1025
    new-instance v4, Lcom/android/internal/policy/impl/GlobalActions$12;

    #@2b
    new-instance v7, Landroid/os/Handler;

    #@2d
    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    #@30
    invoke-direct {v4, p0, v7}, Lcom/android/internal/policy/impl/GlobalActions$12;-><init>(Lcom/android/internal/policy/impl/GlobalActions;Landroid/os/Handler;)V

    #@33
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeObserver:Landroid/database/ContentObserver;

    #@35
    .line 1037
    new-instance v4, Lcom/android/internal/policy/impl/GlobalActions$13;

    #@37
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/GlobalActions$13;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@3a
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHandler:Landroid/os/Handler;

    #@3c
    .line 120
    sput-object p1, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@3e
    .line 121
    iput-object p2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@40
    .line 122
    sget-object v4, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@42
    const-string v7, "audio"

    #@44
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@47
    move-result-object v4

    #@48
    check-cast v4, Landroid/media/AudioManager;

    #@4a
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAudioManager:Landroid/media/AudioManager;

    #@4c
    .line 123
    const-string v4, "dreams"

    #@4e
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@51
    move-result-object v4

    #@52
    invoke-static {v4}, Landroid/service/dreams/IDreamManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/dreams/IDreamManager;

    #@55
    move-result-object v4

    #@56
    iput-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDreamManager:Landroid/service/dreams/IDreamManager;

    #@58
    .line 127
    new-instance v1, Landroid/content/IntentFilter;

    #@5a
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@5d
    .line 128
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@5f
    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@62
    .line 129
    const-string v4, "android.intent.action.SCREEN_OFF"

    #@64
    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@67
    .line 130
    const-string v4, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    #@69
    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6c
    .line 131
    iget-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@6e
    invoke-virtual {p1, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@71
    .line 134
    const-string v4, "phone"

    #@73
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@76
    move-result-object v2

    #@77
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@79
    .line 137
    .local v2, telephonyManager:Landroid/telephony/TelephonyManager;
    iget-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@7b
    const/16 v7, 0x21

    #@7d
    invoke-virtual {v2, v4, v7}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@80
    .line 140
    const-string v4, "connectivity"

    #@82
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@85
    move-result-object v0

    #@86
    check-cast v0, Landroid/net/ConnectivityManager;

    #@88
    .line 142
    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    #@8b
    move-result v4

    #@8c
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasTelephony:Z

    #@8e
    .line 143
    sget-object v4, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@90
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@93
    move-result-object v4

    #@94
    const-string v7, "airplane_mode_on"

    #@96
    invoke-static {v7}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@99
    move-result-object v7

    #@9a
    iget-object v8, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeObserver:Landroid/database/ContentObserver;

    #@9c
    invoke-virtual {v4, v7, v5, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@9f
    .line 146
    sget-object v4, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@a1
    const-string v7, "vibrator"

    #@a3
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a6
    move-result-object v3

    #@a7
    check-cast v3, Landroid/os/Vibrator;

    #@a9
    .line 147
    .local v3, vibrator:Landroid/os/Vibrator;
    if-eqz v3, :cond_b5

    #@ab
    invoke-virtual {v3}, Landroid/os/Vibrator;->hasVibrator()Z

    #@ae
    move-result v4

    #@af
    if-eqz v4, :cond_b5

    #@b1
    move v4, v5

    #@b2
    :goto_b2
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasVibrator:Z

    #@b4
    .line 148
    return-void

    #@b5
    :cond_b5
    move v4, v6

    #@b6
    .line 147
    goto :goto_b2
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/GlobalActions;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mCallState:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/GlobalActions;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput p1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mCallState:I

    #@2
    return p1
.end method

.method static synthetic access$100()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 85
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/GlobalActions;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/GlobalActions;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mKeyguardShowing:Z

    #@2
    return v0
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/GlobalActions;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDeviceProvisioned:Z

    #@2
    return v0
.end method

.method static synthetic access$1400()I
    .registers 1

    #@0
    .prologue
    .line 85
    sget v0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerMode:I

    #@2
    return v0
.end method

.method static synthetic access$1402(I)I
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    sput p0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerMode:I

    #@2
    return p0
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/GlobalActions;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->onAirplaneModeChanged()V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/GlobalActions;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->refreshSilentMode()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/GlobalActions;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->handleShow()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/GlobalActions;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasTelephony:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/GlobalActions;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mIsWaitingForEcmExit:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/android/internal/policy/impl/GlobalActions;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mIsWaitingForEcmExit:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/GlobalActions;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/GlobalActions;->changeAirplaneModeSystemSetting(Z)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/policy/impl/GlobalActions;Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/view/WindowManagerPolicy$WindowManagerFuncs;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@2
    return-object v0
.end method

.method private addUsersToMenu(Ljava/util/ArrayList;)V
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/policy/impl/GlobalActions$Action;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/policy/impl/GlobalActions$Action;>;"
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 464
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@4
    const-string v2, "user"

    #@6
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/os/UserManager;

    #@c
    invoke-virtual {v1}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@f
    move-result-object v10

    #@10
    .line 466
    .local v10, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@13
    move-result v1

    #@14
    if-le v1, v11, :cond_7e

    #@16
    .line 469
    :try_start_16
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@19
    move-result-object v1

    #@1a
    invoke-interface {v1}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1d} :catch_67

    #@1d
    move-result-object v6

    #@1e
    .line 473
    .local v6, currentUser:Landroid/content/pm/UserInfo;
    :goto_1e
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v7

    #@22
    .local v7, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_7e

    #@28
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v5

    #@2c
    check-cast v5, Landroid/content/pm/UserInfo;

    #@2e
    .line 474
    .local v5, user:Landroid/content/pm/UserInfo;
    if-nez v6, :cond_6c

    #@30
    iget v1, v5, Landroid/content/pm/UserInfo;->id:I

    #@32
    if-nez v1, :cond_6a

    #@34
    move v8, v11

    #@35
    .line 476
    .local v8, isCurrentUser:Z
    :goto_35
    iget-object v1, v5, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@37
    if-eqz v1, :cond_76

    #@39
    iget-object v1, v5, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@3b
    invoke-static {v1}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v3

    #@3f
    .line 478
    .local v3, icon:Landroid/graphics/drawable/Drawable;
    :goto_3f
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$8;

    #@41
    const v2, 0x108032b

    #@44
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    iget-object v1, v5, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@4b
    if-eqz v1, :cond_78

    #@4d
    iget-object v1, v5, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@4f
    :goto_4f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    if-eqz v8, :cond_7b

    #@55
    const-string v1, " \u2714"

    #@57
    :goto_57
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    move-object v1, p0

    #@60
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/GlobalActions$8;-><init>(Lcom/android/internal/policy/impl/GlobalActions;ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/content/pm/UserInfo;)V

    #@63
    .line 498
    .local v0, switchToUser:Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@66
    goto :goto_22

    #@67
    .line 470
    .end local v0           #switchToUser:Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;
    .end local v3           #icon:Landroid/graphics/drawable/Drawable;
    .end local v5           #user:Landroid/content/pm/UserInfo;
    .end local v6           #currentUser:Landroid/content/pm/UserInfo;
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #isCurrentUser:Z
    :catch_67
    move-exception v9

    #@68
    .line 471
    .local v9, re:Landroid/os/RemoteException;
    const/4 v6, 0x0

    #@69
    .restart local v6       #currentUser:Landroid/content/pm/UserInfo;
    goto :goto_1e

    #@6a
    .end local v9           #re:Landroid/os/RemoteException;
    .restart local v5       #user:Landroid/content/pm/UserInfo;
    .restart local v7       #i$:Ljava/util/Iterator;
    :cond_6a
    move v8, v12

    #@6b
    .line 474
    goto :goto_35

    #@6c
    :cond_6c
    iget v1, v6, Landroid/content/pm/UserInfo;->id:I

    #@6e
    iget v2, v5, Landroid/content/pm/UserInfo;->id:I

    #@70
    if-ne v1, v2, :cond_74

    #@72
    move v8, v11

    #@73
    goto :goto_35

    #@74
    :cond_74
    move v8, v12

    #@75
    goto :goto_35

    #@76
    .line 476
    .restart local v8       #isCurrentUser:Z
    :cond_76
    const/4 v3, 0x0

    #@77
    goto :goto_3f

    #@78
    .line 478
    .restart local v3       #icon:Landroid/graphics/drawable/Drawable;
    :cond_78
    const-string v1, "Primary"

    #@7a
    goto :goto_4f

    #@7b
    :cond_7b
    const-string v1, ""

    #@7d
    goto :goto_57

    #@7e
    .line 501
    .end local v3           #icon:Landroid/graphics/drawable/Drawable;
    .end local v5           #user:Landroid/content/pm/UserInfo;
    .end local v6           #currentUser:Landroid/content/pm/UserInfo;
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #isCurrentUser:Z
    :cond_7e
    return-void
.end method

.method private awakenIfNecessary()V
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDreamManager:Landroid/service/dreams/IDreamManager;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 170
    :try_start_4
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDreamManager:Landroid/service/dreams/IDreamManager;

    #@6
    invoke-interface {v0}, Landroid/service/dreams/IDreamManager;->isDreaming()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 171
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDreamManager:Landroid/service/dreams/IDreamManager;

    #@e
    invoke-interface {v0}, Landroid/service/dreams/IDreamManager;->awaken()V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_11} :catch_12

    #@11
    .line 177
    :cond_11
    :goto_11
    return-void

    #@12
    .line 173
    :catch_12
    move-exception v0

    #@13
    goto :goto_11
.end method

.method private changeAirplaneModeSystemSetting(Z)V
    .registers 11
    .parameter "on"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 1094
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v6

    #@9
    const-string v7, "airplane_mode_on"

    #@b
    if-eqz p1, :cond_90

    #@d
    move v3, v4

    #@e
    :goto_e
    invoke-static {v6, v7, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@11
    .line 1098
    const-string v3, "ro.airplane.phoneapp"

    #@13
    invoke-static {v3, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@16
    move-result v3

    #@17
    if-eq v3, v4, :cond_6d

    #@19
    .line 1099
    if-eqz p1, :cond_6d

    #@1b
    .line 1100
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneOnDialog:Landroid/app/AlertDialog;

    #@1d
    if-eqz v3, :cond_26

    #@1f
    .line 1101
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneOnDialog:Landroid/app/AlertDialog;

    #@21
    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    #@24
    .line 1102
    iput-object v8, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneOnDialog:Landroid/app/AlertDialog;

    #@26
    .line 1105
    :cond_26
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHandler:Landroid/os/Handler;

    #@28
    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@2b
    .line 1107
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@2d
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@2f
    const v4, 0x20a01cb

    #@32
    invoke-direct {v1, v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@35
    .line 1108
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    const v3, 0x1040014

    #@38
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@3b
    .line 1109
    const-string v3, "ro.build.target_operator"

    #@3d
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    const-string v4, "LGU"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_93

    #@49
    .line 1110
    const v3, 0x2090261

    #@4c
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@4f
    .line 1114
    :goto_4f
    const v3, 0x1010355

    #@52
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@55
    .line 1115
    const v3, 0x104000a

    #@58
    invoke-virtual {v1, v3, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@5b
    .line 1116
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@5e
    move-result-object v0

    #@5f
    .line 1117
    .local v0, alert:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@62
    move-result-object v3

    #@63
    const/16 v4, 0x7d3

    #@65
    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    #@68
    .line 1118
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@6b
    .line 1119
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneOnDialog:Landroid/app/AlertDialog;

    #@6d
    .line 1122
    .end local v0           #alert:Landroid/app/AlertDialog;
    .end local v1           #builder:Landroid/app/AlertDialog$Builder;
    :cond_6d
    new-instance v2, Landroid/content/Intent;

    #@6f
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    #@71
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@74
    .line 1123
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x2000

    #@76
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@79
    .line 1124
    const-string v3, "state"

    #@7b
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@7e
    .line 1125
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@80
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@82
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@85
    .line 1126
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasTelephony:Z

    #@87
    if-nez v3, :cond_8f

    #@89
    .line 1127
    if-eqz p1, :cond_9a

    #@8b
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@8d
    :goto_8d
    iput-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@8f
    .line 1129
    :cond_8f
    return-void

    #@90
    .end local v2           #intent:Landroid/content/Intent;
    :cond_90
    move v3, v5

    #@91
    .line 1094
    goto/16 :goto_e

    #@93
    .line 1112
    .restart local v1       #builder:Landroid/app/AlertDialog$Builder;
    :cond_93
    const v3, 0x2090260

    #@96
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@99
    goto :goto_4f

    #@9a
    .line 1127
    .end local v1           #builder:Landroid/app/AlertDialog$Builder;
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_9a
    sget-object v3, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@9c
    goto :goto_8d
.end method

.method private createDialog()Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 198
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;

    #@4
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAudioManager:Landroid/media/AudioManager;

    #@8
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHandler:Landroid/os/Handler;

    #@a
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;-><init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/os/Handler;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mSilentModeAction:Lcom/android/internal/policy/impl/GlobalActions$Action;

    #@f
    .line 200
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$1;

    #@11
    const v2, 0x10802e1

    #@14
    const v3, 0x10802e0

    #@17
    const v4, 0x2090019

    #@1a
    const v5, 0x2090018

    #@1d
    move-object v1, p0

    #@1e
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/GlobalActions$1;-><init>(Lcom/android/internal/policy/impl/GlobalActions;IIII)V

    #@21
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@23
    .line 279
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->onAirplaneModeChanged()V

    #@26
    .line 281
    new-instance v0, Ljava/util/ArrayList;

    #@28
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2b
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@2d
    .line 284
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@2f
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions$2;

    #@31
    const v2, 0x1080030

    #@34
    const v3, 0x1040104

    #@37
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions$2;-><init>(Lcom/android/internal/policy/impl/GlobalActions;II)V

    #@3a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 310
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@3f
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions$3;

    #@41
    const v2, 0x202029c

    #@44
    const v3, 0x209000e

    #@47
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions$3;-><init>(Lcom/android/internal/policy/impl/GlobalActions;II)V

    #@4a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    .line 331
    const-string v7, ""

    #@4f
    .line 332
    .local v7, homeprop:Ljava/lang/String;
    const-string v0, "service.plushome.currenthome"

    #@51
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v7

    #@55
    .line 333
    const-string v0, "kids"

    #@57
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v0

    #@5b
    if-nez v0, :cond_7b

    #@5d
    .line 334
    const-string v0, "ro.build.target_operator"

    #@5f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    const-string v1, "SKT"

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@68
    move-result v0

    #@69
    if-eqz v0, :cond_7b

    #@6b
    .line 335
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@6d
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions$4;

    #@6f
    const v2, 0x2020295

    #@72
    const v3, 0x2090010

    #@75
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions$4;-><init>(Lcom/android/internal/policy/impl/GlobalActions;II)V

    #@78
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7b
    .line 361
    :cond_7b
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@7d
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@7f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@82
    .line 364
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@84
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@87
    move-result-object v0

    #@88
    const-string v1, "bugreport_in_power_menu"

    #@8a
    invoke-static {v0, v1, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8d
    move-result v0

    #@8e
    if-eqz v0, :cond_a0

    #@90
    .line 366
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@92
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions$5;

    #@94
    const v2, 0x202056f

    #@97
    const v3, 0x1040105

    #@9a
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions$5;-><init>(Lcom/android/internal/policy/impl/GlobalActions;II)V

    #@9d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a0
    .line 414
    :cond_a0
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@a2
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mSilentModeAction:Lcom/android/internal/policy/impl/GlobalActions$Action;

    #@a4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a7
    .line 418
    const-string v0, "fw.power_user_switcher"

    #@a9
    invoke-static {v0, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@ac
    move-result v0

    #@ad
    if-eqz v0, :cond_b4

    #@af
    .line 419
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mItems:Ljava/util/ArrayList;

    #@b1
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/GlobalActions;->addUsersToMenu(Ljava/util/ArrayList;)V

    #@b4
    .line 422
    :cond_b4
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@b6
    const/4 v1, 0x0

    #@b7
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;-><init>(Lcom/android/internal/policy/impl/GlobalActions;Lcom/android/internal/policy/impl/GlobalActions$1;)V

    #@ba
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@bc
    .line 424
    new-instance v9, Lcom/android/internal/app/AlertController$AlertParams;

    #@be
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@c0
    invoke-direct {v9, v0}, Lcom/android/internal/app/AlertController$AlertParams;-><init>(Landroid/content/Context;)V

    #@c3
    .line 425
    .local v9, params:Lcom/android/internal/app/AlertController$AlertParams;
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@c5
    iput-object v0, v9, Lcom/android/internal/app/AlertController$AlertParams;->mAdapter:Landroid/widget/ListAdapter;

    #@c7
    .line 426
    iput-object p0, v9, Lcom/android/internal/app/AlertController$AlertParams;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    #@c9
    .line 427
    iput-boolean v11, v9, Lcom/android/internal/app/AlertController$AlertParams;->mForceInverseBackground:Z

    #@cb
    .line 429
    const-string v0, "ro.build.target_operator"

    #@cd
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d0
    move-result-object v8

    #@d1
    .line 430
    .local v8, operator:Ljava/lang/String;
    const-string v0, "VZW"

    #@d3
    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d6
    move-result v0

    #@d7
    if-eqz v0, :cond_ee

    #@d9
    .line 431
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@db
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@de
    move-result-object v0

    #@df
    const/high16 v1, 0x104

    #@e1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v0

    #@e5
    iput-object v0, v9, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@e7
    .line 432
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$6;

    #@e9
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/GlobalActions$6;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@ec
    iput-object v0, v9, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@ee
    .line 441
    :cond_ee
    new-instance v6, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@f0
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@f2
    invoke-direct {v6, v0, v9}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;-><init>(Landroid/content/Context;Lcom/android/internal/app/AlertController$AlertParams;)V

    #@f5
    .line 442
    .local v6, dialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
    invoke-virtual {v6, v10}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->setCanceledOnTouchOutside(Z)V

    #@f8
    .line 444
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getListView()Landroid/widget/ListView;

    #@fb
    move-result-object v0

    #@fc
    invoke-virtual {v0, v11}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    #@ff
    .line 445
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getListView()Landroid/widget/ListView;

    #@102
    move-result-object v0

    #@103
    invoke-virtual {v0, v11}, Landroid/widget/ListView;->setLongClickable(Z)V

    #@106
    .line 446
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getListView()Landroid/widget/ListView;

    #@109
    move-result-object v0

    #@10a
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions$7;

    #@10c
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/GlobalActions$7;-><init>(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@10f
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    #@112
    .line 455
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@115
    move-result-object v0

    #@116
    const/16 v1, 0x7d8

    #@118
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@11b
    .line 458
    invoke-virtual {v6, p0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@11e
    .line 460
    return-object v6
.end method

.method private handleShow()V
    .registers 4

    #@0
    .prologue
    .line 180
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->awakenIfNecessary()V

    #@3
    .line 181
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->createDialog()Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@6
    move-result-object v1

    #@7
    iput-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@9
    .line 182
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->prepareDialog()V

    #@c
    .line 184
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@e
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@15
    move-result-object v0

    #@16
    .line 185
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    const-string v1, "GlobalActions"

    #@18
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@1b
    .line 186
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@1d
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@24
    .line 187
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@26
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->show()V

    #@29
    .line 188
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@32
    move-result-object v1

    #@33
    const/high16 v2, 0x1

    #@35
    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@38
    .line 189
    return-void
.end method

.method private onAirplaneModeChanged()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1078
    const-string v2, "TMO"

    #@4
    const-string v3, "ro.build.target_operator"

    #@6
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_15

    #@10
    .line 1079
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasTelephony:Z

    #@12
    if-eqz v2, :cond_15

    #@14
    .line 1088
    :goto_14
    return-void

    #@15
    .line 1082
    :cond_15
    sget-object v2, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "airplane_mode_on"

    #@1d
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@20
    move-result v2

    #@21
    if-ne v2, v0, :cond_31

    #@23
    .line 1086
    .local v0, airplaneModeOn:Z
    :goto_23
    if-eqz v0, :cond_33

    #@25
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@27
    :goto_27
    iput-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@29
    .line 1087
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@2b
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2d
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->updateState(Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)V

    #@30
    goto :goto_14

    #@31
    .end local v0           #airplaneModeOn:Z
    :cond_31
    move v0, v1

    #@32
    .line 1082
    goto :goto_23

    #@33
    .line 1086
    .restart local v0       #airplaneModeOn:Z
    :cond_33
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@35
    goto :goto_27
.end method

.method private prepareDialog()V
    .registers 4

    #@0
    .prologue
    .line 504
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->refreshSilentMode()V

    #@3
    .line 505
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneModeOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@5
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAirplaneState:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@7
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->updateState(Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)V

    #@a
    .line 506
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;->notifyDataSetChanged()V

    #@f
    .line 508
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mKeyguardShowing:Z

    #@11
    if-eqz v1, :cond_2d

    #@13
    .line 509
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@15
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@18
    move-result-object v1

    #@19
    const/16 v2, 0x7d9

    #@1b
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@1e
    .line 515
    :goto_1e
    new-instance v0, Landroid/content/IntentFilter;

    #@20
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    #@22
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@25
    .line 516
    .local v0, filter:Landroid/content/IntentFilter;
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@27
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    #@29
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2c
    .line 518
    return-void

    #@2d
    .line 511
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_2d
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@2f
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->getWindow()Landroid/view/Window;

    #@32
    move-result-object v1

    #@33
    const/16 v2, 0x7d8

    #@35
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@38
    goto :goto_1e
.end method

.method private refreshSilentMode()V
    .registers 4

    #@0
    .prologue
    .line 521
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHasVibrator:Z

    #@2
    if-nez v1, :cond_19

    #@4
    .line 522
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAudioManager:Landroid/media/AudioManager;

    #@6
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    #@9
    move-result v1

    #@a
    const/4 v2, 0x2

    #@b
    if-eq v1, v2, :cond_1a

    #@d
    const/4 v0, 0x1

    #@e
    .line 524
    .local v0, silentModeOn:Z
    :goto_e
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mSilentModeAction:Lcom/android/internal/policy/impl/GlobalActions$Action;

    #@10
    check-cast v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;

    #@12
    if-eqz v0, :cond_1c

    #@14
    sget-object v2, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@16
    :goto_16
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;->updateState(Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;)V

    #@19
    .line 527
    .end local v0           #silentModeOn:Z
    :cond_19
    return-void

    #@1a
    .line 522
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_e

    #@1c
    .line 524
    .restart local v0       #silentModeOn:Z
    :cond_1c
    sget-object v2, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@1e
    goto :goto_16
.end method


# virtual methods
.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 1253
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    .line 1254
    const/4 v0, 0x1

    #@d
    .line 1256
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 4
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 538
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@2
    invoke-virtual {v0, p2}, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;->getItem(I)Lcom/android/internal/policy/impl/GlobalActions$Action;

    #@5
    move-result-object v0

    #@6
    instance-of v0, v0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;

    #@8
    if-nez v0, :cond_d

    #@a
    .line 539
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@d
    .line 541
    :cond_d
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mAdapter:Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@f
    invoke-virtual {v0, p2}, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;->getItem(I)Lcom/android/internal/policy/impl/GlobalActions$Action;

    #@12
    move-result-object v0

    #@13
    invoke-interface {v0}, Lcom/android/internal/policy/impl/GlobalActions$Action;->onPress()V

    #@16
    .line 542
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter "dialog"

    #@0
    .prologue
    .line 532
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@7
    .line 534
    return-void
.end method

.method public showDialog(ZZ)V
    .registers 5
    .parameter "keyguardShowing"
    .parameter "isDeviceProvisioned"

    #@0
    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/GlobalActions;->mKeyguardShowing:Z

    #@2
    .line 156
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDeviceProvisioned:Z

    #@4
    .line 157
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@6
    if-eqz v0, :cond_17

    #@8
    .line 158
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->dismiss()V

    #@d
    .line 159
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mDialog:Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@10
    .line 161
    iget-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v1, 0x2

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@16
    .line 165
    :goto_16
    return-void

    #@17
    .line 163
    :cond_17
    invoke-direct {p0}, Lcom/android/internal/policy/impl/GlobalActions;->handleShow()V

    #@1a
    goto :goto_16
.end method
