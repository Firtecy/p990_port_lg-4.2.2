.class Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;
.super Ljava/lang/Object;
.source "KeyguardWidgetPager.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ZInterpolator"
.end annotation


# instance fields
.field private focalLength:F


# direct methods
.method public constructor <init>(F)V
    .registers 2
    .parameter "foc"

    #@0
    .prologue
    .line 462
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 463
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;->focalLength:F

    #@5
    .line 464
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 467
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;->focalLength:F

    #@4
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;->focalLength:F

    #@6
    add-float/2addr v1, p1

    #@7
    div-float/2addr v0, v1

    #@8
    sub-float v0, v3, v0

    #@a
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;->focalLength:F

    #@c
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;->focalLength:F

    #@e
    add-float/2addr v2, v3

    #@f
    div-float/2addr v1, v2

    #@10
    sub-float v1, v3, v1

    #@12
    div-float/2addr v0, v1

    #@13
    return v0
.end method
