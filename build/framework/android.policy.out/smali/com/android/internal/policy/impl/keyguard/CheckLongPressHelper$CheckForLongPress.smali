.class Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;
.super Ljava/lang/Object;
.source "CheckLongPressHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 31
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$000(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@9
    move-result-object v0

    #@a
    if-eqz v0, :cond_3c

    #@c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$000(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Landroid/view/View;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/view/View;->hasWindowFocus()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_3c

    #@18
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@1a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$100(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_3c

    #@20
    .line 35
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@22
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$000(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Landroid/view/View;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Landroid/view/View;->performLongClick()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_3c

    #@2c
    .line 36
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@2e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$000(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;)Landroid/view/View;

    #@31
    move-result-object v0

    #@32
    const/4 v1, 0x0

    #@33
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    #@36
    .line 37
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper$CheckForLongPress;->this$0:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@38
    const/4 v1, 0x1

    #@39
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->access$102(Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;Z)Z

    #@3c
    .line 40
    :cond_3c
    return-void
.end method
