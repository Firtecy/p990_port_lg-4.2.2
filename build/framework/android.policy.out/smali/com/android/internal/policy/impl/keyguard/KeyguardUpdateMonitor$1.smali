.class Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;
.super Landroid/os/Handler;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 318
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 321
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_a0

    #@5
    .line 389
    :goto_5
    return-void

    #@6
    .line 323
    :sswitch_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@b
    goto :goto_5

    #@c
    .line 326
    :sswitch_c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@12
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V

    #@15
    goto :goto_5

    #@16
    .line 329
    :sswitch_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@18
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@1a
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V

    #@1d
    goto :goto_5

    #@1e
    .line 332
    :sswitch_1e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@20
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;

    #@24
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;)V

    #@27
    goto :goto_5

    #@28
    .line 335
    :sswitch_28
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@2c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleRingerModeChange(I)V

    #@2f
    goto :goto_5

    #@30
    .line 338
    :sswitch_30
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@32
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@34
    check-cast v0, Ljava/lang/String;

    #@36
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handlePhoneStateChanged(Ljava/lang/String;)V

    #@39
    goto :goto_5

    #@3a
    .line 341
    :sswitch_3a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@3f
    goto :goto_5

    #@40
    .line 344
    :sswitch_40
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@42
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleDeviceProvisioned()V

    #@45
    goto :goto_5

    #@46
    .line 347
    :sswitch_46
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@48
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleDevicePolicyManagerStateChanged()V

    #@4b
    goto :goto_5

    #@4c
    .line 350
    :sswitch_4c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@4e
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@50
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@52
    check-cast v0, Landroid/os/IRemoteCallback;

    #@54
    invoke-virtual {v1, v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleUserSwitched(ILandroid/os/IRemoteCallback;)V

    #@57
    goto :goto_5

    #@58
    .line 353
    :sswitch_58
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@5a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@5c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleUserRemoved(I)V

    #@5f
    goto :goto_5

    #@60
    .line 356
    :sswitch_60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@62
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@64
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V

    #@67
    goto :goto_5

    #@68
    .line 359
    :sswitch_68
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleBootCompleted()V

    #@6d
    goto :goto_5

    #@6e
    .line 363
    :sswitch_6e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@70
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@72
    check-cast v0, Ljava/lang/Boolean;

    #@74
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleHdmiPlugStateChanged(Ljava/lang/Boolean;)V

    #@77
    goto :goto_5

    #@78
    .line 368
    :sswitch_78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleNextAlarm()V

    #@7d
    goto :goto_5

    #@7e
    .line 373
    :sswitch_7e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@80
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@82
    check-cast v0, Ljava/lang/Integer;

    #@84
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@87
    move-result v0

    #@88
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V

    #@8b
    goto/16 :goto_5

    #@8d
    .line 379
    :sswitch_8d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@91
    check-cast v0, Ljava/lang/Boolean;

    #@93
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleDSDPStateChanged(Ljava/lang/Boolean;)V

    #@96
    goto/16 :goto_5

    #@98
    .line 385
    :sswitch_98
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@9a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@9d
    goto/16 :goto_5

    #@9f
    .line 321
    nop

    #@a0
    :sswitch_data_a0
    .sparse-switch
        0x12d -> :sswitch_6
        0x12e -> :sswitch_c
        0x12f -> :sswitch_16
        0x130 -> :sswitch_1e
        0x131 -> :sswitch_28
        0x132 -> :sswitch_30
        0x133 -> :sswitch_3a
        0x134 -> :sswitch_40
        0x135 -> :sswitch_46
        0x136 -> :sswitch_4c
        0x137 -> :sswitch_58
        0x138 -> :sswitch_60
        0x139 -> :sswitch_68
        0x13a -> :sswitch_78
        0x13b -> :sswitch_6e
        0x13c -> :sswitch_7e
        0x13d -> :sswitch_8d
        0x191 -> :sswitch_98
        0x192 -> :sswitch_98
    .end sparse-switch
.end method
