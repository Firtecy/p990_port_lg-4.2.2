.class Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;
.super Ljava/lang/Object;
.source "LiftToActivateListener.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field private final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mCachedClickableState:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const-string v0, "accessibility"

    #@5
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@d
    .line 37
    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    .line 43
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@2
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_17

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@a
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_17

    #@10
    .line 45
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@13
    move-result v2

    #@14
    packed-switch v2, :pswitch_data_5c

    #@17
    .line 66
    :cond_17
    :goto_17
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1a
    .line 69
    const/4 v2, 0x1

    #@1b
    return v2

    #@1c
    .line 49
    :pswitch_1c
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    #@1f
    move-result v2

    #@20
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;->mCachedClickableState:Z

    #@22
    .line 50
    const/4 v2, 0x0

    #@23
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    #@26
    goto :goto_17

    #@27
    .line 53
    :pswitch_27
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@2a
    move-result v2

    #@2b
    float-to-int v0, v2

    #@2c
    .line 54
    .local v0, x:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    #@2f
    move-result v2

    #@30
    float-to-int v1, v2

    #@31
    .line 55
    .local v1, y:I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    #@34
    move-result v2

    #@35
    if-le v0, v2, :cond_56

    #@37
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    #@3a
    move-result v2

    #@3b
    if-le v1, v2, :cond_56

    #@3d
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@40
    move-result v2

    #@41
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    #@44
    move-result v3

    #@45
    sub-int/2addr v2, v3

    #@46
    if-ge v0, v2, :cond_56

    #@48
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@4b
    move-result v2

    #@4c
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    #@4f
    move-result v3

    #@50
    sub-int/2addr v2, v3

    #@51
    if-ge v1, v2, :cond_56

    #@53
    .line 58
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    #@56
    .line 60
    :cond_56
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;->mCachedClickableState:Z

    #@58
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    #@5b
    goto :goto_17

    #@5c
    .line 45
    :pswitch_data_5c
    .packed-switch 0x9
        :pswitch_1c
        :pswitch_27
    .end packed-switch
.end method
