.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1094
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onKeyguardPackageStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 1097
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    monitor-enter v1

    #@3
    .line 1098
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@8
    move-result-object v0

    #@9
    if-eqz v0, :cond_36

    #@b
    .line 1099
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mStateContainer:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@12
    .line 1100
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@14
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->cleanUp()V

    #@1b
    .line 1101
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@1d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@20
    move-result-object v0

    #@21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@23
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    #@2a
    .line 1102
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2c
    const/4 v2, 0x0

    #@2d
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$502(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@30
    .line 1103
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@32
    const/4 v2, 0x0

    #@33
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->show(Landroid/os/Bundle;)V

    #@36
    .line 1105
    :cond_36
    monitor-exit v1

    #@37
    .line 1106
    return-void

    #@38
    .line 1105
    :catchall_38
    move-exception v0

    #@39
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_3 .. :try_end_3a} :catchall_38

    #@3a
    throw v0
.end method
