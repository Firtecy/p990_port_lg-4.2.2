.class Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;
.super Ljava/lang/Object;
.source "FaceUnlock.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 432
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 5
    .parameter "className"
    .parameter "iservice"

    #@0
    .prologue
    .line 438
    const-string v0, "FULLockscreen"

    #@2
    const-string v1, "Connected to Face Unlock service"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 439
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@9
    invoke-static {p2}, Lcom/android/internal/policy/IFaceLockInterface$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/policy/IFaceLockInterface;

    #@c
    move-result-object v1

    #@d
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$002(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;Lcom/android/internal/policy/IFaceLockInterface;)Lcom/android/internal/policy/IFaceLockInterface;

    #@10
    .line 440
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@12
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x2

    #@17
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@1a
    .line 441
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "className"

    #@0
    .prologue
    .line 448
    const-string v0, "FULLockscreen"

    #@2
    const-string v1, "Unexpected disconnect from Face Unlock service"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 449
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@9
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@c
    move-result-object v0

    #@d
    const/4 v1, 0x3

    #@e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@11
    .line 450
    return-void
.end method
