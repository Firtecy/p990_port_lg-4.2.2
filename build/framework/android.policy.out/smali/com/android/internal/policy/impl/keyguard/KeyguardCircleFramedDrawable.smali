.class Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "KeyguardCircleFramedDrawable.java"


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private mDstRect:Landroid/graphics/RectF;

.field private final mFrameColor:I

.field private mFramePath:Landroid/graphics/Path;

.field private mFrameRect:Landroid/graphics/RectF;

.field private final mFrameShadowColor:I

.field private final mHighlightColor:I

.field private final mPaint:Landroid/graphics/Paint;

.field private mPressed:Z

.field private mScale:F

.field private final mShadowRadius:F

.field private final mSize:I

.field private mSrcRect:Landroid/graphics/Rect;

.field private final mStrokeWidth:F


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;IIFIFI)V
    .registers 21
    .parameter "bitmap"
    .parameter "size"
    .parameter "frameColor"
    .parameter "strokeWidth"
    .parameter "frameShadowColor"
    .parameter "shadowRadius"
    .parameter "highlightColor"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 57
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@5
    .line 58
    move/from16 v0, p6

    #@7
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@9
    .line 59
    move/from16 v0, p3

    #@b
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameColor:I

    #@d
    .line 60
    move/from16 v0, p5

    #@f
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameShadowColor:I

    #@11
    .line 61
    move/from16 v0, p4

    #@13
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@15
    .line 62
    move/from16 v0, p7

    #@17
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mHighlightColor:I

    #@19
    .line 64
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@1b
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@1d
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1f
    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@22
    move-result-object v8

    #@23
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@25
    .line 65
    new-instance v1, Landroid/graphics/Canvas;

    #@27
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@29
    invoke-direct {v1, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@2c
    .line 67
    .local v1, canvas:Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@2f
    move-result v7

    #@30
    .line 68
    .local v7, width:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@33
    move-result v5

    #@34
    .line 69
    .local v5, height:I
    invoke-static {v7, v5}, Ljava/lang/Math;->min(II)I

    #@37
    move-result v6

    #@38
    .line 71
    .local v6, square:I
    new-instance v3, Landroid/graphics/Rect;

    #@3a
    sub-int v8, v7, v6

    #@3c
    div-int/lit8 v8, v8, 0x2

    #@3e
    sub-int v9, v5, v6

    #@40
    div-int/lit8 v9, v9, 0x2

    #@42
    invoke-direct {v3, v8, v9, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    #@45
    .line 72
    .local v3, cropRect:Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/RectF;

    #@47
    const/4 v8, 0x0

    #@48
    const/4 v9, 0x0

    #@49
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@4b
    int-to-float v10, v10

    #@4c
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@4e
    int-to-float v11, v11

    #@4f
    invoke-direct {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@52
    .line 73
    .local v2, circleRect:Landroid/graphics/RectF;
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@54
    const/high16 v9, 0x4000

    #@56
    div-float/2addr v8, v9

    #@57
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@59
    const/high16 v10, 0x4000

    #@5b
    div-float/2addr v9, v10

    #@5c
    invoke-virtual {v2, v8, v9}, Landroid/graphics/RectF;->inset(FF)V

    #@5f
    .line 74
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@61
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@63
    invoke-virtual {v2, v8, v9}, Landroid/graphics/RectF;->inset(FF)V

    #@66
    .line 76
    new-instance v4, Landroid/graphics/Path;

    #@68
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    #@6b
    .line 77
    .local v4, fillPath:Landroid/graphics/Path;
    const/4 v8, 0x0

    #@6c
    const/high16 v9, 0x43b4

    #@6e
    invoke-virtual {v4, v2, v8, v9}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    #@71
    .line 79
    const/4 v8, 0x0

    #@72
    sget-object v9, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@74
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@77
    .line 82
    new-instance v8, Landroid/graphics/Paint;

    #@79
    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    #@7c
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@7e
    .line 83
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@80
    const/4 v9, 0x1

    #@81
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@84
    .line 84
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@86
    const/high16 v9, -0x100

    #@88
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    #@8b
    .line 85
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@8d
    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@8f
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@92
    .line 86
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@94
    invoke-virtual {v1, v4, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@97
    .line 89
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@99
    new-instance v9, Landroid/graphics/PorterDuffXfermode;

    #@9b
    sget-object v10, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@9d
    invoke-direct {v9, v10}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@a0
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@a3
    .line 90
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@a5
    invoke-virtual {v1, p1, v3, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@a8
    .line 93
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@aa
    const/4 v9, 0x0

    #@ab
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@ae
    .line 95
    const/high16 v8, 0x3f80

    #@b0
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mScale:F

    #@b2
    .line 97
    new-instance v8, Landroid/graphics/Rect;

    #@b4
    const/4 v9, 0x0

    #@b5
    const/4 v10, 0x0

    #@b6
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@b8
    iget v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@ba
    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@bd
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSrcRect:Landroid/graphics/Rect;

    #@bf
    .line 98
    new-instance v8, Landroid/graphics/RectF;

    #@c1
    const/4 v9, 0x0

    #@c2
    const/4 v10, 0x0

    #@c3
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@c5
    int-to-float v11, v11

    #@c6
    iget v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSize:I

    #@c8
    int-to-float v12, v12

    #@c9
    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@cc
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mDstRect:Landroid/graphics/RectF;

    #@ce
    .line 99
    new-instance v8, Landroid/graphics/RectF;

    #@d0
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mDstRect:Landroid/graphics/RectF;

    #@d2
    invoke-direct {v8, v9}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@d5
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameRect:Landroid/graphics/RectF;

    #@d7
    .line 100
    new-instance v8, Landroid/graphics/Path;

    #@d9
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    #@dc
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFramePath:Landroid/graphics/Path;

    #@de
    .line 101
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 11
    .parameter "canvas"

    #@0
    .prologue
    const/high16 v7, 0x4000

    #@2
    const/4 v8, 0x0

    #@3
    .line 106
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    #@6
    move-result v3

    #@7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    #@a
    move-result v4

    #@b
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@e
    move-result v3

    #@f
    int-to-float v1, v3

    #@10
    .line 107
    .local v1, outside:F
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mScale:F

    #@12
    mul-float v0, v3, v1

    #@14
    .line 108
    .local v0, inside:F
    sub-float v3, v1, v0

    #@16
    div-float v2, v3, v7

    #@18
    .line 110
    .local v2, pad:F
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mDstRect:Landroid/graphics/RectF;

    #@1a
    sub-float v4, v1, v2

    #@1c
    sub-float v5, v1, v2

    #@1e
    invoke-virtual {v3, v2, v2, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    #@21
    .line 111
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@23
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mSrcRect:Landroid/graphics/Rect;

    #@25
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mDstRect:Landroid/graphics/RectF;

    #@27
    const/4 v6, 0x0

    #@28
    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@2b
    .line 113
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameRect:Landroid/graphics/RectF;

    #@2d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mDstRect:Landroid/graphics/RectF;

    #@2f
    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    #@32
    .line 114
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameRect:Landroid/graphics/RectF;

    #@34
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@36
    div-float/2addr v4, v7

    #@37
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@39
    div-float/2addr v5, v7

    #@3a
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->inset(FF)V

    #@3d
    .line 115
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameRect:Landroid/graphics/RectF;

    #@3f
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@41
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@43
    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->inset(FF)V

    #@46
    .line 117
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFramePath:Landroid/graphics/Path;

    #@48
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    #@4b
    .line 118
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFramePath:Landroid/graphics/Path;

    #@4d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameRect:Landroid/graphics/RectF;

    #@4f
    const/high16 v5, 0x43b4

    #@51
    invoke-virtual {v3, v4, v8, v5}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    #@54
    .line 121
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPressed:Z

    #@56
    if-eqz v3, :cond_83

    #@58
    .line 122
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@5a
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@5c
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@5f
    .line 123
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@61
    const/16 v4, 0x54

    #@63
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mHighlightColor:I

    #@65
    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    #@68
    move-result v5

    #@69
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mHighlightColor:I

    #@6b
    invoke-static {v6}, Landroid/graphics/Color;->green(I)I

    #@6e
    move-result v6

    #@6f
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mHighlightColor:I

    #@71
    invoke-static {v7}, Landroid/graphics/Color;->blue(I)I

    #@74
    move-result v7

    #@75
    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    #@78
    move-result v4

    #@79
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@7c
    .line 127
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFramePath:Landroid/graphics/Path;

    #@7e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@80
    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@83
    .line 129
    :cond_83
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@85
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mStrokeWidth:F

    #@87
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@8a
    .line 130
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@8c
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@8e
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@91
    .line 131
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@93
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPressed:Z

    #@95
    if-eqz v3, :cond_ad

    #@97
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mHighlightColor:I

    #@99
    :goto_99
    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    #@9c
    .line 132
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@9e
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mShadowRadius:F

    #@a0
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameShadowColor:I

    #@a2
    invoke-virtual {v3, v4, v8, v8, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    #@a5
    .line 133
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFramePath:Landroid/graphics/Path;

    #@a7
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPaint:Landroid/graphics/Paint;

    #@a9
    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@ac
    .line 134
    return-void

    #@ad
    .line 131
    :cond_ad
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mFrameColor:I

    #@af
    goto :goto_99
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 150
    const/4 v0, -0x3

    #@1
    return v0
.end method

.method public getScale()F
    .registers 2

    #@0
    .prologue
    .line 141
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mScale:F

    #@2
    return v0
.end method

.method public setAlpha(I)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 155
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter "cf"

    #@0
    .prologue
    .line 159
    return-void
.end method

.method public setPressed(Z)V
    .registers 2
    .parameter "pressed"

    #@0
    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mPressed:Z

    #@2
    .line 146
    return-void
.end method

.method public setScale(F)V
    .registers 2
    .parameter "scale"

    #@0
    .prologue
    .line 137
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->mScale:F

    #@2
    .line 138
    return-void
.end method
