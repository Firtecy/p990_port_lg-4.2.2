.class Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;
.super Landroid/os/Handler;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager$25;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

.field final synthetic val$myConn:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager$25;Landroid/os/Looper;Landroid/content/ServiceConnection;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    #@0
    .prologue
    .line 6069
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@2
    iput-object p3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->val$myConn:Landroid/content/ServiceConnection;

    #@4
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 6072
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotLock:Ljava/lang/Object;

    #@6
    monitor-enter v1

    #@7
    .line 6073
    :try_start_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@9
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@d
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->val$myConn:Landroid/content/ServiceConnection;

    #@f
    if-ne v0, v2, :cond_36

    #@11
    .line 6074
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@13
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@15
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@17
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@19
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1b
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@1d
    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@20
    .line 6075
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@22
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@24
    const/4 v2, 0x0

    #@25
    iput-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@27
    .line 6076
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@29
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2d
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;->this$1:Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@2f
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@31
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotTimeout:Ljava/lang/Runnable;

    #@33
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@36
    .line 6078
    :cond_36
    monitor-exit v1

    #@37
    .line 6079
    return-void

    #@38
    .line 6078
    :catchall_38
    move-exception v0

    #@39
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_7 .. :try_end_3a} :catchall_38

    #@3a
    throw v0
.end method
