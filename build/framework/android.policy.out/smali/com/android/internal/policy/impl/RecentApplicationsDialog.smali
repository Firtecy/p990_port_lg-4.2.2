.class public Lcom/android/internal/policy/impl/RecentApplicationsDialog;
.super Landroid/app/Dialog;
.source "RecentApplicationsDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    }
.end annotation


# static fields
.field private static final DBG_FORCE_EMPTY_LIST:Z = false

.field private static final MAX_RECENT_TASKS:I = 0x10

.field private static final NUM_BUTTONS:I = 0x8

.field private static sStatusBar:Landroid/app/StatusBarManager;


# instance fields
.field mBroadcastIntentFilter:Landroid/content/IntentFilter;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field mCleanup:Ljava/lang/Runnable;

.field mHandler:Landroid/os/Handler;

.field final mIcons:[Landroid/widget/TextView;

.field mNoAppsText:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 75
    const v0, 0x10302fc

    #@3
    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@6
    .line 54
    const/16 v0, 0x8

    #@8
    new-array v0, v0, [Landroid/widget/TextView;

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@c
    .line 56
    new-instance v0, Landroid/content/IntentFilter;

    #@e
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@10
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@13
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mBroadcastIntentFilter:Landroid/content/IntentFilter;

    #@15
    .line 63
    new-instance v0, Landroid/os/Handler;

    #@17
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mHandler:Landroid/os/Handler;

    #@1c
    .line 64
    new-instance v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog$1;

    #@1e
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog$1;-><init>(Lcom/android/internal/policy/impl/RecentApplicationsDialog;)V

    #@21
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mCleanup:Ljava/lang/Runnable;

    #@23
    .line 335
    new-instance v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog$2;

    #@25
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog$2;-><init>(Lcom/android/internal/policy/impl/RecentApplicationsDialog;)V

    #@28
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@2a
    .line 77
    return-void
.end method

.method private reloadButtons()V
    .registers 25

    #@0
    .prologue
    .line 257
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@3
    move-result-object v6

    #@4
    .line 258
    .local v6, context:Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v15

    #@8
    .line 259
    .local v15, pm:Landroid/content/pm/PackageManager;
    const-string v21, "activity"

    #@a
    move-object/from16 v0, v21

    #@c
    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    check-cast v5, Landroid/app/ActivityManager;

    #@12
    .line 261
    .local v5, am:Landroid/app/ActivityManager;
    const/16 v21, 0x10

    #@14
    const/16 v22, 0x2

    #@16
    move/from16 v0, v21

    #@18
    move/from16 v1, v22

    #@1a
    invoke-virtual {v5, v0, v1}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    #@1d
    move-result-object v16

    #@1e
    .line 264
    .local v16, recentTasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    new-instance v21, Landroid/content/Intent;

    #@20
    const-string v22, "android.intent.action.MAIN"

    #@22
    invoke-direct/range {v21 .. v22}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@25
    const-string v22, "android.intent.category.HOME"

    #@27
    invoke-virtual/range {v21 .. v22}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@2a
    move-result-object v21

    #@2b
    const/16 v22, 0x0

    #@2d
    move-object/from16 v0, v21

    #@2f
    move/from16 v1, v22

    #@31
    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@34
    move-result-object v7

    #@35
    .line 268
    .local v7, homeInfo:Landroid/content/pm/ActivityInfo;
    new-instance v10, Lcom/android/internal/policy/impl/IconUtilities;

    #@37
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@3a
    move-result-object v21

    #@3b
    move-object/from16 v0, v21

    #@3d
    invoke-direct {v10, v0}, Lcom/android/internal/policy/impl/IconUtilities;-><init>(Landroid/content/Context;)V

    #@40
    .line 273
    .local v10, iconUtilities:Lcom/android/internal/policy/impl/IconUtilities;
    const/4 v11, 0x0

    #@41
    .line 274
    .local v11, index:I
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    #@44
    move-result v14

    #@45
    .line 275
    .local v14, numTasks:I
    const/4 v8, 0x0

    #@46
    .local v8, i:I
    :goto_46
    if-ge v8, v14, :cond_11c

    #@48
    const/16 v21, 0x8

    #@4a
    move/from16 v0, v21

    #@4c
    if-ge v11, v0, :cond_11c

    #@4e
    .line 276
    move-object/from16 v0, v16

    #@50
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v12

    #@54
    check-cast v12, Landroid/app/ActivityManager$RecentTaskInfo;

    #@56
    .line 281
    .local v12, info:Landroid/app/ActivityManager$RecentTaskInfo;
    new-instance v13, Landroid/content/Intent;

    #@58
    iget-object v0, v12, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    #@5a
    move-object/from16 v21, v0

    #@5c
    move-object/from16 v0, v21

    #@5e
    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@61
    .line 282
    .local v13, intent:Landroid/content/Intent;
    iget-object v0, v12, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    #@63
    move-object/from16 v21, v0

    #@65
    if-eqz v21, :cond_70

    #@67
    .line 283
    iget-object v0, v12, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    #@69
    move-object/from16 v21, v0

    #@6b
    move-object/from16 v0, v21

    #@6d
    invoke-virtual {v13, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@70
    .line 287
    :cond_70
    if-eqz v7, :cond_99

    #@72
    .line 288
    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    #@74
    move-object/from16 v21, v0

    #@76
    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@79
    move-result-object v22

    #@7a
    invoke-virtual/range {v22 .. v22}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@7d
    move-result-object v22

    #@7e
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v21

    #@82
    if-eqz v21, :cond_99

    #@84
    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@86
    move-object/from16 v21, v0

    #@88
    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8b
    move-result-object v22

    #@8c
    invoke-virtual/range {v22 .. v22}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@8f
    move-result-object v22

    #@90
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v21

    #@94
    if-eqz v21, :cond_99

    #@96
    .line 275
    :cond_96
    :goto_96
    add-int/lit8 v8, v8, 0x1

    #@98
    goto :goto_46

    #@99
    .line 296
    :cond_99
    invoke-virtual {v13}, Landroid/content/Intent;->getFlags()I

    #@9c
    move-result v21

    #@9d
    const v22, -0x200001

    #@a0
    and-int v21, v21, v22

    #@a2
    const/high16 v22, 0x1000

    #@a4
    or-int v21, v21, v22

    #@a6
    move/from16 v0, v21

    #@a8
    invoke-virtual {v13, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ab
    .line 298
    const/16 v21, 0x0

    #@ad
    move/from16 v0, v21

    #@af
    invoke-virtual {v15, v13, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@b2
    move-result-object v17

    #@b3
    .line 299
    .local v17, resolveInfo:Landroid/content/pm/ResolveInfo;
    if-eqz v17, :cond_96

    #@b5
    .line 300
    move-object/from16 v0, v17

    #@b7
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@b9
    .line 301
    .local v4, activityInfo:Landroid/content/pm/ActivityInfo;
    invoke-virtual {v4, v15}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@bc
    move-result-object v21

    #@bd
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c0
    move-result-object v19

    #@c1
    .line 302
    .local v19, title:Ljava/lang/String;
    invoke-virtual {v4, v15}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@c4
    move-result-object v9

    #@c5
    .line 304
    .local v9, icon:Landroid/graphics/drawable/Drawable;
    if-eqz v19, :cond_96

    #@c7
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    #@ca
    move-result v21

    #@cb
    if-lez v21, :cond_96

    #@cd
    if-eqz v9, :cond_96

    #@cf
    .line 305
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@d3
    move-object/from16 v21, v0

    #@d5
    aget-object v20, v21, v11

    #@d7
    .line 306
    .local v20, tv:Landroid/widget/TextView;
    move-object/from16 v0, v20

    #@d9
    move-object/from16 v1, v19

    #@db
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@de
    .line 307
    invoke-virtual {v10, v9}, Lcom/android/internal/policy/impl/IconUtilities;->createIconDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    #@e1
    move-result-object v9

    #@e2
    .line 308
    const/16 v21, 0x0

    #@e4
    const/16 v22, 0x0

    #@e6
    const/16 v23, 0x0

    #@e8
    move-object/from16 v0, v20

    #@ea
    move-object/from16 v1, v21

    #@ec
    move-object/from16 v2, v22

    #@ee
    move-object/from16 v3, v23

    #@f0
    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@f3
    .line 309
    new-instance v18, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;

    #@f5
    move-object/from16 v0, v18

    #@f7
    move-object/from16 v1, p0

    #@f9
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;-><init>(Lcom/android/internal/policy/impl/RecentApplicationsDialog;)V

    #@fc
    .line 310
    .local v18, tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    move-object/from16 v0, v18

    #@fe
    iput-object v12, v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->info:Landroid/app/ActivityManager$RecentTaskInfo;

    #@100
    .line 311
    move-object/from16 v0, v18

    #@102
    iput-object v13, v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->intent:Landroid/content/Intent;

    #@104
    .line 312
    move-object/from16 v0, v20

    #@106
    move-object/from16 v1, v18

    #@108
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    #@10b
    .line 313
    const/16 v21, 0x0

    #@10d
    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    #@110
    .line 314
    const/16 v21, 0x0

    #@112
    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setPressed(Z)V

    #@115
    .line 315
    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->clearFocus()V

    #@118
    .line 316
    add-int/lit8 v11, v11, 0x1

    #@11a
    goto/16 :goto_96

    #@11c
    .line 322
    .end local v4           #activityInfo:Landroid/content/pm/ActivityInfo;
    .end local v9           #icon:Landroid/graphics/drawable/Drawable;
    .end local v12           #info:Landroid/app/ActivityManager$RecentTaskInfo;
    .end local v13           #intent:Landroid/content/Intent;
    .end local v17           #resolveInfo:Landroid/content/pm/ResolveInfo;
    .end local v18           #tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    .end local v19           #title:Ljava/lang/String;
    .end local v20           #tv:Landroid/widget/TextView;
    :cond_11c
    move-object/from16 v0, p0

    #@11e
    iget-object v0, v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mNoAppsText:Landroid/view/View;

    #@120
    move-object/from16 v22, v0

    #@122
    if-nez v11, :cond_143

    #@124
    const/16 v21, 0x0

    #@126
    :goto_126
    move-object/from16 v0, v22

    #@128
    move/from16 v1, v21

    #@12a
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@12d
    .line 325
    :goto_12d
    const/16 v21, 0x8

    #@12f
    move/from16 v0, v21

    #@131
    if-ge v11, v0, :cond_146

    #@133
    .line 326
    move-object/from16 v0, p0

    #@135
    iget-object v0, v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@137
    move-object/from16 v21, v0

    #@139
    aget-object v21, v21, v11

    #@13b
    const/16 v22, 0x8

    #@13d
    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setVisibility(I)V

    #@140
    .line 325
    add-int/lit8 v11, v11, 0x1

    #@142
    goto :goto_12d

    #@143
    .line 322
    :cond_143
    const/16 v21, 0x8

    #@145
    goto :goto_126

    #@146
    .line 328
    :cond_146
    return-void
.end method

.method private switchTo(Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;)V
    .registers 6
    .parameter "tag"

    #@0
    .prologue
    .line 202
    iget-object v2, p1, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->info:Landroid/app/ActivityManager$RecentTaskInfo;

    #@2
    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    #@4
    if-ltz v2, :cond_1b

    #@6
    .line 204
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    const-string v3, "activity"

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/ActivityManager;

    #@12
    .line 206
    .local v0, am:Landroid/app/ActivityManager;
    iget-object v2, p1, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->info:Landroid/app/ActivityManager$RecentTaskInfo;

    #@14
    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    #@16
    const/4 v3, 0x1

    #@17
    invoke-virtual {v0, v2, v3}, Landroid/app/ActivityManager;->moveTaskToFront(II)V

    #@1a
    .line 216
    .end local v0           #am:Landroid/app/ActivityManager;
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 207
    :cond_1b
    iget-object v2, p1, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->intent:Landroid/content/Intent;

    #@1d
    if-eqz v2, :cond_1a

    #@1f
    .line 208
    iget-object v2, p1, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->intent:Landroid/content/Intent;

    #@21
    const v3, 0x104000

    #@24
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@27
    .line 211
    :try_start_27
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@2a
    move-result-object v2

    #@2b
    iget-object v3, p1, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;->intent:Landroid/content/Intent;

    #@2d
    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_30
    .catch Landroid/content/ActivityNotFoundException; {:try_start_27 .. :try_end_30} :catch_31

    #@30
    goto :goto_1a

    #@31
    .line 212
    :catch_31
    move-exception v1

    #@32
    .line 213
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "Recent"

    #@34
    const-string v3, "Unable to launch recent task"

    #@36
    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_1a
.end method


# virtual methods
.method public dismissAndSwitch()V
    .registers 5

    #@0
    .prologue
    .line 168
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@2
    array-length v1, v3

    #@3
    .line 169
    .local v1, numIcons:I
    const/4 v2, 0x0

    #@4
    .line 170
    .local v2, tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_11

    #@7
    .line 171
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@9
    aget-object v3, v3, v0

    #@b
    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1a

    #@11
    .line 181
    :cond_11
    if-eqz v2, :cond_16

    #@13
    .line 182
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->switchTo(Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;)V

    #@16
    .line 184
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->dismiss()V

    #@19
    .line 185
    return-void

    #@1a
    .line 174
    :cond_1a
    if-eqz v0, :cond_26

    #@1c
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@1e
    aget-object v3, v3, v0

    #@20
    invoke-virtual {v3}, Landroid/widget/TextView;->hasFocus()Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_3a

    #@26
    .line 175
    :cond_26
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@28
    aget-object v3, v3, v0

    #@2a
    invoke-virtual {v3}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    #@2d
    move-result-object v2

    #@2e
    .end local v2           #tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    check-cast v2, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;

    #@30
    .line 176
    .restart local v2       #tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@32
    aget-object v3, v3, v0

    #@34
    invoke-virtual {v3}, Landroid/widget/TextView;->hasFocus()Z

    #@37
    move-result v3

    #@38
    if-nez v3, :cond_11

    #@3a
    .line 170
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_5
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@2
    .local v0, arr$:[Landroid/widget/TextView;
    array-length v3, v0

    #@3
    .local v3, len$:I
    const/4 v2, 0x0

    #@4
    .local v2, i$:I
    :goto_4
    if-ge v2, v3, :cond_13

    #@6
    aget-object v1, v0, v2

    #@8
    .line 192
    .local v1, b:Landroid/widget/TextView;
    if-ne v1, p1, :cond_17

    #@a
    .line 193
    invoke-virtual {v1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    #@d
    move-result-object v4

    #@e
    check-cast v4, Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;

    #@10
    .line 194
    .local v4, tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->switchTo(Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;)V

    #@13
    .line 198
    .end local v1           #b:Landroid/widget/TextView;
    .end local v4           #tag:Lcom/android/internal/policy/impl/RecentApplicationsDialog$RecentTag;
    :cond_13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->dismiss()V

    #@16
    .line 199
    return-void

    #@17
    .line 191
    .restart local v1       #b:Landroid/widget/TextView;
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_4
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/high16 v12, 0x2

    #@2
    const/4 v11, 0x2

    #@3
    const/4 v10, 0x1

    #@4
    const/4 v9, 0x0

    #@5
    const/4 v8, -0x1

    #@6
    .line 87
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    #@9
    .line 89
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@c
    move-result-object v2

    #@d
    .line 91
    .local v2, context:Landroid/content/Context;
    sget-object v7, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@f
    if-nez v7, :cond_1b

    #@11
    .line 92
    const-string v7, "statusbar"

    #@13
    invoke-virtual {v2, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16
    move-result-object v7

    #@17
    check-cast v7, Landroid/app/StatusBarManager;

    #@19
    sput-object v7, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@1b
    .line 95
    :cond_1b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getWindow()Landroid/view/Window;

    #@1e
    move-result-object v6

    #@1f
    .line 96
    .local v6, window:Landroid/view/Window;
    invoke-virtual {v6, v10}, Landroid/view/Window;->requestFeature(I)Z

    #@22
    .line 97
    const/16 v7, 0x7ec

    #@24
    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    #@27
    .line 98
    invoke-virtual {v6, v12, v12}, Landroid/view/Window;->setFlags(II)V

    #@2a
    .line 100
    const-string v7, "Recents"

    #@2c
    invoke-virtual {v6, v7}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@2f
    .line 102
    const v7, 0x10900b2

    #@32
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->setContentView(I)V

    #@35
    .line 104
    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@38
    move-result-object v5

    #@39
    .line 105
    .local v5, params:Landroid/view/WindowManager$LayoutParams;
    iput v8, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3b
    .line 106
    iput v8, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3d
    .line 107
    invoke-virtual {v6, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@40
    .line 108
    invoke-virtual {v6, v9, v11}, Landroid/view/Window;->setFlags(II)V

    #@43
    .line 110
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@45
    const v7, 0x102035e

    #@48
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@4b
    move-result-object v7

    #@4c
    check-cast v7, Landroid/widget/TextView;

    #@4e
    aput-object v7, v8, v9

    #@50
    .line 111
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@52
    const v7, 0x1020019

    #@55
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v7

    #@59
    check-cast v7, Landroid/widget/TextView;

    #@5b
    aput-object v7, v8, v10

    #@5d
    .line 112
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@5f
    const v7, 0x102001a

    #@62
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@65
    move-result-object v7

    #@66
    check-cast v7, Landroid/widget/TextView;

    #@68
    aput-object v7, v8, v11

    #@6a
    .line 113
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@6c
    const/4 v9, 0x3

    #@6d
    const v7, 0x102001b

    #@70
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@73
    move-result-object v7

    #@74
    check-cast v7, Landroid/widget/TextView;

    #@76
    aput-object v7, v8, v9

    #@78
    .line 114
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@7a
    const/4 v9, 0x4

    #@7b
    const v7, 0x102035f

    #@7e
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@81
    move-result-object v7

    #@82
    check-cast v7, Landroid/widget/TextView;

    #@84
    aput-object v7, v8, v9

    #@86
    .line 115
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@88
    const/4 v9, 0x5

    #@89
    const v7, 0x1020360

    #@8c
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@8f
    move-result-object v7

    #@90
    check-cast v7, Landroid/widget/TextView;

    #@92
    aput-object v7, v8, v9

    #@94
    .line 116
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@96
    const/4 v9, 0x6

    #@97
    const v7, 0x1020361

    #@9a
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@9d
    move-result-object v7

    #@9e
    check-cast v7, Landroid/widget/TextView;

    #@a0
    aput-object v7, v8, v9

    #@a2
    .line 117
    iget-object v8, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@a4
    const/4 v9, 0x7

    #@a5
    const v7, 0x1020362

    #@a8
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@ab
    move-result-object v7

    #@ac
    check-cast v7, Landroid/widget/TextView;

    #@ae
    aput-object v7, v8, v9

    #@b0
    .line 118
    const v7, 0x102035d

    #@b3
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->findViewById(I)Landroid/view/View;

    #@b6
    move-result-object v7

    #@b7
    iput-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mNoAppsText:Landroid/view/View;

    #@b9
    .line 120
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@bb
    .local v0, arr$:[Landroid/widget/TextView;
    array-length v4, v0

    #@bc
    .local v4, len$:I
    const/4 v3, 0x0

    #@bd
    .local v3, i$:I
    :goto_bd
    if-ge v3, v4, :cond_c7

    #@bf
    aget-object v1, v0, v3

    #@c1
    .line 121
    .local v1, b:Landroid/widget/TextView;
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@c4
    .line 120
    add-int/lit8 v3, v3, 0x1

    #@c6
    goto :goto_bd

    #@c7
    .line 123
    .end local v1           #b:Landroid/widget/TextView;
    :cond_c7
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 127
    const/16 v7, 0x3d

    #@3
    if-ne p1, v7, :cond_5c

    #@5
    .line 131
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@8
    move-result v0

    #@9
    .line 132
    .local v0, backward:Z
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@b
    array-length v5, v7

    #@c
    .line 133
    .local v5, numIcons:I
    const/4 v4, 0x0

    #@d
    .line 134
    .local v4, numButtons:I
    :goto_d
    if-ge v4, v5, :cond_1c

    #@f
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@11
    aget-object v7, v7, v4

    #@13
    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    #@16
    move-result v7

    #@17
    if-nez v7, :cond_1c

    #@19
    .line 135
    add-int/lit8 v4, v4, 0x1

    #@1b
    goto :goto_d

    #@1c
    .line 137
    :cond_1c
    if-eqz v4, :cond_4f

    #@1e
    .line 138
    if-eqz v0, :cond_50

    #@20
    add-int/lit8 v3, v4, -0x1

    #@22
    .line 139
    .local v3, nextFocus:I
    :goto_22
    const/4 v2, 0x0

    #@23
    .local v2, i:I
    :goto_23
    if-ge v2, v4, :cond_37

    #@25
    .line 140
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@27
    aget-object v7, v7, v2

    #@29
    invoke-virtual {v7}, Landroid/widget/TextView;->hasFocus()Z

    #@2c
    move-result v7

    #@2d
    if-eqz v7, :cond_57

    #@2f
    .line 141
    if-eqz v0, :cond_52

    #@31
    .line 142
    add-int v7, v2, v4

    #@33
    add-int/lit8 v7, v7, -0x1

    #@35
    rem-int v3, v7, v4

    #@37
    .line 149
    :cond_37
    :goto_37
    if-eqz v0, :cond_5a

    #@39
    move v1, v6

    #@3a
    .line 150
    .local v1, direction:I
    :goto_3a
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@3c
    aget-object v7, v7, v3

    #@3e
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->requestFocus(I)Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_4f

    #@44
    .line 151
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mIcons:[Landroid/widget/TextView;

    #@46
    aget-object v7, v7, v3

    #@48
    invoke-static {v1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    #@4b
    move-result v8

    #@4c
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->playSoundEffect(I)V

    #@4f
    .line 161
    .end local v0           #backward:Z
    .end local v1           #direction:I
    .end local v2           #i:I
    .end local v3           #nextFocus:I
    .end local v4           #numButtons:I
    .end local v5           #numIcons:I
    :cond_4f
    :goto_4f
    return v6

    #@50
    .line 138
    .restart local v0       #backward:Z
    .restart local v4       #numButtons:I
    .restart local v5       #numIcons:I
    :cond_50
    const/4 v3, 0x0

    #@51
    goto :goto_22

    #@52
    .line 144
    .restart local v2       #i:I
    .restart local v3       #nextFocus:I
    :cond_52
    add-int/lit8 v7, v2, 0x1

    #@54
    rem-int v3, v7, v4

    #@56
    .line 146
    goto :goto_37

    #@57
    .line 139
    :cond_57
    add-int/lit8 v2, v2, 0x1

    #@59
    goto :goto_23

    #@5a
    .line 149
    :cond_5a
    const/4 v1, 0x2

    #@5b
    goto :goto_3a

    #@5c
    .line 161
    .end local v0           #backward:Z
    .end local v2           #i:I
    .end local v3           #nextFocus:I
    .end local v4           #numButtons:I
    .end local v5           #numIcons:I
    :cond_5c
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@5f
    move-result v6

    #@60
    goto :goto_4f
.end method

.method public onStart()V
    .registers 4

    #@0
    .prologue
    .line 223
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    #@3
    .line 224
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->reloadButtons()V

    #@6
    .line 225
    sget-object v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 226
    sget-object v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@c
    const/high16 v1, 0x1

    #@e
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    #@11
    .line 230
    :cond_11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@14
    move-result-object v0

    #@15
    iget-object v1, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@17
    iget-object v2, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mBroadcastIntentFilter:Landroid/content/IntentFilter;

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1c
    .line 232
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mHandler:Landroid/os/Handler;

    #@1e
    iget-object v1, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mCleanup:Ljava/lang/Runnable;

    #@20
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@23
    .line 233
    return-void
.end method

.method public onStop()V
    .registers 5

    #@0
    .prologue
    .line 240
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    #@3
    .line 242
    sget-object v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 243
    sget-object v0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->sStatusBar:Landroid/app/StatusBarManager;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    #@d
    .line 247
    :cond_d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->getContext()Landroid/content/Context;

    #@10
    move-result-object v0

    #@11
    iget-object v1, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@16
    .line 249
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mHandler:Landroid/os/Handler;

    #@18
    iget-object v1, p0, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->mCleanup:Ljava/lang/Runnable;

    #@1a
    const-wide/16 v2, 0x64

    #@1c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1f
    .line 250
    return-void
.end method
