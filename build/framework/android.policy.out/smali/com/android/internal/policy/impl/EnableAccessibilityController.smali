.class public Lcom/android/internal/policy/impl/EnableAccessibilityController;
.super Ljava/lang/Object;
.source "EnableAccessibilityController.java"


# static fields
.field private static final ENABLE_ACCESSIBILITY_DELAY_MILLIS:I = 0x1770

.field public static final MESSAGE_ENABLE_ACCESSIBILITY:I = 0x3

.field public static final MESSAGE_SPEAK_ENABLE_CANCELED:I = 0x2

.field public static final MESSAGE_SPEAK_WARNING:I = 0x1

.field private static final SPEAK_WARNING_DELAY_MILLIS:I = 0x7d0


# instance fields
.field private final mAccessibilityManager:Landroid/view/accessibility/IAccessibilityManager;

.field private mCanceled:Z

.field private final mContext:Landroid/content/Context;

.field private mDestroyed:Z

.field private mFirstPointerDownX:F

.field private mFirstPointerDownY:F

.field private final mHandler:Landroid/os/Handler;

.field private mSecondPointerDownX:F

.field private mSecondPointerDownY:F

.field private final mTone:Landroid/media/Ringtone;

.field private final mTouchSlop:F

.field private final mTts:Landroid/speech/tts/TextToSpeech;

.field private final mUserManager:Landroid/os/UserManager;

.field private final mWindowManager:Landroid/view/IWindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v0, Lcom/android/internal/policy/impl/EnableAccessibilityController$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController$1;-><init>(Lcom/android/internal/policy/impl/EnableAccessibilityController;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@a
    .line 78
    const-string v0, "window"

    #@c
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v0

    #@10
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mWindowManager:Landroid/view/IWindowManager;

    #@16
    .line 81
    const-string v0, "accessibility"

    #@18
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mAccessibilityManager:Landroid/view/accessibility/IAccessibilityManager;

    #@22
    .line 101
    iput-object p1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mContext:Landroid/content/Context;

    #@24
    .line 102
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mContext:Landroid/content/Context;

    #@26
    const-string v1, "user"

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/os/UserManager;

    #@2e
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mUserManager:Landroid/os/UserManager;

    #@30
    .line 103
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    #@32
    new-instance v1, Lcom/android/internal/policy/impl/EnableAccessibilityController$2;

    #@34
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController$2;-><init>(Lcom/android/internal/policy/impl/EnableAccessibilityController;)V

    #@37
    invoke-direct {v0, p1, v1}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    #@3a
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTts:Landroid/speech/tts/TextToSpeech;

    #@3c
    .line 111
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@3e
    invoke-static {p1, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTone:Landroid/media/Ringtone;

    #@44
    .line 112
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTone:Landroid/media/Ringtone;

    #@46
    const/4 v1, 0x3

    #@47
    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->setStreamType(I)V

    #@4a
    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4d
    move-result-object v0

    #@4e
    const v1, 0x105006d

    #@51
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@54
    move-result v0

    #@55
    int-to-float v0, v0

    #@56
    iput v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTouchSlop:F

    #@58
    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/EnableAccessibilityController;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/EnableAccessibilityController;)Landroid/speech/tts/TextToSpeech;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTts:Landroid/speech/tts/TextToSpeech;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/EnableAccessibilityController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->enableAccessibility()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/EnableAccessibilityController;)Landroid/media/Ringtone;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTone:Landroid/media/Ringtone;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/EnableAccessibilityController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mDestroyed:Z

    #@2
    return v0
.end method

.method public static canEnableAccessibilityViaGesture(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 118
    invoke-static {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    .line 121
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_17

    #@c
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    #@f
    move-result-object v3

    #@10
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_17

    #@16
    .line 128
    :goto_16
    return v2

    #@17
    :cond_17
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "enable_accessibility_global_gesture_enabled"

    #@1d
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@20
    move-result v3

    #@21
    if-ne v3, v1, :cond_2f

    #@23
    invoke-static {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->getInstalledSpeakingAccessibilityServices(Landroid/content/Context;)Ljava/util/List;

    #@26
    move-result-object v3

    #@27
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_2f

    #@2d
    :goto_2d
    move v2, v1

    #@2e
    goto :goto_16

    #@2f
    :cond_2f
    move v1, v2

    #@30
    goto :goto_2d
.end method

.method private cancel()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    const/4 v1, 0x1

    #@2
    .line 204
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mCanceled:Z

    #@4
    .line 205
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 206
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@11
    .line 210
    :cond_11
    :goto_11
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@16
    .line 211
    return-void

    #@17
    .line 207
    :cond_17
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_11

    #@1f
    .line 208
    iget-object v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@21
    const/4 v1, 0x2

    #@22
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@25
    goto :goto_11
.end method

.method private enableAccessibility()V
    .registers 16

    #@0
    .prologue
    .line 214
    iget-object v13, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v13}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->getInstalledSpeakingAccessibilityServices(Landroid/content/Context;)Ljava/util/List;

    #@5
    move-result-object v11

    #@6
    .line 216
    .local v11, services:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    #@9
    move-result v13

    #@a
    if-eqz v13, :cond_d

    #@c
    .line 278
    :cond_c
    :goto_c
    return-void

    #@d
    .line 219
    :cond_d
    const/4 v6, 0x0

    #@e
    .line 221
    .local v6, keyguardLocked:Z
    :try_start_e
    iget-object v13, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mWindowManager:Landroid/view/IWindowManager;

    #@10
    invoke-interface {v13}, Landroid/view/IWindowManager;->isKeyguardLocked()Z
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_9a

    #@13
    move-result v6

    #@14
    .line 226
    :goto_14
    iget-object v13, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mUserManager:Landroid/os/UserManager;

    #@16
    invoke-virtual {v13}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@19
    move-result-object v13

    #@1a
    invoke-interface {v13}, Ljava/util/List;->size()I

    #@1d
    move-result v13

    #@1e
    const/4 v14, 0x1

    #@1f
    if-le v13, v14, :cond_87

    #@21
    const/4 v4, 0x1

    #@22
    .line 228
    .local v4, hasMoreThanOneUser:Z
    :goto_22
    const/4 v13, 0x0

    #@23
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v8

    #@27
    check-cast v8, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@29
    .line 229
    .local v8, service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget v13, v8, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@2b
    and-int/lit8 v13, v13, 0x4

    #@2d
    if-eqz v13, :cond_89

    #@2f
    const/4 v2, 0x1

    #@30
    .line 232
    .local v2, enableTouchExploration:Z
    :goto_30
    if-nez v2, :cond_47

    #@32
    .line 233
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@35
    move-result v9

    #@36
    .line 234
    .local v9, serviceCount:I
    const/4 v5, 0x1

    #@37
    .local v5, i:I
    :goto_37
    if-ge v5, v9, :cond_47

    #@39
    .line 235
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@3f
    .line 236
    .local v0, candidate:Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget v13, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@41
    and-int/lit8 v13, v13, 0x4

    #@43
    if-eqz v13, :cond_8b

    #@45
    .line 238
    const/4 v2, 0x1

    #@46
    .line 239
    move-object v8, v0

    #@47
    .line 245
    .end local v0           #candidate:Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v5           #i:I
    .end local v9           #serviceCount:I
    :cond_47
    invoke-virtual {v8}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    #@4a
    move-result-object v13

    #@4b
    iget-object v10, v13, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4d
    .line 246
    .local v10, serviceInfo:Landroid/content/pm/ServiceInfo;
    new-instance v1, Landroid/content/ComponentName;

    #@4f
    iget-object v13, v10, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    #@51
    iget-object v14, v10, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    #@53
    invoke-direct {v1, v13, v14}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 247
    .local v1, componentName:Landroid/content/ComponentName;
    if-eqz v6, :cond_5a

    #@58
    if-nez v4, :cond_8e

    #@5a
    .line 248
    :cond_5a
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@5d
    move-result v12

    #@5e
    .line 249
    .local v12, userId:I
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    .line 250
    .local v3, enabledServiceString:Ljava/lang/String;
    iget-object v13, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mContext:Landroid/content/Context;

    #@64
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@67
    move-result-object v7

    #@68
    .line 252
    .local v7, resolver:Landroid/content/ContentResolver;
    const-string v13, "enabled_accessibility_services"

    #@6a
    invoke-static {v7, v13, v3, v12}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@6d
    .line 256
    const-string v13, "touch_exploration_granted_accessibility_services"

    #@6f
    invoke-static {v7, v13, v3, v12}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@72
    .line 260
    if-eqz v2, :cond_7a

    #@74
    .line 261
    const-string v13, "touch_exploration_enabled"

    #@76
    const/4 v14, 0x1

    #@77
    invoke-static {v7, v13, v14, v12}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@7a
    .line 265
    :cond_7a
    const-string v13, "accessibility_script_injection"

    #@7c
    const/4 v14, 0x1

    #@7d
    invoke-static {v7, v13, v14, v12}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@80
    .line 268
    const-string v13, "accessibility_enabled"

    #@82
    const/4 v14, 0x1

    #@83
    invoke-static {v7, v13, v14, v12}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@86
    goto :goto_c

    #@87
    .line 226
    .end local v1           #componentName:Landroid/content/ComponentName;
    .end local v2           #enableTouchExploration:Z
    .end local v3           #enabledServiceString:Ljava/lang/String;
    .end local v4           #hasMoreThanOneUser:Z
    .end local v7           #resolver:Landroid/content/ContentResolver;
    .end local v8           #service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v10           #serviceInfo:Landroid/content/pm/ServiceInfo;
    .end local v12           #userId:I
    :cond_87
    const/4 v4, 0x0

    #@88
    goto :goto_22

    #@89
    .line 229
    .restart local v4       #hasMoreThanOneUser:Z
    .restart local v8       #service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_89
    const/4 v2, 0x0

    #@8a
    goto :goto_30

    #@8b
    .line 234
    .restart local v0       #candidate:Landroid/accessibilityservice/AccessibilityServiceInfo;
    .restart local v2       #enableTouchExploration:Z
    .restart local v5       #i:I
    .restart local v9       #serviceCount:I
    :cond_8b
    add-int/lit8 v5, v5, 0x1

    #@8d
    goto :goto_37

    #@8e
    .line 270
    .end local v0           #candidate:Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v5           #i:I
    .end local v9           #serviceCount:I
    .restart local v1       #componentName:Landroid/content/ComponentName;
    .restart local v10       #serviceInfo:Landroid/content/pm/ServiceInfo;
    :cond_8e
    if-eqz v6, :cond_c

    #@90
    .line 272
    :try_start_90
    iget-object v13, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mAccessibilityManager:Landroid/view/accessibility/IAccessibilityManager;

    #@92
    invoke-interface {v13, v1, v2}, Landroid/view/accessibility/IAccessibilityManager;->temporaryEnableAccessibilityStateUntilKeyguardRemoved(Landroid/content/ComponentName;Z)V
    :try_end_95
    .catch Landroid/os/RemoteException; {:try_start_90 .. :try_end_95} :catch_97

    #@95
    goto/16 :goto_c

    #@97
    .line 274
    :catch_97
    move-exception v13

    #@98
    goto/16 :goto_c

    #@9a
    .line 222
    .end local v1           #componentName:Landroid/content/ComponentName;
    .end local v2           #enableTouchExploration:Z
    .end local v4           #hasMoreThanOneUser:Z
    .end local v8           #service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v10           #serviceInfo:Landroid/content/pm/ServiceInfo;
    :catch_9a
    move-exception v13

    #@9b
    goto/16 :goto_14
.end method

.method private static getInstalledSpeakingAccessibilityServices(Landroid/content/Context;)Ljava/util/List;
    .registers 5
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 135
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 136
    .local v2, services:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-static {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    #@c
    move-result-object v3

    #@d
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@10
    .line 138
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v0

    #@14
    .line 139
    .local v0, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    :cond_14
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_2a

    #@1a
    .line 140
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@20
    .line 141
    .local v1, service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget v3, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@22
    and-int/lit8 v3, v3, 0x1

    #@24
    if-nez v3, :cond_14

    #@26
    .line 142
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@29
    goto :goto_14

    #@2a
    .line 145
    .end local v1           #service:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_2a
    return-object v2
.end method


# virtual methods
.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 149
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mDestroyed:Z

    #@3
    .line 150
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 153
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@5
    move-result v2

    #@6
    const/4 v3, 0x5

    #@7
    if-ne v2, v3, :cond_38

    #@9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@c
    move-result v2

    #@d
    const/4 v3, 0x2

    #@e
    if-ne v2, v3, :cond_38

    #@10
    .line 155
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@13
    move-result v2

    #@14
    iput v2, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mFirstPointerDownX:F

    #@16
    .line 156
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mFirstPointerDownY:F

    #@1c
    .line 157
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@1f
    move-result v1

    #@20
    iput v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mSecondPointerDownX:F

    #@22
    .line 158
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@25
    move-result v1

    #@26
    iput v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mSecondPointerDownY:F

    #@28
    .line 159
    iget-object v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@2a
    const-wide/16 v2, 0x7d0

    #@2c
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2f
    .line 161
    iget-object v1, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mHandler:Landroid/os/Handler;

    #@31
    const/4 v2, 0x3

    #@32
    const-wide/16 v3, 0x1770

    #@34
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@37
    .line 165
    :goto_37
    return v0

    #@38
    :cond_38
    move v0, v1

    #@39
    goto :goto_37
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@5
    move-result v2

    #@6
    .line 170
    .local v2, pointerCount:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@9
    move-result v0

    #@a
    .line 171
    .local v0, action:I
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mCanceled:Z

    #@c
    if-eqz v4, :cond_13

    #@e
    .line 172
    if-ne v0, v8, :cond_12

    #@10
    .line 173
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mCanceled:Z

    #@12
    .line 200
    :cond_12
    :goto_12
    return v8

    #@13
    .line 177
    :cond_13
    packed-switch v0, :pswitch_data_5e

    #@16
    :pswitch_16
    goto :goto_12

    #@17
    .line 184
    :pswitch_17
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    #@1a
    move-result v4

    #@1b
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    #@1e
    move-result v5

    #@1f
    iget v6, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mFirstPointerDownX:F

    #@21
    iget v7, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mFirstPointerDownY:F

    #@23
    invoke-static {v4, v5, v6, v7}, Landroid/util/MathUtils;->dist(FFFF)F

    #@26
    move-result v1

    #@27
    .line 186
    .local v1, firstPointerMove:F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    #@2a
    move-result v4

    #@2b
    iget v5, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTouchSlop:F

    #@2d
    cmpl-float v4, v4, v5

    #@2f
    if-lez v4, :cond_34

    #@31
    .line 187
    invoke-direct {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->cancel()V

    #@34
    .line 189
    :cond_34
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getX(I)F

    #@37
    move-result v4

    #@38
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    #@3b
    move-result v5

    #@3c
    iget v6, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mSecondPointerDownX:F

    #@3e
    iget v7, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mSecondPointerDownY:F

    #@40
    invoke-static {v4, v5, v6, v7}, Landroid/util/MathUtils;->dist(FFFF)F

    #@43
    move-result v3

    #@44
    .line 191
    .local v3, secondPointerMove:F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    #@47
    move-result v4

    #@48
    iget v5, p0, Lcom/android/internal/policy/impl/EnableAccessibilityController;->mTouchSlop:F

    #@4a
    cmpl-float v4, v4, v5

    #@4c
    if-lez v4, :cond_12

    #@4e
    .line 192
    invoke-direct {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->cancel()V

    #@51
    goto :goto_12

    #@52
    .line 179
    .end local v1           #firstPointerMove:F
    .end local v3           #secondPointerMove:F
    :pswitch_52
    const/4 v4, 0x2

    #@53
    if-le v2, v4, :cond_12

    #@55
    .line 180
    invoke-direct {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->cancel()V

    #@58
    goto :goto_12

    #@59
    .line 197
    :pswitch_59
    invoke-direct {p0}, Lcom/android/internal/policy/impl/EnableAccessibilityController;->cancel()V

    #@5c
    goto :goto_12

    #@5d
    .line 177
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x2
        :pswitch_17
        :pswitch_59
        :pswitch_16
        :pswitch_52
        :pswitch_59
    .end packed-switch
.end method
