.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

.field final synthetic val$lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 990
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->val$lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 993
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    monitor-enter v1

    #@3
    .line 994
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->val$lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->cleanUp()V

    #@8
    .line 995
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@d
    move-result-object v0

    #@e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->val$lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@10
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    #@13
    .line 996
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@15
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@18
    move-result-object v0

    #@19
    if-eqz v0, :cond_26

    #@1b
    .line 997
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@1d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@20
    move-result-object v0

    #@21
    const/16 v2, 0x8

    #@23
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@26
    .line 999
    :cond_26
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@28
    const/16 v2, 0x1f4

    #@2a
    iput v2, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mDelayTimeRemoveView:I

    #@2c
    .line 1000
    monitor-exit v1

    #@2d
    .line 1001
    return-void

    #@2e
    .line 1000
    :catchall_2e
    move-exception v0

    #@2f
    monitor-exit v1
    :try_end_30
    .catchall {:try_start_3 .. :try_end_30} :catchall_2e

    #@30
    throw v0
.end method
