.class Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;
.super Ljava/lang/Object;
.source "NextiKeyguardSelectorView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->setSPModeMailUnReadCount(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

.field final synthetic val$count:I


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 694
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x33

    #@2
    const/4 v1, 0x0

    #@3
    .line 697
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@5
    if-lez v0, :cond_2c

    #@7
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@9
    if-ge v0, v2, :cond_2c

    #@b
    .line 701
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$700(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1c

    #@13
    .line 702
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@15
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@1c
    .line 705
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@1e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;

    #@21
    move-result-object v0

    #@22
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@24
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2b
    .line 721
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 707
    :cond_2c
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@2e
    if-gt v2, v0, :cond_4d

    #@30
    .line 711
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@32
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$700(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Z

    #@35
    move-result v0

    #@36
    if-nez v0, :cond_41

    #@38
    .line 712
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@3a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@41
    .line 715
    :cond_41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@43
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;

    #@46
    move-result-object v0

    #@47
    const-string v1, "50+"

    #@49
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4c
    goto :goto_2b

    #@4d
    .line 717
    :cond_4d
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->val$count:I

    #@4f
    if-gtz v0, :cond_2b

    #@51
    .line 719
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@53
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;

    #@56
    move-result-object v0

    #@57
    const/4 v1, 0x4

    #@58
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@5b
    goto :goto_2b
.end method
