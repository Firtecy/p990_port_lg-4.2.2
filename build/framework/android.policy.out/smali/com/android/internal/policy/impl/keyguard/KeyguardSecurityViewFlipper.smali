.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;
.super Landroid/widget/ViewFlipper;
.source "KeyguardSecurityViewFlipper.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "KeyguardSecurityViewFlipper"


# instance fields
.field private mTempRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 47
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attr"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 44
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@a
    .line 52
    return-void
.end method

.method private makeChildMeasureSpec(II)I
    .registers 6
    .parameter "maxSize"
    .parameter "childDimen"

    #@0
    .prologue
    .line 232
    packed-switch p2, :pswitch_data_16

    #@3
    .line 242
    const/high16 v0, 0x4000

    #@5
    .line 243
    .local v0, mode:I
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    #@8
    move-result v1

    #@9
    .line 246
    .local v1, size:I
    :goto_9
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c
    move-result v2

    #@d
    return v2

    #@e
    .line 234
    .end local v0           #mode:I
    .end local v1           #size:I
    :pswitch_e
    const/high16 v0, -0x8000

    #@10
    .line 235
    .restart local v0       #mode:I
    move v1, p1

    #@11
    .line 236
    .restart local v1       #size:I
    goto :goto_9

    #@12
    .line 238
    .end local v0           #mode:I
    .end local v1           #size:I
    :pswitch_12
    const/high16 v0, 0x4000

    #@14
    .line 239
    .restart local v0       #mode:I
    move v1, p1

    #@15
    .line 240
    .restart local v1       #size:I
    goto :goto_9

    #@16
    .line 232
    :pswitch_data_16
    .packed-switch -0x2
        :pswitch_e
        :pswitch_12
    .end packed-switch
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 164
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@2
    return v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 169
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@2
    if-eqz v0, :cond_c

    #@4
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@6
    check-cast p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@8
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;)V

    #@b
    :goto_b
    return-object v0

    #@c
    .restart local p1
    :cond_c
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@e
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    goto :goto_b
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 174
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 3

    #@0
    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 127
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_b

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@9
    move-result-object v1

    #@a
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    .registers 3

    #@0
    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getDisplayedChild()I

    #@3
    move-result v1

    #@4
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    .line 72
    .local v0, child:Landroid/view/View;
    instance-of v1, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@a
    if-eqz v1, :cond_f

    #@c
    .line 73
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@e
    .line 75
    .end local v0           #child:Landroid/view/View;
    :goto_e
    return-object v0

    #@f
    .restart local v0       #child:Landroid/view/View;
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public hideBouncer(I)V
    .registers 7
    .parameter "duration"

    #@0
    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 153
    .local v0, active:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@8
    move-result v4

    #@9
    if-ge v2, v4, :cond_21

    #@b
    .line 154
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v1

    #@f
    .line 155
    .local v1, child:Landroid/view/View;
    instance-of v4, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@11
    if-eqz v4, :cond_1c

    #@13
    move-object v3, v1

    #@14
    .line 156
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@16
    .line 157
    .local v3, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-ne v3, v0, :cond_1f

    #@18
    move v4, p1

    #@19
    :goto_19
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->hideBouncer(I)V

    #@1c
    .line 153
    .end local v3           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_5

    #@1f
    .line 157
    .restart local v3       #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_1f
    const/4 v4, 0x0

    #@20
    goto :goto_19

    #@21
    .line 160
    .end local v1           #child:Landroid/view/View;
    .end local v3           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_21
    return-void
.end method

.method public needsInput()Z
    .registers 3

    #@0
    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 121
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_b

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->needsInput()Z

    #@9
    move-result v1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method protected onMeasure(II)V
    .registers 24
    .parameter "widthSpec"
    .parameter "heightSpec"

    #@0
    .prologue
    .line 179
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v17

    #@4
    .line 180
    .local v17, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v9

    #@8
    .line 190
    .local v9, heightMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@b
    move-result v18

    #@c
    .line 191
    .local v18, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@f
    move-result v10

    #@10
    .line 192
    .local v10, heightSize:I
    move/from16 v14, v18

    #@12
    .line 193
    .local v14, maxWidth:I
    move v13, v10

    #@13
    .line 194
    .local v13, maxHeight:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@16
    move-result v6

    #@17
    .line 195
    .local v6, count:I
    const/4 v11, 0x0

    #@18
    .local v11, i:I
    :goto_18
    if-ge v11, v6, :cond_49

    #@1a
    .line 196
    move-object/from16 v0, p0

    #@1c
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@1f
    move-result-object v3

    #@20
    .line 197
    .local v3, child:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@23
    move-result-object v12

    #@24
    check-cast v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@26
    .line 199
    .local v12, lp:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    iget v0, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxWidth:I

    #@28
    move/from16 v19, v0

    #@2a
    if-lez v19, :cond_36

    #@2c
    iget v0, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxWidth:I

    #@2e
    move/from16 v19, v0

    #@30
    move/from16 v0, v19

    #@32
    if-ge v0, v14, :cond_36

    #@34
    .line 200
    iget v14, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxWidth:I

    #@36
    .line 202
    :cond_36
    iget v0, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxHeight:I

    #@38
    move/from16 v19, v0

    #@3a
    if-lez v19, :cond_46

    #@3c
    iget v0, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxHeight:I

    #@3e
    move/from16 v19, v0

    #@40
    move/from16 v0, v19

    #@42
    if-ge v0, v13, :cond_46

    #@44
    .line 203
    iget v13, v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;->maxHeight:I

    #@46
    .line 195
    :cond_46
    add-int/lit8 v11, v11, 0x1

    #@48
    goto :goto_18

    #@49
    .line 207
    .end local v3           #child:Landroid/view/View;
    .end local v12           #lp:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    :cond_49
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getPaddingLeft()I

    #@4c
    move-result v19

    #@4d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getPaddingRight()I

    #@50
    move-result v20

    #@51
    add-int v15, v19, v20

    #@53
    .line 208
    .local v15, wPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getPaddingTop()I

    #@56
    move-result v19

    #@57
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getPaddingBottom()I

    #@5a
    move-result v20

    #@5b
    add-int v7, v19, v20

    #@5d
    .line 209
    .local v7, hPadding:I
    sub-int/2addr v14, v15

    #@5e
    .line 210
    sub-int/2addr v13, v7

    #@5f
    .line 212
    const/high16 v19, 0x4000

    #@61
    move/from16 v0, v17

    #@63
    move/from16 v1, v19

    #@65
    if-ne v0, v1, :cond_bf

    #@67
    move/from16 v16, v18

    #@69
    .line 213
    .local v16, width:I
    :goto_69
    const/high16 v19, 0x4000

    #@6b
    move/from16 v0, v19

    #@6d
    if-ne v9, v0, :cond_c2

    #@6f
    move v8, v10

    #@70
    .line 214
    .local v8, height:I
    :goto_70
    const/4 v11, 0x0

    #@71
    :goto_71
    if-ge v11, v6, :cond_c4

    #@73
    .line 215
    move-object/from16 v0, p0

    #@75
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@78
    move-result-object v3

    #@79
    .line 216
    .restart local v3       #child:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@7c
    move-result-object v12

    #@7d
    check-cast v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;

    #@7f
    .line 218
    .restart local v12       #lp:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    iget v0, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@81
    move/from16 v19, v0

    #@83
    move-object/from16 v0, p0

    #@85
    move/from16 v1, v19

    #@87
    invoke-direct {v0, v14, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->makeChildMeasureSpec(II)I

    #@8a
    move-result v5

    #@8b
    .line 219
    .local v5, childWidthSpec:I
    iget v0, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@8d
    move/from16 v19, v0

    #@8f
    move-object/from16 v0, p0

    #@91
    move/from16 v1, v19

    #@93
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->makeChildMeasureSpec(II)I

    #@96
    move-result v4

    #@97
    .line 221
    .local v4, childHeightSpec:I
    invoke-virtual {v3, v5, v4}, Landroid/view/View;->measure(II)V

    #@9a
    .line 223
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@9d
    move-result v19

    #@9e
    sub-int v20, v18, v15

    #@a0
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    #@a3
    move-result v19

    #@a4
    move/from16 v0, v16

    #@a6
    move/from16 v1, v19

    #@a8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@ab
    move-result v16

    #@ac
    .line 224
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@af
    move-result v19

    #@b0
    sub-int v20, v10, v7

    #@b2
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    #@b5
    move-result v19

    #@b6
    move/from16 v0, v19

    #@b8
    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    #@bb
    move-result v8

    #@bc
    .line 214
    add-int/lit8 v11, v11, 0x1

    #@be
    goto :goto_71

    #@bf
    .line 212
    .end local v3           #child:Landroid/view/View;
    .end local v4           #childHeightSpec:I
    .end local v5           #childWidthSpec:I
    .end local v8           #height:I
    .end local v12           #lp:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper$LayoutParams;
    .end local v16           #width:I
    :cond_bf
    const/16 v16, 0x0

    #@c1
    goto :goto_69

    #@c2
    .line 213
    .restart local v16       #width:I
    :cond_c2
    const/4 v8, 0x0

    #@c3
    goto :goto_70

    #@c4
    .line 226
    .restart local v8       #height:I
    :cond_c4
    add-int v19, v16, v15

    #@c6
    add-int v20, v8, v7

    #@c8
    move-object/from16 v0, p0

    #@ca
    move/from16 v1, v19

    #@cc
    move/from16 v2, v20

    #@ce
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->setMeasuredDimension(II)V

    #@d1
    .line 227
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 105
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 106
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onPause()V

    #@9
    .line 108
    :cond_9
    return-void
.end method

.method public onResume(I)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 113
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 114
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onResume(I)V

    #@9
    .line 116
    :cond_9
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "ev"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 56
    invoke-super {p0, p1}, Landroid/widget/ViewFlipper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@4
    move-result v2

    #@5
    .line 57
    .local v2, result:Z
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 58
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@e
    move-result v4

    #@f
    if-ge v1, v4, :cond_4a

    #@11
    .line 59
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    .line 60
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_45

    #@1b
    .line 61
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@1d
    invoke-virtual {p0, v0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@20
    .line 62
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@22
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@24
    int-to-float v4, v4

    #@25
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@27
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@29
    int-to-float v5, v5

    #@2a
    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@2d
    .line 63
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_35

    #@33
    if-eqz v2, :cond_48

    #@35
    :cond_35
    const/4 v2, 0x1

    #@36
    .line 64
    :goto_36
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@38
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@3a
    neg-int v4, v4

    #@3b
    int-to-float v4, v4

    #@3c
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->mTempRect:Landroid/graphics/Rect;

    #@3e
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@40
    neg-int v5, v5

    #@41
    int-to-float v5, v5

    #@42
    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@45
    .line 58
    :cond_45
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_b

    #@48
    :cond_48
    move v2, v3

    #@49
    .line 63
    goto :goto_36

    #@4a
    .line 67
    .end local v0           #child:Landroid/view/View;
    :cond_4a
    return v2
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 98
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->reset()V

    #@9
    .line 100
    :cond_9
    return-void
.end method

.method public setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 81
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 82
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V

    #@9
    .line 84
    :cond_9
    return-void
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 3
    .parameter "utils"

    #@0
    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 89
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 90
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@9
    .line 92
    :cond_9
    return-void
.end method

.method public showBouncer(I)V
    .registers 7
    .parameter "duration"

    #@0
    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 141
    .local v0, active:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@8
    move-result v4

    #@9
    if-ge v2, v4, :cond_21

    #@b
    .line 142
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v1

    #@f
    .line 143
    .local v1, child:Landroid/view/View;
    instance-of v4, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@11
    if-eqz v4, :cond_1c

    #@13
    move-object v3, v1

    #@14
    .line 144
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@16
    .line 145
    .local v3, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-ne v3, v0, :cond_1f

    #@18
    move v4, p1

    #@19
    :goto_19
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->showBouncer(I)V

    #@1c
    .line 141
    .end local v3           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_5

    #@1f
    .line 145
    .restart local v3       #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_1f
    const/4 v4, 0x0

    #@20
    goto :goto_19

    #@21
    .line 148
    .end local v1           #child:Landroid/view/View;
    .end local v3           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_21
    return-void
.end method

.method public showUsabilityHint()V
    .registers 2

    #@0
    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getSecurityView()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    move-result-object v0

    #@4
    .line 133
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    if-eqz v0, :cond_9

    #@6
    .line 134
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->showUsabilityHint()V

    #@9
    .line 136
    :cond_9
    return-void
.end method
