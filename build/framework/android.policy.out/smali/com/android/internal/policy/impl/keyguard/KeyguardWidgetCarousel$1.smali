.class Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;
.super Ljava/lang/Object;
.source "KeyguardWidgetCarousel.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mFactor:F

.field mInternal:Landroid/view/animation/Interpolator;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 207
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 208
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    #@7
    const/high16 v1, 0x3fc0

    #@9
    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;->mInternal:Landroid/view/animation/Interpolator;

    #@e
    .line 209
    const/high16 v0, 0x4020

    #@10
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;->mFactor:F

    #@12
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 5
    .parameter "input"

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;->mInternal:Landroid/view/animation/Interpolator;

    #@2
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;->mFactor:F

    #@4
    mul-float/2addr v1, p1

    #@5
    const/high16 v2, 0x3f80

    #@7
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    #@a
    move-result v1

    #@b
    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@e
    move-result v0

    #@f
    return v0
.end method
