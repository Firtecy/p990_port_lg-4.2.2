.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;
.super Landroid/telephony/PhoneStateListener;
.source "KeyguardSimPersoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 202
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    #@5
    .line 207
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "TEL"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_29

    #@11
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    const-string v1, "AU"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_29

    #@1d
    .line 209
    const-string v0, "KeyguardSimPersoView"

    #@1f
    const-string v1, "[PIN/PUK] onServiceStateChanged"

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 210
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->updateEmergencyText()V

    #@29
    .line 212
    :cond_29
    return-void
.end method
