.class Lcom/android/internal/policy/impl/RestartAction$1;
.super Landroid/os/Handler;
.source "RestartAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/RestartAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/RestartAction;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/RestartAction;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 175
    iput-object p1, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 177
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_56

    #@5
    .line 198
    const-string v0, "RestartAction"

    #@7
    const-string v1, "handleMessage() : error"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 201
    :cond_c
    :goto_c
    return-void

    #@d
    .line 179
    :pswitch_d
    invoke-static {}, Lcom/android/internal/policy/impl/RestartAction;->access$000()Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_1a

    #@13
    .line 180
    const-string v0, "RestartAction"

    #@15
    const-string v1, "handleMessage(): MESSAGE_DISMISS"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 182
    :cond_1a
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@1c
    invoke-static {v0}, Lcom/android/internal/policy/impl/RestartAction;->access$100(Lcom/android/internal/policy/impl/RestartAction;)Landroid/app/ProgressDialog;

    #@1f
    move-result-object v0

    #@20
    if-eqz v0, :cond_c

    #@22
    .line 183
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@24
    invoke-static {v0}, Lcom/android/internal/policy/impl/RestartAction;->access$200(Lcom/android/internal/policy/impl/RestartAction;)Landroid/os/Handler;

    #@27
    move-result-object v0

    #@28
    const/4 v1, 0x1

    #@29
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@2c
    .line 184
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@2e
    invoke-static {v0}, Lcom/android/internal/policy/impl/RestartAction;->access$100(Lcom/android/internal/policy/impl/RestartAction;)Landroid/app/ProgressDialog;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@35
    .line 185
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@37
    const/4 v1, 0x0

    #@38
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/RestartAction;->access$102(Lcom/android/internal/policy/impl/RestartAction;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    #@3b
    goto :goto_c

    #@3c
    .line 189
    :pswitch_3c
    invoke-static {}, Lcom/android/internal/policy/impl/RestartAction;->access$000()Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_49

    #@42
    .line 190
    const-string v0, "RestartAction"

    #@44
    const-string v1, "handleMessage(): MESSAGE_REFRESH"

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 192
    :cond_49
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@4b
    invoke-static {v0}, Lcom/android/internal/policy/impl/RestartAction;->access$300(Lcom/android/internal/policy/impl/RestartAction;)V

    #@4e
    goto :goto_c

    #@4f
    .line 195
    :pswitch_4f
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction$1;->this$0:Lcom/android/internal/policy/impl/RestartAction;

    #@51
    invoke-static {v0}, Lcom/android/internal/policy/impl/RestartAction;->access$400(Lcom/android/internal/policy/impl/RestartAction;)V

    #@54
    goto :goto_c

    #@55
    .line 177
    nop

    #@56
    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_d
        :pswitch_3c
        :pswitch_4f
    .end packed-switch
.end method
