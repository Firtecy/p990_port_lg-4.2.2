.class Lcom/android/internal/policy/impl/PhoneWindowManager$8;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1452
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 19

    #@0
    .prologue
    .line 1456
    const-string v1, "ro.lge.b2b.vmware"

    #@2
    const/4 v4, 0x0

    #@3
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_36

    #@9
    move-object/from16 v0, p0

    #@b
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@d
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1900(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_36

    #@13
    .line 1457
    new-instance v15, Landroid/content/Intent;

    #@15
    const-string v1, "lge.intent.action.toast"

    #@17
    invoke-direct {v15, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a
    .line 1458
    .local v15, intent:Landroid/content/Intent;
    const-string v1, "text"

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@20
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@22
    const v5, 0x209002d

    #@25
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v15, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 1459
    move-object/from16 v0, p0

    #@2e
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@30
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v1, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@35
    .line 1490
    .end local v15           #intent:Landroid/content/Intent;
    :cond_35
    :goto_35
    return-void

    #@36
    .line 1465
    :cond_36
    move-object/from16 v0, p0

    #@38
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3a
    iget-boolean v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@3c
    if-nez v1, :cond_52

    #@3e
    move-object/from16 v0, p0

    #@40
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@42
    iget-boolean v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@44
    if-eqz v1, :cond_52

    #@46
    .line 1466
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@49
    move-result-object v16

    #@4a
    .line 1467
    .local v16, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v16, :cond_94

    #@4c
    .line 1469
    :try_start_4c
    invoke-interface/range {v16 .. v16}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z
    :try_end_4f
    .catch Landroid/os/RemoteException; {:try_start_4c .. :try_end_4f} :catch_8b

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_35

    #@52
    .line 1483
    .end local v16           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_52
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@55
    move-result-wide v2

    #@56
    .line 1484
    .local v2, now:J
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@59
    move-result-object v17

    #@5a
    new-instance v1, Landroid/view/KeyEvent;

    #@5c
    const/4 v6, 0x0

    #@5d
    const/16 v7, 0xe1

    #@5f
    const/4 v8, 0x0

    #@60
    const/4 v9, 0x0

    #@61
    const/4 v10, -0x1

    #@62
    const/4 v11, 0x0

    #@63
    const/4 v12, 0x0

    #@64
    const/16 v13, 0x101

    #@66
    move-wide v4, v2

    #@67
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@6a
    const/4 v4, 0x0

    #@6b
    move-object/from16 v0, v17

    #@6d
    invoke-virtual {v0, v1, v4}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@70
    .line 1487
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@73
    move-result-object v17

    #@74
    new-instance v1, Landroid/view/KeyEvent;

    #@76
    const/4 v6, 0x1

    #@77
    const/16 v7, 0xe1

    #@79
    const/4 v8, 0x0

    #@7a
    const/4 v9, 0x0

    #@7b
    const/4 v10, -0x1

    #@7c
    const/4 v11, 0x0

    #@7d
    const/4 v12, 0x0

    #@7e
    const/16 v13, 0x101

    #@80
    move-wide v4, v2

    #@81
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@84
    const/4 v4, 0x0

    #@85
    move-object/from16 v0, v17

    #@87
    invoke-virtual {v0, v1, v4}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@8a
    goto :goto_35

    #@8b
    .line 1472
    .end local v2           #now:J
    .restart local v16       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catch_8b
    move-exception v14

    #@8c
    .line 1473
    .local v14, ex:Landroid/os/RemoteException;
    const-string v1, "WindowManager"

    #@8e
    const-string v4, "ITelephony threw RemoteException"

    #@90
    invoke-static {v1, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@93
    goto :goto_35

    #@94
    .line 1477
    .end local v14           #ex:Landroid/os/RemoteException;
    :cond_94
    const-string v1, "WindowManager"

    #@96
    const-string v4, "telephonyService is null"

    #@98
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    goto :goto_35
.end method
