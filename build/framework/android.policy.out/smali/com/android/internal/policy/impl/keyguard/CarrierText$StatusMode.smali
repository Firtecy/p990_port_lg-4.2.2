.class final enum Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
.super Ljava/lang/Enum;
.source "CarrierText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/CarrierText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "StatusMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum Normal:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum PersoLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimIOError:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimMissingLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimNotReady:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimPermDisabled:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

.field public static final enum SimPukLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 61
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@7
    const-string v1, "Normal"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->Normal:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@e
    .line 62
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@10
    const-string v1, "PersoLocked"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->PersoLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@17
    .line 63
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@19
    const-string v1, "SimMissing"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@20
    .line 64
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@22
    const-string v1, "SimMissingLocked"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissingLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@29
    .line 65
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@2b
    const-string v1, "SimPukLocked"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPukLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@32
    .line 66
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@34
    const-string v1, "SimLocked"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@3c
    .line 67
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@3e
    const-string v1, "SimPermDisabled"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPermDisabled:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@46
    .line 68
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@48
    const-string v1, "SimNotReady"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimNotReady:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@50
    .line 69
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@52
    const-string v1, "SimIOError"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimIOError:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@5b
    .line 60
    const/16 v0, 0x9

    #@5d
    new-array v0, v0, [Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@5f
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->Normal:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@61
    aput-object v1, v0, v3

    #@63
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->PersoLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@65
    aput-object v1, v0, v4

    #@67
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@69
    aput-object v1, v0, v5

    #@6b
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissingLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@6d
    aput-object v1, v0, v6

    #@6f
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPukLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@71
    aput-object v1, v0, v7

    #@73
    const/4 v1, 0x5

    #@74
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@76
    aput-object v2, v0, v1

    #@78
    const/4 v1, 0x6

    #@79
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPermDisabled:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@7b
    aput-object v2, v0, v1

    #@7d
    const/4 v1, 0x7

    #@7e
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimNotReady:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@80
    aput-object v2, v0, v1

    #@82
    const/16 v1, 0x8

    #@84
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimIOError:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@86
    aput-object v2, v0, v1

    #@88
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@8a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 60
    const-class v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    .registers 1

    #@0
    .prologue
    .line 60
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@8
    return-object v0
.end method
