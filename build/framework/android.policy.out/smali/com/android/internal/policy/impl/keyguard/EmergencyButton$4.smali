.class Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;
.super Landroid/telephony/PhoneStateListener;
.source "EmergencyButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 135
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@2
    invoke-static {v2, p1}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$302(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    #@5
    .line 136
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@7
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@9
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$300(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/telephony/ServiceState;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    #@10
    move-result v3

    #@11
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$402(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Z)Z

    #@14
    .line 138
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@16
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$500(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPhoneState()I

    #@21
    move-result v0

    #@22
    .line 139
    .local v0, phoneState:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@24
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$600(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@2f
    move-result-object v1

    #@30
    .line 140
    .local v1, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    const-string v3, "AU"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_41

    #@3c
    .line 141
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;->this$0:Lcom/android/internal/policy/impl/keyguard/EmergencyButton;

    #@3e
    invoke-static {v2, v1, v0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->access$100(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@41
    .line 143
    :cond_41
    return-void
.end method
