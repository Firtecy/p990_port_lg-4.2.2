.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;
.source "KeyguardSimPinView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$8;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$CheckSimPin;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;
    }
.end annotation


# static fields
.field private static final BUILD_TARGET_DCM:Ljava/lang/String; = "DCM"

.field private static final KEYPAD_CANCEL_FONT_SIZE_EN:F = 17.33f

.field private static final KEYPAD_CANCEL_FONT_SIZE_JA:F = 15.33f

.field private static final KEYPAD_NUMBER_FONT_SIZE:F = 23.33f

.field private static final KEYPAD_OK_FONT_SIZE:F = 17.33f

.field private static final LANGUAGE_EN:Ljava/lang/String; = "en"

.field private static final LANGUAGE_JA:Ljava/lang/String; = "ja"

.field protected static final LOG_TAG:Ljava/lang/String; = "KeyguardSimPinView"

.field private static final TEXT_FONT_SIZE_LARGE:F = 23.33f

.field private static final TEXT_FONT_SIZE_MEDIUM:F = 17.33f

.field private static countrycode:Ljava/lang/String;

.field public static mTempString:Ljava/lang/String;

.field private static operator:Ljava/lang/String;


# instance fields
.field private IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private currentLanguage:Ljava/lang/String;

.field private emergency_status:Z

.field private mEmergencyCallButton:Landroid/widget/TextView;

.field private mEmergencyText:Landroid/widget/TextView;

.field protected mHeaderText:Landroid/widget/TextView;

.field protected mHeaderTextString:Ljava/lang/String;

.field protected mPinEntry:Landroid/widget/TextView;

.field protected mRetryText:Landroid/widget/TextView;

.field protected mRetryTextString:Ljava/lang/String;

.field private mServiceState:Landroid/telephony/ServiceState;

.field protected volatile mSimCheckInProgress:Z

.field protected mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field protected mStrTitleWrongPin:Ljava/lang/String;

.field public mdialog:Landroid/app/AlertDialog;

.field protected pin1_retry_count:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 112
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->countrycode:Ljava/lang/String;

    #@3
    .line 113
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->operator:Ljava/lang/String;

    #@5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 156
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 76
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@6
    .line 98
    const-string v0, ""

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->currentLanguage:Ljava/lang/String;

    #@a
    .line 111
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->emergency_status:Z

    #@d
    .line 120
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$1;

    #@f
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@14
    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private showPinDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 474
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@e
    move-result-object v0

    #@f
    const v1, 0x104000a

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v0

    #@17
    const v1, 0x202026e

    #@1a
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@24
    .line 480
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@26
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@29
    move-result-object v0

    #@2a
    const/16 v1, 0x7d9

    #@2c
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@2f
    .line 481
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@31
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@34
    .line 483
    return-void
.end method


# virtual methods
.method protected getPasswordTextViewId()I
    .registers 3

    #@0
    .prologue
    .line 171
    const-string v0, "KeyguardSimPinView"

    #@2
    const-string v1, "[UICC] getPasswordTextViewId"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 172
    const v0, 0x10202d3

    #@a
    return v0
.end method

.method protected getSimUnlockProgressDialog()Landroid/app/Dialog;
    .registers 4

    #@0
    .prologue
    .line 429
    const-string v0, "KeyguardSimPinView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LGE]  getSimUnlockProgressDialog mSimUnlockProgressDialog status="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 430
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@1c
    if-nez v0, :cond_5e

    #@1e
    .line 431
    new-instance v0, Landroid/app/ProgressDialog;

    #@20
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@22
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@25
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@27
    .line 433
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, "KR"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_61

    #@33
    .line 434
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@35
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@37
    const v2, 0x20902b7

    #@3a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@41
    .line 448
    :goto_41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@43
    const/4 v1, 0x1

    #@44
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@47
    .line 449
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@49
    const/4 v1, 0x0

    #@4a
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@4d
    .line 450
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4f
    instance-of v0, v0, Landroid/app/Activity;

    #@51
    if-nez v0, :cond_5e

    #@53
    .line 451
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@55
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@58
    move-result-object v0

    #@59
    const/16 v1, 0x7d9

    #@5b
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@5e
    .line 455
    :cond_5e
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@60
    return-object v0

    #@61
    .line 436
    :cond_61
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    const-string v1, "KDDI"

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v0

    #@6b
    if-eqz v0, :cond_7c

    #@6d
    .line 437
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@6f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@71
    const v2, 0x20902cb

    #@74
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@7b
    goto :goto_41

    #@7c
    .line 439
    :cond_7c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    const-string v1, "DCM"

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v0

    #@86
    if-eqz v0, :cond_97

    #@88
    .line 440
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@8a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@8c
    const v2, 0x20902b1

    #@8f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@96
    goto :goto_41

    #@97
    .line 443
    :cond_97
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@99
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@9b
    const v2, 0x1040557

    #@9e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v1

    #@a2
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@a5
    goto :goto_41
.end method

.method protected onFinishInflate()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const v12, 0x418aa3d7

    #@4
    const/4 v11, 0x1

    #@5
    .line 177
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onFinishInflate()V

    #@8
    .line 184
    const-string v7, "KeyguardSimPinView"

    #@a
    const-string v8, "[KYC] R.id.headerText = 16909051"

    #@c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 185
    const v7, 0x10202fb

    #@12
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@15
    move-result-object v7

    #@16
    check-cast v7, Landroid/widget/TextView;

    #@18
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@1a
    .line 186
    const v7, 0x20d0054

    #@1d
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@20
    move-result-object v7

    #@21
    check-cast v7, Landroid/widget/TextView;

    #@23
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryText:Landroid/widget/TextView;

    #@25
    .line 188
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    const-string v8, "AU"

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v7

    #@2f
    if-eqz v7, :cond_4f

    #@31
    .line 189
    const v7, 0x20d0055

    #@34
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@37
    move-result-object v7

    #@38
    check-cast v7, Landroid/widget/TextView;

    #@3a
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyText:Landroid/widget/TextView;

    #@3c
    .line 190
    const v7, 0x10202b8

    #@3f
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@42
    move-result-object v7

    #@43
    check-cast v7, Landroid/widget/TextView;

    #@45
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@47
    .line 191
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@49
    const v8, 0x1040310

    #@4c
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    #@4f
    .line 195
    :cond_4f
    const-string v7, "KeyguardSimPinView"

    #@51
    const-string v8, "[UICC] Regist ICC Removed BroadcastReceiver"

    #@53
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 196
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@59
    move-result-object v7

    #@5a
    const-string v8, "DCM"

    #@5c
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v7

    #@60
    if-nez v7, :cond_70

    #@62
    .line 197
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@64
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@66
    new-instance v9, Landroid/content/IntentFilter;

    #@68
    const-string v10, "android.intent.action.SIM_STATE_CHANGED"

    #@6a
    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@6d
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@70
    .line 200
    :cond_70
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@73
    move-result-object v7

    #@74
    const-string v8, "KR"

    #@76
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v7

    #@7a
    if-nez v7, :cond_8d

    #@7c
    .line 201
    const v7, 0x10202d3

    #@7f
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@82
    move-result-object v7

    #@83
    check-cast v7, Landroid/widget/TextView;

    #@85
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mPinEntry:Landroid/widget/TextView;

    #@87
    .line 202
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mPinEntry:Landroid/widget/TextView;

    #@89
    const/4 v8, 0x3

    #@8a
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    #@8d
    .line 206
    :cond_8d
    :try_start_8d
    const-string v7, "phone"

    #@8f
    invoke-static {v7}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@92
    move-result-object v7

    #@93
    invoke-static {v7}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@96
    move-result-object v7

    #@97
    invoke-interface {v7}, Lcom/android/internal/telephony/ITelephony;->getIccPin1RetryCount()I

    #@9a
    move-result v7

    #@9b
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->pin1_retry_count:I

    #@9d
    .line 208
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_RESUME:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@9f
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->pin1_retry_count:I

    #@a1
    invoke-virtual {p0, v7, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->setUIStringByOperator(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;I)V

    #@a4
    .line 209
    const-string v7, "KeyguardSimPinView"

    #@a6
    new-instance v8, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v9, "[LGE] onFinishInflate= retrycount"

    #@ad
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->pin1_retry_count:I

    #@b3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v8

    #@b7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v8

    #@bb
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_be
    .catch Landroid/os/RemoteException; {:try_start_8d .. :try_end_be} :catch_199

    #@be
    .line 215
    :goto_be
    const v7, 0x10202df

    #@c1
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@c4
    move-result-object v4

    #@c5
    check-cast v4, Landroid/widget/TextView;

    #@c7
    .line 216
    .local v4, ok:Landroid/widget/TextView;
    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setEnabled(Z)V

    #@ca
    .line 217
    new-instance v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;

    #@cc
    invoke-direct {v7, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@cf
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d2
    .line 228
    const v7, 0x1020283

    #@d5
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@d8
    move-result-object v0

    #@d9
    check-cast v0, Landroid/widget/TextView;

    #@db
    .line 229
    .local v0, cancel:Landroid/widget/TextView;
    if-eqz v0, :cond_e8

    #@dd
    .line 230
    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setEnabled(Z)V

    #@e0
    .line 231
    new-instance v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$3;

    #@e2
    invoke-direct {v7, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@e5
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@e8
    .line 243
    :cond_e8
    const v7, 0x10202d4

    #@eb
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@ee
    move-result-object v5

    #@ef
    .line 244
    .local v5, pinDelete:Landroid/view/View;
    if-eqz v5, :cond_104

    #@f1
    .line 245
    invoke-virtual {v5, v13}, Landroid/view/View;->setVisibility(I)V

    #@f4
    .line 246
    new-instance v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$4;

    #@f6
    invoke-direct {v7, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@f9
    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@fc
    .line 255
    new-instance v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$5;

    #@fe
    invoke-direct {v7, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@101
    invoke-virtual {v5, v7}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@104
    .line 264
    :cond_104
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@106
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@109
    move-result-object v7

    #@10a
    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@10d
    move-result-object v7

    #@10e
    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@110
    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@113
    move-result-object v7

    #@114
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->currentLanguage:Ljava/lang/String;

    #@116
    .line 265
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@119
    move-result-object v7

    #@11a
    const-string v8, "DCM"

    #@11c
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11f
    move-result v7

    #@120
    if-eqz v7, :cond_142

    #@122
    .line 266
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->currentLanguage:Ljava/lang/String;

    #@124
    const-string v8, "ja"

    #@126
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@129
    move-result v7

    #@12a
    if-eqz v7, :cond_1a4

    #@12c
    .line 267
    invoke-virtual {v4, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    #@12f
    .line 268
    const v7, 0x417547ae

    #@132
    invoke-virtual {v0, v11, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    #@135
    .line 273
    :goto_135
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@137
    const v8, 0x41baa3d7

    #@13a
    invoke-virtual {v7, v11, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    #@13d
    .line 274
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryText:Landroid/widget/TextView;

    #@13f
    invoke-virtual {v7, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    #@142
    .line 278
    :cond_142
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@144
    const-string v8, "phone"

    #@146
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@149
    move-result-object v6

    #@14a
    check-cast v6, Landroid/telephony/TelephonyManager;

    #@14c
    .line 279
    .local v6, telephonyManager:Landroid/telephony/TelephonyManager;
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;

    #@14e
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V

    #@151
    .line 291
    .local v3, listener:Landroid/telephony/PhoneStateListener;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@154
    move-result-object v7

    #@155
    const-string v8, "AU"

    #@157
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v7

    #@15b
    if-eqz v7, :cond_168

    #@15d
    .line 292
    invoke-virtual {v6, v3, v11}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@160
    .line 293
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyText:Landroid/widget/TextView;

    #@162
    const v8, 0x1040325

    #@165
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    #@168
    .line 298
    :cond_168
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@16a
    if-nez v7, :cond_178

    #@16c
    .line 299
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@16e
    const-string v8, "statusbar"

    #@170
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@173
    move-result-object v7

    #@174
    check-cast v7, Landroid/app/StatusBarManager;

    #@176
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@178
    .line 302
    :cond_178
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@17a
    if-nez v7, :cond_1ab

    #@17c
    .line 303
    const-string v7, "KeyguardSimPinView"

    #@17e
    const-string v8, "Could not get status bar manager"

    #@180
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@183
    .line 317
    :goto_183
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@185
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    #@188
    move-result-object v8

    #@189
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@18c
    .line 318
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@18e
    const/16 v8, 0x12

    #@190
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setInputType(I)V

    #@193
    .line 321
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@195
    invoke-virtual {v7}, Landroid/widget/TextView;->requestFocus()Z

    #@198
    .line 322
    return-void

    #@199
    .line 211
    .end local v0           #cancel:Landroid/widget/TextView;
    .end local v3           #listener:Landroid/telephony/PhoneStateListener;
    .end local v4           #ok:Landroid/widget/TextView;
    .end local v5           #pinDelete:Landroid/view/View;
    .end local v6           #telephonyManager:Landroid/telephony/TelephonyManager;
    :catch_199
    move-exception v1

    #@19a
    .line 212
    .local v1, ex:Landroid/os/RemoteException;
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@19c
    const v8, 0x1040303

    #@19f
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    #@1a2
    goto/16 :goto_be

    #@1a4
    .line 270
    .end local v1           #ex:Landroid/os/RemoteException;
    .restart local v0       #cancel:Landroid/widget/TextView;
    .restart local v4       #ok:Landroid/widget/TextView;
    .restart local v5       #pinDelete:Landroid/view/View;
    :cond_1a4
    invoke-virtual {v4, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    #@1a7
    .line 271
    invoke-virtual {v0, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    #@1aa
    goto :goto_135

    #@1ab
    .line 308
    .restart local v3       #listener:Landroid/telephony/PhoneStateListener;
    .restart local v6       #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_1ab
    const/4 v2, 0x0

    #@1ac
    .line 311
    .local v2, flags:I
    const/high16 v7, 0x1

    #@1ae
    or-int/2addr v2, v7

    #@1af
    .line 312
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@1b1
    invoke-virtual {v7, v2}, Landroid/app/StatusBarManager;->disable(I)V

    #@1b4
    .line 313
    const-string v7, "KeyguardSimPinView"

    #@1b6
    const-string v8, "Disable the status bar touching !!!"

    #@1b8
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1bb
    goto :goto_183
.end method

.method public onPause()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 349
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 350
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@7
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@a
    .line 351
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@c
    .line 354
    :cond_c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "DCM"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_29

    #@18
    .line 355
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    if-eqz v0, :cond_29

    #@1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1e
    if-eqz v0, :cond_29

    #@20
    .line 356
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@24
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@27
    .line 357
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->IccRemovedBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@29
    .line 361
    :cond_29
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 11
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 365
    const v2, 0x10202df

    #@5
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    .line 366
    .local v1, ok:Landroid/view/View;
    const v2, 0x1020283

    #@c
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->findViewById(I)Landroid/view/View;

    #@f
    move-result-object v0

    #@10
    .line 368
    .local v0, cancel:Landroid/view/View;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    const-string v3, "AU"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_5d

    #@1c
    .line 369
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1e
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    sput-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@28
    .line 370
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@2b
    move-result v2

    #@2c
    const/4 v3, 0x3

    #@2d
    if-ne v2, v3, :cond_71

    #@2f
    const-string v2, "112"

    #@31
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v2

    #@37
    if-nez v2, :cond_4d

    #@39
    const-string v2, "911"

    #@3b
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v2

    #@41
    if-nez v2, :cond_4d

    #@43
    const-string v2, "000"

    #@45
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v2

    #@4b
    if-eqz v2, :cond_71

    #@4d
    .line 371
    :cond_4d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@4f
    const v3, 0x1080084

    #@52
    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@55
    .line 374
    :goto_55
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@57
    const v3, 0x1040310

    #@5a
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    #@5d
    .line 377
    :cond_5d
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@60
    move-result v2

    #@61
    if-lez v2, :cond_7a

    #@63
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@66
    move-result v2

    #@67
    const/4 v3, 0x4

    #@68
    if-ge v2, v3, :cond_7a

    #@6a
    .line 378
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    #@6d
    .line 379
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    #@70
    .line 389
    :goto_70
    return-void

    #@71
    .line 373
    :cond_71
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyCallButton:Landroid/widget/TextView;

    #@73
    const v3, 0x10802cf

    #@76
    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@79
    goto :goto_55

    #@7a
    .line 381
    :cond_7a
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@7d
    move-result v2

    #@7e
    if-nez v2, :cond_87

    #@80
    .line 382
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    #@83
    .line 383
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    #@86
    goto :goto_70

    #@87
    .line 386
    :cond_87
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    #@8a
    .line 387
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    #@8d
    goto :goto_70
.end method

.method public resetState()V
    .registers 3

    #@0
    .prologue
    .line 164
    const-string v0, "KeyguardSimPinView"

    #@2
    const-string v1, "[UICC] resetState"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 166
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@d
    .line 167
    return-void
.end method

.method protected setPopupStringByOperator(I)V
    .registers 9
    .parameter "retryCount"

    #@0
    .prologue
    const v5, 0x20902bc

    #@3
    const/4 v6, 0x0

    #@4
    const/4 v4, 0x1

    #@5
    .line 663
    const/4 v0, 0x0

    #@6
    .line 664
    .local v0, popupMsg:Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStrTitleWrongPin:Ljava/lang/String;

    #@9
    .line 666
    if-lez p1, :cond_d0

    #@b
    .line 668
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    const-string v2, "KR"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_7f

    #@17
    .line 669
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "SKT"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-eqz v1, :cond_49

    #@23
    .line 671
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getResources()Landroid/content/res/Resources;

    #@26
    move-result-object v1

    #@27
    const v2, 0x20902bd

    #@2a
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStrTitleWrongPin:Ljava/lang/String;

    #@30
    .line 672
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@32
    const v2, 0x20902bb

    #@35
    new-array v3, v4, [Ljava/lang/Object;

    #@37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v4

    #@3b
    aput-object v4, v3, v6

    #@3d
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    .line 693
    :cond_41
    :goto_41
    if-eqz v0, :cond_48

    #@43
    .line 694
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mStrTitleWrongPin:Ljava/lang/String;

    #@45
    invoke-direct {p0, v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->showPinDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 695
    :cond_48
    return-void

    #@49
    .line 673
    :cond_49
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, "KT"

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_64

    #@55
    .line 675
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@57
    new-array v2, v4, [Ljava/lang/Object;

    #@59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v3

    #@5d
    aput-object v3, v2, v6

    #@5f
    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    goto :goto_41

    #@64
    .line 676
    :cond_64
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    const-string v2, "LGU"

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v1

    #@6e
    if-eqz v1, :cond_41

    #@70
    .line 678
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@72
    new-array v2, v4, [Ljava/lang/Object;

    #@74
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@77
    move-result-object v3

    #@78
    aput-object v3, v2, v6

    #@7a
    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@7d
    move-result-object v0

    #@7e
    goto :goto_41

    #@7f
    .line 681
    :cond_7f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    const-string v2, "DCM"

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v1

    #@89
    if-eqz v1, :cond_bd

    #@8b
    .line 682
    new-instance v1, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@92
    const v3, 0x20902ae

    #@95
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    const-string v2, "\n"

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@a5
    const v3, 0x2090125

    #@a8
    new-array v4, v4, [Ljava/lang/Object;

    #@aa
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ad
    move-result-object v5

    #@ae
    aput-object v5, v4, v6

    #@b0
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v1

    #@b8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v0

    #@bc
    goto :goto_41

    #@bd
    .line 685
    :cond_bd
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@bf
    const v2, 0x2090116

    #@c2
    new-array v3, v4, [Ljava/lang/Object;

    #@c4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c7
    move-result-object v4

    #@c8
    aput-object v4, v3, v6

    #@ca
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@cd
    move-result-object v0

    #@ce
    goto/16 :goto_41

    #@d0
    .line 689
    :cond_d0
    if-nez p1, :cond_41

    #@d2
    .line 690
    const-string v1, "KeyguardSimPinView"

    #@d4
    new-instance v2, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v3, "[fcmania30] SimPinView setPopupStringByOperator retryCount ="

    #@db
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v2

    #@df
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v2

    #@e3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v2

    #@e7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 691
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@ec
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@ef
    move-result-object v1

    #@f0
    const-string v2, "sim_err_popup_msg"

    #@f2
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@f5
    goto/16 :goto_41
.end method

.method protected setUIStringByOperator(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;I)V
    .registers 10
    .parameter "state"
    .parameter "retryCount"

    #@0
    .prologue
    const v6, 0x20902b9

    #@3
    const v5, 0x2090125

    #@6
    const/4 v4, 0x1

    #@7
    const/4 v3, 0x0

    #@8
    const v2, 0x20902b8

    #@b
    .line 574
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$8;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSimPinView$PinLockState:[I

    #@d
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->ordinal()I

    #@10
    move-result v1

    #@11
    aget v0, v0, v1

    #@13
    packed-switch v0, :pswitch_data_2ba

    #@16
    .line 658
    :cond_16
    :goto_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@18
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@1a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1d
    .line 659
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryText:Landroid/widget/TextView;

    #@1f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@21
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@24
    .line 660
    return-void

    #@25
    .line 578
    :pswitch_25
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    const-string v1, "KR"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_b0

    #@31
    .line 579
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    const-string v1, "SKT"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_5a

    #@3d
    .line 581
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@3f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@49
    .line 582
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@59
    goto :goto_16

    #@5a
    .line 583
    :cond_5a
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    const-string v1, "KT"

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_86

    #@66
    .line 585
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@68
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6b
    move-result-object v0

    #@6c
    const v1, 0x20902ba

    #@6f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@72
    move-result-object v0

    #@73
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@75
    .line 586
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@77
    new-array v1, v4, [Ljava/lang/Object;

    #@79
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7c
    move-result-object v2

    #@7d
    aput-object v2, v1, v3

    #@7f
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@85
    goto :goto_16

    #@86
    .line 587
    :cond_86
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@89
    move-result-object v0

    #@8a
    const-string v1, "LGU"

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v0

    #@90
    if-eqz v0, :cond_16

    #@92
    .line 589
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@94
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@97
    move-result-object v0

    #@98
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@9b
    move-result-object v0

    #@9c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@9e
    .line 590
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@a0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a3
    move-result-object v0

    #@a4
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@a7
    move-result-object v0

    #@a8
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ab
    move-result-object v0

    #@ac
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@ae
    goto/16 :goto_16

    #@b0
    .line 593
    :cond_b0
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@b3
    move-result-object v0

    #@b4
    const-string v1, "DCM"

    #@b6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v0

    #@ba
    if-nez v0, :cond_c8

    #@bc
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@bf
    move-result-object v0

    #@c0
    const-string v1, "TEL"

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v0

    #@c6
    if-eqz v0, :cond_e8

    #@c8
    .line 594
    :cond_c8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@ca
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@cd
    move-result-object v0

    #@ce
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d1
    move-result-object v0

    #@d2
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@d4
    .line 595
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@d7
    move-result-object v0

    #@d8
    new-array v1, v4, [Ljava/lang/Object;

    #@da
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@dd
    move-result-object v2

    #@de
    aput-object v2, v1, v3

    #@e0
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@e3
    move-result-object v0

    #@e4
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@e6
    goto/16 :goto_16

    #@e8
    .line 597
    :cond_e8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@ea
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@ed
    move-result-object v0

    #@ee
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f1
    move-result-object v0

    #@f2
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@f4
    .line 598
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@f7
    move-result-object v0

    #@f8
    new-array v1, v4, [Ljava/lang/Object;

    #@fa
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fd
    move-result-object v2

    #@fe
    aput-object v2, v1, v3

    #@100
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@103
    move-result-object v0

    #@104
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@106
    goto/16 :goto_16

    #@108
    .line 604
    :pswitch_108
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@10b
    move-result-object v0

    #@10c
    const-string v1, "KR"

    #@10e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@111
    move-result v0

    #@112
    if-eqz v0, :cond_195

    #@114
    .line 605
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@117
    move-result-object v0

    #@118
    const-string v1, "SKT"

    #@11a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v0

    #@11e
    if-eqz v0, :cond_13e

    #@120
    .line 607
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@122
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@125
    move-result-object v0

    #@126
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@129
    move-result-object v0

    #@12a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@12c
    .line 608
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@12e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@131
    move-result-object v0

    #@132
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@135
    move-result-object v0

    #@136
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@139
    move-result-object v0

    #@13a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@13c
    goto/16 :goto_16

    #@13e
    .line 609
    :cond_13e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@141
    move-result-object v0

    #@142
    const-string v1, "KT"

    #@144
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@147
    move-result v0

    #@148
    if-eqz v0, :cond_16b

    #@14a
    .line 611
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@14c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14f
    move-result-object v0

    #@150
    const v1, 0x10402fc

    #@153
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@156
    move-result-object v0

    #@157
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@159
    .line 612
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@15b
    new-array v1, v4, [Ljava/lang/Object;

    #@15d
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@160
    move-result-object v2

    #@161
    aput-object v2, v1, v3

    #@163
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@166
    move-result-object v0

    #@167
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@169
    goto/16 :goto_16

    #@16b
    .line 613
    :cond_16b
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@16e
    move-result-object v0

    #@16f
    const-string v1, "LGU"

    #@171
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@174
    move-result v0

    #@175
    if-eqz v0, :cond_16

    #@177
    .line 615
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@179
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17c
    move-result-object v0

    #@17d
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@180
    move-result-object v0

    #@181
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@183
    .line 616
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@185
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@188
    move-result-object v0

    #@189
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@18c
    move-result-object v0

    #@18d
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@190
    move-result-object v0

    #@191
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@193
    goto/16 :goto_16

    #@195
    .line 619
    :cond_195
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@198
    move-result-object v0

    #@199
    const-string v1, "DCM"

    #@19b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19e
    move-result v0

    #@19f
    if-nez v0, :cond_1ad

    #@1a1
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1a4
    move-result-object v0

    #@1a5
    const-string v1, "TEL"

    #@1a7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1aa
    move-result v0

    #@1ab
    if-eqz v0, :cond_1d0

    #@1ad
    .line 620
    :cond_1ad
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getResources()Landroid/content/res/Resources;

    #@1b0
    move-result-object v0

    #@1b1
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@1b7
    .line 625
    :goto_1b7
    if-ltz p2, :cond_16

    #@1b9
    const/4 v0, 0x4

    #@1ba
    if-ge p2, v0, :cond_16

    #@1bc
    .line 626
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@1bf
    move-result-object v0

    #@1c0
    new-array v1, v4, [Ljava/lang/Object;

    #@1c2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c5
    move-result-object v2

    #@1c6
    aput-object v2, v1, v3

    #@1c8
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1cb
    move-result-object v0

    #@1cc
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@1ce
    goto/16 :goto_16

    #@1d0
    .line 623
    :cond_1d0
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1d2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d5
    move-result-object v0

    #@1d6
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1d9
    move-result-object v0

    #@1da
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@1dc
    goto :goto_1b7

    #@1dd
    .line 632
    :pswitch_1dd
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1e0
    move-result-object v0

    #@1e1
    const-string v1, "KR"

    #@1e3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e6
    move-result v0

    #@1e7
    if-eqz v0, :cond_264

    #@1e9
    .line 633
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1ec
    move-result-object v0

    #@1ed
    const-string v1, "SKT"

    #@1ef
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f2
    move-result v0

    #@1f3
    if-eqz v0, :cond_211

    #@1f5
    .line 635
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getResources()Landroid/content/res/Resources;

    #@1f8
    move-result-object v0

    #@1f9
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1fc
    move-result-object v0

    #@1fd
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@1ff
    .line 636
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@201
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@204
    move-result-object v0

    #@205
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@208
    move-result-object v0

    #@209
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@20c
    move-result-object v0

    #@20d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@20f
    goto/16 :goto_16

    #@211
    .line 637
    :cond_211
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@214
    move-result-object v0

    #@215
    const-string v1, "KT"

    #@217
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21a
    move-result v0

    #@21b
    if-eqz v0, :cond_23c

    #@21d
    .line 639
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getResources()Landroid/content/res/Resources;

    #@220
    move-result-object v0

    #@221
    const v1, 0x10402fc

    #@224
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@227
    move-result-object v0

    #@228
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@22a
    .line 640
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@22c
    new-array v1, v4, [Ljava/lang/Object;

    #@22e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@231
    move-result-object v2

    #@232
    aput-object v2, v1, v3

    #@234
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@237
    move-result-object v0

    #@238
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@23a
    goto/16 :goto_16

    #@23c
    .line 641
    :cond_23c
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@23f
    move-result-object v0

    #@240
    const-string v1, "LGU"

    #@242
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@245
    move-result v0

    #@246
    if-eqz v0, :cond_16

    #@248
    .line 643
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getResources()Landroid/content/res/Resources;

    #@24b
    move-result-object v0

    #@24c
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@24f
    move-result-object v0

    #@250
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@252
    .line 644
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@254
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@257
    move-result-object v0

    #@258
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@25b
    move-result-object v0

    #@25c
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@25f
    move-result-object v0

    #@260
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@262
    goto/16 :goto_16

    #@264
    .line 647
    :cond_264
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@267
    move-result-object v0

    #@268
    const-string v1, "DCM"

    #@26a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26d
    move-result v0

    #@26e
    if-nez v0, :cond_27c

    #@270
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@273
    move-result-object v0

    #@274
    const-string v1, "TEL"

    #@276
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@279
    move-result v0

    #@27a
    if-eqz v0, :cond_29c

    #@27c
    .line 648
    :cond_27c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@27e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@281
    move-result-object v0

    #@282
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@285
    move-result-object v0

    #@286
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@288
    .line 649
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@28b
    move-result-object v0

    #@28c
    new-array v1, v4, [Ljava/lang/Object;

    #@28e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@291
    move-result-object v2

    #@292
    aput-object v2, v1, v3

    #@294
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@297
    move-result-object v0

    #@298
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@29a
    goto/16 :goto_16

    #@29c
    .line 652
    :cond_29c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@29f
    move-result-object v0

    #@2a0
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2a3
    move-result-object v0

    #@2a4
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderTextString:Ljava/lang/String;

    #@2a6
    .line 653
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@2a9
    move-result-object v0

    #@2aa
    new-array v1, v4, [Ljava/lang/Object;

    #@2ac
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2af
    move-result-object v2

    #@2b0
    aput-object v2, v1, v3

    #@2b2
    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@2b5
    move-result-object v0

    #@2b6
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mRetryTextString:Ljava/lang/String;

    #@2b8
    goto/16 :goto_16

    #@2ba
    .line 574
    :pswitch_data_2ba
    .packed-switch 0x1
        :pswitch_25
        :pswitch_108
        :pswitch_1dd
    .end packed-switch
.end method

.method protected showPinDialog(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 462
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@3
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@8
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@f
    move-result-object v0

    #@10
    const v1, 0x104000a

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@1d
    .line 467
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@1f
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@22
    move-result-object v0

    #@23
    const/16 v1, 0x7d9

    #@25
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@28
    .line 468
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mdialog:Landroid/app/AlertDialog;

    #@2a
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2d
    .line 470
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 344
    return-void
.end method

.method public updateEmergencyText()V
    .registers 4

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    if-eqz v0, :cond_26

    #@4
    .line 330
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mServiceState:Landroid/telephony/ServiceState;

    #@6
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    #@9
    move-result v0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->emergency_status:Z

    #@c
    .line 331
    const-string v0, "KeyguardSimPinView"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "[LGE] get the service status="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->emergency_status:Z

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 334
    :cond_26
    const-string v0, "KeyguardSimPinView"

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "Service state on PIN/PUK = "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->emergency_status:Z

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 335
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->emergency_status:Z

    #@42
    if-eqz v0, :cond_4d

    #@44
    .line 336
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyText:Landroid/widget/TextView;

    #@46
    const v1, 0x1040325

    #@49
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@4c
    .line 339
    :goto_4c
    return-void

    #@4d
    .line 338
    :cond_4d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mEmergencyText:Landroid/widget/TextView;

    #@4f
    const v1, 0x104030b

    #@52
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@55
    goto :goto_4c
.end method

.method protected verifyPasswordAndUnlock()V
    .registers 5

    #@0
    .prologue
    .line 488
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 489
    .local v0, entry:Ljava/lang/String;
    const-string v1, "KeyguardSimPinView"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "[UICC] verifyPasswordAndUnlock : "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimCheckInProgress:Z

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ", entry="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 491
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    if-ge v1, v2, :cond_4c

    #@35
    .line 496
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@37
    const v2, 0x104009e

    #@3a
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    #@3d
    .line 499
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@3f
    const-string v2, ""

    #@41
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@44
    .line 500
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@46
    const-wide/16 v2, 0x0

    #@48
    invoke-interface {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@4b
    .line 568
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 504
    :cond_4c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getSimUnlockProgressDialog()Landroid/app/Dialog;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    #@53
    .line 507
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimCheckInProgress:Z

    #@55
    if-nez v1, :cond_4b

    #@57
    .line 508
    const/4 v1, 0x1

    #@58
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimCheckInProgress:Z

    #@5a
    .line 509
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@5c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@5e
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    invoke-direct {v1, p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;Ljava/lang/String;)V

    #@69
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->start()V

    #@6c
    goto :goto_4b
.end method
