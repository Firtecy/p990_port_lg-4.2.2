.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;
.super Landroid/telephony/PhoneStateListener;
.source "KeyguardSimPinView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 279
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->access$102(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    #@5
    .line 284
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "AU"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1d

    #@11
    .line 285
    const-string v0, "KeyguardSimPinView"

    #@13
    const-string v1, "[PIN/PUK] onServiceStateChanged"

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 286
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->updateEmergencyText()V

    #@1d
    .line 288
    :cond_1d
    return-void
.end method
