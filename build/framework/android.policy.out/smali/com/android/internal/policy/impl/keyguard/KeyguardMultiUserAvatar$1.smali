.class Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;
.super Ljava/lang/Object;
.source "KeyguardMultiUserAvatar.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->updateVisualsForActive(ZZILjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

.field final synthetic val$finalAlpha:F

.field final synthetic val$finalScale:F

.field final synthetic val$finalTextAlpha:I

.field final synthetic val$initAlpha:F

.field final synthetic val$initScale:F

.field final synthetic val$initTextAlpha:I


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;FFFFII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@2
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initScale:F

    #@4
    iput p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalScale:F

    #@6
    iput p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initAlpha:F

    #@8
    iput p5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalAlpha:F

    #@a
    iput p6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initTextAlpha:I

    #@c
    iput p7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalTextAlpha:I

    #@e
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@11
    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 10
    .parameter "animation"

    #@0
    .prologue
    const/16 v7, 0xff

    #@2
    const/high16 v6, 0x3f80

    #@4
    .line 181
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    #@7
    move-result v1

    #@8
    .line 182
    .local v1, r:F
    sub-float v4, v6, v1

    #@a
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initScale:F

    #@c
    mul-float/2addr v4, v5

    #@d
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalScale:F

    #@f
    mul-float/2addr v5, v1

    #@10
    add-float v2, v4, v5

    #@12
    .line 183
    .local v2, scale:F
    sub-float v4, v6, v1

    #@14
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initAlpha:F

    #@16
    mul-float/2addr v4, v5

    #@17
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalAlpha:F

    #@19
    mul-float/2addr v5, v1

    #@1a
    add-float v0, v4, v5

    #@1c
    .line 184
    .local v0, alpha:F
    sub-float v4, v6, v1

    #@1e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$initTextAlpha:I

    #@20
    int-to-float v5, v5

    #@21
    mul-float/2addr v4, v5

    #@22
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->val$finalTextAlpha:I

    #@24
    int-to-float v5, v5

    #@25
    mul-float/2addr v5, v1

    #@26
    add-float/2addr v4, v5

    #@27
    float-to-int v3, v4

    #@28
    .line 185
    .local v3, textAlpha:I
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@2a
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;)Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardCircleFramedDrawable;->setScale(F)V

    #@31
    .line 186
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@33
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;)Landroid/widget/ImageView;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    #@3a
    .line 187
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@3c
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;)Landroid/widget/TextView;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    #@43
    move-result v5

    #@44
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    #@47
    .line 188
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;

    #@49
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserAvatar;)Landroid/widget/ImageView;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Landroid/widget/ImageView;->invalidate()V

    #@50
    .line 189
    return-void
.end method
