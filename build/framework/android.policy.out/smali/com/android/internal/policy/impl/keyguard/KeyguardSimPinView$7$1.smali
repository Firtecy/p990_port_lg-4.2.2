.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;
.super Ljava/lang/Object;
.source "KeyguardSimPinView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->onSimCheckResponse(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 511
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 513
    const-string v6, "KeyguardSimPinView"

    #@5
    new-instance v7, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v8, "[LGE]  onSimCheckResponse mSimUnlockProgressDialog status="

    #@c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v7

    #@10
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@12
    iget-object v8, v8, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@14
    iget-object v8, v8, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 514
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@23
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@25
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@27
    if-eqz v6, :cond_38

    #@29
    .line 515
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@2b
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@2d
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2f
    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    #@32
    .line 516
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@34
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@36
    iput-object v11, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@38
    .line 518
    :cond_38
    const-string v6, "KeyguardSimPinView"

    #@3a
    new-instance v7, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v8, "[UICC] onSimCheckResponse : "

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v7

    #@45
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->val$success:Z

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v7

    #@4f
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 519
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->val$success:Z

    #@54
    if-eqz v6, :cond_c9

    #@56
    .line 522
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    const-string v7, "KR"

    #@5c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v6

    #@60
    if-nez v6, :cond_9f

    #@62
    .line 524
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@64
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@66
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)Landroid/content/Context;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@6d
    move-result-object v1

    #@6e
    .line 525
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v6, 0x10900e2

    #@71
    invoke-virtual {v1, v6, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@74
    move-result-object v2

    #@75
    .line 528
    .local v2, layout:Landroid/view/View;
    const v6, 0x102000b

    #@78
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7b
    move-result-object v4

    #@7c
    check-cast v4, Landroid/widget/TextView;

    #@7e
    .line 530
    .local v4, text:Landroid/widget/TextView;
    const v6, 0x2090127

    #@81
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    #@84
    .line 533
    new-instance v5, Landroid/widget/Toast;

    #@86
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@88
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@8a
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)Landroid/content/Context;

    #@8d
    move-result-object v6

    #@8e
    invoke-direct {v5, v6}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    #@91
    .line 534
    .local v5, toast:Landroid/widget/Toast;
    invoke-virtual {v5, v10}, Landroid/widget/Toast;->setDuration(I)V

    #@94
    .line 535
    const/16 v6, 0x10

    #@96
    invoke-virtual {v5, v6, v9, v9}, Landroid/widget/Toast;->setGravity(III)V

    #@99
    .line 536
    invoke-virtual {v5, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    #@9c
    .line 537
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    #@9f
    .line 541
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    .end local v2           #layout:Landroid/view/View;
    .end local v4           #text:Landroid/widget/TextView;
    .end local v5           #toast:Landroid/widget/Toast;
    :cond_9f
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@a1
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@a3
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->getContext()Landroid/content/Context;

    #@a6
    move-result-object v6

    #@a7
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@aa
    move-result-object v6

    #@ab
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->reportSimUnlocked()V

    #@ae
    .line 542
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@b0
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@b2
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@b4
    invoke-interface {v6, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@b7
    .line 561
    :goto_b7
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@b9
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@bb
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@bd
    const-wide/16 v7, 0x0

    #@bf
    invoke-interface {v6, v7, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@c2
    .line 562
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@c4
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@c6
    iput-boolean v9, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimCheckInProgress:Z

    #@c8
    .line 563
    return-void

    #@c9
    .line 544
    :cond_c9
    const/4 v3, 0x0

    #@ca
    .line 548
    .local v3, pin1_retry_count:I
    :try_start_ca
    const-string v6, "phone"

    #@cc
    invoke-static {v6}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@cf
    move-result-object v6

    #@d0
    invoke-static {v6}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@d3
    move-result-object v6

    #@d4
    invoke-interface {v6}, Lcom/android/internal/telephony/ITelephony;->getIccPin1RetryCount()I

    #@d7
    move-result v3

    #@d8
    .line 550
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@da
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@dc
    invoke-virtual {v6, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->setPopupStringByOperator(I)V

    #@df
    .line 552
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@e1
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@e3
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_CHECKPIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@e5
    invoke-virtual {v6, v7, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->setUIStringByOperator(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;I)V
    :try_end_e8
    .catch Landroid/os/RemoteException; {:try_start_ca .. :try_end_e8} :catch_f4

    #@e8
    .line 559
    :goto_e8
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@ea
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@ec
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@ee
    const-string v7, ""

    #@f0
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f3
    goto :goto_b7

    #@f4
    .line 554
    :catch_f4
    move-exception v0

    #@f5
    .line 555
    .local v0, ex:Landroid/os/RemoteException;
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;

    #@f7
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@f9
    iget-object v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mHeaderText:Landroid/widget/TextView;

    #@fb
    const v7, 0x1040558

    #@fe
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    #@101
    goto :goto_e8
.end method
