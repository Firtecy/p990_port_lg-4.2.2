.class Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;
.super Ljava/lang/Object;
.source "AccountUnlockScreen.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->asyncCheckPassword()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 281
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 284
    .local p1, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@2
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@5
    move-result-object v3

    #@6
    const/16 v4, 0x7530

    #@8
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@b
    .line 285
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/os/Bundle;

    #@11
    .line 286
    .local v1, result:Landroid/os/Bundle;
    const-string v3, "booleanResult"

    #@13
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@16
    move-result v2

    #@17
    .line 287
    .local v2, verified:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@19
    invoke-static {v3, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    :try_end_1c
    .catchall {:try_start_0 .. :try_end_1c} :catchall_6d
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_1c} :catch_2b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1c} :catch_41
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_1c} :catch_57

    #@1c
    .line 295
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@1e
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@21
    move-result-object v3

    #@22
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;

    #@24
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;)V

    #@27
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@2a
    .line 301
    .end local v1           #result:Landroid/os/Bundle;
    .end local v2           #verified:Z
    :goto_2a
    return-void

    #@2b
    .line 288
    :catch_2b
    move-exception v0

    #@2c
    .line 289
    .local v0, e:Landroid/accounts/OperationCanceledException;
    :try_start_2c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@2e
    const/4 v4, 0x0

    #@2f
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    :try_end_32
    .catchall {:try_start_2c .. :try_end_32} :catchall_6d

    #@32
    .line 295
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@34
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@37
    move-result-object v3

    #@38
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;

    #@3a
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;)V

    #@3d
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@40
    goto :goto_2a

    #@41
    .line 290
    .end local v0           #e:Landroid/accounts/OperationCanceledException;
    :catch_41
    move-exception v0

    #@42
    .line 291
    .local v0, e:Ljava/io/IOException;
    :try_start_42
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@44
    const/4 v4, 0x0

    #@45
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    :try_end_48
    .catchall {:try_start_42 .. :try_end_48} :catchall_6d

    #@48
    .line 295
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@4a
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@4d
    move-result-object v3

    #@4e
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;

    #@50
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;)V

    #@53
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@56
    goto :goto_2a

    #@57
    .line 292
    .end local v0           #e:Ljava/io/IOException;
    :catch_57
    move-exception v0

    #@58
    .line 293
    .local v0, e:Landroid/accounts/AuthenticatorException;
    :try_start_58
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@5a
    const/4 v4, 0x0

    #@5b
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    :try_end_5e
    .catchall {:try_start_58 .. :try_end_5e} :catchall_6d

    #@5e
    .line 295
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@60
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@63
    move-result-object v3

    #@64
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;

    #@66
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;)V

    #@69
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@6c
    goto :goto_2a

    #@6d
    .end local v0           #e:Landroid/accounts/AuthenticatorException;
    :catchall_6d
    move-exception v3

    #@6e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@70
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@73
    move-result-object v4

    #@74
    new-instance v5, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;

    #@76
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;)V

    #@79
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@7c
    throw v3
.end method
