.class Lcom/android/internal/policy/impl/keyguard/PagedView$8;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->zoomIn(Ljava/lang/Runnable;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

.field final synthetic val$onCompleteRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Ljava/lang/Runnable;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 2229
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->val$onCompleteRunnable:Ljava/lang/Runnable;

    #@4
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 2246
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    const/4 v1, 0x0

    #@3
    iput-object v1, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@5
    .line 2247
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 2250
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    const/4 v1, 0x0

    #@3
    iput-object v1, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@5
    .line 2251
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->val$onCompleteRunnable:Ljava/lang/Runnable;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 2252
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->val$onCompleteRunnable:Ljava/lang/Runnable;

    #@b
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@e
    .line 2254
    :cond_e
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 5
    .parameter "animation"

    #@0
    .prologue
    .line 2233
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_29

    #@8
    .line 2234
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    #@11
    move-result-object v0

    #@12
    const/4 v1, 0x0

    #@13
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@19
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$600(Lcom/android/internal/policy/impl/keyguard/PagedView;)J

    #@1c
    move-result-wide v1

    #@1d
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    #@20
    move-result-object v0

    #@21
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/PagedView$8$1;

    #@23
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$8$1;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView$8;)V

    #@26
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    #@29
    .line 2243
    :cond_29
    return-void
.end method
