.class public Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;
.super Ljava/lang/Object;
.source "KeyguardViewMediator.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$4;
    }
.end annotation


# static fields
.field protected static final AWAKE_INTERVAL_DEFAULT_MS:I = 0x2710

.field private static final DBG_WAKE:Z = false

.field private static final DEBUG:Z = false

.field private static final DELAYED_KEYGUARD_ACTION:Ljava/lang/String; = "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

.field private static final ENABLE_INSECURE_STATUS_BAR_EXPAND:Z = true

.field private static final HIDE:I = 0x3

.field private static final KEYGUARD_DISPLAY_TIMEOUT_DELAY_DEFAULT:I = 0x7530

.field private static final KEYGUARD_DONE:I = 0x9

.field private static final KEYGUARD_DONE_AUTHENTICATING:I = 0xb

.field private static final KEYGUARD_DONE_DRAWING:I = 0xa

.field private static final KEYGUARD_DONE_DRAWING_TIMEOUT_MS:I = 0x7d0

.field private static final KEYGUARD_LOCK_AFTER_DELAY_DEFAULT:I = 0x1388

.field private static final KEYGUARD_TIMEOUT:I = 0xd

.field private static final NOTIFY_SCREEN_OFF:I = 0x6

.field private static final NOTIFY_SCREEN_ON:I = 0x7

.field private static final RESET:I = 0x4

.field private static final SET_HIDDEN:I = 0xc

.field private static final SHOW:I = 0x2

.field private static final TAG:Ljava/lang/String; = "KeyguardViewMediator"

.field private static final TIMEOUT:I = 0x1

.field private static final VERIFY_UNLOCK:I = 0x5

.field private static final WAKE_WHEN_READY:I = 0x8


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallback:Lcom/android/internal/policy/impl/PhoneWindowManager;

.field private mContext:Landroid/content/Context;

.field private mDelayedShowingSequence:I

.field private mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

.field private mExternallyEnabled:Z

.field private mHandler:Landroid/os/Handler;

.field private mHidden:Z

.field private mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

.field private mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockSoundId:I

.field private mLockSoundStreamId:I

.field private final mLockSoundVolume:F

.field private mLockSounds:Landroid/media/SoundPool;

.field private mMasterStreamType:I

.field private mNeedToReshowWhenReenabled:Z

.field private mPM:Landroid/os/PowerManager;

.field private mPhoneState:Ljava/lang/String;

.field private mScreenOn:Z

.field private mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mShowLockIcon:Z

.field private mShowing:Z

.field private mShowingLockIcon:Z

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mSuppressNextLockSound:Z

.field private mSystemReady:Z

.field private mUnlockSoundId:I

.field mUpdateCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

.field private mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

.field private mUserPresentIntent:Landroid/content/Intent;

.field private mWaitingUntilKeyguardVisible:Z

.field private mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakelockSequence:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 14
    .parameter "context"
    .parameter "callback"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 358
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 158
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@7
    .line 191
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@9
    .line 198
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@b
    .line 202
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@d
    .line 205
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHidden:Z

    #@f
    .line 232
    sget-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    #@13
    .line 243
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@15
    .line 256
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;

    #@17
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@1a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@1c
    .line 802
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$2;

    #@1e
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@21
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@23
    .line 974
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;

    #@25
    invoke-direct {v0, p0, v9}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V

    #@28
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2a
    .line 359
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2c
    .line 360
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mCallback:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2e
    .line 361
    const-string v0, "power"

    #@30
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/os/PowerManager;

    #@36
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@38
    .line 362
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@3a
    const v1, 0x1000001a

    #@3d
    const-string v3, "keyguard"

    #@3f
    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@45
    .line 364
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@47
    invoke-virtual {v0, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@4a
    .line 365
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@4c
    const-string v1, "show keyguard"

    #@4e
    invoke-virtual {v0, v9, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@51
    move-result-object v0

    #@52
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@54
    .line 366
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@56
    invoke-virtual {v0, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@59
    .line 368
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@5b
    const-string v1, "keyguardWakeAndHandOff"

    #@5d
    invoke-virtual {v0, v9, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@60
    move-result-object v0

    #@61
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@63
    .line 369
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@65
    invoke-virtual {v0, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@68
    .line 371
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@6a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@6c
    new-instance v3, Landroid/content/IntentFilter;

    #@6e
    const-string v4, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    #@70
    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@73
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@76
    .line 373
    const-string v0, "alarm"

    #@78
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7b
    move-result-object v0

    #@7c
    check-cast v0, Landroid/app/AlarmManager;

    #@7e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    #@80
    .line 375
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@82
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;-><init>(Landroid/content/Context;)V

    #@85
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@87
    .line 377
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    #@89
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@8b
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@8e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@90
    .line 378
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardViewProperties;

    #@92
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@94
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@96
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardViewProperties;-><init>(Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V

    #@99
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

    #@9b
    .line 381
    const-string v0, "window"

    #@9d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a0
    move-result-object v2

    #@a1
    check-cast v2, Landroid/view/WindowManager;

    #@a3
    .line 382
    .local v2, wm:Landroid/view/WindowManager;
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@a5
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

    #@a7
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@a9
    move-object v1, p1

    #@aa
    move-object v3, p0

    #@ab
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;-><init>(Landroid/content/Context;Landroid/view/ViewManager;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V

    #@ae
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@b0
    .line 385
    new-instance v0, Landroid/content/Intent;

    #@b2
    const-string v1, "android.intent.action.USER_PRESENT"

    #@b4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b7
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@b9
    .line 386
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@bb
    const/high16 v1, 0x2800

    #@bd
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c0
    .line 389
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@c2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c5
    move-result-object v6

    #@c6
    .line 390
    .local v6, cr:Landroid/content/ContentResolver;
    const-string v0, "show_status_bar_lock"

    #@c8
    invoke-static {v6, v0, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@cb
    move-result v0

    #@cc
    if-ne v0, v9, :cond_126

    #@ce
    move v0, v9

    #@cf
    :goto_cf
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowLockIcon:Z

    #@d1
    .line 392
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@d3
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@d6
    move-result v0

    #@d7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mScreenOn:Z

    #@d9
    .line 394
    new-instance v0, Landroid/media/SoundPool;

    #@db
    invoke-direct {v0, v9, v9, v10}, Landroid/media/SoundPool;-><init>(III)V

    #@de
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@e0
    .line 395
    const-string v0, "lock_sound"

    #@e2
    invoke-static {v6, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@e5
    move-result-object v8

    #@e6
    .line 396
    .local v8, soundPath:Ljava/lang/String;
    if-eqz v8, :cond_f0

    #@e8
    .line 397
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@ea
    invoke-virtual {v0, v8, v9}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@ed
    move-result v0

    #@ee
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundId:I

    #@f0
    .line 399
    :cond_f0
    if-eqz v8, :cond_f6

    #@f2
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundId:I

    #@f4
    if-nez v0, :cond_f6

    #@f6
    .line 402
    :cond_f6
    const-string v0, "unlock_sound"

    #@f8
    invoke-static {v6, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v8

    #@fc
    .line 403
    if-eqz v8, :cond_106

    #@fe
    .line 404
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@100
    invoke-virtual {v0, v8, v9}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@103
    move-result v0

    #@104
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUnlockSoundId:I

    #@106
    .line 406
    :cond_106
    if-eqz v8, :cond_10c

    #@108
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUnlockSoundId:I

    #@10a
    if-nez v0, :cond_10c

    #@10c
    .line 409
    :cond_10c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10f
    move-result-object v0

    #@110
    const v1, 0x10e0005

    #@113
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@116
    move-result v7

    #@117
    .line 411
    .local v7, lockSoundDefaultAttenuation:I
    const-wide/high16 v0, 0x4024

    #@119
    int-to-float v3, v7

    #@11a
    const/high16 v4, 0x41a0

    #@11c
    div-float/2addr v3, v4

    #@11d
    float-to-double v3, v3

    #@11e
    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->pow(DD)D

    #@121
    move-result-wide v0

    #@122
    double-to-float v0, v0

    #@123
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundVolume:F

    #@125
    .line 412
    return-void

    #@126
    .end local v7           #lockSoundDefaultAttenuation:I
    .end local v8           #soundPath:Ljava/lang/String;
    :cond_126
    move v0, v10

    #@127
    .line 390
    goto :goto_cf
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->resetStateLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1002(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleTimeout(I)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleShow()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleHide()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleReset()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleVerifyUnlock()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleNotifyScreenOff()V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleNotifyScreenOn(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleWakeWhenReady(I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleKeyguardDone(Z)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mScreenOn:Z

    #@2
    return v0
.end method

.method static synthetic access$2000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleKeyguardDoneDrawing()V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleSetHidden(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->doKeyguardLocked()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Landroid/content/Intent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@2
    return v0
.end method

.method private adjustStatusBarLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1165
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@3
    if-nez v2, :cond_11

    #@5
    .line 1166
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@7
    const-string v3, "statusbar"

    #@9
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/app/StatusBarManager;

    #@f
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@11
    .line 1169
    :cond_11
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@13
    if-nez v2, :cond_1d

    #@15
    .line 1170
    const-string v2, "KeyguardViewMediator"

    #@17
    const-string v3, "Could not get status bar manager"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1214
    :goto_1c
    return-void

    #@1d
    .line 1172
    :cond_1d
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowLockIcon:Z

    #@1f
    if-eqz v2, :cond_45

    #@21
    .line 1174
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@23
    if-eqz v2, :cond_62

    #@25
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isSecure()Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_62

    #@2b
    .line 1175
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowingLockIcon:Z

    #@2d
    if-nez v2, :cond_45

    #@2f
    .line 1176
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@31
    const v3, 0x1040530

    #@34
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .line 1178
    .local v0, contentDescription:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@3a
    const-string v3, "secure"

    #@3c
    const v4, 0x1080561

    #@3f
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    #@42
    .line 1181
    const/4 v2, 0x1

    #@43
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowingLockIcon:Z

    #@45
    .line 1193
    .end local v0           #contentDescription:Ljava/lang/String;
    :cond_45
    :goto_45
    const/4 v1, 0x0

    #@46
    .line 1194
    .local v1, flags:I
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@48
    if-eqz v2, :cond_5c

    #@4a
    .line 1196
    const/high16 v2, 0x100

    #@4c
    or-int/2addr v1, v2

    #@4d
    .line 1197
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isSecure()Z

    #@50
    move-result v2

    #@51
    if-nez v2, :cond_70

    #@53
    .line 1201
    :goto_53
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isSecure()Z

    #@56
    move-result v2

    #@57
    if-eqz v2, :cond_5c

    #@59
    .line 1203
    const/high16 v2, 0x8

    #@5b
    or-int/2addr v1, v2

    #@5c
    .line 1212
    :cond_5c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@5e
    invoke-virtual {v2, v1}, Landroid/app/StatusBarManager;->disable(I)V

    #@61
    goto :goto_1c

    #@62
    .line 1184
    .end local v1           #flags:I
    :cond_62
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowingLockIcon:Z

    #@64
    if-eqz v2, :cond_45

    #@66
    .line 1185
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@68
    const-string v3, "secure"

    #@6a
    invoke-virtual {v2, v3}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V

    #@6d
    .line 1186
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowingLockIcon:Z

    #@6f
    goto :goto_45

    #@70
    .line 1199
    .restart local v1       #flags:I
    :cond_70
    const/high16 v2, 0x1

    #@72
    or-int/2addr v1, v2

    #@73
    goto :goto_53
.end method

.method private adjustUserActivityLocked()V
    .registers 3

    #@0
    .prologue
    .line 1155
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHidden:Z

    #@6
    if-eqz v1, :cond_13

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    .line 1158
    .local v0, enabled:Z
    :goto_9
    if-nez v0, :cond_12

    #@b
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mScreenOn:Z

    #@d
    if-eqz v1, :cond_12

    #@f
    .line 1160
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->pokeWakelock()V

    #@12
    .line 1162
    :cond_12
    return-void

    #@13
    .line 1155
    .end local v0           #enabled:Z
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_9
.end method

.method private doKeyguardLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 668
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@4
    if-nez v6, :cond_7

    #@6
    .line 712
    :cond_6
    :goto_6
    return-void

    #@7
    .line 684
    :cond_7
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@9
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->isShowing()Z

    #@c
    move-result v6

    #@d
    if-nez v6, :cond_6

    #@f
    .line 690
    const-string v6, "keyguard.no_require_sim"

    #@11
    invoke-static {v6, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@14
    move-result v6

    #@15
    if-nez v6, :cond_47

    #@17
    move v2, v4

    #@18
    .line 692
    .local v2, requireSim:Z
    :goto_18
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@1a
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@1d
    move-result v1

    #@1e
    .line 693
    .local v1, provisioned:Z
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@20
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@23
    move-result-object v3

    #@24
    .line 694
    .local v3, state:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-virtual {v3}, Lcom/android/internal/telephony/IccCardConstants$State;->isPinLocked()Z

    #@27
    move-result v6

    #@28
    if-nez v6, :cond_34

    #@2a
    sget-object v6, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2c
    if-eq v3, v6, :cond_32

    #@2e
    sget-object v6, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@30
    if-ne v3, v6, :cond_49

    #@32
    :cond_32
    if-eqz v2, :cond_49

    #@34
    :cond_34
    move v0, v4

    #@35
    .line 699
    .local v0, lockedOrMissing:Z
    :goto_35
    if-nez v0, :cond_39

    #@37
    if-eqz v1, :cond_6

    #@39
    .line 705
    :cond_39
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3b
    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled()Z

    #@3e
    move-result v4

    #@3f
    if-eqz v4, :cond_43

    #@41
    if-eqz v0, :cond_6

    #@43
    .line 711
    :cond_43
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->showLocked()V

    #@46
    goto :goto_6

    #@47
    .end local v0           #lockedOrMissing:Z
    .end local v1           #provisioned:Z
    .end local v2           #requireSim:Z
    .end local v3           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_47
    move v2, v5

    #@48
    .line 690
    goto :goto_18

    #@49
    .restart local v1       #provisioned:Z
    .restart local v2       #requireSim:Z
    .restart local v3       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_49
    move v0, v5

    #@4a
    .line 694
    goto :goto_35
.end method

.method private handleHide()V
    .registers 3

    #@0
    .prologue
    .line 1132
    monitor-enter p0

    #@1
    .line 1134
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@3
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 1135
    const-string v0, "KeyguardViewMediator"

    #@b
    const-string v1, "attempt to hide the keyguard while waking, ignored"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1136
    monitor-exit p0

    #@11
    .line 1150
    :goto_11
    return-void

    #@12
    .line 1141
    :cond_12
    sget-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@14
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_20

    #@1c
    .line 1142
    const/4 v0, 0x0

    #@1d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->playSounds(Z)V

    #@20
    .line 1145
    :cond_20
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@22
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->hide()V

    #@25
    .line 1146
    const/4 v0, 0x0

    #@26
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@28
    .line 1147
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustUserActivityLocked()V

    #@2b
    .line 1148
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@2e
    .line 1149
    monitor-exit p0

    #@2f
    goto :goto_11

    #@30
    :catchall_30
    move-exception v0

    #@31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_30

    #@32
    throw v0
.end method

.method private handleKeyguardDone(Z)V
    .registers 6
    .parameter "wakeup"

    #@0
    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->handleHide()V

    #@3
    .line 1030
    if-eqz p1, :cond_e

    #@5
    .line 1031
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a
    move-result-wide v2

    #@b
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->wakeUp(J)V

    #@e
    .line 1033
    :cond_e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@13
    .line 1035
    new-instance v0, Landroid/os/UserHandle;

    #@15
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@17
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@1a
    move-result v1

    #@1b
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@1e
    .line 1036
    .local v0, currentUser:Landroid/os/UserHandle;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@20
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@22
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@25
    .line 1037
    return-void
.end method

.method private handleKeyguardDoneDrawing()V
    .registers 3

    #@0
    .prologue
    .line 1044
    monitor-enter p0

    #@1
    .line 1046
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@3
    if-eqz v0, :cond_12

    #@5
    .line 1048
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@8
    .line 1049
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@b
    .line 1054
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@d
    const/16 v1, 0xa

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@12
    .line 1056
    :cond_12
    monitor-exit p0

    #@13
    .line 1057
    return-void

    #@14
    .line 1056
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method private handleNotifyScreenOff()V
    .registers 2

    #@0
    .prologue
    .line 1273
    monitor-enter p0

    #@1
    .line 1275
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->onScreenTurnedOff()V

    #@6
    .line 1276
    monitor-exit p0

    #@7
    .line 1277
    return-void

    #@8
    .line 1276
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private handleNotifyScreenOn(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V
    .registers 3
    .parameter "showListener"

    #@0
    .prologue
    .line 1284
    monitor-enter p0

    #@1
    .line 1286
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@3
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V

    #@6
    .line 1287
    monitor-exit p0

    #@7
    .line 1288
    return-void

    #@8
    .line 1287
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private handleReset()V
    .registers 2

    #@0
    .prologue
    .line 1250
    monitor-enter p0

    #@1
    .line 1252
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->reset()V

    #@6
    .line 1253
    monitor-exit p0

    #@7
    .line 1254
    return-void

    #@8
    .line 1253
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private handleSetHidden(Z)V
    .registers 3
    .parameter "isHidden"

    #@0
    .prologue
    .line 633
    monitor-enter p0

    #@1
    .line 634
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHidden:Z

    #@3
    if-eq v0, p1, :cond_d

    #@5
    .line 635
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHidden:Z

    #@7
    .line 636
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustUserActivityLocked()V

    #@a
    .line 637
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@d
    .line 639
    :cond_d
    monitor-exit p0

    #@e
    .line 640
    return-void

    #@f
    .line 639
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method private handleShow()V
    .registers 3

    #@0
    .prologue
    .line 1107
    monitor-enter p0

    #@1
    .line 1109
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSystemReady:Z

    #@3
    if-nez v0, :cond_7

    #@5
    monitor-exit p0

    #@6
    .line 1125
    :goto_6
    return-void

    #@7
    .line 1111
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->show()V

    #@c
    .line 1112
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@f
    .line 1113
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustUserActivityLocked()V

    #@12
    .line 1114
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->adjustStatusBarLocked()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_29

    #@15
    .line 1116
    :try_start_15
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@18
    move-result-object v0

    #@19
    const-string v1, "lock"

    #@1b
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->closeSystemDialogs(Ljava/lang/String;)V
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1e} :catch_2c

    #@1e
    .line 1121
    :goto_1e
    const/4 v0, 0x1

    #@1f
    :try_start_1f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->playSounds(Z)V

    #@22
    .line 1123
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@24
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@27
    .line 1124
    monitor-exit p0

    #@28
    goto :goto_6

    #@29
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_1f .. :try_end_2b} :catchall_29

    #@2b
    throw v0

    #@2c
    .line 1117
    :catch_2c
    move-exception v0

    #@2d
    goto :goto_1e
.end method

.method private handleTimeout(I)V
    .registers 3
    .parameter "seq"

    #@0
    .prologue
    .line 1066
    monitor-enter p0

    #@1
    .line 1068
    :try_start_1
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakelockSequence:I

    #@3
    if-ne p1, v0, :cond_a

    #@5
    .line 1069
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@a
    .line 1071
    :cond_a
    monitor-exit p0

    #@b
    .line 1072
    return-void

    #@c
    .line 1071
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method private handleVerifyUnlock()V
    .registers 2

    #@0
    .prologue
    .line 1261
    monitor-enter p0

    #@1
    .line 1263
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->verifyUnlock()V

    #@6
    .line 1264
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@9
    .line 1265
    monitor-exit p0

    #@a
    .line 1266
    return-void

    #@b
    .line 1265
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method private handleWakeWhenReady(I)V
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 1222
    monitor-enter p0

    #@1
    .line 1227
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;

    #@3
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager;->wakeWhenReadyTq(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_13

    #@9
    .line 1229
    const-string v0, "KeyguardViewMediator"

    #@b
    const-string v1, "mKeyguardViewManager.wakeWhenReadyTq did not poke wake lock, so poke it ourselves"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1230
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->pokeWakelock()V

    #@13
    .line 1237
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@18
    .line 1239
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_27

    #@20
    .line 1240
    const-string v0, "KeyguardViewMediator"

    #@22
    const-string v1, "mWakeLock not held in mKeyguardViewManager.wakeWhenReadyTq"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1242
    :cond_27
    monitor-exit p0

    #@28
    .line 1243
    return-void

    #@29
    .line 1242
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_1 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method private hideLocked()V
    .registers 4

    #@0
    .prologue
    .line 794
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x3

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 795
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 796
    return-void
.end method

.method private isWakeKeyWhenKeyguardShowing(IZ)Z
    .registers 3
    .parameter "keyCode"
    .parameter "isDocked"

    #@0
    .prologue
    .line 855
    sparse-switch p1, :sswitch_data_8

    #@3
    .line 877
    const/4 p2, 0x1

    #@4
    .end local p2
    :goto_4
    :sswitch_4
    return p2

    #@5
    .line 875
    .restart local p2
    :sswitch_5
    const/4 p2, 0x0

    #@6
    goto :goto_4

    #@7
    .line 855
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x18 -> :sswitch_4
        0x19 -> :sswitch_4
        0x1b -> :sswitch_5
        0x4f -> :sswitch_5
        0x55 -> :sswitch_5
        0x56 -> :sswitch_5
        0x57 -> :sswitch_5
        0x58 -> :sswitch_5
        0x59 -> :sswitch_5
        0x5a -> :sswitch_5
        0x5b -> :sswitch_5
        0x7e -> :sswitch_5
        0x7f -> :sswitch_5
        0x82 -> :sswitch_5
        0xa4 -> :sswitch_4
    .end sparse-switch
.end method

.method private notifyScreenOffLocked()V
    .registers 3

    #@0
    .prologue
    .line 741
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x6

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 742
    return-void
.end method

.method private notifyScreenOnLocked(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V
    .registers 5
    .parameter "showListener"

    #@0
    .prologue
    .line 751
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x7

    #@3
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 752
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 753
    return-void
.end method

.method private playSounds(Z)V
    .registers 10
    .parameter "locked"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1077
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 1078
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@8
    .line 1100
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1082
    :cond_9
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v7

    #@f
    .line 1083
    .local v7, cr:Landroid/content/ContentResolver;
    const-string v0, "lockscreen_sounds_enabled"

    #@11
    invoke-static {v7, v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    if-ne v0, v4, :cond_8

    #@17
    .line 1084
    if-eqz p1, :cond_57

    #@19
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundId:I

    #@1b
    .line 1087
    .local v1, whichSound:I
    :goto_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@1d
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundStreamId:I

    #@1f
    invoke-virtual {v0, v2}, Landroid/media/SoundPool;->stop(I)V

    #@22
    .line 1089
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@24
    if-nez v0, :cond_3e

    #@26
    .line 1090
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@28
    const-string v2, "audio"

    #@2a
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    check-cast v0, Landroid/media/AudioManager;

    #@30
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@32
    .line 1091
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@34
    if-eqz v0, :cond_8

    #@36
    .line 1092
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@38
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterStreamType()I

    #@3b
    move-result v0

    #@3c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mMasterStreamType:I

    #@3e
    .line 1095
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@40
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mMasterStreamType:I

    #@42
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->isStreamMute(I)Z

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_8

    #@48
    .line 1097
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@4a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundVolume:F

    #@4c
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundVolume:F

    #@4e
    const/high16 v6, 0x3f80

    #@50
    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    #@53
    move-result v0

    #@54
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockSoundStreamId:I

    #@56
    goto :goto_8

    #@57
    .line 1084
    .end local v1           #whichSound:I
    :cond_57
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUnlockSoundId:I

    #@59
    goto :goto_1b
.end method

.method private resetStateLocked()V
    .registers 4

    #@0
    .prologue
    .line 720
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x4

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 721
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 722
    return-void
.end method

.method private showLocked()V
    .registers 4

    #@0
    .prologue
    .line 783
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 784
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@7
    const/4 v2, 0x2

    #@8
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 785
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 786
    return-void
.end method

.method private verifyUnlockLocked()V
    .registers 3

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x5

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 731
    return-void
.end method

.method private wakeWhenReadyLocked(I)V
    .registers 6
    .parameter "keyCode"

    #@0
    .prologue
    .line 770
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 772
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@7
    const/16 v2, 0x8

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 773
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 774
    return-void
.end method


# virtual methods
.method public doKeyguardTimeout()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xd

    #@2
    .line 647
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 648
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 649
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@f
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 650
    return-void
.end method

.method public isInputRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 658
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@2
    if-nez v0, :cond_10

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@6
    if-nez v0, :cond_10

    #@8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public isSecure()Z
    .registers 2

    #@0
    .prologue
    .line 799
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mKeyguardViewProperties:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;

    #@2
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewProperties;->isSecure()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 609
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@2
    return v0
.end method

.method public isShowingAndNotHidden()Z
    .registers 2

    #@0
    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHidden:Z

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public keyguardDone(Z)V
    .registers 3
    .parameter "authenticated"

    #@0
    .prologue
    .line 929
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@4
    .line 930
    return-void
.end method

.method public keyguardDone(ZZ)V
    .registers 8
    .parameter "authenticated"
    .parameter "wakeup"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 933
    monitor-enter p0

    #@3
    .line 934
    const v3, 0x11170

    #@6
    const/4 v4, 0x2

    #@7
    :try_start_7
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(II)I

    #@a
    .line 936
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@c
    const/16 v4, 0x9

    #@e
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 937
    .local v0, msg:Landroid/os/Message;
    if-eqz p2, :cond_38

    #@14
    :goto_14
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@16
    .line 938
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@18
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1b
    .line 940
    if-eqz p1, :cond_22

    #@1d
    .line 941
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@1f
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->clearFailedAttempts()V

    #@22
    .line 944
    :cond_22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@24
    if-eqz v1, :cond_36

    #@26
    .line 945
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@28
    invoke-interface {v1, p1}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@2b
    .line 946
    const/4 v1, 0x0

    #@2c
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@2e
    .line 948
    if-eqz p1, :cond_36

    #@30
    .line 951
    const/4 v1, 0x1

    #@31
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@33
    .line 952
    const/4 v1, 0x0

    #@34
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@36
    .line 955
    :cond_36
    monitor-exit p0

    #@37
    .line 956
    return-void

    #@38
    :cond_38
    move v1, v2

    #@39
    .line 937
    goto :goto_14

    #@3a
    .line 955
    .end local v0           #msg:Landroid/os/Message;
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit p0
    :try_end_3c
    .catchall {:try_start_7 .. :try_end_3c} :catchall_3a

    #@3c
    throw v1
.end method

.method public keyguardDoneDrawing()V
    .registers 3

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v1, 0xa

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@7
    .line 965
    return-void
.end method

.method public onScreenTurnedOff(I)V
    .registers 23
    .parameter "why"

    #@0
    .prologue
    .line 433
    monitor-enter p0

    #@1
    .line 434
    const/16 v18, 0x0

    #@3
    :try_start_3
    move/from16 v0, v18

    #@5
    move-object/from16 v1, p0

    #@7
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mScreenOn:Z

    #@9
    .line 440
    move-object/from16 v0, p0

    #@b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@d
    move-object/from16 v18, v0

    #@f
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->getPowerButtonInstantlyLocks()Z

    #@12
    move-result v18

    #@13
    if-nez v18, :cond_21

    #@15
    move-object/from16 v0, p0

    #@17
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@19
    move-object/from16 v18, v0

    #@1b
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@1e
    move-result v18

    #@1f
    if-nez v18, :cond_4a

    #@21
    :cond_21
    const/4 v10, 0x1

    #@22
    .line 443
    .local v10, lockImmediately:Z
    :goto_22
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@26
    move-object/from16 v18, v0

    #@28
    if-eqz v18, :cond_4c

    #@2a
    .line 445
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@2e
    move-object/from16 v18, v0

    #@30
    const/16 v19, 0x0

    #@32
    invoke-interface/range {v18 .. v19}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@35
    .line 446
    const/16 v18, 0x0

    #@37
    move-object/from16 v0, v18

    #@39
    move-object/from16 v1, p0

    #@3b
    iput-object v0, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@3d
    .line 447
    move-object/from16 v0, p0

    #@3f
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@41
    move/from16 v18, v0

    #@43
    if-nez v18, :cond_48

    #@45
    .line 448
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->hideLocked()V

    #@48
    .line 504
    :cond_48
    :goto_48
    monitor-exit p0

    #@49
    .line 505
    return-void

    #@4a
    .line 440
    .end local v10           #lockImmediately:Z
    :cond_4a
    const/4 v10, 0x0

    #@4b
    goto :goto_22

    #@4c
    .line 450
    .restart local v10       #lockImmediately:Z
    :cond_4c
    move-object/from16 v0, p0

    #@4e
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@50
    move/from16 v18, v0

    #@52
    if-eqz v18, :cond_5e

    #@54
    .line 451
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->notifyScreenOffLocked()V

    #@57
    .line 452
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->resetStateLocked()V

    #@5a
    goto :goto_48

    #@5b
    .line 504
    .end local v10           #lockImmediately:Z
    :catchall_5b
    move-exception v18

    #@5c
    monitor-exit p0
    :try_end_5d
    .catchall {:try_start_3 .. :try_end_5d} :catchall_5b

    #@5d
    throw v18

    #@5e
    .line 453
    .restart local v10       #lockImmediately:Z
    :cond_5e
    const/16 v18, 0x3

    #@60
    move/from16 v0, p1

    #@62
    move/from16 v1, v18

    #@64
    if-eq v0, v1, :cond_70

    #@66
    const/16 v18, 0x2

    #@68
    move/from16 v0, p1

    #@6a
    move/from16 v1, v18

    #@6c
    if-ne v0, v1, :cond_118

    #@6e
    if-nez v10, :cond_118

    #@70
    .line 460
    :cond_70
    :try_start_70
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@74
    move-object/from16 v18, v0

    #@76
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@79
    move-result-object v4

    #@7a
    .line 463
    .local v4, cr:Landroid/content/ContentResolver;
    const-string v18, "screen_off_timeout"

    #@7c
    const/16 v19, 0x7530

    #@7e
    move-object/from16 v0, v18

    #@80
    move/from16 v1, v19

    #@82
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@85
    move-result v18

    #@86
    move/from16 v0, v18

    #@88
    int-to-long v5, v0

    #@89
    .line 467
    .local v5, displayTimeout:J
    const-string v18, "lock_screen_lock_after_timeout"

    #@8b
    const/16 v19, 0x1388

    #@8d
    move-object/from16 v0, v18

    #@8f
    move/from16 v1, v19

    #@91
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@94
    move-result v18

    #@95
    move/from16 v0, v18

    #@97
    int-to-long v8, v0

    #@98
    .line 472
    .local v8, lockAfterTimeout:J
    move-object/from16 v0, p0

    #@9a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@9c
    move-object/from16 v18, v0

    #@9e
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@a1
    move-result-object v18

    #@a2
    const/16 v19, 0x0

    #@a4
    invoke-virtual/range {v18 .. v19}, Landroid/app/admin/DevicePolicyManager;->getMaximumTimeToLock(Landroid/content/ComponentName;)J

    #@a7
    move-result-wide v11

    #@a8
    .line 476
    .local v11, policyTimeout:J
    const-wide/16 v18, 0x0

    #@aa
    cmp-long v18, v11, v18

    #@ac
    if-lez v18, :cond_d1

    #@ae
    .line 478
    const-wide/16 v18, 0x0

    #@b0
    move-wide/from16 v0, v18

    #@b2
    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->max(JJ)J

    #@b5
    move-result-wide v5

    #@b6
    .line 479
    sub-long v18, v11, v5

    #@b8
    move-wide/from16 v0, v18

    #@ba
    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    #@bd
    move-result-wide v14

    #@be
    .line 484
    .local v14, timeout:J
    :goto_be
    const-wide/16 v18, 0x0

    #@c0
    cmp-long v18, v14, v18

    #@c2
    if-gtz v18, :cond_d3

    #@c4
    .line 486
    const/16 v18, 0x1

    #@c6
    move/from16 v0, v18

    #@c8
    move-object/from16 v1, p0

    #@ca
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@cc
    .line 487
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->doKeyguardLocked()V

    #@cf
    goto/16 :goto_48

    #@d1
    .line 481
    .end local v14           #timeout:J
    :cond_d1
    move-wide v14, v8

    #@d2
    .restart local v14       #timeout:J
    goto :goto_be

    #@d3
    .line 490
    :cond_d3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d6
    move-result-wide v18

    #@d7
    add-long v16, v18, v14

    #@d9
    .line 491
    .local v16, when:J
    new-instance v7, Landroid/content/Intent;

    #@db
    const-string v18, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    #@dd
    move-object/from16 v0, v18

    #@df
    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e2
    .line 492
    .local v7, intent:Landroid/content/Intent;
    const-string v18, "seq"

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@e8
    move/from16 v19, v0

    #@ea
    move-object/from16 v0, v18

    #@ec
    move/from16 v1, v19

    #@ee
    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@f1
    .line 493
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@f5
    move-object/from16 v18, v0

    #@f7
    const/16 v19, 0x0

    #@f9
    const/high16 v20, 0x1000

    #@fb
    move-object/from16 v0, v18

    #@fd
    move/from16 v1, v19

    #@ff
    move/from16 v2, v20

    #@101
    invoke-static {v0, v1, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@104
    move-result-object v13

    #@105
    .line 495
    .local v13, sender:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@107
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    #@109
    move-object/from16 v18, v0

    #@10b
    const/16 v19, 0x2

    #@10d
    move-object/from16 v0, v18

    #@10f
    move/from16 v1, v19

    #@111
    move-wide/from16 v2, v16

    #@113
    invoke-virtual {v0, v1, v2, v3, v13}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@116
    goto/16 :goto_48

    #@118
    .line 499
    .end local v4           #cr:Landroid/content/ContentResolver;
    .end local v5           #displayTimeout:J
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #lockAfterTimeout:J
    .end local v11           #policyTimeout:J
    .end local v13           #sender:Landroid/app/PendingIntent;
    .end local v14           #timeout:J
    .end local v16           #when:J
    :cond_118
    const/16 v18, 0x4

    #@11a
    move/from16 v0, p1

    #@11c
    move/from16 v1, v18

    #@11e
    if-eq v0, v1, :cond_48

    #@120
    .line 502
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->doKeyguardLocked()V
    :try_end_123
    .catchall {:try_start_70 .. :try_end_123} :catchall_5b

    #@123
    goto/16 :goto_48
.end method

.method public onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V
    .registers 3
    .parameter "showListener"

    #@0
    .prologue
    .line 511
    monitor-enter p0

    #@1
    .line 512
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mScreenOn:Z

    #@4
    .line 513
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@6
    add-int/lit8 v0, v0, 0x1

    #@8
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@a
    .line 515
    if-eqz p1, :cond_f

    #@c
    .line 516
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->notifyScreenOnLocked(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V

    #@f
    .line 518
    :cond_f
    monitor-exit p0

    #@10
    .line 519
    return-void

    #@11
    .line 518
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_2 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public onSystemReady()V
    .registers 3

    #@0
    .prologue
    .line 418
    monitor-enter p0

    #@1
    .line 420
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mSystemReady:Z

    #@4
    .line 421
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;)V

    #@b
    .line 422
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->doKeyguardLocked()V

    #@e
    .line 423
    monitor-exit p0

    #@f
    .line 424
    return-void

    #@10
    .line 423
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public onWakeKeyWhenKeyguardShowingTq(IZ)Z
    .registers 4
    .parameter "keyCode"
    .parameter "isDocked"

    #@0
    .prologue
    .line 836
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isWakeKeyWhenKeyguardShowing(IZ)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 840
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->wakeWhenReadyLocked(I)V

    #@9
    .line 841
    const/4 v0, 0x1

    #@a
    .line 843
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onWakeMotionWhenKeyguardShowingTq()Z
    .registers 2

    #@0
    .prologue
    .line 898
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->wakeWhenReadyLocked(I)V

    #@4
    .line 899
    const/4 v0, 0x1

    #@5
    return v0
.end method

.method public pokeWakelock()V
    .registers 2

    #@0
    .prologue
    .line 908
    const/16 v0, 0x2710

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->pokeWakelock(I)V

    #@5
    .line 909
    return-void
.end method

.method public pokeWakelock(I)V
    .registers 7
    .parameter "holdMs"

    #@0
    .prologue
    .line 913
    monitor-enter p0

    #@1
    .line 915
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@6
    .line 916
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@8
    const/4 v2, 0x1

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 917
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakelockSequence:I

    #@e
    add-int/lit8 v1, v1, 0x1

    #@10
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakelockSequence:I

    #@12
    .line 918
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@14
    const/4 v2, 0x1

    #@15
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWakelockSequence:I

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@1b
    move-result-object v0

    #@1c
    .line 919
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@1e
    int-to-long v2, p1

    #@1f
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@22
    .line 920
    monitor-exit p0

    #@23
    .line 921
    return-void

    #@24
    .line 920
    .end local v0           #msg:Landroid/os/Message;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public setHidden(Z)V
    .registers 7
    .parameter "isHidden"

    #@0
    .prologue
    const/16 v4, 0xc

    #@2
    const/4 v2, 0x0

    #@3
    .line 624
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@5
    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@8
    .line 625
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@a
    if-eqz p1, :cond_17

    #@c
    const/4 v1, 0x1

    #@d
    :goto_d
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 626
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@13
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 627
    return-void

    #@17
    .end local v0           #msg:Landroid/os/Message;
    :cond_17
    move v1, v2

    #@18
    .line 625
    goto :goto_d
.end method

.method public setKeyguardEnabled(Z)V
    .registers 7
    .parameter "enabled"

    #@0
    .prologue
    .line 527
    monitor-enter p0

    #@1
    .line 531
    :try_start_1
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@3
    .line 533
    if-nez p1, :cond_1a

    #@5
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mShowing:Z

    #@7
    if-eqz v1, :cond_1a

    #@9
    .line 534
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 538
    monitor-exit p0

    #@e
    .line 577
    :goto_e
    return-void

    #@f
    .line 544
    :cond_f
    const/4 v1, 0x1

    #@10
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@12
    .line 545
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->hideLocked()V

    #@15
    .line 576
    :cond_15
    :goto_15
    monitor-exit p0

    #@16
    goto :goto_e

    #@17
    :catchall_17
    move-exception v1

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_17

    #@19
    throw v1

    #@1a
    .line 546
    :cond_1a
    if-eqz p1, :cond_15

    #@1c
    :try_start_1c
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@1e
    if-eqz v1, :cond_15

    #@20
    .line 550
    const/4 v1, 0x0

    #@21
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@23
    .line 552
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@25
    if-eqz v1, :cond_34

    #@27
    .line 554
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@29
    const/4 v2, 0x0

    #@2a
    invoke-interface {v1, v2}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@2d
    .line 555
    const/4 v1, 0x0

    #@2e
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@30
    .line 556
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->resetStateLocked()V

    #@33
    goto :goto_15

    #@34
    .line 558
    :cond_34
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->showLocked()V

    #@37
    .line 563
    const/4 v1, 0x1

    #@38
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@3a
    .line 564
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@3c
    const/16 v2, 0xa

    #@3e
    const-wide/16 v3, 0x7d0

    #@40
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@43
    .line 566
    :goto_43
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z
    :try_end_45
    .catchall {:try_start_1c .. :try_end_45} :catchall_17

    #@45
    if-eqz v1, :cond_15

    #@47
    .line 568
    :try_start_47
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_17
    .catch Ljava/lang/InterruptedException; {:try_start_47 .. :try_end_4a} :catch_4b

    #@4a
    goto :goto_43

    #@4b
    .line 569
    :catch_4b
    move-exception v0

    #@4c
    .line 570
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_4c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_53
    .catchall {:try_start_4c .. :try_end_53} :catchall_17

    #@53
    goto :goto_43
.end method

.method public verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 583
    monitor-enter p0

    #@1
    .line 585
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_f

    #@9
    .line 588
    const/4 v0, 0x0

    #@a
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@d
    .line 602
    :goto_d
    monitor-exit p0

    #@e
    .line 603
    return-void

    #@f
    .line 589
    :cond_f
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExternallyEnabled:Z

    #@11
    if-eqz v0, :cond_22

    #@13
    .line 593
    const-string v0, "KeyguardViewMediator"

    #@15
    const-string v1, "verifyUnlock called when not externally disabled"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 594
    const/4 v0, 0x0

    #@1b
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@1e
    goto :goto_d

    #@1f
    .line 602
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_1f

    #@21
    throw v0

    #@22
    .line 595
    :cond_22
    :try_start_22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@24
    if-eqz v0, :cond_2b

    #@26
    .line 597
    const/4 v0, 0x0

    #@27
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@2a
    goto :goto_d

    #@2b
    .line 599
    :cond_2b
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@2d
    .line 600
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->verifyUnlockLocked()V
    :try_end_30
    .catchall {:try_start_22 .. :try_end_30} :catchall_1f

    #@30
    goto :goto_d
.end method
