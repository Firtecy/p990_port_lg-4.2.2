.class Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;
.super Landroid/content/BroadcastReceiver;
.source "NextiKeyguardSelectorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->registerReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 740
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 743
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 745
    .local v0, action:Ljava/lang/String;
    const-string v3, "jp.co.nttdocomo.carriermail.APP_LINK_RECEIVED_MESSAGE_LOCAL"

    #@7
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_19

    #@d
    .line 747
    const-string v3, "spcnt"

    #@f
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12
    move-result v1

    #@13
    .line 748
    .local v1, cnt:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@15
    invoke-static {v3, v1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1000(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V

    #@18
    .line 778
    .end local v1           #cnt:I
    :cond_18
    :goto_18
    return-void

    #@19
    .line 751
    :cond_19
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_38

    #@21
    .line 752
    const-string v3, "SecuritySelectorView"

    #@23
    const-string v4, "KeyguardSelectorView.onReceive(): BOOT_COMPLETED"

    #@25
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 753
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2a
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1100(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@2d
    .line 755
    new-instance v2, Landroid/content/Intent;

    #@2f
    const-string v3, "com.android.internal.policy.impl.CARRIERMAIL_COUNT_UPDATE"

    #@31
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34
    .line 757
    .local v2, intent2:Landroid/content/Intent;
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@37
    goto :goto_18

    #@38
    .line 760
    .end local v2           #intent2:Landroid/content/Intent;
    :cond_38
    const-string v3, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@3a
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_6f

    #@40
    .line 761
    const-string v3, "SecuritySelectorView"

    #@42
    const-string v4, "KeyguardSelectorView.onReceive(): KEYGUARD_UPDATE_LOCAL"

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 762
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@49
    const-string v3, "RemoteViews"

    #@4b
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@4e
    move-result-object v3

    #@4f
    check-cast v3, Landroid/widget/RemoteViews;

    #@51
    iput-object v3, v4, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mRemoteViews:Landroid/widget/RemoteViews;

    #@53
    .line 763
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@55
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5$1;

    #@57
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5$1;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;)V

    #@5a
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1202(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    #@5d
    .line 769
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@5f
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1400(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/os/Handler;

    #@62
    move-result-object v3

    #@63
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@65
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1200(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Ljava/lang/Runnable;

    #@68
    move-result-object v4

    #@69
    const-wide/16 v5, 0x0

    #@6b
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@6e
    goto :goto_18

    #@6f
    .line 772
    :cond_6f
    const-string v3, "com.nttdocomo.android.mascot.widget.LockScreenMascotWidget.ACTION_SCREEN_UNLOCK"

    #@71
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v3

    #@75
    if-eqz v3, :cond_18

    #@77
    .line 774
    const-string v3, "SecuritySelectorView"

    #@79
    const-string v4, "KeyguardSelectorView.onReceive(): LockScreenMascotWidget.ACTION_SCREEN_UNLOCK"

    #@7b
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 776
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@80
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$200(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@83
    move-result-object v3

    #@84
    invoke-interface {v3, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@87
    goto :goto_18
.end method
