.class public Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;
.source "MSimKeyguardSimPinView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$MSimCheckSimPin;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const-string v0, "MSimKeyguardSimPinView"

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 46
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method


# virtual methods
.method protected getSecurityMessageDisplay(I)Ljava/lang/CharSequence;
    .registers 7
    .parameter "resId"

    #@0
    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    const v1, 0x1040573

    #@7
    const/4 v2, 0x2

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    const/4 v3, 0x0

    #@b
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@d
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPinLockedSubscription()I

    #@14
    move-result v4

    #@15
    add-int/lit8 v4, v4, 0x1

    #@17
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v4

    #@1b
    aput-object v4, v2, v3

    #@1d
    const/4 v3, 0x1

    #@1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getContext()Landroid/content/Context;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@29
    move-result-object v4

    #@2a
    aput-object v4, v2, v3

    #@2c
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    return-object v0
.end method

.method public resetState()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@3
    const v1, 0x1040551

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@d
    .line 51
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@f
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    #@12
    .line 52
    return-void
.end method

.method protected verifyPasswordAndUnlock()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 93
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@3
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 94
    .local v0, entry:Ljava/lang/String;
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->TAG:Ljava/lang/String;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "verifyPasswordAndUnlock(): entry = "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 96
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x4

    #@28
    if-ge v1, v2, :cond_45

    #@2a
    .line 98
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@2c
    const v2, 0x1040559

    #@2f
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@32
    move-result-object v2

    #@33
    invoke-interface {v1, v2, v4}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@36
    .line 99
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@38
    const-string v2, ""

    #@3a
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@3d
    .line 100
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@3f
    const-wide/16 v2, 0x0

    #@41
    invoke-interface {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@44
    .line 135
    :cond_44
    :goto_44
    return-void

    #@45
    .line 104
    :cond_45
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getSimUnlockProgressDialog()Landroid/app/Dialog;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    #@4c
    .line 106
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->mSimCheckInProgress:Z

    #@4e
    if-nez v1, :cond_44

    #@50
    .line 107
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->mSimCheckInProgress:Z

    #@52
    .line 109
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->TAG:Ljava/lang/String;

    #@54
    const-string v2, "startCheckSimPin(), Multi SIM enabled"

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 110
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@5b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@5d
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@67
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPinLockedSubscription()I

    #@6e
    move-result v3

    #@6f
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;Ljava/lang/String;I)V

    #@72
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->start()V

    #@75
    goto :goto_44
.end method
