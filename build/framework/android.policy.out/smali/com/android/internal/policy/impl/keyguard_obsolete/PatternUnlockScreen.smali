.class Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;
.super Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;
.source "PatternUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$4;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final MIN_PATTERN_BEFORE_POKE_WAKELOCK:I = 0x2

.field private static final PATTERN_CLEAR_TIMEOUT_MS:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "UnlockScreen"

.field private static final UNLOCK_PATTERN_WAKE_INTERVAL_FIRST_DOTS_MS:I = 0x7d0

.field private static final UNLOCK_PATTERN_WAKE_INTERVAL_MS:I = 0x1b58


# instance fields
.field private mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private mCancelPatternRunnable:Ljava/lang/Runnable;

.field private mCountdownTimer:Landroid/os/CountDownTimer;

.field private mCreationOrientation:I

.field private mEnableFallback:Z

.field private mFailedPatternAttemptsSinceLastTimeout:I

.field private mForgotPatternButton:Landroid/widget/Button;

.field private final mForgotPatternClick:Landroid/view/View$OnClickListener;

.field private mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

.field private mLastPokeTime:J

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockPatternView:Lcom/android/internal/widget/LockPatternView;

.field private mTotalFailedPatternAttempts:I

.field private mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;I)V
    .registers 16
    .parameter "context"
    .parameter "configuration"
    .parameter "lockPatternUtils"
    .parameter "updateMonitor"
    .parameter "callback"
    .parameter "totalFailedAttempts"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 147
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;-><init>(Landroid/content/Context;)V

    #@6
    .line 59
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@8
    .line 60
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mTotalFailedPatternAttempts:I

    #@a
    .line 61
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    #@c
    .line 81
    const-wide/16 v0, -0x1b58

    #@e
    iput-wide v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLastPokeTime:J

    #@10
    .line 86
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$1;

    #@12
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)V

    #@15
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCancelPatternRunnable:Ljava/lang/Runnable;

    #@17
    .line 92
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$2;

    #@19
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)V

    #@1c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternClick:Landroid/view/View$OnClickListener;

    #@1e
    .line 148
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@20
    .line 149
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@22
    .line 150
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@24
    .line 151
    iput p6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mTotalFailedPatternAttempts:I

    #@26
    .line 152
    rem-int/lit8 v0, p6, 0x5

    #@28
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@2a
    .line 161
    iget v0, p2, Landroid/content/res/Configuration;->orientation:I

    #@2c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCreationOrientation:I

    #@2e
    .line 163
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@31
    move-result-object v6

    #@32
    .line 165
    .local v6, inflater:Landroid/view/LayoutInflater;
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCreationOrientation:I

    #@34
    const/4 v1, 0x2

    #@35
    if-eq v0, v1, :cond_b2

    #@37
    .line 166
    const-string v0, "UnlockScreen"

    #@39
    const-string v1, "portrait mode"

    #@3b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 167
    const v0, 0x109006f

    #@41
    invoke-virtual {v6, v0, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@44
    .line 173
    :goto_44
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@46
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@48
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@4a
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@4c
    move-object v1, p0

    #@4d
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;-><init>(Landroid/view/View;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Z)V

    #@50
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@52
    .line 176
    const v0, 0x102030a

    #@55
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Lcom/android/internal/widget/LockPatternView;

    #@5b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@5d
    .line 178
    const v0, 0x1020309

    #@60
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->findViewById(I)Landroid/view/View;

    #@63
    move-result-object v0

    #@64
    check-cast v0, Landroid/widget/Button;

    #@66
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternButton:Landroid/widget/Button;

    #@68
    .line 179
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternButton:Landroid/widget/Button;

    #@6a
    const v1, 0x1040335

    #@6d
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    #@70
    .line 180
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternButton:Landroid/widget/Button;

    #@72
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternClick:Landroid/view/View$OnClickListener;

    #@74
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@77
    .line 184
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@79
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->setDefaultTouchRecepient(Landroid/view/View;)V

    #@7c
    .line 186
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@7e
    invoke-virtual {v0, v7}, Lcom/android/internal/widget/LockPatternView;->setSaveEnabled(Z)V

    #@81
    .line 187
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@83
    invoke-virtual {v0, v7}, Lcom/android/internal/widget/LockPatternView;->setFocusable(Z)V

    #@86
    .line 188
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@88
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;

    #@8a
    invoke-direct {v1, p0, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$1;)V

    #@8d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    #@90
    .line 191
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@94
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled()Z

    #@97
    move-result v0

    #@98
    if-nez v0, :cond_c0

    #@9a
    move v0, v5

    #@9b
    :goto_9b
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/LockPatternView;->setInStealthMode(Z)V

    #@9e
    .line 194
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@a0
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@a2
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    #@a5
    move-result v1

    #@a6
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setTactileFeedbackEnabled(Z)V

    #@a9
    .line 197
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->Normal:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@ab
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@ae
    .line 199
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->setFocusableInTouchMode(Z)V

    #@b1
    .line 200
    return-void

    #@b2
    .line 169
    :cond_b2
    const-string v0, "UnlockScreen"

    #@b4
    const-string v1, "landscape mode"

    #@b6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 170
    const v0, 0x109006e

    #@bc
    invoke-virtual {v6, v0, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@bf
    goto :goto_44

    #@c0
    :cond_c0
    move v0, v7

    #@c1
    .line 191
    goto :goto_9b
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCancelPatternRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    return-object v0
.end method

.method static synthetic access$608(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mTotalFailedPatternAttempts:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mTotalFailedPatternAttempts:I

    #@6
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@2
    return p1
.end method

.method static synthetic access$708(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mFailedPatternAttemptsSinceLastTimeout:I

    #@6
    return v0
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;J)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->handleAttemptLockout(J)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mEnableFallback:Z

    #@2
    return v0
.end method

.method private handleAttemptLockout(J)V
    .registers 11
    .parameter "elapsedRealtimeDeadline"

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    #@5
    .line 386
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    #@b
    .line 387
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v6

    #@f
    .line 388
    .local v6, elapsedRealtime:J
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;

    #@11
    sub-long v2, p1, v6

    #@13
    const-wide/16 v4, 0x3e8

    #@15
    move-object v1, p0

    #@16
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;JJ)V

    #@19
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->start()Landroid/os/CountDownTimer;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    #@1f
    .line 414
    return-void
.end method

.method private hideForgotPatternButton()V
    .registers 3

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternButton:Landroid/widget/Button;

    #@2
    const/16 v1, 0x8

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    #@7
    .line 109
    return-void
.end method

.method private showForgotPatternButton()V
    .registers 3

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mForgotPatternButton:Landroid/widget/Button;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    #@6
    .line 113
    return-void
.end method

.method private updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 116
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$4;->$SwitchMap$com$android$internal$policy$impl$keyguard_obsolete$PatternUnlockScreen$FooterMode:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_18

    #@b
    .line 129
    :goto_b
    return-void

    #@c
    .line 119
    :pswitch_c
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->hideForgotPatternButton()V

    #@f
    goto :goto_b

    #@10
    .line 123
    :pswitch_10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->showForgotPatternButton()V

    #@13
    goto :goto_b

    #@14
    .line 127
    :pswitch_14
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->hideForgotPatternButton()V

    #@17
    goto :goto_b

    #@18
    .line 116
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_c
        :pswitch_10
        :pswitch_14
    .end packed-switch
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 304
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@3
    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@6
    .line 305
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@8
    .line 306
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@a
    .line 307
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@c
    .line 308
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@e
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    #@11
    .line 309
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    .line 213
    .local v0, result:Z
    if-eqz v0, :cond_19

    #@6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9
    move-result-wide v1

    #@a
    iget-wide v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLastPokeTime:J

    #@c
    sub-long/2addr v1, v3

    #@d
    const-wide/16 v3, 0x1af4

    #@f
    cmp-long v1, v1, v3

    #@11
    if-lez v1, :cond_19

    #@13
    .line 216
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@16
    move-result-wide v1

    #@17
    iput-wide v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLastPokeTime:J

    #@19
    .line 218
    :cond_19
    return v0
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 254
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 223
    invoke-super {p0}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->onAttachedToWindow()V

    #@3
    .line 229
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@a
    move-result-object v0

    #@b
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    #@d
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCreationOrientation:I

    #@f
    if-eq v0, v1, :cond_1e

    #@11
    .line 230
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@1a
    move-result-object v1

    #@1b
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@1e
    .line 232
    :cond_1e
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 238
    invoke-super {p0, p1}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 244
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCreationOrientation:I

    #@7
    if-eq v0, v1, :cond_e

    #@9
    .line 245
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@b
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@e
    .line 247
    :cond_e
    return-void
.end method

.method public onKeyboardChange(Z)V
    .registers 2
    .parameter "isKeyboardOpen"

    #@0
    .prologue
    .line 250
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 260
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    #@6
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    #@9
    .line 261
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    #@c
    .line 263
    :cond_c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onPause()V

    #@11
    .line 264
    return-void
.end method

.method public onResume()V
    .registers 5

    #@0
    .prologue
    .line 269
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onResume()V

    #@5
    .line 272
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@7
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternView;->enableInput()V

    #@a
    .line 273
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@c
    const/4 v3, 0x1

    #@d
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    #@10
    .line 274
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    #@12
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    #@15
    .line 277
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@17
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->doesFallbackUnlockScreenExist()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_3d

    #@1d
    .line 278
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->showForgotPatternButton()V

    #@20
    .line 284
    :goto_20
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@22
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getLockoutAttemptDeadline()J

    #@25
    move-result-wide v0

    #@26
    .line 285
    .local v0, deadline:J
    const-wide/16 v2, 0x0

    #@28
    cmp-long v2, v0, v2

    #@2a
    if-eqz v2, :cond_2f

    #@2c
    .line 286
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->handleAttemptLockout(J)V

    #@2f
    .line 290
    :cond_2f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@31
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->isVerifyUnlockOnly()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_41

    #@37
    .line 291
    sget-object v2, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->VerifyUnlocked:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@39
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@3c
    .line 299
    :goto_3c
    return-void

    #@3d
    .line 280
    .end local v0           #deadline:J
    :cond_3d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->hideForgotPatternButton()V

    #@40
    goto :goto_20

    #@41
    .line 292
    .restart local v0       #deadline:J
    :cond_41
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mEnableFallback:Z

    #@43
    if-eqz v2, :cond_50

    #@45
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mTotalFailedPatternAttempts:I

    #@47
    const/4 v3, 0x5

    #@48
    if-lt v2, v3, :cond_50

    #@4a
    .line 294
    sget-object v2, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->ForgotLockPattern:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@4c
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@4f
    goto :goto_3c

    #@50
    .line 296
    :cond_50
    sget-object v2, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->Normal:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@52
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->updateFooter(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@55
    goto :goto_3c
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 313
    invoke-super {p0, p1}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->onWindowFocusChanged(Z)V

    #@3
    .line 314
    if-eqz p1, :cond_8

    #@5
    .line 316
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->onResume()V

    #@8
    .line 318
    :cond_8
    return-void
.end method

.method public setEnableFallback(Z)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->mEnableFallback:Z

    #@2
    .line 205
    return-void
.end method
