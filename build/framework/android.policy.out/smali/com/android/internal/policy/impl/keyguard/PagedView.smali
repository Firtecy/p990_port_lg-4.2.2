.class public abstract Lcom/android/internal/policy/impl/keyguard/PagedView;
.super Landroid/view/ViewGroup;
.source "PagedView.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;,
        Lcom/android/internal/policy/impl/keyguard/PagedView$SavedState;,
        Lcom/android/internal/policy/impl/keyguard/PagedView$ScrollInterpolator;,
        Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;
    }
.end annotation


# static fields
.field protected static final ALPHA_QUANTIZE_LEVEL:F = 1.0E-4f

.field static final AUTOMATIC_PAGE_SPACING:I = -0x1

.field private static final DEBUG:Z = false

.field private static final DISABLE_FLING_TO_DELETE:Z = false

.field private static final DISABLE_TOUCH_INTERACTION:Z = false

.field private static final DISABLE_TOUCH_SIDE_PAGES:Z = true

.field private static final FLING_THRESHOLD_VELOCITY:I = 0x1f4

.field protected static final INVALID_PAGE:I = -0x1

.field protected static final INVALID_POINTER:I = -0x1

.field private static final MIN_FLING_VELOCITY:I = 0xfa

.field private static final MIN_LENGTH_FOR_FLING:I = 0x19

.field private static final MIN_SNAP_VELOCITY:I = 0x5dc

.field protected static final NANOTIME_DIV:F = 1.0E9f

.field private static final OVERSCROLL_ACCELERATE_FACTOR:F = 2.0f

.field private static final OVERSCROLL_DAMP_FACTOR:F = 0.14f

.field protected static final PAGE_SNAP_ANIMATION_DURATION:I = 0x2ee

.field private static final RETURN_TO_ORIGINAL_PAGE_THRESHOLD:F = 0.33f

.field private static final SIGNIFICANT_MOVE_THRESHOLD:F = 0.4f

.field protected static final SLOW_PAGE_SNAP_ANIMATION_DURATION:I = 0x3b6

.field private static final TAG:Ljava/lang/String; = "WidgetPagedView"

.field protected static final TOUCH_STATE_NEXT_PAGE:I = 0x3

.field protected static final TOUCH_STATE_PREV_PAGE:I = 0x2

.field protected static final TOUCH_STATE_REORDERING:I = 0x4

.field protected static final TOUCH_STATE_REST:I = 0x0

.field protected static final TOUCH_STATE_SCROLLING:I = 0x1

.field protected static final sScrollIndicatorFadeInDuration:I = 0x96

.field protected static final sScrollIndicatorFadeOutDuration:I = 0x28a

.field protected static final sScrollIndicatorFlashDuration:I = 0x28a


# instance fields
.field private DELETE_SLIDE_IN_SIDE_PAGE_DURATION:I

.field private DRAG_TO_DELETE_FADE_OUT_DURATION:I

.field private FLING_TO_DELETE_FADE_OUT_DURATION:I

.field private FLING_TO_DELETE_FRICTION:F

.field private FLING_TO_DELETE_MAX_FLING_DEGREES:F

.field private NUM_ANIMATIONS_RUNNING_BEFORE_ZOOM_OUT:I

.field private REORDERING_DELETE_DROP_TARGET_FADE_DURATION:J

.field private REORDERING_DROP_REPOSITION_DURATION:I

.field protected REORDERING_REORDER_REPOSITION_DURATION:I

.field private REORDERING_SIDE_PAGE_BUFFER_PERCENTAGE:F

.field private REORDERING_SIDE_PAGE_HOVER_TIMEOUT:I

.field protected REORDERING_ZOOM_IN_OUT_DURATION:I

.field hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

.field protected mActivePointerId:I

.field protected mAllowOverScroll:Z

.field private mAltTmpRect:Landroid/graphics/Rect;

.field protected mCellCountX:I

.field protected mCellCountY:I

.field protected mChildCountOnLastMeasure:I

.field private mChildOffsets:[I

.field private mChildOffsetsWithLayoutScale:[I

.field private mChildRelativeOffsets:[I

.field protected mContentIsRefreshable:Z

.field protected mCurrentPage:I

.field protected mDeferScrollUpdate:Z

.field private mDeferringForDelete:Z

.field private mDeleteDropTarget:Landroid/view/View;

.field protected mDensity:F

.field protected mDirtyPageContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDownEventOnEdge:Z

.field private mDownMotionX:F

.field private mDownMotionY:F

.field private mDownScrollX:F

.field protected mDragView:Landroid/view/View;

.field private mEdgeSwipeRegionSize:I

.field protected mFadeInAdjacentScreens:Z

.field protected mFirstLayout:Z

.field protected mFlingThresholdVelocity:I

.field protected mFlingToDeleteThresholdVelocity:I

.field protected mForceDrawAllChildrenNextFrame:Z

.field protected mForceScreenScrolled:Z

.field protected mIsDataReady:Z

.field protected mIsPageMoving:Z

.field private mIsReordering:Z

.field protected mLastMotionX:F

.field protected mLastMotionXRemainder:F

.field protected mLastMotionY:F

.field private mLastScreenCenter:I

.field protected mLayoutScale:F

.field protected mLongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mMaxScrollX:I

.field private mMaximumVelocity:I

.field protected mMinFlingVelocity:I

.field private mMinScale:F

.field protected mMinSnapVelocity:I

.field private mMinimumWidth:I

.field protected mNextPage:I

.field private mOnlyAllowEdgeSwipes:Z

.field protected mOverScrollX:I

.field protected mPageSpacing:I

.field private mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

.field private mPagingTouchSlop:I

.field private mParentDownMotionX:F

.field private mParentDownMotionY:F

.field private mPostReorderingPreZoomInRemainingAnimationCount:I

.field private mPostReorderingPreZoomInRunnable:Ljava/lang/Runnable;

.field private mReorderingStarted:Z

.field private mScrollIndicator:Landroid/view/View;

.field private mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

.field private mScrollIndicatorPaddingLeft:I

.field private mScrollIndicatorPaddingRight:I

.field protected mScroller:Landroid/widget/Scroller;

.field private mShouldShowScrollIndicator:Z

.field private mShouldShowScrollIndicatorImmediately:Z

.field private mSidePageHoverIndex:I

.field private mSidePageHoverRunnable:Ljava/lang/Runnable;

.field protected mSmoothingTime:F

.field protected mTempVisiblePagesRange:[I

.field private mTmpInvMatrix:Landroid/graphics/Matrix;

.field private mTmpPoint:[F

.field private mTmpRect:Landroid/graphics/Rect;

.field private mTopAlignPageWhenShrinkingForBouncer:Z

.field protected mTotalMotionX:F

.field protected mTouchSlop:I

.field protected mTouchState:I

.field protected mTouchX:F

.field protected mUnboundedScrollX:I

.field protected mUsePagingTouchSlop:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mViewport:Landroid/graphics/Rect;

.field protected mZoomInOutAnim:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 261
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 262
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 265
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 266
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/high16 v8, 0x3f80

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v6, -0x1

    #@4
    const/4 v5, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 269
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@9
    .line 104
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFirstLayout:Z

    #@b
    .line 109
    iput v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@d
    .line 123
    iput v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastScreenCenter:I

    #@f
    .line 136
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@11
    .line 137
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@13
    .line 146
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCellCountX:I

    #@15
    .line 147
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCellCountY:I

    #@17
    .line 148
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAllowOverScroll:Z

    #@19
    .line 150
    new-array v2, v7, [I

    #@1b
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@1d
    .line 159
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLayoutScale:F

    #@1f
    .line 163
    iput v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@21
    .line 170
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContentIsRefreshable:Z

    #@23
    .line 173
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFadeInAdjacentScreens:Z

    #@25
    .line 177
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUsePagingTouchSlop:Z

    #@27
    .line 181
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferScrollUpdate:Z

    #@29
    .line 183
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@2b
    .line 186
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsDataReady:Z

    #@2d
    .line 193
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicator:Z

    #@2f
    .line 194
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicatorImmediately:Z

    #@31
    .line 201
    new-instance v2, Landroid/graphics/Rect;

    #@33
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@36
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@38
    .line 206
    const/16 v2, 0xc8

    #@3a
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_DROP_REPOSITION_DURATION:I

    #@3c
    .line 207
    const/16 v2, 0x12c

    #@3e
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_REORDER_REPOSITION_DURATION:I

    #@40
    .line 208
    const/16 v2, 0xfa

    #@42
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@44
    .line 209
    const/16 v2, 0x12c

    #@46
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_SIDE_PAGE_HOVER_TIMEOUT:I

    #@48
    .line 210
    const v2, 0x3dcccccd

    #@4b
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_SIDE_PAGE_BUFFER_PERCENTAGE:F

    #@4d
    .line 211
    const-wide/16 v2, 0x96

    #@4f
    iput-wide v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_DELETE_DROP_TARGET_FADE_DURATION:J

    #@51
    .line 212
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@53
    .line 216
    iput v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverIndex:I

    #@55
    .line 218
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mReorderingStarted:Z

    #@57
    .line 223
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->NUM_ANIMATIONS_RUNNING_BEFORE_ZOOM_OUT:I

    #@59
    .line 228
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOnlyAllowEdgeSwipes:Z

    #@5b
    .line 229
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownEventOnEdge:Z

    #@5d
    .line 230
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@5f
    .line 233
    new-instance v2, Landroid/graphics/Matrix;

    #@61
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    #@64
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpInvMatrix:Landroid/graphics/Matrix;

    #@66
    .line 234
    new-array v2, v7, [F

    #@68
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@6a
    .line 235
    new-instance v2, Landroid/graphics/Rect;

    #@6c
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@6f
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@71
    .line 236
    new-instance v2, Landroid/graphics/Rect;

    #@73
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@76
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAltTmpRect:Landroid/graphics/Rect;

    #@78
    .line 239
    const/16 v2, 0x15e

    #@7a
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_FADE_OUT_DURATION:I

    #@7c
    .line 240
    const v2, 0x3d0f5c29

    #@7f
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_FRICTION:F

    #@81
    .line 242
    const/high16 v2, 0x4282

    #@83
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_MAX_FLING_DEGREES:F

    #@85
    .line 243
    const/16 v2, -0x578

    #@87
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingToDeleteThresholdVelocity:I

    #@89
    .line 245
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@8b
    .line 246
    const/16 v2, 0xfa

    #@8d
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->DELETE_SLIDE_IN_SIDE_PAGE_DURATION:I

    #@8f
    .line 247
    const/16 v2, 0x15e

    #@91
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->DRAG_TO_DELETE_FADE_OUT_DURATION:I

    #@93
    .line 253
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTopAlignPageWhenShrinkingForBouncer:Z

    #@95
    .line 1932
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/PagedView$2;

    #@97
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V

    #@9a
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    #@9c
    .line 270
    sget-object v2, Lcom/android/internal/R$styleable;->PagedView:[I

    #@9e
    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@a1
    move-result-object v0

    #@a2
    .line 272
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@a5
    move-result v2

    #@a6
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setPageSpacing(I)V

    #@a9
    .line 273
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@ac
    move-result v2

    #@ad
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorPaddingLeft:I

    #@af
    .line 275
    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@b2
    move-result v2

    #@b3
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorPaddingRight:I

    #@b5
    .line 277
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@b8
    .line 279
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getResources()Landroid/content/res/Resources;

    #@bb
    move-result-object v1

    #@bc
    .line 280
    .local v1, r:Landroid/content/res/Resources;
    const v2, 0x1050076

    #@bf
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@c2
    move-result v2

    #@c3
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@c5
    .line 281
    const v2, 0x1110002

    #@c8
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@cb
    move-result v2

    #@cc
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTopAlignPageWhenShrinkingForBouncer:Z

    #@ce
    .line 284
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setHapticFeedbackEnabled(Z)V

    #@d1
    .line 285
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->init()V

    #@d4
    .line 286
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/PagedView;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 64
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownScrollX:F

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/PagedView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 64
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverIndex:I

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/PagedView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 64
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onPostReorderingAnimationCompleted()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeleteDropTarget:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/PagedView;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_DELETE_DROP_TARGET_FADE_DURATION:J

    #@2
    return-wide v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/PagedView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->DELETE_SLIDE_IN_SIDE_PAGE_DURATION:I

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/android/internal/policy/impl/keyguard/PagedView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/PagedView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_FADE_OUT_DURATION:I

    #@2
    return v0
.end method

.method private acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 1653
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 1654
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 1656
    :cond_a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@f
    .line 1657
    return-void
.end method

.method private createPostDeleteAnimationRunnable(Landroid/view/View;)Ljava/lang/Runnable;
    .registers 3
    .parameter "dragView"

    #@0
    .prologue
    .line 2329
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView$9;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Landroid/view/View;)V

    #@5
    return-object v0
.end method

.method private isFlingingToDelete()Landroid/graphics/PointF;
    .registers 9

    #@0
    .prologue
    .line 2270
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v4

    #@4
    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@7
    move-result-object v0

    #@8
    .line 2271
    .local v0, config:Landroid/view/ViewConfiguration;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    const/16 v5, 0x3e8

    #@c
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@f
    move-result v6

    #@10
    int-to-float v6, v6

    #@11
    invoke-virtual {v4, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@14
    .line 2273
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@16
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@19
    move-result v4

    #@1a
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingToDeleteThresholdVelocity:I

    #@1c
    int-to-float v5, v5

    #@1d
    cmpg-float v4, v4, v5

    #@1f
    if-gez v4, :cond_62

    #@21
    .line 2275
    new-instance v3, Landroid/graphics/PointF;

    #@23
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@25
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@28
    move-result v4

    #@29
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2b
    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@2e
    move-result v5

    #@2f
    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    #@32
    .line 2277
    .local v3, vel:Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    #@34
    const/4 v4, 0x0

    #@35
    const/high16 v5, -0x4080

    #@37
    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    #@3a
    .line 2278
    .local v2, upVec:Landroid/graphics/PointF;
    iget v4, v3, Landroid/graphics/PointF;->x:F

    #@3c
    iget v5, v2, Landroid/graphics/PointF;->x:F

    #@3e
    mul-float/2addr v4, v5

    #@3f
    iget v5, v3, Landroid/graphics/PointF;->y:F

    #@41
    iget v6, v2, Landroid/graphics/PointF;->y:F

    #@43
    mul-float/2addr v5, v6

    #@44
    add-float/2addr v4, v5

    #@45
    invoke-virtual {v3}, Landroid/graphics/PointF;->length()F

    #@48
    move-result v5

    #@49
    invoke-virtual {v2}, Landroid/graphics/PointF;->length()F

    #@4c
    move-result v6

    #@4d
    mul-float/2addr v5, v6

    #@4e
    div-float/2addr v4, v5

    #@4f
    float-to-double v4, v4

    #@50
    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    #@53
    move-result-wide v4

    #@54
    double-to-float v1, v4

    #@55
    .line 2280
    .local v1, theta:F
    float-to-double v4, v1

    #@56
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_MAX_FLING_DEGREES:F

    #@58
    float-to-double v6, v6

    #@59
    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    #@5c
    move-result-wide v6

    #@5d
    cmpg-double v4, v4, v6

    #@5f
    if-gtz v4, :cond_62

    #@61
    .line 2284
    .end local v1           #theta:F
    .end local v2           #upVec:Landroid/graphics/PointF;
    .end local v3           #vel:Landroid/graphics/PointF;
    :goto_61
    return-object v3

    #@62
    :cond_62
    const/4 v3, 0x0

    #@63
    goto :goto_61
.end method

.method private isHoveringOverDeleteDropTarget(II)Z
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2475
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeleteDropTarget:Landroid/view/View;

    #@3
    if-eqz v2, :cond_35

    #@5
    .line 2476
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAltTmpRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 2477
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeleteDropTarget:Landroid/view/View;

    #@c
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/view/View;

    #@12
    .line 2478
    .local v0, parent:Landroid/view/View;
    if-eqz v0, :cond_19

    #@14
    .line 2479
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAltTmpRect:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@19
    .line 2481
    :cond_19
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeleteDropTarget:Landroid/view/View;

    #@1b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@1d
    invoke-virtual {v1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@20
    .line 2482
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@22
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAltTmpRect:Landroid/graphics/Rect;

    #@24
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@26
    neg-int v2, v2

    #@27
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAltTmpRect:Landroid/graphics/Rect;

    #@29
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@2b
    neg-int v3, v3

    #@2c
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@2f
    .line 2483
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@31
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@34
    move-result v1

    #@35
    .line 2485
    .end local v0           #parent:Landroid/view/View;
    :cond_35
    return v1
.end method

.method private isTouchPointInCurrentPage(II)Z
    .registers 9
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1031
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@4
    move-result v2

    #@5
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    .line 1032
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_2c

    #@b
    .line 1033
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@d
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@10
    move-result v3

    #@11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@14
    move-result v4

    #@15
    sub-int/2addr v3, v4

    #@16
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@19
    move-result v4

    #@1a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@1d
    move-result v5

    #@1e
    sub-int/2addr v4, v5

    #@1f
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@22
    move-result v5

    #@23
    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@26
    .line 1035
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@28
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@2b
    move-result v1

    #@2c
    .line 1037
    :cond_2c
    return v1
.end method

.method private isTouchPointInViewportWithBuffer(II)Z
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1024
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@4
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@8
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    #@b
    move-result v2

    #@c
    div-int/lit8 v2, v2, 0x2

    #@e
    sub-int/2addr v1, v2

    #@f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@11
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@13
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@15
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@17
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@19
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    #@1c
    move-result v4

    #@1d
    div-int/lit8 v4, v4, 0x2

    #@1f
    add-int/2addr v3, v4

    #@20
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@22
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@24
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@27
    .line 1026
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpRect:Landroid/graphics/Rect;

    #@29
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@2c
    move-result v0

    #@2d
    return v0
.end method

.method private onDropToDelete()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v12, 0x0

    #@2
    const/4 v11, 0x1

    #@3
    .line 2491
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@5
    .line 2493
    .local v3, dragView:Landroid/view/View;
    const/4 v7, 0x0

    #@6
    .line 2494
    .local v7, toScale:F
    const/4 v6, 0x0

    #@7
    .line 2497
    .local v6, toAlpha:F
    new-instance v2, Ljava/util/ArrayList;

    #@9
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 2498
    .local v2, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    new-instance v4, Landroid/animation/AnimatorSet;

    #@e
    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    #@11
    .line 2499
    .local v4, motionAnim:Landroid/animation/AnimatorSet;
    new-instance v8, Landroid/view/animation/DecelerateInterpolator;

    #@13
    const/high16 v9, 0x4000

    #@15
    invoke-direct {v8, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@18
    invoke-virtual {v4, v8}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@1b
    .line 2500
    const/4 v8, 0x2

    #@1c
    new-array v8, v8, [Landroid/animation/Animator;

    #@1e
    const-string v9, "scaleX"

    #@20
    new-array v10, v11, [F

    #@22
    aput v13, v10, v12

    #@24
    invoke-static {v3, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@27
    move-result-object v9

    #@28
    aput-object v9, v8, v12

    #@2a
    const-string v9, "scaleY"

    #@2c
    new-array v10, v11, [F

    #@2e
    aput v13, v10, v12

    #@30
    invoke-static {v3, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@33
    move-result-object v9

    #@34
    aput-object v9, v8, v11

    #@36
    invoke-virtual {v4, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@39
    .line 2503
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3c
    .line 2505
    new-instance v0, Landroid/animation/AnimatorSet;

    #@3e
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    #@41
    .line 2506
    .local v0, alphaAnim:Landroid/animation/AnimatorSet;
    new-instance v8, Landroid/view/animation/LinearInterpolator;

    #@43
    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@46
    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@49
    .line 2507
    new-array v8, v11, [Landroid/animation/Animator;

    #@4b
    const-string v9, "alpha"

    #@4d
    new-array v10, v11, [F

    #@4f
    aput v13, v10, v12

    #@51
    invoke-static {v3, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@54
    move-result-object v9

    #@55
    aput-object v9, v8, v12

    #@57
    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@5a
    .line 2509
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5d
    .line 2511
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->createPostDeleteAnimationRunnable(Landroid/view/View;)Ljava/lang/Runnable;

    #@60
    move-result-object v5

    #@61
    .line 2513
    .local v5, onAnimationEndRunnable:Ljava/lang/Runnable;
    new-instance v1, Landroid/animation/AnimatorSet;

    #@63
    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    #@66
    .line 2514
    .local v1, anim:Landroid/animation/AnimatorSet;
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    #@69
    .line 2515
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->DRAG_TO_DELETE_FADE_OUT_DURATION:I

    #@6b
    int-to-long v8, v8

    #@6c
    invoke-virtual {v1, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@6f
    .line 2516
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/PagedView$12;

    #@71
    invoke-direct {v8, p0, v5}, Lcom/android/internal/policy/impl/keyguard/PagedView$12;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Ljava/lang/Runnable;)V

    #@74
    invoke-virtual {v1, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@77
    .line 2521
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    #@7a
    .line 2523
    iput-boolean v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@7c
    .line 2524
    return-void
.end method

.method private onPostReorderingAnimationCompleted()V
    .registers 2

    #@0
    .prologue
    .line 2133
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRemainingAnimationCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRemainingAnimationCount:I

    #@6
    .line 2134
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRunnable:Ljava/lang/Runnable;

    #@8
    if-eqz v0, :cond_16

    #@a
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRemainingAnimationCount:I

    #@c
    if-nez v0, :cond_16

    #@e
    .line 2136
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRunnable:Ljava/lang/Runnable;

    #@10
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@13
    .line 2137
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRunnable:Ljava/lang/Runnable;

    #@16
    .line 2139
    :cond_16
    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    .line 1667
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v3

    #@4
    const v4, 0xff00

    #@7
    and-int/2addr v3, v4

    #@8
    shr-int/lit8 v2, v3, 0x8

    #@a
    .line 1669
    .local v2, pointerIndex:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@d
    move-result v1

    #@e
    .line 1670
    .local v1, pointerId:I
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@10
    if-ne v1, v3, :cond_35

    #@12
    .line 1674
    if-nez v2, :cond_36

    #@14
    const/4 v0, 0x1

    #@15
    .line 1675
    .local v0, newPointerIndex:I
    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@18
    move-result v3

    #@19
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@1b
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@1d
    .line 1676
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@20
    move-result v3

    #@21
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@23
    .line 1677
    const/4 v3, 0x0

    #@24
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@26
    .line 1678
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@29
    move-result v3

    #@2a
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@2c
    .line 1679
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2e
    if-eqz v3, :cond_35

    #@30
    .line 1680
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@32
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@35
    .line 1683
    .end local v0           #newPointerIndex:I
    :cond_35
    return-void

    #@36
    .line 1674
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_15
.end method

.method private overScrollInfluenceCurve(F)F
    .registers 4
    .parameter "f"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 1236
    sub-float/2addr p1, v1

    #@3
    .line 1237
    mul-float v0, p1, p1

    #@5
    mul-float/2addr v0, p1

    #@6
    add-float/2addr v0, v1

    #@7
    return v0
.end method

.method private releaseVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 1660
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1661
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@9
    .line 1662
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    .line 1664
    :cond_c
    return-void
.end method

.method private resetTouchState()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1614
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->releaseVelocityTracker()V

    #@4
    .line 1615
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->endReordering()V

    #@7
    .line 1616
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@9
    .line 1617
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@c
    .line 1618
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownEventOnEdge:Z

    #@e
    .line 1619
    return-void
.end method

.method private updateScrollingIndicator()V
    .registers 3

    #@0
    .prologue
    .line 2016
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    if-gt v0, v1, :cond_8

    #@7
    .line 2026
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2017
    :cond_8
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isScrollingIndicatorEnabled()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_7

    #@e
    .line 2019
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollingIndicator()Landroid/view/View;

    #@11
    .line 2020
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@13
    if-eqz v0, :cond_18

    #@15
    .line 2021
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateScrollingIndicatorPosition()V

    #@18
    .line 2023
    :cond_18
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicator:Z

    #@1a
    if-eqz v0, :cond_7

    #@1c
    .line 2024
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicatorImmediately:Z

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->showScrollingIndicator(Z)V

    #@21
    goto :goto_7
.end method

.method private updateScrollingIndicatorPosition()V
    .registers 15

    #@0
    .prologue
    .line 2029
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isScrollingIndicatorEnabled()Z

    #@3
    move-result v10

    #@4
    if-nez v10, :cond_7

    #@6
    .line 2052
    :cond_6
    :goto_6
    return-void

    #@7
    .line 2030
    :cond_7
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@9
    if-eqz v10, :cond_6

    #@b
    .line 2031
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@e
    move-result v6

    #@f
    .line 2032
    .local v6, numPages:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@12
    move-result v8

    #@13
    .line 2033
    .local v8, pageWidth:I
    const/4 v10, 0x0

    #@14
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@17
    move-result v11

    #@18
    add-int/lit8 v11, v11, -0x1

    #@1a
    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    #@1d
    move-result v4

    #@1e
    .line 2034
    .local v4, lastChildIndex:I
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@21
    move-result v10

    #@22
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@25
    move-result v11

    #@26
    sub-int v5, v10, v11

    #@28
    .line 2035
    .local v5, maxScrollX:I
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorPaddingLeft:I

    #@2a
    sub-int v10, v8, v10

    #@2c
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorPaddingRight:I

    #@2e
    sub-int v9, v10, v11

    #@30
    .line 2036
    .local v9, trackWidth:I
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@32
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    #@35
    move-result v10

    #@36
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@38
    invoke-virtual {v11}, Landroid/view/View;->getPaddingLeft()I

    #@3b
    move-result v11

    #@3c
    sub-int/2addr v10, v11

    #@3d
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@3f
    invoke-virtual {v11}, Landroid/view/View;->getPaddingRight()I

    #@42
    move-result v11

    #@43
    sub-int v3, v10, v11

    #@45
    .line 2039
    .local v3, indicatorWidth:I
    const/4 v10, 0x0

    #@46
    const/high16 v11, 0x3f80

    #@48
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@4b
    move-result v12

    #@4c
    int-to-float v12, v12

    #@4d
    int-to-float v13, v5

    #@4e
    div-float/2addr v12, v13

    #@4f
    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    #@52
    move-result v11

    #@53
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    #@56
    move-result v7

    #@57
    .line 2040
    .local v7, offset:F
    div-int v2, v9, v6

    #@59
    .line 2041
    .local v2, indicatorSpace:I
    sub-int v10, v9, v2

    #@5b
    int-to-float v10, v10

    #@5c
    mul-float/2addr v10, v7

    #@5d
    float-to-int v10, v10

    #@5e
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorPaddingLeft:I

    #@60
    add-int v1, v10, v11

    #@62
    .line 2042
    .local v1, indicatorPos:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->hasElasticScrollIndicator()Z

    #@65
    move-result v10

    #@66
    if-eqz v10, :cond_84

    #@68
    .line 2043
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@6a
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    #@6d
    move-result v10

    #@6e
    if-eq v10, v2, :cond_7d

    #@70
    .line 2044
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@72
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@75
    move-result-object v10

    #@76
    iput v2, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@78
    .line 2045
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@7a
    invoke-virtual {v10}, Landroid/view/View;->requestLayout()V

    #@7d
    .line 2051
    :cond_7d
    :goto_7d
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@7f
    int-to-float v11, v1

    #@80
    invoke-virtual {v10, v11}, Landroid/view/View;->setTranslationX(F)V

    #@83
    goto :goto_6

    #@84
    .line 2048
    :cond_84
    div-int/lit8 v10, v2, 0x2

    #@86
    div-int/lit8 v11, v3, 0x2

    #@88
    sub-int v0, v10, v11

    #@8a
    .line 2049
    .local v0, indicatorCenterOffset:I
    add-int/2addr v1, v0

    #@8b
    goto :goto_7d
.end method


# virtual methods
.method protected acceleratedOverScroll(F)V
    .registers 8
    .parameter "amount"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1241
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@4
    move-result v2

    #@5
    .line 1245
    .local v2, screenSize:I
    const/high16 v3, 0x4000

    #@7
    int-to-float v4, v2

    #@8
    div-float v4, p1, v4

    #@a
    mul-float v0, v3, v4

    #@c
    .line 1247
    .local v0, f:F
    cmpl-float v3, v0, v5

    #@e
    if-nez v3, :cond_11

    #@10
    .line 1263
    :goto_10
    return-void

    #@11
    .line 1250
    :cond_11
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@14
    move-result v3

    #@15
    const/high16 v4, 0x3f80

    #@17
    cmpl-float v3, v3, v4

    #@19
    if-ltz v3, :cond_20

    #@1b
    .line 1251
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@1e
    move-result v3

    #@1f
    div-float/2addr v0, v3

    #@20
    .line 1254
    :cond_20
    int-to-float v3, v2

    #@21
    mul-float/2addr v3, v0

    #@22
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@25
    move-result v1

    #@26
    .line 1255
    .local v1, overScrollAmount:I
    cmpg-float v3, p1, v5

    #@28
    if-gez v3, :cond_38

    #@2a
    .line 1256
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@2c
    .line 1257
    const/4 v3, 0x0

    #@2d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@30
    move-result v4

    #@31
    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@34
    .line 1262
    :goto_34
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@37
    goto :goto_10

    #@38
    .line 1259
    :cond_38
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@3a
    add-int/2addr v3, v1

    #@3b
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@3d
    .line 1260
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@3f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@42
    move-result v4

    #@43
    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@46
    goto :goto_34
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 6
    .parameter
    .parameter "direction"
    .parameter "focusableMode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    #@0
    .prologue
    .line 966
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2
    if-ltz v0, :cond_15

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@9
    move-result v1

    #@a
    if-ge v0, v1, :cond_15

    #@c
    .line 967
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@15
    .line 969
    :cond_15
    const/16 v0, 0x11

    #@17
    if-ne p2, v0, :cond_29

    #@19
    .line 970
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@1b
    if-lez v0, :cond_28

    #@1d
    .line 971
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@1f
    add-int/lit8 v0, v0, -0x1

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@28
    .line 978
    :cond_28
    :goto_28
    return-void

    #@29
    .line 973
    :cond_29
    const/16 v0, 0x42

    #@2b
    if-ne p2, v0, :cond_28

    #@2d
    .line 974
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@32
    move-result v1

    #@33
    add-int/lit8 v1, v1, -0x1

    #@35
    if-ge v0, v1, :cond_28

    #@37
    .line 975
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@39
    add-int/lit8 v0, v0, 0x1

    #@3b
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@42
    goto :goto_28
.end method

.method animateDragViewToOriginalPosition()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 2056
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@5
    if-eqz v1, :cond_3f

    #@7
    .line 2057
    new-instance v0, Landroid/animation/AnimatorSet;

    #@9
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    #@c
    .line 2058
    .local v0, anim:Landroid/animation/AnimatorSet;
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_DROP_REPOSITION_DURATION:I

    #@e
    int-to-long v1, v1

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@12
    .line 2059
    const/4 v1, 0x2

    #@13
    new-array v1, v1, [Landroid/animation/Animator;

    #@15
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@17
    const-string v3, "translationX"

    #@19
    new-array v4, v6, [F

    #@1b
    aput v7, v4, v5

    #@1d
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@20
    move-result-object v2

    #@21
    aput-object v2, v1, v5

    #@23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@25
    const-string v3, "translationY"

    #@27
    new-array v4, v6, [F

    #@29
    aput v7, v4, v5

    #@2b
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@2e
    move-result-object v2

    #@2f
    aput-object v2, v1, v6

    #@31
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@34
    .line 2062
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/PagedView$4;

    #@36
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V

    #@39
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@3c
    .line 2068
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    #@3f
    .line 2070
    .end local v0           #anim:Landroid/animation/AnimatorSet;
    :cond_3f
    return-void
.end method

.method boundByReorderablePages(Z[I)V
    .registers 3
    .parameter "isReordering"
    .parameter "range"

    #@0
    .prologue
    .line 830
    return-void
.end method

.method protected cancelScrollingIndicatorAnimations()V
    .registers 2

    #@0
    .prologue
    .line 1969
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1970
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@6
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    #@9
    .line 1972
    :cond_9
    return-void
.end method

.method public computeScroll()V
    .registers 1

    #@0
    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->computeScrollHelper()Z

    #@3
    .line 580
    return-void
.end method

.method protected computeScrollHelper()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v4, -0x1

    #@3
    .line 551
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@5
    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_40

    #@b
    .line 553
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@e
    move-result v1

    #@f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@11
    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    #@14
    move-result v2

    #@15
    if-ne v1, v2, :cond_2d

    #@17
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@1a
    move-result v1

    #@1b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@1d
    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    #@20
    move-result v2

    #@21
    if-ne v1, v2, :cond_2d

    #@23
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@25
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@27
    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    #@2a
    move-result v2

    #@2b
    if-eq v1, v2, :cond_3c

    #@2d
    .line 556
    :cond_2d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@2f
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    #@32
    move-result v1

    #@33
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@35
    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    #@38
    move-result v2

    #@39
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollTo(II)V

    #@3c
    .line 558
    :cond_3c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@3f
    .line 574
    :goto_3f
    return v0

    #@40
    .line 560
    :cond_40
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@42
    if-eq v2, v4, :cond_66

    #@44
    .line 561
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@46
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@49
    move-result v3

    #@4a
    add-int/lit8 v3, v3, -0x1

    #@4c
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@4f
    move-result v2

    #@50
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@53
    move-result v1

    #@54
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@56
    .line 562
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@58
    .line 563
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->notifyPageSwitched()V

    #@5b
    .line 567
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@5d
    if-nez v1, :cond_62

    #@5f
    .line 568
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->pageEndMoving()V

    #@62
    .line 571
    :cond_62
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onPostReorderingAnimationCompleted()V

    #@65
    goto :goto_3f

    #@66
    :cond_66
    move v0, v1

    #@67
    .line 574
    goto :goto_3f
.end method

.method protected dampedOverScroll(F)V
    .registers 8
    .parameter "amount"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1266
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@4
    move-result v2

    #@5
    .line 1268
    .local v2, screenSize:I
    int-to-float v3, v2

    #@6
    div-float v0, p1, v3

    #@8
    .line 1270
    .local v0, f:F
    cmpl-float v3, v0, v5

    #@a
    if-nez v3, :cond_d

    #@c
    .line 1287
    :goto_c
    return-void

    #@d
    .line 1271
    :cond_d
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@10
    move-result v3

    #@11
    div-float v3, v0, v3

    #@13
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@16
    move-result v4

    #@17
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->overScrollInfluenceCurve(F)F

    #@1a
    move-result v4

    #@1b
    mul-float v0, v3, v4

    #@1d
    .line 1274
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@20
    move-result v3

    #@21
    const/high16 v4, 0x3f80

    #@23
    cmpl-float v3, v3, v4

    #@25
    if-ltz v3, :cond_2c

    #@27
    .line 1275
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@2a
    move-result v3

    #@2b
    div-float/2addr v0, v3

    #@2c
    .line 1278
    :cond_2c
    const v3, 0x3e0f5c29

    #@2f
    mul-float/2addr v3, v0

    #@30
    int-to-float v4, v2

    #@31
    mul-float/2addr v3, v4

    #@32
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@35
    move-result v1

    #@36
    .line 1279
    .local v1, overScrollAmount:I
    cmpg-float v3, p1, v5

    #@38
    if-gez v3, :cond_48

    #@3a
    .line 1280
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@3c
    .line 1281
    const/4 v3, 0x0

    #@3d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@40
    move-result v4

    #@41
    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@44
    .line 1286
    :goto_44
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@47
    goto :goto_c

    #@48
    .line 1283
    :cond_48
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@4a
    add-int/2addr v3, v1

    #@4b
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@4d
    .line 1284
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@4f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@52
    move-result v4

    #@53
    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@56
    goto :goto_44
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 1164
    const/high16 v0, 0x3f80

    #@2
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;F)V

    #@5
    .line 1165
    return-void
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;F)V
    .registers 15
    .parameter "ev"
    .parameter "touchSlopScale"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 1173
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@4
    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@7
    move-result v0

    #@8
    .line 1174
    .local v0, pointerIndex:I
    const/4 v10, -0x1

    #@9
    if-ne v0, v10, :cond_c

    #@b
    .line 1205
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1177
    :cond_c
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@f
    move-result v2

    #@10
    .line 1178
    .local v2, x:F
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@13
    move-result v6

    #@14
    .line 1179
    .local v6, y:F
    float-to-int v10, v2

    #@15
    float-to-int v11, v6

    #@16
    invoke-direct {p0, v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isTouchPointInViewportWithBuffer(II)Z

    #@19
    move-result v10

    #@1a
    if-eqz v10, :cond_b

    #@1c
    .line 1183
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOnlyAllowEdgeSwipes:Z

    #@1e
    if-eqz v10, :cond_24

    #@20
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownEventOnEdge:Z

    #@22
    if-eqz v10, :cond_b

    #@24
    .line 1185
    :cond_24
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@26
    sub-float v10, v2, v10

    #@28
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@2b
    move-result v10

    #@2c
    float-to-int v3, v10

    #@2d
    .line 1186
    .local v3, xDiff:I
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@2f
    sub-float v10, v6, v10

    #@31
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@34
    move-result v10

    #@35
    float-to-int v7, v10

    #@36
    .line 1188
    .local v7, yDiff:I
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchSlop:I

    #@38
    int-to-float v10, v10

    #@39
    mul-float/2addr v10, p2

    #@3a
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@3d
    move-result v1

    #@3e
    .line 1189
    .local v1, touchSlop:I
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPagingTouchSlop:I

    #@40
    if-le v3, v10, :cond_83

    #@42
    move v5, v9

    #@43
    .line 1190
    .local v5, xPaged:Z
    :goto_43
    if-le v3, v1, :cond_85

    #@45
    move v4, v9

    #@46
    .line 1191
    .local v4, xMoved:Z
    :goto_46
    if-le v7, v1, :cond_49

    #@48
    move v8, v9

    #@49
    .line 1193
    .local v8, yMoved:Z
    :cond_49
    if-nez v4, :cond_4f

    #@4b
    if-nez v5, :cond_4f

    #@4d
    if-eqz v8, :cond_b

    #@4f
    .line 1194
    :cond_4f
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUsePagingTouchSlop:Z

    #@51
    if-eqz v10, :cond_87

    #@53
    if-eqz v5, :cond_b

    #@55
    .line 1196
    :goto_55
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@57
    .line 1197
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@59
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@5b
    sub-float/2addr v10, v2

    #@5c
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@5f
    move-result v10

    #@60
    add-float/2addr v9, v10

    #@61
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@63
    .line 1198
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@65
    .line 1199
    const/4 v9, 0x0

    #@66
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@68
    .line 1200
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@6b
    move-result v9

    #@6c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@6f
    move-result v10

    #@70
    add-int/2addr v9, v10

    #@71
    int-to-float v9, v9

    #@72
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchX:F

    #@74
    .line 1201
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@77
    move-result-wide v9

    #@78
    long-to-float v9, v9

    #@79
    const v10, 0x4e6e6b28

    #@7c
    div-float/2addr v9, v10

    #@7d
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSmoothingTime:F

    #@7f
    .line 1202
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->pageBeginMoving()V

    #@82
    goto :goto_b

    #@83
    .end local v4           #xMoved:Z
    .end local v5           #xPaged:Z
    .end local v8           #yMoved:Z
    :cond_83
    move v5, v8

    #@84
    .line 1189
    goto :goto_43

    #@85
    .restart local v5       #xPaged:Z
    :cond_85
    move v4, v8

    #@86
    .line 1190
    goto :goto_46

    #@87
    .line 1194
    .restart local v4       #xMoved:Z
    .restart local v8       #yMoved:Z
    :cond_87
    if-eqz v4, :cond_b

    #@89
    goto :goto_55
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter "canvas"

    #@0
    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@3
    move-result v9

    #@4
    div-int/lit8 v2, v9, 0x2

    #@6
    .line 880
    .local v2, halfScreenSize:I
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@8
    add-int v7, v9, v2

    #@a
    .line 882
    .local v7, screenCenter:I
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastScreenCenter:I

    #@c
    if-ne v7, v9, :cond_12

    #@e
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@10
    if-eqz v9, :cond_1a

    #@12
    .line 885
    :cond_12
    const/4 v9, 0x0

    #@13
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@15
    .line 886
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/PagedView;->screenScrolled(I)V

    #@18
    .line 887
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastScreenCenter:I

    #@1a
    .line 891
    :cond_1a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@1d
    move-result v5

    #@1e
    .line 892
    .local v5, pageCount:I
    if-lez v5, :cond_93

    #@20
    .line 893
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@22
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getVisiblePages([I)V

    #@25
    .line 894
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@27
    const/4 v10, 0x0

    #@28
    aget v4, v9, v10

    #@2a
    .line 895
    .local v4, leftScreen:I
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2c
    const/4 v10, 0x1

    #@2d
    aget v6, v9, v10

    #@2f
    .line 896
    .local v6, rightScreen:I
    const/4 v9, -0x1

    #@30
    if-eq v4, v9, :cond_93

    #@32
    const/4 v9, -0x1

    #@33
    if-eq v6, v9, :cond_93

    #@35
    .line 897
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getDrawingTime()J

    #@38
    move-result-wide v0

    #@39
    .line 899
    .local v0, drawingTime:J
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@3c
    .line 900
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@3f
    move-result v9

    #@40
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@43
    move-result v10

    #@44
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@47
    move-result v11

    #@48
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRight()I

    #@4b
    move-result v12

    #@4c
    add-int/2addr v11, v12

    #@4d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getLeft()I

    #@50
    move-result v12

    #@51
    sub-int/2addr v11, v12

    #@52
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@55
    move-result v12

    #@56
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getBottom()I

    #@59
    move-result v13

    #@5a
    add-int/2addr v12, v13

    #@5b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getTop()I

    #@5e
    move-result v13

    #@5f
    sub-int/2addr v12, v13

    #@60
    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@63
    .line 904
    add-int/lit8 v3, v5, -0x1

    #@65
    .local v3, i:I
    :goto_65
    if-ltz v3, :cond_84

    #@67
    .line 905
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@6a
    move-result-object v8

    #@6b
    .line 906
    .local v8, v:Landroid/view/View;
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@6d
    if-ne v8, v9, :cond_72

    #@6f
    .line 904
    :cond_6f
    :goto_6f
    add-int/lit8 v3, v3, -0x1

    #@71
    goto :goto_65

    #@72
    .line 907
    :cond_72
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceDrawAllChildrenNextFrame:Z

    #@74
    if-nez v9, :cond_80

    #@76
    if-gt v4, v3, :cond_6f

    #@78
    if-gt v3, v6, :cond_6f

    #@7a
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/PagedView;->shouldDrawChild(Landroid/view/View;)Z

    #@7d
    move-result v9

    #@7e
    if-eqz v9, :cond_6f

    #@80
    .line 909
    :cond_80
    invoke-virtual {p0, p1, v8, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@83
    goto :goto_6f

    #@84
    .line 913
    .end local v8           #v:Landroid/view/View;
    :cond_84
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@86
    if-eqz v9, :cond_8d

    #@88
    .line 914
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@8a
    invoke-virtual {p0, p1, v9, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@8d
    .line 917
    :cond_8d
    const/4 v9, 0x0

    #@8e
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceDrawAllChildrenNextFrame:Z

    #@90
    .line 918
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@93
    .line 921
    .end local v0           #drawingTime:J
    .end local v3           #i:I
    .end local v4           #leftScreen:I
    .end local v6           #rightScreen:I
    :cond_93
    return-void
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 6
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 950
    const/16 v1, 0x11

    #@3
    if-ne p2, v1, :cond_15

    #@5
    .line 951
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@8
    move-result v1

    #@9
    if-lez v1, :cond_2f

    #@b
    .line 952
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@e
    move-result v1

    #@f
    add-int/lit8 v1, v1, -0x1

    #@11
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@14
    .line 961
    :goto_14
    return v0

    #@15
    .line 955
    :cond_15
    const/16 v1, 0x42

    #@17
    if-ne p2, v1, :cond_2f

    #@19
    .line 956
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@20
    move-result v2

    #@21
    add-int/lit8 v2, v2, -0x1

    #@23
    if-ge v1, v2, :cond_2f

    #@25
    .line 957
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@28
    move-result v1

    #@29
    add-int/lit8 v1, v1, 0x1

    #@2b
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@2e
    goto :goto_14

    #@2f
    .line 961
    :cond_2f
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    #@32
    move-result v0

    #@33
    goto :goto_14
.end method

.method distanceInfluenceForSnapDuration(F)F
    .registers 6
    .parameter "f"

    #@0
    .prologue
    .line 1766
    const/high16 v0, 0x3f00

    #@2
    sub-float/2addr p1, v0

    #@3
    .line 1767
    float-to-double v0, p1

    #@4
    const-wide v2, 0x3fde28c7460698c7L

    #@9
    mul-double/2addr v0, v2

    #@a
    double-to-float p1, v0

    #@b
    .line 1768
    float-to-double v0, p1

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-float v0, v0

    #@11
    return v0
.end method

.method endReordering()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2189
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mReorderingStarted:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 2216
    :cond_5
    :goto_5
    return-void

    #@6
    .line 2190
    :cond_6
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mReorderingStarted:Z

    #@8
    .line 2194
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/PagedView$6;

    #@a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V

    #@d
    .line 2200
    .local v0, onCompleteRunnable:Ljava/lang/Runnable;
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@f
    if-nez v1, :cond_5

    #@11
    .line 2201
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/PagedView$7;

    #@13
    invoke-direct {v1, p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView$7;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Ljava/lang/Runnable;)V

    #@16
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRunnable:Ljava/lang/Runnable;

    #@18
    .line 2207
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->NUM_ANIMATIONS_RUNNING_BEFORE_ZOOM_OUT:I

    #@1a
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPostReorderingPreZoomInRemainingAnimationCount:I

    #@1c
    .line 2210
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@1e
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexOfChild(Landroid/view/View;)I

    #@21
    move-result v1

    #@22
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(II)V

    #@25
    .line 2212
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->animateDragViewToOriginalPosition()V

    #@28
    goto :goto_5
.end method

.method protected flashScrollingIndicator(Z)V
    .registers 5
    .parameter "animated"

    #@0
    .prologue
    .line 1940
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5
    .line 1941
    if-nez p1, :cond_13

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->showScrollingIndicator(Z)V

    #@b
    .line 1942
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    #@d
    const-wide/16 v1, 0x28a

    #@f
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@12
    .line 1943
    return-void

    #@13
    .line 1941
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_8
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .registers 6
    .parameter "focused"

    #@0
    .prologue
    .line 989
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 990
    .local v0, current:Landroid/view/View;
    move-object v2, p1

    #@7
    .line 992
    .local v2, v:Landroid/view/View;
    :goto_7
    if-ne v2, v0, :cond_d

    #@9
    .line 993
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    #@c
    .line 1003
    :cond_c
    return-void

    #@d
    .line 996
    :cond_d
    if-eq v2, p0, :cond_c

    #@f
    .line 999
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@12
    move-result-object v1

    #@13
    .line 1000
    .local v1, parent:Landroid/view/ViewParent;
    instance-of v3, v1, Landroid/view/View;

    #@15
    if-eqz v3, :cond_c

    #@17
    .line 1001
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@1a
    move-result-object v2

    #@1b
    .end local v2           #v:Landroid/view/View;
    check-cast v2, Landroid/view/View;

    #@1d
    .restart local v2       #v:Landroid/view/View;
    goto :goto_7
.end method

.method protected getBoundedScrollProgress(ILandroid/view/View;I)F
    .registers 6
    .parameter "screenCenter"
    .parameter "v"
    .parameter "page"

    #@0
    .prologue
    .line 1212
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@3
    move-result v1

    #@4
    div-int/lit8 v0, v1, 0x2

    #@6
    .line 1214
    .local v0, halfScreenSize:I
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollX:I

    #@8
    add-int/2addr v1, v0

    #@9
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    #@c
    move-result p1

    #@d
    .line 1215
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    #@10
    move-result p1

    #@11
    .line 1217
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollProgress(ILandroid/view/View;I)F

    #@14
    move-result v1

    #@15
    return v1
.end method

.method protected getChildIndexForRelativeOffset(I)I
    .registers 7
    .parameter "relativeOffset"

    #@0
    .prologue
    .line 1695
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 1698
    .local v0, childCount:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_1d

    #@7
    .line 1699
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@a
    move-result v2

    #@b
    .line 1700
    .local v2, left:I
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@12
    move-result v4

    #@13
    add-int v3, v2, v4

    #@15
    .line 1701
    .local v3, right:I
    if-gt v2, p1, :cond_1a

    #@17
    if-gt p1, v3, :cond_1a

    #@19
    .line 1705
    .end local v1           #i:I
    .end local v2           #left:I
    .end local v3           #right:I
    :goto_19
    return v1

    #@1a
    .line 1698
    .restart local v1       #i:I
    .restart local v2       #left:I
    .restart local v3       #right:I
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 1705
    .end local v2           #left:I
    .end local v3           #right:I
    :cond_1d
    const/4 v1, -0x1

    #@1e
    goto :goto_19
.end method

.method protected getChildOffset(I)I
    .registers 7
    .parameter "index"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 781
    if-ltz p1, :cond_b

    #@3
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@6
    move-result v3

    #@7
    add-int/lit8 v3, v3, -0x1

    #@9
    if-le p1, v3, :cond_c

    #@b
    .line 799
    :cond_b
    :goto_b
    return v2

    #@c
    .line 783
    :cond_c
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLayoutScale:F

    #@e
    const/high16 v4, 0x3f80

    #@10
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_22

    #@16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsets:[I

    #@18
    .line 786
    .local v0, childOffsets:[I
    :goto_18
    if-eqz v0, :cond_25

    #@1a
    aget v3, v0, p1

    #@1c
    const/4 v4, -0x1

    #@1d
    if-eq v3, v4, :cond_25

    #@1f
    .line 787
    aget v2, v0, p1

    #@21
    goto :goto_b

    #@22
    .line 783
    .end local v0           #childOffsets:[I
    :cond_22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsetsWithLayoutScale:[I

    #@24
    goto :goto_18

    #@25
    .line 789
    .restart local v0       #childOffsets:[I
    :cond_25
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_b

    #@2b
    .line 792
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@2e
    move-result v2

    #@2f
    .line 793
    .local v2, offset:I
    const/4 v1, 0x0

    #@30
    .local v1, i:I
    :goto_30
    if-ge v1, p1, :cond_41

    #@32
    .line 794
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@39
    move-result v3

    #@3a
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@3c
    add-int/2addr v3, v4

    #@3d
    add-int/2addr v2, v3

    #@3e
    .line 793
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_30

    #@41
    .line 796
    :cond_41
    if-eqz v0, :cond_b

    #@43
    .line 797
    aput v2, v0, p1

    #@45
    goto :goto_b
.end method

.method protected getChildWidth(I)I
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 1711
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    #@7
    move-result v0

    #@8
    .line 1712
    .local v0, measuredWidth:I
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinimumWidth:I

    #@a
    .line 1713
    .local v1, minWidth:I
    if-le v1, v0, :cond_d

    #@c
    .end local v1           #minWidth:I
    :goto_c
    return v1

    #@d
    .restart local v1       #minWidth:I
    :cond_d
    move v1, v0

    #@e
    goto :goto_c
.end method

.method getCurrentPage()I
    .registers 2

    #@0
    .prologue
    .line 402
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2
    return v0
.end method

.method protected getMaxScrollProgress()F
    .registers 2

    #@0
    .prologue
    .line 1208
    const/high16 v0, 0x3f80

    #@2
    return v0
.end method

.method getNextPage()I
    .registers 3

    #@0
    .prologue
    .line 406
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@2
    const/4 v1, -0x1

    #@3
    if-eq v0, v1, :cond_8

    #@5
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@a
    goto :goto_7
.end method

.method getPageAt(I)Landroid/view/View;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method getPageCount()I
    .registers 2

    #@0
    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPageForView(Landroid/view/View;)I
    .registers 7
    .parameter "v"

    #@0
    .prologue
    .line 1881
    const/4 v2, -0x1

    #@1
    .line 1882
    .local v2, result:I
    if-eqz p1, :cond_18

    #@3
    .line 1883
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@6
    move-result-object v3

    #@7
    .line 1884
    .local v3, vp:Landroid/view/ViewParent;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@a
    move-result v0

    #@b
    .line 1885
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_18

    #@e
    .line 1886
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@11
    move-result-object v4

    #@12
    if-ne v3, v4, :cond_15

    #@14
    .line 1891
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #vp:Landroid/view/ViewParent;
    :goto_14
    return v1

    #@15
    .line 1885
    .restart local v0       #count:I
    .restart local v1       #i:I
    .restart local v3       #vp:Landroid/view/ViewParent;
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_c

    #@18
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #vp:Landroid/view/ViewParent;
    :cond_18
    move v1, v2

    #@19
    .line 1891
    goto :goto_14
.end method

.method getPageNearestToCenterOfScreen()I
    .registers 13

    #@0
    .prologue
    .line 1729
    const v7, 0x7fffffff

    #@3
    .line 1730
    .local v7, minDistanceFromScreenCenter:I
    const/4 v8, -0x1

    #@4
    .line 1731
    .local v8, minDistanceFromScreenCenterIndex:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@7
    move-result v10

    #@8
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@b
    move-result v11

    #@c
    add-int/2addr v10, v11

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@10
    move-result v11

    #@11
    div-int/lit8 v11, v11, 0x2

    #@13
    add-int v9, v10, v11

    #@15
    .line 1732
    .local v9, screenCenter:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@18
    move-result v1

    #@19
    .line 1733
    .local v1, childCount:I
    const/4 v5, 0x0

    #@1a
    .local v5, i:I
    :goto_1a
    if-ge v5, v1, :cond_3e

    #@1c
    .line 1734
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@1f
    move-result-object v6

    #@20
    .line 1735
    .local v6, layout:Landroid/view/View;
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@23
    move-result v2

    #@24
    .line 1736
    .local v2, childWidth:I
    div-int/lit8 v4, v2, 0x2

    #@26
    .line 1737
    .local v4, halfChildWidth:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@29
    move-result v10

    #@2a
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@2d
    move-result v11

    #@2e
    add-int/2addr v10, v11

    #@2f
    add-int v0, v10, v4

    #@31
    .line 1738
    .local v0, childCenter:I
    sub-int v10, v0, v9

    #@33
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@36
    move-result v3

    #@37
    .line 1739
    .local v3, distanceFromScreenCenter:I
    if-ge v3, v7, :cond_3b

    #@39
    .line 1740
    move v7, v3

    #@3a
    .line 1741
    move v8, v5

    #@3b
    .line 1733
    :cond_3b
    add-int/lit8 v5, v5, 0x1

    #@3d
    goto :goto_1a

    #@3e
    .line 1744
    .end local v0           #childCenter:I
    .end local v2           #childWidth:I
    .end local v3           #distanceFromScreenCenter:I
    .end local v4           #halfChildWidth:I
    .end local v6           #layout:Landroid/view/View;
    :cond_3e
    return v8
.end method

.method getPageNearestToPoint(F)I
    .registers 6
    .parameter "x"

    #@0
    .prologue
    .line 1717
    const/4 v1, 0x0

    #@1
    .line 1718
    .local v1, index:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@5
    move-result v2

    #@6
    if-ge v0, v2, :cond_20

    #@8
    .line 1719
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@13
    move-result v3

    #@14
    sub-int/2addr v2, v3

    #@15
    int-to-float v2, v2

    #@16
    cmpg-float v2, p1, v2

    #@18
    if-gez v2, :cond_1b

    #@1a
    .line 1725
    .end local v1           #index:I
    :goto_1a
    return v1

    #@1b
    .line 1722
    .restart local v1       #index:I
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    .line 1718
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_2

    #@20
    .line 1725
    :cond_20
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@23
    move-result v2

    #@24
    add-int/lit8 v2, v2, -0x1

    #@26
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@29
    move-result v1

    #@2a
    goto :goto_1a
.end method

.method protected getRelativeChildOffset(I)I
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 804
    if-ltz p1, :cond_a

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v2, v2, -0x1

    #@8
    if-le p1, v2, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 815
    :cond_b
    :goto_b
    return v0

    #@c
    .line 806
    :cond_c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@e
    if-eqz v2, :cond_1c

    #@10
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@12
    aget v2, v2, p1

    #@14
    const/4 v3, -0x1

    #@15
    if-eq v2, v3, :cond_1c

    #@17
    .line 807
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@19
    aget v0, v2, p1

    #@1b
    goto :goto_b

    #@1c
    .line 809
    :cond_1c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingLeft()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingRight()I

    #@23
    move-result v3

    #@24
    add-int v1, v2, v3

    #@26
    .line 810
    .local v1, padding:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingLeft()I

    #@29
    move-result v2

    #@2a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@2d
    move-result v3

    #@2e
    sub-int/2addr v3, v1

    #@2f
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildWidth(I)I

    #@32
    move-result v4

    #@33
    sub-int/2addr v3, v4

    #@34
    div-int/lit8 v3, v3, 0x2

    #@36
    add-int v0, v2, v3

    #@38
    .line 812
    .local v0, offset:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@3a
    if-eqz v2, :cond_b

    #@3c
    .line 813
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@3e
    aput v0, v2, p1

    #@40
    goto :goto_b
.end method

.method protected getScaledMeasuredWidth(Landroid/view/View;)I
    .registers 7
    .parameter "child"

    #@0
    .prologue
    .line 822
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3
    move-result v1

    #@4
    .line 823
    .local v1, measuredWidth:I
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinimumWidth:I

    #@6
    .line 824
    .local v2, minWidth:I
    if-le v2, v1, :cond_12

    #@8
    move v0, v2

    #@9
    .line 825
    .local v0, maxWidth:I
    :goto_9
    int-to-float v3, v0

    #@a
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLayoutScale:F

    #@c
    mul-float/2addr v3, v4

    #@d
    const/high16 v4, 0x3f00

    #@f
    add-float/2addr v3, v4

    #@10
    float-to-int v3, v3

    #@11
    return v3

    #@12
    .end local v0           #maxWidth:I
    :cond_12
    move v0, v1

    #@13
    .line 824
    goto :goto_9
.end method

.method protected getScrollProgress(ILandroid/view/View;I)F
    .registers 11
    .parameter "screenCenter"
    .parameter "v"
    .parameter "page"

    #@0
    .prologue
    .line 1221
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@3
    move-result v4

    #@4
    div-int/lit8 v1, v4, 0x2

    #@6
    .line 1223
    .local v1, halfScreenSize:I
    invoke-virtual {p0, p2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@9
    move-result v4

    #@a
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@c
    add-int v3, v4, v5

    #@e
    .line 1224
    .local v3, totalDistance:I
    invoke-virtual {p0, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@11
    move-result v4

    #@12
    invoke-virtual {p0, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@15
    move-result v5

    #@16
    sub-int/2addr v4, v5

    #@17
    add-int/2addr v4, v1

    #@18
    sub-int v0, p1, v4

    #@1a
    .line 1227
    .local v0, delta:I
    int-to-float v4, v0

    #@1b
    int-to-float v5, v3

    #@1c
    const/high16 v6, 0x3f80

    #@1e
    mul-float/2addr v5, v6

    #@1f
    div-float v2, v4, v5

    #@21
    .line 1228
    .local v2, scrollProgress:F
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMaxScrollProgress()F

    #@24
    move-result v4

    #@25
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    #@28
    move-result v2

    #@29
    .line 1229
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMaxScrollProgress()F

    #@2c
    move-result v4

    #@2d
    neg-float v4, v4

    #@2e
    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    #@31
    move-result v2

    #@32
    .line 1230
    return v2
.end method

.method protected getScrollingIndicator()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1925
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method getViewportHeight()I
    .registers 2

    #@0
    .prologue
    .line 365
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getViewportOffsetX()I
    .registers 3

    #@0
    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMeasuredWidth()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    div-int/lit8 v0, v0, 0x2

    #@b
    return v0
.end method

.method getViewportOffsetY()I
    .registers 3

    #@0
    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMeasuredHeight()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportHeight()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    div-int/lit8 v0, v0, 0x2

    #@b
    return v0
.end method

.method getViewportWidth()I
    .registers 2

    #@0
    .prologue
    .line 362
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected getVisiblePages([I)V
    .registers 4
    .parameter "range"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 834
    aput v0, p1, v0

    #@3
    .line 835
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    aput v1, p1, v0

    #@c
    .line 869
    return-void
.end method

.method protected hasElasticScrollIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 2012
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected hideScrollingIndicator(Z)V
    .registers 7
    .parameter "immediately"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1975
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@5
    move-result v0

    #@6
    if-gt v0, v2, :cond_9

    #@8
    .line 2005
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1976
    :cond_9
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isScrollingIndicatorEnabled()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_8

    #@f
    .line 1978
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollingIndicator()Landroid/view/View;

    #@12
    .line 1979
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@14
    if-eqz v0, :cond_8

    #@16
    .line 1981
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateScrollingIndicatorPosition()V

    #@19
    .line 1982
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->cancelScrollingIndicatorAnimations()V

    #@1c
    .line 1983
    if-eqz p1, :cond_2a

    #@1e
    .line 1984
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@20
    const/4 v1, 0x4

    #@21
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@24
    .line 1985
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@26
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    #@29
    goto :goto_8

    #@2a
    .line 1987
    :cond_2a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@2c
    const-string v1, "alpha"

    #@2e
    new-array v2, v2, [F

    #@30
    const/4 v3, 0x0

    #@31
    aput v4, v2, v3

    #@33
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@39
    .line 1988
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@3b
    const-wide/16 v1, 0x28a

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@40
    .line 1989
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@42
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/PagedView$3;

    #@44
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V

    #@47
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@4a
    .line 2002
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@4c
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@4f
    goto :goto_8
.end method

.method protected hitsNextPage(FF)Z
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1019
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@b
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@e
    move-result v1

    #@f
    sub-int/2addr v0, v1

    #@10
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@12
    add-int/2addr v0, v1

    #@13
    int-to-float v0, v0

    #@14
    cmpl-float v0, p1, v0

    #@16
    if-lez v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method protected hitsPreviousPage(FF)Z
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1012
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@9
    move-result v1

    #@a
    add-int/2addr v0, v1

    #@b
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@d
    sub-int/2addr v0, v1

    #@e
    int-to-float v0, v0

    #@f
    cmpg-float v0, p1, v0

    #@11
    if-gez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method protected indexToPage(I)I
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 418
    return p1
.end method

.method protected init()V
    .registers 5

    #@0
    .prologue
    .line 292
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    #@7
    .line 293
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    #@9
    const/16 v2, 0x20

    #@b
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->ensureCapacity(I)V

    #@e
    .line 294
    new-instance v1, Landroid/widget/Scroller;

    #@10
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getContext()Landroid/content/Context;

    #@13
    move-result-object v2

    #@14
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/PagedView$ScrollInterpolator;

    #@16
    invoke-direct {v3}, Lcom/android/internal/policy/impl/keyguard/PagedView$ScrollInterpolator;-><init>()V

    #@19
    invoke-direct {v1, v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    #@1c
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@1e
    .line 295
    const/4 v1, 0x0

    #@1f
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@21
    .line 297
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getContext()Landroid/content/Context;

    #@24
    move-result-object v1

    #@25
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@28
    move-result-object v0

    #@29
    .line 298
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchSlop:I

    #@2f
    .line 299
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    #@32
    move-result v1

    #@33
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPagingTouchSlop:I

    #@35
    .line 300
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@38
    move-result v1

    #@39
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaximumVelocity:I

    #@3b
    .line 301
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@42
    move-result-object v1

    #@43
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    #@45
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@47
    .line 304
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingToDeleteThresholdVelocity:I

    #@49
    int-to-float v1, v1

    #@4a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@4c
    mul-float/2addr v1, v2

    #@4d
    float-to-int v1, v1

    #@4e
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingToDeleteThresholdVelocity:I

    #@50
    .line 307
    const/high16 v1, 0x43fa

    #@52
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@54
    mul-float/2addr v1, v2

    #@55
    float-to-int v1, v1

    #@56
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingThresholdVelocity:I

    #@58
    .line 308
    const/high16 v1, 0x437a

    #@5a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@5c
    mul-float/2addr v1, v2

    #@5d
    float-to-int v1, v1

    #@5e
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinFlingVelocity:I

    #@60
    .line 309
    const v1, 0x44bb8000

    #@63
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@65
    mul-float/2addr v1, v2

    #@66
    float-to-int v1, v1

    #@67
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinSnapVelocity:I

    #@69
    .line 310
    invoke-virtual {p0, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@6c
    .line 311
    return-void
.end method

.method protected invalidateCachedOffsets()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 762
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 763
    .local v0, count:I
    if-nez v0, :cond_f

    #@8
    .line 764
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsets:[I

    #@a
    .line 765
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@c
    .line 766
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsetsWithLayoutScale:[I

    #@e
    .line 778
    :cond_e
    return-void

    #@f
    .line 770
    :cond_f
    new-array v2, v0, [I

    #@11
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsets:[I

    #@13
    .line 771
    new-array v2, v0, [I

    #@15
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@17
    .line 772
    new-array v2, v0, [I

    #@19
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsetsWithLayoutScale:[I

    #@1b
    .line 773
    const/4 v1, 0x0

    #@1c
    .local v1, i:I
    :goto_1c
    if-ge v1, v0, :cond_e

    #@1e
    .line 774
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsets:[I

    #@20
    aput v3, v2, v1

    #@22
    .line 775
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildRelativeOffsets:[I

    #@24
    aput v3, v2, v1

    #@26
    .line 776
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildOffsetsWithLayoutScale:[I

    #@28
    aput v3, v2, v1

    #@2a
    .line 773
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_1c
.end method

.method protected isDataReady()Z
    .registers 2

    #@0
    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsDataReady:Z

    #@2
    return v0
.end method

.method protected isPageMoving()Z
    .registers 2

    #@0
    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@2
    return v0
.end method

.method isReordering(Z)Z
    .registers 5
    .parameter "testTouchState"

    #@0
    .prologue
    .line 2180
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsReordering:Z

    #@2
    .line 2181
    .local v0, state:Z
    if-eqz p1, :cond_b

    #@4
    .line 2182
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@6
    const/4 v2, 0x4

    #@7
    if-ne v1, v2, :cond_c

    #@9
    const/4 v1, 0x1

    #@a
    :goto_a
    and-int/2addr v0, v1

    #@b
    .line 2184
    :cond_b
    return v0

    #@c
    .line 2182
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method protected isScrollingIndicatorEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1929
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method mapPointFromParentToView(Landroid/view/View;FF)[F
    .registers 7
    .parameter "v"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@6
    move-result v2

    #@7
    int-to-float v2, v2

    #@8
    sub-float v2, p2, v2

    #@a
    aput v2, v0, v1

    #@c
    .line 328
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@e
    const/4 v1, 0x1

    #@f
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@12
    move-result v2

    #@13
    int-to-float v2, v2

    #@14
    sub-float v2, p3, v2

    #@16
    aput v2, v0, v1

    #@18
    .line 329
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpInvMatrix:Landroid/graphics/Matrix;

    #@1e
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    #@21
    .line 330
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpInvMatrix:Landroid/graphics/Matrix;

    #@23
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@25
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@28
    .line 331
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@2a
    return-object v0
.end method

.method mapPointFromViewToParent(Landroid/view/View;FF)[F
    .registers 9
    .parameter "v"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 319
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@4
    aput p2, v0, v3

    #@6
    .line 320
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@8
    aput p3, v0, v4

    #@a
    .line 321
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@10
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@13
    .line 322
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@15
    aget v1, v0, v3

    #@17
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@1a
    move-result v2

    #@1b
    int-to-float v2, v2

    #@1c
    add-float/2addr v1, v2

    #@1d
    aput v1, v0, v3

    #@1f
    .line 323
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@21
    aget v1, v0, v4

    #@23
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@26
    move-result v2

    #@27
    int-to-float v2, v2

    #@28
    add-float/2addr v1, v2

    #@29
    aput v1, v0, v4

    #@2b
    .line 324
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTmpPoint:[F

    #@2d
    return-object v0
.end method

.method protected maxOverScroll()F
    .registers 4

    #@0
    .prologue
    .line 1296
    const/high16 v0, 0x3f80

    #@2
    .line 1297
    .local v0, f:F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@5
    move-result v1

    #@6
    div-float v1, v0, v1

    #@8
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@b
    move-result v2

    #@c
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->overScrollInfluenceCurve(F)F

    #@f
    move-result v2

    #@10
    mul-float v0, v1, v2

    #@12
    .line 1298
    const v1, 0x3e0f5c29

    #@15
    mul-float/2addr v1, v0

    #@16
    return v1
.end method

.method protected notifyPageSwitched()V
    .registers 4

    #@0
    .prologue
    .line 468
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 469
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@6
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@8
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@e
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;->onPageSwitched(Landroid/view/View;I)V

    #@11
    .line 471
    :cond_11
    return-void
.end method

.method protected notifyPageSwitching(I)V
    .registers 4
    .parameter "whichPage"

    #@0
    .prologue
    .line 462
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 463
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@6
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v0, v1, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;->onPageSwitching(Landroid/view/View;I)V

    #@d
    .line 465
    :cond_d
    return-void
.end method

.method public abstract onAddView(Landroid/view/View;I)V
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 751
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@3
    .line 752
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@6
    .line 753
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidateCachedOffsets()V

    #@9
    .line 754
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 758
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@3
    .line 759
    return-void
.end method

.method protected onEndReordering()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2142
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_1a

    #@e
    .line 2143
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@10
    const v2, 0x1040350

    #@13
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@1a
    .line 2146
    :cond_1a
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsReordering:Z

    #@1c
    .line 2149
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@1e
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getVisiblePages([I)V

    #@21
    .line 2150
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@23
    invoke-virtual {p0, v4, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->boundByReorderablePages(Z[I)V

    #@26
    .line 2151
    const/4 v0, 0x0

    #@27
    .local v0, i:I
    :goto_27
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@2a
    move-result v1

    #@2b
    if-ge v0, v1, :cond_45

    #@2d
    .line 2152
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2f
    aget v1, v1, v3

    #@31
    if-lt v0, v1, :cond_39

    #@33
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@35
    aget v1, v1, v4

    #@37
    if-le v0, v1, :cond_42

    #@39
    .line 2153
    :cond_39
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@3c
    move-result-object v1

    #@3d
    const/high16 v2, 0x3f80

    #@3f
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    #@42
    .line 2151
    :cond_42
    add-int/lit8 v0, v0, 0x1

    #@44
    goto :goto_27

    #@45
    .line 2156
    :cond_45
    return-void
.end method

.method public onFlingToDelete(Landroid/graphics/PointF;)V
    .registers 14
    .parameter "vel"

    #@0
    .prologue
    .line 2421
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v4

    #@4
    .line 2428
    .local v4, startTime:J
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/PagedView$10;

    #@6
    invoke-direct {v9, p0, v4, v5}, Lcom/android/internal/policy/impl/keyguard/PagedView$10;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;J)V

    #@9
    .line 2449
    .local v9, tInterpolator:Landroid/animation/TimeInterpolator;
    new-instance v3, Landroid/graphics/Rect;

    #@b
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@e
    .line 2450
    .local v3, from:Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@10
    .line 2451
    .local v1, dragView:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    #@13
    move-result v2

    #@14
    float-to-int v2, v2

    #@15
    iput v2, v3, Landroid/graphics/Rect;->left:I

    #@17
    .line 2452
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    #@1a
    move-result v2

    #@1b
    float-to-int v2, v2

    #@1c
    iput v2, v3, Landroid/graphics/Rect;->top:I

    #@1e
    .line 2453
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;

    #@20
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_FRICTION:F

    #@22
    move-object v2, p1

    #@23
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/policy/impl/keyguard/PagedView$FlingAlongVectorAnimatorUpdateListener;-><init>(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V

    #@26
    .line 2456
    .local v0, updateCb:Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->createPostDeleteAnimationRunnable(Landroid/view/View;)Ljava/lang/Runnable;

    #@29
    move-result-object v8

    #@2a
    .line 2459
    .local v8, onAnimationEndRunnable:Ljava/lang/Runnable;
    new-instance v7, Landroid/animation/ValueAnimator;

    #@2c
    invoke-direct {v7}, Landroid/animation/ValueAnimator;-><init>()V

    #@2f
    .line 2460
    .local v7, mDropAnim:Landroid/animation/ValueAnimator;
    invoke-virtual {v7, v9}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@32
    .line 2461
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->FLING_TO_DELETE_FADE_OUT_DURATION:I

    #@34
    int-to-long v10, v2

    #@35
    invoke-virtual {v7, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@38
    .line 2462
    const/4 v2, 0x2

    #@39
    new-array v2, v2, [F

    #@3b
    fill-array-data v2, :array_54

    #@3e
    invoke-virtual {v7, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@41
    .line 2463
    invoke-virtual {v7, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@44
    .line 2464
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/PagedView$11;

    #@46
    invoke-direct {v2, p0, v8}, Lcom/android/internal/policy/impl/keyguard/PagedView$11;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Ljava/lang/Runnable;)V

    #@49
    invoke-virtual {v7, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@4c
    .line 2469
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->start()V

    #@4f
    .line 2470
    const/4 v2, 0x1

    #@50
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@52
    .line 2471
    return-void

    #@53
    .line 2462
    nop

    #@54
    :array_54
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/16 v4, 0x9

    #@2
    const/4 v3, 0x0

    #@3
    .line 1625
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@6
    move-result v2

    #@7
    and-int/lit8 v2, v2, 0x2

    #@9
    if-eqz v2, :cond_12

    #@b
    .line 1626
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v2

    #@f
    packed-switch v2, :pswitch_data_4a

    #@12
    .line 1649
    :cond_12
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@15
    move-result v2

    #@16
    :goto_16
    return v2

    #@17
    .line 1631
    :pswitch_17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@1a
    move-result v2

    #@1b
    and-int/lit8 v2, v2, 0x1

    #@1d
    if-eqz v2, :cond_39

    #@1f
    .line 1632
    const/4 v1, 0x0

    #@20
    .line 1633
    .local v1, vscroll:F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@23
    move-result v0

    #@24
    .line 1638
    .local v0, hscroll:F
    :goto_24
    cmpl-float v2, v0, v3

    #@26
    if-nez v2, :cond_2c

    #@28
    cmpl-float v2, v1, v3

    #@2a
    if-eqz v2, :cond_12

    #@2c
    .line 1639
    :cond_2c
    cmpl-float v2, v0, v3

    #@2e
    if-gtz v2, :cond_34

    #@30
    cmpl-float v2, v1, v3

    #@32
    if-lez v2, :cond_45

    #@34
    .line 1640
    :cond_34
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollRight()V

    #@37
    .line 1644
    :goto_37
    const/4 v2, 0x1

    #@38
    goto :goto_16

    #@39
    .line 1635
    .end local v0           #hscroll:F
    .end local v1           #vscroll:F
    :cond_39
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@3c
    move-result v2

    #@3d
    neg-float v1, v2

    #@3e
    .line 1636
    .restart local v1       #vscroll:F
    const/16 v2, 0xa

    #@40
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@43
    move-result v0

    #@44
    .restart local v0       #hscroll:F
    goto :goto_24

    #@45
    .line 1642
    :cond_45
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollLeft()V

    #@48
    goto :goto_37

    #@49
    .line 1626
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x8
        :pswitch_17
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2574
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 2541
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 2542
    const/4 v0, 0x1

    #@4
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@7
    .line 2543
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@a
    move-result v0

    #@b
    const/16 v1, 0x1000

    #@d
    if-ne v0, v1, :cond_20

    #@f
    .line 2544
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@11
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@14
    .line 2545
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@16
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    #@19
    .line 2546
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@1c
    move-result v0

    #@1d
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@20
    .line 2548
    :cond_20
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2529
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@4
    .line 2530
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@7
    move-result v1

    #@8
    if-le v1, v0, :cond_2a

    #@a
    :goto_a
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@d
    .line 2531
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@10
    move-result v0

    #@11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@14
    move-result v1

    #@15
    add-int/lit8 v1, v1, -0x1

    #@17
    if-ge v0, v1, :cond_1e

    #@19
    .line 2532
    const/16 v0, 0x1000

    #@1b
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@1e
    .line 2534
    :cond_1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@21
    move-result v0

    #@22
    if-lez v0, :cond_29

    #@24
    .line 2535
    const/16 v0, 0x2000

    #@26
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@29
    .line 2537
    :cond_29
    return-void

    #@2a
    .line 2530
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_a
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 1051
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    #@6
    .line 1054
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@9
    move-result v10

    #@a
    if-gtz v10, :cond_11

    #@c
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@f
    move-result v8

    #@10
    .line 1160
    :cond_10
    :goto_10
    return v8

    #@11
    .line 1061
    :cond_11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@14
    move-result v0

    #@15
    .line 1062
    .local v0, action:I
    const/4 v10, 0x2

    #@16
    if-ne v0, v10, :cond_1c

    #@18
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@1a
    if-eq v10, v8, :cond_10

    #@1c
    .line 1067
    :cond_1c
    and-int/lit16 v10, v0, 0xff

    #@1e
    packed-switch v10, :pswitch_data_da

    #@21
    .line 1160
    :cond_21
    :goto_21
    :pswitch_21
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@23
    if-nez v10, :cond_10

    #@25
    move v8, v9

    #@26
    goto :goto_10

    #@27
    .line 1073
    :pswitch_27
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@29
    const/4 v11, -0x1

    #@2a
    if-eq v10, v11, :cond_30

    #@2c
    .line 1074
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    #@2f
    goto :goto_21

    #@30
    .line 1085
    :cond_30
    :pswitch_30
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@33
    move-result v5

    #@34
    .line 1086
    .local v5, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@37
    move-result v7

    #@38
    .line 1088
    .local v7, y:F
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@3a
    .line 1089
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionY:F

    #@3c
    .line 1090
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@3f
    move-result v10

    #@40
    int-to-float v10, v10

    #@41
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownScrollX:F

    #@43
    .line 1091
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@45
    .line 1092
    iput v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@47
    .line 1093
    invoke-virtual {p0, p0, v5, v7}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@4a
    move-result-object v3

    #@4b
    .line 1094
    .local v3, p:[F
    aget v10, v3, v9

    #@4d
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@4f
    .line 1095
    aget v10, v3, v8

    #@51
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@53
    .line 1096
    iput v12, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@55
    .line 1097
    iput v12, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@57
    .line 1098
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@5a
    move-result v10

    #@5b
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@5d
    .line 1101
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@60
    move-result v10

    #@61
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@63
    add-int v2, v10, v11

    #@65
    .line 1102
    .local v2, leftEdgeBoundary:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMeasuredWidth()I

    #@68
    move-result v10

    #@69
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@6c
    move-result v11

    #@6d
    sub-int/2addr v10, v11

    #@6e
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@70
    sub-int v4, v10, v11

    #@72
    .line 1103
    .local v4, rightEdgeBoundary:I
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@74
    int-to-float v11, v2

    #@75
    cmpg-float v10, v10, v11

    #@77
    if-lez v10, :cond_80

    #@79
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@7b
    int-to-float v11, v4

    #@7c
    cmpl-float v10, v10, v11

    #@7e
    if-ltz v10, :cond_82

    #@80
    .line 1104
    :cond_80
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownEventOnEdge:Z

    #@82
    .line 1112
    :cond_82
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@84
    invoke-virtual {v10}, Landroid/widget/Scroller;->getFinalX()I

    #@87
    move-result v10

    #@88
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@8a
    invoke-virtual {v11}, Landroid/widget/Scroller;->getCurrX()I

    #@8d
    move-result v11

    #@8e
    sub-int/2addr v10, v11

    #@8f
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@92
    move-result v6

    #@93
    .line 1113
    .local v6, xDist:I
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@95
    invoke-virtual {v10}, Landroid/widget/Scroller;->isFinished()Z

    #@98
    move-result v10

    #@99
    if-nez v10, :cond_9f

    #@9b
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchSlop:I

    #@9d
    if-ge v6, v10, :cond_ab

    #@9f
    :cond_9f
    move v1, v8

    #@a0
    .line 1114
    .local v1, finishedScrolling:Z
    :goto_a0
    if-eqz v1, :cond_ad

    #@a2
    .line 1115
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@a4
    .line 1116
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@a6
    invoke-virtual {v10}, Landroid/widget/Scroller;->abortAnimation()V

    #@a9
    goto/16 :goto_21

    #@ab
    .end local v1           #finishedScrolling:Z
    :cond_ab
    move v1, v9

    #@ac
    .line 1113
    goto :goto_a0

    #@ad
    .line 1118
    .restart local v1       #finishedScrolling:Z
    :cond_ad
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@af
    float-to-int v10, v10

    #@b0
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionY:F

    #@b2
    float-to-int v11, v11

    #@b3
    invoke-direct {p0, v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isTouchPointInViewportWithBuffer(II)Z

    #@b6
    move-result v10

    #@b7
    if-eqz v10, :cond_bd

    #@b9
    .line 1119
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@bb
    goto/16 :goto_21

    #@bd
    .line 1121
    :cond_bd
    iput v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@bf
    goto/16 :goto_21

    #@c1
    .line 1143
    .end local v1           #finishedScrolling:Z
    .end local v2           #leftEdgeBoundary:I
    .end local v3           #p:[F
    .end local v4           #rightEdgeBoundary:I
    .end local v5           #x:F
    .end local v6           #xDist:I
    .end local v7           #y:F
    :pswitch_c1
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->resetTouchState()V

    #@c4
    .line 1145
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@c6
    float-to-int v10, v10

    #@c7
    iget v11, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@c9
    float-to-int v11, v11

    #@ca
    invoke-direct {p0, v10, v11}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isTouchPointInCurrentPage(II)Z

    #@cd
    move-result v10

    #@ce
    if-nez v10, :cond_21

    #@d0
    goto/16 :goto_10

    #@d2
    .line 1151
    :pswitch_d2
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@d5
    .line 1152
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->releaseVelocityTracker()V

    #@d8
    goto/16 :goto_21

    #@da
    .line 1067
    :pswitch_data_da
    .packed-switch 0x0
        :pswitch_30
        :pswitch_c1
        :pswitch_27
        :pswitch_c1
        :pswitch_21
        :pswitch_21
        :pswitch_d2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 17
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 708
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsDataReady:Z

    #@2
    if-eqz v9, :cond_a

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@7
    move-result v9

    #@8
    if-nez v9, :cond_b

    #@a
    .line 742
    :cond_a
    :goto_a
    return-void

    #@b
    .line 713
    :cond_b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@e
    move-result v1

    #@f
    .line 715
    .local v1, childCount:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@12
    move-result v7

    #@13
    .line 716
    .local v7, offsetX:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetY()I

    #@16
    move-result v8

    #@17
    .line 719
    .local v8, offsetY:I
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@19
    invoke-virtual {v9, v7, v8}, Landroid/graphics/Rect;->offset(II)V

    #@1c
    .line 721
    const/4 v9, 0x0

    #@1d
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@20
    move-result v9

    #@21
    add-int v3, v7, v9

    #@23
    .line 722
    .local v3, childLeft:I
    const/4 v6, 0x0

    #@24
    .local v6, i:I
    :goto_24
    if-ge v6, v1, :cond_51

    #@26
    .line 723
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@29
    move-result-object v0

    #@2a
    .line 724
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingTop()I

    #@2d
    move-result v9

    #@2e
    add-int v4, v8, v9

    #@30
    .line 725
    .local v4, childTop:I
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@33
    move-result v9

    #@34
    const/16 v10, 0x8

    #@36
    if-eq v9, v10, :cond_4e

    #@38
    .line 726
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@3b
    move-result v5

    #@3c
    .line 727
    .local v5, childWidth:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@3f
    move-result v2

    #@40
    .line 730
    .local v2, childHeight:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@43
    move-result v9

    #@44
    add-int/2addr v9, v3

    #@45
    add-int v10, v4, v2

    #@47
    invoke-virtual {v0, v3, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    #@4a
    .line 732
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@4c
    add-int/2addr v9, v5

    #@4d
    add-int/2addr v3, v9

    #@4e
    .line 722
    .end local v2           #childHeight:I
    .end local v5           #childWidth:I
    :cond_4e
    add-int/lit8 v6, v6, 0x1

    #@50
    goto :goto_24

    #@51
    .line 736
    .end local v0           #child:Landroid/view/View;
    .end local v4           #childTop:I
    :cond_51
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFirstLayout:Z

    #@53
    if-eqz v9, :cond_a

    #@55
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@57
    if-ltz v9, :cond_a

    #@59
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@5b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@5e
    move-result v10

    #@5f
    if-ge v9, v10, :cond_a

    #@61
    .line 737
    const/4 v9, 0x0

    #@62
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setHorizontalScrollBarEnabled(Z)V

    #@65
    .line 738
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateCurrentPageScroll()V

    #@68
    .line 739
    const/4 v9, 0x1

    #@69
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setHorizontalScrollBarEnabled(Z)V

    #@6c
    .line 740
    const/4 v9, 0x0

    #@6d
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFirstLayout:Z

    #@6f
    goto :goto_a
.end method

.method protected onMeasure(II)V
    .registers 33
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 588
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsDataReady:Z

    #@4
    move/from16 v27, v0

    #@6
    if-eqz v27, :cond_e

    #@8
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@b
    move-result v27

    #@c
    if-nez v27, :cond_12

    #@e
    .line 589
    :cond_e
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    #@11
    .line 699
    :goto_11
    return-void

    #@12
    .line 595
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getParent()Landroid/view/ViewParent;

    #@15
    move-result-object v18

    #@16
    check-cast v18, Landroid/view/View;

    #@18
    .line 596
    .local v18, parent:Landroid/view/View;
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@1b
    move-result v25

    #@1c
    .line 597
    .local v25, widthMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1f
    move-result v26

    #@20
    .line 598
    .local v26, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@23
    move-result v11

    #@24
    .line 599
    .local v11, heightMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@27
    move-result v12

    #@28
    .line 602
    .local v12, heightSize:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getResources()Landroid/content/res/Resources;

    #@2b
    move-result-object v27

    #@2c
    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@2f
    move-result-object v10

    #@30
    .line 603
    .local v10, dm:Landroid/util/DisplayMetrics;
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@32
    move/from16 v27, v0

    #@34
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@36
    move/from16 v28, v0

    #@38
    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->max(II)I

    #@3b
    move-result v16

    #@3c
    .line 604
    .local v16, maxSize:I
    const/high16 v27, 0x3fc0

    #@3e
    move/from16 v0, v16

    #@40
    int-to-float v0, v0

    #@41
    move/from16 v28, v0

    #@43
    mul-float v27, v27, v28

    #@45
    move/from16 v0, v27

    #@47
    float-to-int v0, v0

    #@48
    move/from16 v20, v0

    #@4a
    .line 605
    .local v20, parentWidthSize:I
    move/from16 v19, v16

    #@4c
    .line 606
    .local v19, parentHeightSize:I
    move/from16 v0, v20

    #@4e
    int-to-float v0, v0

    #@4f
    move/from16 v27, v0

    #@51
    move-object/from16 v0, p0

    #@53
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@55
    move/from16 v28, v0

    #@57
    div-float v27, v27, v28

    #@59
    move/from16 v0, v27

    #@5b
    float-to-int v0, v0

    #@5c
    move/from16 v22, v0

    #@5e
    .line 607
    .local v22, scaledWidthSize:I
    move/from16 v0, v19

    #@60
    int-to-float v0, v0

    #@61
    move/from16 v27, v0

    #@63
    move-object/from16 v0, p0

    #@65
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@67
    move/from16 v28, v0

    #@69
    div-float v27, v27, v28

    #@6b
    move/from16 v0, v27

    #@6d
    float-to-int v0, v0

    #@6e
    move/from16 v21, v0

    #@70
    .line 608
    .local v21, scaledHeightSize:I
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@74
    move-object/from16 v27, v0

    #@76
    const/16 v28, 0x0

    #@78
    const/16 v29, 0x0

    #@7a
    move-object/from16 v0, v27

    #@7c
    move/from16 v1, v28

    #@7e
    move/from16 v2, v29

    #@80
    move/from16 v3, v26

    #@82
    invoke-virtual {v0, v1, v2, v3, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@85
    .line 610
    if-eqz v25, :cond_89

    #@87
    if-nez v11, :cond_8d

    #@89
    .line 611
    :cond_89
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    #@8c
    goto :goto_11

    #@8d
    .line 616
    :cond_8d
    if-lez v26, :cond_91

    #@8f
    if-gtz v12, :cond_96

    #@91
    .line 617
    :cond_91
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    #@94
    goto/16 :goto_11

    #@96
    .line 626
    :cond_96
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingTop()I

    #@99
    move-result v27

    #@9a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingBottom()I

    #@9d
    move-result v28

    #@9e
    add-int v24, v27, v28

    #@a0
    .line 627
    .local v24, verticalPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingLeft()I

    #@a3
    move-result v27

    #@a4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPaddingRight()I

    #@a7
    move-result v28

    #@a8
    add-int v13, v27, v28

    #@aa
    .line 636
    .local v13, horizontalPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@ad
    move-result v5

    #@ae
    .line 637
    .local v5, childCount:I
    const/4 v14, 0x0

    #@af
    .local v14, i:I
    :goto_af
    if-ge v14, v5, :cond_f3

    #@b1
    .line 639
    move-object/from16 v0, p0

    #@b3
    invoke-virtual {v0, v14}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@b6
    move-result-object v4

    #@b7
    .line 640
    .local v4, child:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@ba
    move-result-object v15

    #@bb
    .line 643
    .local v15, lp:Landroid/view/ViewGroup$LayoutParams;
    iget v0, v15, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@bd
    move/from16 v27, v0

    #@bf
    const/16 v28, -0x2

    #@c1
    move/from16 v0, v27

    #@c3
    move/from16 v1, v28

    #@c5
    if-ne v0, v1, :cond_ed

    #@c7
    .line 644
    const/high16 v9, -0x8000

    #@c9
    .line 650
    .local v9, childWidthMode:I
    :goto_c9
    iget v0, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@cb
    move/from16 v27, v0

    #@cd
    const/16 v28, -0x2

    #@cf
    move/from16 v0, v27

    #@d1
    move/from16 v1, v28

    #@d3
    if-ne v0, v1, :cond_f0

    #@d5
    .line 651
    const/high16 v7, -0x8000

    #@d7
    .line 656
    .local v7, childHeightMode:I
    :goto_d7
    sub-int v27, v26, v13

    #@d9
    move/from16 v0, v27

    #@db
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@de
    move-result v8

    #@df
    .line 658
    .local v8, childWidthMeasureSpec:I
    sub-int v27, v12, v24

    #@e1
    move/from16 v0, v27

    #@e3
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@e6
    move-result v6

    #@e7
    .line 661
    .local v6, childHeightMeasureSpec:I
    invoke-virtual {v4, v8, v6}, Landroid/view/View;->measure(II)V

    #@ea
    .line 637
    add-int/lit8 v14, v14, 0x1

    #@ec
    goto :goto_af

    #@ed
    .line 646
    .end local v6           #childHeightMeasureSpec:I
    .end local v7           #childHeightMode:I
    .end local v8           #childWidthMeasureSpec:I
    .end local v9           #childWidthMode:I
    :cond_ed
    const/high16 v9, 0x4000

    #@ef
    .restart local v9       #childWidthMode:I
    goto :goto_c9

    #@f0
    .line 653
    :cond_f0
    const/high16 v7, 0x4000

    #@f2
    .restart local v7       #childHeightMode:I
    goto :goto_d7

    #@f3
    .line 663
    .end local v4           #child:Landroid/view/View;
    .end local v7           #childHeightMode:I
    .end local v9           #childWidthMode:I
    .end local v15           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_f3
    move-object/from16 v0, p0

    #@f5
    move/from16 v1, v22

    #@f7
    move/from16 v2, v21

    #@f9
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setMeasuredDimension(II)V

    #@fc
    .line 668
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidateCachedOffsets()V

    #@ff
    .line 670
    move-object/from16 v0, p0

    #@101
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildCountOnLastMeasure:I

    #@103
    move/from16 v27, v0

    #@105
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@108
    move-result v28

    #@109
    move/from16 v0, v27

    #@10b
    move/from16 v1, v28

    #@10d
    if-eq v0, v1, :cond_124

    #@10f
    move-object/from16 v0, p0

    #@111
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferringForDelete:Z

    #@113
    move/from16 v27, v0

    #@115
    if-nez v27, :cond_124

    #@117
    .line 671
    move-object/from16 v0, p0

    #@119
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@11b
    move/from16 v27, v0

    #@11d
    move-object/from16 v0, p0

    #@11f
    move/from16 v1, v27

    #@121
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setCurrentPage(I)V

    #@124
    .line 673
    :cond_124
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@127
    move-result v27

    #@128
    move/from16 v0, v27

    #@12a
    move-object/from16 v1, p0

    #@12c
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mChildCountOnLastMeasure:I

    #@12e
    .line 675
    if-lez v5, :cond_169

    #@130
    .line 680
    move-object/from16 v0, p0

    #@132
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@134
    move/from16 v27, v0

    #@136
    const/16 v28, -0x1

    #@138
    move/from16 v0, v27

    #@13a
    move/from16 v1, v28

    #@13c
    if-ne v0, v1, :cond_169

    #@13e
    .line 685
    const/16 v27, 0x0

    #@140
    move-object/from16 v0, p0

    #@142
    move/from16 v1, v27

    #@144
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@147
    move-result v17

    #@148
    .line 686
    .local v17, offset:I
    sub-int v27, v26, v17

    #@14a
    const/16 v28, 0x0

    #@14c
    move-object/from16 v0, p0

    #@14e
    move/from16 v1, v28

    #@150
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@153
    move-result-object v28

    #@154
    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getMeasuredWidth()I

    #@157
    move-result v28

    #@158
    sub-int v27, v27, v28

    #@15a
    move/from16 v0, v17

    #@15c
    move/from16 v1, v27

    #@15e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@161
    move-result v23

    #@162
    .line 688
    .local v23, spacing:I
    move-object/from16 v0, p0

    #@164
    move/from16 v1, v23

    #@166
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setPageSpacing(I)V

    #@169
    .line 692
    .end local v17           #offset:I
    .end local v23           #spacing:I
    :cond_169
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateScrollingIndicatorPosition()V

    #@16c
    .line 694
    if-lez v5, :cond_18c

    #@16e
    .line 695
    add-int/lit8 v27, v5, -0x1

    #@170
    move-object/from16 v0, p0

    #@172
    move/from16 v1, v27

    #@174
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@177
    move-result v27

    #@178
    add-int/lit8 v28, v5, -0x1

    #@17a
    move-object/from16 v0, p0

    #@17c
    move/from16 v1, v28

    #@17e
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@181
    move-result v28

    #@182
    sub-int v27, v27, v28

    #@184
    move/from16 v0, v27

    #@186
    move-object/from16 v1, p0

    #@188
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@18a
    goto/16 :goto_11

    #@18c
    .line 697
    :cond_18c
    const/16 v27, 0x0

    #@18e
    move/from16 v0, v27

    #@190
    move-object/from16 v1, p0

    #@192
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@194
    goto/16 :goto_11
.end method

.method protected onPageBeginMoving()V
    .registers 1

    #@0
    .prologue
    .line 493
    return-void
.end method

.method protected onPageEndMoving()V
    .registers 1

    #@0
    .prologue
    .line 497
    return-void
.end method

.method public abstract onRemoveView(Landroid/view/View;Z)V
.end method

.method public abstract onRemoveViewAnimationCompleted()V
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 7
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 936
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@2
    const/4 v3, -0x1

    #@3
    if-eq v2, v3, :cond_12

    #@5
    .line 937
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@7
    .line 941
    .local v0, focusablePage:I
    :goto_7
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 942
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_15

    #@d
    .line 943
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@10
    move-result v2

    #@11
    .line 945
    :goto_11
    return v2

    #@12
    .line 939
    .end local v0           #focusablePage:I
    .end local v1           #v:Landroid/view/View;
    :cond_12
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@14
    .restart local v0       #focusablePage:I
    goto :goto_7

    #@15
    .line 945
    .restart local v1       #v:Landroid/view/View;
    :cond_15
    const/4 v2, 0x0

    #@16
    goto :goto_11
.end method

.method protected onStartReordering()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2108
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_19

    #@d
    .line 2109
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@f
    const v2, 0x104034f

    #@12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@19
    .line 2114
    :cond_19
    const/4 v1, 0x4

    #@1a
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@1c
    .line 2115
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsReordering:Z

    #@1e
    .line 2118
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@20
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getVisiblePages([I)V

    #@23
    .line 2119
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@25
    invoke-virtual {p0, v3, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->boundByReorderablePages(Z[I)V

    #@28
    .line 2120
    const/4 v0, 0x0

    #@29
    .local v0, i:I
    :goto_29
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@2c
    move-result v1

    #@2d
    if-ge v0, v1, :cond_47

    #@2f
    .line 2121
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@31
    const/4 v2, 0x0

    #@32
    aget v1, v1, v2

    #@34
    if-lt v0, v1, :cond_3c

    #@36
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@38
    aget v1, v1, v3

    #@3a
    if-le v0, v1, :cond_44

    #@3c
    .line 2122
    :cond_3c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@3f
    move-result-object v1

    #@40
    const/4 v2, 0x0

    #@41
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    #@44
    .line 2120
    :cond_44
    add-int/lit8 v0, v0, 0x1

    #@46
    goto :goto_29

    #@47
    .line 2128
    :cond_47
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@4a
    .line 2129
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 36
    .parameter "ev"

    #@0
    .prologue
    .line 1308
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@3
    move-result v31

    #@4
    if-gtz v31, :cond_b

    #@6
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v31

    #@a
    .line 1605
    :goto_a
    return v31

    #@b
    .line 1310
    :cond_b
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    #@e
    .line 1312
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@11
    move-result v4

    #@12
    .line 1314
    .local v4, action:I
    and-int/lit16 v0, v4, 0xff

    #@14
    move/from16 v31, v0

    #@16
    packed-switch v31, :pswitch_data_61a

    #@19
    .line 1605
    :cond_19
    :goto_19
    :pswitch_19
    const/16 v31, 0x1

    #@1b
    goto :goto_a

    #@1c
    .line 1320
    :pswitch_1c
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@20
    move-object/from16 v31, v0

    #@22
    invoke-virtual/range {v31 .. v31}, Landroid/widget/Scroller;->isFinished()Z

    #@25
    move-result v31

    #@26
    if-nez v31, :cond_31

    #@28
    .line 1321
    move-object/from16 v0, p0

    #@2a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@2c
    move-object/from16 v31, v0

    #@2e
    invoke-virtual/range {v31 .. v31}, Landroid/widget/Scroller;->abortAnimation()V

    #@31
    .line 1325
    :cond_31
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@34
    move-result v31

    #@35
    move/from16 v0, v31

    #@37
    move-object/from16 v1, p0

    #@39
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@3b
    move/from16 v0, v31

    #@3d
    move-object/from16 v1, p0

    #@3f
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@41
    .line 1326
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@44
    move-result v31

    #@45
    move/from16 v0, v31

    #@47
    move-object/from16 v1, p0

    #@49
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@4b
    move/from16 v0, v31

    #@4d
    move-object/from16 v1, p0

    #@4f
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionY:F

    #@51
    .line 1327
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@54
    move-result v31

    #@55
    move/from16 v0, v31

    #@57
    int-to-float v0, v0

    #@58
    move/from16 v31, v0

    #@5a
    move/from16 v0, v31

    #@5c
    move-object/from16 v1, p0

    #@5e
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownScrollX:F

    #@60
    .line 1328
    move-object/from16 v0, p0

    #@62
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@64
    move/from16 v31, v0

    #@66
    move-object/from16 v0, p0

    #@68
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@6a
    move/from16 v32, v0

    #@6c
    move-object/from16 v0, p0

    #@6e
    move-object/from16 v1, p0

    #@70
    move/from16 v2, v31

    #@72
    move/from16 v3, v32

    #@74
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@77
    move-result-object v18

    #@78
    .line 1329
    .local v18, p:[F
    const/16 v31, 0x0

    #@7a
    aget v31, v18, v31

    #@7c
    move/from16 v0, v31

    #@7e
    move-object/from16 v1, p0

    #@80
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@82
    .line 1330
    const/16 v31, 0x1

    #@84
    aget v31, v18, v31

    #@86
    move/from16 v0, v31

    #@88
    move-object/from16 v1, p0

    #@8a
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@8c
    .line 1331
    const/16 v31, 0x0

    #@8e
    move/from16 v0, v31

    #@90
    move-object/from16 v1, p0

    #@92
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@94
    .line 1332
    const/16 v31, 0x0

    #@96
    move/from16 v0, v31

    #@98
    move-object/from16 v1, p0

    #@9a
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@9c
    .line 1333
    const/16 v31, 0x0

    #@9e
    move-object/from16 v0, p1

    #@a0
    move/from16 v1, v31

    #@a2
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@a5
    move-result v31

    #@a6
    move/from16 v0, v31

    #@a8
    move-object/from16 v1, p0

    #@aa
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@ac
    .line 1336
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@af
    move-result v31

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@b4
    move/from16 v32, v0

    #@b6
    add-int v16, v31, v32

    #@b8
    .line 1337
    .local v16, leftEdgeBoundary:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getMeasuredWidth()I

    #@bb
    move-result v31

    #@bc
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@bf
    move-result v32

    #@c0
    sub-int v31, v31, v32

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mEdgeSwipeRegionSize:I

    #@c6
    move/from16 v32, v0

    #@c8
    sub-int v27, v31, v32

    #@ca
    .line 1338
    .local v27, rightEdgeBoundary:I
    move-object/from16 v0, p0

    #@cc
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@ce
    move/from16 v31, v0

    #@d0
    move/from16 v0, v16

    #@d2
    int-to-float v0, v0

    #@d3
    move/from16 v32, v0

    #@d5
    cmpg-float v31, v31, v32

    #@d7
    if-lez v31, :cond_e8

    #@d9
    move-object/from16 v0, p0

    #@db
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@dd
    move/from16 v31, v0

    #@df
    move/from16 v0, v27

    #@e1
    int-to-float v0, v0

    #@e2
    move/from16 v32, v0

    #@e4
    cmpl-float v31, v31, v32

    #@e6
    if-ltz v31, :cond_f0

    #@e8
    .line 1339
    :cond_e8
    const/16 v31, 0x1

    #@ea
    move/from16 v0, v31

    #@ec
    move-object/from16 v1, p0

    #@ee
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownEventOnEdge:Z

    #@f0
    .line 1342
    :cond_f0
    move-object/from16 v0, p0

    #@f2
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@f4
    move/from16 v31, v0

    #@f6
    const/16 v32, 0x1

    #@f8
    move/from16 v0, v31

    #@fa
    move/from16 v1, v32

    #@fc
    if-ne v0, v1, :cond_19

    #@fe
    .line 1343
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->pageBeginMoving()V

    #@101
    goto/16 :goto_19

    #@103
    .line 1348
    .end local v16           #leftEdgeBoundary:I
    .end local v18           #p:[F
    .end local v27           #rightEdgeBoundary:I
    :pswitch_103
    move-object/from16 v0, p0

    #@105
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@107
    move/from16 v31, v0

    #@109
    const/16 v32, 0x1

    #@10b
    move/from16 v0, v31

    #@10d
    move/from16 v1, v32

    #@10f
    if-ne v0, v1, :cond_1b7

    #@111
    .line 1350
    move-object/from16 v0, p0

    #@113
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@115
    move/from16 v31, v0

    #@117
    move-object/from16 v0, p1

    #@119
    move/from16 v1, v31

    #@11b
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@11e
    move-result v23

    #@11f
    .line 1353
    .local v23, pointerIndex:I
    if-gez v23, :cond_12a

    #@121
    .line 1354
    const-string v31, "WidgetPagedView"

    #@123
    const-string v32, "MotionEvent.ACTION_MOVE: pointerIndex out of range"

    #@125
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    goto/16 :goto_19

    #@12a
    .line 1357
    :cond_12a
    move-object/from16 v0, p1

    #@12c
    move/from16 v1, v23

    #@12e
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@131
    move-result v30

    #@132
    .line 1358
    .local v30, x:F
    move-object/from16 v0, p0

    #@134
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@136
    move/from16 v31, v0

    #@138
    move-object/from16 v0, p0

    #@13a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@13c
    move/from16 v32, v0

    #@13e
    add-float v31, v31, v32

    #@140
    sub-float v7, v31, v30

    #@142
    .line 1360
    .local v7, deltaX:F
    move-object/from16 v0, p0

    #@144
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@146
    move/from16 v31, v0

    #@148
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    #@14b
    move-result v32

    #@14c
    add-float v31, v31, v32

    #@14e
    move/from16 v0, v31

    #@150
    move-object/from16 v1, p0

    #@152
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@154
    .line 1365
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    #@157
    move-result v31

    #@158
    const/high16 v32, 0x3f80

    #@15a
    cmpl-float v31, v31, v32

    #@15c
    if-ltz v31, :cond_1b2

    #@15e
    .line 1366
    move-object/from16 v0, p0

    #@160
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchX:F

    #@162
    move/from16 v31, v0

    #@164
    add-float v31, v31, v7

    #@166
    move/from16 v0, v31

    #@168
    move-object/from16 v1, p0

    #@16a
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchX:F

    #@16c
    .line 1367
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@16f
    move-result-wide v31

    #@170
    move-wide/from16 v0, v31

    #@172
    long-to-float v0, v0

    #@173
    move/from16 v31, v0

    #@175
    const v32, 0x4e6e6b28

    #@178
    div-float v31, v31, v32

    #@17a
    move/from16 v0, v31

    #@17c
    move-object/from16 v1, p0

    #@17e
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSmoothingTime:F

    #@180
    .line 1368
    move-object/from16 v0, p0

    #@182
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeferScrollUpdate:Z

    #@184
    move/from16 v31, v0

    #@186
    if-nez v31, :cond_1ae

    #@188
    .line 1369
    float-to-int v0, v7

    #@189
    move/from16 v31, v0

    #@18b
    const/16 v32, 0x0

    #@18d
    move-object/from16 v0, p0

    #@18f
    move/from16 v1, v31

    #@191
    move/from16 v2, v32

    #@193
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollBy(II)V

    #@196
    .line 1374
    :goto_196
    move/from16 v0, v30

    #@198
    move-object/from16 v1, p0

    #@19a
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@19c
    .line 1375
    float-to-int v0, v7

    #@19d
    move/from16 v31, v0

    #@19f
    move/from16 v0, v31

    #@1a1
    int-to-float v0, v0

    #@1a2
    move/from16 v31, v0

    #@1a4
    sub-float v31, v7, v31

    #@1a6
    move/from16 v0, v31

    #@1a8
    move-object/from16 v1, p0

    #@1aa
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@1ac
    goto/16 :goto_19

    #@1ae
    .line 1372
    :cond_1ae
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@1b1
    goto :goto_196

    #@1b2
    .line 1377
    :cond_1b2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->awakenScrollBars()Z

    #@1b5
    goto/16 :goto_19

    #@1b7
    .line 1379
    .end local v7           #deltaX:F
    .end local v23           #pointerIndex:I
    .end local v30           #x:F
    :cond_1b7
    move-object/from16 v0, p0

    #@1b9
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@1bb
    move/from16 v31, v0

    #@1bd
    const/16 v32, 0x4

    #@1bf
    move/from16 v0, v31

    #@1c1
    move/from16 v1, v32

    #@1c3
    if-ne v0, v1, :cond_38c

    #@1c5
    .line 1381
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@1c8
    move-result v31

    #@1c9
    move/from16 v0, v31

    #@1cb
    move-object/from16 v1, p0

    #@1cd
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@1cf
    .line 1382
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@1d2
    move-result v31

    #@1d3
    move/from16 v0, v31

    #@1d5
    move-object/from16 v1, p0

    #@1d7
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@1d9
    .line 1386
    move-object/from16 v0, p0

    #@1db
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@1dd
    move/from16 v31, v0

    #@1df
    move-object/from16 v0, p0

    #@1e1
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@1e3
    move/from16 v32, v0

    #@1e5
    move-object/from16 v0, p0

    #@1e7
    move-object/from16 v1, p0

    #@1e9
    move/from16 v2, v31

    #@1eb
    move/from16 v3, v32

    #@1ed
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@1f0
    move-result-object v24

    #@1f1
    .line 1387
    .local v24, pt:[F
    const/16 v31, 0x0

    #@1f3
    aget v31, v24, v31

    #@1f5
    move/from16 v0, v31

    #@1f7
    move-object/from16 v1, p0

    #@1f9
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@1fb
    .line 1388
    const/16 v31, 0x1

    #@1fd
    aget v31, v24, v31

    #@1ff
    move/from16 v0, v31

    #@201
    move-object/from16 v1, p0

    #@203
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@205
    .line 1389
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateDragViewTranslationDuringDrag()V

    #@208
    .line 1392
    move-object/from16 v0, p0

    #@20a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@20c
    move-object/from16 v31, v0

    #@20e
    move-object/from16 v0, p0

    #@210
    move-object/from16 v1, v31

    #@212
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexOfChild(Landroid/view/View;)I

    #@215
    move-result v8

    #@216
    .line 1393
    .local v8, dragViewIndex:I
    move-object/from16 v0, p0

    #@218
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_SIDE_PAGE_BUFFER_PERCENTAGE:F

    #@21a
    move/from16 v31, v0

    #@21c
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@21f
    move-result v32

    #@220
    move/from16 v0, v32

    #@222
    int-to-float v0, v0

    #@223
    move/from16 v32, v0

    #@225
    mul-float v31, v31, v32

    #@227
    move/from16 v0, v31

    #@229
    float-to-int v6, v0

    #@22a
    .line 1395
    .local v6, bufferSize:I
    move-object/from16 v0, p0

    #@22c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@22e
    move-object/from16 v31, v0

    #@230
    move-object/from16 v0, v31

    #@232
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@234
    move/from16 v31, v0

    #@236
    move/from16 v0, v31

    #@238
    int-to-float v0, v0

    #@239
    move/from16 v31, v0

    #@23b
    const/16 v32, 0x0

    #@23d
    move-object/from16 v0, p0

    #@23f
    move-object/from16 v1, p0

    #@241
    move/from16 v2, v31

    #@243
    move/from16 v3, v32

    #@245
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@248
    move-result-object v31

    #@249
    const/16 v32, 0x0

    #@24b
    aget v31, v31, v32

    #@24d
    int-to-float v0, v6

    #@24e
    move/from16 v32, v0

    #@250
    add-float v31, v31, v32

    #@252
    move/from16 v0, v31

    #@254
    float-to-int v15, v0

    #@255
    .line 1397
    .local v15, leftBufferEdge:I
    move-object/from16 v0, p0

    #@257
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mViewport:Landroid/graphics/Rect;

    #@259
    move-object/from16 v31, v0

    #@25b
    move-object/from16 v0, v31

    #@25d
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@25f
    move/from16 v31, v0

    #@261
    move/from16 v0, v31

    #@263
    int-to-float v0, v0

    #@264
    move/from16 v31, v0

    #@266
    const/16 v32, 0x0

    #@268
    move-object/from16 v0, p0

    #@26a
    move-object/from16 v1, p0

    #@26c
    move/from16 v2, v31

    #@26e
    move/from16 v3, v32

    #@270
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@273
    move-result-object v31

    #@274
    const/16 v32, 0x0

    #@276
    aget v31, v31, v32

    #@278
    int-to-float v0, v6

    #@279
    move/from16 v32, v0

    #@27b
    sub-float v31, v31, v32

    #@27d
    move/from16 v0, v31

    #@27f
    float-to-int v0, v0

    #@280
    move/from16 v26, v0

    #@282
    .line 1401
    .local v26, rightBufferEdge:I
    move-object/from16 v0, p0

    #@284
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@286
    move/from16 v31, v0

    #@288
    move/from16 v0, v31

    #@28a
    float-to-int v0, v0

    #@28b
    move/from16 v31, v0

    #@28d
    move-object/from16 v0, p0

    #@28f
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@291
    move/from16 v32, v0

    #@293
    move/from16 v0, v32

    #@295
    float-to-int v0, v0

    #@296
    move/from16 v32, v0

    #@298
    move-object/from16 v0, p0

    #@29a
    move/from16 v1, v31

    #@29c
    move/from16 v2, v32

    #@29e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isHoveringOverDeleteDropTarget(II)Z

    #@2a1
    move-result v13

    #@2a2
    .line 1403
    .local v13, isHoveringOverDelete:Z
    move-object/from16 v0, p0

    #@2a4
    invoke-virtual {v0, v8, v13}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setPageHoveringOverDeleteDropTarget(IZ)V

    #@2a7
    .line 1412
    move-object/from16 v0, p0

    #@2a9
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@2ab
    move/from16 v22, v0

    #@2ad
    .line 1413
    .local v22, parentX:F
    const/16 v19, -0x1

    #@2af
    .line 1414
    .local v19, pageIndexToSnapTo:I
    int-to-float v0, v15

    #@2b0
    move/from16 v31, v0

    #@2b2
    cmpg-float v31, v22, v31

    #@2b4
    if-gez v31, :cond_35e

    #@2b6
    if-lez v8, :cond_35e

    #@2b8
    .line 1415
    add-int/lit8 v19, v8, -0x1

    #@2ba
    .line 1420
    :cond_2ba
    :goto_2ba
    move/from16 v20, v19

    #@2bc
    .line 1421
    .local v20, pageUnderPointIndex:I
    const/16 v31, -0x1

    #@2be
    move/from16 v0, v20

    #@2c0
    move/from16 v1, v31

    #@2c2
    if-le v0, v1, :cond_375

    #@2c4
    if-nez v13, :cond_375

    #@2c6
    .line 1422
    move-object/from16 v0, p0

    #@2c8
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2ca
    move-object/from16 v31, v0

    #@2cc
    const/16 v32, 0x0

    #@2ce
    const/16 v33, 0x0

    #@2d0
    aput v33, v31, v32

    #@2d2
    .line 1423
    move-object/from16 v0, p0

    #@2d4
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2d6
    move-object/from16 v31, v0

    #@2d8
    const/16 v32, 0x1

    #@2da
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@2dd
    move-result v33

    #@2de
    add-int/lit8 v33, v33, -0x1

    #@2e0
    aput v33, v31, v32

    #@2e2
    .line 1424
    const/16 v31, 0x1

    #@2e4
    move-object/from16 v0, p0

    #@2e6
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2e8
    move-object/from16 v32, v0

    #@2ea
    move-object/from16 v0, p0

    #@2ec
    move/from16 v1, v31

    #@2ee
    move-object/from16 v2, v32

    #@2f0
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->boundByReorderablePages(Z[I)V

    #@2f3
    .line 1425
    move-object/from16 v0, p0

    #@2f5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@2f7
    move-object/from16 v31, v0

    #@2f9
    const/16 v32, 0x0

    #@2fb
    aget v31, v31, v32

    #@2fd
    move/from16 v0, v31

    #@2ff
    move/from16 v1, v20

    #@301
    if-gt v0, v1, :cond_19

    #@303
    move-object/from16 v0, p0

    #@305
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@307
    move-object/from16 v31, v0

    #@309
    const/16 v32, 0x1

    #@30b
    aget v31, v31, v32

    #@30d
    move/from16 v0, v20

    #@30f
    move/from16 v1, v31

    #@311
    if-gt v0, v1, :cond_19

    #@313
    move-object/from16 v0, p0

    #@315
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverIndex:I

    #@317
    move/from16 v31, v0

    #@319
    move/from16 v0, v20

    #@31b
    move/from16 v1, v31

    #@31d
    if-eq v0, v1, :cond_19

    #@31f
    move-object/from16 v0, p0

    #@321
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@323
    move-object/from16 v31, v0

    #@325
    invoke-virtual/range {v31 .. v31}, Landroid/widget/Scroller;->isFinished()Z

    #@328
    move-result v31

    #@329
    if-eqz v31, :cond_19

    #@32b
    .line 1428
    move/from16 v0, v20

    #@32d
    move-object/from16 v1, p0

    #@32f
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverIndex:I

    #@331
    .line 1429
    new-instance v31, Lcom/android/internal/policy/impl/keyguard/PagedView$1;

    #@333
    move-object/from16 v0, v31

    #@335
    move-object/from16 v1, p0

    #@337
    move/from16 v2, v20

    #@339
    invoke-direct {v0, v1, v2, v8}, Lcom/android/internal/policy/impl/keyguard/PagedView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;II)V

    #@33c
    move-object/from16 v0, v31

    #@33e
    move-object/from16 v1, p0

    #@340
    iput-object v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverRunnable:Ljava/lang/Runnable;

    #@342
    .line 1479
    move-object/from16 v0, p0

    #@344
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverRunnable:Ljava/lang/Runnable;

    #@346
    move-object/from16 v31, v0

    #@348
    move-object/from16 v0, p0

    #@34a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_SIDE_PAGE_HOVER_TIMEOUT:I

    #@34c
    move/from16 v32, v0

    #@34e
    move/from16 v0, v32

    #@350
    int-to-long v0, v0

    #@351
    move-wide/from16 v32, v0

    #@353
    move-object/from16 v0, p0

    #@355
    move-object/from16 v1, v31

    #@357
    move-wide/from16 v2, v32

    #@359
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@35c
    goto/16 :goto_19

    #@35e
    .line 1416
    .end local v20           #pageUnderPointIndex:I
    :cond_35e
    move/from16 v0, v26

    #@360
    int-to-float v0, v0

    #@361
    move/from16 v31, v0

    #@363
    cmpl-float v31, v22, v31

    #@365
    if-lez v31, :cond_2ba

    #@367
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@36a
    move-result v31

    #@36b
    add-int/lit8 v31, v31, -0x1

    #@36d
    move/from16 v0, v31

    #@36f
    if-ge v8, v0, :cond_2ba

    #@371
    .line 1417
    add-int/lit8 v19, v8, 0x1

    #@373
    goto/16 :goto_2ba

    #@375
    .line 1482
    .restart local v20       #pageUnderPointIndex:I
    :cond_375
    move-object/from16 v0, p0

    #@377
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverRunnable:Ljava/lang/Runnable;

    #@379
    move-object/from16 v31, v0

    #@37b
    move-object/from16 v0, p0

    #@37d
    move-object/from16 v1, v31

    #@37f
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@382
    .line 1483
    const/16 v31, -0x1

    #@384
    move/from16 v0, v31

    #@386
    move-object/from16 v1, p0

    #@388
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverIndex:I

    #@38a
    goto/16 :goto_19

    #@38c
    .line 1486
    .end local v6           #bufferSize:I
    .end local v8           #dragViewIndex:I
    .end local v13           #isHoveringOverDelete:Z
    .end local v15           #leftBufferEdge:I
    .end local v19           #pageIndexToSnapTo:I
    .end local v20           #pageUnderPointIndex:I
    .end local v22           #parentX:F
    .end local v24           #pt:[F
    .end local v26           #rightBufferEdge:I
    :cond_38c
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    #@38f
    goto/16 :goto_19

    #@391
    .line 1491
    :pswitch_391
    move-object/from16 v0, p0

    #@393
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@395
    move/from16 v31, v0

    #@397
    const/16 v32, 0x1

    #@399
    move/from16 v0, v31

    #@39b
    move/from16 v1, v32

    #@39d
    if-ne v0, v1, :cond_502

    #@39f
    .line 1492
    move-object/from16 v0, p0

    #@3a1
    iget v5, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mActivePointerId:I

    #@3a3
    .line 1493
    .local v5, activePointerId:I
    move-object/from16 v0, p1

    #@3a5
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@3a8
    move-result v23

    #@3a9
    .line 1496
    .restart local v23       #pointerIndex:I
    if-gez v23, :cond_3b4

    #@3ab
    .line 1497
    const-string v31, "WidgetPagedView"

    #@3ad
    const-string v32, "MotionEvent.ACTION_UP: pointerIndex out of range"

    #@3af
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b2
    goto/16 :goto_19

    #@3b4
    .line 1500
    :cond_3b4
    move-object/from16 v0, p1

    #@3b6
    move/from16 v1, v23

    #@3b8
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@3bb
    move-result v30

    #@3bc
    .line 1501
    .restart local v30       #x:F
    move-object/from16 v0, p0

    #@3be
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3c0
    move-object/from16 v28, v0

    #@3c2
    .line 1502
    .local v28, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v31, 0x3e8

    #@3c4
    move-object/from16 v0, p0

    #@3c6
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaximumVelocity:I

    #@3c8
    move/from16 v32, v0

    #@3ca
    move/from16 v0, v32

    #@3cc
    int-to-float v0, v0

    #@3cd
    move/from16 v32, v0

    #@3cf
    move-object/from16 v0, v28

    #@3d1
    move/from16 v1, v31

    #@3d3
    move/from16 v2, v32

    #@3d5
    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@3d8
    .line 1503
    move-object/from16 v0, v28

    #@3da
    invoke-virtual {v0, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@3dd
    move-result v31

    #@3de
    move/from16 v0, v31

    #@3e0
    float-to-int v0, v0

    #@3e1
    move/from16 v29, v0

    #@3e3
    .line 1504
    .local v29, velocityX:I
    move-object/from16 v0, p0

    #@3e5
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@3e7
    move/from16 v31, v0

    #@3e9
    sub-float v31, v30, v31

    #@3eb
    move/from16 v0, v31

    #@3ed
    float-to-int v7, v0

    #@3ee
    .line 1505
    .local v7, deltaX:I
    move-object/from16 v0, p0

    #@3f0
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@3f2
    move/from16 v31, v0

    #@3f4
    move-object/from16 v0, p0

    #@3f6
    move/from16 v1, v31

    #@3f8
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@3fb
    move-result-object v31

    #@3fc
    move-object/from16 v0, p0

    #@3fe
    move-object/from16 v1, v31

    #@400
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    #@403
    move-result v21

    #@404
    .line 1506
    .local v21, pageWidth:I
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@407
    move-result v31

    #@408
    move/from16 v0, v31

    #@40a
    int-to-float v0, v0

    #@40b
    move/from16 v31, v0

    #@40d
    move/from16 v0, v21

    #@40f
    int-to-float v0, v0

    #@410
    move/from16 v32, v0

    #@412
    const v33, 0x3ecccccd

    #@415
    mul-float v32, v32, v33

    #@417
    cmpl-float v31, v31, v32

    #@419
    if-lez v31, :cond_4bd

    #@41b
    const/4 v14, 0x1

    #@41c
    .line 1509
    .local v14, isSignificantMove:Z
    :goto_41c
    move-object/from16 v0, p0

    #@41e
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@420
    move/from16 v31, v0

    #@422
    move-object/from16 v0, p0

    #@424
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@426
    move/from16 v32, v0

    #@428
    move-object/from16 v0, p0

    #@42a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionXRemainder:F

    #@42c
    move/from16 v33, v0

    #@42e
    add-float v32, v32, v33

    #@430
    sub-float v32, v32, v30

    #@432
    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->abs(F)F

    #@435
    move-result v32

    #@436
    add-float v31, v31, v32

    #@438
    move/from16 v0, v31

    #@43a
    move-object/from16 v1, p0

    #@43c
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@43e
    .line 1511
    move-object/from16 v0, p0

    #@440
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTotalMotionX:F

    #@442
    move/from16 v31, v0

    #@444
    const/high16 v32, 0x41c8

    #@446
    cmpl-float v31, v31, v32

    #@448
    if-lez v31, :cond_4c0

    #@44a
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@44d
    move-result v31

    #@44e
    move-object/from16 v0, p0

    #@450
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mFlingThresholdVelocity:I

    #@452
    move/from16 v32, v0

    #@454
    move/from16 v0, v31

    #@456
    move/from16 v1, v32

    #@458
    if-le v0, v1, :cond_4c0

    #@45a
    const/4 v12, 0x1

    #@45b
    .line 1517
    .local v12, isFling:Z
    :goto_45b
    const/16 v25, 0x0

    #@45d
    .line 1518
    .local v25, returnToOriginalPage:Z
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@460
    move-result v31

    #@461
    move/from16 v0, v31

    #@463
    int-to-float v0, v0

    #@464
    move/from16 v31, v0

    #@466
    move/from16 v0, v21

    #@468
    int-to-float v0, v0

    #@469
    move/from16 v32, v0

    #@46b
    const v33, 0x3ea8f5c3

    #@46e
    mul-float v32, v32, v33

    #@470
    cmpl-float v31, v31, v32

    #@472
    if-lez v31, :cond_48c

    #@474
    move/from16 v0, v29

    #@476
    int-to-float v0, v0

    #@477
    move/from16 v31, v0

    #@479
    invoke-static/range {v31 .. v31}, Ljava/lang/Math;->signum(F)F

    #@47c
    move-result v31

    #@47d
    int-to-float v0, v7

    #@47e
    move/from16 v32, v0

    #@480
    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->signum(F)F

    #@483
    move-result v32

    #@484
    cmpl-float v31, v31, v32

    #@486
    if-eqz v31, :cond_48c

    #@488
    if-eqz v12, :cond_48c

    #@48a
    .line 1520
    const/16 v25, 0x1

    #@48c
    .line 1527
    :cond_48c
    if-eqz v14, :cond_492

    #@48e
    if-lez v7, :cond_492

    #@490
    if-eqz v12, :cond_496

    #@492
    :cond_492
    if-eqz v12, :cond_4cb

    #@494
    if-lez v29, :cond_4cb

    #@496
    :cond_496
    move-object/from16 v0, p0

    #@498
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@49a
    move/from16 v31, v0

    #@49c
    if-lez v31, :cond_4cb

    #@49e
    .line 1529
    if-eqz v25, :cond_4c2

    #@4a0
    move-object/from16 v0, p0

    #@4a2
    iget v9, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@4a4
    .line 1530
    .local v9, finalPage:I
    :goto_4a4
    move-object/from16 v0, p0

    #@4a6
    move/from16 v1, v29

    #@4a8
    invoke-virtual {v0, v9, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPageWithVelocity(II)V

    #@4ab
    .line 1588
    .end local v5           #activePointerId:I
    .end local v7           #deltaX:I
    .end local v9           #finalPage:I
    .end local v12           #isFling:Z
    .end local v14           #isSignificantMove:Z
    .end local v21           #pageWidth:I
    .end local v23           #pointerIndex:I
    .end local v25           #returnToOriginalPage:Z
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v29           #velocityX:I
    .end local v30           #x:F
    :cond_4ab
    :goto_4ab
    move-object/from16 v0, p0

    #@4ad
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSidePageHoverRunnable:Ljava/lang/Runnable;

    #@4af
    move-object/from16 v31, v0

    #@4b1
    move-object/from16 v0, p0

    #@4b3
    move-object/from16 v1, v31

    #@4b5
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@4b8
    .line 1590
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->resetTouchState()V

    #@4bb
    goto/16 :goto_19

    #@4bd
    .line 1506
    .restart local v5       #activePointerId:I
    .restart local v7       #deltaX:I
    .restart local v21       #pageWidth:I
    .restart local v23       #pointerIndex:I
    .restart local v28       #velocityTracker:Landroid/view/VelocityTracker;
    .restart local v29       #velocityX:I
    .restart local v30       #x:F
    :cond_4bd
    const/4 v14, 0x0

    #@4be
    goto/16 :goto_41c

    #@4c0
    .line 1511
    .restart local v14       #isSignificantMove:Z
    :cond_4c0
    const/4 v12, 0x0

    #@4c1
    goto :goto_45b

    #@4c2
    .line 1529
    .restart local v12       #isFling:Z
    .restart local v25       #returnToOriginalPage:Z
    :cond_4c2
    move-object/from16 v0, p0

    #@4c4
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@4c6
    move/from16 v31, v0

    #@4c8
    add-int/lit8 v9, v31, -0x1

    #@4ca
    goto :goto_4a4

    #@4cb
    .line 1531
    :cond_4cb
    if-eqz v14, :cond_4d1

    #@4cd
    if-gez v7, :cond_4d1

    #@4cf
    if-eqz v12, :cond_4d5

    #@4d1
    :cond_4d1
    if-eqz v12, :cond_4fe

    #@4d3
    if-gez v29, :cond_4fe

    #@4d5
    :cond_4d5
    move-object/from16 v0, p0

    #@4d7
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@4d9
    move/from16 v31, v0

    #@4db
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@4de
    move-result v32

    #@4df
    add-int/lit8 v32, v32, -0x1

    #@4e1
    move/from16 v0, v31

    #@4e3
    move/from16 v1, v32

    #@4e5
    if-ge v0, v1, :cond_4fe

    #@4e7
    .line 1534
    if-eqz v25, :cond_4f5

    #@4e9
    move-object/from16 v0, p0

    #@4eb
    iget v9, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@4ed
    .line 1535
    .restart local v9       #finalPage:I
    :goto_4ed
    move-object/from16 v0, p0

    #@4ef
    move/from16 v1, v29

    #@4f1
    invoke-virtual {v0, v9, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPageWithVelocity(II)V

    #@4f4
    goto :goto_4ab

    #@4f5
    .line 1534
    .end local v9           #finalPage:I
    :cond_4f5
    move-object/from16 v0, p0

    #@4f7
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@4f9
    move/from16 v31, v0

    #@4fb
    add-int/lit8 v9, v31, 0x1

    #@4fd
    goto :goto_4ed

    #@4fe
    .line 1537
    :cond_4fe
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToDestination()V

    #@501
    goto :goto_4ab

    #@502
    .line 1539
    .end local v5           #activePointerId:I
    .end local v7           #deltaX:I
    .end local v12           #isFling:Z
    .end local v14           #isSignificantMove:Z
    .end local v21           #pageWidth:I
    .end local v23           #pointerIndex:I
    .end local v25           #returnToOriginalPage:Z
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v29           #velocityX:I
    .end local v30           #x:F
    :cond_502
    move-object/from16 v0, p0

    #@504
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@506
    move/from16 v31, v0

    #@508
    const/16 v32, 0x2

    #@50a
    move/from16 v0, v31

    #@50c
    move/from16 v1, v32

    #@50e
    if-ne v0, v1, :cond_538

    #@510
    .line 1543
    const/16 v31, 0x0

    #@512
    move-object/from16 v0, p0

    #@514
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@516
    move/from16 v32, v0

    #@518
    add-int/lit8 v32, v32, -0x1

    #@51a
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->max(II)I

    #@51d
    move-result v17

    #@51e
    .line 1544
    .local v17, nextPage:I
    move-object/from16 v0, p0

    #@520
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@522
    move/from16 v31, v0

    #@524
    move/from16 v0, v17

    #@526
    move/from16 v1, v31

    #@528
    if-eq v0, v1, :cond_533

    #@52a
    .line 1545
    move-object/from16 v0, p0

    #@52c
    move/from16 v1, v17

    #@52e
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@531
    goto/16 :goto_4ab

    #@533
    .line 1547
    :cond_533
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToDestination()V

    #@536
    goto/16 :goto_4ab

    #@538
    .line 1549
    .end local v17           #nextPage:I
    :cond_538
    move-object/from16 v0, p0

    #@53a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@53c
    move/from16 v31, v0

    #@53e
    const/16 v32, 0x3

    #@540
    move/from16 v0, v31

    #@542
    move/from16 v1, v32

    #@544
    if-ne v0, v1, :cond_572

    #@546
    .line 1553
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@549
    move-result v31

    #@54a
    add-int/lit8 v31, v31, -0x1

    #@54c
    move-object/from16 v0, p0

    #@54e
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@550
    move/from16 v32, v0

    #@552
    add-int/lit8 v32, v32, 0x1

    #@554
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(II)I

    #@557
    move-result v17

    #@558
    .line 1554
    .restart local v17       #nextPage:I
    move-object/from16 v0, p0

    #@55a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@55c
    move/from16 v31, v0

    #@55e
    move/from16 v0, v17

    #@560
    move/from16 v1, v31

    #@562
    if-eq v0, v1, :cond_56d

    #@564
    .line 1555
    move-object/from16 v0, p0

    #@566
    move/from16 v1, v17

    #@568
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@56b
    goto/16 :goto_4ab

    #@56d
    .line 1557
    :cond_56d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToDestination()V

    #@570
    goto/16 :goto_4ab

    #@572
    .line 1559
    .end local v17           #nextPage:I
    :cond_572
    move-object/from16 v0, p0

    #@574
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@576
    move/from16 v31, v0

    #@578
    const/16 v32, 0x4

    #@57a
    move/from16 v0, v31

    #@57c
    move/from16 v1, v32

    #@57e
    if-ne v0, v1, :cond_5f9

    #@580
    .line 1561
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@583
    move-result v31

    #@584
    move/from16 v0, v31

    #@586
    move-object/from16 v1, p0

    #@588
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@58a
    .line 1562
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@58d
    move-result v31

    #@58e
    move/from16 v0, v31

    #@590
    move-object/from16 v1, p0

    #@592
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@594
    .line 1566
    move-object/from16 v0, p0

    #@596
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@598
    move/from16 v31, v0

    #@59a
    move-object/from16 v0, p0

    #@59c
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@59e
    move/from16 v32, v0

    #@5a0
    move-object/from16 v0, p0

    #@5a2
    move-object/from16 v1, p0

    #@5a4
    move/from16 v2, v31

    #@5a6
    move/from16 v3, v32

    #@5a8
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromViewToParent(Landroid/view/View;FF)[F

    #@5ab
    move-result-object v24

    #@5ac
    .line 1567
    .restart local v24       #pt:[F
    const/16 v31, 0x0

    #@5ae
    aget v31, v24, v31

    #@5b0
    move/from16 v0, v31

    #@5b2
    move-object/from16 v1, p0

    #@5b4
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@5b6
    .line 1568
    const/16 v31, 0x1

    #@5b8
    aget v31, v24, v31

    #@5ba
    move/from16 v0, v31

    #@5bc
    move-object/from16 v1, p0

    #@5be
    iput v0, v1, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@5c0
    .line 1569
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateDragViewTranslationDuringDrag()V

    #@5c3
    .line 1570
    const/4 v11, 0x0

    #@5c4
    .line 1573
    .local v11, handledFling:Z
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isFlingingToDelete()Landroid/graphics/PointF;

    #@5c7
    move-result-object v10

    #@5c8
    .line 1574
    .local v10, flingToDeleteVector:Landroid/graphics/PointF;
    if-eqz v10, :cond_5d0

    #@5ca
    .line 1575
    move-object/from16 v0, p0

    #@5cc
    invoke-virtual {v0, v10}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onFlingToDelete(Landroid/graphics/PointF;)V

    #@5cf
    .line 1576
    const/4 v11, 0x1

    #@5d0
    .line 1579
    :cond_5d0
    if-nez v11, :cond_4ab

    #@5d2
    move-object/from16 v0, p0

    #@5d4
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@5d6
    move/from16 v31, v0

    #@5d8
    move/from16 v0, v31

    #@5da
    float-to-int v0, v0

    #@5db
    move/from16 v31, v0

    #@5dd
    move-object/from16 v0, p0

    #@5df
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@5e1
    move/from16 v32, v0

    #@5e3
    move/from16 v0, v32

    #@5e5
    float-to-int v0, v0

    #@5e6
    move/from16 v32, v0

    #@5e8
    move-object/from16 v0, p0

    #@5ea
    move/from16 v1, v31

    #@5ec
    move/from16 v2, v32

    #@5ee
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isHoveringOverDeleteDropTarget(II)Z

    #@5f1
    move-result v31

    #@5f2
    if-eqz v31, :cond_4ab

    #@5f4
    .line 1581
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onDropToDelete()V

    #@5f7
    goto/16 :goto_4ab

    #@5f9
    .line 1584
    .end local v10           #flingToDeleteVector:Landroid/graphics/PointF;
    .end local v11           #handledFling:Z
    .end local v24           #pt:[F
    :cond_5f9
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onUnhandledTap(Landroid/view/MotionEvent;)V

    #@5fc
    goto/16 :goto_4ab

    #@5fe
    .line 1594
    :pswitch_5fe
    move-object/from16 v0, p0

    #@600
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchState:I

    #@602
    move/from16 v31, v0

    #@604
    const/16 v32, 0x1

    #@606
    move/from16 v0, v31

    #@608
    move/from16 v1, v32

    #@60a
    if-ne v0, v1, :cond_60f

    #@60c
    .line 1595
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToDestination()V

    #@60f
    .line 1597
    :cond_60f
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->resetTouchState()V

    #@612
    goto/16 :goto_19

    #@614
    .line 1601
    :pswitch_614
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@617
    goto/16 :goto_19

    #@619
    .line 1314
    nop

    #@61a
    :pswitch_data_61a
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_391
        :pswitch_103
        :pswitch_5fe
        :pswitch_19
        :pswitch_19
        :pswitch_614
    .end packed-switch
.end method

.method protected onUnhandledTap(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "ev"

    #@0
    .prologue
    .line 1621
    return-void
.end method

.method protected overScroll(F)V
    .registers 2
    .parameter "amount"

    #@0
    .prologue
    .line 1290
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->dampedOverScroll(F)V

    #@3
    .line 1291
    return-void
.end method

.method protected pageBeginMoving()V
    .registers 2

    #@0
    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 475
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@7
    .line 476
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onPageBeginMoving()V

    #@a
    .line 478
    :cond_a
    return-void
.end method

.method protected pageEndMoving()V
    .registers 2

    #@0
    .prologue
    .line 481
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 482
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsPageMoving:Z

    #@7
    .line 483
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onPageEndMoving()V

    #@a
    .line 485
    :cond_a
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 6
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2552
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 2569
    :goto_7
    return v0

    #@8
    .line 2555
    :cond_8
    sparse-switch p1, :sswitch_data_28

    #@b
    .line 2569
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7

    #@d
    .line 2557
    :sswitch_d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@10
    move-result v1

    #@11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@14
    move-result v2

    #@15
    add-int/lit8 v2, v2, -0x1

    #@17
    if-ge v1, v2, :cond_b

    #@19
    .line 2558
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollRight()V

    #@1c
    goto :goto_7

    #@1d
    .line 2563
    :sswitch_1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@20
    move-result v1

    #@21
    if-lez v1, :cond_b

    #@23
    .line 2564
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollLeft()V

    #@26
    goto :goto_7

    #@27
    .line 2555
    nop

    #@28
    :sswitch_data_28
    .sparse-switch
        0x1000 -> :sswitch_d
        0x2000 -> :sswitch_1d
    .end sparse-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 1687
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@3
    .line 1688
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexOfChild(Landroid/view/View;)I

    #@6
    move-result v1

    #@7
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexToPage(I)I

    #@a
    move-result v0

    #@b
    .line 1689
    .local v0, page:I
    if-ltz v0, :cond_1c

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getCurrentPage()I

    #@10
    move-result v1

    #@11
    if-eq v0, v1, :cond_1c

    #@13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isInTouchMode()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1c

    #@19
    .line 1690
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@1c
    .line 1692
    :cond_1c
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 6
    .parameter "child"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 925
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v1

    #@4
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexToPage(I)I

    #@7
    move-result v0

    #@8
    .line 926
    .local v0, page:I
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@a
    if-ne v0, v1, :cond_14

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@e
    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_19

    #@14
    .line 927
    :cond_14
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@17
    .line 928
    const/4 v1, 0x1

    #@18
    .line 930
    :goto_18
    return v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method

.method protected screenScrolled(I)V
    .registers 2
    .parameter "screenCenter"

    #@0
    .prologue
    .line 745
    return-void
.end method

.method public scrollBy(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 515
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUnboundedScrollX:I

    #@2
    add-int/2addr v0, p1

    #@3
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollY()I

    #@6
    move-result v1

    #@7
    add-int/2addr v1, p2

    #@8
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollTo(II)V

    #@b
    .line 516
    return-void
.end method

.method public scrollLeft()V
    .registers 2

    #@0
    .prologue
    .line 1865
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 1866
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@a
    if-lez v0, :cond_13

    #@c
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@e
    add-int/lit8 v0, v0, -0x1

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@13
    .line 1870
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1868
    :cond_14
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@16
    if-lez v0, :cond_13

    #@18
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@1a
    add-int/lit8 v0, v0, -0x1

    #@1c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@1f
    goto :goto_13
.end method

.method public scrollRight()V
    .registers 3

    #@0
    .prologue
    .line 1873
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 1874
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@d
    move-result v1

    #@e
    add-int/lit8 v1, v1, -0x1

    #@10
    if-ge v0, v1, :cond_19

    #@12
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@14
    add-int/lit8 v0, v0, 0x1

    #@16
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@19
    .line 1878
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 1876
    :cond_1a
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@1c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@1f
    move-result v1

    #@20
    add-int/lit8 v1, v1, -0x1

    #@22
    if-ge v0, v1, :cond_19

    #@24
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@26
    add-int/lit8 v0, v0, 0x1

    #@28
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(I)V

    #@2b
    goto :goto_19
.end method

.method public scrollTo(II)V
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 520
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUnboundedScrollX:I

    #@4
    .line 522
    if-gez p1, :cond_39

    #@6
    .line 523
    invoke-super {p0, v3, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@9
    .line 524
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAllowOverScroll:Z

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 525
    int-to-float v1, p1

    #@e
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->overScroll(F)V

    #@11
    .line 537
    :cond_11
    :goto_11
    int-to-float v1, p1

    #@12
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTouchX:F

    #@14
    .line 538
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@17
    move-result-wide v1

    #@18
    long-to-float v1, v1

    #@19
    const v2, 0x4e6e6b28

    #@1c
    div-float/2addr v1, v2

    #@1d
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mSmoothingTime:F

    #@1f
    .line 541
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isReordering(Z)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_38

    #@25
    .line 542
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@27
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@29
    invoke-virtual {p0, p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromParentToView(Landroid/view/View;FF)[F

    #@2c
    move-result-object v0

    #@2d
    .line 543
    .local v0, p:[F
    aget v1, v0, v3

    #@2f
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@31
    .line 544
    aget v1, v0, v4

    #@33
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@35
    .line 545
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateDragViewTranslationDuringDrag()V

    #@38
    .line 547
    .end local v0           #p:[F
    :cond_38
    return-void

    #@39
    .line 527
    :cond_39
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@3b
    if-le p1, v1, :cond_4f

    #@3d
    .line 528
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@3f
    invoke-super {p0, v1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@42
    .line 529
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mAllowOverScroll:Z

    #@44
    if-eqz v1, :cond_11

    #@46
    .line 530
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@48
    sub-int v1, p1, v1

    #@4a
    int-to-float v1, v1

    #@4b
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->overScroll(F)V

    #@4e
    goto :goto_11

    #@4f
    .line 533
    :cond_4f
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@51
    .line 534
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    #@54
    goto :goto_11
.end method

.method setCurrentPage(I)V
    .registers 4
    .parameter "currentPage"

    #@0
    .prologue
    .line 439
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->notifyPageSwitching(I)V

    #@3
    .line 440
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@5
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 441
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@d
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    #@10
    .line 445
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_17

    #@16
    .line 455
    :goto_16
    return-void

    #@17
    .line 449
    :cond_17
    const/4 v0, 0x1

    #@18
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@1a
    .line 450
    const/4 v0, 0x0

    #@1b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@1e
    move-result v1

    #@1f
    add-int/lit8 v1, v1, -0x1

    #@21
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    #@24
    move-result v1

    #@25
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@28
    move-result v0

    #@29
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2b
    .line 451
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateCurrentPageScroll()V

    #@2e
    .line 452
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateScrollingIndicator()V

    #@31
    .line 453
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->notifyPageSwitched()V

    #@34
    .line 454
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@37
    goto :goto_16
.end method

.method protected setDataIsReady()V
    .registers 2

    #@0
    .prologue
    .line 389
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mIsDataReady:Z

    #@3
    .line 390
    return-void
.end method

.method setDeleteDropTarget(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 314
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDeleteDropTarget:Landroid/view/View;

    #@2
    .line 315
    return-void
.end method

.method public setMinScale(F)V
    .registers 2
    .parameter "f"

    #@0
    .prologue
    .line 344
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@2
    .line 345
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->requestLayout()V

    #@5
    .line 346
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .registers 5
    .parameter "l"

    #@0
    .prologue
    .line 506
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    #@2
    .line 507
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@5
    move-result v0

    #@6
    .line 508
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_13

    #@9
    .line 509
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@10
    .line 508
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_7

    #@13
    .line 511
    :cond_13
    return-void
.end method

.method public setOnlyAllowEdgeSwipes(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 458
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOnlyAllowEdgeSwipes:Z

    #@2
    .line 459
    return-void
.end method

.method protected setPageHoveringOverDeleteDropTarget(IZ)V
    .registers 3
    .parameter "viewIndex"
    .parameter "isHovering"

    #@0
    .prologue
    .line 2488
    return-void
.end method

.method public setPageSpacing(I)V
    .registers 2
    .parameter "pageSpacing"

    #@0
    .prologue
    .line 702
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@2
    .line 703
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidateCachedOffsets()V

    #@5
    .line 704
    return-void
.end method

.method public setPageSwitchListener(Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;)V
    .registers 5
    .parameter "pageSwitchListener"

    #@0
    .prologue
    .line 378
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@2
    .line 379
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@4
    if-eqz v0, :cond_13

    #@6
    .line 380
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSwitchListener:Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;

    #@8
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@a
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@10
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;->onPageSwitched(Landroid/view/View;I)V

    #@13
    .line 382
    :cond_13
    return-void
.end method

.method public setScaleX(F)V
    .registers 6
    .parameter "scaleX"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 350
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setScaleX(F)V

    #@4
    .line 351
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isReordering(Z)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1e

    #@a
    .line 352
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionX:F

    #@c
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mParentDownMotionY:F

    #@e
    invoke-virtual {p0, p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->mapPointFromParentToView(Landroid/view/View;FF)[F

    #@11
    move-result-object v0

    #@12
    .line 353
    .local v0, p:[F
    const/4 v1, 0x0

    #@13
    aget v1, v0, v1

    #@15
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@17
    .line 354
    aget v1, v0, v3

    #@19
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@1b
    .line 355
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateDragViewTranslationDuringDrag()V

    #@1e
    .line 357
    .end local v0           #p:[F
    :cond_1e
    return-void
.end method

.method protected shouldDrawChild(Landroid/view/View;)Z
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 872
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    #@3
    move-result v0

    #@4
    const/4 v1, 0x0

    #@5
    cmpl-float v0, v0, v1

    #@7
    if-lez v0, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method protected shouldSetTopAlignedPivotForWidget(I)Z
    .registers 3
    .parameter "childIndex"

    #@0
    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTopAlignPageWhenShrinkingForBouncer:Z

    #@2
    return v0
.end method

.method protected showScrollingIndicator(Z)V
    .registers 7
    .parameter "immediately"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 1946
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicator:Z

    #@6
    .line 1947
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicatorImmediately:Z

    #@8
    .line 1948
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@b
    move-result v0

    #@c
    if-gt v0, v2, :cond_f

    #@e
    .line 1966
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1949
    :cond_f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->isScrollingIndicatorEnabled()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_e

    #@15
    .line 1951
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mShouldShowScrollIndicator:Z

    #@17
    .line 1952
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollingIndicator()Landroid/view/View;

    #@1a
    .line 1953
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@1c
    if-eqz v0, :cond_e

    #@1e
    .line 1955
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->updateScrollingIndicatorPosition()V

    #@21
    .line 1956
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@23
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@26
    .line 1957
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->cancelScrollingIndicatorAnimations()V

    #@29
    .line 1958
    if-eqz p1, :cond_31

    #@2b
    .line 1959
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@2d
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    #@30
    goto :goto_e

    #@31
    .line 1961
    :cond_31
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicator:Landroid/view/View;

    #@33
    const-string v1, "alpha"

    #@35
    new-array v2, v2, [F

    #@37
    aput v4, v2, v3

    #@39
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@3f
    .line 1962
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@41
    const-wide/16 v1, 0x96

    #@43
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@46
    .line 1963
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScrollIndicatorAnimator:Landroid/animation/ValueAnimator;

    #@48
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@4b
    goto :goto_e
.end method

.method protected snapToDestination()V
    .registers 3

    #@0
    .prologue
    .line 1748
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageNearestToCenterOfScreen()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x2ee

    #@6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(II)V

    #@9
    .line 1749
    return-void
.end method

.method protected snapToPage(I)V
    .registers 3
    .parameter "whichPage"

    #@0
    .prologue
    .line 1809
    const/16 v0, 0x2ee

    #@2
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(II)V

    #@5
    .line 1810
    return-void
.end method

.method protected snapToPage(II)V
    .registers 4
    .parameter "whichPage"
    .parameter "duration"

    #@0
    .prologue
    .line 1816
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(IIZ)V

    #@4
    .line 1817
    return-void
.end method

.method protected snapToPage(III)V
    .registers 5
    .parameter "whichPage"
    .parameter "delta"
    .parameter "duration"

    #@0
    .prologue
    .line 1831
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(IIIZ)V

    #@4
    .line 1832
    return-void
.end method

.method protected snapToPage(IIIZ)V
    .registers 12
    .parameter "whichPage"
    .parameter "delta"
    .parameter "duration"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1834
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mNextPage:I

    #@3
    .line 1835
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->notifyPageSwitching(I)V

    #@6
    .line 1836
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getFocusedChild()Landroid/view/View;

    #@9
    move-result-object v6

    #@a
    .line 1837
    .local v6, focusedChild:Landroid/view/View;
    if-eqz v6, :cond_1b

    #@c
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@e
    if-eq p1, v0, :cond_1b

    #@10
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    if-ne v6, v0, :cond_1b

    #@18
    .line 1839
    invoke-virtual {v6}, Landroid/view/View;->clearFocus()V

    #@1b
    .line 1842
    :cond_1b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->pageBeginMoving()V

    #@1e
    .line 1843
    invoke-virtual {p0, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->awakenScrollBars(I)Z

    #@21
    .line 1844
    if-eqz p4, :cond_4a

    #@23
    .line 1845
    const/4 p3, 0x0

    #@24
    .line 1850
    :cond_24
    :goto_24
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@26
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_31

    #@2c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@2e
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    #@31
    .line 1851
    :cond_31
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@33
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUnboundedScrollX:I

    #@35
    move v3, p2

    #@36
    move v4, v2

    #@37
    move v5, p3

    #@38
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@3b
    .line 1853
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->notifyPageSwitched()V

    #@3e
    .line 1856
    if-eqz p4, :cond_43

    #@40
    .line 1857
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->computeScroll()V

    #@43
    .line 1860
    :cond_43
    const/4 v0, 0x1

    #@44
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mForceScreenScrolled:Z

    #@46
    .line 1861
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->invalidate()V

    #@49
    .line 1862
    return-void

    #@4a
    .line 1846
    :cond_4a
    if-nez p3, :cond_24

    #@4c
    .line 1847
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@4f
    move-result p3

    #@50
    goto :goto_24
.end method

.method protected snapToPage(IIZ)V
    .registers 9
    .parameter "whichPage"
    .parameter "duration"
    .parameter "immediate"

    #@0
    .prologue
    .line 1819
    const/4 v3, 0x0

    #@1
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@4
    move-result v4

    #@5
    add-int/lit8 v4, v4, -0x1

    #@7
    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    #@a
    move-result v4

    #@b
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@e
    move-result p1

    #@f
    .line 1824
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@12
    move-result v3

    #@13
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@16
    move-result v4

    #@17
    sub-int v1, v3, v4

    #@19
    .line 1825
    .local v1, newX:I
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUnboundedScrollX:I

    #@1b
    .line 1826
    .local v2, sX:I
    sub-int v0, v1, v2

    #@1d
    .line 1827
    .local v0, delta:I
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(IIIZ)V

    #@20
    .line 1828
    return-void
.end method

.method protected snapToPageImmediately(I)V
    .registers 4
    .parameter "whichPage"

    #@0
    .prologue
    .line 1812
    const/16 v0, 0x2ee

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(IIZ)V

    #@6
    .line 1813
    return-void
.end method

.method protected snapToPageWithVelocity(II)V
    .registers 12
    .parameter "whichPage"
    .parameter "velocity"

    #@0
    .prologue
    const/high16 v8, 0x3f80

    #@2
    .line 1772
    const/4 v6, 0x0

    #@3
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildCount()I

    #@6
    move-result v7

    #@7
    add-int/lit8 v7, v7, -0x1

    #@9
    invoke-static {p1, v7}, Ljava/lang/Math;->min(II)I

    #@c
    move-result v7

    #@d
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@10
    move-result p1

    #@11
    .line 1773
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportWidth()I

    #@14
    move-result v6

    #@15
    div-int/lit8 v4, v6, 0x2

    #@17
    .line 1778
    .local v4, halfScreenSize:I
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@1a
    move-result v6

    #@1b
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@1e
    move-result v7

    #@1f
    sub-int v5, v6, v7

    #@21
    .line 1779
    .local v5, newX:I
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mUnboundedScrollX:I

    #@23
    sub-int v0, v5, v6

    #@25
    .line 1780
    .local v0, delta:I
    const/4 v3, 0x0

    #@26
    .line 1782
    .local v3, duration:I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@29
    move-result v6

    #@2a
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinFlingVelocity:I

    #@2c
    if-ge v6, v7, :cond_34

    #@2e
    .line 1785
    const/16 v6, 0x2ee

    #@30
    invoke-virtual {p0, p1, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(II)V

    #@33
    .line 1806
    :goto_33
    return-void

    #@34
    .line 1793
    :cond_34
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@37
    move-result v6

    #@38
    int-to-float v6, v6

    #@39
    mul-float/2addr v6, v8

    #@3a
    mul-int/lit8 v7, v4, 0x2

    #@3c
    int-to-float v7, v7

    #@3d
    div-float/2addr v6, v7

    #@3e
    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    #@41
    move-result v2

    #@42
    .line 1794
    .local v2, distanceRatio:F
    int-to-float v6, v4

    #@43
    int-to-float v7, v4

    #@44
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->distanceInfluenceForSnapDuration(F)F

    #@47
    move-result v8

    #@48
    mul-float/2addr v7, v8

    #@49
    add-float v1, v6, v7

    #@4b
    .line 1797
    .local v1, distance:F
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@4e
    move-result p2

    #@4f
    .line 1798
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinSnapVelocity:I

    #@51
    invoke-static {v6, p2}, Ljava/lang/Math;->max(II)I

    #@54
    move-result p2

    #@55
    .line 1803
    const/high16 v6, 0x447a

    #@57
    int-to-float v7, p2

    #@58
    div-float v7, v1, v7

    #@5a
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    #@5d
    move-result v7

    #@5e
    mul-float/2addr v6, v7

    #@5f
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    #@62
    move-result v6

    #@63
    mul-int/lit8 v3, v6, 0x4

    #@65
    .line 1805
    invoke-virtual {p0, p1, v0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPage(III)V

    #@68
    goto :goto_33
.end method

.method public startReordering()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 2159
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageNearestToCenterOfScreen()I

    #@5
    move-result v0

    #@6
    .line 2160
    .local v0, dragViewIndex:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@8
    aput v2, v3, v2

    #@a
    .line 2161
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@f
    move-result v4

    #@10
    add-int/lit8 v4, v4, -0x1

    #@12
    aput v4, v3, v1

    #@14
    .line 2162
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@16
    invoke-virtual {p0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->boundByReorderablePages(Z[I)V

    #@19
    .line 2163
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mReorderingStarted:Z

    #@1b
    .line 2166
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@1d
    aget v3, v3, v2

    #@1f
    if-gt v3, v0, :cond_37

    #@21
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@23
    aget v3, v3, v1

    #@25
    if-gt v0, v3, :cond_37

    #@27
    .line 2168
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->zoomOut()Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_36

    #@2d
    .line 2170
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@30
    move-result-object v2

    #@31
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@33
    .line 2172
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onStartReordering()V

    #@36
    .line 2176
    :cond_36
    :goto_36
    return v1

    #@37
    :cond_37
    move v1, v2

    #@38
    goto :goto_36
.end method

.method protected updateCurrentPageScroll()V
    .registers 6

    #@0
    .prologue
    .line 427
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@5
    move-result v1

    #@6
    .line 428
    .local v1, offset:I
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@8
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getRelativeChildOffset(I)I

    #@b
    move-result v2

    #@c
    .line 429
    .local v2, relOffset:I
    sub-int v0, v1, v2

    #@e
    .line 430
    .local v0, newX:I
    const/4 v3, 0x0

    #@f
    invoke-virtual {p0, v0, v3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->scrollTo(II)V

    #@12
    .line 431
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@14
    invoke-virtual {v3, v0}, Landroid/widget/Scroller;->setFinalX(I)V

    #@17
    .line 432
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mScroller:Landroid/widget/Scroller;

    #@19
    const/4 v4, 0x1

    #@1a
    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@1d
    .line 433
    return-void
.end method

.method updateDragViewTranslationDuringDrag()V
    .registers 5

    #@0
    .prologue
    .line 335
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionX:F

    #@2
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionX:F

    #@4
    sub-float/2addr v2, v3

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScrollX()I

    #@8
    move-result v3

    #@9
    int-to-float v3, v3

    #@a
    add-float/2addr v2, v3

    #@b
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownScrollX:F

    #@d
    sub-float v0, v2, v3

    #@f
    .line 336
    .local v0, x:F
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mLastMotionY:F

    #@11
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDownMotionY:F

    #@13
    sub-float v1, v2, v3

    #@15
    .line 337
    .local v1, y:F
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@17
    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    #@1a
    .line 338
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@1c
    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    #@1f
    .line 341
    return-void
.end method

.method protected zoomIn(Ljava/lang/Runnable;)Z
    .registers 9
    .parameter "onCompleteRunnable"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    const/high16 v6, 0x3f80

    #@4
    .line 2220
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@6
    if-eqz v2, :cond_15

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@a
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_15

    #@10
    .line 2221
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@12
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    #@15
    .line 2223
    :cond_15
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaleX()F

    #@18
    move-result v2

    #@19
    cmpg-float v2, v2, v6

    #@1b
    if-ltz v2, :cond_25

    #@1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaleY()F

    #@20
    move-result v2

    #@21
    cmpg-float v2, v2, v6

    #@23
    if-gez v2, :cond_65

    #@25
    .line 2224
    :cond_25
    new-instance v2, Landroid/animation/AnimatorSet;

    #@27
    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    #@2a
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@2c
    .line 2225
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@2e
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@30
    int-to-long v3, v3

    #@31
    invoke-virtual {v2, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@34
    .line 2226
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@36
    const/4 v3, 0x2

    #@37
    new-array v3, v3, [Landroid/animation/Animator;

    #@39
    const-string v4, "scaleX"

    #@3b
    new-array v5, v1, [F

    #@3d
    aput v6, v5, v0

    #@3f
    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@42
    move-result-object v4

    #@43
    aput-object v4, v3, v0

    #@45
    const-string v4, "scaleY"

    #@47
    new-array v5, v1, [F

    #@49
    aput v6, v5, v0

    #@4b
    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@4e
    move-result-object v0

    #@4f
    aput-object v0, v3, v1

    #@51
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@54
    .line 2229
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@56
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/PagedView$8;

    #@58
    invoke-direct {v2, p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView$8;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Ljava/lang/Runnable;)V

    #@5b
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@5e
    .line 2256
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@60
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    #@63
    move v0, v1

    #@64
    .line 2263
    :cond_64
    :goto_64
    return v0

    #@65
    .line 2259
    :cond_65
    if-eqz p1, :cond_64

    #@67
    .line 2260
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    #@6a
    goto :goto_64
.end method

.method protected zoomOut()Z
    .registers 8

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    const/4 v0, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 2074
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@6
    if-eqz v2, :cond_15

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@a
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_15

    #@10
    .line 2075
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@12
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    #@15
    .line 2078
    :cond_15
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaleX()F

    #@18
    move-result v2

    #@19
    cmpg-float v2, v2, v3

    #@1b
    if-ltz v2, :cond_68

    #@1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getScaleY()F

    #@20
    move-result v2

    #@21
    cmpg-float v2, v2, v3

    #@23
    if-ltz v2, :cond_68

    #@25
    .line 2079
    new-instance v2, Landroid/animation/AnimatorSet;

    #@27
    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    #@2a
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@2c
    .line 2080
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@2e
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@30
    int-to-long v3, v3

    #@31
    invoke-virtual {v2, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@34
    .line 2081
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@36
    const/4 v3, 0x2

    #@37
    new-array v3, v3, [Landroid/animation/Animator;

    #@39
    const-string v4, "scaleX"

    #@3b
    new-array v5, v0, [F

    #@3d
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@3f
    aput v6, v5, v1

    #@41
    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@44
    move-result-object v4

    #@45
    aput-object v4, v3, v1

    #@47
    const-string v4, "scaleY"

    #@49
    new-array v5, v0, [F

    #@4b
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMinScale:F

    #@4d
    aput v6, v5, v1

    #@4f
    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@52
    move-result-object v1

    #@53
    aput-object v1, v3, v0

    #@55
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@58
    .line 2084
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@5a
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/PagedView$5;

    #@5c
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V

    #@5f
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@62
    .line 2101
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@64
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    #@67
    .line 2104
    :goto_67
    return v0

    #@68
    :cond_68
    move v0, v1

    #@69
    goto :goto_67
.end method
