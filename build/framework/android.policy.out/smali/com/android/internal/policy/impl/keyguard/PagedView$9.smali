.class Lcom/android/internal/policy/impl/keyguard/PagedView$9;
.super Ljava/lang/Object;
.source "PagedView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->createPostDeleteAnimationRunnable(Landroid/view/View;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

.field final synthetic val$dragView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 2329
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->val$dragView:Landroid/view/View;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 23

    #@0
    .prologue
    .line 2332
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@4
    move-object/from16 v16, v0

    #@6
    move-object/from16 v0, p0

    #@8
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->val$dragView:Landroid/view/View;

    #@a
    move-object/from16 v17, v0

    #@c
    invoke-virtual/range {v16 .. v17}, Lcom/android/internal/policy/impl/keyguard/PagedView;->indexOfChild(Landroid/view/View;)I

    #@f
    move-result v4

    #@10
    .line 2339
    .local v4, dragViewIndex:I
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@14
    move-object/from16 v16, v0

    #@16
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1a
    move-object/from16 v17, v0

    #@1c
    move-object/from16 v0, v17

    #@1e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@20
    move-object/from16 v17, v0

    #@22
    invoke-virtual/range {v16 .. v17}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getVisiblePages([I)V

    #@25
    .line 2340
    move-object/from16 v0, p0

    #@27
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@29
    move-object/from16 v16, v0

    #@2b
    const/16 v17, 0x1

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@31
    move-object/from16 v18, v0

    #@33
    move-object/from16 v0, v18

    #@35
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@37
    move-object/from16 v18, v0

    #@39
    invoke-virtual/range {v16 .. v18}, Lcom/android/internal/policy/impl/keyguard/PagedView;->boundByReorderablePages(Z[I)V

    #@3c
    .line 2341
    move-object/from16 v0, p0

    #@3e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@40
    move-object/from16 v16, v0

    #@42
    move-object/from16 v0, v16

    #@44
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@46
    move-object/from16 v16, v0

    #@48
    const/16 v17, 0x0

    #@4a
    aget v16, v16, v17

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@50
    move-object/from16 v17, v0

    #@52
    move-object/from16 v0, v17

    #@54
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@56
    move-object/from16 v17, v0

    #@58
    const/16 v18, 0x1

    #@5a
    aget v17, v17, v18

    #@5c
    move/from16 v0, v16

    #@5e
    move/from16 v1, v17

    #@60
    if-ne v0, v1, :cond_18e

    #@62
    const/4 v7, 0x1

    #@63
    .line 2342
    .local v7, isLastWidgetPage:Z
    :goto_63
    if-nez v7, :cond_79

    #@65
    move-object/from16 v0, p0

    #@67
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@69
    move-object/from16 v16, v0

    #@6b
    move-object/from16 v0, v16

    #@6d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@6f
    move-object/from16 v16, v0

    #@71
    const/16 v17, 0x0

    #@73
    aget v16, v16, v17

    #@75
    move/from16 v0, v16

    #@77
    if-le v4, v0, :cond_191

    #@79
    :cond_79
    const/4 v13, 0x1

    #@7a
    .line 2346
    .local v13, slideFromLeft:Z
    :goto_7a
    if-eqz v13, :cond_87

    #@7c
    .line 2347
    move-object/from16 v0, p0

    #@7e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@80
    move-object/from16 v16, v0

    #@82
    add-int/lit8 v17, v4, -0x1

    #@84
    invoke-virtual/range {v16 .. v17}, Lcom/android/internal/policy/impl/keyguard/PagedView;->snapToPageImmediately(I)V

    #@87
    .line 2350
    :cond_87
    if-eqz v7, :cond_194

    #@89
    const/4 v5, 0x0

    #@8a
    .line 2351
    .local v5, firstIndex:I
    :goto_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@8e
    move-object/from16 v16, v0

    #@90
    move-object/from16 v0, v16

    #@92
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@94
    move-object/from16 v16, v0

    #@96
    const/16 v17, 0x1

    #@98
    aget v16, v16, v17

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@9e
    move-object/from16 v17, v0

    #@a0
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getPageCount()I

    #@a3
    move-result v17

    #@a4
    add-int/lit8 v17, v17, -0x1

    #@a6
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    #@a9
    move-result v8

    #@aa
    .line 2352
    .local v8, lastIndex:I
    if-eqz v13, :cond_1a6

    #@ac
    move v9, v5

    #@ad
    .line 2353
    .local v9, lowerIndex:I
    :goto_ad
    if-eqz v13, :cond_1aa

    #@af
    add-int/lit8 v14, v4, -0x1

    #@b1
    .line 2354
    .local v14, upperIndex:I
    :goto_b1
    new-instance v3, Ljava/util/ArrayList;

    #@b3
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@b6
    .line 2355
    .local v3, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    move v6, v9

    #@b7
    .local v6, i:I
    :goto_b7
    if-gt v6, v14, :cond_1e4

    #@b9
    .line 2356
    move-object/from16 v0, p0

    #@bb
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@bd
    move-object/from16 v16, v0

    #@bf
    move-object/from16 v0, v16

    #@c1
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildAt(I)Landroid/view/View;

    #@c4
    move-result-object v15

    #@c5
    .line 2360
    .local v15, v:Landroid/view/View;
    const/4 v11, 0x0

    #@c6
    .line 2361
    .local v11, oldX:I
    const/4 v10, 0x0

    #@c7
    .line 2362
    .local v10, newX:I
    if-eqz v13, :cond_1c7

    #@c9
    .line 2363
    if-nez v6, :cond_1ad

    #@cb
    .line 2365
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@cf
    move-object/from16 v16, v0

    #@d1
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@d4
    move-result v16

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@d9
    move-object/from16 v17, v0

    #@db
    move-object/from16 v0, v17

    #@dd
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@e0
    move-result v17

    #@e1
    add-int v16, v16, v17

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@e7
    move-object/from16 v17, v0

    #@e9
    move-object/from16 v0, v17

    #@eb
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildWidth(I)I

    #@ee
    move-result v17

    #@ef
    sub-int v16, v16, v17

    #@f1
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@f5
    move-object/from16 v17, v0

    #@f7
    move-object/from16 v0, v17

    #@f9
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mPageSpacing:I

    #@fb
    move/from16 v17, v0

    #@fd
    sub-int v11, v16, v17

    #@ff
    .line 2370
    :goto_ff
    move-object/from16 v0, p0

    #@101
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@103
    move-object/from16 v16, v0

    #@105
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@108
    move-result v16

    #@109
    move-object/from16 v0, p0

    #@10b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@10d
    move-object/from16 v17, v0

    #@10f
    move-object/from16 v0, v17

    #@111
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@114
    move-result v17

    #@115
    add-int v10, v16, v17

    #@117
    .line 2378
    :goto_117
    invoke-virtual {v15}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@11a
    move-result-object v2

    #@11b
    check-cast v2, Landroid/animation/AnimatorSet;

    #@11d
    .line 2379
    .local v2, anim:Landroid/animation/AnimatorSet;
    if-eqz v2, :cond_122

    #@11f
    .line 2380
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    #@122
    .line 2385
    :cond_122
    invoke-virtual {v15}, Landroid/view/View;->getAlpha()F

    #@125
    move-result v16

    #@126
    const v17, 0x3c23d70a

    #@129
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(FF)F

    #@12c
    move-result v16

    #@12d
    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setAlpha(F)V

    #@130
    .line 2386
    sub-int v16, v11, v10

    #@132
    move/from16 v0, v16

    #@134
    int-to-float v0, v0

    #@135
    move/from16 v16, v0

    #@137
    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setTranslationX(F)V

    #@13a
    .line 2387
    new-instance v2, Landroid/animation/AnimatorSet;

    #@13c
    .end local v2           #anim:Landroid/animation/AnimatorSet;
    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    #@13f
    .line 2388
    .restart local v2       #anim:Landroid/animation/AnimatorSet;
    const/16 v16, 0x2

    #@141
    move/from16 v0, v16

    #@143
    new-array v0, v0, [Landroid/animation/Animator;

    #@145
    move-object/from16 v16, v0

    #@147
    const/16 v17, 0x0

    #@149
    const-string v18, "translationX"

    #@14b
    const/16 v19, 0x1

    #@14d
    move/from16 v0, v19

    #@14f
    new-array v0, v0, [F

    #@151
    move-object/from16 v19, v0

    #@153
    const/16 v20, 0x0

    #@155
    const/16 v21, 0x0

    #@157
    aput v21, v19, v20

    #@159
    move-object/from16 v0, v18

    #@15b
    move-object/from16 v1, v19

    #@15d
    invoke-static {v15, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@160
    move-result-object v18

    #@161
    aput-object v18, v16, v17

    #@163
    const/16 v17, 0x1

    #@165
    const-string v18, "alpha"

    #@167
    const/16 v19, 0x1

    #@169
    move/from16 v0, v19

    #@16b
    new-array v0, v0, [F

    #@16d
    move-object/from16 v19, v0

    #@16f
    const/16 v20, 0x0

    #@171
    const/high16 v21, 0x3f80

    #@173
    aput v21, v19, v20

    #@175
    move-object/from16 v0, v18

    #@177
    move-object/from16 v1, v19

    #@179
    invoke-static {v15, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@17c
    move-result-object v18

    #@17d
    aput-object v18, v16, v17

    #@17f
    move-object/from16 v0, v16

    #@181
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@184
    .line 2391
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@187
    .line 2392
    invoke-virtual {v15, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@18a
    .line 2355
    add-int/lit8 v6, v6, 0x1

    #@18c
    goto/16 :goto_b7

    #@18e
    .line 2341
    .end local v2           #anim:Landroid/animation/AnimatorSet;
    .end local v3           #animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .end local v5           #firstIndex:I
    .end local v6           #i:I
    .end local v7           #isLastWidgetPage:Z
    .end local v8           #lastIndex:I
    .end local v9           #lowerIndex:I
    .end local v10           #newX:I
    .end local v11           #oldX:I
    .end local v13           #slideFromLeft:Z
    .end local v14           #upperIndex:I
    .end local v15           #v:Landroid/view/View;
    :cond_18e
    const/4 v7, 0x0

    #@18f
    goto/16 :goto_63

    #@191
    .line 2342
    .restart local v7       #isLastWidgetPage:Z
    :cond_191
    const/4 v13, 0x0

    #@192
    goto/16 :goto_7a

    #@194
    .line 2350
    .restart local v13       #slideFromLeft:Z
    :cond_194
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@198
    move-object/from16 v16, v0

    #@19a
    move-object/from16 v0, v16

    #@19c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@19e
    move-object/from16 v16, v0

    #@1a0
    const/16 v17, 0x0

    #@1a2
    aget v5, v16, v17

    #@1a4
    goto/16 :goto_8a

    #@1a6
    .line 2352
    .restart local v5       #firstIndex:I
    .restart local v8       #lastIndex:I
    :cond_1a6
    add-int/lit8 v9, v4, 0x1

    #@1a8
    goto/16 :goto_ad

    #@1aa
    .restart local v9       #lowerIndex:I
    :cond_1aa
    move v14, v8

    #@1ab
    .line 2353
    goto/16 :goto_b1

    #@1ad
    .line 2368
    .restart local v3       #animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .restart local v6       #i:I
    .restart local v10       #newX:I
    .restart local v11       #oldX:I
    .restart local v14       #upperIndex:I
    .restart local v15       #v:Landroid/view/View;
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1b1
    move-object/from16 v16, v0

    #@1b3
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getViewportOffsetX()I

    #@1b6
    move-result v16

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1bb
    move-object/from16 v17, v0

    #@1bd
    add-int/lit8 v18, v6, -0x1

    #@1bf
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@1c2
    move-result v17

    #@1c3
    add-int v11, v16, v17

    #@1c5
    goto/16 :goto_ff

    #@1c7
    .line 2372
    :cond_1c7
    move-object/from16 v0, p0

    #@1c9
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1cb
    move-object/from16 v16, v0

    #@1cd
    move-object/from16 v0, v16

    #@1cf
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@1d2
    move-result v16

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1d7
    move-object/from16 v17, v0

    #@1d9
    add-int/lit8 v18, v6, -0x1

    #@1db
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/policy/impl/keyguard/PagedView;->getChildOffset(I)I

    #@1de
    move-result v17

    #@1df
    sub-int v11, v16, v17

    #@1e1
    .line 2373
    const/4 v10, 0x0

    #@1e2
    goto/16 :goto_117

    #@1e4
    .line 2395
    .end local v10           #newX:I
    .end local v11           #oldX:I
    .end local v15           #v:Landroid/view/View;
    :cond_1e4
    new-instance v12, Landroid/animation/AnimatorSet;

    #@1e6
    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    #@1e9
    .line 2396
    .local v12, slideAnimations:Landroid/animation/AnimatorSet;
    invoke-virtual {v12, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    #@1ec
    .line 2397
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@1f0
    move-object/from16 v16, v0

    #@1f2
    invoke-static/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$700(Lcom/android/internal/policy/impl/keyguard/PagedView;)I

    #@1f5
    move-result v16

    #@1f6
    move/from16 v0, v16

    #@1f8
    int-to-long v0, v0

    #@1f9
    move-wide/from16 v16, v0

    #@1fb
    move-wide/from16 v0, v16

    #@1fd
    invoke-virtual {v12, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@200
    .line 2398
    new-instance v16, Lcom/android/internal/policy/impl/keyguard/PagedView$9$1;

    #@202
    move-object/from16 v0, v16

    #@204
    move-object/from16 v1, p0

    #@206
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/PagedView$9$1;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView$9;)V

    #@209
    move-object/from16 v0, v16

    #@20b
    invoke-virtual {v12, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@20e
    .line 2412
    invoke-virtual {v12}, Landroid/animation/AnimatorSet;->start()V

    #@211
    .line 2414
    move-object/from16 v0, p0

    #@213
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@215
    move-object/from16 v16, v0

    #@217
    move-object/from16 v0, p0

    #@219
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->val$dragView:Landroid/view/View;

    #@21b
    move-object/from16 v17, v0

    #@21d
    invoke-virtual/range {v16 .. v17}, Lcom/android/internal/policy/impl/keyguard/PagedView;->removeView(Landroid/view/View;)V

    #@220
    .line 2415
    move-object/from16 v0, p0

    #@222
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@224
    move-object/from16 v16, v0

    #@226
    move-object/from16 v0, p0

    #@228
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView$9;->val$dragView:Landroid/view/View;

    #@22a
    move-object/from16 v17, v0

    #@22c
    const/16 v18, 0x1

    #@22e
    invoke-virtual/range {v16 .. v18}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onRemoveView(Landroid/view/View;Z)V

    #@231
    .line 2416
    return-void
.end method
