.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimArgs"
.end annotation


# instance fields
.field public final simState:Lcom/android/internal/telephony/IccCardConstants$State;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 199
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 200
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@5
    .line 201
    return-void
.end method

.method static fromIntent(Landroid/content/Intent;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    .line 205
    const-string v4, "android.intent.action.SIM_STATE_CHANGED"

    #@2
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v5

    #@6
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_14

    #@c
    .line 206
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v5, "only handles intent ACTION_SIM_STATE_CHANGED"

    #@10
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v4

    #@14
    .line 208
    :cond_14
    const-string v4, "ss"

    #@16
    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    .line 209
    .local v3, stateExtra:Ljava/lang/String;
    const-string v4, "ABSENT"

    #@1c
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_3b

    #@22
    .line 210
    const-string v4, "reason"

    #@24
    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 213
    .local v0, absentReason:Ljava/lang/String;
    const-string v4, "PERM_DISABLED"

    #@2a
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_38

    #@30
    .line 215
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@32
    .line 236
    .end local v0           #absentReason:Ljava/lang/String;
    .local v2, state:Lcom/android/internal/telephony/IccCardConstants$State;
    :goto_32
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;

    #@34
    invoke-direct {v4, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;-><init>(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@37
    return-object v4

    #@38
    .line 217
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    .restart local v0       #absentReason:Ljava/lang/String;
    :cond_38
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3a
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@3b
    .line 219
    .end local v0           #absentReason:Ljava/lang/String;
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_3b
    const-string v4, "READY"

    #@3d
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_46

    #@43
    .line 220
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@45
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@46
    .line 221
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_46
    const-string v4, "LOCKED"

    #@48
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_78

    #@4e
    .line 222
    const-string v4, "reason"

    #@50
    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    .line 224
    .local v1, lockedReason:Ljava/lang/String;
    const-string v4, "PIN"

    #@56
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_5f

    #@5c
    .line 225
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@5e
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@5f
    .line 226
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_5f
    const-string v4, "PUK"

    #@61
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v4

    #@65
    if-eqz v4, :cond_6a

    #@67
    .line 227
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@69
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@6a
    .line 228
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_6a
    const-string v4, "PERSO"

    #@6c
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v4

    #@70
    if-eqz v4, :cond_75

    #@72
    .line 229
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@74
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@75
    .line 231
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_75
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@77
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32

    #@78
    .line 234
    .end local v1           #lockedReason:Ljava/lang/String;
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_78
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7a
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_32
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
