.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;
.super Ljava/lang/Object;
.source "KeyguardViewMediator.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 629
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public keyguardDone(Z)V
    .registers 4
    .parameter "authenticated"

    #@0
    .prologue
    .line 643
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@6
    .line 644
    return-void
.end method

.method public keyguardDoneDrawing()V
    .registers 6

    #@0
    .prologue
    .line 648
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isBooting:Z

    #@2
    if-eqz v1, :cond_34

    #@4
    .line 649
    const v1, 0xd6db

    #@7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a
    move-result-wide v2

    #@b
    invoke-static {v1, v2, v3}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@e
    .line 651
    :try_start_e
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v1}, Landroid/app/IActivityManager;->bootAniEnd()V

    #@15
    .line 652
    const-string v1, "boot_time"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "LockScreen Start! [ 55003 ] ==>"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@25
    move-result-wide v3

    #@26
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_31} :catch_40

    #@31
    .line 657
    :goto_31
    const/4 v1, 0x0

    #@32
    sput-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isBooting:Z

    #@34
    .line 660
    :cond_34
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@36
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/os/Handler;

    #@39
    move-result-object v1

    #@3a
    const/16 v2, 0xa

    #@3c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@3f
    .line 661
    return-void

    #@40
    .line 653
    :catch_40
    move-exception v0

    #@41
    .line 654
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "KeyguardViewMediator"

    #@43
    const-string v2, "Failed to stop bootAnimation."

    #@45
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_31
.end method

.method public keyguardDonePending()V
    .registers 3

    #@0
    .prologue
    .line 675
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2102(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@6
    .line 676
    return-void
.end method

.method public onUserActivityTimeoutChanged()V
    .registers 2

    #@0
    .prologue
    .line 670
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->updateUserActivityTimeout()V

    #@9
    .line 671
    return-void
.end method

.method public setNaviHidden(Z)V
    .registers 4
    .parameter "isNaviHidden"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 682
    if-ne p1, v1, :cond_9

    #@3
    .line 683
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@5
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@8
    .line 687
    :goto_8
    return-void

    #@9
    .line 685
    :cond_9
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@f
    goto :goto_8
.end method

.method public setNeedsInput(Z)V
    .registers 3
    .parameter "needsInput"

    #@0
    .prologue
    .line 665
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->setNeedsInput(Z)V

    #@9
    .line 666
    return-void
.end method

.method public userActivity()V
    .registers 2

    #@0
    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity()V

    #@5
    .line 636
    return-void
.end method

.method public userActivity(J)V
    .registers 4
    .parameter "holdMs"

    #@0
    .prologue
    .line 639
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity(J)V

    #@5
    .line 640
    return-void
.end method

.method public wakeUp()V
    .registers 2

    #@0
    .prologue
    .line 631
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeUp()V

    #@5
    .line 632
    return-void
.end method
