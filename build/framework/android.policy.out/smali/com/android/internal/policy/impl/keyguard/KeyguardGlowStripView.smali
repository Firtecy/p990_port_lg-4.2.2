.class public Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;
.super Landroid/widget/LinearLayout;
.source "KeyguardGlowStripView.java"


# static fields
.field private static final DURATION:I = 0x1f4

.field private static final SLIDING_WINDOW_SIZE:F = 0.4f


# instance fields
.field private mAnimationProgress:F

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mDotAlphaInterpolator:Landroid/view/animation/Interpolator;

.field private mDotDrawable:Landroid/graphics/drawable/Drawable;

.field private mDotSize:I

.field private mDotStripTop:I

.field private mDrawDots:Z

.field private mHorizontalDotGap:I

.field private mLeftToRight:Z

.field private mNumDots:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 56
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 60
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 48
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mLeftToRight:Z

    #@7
    .line 50
    const/4 v1, 0x0

    #@8
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimationProgress:F

    #@a
    .line 51
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDrawDots:Z

    #@c
    .line 53
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    #@e
    const/high16 v2, 0x3f00

    #@10
    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@13
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotAlphaInterpolator:Landroid/view/animation/Interpolator;

    #@15
    .line 66
    sget-object v1, Lcom/android/internal/R$styleable;->KeyguardGlowStripView:[I

    #@17
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@1a
    move-result-object v0

    #@1b
    .line 67
    .local v0, a:Landroid/content/res/TypedArray;
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@1d
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@20
    move-result v1

    #@21
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@23
    .line 68
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@25
    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@28
    move-result v1

    #@29
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@2b
    .line 69
    const/4 v1, 0x2

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2f
    move-result-object v1

    #@30
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    #@32
    .line 70
    const/4 v1, 0x3

    #@33
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mLeftToRight:Z

    #@35
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@38
    move-result v1

    #@39
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mLeftToRight:Z

    #@3b
    .line 71
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDrawDots:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 38
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimationProgress:F

    #@2
    return p1
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/high16 v9, 0x3f80

    #@3
    const v8, 0x3e4ccccd

    #@6
    .line 82
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@9
    .line 84
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDrawDots:Z

    #@b
    if-nez v5, :cond_e

    #@d
    .line 106
    :cond_d
    return-void

    #@e
    .line 86
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->getPaddingLeft()I

    #@11
    move-result v4

    #@12
    .line 87
    .local v4, xOffset:I
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    #@14
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@16
    iget v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@18
    invoke-virtual {v5, v10, v10, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1b
    .line 89
    const/4 v2, 0x0

    #@1c
    .local v2, i:I
    :goto_1c
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@1e
    if-ge v2, v5, :cond_d

    #@20
    .line 92
    int-to-float v5, v2

    #@21
    mul-float/2addr v5, v9

    #@22
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@24
    add-int/lit8 v6, v6, -0x1

    #@26
    int-to-float v6, v6

    #@27
    div-float/2addr v5, v6

    #@28
    const v6, 0x3f19999a

    #@2b
    mul-float/2addr v5, v6

    #@2c
    add-float v3, v8, v5

    #@2e
    .line 94
    .local v3, relativeDotPosition:F
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimationProgress:F

    #@30
    sub-float v5, v3, v5

    #@32
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    #@35
    move-result v1

    #@36
    .line 95
    .local v1, distance:F
    const/4 v5, 0x0

    #@37
    div-float v6, v1, v8

    #@39
    sub-float v6, v9, v6

    #@3b
    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    #@3e
    move-result v0

    #@3f
    .line 97
    .local v0, alpha:F
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotAlphaInterpolator:Landroid/view/animation/Interpolator;

    #@41
    invoke-interface {v5, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@44
    move-result v0

    #@45
    .line 99
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@48
    .line 100
    int-to-float v5, v4

    #@49
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotStripTop:I

    #@4b
    int-to-float v6, v6

    #@4c
    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    #@4f
    .line 101
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    #@51
    const/high16 v6, 0x437f

    #@53
    mul-float/2addr v6, v0

    #@54
    float-to-int v6, v6

    #@55
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@58
    .line 102
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    #@5a
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@5d
    .line 103
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@60
    .line 104
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@62
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mHorizontalDotGap:I

    #@64
    add-int/2addr v5, v6

    #@65
    add-int/2addr v4, v5

    #@66
    .line 89
    add-int/lit8 v2, v2, 0x1

    #@68
    goto :goto_1c
.end method

.method public makeEmGo()V
    .registers 6

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    const/4 v2, 0x0

    #@3
    .line 109
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@5
    if-eqz v4, :cond_c

    #@7
    .line 110
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@9
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    #@c
    .line 112
    :cond_c
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mLeftToRight:Z

    #@e
    if-eqz v4, :cond_50

    #@10
    move v0, v2

    #@11
    .line 113
    .local v0, from:F
    :goto_11
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mLeftToRight:Z

    #@13
    if-eqz v4, :cond_52

    #@15
    move v1, v3

    #@16
    .line 114
    .local v1, to:F
    :goto_16
    const/4 v2, 0x2

    #@17
    new-array v2, v2, [F

    #@19
    const/4 v3, 0x0

    #@1a
    aput v0, v2, v3

    #@1c
    const/4 v3, 0x1

    #@1d
    aput v1, v2, v3

    #@1f
    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    #@22
    move-result-object v2

    #@23
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@25
    .line 115
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@27
    const-wide/16 v3, 0x1f4

    #@29
    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@2c
    .line 116
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@2e
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    #@30
    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@33
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@36
    .line 117
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@38
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView$1;

    #@3a
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;)V

    #@3d
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@40
    .line 130
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@42
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView$2;

    #@44
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;)V

    #@47
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@4a
    .line 137
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mAnimator:Landroid/animation/ValueAnimator;

    #@4c
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    #@4f
    .line 138
    return-void

    #@50
    .end local v0           #from:F
    .end local v1           #to:F
    :cond_50
    move v0, v3

    #@51
    .line 112
    goto :goto_11

    #@52
    .restart local v0       #from:F
    :cond_52
    move v1, v2

    #@53
    .line 113
    goto :goto_16
.end method

.method protected onSizeChanged(IIII)V
    .registers 8
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->getPaddingLeft()I

    #@3
    move-result v1

    #@4
    sub-int v1, p1, v1

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->getPaddingRight()I

    #@9
    move-result v2

    #@a
    sub-int v0, v1, v2

    #@c
    .line 75
    .local v0, availableWidth:I
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotSize:I

    #@e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@10
    mul-int/2addr v1, v2

    #@11
    sub-int v1, v0, v1

    #@13
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mNumDots:I

    #@15
    add-int/lit8 v2, v2, -0x1

    #@17
    div-int/2addr v1, v2

    #@18
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mHorizontalDotGap:I

    #@1a
    .line 76
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->getPaddingTop()I

    #@1d
    move-result v1

    #@1e
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->mDotStripTop:I

    #@20
    .line 77
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardGlowStripView;->invalidate()V

    #@23
    .line 78
    return-void
.end method
