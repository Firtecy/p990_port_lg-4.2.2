.class final enum Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
.super Ljava/lang/Enum;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/GlobalActions$ToggleAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

.field public static final enum Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

.field public static final enum On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

.field public static final enum TurningOff:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

.field public static final enum TurningOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;


# instance fields
.field private final inTransition:Z


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 724
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@6
    const-string v1, "Off"

    #@8
    invoke-direct {v0, v1, v2, v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;-><init>(Ljava/lang/String;IZ)V

    #@b
    sput-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@d
    .line 725
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@f
    const-string v1, "TurningOn"

    #@11
    invoke-direct {v0, v1, v3, v3}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;-><init>(Ljava/lang/String;IZ)V

    #@14
    sput-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@16
    .line 726
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@18
    const-string v1, "TurningOff"

    #@1a
    invoke-direct {v0, v1, v4, v3}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;-><init>(Ljava/lang/String;IZ)V

    #@1d
    sput-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOff:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@1f
    .line 727
    new-instance v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@21
    const-string v1, "On"

    #@23
    invoke-direct {v0, v1, v5, v2}, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;-><init>(Ljava/lang/String;IZ)V

    #@26
    sput-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@28
    .line 723
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2b
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->Off:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOn:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->TurningOff:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->On:Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->$VALUES:[Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .registers 4
    .parameter
    .parameter
    .parameter "intermediate"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 731
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 732
    iput-boolean p3, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->inTransition:Z

    #@5
    .line 733
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 723
    const-class v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;
    .registers 1

    #@0
    .prologue
    .line 723
    sget-object v0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->$VALUES:[Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;

    #@8
    return-object v0
.end method


# virtual methods
.method public inTransition()Z
    .registers 2

    #@0
    .prologue
    .line 736
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/GlobalActions$ToggleAction$State;->inTransition:Z

    #@2
    return v0
.end method
