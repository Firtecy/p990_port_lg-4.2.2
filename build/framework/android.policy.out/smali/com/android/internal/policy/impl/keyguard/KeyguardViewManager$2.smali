.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 204
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 10
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 207
    const/4 v0, 0x0

    #@1
    .line 208
    .local v0, cnt:I
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@4
    move-result-object v4

    #@5
    const-string v5, "KeyguardViewManager.ServiceConnection.onServiceConnected(): IScreenLockService onServiceConnected"

    #@7
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 210
    invoke-static {p2}, Lcom/nttdocomo/android/screenlockservice/IScreenLockService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/nttdocomo/android/screenlockservice/IScreenLockService;

    #@d
    move-result-object v3

    #@e
    .line 213
    .local v3, spLinkService:Lcom/nttdocomo/android/screenlockservice/IScreenLockService;
    if-eqz v3, :cond_45

    #@10
    .line 214
    :try_start_10
    invoke-interface {v3}, Lcom/nttdocomo/android/screenlockservice/IScreenLockService;->getUnreadCount()I

    #@13
    move-result v0

    #@14
    .line 215
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "KeyguardViewManager.ServiceConnection.onServiceConnected(): spmode cnt="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 219
    new-instance v2, Landroid/content/Intent;

    #@30
    const-string v4, "jp.co.nttdocomo.carriermail.APP_LINK_RECEIVED_MESSAGE_LOCAL"

    #@32
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@35
    .line 221
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "spcnt"

    #@37
    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3a
    .line 222
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3c
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@3f
    move-result-object v4

    #@40
    const-string v5, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@42
    invoke-virtual {v4, v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_45} :catch_55

    #@45
    .line 230
    .end local v2           #intent:Landroid/content/Intent;
    :cond_45
    :goto_45
    :try_start_45
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@47
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@4a
    move-result-object v4

    #@4b
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@4d
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/ServiceConnection;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v4, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_54
    .catch Ljava/lang/IllegalArgumentException; {:try_start_45 .. :try_end_54} :catch_60

    #@54
    .line 235
    :goto_54
    return-void

    #@55
    .line 225
    :catch_55
    move-exception v1

    #@56
    .line 226
    .local v1, e:Ljava/lang/Exception;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    const-string v5, "KeyguardViewManager.ServiceConnection.onServiceConnected(): can\'t get unread count"

    #@5c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_45

    #@60
    .line 231
    .end local v1           #e:Ljava/lang/Exception;
    :catch_60
    move-exception v1

    #@61
    .line 232
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    new-instance v5, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v6, "KeyguardViewManager.ServiceConnection.onServiceConnected(): "

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_54
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 238
    return-void
.end method
