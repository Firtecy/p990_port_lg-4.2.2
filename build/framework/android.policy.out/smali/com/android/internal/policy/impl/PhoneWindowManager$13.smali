.class Lcom/android/internal/policy/impl/PhoneWindowManager$13;
.super Landroid/content/BroadcastReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1729
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 24
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1732
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 1734
    .local v3, action:Ljava/lang/String;
    const-string v18, "WindowManager"

    #@6
    new-instance v19, Ljava/lang/StringBuilder;

    #@8
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v20, "112APP_KEY : onReceive action = "

    #@d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v19

    #@11
    move-object/from16 v0, v19

    #@13
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v19

    #@17
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v19

    #@1b
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1736
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@22
    move-object/from16 v18, v0

    #@24
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2600(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@27
    move-result v18

    #@28
    if-nez v18, :cond_2b

    #@2a
    .line 1844
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 1740
    :cond_2b
    const-string v18, "android.intent.action.NEW_OUTGOING_CALL"

    #@2d
    move-object/from16 v0, v18

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v18

    #@33
    if-eqz v18, :cond_b5

    #@35
    .line 1741
    const-string v18, "android.intent.extra.PHONE_NUMBER"

    #@37
    move-object/from16 v0, p2

    #@39
    move-object/from16 v1, v18

    #@3b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    .line 1743
    .local v5, callingNumber:Ljava/lang/String;
    const-string v18, "WindowManager"

    #@41
    new-instance v19, Ljava/lang/StringBuilder;

    #@43
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v20, "112APP_KEY : callingNumber="

    #@48
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v19

    #@4c
    move-object/from16 v0, v19

    #@4e
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v19

    #@52
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v19

    #@56
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 1745
    if-nez v5, :cond_63

    #@5b
    .line 1746
    const-string v18, "WindowManager"

    #@5d
    const-string v19, "112APP_KEY : callingNumber is null, so returns. "

    #@5f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_2a

    #@63
    .line 1750
    :cond_63
    const-string v18, "112"

    #@65
    move-object/from16 v0, v18

    #@67
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v18

    #@6b
    if-nez v18, :cond_77

    #@6d
    const-string v18, "02112"

    #@6f
    move-object/from16 v0, v18

    #@71
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v18

    #@75
    if-eqz v18, :cond_b5

    #@77
    .line 1751
    :cond_77
    move-object/from16 v0, p0

    #@79
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@7b
    move-object/from16 v18, v0

    #@7d
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@80
    move-result v18

    #@81
    if-nez v18, :cond_ee

    #@83
    .line 1752
    move-object/from16 v0, p0

    #@85
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@87
    move-object/from16 v18, v0

    #@89
    move-object/from16 v0, v18

    #@8b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8d
    move-object/from16 v18, v0

    #@8f
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@92
    move-result-object v18

    #@93
    const-string v19, "go.police.report"

    #@95
    invoke-virtual/range {v18 .. v19}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@98
    move-result-object v12

    #@99
    .line 1753
    .local v12, intent112App:Landroid/content/Intent;
    const-string v18, "startFlag"

    #@9b
    const-string v19, "001"

    #@9d
    move-object/from16 v0, v18

    #@9f
    move-object/from16 v1, v19

    #@a1
    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a4
    .line 1754
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@a8
    move-object/from16 v18, v0

    #@aa
    move-object/from16 v0, v18

    #@ac
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@ae
    move-object/from16 v18, v0

    #@b0
    move-object/from16 v0, v18

    #@b2
    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@b5
    .line 1761
    .end local v5           #callingNumber:Ljava/lang/String;
    .end local v12           #intent112App:Landroid/content/Intent;
    :cond_b5
    :goto_b5
    const-string v18, "android.intent.KOR_GO_POLICE_REPORT"

    #@b7
    move-object/from16 v0, v18

    #@b9
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc
    move-result v18

    #@bd
    if-eqz v18, :cond_2a

    #@bf
    .line 1762
    const-string v18, "IntentFlag"

    #@c1
    move-object/from16 v0, p2

    #@c3
    move-object/from16 v1, v18

    #@c5
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@c8
    move-result-object v13

    #@c9
    .line 1763
    .local v13, intentFlag:Ljava/lang/String;
    const-string v18, "WindowManager"

    #@cb
    new-instance v19, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v20, "IntentFlag = "

    #@d2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v19

    #@d6
    move-object/from16 v0, v19

    #@d8
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v19

    #@dc
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v19

    #@e0
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 1765
    if-nez v13, :cond_fa

    #@e5
    .line 1766
    const-string v18, "WindowManager"

    #@e7
    const-string v19, "112APP_KEY : intentFlag is null, so returns. "

    #@e9
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    goto/16 :goto_2a

    #@ee
    .line 1756
    .end local v13           #intentFlag:Ljava/lang/String;
    .restart local v5       #callingNumber:Ljava/lang/String;
    :cond_ee
    move-object/from16 v0, p0

    #@f0
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@f2
    move-object/from16 v18, v0

    #@f4
    const/16 v19, 0x0

    #@f6
    invoke-static/range {v18 .. v19}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2502(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@f9
    goto :goto_b5

    #@fa
    .line 1770
    .end local v5           #callingNumber:Ljava/lang/String;
    .restart local v13       #intentFlag:Ljava/lang/String;
    :cond_fa
    const/4 v7, 0x0

    #@fb
    .line 1771
    .local v7, dataNetworkEnable:Z
    const/16 v17, 0x0

    #@fd
    .line 1772
    .local v17, wifiNetworkEnable:Z
    const/4 v9, 0x0

    #@fe
    .line 1773
    .local v9, gpsProviderValue:Z
    const/16 v16, 0x0

    #@100
    .line 1774
    .local v16, networkProviderEnable:Z
    const/4 v4, 0x0

    #@101
    .line 1776
    .local v4, agpsProviderEnable:Z
    const-string v18, "start"

    #@103
    move-object/from16 v0, v18

    #@105
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@108
    move-result v18

    #@109
    if-eqz v18, :cond_1e9

    #@10b
    .line 1778
    const/4 v4, 0x1

    #@10c
    move/from16 v16, v4

    #@10e
    .local v16, networkProviderEnable:I
    move v9, v4

    #@10f
    .local v9, gpsProviderValue:I
    move/from16 v17, v4

    #@111
    .local v17, wifiNetworkEnable:I
    move v7, v4

    #@112
    .line 1816
    .end local v7           #dataNetworkEnable:Z
    .end local v9           #gpsProviderValue:I
    .end local v16           #networkProviderEnable:I
    .end local v17           #wifiNetworkEnable:I
    :cond_112
    :goto_112
    const-string v18, "WindowManager"

    #@114
    new-instance v19, Ljava/lang/StringBuilder;

    #@116
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v20, "112APP_KEY : Now set what app wants. data : "

    #@11b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v19

    #@11f
    move-object/from16 v0, v19

    #@121
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@124
    move-result-object v19

    #@125
    const-string v20, ", wifi : "

    #@127
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v19

    #@12b
    move-object/from16 v0, v19

    #@12d
    move/from16 v1, v17

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@132
    move-result-object v19

    #@133
    const-string v20, ", gps : "

    #@135
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v19

    #@139
    move-object/from16 v0, v19

    #@13b
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v19

    #@13f
    const-string v20, ", network : "

    #@141
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v19

    #@145
    move-object/from16 v0, v19

    #@147
    move/from16 v1, v16

    #@149
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v19

    #@14d
    const-string v20, ", agps : "

    #@14f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v19

    #@153
    move-object/from16 v0, v19

    #@155
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@158
    move-result-object v19

    #@159
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v19

    #@15d
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 1820
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@164
    move-object/from16 v18, v0

    #@166
    move-object/from16 v0, v18

    #@168
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@16a
    move-object/from16 v18, v0

    #@16c
    const-string v19, "connectivity"

    #@16e
    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@171
    move-result-object v6

    #@172
    check-cast v6, Landroid/net/ConnectivityManager;

    #@174
    .line 1822
    .local v6, connectivityManager:Landroid/net/ConnectivityManager;
    if-eqz v6, :cond_179

    #@176
    .line 1823
    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    #@179
    .line 1827
    :cond_179
    move-object/from16 v0, p0

    #@17b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@17d
    move-object/from16 v18, v0

    #@17f
    move-object/from16 v0, v18

    #@181
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@183
    move-object/from16 v18, v0

    #@185
    const-string v19, "wifi"

    #@187
    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@18a
    move-result-object v15

    #@18b
    check-cast v15, Landroid/net/wifi/WifiManager;

    #@18d
    .line 1829
    .local v15, mWifiManager:Landroid/net/wifi/WifiManager;
    if-eqz v6, :cond_194

    #@18f
    .line 1830
    move/from16 v0, v17

    #@191
    invoke-virtual {v15, v0}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@194
    .line 1834
    :cond_194
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@198
    move-object/from16 v18, v0

    #@19a
    move-object/from16 v0, v18

    #@19c
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@19e
    move-object/from16 v18, v0

    #@1a0
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a3
    move-result-object v18

    #@1a4
    const-string v19, "gps"

    #@1a6
    move-object/from16 v0, v18

    #@1a8
    move-object/from16 v1, v19

    #@1aa
    invoke-static {v0, v1, v9}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@1ad
    .line 1840
    move-object/from16 v0, p0

    #@1af
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1b1
    move-object/from16 v18, v0

    #@1b3
    move-object/from16 v0, v18

    #@1b5
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1b7
    move-object/from16 v18, v0

    #@1b9
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1bc
    move-result-object v18

    #@1bd
    const-string v19, "network"

    #@1bf
    move-object/from16 v0, v18

    #@1c1
    move-object/from16 v1, v19

    #@1c3
    move/from16 v2, v16

    #@1c5
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    #@1c8
    .line 1842
    move-object/from16 v0, p0

    #@1ca
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1cc
    move-object/from16 v18, v0

    #@1ce
    move-object/from16 v0, v18

    #@1d0
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1d2
    move-object/from16 v18, v0

    #@1d4
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d7
    move-result-object v19

    #@1d8
    const-string v20, "assisted_gps_enabled"

    #@1da
    if-eqz v4, :cond_2d6

    #@1dc
    const/16 v18, 0x1

    #@1de
    :goto_1de
    move-object/from16 v0, v19

    #@1e0
    move-object/from16 v1, v20

    #@1e2
    move/from16 v2, v18

    #@1e4
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1e7
    goto/16 :goto_2a

    #@1e9
    .line 1779
    .end local v6           #connectivityManager:Landroid/net/ConnectivityManager;
    .end local v15           #mWifiManager:Landroid/net/wifi/WifiManager;
    .restart local v7       #dataNetworkEnable:Z
    .local v9, gpsProviderValue:Z
    .local v16, networkProviderEnable:Z
    .local v17, wifiNetworkEnable:Z
    :cond_1e9
    const-string v18, "end"

    #@1eb
    move-object/from16 v0, v18

    #@1ed
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f0
    move-result v18

    #@1f1
    if-eqz v18, :cond_112

    #@1f3
    .line 1784
    const/4 v10, 0x0

    #@1f4
    .line 1786
    .local v10, inReader:Ljava/io/BufferedReader;
    :try_start_1f4
    new-instance v11, Ljava/io/BufferedReader;

    #@1f6
    new-instance v18, Ljava/io/FileReader;

    #@1f8
    const-string v19, "/data/data/go.police.report/files/report.txt"

    #@1fa
    invoke-direct/range {v18 .. v19}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@1fd
    move-object/from16 v0, v18

    #@1ff
    invoke-direct {v11, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_202
    .catchall {:try_start_1f4 .. :try_end_202} :catchall_2cf
    .catch Ljava/lang/Exception; {:try_start_1f4 .. :try_end_202} :catch_2aa

    #@202
    .line 1787
    .end local v10           #inReader:Ljava/io/BufferedReader;
    .local v11, inReader:Ljava/io/BufferedReader;
    const/4 v14, 0x0

    #@203
    .line 1788
    .local v14, line:Ljava/lang/String;
    :cond_203
    :goto_203
    :try_start_203
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@206
    move-result-object v14

    #@207
    if-eqz v14, :cond_2a0

    #@209
    .line 1789
    const-string v18, "WindowManager"

    #@20b
    new-instance v19, Ljava/lang/StringBuilder;

    #@20d
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@210
    const-string v20, "112APP_KEY : Apps requests : "

    #@212
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@215
    move-result-object v19

    #@216
    move-object/from16 v0, v19

    #@218
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v19

    #@21c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21f
    move-result-object v19

    #@220
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@223
    .line 1791
    const-string v18, "dataNetwork"

    #@225
    move-object/from16 v0, v18

    #@227
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@22a
    move-result v18

    #@22b
    if-eqz v18, :cond_23b

    #@22d
    .line 1792
    const-string v18, "OFF"

    #@22f
    move-object/from16 v0, v18

    #@231
    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@234
    move-result v18

    #@235
    if-eqz v18, :cond_239

    #@237
    const/4 v7, 0x0

    #@238
    :goto_238
    goto :goto_203

    #@239
    :cond_239
    const/4 v7, 0x1

    #@23a
    goto :goto_238

    #@23b
    .line 1793
    :cond_23b
    const-string v18, "wifiNetwork"

    #@23d
    move-object/from16 v0, v18

    #@23f
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@242
    move-result v18

    #@243
    if-eqz v18, :cond_255

    #@245
    .line 1794
    const-string v18, "OFF"

    #@247
    move-object/from16 v0, v18

    #@249
    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@24c
    move-result v18

    #@24d
    if-eqz v18, :cond_252

    #@24f
    const/16 v17, 0x0

    #@251
    :goto_251
    goto :goto_203

    #@252
    :cond_252
    const/16 v17, 0x1

    #@254
    goto :goto_251

    #@255
    .line 1795
    :cond_255
    const-string v18, "gpsProvider"

    #@257
    move-object/from16 v0, v18

    #@259
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@25c
    move-result v18

    #@25d
    if-eqz v18, :cond_26d

    #@25f
    .line 1796
    const-string v18, "OFF"

    #@261
    move-object/from16 v0, v18

    #@263
    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@266
    move-result v18

    #@267
    if-eqz v18, :cond_26b

    #@269
    const/4 v9, 0x0

    #@26a
    :goto_26a
    goto :goto_203

    #@26b
    :cond_26b
    const/4 v9, 0x1

    #@26c
    goto :goto_26a

    #@26d
    .line 1797
    :cond_26d
    const-string v18, "networkProvider"

    #@26f
    move-object/from16 v0, v18

    #@271
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@274
    move-result v18

    #@275
    if-eqz v18, :cond_287

    #@277
    .line 1798
    const-string v18, "OFF"

    #@279
    move-object/from16 v0, v18

    #@27b
    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@27e
    move-result v18

    #@27f
    if-eqz v18, :cond_284

    #@281
    const/16 v16, 0x0

    #@283
    :goto_283
    goto :goto_203

    #@284
    :cond_284
    const/16 v16, 0x1

    #@286
    goto :goto_283

    #@287
    .line 1799
    :cond_287
    const-string v18, "agpsProvider"

    #@289
    move-object/from16 v0, v18

    #@28b
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@28e
    move-result v18

    #@28f
    if-eqz v18, :cond_203

    #@291
    .line 1800
    const-string v18, "OFF"

    #@293
    move-object/from16 v0, v18

    #@295
    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_298
    .catchall {:try_start_203 .. :try_end_298} :catchall_2dc
    .catch Ljava/lang/Exception; {:try_start_203 .. :try_end_298} :catch_2df

    #@298
    move-result v18

    #@299
    if-eqz v18, :cond_29e

    #@29b
    const/4 v4, 0x0

    #@29c
    :goto_29c
    goto/16 :goto_203

    #@29e
    :cond_29e
    const/4 v4, 0x1

    #@29f
    goto :goto_29c

    #@2a0
    .line 1806
    :cond_2a0
    if-eqz v11, :cond_112

    #@2a2
    .line 1808
    :try_start_2a2
    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V
    :try_end_2a5
    .catch Ljava/io/IOException; {:try_start_2a2 .. :try_end_2a5} :catch_2a7

    #@2a5
    goto/16 :goto_112

    #@2a7
    .line 1809
    :catch_2a7
    move-exception v18

    #@2a8
    goto/16 :goto_112

    #@2aa
    .line 1803
    .end local v11           #inReader:Ljava/io/BufferedReader;
    .end local v14           #line:Ljava/lang/String;
    .restart local v10       #inReader:Ljava/io/BufferedReader;
    :catch_2aa
    move-exception v8

    #@2ab
    .line 1804
    .local v8, e:Ljava/lang/Exception;
    :goto_2ab
    :try_start_2ab
    const-string v18, "WindowManager"

    #@2ad
    new-instance v19, Ljava/lang/StringBuilder;

    #@2af
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2b2
    const-string v20, "112APP_KEY : Exception in BufferedReader : "

    #@2b4
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b7
    move-result-object v19

    #@2b8
    move-object/from16 v0, v19

    #@2ba
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2bd
    move-result-object v19

    #@2be
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c1
    move-result-object v19

    #@2c2
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c5
    .catchall {:try_start_2ab .. :try_end_2c5} :catchall_2cf

    #@2c5
    .line 1806
    if-eqz v10, :cond_112

    #@2c7
    .line 1808
    :try_start_2c7
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V
    :try_end_2ca
    .catch Ljava/io/IOException; {:try_start_2c7 .. :try_end_2ca} :catch_2cc

    #@2ca
    goto/16 :goto_112

    #@2cc
    .line 1809
    :catch_2cc
    move-exception v18

    #@2cd
    goto/16 :goto_112

    #@2cf
    .line 1806
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_2cf
    move-exception v18

    #@2d0
    :goto_2d0
    if-eqz v10, :cond_2d5

    #@2d2
    .line 1808
    :try_start_2d2
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V
    :try_end_2d5
    .catch Ljava/io/IOException; {:try_start_2d2 .. :try_end_2d5} :catch_2da

    #@2d5
    .line 1810
    :cond_2d5
    :goto_2d5
    throw v18

    #@2d6
    .line 1842
    .end local v7           #dataNetworkEnable:Z
    .end local v9           #gpsProviderValue:Z
    .end local v10           #inReader:Ljava/io/BufferedReader;
    .end local v16           #networkProviderEnable:Z
    .end local v17           #wifiNetworkEnable:Z
    .restart local v6       #connectivityManager:Landroid/net/ConnectivityManager;
    .restart local v15       #mWifiManager:Landroid/net/wifi/WifiManager;
    :cond_2d6
    const/16 v18, 0x0

    #@2d8
    goto/16 :goto_1de

    #@2da
    .line 1809
    .end local v6           #connectivityManager:Landroid/net/ConnectivityManager;
    .end local v15           #mWifiManager:Landroid/net/wifi/WifiManager;
    .restart local v7       #dataNetworkEnable:Z
    .restart local v9       #gpsProviderValue:Z
    .restart local v10       #inReader:Ljava/io/BufferedReader;
    .restart local v16       #networkProviderEnable:Z
    .restart local v17       #wifiNetworkEnable:Z
    :catch_2da
    move-exception v19

    #@2db
    goto :goto_2d5

    #@2dc
    .line 1806
    .end local v10           #inReader:Ljava/io/BufferedReader;
    .restart local v11       #inReader:Ljava/io/BufferedReader;
    .restart local v14       #line:Ljava/lang/String;
    :catchall_2dc
    move-exception v18

    #@2dd
    move-object v10, v11

    #@2de
    .end local v11           #inReader:Ljava/io/BufferedReader;
    .restart local v10       #inReader:Ljava/io/BufferedReader;
    goto :goto_2d0

    #@2df
    .line 1803
    .end local v10           #inReader:Ljava/io/BufferedReader;
    .restart local v11       #inReader:Ljava/io/BufferedReader;
    :catch_2df
    move-exception v8

    #@2e0
    move-object v10, v11

    #@2e1
    .end local v11           #inReader:Ljava/io/BufferedReader;
    .restart local v10       #inReader:Ljava/io/BufferedReader;
    goto :goto_2ab
.end method
