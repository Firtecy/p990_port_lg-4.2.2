.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;
.super Ljava/lang/Object;
.source "LockScreen.java"

# interfaces
.implements Lcom/android/internal/widget/WaveView$OnTriggerListener;
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WaveViewMethods"
.end annotation


# instance fields
.field private final mWaveView:Lcom/android/internal/widget/WaveView;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/WaveView;)V
    .registers 3
    .parameter
    .parameter "waveView"

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 211
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->mWaveView:Lcom/android/internal/widget/WaveView;

    #@7
    .line 212
    return-void
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->mWaveView:Lcom/android/internal/widget/WaveView;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/WaveView;->setOnTriggerListener(Lcom/android/internal/widget/WaveView$OnTriggerListener;)V

    #@6
    .line 249
    return-void
.end method

.method public getTargetPosition(I)I
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 245
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->mWaveView:Lcom/android/internal/widget/WaveView;

    #@2
    return-object v0
.end method

.method public onGrabbedStateChange(Landroid/view/View;I)V
    .registers 5
    .parameter "v"
    .parameter "grabbedState"

    #@0
    .prologue
    .line 225
    const/16 v0, 0xa

    #@2
    if-ne p2, v0, :cond_f

    #@4
    .line 226
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@6
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@9
    move-result-object v0

    #@a
    const/16 v1, 0x7530

    #@c
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@f
    .line 228
    :cond_f
    return-void
.end method

.method public onTrigger(Landroid/view/View;I)V
    .registers 4
    .parameter "v"
    .parameter "whichHandle"

    #@0
    .prologue
    .line 215
    const/16 v0, 0xa

    #@2
    if-ne p2, v0, :cond_9

    #@4
    .line 216
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@6
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@9
    .line 218
    :cond_9
    return-void
.end method

.method public ping()V
    .registers 1

    #@0
    .prologue
    .line 240
    return-void
.end method

.method public reset(Z)V
    .registers 3
    .parameter "animate"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;->mWaveView:Lcom/android/internal/widget/WaveView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/WaveView;->reset()V

    #@5
    .line 238
    return-void
.end method

.method public setEnabled(IZ)V
    .registers 3
    .parameter "resourceId"
    .parameter "enabled"

    #@0
    .prologue
    .line 243
    return-void
.end method

.method public updateResources()V
    .registers 1

    #@0
    .prologue
    .line 231
    return-void
.end method
