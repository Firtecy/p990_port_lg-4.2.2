.class Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;
.super Landroid/os/CountDownTimer;
.source "PatternUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->handleAttemptLockout(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;JJ)V
    .registers 6
    .parameter
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 388
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    #@5
    return-void
.end method


# virtual methods
.method public onFinish()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 401
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@3
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    #@a
    .line 402
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@12
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->getContext()Landroid/content/Context;

    #@15
    move-result-object v1

    #@16
    const v2, 0x104030f

    #@19
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@20
    .line 404
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@22
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->updateStatusLines(Z)V

    #@29
    .line 406
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2b
    const/4 v1, 0x0

    #@2c
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$702(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;I)I

    #@2f
    .line 407
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@31
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_3f

    #@37
    .line 408
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@39
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->ForgotLockPattern:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@3b
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@3e
    .line 412
    :goto_3e
    return-void

    #@3f
    .line 410
    :cond_3f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@41
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;->Normal:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;

    #@43
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$FooterMode;)V

    #@46
    goto :goto_3e
.end method

.method public onTick(J)V
    .registers 11
    .parameter "millisUntilFinished"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 392
    const-wide/16 v1, 0x3e8

    #@3
    div-long v1, p1, v1

    #@5
    long-to-int v0, v1

    #@6
    .line 393
    .local v0, secondsRemaining:I
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@8
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@b
    move-result-object v1

    #@c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->getContext()Landroid/content/Context;

    #@11
    move-result-object v2

    #@12
    const v3, 0x1040334

    #@15
    new-array v4, v7, [Ljava/lang/Object;

    #@17
    const/4 v5, 0x0

    #@18
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v6

    #@1c
    aput-object v6, v4, v5

    #@1e
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@25
    .line 396
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@27
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->updateStatusLines(Z)V

    #@2e
    .line 397
    return-void
.end method
