.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;
.super Ljava/lang/Object;
.source "LockPatternKeyguardView.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountAnalyzer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccountIndex:I

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mAccounts:[Landroid/accounts/Account;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;


# direct methods
.method private constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Landroid/accounts/AccountManager;)V
    .registers 4
    .parameter
    .parameter "accountManager"

    #@0
    .prologue
    .line 467
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 468
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountManager:Landroid/accounts/AccountManager;

    #@7
    .line 469
    const-string v0, "com.google"

    #@9
    invoke-virtual {p2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@f
    .line 470
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Landroid/accounts/AccountManager;Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 462
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Landroid/accounts/AccountManager;)V

    #@3
    return-void
.end method

.method private next()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 475
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@3
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_10

    #@9
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@d
    array-length v1, v1

    #@e
    if-lt v0, v1, :cond_35

    #@10
    .line 476
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@12
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    if-nez v0, :cond_19

    #@18
    .line 486
    :cond_18
    :goto_18
    return-void

    #@19
    .line 478
    :cond_19
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@1b
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Landroid/view/View;

    #@1e
    move-result-object v0

    #@1f
    instance-of v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@21
    if-eqz v0, :cond_18

    #@23
    .line 479
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@25
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Landroid/view/View;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@2d
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z

    #@30
    move-result v1

    #@31
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->setEnableFallback(Z)V

    #@34
    goto :goto_18

    #@35
    .line 485
    :cond_35
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountManager:Landroid/accounts/AccountManager;

    #@37
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@39
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@3b
    aget-object v1, v1, v3

    #@3d
    move-object v3, v2

    #@3e
    move-object v4, p0

    #@3f
    move-object v5, v2

    #@40
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    #@43
    goto :goto_18
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 496
    .local p1, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/os/Bundle;

    #@6
    .line 497
    .local v0, result:Landroid/os/Bundle;
    const-string v1, "intent"

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v1

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 498
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$902(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    :try_end_14
    .catchall {:try_start_0 .. :try_end_14} :catchall_3f
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_14} :catch_1e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_14} :catch_29
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_14} :catch_34

    #@14
    .line 507
    :cond_14
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@16
    add-int/lit8 v1, v1, 0x1

    #@18
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@1a
    .line 508
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@1d
    .line 510
    .end local v0           #result:Landroid/os/Bundle;
    :goto_1d
    return-void

    #@1e
    .line 500
    :catch_1e
    move-exception v1

    #@1f
    .line 507
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@21
    add-int/lit8 v1, v1, 0x1

    #@23
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@25
    .line 508
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@28
    goto :goto_1d

    #@29
    .line 502
    :catch_29
    move-exception v1

    #@2a
    .line 507
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@30
    .line 508
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@33
    goto :goto_1d

    #@34
    .line 504
    :catch_34
    move-exception v1

    #@35
    .line 507
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@37
    add-int/lit8 v1, v1, 0x1

    #@39
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@3b
    .line 508
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@3e
    goto :goto_1d

    #@3f
    .line 507
    :catchall_3f
    move-exception v1

    #@40
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@42
    add-int/lit8 v2, v2, 0x1

    #@44
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@46
    .line 508
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@49
    throw v1
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 489
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->access$902(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z

    #@6
    .line 490
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->mAccountIndex:I

    #@8
    .line 491
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->next()V

    #@b
    .line 492
    return-void
.end method
