.class Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;
.super Ljava/lang/Object;
.source "MSimKeyguardSimPinView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->onSimCheckResponse(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 115
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@3
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 116
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@f
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    #@12
    .line 118
    :cond_12
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->val$success:Z

    #@14
    if-eqz v0, :cond_45

    #@16
    .line 121
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@18
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v0

    #@1e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@21
    move-result-object v0

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@24
    iget v1, v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$MSimCheckSimPin;->mSubscription:I

    #@26
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->reportSimUnlocked(I)V

    #@29
    .line 122
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@2b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@2d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2f
    invoke-interface {v0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@32
    .line 128
    :goto_32
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@34
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@36
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@38
    const-wide/16 v1, 0x0

    #@3a
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@3d
    .line 129
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@3f
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@41
    const/4 v1, 0x0

    #@42
    iput-boolean v1, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->mSimCheckInProgress:Z

    #@44
    .line 130
    return-void

    #@45
    .line 124
    :cond_45
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@47
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@49
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@4b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@4d
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@4f
    const v2, 0x1040558

    #@52
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@55
    move-result-object v1

    #@56
    invoke-interface {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@59
    .line 126
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;

    #@5b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPinView;

    #@5d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@5f
    const-string v1, ""

    #@61
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@64
    goto :goto_32
.end method
