.class Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;
.super Ljava/lang/Object;
.source "AccountUnlockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->postOnCheckPasswordResult(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 175
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 177
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->val$success:Z

    #@3
    if-eqz v1, :cond_4f

    #@5
    .line 179
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@7
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@e
    .line 180
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@10
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLockPatternEnabled(Z)V

    #@17
    .line 181
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@19
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;

    #@1c
    move-result-object v1

    #@1d
    const/4 v2, 0x0

    #@1e
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;)V

    #@21
    .line 185
    new-instance v0, Landroid/content/Intent;

    #@23
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@26
    .line 186
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.settings"

    #@28
    const-string v2, "com.android.settings.ChooseLockGeneric"

    #@2a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 187
    const/high16 v1, 0x1000

    #@2f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@32
    .line 188
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@34
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/content/Context;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@3b
    .line 189
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@3d
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@40
    move-result-object v1

    #@41
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    #@44
    .line 192
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@46
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@49
    move-result-object v1

    #@4a
    const/4 v2, 0x1

    #@4b
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->keyguardDone(Z)V

    #@4e
    .line 198
    .end local v0           #intent:Landroid/content/Intent;
    :goto_4e
    return-void

    #@4f
    .line 194
    :cond_4f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@51
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/TextView;

    #@54
    move-result-object v1

    #@55
    const v2, 0x104033c

    #@58
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    #@5b
    .line 195
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@5d
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;

    #@60
    move-result-object v1

    #@61
    const-string v2, ""

    #@63
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@66
    .line 196
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@68
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@6b
    move-result-object v1

    #@6c
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportFailedUnlockAttempt()V

    #@6f
    goto :goto_4e
.end method
