.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;
.super Landroid/telephony/PhoneStateListener;
.source "KeyguardSimPukView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 325
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$402(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    #@5
    .line 331
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "AU"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1d

    #@11
    .line 332
    const-string v0, "KeyguardSimPukView"

    #@13
    const-string v1, "[PIN/PUK] onServiceStateChanged"

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 333
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->updateEmergencyText()V

    #@1d
    .line 335
    :cond_1d
    return-void
.end method
