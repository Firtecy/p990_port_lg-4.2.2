.class public abstract Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;
.super Ljava/lang/Object;
.source "KeyguardActivityLauncher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DEBUG:Z = false

.field private static final INSECURE_CAMERA_INTENT:Landroid/content/Intent; = null

.field private static final META_DATA_KEYGUARD_LAYOUT:Ljava/lang/String; = "com.android.keyguard.layout"

.field private static final SECURE_CAMERA_INTENT:Landroid/content/Intent;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 45
    const-class v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@8
    .line 46
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@a
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@c
    .line 48
    new-instance v0, Landroid/content/Intent;

    #@e
    const-string v1, "android.media.action.STILL_IMAGE_CAMERA_SECURE"

    #@10
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    const/high16 v1, 0x80

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@18
    move-result-object v0

    #@19
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@1b
    .line 51
    new-instance v0, Landroid/content/Intent;

    #@1d
    const-string v1, "android.media.action.STILL_IMAGE_CAMERA"

    #@1f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@22
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->INSECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@24
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->dismissKeyguardOnNextActivity()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->startActivityForCurrentUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@3
    return-void
.end method

.method static synthetic access$200()Z
    .registers 1

    #@0
    .prologue
    .line 44
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 44
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private dismissKeyguardOnNextActivity()V
    .registers 4

    #@0
    .prologue
    .line 203
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/app/IActivityManager;->dismissKeyguardOnNextActivity()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 207
    :goto_7
    return-void

    #@8
    .line 204
    :catch_8
    move-exception v0

    #@9
    .line 205
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@b
    const-string v2, "can\'t dismiss keyguard on launch"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_7
.end method

.method private getCameraIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->INSECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@f
    goto :goto_c
.end method

.method private startActivityForCurrentUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 11
    .parameter "intent"
    .parameter "options"
    .parameter "worker"
    .parameter "onStarted"

    #@0
    .prologue
    .line 211
    new-instance v4, Landroid/os/UserHandle;

    #@2
    const/4 v0, -0x2

    #@3
    invoke-direct {v4, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@6
    .line 212
    .local v4, user:Landroid/os/UserHandle;
    if-eqz p3, :cond_a

    #@8
    if-nez p4, :cond_12

    #@a
    .line 213
    :cond_a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getContext()Landroid/content/Context;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1, p2, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@11
    .line 246
    :goto_11
    return-void

    #@12
    .line 217
    :cond_12
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;

    #@14
    move-object v1, p0

    #@15
    move-object v2, p1

    #@16
    move-object v3, p2

    #@17
    move-object v5, p4

    #@18
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;Ljava/lang/Runnable;)V

    #@1b
    invoke-virtual {p3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1e
    goto :goto_11
.end method

.method private wouldLaunchResolverActivity(Landroid/content/Intent;)Z
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    const/high16 v4, 0x1

    #@2
    .line 253
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getContext()Landroid/content/Context;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9
    move-result-object v1

    #@a
    .line 254
    .local v1, packageManager:Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v1, p1, v4, v3}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    #@15
    move-result-object v2

    #@16
    .line 256
    .local v2, resolved:Landroid/content/pm/ResolveInfo;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@1d
    move-result v3

    #@1e
    invoke-virtual {v1, p1, v4, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@21
    move-result-object v0

    #@22
    .line 258
    .local v0, appList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->wouldLaunchResolverActivity(Landroid/content/pm/ResolveInfo;Ljava/util/List;)Z

    #@25
    move-result v3

    #@26
    return v3
.end method

.method private wouldLaunchResolverActivity(Landroid/content/pm/ResolveInfo;Ljava/util/List;)Z
    .registers 7
    .parameter "resolved"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 264
    .local p2, appList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@4
    move-result v2

    #@5
    if-ge v0, v2, :cond_2e

    #@7
    .line 265
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Landroid/content/pm/ResolveInfo;

    #@d
    .line 266
    .local v1, tmp:Landroid/content/pm/ResolveInfo;
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@f
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@11
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@13
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_2b

    #@1b
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1d
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    #@1f
    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@21
    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_2b

    #@29
    .line 268
    const/4 v2, 0x0

    #@2a
    .line 271
    .end local v1           #tmp:Landroid/content/pm/ResolveInfo;
    :goto_2a
    return v2

    #@2b
    .line 264
    .restart local v1       #tmp:Landroid/content/pm/ResolveInfo;
    :cond_2b
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_1

    #@2e
    .line 271
    .end local v1           #tmp:Landroid/content/pm/ResolveInfo;
    :cond_2e
    const/4 v2, 0x1

    #@2f
    goto :goto_2a
.end method


# virtual methods
.method abstract getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
.end method

.method public getCameraWidgetInfo()Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 66
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;

    #@3
    invoke-direct {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;-><init>()V

    #@6
    .line 67
    .local v1, info:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getCameraIntent()Landroid/content/Intent;

    #@9
    move-result-object v2

    #@a
    .line 68
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getContext()Landroid/content/Context;

    #@d
    move-result-object v7

    #@e
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@11
    move-result-object v4

    #@12
    .line 69
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    const/high16 v7, 0x1

    #@14
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@17
    move-result-object v8

    #@18
    invoke-virtual {v8}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@1b
    move-result v8

    #@1c
    invoke-virtual {v4, v2, v7, v8}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@1f
    move-result-object v0

    #@20
    .line 71
    .local v0, appList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@23
    move-result v7

    #@24
    if-nez v7, :cond_33

    #@26
    .line 72
    sget-boolean v7, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@28
    if-eqz v7, :cond_31

    #@2a
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@2c
    const-string v8, "getCameraWidgetInfo(): Nothing found"

    #@2e
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    :cond_31
    move-object v1, v6

    #@32
    .line 97
    .end local v1           #info:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;
    :cond_32
    :goto_32
    return-object v1

    #@33
    .line 75
    .restart local v1       #info:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;
    :cond_33
    const v7, 0x10080

    #@36
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v8}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@3d
    move-result v8

    #@3e
    invoke-virtual {v4, v2, v7, v8}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    #@41
    move-result-object v5

    #@42
    .line 78
    .local v5, resolved:Landroid/content/pm/ResolveInfo;
    sget-boolean v7, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@44
    if-eqz v7, :cond_5e

    #@46
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@48
    new-instance v8, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v9, "getCameraWidgetInfo(): resolved: "

    #@4f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 79
    :cond_5e
    invoke-direct {p0, v5, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->wouldLaunchResolverActivity(Landroid/content/pm/ResolveInfo;Ljava/util/List;)Z

    #@61
    move-result v7

    #@62
    if-eqz v7, :cond_70

    #@64
    .line 80
    sget-boolean v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@66
    if-eqz v6, :cond_32

    #@68
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@6a
    const-string v7, "getCameraWidgetInfo(): Would launch resolver"

    #@6c
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_32

    #@70
    .line 83
    :cond_70
    if-eqz v5, :cond_76

    #@72
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@74
    if-nez v7, :cond_78

    #@76
    :cond_76
    move-object v1, v6

    #@77
    .line 84
    goto :goto_32

    #@78
    .line 86
    :cond_78
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@7a
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@7c
    if-eqz v6, :cond_88

    #@7e
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@80
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@82
    invoke-virtual {v6}, Landroid/os/Bundle;->isEmpty()Z

    #@85
    move-result v6

    #@86
    if-eqz v6, :cond_94

    #@88
    .line 87
    :cond_88
    sget-boolean v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@8a
    if-eqz v6, :cond_32

    #@8c
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@8e
    const-string v7, "getCameraWidgetInfo(): no metadata found"

    #@90
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    goto :goto_32

    #@94
    .line 90
    :cond_94
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@96
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@98
    const-string v7, "com.android.keyguard.layout"

    #@9a
    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@9d
    move-result v3

    #@9e
    .line 91
    .local v3, layoutId:I
    if-nez v3, :cond_ac

    #@a0
    .line 92
    sget-boolean v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@a2
    if-eqz v6, :cond_32

    #@a4
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@a6
    const-string v7, "getCameraWidgetInfo(): no layout specified"

    #@a8
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_32

    #@ac
    .line 95
    :cond_ac
    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@ae
    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    #@b0
    iput-object v6, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;->contextPackage:Ljava/lang/String;

    #@b2
    .line 96
    iput v3, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$CameraWidgetInfo;->layoutId:I

    #@b4
    goto/16 :goto_32
.end method

.method abstract getContext()Landroid/content/Context;
.end method

.method abstract getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;
.end method

.method public launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 13
    .parameter "intent"
    .parameter "showsWhileLocked"
    .parameter "useDefaultAnimations"
    .parameter "worker"
    .parameter "onStarted"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 156
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getContext()Landroid/content/Context;

    #@4
    move-result-object v6

    #@5
    .line 157
    .local v6, context:Landroid/content/Context;
    if-eqz p3, :cond_11

    #@7
    const/4 v3, 0x0

    #@8
    .local v3, animation:Landroid/os/Bundle;
    :goto_8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move v2, p2

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    .line 159
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivityWithAnimation(Landroid/content/Intent;ZLandroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@10
    .line 160
    return-void

    #@11
    .line 157
    .end local v3           #animation:Landroid/os/Bundle;
    :cond_11
    invoke-static {v6, v0, v0}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    #@18
    move-result-object v3

    #@19
    goto :goto_8
.end method

.method public launchActivityWithAnimation(Landroid/content/Intent;ZLandroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 16
    .parameter "intent"
    .parameter "showsWhileLocked"
    .parameter "animation"
    .parameter "worker"
    .parameter "onStarted"

    #@0
    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@3
    move-result-object v9

    #@4
    .line 169
    .local v9, lockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;
    const/high16 v0, 0x3400

    #@6
    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@9
    .line 173
    invoke-virtual {v9}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@c
    move-result v8

    #@d
    .line 174
    .local v8, isSecure:Z
    if-eqz v8, :cond_11

    #@f
    if-eqz p2, :cond_58

    #@11
    .line 175
    :cond_11
    if-nez v8, :cond_16

    #@13
    .line 176
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->dismissKeyguardOnNextActivity()V

    #@16
    .line 179
    :cond_16
    :try_start_16
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->DEBUG:Z

    #@18
    if-eqz v0, :cond_36

    #@1a
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@1c
    const-string v1, "Starting activity for intent %s at %s"

    #@1e
    const/4 v2, 0x2

    #@1f
    new-array v2, v2, [Ljava/lang/Object;

    #@21
    const/4 v3, 0x0

    #@22
    aput-object p1, v2, v3

    #@24
    const/4 v3, 0x1

    #@25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@28
    move-result-wide v4

    #@29
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2c
    move-result-object v4

    #@2d
    aput-object v4, v2, v3

    #@2f
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 181
    :cond_36
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->startActivityForCurrentUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_39
    .catch Landroid/content/ActivityNotFoundException; {:try_start_16 .. :try_end_39} :catch_3a

    #@39
    .line 199
    :goto_39
    return-void

    #@3a
    .line 182
    :catch_3a
    move-exception v7

    #@3b
    .line 183
    .local v7, e:Landroid/content/ActivityNotFoundException;
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->TAG:Ljava/lang/String;

    #@3d
    new-instance v1, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v2, "Activity not found for intent + "

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    goto :goto_39

    #@58
    .line 188
    .end local v7           #e:Landroid/content/ActivityNotFoundException;
    :cond_58
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@5b
    move-result-object v6

    #@5c
    .line 189
    .local v6, callback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;

    #@5e
    move-object v1, p0

    #@5f
    move-object v2, p1

    #@60
    move-object v3, p3

    #@61
    move-object v4, p4

    #@62
    move-object v5, p5

    #@63
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@66
    invoke-interface {v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->setOnDismissAction(Lcom/android/internal/policy/impl/keyguard/OnDismissAction;)V

    #@69
    .line 197
    const/4 v0, 0x0

    #@6a
    invoke-interface {v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@6d
    goto :goto_39
.end method

.method public launchCamera(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 13
    .parameter "worker"
    .parameter "onSecureCameraStarted"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 101
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    #@5
    move-result-object v9

    #@6
    .line 102
    .local v9, lockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;
    invoke-virtual {v9}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_28

    #@c
    .line 104
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@e
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->wouldLaunchResolverActivity(Landroid/content/Intent;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1d

    #@14
    .line 109
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@16
    move-object v0, p0

    #@17
    move v3, v2

    #@18
    move-object v5, v4

    #@19
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V

    #@1c
    .line 117
    :goto_1c
    return-void

    #@1d
    .line 111
    :cond_1d
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@1f
    const/4 v5, 0x1

    #@20
    move-object v3, p0

    #@21
    move v6, v2

    #@22
    move-object v7, p1

    #@23
    move-object v8, p2

    #@24
    invoke-virtual/range {v3 .. v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V

    #@27
    goto :goto_1c

    #@28
    .line 115
    :cond_28
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->INSECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@2a
    move-object v0, p0

    #@2b
    move v3, v2

    #@2c
    move-object v5, v4

    #@2d
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V

    #@30
    goto :goto_1c
.end method

.method public launchWidgetPicker(I)V
    .registers 9
    .parameter "appWidgetId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x0

    #@3
    .line 120
    new-instance v1, Landroid/content/Intent;

    #@5
    const-string v0, "android.appwidget.action.KEYGUARD_APPWIDGET_PICK"

    #@7
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a
    .line 122
    .local v1, pickIntent:Landroid/content/Intent;
    const-string v0, "appWidgetId"

    #@c
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@f
    .line 123
    const-string v0, "customSort"

    #@11
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@14
    .line 124
    const-string v0, "categoryFilter"

    #@16
    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@19
    .line 127
    new-instance v6, Landroid/os/Bundle;

    #@1b
    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    #@1e
    .line 128
    .local v6, options:Landroid/os/Bundle;
    const-string v0, "appWidgetCategory"

    #@20
    invoke-virtual {v6, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@23
    .line 130
    const-string v0, "appWidgetOptions"

    #@25
    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@28
    .line 131
    const/high16 v0, 0x3480

    #@2a
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2d
    move-object v0, p0

    #@2e
    move v3, v2

    #@2f
    move-object v5, v4

    #@30
    .line 137
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V

    #@33
    .line 138
    return-void
.end method
