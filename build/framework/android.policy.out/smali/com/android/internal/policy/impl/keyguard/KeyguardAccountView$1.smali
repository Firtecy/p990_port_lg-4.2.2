.class Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;
.super Ljava/lang/Object;
.source "KeyguardAccountView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->postOnCheckPasswordResult(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 166
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 168
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->val$success:Z

    #@4
    if-eqz v1, :cond_5e

    #@6
    .line 170
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@8
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/widget/LockPatternUtils;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@f
    .line 171
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@11
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/widget/LockPatternUtils;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLockPatternEnabled(Z)V

    #@18
    .line 172
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@1a
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/widget/LockPatternUtils;

    #@1d
    move-result-object v1

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;)V

    #@22
    .line 176
    new-instance v0, Landroid/content/Intent;

    #@24
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@27
    .line 177
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.settings"

    #@29
    const-string v2, "com.android.settings.ChooseLockGeneric"

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2e
    .line 178
    const/high16 v1, 0x1000

    #@30
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@33
    .line 179
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@35
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/content/Context;

    #@38
    move-result-object v1

    #@39
    new-instance v2, Landroid/os/UserHandle;

    #@3b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@3d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/widget/LockPatternUtils;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@44
    move-result v3

    #@45
    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@48
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@4b
    .line 181
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@4d
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@50
    move-result-object v1

    #@51
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->reportSuccessfulUnlockAttempt()V

    #@54
    .line 184
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@56
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@59
    move-result-object v1

    #@5a
    invoke-interface {v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@5d
    .line 190
    .end local v0           #intent:Landroid/content/Intent;
    :goto_5d
    return-void

    #@5e
    .line 186
    :cond_5e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@60
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@63
    move-result-object v1

    #@64
    const v2, 0x1040562

    #@67
    invoke-interface {v1, v2, v4}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(IZ)V

    #@6a
    .line 187
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@6c
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/widget/EditText;

    #@6f
    move-result-object v1

    #@70
    const-string v2, ""

    #@72
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@75
    .line 188
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;

    #@77
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@7a
    move-result-object v1

    #@7b
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->reportFailedUnlockAttempt()V

    #@7e
    goto :goto_5d
.end method
