.class Lcom/android/internal/policy/impl/RestartAction;
.super Ljava/lang/Object;
.source "RestartAction.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DEBUG:Z = false

.field private static final MESSAGE_DISMISS:I = 0x0

.field private static final MESSAGE_REFRESH:I = 0x1

.field private static final MESSAGE_SHOW:I = 0x2

.field private static final TAG:Ljava/lang/String; = "RestartAction"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mCount:I

.field private mDefalutCount:I

.field private mDialog:Landroid/app/ProgressDialog;

.field private mHandler:Landroid/os/Handler;

.field private mKeyguardShowing:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 23
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@2
    const-string v1, "eng"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@c
    const-string v1, "userdebug"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_18

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    sput-boolean v0, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@17
    return-void

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_15
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 27
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mKeyguardShowing:Z

    #@6
    .line 28
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@8
    .line 29
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@a
    .line 175
    new-instance v0, Lcom/android/internal/policy/impl/RestartAction$1;

    #@c
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/RestartAction$1;-><init>(Lcom/android/internal/policy/impl/RestartAction;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@11
    .line 35
    sput-object p1, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@13
    .line 36
    const/4 v0, 0x4

    #@14
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@16
    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "defaultCount"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 27
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mKeyguardShowing:Z

    #@6
    .line 28
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@8
    .line 29
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@a
    .line 175
    new-instance v0, Lcom/android/internal/policy/impl/RestartAction$1;

    #@c
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/RestartAction$1;-><init>(Lcom/android/internal/policy/impl/RestartAction;)V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@11
    .line 44
    sput-object p1, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@13
    .line 45
    iput p2, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@15
    .line 46
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 20
    sget-boolean v0, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/RestartAction;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/RestartAction;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 20
    iput-object p1, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/RestartAction;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/RestartAction;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RestartAction;->refreshRestartDialog()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/RestartAction;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RestartAction;->handleShow()V

    #@3
    return-void
.end method

.method private createDialog()Landroid/app/ProgressDialog;
    .registers 4

    #@0
    .prologue
    .line 82
    sget-boolean v1, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    .line 83
    const-string v1, "RestartAction"

    #@6
    const-string v2, "createDialog()"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 86
    :cond_b
    new-instance v0, Landroid/app/ProgressDialog;

    #@d
    sget-object v1, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@f
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@12
    .line 89
    .local v0, dialog:Landroid/app/ProgressDialog;
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@15
    move-result-object v1

    #@16
    const/16 v2, 0x7d8

    #@18
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@1b
    .line 91
    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@1e
    .line 93
    return-object v0
.end method

.method private getCountMsg(I)Landroid/text/SpannableStringBuilder;
    .registers 15
    .parameter "count"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 137
    sget-boolean v7, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@5
    if-eqz v7, :cond_1f

    #@7
    .line 138
    const-string v7, "RestartAction"

    #@9
    new-instance v8, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v9, "getCountMsg() count = "

    #@10
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v8

    #@14
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v8

    #@18
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v8

    #@1c
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 141
    :cond_1f
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@21
    new-instance v7, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v8, ""

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    invoke-direct {v0, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@37
    .line 142
    .local v0, counter:Landroid/text/SpannableStringBuilder;
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    #@39
    const-string v8, "#4baac8"

    #@3b
    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    #@3e
    move-result v8

    #@3f
    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    #@42
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    #@45
    move-result v8

    #@46
    const/16 v9, 0x21

    #@48
    invoke-virtual {v0, v7, v10, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@4b
    .line 144
    sget-object v7, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@4d
    const v8, 0x2090012

    #@50
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    .line 145
    .local v1, counterFormat:Ljava/lang/String;
    const-string v7, "%d"

    #@56
    invoke-virtual {v1, v7, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    .line 147
    .local v2, counterFormats:[Ljava/lang/String;
    new-instance v6, Landroid/text/SpannableStringBuilder;

    #@5c
    const-string v7, ""

    #@5e
    invoke-direct {v6, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@61
    .line 149
    .local v6, spannedCount:Landroid/text/SpannableStringBuilder;
    const/4 v7, 0x0

    #@62
    :try_start_62
    aget-object v7, v2, v7

    #@64
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@67
    .line 150
    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@6a
    .line 151
    const/4 v7, 0x1

    #@6b
    aget-object v7, v2, v7

    #@6d
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@70
    .line 153
    sget-object v7, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@72
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@75
    move-result-object v7

    #@76
    const v8, 0x2060017

    #@79
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@7c
    move-result v7

    #@7d
    if-eqz v7, :cond_a5

    #@7f
    .line 156
    sget-object v7, Lcom/android/internal/policy/impl/RestartAction;->mContext:Landroid/content/Context;

    #@81
    const v8, 0x2090013

    #@84
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@87
    move-result-object v4

    #@88
    .line 157
    .local v4, embeddedFormat:Ljava/lang/String;
    const-string v7, "%d"

    #@8a
    const/4 v8, 0x2

    #@8b
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    .line 158
    .local v5, embeddedFormats:[Ljava/lang/String;
    const-string v7, "\n\n"

    #@91
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@94
    .line 159
    const/4 v7, 0x0

    #@95
    aget-object v7, v5, v7

    #@97
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@9a
    .line 160
    const-string v7, "2"

    #@9c
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@9f
    .line 161
    const/4 v7, 0x1

    #@a0
    aget-object v7, v5, v7

    #@a2
    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_a5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_62 .. :try_end_a5} :catch_a6

    #@a5
    .line 168
    .end local v4           #embeddedFormat:Ljava/lang/String;
    .end local v5           #embeddedFormats:[Ljava/lang/String;
    :cond_a5
    :goto_a5
    return-object v6

    #@a6
    .line 163
    :catch_a6
    move-exception v3

    #@a7
    .line 164
    .local v3, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v6, Landroid/text/SpannableStringBuilder;

    #@a9
    .end local v6           #spannedCount:Landroid/text/SpannableStringBuilder;
    new-array v7, v11, [Ljava/lang/Object;

    #@ab
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ae
    move-result-object v8

    #@af
    aput-object v8, v7, v10

    #@b1
    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b4
    move-result-object v7

    #@b5
    invoke-direct {v6, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@b8
    .restart local v6       #spannedCount:Landroid/text/SpannableStringBuilder;
    goto :goto_a5
.end method

.method private handleShow()V
    .registers 5

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RestartAction;->createDialog()Landroid/app/ProgressDialog;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@6
    .line 70
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RestartAction;->prepareDialog()V

    #@9
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@b
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    #@e
    .line 73
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@10
    const/4 v1, 0x1

    #@11
    const-wide/16 v2, 0x3e8

    #@13
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@16
    .line 74
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@18
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1f
    move-result-object v0

    #@20
    const/high16 v1, 0x1

    #@22
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@25
    .line 75
    return-void
.end method

.method private prepareDialog()V
    .registers 4

    #@0
    .prologue
    .line 97
    sget-boolean v0, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 98
    const-string v0, "RestartAction"

    #@6
    const-string v1, "prepareDialog()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 102
    :cond_b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mKeyguardShowing:Z

    #@d
    if-eqz v0, :cond_50

    #@f
    .line 103
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@11
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@14
    move-result-object v0

    #@15
    const/16 v1, 0x7d9

    #@17
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@1a
    .line 109
    :goto_1a
    sget-boolean v0, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@1c
    if-eqz v0, :cond_38

    #@1e
    .line 110
    const-string v0, "RestartAction"

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "prepareDialog() mDefalutCount = "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 113
    :cond_38
    iget v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDefalutCount:I

    #@3a
    iput v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@3c
    .line 115
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@3e
    const v1, 0x209000f

    #@41
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    #@44
    .line 116
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@46
    iget v1, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@48
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/RestartAction;->getCountMsg(I)Landroid/text/SpannableStringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@4f
    .line 117
    return-void

    #@50
    .line 105
    :cond_50
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@52
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@55
    move-result-object v0

    #@56
    const/16 v1, 0x7d8

    #@58
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@5b
    goto :goto_1a
.end method

.method private refreshRestartDialog()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 120
    sget-boolean v0, Lcom/android/internal/policy/impl/RestartAction;->DEBUG:Z

    #@3
    if-eqz v0, :cond_1f

    #@5
    .line 121
    const-string v0, "RestartAction"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "refreshRestartDialog() mCount = "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 124
    :cond_1f
    iget v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@21
    if-le v0, v3, :cond_3a

    #@23
    .line 125
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@25
    iget v1, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@27
    add-int/lit8 v1, v1, -0x1

    #@29
    iput v1, p0, Lcom/android/internal/policy/impl/RestartAction;->mCount:I

    #@2b
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/RestartAction;->getCountMsg(I)Landroid/text/SpannableStringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@32
    .line 126
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@34
    const-wide/16 v1, 0x3e8

    #@36
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@39
    .line 130
    :goto_39
    return-void

    #@3a
    .line 128
    :cond_3a
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@3c
    const/4 v1, 0x0

    #@3d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@40
    goto :goto_39
.end method


# virtual methods
.method public dismissDialog()V
    .registers 3

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 66
    return-void
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    .line 206
    const/4 v0, 0x1

    #@d
    .line 208
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter "dialog"

    #@0
    .prologue
    .line 134
    return-void
.end method

.method public showDialog(Z)V
    .registers 4
    .parameter "keyguardShowing"

    #@0
    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/RestartAction;->mKeyguardShowing:Z

    #@2
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@4
    if-eqz v0, :cond_15

    #@6
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@8
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@b
    .line 56
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mDialog:Landroid/app/ProgressDialog;

    #@e
    .line 58
    iget-object v0, p0, Lcom/android/internal/policy/impl/RestartAction;->mHandler:Landroid/os/Handler;

    #@10
    const/4 v1, 0x2

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@14
    .line 62
    :goto_14
    return-void

    #@15
    .line 60
    :cond_15
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RestartAction;->handleShow()V

    #@18
    goto :goto_14
.end method
