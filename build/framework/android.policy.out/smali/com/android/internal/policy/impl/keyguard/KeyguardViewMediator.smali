.class public Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;
.super Ljava/lang/Object;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$8;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;
    }
.end annotation


# static fields
.field public static final ACTION_LOCKOUT_FOR_SWIPE:Ljava/lang/String; = "com.lge.mdm.intent.action.LOCKOUT_FOR_SWIPE"

.field protected static final AWAKE_INTERVAL_DEFAULT_MS:I = 0x2710

.field private static final CLEAR_LOCK_PATTERN:I = 0xf

.field private static final DBG_WAKE:Z = false

.field static final DEBUG:Z = true

.field private static final DELAYED_KEYGUARD_ACTION:Ljava/lang/String; = "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

.field private static final ENABLE_INSECURE_STATUS_BAR_EXPAND:Z = true

.field private static final HIDE:I = 0x3

.field private static final KEYGUARD_DISPLAY_TIMEOUT_DELAY_DEFAULT:I = 0x7530

.field private static final KEYGUARD_DONE:I = 0x9

.field private static final KEYGUARD_DONE_AUTHENTICATING:I = 0xb

.field private static final KEYGUARD_DONE_DRAWING:I = 0xa

.field private static final KEYGUARD_DONE_DRAWING_TIMEOUT_MS:I = 0x7d0

.field private static final KEYGUARD_LOCK_AFTER_DELAY_DEFAULT:I = 0x1388

.field private static final KEYGUARD_TIMEOUT:I = 0xd

.field private static final LGMDM_EXPIRE_PASSWORD_RESET:I = 0x1e

.field private static final NOTIFY_SCREEN_OFF:I = 0x6

.field private static final NOTIFY_SCREEN_ON:I = 0x7

.field private static final OMADM_LOCK_SCREEN_UPDATE_ACTION:Ljava/lang/String; = "android.intent.action.OMADM_DEVICE_LOCK_MSG"

.field private static final RESET:I = 0x4

.field private static final SET_HIDDEN:I = 0xc

.field private static final SET_LOCKTIMER_STATE:I = 0x15

.field private static final SET_OMADM:I = 0x10

.field private static final SHOW:I = 0x2

.field private static final SHOW_ASSISTANT:I = 0xe

.field private static final SKT_LOCK_SHOW:I = 0x11

.field private static final TAG:Ljava/lang/String; = "KeyguardViewMediator"

.field private static final VERIFY_UNLOCK:I = 0x5

.field private static final WAKE_WHEN_READY:I = 0x8

.field private static final carrier:Ljava/lang/String;

.field private static final country:Ljava/lang/String;

.field private static dcmPermLockControl:Z

.field static isBooting:Z


# instance fields
.field private final ACTION_EXPIRED_PASSWORD_NOTIFICATION:Ljava/lang/String;

.field private final ACTION_LGMDM_EXPIRE_PASSWORD_RESET:Ljava/lang/String;

.field BootingStatusBarState:Ljava/lang/String;

.field private final MAX_LOCK_EFFECT:I

.field private isNaviHide:Z

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mBroadCastReceiverMDM:Landroid/content/BroadcastReceiver;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mDelayedShowingSequence:I

.field private mDisableExpandStatusBar:Z

.field private mEnableUserUnlock:Z

.field private mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

.field private mExternallyEnabled:Z

.field private mHandler:Landroid/os/Handler;

.field private mHidden:Z

.field private mIsLockEffectEnabled:Z

.field private mIsPattern:Z

.field private mIsPatternEffectEnabled:Z

.field private mIsShowingKeyguardRequestedButNotHandledYet:Z

.field private mKeyguardDonePending:Z

.field private mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

.field private mLockEffectSoundId:[I

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockSoundId:I

.field private mLockSoundStreamId:I

.field private final mLockSoundVolume:F

.field private mLockSounds:Landroid/media/SoundPool;

.field private mLockoutMode:Z

.field private mMasterStreamType:I

.field private mNeedToReshowWhenReenabled:Z

.field private mPM:Landroid/os/PowerManager;

.field private mPatternLockEffectSoundId:[I

.field private mPatternUnlockEffectSoundId:[I

.field private mPhoneState:Ljava/lang/String;

.field private mQuickCoverWindowShowing:Z

.field private mScreenOn:Z

.field private mSearchManager:Landroid/app/SearchManager;

.field private mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

.field private mSender:Landroid/app/PendingIntent;

.field private mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mShowLockIcon:Z

.field private mShowing:Z

.field private mShowingLockIcon:Z

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mStrPassword:Ljava/lang/String;

.field private mSuppressNextLockSound:Z

.field private mSystemReady:Z

.field private mUnlockEffectSoundId:[I

.field private mUnlockSoundId:I

.field mUpdateCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

.field private mUserManager:Landroid/os/UserManager;

.field private mUserMsg:Ljava/lang/String;

.field private mUserPresentIntent:Landroid/content/Intent;

.field mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

.field private mWaitingUntilKeyguardVisible:Z

.field private mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

.field private newstate:Lcom/android/internal/telephony/IccCardConstants$State;

.field private oldstate:Lcom/android/internal/telephony/IccCardConstants$State;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 314
    sput-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->dcmPermLockControl:Z

    #@3
    .line 315
    const-string v0, "ro.build.target_operator"

    #@5
    const-string v1, "COM"

    #@7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->carrier:Ljava/lang/String;

    #@d
    .line 317
    const-string v0, "ro.build.target_country"

    #@f
    const-string v1, "COM"

    #@11
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->country:Ljava/lang/String;

    #@17
    .line 332
    sput-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isBooting:Z

    #@19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 16
    .parameter "context"
    .parameter "lockPatternUtils"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/16 v11, 0xa

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v10, 0x0

    #@5
    .line 720
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 145
    const-string v8, "com.lge.mdm.intent.action.EXPIRATION_PASSWORD_CHANGE"

    #@a
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->ACTION_LGMDM_EXPIRE_PASSWORD_RESET:Ljava/lang/String;

    #@c
    .line 146
    const-string v8, "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

    #@e
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->ACTION_EXPIRED_PASSWORD_NOTIFICATION:Ljava/lang/String;

    #@10
    .line 147
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSender:Landroid/app/PendingIntent;

    #@12
    .line 180
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@14
    .line 184
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockoutMode:Z

    #@16
    .line 201
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@18
    .line 234
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@1a
    .line 241
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@1c
    .line 245
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@1e
    .line 246
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mQuickCoverWindowShowing:Z

    #@20
    .line 248
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@22
    .line 270
    sget-object v8, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@24
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    #@26
    .line 281
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@28
    .line 283
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@2a
    .line 290
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mEnableUserUnlock:Z

    #@2c
    .line 291
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStrPassword:Ljava/lang/String;

    #@2e
    .line 292
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserMsg:Ljava/lang/String;

    #@30
    .line 298
    iput v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->MAX_LOCK_EFFECT:I

    #@32
    .line 299
    new-array v8, v11, [I

    #@34
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockEffectSoundId:[I

    #@36
    .line 300
    new-array v8, v11, [I

    #@38
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockEffectSoundId:[I

    #@3a
    .line 301
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsLockEffectEnabled:Z

    #@3c
    .line 304
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsShowingKeyguardRequestedButNotHandledYet:Z

    #@3e
    .line 307
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPatternEffectEnabled:Z

    #@40
    .line 308
    new-array v8, v11, [I

    #@42
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternLockEffectSoundId:[I

    #@44
    .line 309
    new-array v8, v11, [I

    #@46
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternUnlockEffectSoundId:[I

    #@48
    .line 310
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPattern:Z

    #@4a
    .line 319
    sget-object v8, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4c
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->oldstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4e
    .line 320
    sget-object v8, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@50
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->newstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@52
    .line 324
    iput-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isNaviHide:Z

    #@54
    .line 394
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;

    #@56
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@59
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@5b
    .line 629
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;

    #@5d
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@60
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@62
    .line 1536
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;

    #@64
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@67
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@69
    .line 1756
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$4;

    #@6b
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@6e
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mBroadCastReceiverMDM:Landroid/content/BroadcastReceiver;

    #@70
    .line 1856
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;

    #@72
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@75
    move-result-object v11

    #@76
    invoke-direct {v8, p0, v11, v12, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@79
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@7b
    .line 721
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@7d
    .line 722
    const-string v8, "power"

    #@7f
    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@82
    move-result-object v8

    #@83
    check-cast v8, Landroid/os/PowerManager;

    #@85
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@87
    .line 723
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@89
    const-string v11, "user"

    #@8b
    invoke-virtual {v8, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8e
    move-result-object v8

    #@8f
    check-cast v8, Landroid/os/UserManager;

    #@91
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserManager:Landroid/os/UserManager;

    #@93
    .line 724
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@95
    const-string v11, "show keyguard"

    #@97
    invoke-virtual {v8, v9, v11}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@9a
    move-result-object v8

    #@9b
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9d
    .line 725
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9f
    invoke-virtual {v8, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@a2
    .line 727
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@a4
    const-string v11, "keyguardWakeAndHandOff"

    #@a6
    invoke-virtual {v8, v9, v11}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@a9
    move-result-object v8

    #@aa
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@ac
    .line 728
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@ae
    invoke-virtual {v8, v10}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@b1
    .line 730
    new-instance v1, Landroid/content/IntentFilter;

    #@b3
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@b6
    .line 731
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v8, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    #@b8
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@bb
    .line 732
    const-string v8, "android.intent.action.SIM_STATE_CHANGED"

    #@bd
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c0
    .line 734
    const-string v8, "android.intent.action.LTE_EMM_REJECT"

    #@c2
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c5
    .line 735
    const-string v8, "com.lge.intent.action.LTE_MISSING_PHONE"

    #@c7
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ca
    .line 739
    const-string v8, "android.intent.action.OMADM_DEVICE_LOCK_MSG"

    #@cc
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@cf
    .line 742
    const-string v8, "com.lge.lollipop.action.UNLOCKSCREEN"

    #@d1
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d4
    .line 745
    const-string v8, "com.lge.mdm.intent.action.LOCKOUT_FOR_SWIPE"

    #@d6
    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d9
    .line 747
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@db
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@dd
    invoke-virtual {v8, v11, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@e0
    .line 749
    const-string v8, "alarm"

    #@e2
    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e5
    move-result-object v8

    #@e6
    check-cast v8, Landroid/app/AlarmManager;

    #@e8
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    #@ea
    .line 751
    invoke-static {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@ed
    move-result-object v8

    #@ee
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@f0
    .line 754
    sget-boolean v8, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@f2
    if-eqz v8, :cond_f7

    #@f4
    .line 755
    invoke-static {p1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->initialize(Landroid/content/Context;)V

    #@f7
    .line 759
    :cond_f7
    if-eqz p2, :cond_200

    #@f9
    .end local p2
    :goto_f9
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@fb
    .line 761
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@fd
    invoke-virtual {v8, v10}, Lcom/android/internal/widget/LockPatternUtils;->setCurrentUser(I)V

    #@100
    .line 763
    const-string v8, "window"

    #@102
    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@105
    move-result-object v7

    #@106
    check-cast v7, Landroid/view/WindowManager;

    #@108
    .line 765
    .local v7, wm:Landroid/view/WindowManager;
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@10a
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@10c
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@10e
    invoke-direct {v8, p1, v7, v11, v12}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;-><init>(Landroid/content/Context;Landroid/view/ViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;Lcom/android/internal/widget/LockPatternUtils;)V

    #@111
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@113
    .line 768
    new-instance v8, Landroid/content/Intent;

    #@115
    const-string v11, "android.intent.action.USER_PRESENT"

    #@117
    invoke-direct {v8, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11a
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@11c
    .line 769
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@11e
    const/high16 v11, 0x2800

    #@120
    invoke-virtual {v8, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@123
    .line 772
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@125
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@128
    move-result-object v0

    #@129
    .line 773
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v8, "show_status_bar_lock"

    #@12b
    invoke-static {v0, v8, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12e
    move-result v8

    #@12f
    if-ne v8, v9, :cond_209

    #@131
    move v8, v9

    #@132
    :goto_132
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowLockIcon:Z

    #@134
    .line 775
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@136
    invoke-virtual {v8}, Landroid/os/PowerManager;->isScreenOn()Z

    #@139
    move-result v8

    #@13a
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@13c
    .line 778
    sget-boolean v8, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@13e
    if-eqz v8, :cond_215

    #@140
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@143
    move-result-object v8

    #@144
    if-eqz v8, :cond_215

    #@146
    .line 780
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@149
    move-result-object v8

    #@14a
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->shouldShowSwipeBeforeSecure()Z

    #@14d
    move-result v8

    #@14e
    if-nez v8, :cond_20c

    #@150
    .line 781
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@152
    sget v11, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@154
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setLockTimerState(I)V

    #@157
    .line 790
    :goto_157
    new-instance v8, Landroid/media/SoundPool;

    #@159
    invoke-direct {v8, v9, v9, v10}, Landroid/media/SoundPool;-><init>(III)V

    #@15c
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@15e
    .line 792
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@160
    invoke-direct {v8, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;-><init>(Landroid/content/Context;)V

    #@163
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@165
    .line 793
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@167
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@169
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@16c
    .line 794
    sget-boolean v8, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@16e
    if-eqz v8, :cond_21e

    #@170
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@173
    move-result-object v8

    #@174
    if-eqz v8, :cond_21e

    #@176
    .line 796
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@179
    move-result-object v8

    #@17a
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isKeyguardEffectEnabled()Z

    #@17d
    move-result v8

    #@17e
    if-eqz v8, :cond_1a2

    #@180
    .line 797
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsLockEffectEnabled:Z

    #@182
    .line 798
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@185
    move-result-object v8

    #@186
    const-string v11, "lockscreen_effect_sound_name_lock"

    #@188
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    #@18b
    move-result-object v4

    #@18c
    .line 800
    .local v4, lockSoundName:[Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@18f
    move-result-object v8

    #@190
    const-string v11, "lockscreen_effect_sound_name_unlock"

    #@192
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    #@195
    move-result-object v6

    #@196
    .line 803
    .local v6, unlockSoundName:[Ljava/lang/String;
    invoke-direct {p0, v4, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->loadEffectSounds([Ljava/lang/String;Z)[I

    #@199
    move-result-object v8

    #@19a
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockEffectSoundId:[I

    #@19c
    .line 804
    invoke-direct {p0, v6, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->loadEffectSounds([Ljava/lang/String;Z)[I

    #@19f
    move-result-object v8

    #@1a0
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockEffectSoundId:[I

    #@1a2
    .line 808
    .end local v4           #lockSoundName:[Ljava/lang/String;
    .end local v6           #unlockSoundName:[Ljava/lang/String;
    :cond_1a2
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1a5
    move-result-object v8

    #@1a6
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isPatternEffectEnabled()Z

    #@1a9
    move-result v8

    #@1aa
    if-eqz v8, :cond_1ce

    #@1ac
    .line 809
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPatternEffectEnabled:Z

    #@1ae
    .line 810
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1b1
    move-result-object v8

    #@1b2
    const-string v11, "lockscreen_pattern_effect_sound_name_lock"

    #@1b4
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    #@1b7
    move-result-object v4

    #@1b8
    .line 812
    .restart local v4       #lockSoundName:[Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1bb
    move-result-object v8

    #@1bc
    const-string v11, "lockscreen_pattern_effect_sound_name_unlock"

    #@1be
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    #@1c1
    move-result-object v6

    #@1c2
    .line 815
    .restart local v6       #unlockSoundName:[Ljava/lang/String;
    invoke-direct {p0, v4, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->loadEffectSounds([Ljava/lang/String;Z)[I

    #@1c5
    move-result-object v8

    #@1c6
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternLockEffectSoundId:[I

    #@1c8
    .line 816
    invoke-direct {p0, v6, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->loadEffectSounds([Ljava/lang/String;Z)[I

    #@1cb
    move-result-object v8

    #@1cc
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternUnlockEffectSoundId:[I

    #@1ce
    .line 834
    .end local v4           #lockSoundName:[Ljava/lang/String;
    .end local v6           #unlockSoundName:[Ljava/lang/String;
    :cond_1ce
    :goto_1ce
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d1
    move-result-object v8

    #@1d2
    const v9, 0x10e0005

    #@1d5
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    #@1d8
    move-result v3

    #@1d9
    .line 838
    .local v3, lockSoundDefaultAttenuation:I
    const/high16 v8, 0x3f80

    #@1db
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundVolume:F

    #@1dd
    .line 842
    new-instance v2, Landroid/content/IntentFilter;

    #@1df
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@1e2
    .line 843
    .local v2, filterMDM:Landroid/content/IntentFilter;
    const-string v8, "com.lge.mdm.intent.action.EXPIRATION_PASSWORD_CHANGE"

    #@1e4
    invoke-virtual {v2, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e7
    .line 844
    const-string v8, "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

    #@1e9
    invoke-virtual {v2, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1ec
    .line 845
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mBroadCastReceiverMDM:Landroid/content/BroadcastReceiver;

    #@1ee
    invoke-virtual {p1, v8, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1f1
    .line 848
    const-string v8, "service.keyguard.status"

    #@1f3
    const-string v9, "0"

    #@1f5
    invoke-static {v8, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1f8
    .line 849
    const-string v8, "KeyguardViewMediator"

    #@1fa
    const-string v9, "LockScreen status : 0 (Lock released)"

    #@1fc
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ff
    .line 850
    return-void

    #@200
    .line 759
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v2           #filterMDM:Landroid/content/IntentFilter;
    .end local v3           #lockSoundDefaultAttenuation:I
    .end local v7           #wm:Landroid/view/WindowManager;
    .restart local p2
    :cond_200
    new-instance p2, Lcom/android/internal/widget/LockPatternUtils;

    #@202
    .end local p2
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@204
    invoke-direct {p2, v8}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@207
    goto/16 :goto_f9

    #@209
    .restart local v0       #cr:Landroid/content/ContentResolver;
    .restart local v7       #wm:Landroid/view/WindowManager;
    :cond_209
    move v8, v10

    #@20a
    .line 773
    goto/16 :goto_132

    #@20c
    .line 783
    :cond_20c
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@20e
    sget v11, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@210
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setLockTimerState(I)V

    #@213
    goto/16 :goto_157

    #@215
    .line 786
    :cond_215
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@217
    sget v11, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@219
    invoke-virtual {v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setLockTimerState(I)V

    #@21c
    goto/16 :goto_157

    #@21e
    .line 819
    :cond_21e
    const-string v8, "lock_sound"

    #@220
    invoke-static {v0, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@223
    move-result-object v5

    #@224
    .line 820
    .local v5, soundPath:Ljava/lang/String;
    if-eqz v5, :cond_22e

    #@226
    .line 821
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@228
    invoke-virtual {v8, v5, v9}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@22b
    move-result v8

    #@22c
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundId:I

    #@22e
    .line 823
    :cond_22e
    if-eqz v5, :cond_234

    #@230
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundId:I

    #@232
    if-nez v8, :cond_24c

    #@234
    .line 824
    :cond_234
    const-string v8, "KeyguardViewMediator"

    #@236
    new-instance v10, Ljava/lang/StringBuilder;

    #@238
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@23b
    const-string v11, "failed to load lock sound from "

    #@23d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v10

    #@241
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v10

    #@245
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@248
    move-result-object v10

    #@249
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24c
    .line 826
    :cond_24c
    const-string v8, "unlock_sound"

    #@24e
    invoke-static {v0, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@251
    move-result-object v5

    #@252
    .line 827
    if-eqz v5, :cond_25c

    #@254
    .line 828
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@256
    invoke-virtual {v8, v5, v9}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@259
    move-result v8

    #@25a
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockSoundId:I

    #@25c
    .line 830
    :cond_25c
    if-eqz v5, :cond_262

    #@25e
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockSoundId:I

    #@260
    if-nez v8, :cond_1ce

    #@262
    .line 831
    :cond_262
    const-string v8, "KeyguardViewMediator"

    #@264
    new-instance v9, Ljava/lang/StringBuilder;

    #@266
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@269
    const-string v10, "failed to load unlock sound from "

    #@26b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26e
    move-result-object v9

    #@26f
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@272
    move-result-object v9

    #@273
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@276
    move-result-object v9

    #@277
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27a
    goto/16 :goto_1ce
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateLocked(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->newstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->newstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@2
    return p1
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showLocked(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$1400()Z
    .registers 1

    #@0
    .prologue
    .line 110
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->dcmPermLockControl:Z

    #@2
    return v0
.end method

.method static synthetic access$1402(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    sput-boolean p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->dcmPermLockControl:Z

    #@2
    return p0
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->dcmPermLock()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->goinToSleepAndCloseSystemDialog(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1902(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mQuickCoverWindowShowing:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@2
    return p1
.end method

.method static synthetic access$2202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isNaviHide:Z

    #@2
    return p1
.end method

.method static synthetic access$2300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@2
    return v0
.end method

.method static synthetic access$2400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateWithKeepingAwakeLocked(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$2602(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@2
    return p1
.end method

.method static synthetic access$2700()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 110
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->country:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isMatchedIMSI()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->startSKTOTA()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->startKTFOTA()V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->startLGUOTA()V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockoutMode:Z

    #@2
    return v0
.end method

.method static synthetic access$3202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockoutMode:Z

    #@2
    return p1
.end method

.method static synthetic access$3300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->notifyExpirePasswordReset()V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSender:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleShow(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$3700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleHide()V

    #@3
    return-void
.end method

.method static synthetic access$3800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleResetWithKeepingAwake(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleReset(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@2
    return v0
.end method

.method static synthetic access$4000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleVerifyUnlock()V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleNotifyScreenOff()V

    #@3
    return-void
.end method

.method static synthetic access$4200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleNotifyScreenOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleWakeWhenReady(I)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleKeyguardDone(Z)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleKeyguardDoneDrawing()V

    #@3
    return-void
.end method

.method static synthetic access$4600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleSetHidden(Z)V

    #@3
    return-void
.end method

.method static synthetic access$4700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleSKTLockShow()V

    #@3
    return-void
.end method

.method static synthetic access$4900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleExpirePasswordReset()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$5000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleClearLockPattern()V

    #@3
    return-void
.end method

.method static synthetic access$5100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleOMADM()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->sendUserPresentBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$800()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 110
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->carrier:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->oldstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->oldstate:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    return-object p1
.end method

.method private adjustStatusBarLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2114
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@3
    if-nez v3, :cond_11

    #@5
    .line 2115
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@7
    const-string v4, "statusbar"

    #@9
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/app/StatusBarManager;

    #@f
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@11
    .line 2118
    :cond_11
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@13
    if-nez v3, :cond_1d

    #@15
    .line 2119
    const-string v3, "KeyguardViewMediator"

    #@17
    const-string v4, "Could not get status bar manager"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 2180
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 2121
    :cond_1d
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowLockIcon:Z

    #@1f
    if-eqz v3, :cond_45

    #@21
    .line 2123
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@23
    if-eqz v3, :cond_eb

    #@25
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_eb

    #@2b
    .line 2124
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowingLockIcon:Z

    #@2d
    if-nez v3, :cond_45

    #@2f
    .line 2125
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@31
    const v4, 0x1040530

    #@34
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .line 2127
    .local v0, contentDescription:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@3a
    const-string v4, "secure"

    #@3c
    const v5, 0x1080561

    #@3f
    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    #@42
    .line 2130
    const/4 v3, 0x1

    #@43
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowingLockIcon:Z

    #@45
    .line 2142
    .end local v0           #contentDescription:Ljava/lang/String;
    :cond_45
    :goto_45
    const/4 v1, 0x0

    #@46
    .line 2143
    .local v1, flags:I
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@48
    if-eqz v3, :cond_9c

    #@4a
    .line 2147
    const/high16 v3, 0x100

    #@4c
    or-int/2addr v1, v3

    #@4d
    .line 2150
    const/4 v2, 0x0

    #@4e
    .line 2151
    .local v2, lockoutMode:Z
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@50
    if-eqz v3, :cond_7b

    #@52
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@55
    move-result-object v3

    #@56
    if-eqz v3, :cond_7b

    #@58
    .line 2152
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@5b
    move-result-object v3

    #@5c
    const/4 v4, 0x0

    #@5d
    invoke-interface {v3, v4}, Lcom/lge/cappuccino/IMdm;->getLockoutNow(Landroid/content/ComponentName;)Z

    #@60
    move-result v2

    #@61
    .line 2153
    if-eqz v2, :cond_7b

    #@63
    const-string v3, "KeyguardViewMediator"

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "adjustStatusBarLocked : lockoutMode is "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 2157
    :cond_7b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@7e
    move-result v3

    #@7f
    if-nez v3, :cond_87

    #@81
    if-nez v2, :cond_87

    #@83
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@85
    if-eqz v3, :cond_8a

    #@87
    .line 2160
    :cond_87
    const/high16 v3, 0x1

    #@89
    or-int/2addr v1, v3

    #@8a
    .line 2162
    :cond_8a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@8d
    move-result v3

    #@8e
    if-nez v3, :cond_96

    #@90
    if-nez v2, :cond_96

    #@92
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@94
    if-eqz v3, :cond_99

    #@96
    .line 2164
    :cond_96
    const/high16 v3, 0x8

    #@98
    or-int/2addr v1, v3

    #@99
    .line 2167
    :cond_99
    const/high16 v3, 0x200

    #@9b
    or-int/2addr v1, v3

    #@9c
    .line 2172
    .end local v2           #lockoutMode:Z
    :cond_9c
    const-string v3, "KeyguardViewMediator"

    #@9e
    new-instance v4, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v5, "adjustStatusBarLocked: mShowing="

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    const-string v5, " mHidden="

    #@b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    const-string v5, " isSecure="

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@c4
    move-result v5

    #@c5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v4

    #@c9
    const-string v5, " --> flags=0x"

    #@cb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v4

    #@cf
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v4

    #@db
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 2176
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@e0
    instance-of v3, v3, Landroid/app/Activity;

    #@e2
    if-nez v3, :cond_1c

    #@e4
    .line 2177
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@e6
    invoke-virtual {v3, v1}, Landroid/app/StatusBarManager;->disable(I)V

    #@e9
    goto/16 :goto_1c

    #@eb
    .line 2133
    .end local v1           #flags:I
    :cond_eb
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowingLockIcon:Z

    #@ed
    if-eqz v3, :cond_45

    #@ef
    .line 2134
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStatusBarManager:Landroid/app/StatusBarManager;

    #@f1
    const-string v4, "secure"

    #@f3
    invoke-virtual {v3, v4}, Landroid/app/StatusBarManager;->removeIcon(Ljava/lang/String;)V

    #@f6
    .line 2135
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowingLockIcon:Z

    #@f8
    goto/16 :goto_45
.end method

.method private cancelDoKeyguardLaterLocked()V
    .registers 2

    #@0
    .prologue
    .line 1095
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@6
    .line 1096
    return-void
.end method

.method private dcmPermLock()V
    .registers 13

    #@0
    .prologue
    .line 2480
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->dcmPermLockControl:Z

    #@3
    .line 2481
    const/4 v8, 0x0

    #@4
    .line 2483
    .local v8, listener:Landroid/content/DialogInterface$OnClickListener;
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$6;

    #@6
    .end local v8           #listener:Landroid/content/DialogInterface$OnClickListener;
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@9
    .line 2492
    .restart local v8       #listener:Landroid/content/DialogInterface$OnClickListener;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@b
    const v1, 0x104046f

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    .line 2493
    .local v7, buttonTxt:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@14
    const v1, 0x209026e

    #@17
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1a
    move-result-object v11

    #@1b
    .line 2494
    .local v11, title_dcm:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@1d
    const v1, 0x209026f

    #@20
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v9

    #@24
    .line 2496
    .local v9, message_dcm:Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@26
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@28
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@2b
    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@3a
    move-result-object v6

    #@3b
    .line 2501
    .local v6, dialog_dcm:Landroid/app/AlertDialog;
    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@3e
    move-result-object v0

    #@3f
    const/16 v1, 0x7da

    #@41
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@44
    .line 2502
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    #@47
    .line 2503
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$7;

    #@49
    const-wide/16 v2, 0x1770

    #@4b
    const-wide/16 v4, 0x3e8

    #@4d
    move-object v1, p0

    #@4e
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$7;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;JJLandroid/app/AlertDialog;)V

    #@51
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$7;->start()Landroid/os/CountDownTimer;

    #@54
    move-result-object v10

    #@55
    .line 2512
    .local v10, timer:Landroid/os/CountDownTimer;
    return-void
.end method

.method private doKeyguardLaterLocked(I)V
    .registers 23
    .parameter "why"

    #@0
    .prologue
    .line 988
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v18, v0

    #@6
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v4

    #@a
    .line 991
    .local v4, cr:Landroid/content/ContentResolver;
    const-string v18, "screen_off_timeout"

    #@c
    const/16 v19, 0x7530

    #@e
    move-object/from16 v0, v18

    #@10
    move/from16 v1, v19

    #@12
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15
    move-result v18

    #@16
    move/from16 v0, v18

    #@18
    int-to-long v5, v0

    #@19
    .line 995
    .local v5, displayTimeout:J
    const-string v18, "lock_screen_lock_after_timeout"

    #@1b
    const/16 v19, 0x1388

    #@1d
    move-object/from16 v0, v18

    #@1f
    move/from16 v1, v19

    #@21
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@24
    move-result v18

    #@25
    move/from16 v0, v18

    #@27
    int-to-long v8, v0

    #@28
    .line 1000
    .local v8, lockAfterTimeout:J
    move-object/from16 v0, p0

    #@2a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2c
    move-object/from16 v18, v0

    #@2e
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@31
    move-result-object v18

    #@32
    const/16 v19, 0x0

    #@34
    move-object/from16 v0, p0

    #@36
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@38
    move-object/from16 v20, v0

    #@3a
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@3d
    move-result v20

    #@3e
    invoke-virtual/range {v18 .. v20}, Landroid/app/admin/DevicePolicyManager;->getMaximumTimeToLock(Landroid/content/ComponentName;I)J

    #@41
    move-result-wide v10

    #@42
    .line 1004
    .local v10, policyTimeout:J
    const-wide/16 v18, 0x0

    #@44
    cmp-long v18, v10, v18

    #@46
    if-lez v18, :cond_70

    #@48
    .line 1006
    const-wide/16 v18, 0x0

    #@4a
    move-wide/from16 v0, v18

    #@4c
    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->max(JJ)J

    #@4f
    move-result-wide v5

    #@50
    .line 1007
    sub-long v18, v10, v5

    #@52
    move-wide/from16 v0, v18

    #@54
    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    #@57
    move-result-wide v14

    #@58
    .line 1012
    .local v14, timeout:J
    :goto_58
    const-wide/16 v18, 0x0

    #@5a
    cmp-long v18, v14, v18

    #@5c
    if-gtz v18, :cond_93

    #@5e
    .line 1014
    move-object/from16 v0, p0

    #@60
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@62
    move/from16 v18, v0

    #@64
    if-eqz v18, :cond_72

    #@66
    .line 1015
    const/16 v18, 0x0

    #@68
    move-object/from16 v0, p0

    #@6a
    move-object/from16 v1, v18

    #@6c
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateLocked(Landroid/os/Bundle;)V

    #@6f
    .line 1092
    :cond_6f
    :goto_6f
    return-void

    #@70
    .line 1009
    .end local v14           #timeout:J
    :cond_70
    move-wide v14, v8

    #@71
    .restart local v14       #timeout:J
    goto :goto_58

    #@72
    .line 1020
    :cond_72
    const/16 v18, 0x2

    #@74
    move/from16 v0, p1

    #@76
    move/from16 v1, v18

    #@78
    if-eq v0, v1, :cond_82

    #@7a
    .line 1021
    const/16 v18, 0x1

    #@7c
    move/from16 v0, v18

    #@7e
    move-object/from16 v1, p0

    #@80
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@82
    .line 1023
    :cond_82
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@84
    const/16 v19, 0x1

    #@86
    move-object/from16 v0, p0

    #@88
    move/from16 v1, v18

    #@8a
    move/from16 v2, v19

    #@8c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@8f
    .line 1024
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@92
    goto :goto_6f

    #@93
    .line 1029
    :cond_93
    const/4 v12, 0x0

    #@94
    .line 1030
    .local v12, sendPendingIntent:Z
    move-object/from16 v0, p0

    #@96
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@98
    move/from16 v18, v0

    #@9a
    if-eqz v18, :cond_a5

    #@9c
    .line 1031
    const/16 v18, 0x0

    #@9e
    move-object/from16 v0, p0

    #@a0
    move-object/from16 v1, v18

    #@a2
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateLocked(Landroid/os/Bundle;)V

    #@a5
    .line 1033
    :cond_a5
    move-object/from16 v0, p0

    #@a7
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a9
    move-object/from16 v18, v0

    #@ab
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@ae
    move-result v18

    #@af
    sget v19, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@b1
    move/from16 v0, v18

    #@b3
    move/from16 v1, v19

    #@b5
    if-ne v0, v1, :cond_129

    #@b7
    .line 1034
    move-object/from16 v0, p0

    #@b9
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@bb
    move/from16 v18, v0

    #@bd
    if-nez v18, :cond_c0

    #@bf
    .line 1035
    const/4 v12, 0x1

    #@c0
    .line 1077
    :cond_c0
    :goto_c0
    if-eqz v12, :cond_6f

    #@c2
    .line 1079
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c5
    move-result-wide v18

    #@c6
    add-long v16, v18, v14

    #@c8
    .line 1080
    .local v16, when:J
    new-instance v7, Landroid/content/Intent;

    #@ca
    const-string v18, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    #@cc
    move-object/from16 v0, v18

    #@ce
    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d1
    .line 1081
    .local v7, intent:Landroid/content/Intent;
    const-string v18, "seq"

    #@d3
    move-object/from16 v0, p0

    #@d5
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@d7
    move/from16 v19, v0

    #@d9
    move-object/from16 v0, v18

    #@db
    move/from16 v1, v19

    #@dd
    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@e0
    .line 1082
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@e4
    move-object/from16 v18, v0

    #@e6
    const/16 v19, 0x0

    #@e8
    const/high16 v20, 0x1000

    #@ea
    move-object/from16 v0, v18

    #@ec
    move/from16 v1, v19

    #@ee
    move/from16 v2, v20

    #@f0
    invoke-static {v0, v1, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@f3
    move-result-object v13

    #@f4
    .line 1085
    .local v13, sender:Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    #@f6
    iput-object v13, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSender:Landroid/app/PendingIntent;

    #@f8
    .line 1087
    move-object/from16 v0, p0

    #@fa
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAlarmManager:Landroid/app/AlarmManager;

    #@fc
    move-object/from16 v18, v0

    #@fe
    const/16 v19, 0x2

    #@100
    move-object/from16 v0, v18

    #@102
    move/from16 v1, v19

    #@104
    move-wide/from16 v2, v16

    #@106
    invoke-virtual {v0, v1, v2, v3, v13}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@109
    .line 1088
    const-string v18, "KeyguardViewMediator"

    #@10b
    new-instance v19, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v20, "setting alarm to turn off keyguard, seq = "

    #@112
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v19

    #@116
    move-object/from16 v0, p0

    #@118
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@11a
    move/from16 v20, v0

    #@11c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v19

    #@120
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v19

    #@124
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    goto/16 :goto_6f

    #@129
    .line 1047
    .end local v7           #intent:Landroid/content/Intent;
    .end local v13           #sender:Landroid/app/PendingIntent;
    .end local v16           #when:J
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@12d
    move/from16 v18, v0

    #@12f
    if-nez v18, :cond_184

    #@131
    .line 1048
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@135
    move-object/from16 v18, v0

    #@137
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@13a
    move-result v18

    #@13b
    if-eqz v18, :cond_176

    #@13d
    .line 1049
    const/16 v18, 0x2

    #@13f
    move/from16 v0, p1

    #@141
    move/from16 v1, v18

    #@143
    if-eq v0, v1, :cond_14d

    #@145
    .line 1050
    const/16 v18, 0x1

    #@147
    move/from16 v0, v18

    #@149
    move-object/from16 v1, p0

    #@14b
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@14d
    .line 1052
    :cond_14d
    move-object/from16 v0, p0

    #@14f
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@151
    move/from16 v18, v0

    #@153
    if-nez v18, :cond_168

    #@155
    .line 1053
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_START:I

    #@157
    const/16 v19, 0x1

    #@159
    move-object/from16 v0, p0

    #@15b
    move/from16 v1, v18

    #@15d
    move/from16 v2, v19

    #@15f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@162
    .line 1054
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@165
    .line 1061
    :goto_165
    const/4 v12, 0x1

    #@166
    goto/16 :goto_c0

    #@168
    .line 1056
    :cond_168
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@16a
    const/16 v19, 0x1

    #@16c
    move-object/from16 v0, p0

    #@16e
    move/from16 v1, v18

    #@170
    move/from16 v2, v19

    #@172
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@175
    goto :goto_165

    #@176
    .line 1059
    :cond_176
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@178
    const/16 v19, 0x1

    #@17a
    move-object/from16 v0, p0

    #@17c
    move/from16 v1, v18

    #@17e
    move/from16 v2, v19

    #@180
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@183
    goto :goto_165

    #@184
    .line 1063
    :cond_184
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@188
    move-object/from16 v18, v0

    #@18a
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@18d
    move-result v18

    #@18e
    sget v19, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@190
    move/from16 v0, v18

    #@192
    move/from16 v1, v19

    #@194
    if-ne v0, v1, :cond_1a5

    #@196
    .line 1064
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@198
    const/16 v19, 0x1

    #@19a
    move-object/from16 v0, p0

    #@19c
    move/from16 v1, v18

    #@19e
    move/from16 v2, v19

    #@1a0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@1a3
    goto/16 :goto_c0

    #@1a5
    .line 1066
    :cond_1a5
    move-object/from16 v0, p0

    #@1a7
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1a9
    move-object/from16 v18, v0

    #@1ab
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@1ae
    move-result v18

    #@1af
    if-eqz v18, :cond_1c1

    #@1b1
    .line 1067
    const/4 v12, 0x1

    #@1b2
    .line 1068
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_START:I

    #@1b4
    const/16 v19, 0x1

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    move/from16 v1, v18

    #@1ba
    move/from16 v2, v19

    #@1bc
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@1bf
    goto/16 :goto_c0

    #@1c1
    .line 1071
    :cond_1c1
    sget v18, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@1c3
    const/16 v19, 0x1

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    move/from16 v1, v18

    #@1c9
    move/from16 v2, v19

    #@1cb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@1ce
    goto/16 :goto_c0
.end method

.method private doKeyguardLocked()V
    .registers 2

    #@0
    .prologue
    .line 1301
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked(Landroid/os/Bundle;)V

    #@4
    .line 1302
    return-void
.end method

.method private doKeyguardLocked(Landroid/os/Bundle;)V
    .registers 10
    .parameter "options"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    .line 1309
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@4
    if-nez v6, :cond_e

    #@6
    .line 1310
    const-string v5, "KeyguardViewMediator"

    #@8
    const-string v6, "doKeyguard: not showing because externally disabled"

    #@a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1390
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1325
    :cond_e
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@15
    move-result v6

    #@16
    if-eq v6, v7, :cond_d

    #@18
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1a
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isVTActive()Z

    #@1d
    move-result v6

    #@1e
    if-nez v6, :cond_d

    #@20
    .line 1331
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@22
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->isShowing()Z

    #@25
    move-result v6

    #@26
    if-eqz v6, :cond_30

    #@28
    .line 1332
    const-string v5, "KeyguardViewMediator"

    #@2a
    const-string v6, "doKeyguard: not showing because it is already showing"

    #@2c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_d

    #@30
    .line 1337
    :cond_30
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@32
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@35
    move-result v3

    #@36
    .line 1338
    .local v3, provisioned:Z
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@3d
    move-result v2

    #@3e
    .line 1339
    .local v2, numPhones:I
    new-array v4, v2, [Lcom/android/internal/telephony/IccCardConstants$State;

    #@40
    .line 1340
    .local v4, state:[Lcom/android/internal/telephony/IccCardConstants$State;
    const/4 v1, 0x0

    #@41
    .line 1341
    .local v1, lockedOrMissing:Z
    const/4 v0, 0x0

    #@42
    .local v0, i:I
    :goto_42
    if-ge v0, v2, :cond_59

    #@44
    .line 1342
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@46
    invoke-virtual {v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    #@49
    move-result-object v6

    #@4a
    aput-object v6, v4, v0

    #@4c
    .line 1343
    if-nez v1, :cond_56

    #@4e
    aget-object v6, v4, v0

    #@50
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isLockedOrMissing(Lcom/android/internal/telephony/IccCardConstants$State;)Z

    #@53
    move-result v6

    #@54
    if-eqz v6, :cond_65

    #@56
    :cond_56
    move v1, v5

    #@57
    .line 1344
    :goto_57
    if-eqz v1, :cond_67

    #@59
    .line 1347
    :cond_59
    if-nez v1, :cond_6a

    #@5b
    if-nez v3, :cond_6a

    #@5d
    .line 1348
    const-string v5, "KeyguardViewMediator"

    #@5f
    const-string v6, "doKeyguard: not showing because device isn\'t provisioned and the sim is not locked or missing"

    #@61
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_d

    #@65
    .line 1343
    :cond_65
    const/4 v1, 0x0

    #@66
    goto :goto_57

    #@67
    .line 1341
    :cond_67
    add-int/lit8 v0, v0, 0x1

    #@69
    goto :goto_42

    #@6a
    .line 1353
    :cond_6a
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserManager:Landroid/os/UserManager;

    #@6c
    invoke-virtual {v6, v5}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    #@6f
    move-result-object v5

    #@70
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@73
    move-result v5

    #@74
    if-ge v5, v7, :cond_88

    #@76
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@78
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled()Z

    #@7b
    move-result v5

    #@7c
    if-eqz v5, :cond_88

    #@7e
    if-nez v1, :cond_88

    #@80
    .line 1355
    const-string v5, "KeyguardViewMediator"

    #@82
    const-string v6, "doKeyguard: not showing because lockscreen is off"

    #@84
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    goto :goto_d

    #@88
    .line 1361
    :cond_88
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8a
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDsdpPlayed()Z

    #@8d
    move-result v5

    #@8e
    if-eqz v5, :cond_99

    #@90
    .line 1362
    const-string v5, "KeyguardViewMediator"

    #@92
    const-string v6, "doKeyguard: LockScreen is not showing because DSDP is played"

    #@94
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto/16 :goto_d

    #@99
    .line 1369
    :cond_99
    sget-boolean v5, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@9b
    if-eqz v5, :cond_b4

    #@9d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@a0
    move-result v5

    #@a1
    if-nez v5, :cond_b4

    #@a3
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a5
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHdmiPluggedIn()Z

    #@a8
    move-result v5

    #@a9
    if-eqz v5, :cond_b4

    #@ab
    .line 1372
    const-string v5, "KeyguardViewMediator"

    #@ad
    const-string v6, "doKeyguard: slide lock is not showing because MHL is connected"

    #@af
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    goto/16 :goto_d

    #@b4
    .line 1379
    :cond_b4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@b7
    move-result v5

    #@b8
    if-nez v5, :cond_db

    #@ba
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@bc
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@bf
    move-result v5

    #@c0
    if-eqz v5, :cond_db

    #@c2
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@c4
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCover()Z

    #@c7
    move-result v5

    #@c8
    if-nez v5, :cond_d2

    #@ca
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@cc
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@cf
    move-result v5

    #@d0
    if-eqz v5, :cond_db

    #@d2
    .line 1382
    :cond_d2
    const-string v5, "KeyguardViewMediator"

    #@d4
    const-string v6, "doKeyguard: swipe lock is not showing because quick cover is closed"

    #@d6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    goto/16 :goto_d

    #@db
    .line 1388
    :cond_db
    const-string v5, "KeyguardViewMediator"

    #@dd
    const-string v6, "doKeyguard: showing the lock screen"

    #@df
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 1389
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showLocked(Landroid/os/Bundle;)V

    #@e5
    goto/16 :goto_d
.end method

.method private goinToSleepAndCloseSystemDialog(Z)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 608
    if-eqz p1, :cond_b

    #@2
    .line 610
    :try_start_2
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5
    move-result-object v1

    #@6
    const-string v2, "quick_cover_window"

    #@8
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->closeSystemDialogs(Ljava/lang/String;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_b} :catch_c

    #@b
    .line 626
    :cond_b
    :goto_b
    return-void

    #@c
    .line 613
    :catch_c
    move-exception v0

    #@d
    .line 614
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@10
    goto :goto_b
.end method

.method private handleClearLockPattern()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2271
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleClearLockPattern()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2273
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->clearLockPattern()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_1f

    #@10
    .line 2274
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 2275
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@1a
    .line 2276
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1c
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Z)V

    #@1f
    .line 2281
    :cond_1f
    invoke-virtual {p0, v2, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@22
    .line 2282
    return-void
.end method

.method private handleExpirePasswordReset()V
    .registers 3

    #@0
    .prologue
    .line 2411
    monitor-enter p0

    #@1
    .line 2413
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "LGMDM : handleExpirePasswordReset"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2415
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->expirePasswordReset()V

    #@d
    .line 2416
    monitor-exit p0

    #@e
    .line 2417
    return-void

    #@f
    .line 2416
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method private handleHide()V
    .registers 3

    #@0
    .prologue
    .line 2087
    monitor-enter p0

    #@1
    .line 2088
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleHide"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2089
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_19

    #@10
    .line 2090
    const-string v0, "KeyguardViewMediator"

    #@12
    const-string v1, "attempt to hide the keyguard while waking, ignored"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 2091
    monitor-exit p0

    #@18
    .line 2111
    :goto_18
    return-void

    #@19
    .line 2096
    :cond_19
    sget-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@1b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPhoneState:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2f

    #@23
    .line 2098
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@25
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isVTActive()Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_2f

    #@2b
    .line 2099
    const/4 v0, 0x0

    #@2c
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->playSounds(Z)V

    #@2f
    .line 2104
    :cond_2f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@31
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->hide()V

    #@34
    .line 2105
    const/4 v0, 0x0

    #@35
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@37
    .line 2106
    const/4 v0, 0x0

    #@38
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@3a
    .line 2107
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@3d
    .line 2109
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@3f
    const/4 v1, 0x0

    #@40
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@43
    .line 2110
    monitor-exit p0

    #@44
    goto :goto_18

    #@45
    :catchall_45
    move-exception v0

    #@46
    monitor-exit p0
    :try_end_47
    .catchall {:try_start_1 .. :try_end_47} :catchall_45

    #@47
    throw v0
.end method

.method private handleKeyguardDone(Z)V
    .registers 4
    .parameter "wakeup"

    #@0
    .prologue
    .line 1941
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "handleKeyguardDone"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1942
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->handleHide()V

    #@a
    .line 1943
    if-eqz p1, :cond_f

    #@c
    .line 1944
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeUp()V

    #@f
    .line 1947
    :cond_f
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->sendUserPresentBroadcast()V

    #@12
    .line 1950
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@14
    if-eqz v0, :cond_37

    #@16
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@19
    move-result-object v0

    #@1a
    if-eqz v0, :cond_37

    #@1c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1f
    move-result-object v0

    #@20
    const-string v1, "config_feature_attach_reject_lgu"

    #@22
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_37

    #@28
    .line 1952
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@2a
    if-eqz v0, :cond_37

    #@2c
    .line 1953
    const-string v0, "KeyguardViewMediator"

    #@2e
    const-string v1, "Lock the screen again by RejectCode"

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1954
    const/4 v0, 0x0

    #@34
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showLocked(Landroid/os/Bundle;)V

    #@37
    .line 1957
    :cond_37
    return-void
.end method

.method private handleKeyguardDoneDrawing()V
    .registers 3

    #@0
    .prologue
    .line 1971
    monitor-enter p0

    #@1
    .line 1973
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@3
    if-eqz v0, :cond_19

    #@5
    .line 1974
    const-string v0, "KeyguardViewMediator"

    #@7
    const-string v1, "handleKeyguardDoneDrawing: notifying mWaitingUntilKeyguardVisible"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1975
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@f
    .line 1976
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@12
    .line 1981
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@14
    const/16 v1, 0xa

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@19
    .line 1983
    :cond_19
    monitor-exit p0

    #@1a
    .line 1984
    return-void

    #@1b
    .line 1983
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method private handleNotifyScreenOff()V
    .registers 3

    #@0
    .prologue
    .line 2240
    monitor-enter p0

    #@1
    .line 2241
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleNotifyScreenOff"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2242
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->onScreenTurnedOff()V

    #@d
    .line 2243
    monitor-exit p0

    #@e
    .line 2244
    return-void

    #@f
    .line 2243
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method private handleNotifyScreenOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V
    .registers 4
    .parameter "showListener"

    #@0
    .prologue
    .line 2251
    monitor-enter p0

    #@1
    .line 2252
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleNotifyScreenOn"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2254
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@d
    move-result v0

    #@e
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_START:I

    #@10
    if-ne v0, v1, :cond_18

    #@12
    .line 2255
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_STOPED:I

    #@14
    const/4 v1, 0x0

    #@15
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@18
    .line 2257
    :cond_18
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@1a
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@1d
    .line 2258
    monitor-exit p0

    #@1e
    .line 2259
    return-void

    #@1f
    .line 2258
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method private handleOMADM()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2292
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleClearLockPattern()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2293
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->clearLockPattern()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_34

    #@10
    .line 2294
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    if-eqz v0, :cond_26

    #@14
    .line 2295
    const-string v0, "KeyguardViewMediator"

    #@16
    const-string v1, "mLockPatternUtils"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2296
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1d
    const/4 v1, 0x0

    #@1e
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@21
    .line 2297
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@23
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Z)V

    #@26
    .line 2303
    :cond_26
    :goto_26
    monitor-enter p0

    #@27
    .line 2305
    :try_start_27
    const-string v0, "KeyguardViewMediator"

    #@29
    const-string v1, "OMADM showLocked"

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 2308
    const/4 v0, 0x0

    #@2f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showLocked(Landroid/os/Bundle;)V

    #@32
    .line 2309
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_27 .. :try_end_33} :catchall_38

    #@33
    .line 2310
    return-void

    #@34
    .line 2300
    :cond_34
    invoke-virtual {p0, v2, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@37
    goto :goto_26

    #@38
    .line 2309
    :catchall_38
    move-exception v0

    #@39
    :try_start_39
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    throw v0
.end method

.method private handleReset(Landroid/os/Bundle;)V
    .registers 4
    .parameter "options"

    #@0
    .prologue
    .line 2213
    monitor-enter p0

    #@1
    .line 2214
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleReset"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2217
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 2218
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@e
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->reset(Landroid/os/Bundle;)V

    #@11
    .line 2220
    :cond_11
    monitor-exit p0

    #@12
    .line 2221
    return-void

    #@13
    .line 2220
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private handleResetWithKeepingAwake(Landroid/os/Bundle;)V
    .registers 4
    .parameter "options"

    #@0
    .prologue
    .line 2463
    monitor-enter p0

    #@1
    .line 2466
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@3
    if-eqz v0, :cond_14

    #@5
    .line 2468
    const-string v0, "KeyguardViewMediator"

    #@7
    const-string v1, "handleResetWithKeepingAwake"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 2470
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@e
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->reset(Landroid/os/Bundle;)V

    #@11
    .line 2471
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@14
    .line 2473
    :cond_14
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@16
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@19
    .line 2474
    monitor-exit p0

    #@1a
    .line 2475
    return-void

    #@1b
    .line 2474
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method private handleSKTLockShow()V
    .registers 5

    #@0
    .prologue
    .line 2338
    monitor-enter p0

    #@1
    .line 2339
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "[SKT Lock&Wipe] handleSKTLockShow"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2340
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSystemReady:Z

    #@a
    if-nez v0, :cond_e

    #@c
    monitor-exit p0

    #@d
    .line 2356
    :goto_d
    return-void

    #@e
    .line 2342
    :cond_e
    const/4 v0, 0x1

    #@f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->playSounds(Z)V

    #@12
    .line 2344
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->show(Landroid/os/Bundle;)V

    #@18
    .line 2345
    const/4 v0, 0x1

    #@19
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@1b
    .line 2346
    const/4 v0, 0x0

    #@1c
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@1e
    .line 2347
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@21
    .line 2348
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity()V
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_3f

    #@24
    .line 2350
    :try_start_24
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@27
    move-result-object v0

    #@28
    const-string v1, "lock"

    #@2a
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->closeSystemDialogs(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_24 .. :try_end_2d} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2d} :catch_42

    #@2d
    .line 2353
    :goto_2d
    :try_start_2d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2f
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mEnableUserUnlock:Z

    #@31
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStrPassword:Ljava/lang/String;

    #@33
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserMsg:Ljava/lang/String;

    #@35
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V

    #@38
    .line 2354
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3d
    .line 2355
    monitor-exit p0

    #@3e
    goto :goto_d

    #@3f
    :catchall_3f
    move-exception v0

    #@40
    monitor-exit p0
    :try_end_41
    .catchall {:try_start_2d .. :try_end_41} :catchall_3f

    #@41
    throw v0

    #@42
    .line 2351
    :catch_42
    move-exception v0

    #@43
    goto :goto_2d
.end method

.method private handleSetHidden(Z)V
    .registers 3
    .parameter "isHidden"

    #@0
    .prologue
    .line 1273
    monitor-enter p0

    #@1
    .line 1274
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1275
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@7
    .line 1276
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@a
    .line 1278
    :cond_a
    monitor-exit p0

    #@b
    .line 1279
    return-void

    #@c
    .line 1278
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method private handleShow(Landroid/os/Bundle;)V
    .registers 4
    .parameter "options"

    #@0
    .prologue
    .line 2050
    monitor-enter p0

    #@1
    .line 2051
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleShow"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2052
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSystemReady:Z

    #@a
    if-nez v0, :cond_e

    #@c
    monitor-exit p0

    #@d
    .line 2080
    :goto_d
    return-void

    #@e
    .line 2055
    :cond_e
    const/4 v0, 0x1

    #@f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->playSounds(Z)V

    #@12
    .line 2057
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@14
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@17
    move-result v0

    #@18
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@1a
    if-ne v0, v1, :cond_22

    #@1c
    .line 2058
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@22
    .line 2062
    :cond_22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@24
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->show(Landroid/os/Bundle;)V

    #@27
    .line 2064
    const/4 v0, 0x0

    #@28
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsShowingKeyguardRequestedButNotHandledYet:Z

    #@2a
    .line 2066
    const/4 v0, 0x1

    #@2b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2d
    .line 2067
    const/4 v0, 0x0

    #@2e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@30
    .line 2068
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->adjustStatusBarLocked()V

    #@33
    .line 2069
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity()V
    :try_end_36
    .catchall {:try_start_1 .. :try_end_36} :catchall_46

    #@36
    .line 2071
    :try_start_36
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@39
    move-result-object v0

    #@3a
    const-string v1, "lock"

    #@3c
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->closeSystemDialogs(Ljava/lang/String;)V
    :try_end_3f
    .catchall {:try_start_36 .. :try_end_3f} :catchall_46
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_3f} :catch_49

    #@3f
    .line 2078
    :goto_3f
    :try_start_3f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@41
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@44
    .line 2079
    monitor-exit p0

    #@45
    goto :goto_d

    #@46
    :catchall_46
    move-exception v0

    #@47
    monitor-exit p0
    :try_end_48
    .catchall {:try_start_3f .. :try_end_48} :catchall_46

    #@48
    throw v0

    #@49
    .line 2072
    :catch_49
    move-exception v0

    #@4a
    goto :goto_3f
.end method

.method private handleVerifyUnlock()V
    .registers 3

    #@0
    .prologue
    .line 2228
    monitor-enter p0

    #@1
    .line 2229
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "handleVerifyUnlock"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2230
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->verifyUnlock()V

    #@d
    .line 2231
    const/4 v0, 0x1

    #@e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@10
    .line 2232
    monitor-exit p0

    #@11
    .line 2233
    return-void

    #@12
    .line 2232
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method private handleWakeWhenReady(I)V
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 2188
    monitor-enter p0

    #@1
    .line 2193
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->wakeWhenReadyTq(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_13

    #@9
    .line 2195
    const-string v0, "KeyguardViewMediator"

    #@b
    const-string v1, "mKeyguardViewManager.wakeWhenReadyTq did not poke wake lock, so poke it ourselves"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 2197
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeUp()V

    #@13
    .line 2204
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@15
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@18
    .line 2205
    monitor-exit p0

    #@19
    .line 2206
    return-void

    #@1a
    .line 2205
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method private hideLocked()V
    .registers 4

    #@0
    .prologue
    .line 1496
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "hideLocked"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1497
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v2, 0x3

    #@a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1498
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 1499
    return-void
.end method

.method private isAssistantAvailable()Z
    .registers 4

    #@0
    .prologue
    .line 2515
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSearchManager:Landroid/app/SearchManager;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSearchManager:Landroid/app/SearchManager;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@8
    const/4 v2, -0x2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@c
    move-result-object v0

    #@d
    if-eqz v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private isMatchedIMSI()Z
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2373
    const/4 v0, 0x0

    #@2
    .line 2374
    .local v0, getSubscriberId1:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .line 2375
    .local v1, getSubscriberId2:Ljava/lang/String;
    const/4 v2, 0x0

    #@4
    .line 2376
    .local v2, getUsimPersoEfImsi1:Ljava/lang/String;
    const/4 v3, 0x0

    #@5
    .line 2377
    .local v3, getUsimPersoEfImsi2:Ljava/lang/String;
    const/4 v5, 0x1

    #@6
    .line 2378
    .local v5, matched:Z
    const/4 v4, 0x0

    #@7
    .line 2380
    .local v4, mTelephonyManager:Landroid/telephony/TelephonyManager;
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@9
    const-string v8, "phone"

    #@b
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    .end local v4           #mTelephonyManager:Landroid/telephony/TelephonyManager;
    check-cast v4, Landroid/telephony/TelephonyManager;

    #@11
    .line 2383
    .restart local v4       #mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 2385
    const-string v7, "KeyguardViewMediator"

    #@17
    new-instance v8, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v9, "[jekim] getSubscriberId1(Read) = "

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2387
    if-eqz v0, :cond_35

    #@2f
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@32
    move-result v7

    #@33
    if-nez v7, :cond_3d

    #@35
    .line 2388
    :cond_35
    const-string v7, "KeyguardViewMediator"

    #@37
    const-string v8, "[isUsimPersoLockScreenMode] getSubscriberId1  NULL"

    #@39
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 2402
    :goto_3c
    return v6

    #@3d
    .line 2391
    :cond_3d
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    .line 2392
    const-string v7, "KeyguardViewMediator"

    #@43
    new-instance v8, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v9, "[jekim] getSubscriberId2 = "

    #@4a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v8

    #@4e
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v8

    #@52
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v8

    #@56
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2394
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@5b
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5e
    move-result-object v7

    #@5f
    const-string v8, "usim_perso_imsi"

    #@61
    invoke-static {v7, v8}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v5

    #@69
    .line 2395
    const-string v7, "KeyguardViewMediator"

    #@6b
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v9, "[jekim] matched = "

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 2397
    if-eqz v5, :cond_8c

    #@83
    .line 2398
    const-string v6, "KeyguardViewMediator"

    #@85
    const-string v7, "[jekim]  IMSI matched !!- RETURN LOCKMODE FALSE"

    #@87
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 2399
    const/4 v6, 0x1

    #@8b
    goto :goto_3c

    #@8c
    .line 2401
    :cond_8c
    const-string v7, "KeyguardViewMediator"

    #@8e
    const-string v8, "[jekim] IMSI  NOT matched!!- RETURN LOCKMODE TRUE"

    #@90
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    goto :goto_3c
.end method

.method private loadEffectSounds([Ljava/lang/String;Z)[I
    .registers 14
    .parameter "soundName"
    .parameter "locked"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 853
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    .line 854
    .local v0, cr:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@b
    move-result-object v7

    #@c
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getPackageContext()Landroid/content/Context;

    #@f
    move-result-object v2

    #@10
    .line 856
    .local v2, packageContext:Landroid/content/Context;
    const-string v5, "/system/media/audio/ui"

    #@12
    .line 858
    .local v5, soundPathPrefix:Ljava/lang/String;
    const/16 v7, 0xa

    #@14
    new-array v3, v7, [I

    #@16
    .line 860
    .local v3, soundId:[I
    if-eqz p1, :cond_70

    #@18
    .line 861
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    array-length v7, p1

    #@1a
    if-ge v1, v7, :cond_a0

    #@1c
    .line 862
    aget-object v7, p1, v1

    #@1e
    if-eqz v7, :cond_34

    #@20
    .line 864
    aget-object v7, p1, v1

    #@22
    const-string v8, "/system/media/audio/ui"

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_59

    #@2a
    .line 865
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@2c
    aget-object v8, p1, v1

    #@2e
    invoke-virtual {v7, v8, v10}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@31
    move-result v7

    #@32
    aput v7, v3, v1

    #@34
    .line 875
    :cond_34
    :goto_34
    aget-object v7, p1, v1

    #@36
    if-eqz v7, :cond_3c

    #@38
    aget v7, v3, v1

    #@3a
    if-nez v7, :cond_56

    #@3c
    .line 877
    :cond_3c
    const-string v7, "KeyguardViewMediator"

    #@3e
    new-instance v8, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v9, "failed to load sound from "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    aget-object v9, p1, v1

    #@4b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v8

    #@53
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 861
    :cond_56
    add-int/lit8 v1, v1, 0x1

    #@58
    goto :goto_19

    #@59
    .line 867
    :cond_59
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@5c
    move-result-object v7

    #@5d
    aget-object v8, p1, v1

    #@5f
    const-string v9, "raw"

    #@61
    invoke-virtual {v7, v8, v9}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    move-result v6

    #@65
    .line 870
    .local v6, soundResId:I
    if-eqz v6, :cond_34

    #@67
    .line 871
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@69
    invoke-virtual {v7, v2, v6, v10}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    #@6c
    move-result v7

    #@6d
    aput v7, v3, v1

    #@6f
    goto :goto_34

    #@70
    .line 883
    .end local v1           #i:I
    .end local v6           #soundResId:I
    :cond_70
    if-eqz p2, :cond_a1

    #@72
    .line 884
    const-string v7, "lock_sound"

    #@74
    invoke-static {v0, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    .line 888
    .local v4, soundPath:Ljava/lang/String;
    :goto_78
    if-eqz v4, :cond_82

    #@7a
    .line 889
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@7c
    invoke-virtual {v7, v4, v10}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@7f
    move-result v7

    #@80
    aput v7, v3, v8

    #@82
    .line 891
    :cond_82
    if-eqz v4, :cond_88

    #@84
    aget v7, v3, v8

    #@86
    if-nez v7, :cond_a0

    #@88
    .line 893
    :cond_88
    const-string v7, "KeyguardViewMediator"

    #@8a
    new-instance v8, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v9, "failed to load sound from "

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v8

    #@99
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v8

    #@9d
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 897
    .end local v4           #soundPath:Ljava/lang/String;
    :cond_a0
    return-object v3

    #@a1
    .line 886
    :cond_a1
    const-string v7, "unlock_sound"

    #@a3
    invoke-static {v0, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@a6
    move-result-object v4

    #@a7
    .restart local v4       #soundPath:Ljava/lang/String;
    goto :goto_78
.end method

.method private maybeSendUserPresentBroadcast()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1114
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSystemReady:Z

    #@3
    if-eqz v0, :cond_1c

    #@5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_1c

    #@d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserManager:Landroid/os/UserManager;

    #@f
    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    #@12
    move-result-object v0

    #@13
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@16
    move-result v0

    #@17
    if-ne v0, v1, :cond_1c

    #@19
    .line 1119
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->sendUserPresentBroadcast()V

    #@1c
    .line 1121
    :cond_1c
    return-void
.end method

.method private notifyExpirePasswordReset()V
    .registers 3

    #@0
    .prologue
    .line 2423
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "LGMDM : notifyExpirePasswordReset"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2425
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x1e

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e
    .line 2426
    return-void
.end method

.method private notifyScreenOffLocked()V
    .registers 3

    #@0
    .prologue
    .line 1436
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "notifyScreenOffLocked"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1437
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x6

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@d
    .line 1438
    return-void
.end method

.method private notifyScreenOnLocked(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V
    .registers 5
    .parameter "showListener"

    #@0
    .prologue
    .line 1446
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "notifyScreenOnLocked"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1447
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v2, 0x7

    #@a
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1448
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 1449
    return-void
.end method

.method private playSounds(Z)V
    .registers 11
    .parameter "locked"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1989
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 1990
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@8
    .line 2043
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1995
    :cond_9
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@b
    if-nez v0, :cond_8

    #@d
    .line 1999
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v7

    #@13
    .line 2000
    .local v7, cr:Landroid/content/ContentResolver;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@18
    move-result-object v0

    #@19
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1b
    if-ne v0, v2, :cond_78

    #@1d
    move v0, v4

    #@1e
    :goto_1e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPattern:Z

    #@20
    .line 2001
    const-string v0, "lockscreen_sounds_enabled"

    #@22
    invoke-static {v7, v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@25
    move-result v0

    #@26
    if-ne v0, v4, :cond_8

    #@28
    .line 2007
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPattern:Z

    #@2a
    if-eqz v0, :cond_7f

    #@2c
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsPatternEffectEnabled:Z

    #@2e
    if-eqz v0, :cond_7f

    #@30
    .line 2009
    const-string v0, "lock_screen_pattern_effect"

    #@32
    invoke-static {v7, v0, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@35
    move-result v8

    #@36
    .line 2010
    .local v8, effectType:I
    if-eqz p1, :cond_7a

    #@38
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternLockEffectSoundId:[I

    #@3a
    aget v1, v0, v8

    #@3c
    .line 2030
    .end local v8           #effectType:I
    .local v1, whichSound:I
    :goto_3c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@3e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundStreamId:I

    #@40
    invoke-virtual {v0, v2}, Landroid/media/SoundPool;->stop(I)V

    #@43
    .line 2032
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@45
    if-nez v0, :cond_5f

    #@47
    .line 2033
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@49
    const-string v2, "audio"

    #@4b
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4e
    move-result-object v0

    #@4f
    check-cast v0, Landroid/media/AudioManager;

    #@51
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@53
    .line 2034
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@55
    if-eqz v0, :cond_8

    #@57
    .line 2035
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@59
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterStreamType()I

    #@5c
    move-result v0

    #@5d
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mMasterStreamType:I

    #@5f
    .line 2038
    :cond_5f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mAudioManager:Landroid/media/AudioManager;

    #@61
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mMasterStreamType:I

    #@63
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->isStreamMute(I)Z

    #@66
    move-result v0

    #@67
    if-nez v0, :cond_8

    #@69
    .line 2040
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSounds:Landroid/media/SoundPool;

    #@6b
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundVolume:F

    #@6d
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundVolume:F

    #@6f
    const/high16 v6, 0x3f80

    #@71
    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    #@74
    move-result v0

    #@75
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundStreamId:I

    #@77
    goto :goto_8

    #@78
    .end local v1           #whichSound:I
    :cond_78
    move v0, v5

    #@79
    .line 2000
    goto :goto_1e

    #@7a
    .line 2010
    .restart local v8       #effectType:I
    :cond_7a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPatternUnlockEffectSoundId:[I

    #@7c
    aget v1, v0, v8

    #@7e
    goto :goto_3c

    #@7f
    .line 2013
    .end local v8           #effectType:I
    :cond_7f
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsLockEffectEnabled:Z

    #@81
    if-nez v0, :cond_8b

    #@83
    .line 2014
    if-eqz p1, :cond_88

    #@85
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockSoundId:I

    #@87
    .restart local v1       #whichSound:I
    :goto_87
    goto :goto_3c

    #@88
    .end local v1           #whichSound:I
    :cond_88
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockSoundId:I

    #@8a
    goto :goto_87

    #@8b
    .line 2018
    :cond_8b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@8e
    move-result v0

    #@8f
    if-nez v0, :cond_9f

    #@91
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@93
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@96
    move-result-object v0

    #@97
    const-string v2, "lockscreen_type_password_unspecified"

    #@99
    invoke-static {v0, v2, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@9c
    move-result v0

    #@9d
    if-eqz v0, :cond_a7

    #@9f
    .line 2021
    :cond_9f
    const/4 v8, 0x0

    #@a0
    .line 2026
    .restart local v8       #effectType:I
    :goto_a0
    if-eqz p1, :cond_ae

    #@a2
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockEffectSoundId:[I

    #@a4
    aget v1, v0, v8

    #@a6
    .restart local v1       #whichSound:I
    :goto_a6
    goto :goto_3c

    #@a7
    .line 2023
    .end local v1           #whichSound:I
    .end local v8           #effectType:I
    :cond_a7
    const-string v0, "lock_screen_lock_effect"

    #@a9
    invoke-static {v7, v0, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@ac
    move-result v8

    #@ad
    .restart local v8       #effectType:I
    goto :goto_a0

    #@ae
    .line 2026
    :cond_ae
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUnlockEffectSoundId:[I

    #@b0
    aget v1, v0, v8

    #@b2
    goto :goto_a6
.end method

.method private resetStateLocked(Landroid/os/Bundle;)V
    .registers 5
    .parameter "options"

    #@0
    .prologue
    .line 1415
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "resetStateLocked"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1416
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v2, 0x4

    #@a
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1417
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 1418
    return-void
.end method

.method private resetStateWithKeepingAwakeLocked(Landroid/os/Bundle;)V
    .registers 7
    .parameter "options"

    #@0
    .prologue
    .line 2455
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "resetStateWithKeepingAwakeLocked"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2457
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@c
    .line 2458
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v2, 0x4

    #@f
    const/4 v3, 0x1

    #@10
    const/4 v4, 0x0

    #@11
    invoke-virtual {v1, v2, v3, v4, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 2459
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@17
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 2460
    return-void
.end method

.method private sendUserPresentBroadcast()V
    .registers 4

    #@0
    .prologue
    .line 1960
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2
    instance-of v1, v1, Landroid/app/Activity;

    #@4
    if-nez v1, :cond_18

    #@6
    .line 1961
    new-instance v0, Landroid/os/UserHandle;

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@d
    move-result v1

    #@e
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@11
    .line 1962
    .local v0, currentUser:Landroid/os/UserHandle;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@13
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserPresentIntent:Landroid/content/Intent;

    #@15
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@18
    .line 1964
    .end local v0           #currentUser:Landroid/os/UserHandle;
    :cond_18
    return-void
.end method

.method private setLockTimerStateLocked(IZ)V
    .registers 7
    .parameter "state"
    .parameter "asyncWithUIThread"

    #@0
    .prologue
    .line 2444
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@5
    move-result v0

    #@6
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@8
    if-eq v0, v1, :cond_1a

    #@a
    .line 2445
    if-eqz p2, :cond_1b

    #@c
    .line 2446
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    const/16 v2, 0x15

    #@12
    const/4 v3, 0x0

    #@13
    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 2451
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 2448
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1d
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setLockTimerState(I)V

    #@20
    goto :goto_1a
.end method

.method private showLocked(Landroid/os/Bundle;)V
    .registers 5
    .parameter "options"

    #@0
    .prologue
    .line 1477
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "showLocked"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1479
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@c
    .line 1480
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v2, 0x2

    #@f
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    .line 1481
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@15
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 1484
    const/4 v1, 0x1

    #@19
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsShowingKeyguardRequestedButNotHandledYet:Z

    #@1b
    .line 1489
    return-void
.end method

.method private startKTFOTA()V
    .registers 4

    #@0
    .prologue
    .line 1689
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "[OTA] startKTFOTA"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1693
    new-instance v0, Landroid/content/Intent;

    #@9
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@c
    .line 1694
    .local v0, KTota:Landroid/content/Intent;
    const-string v1, "com.lge.ota"

    #@e
    const-string v2, "com.lge.ota.KTNoUSIMActivityForLockScreen"

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 1695
    const/high16 v1, 0x1000

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@18
    .line 1696
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@1d
    .line 1697
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_26

    #@23
    .line 1698
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@26
    .line 1700
    :cond_26
    return-void
.end method

.method private startLGUOTA()V
    .registers 4

    #@0
    .prologue
    .line 1704
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "[OTA] startLGUOTA"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1708
    new-instance v0, Landroid/content/Intent;

    #@9
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@c
    .line 1709
    .local v0, LGTota:Landroid/content/Intent;
    const-string v1, "com.lge.ota"

    #@e
    const-string v2, "com.lge.ota.LGTNoUSIMActivityForLockScreen"

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 1710
    const/high16 v1, 0x1000

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@18
    .line 1711
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@1d
    .line 1712
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_26

    #@23
    .line 1713
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@26
    .line 1715
    :cond_26
    return-void
.end method

.method private startSKTOTA()V
    .registers 5

    #@0
    .prologue
    .line 1674
    const-string v1, "KeyguardViewMediator"

    #@2
    const-string v2, "[OTA] startSKTOTA"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1678
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v1

    #@d
    const-string v2, "skt_ota_usim_download"

    #@f
    const/4 v3, 0x0

    #@10
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@13
    .line 1681
    new-instance v0, Landroid/content/Intent;

    #@15
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@18
    .line 1682
    .local v0, otaIntent:Landroid/content/Intent;
    const-string v1, "com.lge.ota"

    #@1a
    const-string v2, "com.lge.ota.SKTUsimDownloadActivity"

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 1683
    const/high16 v1, 0x1000

    #@21
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@24
    .line 1684
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@29
    .line 1685
    return-void
.end method

.method private verifyUnlockLocked()V
    .registers 3

    #@0
    .prologue
    .line 1425
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "verifyUnlockLocked"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1426
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x5

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@d
    .line 1427
    return-void
.end method

.method private wakeWhenReady(I)V
    .registers 6
    .parameter "keyCode"

    #@0
    .prologue
    .line 1466
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWakeAndHandOff:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 1468
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@7
    const/16 v2, 0x8

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1469
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 1470
    return-void
.end method


# virtual methods
.method public clearLockPattern()V
    .registers 3

    #@0
    .prologue
    .line 2264
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "clearLockPattern message is sent"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2266
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0xf

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e
    .line 2267
    return-void
.end method

.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 1404
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@6
    if-nez v0, :cond_d

    #@8
    .line 1405
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->dismiss()V

    #@d
    .line 1407
    :cond_d
    return-void
.end method

.method public doKeyguardTimeout(Landroid/os/Bundle;)V
    .registers 5
    .parameter "options"

    #@0
    .prologue
    const/16 v2, 0xd

    #@2
    .line 1286
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 1287
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 1288
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@f
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 1289
    return-void
.end method

.method public handleShowAssistant()V
    .registers 2

    #@0
    .prologue
    .line 2439
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->showAssistant()V

    #@5
    .line 2440
    return-void
.end method

.method public hideSKTLocked()V
    .registers 3

    #@0
    .prologue
    .line 2363
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "[SKT Lock&Wipe] hideSKTLocked()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2364
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardViewManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->hideSKTLocked()V

    #@c
    .line 2365
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->hideLocked()V

    #@f
    .line 2366
    return-void
.end method

.method public isDisabledUnlock()Z
    .registers 2

    #@0
    .prologue
    .line 706
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDisableExpandStatusBar:Z

    #@2
    return v0
.end method

.method public isDismissable()Z
    .registers 2

    #@0
    .prologue
    .line 2430
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@2
    if-nez v0, :cond_a

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isInputRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 1297
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2
    if-nez v0, :cond_14

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@6
    if-nez v0, :cond_14

    #@8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_14

    #@10
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mQuickCoverWindowShowing:Z

    #@12
    if-eqz v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method isLockedOrMissing(Lcom/android/internal/telephony/IccCardConstants$State;)Z
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1393
    const-string v3, "keyguard.no_require_sim"

    #@4
    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_27

    #@a
    move v0, v1

    #@b
    .line 1395
    .local v0, requireSim:Z
    :goto_b
    const-string v3, "KR"

    #@d
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->country:Ljava/lang/String;

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_29

    #@15
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->isPinLocked()Z

    #@18
    move-result v3

    #@19
    if-nez v3, :cond_25

    #@1b
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1d
    if-eq p1, v3, :cond_25

    #@1f
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@21
    if-ne p1, v3, :cond_26

    #@23
    if-eqz v0, :cond_26

    #@25
    :cond_25
    move v2, v1

    #@26
    :cond_26
    :goto_26
    return v2

    #@27
    .end local v0           #requireSim:Z
    :cond_27
    move v0, v2

    #@28
    .line 1393
    goto :goto_b

    #@29
    .line 1395
    .restart local v0       #requireSim:Z
    :cond_29
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->isPinLocked()Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_39

    #@2f
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@31
    if-eq p1, v3, :cond_37

    #@33
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@35
    if-ne p1, v3, :cond_26

    #@37
    :cond_37
    if-eqz v0, :cond_26

    #@39
    :cond_39
    move v2, v1

    #@3a
    goto :goto_26
.end method

.method public isLockoutMode()Z
    .registers 2

    #@0
    .prologue
    .line 711
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockoutMode:Z

    #@2
    return v0
.end method

.method public isNavigationHidden()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2522
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isNaviHide:Z

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 2525
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isSKTGlobalUSIMDownloadRequires()Z
    .registers 2

    #@0
    .prologue
    .line 1727
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isSKTNoIMSIUSIMDownloadRequires()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1744
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimType()I

    #@6
    move-result v1

    #@7
    .line 1745
    .local v1, usimType:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@9
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimIsEmpty()I

    #@c
    move-result v0

    #@d
    .line 1747
    .local v0, isUsimEmpty:I
    if-ne v1, v2, :cond_13

    #@f
    const/4 v3, 0x2

    #@10
    if-ne v0, v3, :cond_13

    #@12
    .line 1752
    :goto_12
    return v2

    #@13
    :cond_13
    const/4 v2, 0x0

    #@14
    goto :goto_12
.end method

.method public isSecure()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1503
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@6
    move-result v0

    #@7
    .line 1504
    .local v0, isLockTypeSecure:Z
    if-eqz v0, :cond_33

    #@9
    .line 1505
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@b
    if-nez v3, :cond_12

    #@d
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mIsShowingKeyguardRequestedButNotHandledYet:Z

    #@f
    if-nez v3, :cond_12

    #@11
    .line 1519
    :cond_11
    :goto_11
    return v2

    #@12
    .line 1508
    :cond_12
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@14
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@1b
    move-result v1

    #@1c
    .line 1510
    .local v1, lockTimerState:I
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@1e
    if-eq v1, v3, :cond_11

    #@20
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@22
    if-eq v1, v3, :cond_11

    #@24
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@26
    if-eq v1, v3, :cond_11

    #@28
    .line 1515
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2a
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimPinSecure()Z

    #@31
    move-result v2

    #@32
    goto :goto_11

    #@33
    .line 1519
    .end local v1           #lockTimerState:I
    :cond_33
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@35
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimPinSecure()Z

    #@3c
    move-result v2

    #@3d
    goto :goto_11
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 1248
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mQuickCoverWindowShowing:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isShowingAndNotHidden()Z
    .registers 2

    #@0
    .prologue
    .line 1255
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mQuickCoverWindowShowing:Z

    #@6
    if-eqz v0, :cond_e

    #@8
    :cond_8
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHidden:Z

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public keyguardDone(ZZ)V
    .registers 9
    .parameter "authenticated"
    .parameter "wakeup"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1822
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@4
    .line 1823
    monitor-enter p0

    #@5
    .line 1824
    const v3, 0x11170

    #@8
    const/4 v4, 0x2

    #@9
    :try_start_9
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(II)I

    #@c
    .line 1825
    const-string v3, "KeyguardViewMediator"

    #@e
    new-instance v4, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v5, "keyguardDone("

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, ")"

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1826
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2c
    const/16 v4, 0x9

    #@2e
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v0

    #@32
    .line 1827
    .local v0, msg:Landroid/os/Message;
    if-eqz p2, :cond_5e

    #@34
    :goto_34
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@36
    .line 1828
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@38
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3b
    .line 1830
    if-eqz p1, :cond_48

    #@3d
    .line 1831
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3f
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->clearFailedUnlockAttempts()V

    #@42
    .line 1832
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@44
    const/4 v2, 0x0

    #@45
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setInputHalfFailedAttempts(Z)V

    #@48
    .line 1835
    :cond_48
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@4a
    if-eqz v1, :cond_5c

    #@4c
    .line 1836
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@4e
    invoke-interface {v1, p1}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@51
    .line 1837
    const/4 v1, 0x0

    #@52
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@54
    .line 1839
    if-eqz p1, :cond_5c

    #@56
    .line 1842
    const/4 v1, 0x1

    #@57
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@59
    .line 1843
    const/4 v1, 0x0

    #@5a
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@5c
    .line 1846
    :cond_5c
    monitor-exit p0

    #@5d
    .line 1847
    return-void

    #@5e
    :cond_5e
    move v1, v2

    #@5f
    .line 1827
    goto :goto_34

    #@60
    .line 1846
    .end local v0           #msg:Landroid/os/Message;
    :catchall_60
    move-exception v1

    #@61
    monitor-exit p0
    :try_end_62
    .catchall {:try_start_9 .. :try_end_62} :catchall_60

    #@62
    throw v1
.end method

.method public onDreamingStarted()V
    .registers 3

    #@0
    .prologue
    .line 1128
    monitor-enter p0

    #@1
    .line 1129
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@3
    if-eqz v0, :cond_30

    #@5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_30

    #@d
    .line 1132
    const-string v0, "KeyguardViewMediator"

    #@f
    const-string v1, "onDreamingStarted()"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 1133
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@16
    if-eqz v0, :cond_2c

    #@18
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@1d
    move-result v0

    #@1e
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@20
    if-eq v0, v1, :cond_30

    #@22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@24
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@27
    move-result v0

    #@28
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@2a
    if-eq v0, v1, :cond_30

    #@2c
    .line 1136
    :cond_2c
    const/4 v0, 0x3

    #@2d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLaterLocked(I)V

    #@30
    .line 1140
    :cond_30
    monitor-exit p0

    #@31
    .line 1141
    return-void

    #@32
    .line 1140
    :catchall_32
    move-exception v0

    #@33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method public onDreamingStopped()V
    .registers 3

    #@0
    .prologue
    .line 1147
    monitor-enter p0

    #@1
    .line 1148
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@3
    if-eqz v0, :cond_1c

    #@5
    .line 1149
    const-string v0, "KeyguardViewMediator"

    #@7
    const-string v1, "onDreamingStopped()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1151
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@e
    if-nez v0, :cond_19

    #@10
    .line 1152
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSuppressNextLockSound:Z

    #@13
    .line 1153
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@15
    const/4 v1, 0x1

    #@16
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@19
    .line 1156
    :cond_19
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->cancelDoKeyguardLaterLocked()V

    #@1c
    .line 1158
    :cond_1c
    monitor-exit p0

    #@1d
    .line 1159
    return-void

    #@1e
    .line 1158
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public onScreenTurnedOff(I)V
    .registers 6
    .parameter "why"

    #@0
    .prologue
    .line 943
    monitor-enter p0

    #@1
    .line 944
    const/4 v1, 0x0

    #@2
    :try_start_2
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@4
    .line 945
    const-string v1, "KeyguardViewMediator"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "onScreenTurnedOff("

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ")"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 947
    const/4 v1, 0x0

    #@23
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mKeyguardDonePending:Z

    #@25
    .line 948
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->notifyScreenOffLocked()V

    #@28
    .line 956
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2a
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getPowerButtonInstantlyLocks()Z

    #@2d
    move-result v0

    #@2e
    .line 959
    .local v0, lockImmediately:Z
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@30
    if-eqz v1, :cond_4b

    #@32
    .line 960
    const-string v1, "KeyguardViewMediator"

    #@34
    const-string v2, "pending exit secure callback cancelled"

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 961
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@3b
    const/4 v2, 0x0

    #@3c
    invoke-interface {v1, v2}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@3f
    .line 962
    const/4 v1, 0x0

    #@40
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@42
    .line 963
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@44
    if-nez v1, :cond_49

    #@46
    .line 964
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->hideLocked()V

    #@49
    .line 979
    :cond_49
    :goto_49
    monitor-exit p0

    #@4a
    .line 980
    return-void

    #@4b
    .line 966
    :cond_4b
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@4d
    if-eqz v1, :cond_6b

    #@4f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@51
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@54
    move-result v1

    #@55
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@57
    if-eq v1, v2, :cond_63

    #@59
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@5b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@5e
    move-result v1

    #@5f
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@61
    if-ne v1, v2, :cond_6b

    #@63
    .line 970
    :cond_63
    const/4 v1, 0x0

    #@64
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateLocked(Landroid/os/Bundle;)V

    #@67
    goto :goto_49

    #@68
    .line 979
    .end local v0           #lockImmediately:Z
    :catchall_68
    move-exception v1

    #@69
    monitor-exit p0
    :try_end_6a
    .catchall {:try_start_2 .. :try_end_6a} :catchall_68

    #@6a
    throw v1

    #@6b
    .line 971
    .restart local v0       #lockImmediately:Z
    :cond_6b
    :try_start_6b
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@6d
    if-nez v1, :cond_77

    #@6f
    const/4 v1, 0x3

    #@70
    if-eq p1, v1, :cond_77

    #@72
    const/4 v1, 0x2

    #@73
    if-ne p1, v1, :cond_7b

    #@75
    if-nez v0, :cond_7b

    #@77
    .line 973
    :cond_77
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLaterLocked(I)V

    #@7a
    goto :goto_49

    #@7b
    .line 974
    :cond_7b
    const/4 v1, 0x4

    #@7c
    if-eq p1, v1, :cond_49

    #@7e
    .line 977
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V
    :try_end_81
    .catchall {:try_start_6b .. :try_end_81} :catchall_68

    #@81
    goto :goto_49
.end method

.method public onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V
    .registers 5
    .parameter "showListener"

    #@0
    .prologue
    .line 1102
    monitor-enter p0

    #@1
    .line 1103
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mScreenOn:Z

    #@4
    .line 1104
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->cancelDoKeyguardLaterLocked()V

    #@7
    .line 1105
    const-string v0, "KeyguardViewMediator"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "onScreenTurnedOn, seq = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mDelayedShowingSequence:I

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1106
    if-eqz p1, :cond_26

    #@23
    .line 1107
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->notifyScreenOnLocked(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@26
    .line 1109
    :cond_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_2 .. :try_end_27} :catchall_2b

    #@27
    .line 1110
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->maybeSendUserPresentBroadcast()V

    #@2a
    .line 1111
    return-void

    #@2b
    .line 1109
    :catchall_2b
    move-exception v0

    #@2c
    :try_start_2c
    monitor-exit p0
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method

.method public onSystemReady()V
    .registers 3

    #@0
    .prologue
    .line 904
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "search"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/SearchManager;

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSearchManager:Landroid/app/SearchManager;

    #@c
    .line 905
    monitor-enter p0

    #@d
    .line 906
    :try_start_d
    const-string v0, "KeyguardViewMediator"

    #@f
    const-string v1, "onSystemReady"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 907
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mSystemReady:Z

    #@17
    .line 908
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@19
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@1e
    .line 911
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@20
    const/4 v1, 0x1

    #@21
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setLockTimerStateLocked(IZ)V

    #@24
    .line 921
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_49

    #@2c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2e
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakInstalled()Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_49

    #@34
    .line 923
    const-string v0, "KeyguardViewMediator"

    #@36
    const-string v1, "suppressing biometric unlock during boot"

    #@38
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 924
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3d
    const/4 v1, 0x0

    #@3e
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@41
    .line 929
    :goto_41
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->doKeyguardLocked()V

    #@44
    .line 930
    monitor-exit p0
    :try_end_45
    .catchall {:try_start_d .. :try_end_45} :catchall_50

    #@45
    .line 933
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->maybeSendUserPresentBroadcast()V

    #@48
    .line 934
    return-void

    #@49
    .line 926
    :cond_49
    :try_start_49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@4b
    const/4 v1, 0x1

    #@4c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@4f
    goto :goto_41

    #@50
    .line 930
    :catchall_50
    move-exception v0

    #@51
    monitor-exit p0
    :try_end_52
    .catchall {:try_start_49 .. :try_end_52} :catchall_50

    #@52
    throw v0
.end method

.method public onWakeKeyWhenKeyguardShowingTq(I)V
    .registers 5
    .parameter "keyCode"

    #@0
    .prologue
    .line 1794
    const-string v0, "KeyguardViewMediator"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onWakeKeyWhenKeyguardShowing("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1799
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeWhenReady(I)V

    #@21
    .line 1800
    return-void
.end method

.method public onWakeMotionWhenKeyguardShowingTq()V
    .registers 3

    #@0
    .prologue
    .line 1813
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "onWakeMotionWhenKeyguardShowing()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1818
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeWhenReady(I)V

    #@b
    .line 1819
    return-void
.end method

.method public setCurrentUser(I)V
    .registers 3
    .parameter "newUserId"

    #@0
    .prologue
    .line 1533
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setCurrentUser(I)V

    #@5
    .line 1534
    return-void
.end method

.method public setHidden(Z)V
    .registers 8
    .parameter "isHidden"

    #@0
    .prologue
    const/16 v5, 0xc

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 1263
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6
    if-nez p1, :cond_1f

    #@8
    move v1, v2

    #@9
    :goto_9
    invoke-virtual {v4, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->sendKeyguardVisibilityChanged(Z)V

    #@c
    .line 1264
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@11
    .line 1265
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@13
    if-eqz p1, :cond_21

    #@15
    :goto_15
    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 1266
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@1b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1e
    .line 1267
    return-void

    #@1f
    .end local v0           #msg:Landroid/os/Message;
    :cond_1f
    move v1, v3

    #@20
    .line 1263
    goto :goto_9

    #@21
    :cond_21
    move v2, v3

    #@22
    .line 1265
    goto :goto_15
.end method

.method public setKeyguardEnabled(Z)V
    .registers 7
    .parameter "enabled"

    #@0
    .prologue
    .line 1167
    monitor-enter p0

    #@1
    .line 1168
    :try_start_1
    const-string v1, "KeyguardViewMediator"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "setKeyguardEnabled("

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    const-string v3, ")"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1170
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@21
    .line 1172
    if-nez p1, :cond_46

    #@23
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowing:Z

    #@25
    if-eqz v1, :cond_46

    #@27
    .line 1173
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@29
    if-eqz v1, :cond_34

    #@2b
    .line 1174
    const-string v1, "KeyguardViewMediator"

    #@2d
    const-string v2, "in process of verifyUnlock request, ignoring"

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1177
    monitor-exit p0

    #@33
    .line 1216
    :goto_33
    return-void

    #@34
    .line 1181
    :cond_34
    const-string v1, "KeyguardViewMediator"

    #@36
    const-string v2, "remembering to reshow, hiding keyguard, disabling status bar expansion"

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1183
    const/4 v1, 0x1

    #@3c
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@3e
    .line 1184
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->hideLocked()V

    #@41
    .line 1215
    :cond_41
    :goto_41
    monitor-exit p0

    #@42
    goto :goto_33

    #@43
    :catchall_43
    move-exception v1

    #@44
    monitor-exit p0
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_43

    #@45
    throw v1

    #@46
    .line 1185
    :cond_46
    if-eqz p1, :cond_41

    #@48
    :try_start_48
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@4a
    if-eqz v1, :cond_41

    #@4c
    .line 1187
    const-string v1, "KeyguardViewMediator"

    #@4e
    const-string v2, "previously hidden, reshowing, reenabling status bar expansion"

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 1189
    const/4 v1, 0x0

    #@54
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mNeedToReshowWhenReenabled:Z

    #@56
    .line 1191
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@58
    if-eqz v1, :cond_6f

    #@5a
    .line 1192
    const-string v1, "KeyguardViewMediator"

    #@5c
    const-string v2, "onKeyguardExitResult(false), resetting"

    #@5e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 1193
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@63
    const/4 v2, 0x0

    #@64
    invoke-interface {v1, v2}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@67
    .line 1194
    const/4 v1, 0x0

    #@68
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@6a
    .line 1195
    const/4 v1, 0x0

    #@6b
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->resetStateLocked(Landroid/os/Bundle;)V

    #@6e
    goto :goto_41

    #@6f
    .line 1197
    :cond_6f
    const/4 v1, 0x0

    #@70
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showLocked(Landroid/os/Bundle;)V

    #@73
    .line 1202
    const/4 v1, 0x1

    #@74
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z

    #@76
    .line 1203
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@78
    const/16 v2, 0xa

    #@7a
    const-wide/16 v3, 0x7d0

    #@7c
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@7f
    .line 1204
    const-string v1, "KeyguardViewMediator"

    #@81
    const-string v2, "waiting until mWaitingUntilKeyguardVisible is false"

    #@83
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1205
    :goto_86
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mWaitingUntilKeyguardVisible:Z
    :try_end_88
    .catchall {:try_start_48 .. :try_end_88} :catchall_43

    #@88
    if-eqz v1, :cond_97

    #@8a
    .line 1207
    :try_start_8a
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8d
    .catchall {:try_start_8a .. :try_end_8d} :catchall_43
    .catch Ljava/lang/InterruptedException; {:try_start_8a .. :try_end_8d} :catch_8e

    #@8d
    goto :goto_86

    #@8e
    .line 1208
    :catch_8e
    move-exception v0

    #@8f
    .line 1209
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_8f
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@96
    goto :goto_86

    #@97
    .line 1212
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_97
    const-string v1, "KeyguardViewMediator"

    #@99
    const-string v2, "done waiting for mWaitingUntilKeyguardVisible"

    #@9b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9e
    .catchall {:try_start_8f .. :try_end_9e} :catchall_43

    #@9e
    goto :goto_41
.end method

.method public setOMADM()V
    .registers 3

    #@0
    .prologue
    .line 2287
    const-string v0, "KeyguardViewMediator"

    #@2
    const-string v1, "OMADM message is sent"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2288
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x10

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e
    .line 2289
    return-void
.end method

.method public showAssistant()V
    .registers 4

    #@0
    .prologue
    .line 2434
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xe

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 2435
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 2436
    return-void
.end method

.method public showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    .line 2323
    const-string v1, "KeyguardViewMediator"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[SKT Lock&Wipe] showSKTLocked("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ")"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 2324
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mEnableUserUnlock:Z

    #@34
    .line 2325
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mStrPassword:Ljava/lang/String;

    #@36
    .line 2326
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUserMsg:Ljava/lang/String;

    #@38
    .line 2328
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mShowKeyguardWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3a
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@3d
    .line 2329
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@3f
    const/16 v2, 0x11

    #@41
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@44
    move-result-object v0

    #@45
    .line 2330
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mHandler:Landroid/os/Handler;

    #@47
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@4a
    .line 2331
    return-void
.end method

.method public userActivity()V
    .registers 3

    #@0
    .prologue
    .line 695
    const-wide/16 v0, 0x2710

    #@2
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity(J)V

    #@5
    .line 696
    return-void
.end method

.method public userActivity(J)V
    .registers 7
    .parameter "holdMs"

    #@0
    .prologue
    .line 701
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v1

    #@6
    const/4 v3, 0x0

    #@7
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@a
    .line 702
    return-void
.end method

.method public verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 1222
    monitor-enter p0

    #@1
    .line 1223
    :try_start_1
    const-string v0, "KeyguardViewMediator"

    #@3
    const-string v1, "verifyUnlock"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1224
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_1d

    #@10
    .line 1226
    const-string v0, "KeyguardViewMediator"

    #@12
    const-string v1, "ignoring because device isn\'t provisioned"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1227
    const/4 v0, 0x0

    #@18
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@1b
    .line 1241
    :goto_1b
    monitor-exit p0

    #@1c
    .line 1242
    return-void

    #@1d
    .line 1228
    :cond_1d
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExternallyEnabled:Z

    #@1f
    if-eqz v0, :cond_30

    #@21
    .line 1232
    const-string v0, "KeyguardViewMediator"

    #@23
    const-string v1, "verifyUnlock called when not externally disabled"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1233
    const/4 v0, 0x0

    #@29
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@2c
    goto :goto_1b

    #@2d
    .line 1241
    :catchall_2d
    move-exception v0

    #@2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_1 .. :try_end_2f} :catchall_2d

    #@2f
    throw v0

    #@30
    .line 1234
    :cond_30
    :try_start_30
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@32
    if-eqz v0, :cond_39

    #@34
    .line 1236
    const/4 v0, 0x0

    #@35
    invoke-interface {p1, v0}, Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;->onKeyguardExitResult(Z)V

    #@38
    goto :goto_1b

    #@39
    .line 1238
    :cond_39
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mExitSecureCallback:Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;

    #@3b
    .line 1239
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->verifyUnlockLocked()V
    :try_end_3e
    .catchall {:try_start_30 .. :try_end_3e} :catchall_2d

    #@3e
    goto :goto_1b
.end method

.method public wakeUp()V
    .registers 4

    #@0
    .prologue
    .line 691
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->mPM:Landroid/os/PowerManager;

    #@2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v1

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->wakeUp(J)V

    #@9
    .line 692
    return-void
.end method
