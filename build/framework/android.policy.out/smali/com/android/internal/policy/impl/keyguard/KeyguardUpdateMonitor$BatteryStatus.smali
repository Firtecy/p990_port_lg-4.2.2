.class public Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BatteryStatus"
.end annotation


# instance fields
.field public final health:I

.field public final level:I

.field public final plugged:I

.field public final status:I

.field public final temperature:I


# direct methods
.method public constructor <init>(IIII)V
    .registers 6
    .parameter "status"
    .parameter "level"
    .parameter "plugged"
    .parameter "health"

    #@0
    .prologue
    .line 595
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 596
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@5
    .line 597
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@7
    .line 598
    iput p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@9
    .line 599
    iput p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->health:I

    #@b
    .line 601
    const/4 v0, 0x0

    #@c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->temperature:I

    #@e
    .line 602
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "status"
    .parameter "level"
    .parameter "plugged"
    .parameter "health"
    .parameter "temperature"

    #@0
    .prologue
    .line 605
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 606
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@5
    .line 607
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@7
    .line 608
    iput p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@9
    .line 609
    iput p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->health:I

    #@b
    .line 610
    iput p5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->temperature:I

    #@d
    .line 611
    return-void
.end method


# virtual methods
.method public isBatteryLow()Z
    .registers 3

    #@0
    .prologue
    .line 640
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@2
    const/4 v1, 0x4

    #@3
    if-ge v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isCharged()Z
    .registers 3

    #@0
    .prologue
    .line 632
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@2
    const/4 v1, 0x5

    #@3
    if-eq v0, v1, :cond_b

    #@5
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@7
    const/16 v1, 0x64

    #@9
    if-lt v0, v1, :cond_d

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public isPluggedIn()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 620
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@3
    if-eq v1, v0, :cond_f

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@7
    const/4 v2, 0x2

    #@8
    if-eq v1, v2, :cond_f

    #@a
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@c
    const/4 v2, 0x4

    #@d
    if-ne v1, v2, :cond_10

    #@f
    :cond_f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method
