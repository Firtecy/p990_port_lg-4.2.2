.class Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;
.super Ljava/lang/Object;
.source "KeyguardHostView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showHalfWrongPasswordDialog(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

.field final synthetic val$ad:Landroid/app/AlertDialog;

.field final synthetic val$entercode:Landroid/widget/EditText;

.field final synthetic val$mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Landroid/widget/EditText;Landroid/app/AlertDialog;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 2050
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$entercode:Landroid/widget/EditText;

    #@4
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$ad:Landroid/app/AlertDialog;

    #@6
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$mContext:Landroid/content/Context;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 2053
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$entercode:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v1, "life is good"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_29

    #@12
    .line 2054
    const-string v0, "KeyguardHostView"

    #@14
    const-string v1, "user inputs life is good correctly!!"

    #@16
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 2055
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$ad:Landroid/app/AlertDialog;

    #@1b
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@1e
    .line 2057
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$mContext:Landroid/content/Context;

    #@20
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@23
    move-result-object v0

    #@24
    const/4 v1, 0x1

    #@25
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setInputHalfFailedAttempts(Z)V

    #@28
    .line 2064
    :goto_28
    return-void

    #@29
    .line 2060
    :cond_29
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;->val$entercode:Landroid/widget/EditText;

    #@2b
    const-string v1, ""

    #@2d
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@30
    .line 2062
    const-string v0, "KeyguardHostView"

    #@32
    const-string v1, "mismatch with life is good!!"

    #@34
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_28
.end method
