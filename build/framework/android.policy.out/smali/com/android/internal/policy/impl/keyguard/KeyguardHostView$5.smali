.class Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;
.super Landroid/widget/RemoteViews$OnClickHandler;
.source "KeyguardHostView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 979
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@2
    invoke-direct {p0}, Landroid/widget/RemoteViews$OnClickHandler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z
    .registers 7
    .parameter "view"
    .parameter "pendingIntent"
    .parameter "fillInIntent"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 984
    invoke-virtual {p2}, Landroid/app/PendingIntent;->isActivity()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_32

    #@7
    .line 985
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@9
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;

    #@b
    invoke-direct {v2, p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)V

    #@e
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->setOnDismissAction(Lcom/android/internal/policy/impl/keyguard/OnDismissAction;)V

    #@11
    .line 1007
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@13
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->access$1900(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->isChallengeShowing()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_27

    #@1d
    .line 1008
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@1f
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->access$1900(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->showBouncer(Z)V

    #@26
    .line 1014
    :goto_26
    return v0

    #@27
    .line 1010
    :cond_27
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@29
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2c
    move-result-object v1

    #@2d
    const/4 v2, 0x0

    #@2e
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@31
    goto :goto_26

    #@32
    .line 1014
    :cond_32
    invoke-super {p0, p1, p2, p3}, Landroid/widget/RemoteViews$OnClickHandler;->onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    #@35
    move-result v0

    #@36
    goto :goto_26
.end method
