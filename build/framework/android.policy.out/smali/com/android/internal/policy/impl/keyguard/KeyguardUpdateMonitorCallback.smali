.class public Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitorCallback.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method initialize()V
    .registers 1

    #@0
    .prologue
    .line 129
    return-void
.end method

.method onAccessoryStateChanged(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 144
    return-void
.end method

.method onAlarmChanged()V
    .registers 1

    #@0
    .prologue
    .line 130
    return-void
.end method

.method public onBootCompleted()V
    .registers 1

    #@0
    .prologue
    .line 110
    return-void
.end method

.method public onClockVisibilityChanged()V
    .registers 1

    #@0
    .prologue
    .line 75
    return-void
.end method

.method public onDevicePolicyManagerStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 86
    return-void
.end method

.method public onDeviceProvisioned()V
    .registers 1

    #@0
    .prologue
    .line 80
    return-void
.end method

.method onHdmiPlugStateChanged(Z)V
    .registers 2
    .parameter "connected"

    #@0
    .prologue
    .line 141
    return-void
.end method

.method public onInstantlyLgWidgetStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 148
    return-void
.end method

.method public onKeyguardVisibilityChanged(Z)V
    .registers 2
    .parameter "showing"

    #@0
    .prologue
    .line 69
    return-void
.end method

.method public onPhoneStateChanged(I)V
    .registers 2
    .parameter "phoneState"

    #@0
    .prologue
    .line 63
    return-void
.end method

.method public onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 2
    .parameter "status"

    #@0
    .prologue
    .line 34
    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 48
    return-void
.end method

.method onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "plmn"
    .parameter "spn"
    .parameter "subscription"

    #@0
    .prologue
    .line 127
    return-void
.end method

.method public onRingerModeChanged(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 55
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 2
    .parameter "simState"

    #@0
    .prologue
    .line 97
    return-void
.end method

.method onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 3
    .parameter "simState"
    .parameter "subscription"

    #@0
    .prologue
    .line 117
    return-void
.end method

.method public onTimeChanged()V
    .registers 1

    #@0
    .prologue
    .line 39
    return-void
.end method

.method public onUserRemoved(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 102
    return-void
.end method

.method public onUserSwitched(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 91
    return-void
.end method
