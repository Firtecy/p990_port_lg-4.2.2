.class public Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;
.super Landroid/view/View$AccessibilityDelegate;
.source "ObscureSpeechDelegate.java"


# static fields
.field public static sAnnouncedHeadset:Z


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->sAnnouncedHeadset:Z

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    #@3
    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->mContentResolver:Landroid/content/ContentResolver;

    #@9
    .line 44
    const-string v0, "audio"

    #@b
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/media/AudioManager;

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->mAudioManager:Landroid/media/AudioManager;

    #@13
    .line 45
    return-void
.end method

.method private shouldObscureSpeech()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 88
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->mContentResolver:Landroid/content/ContentResolver;

    #@3
    const-string v2, "speak_password"

    #@5
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 99
    :cond_b
    :goto_b
    return v0

    #@c
    .line 94
    :cond_c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->mAudioManager:Landroid/media/AudioManager;

    #@e
    invoke-virtual {v1}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_b

    #@14
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->mAudioManager:Landroid/media/AudioManager;

    #@16
    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_b

    #@1c
    .line 99
    const/4 v0, 0x1

    #@1d
    goto :goto_b
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 5
    .parameter "host"
    .parameter "info"

    #@0
    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 77
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->shouldObscureSpeech()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_1b

    #@9
    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    .line 79
    .local v0, ctx:Landroid/content/Context;
    const/4 v1, 0x0

    #@e
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    #@11
    .line 80
    const v1, 0x1040509

    #@14
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    #@1b
    .line 83
    .end local v0           #ctx:Landroid/content/Context;
    :cond_1b
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "host"
    .parameter "event"

    #@0
    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 65
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@6
    move-result v0

    #@7
    const/16 v1, 0x4000

    #@9
    if-eq v0, v1, :cond_26

    #@b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->shouldObscureSpeech()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_26

    #@11
    .line 67
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@14
    move-result-object v0

    #@15
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@18
    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v0

    #@1c
    const v1, 0x1040509

    #@1f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@26
    .line 71
    :cond_26
    return-void
.end method

.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .registers 5
    .parameter "host"
    .parameter "eventType"

    #@0
    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    #@3
    .line 53
    const v0, 0x8000

    #@6
    if-ne p2, v0, :cond_23

    #@8
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->sAnnouncedHeadset:Z

    #@a
    if-nez v0, :cond_23

    #@c
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->shouldObscureSpeech()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_23

    #@12
    .line 55
    const/4 v0, 0x1

    #@13
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->sAnnouncedHeadset:Z

    #@15
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@18
    move-result-object v0

    #@19
    const v1, 0x1040508

    #@1c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@23
    .line 59
    :cond_23
    return-void
.end method
