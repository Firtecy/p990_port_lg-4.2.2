.class Lcom/android/internal/policy/impl/PhoneWindowManager$47;
.super Landroid/content/BroadcastReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8466
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 8469
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 8470
    .local v0, action:Ljava/lang/String;
    const-string v2, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_34

    #@e
    .line 8471
    const-string v2, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@10
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v1

    #@14
    .line 8472
    .local v1, currentState:I
    const/4 v2, 0x5

    #@15
    if-ne v1, v2, :cond_35

    #@17
    .line 8473
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@19
    invoke-static {v2, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1102(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@1c
    .line 8474
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1e
    iget-boolean v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@20
    if-eqz v2, :cond_34

    #@22
    .line 8475
    const-string v2, "WindowManager"

    #@24
    const-string v3, "cover closed - stop daydream"

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 8476
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2b
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@2d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@30
    move-result-wide v3

    #@31
    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(J)V

    #@34
    .line 8486
    .end local v1           #currentState:I
    :cond_34
    :goto_34
    return-void

    #@35
    .line 8478
    .restart local v1       #currentState:I
    :cond_35
    const/4 v2, 0x6

    #@36
    if-ne v1, v2, :cond_3e

    #@38
    .line 8479
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3a
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1102(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@3d
    goto :goto_34

    #@3e
    .line 8480
    :cond_3e
    const/4 v2, 0x2

    #@3f
    if-ne v1, v2, :cond_47

    #@41
    .line 8481
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@43
    invoke-static {v2, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$3802(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@46
    goto :goto_34

    #@47
    .line 8482
    :cond_47
    if-ne v1, v4, :cond_34

    #@49
    .line 8483
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4b
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$3802(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@4e
    goto :goto_34
.end method
