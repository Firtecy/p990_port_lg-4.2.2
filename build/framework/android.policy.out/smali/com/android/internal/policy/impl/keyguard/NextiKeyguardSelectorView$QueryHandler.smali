.class Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "NextiKeyguardSelectorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Landroid/content/ContentResolver;)V
    .registers 3
    .parameter
    .parameter "cr"

    #@0
    .prologue
    .line 598
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    .line 599
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    #@5
    .line 600
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .registers 8
    .parameter "token"
    .parameter "cookie"
    .parameter "cursor"

    #@0
    .prologue
    .line 607
    packed-switch p1, :pswitch_data_38

    #@3
    .line 622
    :cond_3
    :goto_3
    return-void

    #@4
    .line 610
    :pswitch_4
    if-eqz p3, :cond_3

    #@6
    .line 611
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$500()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_28

    #@c
    const-string v1, "SecuritySelectorView"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "NextiKeyguardSelectorView.QueryHandler.onQueryComplete(): call log query complete."

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    #@1c
    move-result v3

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 614
    :cond_28
    const/4 v0, -0x1

    #@29
    .line 615
    .local v0, missedcount:I
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    #@2c
    move-result v0

    #@2d
    .line 616
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    #@30
    .line 617
    if-ltz v0, :cond_3

    #@32
    .line 618
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@34
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$600(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V

    #@37
    goto :goto_3

    #@38
    .line 607
    :pswitch_data_38
    .packed-switch -0x1
        :pswitch_4
    .end packed-switch
.end method

.method protected onUpdateComplete(ILjava/lang/Object;I)V
    .registers 4
    .parameter "token"
    .parameter "cookie"
    .parameter "result"

    #@0
    .prologue
    .line 625
    return-void
.end method
