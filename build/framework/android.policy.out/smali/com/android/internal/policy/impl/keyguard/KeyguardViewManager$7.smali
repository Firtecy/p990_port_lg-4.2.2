.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.source "KeyguardViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1110
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onKeyguardVisibilityChanged(Z)V
    .registers 4
    .parameter "showing"

    #@0
    .prologue
    .line 1113
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@2
    if-eqz v0, :cond_6b

    #@4
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_6b

    #@a
    .line 1115
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@e
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@19
    .line 1117
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@1b
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1e
    move-result-object v0

    #@1f
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@21
    if-eq v0, v1, :cond_37

    #@23
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@25
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@28
    move-result-object v0

    #@29
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2b
    if-eq v0, v1, :cond_37

    #@2d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@32
    move-result-object v0

    #@33
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@35
    if-ne v0, v1, :cond_6c

    #@37
    .line 1121
    :cond_37
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, "onKeyguardVisibilityChanged(PIN/PUK)"

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1122
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@42
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@45
    move-result-object v0

    #@46
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@48
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getSystemUiVisibility()I

    #@4f
    move-result v1

    #@50
    and-int/lit8 v1, v1, -0x11

    #@52
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    #@55
    .line 1129
    :cond_55
    :goto_55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@57
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@5a
    move-result-object v0

    #@5b
    instance-of v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@5d
    if-eqz v0, :cond_6b

    #@5f
    .line 1130
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@61
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@64
    move-result-object v0

    #@65
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@67
    const/4 v1, 0x0

    #@68
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->setKeyguardIsVisible(ZZ)V

    #@6b
    .line 1133
    :cond_6b
    return-void

    #@6c
    .line 1124
    :cond_6c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@6f
    move-result-object v0

    #@70
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isScreenDecoTransparent()Z

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_55

    #@76
    .line 1125
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@79
    move-result-object v0

    #@7a
    const-string v1, "onKeyguardVisibilityChanged()"

    #@7c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 1126
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@81
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@84
    move-result-object v0

    #@85
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@87
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getSystemUiVisibility()I

    #@8e
    move-result v1

    #@8f
    or-int/lit8 v1, v1, 0x10

    #@91
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    #@94
    goto :goto_55
.end method
