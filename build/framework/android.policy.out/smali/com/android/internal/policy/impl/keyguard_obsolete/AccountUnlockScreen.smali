.class public Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;
.super Landroid/widget/RelativeLayout;
.source "AccountUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final AWAKE_POKE_MILLIS:I = 0x7530

.field private static final LOCK_PATTERN_CLASS:Ljava/lang/String; = "com.android.settings.ChooseLockGeneric"

.field private static final LOCK_PATTERN_PACKAGE:Ljava/lang/String; = "com.android.settings"


# instance fields
.field private mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private mCheckingDialog:Landroid/app/ProgressDialog;

.field private mInstructions:Landroid/widget/TextView;

.field private mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLogin:Landroid/widget/EditText;

.field private mOk:Landroid/widget/Button;

.field private mPassword:Landroid/widget/EditText;

.field private mTopHeader:Landroid/widget/TextView;

.field private mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 12
    .parameter "context"
    .parameter "configuration"
    .parameter "updateMonitor"
    .parameter "callback"
    .parameter "lockPatternUtils"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 88
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    #@4
    .line 89
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@6
    .line 90
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@8
    .line 92
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@b
    move-result-object v0

    #@c
    const v1, 0x1090062

    #@f
    invoke-virtual {v0, v1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@12
    .line 95
    const v0, 0x10202e1

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/widget/TextView;

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mTopHeader:Landroid/widget/TextView;

    #@1d
    .line 96
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mTopHeader:Landroid/widget/TextView;

    #@1f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_85

    #@27
    const v0, 0x1040337

    #@2a
    :goto_2a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    #@2d
    .line 100
    const v0, 0x10202e3

    #@30
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findViewById(I)Landroid/view/View;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/widget/TextView;

    #@36
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mInstructions:Landroid/widget/TextView;

    #@38
    .line 102
    const v0, 0x10202b1

    #@3b
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findViewById(I)Landroid/view/View;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Landroid/widget/EditText;

    #@41
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@43
    .line 103
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@45
    new-array v1, v5, [Landroid/text/InputFilter;

    #@47
    const/4 v2, 0x0

    #@48
    new-instance v3, Landroid/text/LoginFilter$UsernameFilterGeneric;

    #@4a
    invoke-direct {v3}, Landroid/text/LoginFilter$UsernameFilterGeneric;-><init>()V

    #@4d
    aput-object v3, v1, v2

    #@4f
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@52
    .line 104
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@54
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@57
    .line 106
    const v0, 0x10202b2

    #@5a
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findViewById(I)Landroid/view/View;

    #@5d
    move-result-object v0

    #@5e
    check-cast v0, Landroid/widget/EditText;

    #@60
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mPassword:Landroid/widget/EditText;

    #@62
    .line 107
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mPassword:Landroid/widget/EditText;

    #@64
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@67
    .line 109
    const v0, 0x10202b3

    #@6a
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findViewById(I)Landroid/view/View;

    #@6d
    move-result-object v0

    #@6e
    check-cast v0, Landroid/widget/Button;

    #@70
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mOk:Landroid/widget/Button;

    #@72
    .line 110
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mOk:Landroid/widget/Button;

    #@74
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@77
    .line 112
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@79
    .line 114
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@7b
    move-object v1, p0

    #@7c
    move-object v2, p3

    #@7d
    move-object v3, p5

    #@7e
    move-object v4, p4

    #@7f
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;-><init>(Landroid/view/View;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Z)V

    #@82
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@84
    .line 116
    return-void

    #@85
    .line 96
    :cond_85
    const v0, 0x1040336

    #@88
    goto :goto_2a
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mInstructions:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mPassword:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->postOnCheckPasswordResult(Z)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/app/Dialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->getProgressDialog()Landroid/app/Dialog;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method private asyncCheckPassword()V
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 269
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@3
    const/16 v4, 0x7530

    #@5
    invoke-interface {v0, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@8
    .line 270
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@a
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11
    move-result-object v6

    #@12
    .line 271
    .local v6, login:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mPassword:Landroid/widget/EditText;

    #@14
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v7

    #@1c
    .line 272
    .local v7, password:Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->findIntendedAccount(Ljava/lang/String;)Landroid/accounts/Account;

    #@1f
    move-result-object v1

    #@20
    .line 273
    .local v1, account:Landroid/accounts/Account;
    if-nez v1, :cond_27

    #@22
    .line 274
    const/4 v0, 0x0

    #@23
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->postOnCheckPasswordResult(Z)V

    #@26
    .line 303
    :goto_26
    return-void

    #@27
    .line 277
    :cond_27
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->getProgressDialog()Landroid/app/Dialog;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@2e
    .line 278
    new-instance v2, Landroid/os/Bundle;

    #@30
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@33
    .line 279
    .local v2, options:Landroid/os/Bundle;
    const-string v0, "password"

    #@35
    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 280
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mContext:Landroid/content/Context;

    #@3a
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@3d
    move-result-object v0

    #@3e
    new-instance v4, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;

    #@40
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;)V

    #@43
    move-object v5, v3

    #@44
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    #@47
    goto :goto_26
.end method

.method private findIntendedAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .registers 15
    .parameter "username"

    #@0
    .prologue
    const/16 v12, 0x40

    #@2
    .line 233
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@7
    move-result-object v10

    #@8
    const-string v11, "com.google"

    #@a
    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    #@d
    move-result-object v2

    #@e
    .line 239
    .local v2, accounts:[Landroid/accounts/Account;
    const/4 v4, 0x0

    #@f
    .line 240
    .local v4, bestAccount:Landroid/accounts/Account;
    const/4 v5, 0x0

    #@10
    .line 241
    .local v5, bestScore:I
    move-object v3, v2

    #@11
    .local v3, arr$:[Landroid/accounts/Account;
    array-length v8, v3

    #@12
    .local v8, len$:I
    const/4 v7, 0x0

    #@13
    .local v7, i$:I
    :goto_13
    if-ge v7, v8, :cond_5b

    #@15
    aget-object v0, v3, v7

    #@17
    .line 242
    .local v0, a:Landroid/accounts/Account;
    const/4 v9, 0x0

    #@18
    .line 243
    .local v9, score:I
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@1a
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v10

    #@1e
    if-eqz v10, :cond_28

    #@20
    .line 244
    const/4 v9, 0x4

    #@21
    .line 258
    :cond_21
    :goto_21
    if-le v9, v5, :cond_57

    #@23
    .line 259
    move-object v4, v0

    #@24
    .line 260
    move v5, v9

    #@25
    .line 241
    :cond_25
    :goto_25
    add-int/lit8 v7, v7, 0x1

    #@27
    goto :goto_13

    #@28
    .line 245
    :cond_28
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2d
    move-result v10

    #@2e
    if-eqz v10, :cond_32

    #@30
    .line 246
    const/4 v9, 0x3

    #@31
    goto :goto_21

    #@32
    .line 247
    :cond_32
    invoke-virtual {p1, v12}, Ljava/lang/String;->indexOf(I)I

    #@35
    move-result v10

    #@36
    if-gez v10, :cond_21

    #@38
    .line 248
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@3a
    invoke-virtual {v10, v12}, Ljava/lang/String;->indexOf(I)I

    #@3d
    move-result v6

    #@3e
    .line 249
    .local v6, i:I
    if-ltz v6, :cond_21

    #@40
    .line 250
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@42
    const/4 v11, 0x0

    #@43
    invoke-virtual {v10, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    .line 251
    .local v1, aUsername:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v10

    #@4b
    if-eqz v10, :cond_4f

    #@4d
    .line 252
    const/4 v9, 0x2

    #@4e
    goto :goto_21

    #@4f
    .line 253
    :cond_4f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@52
    move-result v10

    #@53
    if-eqz v10, :cond_21

    #@55
    .line 254
    const/4 v9, 0x1

    #@56
    goto :goto_21

    #@57
    .line 261
    .end local v1           #aUsername:Ljava/lang/String;
    .end local v6           #i:I
    :cond_57
    if-ne v9, v5, :cond_25

    #@59
    .line 262
    const/4 v4, 0x0

    #@5a
    goto :goto_25

    #@5b
    .line 265
    .end local v0           #a:Landroid/accounts/Account;
    .end local v9           #score:I
    :cond_5b
    return-object v4
.end method

.method private getProgressDialog()Landroid/app/Dialog;
    .registers 4

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@2
    if-nez v0, :cond_32

    #@4
    .line 307
    new-instance v0, Landroid/app/ProgressDialog;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@d
    .line 308
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mContext:Landroid/content/Context;

    #@11
    const v2, 0x104033e

    #@14
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@1b
    .line 310
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@1d
    const/4 v1, 0x1

    #@1e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@21
    .line 311
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@27
    .line 312
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@29
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@2c
    move-result-object v0

    #@2d
    const/16 v1, 0x7d9

    #@2f
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@32
    .line 315
    :cond_32
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@34
    return-object v0
.end method

.method private postOnCheckPasswordResult(Z)V
    .registers 4
    .parameter "success"

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;Z)V

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 200
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 119
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 122
    return-void
.end method

.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 156
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 157
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@7
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    #@a
    .line 159
    :cond_a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@c
    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@f
    .line 160
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@11
    .line 161
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@13
    .line 162
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@15
    .line 163
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 204
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_23

    #@6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x4

    #@b
    if-ne v0, v1, :cond_23

    #@d
    .line 206
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1c

    #@15
    .line 207
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@17
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToLockScreen()V

    #@1a
    .line 211
    :goto_1a
    const/4 v0, 0x1

    #@1b
    .line 213
    :goto_1b
    return v0

    #@1c
    .line 209
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->forgotPattern(Z)V

    #@22
    goto :goto_1a

    #@23
    .line 213
    :cond_23
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@26
    move-result v0

    #@27
    goto :goto_1b
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 137
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@5
    .line 168
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mOk:Landroid/widget/Button;

    #@7
    if-ne p1, v0, :cond_c

    #@9
    .line 169
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->asyncCheckPassword()V

    #@c
    .line 171
    :cond_c
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onPause()V

    #@5
    .line 143
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onResume()V
    .registers 3

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@2
    const-string v1, ""

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@7
    .line 149
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mPassword:Landroid/widget/EditText;

    #@9
    const-string v1, ""

    #@b
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@e
    .line 150
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mLogin:Landroid/widget/EditText;

    #@10
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    #@13
    .line 151
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onResume()V

    #@18
    .line 152
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    const/16 v1, 0x7530

    #@4
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@7
    .line 126
    return-void
.end method
