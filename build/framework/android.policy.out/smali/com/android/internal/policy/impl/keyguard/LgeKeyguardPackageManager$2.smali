.class Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;
.super Landroid/content/BroadcastReceiver;
.source "LgeKeyguardPackageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 125
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 127
    .local v0, action:Ljava/lang/String;
    const-string v3, "LgeKeyguardPackageManager"

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "received broadcast "

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 130
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 134
    .local v1, packageName:Ljava/lang/String;
    if-eqz v1, :cond_2e

    #@26
    const-string v3, "com.lge.lockscreen"

    #@28
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v3

    #@2c
    if-nez v3, :cond_2f

    #@2e
    .line 158
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 139
    :cond_2f
    const-string v3, "android.intent.extra.REPLACING"

    #@31
    const/4 v4, 0x0

    #@32
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@35
    move-result v2

    #@36
    .line 142
    .local v2, replacing:Z
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    #@38
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_6c

    #@3e
    .line 143
    if-eqz v2, :cond_56

    #@40
    .line 144
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@42
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@45
    move-result-object v3

    #@46
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@48
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@4b
    move-result-object v4

    #@4c
    const/16 v5, 0x66

    #@4e
    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@55
    goto :goto_2e

    #@56
    .line 147
    :cond_56
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@58
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@5b
    move-result-object v3

    #@5c
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@5e
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@61
    move-result-object v4

    #@62
    const/16 v5, 0x64

    #@64
    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@6b
    goto :goto_2e

    #@6c
    .line 150
    :cond_6c
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    #@6e
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v3

    #@72
    if-eqz v3, :cond_8c

    #@74
    .line 151
    if-nez v2, :cond_2e

    #@76
    .line 152
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@78
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@7b
    move-result-object v3

    #@7c
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;->this$0:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@7e
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;

    #@81
    move-result-object v4

    #@82
    const/16 v5, 0x65

    #@84
    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@8b
    goto :goto_2e

    #@8c
    .line 155
    :cond_8c
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    #@8e
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_2e

    #@94
    goto :goto_2e
.end method
