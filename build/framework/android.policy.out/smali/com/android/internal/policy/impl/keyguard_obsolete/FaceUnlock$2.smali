.class Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;
.super Lcom/android/internal/policy/IFaceLockCallback$Stub;
.source "FaceUnlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 504
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/IFaceLockCallback$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x5

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@a
    .line 521
    return-void
.end method

.method public pokeWakelock(I)V
    .registers 6
    .parameter "millis"

    #@0
    .prologue
    .line 549
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@5
    move-result-object v1

    #@6
    const/16 v2, 0x8

    #@8
    const/4 v3, -0x1

    #@9
    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 550
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@f
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 551
    return-void
.end method

.method public reportFailedAttempt()V
    .registers 3

    #@0
    .prologue
    .line 529
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x6

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@a
    .line 530
    return-void
.end method

.method public unlock()V
    .registers 3

    #@0
    .prologue
    .line 511
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x4

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@a
    .line 512
    return-void
.end method
