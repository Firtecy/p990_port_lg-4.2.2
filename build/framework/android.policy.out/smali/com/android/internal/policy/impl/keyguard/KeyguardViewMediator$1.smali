.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 394
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method onAccessoryStateChanged(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 563
    const-string v2, "KeyguardViewMediator"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "onAccessoryStateChanged: state = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, ", mShowing = "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1a
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@1d
    move-result v4

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 564
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2b
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_d7

    #@39
    .line 565
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@3b
    monitor-enter v3

    #@3c
    .line 566
    :try_start_3c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@3e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@41
    move-result-object v2

    #@42
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@49
    move-result v2

    #@4a
    if-eqz v2, :cond_ae

    #@4c
    .line 567
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@4e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@55
    move-result v1

    #@56
    .line 568
    .local v1, provisioned:Z
    if-nez v1, :cond_61

    #@58
    .line 569
    const-string v2, "KeyguardViewMediator"

    #@5a
    const-string v4, "onAccessoryStateChanged: provisioned is false"

    #@5c
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 570
    monitor-exit v3

    #@60
    .line 602
    .end local v1           #provisioned:Z
    :cond_60
    :goto_60
    return-void

    #@61
    .line 573
    .restart local v1       #provisioned:Z
    :cond_61
    const-string v2, "sys.lge.dsdp.mode"

    #@63
    const-string v4, "stop"

    #@65
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    .line 574
    .local v0, mDsdpState:Ljava/lang/String;
    const-string v2, "start"

    #@6b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v2

    #@6f
    if-eqz v2, :cond_8e

    #@71
    .line 575
    const-string v2, "KeyguardViewMediator"

    #@73
    new-instance v4, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v5, "onAccessoryStateChanged: mDsdpState = "

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 576
    monitor-exit v3

    #@8a
    goto :goto_60

    #@8b
    .line 589
    .end local v0           #mDsdpState:Ljava/lang/String;
    .end local v1           #provisioned:Z
    :catchall_8b
    move-exception v2

    #@8c
    monitor-exit v3
    :try_end_8d
    .catchall {:try_start_3c .. :try_end_8d} :catchall_8b

    #@8d
    throw v2

    #@8e
    .line 578
    .restart local v0       #mDsdpState:Ljava/lang/String;
    .restart local v1       #provisioned:Z
    :cond_8e
    :try_start_8e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@90
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@93
    move-result-object v2

    #@94
    if-eqz v2, :cond_ac

    #@96
    .line 579
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@98
    const/4 v4, 0x1

    #@99
    invoke-static {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)V

    #@9c
    .line 580
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@9e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a1
    move-result-object v2

    #@a2
    const/4 v4, 0x1

    #@a3
    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->showQuickCoverWindow(Z)V

    #@a6
    .line 581
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@a8
    const/4 v4, 0x1

    #@a9
    invoke-static {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1902(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@ac
    .line 589
    .end local v0           #mDsdpState:Ljava/lang/String;
    .end local v1           #provisioned:Z
    :cond_ac
    :goto_ac
    monitor-exit v3

    #@ad
    goto :goto_60

    #@ae
    .line 583
    :cond_ae
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@b0
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@b3
    move-result-object v2

    #@b4
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverOpened()Z

    #@bb
    move-result v2

    #@bc
    if-eqz v2, :cond_ac

    #@be
    .line 584
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@c0
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@c3
    move-result-object v2

    #@c4
    if-eqz v2, :cond_ac

    #@c6
    .line 585
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@c8
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@cb
    move-result-object v2

    #@cc
    const/4 v4, 0x0

    #@cd
    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->showQuickCoverWindow(Z)V

    #@d0
    .line 586
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@d2
    const/4 v4, 0x0

    #@d3
    invoke-static {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1902(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z
    :try_end_d6
    .catchall {:try_start_8e .. :try_end_d6} :catchall_8b

    #@d6
    goto :goto_ac

    #@d7
    .line 591
    :cond_d7
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@d9
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@dc
    move-result-object v2

    #@dd
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCover()Z

    #@e0
    move-result v2

    #@e1
    if-eqz v2, :cond_60

    #@e3
    .line 592
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@e5
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@e8
    move-result-object v2

    #@e9
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@ec
    move-result v2

    #@ed
    if-eqz v2, :cond_f6

    #@ef
    .line 593
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@f1
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@f4
    goto/16 :goto_60

    #@f6
    .line 594
    :cond_f6
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@f8
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@fb
    move-result-object v2

    #@fc
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverOpened()Z

    #@ff
    move-result v2

    #@100
    if-eqz v2, :cond_60

    #@102
    .line 595
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@104
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@107
    move-result v2

    #@108
    if-nez v2, :cond_111

    #@10a
    .line 596
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@10c
    invoke-virtual {v2, v5, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@10f
    goto/16 :goto_60

    #@111
    .line 598
    :cond_111
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@113
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->wakeUp()V

    #@116
    goto/16 :goto_60
.end method

.method public onClockVisibilityChanged()V
    .registers 2

    #@0
    .prologue
    .line 435
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@5
    .line 436
    return-void
.end method

.method public onDeviceProvisioned()V
    .registers 2

    #@0
    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@5
    .line 441
    return-void
.end method

.method onHdmiPlugStateChanged(Z)V
    .registers 5
    .parameter "connected"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 548
    if-eqz p1, :cond_27

    #@3
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_27

    #@b
    .line 550
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@d
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_27

    #@13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isDisabledUnlock()Z

    #@18
    move-result v0

    #@19
    if-nez v0, :cond_27

    #@1b
    .line 553
    const-string v0, "KeyguardViewMediator"

    #@1d
    const-string v1, "keyguardDone is called from onHdmiPlugStateChanged"

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 555
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@24
    invoke-virtual {v0, v2, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@27
    .line 558
    :cond_27
    return-void
.end method

.method public onPhoneStateChanged(I)V
    .registers 5
    .parameter "phoneState"

    #@0
    .prologue
    .line 417
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    monitor-enter v1

    #@3
    .line 418
    if-nez p1, :cond_21

    #@5
    :try_start_5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@7
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_21

    #@d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_21

    #@15
    .line 426
    const-string v0, "KeyguardViewMediator"

    #@17
    const-string v2, "screen is off and call ended, let\'s make sure the keyguard is showing"

    #@19
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 428
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@21
    .line 430
    :cond_21
    monitor-exit v1

    #@22
    .line 431
    return-void

    #@23
    .line 430
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_23

    #@25
    throw v0
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 3
    .parameter "simState"

    #@0
    .prologue
    .line 445
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@b
    .line 446
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 9
    .parameter "simState"
    .parameter "subscription"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 450
    const-string v3, "KeyguardViewMediator"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "onSimStateChanged: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 452
    const-string v3, "DCM"

    #@1c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$800()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_64

    #@26
    .line 454
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@28
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2a
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$902(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@31
    .line 455
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@33
    invoke-static {v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@36
    .line 456
    const-string v3, "KeyguardViewMediator"

    #@38
    new-instance v4, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v5, "old state: "

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@45
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, " new state: "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@55
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 459
    :cond_64
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$8;->$SwitchMap$com$android$internal$telephony$IccCardConstants$State:[I

    #@66
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    #@69
    move-result v4

    #@6a
    aget v3, v3, v4

    #@6c
    packed-switch v3, :pswitch_data_186

    #@6f
    .line 544
    :cond_6f
    :goto_6f
    return-void

    #@70
    .line 464
    :pswitch_70
    monitor-enter p0

    #@71
    .line 465
    :try_start_71
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@73
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@7a
    move-result v3

    #@7b
    if-nez v3, :cond_9d

    #@7d
    .line 466
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@7f
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@82
    move-result v2

    #@83
    if-nez v2, :cond_96

    #@85
    .line 467
    const-string v2, "KeyguardViewMediator"

    #@87
    const-string v3, "ICC_ABSENT isn\'t showing, we need to show the keyguard since the device isn\'t provisioned yet."

    #@89
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 470
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@91
    .line 498
    :cond_91
    :goto_91
    monitor-exit p0

    #@92
    goto :goto_6f

    #@93
    :catchall_93
    move-exception v2

    #@94
    monitor-exit p0
    :try_end_95
    .catchall {:try_start_71 .. :try_end_95} :catchall_93

    #@95
    throw v2

    #@96
    .line 472
    :cond_96
    :try_start_96
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@98
    const/4 v3, 0x0

    #@99
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@9c
    goto :goto_91

    #@9d
    .line 477
    :cond_9d
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@9f
    if-ne p1, v3, :cond_91

    #@a1
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@a3
    if-eqz v3, :cond_91

    #@a5
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@a8
    move-result-object v3

    #@a9
    if-eqz v3, :cond_91

    #@ab
    .line 479
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@ae
    move-result-object v3

    #@af
    const-string v4, "config_feature_menulock"

    #@b1
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@b4
    move-result v3

    #@b5
    if-eqz v3, :cond_91

    #@b7
    .line 481
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@b9
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@bc
    move-result-object v3

    #@bd
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c0
    move-result-object v0

    #@c1
    .line 483
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v3, "enable_menu_without_usim"

    #@c3
    const/4 v4, 0x1

    #@c4
    invoke-static {v0, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c7
    move-result v3

    #@c8
    if-nez v3, :cond_ef

    #@ca
    .line 485
    .local v1, mIsMenuLock:Z
    :goto_ca
    if-eqz v1, :cond_d2

    #@cc
    .line 486
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@ce
    const/4 v3, 0x1

    #@cf
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@d2
    .line 488
    :cond_d2
    if-eqz v1, :cond_91

    #@d4
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@d6
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@dd
    move-result v2

    #@de
    if-nez v2, :cond_91

    #@e0
    .line 489
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@e2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@e5
    move-result v2

    #@e6
    if-nez v2, :cond_f1

    #@e8
    .line 490
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@ea
    const/4 v3, 0x0

    #@eb
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@ee
    goto :goto_91

    #@ef
    .end local v1           #mIsMenuLock:Z
    :cond_ef
    move v1, v2

    #@f0
    .line 483
    goto :goto_ca

    #@f1
    .line 492
    .restart local v1       #mIsMenuLock:Z
    :cond_f1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@f3
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    :try_end_f6
    .catchall {:try_start_96 .. :try_end_f6} :catchall_93

    #@f6
    goto :goto_91

    #@f7
    .line 502
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #mIsMenuLock:Z
    :pswitch_f7
    monitor-enter p0

    #@f8
    .line 503
    :try_start_f8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@fa
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@fd
    move-result v2

    #@fe
    if-nez v2, :cond_112

    #@100
    .line 504
    const-string v2, "KeyguardViewMediator"

    #@102
    const-string v3, "[kjj]INTENT_VALUE_ICC_LOCKED and keygaurd isn\'t showing; need to show keyguard so user can enter sim pin"

    #@104
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@107
    .line 506
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@109
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@10c
    .line 510
    :goto_10c
    monitor-exit p0

    #@10d
    goto/16 :goto_6f

    #@10f
    :catchall_10f
    move-exception v2

    #@110
    monitor-exit p0
    :try_end_111
    .catchall {:try_start_f8 .. :try_end_111} :catchall_10f

    #@111
    throw v2

    #@112
    .line 508
    :cond_112
    :try_start_112
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@114
    const/4 v3, 0x0

    #@115
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V
    :try_end_118
    .catchall {:try_start_112 .. :try_end_118} :catchall_10f

    #@118
    goto :goto_10c

    #@119
    .line 513
    :pswitch_119
    monitor-enter p0

    #@11a
    .line 514
    :try_start_11a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@11c
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@11f
    move-result v3

    #@120
    if-nez v3, :cond_160

    #@122
    .line 515
    const-string v3, "KeyguardViewMediator"

    #@124
    const-string v4, "PERM_DISABLED and keygaurd isn\'t showing."

    #@126
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@129
    .line 517
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@12b
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@12e
    .line 523
    :goto_12e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@130
    const/4 v4, 0x1

    #@131
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@134
    .line 524
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@136
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@139
    .line 525
    monitor-exit p0
    :try_end_13a
    .catchall {:try_start_11a .. :try_end_13a} :catchall_16e

    #@13a
    .line 527
    const-string v3, "DCM"

    #@13c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$800()Ljava/lang/String;

    #@13f
    move-result-object v4

    #@140
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@143
    move-result v3

    #@144
    if-eqz v3, :cond_6f

    #@146
    .line 528
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@148
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/telephony/IccCardConstants$State;

    #@14b
    move-result-object v3

    #@14c
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@14e
    if-ne v3, v4, :cond_153

    #@150
    .line 529
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1402(Z)Z

    #@153
    .line 531
    :cond_153
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1400()Z

    #@156
    move-result v2

    #@157
    if-eqz v2, :cond_6f

    #@159
    .line 532
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@15b
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@15e
    goto/16 :goto_6f

    #@160
    .line 519
    :cond_160
    :try_start_160
    const-string v3, "KeyguardViewMediator"

    #@162
    const-string v4, "PERM_DISABLED, resetStateLocked toshow permanently disabled message in lockscreen."

    #@164
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@167
    .line 521
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@169
    const/4 v4, 0x0

    #@16a
    invoke-static {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@16d
    goto :goto_12e

    #@16e
    .line 525
    :catchall_16e
    move-exception v2

    #@16f
    monitor-exit p0
    :try_end_170
    .catchall {:try_start_160 .. :try_end_170} :catchall_16e

    #@170
    throw v2

    #@171
    .line 537
    :pswitch_171
    monitor-enter p0

    #@172
    .line 538
    :try_start_172
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@174
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@177
    move-result v2

    #@178
    if-eqz v2, :cond_180

    #@17a
    .line 539
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@17c
    const/4 v3, 0x0

    #@17d
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@180
    .line 541
    :cond_180
    monitor-exit p0

    #@181
    goto/16 :goto_6f

    #@183
    :catchall_183
    move-exception v2

    #@184
    monitor-exit p0
    :try_end_185
    .catchall {:try_start_172 .. :try_end_185} :catchall_183

    #@185
    throw v2

    #@186
    .line 459
    :pswitch_data_186
    .packed-switch 0x1
        :pswitch_70
        :pswitch_70
        :pswitch_f7
        :pswitch_f7
        :pswitch_119
        :pswitch_171
    .end packed-switch
.end method

.method public onUserRemoved(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 411
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->removeUser(I)V

    #@9
    .line 412
    return-void
.end method

.method public onUserSwitched(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 401
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    monitor-enter v1

    #@3
    .line 402
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@9
    .line 403
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@b
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@e
    .line 405
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@10
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@13
    move-result-object v0

    #@14
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@17
    move-result-object v0

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@1c
    .line 406
    monitor-exit v1

    #@1d
    .line 407
    return-void

    #@1e
    .line 406
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method
