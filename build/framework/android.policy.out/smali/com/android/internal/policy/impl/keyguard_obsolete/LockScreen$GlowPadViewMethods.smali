.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;
.super Ljava/lang/Object;
.source "LockScreen.java"

# interfaces
.implements Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GlowPadViewMethods"
.end annotation


# instance fields
.field private final mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/multiwaveview/GlowPadView;)V
    .registers 3
    .parameter
    .parameter "glowPadView"

    #@0
    .prologue
    .line 256
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 257
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@7
    .line 258
    return-void
.end method

.method private launchActivity(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 347
    const/high16 v1, 0x3400

    #@2
    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@5
    .line 352
    :try_start_5
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v1}, Landroid/app/IActivityManager;->dismissKeyguardOnNextActivity()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_c} :catch_1c

    #@c
    .line 357
    :goto_c
    :try_start_c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@e
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;

    #@11
    move-result-object v1

    #@12
    new-instance v2, Landroid/os/UserHandle;

    #@14
    const/4 v3, -0x2

    #@15
    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@18
    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_c .. :try_end_1b} :catch_25

    #@1b
    .line 361
    :goto_1b
    return-void

    #@1c
    .line 353
    :catch_1c
    move-exception v0

    #@1d
    .line 354
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "LockScreen"

    #@1f
    const-string v2, "can\'t dismiss keyguard on launch"

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_c

    #@25
    .line 358
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_25
    move-exception v0

    #@26
    .line 359
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v1, "LockScreen"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Activity not found for intent + "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    goto :goto_1b
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 393
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setOnTriggerListener(Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;)V

    #@6
    .line 394
    return-void
.end method

.method public getTargetPosition(I)I
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getTargetPosition(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    return-object v0
.end method

.method public isTargetPresent(I)Z
    .registers 4
    .parameter "resId"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getTargetPosition(I)I

    #@5
    move-result v0

    #@6
    const/4 v1, -0x1

    #@7
    if-eq v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onFinishFinalAnimation()V
    .registers 1

    #@0
    .prologue
    .line 398
    return-void
.end method

.method public onGrabbed(Landroid/view/View;I)V
    .registers 3
    .parameter "v"
    .parameter "handle"

    #@0
    .prologue
    .line 304
    return-void
.end method

.method public onGrabbedStateChange(Landroid/view/View;I)V
    .registers 4
    .parameter "v"
    .parameter "handle"

    #@0
    .prologue
    .line 367
    if-eqz p2, :cond_b

    #@2
    .line 368
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@b
    .line 370
    :cond_b
    return-void
.end method

.method public onReleased(Landroid/view/View;I)V
    .registers 3
    .parameter "v"
    .parameter "handle"

    #@0
    .prologue
    .line 308
    return-void
.end method

.method public onTrigger(Landroid/view/View;I)V
    .registers 8
    .parameter "v"
    .parameter "target"

    #@0
    .prologue
    .line 311
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v2, p2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getResourceIdForTarget(I)I

    #@5
    move-result v1

    #@6
    .line 312
    .local v1, resId:I
    sparse-switch v1, :sswitch_data_68

    #@9
    .line 340
    :goto_9
    return-void

    #@a
    .line 314
    :sswitch_a
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@c
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;

    #@f
    move-result-object v2

    #@10
    const-string v3, "search"

    #@12
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/app/SearchManager;

    #@18
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@1a
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;

    #@1d
    move-result-object v3

    #@1e
    const/4 v4, -0x2

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@22
    move-result-object v0

    #@23
    .line 317
    .local v0, assistIntent:Landroid/content/Intent;
    if-eqz v0, :cond_32

    #@25
    .line 318
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->launchActivity(Landroid/content/Intent;)V

    #@28
    .line 322
    :goto_28
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2a
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2d
    move-result-object v2

    #@2e
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@31
    goto :goto_9

    #@32
    .line 320
    :cond_32
    const-string v2, "LockScreen"

    #@34
    const-string v3, "Failed to get intent for assist activity"

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_28

    #@3a
    .line 326
    .end local v0           #assistIntent:Landroid/content/Intent;
    :sswitch_3a
    new-instance v2, Landroid/content/Intent;

    #@3c
    const-string v3, "android.media.action.STILL_IMAGE_CAMERA"

    #@3e
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@41
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->launchActivity(Landroid/content/Intent;)V

    #@44
    .line 327
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@46
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@49
    move-result-object v2

    #@4a
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@4d
    goto :goto_9

    #@4e
    .line 331
    :sswitch_4e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@50
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@53
    .line 332
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@55
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@58
    move-result-object v2

    #@59
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@5c
    goto :goto_9

    #@5d
    .line 337
    :sswitch_5d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@5f
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@62
    move-result-object v2

    #@63
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToUnlockScreen()V

    #@66
    goto :goto_9

    #@67
    .line 312
    nop

    #@68
    :sswitch_data_68
    .sparse-switch
        0x108029a -> :sswitch_a
        0x10802e6 -> :sswitch_3a
        0x10802fb -> :sswitch_4e
        0x1080304 -> :sswitch_5d
        0x1080307 -> :sswitch_5d
    .end sparse-switch
.end method

.method public ping()V
    .registers 2

    #@0
    .prologue
    .line 381
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->ping()V

    #@5
    .line 382
    return-void
.end method

.method public reset(Z)V
    .registers 3
    .parameter "animate"

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->reset(Z)V

    #@5
    .line 378
    return-void
.end method

.method public setEnabled(IZ)V
    .registers 4
    .parameter "resourceId"
    .parameter "enabled"

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setEnableTarget(IZ)V

    #@5
    .line 386
    return-void
.end method

.method public updateResources()V
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const v9, 0x108029a

    #@5
    .line 266
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@7
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_9f

    #@d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@f
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_9f

    #@15
    .line 268
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@17
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_9b

    #@1d
    const v3, 0x107000c

    #@20
    .line 273
    .local v3, resId:I
    :goto_20
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@22
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getTargetResourceId()I

    #@25
    move-result v4

    #@26
    if-eq v4, v3, :cond_2d

    #@28
    .line 274
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2a
    invoke-virtual {v4, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setTargetResources(I)V

    #@2d
    .line 278
    :cond_2d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2f
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@32
    move-result v4

    #@33
    if-nez v4, :cond_80

    #@35
    .line 279
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@37
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1200(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;

    #@3a
    move-result-object v4

    #@3b
    const-string v7, "search"

    #@3d
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@40
    move-result-object v4

    #@41
    check-cast v4, Landroid/app/SearchManager;

    #@43
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@45
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;

    #@48
    move-result-object v7

    #@49
    const/4 v8, -0x2

    #@4a
    invoke-virtual {v4, v7, v8}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@4d
    move-result-object v1

    #@4e
    .line 281
    .local v1, intent:Landroid/content/Intent;
    if-eqz v1, :cond_80

    #@50
    .line 285
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@53
    move-result-object v0

    #@54
    .line 286
    .local v0, component:Landroid/content/ComponentName;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@56
    const-string v7, "com.android.systemui.action_assist_icon_google"

    #@58
    invoke-virtual {v4, v0, v7, v9}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->replaceTargetDrawablesIfPresent(Landroid/content/ComponentName;Ljava/lang/String;I)Z

    #@5b
    move-result v2

    #@5c
    .line 290
    .local v2, replaced:Z
    if-nez v2, :cond_80

    #@5e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->mGlowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@60
    const-string v7, "com.android.systemui.action_assist_icon"

    #@62
    invoke-virtual {v4, v0, v7, v9}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->replaceTargetDrawablesIfPresent(Landroid/content/ComponentName;Ljava/lang/String;I)Z

    #@65
    move-result v4

    #@66
    if-nez v4, :cond_80

    #@68
    .line 293
    const-string v4, "LockScreen"

    #@6a
    new-instance v7, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v8, "Couldn\'t grab icon from package "

    #@71
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v7

    #@7d
    invoke-static {v4, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 298
    .end local v0           #component:Landroid/content/ComponentName;
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #replaced:Z
    :cond_80
    const v7, 0x10802e6

    #@83
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@85
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@88
    move-result v4

    #@89
    if-nez v4, :cond_a4

    #@8b
    move v4, v5

    #@8c
    :goto_8c
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->setEnabled(IZ)V

    #@8f
    .line 299
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@91
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@94
    move-result v4

    #@95
    if-nez v4, :cond_a6

    #@97
    :goto_97
    invoke-virtual {p0, v9, v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->setEnabled(IZ)V

    #@9a
    .line 300
    return-void

    #@9b
    .line 268
    .end local v3           #resId:I
    :cond_9b
    const v3, 0x107000f

    #@9e
    goto :goto_20

    #@9f
    .line 271
    :cond_9f
    const v3, 0x1070011

    #@a2
    .restart local v3       #resId:I
    goto/16 :goto_20

    #@a4
    :cond_a4
    move v4, v6

    #@a5
    .line 298
    goto :goto_8c

    #@a6
    :cond_a6
    move v5, v6

    #@a7
    .line 299
    goto :goto_97
.end method
