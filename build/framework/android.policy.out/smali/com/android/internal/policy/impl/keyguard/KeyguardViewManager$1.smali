.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;
.super Landroid/content/BroadcastReceiver;
.source "KeyguardViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 151
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 153
    .local v0, action:Ljava/lang/String;
    const-string v3, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE"

    #@8
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_5d

    #@e
    .line 154
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    const-string v4, "KeyguardViewManager.BroadcastReceiver.onReceive(): message receive com.nttdocomo.android.mascot.KEYGUARD_UPDATE"

    #@14
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 156
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@19
    const-string v3, "RemoteViews"

    #@1b
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Landroid/widget/RemoteViews;

    #@21
    iput-object v3, v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@23
    .line 157
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@25
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@27
    if-nez v3, :cond_53

    #@29
    .line 158
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, "KeyguardViewManager.BroadcastReceiver.onReceive(): mascot is null"

    #@2f
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 165
    :goto_32
    new-instance v2, Landroid/content/Intent;

    #@34
    const-string v3, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@36
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@39
    .line 166
    .local v2, screenIntent:Landroid/content/Intent;
    const-string v3, "RemoteViews"

    #@3b
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3d
    iget-object v4, v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@3f
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@42
    .line 167
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@44
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@47
    move-result-object v3

    #@48
    const-string v4, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@4a
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@4d
    .line 169
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@4f
    invoke-static {v3, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Z)Z

    #@52
    .line 201
    .end local v2           #screenIntent:Landroid/content/Intent;
    :cond_52
    :goto_52
    return-void

    #@53
    .line 162
    :cond_53
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    const-string v4, "KeyguardViewManager.BroadcastReceiver.onReceive(): mascot is exist"

    #@59
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_32

    #@5d
    .line 171
    :cond_5d
    const-string v3, "jp.co.nttdocomo.carriermail.APP_LINK_RECEIVED_MESSAGE"

    #@5f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v3

    #@63
    if-nez v3, :cond_6d

    #@65
    const-string v3, "com.android.internal.policy.impl.CARRIERMAIL_COUNT_UPDATE"

    #@67
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v3

    #@6b
    if-eqz v3, :cond_c0

    #@6d
    .line 174
    :cond_6d
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    const-string v4, "KeyguardViewManager.BroadcastReceiver.onReceive(): message receive bindService IScreenLockService"

    #@73
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 176
    new-instance v2, Landroid/content/Intent;

    #@78
    const-class v3, Lcom/nttdocomo/android/screenlockservice/IScreenLockService;

    #@7a
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@81
    .line 177
    .restart local v2       #screenIntent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@83
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@86
    move-result-object v3

    #@87
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@89
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/ServiceConnection;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v3, v2, v4, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@90
    move-result v3

    #@91
    if-nez v3, :cond_9c

    #@93
    .line 179
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    const-string v4, "KeyguardViewManager.BroadcastReceiver.onReceive(): can\'t connect IScreenLockService"

    #@99
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 182
    :cond_9c
    const-string v3, "com.android.internal.policy.impl.CARRIERMAIL_COUNT_UPDATE"

    #@9e
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v3

    #@a2
    if-eqz v3, :cond_52

    #@a4
    .line 183
    new-instance v1, Landroid/content/Intent;

    #@a6
    const-string v3, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@a8
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ab
    .line 185
    .local v1, charaIntent:Landroid/content/Intent;
    const-string v3, "RemoteViews"

    #@ad
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@af
    iget-object v4, v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@b1
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@b4
    .line 186
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@b6
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@b9
    move-result-object v3

    #@ba
    const-string v4, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@bc
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@bf
    goto :goto_52

    #@c0
    .line 190
    .end local v1           #charaIntent:Landroid/content/Intent;
    .end local v2           #screenIntent:Landroid/content/Intent;
    :cond_c0
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@c2
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v3

    #@c6
    if-eqz v3, :cond_52

    #@c8
    .line 191
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@ca
    invoke-static {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$402(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Z)Z

    #@cd
    .line 193
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@cf
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Z

    #@d2
    move-result v3

    #@d3
    if-eqz v3, :cond_52

    #@d5
    .line 194
    new-instance v2, Landroid/content/Intent;

    #@d7
    const-string v3, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@d9
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@dc
    .line 195
    .restart local v2       #screenIntent:Landroid/content/Intent;
    const-string v3, "RemoteViews"

    #@de
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@e0
    iget-object v4, v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@e2
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@e5
    .line 196
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@e7
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;

    #@ea
    move-result-object v3

    #@eb
    const-string v4, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@ed
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@f0
    .line 198
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@f2
    invoke-static {v3, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Z)Z

    #@f5
    goto/16 :goto_52
.end method
