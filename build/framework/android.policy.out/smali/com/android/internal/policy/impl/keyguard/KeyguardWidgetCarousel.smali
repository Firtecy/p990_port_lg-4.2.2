.class public Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;
.source "KeyguardWidgetCarousel.java"


# static fields
.field private static CAMERA_DISTANCE:F

.field private static MAX_SCROLL_PROGRESS:F


# instance fields
.field private mAdjacentPagesAngle:F

.field protected mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

.field mFastFadeInterpolator:Landroid/view/animation/Interpolator;

.field mSlowFadeInterpolator:Landroid/view/animation/Interpolator;

.field mTmpTransform:[F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const v0, 0x3fa66666

    #@3
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->MAX_SCROLL_PROGRESS:F

    #@5
    .line 37
    const v0, 0x461c4000

    #@8
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->CAMERA_DISTANCE:F

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 46
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 39
    const/4 v0, 0x3

    #@4
    new-array v0, v0, [F

    #@6
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mTmpTransform:[F

    #@8
    .line 207
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;

    #@a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mFastFadeInterpolator:Landroid/view/animation/Interpolator;

    #@f
    .line 216
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$2;

    #@11
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;)V

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mSlowFadeInterpolator:Landroid/view/animation/Interpolator;

    #@16
    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v0

    #@1a
    const v1, 0x10e0040

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@20
    move-result v0

    #@21
    int-to-float v0, v0

    #@22
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mAdjacentPagesAngle:F

    #@24
    .line 52
    return-void
.end method

.method private getTransformForPage(II[F)V
    .registers 13
    .parameter "screenCenter"
    .parameter "index"
    .parameter "transform"

    #@0
    .prologue
    const/high16 v8, 0x4000

    #@2
    .line 195
    invoke-virtual {p0, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildAt(I)Landroid/view/View;

    #@5
    move-result-object v1

    #@6
    .line 196
    .local v1, child:Landroid/view/View;
    invoke-virtual {p0, p1, v1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getBoundedScrollProgress(ILandroid/view/View;I)F

    #@9
    move-result v0

    #@a
    .line 197
    .local v0, boundedProgress:F
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mAdjacentPagesAngle:F

    #@c
    neg-float v6, v6

    #@d
    mul-float v4, v6, v0

    #@f
    .line 198
    .local v4, rotationY:F
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    #@12
    move-result v5

    #@13
    .line 199
    .local v5, width:I
    int-to-float v6, v5

    #@14
    div-float/2addr v6, v8

    #@15
    int-to-float v7, v5

    #@16
    div-float/2addr v7, v8

    #@17
    mul-float/2addr v7, v0

    #@18
    add-float v2, v6, v7

    #@1a
    .line 200
    .local v2, pivotX:F
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    #@1d
    move-result v6

    #@1e
    div-int/lit8 v6, v6, 0x2

    #@20
    int-to-float v3, v6

    #@21
    .line 202
    .local v3, pivotY:F
    const/4 v6, 0x0

    #@22
    aput v2, p3, v6

    #@24
    .line 203
    const/4 v6, 0x1

    #@25
    aput v3, p3, v6

    #@27
    .line 204
    const/4 v6, 0x2

    #@28
    aput v4, p3, v6

    #@2a
    .line 205
    return-void
.end method

.method private updatePageAlphaValues(I)V
    .registers 9
    .parameter "screenCenter"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 86
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@3
    if-eqz v6, :cond_d

    #@5
    .line 87
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@7
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->cancel()V

    #@a
    .line 88
    const/4 v6, 0x0

    #@b
    iput-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@d
    .line 90
    :cond_d
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mShowingInitialHints:Z

    #@f
    if-nez v6, :cond_17

    #@11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->isPageMoving()Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_3c

    #@17
    :cond_17
    const/4 v4, 0x1

    #@18
    .line 91
    .local v4, showSidePages:Z
    :goto_18
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->isReordering(Z)Z

    #@1b
    move-result v5

    #@1c
    if-nez v5, :cond_3e

    #@1e
    .line 92
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildCount()I

    #@22
    move-result v5

    #@23
    if-ge v2, v5, :cond_3e

    #@25
    .line 93
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@28
    move-result-object v0

    #@29
    .line 94
    .local v0, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v0, :cond_39

    #@2b
    .line 95
    invoke-virtual {p0, p1, v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getOutlineAlphaForPage(IIZ)F

    #@2e
    move-result v3

    #@2f
    .line 96
    .local v3, outlineAlpha:F
    invoke-virtual {p0, p1, v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getAlphaForPage(IIZ)F

    #@32
    move-result v1

    #@33
    .line 97
    .local v1, contentAlpha:F
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@36
    .line 98
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentAlpha(F)V

    #@39
    .line 92
    .end local v1           #contentAlpha:F
    .end local v3           #outlineAlpha:F
    :cond_39
    add-int/lit8 v2, v2, 0x1

    #@3b
    goto :goto_1f

    #@3c
    .end local v0           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v2           #i:I
    .end local v4           #showSidePages:Z
    :cond_3c
    move v4, v5

    #@3d
    .line 90
    goto :goto_18

    #@3e
    .line 102
    .restart local v4       #showSidePages:Z
    :cond_3e
    return-void
.end method


# virtual methods
.method animatePagesToCarousel()V
    .registers 23

    #@0
    .prologue
    .line 228
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@4
    move-object/from16 v17, v0

    #@6
    if-eqz v17, :cond_19

    #@8
    .line 229
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@c
    move-object/from16 v17, v0

    #@e
    invoke-virtual/range {v17 .. v17}, Landroid/animation/AnimatorSet;->cancel()V

    #@11
    .line 230
    const/16 v17, 0x0

    #@13
    move-object/from16 v0, v17

    #@15
    move-object/from16 v1, p0

    #@17
    iput-object v0, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@19
    .line 233
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildCount()I

    #@1c
    move-result v7

    #@1d
    .line 239
    .local v7, count:I
    new-instance v5, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@22
    .line 241
    .local v5, anims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    const/4 v11, 0x0

    #@23
    .local v11, i:I
    :goto_23
    if-ge v11, v7, :cond_145

    #@25
    .line 242
    move-object/from16 v0, p0

    #@27
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2a
    move-result-object v6

    #@2b
    .line 243
    .local v6, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    move-object/from16 v0, p0

    #@2d
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@2f
    move/from16 v17, v0

    #@31
    const/16 v18, 0x1

    #@33
    move-object/from16 v0, p0

    #@35
    move/from16 v1, v17

    #@37
    move/from16 v2, v18

    #@39
    invoke-virtual {v0, v1, v11, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getAlphaForPage(IIZ)F

    #@3c
    move-result v9

    #@3d
    .line 244
    .local v9, finalAlpha:F
    move-object/from16 v0, p0

    #@3f
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@41
    move/from16 v17, v0

    #@43
    const/16 v18, 0x1

    #@45
    move-object/from16 v0, p0

    #@47
    move/from16 v1, v17

    #@49
    move/from16 v2, v18

    #@4b
    invoke-virtual {v0, v1, v11, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getOutlineAlphaForPage(IIZ)F

    #@4e
    move-result v10

    #@4f
    .line 245
    .local v10, finalOutlineAlpha:F
    move-object/from16 v0, p0

    #@51
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@53
    move/from16 v17, v0

    #@55
    move-object/from16 v0, p0

    #@57
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mTmpTransform:[F

    #@59
    move-object/from16 v18, v0

    #@5b
    move-object/from16 v0, p0

    #@5d
    move/from16 v1, v17

    #@5f
    move-object/from16 v2, v18

    #@61
    invoke-direct {v0, v1, v11, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getTransformForPage(II[F)V

    #@64
    .line 247
    move-object/from16 v0, p0

    #@66
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@68
    move/from16 v17, v0

    #@6a
    add-int/lit8 v17, v17, -0x1

    #@6c
    move/from16 v0, v17

    #@6e
    if-lt v11, v0, :cond_120

    #@70
    move-object/from16 v0, p0

    #@72
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@74
    move/from16 v17, v0

    #@76
    add-int/lit8 v17, v17, 0x1

    #@78
    move/from16 v0, v17

    #@7a
    if-gt v11, v0, :cond_120

    #@7c
    const/4 v12, 0x1

    #@7d
    .line 250
    .local v12, inVisibleRange:Z
    :goto_7d
    const-string v17, "contentAlpha"

    #@7f
    const/16 v18, 0x1

    #@81
    move/from16 v0, v18

    #@83
    new-array v0, v0, [F

    #@85
    move-object/from16 v18, v0

    #@87
    const/16 v19, 0x0

    #@89
    aput v9, v18, v19

    #@8b
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@8e
    move-result-object v4

    #@8f
    .line 251
    .local v4, alpha:Landroid/animation/PropertyValuesHolder;
    const-string v17, "backgroundAlpha"

    #@91
    const/16 v18, 0x1

    #@93
    move/from16 v0, v18

    #@95
    new-array v0, v0, [F

    #@97
    move-object/from16 v18, v0

    #@99
    const/16 v19, 0x0

    #@9b
    aput v10, v18, v19

    #@9d
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@a0
    move-result-object v13

    #@a1
    .line 252
    .local v13, outlineAlpha:Landroid/animation/PropertyValuesHolder;
    const-string v17, "pivotX"

    #@a3
    const/16 v18, 0x1

    #@a5
    move/from16 v0, v18

    #@a7
    new-array v0, v0, [F

    #@a9
    move-object/from16 v18, v0

    #@ab
    const/16 v19, 0x0

    #@ad
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mTmpTransform:[F

    #@b1
    move-object/from16 v20, v0

    #@b3
    const/16 v21, 0x0

    #@b5
    aget v20, v20, v21

    #@b7
    aput v20, v18, v19

    #@b9
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@bc
    move-result-object v14

    #@bd
    .line 253
    .local v14, pivotX:Landroid/animation/PropertyValuesHolder;
    const-string v17, "pivotY"

    #@bf
    const/16 v18, 0x1

    #@c1
    move/from16 v0, v18

    #@c3
    new-array v0, v0, [F

    #@c5
    move-object/from16 v18, v0

    #@c7
    const/16 v19, 0x0

    #@c9
    move-object/from16 v0, p0

    #@cb
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mTmpTransform:[F

    #@cd
    move-object/from16 v20, v0

    #@cf
    const/16 v21, 0x1

    #@d1
    aget v20, v20, v21

    #@d3
    aput v20, v18, v19

    #@d5
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@d8
    move-result-object v15

    #@d9
    .line 254
    .local v15, pivotY:Landroid/animation/PropertyValuesHolder;
    const-string v17, "rotationY"

    #@db
    const/16 v18, 0x1

    #@dd
    move/from16 v0, v18

    #@df
    new-array v0, v0, [F

    #@e1
    move-object/from16 v18, v0

    #@e3
    const/16 v19, 0x0

    #@e5
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mTmpTransform:[F

    #@e9
    move-object/from16 v20, v0

    #@eb
    const/16 v21, 0x2

    #@ed
    aget v20, v20, v21

    #@ef
    aput v20, v18, v19

    #@f1
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@f4
    move-result-object v16

    #@f5
    .line 256
    .local v16, rotationY:Landroid/animation/PropertyValuesHolder;
    if-eqz v12, :cond_123

    #@f7
    .line 258
    const/16 v17, 0x5

    #@f9
    move/from16 v0, v17

    #@fb
    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    #@fd
    move-object/from16 v17, v0

    #@ff
    const/16 v18, 0x0

    #@101
    aput-object v4, v17, v18

    #@103
    const/16 v18, 0x1

    #@105
    aput-object v13, v17, v18

    #@107
    const/16 v18, 0x2

    #@109
    aput-object v14, v17, v18

    #@10b
    const/16 v18, 0x3

    #@10d
    aput-object v15, v17, v18

    #@10f
    const/16 v18, 0x4

    #@111
    aput-object v16, v17, v18

    #@113
    move-object/from16 v0, v17

    #@115
    invoke-static {v6, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@118
    move-result-object v3

    #@119
    .line 264
    .local v3, a:Landroid/animation/ObjectAnimator;
    :goto_119
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11c
    .line 241
    add-int/lit8 v11, v11, 0x1

    #@11e
    goto/16 :goto_23

    #@120
    .line 247
    .end local v3           #a:Landroid/animation/ObjectAnimator;
    .end local v4           #alpha:Landroid/animation/PropertyValuesHolder;
    .end local v12           #inVisibleRange:Z
    .end local v13           #outlineAlpha:Landroid/animation/PropertyValuesHolder;
    .end local v14           #pivotX:Landroid/animation/PropertyValuesHolder;
    .end local v15           #pivotY:Landroid/animation/PropertyValuesHolder;
    .end local v16           #rotationY:Landroid/animation/PropertyValuesHolder;
    :cond_120
    const/4 v12, 0x0

    #@121
    goto/16 :goto_7d

    #@123
    .line 261
    .restart local v4       #alpha:Landroid/animation/PropertyValuesHolder;
    .restart local v12       #inVisibleRange:Z
    .restart local v13       #outlineAlpha:Landroid/animation/PropertyValuesHolder;
    .restart local v14       #pivotX:Landroid/animation/PropertyValuesHolder;
    .restart local v15       #pivotY:Landroid/animation/PropertyValuesHolder;
    .restart local v16       #rotationY:Landroid/animation/PropertyValuesHolder;
    :cond_123
    const/16 v17, 0x2

    #@125
    move/from16 v0, v17

    #@127
    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    #@129
    move-object/from16 v17, v0

    #@12b
    const/16 v18, 0x0

    #@12d
    aput-object v4, v17, v18

    #@12f
    const/16 v18, 0x1

    #@131
    aput-object v13, v17, v18

    #@133
    move-object/from16 v0, v17

    #@135
    invoke-static {v6, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@138
    move-result-object v3

    #@139
    .line 262
    .restart local v3       #a:Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p0

    #@13b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mFastFadeInterpolator:Landroid/view/animation/Interpolator;

    #@13d
    move-object/from16 v17, v0

    #@13f
    move-object/from16 v0, v17

    #@141
    invoke-virtual {v3, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@144
    goto :goto_119

    #@145
    .line 267
    .end local v3           #a:Landroid/animation/ObjectAnimator;
    .end local v4           #alpha:Landroid/animation/PropertyValuesHolder;
    .end local v6           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v9           #finalAlpha:F
    .end local v10           #finalOutlineAlpha:F
    .end local v12           #inVisibleRange:Z
    .end local v13           #outlineAlpha:Landroid/animation/PropertyValuesHolder;
    .end local v14           #pivotX:Landroid/animation/PropertyValuesHolder;
    .end local v15           #pivotY:Landroid/animation/PropertyValuesHolder;
    .end local v16           #rotationY:Landroid/animation/PropertyValuesHolder;
    :cond_145
    move-object/from16 v0, p0

    #@147
    iget v8, v0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@149
    .line 268
    .local v8, duration:I
    new-instance v17, Landroid/animation/AnimatorSet;

    #@14b
    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    #@14e
    move-object/from16 v0, v17

    #@150
    move-object/from16 v1, p0

    #@152
    iput-object v0, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@154
    .line 269
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@158
    move-object/from16 v17, v0

    #@15a
    move-object/from16 v0, v17

    #@15c
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    #@15f
    .line 271
    move-object/from16 v0, p0

    #@161
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@163
    move-object/from16 v17, v0

    #@165
    int-to-long v0, v8

    #@166
    move-wide/from16 v18, v0

    #@168
    invoke-virtual/range {v17 .. v19}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@16b
    .line 272
    move-object/from16 v0, p0

    #@16d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@16f
    move-object/from16 v17, v0

    #@171
    invoke-virtual/range {v17 .. v17}, Landroid/animation/AnimatorSet;->start()V

    #@174
    .line 273
    return-void
.end method

.method animatePagesToNeutral()V
    .registers 15

    #@0
    .prologue
    .line 157
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@2
    if-eqz v10, :cond_c

    #@4
    .line 158
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@6
    invoke-virtual {v10}, Landroid/animation/AnimatorSet;->cancel()V

    #@9
    .line 159
    const/4 v10, 0x0

    #@a
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@c
    .line 162
    :cond_c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildCount()I

    #@f
    move-result v4

    #@10
    .line 166
    .local v4, count:I
    new-instance v2, Ljava/util/ArrayList;

    #@12
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@15
    .line 168
    .local v2, anims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    const/4 v6, 0x0

    #@16
    .local v6, i:I
    :goto_16
    if-ge v6, v4, :cond_7c

    #@18
    .line 169
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@1b
    move-result-object v3

    #@1c
    .line 170
    .local v3, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@1e
    add-int/lit8 v10, v10, -0x1

    #@20
    if-lt v6, v10, :cond_7a

    #@22
    iget v10, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@24
    add-int/lit8 v10, v10, 0x1

    #@26
    if-gt v6, v10, :cond_7a

    #@28
    const/4 v7, 0x1

    #@29
    .line 171
    .local v7, inVisibleRange:Z
    :goto_29
    if-nez v7, :cond_2f

    #@2b
    .line 172
    const/4 v10, 0x0

    #@2c
    invoke-virtual {v3, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setRotationY(F)V

    #@2f
    .line 174
    :cond_2f
    const-string v10, "contentAlpha"

    #@31
    const/4 v11, 0x1

    #@32
    new-array v11, v11, [F

    #@34
    const/4 v12, 0x0

    #@35
    const/high16 v13, 0x3f80

    #@37
    aput v13, v11, v12

    #@39
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@3c
    move-result-object v1

    #@3d
    .line 175
    .local v1, alpha:Landroid/animation/PropertyValuesHolder;
    const-string v10, "backgroundAlpha"

    #@3f
    const/4 v11, 0x1

    #@40
    new-array v11, v11, [F

    #@42
    const/4 v12, 0x0

    #@43
    const v13, 0x3f19999a

    #@46
    aput v13, v11, v12

    #@48
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@4b
    move-result-object v8

    #@4c
    .line 177
    .local v8, outlineAlpha:Landroid/animation/PropertyValuesHolder;
    const-string v10, "rotationY"

    #@4e
    const/4 v11, 0x1

    #@4f
    new-array v11, v11, [F

    #@51
    const/4 v12, 0x0

    #@52
    const/4 v13, 0x0

    #@53
    aput v13, v11, v12

    #@55
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@58
    move-result-object v9

    #@59
    .line 178
    .local v9, rotationY:Landroid/animation/PropertyValuesHolder;
    const/4 v10, 0x3

    #@5a
    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    #@5c
    const/4 v11, 0x0

    #@5d
    aput-object v1, v10, v11

    #@5f
    const/4 v11, 0x1

    #@60
    aput-object v8, v10, v11

    #@62
    const/4 v11, 0x2

    #@63
    aput-object v9, v10, v11

    #@65
    invoke-static {v3, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@68
    move-result-object v0

    #@69
    .line 179
    .local v0, a:Landroid/animation/ObjectAnimator;
    const/4 v10, 0x0

    #@6a
    invoke-virtual {v3, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setVisibility(I)V

    #@6d
    .line 180
    if-nez v7, :cond_74

    #@6f
    .line 181
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mSlowFadeInterpolator:Landroid/view/animation/Interpolator;

    #@71
    invoke-virtual {v0, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@74
    .line 183
    :cond_74
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@77
    .line 168
    add-int/lit8 v6, v6, 0x1

    #@79
    goto :goto_16

    #@7a
    .line 170
    .end local v0           #a:Landroid/animation/ObjectAnimator;
    .end local v1           #alpha:Landroid/animation/PropertyValuesHolder;
    .end local v7           #inVisibleRange:Z
    .end local v8           #outlineAlpha:Landroid/animation/PropertyValuesHolder;
    .end local v9           #rotationY:Landroid/animation/PropertyValuesHolder;
    :cond_7a
    const/4 v7, 0x0

    #@7b
    goto :goto_29

    #@7c
    .line 186
    .end local v3           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_7c
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@7e
    .line 187
    .local v5, duration:I
    new-instance v10, Landroid/animation/AnimatorSet;

    #@80
    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    #@83
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@85
    .line 188
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@87
    invoke-virtual {v10, v2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    #@8a
    .line 190
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@8c
    int-to-long v11, v5

    #@8d
    invoke-virtual {v10, v11, v12}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@90
    .line 191
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mChildrenTransformsAnimator:Landroid/animation/AnimatorSet;

    #@92
    invoke-virtual {v10}, Landroid/animation/AnimatorSet;->start()V

    #@95
    .line 192
    return-void
.end method

.method public getAlphaForPage(IIZ)F
    .registers 10
    .parameter "screenCenter"
    .parameter "index"
    .parameter "showSidePages"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/high16 v4, 0x3f80

    #@3
    .line 59
    invoke-virtual {p0, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildAt(I)Landroid/view/View;

    #@6
    move-result-object v1

    #@7
    .line 60
    .local v1, child:Landroid/view/View;
    if-nez v1, :cond_a

    #@9
    .line 72
    :cond_9
    :goto_9
    return v0

    #@a
    .line 62
    :cond_a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@d
    move-result v5

    #@e
    add-int/lit8 v5, v5, -0x1

    #@10
    if-lt p2, v5, :cond_27

    #@12
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@15
    move-result v5

    #@16
    add-int/lit8 v5, v5, 0x1

    #@18
    if-gt p2, v5, :cond_27

    #@1a
    const/4 v2, 0x1

    #@1b
    .line 63
    .local v2, inVisibleRange:Z
    :goto_1b
    invoke-virtual {p0, p1, v1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getScrollProgress(ILandroid/view/View;I)F

    #@1e
    move-result v3

    #@1f
    .line 65
    .local v3, scrollProgress:F
    invoke-virtual {p0, p2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->isOverScrollChild(IF)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_29

    #@25
    move v0, v4

    #@26
    .line 66
    goto :goto_9

    #@27
    .line 62
    .end local v2           #inVisibleRange:Z
    .end local v3           #scrollProgress:F
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_1b

    #@29
    .line 67
    .restart local v2       #inVisibleRange:Z
    .restart local v3       #scrollProgress:F
    :cond_29
    if-eqz p3, :cond_2d

    #@2b
    if-nez v2, :cond_33

    #@2d
    :cond_2d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@30
    move-result v5

    #@31
    if-ne p2, v5, :cond_9

    #@33
    .line 68
    :cond_33
    invoke-virtual {p0, p1, v1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getBoundedScrollProgress(ILandroid/view/View;I)F

    #@36
    move-result v3

    #@37
    .line 69
    sget v5, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->MAX_SCROLL_PROGRESS:F

    #@39
    div-float v5, v3, v5

    #@3b
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    #@3e
    move-result v5

    #@3f
    mul-float/2addr v5, v4

    #@40
    sub-float v0, v4, v5

    #@42
    .line 70
    .local v0, alpha:F
    goto :goto_9
.end method

.method protected getMaxScrollProgress()F
    .registers 2

    #@0
    .prologue
    .line 55
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->MAX_SCROLL_PROGRESS:F

    #@2
    return v0
.end method

.method public getOutlineAlphaForPage(IIZ)F
    .registers 6
    .parameter "screenCenter"
    .parameter "index"
    .parameter "showSidePages"

    #@0
    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@3
    move-result v1

    #@4
    add-int/lit8 v1, v1, -0x1

    #@6
    if-lt p2, v1, :cond_18

    #@8
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@b
    move-result v1

    #@c
    add-int/lit8 v1, v1, 0x1

    #@e
    if-gt p2, v1, :cond_18

    #@10
    const/4 v0, 0x1

    #@11
    .line 78
    .local v0, inVisibleRange:Z
    :goto_11
    if-eqz v0, :cond_1a

    #@13
    .line 79
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getOutlineAlphaForPage(IIZ)F

    #@16
    move-result v1

    #@17
    .line 81
    :goto_17
    return v1

    #@18
    .line 77
    .end local v0           #inVisibleRange:Z
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_11

    #@1a
    .line 81
    .restart local v0       #inVisibleRange:Z
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_17
.end method

.method protected onEndReordering()V
    .registers 3

    #@0
    .prologue
    .line 287
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->onEndReordering()V

    #@3
    .line 288
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->fadeInSecurity(I)V

    #@a
    .line 289
    return-void
.end method

.method protected reorderStarting()V
    .registers 3

    #@0
    .prologue
    .line 276
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->REORDERING_ZOOM_IN_OUT_DURATION:I

    #@4
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->fadeOutSecurity(I)V

    #@7
    .line 277
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->animatePagesToNeutral()V

    #@a
    .line 278
    return-void
.end method

.method protected screenScrolled(I)V
    .registers 16
    .parameter "screenCenter"

    #@0
    .prologue
    const/high16 v13, 0x4000

    #@2
    const/4 v12, 0x0

    #@3
    const/4 v10, 0x0

    #@4
    .line 122
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@6
    .line 123
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->updatePageAlphaValues(I)V

    #@9
    .line 124
    invoke-virtual {p0, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->isReordering(Z)Z

    #@c
    move-result v9

    #@d
    if-eqz v9, :cond_10

    #@f
    .line 154
    :cond_f
    return-void

    #@10
    .line 125
    :cond_10
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildCount()I

    #@14
    move-result v9

    #@15
    if-ge v2, v9, :cond_f

    #@17
    .line 126
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@1a
    move-result-object v7

    #@1b
    .line 127
    .local v7, v:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-virtual {p0, p1, v7, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getScrollProgress(ILandroid/view/View;I)F

    #@1e
    move-result v6

    #@1f
    .line 128
    .local v6, scrollProgress:F
    invoke-virtual {p0, p1, v7, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getBoundedScrollProgress(ILandroid/view/View;I)F

    #@22
    move-result v1

    #@23
    .line 129
    .local v1, boundedProgress:F
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@25
    if-eq v7, v9, :cond_29

    #@27
    if-nez v7, :cond_2c

    #@29
    .line 125
    :cond_29
    :goto_29
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_11

    #@2c
    .line 130
    :cond_2c
    sget v9, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->CAMERA_DISTANCE:F

    #@2e
    invoke-virtual {v7, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setCameraDistance(F)V

    #@31
    .line 132
    invoke-virtual {p0, v2, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->isOverScrollChild(IF)Z

    #@34
    move-result v9

    #@35
    if-eqz v9, :cond_59

    #@37
    .line 133
    sget v9, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->OVERSCROLL_MAX_ROTATION:F

    #@39
    neg-float v9, v9

    #@3a
    mul-float/2addr v9, v6

    #@3b
    invoke-virtual {v7, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setRotationY(F)V

    #@3e
    .line 134
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@41
    move-result v11

    #@42
    cmpg-float v9, v6, v12

    #@44
    if-gez v9, :cond_57

    #@46
    const/4 v9, 0x1

    #@47
    :goto_47
    invoke-virtual {v7, v11, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setOverScrollAmount(FZ)V

    #@4a
    .line 145
    :goto_4a
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getAlpha()F

    #@4d
    move-result v0

    #@4e
    .line 148
    .local v0, alpha:F
    cmpl-float v9, v0, v12

    #@50
    if-nez v9, :cond_7d

    #@52
    .line 149
    const/4 v9, 0x4

    #@53
    invoke-virtual {v7, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setVisibility(I)V

    #@56
    goto :goto_29

    #@57
    .end local v0           #alpha:F
    :cond_57
    move v9, v10

    #@58
    .line 134
    goto :goto_47

    #@59
    .line 136
    :cond_59
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredWidth()I

    #@5c
    move-result v8

    #@5d
    .line 137
    .local v8, width:I
    int-to-float v9, v8

    #@5e
    div-float/2addr v9, v13

    #@5f
    int-to-float v11, v8

    #@60
    div-float/2addr v11, v13

    #@61
    mul-float/2addr v11, v1

    #@62
    add-float v3, v9, v11

    #@64
    .line 138
    .local v3, pivotX:F
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredHeight()I

    #@67
    move-result v9

    #@68
    div-int/lit8 v9, v9, 0x2

    #@6a
    int-to-float v4, v9

    #@6b
    .line 139
    .local v4, pivotY:F
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->mAdjacentPagesAngle:F

    #@6d
    neg-float v9, v9

    #@6e
    mul-float v5, v9, v1

    #@70
    .line 140
    .local v5, rotationY:F
    invoke-virtual {v7, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setPivotX(F)V

    #@73
    .line 141
    invoke-virtual {v7, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setPivotY(F)V

    #@76
    .line 142
    invoke-virtual {v7, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setRotationY(F)V

    #@79
    .line 143
    invoke-virtual {v7, v12, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setOverScrollAmount(FZ)V

    #@7c
    goto :goto_4a

    #@7d
    .line 150
    .end local v3           #pivotX:F
    .end local v4           #pivotY:F
    .end local v5           #rotationY:F
    .end local v8           #width:I
    .restart local v0       #alpha:F
    :cond_7d
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getVisibility()I

    #@80
    move-result v9

    #@81
    if-eqz v9, :cond_29

    #@83
    .line 151
    invoke-virtual {v7, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setVisibility(I)V

    #@86
    goto :goto_29
.end method

.method public showInitialPageHints()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 105
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mShowingInitialHints:Z

    #@4
    .line 106
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getChildCount()I

    #@7
    move-result v1

    #@8
    .line 107
    .local v1, count:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v1, :cond_39

    #@b
    .line 108
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@e
    move-result v5

    #@f
    add-int/lit8 v5, v5, -0x1

    #@11
    if-lt v2, v5, :cond_30

    #@13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getNextPage()I

    #@16
    move-result v5

    #@17
    add-int/lit8 v5, v5, 0x1

    #@19
    if-gt v2, v5, :cond_30

    #@1b
    move v3, v4

    #@1c
    .line 109
    .local v3, inVisibleRange:Z
    :goto_1c
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@1f
    move-result-object v0

    #@20
    .line 110
    .local v0, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v3, :cond_32

    #@22
    .line 111
    const v5, 0x3f19999a

    #@25
    invoke-virtual {v0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@28
    .line 112
    const/high16 v5, 0x3f80

    #@2a
    invoke-virtual {v0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentAlpha(F)V

    #@2d
    .line 107
    :goto_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_9

    #@30
    .line 108
    .end local v0           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v3           #inVisibleRange:Z
    :cond_30
    const/4 v3, 0x0

    #@31
    goto :goto_1c

    #@32
    .line 114
    .restart local v0       #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .restart local v3       #inVisibleRange:Z
    :cond_32
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@35
    .line 115
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentAlpha(F)V

    #@38
    goto :goto_2d

    #@39
    .line 118
    .end local v0           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v3           #inVisibleRange:Z
    :cond_39
    return-void
.end method

.method protected zoomIn(Ljava/lang/Runnable;)Z
    .registers 3
    .parameter "onCompleteRunnable"

    #@0
    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetCarousel;->animatePagesToCarousel()V

    #@3
    .line 282
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->zoomIn(Ljava/lang/Runnable;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method
