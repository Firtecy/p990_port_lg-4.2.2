.class public Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;
.super Landroid/view/ViewGroup;
.source "MultiPaneChallengeLayout.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    }
.end annotation


# static fields
.field public static final ANIMATE_BOUNCE_DURATION:I = 0x15e

.field public static final HORIZONTAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MultiPaneChallengeLayout"

.field public static final VERTICAL:I = 0x1


# instance fields
.field private mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

.field private mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

.field private final mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mIsBouncing:Z

.field final mOrientation:I

.field private final mScrimClickListener:Landroid/view/View$OnClickListener;

.field private mScrimView:Landroid/view/View;

.field private final mTempRect:Landroid/graphics/Rect;

.field private mUserSwitcherView:Landroid/view/View;

.field private final mZeroPadding:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 63
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 50
    new-instance v2, Landroid/graphics/Rect;

    #@6
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mTempRect:Landroid/graphics/Rect;

    #@b
    .line 51
    new-instance v2, Landroid/graphics/Rect;

    #@d
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@10
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mZeroPadding:Landroid/graphics/Rect;

    #@12
    .line 55
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$1;

    #@14
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$1;-><init>(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;)V

    #@17
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimClickListener:Landroid/view/View$OnClickListener;

    #@19
    .line 73
    sget-object v2, Lcom/android/internal/R$styleable;->MultiPaneChallengeLayout:[I

    #@1b
    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1e
    move-result-object v0

    #@1f
    .line 75
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@22
    move-result v2

    #@23
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mOrientation:I

    #@25
    .line 77
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@28
    .line 79
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getResources()Landroid/content/res/Resources;

    #@2b
    move-result-object v1

    #@2c
    .line 80
    .local v1, res:Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@2f
    move-result-object v2

    #@30
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@32
    .line 82
    const/16 v2, 0x100

    #@34
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->setSystemUiVisibility(I)V

    #@37
    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method private getVirtualHeight(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;II)I
    .registers 8
    .parameter "lp"
    .parameter "height"
    .parameter "heightUsed"

    #@0
    .prologue
    .line 184
    move v1, p2

    #@1
    .line 185
    .local v1, virtualHeight:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getRootView()Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    .line 186
    .local v0, root:Landroid/view/View;
    if-eqz v0, :cond_11

    #@7
    .line 190
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@9
    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    #@b
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    #@e
    move-result v3

    #@f
    sub-int v1, v2, v3

    #@11
    .line 192
    :cond_11
    iget v2, p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@13
    const/4 v3, 0x1

    #@14
    if-eq v2, v3, :cond_1b

    #@16
    iget v2, p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@18
    const/4 v3, 0x3

    #@19
    if-ne v2, v3, :cond_1e

    #@1b
    .line 197
    :cond_1b
    sub-int p2, v1, p3

    #@1d
    .line 201
    .end local p2
    :cond_1d
    :goto_1d
    return p2

    #@1e
    .line 198
    .restart local p2
    :cond_1e
    iget v2, p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@20
    const/4 v3, 0x7

    #@21
    if-eq v2, v3, :cond_1d

    #@23
    .line 201
    sub-int v2, v1, p3

    #@25
    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    #@28
    move-result p2

    #@29
    goto :goto_1d
.end method

.method private layoutWithGravity(IILandroid/view/View;Landroid/graphics/Rect;Z)V
    .registers 29
    .parameter "width"
    .parameter "height"
    .parameter "child"
    .parameter "padding"
    .parameter "adjustPadding"

    #@0
    .prologue
    .line 362
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v16

    #@4
    check-cast v16, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@6
    .line 364
    .local v16, lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    move-object/from16 v0, p4

    #@8
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@a
    move/from16 v21, v0

    #@c
    move-object/from16 v0, p4

    #@e
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@10
    move/from16 v22, v0

    #@12
    add-int v21, v21, v22

    #@14
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingTop()I

    #@17
    move-result v22

    #@18
    sub-int v21, v21, v22

    #@1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingBottom()I

    #@1d
    move-result v22

    #@1e
    sub-int v12, v21, v22

    #@20
    .line 365
    .local v12, heightUsed:I
    move-object/from16 v0, p0

    #@22
    move-object/from16 v1, v16

    #@24
    move/from16 v2, p2

    #@26
    invoke-direct {v0, v1, v2, v12}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getVirtualHeight(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;II)I

    #@29
    move-result p2

    #@2a
    .line 367
    move-object/from16 v0, v16

    #@2c
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->gravity:I

    #@2e
    move/from16 v21, v0

    #@30
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getLayoutDirection()I

    #@33
    move-result v22

    #@34
    invoke-static/range {v21 .. v22}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@37
    move-result v11

    #@38
    .line 369
    .local v11, gravity:I
    move-object/from16 v0, v16

    #@3a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@3c
    move/from16 v21, v0

    #@3e
    const/16 v22, 0x0

    #@40
    cmpl-float v21, v21, v22

    #@42
    if-lez v21, :cond_bb

    #@44
    const/4 v9, 0x1

    #@45
    .line 370
    .local v9, fixedLayoutSize:Z
    :goto_45
    if-eqz v9, :cond_bd

    #@47
    move-object/from16 v0, p0

    #@49
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mOrientation:I

    #@4b
    move/from16 v21, v0

    #@4d
    if-nez v21, :cond_bd

    #@4f
    const/4 v8, 0x1

    #@50
    .line 371
    .local v8, fixedLayoutHorizontal:Z
    :goto_50
    if-eqz v9, :cond_bf

    #@52
    move-object/from16 v0, p0

    #@54
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mOrientation:I

    #@56
    move/from16 v21, v0

    #@58
    const/16 v22, 0x1

    #@5a
    move/from16 v0, v21

    #@5c
    move/from16 v1, v22

    #@5e
    if-ne v0, v1, :cond_bf

    #@60
    const/4 v10, 0x1

    #@61
    .line 375
    .local v10, fixedLayoutVertical:Z
    :goto_61
    if-eqz v8, :cond_c1

    #@63
    .line 376
    move-object/from16 v0, p4

    #@65
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@67
    move/from16 v21, v0

    #@69
    sub-int v21, p1, v21

    #@6b
    move-object/from16 v0, p4

    #@6d
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@6f
    move/from16 v22, v0

    #@71
    sub-int v18, v21, v22

    #@73
    .line 377
    .local v18, paddedWidth:I
    move/from16 v0, v18

    #@75
    int-to-float v0, v0

    #@76
    move/from16 v21, v0

    #@78
    move-object/from16 v0, v16

    #@7a
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@7c
    move/from16 v22, v0

    #@7e
    mul-float v21, v21, v22

    #@80
    const/high16 v22, 0x3f00

    #@82
    add-float v21, v21, v22

    #@84
    move/from16 v0, v21

    #@86
    float-to-int v4, v0

    #@87
    .line 378
    .local v4, adjustedWidth:I
    move/from16 v3, p2

    #@89
    .line 388
    .end local v18           #paddedWidth:I
    .local v3, adjustedHeight:I
    :goto_89
    invoke-static {v11}, Landroid/view/Gravity;->isVertical(I)Z

    #@8c
    move-result v14

    #@8d
    .line 389
    .local v14, isVertical:Z
    invoke-static {v11}, Landroid/view/Gravity;->isHorizontal(I)Z

    #@90
    move-result v13

    #@91
    .line 390
    .local v13, isHorizontal:Z
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getMeasuredWidth()I

    #@94
    move-result v7

    #@95
    .line 391
    .local v7, childWidth:I
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getMeasuredHeight()I

    #@98
    move-result v6

    #@99
    .line 393
    .local v6, childHeight:I
    move-object/from16 v0, p4

    #@9b
    iget v15, v0, Landroid/graphics/Rect;->left:I

    #@9d
    .line 394
    .local v15, left:I
    move-object/from16 v0, p4

    #@9f
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@a1
    move/from16 v20, v0

    #@a3
    .line 395
    .local v20, top:I
    add-int v19, v15, v7

    #@a5
    .line 396
    .local v19, right:I
    add-int v5, v20, v6

    #@a7
    .line 397
    .local v5, bottom:I
    and-int/lit8 v21, v11, 0x70

    #@a9
    sparse-switch v21, :sswitch_data_1ea

    #@ac
    .line 422
    :cond_ac
    :goto_ac
    and-int/lit8 v21, v11, 0x7

    #@ae
    packed-switch v21, :pswitch_data_1f8

    #@b1
    .line 448
    :cond_b1
    :goto_b1
    :pswitch_b1
    move-object/from16 v0, p3

    #@b3
    move/from16 v1, v20

    #@b5
    move/from16 v2, v19

    #@b7
    invoke-virtual {v0, v15, v1, v2, v5}, Landroid/view/View;->layout(IIII)V

    #@ba
    .line 449
    return-void

    #@bb
    .line 369
    .end local v3           #adjustedHeight:I
    .end local v4           #adjustedWidth:I
    .end local v5           #bottom:I
    .end local v6           #childHeight:I
    .end local v7           #childWidth:I
    .end local v8           #fixedLayoutHorizontal:Z
    .end local v9           #fixedLayoutSize:Z
    .end local v10           #fixedLayoutVertical:Z
    .end local v13           #isHorizontal:Z
    .end local v14           #isVertical:Z
    .end local v15           #left:I
    .end local v19           #right:I
    .end local v20           #top:I
    :cond_bb
    const/4 v9, 0x0

    #@bc
    goto :goto_45

    #@bd
    .line 370
    .restart local v9       #fixedLayoutSize:Z
    :cond_bd
    const/4 v8, 0x0

    #@be
    goto :goto_50

    #@bf
    .line 371
    .restart local v8       #fixedLayoutHorizontal:Z
    :cond_bf
    const/4 v10, 0x0

    #@c0
    goto :goto_61

    #@c1
    .line 379
    .restart local v10       #fixedLayoutVertical:Z
    :cond_c1
    if-eqz v10, :cond_e6

    #@c3
    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingTop()I

    #@c6
    move-result v21

    #@c7
    sub-int v21, p2, v21

    #@c9
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingBottom()I

    #@cc
    move-result v22

    #@cd
    sub-int v17, v21, v22

    #@cf
    .line 381
    .local v17, paddedHeight:I
    move/from16 v4, p1

    #@d1
    .line 382
    .restart local v4       #adjustedWidth:I
    move/from16 v0, v17

    #@d3
    int-to-float v0, v0

    #@d4
    move/from16 v21, v0

    #@d6
    move-object/from16 v0, v16

    #@d8
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@da
    move/from16 v22, v0

    #@dc
    mul-float v21, v21, v22

    #@de
    const/high16 v22, 0x3f00

    #@e0
    add-float v21, v21, v22

    #@e2
    move/from16 v0, v21

    #@e4
    float-to-int v3, v0

    #@e5
    .line 383
    .restart local v3       #adjustedHeight:I
    goto :goto_89

    #@e6
    .line 384
    .end local v3           #adjustedHeight:I
    .end local v4           #adjustedWidth:I
    .end local v17           #paddedHeight:I
    :cond_e6
    move/from16 v4, p1

    #@e8
    .line 385
    .restart local v4       #adjustedWidth:I
    move/from16 v3, p2

    #@ea
    .restart local v3       #adjustedHeight:I
    goto :goto_89

    #@eb
    .line 399
    .restart local v5       #bottom:I
    .restart local v6       #childHeight:I
    .restart local v7       #childWidth:I
    .restart local v13       #isHorizontal:Z
    .restart local v14       #isVertical:Z
    .restart local v15       #left:I
    .restart local v19       #right:I
    .restart local v20       #top:I
    :sswitch_eb
    if-eqz v10, :cond_114

    #@ed
    move-object/from16 v0, p4

    #@ef
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@f1
    move/from16 v21, v0

    #@f3
    sub-int v22, v3, v6

    #@f5
    div-int/lit8 v22, v22, 0x2

    #@f7
    add-int v20, v21, v22

    #@f9
    .line 401
    :goto_f9
    add-int v5, v20, v6

    #@fb
    .line 402
    if-eqz p5, :cond_ac

    #@fd
    if-eqz v14, :cond_ac

    #@ff
    .line 403
    move-object/from16 v0, p4

    #@101
    iput v5, v0, Landroid/graphics/Rect;->top:I

    #@103
    .line 404
    move-object/from16 v0, p4

    #@105
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@107
    move/from16 v21, v0

    #@109
    div-int/lit8 v22, v6, 0x2

    #@10b
    add-int v21, v21, v22

    #@10d
    move/from16 v0, v21

    #@10f
    move-object/from16 v1, p4

    #@111
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    #@113
    goto :goto_ac

    #@114
    .line 399
    :cond_114
    move-object/from16 v0, p4

    #@116
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@118
    move/from16 v20, v0

    #@11a
    goto :goto_f9

    #@11b
    .line 408
    :sswitch_11b
    if-eqz v10, :cond_14b

    #@11d
    move-object/from16 v0, p4

    #@11f
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@121
    move/from16 v21, v0

    #@123
    add-int v21, v21, p2

    #@125
    sub-int v22, v3, v6

    #@127
    div-int/lit8 v22, v22, 0x2

    #@129
    sub-int v5, v21, v22

    #@12b
    .line 411
    :goto_12b
    sub-int v20, v5, v6

    #@12d
    .line 412
    if-eqz p5, :cond_ac

    #@12f
    if-eqz v14, :cond_ac

    #@131
    .line 413
    sub-int v21, p2, v20

    #@133
    move/from16 v0, v21

    #@135
    move-object/from16 v1, p4

    #@137
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    #@139
    .line 414
    move-object/from16 v0, p4

    #@13b
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@13d
    move/from16 v21, v0

    #@13f
    div-int/lit8 v22, v6, 0x2

    #@141
    add-int v21, v21, v22

    #@143
    move/from16 v0, v21

    #@145
    move-object/from16 v1, p4

    #@147
    iput v0, v1, Landroid/graphics/Rect;->top:I

    #@149
    goto/16 :goto_ac

    #@14b
    .line 408
    :cond_14b
    move-object/from16 v0, p4

    #@14d
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@14f
    move/from16 v21, v0

    #@151
    add-int v5, v21, p2

    #@153
    goto :goto_12b

    #@154
    .line 418
    :sswitch_154
    move-object/from16 v0, p4

    #@156
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@158
    move/from16 v21, v0

    #@15a
    sub-int v22, p2, v6

    #@15c
    div-int/lit8 v22, v22, 0x2

    #@15e
    add-int v20, v21, v22

    #@160
    .line 419
    add-int v5, v20, v6

    #@162
    goto/16 :goto_ac

    #@164
    .line 424
    :pswitch_164
    if-eqz v8, :cond_192

    #@166
    move-object/from16 v0, p4

    #@168
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@16a
    move/from16 v21, v0

    #@16c
    sub-int v22, v4, v7

    #@16e
    div-int/lit8 v22, v22, 0x2

    #@170
    add-int v15, v21, v22

    #@172
    .line 426
    :goto_172
    add-int v19, v15, v7

    #@174
    .line 427
    if-eqz p5, :cond_b1

    #@176
    if-eqz v13, :cond_b1

    #@178
    if-nez v14, :cond_b1

    #@17a
    .line 428
    move/from16 v0, v19

    #@17c
    move-object/from16 v1, p4

    #@17e
    iput v0, v1, Landroid/graphics/Rect;->left:I

    #@180
    .line 429
    move-object/from16 v0, p4

    #@182
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@184
    move/from16 v21, v0

    #@186
    div-int/lit8 v22, v7, 0x2

    #@188
    add-int v21, v21, v22

    #@18a
    move/from16 v0, v21

    #@18c
    move-object/from16 v1, p4

    #@18e
    iput v0, v1, Landroid/graphics/Rect;->right:I

    #@190
    goto/16 :goto_b1

    #@192
    .line 424
    :cond_192
    move-object/from16 v0, p4

    #@194
    iget v15, v0, Landroid/graphics/Rect;->left:I

    #@196
    goto :goto_172

    #@197
    .line 433
    :pswitch_197
    if-eqz v8, :cond_1c9

    #@199
    move-object/from16 v0, p4

    #@19b
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@19d
    move/from16 v21, v0

    #@19f
    sub-int v21, p1, v21

    #@1a1
    sub-int v22, v4, v7

    #@1a3
    div-int/lit8 v22, v22, 0x2

    #@1a5
    sub-int v19, v21, v22

    #@1a7
    .line 436
    :goto_1a7
    sub-int v15, v19, v7

    #@1a9
    .line 437
    if-eqz p5, :cond_b1

    #@1ab
    if-eqz v13, :cond_b1

    #@1ad
    if-nez v14, :cond_b1

    #@1af
    .line 438
    sub-int v21, p1, v15

    #@1b1
    move/from16 v0, v21

    #@1b3
    move-object/from16 v1, p4

    #@1b5
    iput v0, v1, Landroid/graphics/Rect;->right:I

    #@1b7
    .line 439
    move-object/from16 v0, p4

    #@1b9
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@1bb
    move/from16 v21, v0

    #@1bd
    div-int/lit8 v22, v7, 0x2

    #@1bf
    add-int v21, v21, v22

    #@1c1
    move/from16 v0, v21

    #@1c3
    move-object/from16 v1, p4

    #@1c5
    iput v0, v1, Landroid/graphics/Rect;->left:I

    #@1c7
    goto/16 :goto_b1

    #@1c9
    .line 433
    :cond_1c9
    move-object/from16 v0, p4

    #@1cb
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@1cd
    move/from16 v21, v0

    #@1cf
    sub-int v19, p1, v21

    #@1d1
    goto :goto_1a7

    #@1d2
    .line 443
    :pswitch_1d2
    move-object/from16 v0, p4

    #@1d4
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@1d6
    move/from16 v21, v0

    #@1d8
    sub-int v21, p1, v21

    #@1da
    move-object/from16 v0, p4

    #@1dc
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@1de
    move/from16 v22, v0

    #@1e0
    sub-int v18, v21, v22

    #@1e2
    .line 444
    .restart local v18       #paddedWidth:I
    sub-int v21, v18, v7

    #@1e4
    div-int/lit8 v15, v21, 0x2

    #@1e6
    .line 445
    add-int v19, v15, v7

    #@1e8
    goto/16 :goto_b1

    #@1ea
    .line 397
    :sswitch_data_1ea
    .sparse-switch
        0x10 -> :sswitch_154
        0x30 -> :sswitch_eb
        0x50 -> :sswitch_11b
    .end sparse-switch

    #@1f8
    .line 422
    :pswitch_data_1f8
    .packed-switch 0x1
        :pswitch_1d2
        :pswitch_b1
        :pswitch_164
        :pswitch_b1
        :pswitch_197
    .end packed-switch
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 470
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@2
    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 465
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@2
    invoke-direct {v0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 453
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1, p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 458
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@2
    if-eqz v0, :cond_c

    #@4
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@6
    check-cast p1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@8
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;-><init>(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;)V

    #@b
    :goto_b
    return-object v0

    #@c
    .restart local p1
    :cond_c
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@e
    if-eqz v0, :cond_18

    #@10
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@12
    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@14
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@17
    goto :goto_b

    #@18
    .restart local p1
    :cond_18
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@1a
    invoke-direct {v0, p1}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@1d
    goto :goto_b
.end method

.method public getBouncerAnimationDuration()I
    .registers 2

    #@0
    .prologue
    .line 101
    const/16 v0, 0x15e

    #@2
    return v0
.end method

.method public hideBouncer()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 130
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 150
    :cond_5
    :goto_5
    return-void

    #@6
    .line 131
    :cond_6
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@8
    .line 132
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@a
    if-eqz v1, :cond_35

    #@c
    .line 133
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 134
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@12
    const/16 v2, 0x15e

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->hideBouncer(I)V

    #@17
    .line 137
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@19
    const-string v2, "alpha"

    #@1b
    const/4 v3, 0x1

    #@1c
    new-array v3, v3, [F

    #@1e
    const/4 v4, 0x0

    #@1f
    aput v4, v3, v5

    #@21
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@24
    move-result-object v0

    #@25
    .line 138
    .local v0, anim:Landroid/animation/Animator;
    const-wide/16 v1, 0x15e

    #@27
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@2a
    .line 139
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$3;

    #@2c
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$3;-><init>(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;)V

    #@2f
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@32
    .line 145
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@35
    .line 147
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_35
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@37
    if-eqz v1, :cond_5

    #@39
    .line 148
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@3b
    invoke-interface {v1, v5}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;->onBouncerStateChanged(Z)V

    #@3e
    goto :goto_5
.end method

.method public isBouncing()Z
    .registers 2

    #@0
    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@2
    return v0
.end method

.method public isChallengeOverlapping()Z
    .registers 2

    #@0
    .prologue
    .line 92
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isChallengeShowing()Z
    .registers 2

    #@0
    .prologue
    .line 87
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 20
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 326
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mTempRect:Landroid/graphics/Rect;

    #@2
    .line 327
    .local v4, padding:Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingLeft()I

    #@5
    move-result v0

    #@6
    iput v0, v4, Landroid/graphics/Rect;->left:I

    #@8
    .line 328
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingTop()I

    #@b
    move-result v0

    #@c
    iput v0, v4, Landroid/graphics/Rect;->top:I

    #@e
    .line 329
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingRight()I

    #@11
    move-result v0

    #@12
    iput v0, v4, Landroid/graphics/Rect;->right:I

    #@14
    .line 330
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getPaddingBottom()I

    #@17
    move-result v0

    #@18
    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    #@1a
    .line 331
    sub-int v1, p4, p2

    #@1c
    .line 332
    .local v1, width:I
    sub-int v2, p5, p3

    #@1e
    .line 336
    .local v2, height:I
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@20
    if-eqz v0, :cond_33

    #@22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@24
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@27
    move-result v0

    #@28
    const/16 v3, 0x8

    #@2a
    if-eq v0, v3, :cond_33

    #@2c
    .line 337
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@2e
    const/4 v5, 0x1

    #@2f
    move-object v0, p0

    #@30
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->layoutWithGravity(IILandroid/view/View;Landroid/graphics/Rect;Z)V

    #@33
    .line 340
    :cond_33
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getChildCount()I

    #@36
    move-result v11

    #@37
    .line 341
    .local v11, count:I
    const/4 v12, 0x0

    #@38
    .local v12, i:I
    :goto_38
    if-ge v12, v11, :cond_73

    #@3a
    .line 342
    invoke-virtual {p0, v12}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@3d
    move-result-object v8

    #@3e
    .line 343
    .local v8, child:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@41
    move-result-object v13

    #@42
    check-cast v13, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@44
    .line 346
    .local v13, lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@46
    if-eq v8, v0, :cond_50

    #@48
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    #@4b
    move-result v0

    #@4c
    const/16 v3, 0x8

    #@4e
    if-ne v0, v3, :cond_53

    #@50
    .line 341
    :cond_50
    :goto_50
    add-int/lit8 v12, v12, 0x1

    #@52
    goto :goto_38

    #@53
    .line 348
    :cond_53
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@55
    if-ne v8, v0, :cond_5d

    #@57
    .line 349
    const/4 v0, 0x0

    #@58
    const/4 v3, 0x0

    #@59
    invoke-virtual {v8, v0, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    #@5c
    goto :goto_50

    #@5d
    .line 351
    :cond_5d
    iget v0, v13, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@5f
    const/4 v3, 0x7

    #@60
    if-ne v0, v3, :cond_6c

    #@62
    .line 352
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mZeroPadding:Landroid/graphics/Rect;

    #@64
    const/4 v10, 0x0

    #@65
    move-object v5, p0

    #@66
    move v6, v1

    #@67
    move v7, v2

    #@68
    invoke-direct/range {v5 .. v10}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->layoutWithGravity(IILandroid/view/View;Landroid/graphics/Rect;Z)V

    #@6b
    goto :goto_50

    #@6c
    .line 356
    :cond_6c
    const/4 v5, 0x0

    #@6d
    move-object v0, p0

    #@6e
    move-object v3, v8

    #@6f
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->layoutWithGravity(IILandroid/view/View;Landroid/graphics/Rect;Z)V

    #@72
    goto :goto_50

    #@73
    .line 358
    .end local v8           #child:Landroid/view/View;
    .end local v13           #lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    :cond_73
    return-void
.end method

.method protected onMeasure(II)V
    .registers 19
    .parameter "widthSpec"
    .parameter "heightSpec"

    #@0
    .prologue
    .line 206
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v2

    #@4
    const/high16 v5, 0x4000

    #@6
    if-ne v2, v5, :cond_10

    #@8
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v2

    #@c
    const/high16 v5, 0x4000

    #@e
    if-eq v2, v5, :cond_18

    #@10
    .line 208
    :cond_10
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v5, "MultiPaneChallengeLayout must be measured with an exact size"

    #@14
    invoke-direct {v2, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 212
    :cond_18
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1b
    move-result v14

    #@1c
    .line 213
    .local v14, width:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1f
    move-result v9

    #@20
    .line 214
    .local v9, height:I
    move-object/from16 v0, p0

    #@22
    invoke-virtual {v0, v14, v9}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->setMeasuredDimension(II)V

    #@25
    .line 216
    const/4 v15, 0x0

    #@26
    .line 217
    .local v15, widthUsed:I
    const/4 v10, 0x0

    #@27
    .line 221
    .local v10, heightUsed:I
    const/4 v2, 0x0

    #@28
    move-object/from16 v0, p0

    #@2a
    iput-object v2, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@2c
    .line 222
    const/4 v2, 0x0

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput-object v2, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@31
    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getChildCount()I

    #@34
    move-result v8

    #@35
    .line 224
    .local v8, count:I
    const/4 v11, 0x0

    #@36
    .local v11, i:I
    :goto_36
    if-ge v11, v8, :cond_f1

    #@38
    .line 225
    move-object/from16 v0, p0

    #@3a
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@3d
    move-result-object v3

    #@3e
    .line 226
    .local v3, child:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@41
    move-result-object v12

    #@42
    check-cast v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@44
    .line 228
    .local v12, lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@46
    const/4 v5, 0x2

    #@47
    if-ne v2, v5, :cond_6c

    #@49
    .line 229
    move-object/from16 v0, p0

    #@4b
    iget-object v2, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@4d
    if-eqz v2, :cond_57

    #@4f
    .line 230
    new-instance v2, Ljava/lang/IllegalStateException;

    #@51
    const-string v5, "There may only be one child of type challenge"

    #@53
    invoke-direct {v2, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@56
    throw v2

    #@57
    .line 233
    :cond_57
    instance-of v2, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@59
    if-nez v2, :cond_63

    #@5b
    .line 234
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5d
    const-string v5, "Challenge must be a KeyguardSecurityContainer"

    #@5f
    invoke-direct {v2, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@62
    throw v2

    #@63
    .line 237
    :cond_63
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@65
    .end local v3           #child:Landroid/view/View;
    move-object/from16 v0, p0

    #@67
    iput-object v3, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@69
    .line 224
    :cond_69
    :goto_69
    add-int/lit8 v11, v11, 0x1

    #@6b
    goto :goto_36

    #@6c
    .line 238
    .restart local v3       #child:Landroid/view/View;
    :cond_6c
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@6e
    const/4 v5, 0x3

    #@6f
    if-ne v2, v5, :cond_de

    #@71
    .line 239
    move-object/from16 v0, p0

    #@73
    iget-object v2, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@75
    if-eqz v2, :cond_7f

    #@77
    .line 240
    new-instance v2, Ljava/lang/IllegalStateException;

    #@79
    const-string v5, "There may only be one child of type userSwitcher"

    #@7b
    invoke-direct {v2, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v2

    #@7f
    .line 243
    :cond_7f
    move-object/from16 v0, p0

    #@81
    iput-object v3, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mUserSwitcherView:Landroid/view/View;

    #@83
    .line 245
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@86
    move-result v2

    #@87
    const/16 v5, 0x8

    #@89
    if-eq v2, v5, :cond_69

    #@8b
    .line 247
    move/from16 v4, p1

    #@8d
    .line 248
    .local v4, adjustedWidthSpec:I
    move/from16 v6, p2

    #@8f
    .line 249
    .local v6, adjustedHeightSpec:I
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxWidth:I

    #@91
    if-ltz v2, :cond_9f

    #@93
    .line 250
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxWidth:I

    #@95
    invoke-static {v2, v14}, Ljava/lang/Math;->min(II)I

    #@98
    move-result v2

    #@99
    const/high16 v5, 0x4000

    #@9b
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@9e
    move-result v4

    #@9f
    .line 253
    :cond_9f
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxHeight:I

    #@a1
    if-ltz v2, :cond_af

    #@a3
    .line 254
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxHeight:I

    #@a5
    invoke-static {v2, v9}, Ljava/lang/Math;->min(II)I

    #@a8
    move-result v2

    #@a9
    const/high16 v5, 0x4000

    #@ab
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@ae
    move-result v6

    #@af
    .line 258
    :cond_af
    const/4 v5, 0x0

    #@b0
    const/4 v7, 0x0

    #@b1
    move-object/from16 v2, p0

    #@b3
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@b6
    .line 262
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->gravity:I

    #@b8
    invoke-static {v2}, Landroid/view/Gravity;->isVertical(I)Z

    #@bb
    move-result v2

    #@bc
    if-eqz v2, :cond_ca

    #@be
    .line 263
    int-to-float v2, v10

    #@bf
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@c2
    move-result v5

    #@c3
    int-to-float v5, v5

    #@c4
    const/high16 v7, 0x3fc0

    #@c6
    mul-float/2addr v5, v7

    #@c7
    add-float/2addr v2, v5

    #@c8
    float-to-int v10, v2

    #@c9
    goto :goto_69

    #@ca
    .line 264
    :cond_ca
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->gravity:I

    #@cc
    invoke-static {v2}, Landroid/view/Gravity;->isHorizontal(I)Z

    #@cf
    move-result v2

    #@d0
    if-eqz v2, :cond_69

    #@d2
    .line 265
    int-to-float v2, v15

    #@d3
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@d6
    move-result v5

    #@d7
    int-to-float v5, v5

    #@d8
    const/high16 v7, 0x3fc0

    #@da
    mul-float/2addr v5, v7

    #@db
    add-float/2addr v2, v5

    #@dc
    float-to-int v15, v2

    #@dd
    goto :goto_69

    #@de
    .line 267
    .end local v4           #adjustedWidthSpec:I
    .end local v6           #adjustedHeightSpec:I
    :cond_de
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@e0
    const/4 v5, 0x4

    #@e1
    if-ne v2, v5, :cond_69

    #@e3
    .line 268
    move-object/from16 v0, p0

    #@e5
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->setScrimView(Landroid/view/View;)V

    #@e8
    .line 269
    move/from16 v0, p1

    #@ea
    move/from16 v1, p2

    #@ec
    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    #@ef
    goto/16 :goto_69

    #@f1
    .line 274
    .end local v3           #child:Landroid/view/View;
    .end local v12           #lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    :cond_f1
    const/4 v11, 0x0

    #@f2
    :goto_f2
    if-ge v11, v8, :cond_194

    #@f4
    .line 275
    move-object/from16 v0, p0

    #@f6
    invoke-virtual {v0, v11}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getChildAt(I)Landroid/view/View;

    #@f9
    move-result-object v3

    #@fa
    .line 276
    .restart local v3       #child:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@fd
    move-result-object v12

    #@fe
    check-cast v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;

    #@100
    .line 278
    .restart local v12       #lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@102
    const/4 v5, 0x3

    #@103
    if-eq v2, v5, :cond_112

    #@105
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->childType:I

    #@107
    const/4 v5, 0x4

    #@108
    if-eq v2, v5, :cond_112

    #@10a
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@10d
    move-result v2

    #@10e
    const/16 v5, 0x8

    #@110
    if-ne v2, v5, :cond_115

    #@112
    .line 274
    :cond_112
    :goto_112
    add-int/lit8 v11, v11, 0x1

    #@114
    goto :goto_f2

    #@115
    .line 285
    :cond_115
    move-object/from16 v0, p0

    #@117
    invoke-direct {v0, v12, v9, v10}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->getVirtualHeight(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;II)I

    #@11a
    move-result v13

    #@11b
    .line 289
    .local v13, virtualHeight:I
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@11d
    const/4 v5, 0x0

    #@11e
    cmpl-float v2, v2, v5

    #@120
    if-lez v2, :cond_185

    #@122
    .line 290
    move-object/from16 v0, p0

    #@124
    iget v2, v0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mOrientation:I

    #@126
    if-nez v2, :cond_16e

    #@128
    .line 291
    sub-int v2, v14, v15

    #@12a
    int-to-float v2, v2

    #@12b
    iget v5, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@12d
    mul-float/2addr v2, v5

    #@12e
    const/high16 v5, 0x3f00

    #@130
    add-float/2addr v2, v5

    #@131
    float-to-int v2, v2

    #@132
    const/high16 v5, 0x4000

    #@134
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@137
    move-result v4

    #@138
    .line 294
    .restart local v4       #adjustedWidthSpec:I
    const/high16 v2, 0x4000

    #@13a
    invoke-static {v13, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13d
    move-result v6

    #@13e
    .line 309
    .restart local v6       #adjustedHeightSpec:I
    :goto_13e
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxWidth:I

    #@140
    if-ltz v2, :cond_152

    #@142
    .line 310
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxWidth:I

    #@144
    invoke-static {v4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@147
    move-result v5

    #@148
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    #@14b
    move-result v2

    #@14c
    const/high16 v5, 0x4000

    #@14e
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@151
    move-result v4

    #@152
    .line 314
    :cond_152
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxHeight:I

    #@154
    if-ltz v2, :cond_166

    #@156
    .line 315
    iget v2, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->maxHeight:I

    #@158
    invoke-static {v6}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@15b
    move-result v5

    #@15c
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    #@15f
    move-result v2

    #@160
    const/high16 v5, 0x4000

    #@162
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@165
    move-result v6

    #@166
    .line 320
    :cond_166
    const/4 v5, 0x0

    #@167
    const/4 v7, 0x0

    #@168
    move-object/from16 v2, p0

    #@16a
    invoke-virtual/range {v2 .. v7}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@16d
    goto :goto_112

    #@16e
    .line 297
    .end local v4           #adjustedWidthSpec:I
    .end local v6           #adjustedHeightSpec:I
    :cond_16e
    sub-int v2, v14, v15

    #@170
    const/high16 v5, 0x4000

    #@172
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@175
    move-result v4

    #@176
    .line 299
    .restart local v4       #adjustedWidthSpec:I
    int-to-float v2, v13

    #@177
    iget v5, v12, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;->centerWithinArea:F

    #@179
    mul-float/2addr v2, v5

    #@17a
    const/high16 v5, 0x3f00

    #@17c
    add-float/2addr v2, v5

    #@17d
    float-to-int v2, v2

    #@17e
    const/high16 v5, 0x4000

    #@180
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@183
    move-result v6

    #@184
    .restart local v6       #adjustedHeightSpec:I
    goto :goto_13e

    #@185
    .line 304
    .end local v4           #adjustedWidthSpec:I
    .end local v6           #adjustedHeightSpec:I
    :cond_185
    sub-int v2, v14, v15

    #@187
    const/high16 v5, 0x4000

    #@189
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@18c
    move-result v4

    #@18d
    .line 306
    .restart local v4       #adjustedWidthSpec:I
    const/high16 v2, 0x4000

    #@18f
    invoke-static {v13, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@192
    move-result v6

    #@193
    .restart local v6       #adjustedHeightSpec:I
    goto :goto_13e

    #@194
    .line 322
    .end local v3           #child:Landroid/view/View;
    .end local v4           #adjustedWidthSpec:I
    .end local v6           #adjustedHeightSpec:I
    .end local v12           #lp:Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$LayoutParams;
    .end local v13           #virtualHeight:I
    :cond_194
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@6
    if-eq p1, v0, :cond_b

    #@8
    .line 167
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->hideBouncer()V

    #@b
    .line 169
    :cond_b
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@e
    .line 170
    return-void
.end method

.method public setOnBouncerStateChangedListener(Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@2
    .line 160
    return-void
.end method

.method setScrimView(Landroid/view/View;)V
    .registers 4
    .parameter "scrim"

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 174
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@a
    .line 176
    :cond_a
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@c
    .line 177
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@e
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@10
    if-eqz v0, :cond_2f

    #@12
    const/high16 v0, 0x3f80

    #@14
    :goto_14
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    #@17
    .line 178
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@19
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@1b
    if-eqz v0, :cond_31

    #@1d
    const/4 v0, 0x0

    #@1e
    :goto_1e
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    #@21
    .line 179
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@23
    const/4 v1, 0x1

    #@24
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    #@27
    .line 180
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@29
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimClickListener:Landroid/view/View$OnClickListener;

    #@2b
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2e
    .line 181
    return-void

    #@2f
    .line 177
    :cond_2f
    const/4 v0, 0x0

    #@30
    goto :goto_14

    #@31
    .line 178
    :cond_31
    const/4 v0, 0x4

    #@32
    goto :goto_1e
.end method

.method public showBouncer()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 106
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 126
    :cond_5
    :goto_5
    return-void

    #@6
    .line 107
    :cond_6
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mIsBouncing:Z

    #@8
    .line 108
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@a
    if-eqz v1, :cond_36

    #@c
    .line 109
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 110
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mChallengeView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;

    #@12
    const/16 v2, 0x15e

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->showBouncer(I)V

    #@17
    .line 113
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mScrimView:Landroid/view/View;

    #@19
    const-string v2, "alpha"

    #@1b
    new-array v3, v6, [F

    #@1d
    const/4 v4, 0x0

    #@1e
    const/high16 v5, 0x3f80

    #@20
    aput v5, v3, v4

    #@22
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@25
    move-result-object v0

    #@26
    .line 114
    .local v0, anim:Landroid/animation/Animator;
    const-wide/16 v1, 0x15e

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@2b
    .line 115
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$2;

    #@2d
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout$2;-><init>(Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;)V

    #@30
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@33
    .line 121
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@36
    .line 123
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_36
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@38
    if-eqz v1, :cond_5

    #@3a
    .line 124
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MultiPaneChallengeLayout;->mBouncerListener:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;

    #@3c
    invoke-interface {v1, v6}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;->onBouncerStateChanged(Z)V

    #@3f
    goto :goto_5
.end method

.method public showChallenge(Z)V
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 97
    return-void
.end method
