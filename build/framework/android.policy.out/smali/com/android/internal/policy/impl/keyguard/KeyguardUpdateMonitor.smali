.class public Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;
    }
.end annotation


# static fields
.field private static final ACTION_HOME_WIFI_CONNECTED:Ljava/lang/String; = "com.lge.qremote.settings.action.HomeWiFiConnected"

.field public static final ACTION_OTA_USIM_REFRESH_TO_RESET:Ljava/lang/String; = "android.intent.action.OTA_USIM_REFRESH_TO_RESET"

.field private static final DEBUG:Z = true

.field private static final DEBUG_SIM_STATES:Z = true

.field private static final FAILED_BIOMETRIC_UNLOCK_ATTEMPTS_BEFORE_BACKUP:I = 0x3

.field public static LOCKTIMER_STATE_DISABLE:I = 0x0

.field public static LOCKTIMER_STATE_END:I = 0x0

.field public static LOCKTIMER_STATE_NONE:I = 0x0

.field public static LOCKTIMER_STATE_START:I = 0x0

.field public static LOCKTIMER_STATE_STOPED:I = 0x0

.field private static final LOW_BATTERY_THRESHOLD:I = 0x4

.field private static final META_CHANGED:Ljava/lang/String; = "com.lge.music.metachanged"

.field private static final MSG_ACCESSORY_STATE_CHANGED:I = 0x13c

.field private static final MSG_BATTERY_UPDATE:I = 0x12e

.field protected static final MSG_BOOT_COMPLETED:I = 0x139

.field private static final MSG_CARRIER_INFO_UPDATE:I = 0x12f

.field private static final MSG_CLOCK_VISIBILITY_CHANGED:I = 0x133

.field private static final MSG_DEVICE_PROVISIONED:I = 0x134

.field private static final MSG_DPM_STATE_CHANGED:I = 0x135

.field private static final MSG_DSDP_STATE_CHANGED:I = 0x13d

.field private static final MSG_HDMI_PLUG_STATE_CHANGED:I = 0x13b

.field private static final MSG_HOME_WIFI_STATE_CHANGED:I = 0x191

.field private static final MSG_KEYGUARD_VISIBILITY_CHANGED:I = 0x138

.field private static final MSG_MUSIC_STATE_CHANGED:I = 0x192

.field private static final MSG_NEXT_ALARM:I = 0x13a

.field private static final MSG_PHONE_STATE_CHANGED:I = 0x132

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x131

.field private static final MSG_SIM_STATE_CHANGE:I = 0x130

.field private static final MSG_TIME_UPDATE:I = 0x12d

.field private static final MSG_USER_REMOVED:I = 0x137

.field private static final MSG_USER_SWITCHED:I = 0x136

.field private static final PLAYBACK_COMPLETE:Ljava/lang/String; = "com.lge.music.playbackcomplete"

.field private static final PLAYSTATE_CHANGED:Ljava/lang/String; = "com.lge.music.playstatechanged"

.field private static final TAG:Ljava/lang/String; = "KeyguardUpdateMonitor"

.field public static final isMultiSimEnabled:Z

.field private static mDsdpEnable:Z

.field public static mOTARefreshToReset:Z

.field public static mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

.field private static sInstance:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;


# instance fields
.field private NEXT_ALARM_CALL:Ljava/lang/String;

.field private bSupportChargingCurrent:Z

.field private getSimloaded:Z

.field private isHomeWifiConnected:Z

.field private isLgMusicplaying:Z

.field private mAlternateUnlockEnabled:Z

.field private mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

.field private mBootCompleted:Z

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field private mChargingCurrent:I

.field private mClockVisible:Z

.field private final mContext:Landroid/content/Context;

.field mCoverState:I

.field private mDeviceProvisioned:Z

.field private mDeviceProvisionedObserver:Landroid/database/ContentObserver;

.field private mDsdpOn:Z

.field private mFailedAttempts:I

.field private mFailedBiometricUnlockAttempts:I

.field private final mHandler:Landroid/os/Handler;

.field private mHdmiPlugged:Z

.field private mInputHalfFailedAttempts:Z

.field private mKeyguardIsVisible:Z

.field private mLockTimerState:I

.field private mPhoneState:I

.field private mRingMode:I

.field private mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

.field private mTelephonyPlmn:[Ljava/lang/CharSequence;

.field private mTelephonySpn:[Ljava/lang/CharSequence;

.field private mUsimService:Lcom/android/internal/telephony/uicc/UsimService;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 124
    sput-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpEnable:Z

    #@3
    .line 176
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@a
    move-result v0

    #@b
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@d
    .line 191
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_DEFAULT:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@f
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@11
    .line 195
    sput-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mOTARefreshToReset:Z

    #@13
    .line 1451
    sput v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@15
    .line 1452
    const/4 v0, 0x1

    #@16
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_START:I

    #@18
    .line 1453
    const/4 v0, 0x2

    #@19
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_STOPED:I

    #@1b
    .line 1454
    const/4 v0, 0x3

    #@1c
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@1e
    .line 1455
    const/4 v0, 0x4

    #@1f
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@21
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 652
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 116
    const-string v5, "alarm.current.nextAlarm"

    #@7
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->NEXT_ALARM_CALL:Ljava/lang/String;

    #@9
    .line 123
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpOn:Z

    #@b
    .line 135
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isLgMusicplaying:Z

    #@d
    .line 136
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected:Z

    #@f
    .line 142
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mInputHalfFailedAttempts:Z

    #@11
    .line 143
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@13
    .line 157
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->bSupportChargingCurrent:Z

    #@15
    .line 158
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mChargingCurrent:I

    #@17
    .line 164
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@19
    .line 165
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@1b
    .line 171
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@1e
    move-result-object v5

    #@1f
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@21
    .line 192
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@23
    .line 197
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimloaded:Z

    #@25
    .line 318
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;

    #@27
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@2a
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@2c
    .line 392
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;

    #@2e
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@31
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@33
    .line 1456
    sget v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@35
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mLockTimerState:I

    #@37
    .line 1472
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCoverState:I

    #@39
    .line 653
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@3b
    .line 655
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisionedInSettingsDb()Z

    #@3e
    move-result v5

    #@3f
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@41
    .line 658
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@43
    if-nez v5, :cond_48

    #@45
    .line 659
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->watchForDeviceProvisioning()V

    #@48
    .line 661
    :cond_48
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4d
    move-result-object v5

    #@4e
    const v6, 0x2060029

    #@51
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@54
    move-result v5

    #@55
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->bSupportChargingCurrent:Z

    #@57
    .line 664
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@59
    const/4 v6, 0x1

    #@5a
    const/16 v7, 0x64

    #@5c
    invoke-direct {v5, v6, v7, v8, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;-><init>(IIII)V

    #@5f
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@61
    .line 667
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@68
    move-result v4

    #@69
    .line 669
    .local v4, numPhones:I
    new-array v5, v4, [Ljava/lang/CharSequence;

    #@6b
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@6d
    .line 670
    new-array v5, v4, [Ljava/lang/CharSequence;

    #@6f
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@71
    .line 671
    new-array v5, v4, [Lcom/android/internal/telephony/IccCardConstants$State;

    #@73
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@75
    .line 672
    const/4 v3, 0x0

    #@76
    .local v3, i:I
    :goto_76
    if-ge v3, v4, :cond_8d

    #@78
    .line 673
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@7a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    #@7d
    move-result-object v6

    #@7e
    aput-object v6, v5, v3

    #@80
    .line 674
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@82
    aput-object v9, v5, v3

    #@84
    .line 675
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@86
    sget-object v6, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@88
    aput-object v6, v5, v3

    #@8a
    .line 672
    add-int/lit8 v3, v3, 0x1

    #@8c
    goto :goto_76

    #@8d
    .line 679
    :cond_8d
    new-instance v2, Landroid/content/IntentFilter;

    #@8f
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@92
    .line 680
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.TIME_TICK"

    #@94
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@97
    .line 681
    const-string v5, "android.intent.action.TIME_SET"

    #@99
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9c
    .line 682
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    #@9e
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a1
    .line 683
    const-string v5, "android.intent.action.TIMEZONE_CHANGED"

    #@a3
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a6
    .line 684
    const-string v5, "android.intent.action.SIM_STATE_CHANGED"

    #@a8
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ab
    .line 685
    const-string v5, "android.intent.action.PHONE_STATE"

    #@ad
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b0
    .line 686
    const-string v5, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@b2
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b5
    .line 687
    const-string v5, "android.media.RINGER_MODE_CHANGED"

    #@b7
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ba
    .line 688
    const-string v5, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    #@bc
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@bf
    .line 689
    const-string v5, "android.intent.action.USER_REMOVED"

    #@c1
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c4
    .line 690
    const-string v5, "android.intent.action.HDMI_PLUGGED"

    #@c6
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c9
    .line 691
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->NEXT_ALARM_CALL:Ljava/lang/String;

    #@cb
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ce
    .line 692
    const-string v5, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@d0
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d3
    .line 694
    const-string v5, "com.lge.intent.action.LG_DUAL_SCREEN_STATUS_CHANGED"

    #@d5
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d8
    .line 697
    const-string v5, "com.lge.qremote.settings.action.HomeWiFiConnected"

    #@da
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@dd
    .line 698
    const-string v5, "android.net.wifi.STATE_CHANGE"

    #@df
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@e2
    .line 699
    const-string v5, "com.lge.music.playstatechanged"

    #@e4
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@e7
    .line 700
    const-string v5, "com.lge.music.metachanged"

    #@e9
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ec
    .line 701
    const-string v5, "com.lge.music.playbackcomplete"

    #@ee
    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f1
    .line 703
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@f3
    invoke-virtual {p1, v5, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@f6
    .line 705
    new-instance v0, Landroid/content/IntentFilter;

    #@f8
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@fb
    .line 706
    .local v0, bootCompleteFilter:Landroid/content/IntentFilter;
    const/16 v5, 0x3e8

    #@fd
    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->setPriority(I)V

    #@100
    .line 707
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    #@102
    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@105
    .line 708
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@107
    invoke-virtual {p1, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@10a
    .line 711
    :try_start_10a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@10d
    move-result-object v5

    #@10e
    new-instance v6, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$3;

    #@110
    invoke-direct {v6, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V

    #@113
    invoke-interface {v5, v6}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V
    :try_end_116
    .catch Landroid/os/RemoteException; {:try_start_10a .. :try_end_116} :catch_117

    #@116
    .line 726
    :goto_116
    return-void

    #@117
    .line 722
    :catch_117
    move-exception v1

    #@118
    .line 724
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@11b
    goto :goto_116
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleTimeUpdate()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleBatteryUpdate(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)[Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->bSupportChargingCurrent:Z

    #@2
    return v0
.end method

.method static synthetic access$1402(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mChargingCurrent:I

    #@2
    return p1
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->NEXT_ALARM_CALL:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1600()Z
    .registers 1

    #@0
    .prologue
    .line 92
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpEnable:Z

    #@2
    return v0
.end method

.method static synthetic access$1602(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 92
    sput-boolean p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpEnable:Z

    #@2
    return p0
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$1702(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$1802(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isLgMusicplaying:Z

    #@2
    return p1
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return v0
.end method

.method static synthetic access$1902(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleCarrierInfoUpdate(I)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisionedInSettingsDb()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleClockVisibilityChanged()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleKeyguardVisibilityChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleAccessoryStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleInstantlyLgWidgetStateChanged()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)[Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method private getDefaultPlmn()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 1129
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x104030b

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 646
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->sInstance:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 647
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->sInstance:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@b
    .line 649
    :cond_b
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->sInstance:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@d
    return-object v0
.end method

.method private getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 1118
    const-string v1, "showPlmn"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_17

    #@9
    .line 1119
    const-string v1, "plmn"

    #@b
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1120
    .local v0, plmn:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@11
    .line 1122
    .end local v0           #plmn:Ljava/lang/String;
    :goto_11
    return-object v0

    #@12
    .line 1120
    .restart local v0       #plmn:Ljava/lang/String;
    :cond_12
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    #@15
    move-result-object v0

    #@16
    goto :goto_11

    #@17
    .line 1122
    .end local v0           #plmn:Ljava/lang/String;
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_11
.end method

.method private getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 1137
    const-string v1, "showSpn"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_12

    #@9
    .line 1138
    const-string v1, "spn"

    #@b
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1139
    .local v0, spn:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@11
    .line 1143
    .end local v0           #spn:Ljava/lang/String;
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private handleAccessoryStateChanged(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 1529
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    const-string v3, "handleAccessoryStateChanged()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1530
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v2

    #@e
    if-ge v1, v2, :cond_26

    #@10
    .line 1531
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@18
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1e
    .line 1532
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_23

    #@20
    .line 1533
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onAccessoryStateChanged(I)V

    #@23
    .line 1530
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_8

    #@26
    .line 1536
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_26
    return-void
.end method

.method private handleBatteryUpdate(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 8
    .parameter "status"

    #@0
    .prologue
    .line 960
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@2
    invoke-static {v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isBatteryUpdateInteresting(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)Z

    #@5
    move-result v0

    #@6
    .line 961
    .local v0, batteryUpdateInteresting:Z
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@8
    .line 962
    if-eqz v0, :cond_59

    #@a
    .line 964
    const-string v3, "KeyguardUpdateMonitor"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "handleBatteryUpdate ("

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@19
    iget v5, v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->plugged:I

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    const-string v5, ") ("

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@27
    iget v5, v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, ")"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 966
    const/4 v2, 0x0

    #@3b
    .local v2, i:I
    :goto_3b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@40
    move-result v3

    #@41
    if-ge v2, v3, :cond_59

    #@43
    .line 967
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@45
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@48
    move-result-object v3

    #@49
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@4b
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@4e
    move-result-object v1

    #@4f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@51
    .line 968
    .local v1, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v1, :cond_56

    #@53
    .line 969
    invoke-virtual {v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V

    #@56
    .line 966
    :cond_56
    add-int/lit8 v2, v2, 0x1

    #@58
    goto :goto_3b

    #@59
    .line 973
    .end local v1           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    .end local v2           #i:I
    :cond_59
    return-void
.end method

.method private handleCarrierInfoUpdate(I)V
    .registers 7
    .parameter "subscription"

    #@0
    .prologue
    .line 979
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "handleCarrierInfoUpdate: plmn = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@f
    aget-object v4, v4, p1

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, ", spn = "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@1d
    aget-object v4, v4, p1

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, ", subscription = "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 982
    const/4 v1, 0x0

    #@35
    .local v1, i:I
    :goto_35
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v2

    #@3b
    if-ge v1, v2, :cond_6b

    #@3d
    .line 983
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v2

    #@43
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@45
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@4b
    .line 984
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_5c

    #@4d
    .line 985
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@4f
    if-eqz v2, :cond_5f

    #@51
    .line 986
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@53
    aget-object v2, v2, p1

    #@55
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@57
    aget-object v3, v3, p1

    #@59
    invoke-virtual {v0, v2, v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    #@5c
    .line 982
    :cond_5c
    :goto_5c
    add-int/lit8 v1, v1, 0x1

    #@5e
    goto :goto_35

    #@5f
    .line 989
    :cond_5f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@61
    aget-object v2, v2, p1

    #@63
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@65
    aget-object v3, v3, p1

    #@67
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@6a
    goto :goto_5c

    #@6b
    .line 994
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_6b
    return-void
.end method

.method private handleClockVisibilityChanged()V
    .registers 5

    #@0
    .prologue
    .line 1029
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    const-string v3, "handleClockVisibilityChanged()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1030
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v2

    #@e
    if-ge v1, v2, :cond_26

    #@10
    .line 1031
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@18
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1e
    .line 1032
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_23

    #@20
    .line 1033
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onClockVisibilityChanged()V

    #@23
    .line 1030
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_8

    #@26
    .line 1036
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_26
    return-void
.end method

.method private handleInstantlyLgWidgetStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 1055
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 1056
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@17
    .line 1057
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1c

    #@19
    .line 1058
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onInstantlyLgWidgetStateChanged()V

    #@1c
    .line 1055
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 1061
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_1f
    return-void
.end method

.method private handleKeyguardVisibilityChanged(I)V
    .registers 6
    .parameter "showing"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1043
    if-ne p1, v2, :cond_24

    #@3
    .line 1044
    .local v2, isShowing:Z
    :goto_3
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mKeyguardIsVisible:Z

    #@5
    .line 1045
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v1, v3, :cond_26

    #@e
    .line 1046
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@16
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1c
    .line 1047
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_21

    #@1e
    .line 1048
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onKeyguardVisibilityChanged(Z)V

    #@21
    .line 1045
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_6

    #@24
    .line 1043
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    .end local v1           #i:I
    .end local v2           #isShowing:Z
    :cond_24
    const/4 v2, 0x0

    #@25
    goto :goto_3

    #@26
    .line 1051
    .restart local v1       #i:I
    .restart local v2       #isShowing:Z
    :cond_26
    return-void
.end method

.method private handleSimStateChange(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;)V
    .registers 9
    .parameter "simArgs"

    #@0
    .prologue
    .line 1000
    iget-object v2, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    .line 1001
    .local v2, state:Lcom/android/internal/telephony/IccCardConstants$State;
    iget v3, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->subscription:I

    #@4
    .line 1003
    .local v3, subscription:I
    const-string v4, "KeyguardUpdateMonitor"

    #@6
    new-instance v5, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v6, "handleSimStateChange: intentValue = "

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, " "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    const-string v6, "state resolved to "

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v2}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, " "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    const-string v6, "subscription ="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1007
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@42
    if-eq v2, v4, :cond_97

    #@44
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@46
    aget-object v4, v4, v3

    #@48
    if-eq v2, v4, :cond_97

    #@4a
    .line 1008
    const-string v4, "KeyguardUpdateMonitor"

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "dispatching state: "

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    const-string v6, "subscription: "

    #@5d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 1011
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@6e
    aput-object v2, v4, v3

    #@70
    .line 1012
    const/4 v1, 0x0

    #@71
    .local v1, i:I
    :goto_71
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@73
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@76
    move-result v4

    #@77
    if-ge v1, v4, :cond_97

    #@79
    .line 1013
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7e
    move-result-object v4

    #@7f
    check-cast v4, Ljava/lang/ref/WeakReference;

    #@81
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@84
    move-result-object v0

    #@85
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@87
    .line 1014
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_90

    #@89
    .line 1015
    sget-boolean v4, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@8b
    if-eqz v4, :cond_93

    #@8d
    .line 1016
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@90
    .line 1012
    :cond_90
    :goto_90
    add-int/lit8 v1, v1, 0x1

    #@92
    goto :goto_71

    #@93
    .line 1018
    :cond_93
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@96
    goto :goto_90

    #@97
    .line 1023
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    .end local v1           #i:I
    :cond_97
    return-void
.end method

.method private handleTimeUpdate()V
    .registers 5

    #@0
    .prologue
    .line 946
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    const-string v3, "handleTimeUpdate"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 947
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v2

    #@e
    if-ge v1, v2, :cond_26

    #@10
    .line 948
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@18
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1e
    .line 949
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_23

    #@20
    .line 950
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onTimeChanged()V

    #@23
    .line 947
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_8

    #@26
    .line 953
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_26
    return-void
.end method

.method private static isBatteryUpdateInteresting(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)Z
    .registers 9
    .parameter "old"
    .parameter "current"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1090
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@5
    move-result v0

    #@6
    .line 1091
    .local v0, nowPluggedIn:Z
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@9
    move-result v2

    #@a
    .line 1092
    .local v2, wasPluggedIn:Z
    if-ne v2, v3, :cond_1a

    #@c
    if-ne v0, v3, :cond_1a

    #@e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@10
    iget v6, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@12
    if-eq v5, v6, :cond_1a

    #@14
    move v1, v3

    #@15
    .line 1097
    .local v1, stateChangedWhilePluggedIn:Z
    :goto_15
    if-ne v2, v0, :cond_19

    #@17
    if-eqz v1, :cond_1c

    #@19
    .line 1110
    :cond_19
    :goto_19
    return v3

    #@1a
    .end local v1           #stateChangedWhilePluggedIn:Z
    :cond_1a
    move v1, v4

    #@1b
    .line 1092
    goto :goto_15

    #@1c
    .line 1102
    .restart local v1       #stateChangedWhilePluggedIn:Z
    :cond_1c
    if-eqz v0, :cond_24

    #@1e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@20
    iget v6, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@22
    if-ne v5, v6, :cond_19

    #@24
    .line 1107
    :cond_24
    if-nez v0, :cond_32

    #@26
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->isBatteryLow()Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_32

    #@2c
    iget v5, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@2e
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@30
    if-ne v5, v6, :cond_19

    #@32
    :cond_32
    move v3, v4

    #@33
    .line 1110
    goto :goto_19
.end method

.method private isDeviceProvisionedInSettingsDb()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 729
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "device_provisioned"

    #@9
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_10

    #@f
    const/4 v0, 0x1

    #@10
    :cond_10
    return v0
.end method

.method public static isSimLocked(Lcom/android/internal/telephony/IccCardConstants$State;)Z
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 1369
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    if-eq p0, v0, :cond_c

    #@4
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@6
    if-eq p0, v0, :cond_c

    #@8
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a
    if-ne p0, v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public static isSimPinSecure(Lcom/android/internal/telephony/IccCardConstants$State;)Z
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1389
    move-object v0, p0

    #@1
    .line 1390
    .local v0, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3
    if-eq v0, v1, :cond_15

    #@5
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7
    if-eq v0, v1, :cond_15

    #@9
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@b
    if-eq v0, v1, :cond_15

    #@d
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@f
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@11
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_DOING_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@13
    if-ne v1, v2, :cond_17

    #@15
    :cond_15
    const/4 v1, 0x1

    #@16
    :goto_16
    return v1

    #@17
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_16
.end method

.method private isSupportQuickCover()Z
    .registers 3

    #@0
    .prologue
    .line 1545
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    .line 1546
    .local v0, res:Landroid/content/res/Resources;
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@8
    if-eqz v1, :cond_1c

    #@a
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@d
    move-result-object v1

    #@e
    if-eqz v1, :cond_1c

    #@10
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isSupportQuickCover()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    const/4 v1, 0x1

    #@1b
    :goto_1b
    return v1

    #@1c
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_1b
.end method

.method private isSupportQuickCoverWindow()Z
    .registers 3

    #@0
    .prologue
    .line 1539
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    .line 1540
    .local v0, res:Landroid/content/res/Resources;
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@8
    if-eqz v1, :cond_1c

    #@a
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@d
    move-result-object v1

    #@e
    if-eqz v1, :cond_1c

    #@10
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isSupportQuickCoverWindow()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    const/4 v1, 0x1

    #@1b
    :goto_1b
    return v1

    #@1c
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_1b
.end method

.method private sendUpdates(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V
    .registers 6
    .parameter "callback"

    #@0
    .prologue
    .line 1182
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@2
    invoke-virtual {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;)V

    #@5
    .line 1183
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onTimeChanged()V

    #@8
    .line 1184
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mRingMode:I

    #@a
    invoke-virtual {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRingerModeChanged(I)V

    #@d
    .line 1185
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@f
    invoke-virtual {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onPhoneStateChanged(I)V

    #@12
    .line 1186
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@19
    move-result v1

    #@1a
    .line 1187
    .local v1, subscription:I
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@1c
    if-eqz v2, :cond_3e

    #@1e
    .line 1188
    const/4 v0, 0x0

    #@1f
    .local v0, i:I
    :goto_1f
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@26
    move-result v2

    #@27
    if-ge v0, v2, :cond_50

    #@29
    .line 1189
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@2b
    aget-object v2, v2, v0

    #@2d
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@2f
    aget-object v3, v3, v0

    #@31
    invoke-virtual {p1, v2, v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    #@34
    .line 1190
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@36
    aget-object v2, v2, v0

    #@38
    invoke-virtual {p1, v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@3b
    .line 1188
    add-int/lit8 v0, v0, 0x1

    #@3d
    goto :goto_1f

    #@3e
    .line 1193
    .end local v0           #i:I
    :cond_3e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@40
    aget-object v2, v2, v1

    #@42
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@44
    aget-object v3, v3, v1

    #@46
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@49
    .line 1195
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@4b
    aget-object v2, v2, v1

    #@4d
    invoke-virtual {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@50
    .line 1197
    :cond_50
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onClockVisibilityChanged()V

    #@53
    .line 1198
    return-void
.end method

.method private watchForDeviceProvisioning()V
    .registers 6

    #@0
    .prologue
    .line 734
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$4;

    #@2
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@4
    invoke-direct {v1, p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Landroid/os/Handler;)V

    #@7
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisionedObserver:Landroid/database/ContentObserver;

    #@9
    .line 746
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v1

    #@f
    const-string v2, "device_provisioned"

    #@11
    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@14
    move-result-object v2

    #@15
    const/4 v3, 0x0

    #@16
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisionedObserver:Landroid/database/ContentObserver;

    #@18
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1b
    .line 752
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisionedInSettingsDb()Z

    #@1e
    move-result v0

    #@1f
    .line 753
    .local v0, provisioned:Z
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@21
    if-eq v0, v1, :cond_36

    #@23
    .line 754
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@25
    .line 755
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@27
    if-eqz v1, :cond_36

    #@29
    .line 756
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@2b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@2d
    const/16 v3, 0x134

    #@2f
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@36
    .line 759
    :cond_36
    return-void
.end method


# virtual methods
.method public clearFailedUnlockAttempts()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1321
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@3
    .line 1322
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@5
    .line 1323
    return-void
.end method

.method public getChargingCurrent()I
    .registers 2

    #@0
    .prologue
    .line 1553
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->bSupportChargingCurrent:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1554
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mChargingCurrent:I

    #@6
    .line 1557
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method protected getCoverType()I
    .registers 5

    #@0
    .prologue
    .line 1507
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "cover_type"

    #@8
    const/4 v3, 0x0

    #@9
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v0

    #@d
    .line 1510
    .local v0, cover_type:I
    const-string v1, "KeyguardUpdateMonitor"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "getCoverType : "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1512
    return v0
.end method

.method public getFailedUnlockAttempts()I
    .registers 2

    #@0
    .prologue
    .line 1317
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@2
    return v0
.end method

.method public getInputHalfFailedAttempts()Z
    .registers 2

    #@0
    .prologue
    .line 1413
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mInputHalfFailedAttempts:Z

    #@2
    return v0
.end method

.method public getLockTimerState()I
    .registers 2

    #@0
    .prologue
    .line 1459
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mLockTimerState:I

    #@2
    return v0
.end method

.method public getMaxBiometricUnlockAttemptsReached()Z
    .registers 3

    #@0
    .prologue
    .line 1342
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@2
    const/4 v1, 0x3

    #@3
    if-lt v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getPhoneState()I
    .registers 2

    #@0
    .prologue
    .line 1334
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@2
    return v0
.end method

.method public getPinLockedSubscription()I
    .registers 4

    #@0
    .prologue
    .line 1223
    const/4 v0, 0x0

    #@1
    .line 1224
    .local v0, sub:I
    const/4 v0, 0x0

    #@2
    :goto_2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@4
    array-length v1, v1

    #@5
    if-ge v0, v1, :cond_f

    #@7
    .line 1225
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@9
    aget-object v1, v1, v0

    #@b
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@d
    if-ne v1, v2, :cond_10

    #@f
    .line 1229
    :cond_f
    return v0

    #@10
    .line 1224
    :cond_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_2
.end method

.method public getPukLockedSubscription()I
    .registers 4

    #@0
    .prologue
    .line 1233
    const/4 v0, 0x0

    #@1
    .line 1234
    .local v0, sub:I
    const/4 v0, 0x0

    #@2
    :goto_2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@4
    array-length v1, v1

    #@5
    if-ge v0, v1, :cond_f

    #@7
    .line 1235
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@9
    aget-object v1, v1, v0

    #@b
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@d
    if-ne v1, v2, :cond_10

    #@f
    .line 1239
    :cond_f
    return v0

    #@10
    .line 1234
    :cond_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_2
.end method

.method public getSimState()Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2

    #@0
    .prologue
    .line 1215
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 1219
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getTelephonyPlmn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1293
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getTelephonyPlmn(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getTelephonyPlmn(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 1297
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getTelephonySpn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1301
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getTelephonySpn(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getTelephonySpn(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 1305
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getUsimIsEmpty()I
    .registers 6

    #@0
    .prologue
    .line 300
    const/4 v1, 0x0

    #@1
    .line 301
    .local v1, isUsimEmpty:I
    const-string v2, "KeyguardUpdateMonitor"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "getUsimIsEmpty() getUsimStateLoaded() : "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimStateLoaded()Z

    #@11
    move-result v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 302
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimStateLoaded()Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_4c

    #@23
    .line 304
    :try_start_23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@25
    if-nez v2, :cond_2e

    #@27
    .line 305
    new-instance v2, Lcom/android/internal/telephony/uicc/UsimService;

    #@29
    invoke-direct {v2}, Lcom/android/internal/telephony/uicc/UsimService;-><init>()V

    #@2c
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@2e
    .line 306
    :cond_2e
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimService:Lcom/android/internal/telephony/uicc/UsimService;

    #@30
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UsimService;->getUsimIsEmpty()I
    :try_end_33
    .catch Ljava/lang/RuntimeException; {:try_start_23 .. :try_end_33} :catch_4d

    #@33
    move-result v1

    #@34
    .line 313
    :goto_34
    const-string v2, "KeyguardUpdateMonitor"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "getUsimIsEmpty() : "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 315
    :cond_4c
    return v1

    #@4d
    .line 307
    :catch_4d
    move-exception v0

    #@4e
    .line 309
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "KeyguardUpdateMonitor"

    #@50
    const-string v3, "[UsimService] exception occurred while processing ReadFromSIM method - getUsimIsEmpty()"

    #@52
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 311
    const/4 v1, 0x0

    #@56
    goto :goto_34
.end method

.method public getUsimStateLoaded()Z
    .registers 4

    #@0
    .prologue
    .line 199
    const-string v0, "KeyguardUpdateMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getUsimStateLoaded() getSimloaded : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimloaded:Z

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 200
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimloaded:Z

    #@1c
    return v0
.end method

.method public getUsimType()I
    .registers 5

    #@0
    .prologue
    .line 287
    const-string v1, "KeyguardUpdateMonitor"

    #@2
    const-string v2, "########## NO MORE USE THIS METHOD : UsimService.getUsimType() ##########"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 288
    const-string v1, "KeyguardUpdateMonitor"

    #@9
    const-string v2, "########## USE SYSTEM PROPERTY     : gsm.sim.type ###########"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 289
    const-string v1, "gsm.sim.type"

    #@10
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 290
    .local v0, gsmSimType:Ljava/lang/String;
    const-string v1, "KeyguardUpdateMonitor"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "getUsimType() : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 291
    const-string v1, "skt"

    #@2e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_36

    #@34
    const/4 v1, 0x1

    #@35
    .line 296
    :goto_35
    return v1

    #@36
    .line 292
    :cond_36
    const-string v1, "kt"

    #@38
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_40

    #@3e
    const/4 v1, 0x2

    #@3f
    goto :goto_35

    #@40
    .line 293
    :cond_40
    const-string v1, "lgu"

    #@42
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_4a

    #@48
    const/4 v1, 0x5

    #@49
    goto :goto_35

    #@4a
    .line 294
    :cond_4a
    const-string v1, "test"

    #@4c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_54

    #@52
    const/4 v1, 0x4

    #@53
    goto :goto_35

    #@54
    .line 295
    :cond_54
    const-string v1, "unknown"

    #@56
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v1

    #@5a
    if-eqz v1, :cond_5e

    #@5c
    const/4 v1, 0x3

    #@5d
    goto :goto_35

    #@5e
    .line 296
    :cond_5e
    const/4 v1, 0x0

    #@5f
    goto :goto_35
.end method

.method protected handleBootCompleted()V
    .registers 4

    #@0
    .prologue
    .line 794
    const/4 v2, 0x1

    #@1
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBootCompleted:Z

    #@3
    .line 795
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    if-ge v1, v2, :cond_22

    #@c
    .line 796
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@14
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@1a
    .line 797
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1f

    #@1c
    .line 798
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onBootCompleted()V

    #@1f
    .line 795
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_4

    #@22
    .line 801
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_22
    return-void
.end method

.method protected handleDSDPStateChanged(Ljava/lang/Boolean;)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 831
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@3
    move-result v0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpOn:Z

    #@6
    .line 833
    const-string v0, "KeyguardUpdateMonitor"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "handleDSDPStateChanged("

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpOn:Z

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ")"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 835
    return-void
.end method

.method protected handleDevicePolicyManagerStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 765
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_20

    #@a
    .line 766
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@12
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@18
    .line 767
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1d

    #@1a
    .line 768
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onDevicePolicyManagerStateChanged()V

    #@1d
    .line 765
    :cond_1d
    add-int/lit8 v1, v1, -0x1

    #@1f
    goto :goto_8

    #@20
    .line 771
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_20
    return-void
.end method

.method protected handleDeviceProvisioned()V
    .registers 5

    #@0
    .prologue
    .line 873
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 874
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@17
    .line 875
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1c

    #@19
    .line 876
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onDeviceProvisioned()V

    #@1c
    .line 873
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 879
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_1f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisionedObserver:Landroid/database/ContentObserver;

    #@21
    if-eqz v2, :cond_31

    #@23
    .line 881
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v2

    #@29
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisionedObserver:Landroid/database/ContentObserver;

    #@2b
    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@2e
    .line 882
    const/4 v2, 0x0

    #@2f
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisionedObserver:Landroid/database/ContentObserver;

    #@31
    .line 884
    :cond_31
    return-void
.end method

.method protected handleHdmiPlugStateChanged(Ljava/lang/Boolean;)V
    .registers 7
    .parameter "connected"

    #@0
    .prologue
    .line 808
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_4a

    #@6
    .line 809
    const/4 v2, 0x1

    #@7
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@9
    .line 814
    :goto_9
    const-string v2, "KeyguardUpdateMonitor"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "handleHdmiPlugStateChanged("

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, ")"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 817
    const/4 v1, 0x0

    #@2a
    .local v1, i:I
    :goto_2a
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2f
    move-result v2

    #@30
    if-ge v1, v2, :cond_4e

    #@32
    .line 818
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v2

    #@38
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@3a
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@3d
    move-result-object v0

    #@3e
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@40
    .line 819
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_47

    #@42
    .line 820
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@44
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onHdmiPlugStateChanged(Z)V

    #@47
    .line 817
    :cond_47
    add-int/lit8 v1, v1, 0x1

    #@49
    goto :goto_2a

    #@4a
    .line 811
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    .end local v1           #i:I
    :cond_4a
    const/4 v2, 0x0

    #@4b
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@4d
    goto :goto_9

    #@4e
    .line 823
    .restart local v1       #i:I
    :cond_4e
    return-void
.end method

.method protected handleNextAlarm()V
    .registers 4

    #@0
    .prologue
    .line 840
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 841
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@17
    .line 842
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1c

    #@19
    .line 843
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onAlarmChanged()V

    #@1c
    .line 840
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 846
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_1f
    return-void
.end method

.method protected handlePhoneStateChanged(Ljava/lang/String;)V
    .registers 10
    .parameter "newState"

    #@0
    .prologue
    .line 890
    const-string v5, "KeyguardUpdateMonitor"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "handlePhoneStateChanged("

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, ")"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 891
    sget-object v5, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@20
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_4a

    #@26
    .line 892
    const/4 v5, 0x0

    #@27
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@29
    .line 898
    :cond_29
    :goto_29
    const/4 v2, 0x0

    #@2a
    .local v2, i:I
    :goto_2a
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2f
    move-result v5

    #@30
    if-ge v2, v5, :cond_62

    #@32
    .line 899
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v5

    #@38
    check-cast v5, Ljava/lang/ref/WeakReference;

    #@3a
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@3d
    move-result-object v0

    #@3e
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@40
    .line 900
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_47

    #@42
    .line 901
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@44
    invoke-virtual {v0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onPhoneStateChanged(I)V

    #@47
    .line 898
    :cond_47
    add-int/lit8 v2, v2, 0x1

    #@49
    goto :goto_2a

    #@4a
    .line 893
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    .end local v2           #i:I
    :cond_4a
    sget-object v5, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@4c
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v5

    #@50
    if-eqz v5, :cond_56

    #@52
    .line 894
    const/4 v5, 0x2

    #@53
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@55
    goto :goto_29

    #@56
    .line 895
    :cond_56
    sget-object v5, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@58
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v5

    #@5c
    if-eqz v5, :cond_29

    #@5e
    .line 896
    const/4 v5, 0x1

    #@5f
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mPhoneState:I

    #@61
    goto :goto_29

    #@62
    .line 905
    .restart local v2       #i:I
    :cond_62
    const/4 v3, 0x0

    #@63
    .line 907
    .local v3, touchCall:Ljava/io/BufferedWriter;
    :try_start_63
    new-instance v4, Ljava/io/BufferedWriter;

    #@65
    new-instance v5, Ljava/io/FileWriter;

    #@67
    new-instance v6, Ljava/lang/String;

    #@69
    const-string v7, "/sys/devices/virtual/input/lge_touch/incoming_call"

    #@6b
    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@6e
    invoke-direct {v5, v6}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@71
    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_74
    .catchall {:try_start_63 .. :try_end_74} :catchall_121
    .catch Ljava/lang/Exception; {:try_start_63 .. :try_end_74} :catch_123

    #@74
    .line 908
    .end local v3           #touchCall:Ljava/io/BufferedWriter;
    .local v4, touchCall:Ljava/io/BufferedWriter;
    :try_start_74
    const-string v5, "RINGING"

    #@76
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v5

    #@7a
    if-eqz v5, :cond_a2

    #@7c
    .line 909
    if-eqz v4, :cond_83

    #@7e
    const-string v5, "1"

    #@80
    invoke-virtual {v4, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@83
    .line 910
    :cond_83
    const-string v5, "KeyguardUpdateMonitor"

    #@85
    new-instance v6, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v7, "Incoming Call Flag set 1 "

    #@8c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v6

    #@94
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v6

    #@98
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9b
    .catchall {:try_start_74 .. :try_end_9b} :catchall_114
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_9b} :catch_ca

    #@9b
    .line 922
    :cond_9b
    :goto_9b
    if-eqz v4, :cond_a0

    #@9d
    :try_start_9d
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_a0
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a0} :catch_11c

    #@a0
    :cond_a0
    move-object v3, v4

    #@a1
    .line 926
    .end local v4           #touchCall:Ljava/io/BufferedWriter;
    .restart local v3       #touchCall:Ljava/io/BufferedWriter;
    :cond_a1
    :goto_a1
    return-void

    #@a2
    .line 911
    .end local v3           #touchCall:Ljava/io/BufferedWriter;
    .restart local v4       #touchCall:Ljava/io/BufferedWriter;
    :cond_a2
    :try_start_a2
    const-string v5, "OFFHOOK"

    #@a4
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v5

    #@a8
    if-eqz v5, :cond_ec

    #@aa
    .line 912
    if-eqz v4, :cond_b1

    #@ac
    const-string v5, "0"

    #@ae
    invoke-virtual {v4, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@b1
    .line 913
    :cond_b1
    const-string v5, "KeyguardUpdateMonitor"

    #@b3
    new-instance v6, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v7, "Incoming Call Flag set 0 "

    #@ba
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v6

    #@c2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v6

    #@c6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c9
    .catchall {:try_start_a2 .. :try_end_c9} :catchall_114
    .catch Ljava/lang/Exception; {:try_start_a2 .. :try_end_c9} :catch_ca

    #@c9
    goto :goto_9b

    #@ca
    .line 918
    :catch_ca
    move-exception v1

    #@cb
    move-object v3, v4

    #@cc
    .line 919
    .end local v4           #touchCall:Ljava/io/BufferedWriter;
    .local v1, e:Ljava/lang/Exception;
    .restart local v3       #touchCall:Ljava/io/BufferedWriter;
    :goto_cc
    :try_start_cc
    const-string v5, "Touch"

    #@ce
    new-instance v6, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v7, "Incoming Call write fail"

    #@d5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v6

    #@d9
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v6

    #@dd
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v6

    #@e1
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e4
    .catchall {:try_start_cc .. :try_end_e4} :catchall_121

    #@e4
    .line 922
    if-eqz v3, :cond_a1

    #@e6
    :try_start_e6
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_e9
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_e9} :catch_ea

    #@e9
    goto :goto_a1

    #@ea
    .line 923
    :catch_ea
    move-exception v5

    #@eb
    goto :goto_a1

    #@ec
    .line 914
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #touchCall:Ljava/io/BufferedWriter;
    .restart local v4       #touchCall:Ljava/io/BufferedWriter;
    :cond_ec
    :try_start_ec
    const-string v5, "IDLE"

    #@ee
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f1
    move-result v5

    #@f2
    if-eqz v5, :cond_9b

    #@f4
    .line 915
    if-eqz v4, :cond_fb

    #@f6
    const-string v5, "0"

    #@f8
    invoke-virtual {v4, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@fb
    .line 916
    :cond_fb
    const-string v5, "KeyguardUpdateMonitor"

    #@fd
    new-instance v6, Ljava/lang/StringBuilder;

    #@ff
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@102
    const-string v7, "Incoming Call Flag set 0 "

    #@104
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v6

    #@108
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v6

    #@10c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v6

    #@110
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_113
    .catchall {:try_start_ec .. :try_end_113} :catchall_114
    .catch Ljava/lang/Exception; {:try_start_ec .. :try_end_113} :catch_ca

    #@113
    goto :goto_9b

    #@114
    .line 921
    :catchall_114
    move-exception v5

    #@115
    move-object v3, v4

    #@116
    .line 922
    .end local v4           #touchCall:Ljava/io/BufferedWriter;
    .restart local v3       #touchCall:Ljava/io/BufferedWriter;
    :goto_116
    if-eqz v3, :cond_11b

    #@118
    :try_start_118
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_11b
    .catch Ljava/lang/Exception; {:try_start_118 .. :try_end_11b} :catch_11f

    #@11b
    .line 923
    :cond_11b
    :goto_11b
    throw v5

    #@11c
    .end local v3           #touchCall:Ljava/io/BufferedWriter;
    .restart local v4       #touchCall:Ljava/io/BufferedWriter;
    :catch_11c
    move-exception v5

    #@11d
    move-object v3, v4

    #@11e
    .line 924
    .end local v4           #touchCall:Ljava/io/BufferedWriter;
    .restart local v3       #touchCall:Ljava/io/BufferedWriter;
    goto :goto_a1

    #@11f
    .line 923
    :catch_11f
    move-exception v6

    #@120
    goto :goto_11b

    #@121
    .line 921
    :catchall_121
    move-exception v5

    #@122
    goto :goto_116

    #@123
    .line 918
    :catch_123
    move-exception v1

    #@124
    goto :goto_cc
.end method

.method protected handleRingerModeChange(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    .line 932
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "handleRingerModeChange("

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ")"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 933
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mRingMode:I

    #@20
    .line 934
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v2

    #@27
    if-ge v1, v2, :cond_3f

    #@29
    .line 935
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v2

    #@2f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@31
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@37
    .line 936
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_3c

    #@39
    .line 937
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onRingerModeChanged(I)V

    #@3c
    .line 934
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_21

    #@3f
    .line 940
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_3f
    return-void
.end method

.method protected handleUserRemoved(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 861
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 862
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@17
    .line 863
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1c

    #@19
    .line 864
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onUserRemoved(I)V

    #@1c
    .line 861
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 867
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_1f
    return-void
.end method

.method protected handleUserSwitched(ILandroid/os/IRemoteCallback;)V
    .registers 6
    .parameter "userId"
    .parameter "reply"

    #@0
    .prologue
    .line 777
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 778
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@17
    .line 779
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_1c

    #@19
    .line 780
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onUserSwitched(I)V

    #@1c
    .line 777
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 783
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_1f
    const/4 v2, 0x0

    #@20
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@23
    .line 785
    const/4 v2, 0x0

    #@24
    :try_start_24
    invoke-interface {p2, v2}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_27} :catch_28

    #@27
    .line 788
    :goto_27
    return-void

    #@28
    .line 786
    :catch_28
    move-exception v2

    #@29
    goto :goto_27
.end method

.method public hasBootCompleted()Z
    .registers 2

    #@0
    .prologue
    .line 854
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mBootCompleted:Z

    #@2
    return v0
.end method

.method public isAlternateUnlockEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1346
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mAlternateUnlockEnabled:Z

    #@2
    return v0
.end method

.method public isClockVisible()Z
    .registers 2

    #@0
    .prologue
    .line 1330
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mClockVisible:Z

    #@2
    return v0
.end method

.method public isDeviceProvisioned()Z
    .registers 2

    #@0
    .prologue
    .line 1313
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return v0
.end method

.method public isDsdpPlayed()Z
    .registers 4

    #@0
    .prologue
    .line 1287
    const-string v0, "KeyguardUpdateMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isDsdpOn :"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpOn:Z

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1288
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mDsdpOn:Z

    #@1c
    return v0
.end method

.method public isHdmiPluggedIn()Z
    .registers 4

    #@0
    .prologue
    .line 1274
    const-string v0, "KeyguardUpdateMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isHdmiPluggedIn :"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1275
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHdmiPlugged:Z

    #@1c
    return v0
.end method

.method public isHomeWifiConnected()Z
    .registers 2

    #@0
    .prologue
    .line 1081
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected:Z

    #@2
    return v0
.end method

.method public isKTUSIMRegiRequires()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 231
    const-string v2, "KeyguardUpdateMonitor"

    #@3
    const-string v3, "isKTUSIMDownloadRequires()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 246
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimType()I

    #@b
    move-result v1

    #@c
    .line 247
    .local v1, usimType:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimIsEmpty()I

    #@f
    move-result v0

    #@10
    .line 249
    .local v0, isUsimEmpty:I
    if-ne v1, v4, :cond_16

    #@12
    if-ne v0, v4, :cond_16

    #@14
    .line 251
    const/4 v2, 0x1

    #@15
    .line 254
    :goto_15
    return v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_15
.end method

.method public isKeyguardVisible()Z
    .registers 2

    #@0
    .prologue
    .line 1086
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mKeyguardIsVisible:Z

    #@2
    return v0
.end method

.method public isLGTUSIMRegiRequires()Z
    .registers 5

    #@0
    .prologue
    .line 259
    const-string v2, "KeyguardUpdateMonitor"

    #@2
    const-string v3, "isLGTUSIMDownloadRequires()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 274
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimType()I

    #@a
    move-result v1

    #@b
    .line 275
    .local v1, usimType:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimIsEmpty()I

    #@e
    move-result v0

    #@f
    .line 277
    .local v0, isUsimEmpty:I
    const/4 v2, 0x5

    #@10
    if-ne v1, v2, :cond_17

    #@12
    const/4 v2, 0x2

    #@13
    if-ne v0, v2, :cond_17

    #@15
    .line 279
    const/4 v2, 0x1

    #@16
    .line 282
    :goto_16
    return v2

    #@17
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16
.end method

.method public isLgMusicplaying()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1065
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v2

    #@7
    .line 1069
    .local v2, pm:Landroid/content/pm/PackageManager;
    :try_start_7
    const-string v4, "com.lge.sizechangable.musicwidget.widget"

    #@9
    const/4 v5, 0x0

    #@a
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_d} :catch_18

    #@d
    move-result-object v1

    #@e
    .line 1074
    .local v1, packageInfo:Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_1a

    #@10
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    #@12
    const v5, 0x263e0a1

    #@15
    if-ge v4, v5, :cond_1a

    #@17
    .line 1077
    .end local v1           #packageInfo:Landroid/content/pm/PackageInfo;
    :goto_17
    return v3

    #@18
    .line 1070
    :catch_18
    move-exception v0

    #@19
    .line 1071
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_17

    #@1a
    .line 1077
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_1a
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isLgMusicplaying:Z

    #@1c
    goto :goto_17
.end method

.method public isQuickCoverClosed()Z
    .registers 3

    #@0
    .prologue
    .line 1475
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCoverState:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ne v0, v1, :cond_7

    #@5
    .line 1476
    const/4 v0, 0x1

    #@6
    .line 1478
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method protected isQuickCoverEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1489
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "quick_cover_enable"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_29

    #@10
    .line 1492
    .local v0, quickCoverEnabled:Z
    :goto_10
    const-string v1, "KeyguardUpdateMonitor"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "isQuickCoverEnabled : "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1494
    return v0

    #@29
    .end local v0           #quickCoverEnabled:Z
    :cond_29
    move v0, v1

    #@2a
    .line 1489
    goto :goto_10
.end method

.method public isQuickCoverOpened()Z
    .registers 3

    #@0
    .prologue
    .line 1482
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCoverState:I

    #@2
    const/4 v1, 0x6

    #@3
    if-ne v0, v1, :cond_7

    #@5
    .line 1483
    const/4 v0, 0x1

    #@6
    .line 1485
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method protected isQuickCoverWindowEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1498
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "quick_view_enable"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_29

    #@10
    .line 1501
    .local v0, quickCoverWindowEnabled:Z
    :goto_10
    const-string v1, "KeyguardUpdateMonitor"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v3, "isQuickCoverWindowEnabled : "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1503
    return v0

    #@29
    .end local v0           #quickCoverWindowEnabled:Z
    :cond_29
    move v0, v1

    #@2a
    .line 1498
    goto :goto_10
.end method

.method public isSimLocked()Z
    .registers 7

    #@0
    .prologue
    .line 1354
    const/4 v2, 0x0

    #@1
    .line 1355
    .local v2, isLocked:Z
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@3
    .local v0, arr$:[Lcom/android/internal/telephony/IccCardConstants$State;
    array-length v3, v0

    #@4
    .local v3, len$:I
    const/4 v1, 0x0

    #@5
    .local v1, i$:I
    :goto_5
    if-ge v1, v3, :cond_10

    #@7
    aget-object v4, v0, v1

    #@9
    .line 1356
    .local v4, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimLocked(Lcom/android/internal/telephony/IccCardConstants$State;)Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_11

    #@f
    .line 1357
    const/4 v2, 0x1

    #@10
    .line 1361
    .end local v4           #simState:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_10
    return v2

    #@11
    .line 1355
    .restart local v4       #simState:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_5
.end method

.method public isSimLocked(I)Z
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 1365
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimLocked(Lcom/android/internal/telephony/IccCardConstants$State;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isSimPinSecure()Z
    .registers 7

    #@0
    .prologue
    .line 1375
    const/4 v2, 0x0

    #@1
    .line 1376
    .local v2, isSecure:Z
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@3
    .local v0, arr$:[Lcom/android/internal/telephony/IccCardConstants$State;
    array-length v3, v0

    #@4
    .local v3, len$:I
    const/4 v1, 0x0

    #@5
    .local v1, i$:I
    :goto_5
    if-ge v1, v3, :cond_10

    #@7
    aget-object v4, v0, v1

    #@9
    .line 1377
    .local v4, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimPinSecure(Lcom/android/internal/telephony/IccCardConstants$State;)Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_11

    #@f
    .line 1378
    const/4 v2, 0x1

    #@10
    .line 1382
    .end local v4           #simState:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_10
    return v2

    #@11
    .line 1376
    .restart local v4       #simState:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_5
.end method

.method public isSimPinSecure(I)Z
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 1385
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimPinSecure(Lcom/android/internal/telephony/IccCardConstants$State;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method protected isUsingQuickCover()Z
    .registers 2

    #@0
    .prologue
    .line 1522
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSupportQuickCover()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getCoverType()I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_14

    #@12
    .line 1523
    const/4 v0, 0x1

    #@13
    .line 1525
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method protected isUsingQuickCoverWindow()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1516
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSupportQuickCoverWindow()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_14

    #@7
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverWindowEnabled()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_14

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getCoverType()I

    #@10
    move-result v1

    #@11
    if-ne v1, v0, :cond_14

    #@13
    .line 1519
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public isVTActive()Z
    .registers 6

    #@0
    .prologue
    .line 1430
    const/4 v2, 0x0

    #@1
    .line 1431
    .local v2, vtCallState:I
    const-string v3, "phone"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v3

    #@7
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v1

    #@b
    .line 1434
    .local v1, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_11

    #@d
    .line 1436
    :try_start_d
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getCallState()I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_10} :catch_17

    #@10
    move-result v2

    #@11
    .line 1442
    :cond_11
    :goto_11
    const/16 v3, 0x64

    #@13
    if-lt v2, v3, :cond_20

    #@15
    .line 1443
    const/4 v3, 0x1

    #@16
    .line 1446
    :goto_16
    return v3

    #@17
    .line 1437
    :catch_17
    move-exception v0

    #@18
    .line 1438
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "KeyguardUpdateMonitor"

    #@1a
    const-string v4, "isVTActive - Failed getVtCallState !!!"

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_11

    #@20
    .line 1446
    .end local v0           #e:Ljava/lang/Exception;
    :cond_20
    const/4 v3, 0x0

    #@21
    goto :goto_16
.end method

.method public registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    .line 1168
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1b

    #@9
    .line 1169
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    if-ne v1, p1, :cond_18

    #@17
    .line 1178
    :goto_17
    return-void

    #@18
    .line 1168
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 1175
    :cond_1b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@1d
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@1f
    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@22
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 1176
    const/4 v1, 0x0

    #@26
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@29
    .line 1177
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->sendUpdates(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@2c
    goto :goto_17
.end method

.method public removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 1153
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_20

    #@a
    .line 1154
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@12
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    if-ne v1, p1, :cond_1d

    #@18
    .line 1155
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1d
    .line 1153
    :cond_1d
    add-int/lit8 v0, v0, -0x1

    #@1f
    goto :goto_8

    #@20
    .line 1158
    :cond_20
    return-void
.end method

.method public reportClockVisible(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 1210
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mClockVisible:Z

    #@2
    .line 1211
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@4
    const/16 v1, 0x133

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 1212
    return-void
.end method

.method public reportFailedBiometricUnlockAttempt()V
    .registers 2

    #@0
    .prologue
    .line 1338
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@6
    .line 1339
    return-void
.end method

.method public reportFailedUnlockAttempt()V
    .registers 2

    #@0
    .prologue
    .line 1326
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@6
    .line 1327
    return-void
.end method

.method public reportSimUnlocked()V
    .registers 2

    #@0
    .prologue
    .line 1251
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->reportSimUnlocked(I)V

    #@b
    .line 1252
    return-void
.end method

.method public reportSimUnlocked(I)V
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    .line 1255
    const-string v0, "KeyguardUpdateMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "--msim--: reportSimUnlocked("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1261
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;

    #@20
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@22
    invoke-direct {v0, v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;-><init>(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@25
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;)V

    #@28
    .line 1262
    return-void
.end method

.method public reportUSIMPersoFinish()V
    .registers 6

    #@0
    .prologue
    .line 219
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@2
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_FINISH_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@4
    sput-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@6
    .line 220
    const-string v2, "KeyguardUpdateMonitor"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "reportUSIMPersoFinish: mUsimPersoFinishState = "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 221
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v2

    #@27
    if-ge v1, v2, :cond_41

    #@29
    .line 222
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v2

    #@2f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@31
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@37
    .line 223
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    if-eqz v0, :cond_3e

    #@39
    .line 224
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3b
    invoke-virtual {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@3e
    .line 221
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_21

    #@41
    .line 227
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
    :cond_41
    return-void
.end method

.method public sendKeyguardVisibilityChanged(Z)V
    .registers 5
    .parameter "showing"

    #@0
    .prologue
    const/16 v2, 0x138

    #@2
    .line 1203
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 1204
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 1205
    .local v0, message:Landroid/os/Message;
    if-eqz p1, :cond_16

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@12
    .line 1206
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 1207
    return-void

    #@16
    .line 1205
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_10
.end method

.method public setAlternateUnlockEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 1350
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mAlternateUnlockEnabled:Z

    #@2
    .line 1351
    return-void
.end method

.method public setFailedUnlockAttempts(I)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 1424
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@2
    .line 1425
    return-void
.end method

.method public setInputHalfFailedAttempts(Z)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 1403
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mInputHalfFailedAttempts:Z

    #@2
    .line 1404
    return-void
.end method

.method public setLockTimerState(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1463
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mLockTimerState:I

    #@2
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@4
    if-ne v0, v1, :cond_16

    #@6
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_START:I

    #@8
    if-eq p1, v0, :cond_e

    #@a
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_STOPED:I

    #@c
    if-ne p1, v0, :cond_16

    #@e
    .line 1465
    :cond_e
    const-string v0, "KeyguardUpdateMonitor"

    #@10
    const-string v1, "Do not change LockTimer. Lock Timer state cannot revert."

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 1469
    :goto_15
    return-void

    #@16
    .line 1468
    :cond_16
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mLockTimerState:I

    #@18
    goto :goto_15
.end method

.method public setUsimPersoLocking()V
    .registers 4

    #@0
    .prologue
    .line 213
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@2
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_DOING_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@4
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@6
    .line 214
    const-string v0, "KeyguardUpdateMonitor"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "setUsimPersoLocking: mUsimPersoFinishState = "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 215
    return-void
.end method

.method public setUsimStateLoaded(Z)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 203
    const-string v0, "KeyguardUpdateMonitor"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setUsimStateLoaded() getSimloaded : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 204
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimloaded:Z

    #@1a
    .line 205
    return-void
.end method
