.class Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;
.super Ljava/lang/Object;
.source "KeyguardPINView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@4
    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 97
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@e
    const-string v1, ""

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@13
    .line 99
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->doHapticKeyClick()V

    #@18
    .line 100
    const/4 v0, 0x1

    #@19
    return v0
.end method
