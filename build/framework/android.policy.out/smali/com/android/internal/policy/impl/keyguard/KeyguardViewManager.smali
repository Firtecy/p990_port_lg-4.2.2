.class public Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.super Ljava/lang/Object;
.source "KeyguardViewManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;
    }
.end annotation


# static fields
.field public static final ACTION_LOCKSCREEN_CREATE:Ljava/lang/String; = "com.lge.lockscreen.intent.action.LOCKSCREEN_CREATE"

.field private static final DEBUG:Z = true

.field static final DIGIT_PRESS_WAKE_MILLIS:I = 0x1388

.field private static TAG:Ljava/lang/String;

.field public static USE_UPPER_CASE:Z


# instance fields
.field private mBroadCastReceiver:Landroid/content/BroadcastReceiver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

.field mDelayTimeRemoveView:I

.field private mIsBootCompleted:Z

.field private mIsWaitingForBootComplete:Z

.field private mKeyguardHost:Landroid/widget/FrameLayout;

.field private mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mNeedsInput:Z

.field private mPkgStateCallback:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;

.field private mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

.field mRemoteViews:Landroid/widget/RemoteViews;

.field private mScreenOn:Z

.field private mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field mStateContainer:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpdateMonitorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private final mViewManager:Landroid/view/ViewManager;

.field private final mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

.field private mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    const-string v0, "KeyguardViewManager"

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@4
    .line 72
    const/4 v0, 0x0

    #@5
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->USE_UPPER_CASE:Z

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 10
    .parameter "context"
    .parameter "viewManager"
    .parameter "callback"
    .parameter "lockPatternUtils"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 82
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mNeedsInput:Z

    #@7
    .line 89
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mScreenOn:Z

    #@9
    .line 93
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsWaitingForBootComplete:Z

    #@b
    .line 94
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsBootCompleted:Z

    #@d
    .line 95
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@f
    .line 107
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@11
    .line 148
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;

    #@13
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V

    #@16
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@18
    .line 204
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;

    #@1a
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V

    #@1d
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mServiceConnection:Landroid/content/ServiceConnection;

    #@1f
    .line 514
    new-instance v2, Landroid/util/SparseArray;

    #@21
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@24
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mStateContainer:Landroid/util/SparseArray;

    #@26
    .line 1093
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;

    #@28
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V

    #@2b
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mPkgStateCallback:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;

    #@2d
    .line 1110
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;

    #@2f
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$7;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)V

    #@32
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mUpdateMonitorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@34
    .line 1138
    const/16 v2, 0x1f4

    #@36
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mDelayTimeRemoveView:I

    #@38
    .line 118
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@3a
    .line 119
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@3c
    .line 120
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@3e
    .line 121
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@40
    .line 123
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@42
    if-eqz v2, :cond_53

    #@44
    .line 124
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@47
    move-result-object v2

    #@48
    if-eqz v2, :cond_53

    #@4a
    .line 125
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@4d
    move-result-object v2

    #@4e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mPkgStateCallback:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;

    #@50
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->registerPackageStateChangedCallback(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;)V

    #@53
    .line 129
    :cond_53
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@55
    if-eqz v2, :cond_81

    #@57
    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    #@59
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5c
    .line 132
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "jp.co.nttdocomo.carriermail.APP_LINK_RECEIVED_MESSAGE"

    #@5e
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@61
    .line 133
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@63
    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@66
    .line 134
    new-instance v1, Landroid/content/IntentFilter;

    #@68
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@6b
    .line 135
    .local v1, screenFilter:Landroid/content/IntentFilter;
    const-string v2, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE"

    #@6d
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@70
    .line 136
    const-string v2, "com.android.internal.policy.impl.CARRIERMAIL_COUNT_UPDATE"

    #@72
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@75
    .line 137
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    #@77
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7a
    .line 138
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@7c
    const-string v3, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@7e
    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@81
    .line 144
    .end local v0           #filter:Landroid/content/IntentFilter;
    .end local v1           #screenFilter:Landroid/content/IntentFilter;
    :cond_81
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@83
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@85
    invoke-direct {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;-><init>(Landroid/content/Context;)V

    #@88
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@8a
    .line 145
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 69
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsWaitingForBootComplete:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsWaitingForBootComplete:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mServiceConnection:Landroid/content/ServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;ZZLandroid/os/Bundle;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    return-object v0
.end method

.method private createKeyguardHostAndQuickCover()V
    .registers 10

    #@0
    .prologue
    const/high16 v8, 0x100

    #@2
    const/4 v1, -0x1

    #@3
    .line 1254
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@5
    if-nez v2, :cond_c3

    #@7
    .line 1255
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@9
    const-string v5, "keyguard host is null, creating it..."

    #@b
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1256
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@10
    instance-of v6, v2, Landroid/app/Activity;

    #@12
    .line 1257
    .local v6, isActivity:Z
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@14
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@16
    invoke-direct {v2, p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Landroid/content/Context;)V

    #@19
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@1b
    .line 1259
    const v4, 0x110900

    #@1e
    .line 1265
    .local v4, flags:I
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@20
    if-eqz v2, :cond_2c

    #@22
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@25
    move-result-object v2

    #@26
    if-eqz v2, :cond_2c

    #@28
    .line 1267
    const v2, -0x100001

    #@2b
    and-int/2addr v4, v2

    #@2c
    .line 1270
    :cond_2c
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mNeedsInput:Z

    #@2e
    if-nez v2, :cond_33

    #@30
    .line 1271
    const/high16 v2, 0x2

    #@32
    or-int/2addr v4, v2

    #@33
    .line 1274
    :cond_33
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@36
    move-result v2

    #@37
    if-nez v2, :cond_4d

    #@39
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@3b
    if-eqz v2, :cond_4e

    #@3d
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@40
    move-result-object v2

    #@41
    if-eqz v2, :cond_4e

    #@43
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isHardwareAccelerated()Z

    #@4a
    move-result v2

    #@4b
    if-eqz v2, :cond_4e

    #@4d
    .line 1278
    :cond_4d
    or-int/2addr v4, v8

    #@4e
    .line 1281
    :cond_4e
    const/4 v7, -0x1

    #@4f
    .line 1282
    .local v7, stretch:I
    if-eqz v6, :cond_c4

    #@51
    const/4 v3, 0x2

    #@52
    .line 1284
    .local v3, type:I
    :goto_52
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@54
    const/4 v5, -0x3

    #@55
    move v2, v1

    #@56
    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    #@59
    .line 1286
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x10

    #@5b
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@5d
    .line 1287
    const v1, 0x10301e2

    #@60
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@62
    .line 1288
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@65
    move-result v1

    #@66
    if-eqz v1, :cond_73

    #@68
    .line 1289
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@6a
    or-int/2addr v1, v8

    #@6b
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@6d
    .line 1290
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@6f
    or-int/lit8 v1, v1, 0x2

    #@71
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@73
    .line 1293
    :cond_73
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@75
    or-int/lit8 v1, v1, 0x8

    #@77
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@79
    .line 1294
    if-eqz v6, :cond_81

    #@7b
    .line 1295
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@7d
    or-int/lit8 v1, v1, 0x10

    #@7f
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@81
    .line 1297
    :cond_81
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@83
    or-int/lit8 v1, v1, 0x4

    #@85
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@87
    .line 1298
    if-eqz v6, :cond_c7

    #@89
    const-string v1, "KeyguardMock"

    #@8b
    :goto_8b
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@8e
    .line 1299
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@90
    .line 1300
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@92
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@94
    invoke-interface {v1, v2, v0}, Landroid/view/ViewManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@97
    .line 1302
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@99
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@a0
    move-result v1

    #@a1
    if-eqz v1, :cond_c3

    #@a3
    .line 1303
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@a6
    move-result-object v1

    #@a7
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@a9
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@ab
    invoke-virtual {v1, v2, v5}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->createQuickCoverHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@ae
    move-result-object v1

    #@af
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b1
    .line 1304
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b3
    if-eqz v1, :cond_c3

    #@b5
    .line 1305
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b7
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@b9
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setViewMediatorCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;)V

    #@bc
    .line 1306
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@be
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@c0
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@c3
    .line 1310
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    .end local v3           #type:I
    .end local v4           #flags:I
    .end local v6           #isActivity:Z
    .end local v7           #stretch:I
    :cond_c3
    return-void

    #@c4
    .line 1282
    .restart local v4       #flags:I
    .restart local v6       #isActivity:Z
    .restart local v7       #stretch:I
    :cond_c4
    const/16 v3, 0x7d4

    #@c6
    goto :goto_52

    #@c7
    .line 1298
    .restart local v0       #lp:Landroid/view/WindowManager$LayoutParams;
    .restart local v3       #type:I
    :cond_c7
    const-string v1, "Keyguard"

    #@c9
    goto :goto_8b
.end method

.method private inflateKeyguardView(Landroid/os/Bundle;)V
    .registers 15
    .parameter "options"

    #@0
    .prologue
    .line 594
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2
    const-string v11, "inflateKeyguardView"

    #@4
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 595
    sget-boolean v10, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@9
    if-eqz v10, :cond_1e

    #@b
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@e
    move-result-object v10

    #@f
    if-eqz v10, :cond_1e

    #@11
    .line 597
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@13
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@16
    move-result-object v11

    #@17
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    #@1a
    move-result-object v11

    #@1b
    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@1e
    .line 601
    :cond_1e
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@20
    const v11, 0x10202c1

    #@23
    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v7

    #@27
    .line 602
    .local v7, v:Landroid/view/View;
    if-eqz v7, :cond_2e

    #@29
    .line 603
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@2b
    invoke-virtual {v10, v7}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    #@2e
    .line 607
    :cond_2e
    sget-boolean v10, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@30
    if-eqz v10, :cond_43

    #@32
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@35
    move-result-object v10

    #@36
    if-eqz v10, :cond_43

    #@38
    .line 609
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@3b
    move-result-object v10

    #@3c
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@3e
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@40
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->destroyKeyguardHostView(Landroid/content/Context;Landroid/view/ViewGroup;)V

    #@43
    .line 613
    :cond_43
    const/4 v2, 0x0

    #@44
    .line 614
    .local v2, createPkgLockScreen:Z
    sget-boolean v10, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@46
    if-eqz v10, :cond_9b

    #@48
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@4b
    move-result-object v10

    #@4c
    if-eqz v10, :cond_9b

    #@4e
    .line 616
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@50
    invoke-virtual {v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@53
    move-result-object v10

    #@54
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@56
    .line 617
    sget-boolean v10, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@58
    if-eqz v10, :cond_175

    #@5a
    .line 618
    const/4 v0, 0x1

    #@5b
    .line 619
    .local v0, LOCK_SCREEN_TYPE_EASYUI:I
    const/4 v1, 0x2

    #@5c
    .line 620
    .local v1, LOCK_SCREEN_TYPE_TOUCH:I
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@61
    move-result-object v10

    #@62
    const-string v11, "lockscreen_type_password_unspecified"

    #@64
    const/4 v12, 0x0

    #@65
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@68
    move-result v3

    #@69
    .line 622
    .local v3, curLockScreenType:I
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@6b
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@6e
    move-result v10

    #@6f
    if-nez v10, :cond_172

    #@71
    .line 623
    if-ne v3, v1, :cond_16f

    #@73
    .line 624
    const/4 v2, 0x0

    #@74
    .line 635
    .end local v0           #LOCK_SCREEN_TYPE_EASYUI:I
    .end local v1           #LOCK_SCREEN_TYPE_TOUCH:I
    .end local v3           #curLockScreenType:I
    :goto_74
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@76
    sget-object v11, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@78
    if-eq v10, v11, :cond_80

    #@7a
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@7c
    sget-object v11, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@7e
    if-ne v10, v11, :cond_9b

    #@80
    .line 637
    :cond_80
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@82
    new-instance v11, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v12, "make createPkgLockScreen false : "

    #@89
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v11

    #@8d
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v11

    #@93
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v11

    #@97
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 638
    const/4 v2, 0x0

    #@9b
    .line 642
    :cond_9b
    if-eqz v2, :cond_178

    #@9d
    .line 643
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@9f
    const-string v11, "inflateKeyguardView - createPkgLockScreen : true"

    #@a1
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 644
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@a7
    move-result-object v10

    #@a8
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@aa
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@ac
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->createKeyguardHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@af
    move-result-object v10

    #@b0
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b2
    .line 647
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b4
    if-nez v10, :cond_d1

    #@b6
    .line 648
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@b8
    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@bb
    move-result-object v4

    #@bc
    .line 649
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v10, 0x1090058

    #@bf
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@c1
    const/4 v12, 0x0

    #@c2
    invoke-virtual {v4, v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@c5
    move-result-object v8

    #@c6
    .line 650
    .local v8, view:Landroid/view/View;
    const v10, 0x10202c1

    #@c9
    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@cc
    move-result-object v10

    #@cd
    check-cast v10, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@cf
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@d1
    .line 652
    .end local v4           #inflater:Landroid/view/LayoutInflater;
    .end local v8           #view:Landroid/view/View;
    :cond_d1
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@d3
    const-string v11, "multi_pane_challenge"

    #@d5
    invoke-virtual {v10, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@d8
    move-result-object v10

    #@d9
    if-eqz v10, :cond_128

    #@db
    .line 653
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@dd
    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e0
    move-result-object v10

    #@e1
    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@e4
    move-result-object v10

    #@e5
    iget v6, v10, Landroid/content/res/Configuration;->orientation:I

    #@e7
    .line 654
    .local v6, orientationOfView:I
    const/4 v10, 0x1

    #@e8
    if-ne v6, v10, :cond_128

    #@ea
    .line 655
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@ec
    invoke-virtual {v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->cleanUp()V

    #@ef
    .line 656
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@f2
    move-result-object v10

    #@f3
    if-eqz v10, :cond_102

    #@f5
    .line 657
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@f7
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@fa
    move-result-object v11

    #@fb
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    #@fe
    move-result-object v11

    #@ff
    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@102
    .line 660
    :cond_102
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@104
    new-instance v11, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v12, "inflateKeyguardView: View has multi_pane_challenge, orientation = "

    #@10b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v11

    #@10f
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v11

    #@113
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v11

    #@117
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    .line 661
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@11d
    move-result-object v10

    #@11e
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@120
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@122
    invoke-virtual {v10, v11, v12}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->createKeyguardHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@125
    move-result-object v10

    #@126
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@128
    .line 666
    .end local v6           #orientationOfView:I
    :cond_128
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@12a
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@12c
    const/4 v12, 0x0

    #@12d
    invoke-virtual {v10, v11, v12}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    #@130
    .line 679
    :goto_130
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@132
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@134
    invoke-virtual {v10, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@137
    .line 680
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@139
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@13b
    invoke-virtual {v10, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setViewMediatorCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;)V

    #@13e
    .line 686
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@140
    if-eqz v10, :cond_15e

    #@142
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@144
    instance-of v10, v10, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@146
    if-eqz v10, :cond_15e

    #@148
    .line 687
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@14a
    const v11, 0x10202cd

    #@14d
    invoke-virtual {v10, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewById(I)Landroid/view/View;

    #@150
    move-result-object v5

    #@151
    check-cast v5, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;

    #@153
    .line 690
    .local v5, kpv:Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;
    if-eqz v5, :cond_15e

    #@155
    .line 691
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@157
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->needsInput()Z

    #@15a
    move-result v11

    #@15b
    invoke-interface {v10, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNeedsInput(Z)V

    #@15e
    .line 695
    .end local v5           #kpv:Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;
    :cond_15e
    if-eqz p1, :cond_16e

    #@160
    .line 696
    const-string v10, "showappwidget"

    #@162
    const/4 v11, 0x0

    #@163
    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@166
    move-result v9

    #@167
    .line 698
    .local v9, widgetToShow:I
    if-eqz v9, :cond_16e

    #@169
    .line 699
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@16b
    invoke-virtual {v10, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->goToWidget(I)V

    #@16e
    .line 702
    .end local v9           #widgetToShow:I
    :cond_16e
    return-void

    #@16f
    .line 626
    .restart local v0       #LOCK_SCREEN_TYPE_EASYUI:I
    .restart local v1       #LOCK_SCREEN_TYPE_TOUCH:I
    .restart local v3       #curLockScreenType:I
    :cond_16f
    const/4 v2, 0x1

    #@170
    goto/16 :goto_74

    #@172
    .line 629
    :cond_172
    const/4 v2, 0x1

    #@173
    goto/16 :goto_74

    #@175
    .line 632
    .end local v0           #LOCK_SCREEN_TYPE_EASYUI:I
    .end local v1           #LOCK_SCREEN_TYPE_TOUCH:I
    .end local v3           #curLockScreenType:I
    :cond_175
    const/4 v2, 0x1

    #@176
    goto/16 :goto_74

    #@178
    .line 668
    :cond_178
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@17a
    const-string v11, "inflateKeyguardView - createPkgLockScreen : false"

    #@17c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    .line 672
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@181
    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@184
    move-result-object v4

    #@185
    .line 673
    .restart local v4       #inflater:Landroid/view/LayoutInflater;
    const v10, 0x1090058

    #@188
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@18a
    const/4 v12, 0x0

    #@18b
    invoke-virtual {v4, v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@18e
    move-result-object v8

    #@18f
    .line 674
    .restart local v8       #view:Landroid/view/View;
    const v10, 0x10202c1

    #@192
    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@195
    move-result-object v10

    #@196
    check-cast v10, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@198
    iput-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@19a
    .line 675
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@19c
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@19e
    const/4 v12, 0x0

    #@19f
    invoke-virtual {v10, v11, v12}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    #@1a2
    goto :goto_130
.end method

.method private maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V
    .registers 11
    .parameter "enableScreenRotation"
    .parameter "force"
    .parameter "options"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    .line 518
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@4
    instance-of v1, v3, Landroid/app/Activity;

    #@6
    .line 520
    .local v1, isActivity:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@8
    if-eqz v3, :cond_11

    #@a
    .line 521
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@c
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mStateContainer:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->saveHierarchyState(Landroid/util/SparseArray;)V

    #@11
    .line 525
    :cond_11
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->createKeyguardHostAndQuickCover()V

    #@14
    .line 527
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@16
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_36

    #@20
    .line 528
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@22
    if-eqz v3, :cond_36

    #@24
    .line 529
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@26
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_cd

    #@30
    .line 530
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@32
    const/4 v4, 0x0

    #@33
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@36
    .line 537
    :cond_36
    :goto_36
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@38
    if-eqz v3, :cond_44

    #@3a
    .line 538
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@3c
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@3e
    const v5, -0x100001

    #@41
    and-int/2addr v4, v5

    #@42
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@44
    .line 552
    :cond_44
    if-nez p2, :cond_52

    #@46
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@48
    if-eqz v3, :cond_52

    #@4a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@4c
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->needRecreateMe()Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_90

    #@52
    .line 554
    :cond_52
    const/4 v2, 0x1

    #@53
    .line 555
    .local v2, keyguardVisible:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@55
    instance-of v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@57
    if-eqz v3, :cond_74

    #@59
    .line 556
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@5b
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@5d
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->isKeyguardVisible()Z

    #@60
    move-result v2

    #@61
    .line 557
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@63
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@65
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@67
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@69
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v3, v4}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@74
    .line 560
    :cond_74
    invoke-direct {p0, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->inflateKeyguardView(Landroid/os/Bundle;)V

    #@77
    .line 561
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@79
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->requestFocus()Z

    #@7c
    .line 563
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@7e
    instance-of v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@80
    if-eqz v3, :cond_90

    #@82
    .line 564
    if-nez v2, :cond_90

    #@84
    .line 565
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@86
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@89
    .line 566
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@8b
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@8d
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->requestRecreateKeyguardViewWhenReshown()V

    #@90
    .line 574
    .end local v2           #keyguardVisible:Z
    :cond_90
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->updateUserActivityTimeoutInWindowLayoutParams()V

    #@93
    .line 575
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@95
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@97
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@99
    invoke-interface {v3, v4, v5}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@9c
    .line 577
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@9e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mStateContainer:Landroid/util/SparseArray;

    #@a0
    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    #@a3
    .line 580
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@a5
    if-eqz v3, :cond_cc

    #@a7
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@aa
    move-result-object v3

    #@ab
    if-eqz v3, :cond_cc

    #@ad
    .line 581
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@b0
    move-result-object v3

    #@b1
    const-string v4, "config_feature_enable_friend_mode_pattern"

    #@b3
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@b6
    move-result v3

    #@b7
    if-eqz v3, :cond_cc

    #@b9
    .line 582
    const-string v3, "service.plushome.currenthome"

    #@bb
    const-string v4, "standard"

    #@bd
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c0
    .line 584
    :try_start_c0
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@c2
    new-instance v4, Landroid/content/Intent;

    #@c4
    const-string v5, "com.lge.lockscreen.intent.action.LOCKSCREEN_CREATE"

    #@c6
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c9
    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_cc
    .catch Ljava/lang/Exception; {:try_start_c0 .. :try_end_cc} :catch_d4

    #@cc
    .line 591
    :cond_cc
    :goto_cc
    return-void

    #@cd
    .line 532
    :cond_cd
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@cf
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@d2
    goto/16 :goto_36

    #@d4
    .line 585
    :catch_d4
    move-exception v0

    #@d5
    .line 586
    .local v0, e:Ljava/lang/Exception;
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@d7
    const-string v4, "LockScreen sendBroadcast fail(ACTION_LOCKSCREEN_CREATE)"

    #@d9
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto :goto_cc
.end method

.method private maybeEnableScreenRotation(Z)V
    .registers 7
    .parameter "enableScreenRotation"

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x2

    #@2
    .line 732
    const-string v1, "ro.product.model"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "VS980 4G"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_58

    #@10
    .line 733
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@12
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@15
    move-result-object v0

    #@16
    .line 735
    .local v0, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    if-eqz p1, :cond_2d

    #@18
    .line 736
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@1a
    const-string v2, "VS980 Rotation sensor for lock screen On!"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 737
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@21
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@23
    .line 746
    :goto_23
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@25
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@27
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@29
    invoke-interface {v1, v2, v3}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@2c
    .line 758
    .end local v0           #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :goto_2c
    return-void

    #@2d
    .line 739
    .restart local v0       #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_2d
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "VS980 Rotation sensor for lock screen Off! securityMode = "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 740
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@47
    if-eq v0, v1, :cond_4d

    #@49
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@4b
    if-ne v0, v1, :cond_53

    #@4d
    .line 741
    :cond_4d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@4f
    const/4 v2, 0x7

    #@50
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@52
    goto :goto_23

    #@53
    .line 743
    :cond_53
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@55
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@57
    goto :goto_23

    #@58
    .line 748
    .end local v0           #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_58
    if-eqz p1, :cond_6f

    #@5a
    .line 749
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@5c
    const-string v2, "Rotation sensor for lock screen On!"

    #@5e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 750
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@63
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@65
    .line 755
    :goto_65
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@67
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@69
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@6b
    invoke-interface {v1, v2, v3}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@6e
    goto :goto_2c

    #@6f
    .line 752
    :cond_6f
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@71
    const-string v2, "Rotation sensor for lock screen Off!"

    #@73
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 753
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@78
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@7a
    goto :goto_65
.end method

.method private setKeyguardState(Ljava/lang/String;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    .line 1043
    const/4 v2, 0x0

    #@1
    .line 1044
    .local v2, writer:Ljava/io/FileWriter;
    const-string v1, "/sys/devices/virtual/input/lge_touch/keyguard"

    #@3
    .line 1047
    .local v1, path:Ljava/lang/String;
    :try_start_3
    new-instance v3, Ljava/io/FileWriter;

    #@5
    new-instance v4, Ljava/io/File;

    #@7
    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_4b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_d} :catch_24

    #@d
    .line 1049
    .end local v2           #writer:Ljava/io/FileWriter;
    .local v3, writer:Ljava/io/FileWriter;
    if-eqz v3, :cond_1a

    #@f
    .line 1050
    const/4 v4, 0x0

    #@10
    :try_start_10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13
    move-result v5

    #@14
    invoke-virtual {v3, p1, v4, v5}, Ljava/io/FileWriter;->write(Ljava/lang/String;II)V

    #@17
    .line 1051
    invoke-virtual {v3}, Ljava/io/FileWriter;->flush()V
    :try_end_1a
    .catchall {:try_start_10 .. :try_end_1a} :catchall_54
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_1a} :catch_57

    #@1a
    .line 1057
    :cond_1a
    if-eqz v3, :cond_1f

    #@1c
    .line 1058
    :try_start_1c
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1f} :catch_21

    #@1f
    :cond_1f
    move-object v2, v3

    #@20
    .line 1062
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v2       #writer:Ljava/io/FileWriter;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1060
    .end local v2           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    :catch_21
    move-exception v4

    #@22
    move-object v2, v3

    #@23
    .line 1061
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v2       #writer:Ljava/io/FileWriter;
    goto :goto_20

    #@24
    .line 1053
    :catch_24
    move-exception v0

    #@25
    .line 1054
    .local v0, e:Ljava/lang/Exception;
    :goto_25
    :try_start_25
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@27
    new-instance v5, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v6, "setKeyguardState("

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    const-string v6, "): exception occurs"

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_25 .. :try_end_43} :catchall_4b

    #@43
    .line 1057
    if-eqz v2, :cond_20

    #@45
    .line 1058
    :try_start_45
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_48} :catch_49

    #@48
    goto :goto_20

    #@49
    .line 1060
    :catch_49
    move-exception v4

    #@4a
    goto :goto_20

    #@4b
    .line 1056
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_4b
    move-exception v4

    #@4c
    .line 1057
    :goto_4c
    if-eqz v2, :cond_51

    #@4e
    .line 1058
    :try_start_4e
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_51} :catch_52

    #@51
    .line 1060
    :cond_51
    :goto_51
    throw v4

    #@52
    :catch_52
    move-exception v5

    #@53
    goto :goto_51

    #@54
    .line 1056
    .end local v2           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    :catchall_54
    move-exception v4

    #@55
    move-object v2, v3

    #@56
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v2       #writer:Ljava/io/FileWriter;
    goto :goto_4c

    #@57
    .line 1053
    .end local v2           #writer:Ljava/io/FileWriter;
    .restart local v3       #writer:Ljava/io/FileWriter;
    :catch_57
    move-exception v0

    #@58
    move-object v2, v3

    #@59
    .end local v3           #writer:Ljava/io/FileWriter;
    .restart local v2       #writer:Ljava/io/FileWriter;
    goto :goto_25
.end method

.method private shouldEnableScreenRotation()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 334
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v0

    #@7
    .line 335
    .local v0, res:Landroid/content/res/Resources;
    const-string v2, "lockscreen.rot_override"

    #@9
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_2c

    #@f
    const v2, 0x1110027

    #@12
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_2c

    #@18
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@1a
    if-eqz v2, :cond_2d

    #@1c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1f
    move-result-object v2

    #@20
    if-eqz v2, :cond_2d

    #@22
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isScreenRotationEnabled()Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_2d

    #@2c
    :cond_2c
    const/4 v1, 0x1

    #@2d
    :cond_2d
    return v1
.end method

.method private updateUserActivityTimeoutInQuickCoverWindow()V
    .registers 6

    #@0
    .prologue
    .line 1241
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    if-eqz v2, :cond_11

    #@4
    .line 1242
    const-wide/16 v0, 0x1b58

    #@6
    .line 1243
    .local v0, timeout:J
    const-wide/16 v2, 0x0

    #@8
    cmp-long v2, v0, v2

    #@a
    if-ltz v2, :cond_11

    #@c
    .line 1244
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@e
    iput-wide v0, v2, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@10
    .line 1251
    .end local v0           #timeout:J
    :goto_10
    return-void

    #@11
    .line 1250
    :cond_11
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@13
    const-wide/16 v3, 0x2710

    #@15
    iput-wide v3, v2, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@17
    goto :goto_10
.end method

.method private updateUserActivityTimeoutInWindowLayoutParams()V
    .registers 6

    #@0
    .prologue
    .line 717
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    if-eqz v2, :cond_15

    #@4
    .line 718
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@6
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getUserActivityTimeout()J

    #@9
    move-result-wide v0

    #@a
    .line 719
    .local v0, timeout:J
    const-wide/16 v2, 0x0

    #@c
    cmp-long v2, v0, v2

    #@e
    if-ltz v2, :cond_15

    #@10
    .line 720
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@12
    iput-wide v0, v2, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@14
    .line 727
    .end local v0           #timeout:J
    :goto_14
    return-void

    #@15
    .line 726
    :cond_15
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@17
    const-wide/16 v3, 0x2710

    #@19
    iput-wide v3, v2, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    goto :goto_14
.end method

.method private waitForEffectSurfaceDrawn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)Z
    .registers 4
    .parameter "showListener"

    #@0
    .prologue
    .line 841
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$3;

    #@6
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->waitForEffectSurfaceDrawn(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IEffectSurfaceDrawingCompleteListener;)Z

    #@c
    move-result v0

    #@d
    return v0
.end method


# virtual methods
.method public clearLockPattern()Z
    .registers 3

    #@0
    .prologue
    .line 1066
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2
    const-string v1, "clearLockPattern()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1067
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@9
    if-nez v0, :cond_d

    #@b
    .line 1068
    const/4 v0, 0x0

    #@c
    .line 1071
    :goto_c
    return v0

    #@d
    .line 1070
    :cond_d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->clearLockPattern()V

    #@12
    .line 1071
    const/4 v0, 0x1

    #@13
    goto :goto_c
.end method

.method public declared-synchronized dismiss()V
    .registers 2

    #@0
    .prologue
    .line 1012
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mScreenOn:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1013
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->dismiss()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 1015
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 1012
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public declared-synchronized expirePasswordReset()V
    .registers 3

    #@0
    .prologue
    .line 1027
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    const-string v1, "LGMDM expirePasswordReset()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1029
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 1030
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->expirePasswordReset()V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 1032
    :cond_11
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 1027
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method public declared-synchronized hide()V
    .registers 8

    #@0
    .prologue
    .line 942
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_1b

    #@d
    .line 943
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@f
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_a0

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_1b

    #@19
    .line 1005
    :cond_19
    :goto_19
    monitor-exit p0

    #@1a
    return-void

    #@1b
    .line 947
    :cond_1b
    :try_start_1b
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@1d
    const-string v4, "hide()"

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 948
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_GHOST_FINGER:Z

    #@24
    if-eqz v3, :cond_2b

    #@26
    .line 949
    const-string v3, "0"

    #@28
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->setKeyguardState(Ljava/lang/String;)V

    #@2b
    .line 953
    :cond_2b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@2d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@30
    move-result-object v3

    #@31
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mUpdateMonitorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@33
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@36
    .line 957
    const-string v3, "service.keyguard.status"

    #@38
    const-string v4, "0"

    #@3a
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 958
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3f
    const-string v4, "LockScreen status : 0 (Lock released)"

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 961
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@46
    if-eqz v3, :cond_19

    #@48
    .line 962
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@4a
    const/16 v4, 0x8

    #@4c
    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@4f
    .line 965
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@51
    if-eqz v3, :cond_6f

    #@53
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mNeedsInput:Z

    #@55
    if-eqz v3, :cond_6f

    #@57
    .line 966
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@59
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, "input_method"

    #@5f
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@62
    move-result-object v0

    #@63
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@65
    .line 968
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@67
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getWindowToken()Landroid/os/IBinder;

    #@6a
    move-result-object v3

    #@6b
    const/4 v4, 0x0

    #@6c
    invoke-virtual {v0, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@6f
    .line 975
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_6f
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mStateContainer:Landroid/util/SparseArray;

    #@71
    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    #@74
    .line 979
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@76
    if-eqz v3, :cond_19

    #@78
    .line 980
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@7a
    if-eqz v3, :cond_8c

    #@7c
    .line 982
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@7e
    const v4, 0x20d005a

    #@81
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewById(I)Landroid/view/View;

    #@84
    move-result-object v2

    #@85
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@87
    .line 984
    .local v2, nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    if-eqz v2, :cond_8c

    #@89
    .line 985
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->cleanUp()V

    #@8c
    .line 988
    .end local v2           #nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    :cond_8c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@8e
    .line 989
    .local v1, lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    const/4 v3, 0x0

    #@8f
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@91
    .line 990
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@93
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;

    #@95
    invoke-direct {v4, p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;)V

    #@98
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mDelayTimeRemoveView:I

    #@9a
    int-to-long v5, v5

    #@9b
    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_9e
    .catchall {:try_start_1b .. :try_end_9e} :catchall_a0

    #@9e
    goto/16 :goto_19

    #@a0
    .line 942
    .end local v1           #lastView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    :catchall_a0
    move-exception v3

    #@a1
    monitor-exit p0

    #@a2
    throw v3
.end method

.method public declared-synchronized hideSKTLocked()V
    .registers 3

    #@0
    .prologue
    .line 1084
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 1085
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@7
    const-string v1, "[SKT Lock&Wipe] hideSKTLocked()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1086
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->hideSKTLocked()V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 1088
    :cond_11
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 1084
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method public declared-synchronized isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 1021
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    #@3
    if-eqz v0, :cond_8

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_6

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized onScreenTurnedOff()V
    .registers 3

    #@0
    .prologue
    .line 825
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    const-string v1, "onScreenTurnedOff()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 826
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mScreenOn:Z

    #@b
    .line 829
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_20

    #@17
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 831
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@1d
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onScreenTurnedOff()V

    #@20
    .line 835
    :cond_20
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@22
    if-eqz v0, :cond_29

    #@24
    .line 836
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onScreenTurnedOff()V
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_2b

    #@29
    .line 838
    :cond_29
    monitor-exit p0

    #@2a
    return-void

    #@2b
    .line 825
    :catchall_2b
    move-exception v0

    #@2c
    monitor-exit p0

    #@2d
    throw v0
.end method

.method public declared-synchronized onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V
    .registers 6
    .parameter "showListener"

    #@0
    .prologue
    .line 861
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    const-string v3, "onScreenTurnedOn()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 862
    const/4 v2, 0x1

    #@9
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mScreenOn:Z

    #@b
    .line 865
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@d
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_20

    #@17
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@19
    if-eqz v2, :cond_20

    #@1b
    .line 867
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@1d
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onScreenTurnedOn()V

    #@20
    .line 871
    :cond_20
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@22
    if-eqz v2, :cond_68

    #@24
    .line 872
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@26
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onScreenTurnedOn()V

    #@29
    .line 876
    if-eqz p1, :cond_5e

    #@2b
    .line 877
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2d
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getVisibility()I

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_60

    #@33
    .line 878
    const/4 v1, 0x0

    #@34
    .line 879
    .local v1, waitForEffectSurfaceDrawing:Z
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@36
    if-eqz v2, :cond_52

    #@38
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@3b
    move-result-object v2

    #@3c
    if-eqz v2, :cond_52

    #@3e
    .line 881
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@40
    const-string v3, "EffectSurfaceView"

    #@42
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@45
    move-result-object v0

    #@46
    .line 883
    .local v0, effectSurfaceView:Landroid/view/View;
    if-eqz v0, :cond_52

    #@48
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@4b
    move-result-object v2

    #@4c
    if-eqz v2, :cond_52

    #@4e
    .line 884
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->waitForEffectSurfaceDrawn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)Z

    #@51
    move-result v1

    #@52
    .line 888
    .end local v0           #effectSurfaceView:Landroid/view/View;
    :cond_52
    if-nez v1, :cond_5e

    #@54
    .line 891
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@56
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$4;

    #@58
    invoke-direct {v3, p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@5b
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z
    :try_end_5e
    .catchall {:try_start_1 .. :try_end_5e} :catchall_65

    #@5e
    .line 909
    .end local v1           #waitForEffectSurfaceDrawing:Z
    :cond_5e
    :goto_5e
    monitor-exit p0

    #@5f
    return-void

    #@60
    .line 903
    :cond_60
    const/4 v2, 0x0

    #@61
    :try_start_61
    invoke-interface {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;->onShown(Landroid/os/IBinder;)V
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_65

    #@64
    goto :goto_5e

    #@65
    .line 861
    :catchall_65
    move-exception v2

    #@66
    monitor-exit p0

    #@67
    throw v2

    #@68
    .line 906
    :cond_68
    if-eqz p1, :cond_5e

    #@6a
    .line 907
    const/4 v2, 0x0

    #@6b
    :try_start_6b
    invoke-interface {p1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;->onShown(Landroid/os/IBinder;)V
    :try_end_6e
    .catchall {:try_start_6b .. :try_end_6e} :catchall_65

    #@6e
    goto :goto_5e
.end method

.method public declared-synchronized reset(Landroid/os/Bundle;)V
    .registers 7
    .parameter "options"

    #@0
    .prologue
    .line 796
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    const-string v3, "[kjj]reset()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 797
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@a
    if-eqz v2, :cond_5a

    #@c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@f
    move-result-object v2

    #@10
    if-eqz v2, :cond_5a

    #@12
    .line 799
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@14
    if-eqz v2, :cond_3c

    #@16
    .line 800
    const/4 v0, 0x0

    #@17
    .line 801
    .local v0, LOCK_SCREEN_TYPE_SWIPE:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@19
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "lockscreen_type_password_unspecified"

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@23
    move-result v1

    #@24
    .line 803
    .local v1, locksetting:I
    if-ne v1, v0, :cond_30

    #@26
    .line 804
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@29
    move-result v2

    #@2a
    const/4 v3, 0x0

    #@2b
    invoke-direct {p0, v2, v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_39

    #@2e
    .line 822
    .end local v0           #LOCK_SCREEN_TYPE_SWIPE:I
    .end local v1           #locksetting:I
    :goto_2e
    monitor-exit p0

    #@2f
    return-void

    #@30
    .line 806
    .restart local v0       #LOCK_SCREEN_TYPE_SWIPE:I
    .restart local v1       #locksetting:I
    :cond_30
    :try_start_30
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@33
    move-result v2

    #@34
    const/4 v3, 0x1

    #@35
    invoke-direct {p0, v2, v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V
    :try_end_38
    .catchall {:try_start_30 .. :try_end_38} :catchall_39

    #@38
    goto :goto_2e

    #@39
    .line 796
    .end local v0           #LOCK_SCREEN_TYPE_SWIPE:I
    .end local v1           #locksetting:I
    :catchall_39
    move-exception v2

    #@3a
    monitor-exit p0

    #@3b
    throw v2

    #@3c
    .line 810
    :cond_3c
    :try_start_3c
    const-string v2, "ro.product.model"

    #@3e
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    const-string v3, "VS980 4G"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v2

    #@48
    if-eqz v2, :cond_51

    #@4a
    .line 811
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@4d
    move-result v2

    #@4e
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeEnableScreenRotation(Z)V

    #@51
    .line 815
    :cond_51
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@54
    move-result v2

    #@55
    const/4 v3, 0x0

    #@56
    invoke-direct {p0, v2, v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V

    #@59
    goto :goto_2e

    #@5a
    .line 820
    :cond_5a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@5d
    move-result v2

    #@5e
    const/4 v3, 0x1

    #@5f
    invoke-direct {p0, v2, v3, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V
    :try_end_62
    .catchall {:try_start_3c .. :try_end_62} :catchall_39

    #@62
    goto :goto_2e
.end method

.method public setNeedsInput(Z)V
    .registers 7
    .parameter "needsInput"

    #@0
    .prologue
    .line 761
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mNeedsInput:Z

    #@2
    .line 762
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@4
    if-eqz v2, :cond_1b

    #@6
    .line 763
    if-eqz p1, :cond_1c

    #@8
    .line 764
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@c
    const v4, -0x20001

    #@f
    and-int/2addr v3, v4

    #@10
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@12
    .line 784
    :cond_12
    :goto_12
    :try_start_12
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@14
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@16
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@18
    invoke-interface {v2, v3, v4}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_1b} :catch_3e

    #@1b
    .line 790
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 767
    :cond_1c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@1e
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@20
    const/high16 v4, 0x2

    #@22
    or-int/2addr v3, v4

    #@23
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@25
    .line 771
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@27
    if-nez v2, :cond_12

    #@29
    .line 772
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@2b
    if-eqz v2, :cond_12

    #@2d
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@30
    move-result-object v2

    #@31
    if-eqz v2, :cond_12

    #@33
    .line 774
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@36
    move-result-object v1

    #@37
    .line 775
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_12

    #@39
    .line 776
    const/4 v2, 0x0

    #@3a
    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->startGettingWindowFocus(Landroid/view/View;)V

    #@3d
    goto :goto_12

    #@3e
    .line 785
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :catch_3e
    move-exception v0

    #@3f
    .line 787
    .local v0, e:Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@41
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "Can\'t update input method on "

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v4, " window not attached"

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_1b
.end method

.method public declared-synchronized show(Landroid/os/Bundle;)V
    .registers 10
    .parameter "options"

    #@0
    .prologue
    .line 246
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    new-instance v6, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v7, "show(); mKeyguardView=="

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 247
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_GHOST_FINGER:Z

    #@1d
    if-eqz v5, :cond_24

    #@1f
    .line 248
    const-string v5, "1"

    #@21
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->setKeyguardState(Ljava/lang/String;)V

    #@24
    .line 251
    :cond_24
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@26
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@29
    move-result-object v5

    #@2a
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mUpdateMonitorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@2c
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@2f
    .line 253
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@31
    instance-of v5, v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@33
    if-eqz v5, :cond_47

    #@35
    .line 254
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@37
    check-cast v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;

    #@39
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@3b
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isKeyguardVisible()Z

    #@42
    move-result v6

    #@43
    const/4 v7, 0x1

    #@44
    invoke-virtual {v5, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->setKeyguardIsVisible(ZZ)V

    #@47
    .line 260
    :cond_47
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@49
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_13c

    #@4f
    .line 261
    const-string v5, "service.keyguard.status"

    #@51
    const-string v6, "2"

    #@53
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 262
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@58
    const-string v6, "LockScreen status : 2 (Secure Lock)"

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 270
    :goto_5d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@60
    move-result v0

    #@61
    .line 272
    .local v0, enableScreenRotation:Z
    const/4 v5, 0x0

    #@62
    invoke-direct {p0, v0, v5, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V

    #@65
    .line 273
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeEnableScreenRotation(Z)V

    #@68
    .line 279
    const/high16 v4, 0x20

    #@6a
    .line 281
    .local v4, visFlags:I
    sget-boolean v5, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@6c
    if-eqz v5, :cond_82

    #@6e
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@71
    move-result-object v5

    #@72
    if-eqz v5, :cond_82

    #@74
    .line 283
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isNaviBarRemoved()Z

    #@7b
    move-result v5

    #@7c
    if-eqz v5, :cond_82

    #@7e
    .line 284
    const v5, -0x200001

    #@81
    and-int/2addr v4, v5

    #@82
    .line 288
    :cond_82
    sget-boolean v5, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@84
    if-eqz v5, :cond_ac

    #@86
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@89
    move-result-object v5

    #@8a
    if-eqz v5, :cond_ac

    #@8c
    .line 291
    or-int/lit16 v4, v4, 0x600

    #@8e
    .line 292
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@91
    move-result-object v5

    #@92
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isScreenDecoTransparent()Z

    #@95
    move-result v5

    #@96
    if-eqz v5, :cond_ac

    #@98
    .line 293
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@9a
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@9c
    if-eq v5, v6, :cond_ac

    #@9e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a0
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a2
    if-eq v5, v6, :cond_ac

    #@a4
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mCurrentSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a6
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a8
    if-eq v5, v6, :cond_ac

    #@aa
    .line 296
    or-int/lit8 v4, v4, 0x10

    #@ac
    .line 300
    :cond_ac
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@ae
    new-instance v6, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v7, "show:setSystemUiVisibility("

    #@b5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v6

    #@b9
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v6

    #@c1
    const-string v7, ")"

    #@c3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v6

    #@cb
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 301
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@d0
    invoke-virtual {v5, v4}, Landroid/widget/FrameLayout;->setSystemUiVisibility(I)V

    #@d3
    .line 303
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@d5
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@d7
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@d9
    invoke-interface {v5, v6, v7}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@dc
    .line 304
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@de
    const/4 v6, 0x0

    #@df
    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@e2
    .line 305
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@e4
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->show()V

    #@e7
    .line 306
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@e9
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->requestFocus()Z

    #@ec
    .line 307
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@ee
    if-eqz v5, :cond_13a

    #@f0
    .line 309
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@f2
    const v6, 0x20d005a

    #@f5
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewById(I)Landroid/view/View;

    #@f8
    move-result-object v2

    #@f9
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@fb
    .line 311
    .local v2, nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    if-eqz v2, :cond_13a

    #@fd
    .line 312
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsBootCompleted:Z

    #@ff
    if-eqz v5, :cond_14f

    #@101
    .line 313
    new-instance v1, Landroid/content/Intent;

    #@103
    const-string v5, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@105
    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@108
    .line 315
    .local v1, mascotIntent:Landroid/content/Intent;
    const-string v5, "RemoteViews"

    #@10a
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mRemoteViews:Landroid/widget/RemoteViews;

    #@10c
    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10f
    .line 316
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@111
    const-string v6, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@113
    invoke-virtual {v5, v1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@116
    .line 323
    .end local v1           #mascotIntent:Landroid/content/Intent;
    :goto_116
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@118
    const-string v6, "KeyguardViewManager.show(): bindService IScreenLockService"

    #@11a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11d
    .line 324
    new-instance v3, Landroid/content/Intent;

    #@11f
    const-class v5, Lcom/nttdocomo/android/screenlockservice/IScreenLockService;

    #@121
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@124
    move-result-object v5

    #@125
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@128
    .line 325
    .local v3, screenLockServiceIntent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@12a
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mServiceConnection:Landroid/content/ServiceConnection;

    #@12c
    const/4 v7, 0x1

    #@12d
    invoke-virtual {v5, v3, v6, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@130
    move-result v5

    #@131
    if-nez v5, :cond_13a

    #@133
    .line 326
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@135
    const-string v6, "KeyguardViewManager.show(): can\'t connect IScreenLockService"

    #@137
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13a
    .catchall {:try_start_1 .. :try_end_13a} :catchall_14c

    #@13a
    .line 331
    .end local v2           #nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    .end local v3           #screenLockServiceIntent:Landroid/content/Intent;
    :cond_13a
    monitor-exit p0

    #@13b
    return-void

    #@13c
    .line 265
    .end local v0           #enableScreenRotation:Z
    .end local v4           #visFlags:I
    :cond_13c
    :try_start_13c
    const-string v5, "service.keyguard.status"

    #@13e
    const-string v6, "1"

    #@140
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@143
    .line 266
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@145
    const-string v6, "LockScreen status : 1 (Normal Lock)"

    #@147
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14a
    .catchall {:try_start_13c .. :try_end_14a} :catchall_14c

    #@14a
    goto/16 :goto_5d

    #@14c
    .line 246
    :catchall_14c
    move-exception v5

    #@14d
    monitor-exit p0

    #@14e
    throw v5

    #@14f
    .line 319
    .restart local v0       #enableScreenRotation:Z
    .restart local v2       #nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    .restart local v4       #visFlags:I
    :cond_14f
    const/4 v5, 0x1

    #@150
    :try_start_150
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mIsWaitingForBootComplete:Z
    :try_end_152
    .catchall {:try_start_150 .. :try_end_152} :catchall_14c

    #@152
    goto :goto_116
.end method

.method public showAssistant()V
    .registers 2

    #@0
    .prologue
    .line 1036
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1037
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->showAssistant()V

    #@9
    .line 1039
    :cond_9
    return-void
.end method

.method public showQuickCoverWindow(Z)V
    .registers 13
    .parameter "value"

    #@0
    .prologue
    const/16 v10, 0x8

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 1140
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@6
    if-eqz v5, :cond_10

    #@8
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    if-eqz v5, :cond_10

    #@c
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@e
    if-nez v5, :cond_13

    #@10
    .line 1141
    :cond_10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->createKeyguardHostAndQuickCover()V

    #@13
    .line 1144
    :cond_13
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@16
    move-result v1

    #@17
    .line 1145
    .local v1, enableScreenRotation:Z
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeEnableScreenRotation(Z)V

    #@1a
    .line 1147
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@1c
    if-nez v5, :cond_3e

    #@1e
    .line 1148
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@21
    move-result-object v5

    #@22
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@24
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@26
    invoke-virtual {v5, v6, v7}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->createQuickCoverHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@29
    move-result-object v5

    #@2a
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2c
    .line 1149
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@2e
    if-eqz v5, :cond_3e

    #@30
    .line 1150
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@32
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@34
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setViewMediatorCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;)V

    #@37
    .line 1151
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@39
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@3b
    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@3e
    .line 1155
    :cond_3e
    const/16 v5, 0x1f4

    #@40
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mDelayTimeRemoveView:I

    #@42
    .line 1157
    if-eqz p1, :cond_a3

    #@44
    .line 1158
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@46
    if-eqz v5, :cond_a2

    #@48
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@4a
    if-eqz v5, :cond_a2

    #@4c
    .line 1159
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@4e
    if-eqz v5, :cond_55

    #@50
    .line 1160
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@52
    invoke-virtual {v5, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@55
    .line 1162
    :cond_55
    const-string v5, "dhlee"

    #@57
    new-instance v6, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    const-string v7, ": showQuickCoverWindow(true)"

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v6

    #@6c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 1163
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@71
    invoke-virtual {v5, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@74
    .line 1165
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@76
    const/4 v6, 0x4

    #@77
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setSystemUiVisibility(I)V

    #@7a
    .line 1166
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@7c
    invoke-virtual {v5, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@7f
    .line 1168
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@81
    iget v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@83
    and-int/lit16 v6, v6, -0x801

    #@85
    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@87
    .line 1169
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@89
    iget v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@8b
    or-int/lit16 v6, v6, 0x400

    #@8d
    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@8f
    .line 1171
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@91
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@93
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@95
    invoke-interface {v5, v6, v7}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@98
    .line 1172
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@9a
    invoke-interface {v5, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNeedsInput(Z)V

    #@9d
    .line 1173
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@9f
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->requestFocus()Z

    #@a2
    .line 1238
    :cond_a2
    :goto_a2
    return-void

    #@a3
    .line 1176
    :cond_a3
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mDelayTimeRemoveView:I

    #@a5
    .line 1178
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@a7
    if-eqz v5, :cond_a2

    #@a9
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@ab
    if-eqz v5, :cond_a2

    #@ad
    .line 1179
    const-string v5, "dhlee"

    #@af
    new-instance v6, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@b6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    const-string v7, ": showQuickCoverWindow(false)"

    #@bc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v6

    #@c0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v6

    #@c4
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 1181
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@c9
    iget v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@cb
    or-int/lit16 v6, v6, 0x800

    #@cd
    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@cf
    .line 1182
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@d1
    iget v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@d3
    and-int/lit16 v6, v6, -0x401

    #@d5
    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@d7
    .line 1184
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@d9
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@db
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@dd
    invoke-interface {v5, v6, v7}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@e0
    .line 1185
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@e2
    if-eqz v5, :cond_a2

    #@e4
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@e6
    if-eqz v5, :cond_a2

    #@e8
    .line 1186
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@ea
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@ed
    move-result-object v5

    #@ee
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f0
    if-ne v5, v6, :cond_12f

    #@f2
    .line 1187
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@f4
    if-eqz v5, :cond_fc

    #@f6
    .line 1188
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@f8
    invoke-interface {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDone(Z)V

    #@fb
    goto :goto_a2

    #@fc
    .line 1190
    :cond_fc
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@fe
    invoke-virtual {v5, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@101
    .line 1191
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@103
    invoke-virtual {v5, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@106
    .line 1192
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@108
    invoke-interface {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->wakeUp()V

    #@10b
    .line 1195
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@10d
    instance-of v5, v5, Landroid/app/Activity;

    #@10f
    if-nez v5, :cond_a2

    #@111
    .line 1196
    new-instance v3, Landroid/content/Intent;

    #@113
    const-string v5, "android.intent.action.USER_PRESENT"

    #@115
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@118
    .line 1197
    .local v3, mUserPresentIntent:Landroid/content/Intent;
    const/high16 v5, 0x2800

    #@11a
    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11d
    .line 1199
    new-instance v0, Landroid/os/UserHandle;

    #@11f
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@121
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@124
    move-result v5

    #@125
    invoke-direct {v0, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@128
    .line 1200
    .local v0, currentUser:Landroid/os/UserHandle;
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@12a
    invoke-virtual {v5, v3, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@12d
    goto/16 :goto_a2

    #@12f
    .line 1204
    .end local v0           #currentUser:Landroid/os/UserHandle;
    .end local v3           #mUserPresentIntent:Landroid/content/Intent;
    :cond_12f
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@131
    if-eqz v5, :cond_1d2

    #@133
    .line 1205
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@135
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@138
    move-result-object v5

    #@139
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@13b
    if-eq v5, v6, :cond_147

    #@13d
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@13f
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@142
    move-result-object v5

    #@143
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@145
    if-ne v5, v6, :cond_14c

    #@147
    .line 1206
    :cond_147
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@149
    invoke-interface {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNeedsInput(Z)V

    #@14c
    .line 1209
    :cond_14c
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@14e
    const-string v6, "keyguard_account_view"

    #@150
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@153
    move-result-object v2

    #@154
    .line 1210
    .local v2, kav:Landroid/view/View;
    if-eqz v2, :cond_168

    #@156
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@159
    move-result v5

    #@15a
    if-nez v5, :cond_168

    #@15c
    .line 1211
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@15e
    invoke-interface {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNeedsInput(Z)V

    #@161
    .line 1212
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@163
    const-string v6, ": kav != null, kav needsinput = true"

    #@165
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@168
    .line 1214
    :cond_168
    sget-boolean v5, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@16a
    if-eqz v5, :cond_17f

    #@16c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@16f
    move-result-object v5

    #@170
    if-eqz v5, :cond_17f

    #@172
    .line 1216
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@174
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@177
    move-result-object v6

    #@178
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    #@17b
    move-result-object v6

    #@17c
    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@17f
    .line 1219
    :cond_17f
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@181
    invoke-virtual {v5, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@184
    .line 1220
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@186
    const-string v6, "multi_pane_challenge"

    #@188
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@18b
    move-result-object v5

    #@18c
    if-eqz v5, :cond_1c2

    #@18e
    .line 1221
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@190
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@193
    move-result-object v5

    #@194
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@197
    move-result-object v5

    #@198
    iget v4, v5, Landroid/content/res/Configuration;->orientation:I

    #@19a
    .line 1222
    .local v4, viewOfOrientation:I
    if-ne v4, v9, :cond_1c2

    #@19c
    .line 1223
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@19e
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string v7, "mKeyguardView has multi_pane_challenge, orientation = "

    #@1a5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v6

    #@1a9
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v6

    #@1ad
    const-string v7, ", recreate mKeyguardView"

    #@1af
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v6

    #@1b3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v6

    #@1b7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    .line 1224
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->shouldEnableScreenRotation()Z

    #@1bd
    move-result v5

    #@1be
    const/4 v6, 0x0

    #@1bf
    invoke-direct {p0, v5, v9, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->maybeCreateKeyguardLocked(ZZLandroid/os/Bundle;)V

    #@1c2
    .line 1230
    .end local v2           #kav:Landroid/view/View;
    .end local v4           #viewOfOrientation:I
    :cond_1c2
    :goto_1c2
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mQuickCoverWindowHostView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@1c4
    invoke-virtual {v5, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@1c7
    .line 1231
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@1c9
    if-eqz v5, :cond_a2

    #@1cb
    .line 1232
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@1cd
    invoke-interface {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->wakeUp()V

    #@1d0
    goto/16 :goto_a2

    #@1d2
    .line 1228
    :cond_1d2
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@1d4
    invoke-virtual {v5, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@1d7
    goto :goto_1c2
.end method

.method public declared-synchronized showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    .line 1077
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@3
    if-eqz v0, :cond_3c

    #@5
    .line 1078
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "[SKT Lock&Wipe] showSKTLocked("

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, ", "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, ", "

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, ")"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1079
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@39
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_3c
    .catchall {:try_start_1 .. :try_end_3c} :catchall_3e

    #@3c
    .line 1081
    :cond_3c
    monitor-exit p0

    #@3d
    return-void

    #@3e
    .line 1077
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit p0

    #@40
    throw v0
.end method

.method public updateUserActivityTimeout()V
    .registers 4

    #@0
    .prologue
    .line 705
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_25

    #@c
    .line 706
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mContext:Landroid/content/Context;

    #@e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 707
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->updateUserActivityTimeoutInQuickCoverWindow()V

    #@1b
    .line 712
    :cond_1b
    :goto_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mViewManager:Landroid/view/ViewManager;

    #@1d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardHost:Landroid/widget/FrameLayout;

    #@1f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@21
    invoke-interface {v0, v1, v2}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@24
    .line 713
    return-void

    #@25
    .line 710
    :cond_25
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->updateUserActivityTimeoutInWindowLayoutParams()V

    #@28
    goto :goto_1b
.end method

.method public declared-synchronized verifyUnlock()V
    .registers 3

    #@0
    .prologue
    .line 912
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@3
    const-string v1, "verifyUnlock()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 913
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->show(Landroid/os/Bundle;)V

    #@c
    .line 914
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->verifyUnlock()V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 915
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 912
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method public wakeWhenReadyTq(I)Z
    .registers 5
    .parameter "keyCode"

    #@0
    .prologue
    .line 929
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "wakeWhenReady("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 930
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@20
    if-eqz v0, :cond_29

    #@22
    .line 931
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->mKeyguardView:Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@24
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->wakeWhenReadyTq(I)V

    #@27
    .line 932
    const/4 v0, 0x1

    #@28
    .line 935
    :goto_28
    return v0

    #@29
    .line 934
    :cond_29
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->TAG:Ljava/lang/String;

    #@2b
    const-string v1, "mKeyguardView is null in wakeWhenReadyTq"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 935
    const/4 v0, 0x0

    #@31
    goto :goto_28
.end method
