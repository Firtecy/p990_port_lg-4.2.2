.class final Lcom/android/internal/policy/impl/PhoneWindow$DecorView;
.super Landroid/widget/FrameLayout;
.source "PhoneWindow.java"

# interfaces
.implements Lcom/android/internal/view/RootViewSurfaceTaker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DecorView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/PhoneWindow$DecorView$ActionModeCallbackWrapper;
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModePopup:Landroid/widget/PopupWindow;

.field private mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

.field private final mBackgroundPadding:Landroid/graphics/Rect;

.field private mChanging:Z

.field mDefaultOpacity:I

.field private mDownY:I

.field private final mDrawingBounds:Landroid/graphics/Rect;

.field private final mFeatureId:I

.field private final mFrameOffsets:Landroid/graphics/Rect;

.field private final mFramePadding:Landroid/graphics/Rect;

.field private mMenuBackground:Landroid/graphics/drawable/Drawable;

.field private mShowActionModePopup:Ljava/lang/Runnable;

.field private mWatchingForMenu:Z

.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindow;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/PhoneWindow;Landroid/content/Context;I)V
    .registers 5
    .parameter
    .parameter "context"
    .parameter "featureId"

    #@0
    .prologue
    .line 2025
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    .line 2026
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@5
    .line 2001
    const/4 v0, -0x1

    #@6
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mDefaultOpacity:I

    #@8
    .line 2006
    new-instance v0, Landroid/graphics/Rect;

    #@a
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mDrawingBounds:Landroid/graphics/Rect;

    #@f
    .line 2008
    new-instance v0, Landroid/graphics/Rect;

    #@11
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@16
    .line 2010
    new-instance v0, Landroid/graphics/Rect;

    #@18
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@1d
    .line 2012
    new-instance v0, Landroid/graphics/Rect;

    #@1f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@22
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFrameOffsets:Landroid/graphics/Rect;

    #@24
    .line 2027
    iput p3, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@26
    .line 2028
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Lcom/android/internal/widget/ActionBarContextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2000
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Landroid/widget/PopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2000
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2000
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mShowActionModePopup:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Landroid/view/ActionMode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2000
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2000
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@2
    return-object p1
.end method

.method private drawableChanged()V
    .registers 12

    #@0
    .prologue
    const/4 v10, -0x1

    #@1
    .line 2625
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mChanging:Z

    #@3
    if-eqz v5, :cond_6

    #@5
    .line 2683
    :cond_5
    :goto_5
    return-void

    #@6
    .line 2629
    :cond_6
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@8
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@a
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@c
    iget v6, v6, Landroid/graphics/Rect;->left:I

    #@e
    add-int/2addr v5, v6

    #@f
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@11
    iget v6, v6, Landroid/graphics/Rect;->top:I

    #@13
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@15
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@17
    add-int/2addr v6, v7

    #@18
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@1a
    iget v7, v7, Landroid/graphics/Rect;->right:I

    #@1c
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@1e
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@20
    add-int/2addr v7, v8

    #@21
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@23
    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    #@25
    iget-object v9, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@27
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@29
    add-int/2addr v8, v9

    #@2a
    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setPadding(IIII)V

    #@2d
    .line 2632
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->requestLayout()V

    #@30
    .line 2633
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->invalidate()V

    #@33
    .line 2635
    const/4 v4, -0x1

    #@34
    .line 2642
    .local v4, opacity:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@37
    move-result-object v0

    #@38
    .line 2643
    .local v0, bg:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getForeground()Landroid/graphics/drawable/Drawable;

    #@3b
    move-result-object v2

    #@3c
    .line 2644
    .local v2, fg:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_44

    #@3e
    .line 2645
    if-nez v2, :cond_50

    #@40
    .line 2646
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@43
    move-result v4

    #@44
    .line 2679
    :cond_44
    :goto_44
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mDefaultOpacity:I

    #@46
    .line 2680
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@48
    if-gez v5, :cond_5

    #@4a
    .line 2681
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@4c
    invoke-static {v5, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1800(Lcom/android/internal/policy/impl/PhoneWindow;I)V

    #@4f
    goto :goto_5

    #@50
    .line 2647
    :cond_50
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@52
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@54
    if-gtz v5, :cond_83

    #@56
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@58
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@5a
    if-gtz v5, :cond_83

    #@5c
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@5e
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@60
    if-gtz v5, :cond_83

    #@62
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@64
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@66
    if-gtz v5, :cond_83

    #@68
    .line 2651
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@6b
    move-result v3

    #@6c
    .line 2652
    .local v3, fop:I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@6f
    move-result v1

    #@70
    .line 2655
    .local v1, bop:I
    if-eq v3, v10, :cond_74

    #@72
    if-ne v1, v10, :cond_76

    #@74
    .line 2656
    :cond_74
    const/4 v4, -0x1

    #@75
    goto :goto_44

    #@76
    .line 2657
    :cond_76
    if-nez v3, :cond_7a

    #@78
    .line 2658
    move v4, v1

    #@79
    goto :goto_44

    #@7a
    .line 2659
    :cond_7a
    if-nez v1, :cond_7e

    #@7c
    .line 2660
    move v4, v3

    #@7d
    goto :goto_44

    #@7e
    .line 2662
    :cond_7e
    invoke-static {v3, v1}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    #@81
    move-result v4

    #@82
    goto :goto_44

    #@83
    .line 2670
    .end local v1           #bop:I
    .end local v3           #fop:I
    :cond_83
    const/4 v4, -0x3

    #@84
    goto :goto_44
.end method

.method private isOutOfBounds(II)Z
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v0, -0x5

    #@1
    .line 2197
    if-lt p1, v0, :cond_15

    #@3
    if-lt p2, v0, :cond_15

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getWidth()I

    #@8
    move-result v0

    #@9
    add-int/lit8 v0, v0, 0x5

    #@b
    if-gt p1, v0, :cond_15

    #@d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getHeight()I

    #@10
    move-result v0

    #@11
    add-int/lit8 v0, v0, 0x5

    #@13
    if-le p2, v0, :cond_17

    #@15
    :cond_15
    const/4 v0, 0x1

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method


# virtual methods
.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 2141
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@5
    move-result-object v0

    #@6
    .line 2142
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_19

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_19

    #@10
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@12
    if-gez v1, :cond_19

    #@14
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@17
    move-result v1

    #@18
    :goto_18
    return v1

    #@19
    :cond_19
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_18
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 11
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 2032
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v4

    #@6
    .line 2033
    .local v4, keyCode:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@9
    move-result v0

    #@a
    .line 2034
    .local v0, action:I
    if-nez v0, :cond_2c

    #@c
    move v3, v5

    #@d
    .line 2036
    .local v3, isDown:Z
    :goto_d
    if-eqz v3, :cond_4e

    #@f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@12
    move-result v7

    #@13
    if-nez v7, :cond_4e

    #@15
    .line 2039
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@17
    invoke-static {v7}, Lcom/android/internal/policy/impl/PhoneWindow;->access$800(Lcom/android/internal/policy/impl/PhoneWindow;)I

    #@1a
    move-result v7

    #@1b
    if-lez v7, :cond_2e

    #@1d
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1f
    invoke-static {v7}, Lcom/android/internal/policy/impl/PhoneWindow;->access$800(Lcom/android/internal/policy/impl/PhoneWindow;)I

    #@22
    move-result v7

    #@23
    if-eq v7, v4, :cond_2e

    #@25
    .line 2040
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@28
    move-result v2

    #@29
    .line 2041
    .local v2, handled:Z
    if-eqz v2, :cond_2e

    #@2b
    .line 2066
    .end local v2           #handled:Z
    :cond_2b
    :goto_2b
    return v5

    #@2c
    .end local v3           #isDown:Z
    :cond_2c
    move v3, v6

    #@2d
    .line 2034
    goto :goto_d

    #@2e
    .line 2048
    .restart local v3       #isDown:Z
    :cond_2e
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@30
    invoke-static {v7}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@33
    move-result-object v7

    #@34
    if-eqz v7, :cond_4e

    #@36
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@38
    invoke-static {v7}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@3b
    move-result-object v7

    #@3c
    iget-boolean v7, v7, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@3e
    if-eqz v7, :cond_4e

    #@40
    .line 2049
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@42
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@44
    invoke-static {v8}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@47
    move-result-object v8

    #@48
    invoke-static {v7, v8, v4, p1, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1000(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    #@4b
    move-result v6

    #@4c
    if-nez v6, :cond_2b

    #@4e
    .line 2055
    :cond_4e
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@50
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@53
    move-result v6

    #@54
    if-nez v6, :cond_6e

    #@56
    .line 2056
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@58
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@5b
    move-result-object v1

    #@5c
    .line 2057
    .local v1, cb:Landroid/view/Window$Callback;
    if-eqz v1, :cond_7d

    #@5e
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@60
    if-gez v6, :cond_7d

    #@62
    invoke-interface {v1, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@65
    move-result v2

    #@66
    .line 2060
    .restart local v2       #handled:Z
    :goto_66
    if-eqz v2, :cond_6e

    #@68
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->isRingingAndHeadsetKeyEvent(Landroid/view/KeyEvent;)Z

    #@6b
    move-result v6

    #@6c
    if-eqz v6, :cond_2b

    #@6e
    .line 2066
    .end local v1           #cb:Landroid/view/Window$Callback;
    .end local v2           #handled:Z
    :cond_6e
    if-eqz v3, :cond_82

    #@70
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@72
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@74
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@77
    move-result v7

    #@78
    invoke-virtual {v5, v6, v7, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->onKeyDown(IILandroid/view/KeyEvent;)Z

    #@7b
    move-result v5

    #@7c
    goto :goto_2b

    #@7d
    .line 2057
    .restart local v1       #cb:Landroid/view/Window$Callback;
    :cond_7d
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@80
    move-result v2

    #@81
    goto :goto_66

    #@82
    .line 2066
    .end local v1           #cb:Landroid/view/Window$Callback;
    :cond_82
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@84
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@86
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@89
    move-result v7

    #@8a
    invoke-virtual {v5, v6, v7, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->onKeyUp(IILandroid/view/KeyEvent;)Z

    #@8d
    move-result v5

    #@8e
    goto :goto_2b
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 10
    .parameter "ev"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 2089
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@4
    invoke-static {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@7
    move-result-object v5

    #@8
    if-eqz v5, :cond_2d

    #@a
    .line 2090
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@c
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@e
    invoke-static {v6}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@15
    move-result v7

    #@16
    invoke-static {v5, v6, v7, p1, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1000(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    #@19
    move-result v1

    #@1a
    .line 2092
    .local v1, handled:Z
    if-eqz v1, :cond_2d

    #@1c
    .line 2093
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1e
    invoke-static {v4}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@21
    move-result-object v4

    #@22
    if-eqz v4, :cond_2c

    #@24
    .line 2094
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@26
    invoke-static {v4}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@29
    move-result-object v4

    #@2a
    iput-boolean v3, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@2c
    .line 2122
    :cond_2c
    :goto_2c
    return v3

    #@2d
    .line 2101
    .end local v1           #handled:Z
    :cond_2d
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2f
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@32
    move-result-object v0

    #@33
    .line 2102
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_6a

    #@35
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@37
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@3a
    move-result v5

    #@3b
    if-nez v5, :cond_6a

    #@3d
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@3f
    if-gez v5, :cond_6a

    #@41
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@44
    move-result v1

    #@45
    .line 2104
    .restart local v1       #handled:Z
    :goto_45
    if-nez v1, :cond_2c

    #@47
    .line 2112
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@49
    invoke-static {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@4c
    move-result-object v5

    #@4d
    if-nez v5, :cond_68

    #@4f
    .line 2113
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@51
    invoke-static {v5, v4, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1200(Lcom/android/internal/policy/impl/PhoneWindow;IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@54
    move-result-object v2

    #@55
    .line 2114
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@57
    invoke-virtual {v5, v2, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@5a
    .line 2115
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@5c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5f
    move-result v6

    #@60
    invoke-static {v5, v2, v6, p1, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1000(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    #@63
    move-result v1

    #@64
    .line 2117
    iput-boolean v4, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@66
    .line 2118
    if-nez v1, :cond_2c

    #@68
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_68
    move v3, v4

    #@69
    .line 2122
    goto :goto_2c

    #@6a
    .line 2102
    .end local v1           #handled:Z
    :cond_6a
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@6d
    move-result v1

    #@6e
    goto :goto_45
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 2300
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@5
    move-result-object v0

    #@6
    .line 2301
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_18

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_18

    #@10
    .line 2302
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    .line 2303
    const/4 v1, 0x1

    #@17
    .line 2306
    :goto_17
    return v1

    #@18
    :cond_18
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@1b
    move-result v1

    #@1c
    goto :goto_17
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 2127
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@5
    move-result-object v0

    #@6
    .line 2128
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_19

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_19

    #@10
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@12
    if-gez v1, :cond_19

    #@14
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@17
    move-result v1

    #@18
    :goto_18
    return v1

    #@19
    :cond_19
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_18
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 2134
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@5
    move-result-object v0

    #@6
    .line 2135
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_19

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_19

    #@10
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@12
    if-gez v1, :cond_19

    #@14
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@17
    move-result v1

    #@18
    :goto_18
    return v1

    #@19
    :cond_19
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_18
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 2457
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    #@3
    .line 2459
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mMenuBackground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 2460
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mMenuBackground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@c
    .line 2462
    :cond_c
    return-void
.end method

.method public finishChanging()V
    .registers 2

    #@0
    .prologue
    .line 2579
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mChanging:Z

    #@3
    .line 2580
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->drawableChanged()V

    #@6
    .line 2581
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "insets"

    #@0
    .prologue
    .line 2617
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFrameOffsets:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5
    .line 2618
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getForeground()Landroid/graphics/drawable/Drawable;

    #@8
    move-result-object v0

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 2619
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->drawableChanged()V

    #@e
    .line 2621
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public isRingingAndHeadsetKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2072
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v0

    #@6
    const/16 v3, 0x4f

    #@8
    if-ne v0, v3, :cond_39

    #@a
    .line 2073
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@c
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1100(Lcom/android/internal/policy/impl/PhoneWindow;)Landroid/telephony/TelephonyManager;

    #@f
    move-result-object v0

    #@10
    if-nez v0, :cond_21

    #@12
    .line 2074
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@14
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@16
    const-string v4, "phone"

    #@18
    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@1e
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1102(Lcom/android/internal/policy/impl/PhoneWindow;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;

    #@21
    .line 2077
    :cond_21
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@23
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1100(Lcom/android/internal/policy/impl/PhoneWindow;)Landroid/telephony/TelephonyManager;

    #@26
    move-result-object v0

    #@27
    if-eqz v0, :cond_39

    #@29
    .line 2078
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2b
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1100(Lcom/android/internal/policy/impl/PhoneWindow;)Landroid/telephony/TelephonyManager;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@32
    move-result v0

    #@33
    if-ne v0, v1, :cond_37

    #@35
    move v0, v1

    #@36
    .line 2081
    :goto_36
    return v0

    #@37
    :cond_37
    move v0, v2

    #@38
    .line 2078
    goto :goto_36

    #@39
    :cond_39
    move v0, v2

    #@3a
    .line 2081
    goto :goto_36
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 2714
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    #@3
    .line 2716
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->updateWindowResizeState()V

    #@6
    .line 2718
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@b
    move-result-object v0

    #@c
    .line 2719
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1d

    #@e
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@10
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_1d

    #@16
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@18
    if-gez v1, :cond_1d

    #@1a
    .line 2720
    invoke-interface {v0}, Landroid/view/Window$Callback;->onAttachedToWindow()V

    #@1d
    .line 2723
    :cond_1d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@1f
    const/4 v2, -0x1

    #@20
    if-ne v1, v2, :cond_27

    #@22
    .line 2731
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@24
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$2100(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@27
    .line 2733
    :cond_27
    return-void
.end method

.method public onCloseSystemDialogs(Ljava/lang/String;)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 2764
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@2
    if-ltz v0, :cond_9

    #@4
    .line 2765
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PhoneWindow;->closeAllPanels()V

    #@9
    .line 2767
    :cond_9
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2737
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    #@4
    .line 2739
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@9
    move-result-object v0

    #@a
    .line 2740
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_13

    #@c
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@e
    if-gez v2, :cond_13

    #@10
    .line 2741
    invoke-interface {v0}, Landroid/view/Window$Callback;->onDetachedFromWindow()V

    #@13
    .line 2744
    :cond_13
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@15
    invoke-static {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;

    #@18
    move-result-object v2

    #@19
    if-eqz v2, :cond_24

    #@1b
    .line 2745
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1d
    invoke-static {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->dismissPopupMenus()V

    #@24
    .line 2748
    :cond_24
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@26
    if-eqz v2, :cond_3d

    #@28
    .line 2749
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mShowActionModePopup:Ljava/lang/Runnable;

    #@2a
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@2d
    .line 2750
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@2f
    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_3a

    #@35
    .line 2751
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@37
    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    #@3a
    .line 2753
    :cond_3a
    const/4 v2, 0x0

    #@3b
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@3d
    .line 2756
    :cond_3d
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@3f
    invoke-static {v2, v3, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1200(Lcom/android/internal/policy/impl/PhoneWindow;IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@42
    move-result-object v1

    #@43
    .line 2757
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v1, :cond_52

    #@45
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@47
    if-eqz v2, :cond_52

    #@49
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@4b
    if-gez v2, :cond_52

    #@4d
    .line 2758
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@4f
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->close()V

    #@52
    .line 2760
    :cond_52
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 2203
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    .line 2204
    .local v0, action:I
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@6
    if-ltz v3, :cond_23

    #@8
    .line 2205
    if-nez v0, :cond_23

    #@a
    .line 2206
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@d
    move-result v3

    #@e
    float-to-int v1, v3

    #@f
    .line 2207
    .local v1, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v3

    #@13
    float-to-int v2, v3

    #@14
    .line 2208
    .local v2, y:I
    invoke-direct {p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->isOutOfBounds(II)Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_23

    #@1a
    .line 2209
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1c
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@1e
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(I)V

    #@21
    .line 2210
    const/4 v3, 0x1

    #@22
    .line 2216
    .end local v1           #x:I
    .end local v2           #y:I
    :goto_22
    return v3

    #@23
    :cond_23
    const/4 v3, 0x0

    #@24
    goto :goto_22
.end method

.method protected onMeasure(II)V
    .registers 25
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 2355
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v20

    #@4
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v20

    #@8
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@b
    move-result-object v10

    #@c
    .line 2356
    .local v10, metrics:Landroid/util/DisplayMetrics;
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@e
    move/from16 v20, v0

    #@10
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@12
    move/from16 v21, v0

    #@14
    move/from16 v0, v20

    #@16
    move/from16 v1, v21

    #@18
    if-ge v0, v1, :cond_135

    #@1a
    const/4 v7, 0x1

    #@1b
    .line 2358
    .local v7, isPortrait:Z
    :goto_1b
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@1e
    move-result v18

    #@1f
    .line 2359
    .local v18, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@22
    move-result v5

    #@23
    .line 2361
    .local v5, heightMode:I
    const/4 v2, 0x0

    #@24
    .line 2362
    .local v2, fixedWidth:Z
    const/high16 v20, -0x8000

    #@26
    move/from16 v0, v18

    #@28
    move/from16 v1, v20

    #@2a
    if-ne v0, v1, :cond_6a

    #@2c
    .line 2363
    if-eqz v7, :cond_138

    #@2e
    move-object/from16 v0, p0

    #@30
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@32
    move-object/from16 v20, v0

    #@34
    move-object/from16 v0, v20

    #@36
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMinor:Landroid/util/TypedValue;

    #@38
    .line 2364
    .local v15, tvw:Landroid/util/TypedValue;
    :goto_38
    if-eqz v15, :cond_6a

    #@3a
    iget v0, v15, Landroid/util/TypedValue;->type:I

    #@3c
    move/from16 v20, v0

    #@3e
    if-eqz v20, :cond_6a

    #@40
    .line 2366
    iget v0, v15, Landroid/util/TypedValue;->type:I

    #@42
    move/from16 v20, v0

    #@44
    const/16 v21, 0x5

    #@46
    move/from16 v0, v20

    #@48
    move/from16 v1, v21

    #@4a
    if-ne v0, v1, :cond_144

    #@4c
    .line 2367
    invoke-virtual {v15, v10}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@4f
    move-result v20

    #@50
    move/from16 v0, v20

    #@52
    float-to-int v0, v0

    #@53
    move/from16 v16, v0

    #@55
    .line 2374
    .local v16, w:I
    :goto_55
    if-lez v16, :cond_6a

    #@57
    .line 2375
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@5a
    move-result v19

    #@5b
    .line 2376
    .local v19, widthSize:I
    move/from16 v0, v16

    #@5d
    move/from16 v1, v19

    #@5f
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@62
    move-result v20

    #@63
    const/high16 v21, 0x4000

    #@65
    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@68
    move-result p1

    #@69
    .line 2378
    const/4 v2, 0x1

    #@6a
    .line 2383
    .end local v15           #tvw:Landroid/util/TypedValue;
    .end local v16           #w:I
    .end local v19           #widthSize:I
    :cond_6a
    const/high16 v20, -0x8000

    #@6c
    move/from16 v0, v20

    #@6e
    if-ne v5, v0, :cond_a7

    #@70
    .line 2384
    if-eqz v7, :cond_175

    #@72
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@76
    move-object/from16 v20, v0

    #@78
    move-object/from16 v0, v20

    #@7a
    iget-object v14, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMajor:Landroid/util/TypedValue;

    #@7c
    .line 2385
    .local v14, tvh:Landroid/util/TypedValue;
    :goto_7c
    if-eqz v14, :cond_a7

    #@7e
    iget v0, v14, Landroid/util/TypedValue;->type:I

    #@80
    move/from16 v20, v0

    #@82
    if-eqz v20, :cond_a7

    #@84
    .line 2387
    iget v0, v14, Landroid/util/TypedValue;->type:I

    #@86
    move/from16 v20, v0

    #@88
    const/16 v21, 0x5

    #@8a
    move/from16 v0, v20

    #@8c
    move/from16 v1, v21

    #@8e
    if-ne v0, v1, :cond_181

    #@90
    .line 2388
    invoke-virtual {v14, v10}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@93
    move-result v20

    #@94
    move/from16 v0, v20

    #@96
    float-to-int v3, v0

    #@97
    .line 2395
    .local v3, h:I
    :goto_97
    if-lez v3, :cond_a7

    #@99
    .line 2396
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9c
    move-result v6

    #@9d
    .line 2397
    .local v6, heightSize:I
    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    #@a0
    move-result v20

    #@a1
    const/high16 v21, 0x4000

    #@a3
    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@a6
    move-result p2

    #@a7
    .line 2403
    .end local v3           #h:I
    .end local v6           #heightSize:I
    .end local v14           #tvh:Landroid/util/TypedValue;
    :cond_a7
    invoke-super/range {p0 .. p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@aa
    .line 2405
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getMeasuredWidth()I

    #@ad
    move-result v17

    #@ae
    .line 2406
    .local v17, width:I
    const/4 v9, 0x0

    #@af
    .line 2408
    .local v9, measure:Z
    const/high16 v20, 0x4000

    #@b1
    move/from16 v0, v17

    #@b3
    move/from16 v1, v20

    #@b5
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@b8
    move-result p1

    #@b9
    .line 2410
    if-nez v2, :cond_f5

    #@bb
    const/high16 v20, -0x8000

    #@bd
    move/from16 v0, v18

    #@bf
    move/from16 v1, v20

    #@c1
    if-ne v0, v1, :cond_f5

    #@c3
    .line 2411
    if-eqz v7, :cond_1af

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@c9
    move-object/from16 v20, v0

    #@cb
    move-object/from16 v0, v20

    #@cd
    iget-object v12, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMinor:Landroid/util/TypedValue;

    #@cf
    .line 2412
    .local v12, tv:Landroid/util/TypedValue;
    :goto_cf
    iget v0, v12, Landroid/util/TypedValue;->type:I

    #@d1
    move/from16 v20, v0

    #@d3
    if-eqz v20, :cond_f5

    #@d5
    .line 2414
    iget v0, v12, Landroid/util/TypedValue;->type:I

    #@d7
    move/from16 v20, v0

    #@d9
    const/16 v21, 0x5

    #@db
    move/from16 v0, v20

    #@dd
    move/from16 v1, v21

    #@df
    if-ne v0, v1, :cond_1bb

    #@e1
    .line 2415
    invoke-virtual {v12, v10}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@e4
    move-result v20

    #@e5
    move/from16 v0, v20

    #@e7
    float-to-int v11, v0

    #@e8
    .line 2422
    .local v11, min:I
    :goto_e8
    move/from16 v0, v17

    #@ea
    if-ge v0, v11, :cond_f5

    #@ec
    .line 2423
    const/high16 v20, 0x4000

    #@ee
    move/from16 v0, v20

    #@f0
    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@f3
    move-result p1

    #@f4
    .line 2424
    const/4 v9, 0x1

    #@f5
    .line 2432
    .end local v11           #min:I
    .end local v12           #tv:Landroid/util/TypedValue;
    :cond_f5
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getMeasuredHeight()I

    #@f8
    move-result v4

    #@f9
    .line 2433
    .local v4, height:I
    if-eqz v7, :cond_1e9

    #@fb
    move-object/from16 v0, p0

    #@fd
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@ff
    move-object/from16 v20, v0

    #@101
    move-object/from16 v0, v20

    #@103
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@105
    .line 2434
    .local v13, tv2:Landroid/util/TypedValue;
    :goto_105
    const/high16 v20, 0x4000

    #@107
    move/from16 v0, v20

    #@109
    if-eq v5, v0, :cond_12f

    #@10b
    iget v0, v13, Landroid/util/TypedValue;->type:I

    #@10d
    move/from16 v20, v0

    #@10f
    if-eqz v20, :cond_12f

    #@111
    .line 2436
    iget v0, v13, Landroid/util/TypedValue;->type:I

    #@113
    move/from16 v20, v0

    #@115
    const/16 v21, 0x5

    #@117
    move/from16 v0, v20

    #@119
    move/from16 v1, v21

    #@11b
    if-ne v0, v1, :cond_1f5

    #@11d
    .line 2437
    invoke-virtual {v13, v10}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@120
    move-result v20

    #@121
    move/from16 v0, v20

    #@123
    float-to-int v8, v0

    #@124
    .line 2443
    .local v8, max:I
    :goto_124
    if-le v4, v8, :cond_12f

    #@126
    .line 2444
    const/high16 v20, 0x4000

    #@128
    move/from16 v0, v20

    #@12a
    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@12d
    move-result p2

    #@12e
    .line 2445
    const/4 v9, 0x1

    #@12f
    .line 2450
    .end local v8           #max:I
    :cond_12f
    if-eqz v9, :cond_134

    #@131
    .line 2451
    invoke-super/range {p0 .. p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@134
    .line 2453
    :cond_134
    return-void

    #@135
    .line 2356
    .end local v2           #fixedWidth:Z
    .end local v4           #height:I
    .end local v5           #heightMode:I
    .end local v7           #isPortrait:Z
    .end local v9           #measure:Z
    .end local v13           #tv2:Landroid/util/TypedValue;
    .end local v17           #width:I
    .end local v18           #widthMode:I
    :cond_135
    const/4 v7, 0x0

    #@136
    goto/16 :goto_1b

    #@138
    .line 2363
    .restart local v2       #fixedWidth:Z
    .restart local v5       #heightMode:I
    .restart local v7       #isPortrait:Z
    .restart local v18       #widthMode:I
    :cond_138
    move-object/from16 v0, p0

    #@13a
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@13c
    move-object/from16 v20, v0

    #@13e
    move-object/from16 v0, v20

    #@140
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMajor:Landroid/util/TypedValue;

    #@142
    goto/16 :goto_38

    #@144
    .line 2368
    .restart local v15       #tvw:Landroid/util/TypedValue;
    :cond_144
    iget v0, v15, Landroid/util/TypedValue;->type:I

    #@146
    move/from16 v20, v0

    #@148
    const/16 v21, 0x6

    #@14a
    move/from16 v0, v20

    #@14c
    move/from16 v1, v21

    #@14e
    if-ne v0, v1, :cond_171

    #@150
    .line 2369
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@152
    move/from16 v20, v0

    #@154
    move/from16 v0, v20

    #@156
    int-to-float v0, v0

    #@157
    move/from16 v20, v0

    #@159
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@15b
    move/from16 v21, v0

    #@15d
    move/from16 v0, v21

    #@15f
    int-to-float v0, v0

    #@160
    move/from16 v21, v0

    #@162
    move/from16 v0, v20

    #@164
    move/from16 v1, v21

    #@166
    invoke-virtual {v15, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    #@169
    move-result v20

    #@16a
    move/from16 v0, v20

    #@16c
    float-to-int v0, v0

    #@16d
    move/from16 v16, v0

    #@16f
    .restart local v16       #w:I
    goto/16 :goto_55

    #@171
    .line 2371
    .end local v16           #w:I
    :cond_171
    const/16 v16, 0x0

    #@173
    .restart local v16       #w:I
    goto/16 :goto_55

    #@175
    .line 2384
    .end local v15           #tvw:Landroid/util/TypedValue;
    .end local v16           #w:I
    :cond_175
    move-object/from16 v0, p0

    #@177
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@179
    move-object/from16 v20, v0

    #@17b
    move-object/from16 v0, v20

    #@17d
    iget-object v14, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMinor:Landroid/util/TypedValue;

    #@17f
    goto/16 :goto_7c

    #@181
    .line 2389
    .restart local v14       #tvh:Landroid/util/TypedValue;
    :cond_181
    iget v0, v14, Landroid/util/TypedValue;->type:I

    #@183
    move/from16 v20, v0

    #@185
    const/16 v21, 0x6

    #@187
    move/from16 v0, v20

    #@189
    move/from16 v1, v21

    #@18b
    if-ne v0, v1, :cond_1ac

    #@18d
    .line 2390
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@18f
    move/from16 v20, v0

    #@191
    move/from16 v0, v20

    #@193
    int-to-float v0, v0

    #@194
    move/from16 v20, v0

    #@196
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@198
    move/from16 v21, v0

    #@19a
    move/from16 v0, v21

    #@19c
    int-to-float v0, v0

    #@19d
    move/from16 v21, v0

    #@19f
    move/from16 v0, v20

    #@1a1
    move/from16 v1, v21

    #@1a3
    invoke-virtual {v14, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    #@1a6
    move-result v20

    #@1a7
    move/from16 v0, v20

    #@1a9
    float-to-int v3, v0

    #@1aa
    .restart local v3       #h:I
    goto/16 :goto_97

    #@1ac
    .line 2392
    .end local v3           #h:I
    :cond_1ac
    const/4 v3, 0x0

    #@1ad
    .restart local v3       #h:I
    goto/16 :goto_97

    #@1af
    .line 2411
    .end local v3           #h:I
    .end local v14           #tvh:Landroid/util/TypedValue;
    .restart local v9       #measure:Z
    .restart local v17       #width:I
    :cond_1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1b3
    move-object/from16 v20, v0

    #@1b5
    move-object/from16 v0, v20

    #@1b7
    iget-object v12, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMajor:Landroid/util/TypedValue;

    #@1b9
    goto/16 :goto_cf

    #@1bb
    .line 2416
    .restart local v12       #tv:Landroid/util/TypedValue;
    :cond_1bb
    iget v0, v12, Landroid/util/TypedValue;->type:I

    #@1bd
    move/from16 v20, v0

    #@1bf
    const/16 v21, 0x6

    #@1c1
    move/from16 v0, v20

    #@1c3
    move/from16 v1, v21

    #@1c5
    if-ne v0, v1, :cond_1e6

    #@1c7
    .line 2417
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1c9
    move/from16 v20, v0

    #@1cb
    move/from16 v0, v20

    #@1cd
    int-to-float v0, v0

    #@1ce
    move/from16 v20, v0

    #@1d0
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1d2
    move/from16 v21, v0

    #@1d4
    move/from16 v0, v21

    #@1d6
    int-to-float v0, v0

    #@1d7
    move/from16 v21, v0

    #@1d9
    move/from16 v0, v20

    #@1db
    move/from16 v1, v21

    #@1dd
    invoke-virtual {v12, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    #@1e0
    move-result v20

    #@1e1
    move/from16 v0, v20

    #@1e3
    float-to-int v11, v0

    #@1e4
    .restart local v11       #min:I
    goto/16 :goto_e8

    #@1e6
    .line 2419
    .end local v11           #min:I
    :cond_1e6
    const/4 v11, 0x0

    #@1e7
    .restart local v11       #min:I
    goto/16 :goto_e8

    #@1e9
    .line 2433
    .end local v11           #min:I
    .end local v12           #tv:Landroid/util/TypedValue;
    .restart local v4       #height:I
    :cond_1e9
    move-object/from16 v0, p0

    #@1eb
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1ed
    move-object/from16 v20, v0

    #@1ef
    move-object/from16 v0, v20

    #@1f1
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@1f3
    goto/16 :goto_105

    #@1f5
    .line 2438
    .restart local v13       #tv2:Landroid/util/TypedValue;
    :cond_1f5
    iget v0, v13, Landroid/util/TypedValue;->type:I

    #@1f7
    move/from16 v20, v0

    #@1f9
    const/16 v21, 0x6

    #@1fb
    move/from16 v0, v20

    #@1fd
    move/from16 v1, v21

    #@1ff
    if-ne v0, v1, :cond_220

    #@201
    .line 2439
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@203
    move/from16 v20, v0

    #@205
    move/from16 v0, v20

    #@207
    int-to-float v0, v0

    #@208
    move/from16 v20, v0

    #@20a
    iget v0, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@20c
    move/from16 v21, v0

    #@20e
    move/from16 v0, v21

    #@210
    int-to-float v0, v0

    #@211
    move/from16 v21, v0

    #@213
    move/from16 v0, v20

    #@215
    move/from16 v1, v21

    #@217
    invoke-virtual {v13, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    #@21a
    move-result v20

    #@21b
    move/from16 v0, v20

    #@21d
    float-to-int v8, v0

    #@21e
    .restart local v8       #max:I
    goto/16 :goto_124

    #@220
    .line 2441
    .end local v8           #max:I
    :cond_220
    iget v8, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    #@222
    .restart local v8       #max:I
    goto/16 :goto_124
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2193
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasWindowFocus"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2687
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    #@4
    .line 2689
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1902(Lcom/android/internal/policy/impl/PhoneWindow;Z)Z

    #@9
    .line 2692
    if-nez p1, :cond_18

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@d
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$800(Lcom/android/internal/policy/impl/PhoneWindow;)I

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_18

    #@13
    .line 2693
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@15
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(I)V

    #@18
    .line 2696
    :cond_18
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@1d
    move-result-object v0

    #@1e
    .line 2697
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_2f

    #@20
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@22
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_2f

    #@28
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@2a
    if-gez v1, :cond_2f

    #@2c
    .line 2698
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->onWindowFocusChanged(Z)V

    #@2f
    .line 2700
    :cond_2f
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@31
    if-eqz v1, :cond_38

    #@33
    .line 2702
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@35
    invoke-static {v1, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$2002(Lcom/android/internal/policy/impl/PhoneWindow;Z)Z

    #@38
    .line 2704
    :cond_38
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .registers 4
    .parameter "eventType"

    #@0
    .prologue
    .line 2280
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 2296
    :goto_c
    return-void

    #@d
    .line 2287
    :cond_d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@f
    if-eqz v0, :cond_20

    #@11
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@13
    const/4 v1, 0x6

    #@14
    if-eq v0, v1, :cond_20

    #@16
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@18
    const/4 v1, 0x2

    #@19
    if-eq v0, v1, :cond_20

    #@1b
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@1d
    const/4 v1, 0x5

    #@1e
    if-ne v0, v1, :cond_30

    #@20
    :cond_20
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getChildCount()I

    #@23
    move-result v0

    #@24
    const/4 v1, 0x1

    #@25
    if-ne v0, v1, :cond_30

    #@27
    .line 2292
    const/4 v0, 0x0

    #@28
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getChildAt(I)Landroid/view/View;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {v0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@2f
    goto :goto_c

    #@30
    .line 2294
    :cond_30
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    #@33
    goto :goto_c
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 2597
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3
    .line 2598
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getWindowToken()Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 2599
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->updateWindowResizeState()V

    #@c
    .line 2601
    :cond_c
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 14
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 2311
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->setFrame(IIII)Z

    #@3
    move-result v1

    #@4
    .line 2312
    .local v1, changed:Z
    if-eqz v1, :cond_65

    #@6
    .line 2313
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mDrawingBounds:Landroid/graphics/Rect;

    #@8
    .line 2314
    .local v2, drawingBounds:Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getDrawingRect(Landroid/graphics/Rect;)V

    #@b
    .line 2316
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getForeground()Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v3

    #@f
    .line 2317
    .local v3, fg:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_5c

    #@11
    .line 2318
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFrameOffsets:Landroid/graphics/Rect;

    #@13
    .line 2319
    .local v4, frameOffsets:Landroid/graphics/Rect;
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@15
    iget v7, v4, Landroid/graphics/Rect;->left:I

    #@17
    add-int/2addr v6, v7

    #@18
    iput v6, v2, Landroid/graphics/Rect;->left:I

    #@1a
    .line 2320
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@1c
    iget v7, v4, Landroid/graphics/Rect;->top:I

    #@1e
    add-int/2addr v6, v7

    #@1f
    iput v6, v2, Landroid/graphics/Rect;->top:I

    #@21
    .line 2321
    iget v6, v2, Landroid/graphics/Rect;->right:I

    #@23
    iget v7, v4, Landroid/graphics/Rect;->right:I

    #@25
    sub-int/2addr v6, v7

    #@26
    iput v6, v2, Landroid/graphics/Rect;->right:I

    #@28
    .line 2322
    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    #@2a
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    #@2c
    sub-int/2addr v6, v7

    #@2d
    iput v6, v2, Landroid/graphics/Rect;->bottom:I

    #@2f
    .line 2323
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@32
    .line 2324
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@34
    .line 2325
    .local v5, framePadding:Landroid/graphics/Rect;
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@36
    iget v7, v5, Landroid/graphics/Rect;->left:I

    #@38
    iget v8, v4, Landroid/graphics/Rect;->left:I

    #@3a
    sub-int/2addr v7, v8

    #@3b
    add-int/2addr v6, v7

    #@3c
    iput v6, v2, Landroid/graphics/Rect;->left:I

    #@3e
    .line 2326
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@40
    iget v7, v5, Landroid/graphics/Rect;->top:I

    #@42
    iget v8, v4, Landroid/graphics/Rect;->top:I

    #@44
    sub-int/2addr v7, v8

    #@45
    add-int/2addr v6, v7

    #@46
    iput v6, v2, Landroid/graphics/Rect;->top:I

    #@48
    .line 2327
    iget v6, v2, Landroid/graphics/Rect;->right:I

    #@4a
    iget v7, v5, Landroid/graphics/Rect;->right:I

    #@4c
    iget v8, v4, Landroid/graphics/Rect;->right:I

    #@4e
    sub-int/2addr v7, v8

    #@4f
    sub-int/2addr v6, v7

    #@50
    iput v6, v2, Landroid/graphics/Rect;->right:I

    #@52
    .line 2328
    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    #@54
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    #@56
    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    #@58
    sub-int/2addr v7, v8

    #@59
    sub-int/2addr v6, v7

    #@5a
    iput v6, v2, Landroid/graphics/Rect;->bottom:I

    #@5c
    .line 2331
    .end local v4           #frameOffsets:Landroid/graphics/Rect;
    .end local v5           #framePadding:Landroid/graphics/Rect;
    :cond_5c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@5f
    move-result-object v0

    #@60
    .line 2332
    .local v0, bg:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_65

    #@62
    .line 2333
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@65
    .line 2350
    .end local v0           #bg:Landroid/graphics/drawable/Drawable;
    .end local v2           #drawingBounds:Landroid/graphics/Rect;
    .end local v3           #fg:Landroid/graphics/drawable/Drawable;
    :cond_65
    return v1
.end method

.method public setSurfaceFormat(I)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 2782
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->setFormat(I)V

    #@5
    .line 2783
    return-void
.end method

.method public setSurfaceKeepScreenOn(Z)V
    .registers 4
    .parameter "keepOn"

    #@0
    .prologue
    const/16 v1, 0x80

    #@2
    .line 2786
    if-eqz p1, :cond_a

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->addFlags(I)V

    #@9
    .line 2788
    :goto_9
    return-void

    #@a
    .line 2787
    :cond_a
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->clearFlags(I)V

    #@f
    goto :goto_9
.end method

.method public setSurfaceType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 2778
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->setType(I)V

    #@5
    .line 2779
    return-void
.end method

.method public setWindowBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    #@0
    .prologue
    .line 2584
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    if-eq v0, p1, :cond_13

    #@6
    .line 2585
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 2586
    if-eqz p1, :cond_14

    #@b
    .line 2587
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@d
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@10
    .line 2591
    :goto_10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->drawableChanged()V

    #@13
    .line 2593
    :cond_13
    return-void

    #@14
    .line 2589
    :cond_14
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mBackgroundPadding:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@19
    goto :goto_10
.end method

.method public setWindowFrame(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    #@0
    .prologue
    .line 2604
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getForeground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    if-eq v0, p1, :cond_13

    #@6
    .line 2605
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 2606
    if-eqz p1, :cond_14

    #@b
    .line 2607
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@d
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@10
    .line 2611
    :goto_10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->drawableChanged()V

    #@13
    .line 2613
    :cond_13
    return-void

    #@14
    .line 2609
    :cond_14
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFramePadding:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@19
    goto :goto_10
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 6
    .parameter "originalView"

    #@0
    .prologue
    .line 2468
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@2
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1400(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@5
    move-result-object v1

    #@6
    if-nez v1, :cond_43

    #@8
    .line 2469
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@a
    new-instance v2, Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getContext()Landroid/content/Context;

    #@f
    move-result-object v3

    #@10
    invoke-direct {v2, v3}, Lcom/android/internal/view/menu/ContextMenuBuilder;-><init>(Landroid/content/Context;)V

    #@13
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1402(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/view/menu/ContextMenuBuilder;)Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@16
    .line 2470
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@18
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1400(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@1b
    move-result-object v1

    #@1c
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1e
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuCallback:Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;

    #@20
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/ContextMenuBuilder;->setCallback(Lcom/android/internal/view/menu/MenuBuilder$Callback;)V

    #@23
    .line 2475
    :goto_23
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@25
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1400(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, p1, v2}, Lcom/android/internal/view/menu/ContextMenuBuilder;->show(Landroid/view/View;Landroid/os/IBinder;)Lcom/android/internal/view/menu/MenuDialogHelper;

    #@30
    move-result-object v0

    #@31
    .line 2477
    .local v0, helper:Lcom/android/internal/view/menu/MenuDialogHelper;
    if-eqz v0, :cond_3a

    #@33
    .line 2478
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@35
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuCallback:Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;

    #@37
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuDialogHelper;->setPresenterCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@3a
    .line 2480
    :cond_3a
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@3c
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1502(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/view/menu/MenuDialogHelper;)Lcom/android/internal/view/menu/MenuDialogHelper;

    #@3f
    .line 2481
    if-eqz v0, :cond_4d

    #@41
    const/4 v1, 0x1

    #@42
    :goto_42
    return v1

    #@43
    .line 2472
    .end local v0           #helper:Lcom/android/internal/view/menu/MenuDialogHelper;
    :cond_43
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@45
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1400(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Lcom/android/internal/view/menu/ContextMenuBuilder;->clearAll()V

    #@4c
    goto :goto_23

    #@4d
    .line 2481
    .restart local v0       #helper:Lcom/android/internal/view/menu/MenuDialogHelper;
    :cond_4d
    const/4 v1, 0x0

    #@4e
    goto :goto_42
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 13
    .parameter "callback"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 2494
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@5
    if-eqz v5, :cond_c

    #@7
    .line 2495
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@9
    invoke-virtual {v5}, Landroid/view/ActionMode;->finish()V

    #@c
    .line 2498
    :cond_c
    new-instance v4, Lcom/android/internal/policy/impl/PhoneWindow$DecorView$ActionModeCallbackWrapper;

    #@e
    invoke-direct {v4, p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView$ActionModeCallbackWrapper;-><init>(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;Landroid/view/ActionMode$Callback;)V

    #@11
    .line 2499
    .local v4, wrappedCallback:Landroid/view/ActionMode$Callback;
    const/4 v2, 0x0

    #@12
    .line 2500
    .local v2, mode:Landroid/view/ActionMode;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@14
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@17
    move-result-object v5

    #@18
    if-eqz v5, :cond_2c

    #@1a
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@1c
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_2c

    #@22
    .line 2502
    :try_start_22
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@24
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@27
    move-result-object v5

    #@28
    invoke-interface {v5, v4}, Landroid/view/Window$Callback;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    :try_end_2b
    .catch Ljava/lang/AbstractMethodError; {:try_start_22 .. :try_end_2b} :catch_124

    #@2b
    move-result-object v2

    #@2c
    .line 2507
    :cond_2c
    :goto_2c
    if-eqz v2, :cond_52

    #@2e
    .line 2508
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@30
    .line 2564
    :cond_30
    :goto_30
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@32
    if-eqz v5, :cond_4f

    #@34
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@36
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@39
    move-result-object v5

    #@3a
    if-eqz v5, :cond_4f

    #@3c
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@3e
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@41
    move-result v5

    #@42
    if-nez v5, :cond_4f

    #@44
    .line 2566
    :try_start_44
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@46
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@49
    move-result-object v5

    #@4a
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@4c
    invoke-interface {v5, v6}, Landroid/view/Window$Callback;->onActionModeStarted(Landroid/view/ActionMode;)V
    :try_end_4f
    .catch Ljava/lang/AbstractMethodError; {:try_start_44 .. :try_end_4f} :catch_121

    #@4f
    .line 2571
    :cond_4f
    :goto_4f
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@51
    return-object v5

    #@52
    .line 2510
    :cond_52
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@54
    if-nez v5, :cond_c3

    #@56
    .line 2511
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@58
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindow;->isFloating()Z

    #@5b
    move-result v5

    #@5c
    if-eqz v5, :cond_107

    #@5e
    .line 2512
    new-instance v5, Lcom/android/internal/widget/ActionBarContextView;

    #@60
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@62
    invoke-direct {v5, v8}, Lcom/android/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    #@65
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@67
    .line 2513
    new-instance v5, Landroid/widget/PopupWindow;

    #@69
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@6b
    const v9, 0x10103e4

    #@6e
    invoke-direct {v5, v8, v10, v9}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@71
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@73
    .line 2515
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@75
    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setLayoutInScreenEnabled(Z)V

    #@78
    .line 2516
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@7a
    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setLayoutInsetDecor(Z)V

    #@7d
    .line 2517
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@7f
    const/4 v8, 0x2

    #@80
    invoke-virtual {v5, v8}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@83
    .line 2519
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@85
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@87
    invoke-virtual {v5, v8}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@8a
    .line 2520
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@8c
    const/4 v8, -0x1

    #@8d
    invoke-virtual {v5, v8}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@90
    .line 2522
    new-instance v1, Landroid/util/TypedValue;

    #@92
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@95
    .line 2523
    .local v1, heightValue:Landroid/util/TypedValue;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@97
    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@9a
    move-result-object v5

    #@9b
    const v8, 0x10102eb

    #@9e
    invoke-virtual {v5, v8, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@a1
    .line 2525
    iget v5, v1, Landroid/util/TypedValue;->data:I

    #@a3
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mContext:Landroid/content/Context;

    #@a5
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a8
    move-result-object v8

    #@a9
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@ac
    move-result-object v8

    #@ad
    invoke-static {v5, v8}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@b0
    move-result v0

    #@b1
    .line 2527
    .local v0, height:I
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@b3
    invoke-virtual {v5, v0}, Lcom/android/internal/widget/ActionBarContextView;->setContentHeight(I)V

    #@b6
    .line 2528
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@b8
    const/4 v8, -0x2

    #@b9
    invoke-virtual {v5, v8}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@bc
    .line 2529
    new-instance v5, Lcom/android/internal/policy/impl/PhoneWindow$DecorView$1;

    #@be
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)V

    #@c1
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mShowActionModePopup:Ljava/lang/Runnable;

    #@c3
    .line 2545
    .end local v0           #height:I
    .end local v1           #heightValue:Landroid/util/TypedValue;
    :cond_c3
    :goto_c3
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@c5
    if-eqz v5, :cond_30

    #@c7
    .line 2546
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@c9
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarContextView;->killMode()V

    #@cc
    .line 2547
    new-instance v2, Lcom/android/internal/view/StandaloneActionMode;

    #@ce
    .end local v2           #mode:Landroid/view/ActionMode;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getContext()Landroid/content/Context;

    #@d1
    move-result-object v8

    #@d2
    iget-object v9, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@d4
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@d6
    if-nez v5, :cond_11b

    #@d8
    move v5, v6

    #@d9
    :goto_d9
    invoke-direct {v2, v8, v9, v4, v5}, Lcom/android/internal/view/StandaloneActionMode;-><init>(Landroid/content/Context;Lcom/android/internal/widget/ActionBarContextView;Landroid/view/ActionMode$Callback;Z)V

    #@dc
    .line 2549
    .restart local v2       #mode:Landroid/view/ActionMode;
    invoke-virtual {v2}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    #@df
    move-result-object v5

    #@e0
    invoke-interface {p1, v2, v5}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    #@e3
    move-result v5

    #@e4
    if-eqz v5, :cond_11d

    #@e6
    .line 2550
    invoke-virtual {v2}, Landroid/view/ActionMode;->invalidate()V

    #@e9
    .line 2551
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@eb
    invoke-virtual {v5, v2}, Lcom/android/internal/widget/ActionBarContextView;->initForMode(Landroid/view/ActionMode;)V

    #@ee
    .line 2552
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@f0
    invoke-virtual {v5, v7}, Lcom/android/internal/widget/ActionBarContextView;->setVisibility(I)V

    #@f3
    .line 2553
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@f5
    .line 2554
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModePopup:Landroid/widget/PopupWindow;

    #@f7
    if-eqz v5, :cond_fe

    #@f9
    .line 2555
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mShowActionModePopup:Ljava/lang/Runnable;

    #@fb
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->post(Ljava/lang/Runnable;)Z

    #@fe
    .line 2557
    :cond_fe
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@100
    const/16 v6, 0x20

    #@102
    invoke-virtual {v5, v6}, Lcom/android/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    #@105
    goto/16 :goto_30

    #@107
    .line 2537
    :cond_107
    const v5, 0x1020367

    #@10a
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->findViewById(I)Landroid/view/View;

    #@10d
    move-result-object v3

    #@10e
    check-cast v3, Landroid/view/ViewStub;

    #@110
    .line 2539
    .local v3, stub:Landroid/view/ViewStub;
    if-eqz v3, :cond_c3

    #@112
    .line 2540
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    #@115
    move-result-object v5

    #@116
    check-cast v5, Lcom/android/internal/widget/ActionBarContextView;

    #@118
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionModeView:Lcom/android/internal/widget/ActionBarContextView;

    #@11a
    goto :goto_c3

    #@11b
    .end local v2           #mode:Landroid/view/ActionMode;
    .end local v3           #stub:Landroid/view/ViewStub;
    :cond_11b
    move v5, v7

    #@11c
    .line 2547
    goto :goto_d9

    #@11d
    .line 2560
    .restart local v2       #mode:Landroid/view/ActionMode;
    :cond_11d
    iput-object v10, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@11f
    goto/16 :goto_30

    #@121
    .line 2567
    :catch_121
    move-exception v5

    #@122
    goto/16 :goto_4f

    #@124
    .line 2503
    :catch_124
    move-exception v5

    #@125
    goto/16 :goto_2c
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4
    .parameter "originalView"
    .parameter "callback"

    #@0
    .prologue
    .line 2489
    invoke-virtual {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public startChanging()V
    .registers 2

    #@0
    .prologue
    .line 2575
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mChanging:Z

    #@3
    .line 2576
    return-void
.end method

.method public superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2188
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public superDispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2147
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_8

    #@7
    .line 2172
    :cond_7
    :goto_7
    return v1

    #@8
    .line 2153
    :cond_8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@b
    move-result v2

    #@c
    const/4 v3, 0x4

    #@d
    if-ne v2, v3, :cond_3f

    #@f
    .line 2154
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@12
    move-result v0

    #@13
    .line 2156
    .local v0, action:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@15
    if-eqz v2, :cond_1f

    #@17
    .line 2157
    if-ne v0, v1, :cond_7

    #@19
    .line 2158
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mActionMode:Landroid/view/ActionMode;

    #@1b
    invoke-virtual {v2}, Landroid/view/ActionMode;->finish()V

    #@1e
    goto :goto_7

    #@1f
    .line 2164
    :cond_1f
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@21
    invoke-static {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;

    #@24
    move-result-object v2

    #@25
    if-eqz v2, :cond_3f

    #@27
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@29
    invoke-static {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->hasExpandedActionView()Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_3f

    #@33
    .line 2165
    if-ne v0, v1, :cond_7

    #@35
    .line 2166
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@37
    invoke-static {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Lcom/android/internal/widget/ActionBarView;->collapseActionView()V

    #@3e
    goto :goto_7

    #@3f
    .line 2172
    .end local v0           #action:I
    :cond_3f
    const/4 v1, 0x0

    #@40
    goto :goto_7
.end method

.method public superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2176
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public superDispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2180
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2184
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method updateWindowResizeState()V
    .registers 4

    #@0
    .prologue
    .line 2707
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    .line 2708
    .local v0, bg:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_d

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@9
    move-result v1

    #@a
    const/4 v2, -0x1

    #@b
    if-eq v1, v2, :cond_12

    #@d
    :cond_d
    const/4 v1, 0x1

    #@e
    :goto_e
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->hackTurnOffWindowResizeAnim(Z)V

    #@11
    .line 2710
    return-void

    #@12
    .line 2708
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public willYouTakeTheInputQueue()Landroid/view/InputQueue$Callback;
    .registers 2

    #@0
    .prologue
    .line 2774
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@2
    if-gez v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTakeInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public willYouTakeTheSurface()Landroid/view/SurfaceHolder$Callback2;
    .registers 2

    #@0
    .prologue
    .line 2770
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mFeatureId:I

    #@2
    if-gez v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->this$0:Lcom/android/internal/policy/impl/PhoneWindow;

    #@6
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTakeSurfaceCallback:Landroid/view/SurfaceHolder$Callback2;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method
