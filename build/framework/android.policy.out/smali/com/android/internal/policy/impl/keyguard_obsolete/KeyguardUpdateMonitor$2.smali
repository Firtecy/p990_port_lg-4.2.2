.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;
.super Landroid/content/BroadcastReceiver;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 141
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 144
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 147
    .local v0, action:Ljava/lang/String;
    const-string v7, "android.intent.action.TIME_TICK"

    #@8
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v7

    #@c
    if-nez v7, :cond_1e

    #@e
    const-string v7, "android.intent.action.TIME_SET"

    #@10
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v7

    #@14
    if-nez v7, :cond_1e

    #@16
    const-string v7, "android.intent.action.TIMEZONE_CHANGED"

    #@18
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v7

    #@1c
    if-eqz v7, :cond_34

    #@1e
    .line 150
    :cond_1e
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@20
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@23
    move-result-object v7

    #@24
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@26
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@29
    move-result-object v8

    #@2a
    const/16 v9, 0x12d

    #@2c
    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@33
    .line 186
    :cond_33
    :goto_33
    return-void

    #@34
    .line 151
    :cond_34
    const-string v7, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@36
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v7

    #@3a
    if-eqz v7, :cond_68

    #@3c
    .line 152
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@3e
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@40
    invoke-static {v8, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@43
    move-result-object v8

    #@44
    invoke-static {v7, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@47
    .line 153
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@49
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@4b
    invoke-static {v8, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v7, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$802(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@52
    .line 154
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@54
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@57
    move-result-object v7

    #@58
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@5a
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@5d
    move-result-object v8

    #@5e
    const/16 v9, 0x12f

    #@60
    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@67
    goto :goto_33

    #@68
    .line 155
    :cond_68
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    #@6a
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v7

    #@6e
    if-eqz v7, :cond_a3

    #@70
    .line 156
    const-string v7, "status"

    #@72
    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@75
    move-result v6

    #@76
    .line 157
    .local v6, status:I
    const-string v7, "plugged"

    #@78
    invoke-virtual {p2, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@7b
    move-result v4

    #@7c
    .line 158
    .local v4, plugged:I
    const-string v7, "level"

    #@7e
    invoke-virtual {p2, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@81
    move-result v2

    #@82
    .line 159
    .local v2, level:I
    const-string v7, "health"

    #@84
    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@87
    move-result v1

    #@88
    .line 160
    .local v1, health:I
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@8a
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@8d
    move-result-object v7

    #@8e
    const/16 v8, 0x12e

    #@90
    new-instance v9, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@92
    invoke-direct {v9, v6, v2, v4, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;-><init>(IIII)V

    #@95
    invoke-virtual {v7, v8, v9}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@98
    move-result-object v3

    #@99
    .line 162
    .local v3, msg:Landroid/os/Message;
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@9b
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@9e
    move-result-object v7

    #@9f
    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@a2
    goto :goto_33

    #@a3
    .line 163
    .end local v1           #health:I
    .end local v2           #level:I
    .end local v3           #msg:Landroid/os/Message;
    .end local v4           #plugged:I
    .end local v6           #status:I
    :cond_a3
    const-string v7, "android.intent.action.SIM_STATE_CHANGED"

    #@a5
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v7

    #@a9
    if-eqz v7, :cond_c6

    #@ab
    .line 168
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@ad
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@b0
    move-result-object v7

    #@b1
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@b3
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@b6
    move-result-object v8

    #@b7
    const/16 v9, 0x130

    #@b9
    invoke-static {p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;->fromIntent(Landroid/content/Intent;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;

    #@bc
    move-result-object v10

    #@bd
    invoke-virtual {v8, v9, v10}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c0
    move-result-object v8

    #@c1
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@c4
    goto/16 :goto_33

    #@c6
    .line 170
    :cond_c6
    const-string v7, "android.media.RINGER_MODE_CHANGED"

    #@c8
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cb
    move-result v7

    #@cc
    if-eqz v7, :cond_ec

    #@ce
    .line 171
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@d0
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@d3
    move-result-object v7

    #@d4
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@d6
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@d9
    move-result-object v8

    #@da
    const/16 v9, 0x131

    #@dc
    const-string v10, "android.media.EXTRA_RINGER_MODE"

    #@de
    const/4 v11, -0x1

    #@df
    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@e2
    move-result v10

    #@e3
    invoke-virtual {v8, v9, v10, v12}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@ea
    goto/16 :goto_33

    #@ec
    .line 173
    :cond_ec
    const-string v7, "android.intent.action.PHONE_STATE"

    #@ee
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f1
    move-result v7

    #@f2
    if-eqz v7, :cond_111

    #@f4
    .line 174
    const-string v7, "state"

    #@f6
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@f9
    move-result-object v5

    #@fa
    .line 175
    .local v5, state:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@fc
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@ff
    move-result-object v7

    #@100
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@102
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@105
    move-result-object v8

    #@106
    const/16 v9, 0x132

    #@108
    invoke-virtual {v8, v9, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10b
    move-result-object v8

    #@10c
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10f
    goto/16 :goto_33

    #@111
    .line 176
    .end local v5           #state:Ljava/lang/String;
    :cond_111
    const-string v7, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    #@113
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v7

    #@117
    if-eqz v7, :cond_130

    #@119
    .line 178
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@11b
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@11e
    move-result-object v7

    #@11f
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@121
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@124
    move-result-object v8

    #@125
    const/16 v9, 0x135

    #@127
    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@12a
    move-result-object v8

    #@12b
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@12e
    goto/16 :goto_33

    #@130
    .line 179
    :cond_130
    const-string v7, "android.intent.action.USER_SWITCHED"

    #@132
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@135
    move-result v7

    #@136
    if-eqz v7, :cond_155

    #@138
    .line 180
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@13a
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@13d
    move-result-object v7

    #@13e
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@140
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@143
    move-result-object v8

    #@144
    const/16 v9, 0x136

    #@146
    const-string v10, "android.intent.extra.user_handle"

    #@148
    invoke-virtual {p2, v10, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14b
    move-result v10

    #@14c
    invoke-virtual {v8, v9, v10, v12}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@14f
    move-result-object v8

    #@150
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@153
    goto/16 :goto_33

    #@155
    .line 182
    :cond_155
    const-string v7, "android.intent.action.USER_REMOVED"

    #@157
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v7

    #@15b
    if-eqz v7, :cond_33

    #@15d
    .line 183
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@15f
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@162
    move-result-object v7

    #@163
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@165
    invoke-static {v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@168
    move-result-object v8

    #@169
    const/16 v9, 0x137

    #@16b
    const-string v10, "android.intent.extra.user_handle"

    #@16d
    invoke-virtual {p2, v10, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@170
    move-result v10

    #@171
    invoke-virtual {v8, v9, v10, v12}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@174
    move-result-object v8

    #@175
    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@178
    goto/16 :goto_33
.end method
