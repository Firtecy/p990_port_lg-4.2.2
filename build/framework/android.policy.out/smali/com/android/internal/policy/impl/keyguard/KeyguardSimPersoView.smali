.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;
.source "KeyguardSimPersoView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;
    }
.end annotation


# static fields
.field private static KT_UsimPerso_retry_count:I = 0x0

.field protected static final LOG_TAG:Ljava/lang/String; = "KeyguardSimPersoView"

.field private static countrycode:Ljava/lang/String;

.field static maxDigits:I

.field private static operator:Ljava/lang/String;

.field public static service_provider:Ljava/lang/String;


# instance fields
.field private mEmergencyText:Landroid/widget/TextView;

.field protected mHeaderText:Landroid/widget/TextView;

.field protected mHeaderTextString:Ljava/lang/String;

.field mImm:Landroid/view/inputmethod/InputMethodManager;

.field public mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

.field protected mPinEntry:Landroid/widget/TextView;

.field protected mPopupMsg:Ljava/lang/String;

.field protected mRetryText:Landroid/widget/TextView;

.field protected mRetryTextString:Ljava/lang/String;

.field private mServiceState:Landroid/telephony/ServiceState;

.field protected mStrTitleWrongPin:Ljava/lang/String;

.field private mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

.field protected volatile mUsimPersoUnlockProgress:Z

.field public mdialog:Landroid/app/AlertDialog;

.field minDigits:I

.field private service_status:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 77
    const/4 v0, 0x5

    #@2
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@4
    .line 85
    const/4 v0, 0x0

    #@5
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->maxDigits:I

    #@7
    .line 95
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_provider:Ljava/lang/String;

    #@9
    .line 101
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->countrycode:Ljava/lang/String;

    #@b
    .line 102
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->operator:Ljava/lang/String;

    #@d
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 111
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 71
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@6
    .line 84
    const/4 v0, 0x0

    #@7
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->minDigits:I

    #@9
    .line 94
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_NONE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@d
    .line 100
    const/4 v0, 0x2

    #@e
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@10
    .line 116
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->showUsimPersoDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500()I
    .registers 1

    #@0
    .prologue
    .line 66
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@2
    return v0
.end method

.method static synthetic access$510()I
    .registers 2

    #@0
    .prologue
    .line 66
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    sput v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@6
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private getUSimPersoProgressDialog()Landroid/app/Dialog;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 444
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@3
    if-nez v0, :cond_4c

    #@5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@7
    if-eqz v0, :cond_4c

    #@9
    .line 445
    new-instance v0, Landroid/app/ProgressDialog;

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@d
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@12
    .line 446
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    const-string v1, "KR"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_4f

    #@1e
    .line 447
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@20
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@22
    const v2, 0x20902b7

    #@25
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@2c
    .line 453
    :goto_2c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@2e
    const/4 v1, 0x1

    #@2f
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@32
    .line 454
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@34
    const/4 v1, 0x0

    #@35
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@38
    .line 455
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@3a
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@3d
    move-result-object v0

    #@3e
    const/16 v1, 0x7d9

    #@40
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@43
    .line 457
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@45
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    #@4c
    .line 461
    :cond_4c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@4e
    return-object v0

    #@4f
    .line 450
    :cond_4f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@51
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@53
    const v2, 0x104032d

    #@56
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@5d
    goto :goto_2c
.end method

.method private showUsimPersoDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 464
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@e
    move-result-object v1

    #@f
    const v2, 0x104000a

    #@12
    const/4 v3, 0x0

    #@13
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v1

    #@17
    const v2, 0x202026e

    #@1a
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@21
    move-result-object v0

    #@22
    .line 470
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@25
    move-result-object v1

    #@26
    const/16 v2, 0x7d9

    #@28
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@2b
    .line 471
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2e
    .line 472
    return-void
.end method


# virtual methods
.method protected getPasswordTextViewId()I
    .registers 2

    #@0
    .prologue
    .line 125
    const v0, 0x10202d3

    #@3
    return v0
.end method

.method protected onFinishInflate()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 130
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onFinishInflate()V

    #@4
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v5

    #@8
    const-string v6, "input_method"

    #@a
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v5

    #@e
    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    #@10
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@12
    .line 135
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@14
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->getWindowToken()Landroid/os/IBinder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v5, v6, v8}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1b
    .line 141
    const v5, 0x10202fb

    #@1e
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Landroid/widget/TextView;

    #@24
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderText:Landroid/widget/TextView;

    #@26
    .line 142
    const v5, 0x20d0054

    #@29
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@2c
    move-result-object v5

    #@2d
    check-cast v5, Landroid/widget/TextView;

    #@2f
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryText:Landroid/widget/TextView;

    #@31
    .line 144
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->readStringFromResource()V

    #@34
    .line 146
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderText:Landroid/widget/TextView;

    #@36
    if-eqz v5, :cond_3f

    #@38
    .line 147
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderText:Landroid/widget/TextView;

    #@3a
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderTextString:Ljava/lang/String;

    #@3c
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@3f
    .line 148
    :cond_3f
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@41
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@43
    if-ne v5, v6, :cond_db

    #@45
    .line 149
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryText:Landroid/widget/TextView;

    #@47
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryTextString:Ljava/lang/String;

    #@49
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4c
    .line 154
    :goto_4c
    const v5, 0x10202df

    #@4f
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@52
    move-result-object v2

    #@53
    check-cast v2, Landroid/widget/TextView;

    #@55
    .line 155
    .local v2, ok:Landroid/widget/TextView;
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    #@58
    .line 156
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$1;

    #@5a
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V

    #@5d
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@60
    .line 164
    const v5, 0x1020283

    #@63
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Landroid/widget/TextView;

    #@69
    .line 165
    .local v0, cancel:Landroid/widget/TextView;
    if-eqz v0, :cond_76

    #@6b
    .line 166
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    #@6e
    .line 167
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$2;

    #@70
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V

    #@73
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@76
    .line 179
    :cond_76
    const v5, 0x10202d4

    #@79
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@7c
    move-result-object v3

    #@7d
    .line 180
    .local v3, pinDelete:Landroid/view/View;
    if-eqz v3, :cond_92

    #@7f
    .line 181
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    #@82
    .line 182
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$3;

    #@84
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V

    #@87
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@8a
    .line 191
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$4;

    #@8c
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V

    #@8f
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@92
    .line 201
    :cond_92
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@94
    const-string v6, "phone"

    #@96
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@99
    move-result-object v4

    #@9a
    check-cast v4, Landroid/telephony/TelephonyManager;

    #@9c
    .line 202
    .local v4, telephonyManager:Landroid/telephony/TelephonyManager;
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;

    #@9e
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)V

    #@a1
    .line 215
    .local v1, listener:Landroid/telephony/PhoneStateListener;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@a4
    move-result-object v5

    #@a5
    const-string v6, "TEL"

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v5

    #@ab
    if-eqz v5, :cond_c5

    #@ad
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@b0
    move-result-object v5

    #@b1
    const-string v6, "AU"

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v5

    #@b7
    if-eqz v5, :cond_c5

    #@b9
    .line 216
    const/4 v5, 0x1

    #@ba
    invoke-virtual {v4, v1, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@bd
    .line 217
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mEmergencyText:Landroid/widget/TextView;

    #@bf
    const v6, 0x1040325

    #@c2
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    #@c5
    .line 220
    :cond_c5
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@c7
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    #@ca
    move-result-object v6

    #@cb
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@ce
    .line 221
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@d0
    const/16 v6, 0x12

    #@d2
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setInputType(I)V

    #@d5
    .line 224
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@d7
    invoke-virtual {v5}, Landroid/widget/TextView;->requestFocus()Z

    #@da
    .line 225
    return-void

    #@db
    .line 151
    .end local v0           #cancel:Landroid/widget/TextView;
    .end local v1           #listener:Landroid/telephony/PhoneStateListener;
    .end local v2           #ok:Landroid/widget/TextView;
    .end local v3           #pinDelete:Landroid/view/View;
    .end local v4           #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_db
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryText:Landroid/widget/TextView;

    #@dd
    new-instance v6, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryTextString:Ljava/lang/String;

    #@e4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v6

    #@e8
    sget v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@ea
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v6

    #@ee
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v6

    #@f2
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f5
    goto/16 :goto_4c
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 251
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 252
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@9
    .line 253
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoProgressDialog:Landroid/app/ProgressDialog;

    #@c
    .line 255
    :cond_c
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 11
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 259
    const v2, 0x10202df

    #@5
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    .line 260
    .local v1, ok:Landroid/view/View;
    const v2, 0x1020283

    #@c
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->findViewById(I)Landroid/view/View;

    #@f
    move-result-object v0

    #@10
    .line 262
    .local v0, cancel:Landroid/view/View;
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@13
    move-result v2

    #@14
    if-lez v2, :cond_24

    #@16
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@19
    move-result v2

    #@1a
    const/4 v3, 0x4

    #@1b
    if-ge v2, v3, :cond_24

    #@1d
    .line 263
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    #@20
    .line 264
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    #@23
    .line 278
    :cond_23
    :goto_23
    return-void

    #@24
    .line 266
    :cond_24
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_31

    #@2a
    .line 267
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    #@2d
    .line 268
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    #@30
    goto :goto_23

    #@31
    .line 270
    :cond_31
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->minDigits:I

    #@33
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@36
    move-result v3

    #@37
    if-le v2, v3, :cond_41

    #@39
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->maxDigits:I

    #@3b
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@3e
    move-result v3

    #@3f
    if-gt v2, v3, :cond_23

    #@41
    .line 271
    :cond_41
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->KT_UsimPerso_retry_count:I

    #@43
    if-nez v2, :cond_4c

    #@45
    .line 272
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    #@48
    .line 276
    :goto_48
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    #@4b
    goto :goto_23

    #@4c
    .line 274
    :cond_4c
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    #@4f
    goto :goto_48
.end method

.method public readNetworkOperator()V
    .registers 4

    #@0
    .prologue
    .line 430
    const-string v0, "ro.build.target_operator"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "SKT"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_2d

    #@e
    .line 431
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@12
    .line 439
    :goto_12
    const-string v0, "KeyguardSimPersoView"

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "readNetworkOperator: mNetworkOpeator: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 441
    return-void

    #@2d
    .line 432
    :cond_2d
    const-string v0, "ro.build.target_operator"

    #@2f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    const-string v1, "KT"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_40

    #@3b
    .line 433
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_KT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@3d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@3f
    goto :goto_12

    #@40
    .line 434
    :cond_40
    const-string v0, "ro.build.target_operator"

    #@42
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    const-string v1, "LGT"

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_53

    #@4e
    .line 435
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_LGT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@50
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@52
    goto :goto_12

    #@53
    .line 437
    :cond_53
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_NONE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@55
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@57
    goto :goto_12
.end method

.method public readStringFromResource()V
    .registers 8

    #@0
    .prologue
    const v6, 0x20902d3

    #@3
    const v5, 0x20902d2

    #@6
    const v4, 0x20902d0

    #@9
    const/4 v3, 0x4

    #@a
    .line 395
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v0

    #@e
    .line 398
    .local v0, res:Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->readNetworkOperator()V

    #@11
    .line 400
    if-eqz v0, :cond_39

    #@13
    .line 403
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@15
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@17
    if-ne v1, v2, :cond_3a

    #@19
    .line 406
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderTextString:Ljava/lang/String;

    #@1f
    .line 407
    const v1, 0x20902d1

    #@22
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryTextString:Ljava/lang/String;

    #@28
    .line 408
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mStrTitleWrongPin:Ljava/lang/String;

    #@2e
    .line 409
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mPopupMsg:Ljava/lang/String;

    #@34
    .line 410
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->minDigits:I

    #@36
    .line 411
    const/4 v1, 0x3

    #@37
    sput v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->maxDigits:I

    #@39
    .line 424
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 413
    :cond_3a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mNetworkOpeator:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@3c
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_KT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@3e
    if-ne v1, v2, :cond_39

    #@40
    .line 416
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderTextString:Ljava/lang/String;

    #@46
    .line 417
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@48
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4b
    move-result-object v1

    #@4c
    const v2, 0x2090114

    #@4f
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mRetryTextString:Ljava/lang/String;

    #@59
    .line 418
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mStrTitleWrongPin:Ljava/lang/String;

    #@5f
    .line 419
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mPopupMsg:Ljava/lang/String;

    #@65
    .line 420
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->minDigits:I

    #@67
    .line 421
    const/4 v1, 0x7

    #@68
    sput v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->maxDigits:I

    #@6a
    goto :goto_39
.end method

.method public resetState()V
    .registers 3

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@6
    .line 121
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 246
    return-void
.end method

.method public updateEmergencyText()V
    .registers 4

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    if-eqz v0, :cond_26

    #@4
    .line 232
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mServiceState:Landroid/telephony/ServiceState;

    #@6
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@c
    .line 233
    const-string v0, "KeyguardSimPersoView"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "[LGE] get the service status="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 236
    :cond_26
    const-string v0, "KeyguardSimPersoView"

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "Service state on PIN/PUK = "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 237
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@42
    const/4 v1, 0x2

    #@43
    if-eq v0, v1, :cond_4a

    #@45
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->service_status:I

    #@47
    const/4 v1, 0x1

    #@48
    if-ne v0, v1, :cond_53

    #@4a
    .line 238
    :cond_4a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mEmergencyText:Landroid/widget/TextView;

    #@4c
    const v1, 0x1040325

    #@4f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@52
    .line 241
    :goto_52
    return-void

    #@53
    .line 240
    :cond_53
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mEmergencyText:Landroid/widget/TextView;

    #@55
    const-string v1, ""

    #@57
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5a
    goto :goto_52
.end method

.method protected verifyPasswordAndUnlock()V
    .registers 5

    #@0
    .prologue
    .line 320
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 322
    .local v0, entry:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->minDigits:I

    #@10
    if-ge v1, v2, :cond_29

    #@12
    .line 326
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mHeaderText:Landroid/widget/TextView;

    #@14
    const v2, 0x104009e

    #@17
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    #@1a
    .line 328
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1c
    const-string v2, ""

    #@1e
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@21
    .line 329
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@23
    const-wide/16 v2, 0x0

    #@25
    invoke-interface {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@28
    .line 390
    :cond_28
    :goto_28
    return-void

    #@29
    .line 333
    :cond_29
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->getUSimPersoProgressDialog()Landroid/app/Dialog;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    #@30
    .line 335
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoUnlockProgress:Z

    #@32
    if-nez v1, :cond_28

    #@34
    .line 336
    const/4 v1, 0x1

    #@35
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->mUsimPersoUnlockProgress:Z

    #@37
    .line 337
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$6;

    #@39
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@3b
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;Ljava/lang/String;)V

    #@46
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$6;->start()V

    #@49
    goto :goto_28
.end method
