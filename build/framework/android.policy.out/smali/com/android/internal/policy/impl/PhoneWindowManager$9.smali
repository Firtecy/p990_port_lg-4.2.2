.class Lcom/android/internal/policy/impl/PhoneWindowManager$9;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1518
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1521
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2002(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@6
    .line 1523
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@8
    iget-boolean v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@a
    if-eqz v1, :cond_32

    #@c
    .line 1524
    const-string v1, "service.keyguard.status"

    #@e
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 1525
    .local v0, keyguard_status:I
    if-eqz v0, :cond_17

    #@14
    const/4 v1, 0x1

    #@15
    if-ne v0, v1, :cond_31

    #@17
    .line 1526
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@19
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isKeyguardLocked()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_24

    #@1f
    .line 1527
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@21
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dismissKeyguardLw()V

    #@24
    .line 1529
    :cond_24
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@26
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@28
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyPkg:Ljava/lang/String;

    #@2a
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2c
    iget-object v3, v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyClass:Ljava/lang/String;

    #@2e
    invoke-static {v1, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2100(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 1535
    .end local v0           #keyguard_status:I
    :cond_31
    :goto_31
    return-void

    #@32
    .line 1533
    :cond_32
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@34
    invoke-static {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2200(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@37
    goto :goto_31
.end method
