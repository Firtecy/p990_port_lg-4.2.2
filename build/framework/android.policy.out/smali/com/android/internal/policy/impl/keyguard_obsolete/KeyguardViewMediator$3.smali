.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;
.super Landroid/os/Handler;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 974
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Z)V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 977
    iget v2, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_72

    #@7
    .line 1020
    :goto_7
    return-void

    #@8
    .line 979
    :pswitch_8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@c
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;I)V

    #@f
    goto :goto_7

    #@10
    .line 982
    :pswitch_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@12
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1200(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@15
    goto :goto_7

    #@16
    .line 985
    :pswitch_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@18
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1300(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@1b
    goto :goto_7

    #@1c
    .line 988
    :pswitch_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@1e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@21
    goto :goto_7

    #@22
    .line 991
    :pswitch_22
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@24
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@27
    goto :goto_7

    #@28
    .line 994
    :pswitch_28
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1600(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@2d
    goto :goto_7

    #@2e
    .line 997
    :pswitch_2e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@30
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@32
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;

    #@34
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewManager$ShowListener;)V

    #@37
    goto :goto_7

    #@38
    .line 1000
    :pswitch_38
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@3a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@3c
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1800(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;I)V

    #@3f
    goto :goto_7

    #@40
    .line 1003
    :pswitch_40
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@42
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@44
    if-eqz v3, :cond_4a

    #@46
    :goto_46
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V

    #@49
    goto :goto_7

    #@4a
    :cond_4a
    move v0, v1

    #@4b
    goto :goto_46

    #@4c
    .line 1006
    :pswitch_4c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@4e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$2000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@51
    goto :goto_7

    #@52
    .line 1009
    :pswitch_52
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@54
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->keyguardDone(Z)V

    #@57
    goto :goto_7

    #@58
    .line 1012
    :pswitch_58
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@5a
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@5c
    if-eqz v3, :cond_62

    #@5e
    :goto_5e
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$2100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;Z)V

    #@61
    goto :goto_7

    #@62
    :cond_62
    move v0, v1

    #@63
    goto :goto_5e

    #@64
    .line 1015
    :pswitch_64
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@66
    monitor-enter v1

    #@67
    .line 1016
    :try_start_67
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@69
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@6c
    .line 1017
    monitor-exit v1

    #@6d
    goto :goto_7

    #@6e
    :catchall_6e
    move-exception v0

    #@6f
    monitor-exit v1
    :try_end_70
    .catchall {:try_start_67 .. :try_end_70} :catchall_6e

    #@70
    throw v0

    #@71
    .line 977
    nop

    #@72
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_8
        :pswitch_10
        :pswitch_16
        :pswitch_1c
        :pswitch_22
        :pswitch_28
        :pswitch_2e
        :pswitch_38
        :pswitch_40
        :pswitch_4c
        :pswitch_52
        :pswitch_58
        :pswitch_64
    .end packed-switch
.end method
