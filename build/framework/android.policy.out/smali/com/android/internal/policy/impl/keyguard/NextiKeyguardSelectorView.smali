.class public Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
.super Landroid/widget/LinearLayout;
.source "NextiKeyguardSelectorView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;
    }
.end annotation


# static fields
.field private static final ASSIST_ICON_METADATA_NAME:Ljava/lang/String; = "com.android.systemui.action_assist_icon"

.field private static final CALL_LOG_PROJECTION:[Ljava/lang/String; = null

.field private static final CALL_LOG_TOKEN:I = -0x1

#the value of this static final field might be set in the static constructor
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "SecuritySelectorView"

.field public static mCameraDeterrenceFlag:Z

.field private static sBroadCastReceiver:Landroid/content/BroadcastReceiver;


# instance fields
.field private final mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

.field private mAnim:Landroid/animation/ObjectAnimator;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBouncerFrame:Landroid/graphics/drawable/Drawable;

.field private mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

.field private mCameraDisabled:Z

.field mCharalayout:Landroid/widget/LinearLayout;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mDocTouchIcon:Landroid/widget/ImageView;

.field private mFadeView:Landroid/view/View;

.field private mHandlerChara:Landroid/os/Handler;

.field mHandlerSp:Landroid/os/Handler;

.field mHandlerTel:Landroid/os/Handler;

.field private final mHasVibrator:Z

.field private mHistoryAtticon2:Landroid/widget/TextView;

.field private mHistoryAtticon3:Landroid/widget/TextView;

.field mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mIsBouncing:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mMascotVoiceIcon:Landroid/widget/ImageView;

.field mRemoteViews:Landroid/widget/RemoteViews;

.field private mRunnable:Ljava/lang/Runnable;

.field private mSearchDisabled:Z

.field private mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

.field private mSilentMode:Z

.field private mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 78
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@3
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->DEBUG:Z

    #@5
    .line 102
    const/4 v0, 0x5

    #@6
    new-array v0, v0, [Ljava/lang/String;

    #@8
    const-string v1, "_id"

    #@a
    aput-object v1, v0, v3

    #@c
    const/4 v1, 0x1

    #@d
    const-string v2, "number"

    #@f
    aput-object v2, v0, v1

    #@11
    const/4 v1, 0x2

    #@12
    const-string v2, "date"

    #@14
    aput-object v2, v0, v1

    #@16
    const/4 v1, 0x3

    #@17
    const-string v2, "duration"

    #@19
    aput-object v2, v0, v1

    #@1b
    const/4 v1, 0x4

    #@1c
    const-string v2, "type"

    #@1e
    aput-object v2, v0, v1

    #@20
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->CALL_LOG_PROJECTION:[Ljava/lang/String;

    #@22
    .line 175
    const/4 v0, 0x0

    #@23
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@25
    .line 176
    sput-boolean v3, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDeterrenceFlag:Z

    #@27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 273
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 274
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 277
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 110
    new-instance v1, Landroid/os/Handler;

    #@5
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerTel:Landroid/os/Handler;

    #@a
    .line 112
    new-instance v1, Landroid/os/Handler;

    #@c
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerSp:Landroid/os/Handler;

    #@11
    .line 114
    new-instance v1, Landroid/os/Handler;

    #@13
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@16
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerChara:Landroid/os/Handler;

    #@18
    .line 130
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@1b
    .line 232
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;

    #@1d
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@20
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@22
    .line 255
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$2;

    #@24
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@27
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@29
    .line 278
    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    #@2b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v2

    #@2f
    invoke-direct {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@32
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@34
    .line 280
    const-string v1, "vibrator"

    #@36
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/os/Vibrator;

    #@3c
    .line 281
    .local v0, vibrator:Landroid/os/Vibrator;
    if-nez v0, :cond_52

    #@3e
    const/4 v1, 0x0

    #@3f
    :goto_3f
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHasVibrator:Z

    #@41
    .line 282
    const-string v1, "audio"

    #@43
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@46
    move-result-object v1

    #@47
    check-cast v1, Landroid/media/AudioManager;

    #@49
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAudioManager:Landroid/media/AudioManager;

    #@4b
    .line 283
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->isSilentMode()Z

    #@4e
    move-result v1

    #@4f
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@51
    .line 285
    return-void

    #@52
    .line 281
    :cond_52
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    #@55
    move-result v1

    #@56
    goto :goto_3f
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateTargets()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->setSPModeMailUnReadCount(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getMissedCallCount()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mRunnable:Ljava/lang/Runnable;

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateView()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerChara:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1500()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 75
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->CALL_LOG_PROJECTION:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500()Z
    .registers 1

    #@0
    .prologue
    .line 75
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->setMissedCallCount(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method private getMissedCallCount()V
    .registers 11

    #@0
    .prologue
    .line 634
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Landroid/content/ContentResolver;)V

    #@b
    .line 637
    .local v0, queryHandler:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;
    new-instance v9, Ljava/lang/StringBuilder;

    #@d
    const-string v1, "type="

    #@f
    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@12
    .line 638
    .local v9, where:Ljava/lang/StringBuilder;
    const/4 v1, 0x3

    #@13
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    .line 639
    const-string v1, " AND new=1"

    #@18
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 643
    const/4 v1, -0x1

    #@1c
    const/4 v2, 0x0

    #@1d
    :try_start_1d
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@1f
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->CALL_LOG_PROJECTION:[Ljava/lang/String;

    #@21
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    const/4 v6, 0x0

    #@26
    const-string v7, "date DESC"

    #@28
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_2b} :catch_2c

    #@2b
    .line 648
    :goto_2b
    return-void

    #@2c
    .line 645
    :catch_2c
    move-exception v8

    #@2d
    .line 646
    .local v8, e:Ljava/lang/Exception;
    const-string v1, "SecuritySelectorView"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "NextiKeyguardSelectorView.getMissedCallCount(): missedcall query exception="

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_2b
.end method

.method private isSilentMode()Z
    .registers 3

    #@0
    .prologue
    .line 949
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAudioManager:Landroid/media/AudioManager;

    #@2
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-eq v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private launchActivity(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 978
    const v1, 0x34008000

    #@3
    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@6
    .line 986
    :try_start_6
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Landroid/app/IActivityManager;->dismissKeyguardOnNextActivity()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_13

    #@d
    .line 992
    :goto_d
    :try_start_d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_12
    .catch Landroid/content/ActivityNotFoundException; {:try_start_d .. :try_end_12} :catch_1c

    #@12
    .line 997
    :goto_12
    return-void

    #@13
    .line 987
    :catch_13
    move-exception v0

    #@14
    .line 988
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SecuritySelectorView"

    #@16
    const-string v2, "KeyguardSelectorView.launchActivity): can\'t dismiss keyguard on launch"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_d

    #@1c
    .line 993
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_1c
    move-exception v0

    #@1d
    .line 994
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v1, "SecuritySelectorView"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "KeyguardSelectorView.launchActivity(): Activity not found for intent + "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_12
.end method

.method private registerContentObserver()V
    .registers 5

    #@0
    .prologue
    .line 815
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@2
    if-nez v0, :cond_20

    #@4
    .line 816
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;

    #@6
    new-instance v1, Landroid/os/Handler;

    #@8
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@b
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Landroid/os/Handler;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@10
    .line 862
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17
    move-result-object v0

    #@18
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@1a
    const/4 v2, 0x1

    #@1b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@1d
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@20
    .line 865
    :cond_20
    return-void
.end method

.method private registerReceiver()V
    .registers 6

    #@0
    .prologue
    .line 739
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@2
    if-nez v1, :cond_39

    #@4
    .line 740
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;

    #@6
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;)V

    #@9
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@b
    .line 780
    new-instance v0, Landroid/content/IntentFilter;

    #@d
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@10
    .line 781
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "jp.co.nttdocomo.carriermail.APP_LINK_RECEIVED_MESSAGE_LOCAL"

    #@12
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15
    .line 782
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@17
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a
    .line 783
    const-string v1, "com.nttdocomo.android.mascot.KEYGUARD_UPDATE_LOCAL"

    #@1c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1f
    .line 784
    const-string v1, "com.nttdocomo.android.mascot.widget.LockScreenMascotWidget.ACTION_SCREEN_UNLOCK"

    #@21
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@24
    .line 787
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@26
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@28
    const-string v3, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@2a
    const/4 v4, 0x0

    #@2b
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@2e
    .line 789
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->DEBUG:Z

    #@30
    if-eqz v1, :cond_39

    #@32
    const-string v1, "SecuritySelectorView"

    #@34
    const-string v2, "NextiKeyguardSelectorView.registerReceiver(): regist "

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 792
    .end local v0           #intentFilter:Landroid/content/IntentFilter;
    :cond_39
    return-void
.end method

.method private setMissedCallCount(I)V
    .registers 4
    .parameter "count"

    #@0
    .prologue
    .line 655
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerTel:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$3;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 686
    return-void
.end method

.method private setSPModeMailUnReadCount(I)V
    .registers 4
    .parameter "count"

    #@0
    .prologue
    .line 694
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHandlerSp:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 723
    return-void
.end method

.method private toggleRingMode()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 954
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@4
    if-nez v0, :cond_17

    #@6
    move v0, v1

    #@7
    :goto_7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@9
    .line 955
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@b
    if-eqz v0, :cond_1b

    #@d
    .line 956
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAudioManager:Landroid/media/AudioManager;

    #@f
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHasVibrator:Z

    #@11
    if-eqz v3, :cond_19

    #@13
    :goto_13
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@16
    .line 962
    :goto_16
    return-void

    #@17
    :cond_17
    move v0, v2

    #@18
    .line 954
    goto :goto_7

    #@19
    :cond_19
    move v1, v2

    #@1a
    .line 956
    goto :goto_13

    #@1b
    .line 960
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAudioManager:Landroid/media/AudioManager;

    #@1d
    const/4 v1, 0x2

    #@1e
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@21
    goto :goto_16
.end method

.method private unregisterReceiver()V
    .registers 5

    #@0
    .prologue
    .line 801
    :try_start_0
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@2
    if-eqz v1, :cond_19

    #@4
    .line 802
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@6
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@8
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 803
    const/4 v1, 0x0

    #@c
    sput-object v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->sBroadCastReceiver:Landroid/content/BroadcastReceiver;

    #@e
    .line 804
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->DEBUG:Z

    #@10
    if-eqz v1, :cond_19

    #@12
    const-string v1, "SecuritySelectorView"

    #@14
    const-string v2, "NextiKeyguardSelectorView.unregisterReceiver(): unregist"

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    .line 810
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 807
    :catch_1a
    move-exception v0

    #@1b
    .line 808
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "SecuritySelectorView"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "NextiKeyguardSelectorView.unregisterReceiver(): exception:"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_19
.end method

.method private updateTargets()V
    .registers 16

    #@0
    .prologue
    .line 373
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    invoke-virtual {v12}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@5
    move-result v2

    #@6
    .line 374
    .local v2, currentUserHandle:I
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@8
    invoke-virtual {v12}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@b
    move-result-object v6

    #@c
    .line 375
    .local v6, dpm:Landroid/app/admin/DevicePolicyManager;
    const/4 v12, 0x0

    #@d
    invoke-virtual {v6, v12, v2}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    #@10
    move-result v5

    #@11
    .line 376
    .local v5, disabledFeatures:I
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@13
    invoke-virtual {v12}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@16
    move-result v12

    #@17
    if-eqz v12, :cond_8a

    #@19
    and-int/lit8 v12, v5, 0x2

    #@1b
    if-eqz v12, :cond_8a

    #@1d
    const/4 v11, 0x1

    #@1e
    .line 378
    .local v11, secureCameraDisabled:Z
    :goto_1e
    const/4 v12, 0x0

    #@1f
    invoke-virtual {v6, v12, v2}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    #@22
    move-result v12

    #@23
    if-nez v12, :cond_27

    #@25
    if-eqz v11, :cond_8c

    #@27
    :cond_27
    const/4 v1, 0x1

    #@28
    .line 380
    .local v1, cameraDisabledByAdmin:Z
    :goto_28
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@2b
    move-result-object v12

    #@2c
    invoke-static {v12}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2f
    move-result-object v8

    #@30
    .line 381
    .local v8, monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimLocked()Z

    #@33
    move-result v4

    #@34
    .line 405
    .local v4, disabledBySimState:Z
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@36
    const-string v13, "search"

    #@38
    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3b
    move-result-object v12

    #@3c
    check-cast v12, Landroid/app/SearchManager;

    #@3e
    iget-object v13, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@40
    const/4 v14, -0x2

    #@41
    invoke-virtual {v12, v13, v14}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@44
    move-result-object v12

    #@45
    if-eqz v12, :cond_8e

    #@47
    const/4 v10, 0x1

    #@48
    .line 408
    .local v10, searchActionAvailable:Z
    :goto_48
    const/4 v3, 0x0

    #@49
    .line 410
    .local v3, disabledByNoCamera:Z
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@4e
    move-result-object v9

    #@4f
    .line 411
    .local v9, packageManager:Landroid/content/pm/PackageManager;
    if-eqz v1, :cond_90

    #@51
    .line 412
    const-string v12, "SecuritySelectorView"

    #@53
    const-string v13, "NextiKeyguardSelectorView.updateTargets(): Camera disabled by Device Policy"

    #@55
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 426
    :cond_58
    :goto_58
    if-nez v1, :cond_5e

    #@5a
    if-nez v4, :cond_5e

    #@5c
    if-eqz v3, :cond_b5

    #@5e
    :cond_5e
    const/4 v12, 0x1

    #@5f
    :goto_5f
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDisabled:Z

    #@61
    .line 427
    if-nez v4, :cond_65

    #@63
    if-nez v10, :cond_b7

    #@65
    :cond_65
    const/4 v12, 0x1

    #@66
    :goto_66
    iput-boolean v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSearchDisabled:Z

    #@68
    .line 431
    iget-boolean v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSearchDisabled:Z

    #@6a
    if-nez v12, :cond_70

    #@6c
    iget-boolean v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@6e
    if-eqz v12, :cond_b9

    #@70
    .line 433
    :cond_70
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@72
    const/4 v13, 0x4

    #@73
    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    #@76
    .line 438
    :goto_76
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@78
    if-eqz v12, :cond_86

    #@7a
    .line 441
    if-nez v4, :cond_80

    #@7c
    iget-boolean v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@7e
    if-eqz v12, :cond_c0

    #@80
    .line 443
    :cond_80
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@82
    const/4 v13, 0x4

    #@83
    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@86
    .line 449
    :cond_86
    :goto_86
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateResources()V

    #@89
    .line 450
    return-void

    #@8a
    .line 376
    .end local v1           #cameraDisabledByAdmin:Z
    .end local v3           #disabledByNoCamera:Z
    .end local v4           #disabledBySimState:Z
    .end local v8           #monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    .end local v9           #packageManager:Landroid/content/pm/PackageManager;
    .end local v10           #searchActionAvailable:Z
    .end local v11           #secureCameraDisabled:Z
    :cond_8a
    const/4 v11, 0x0

    #@8b
    goto :goto_1e

    #@8c
    .line 378
    .restart local v11       #secureCameraDisabled:Z
    :cond_8c
    const/4 v1, 0x0

    #@8d
    goto :goto_28

    #@8e
    .line 405
    .restart local v1       #cameraDisabledByAdmin:Z
    .restart local v4       #disabledBySimState:Z
    .restart local v8       #monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    :cond_8e
    const/4 v10, 0x0

    #@8f
    goto :goto_48

    #@90
    .line 413
    .restart local v3       #disabledByNoCamera:Z
    .restart local v9       #packageManager:Landroid/content/pm/PackageManager;
    .restart local v10       #searchActionAvailable:Z
    :cond_90
    if-eqz v4, :cond_9a

    #@92
    .line 414
    const-string v12, "SecuritySelectorView"

    #@94
    const-string v13, "NextiKeyguardSelectorView.updateTargets(): disabled by Sim State"

    #@96
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    goto :goto_58

    #@9a
    .line 417
    :cond_9a
    new-instance v7, Landroid/content/Intent;

    #@9c
    const-string v12, "android.media.action.STILL_IMAGE_CAMERA"

    #@9e
    invoke-direct {v7, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a1
    .line 419
    .local v7, intent:Landroid/content/Intent;
    const/4 v12, 0x0

    #@a2
    invoke-virtual {v9, v7, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@a5
    move-result-object v0

    #@a6
    .line 420
    .local v0, applications:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@a9
    move-result v12

    #@aa
    if-gtz v12, :cond_58

    #@ac
    .line 422
    const-string v12, "SecuritySelectorView"

    #@ae
    const-string v13, "NextiKeyguardSelectorView.updateTargets(): There is no camera"

    #@b0
    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 423
    const/4 v3, 0x1

    #@b4
    goto :goto_58

    #@b5
    .line 426
    .end local v0           #applications:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v7           #intent:Landroid/content/Intent;
    :cond_b5
    const/4 v12, 0x0

    #@b6
    goto :goto_5f

    #@b7
    .line 427
    :cond_b7
    const/4 v12, 0x0

    #@b8
    goto :goto_66

    #@b9
    .line 435
    :cond_b9
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@bb
    const/4 v13, 0x0

    #@bc
    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    #@bf
    goto :goto_76

    #@c0
    .line 445
    :cond_c0
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@c2
    const/4 v13, 0x0

    #@c3
    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@c6
    goto :goto_86
.end method

.method private updateView()V
    .registers 6

    #@0
    .prologue
    .line 918
    const v3, 0x20d005c

    #@3
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v3

    #@7
    check-cast v3, Landroid/widget/LinearLayout;

    #@9
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@b
    .line 920
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@d
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    #@10
    move-result v3

    #@11
    if-lez v3, :cond_18

    #@13
    .line 921
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@15
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    #@18
    .line 923
    :cond_18
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mRemoteViews:Landroid/widget/RemoteViews;

    #@1a
    if-eqz v3, :cond_59

    #@1c
    .line 925
    :try_start_1c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mRemoteViews:Landroid/widget/RemoteViews;

    #@1e
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v3, v4, p0}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    #@23
    move-result-object v1

    #@24
    .line 926
    .local v1, expanded:Landroid/view/View;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@26
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@29
    .line 928
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@30
    move-result-object v2

    #@31
    .line 929
    .local v2, monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimLocked()Z

    #@34
    move-result v0

    #@35
    .line 932
    .local v0, disabledBySimState:Z
    if-nez v0, :cond_3b

    #@37
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@39
    if-eqz v3, :cond_50

    #@3b
    .line 934
    :cond_3b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@3d
    const/4 v4, 0x4

    #@3e
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@41
    .line 935
    const-string v3, "SecuritySelectorView"

    #@43
    const-string v4, "NextiKeyguardSelectorView.updateView(): disabled by Sim State"

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_48} :catch_57

    #@48
    .line 941
    .end local v0           #disabledBySimState:Z
    .end local v1           #expanded:Landroid/view/View;
    .end local v2           #monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    :goto_48
    const-string v3, "SecuritySelectorView"

    #@4a
    const-string v4, "NextiKeyguardSelectorView.updateView(): set remoteviews"

    #@4c
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 946
    :goto_4f
    return-void

    #@50
    .line 937
    .restart local v0       #disabledBySimState:Z
    .restart local v1       #expanded:Landroid/view/View;
    .restart local v2       #monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    :cond_50
    :try_start_50
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@52
    const/4 v4, 0x0

    #@53
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_50 .. :try_end_56} :catch_57

    #@56
    goto :goto_48

    #@57
    .line 939
    .end local v0           #disabledBySimState:Z
    .end local v1           #expanded:Landroid/view/View;
    .end local v2           #monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    :catch_57
    move-exception v3

    #@58
    goto :goto_48

    #@59
    .line 944
    :cond_59
    const-string v3, "SecuritySelectorView"

    #@5b
    const-string v4, "NextiKeyguardSelectorView.updateView(): remoteviews is null"

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_4f
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 1001
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "SecuritySelectorView"

    #@6
    const-string v1, "KeyguardSelectorView.cleanUp()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1003
    :cond_b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@d
    if-eqz v0, :cond_1f

    #@f
    .line 1004
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@19
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@1c
    .line 1005
    const/4 v0, 0x0

    #@1d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@1f
    .line 1007
    :cond_1f
    const/4 v0, 0x0

    #@20
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDeterrenceFlag:Z

    #@22
    .line 1008
    return-void
.end method

.method doTransition(Landroid/view/View;F)V
    .registers 6
    .parameter "view"
    .parameter "to"

    #@0
    .prologue
    .line 485
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAnim:Landroid/animation/ObjectAnimator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 486
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAnim:Landroid/animation/ObjectAnimator;

    #@6
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    #@9
    .line 488
    :cond_9
    const-string v0, "alpha"

    #@b
    const/4 v1, 0x1

    #@c
    new-array v1, v1, [F

    #@e
    const/4 v2, 0x0

    #@f
    aput p2, v1, v2

    #@11
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAnim:Landroid/animation/ObjectAnimator;

    #@17
    .line 489
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mAnim:Landroid/animation/ObjectAnimator;

    #@19
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@1c
    .line 490
    return-void
.end method

.method public getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 2

    #@0
    .prologue
    .line 541
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    return-object v0
.end method

.method public hideBouncer(I)V
    .registers 8
    .parameter "duration"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 571
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@3
    .line 572
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@5
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mFadeView:Landroid/view/View;

    #@7
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mBouncerFrame:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-static {v2, v3, v4, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewHelper;->hideBouncer(Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    #@c
    .line 576
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateTargets()V

    #@f
    .line 577
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@11
    if-eqz v2, :cond_18

    #@13
    .line 578
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@15
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    #@18
    .line 581
    :cond_18
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@1a
    if-eqz v2, :cond_31

    #@1c
    .line 582
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@1e
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@29
    move-result v0

    #@2a
    .line 583
    .local v0, atticon2length:I
    if-lez v0, :cond_31

    #@2c
    .line 584
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@2e
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    #@31
    .line 587
    .end local v0           #atticon2length:I
    :cond_31
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@33
    if-eqz v2, :cond_4a

    #@35
    .line 588
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@37
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@42
    move-result v1

    #@43
    .line 589
    .local v1, atticon3length:I
    if-lez v1, :cond_4a

    #@45
    .line 590
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@47
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    #@4a
    .line 594
    .end local v1           #atticon3length:I
    :cond_4a
    return-void
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 509
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "view"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const-wide/16 v6, 0x0

    #@4
    .line 869
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@6
    if-ne p1, v0, :cond_2e

    #@8
    .line 870
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDisabled:Z

    #@a
    if-nez v0, :cond_22

    #@c
    .line 874
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDeterrenceFlag:Z

    #@e
    if-nez v0, :cond_1c

    #@10
    .line 875
    new-instance v0, Landroid/content/Intent;

    #@12
    const-string v2, "android.media.action.STILL_IMAGE_CAMERA"

    #@14
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->launchActivity(Landroid/content/Intent;)V

    #@1a
    .line 876
    sput-boolean v3, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDeterrenceFlag:Z

    #@1c
    .line 879
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@1e
    invoke-interface {v0, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@21
    .line 898
    :cond_21
    :goto_21
    return-void

    #@22
    .line 882
    :cond_22
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->toggleRingMode()V

    #@25
    .line 883
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateResources()V

    #@28
    .line 884
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2a
    invoke-interface {v0, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@2d
    goto :goto_21

    #@2e
    .line 886
    :cond_2e
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@30
    if-ne p1, v0, :cond_21

    #@32
    .line 887
    const-string v0, "SecuritySelectorView"

    #@34
    const-string v2, "NextiKeyguardSelectorView.onClick(): AssistIcon"

    #@36
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 888
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@3b
    const-string v2, "search"

    #@3d
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@40
    move-result-object v0

    #@41
    check-cast v0, Landroid/app/SearchManager;

    #@43
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@45
    const/4 v5, -0x2

    #@46
    invoke-virtual {v0, v2, v5}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@49
    move-result-object v1

    #@4a
    .line 891
    .local v1, assistIntent:Landroid/content/Intent;
    if-eqz v1, :cond_59

    #@4c
    .line 892
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@4e
    const/4 v2, 0x0

    #@4f
    move-object v5, v4

    #@50
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivity(Landroid/content/Intent;ZZLandroid/os/Handler;Ljava/lang/Runnable;)V

    #@53
    .line 896
    :goto_53
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@55
    invoke-interface {v0, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@58
    goto :goto_21

    #@59
    .line 894
    :cond_59
    const-string v0, "SecuritySelectorView"

    #@5b
    const-string v2, "Failed to get intent for assist activity"

    #@5d
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_53
.end method

.method protected onFinishInflate()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x0

    #@2
    .line 289
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    #@5
    .line 294
    const v5, 0x20d006d

    #@8
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v5

    #@c
    check-cast v5, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@e
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@10
    .line 295
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@12
    if-eqz v5, :cond_1e

    #@14
    .line 296
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@16
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->initFrame()V

    #@19
    .line 297
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@1b
    invoke-virtual {v5, p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setOnTriggerListener(Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;)V

    #@1e
    .line 300
    :cond_1e
    const v5, 0x20d0063

    #@21
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Landroid/widget/TextView;

    #@27
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@29
    .line 301
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@2b
    if-eqz v5, :cond_3d

    #@2d
    .line 302
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@2f
    const-string v6, "/system/fonts/Roboto-Bold.ttf"

    #@31
    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@38
    .line 303
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@3d
    .line 306
    :cond_3d
    const v5, 0x20d0065

    #@40
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Landroid/widget/TextView;

    #@46
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@48
    .line 307
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@4a
    if-eqz v5, :cond_5c

    #@4c
    .line 308
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@4e
    const-string v6, "/system/fonts/Roboto-Bold.ttf"

    #@50
    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@57
    .line 309
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@59
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@5c
    .line 312
    :cond_5c
    const v5, 0x20d005e

    #@5f
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@62
    move-result-object v5

    #@63
    check-cast v5, Landroid/widget/ImageView;

    #@65
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@67
    .line 313
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@69
    if-eqz v5, :cond_75

    #@6b
    .line 314
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@6d
    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@70
    .line 315
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@72
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setFocusable(Z)V

    #@75
    .line 318
    :cond_75
    const v5, 0x20d0060

    #@78
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@7b
    move-result-object v5

    #@7c
    check-cast v5, Landroid/widget/ImageView;

    #@7e
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@80
    .line 319
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@82
    if-eqz v5, :cond_8e

    #@84
    .line 320
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@86
    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@89
    .line 321
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@8b
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setFocusable(Z)V

    #@8e
    .line 325
    :cond_8e
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->unregisterReceiver()V

    #@91
    .line 326
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->registerReceiver()V

    #@94
    .line 328
    const-string v5, "1"

    #@96
    const-string v6, "sys.boot_completed"

    #@98
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9b
    move-result-object v6

    #@9c
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v3

    #@a0
    .line 330
    .local v3, isBootCompleted:Z
    if-eqz v3, :cond_bc

    #@a2
    .line 332
    :try_start_a2
    new-instance v2, Landroid/content/Intent;

    #@a4
    const-string v5, "com.android.internal.policy.impl.CARRIERMAIL_COUNT_UPDATE"

    #@a6
    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a9
    .line 334
    .local v2, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@ab
    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@ae
    .line 336
    new-instance v4, Landroid/content/Intent;

    #@b0
    const-string v5, "com.android.internal.policy.impl.keyguard.ACTION_SCREEN_DISPLAY"

    #@b2
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b5
    .line 339
    .local v4, screenIntent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContext:Landroid/content/Context;

    #@b7
    const-string v6, "com.nttdocomo.android.screenlockservice.DCM_SCREEN"

    #@b9
    invoke-virtual {v5, v4, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_bc
    .catch Ljava/lang/IllegalStateException; {:try_start_a2 .. :try_end_bc} :catch_d4

    #@bc
    .line 348
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #screenIntent:Landroid/content/Intent;
    :cond_bc
    :goto_bc
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->updateTargets()V

    #@bf
    .line 350
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$Helper;

    #@c1
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$Helper;-><init>(Landroid/view/View;)V

    #@c4
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@c6
    .line 351
    const v5, 0x20d005b

    #@c9
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@cc
    move-result-object v0

    #@cd
    .line 352
    .local v0, bouncerFrameView:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    #@d0
    move-result-object v5

    #@d1
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mBouncerFrame:Landroid/graphics/drawable/Drawable;

    #@d3
    .line 353
    return-void

    #@d4
    .line 342
    .end local v0           #bouncerFrameView:Landroid/view/View;
    :catch_d4
    move-exception v1

    #@d5
    .line 343
    .local v1, e:Ljava/lang/IllegalStateException;
    const-string v5, "SecuritySelectorView"

    #@d7
    const-string v6, "NextiKeyguardSelectorView.onFinishInflate(): IllegalStateException Occurred"

    #@d9
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto :goto_bc
.end method

.method public onPause()V
    .registers 3

    #@0
    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@d
    .line 516
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 517
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onPause()V

    #@16
    .line 519
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@18
    if-eqz v0, :cond_2a

    #@1a
    .line 520
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@21
    move-result-object v0

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@24
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@27
    .line 521
    const/4 v0, 0x0

    #@28
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mContentObserver:Landroid/database/ContentObserver;

    #@2a
    .line 524
    :cond_2a
    return-void
.end method

.method public onResume(I)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@d
    .line 530
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 531
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mUnlockTouchPoint:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onResume()V

    #@16
    .line 533
    :cond_16
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->registerContentObserver()V

    #@19
    .line 534
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getMissedCallCount()V

    #@1c
    .line 535
    const/4 v0, 0x0

    #@1d
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDeterrenceFlag:Z

    #@1f
    .line 537
    return-void
.end method

.method public onScreenTimeOutExtended()V
    .registers 4

    #@0
    .prologue
    .line 969
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    const-wide/16 v1, 0x0

    #@4
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@7
    .line 970
    return-void
.end method

.method public onUnlocked()V
    .registers 3

    #@0
    .prologue
    .line 965
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@6
    .line 966
    return-void
.end method

.method public reset()V
    .registers 1

    #@0
    .prologue
    .line 505
    return-void
.end method

.method public setCarrierArea(Landroid/view/View;)V
    .registers 2
    .parameter "carrierArea"

    #@0
    .prologue
    .line 356
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mFadeView:Landroid/view/View;

    #@2
    .line 357
    return-void
.end method

.method public setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 493
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    .line 494
    return-void
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 2
    .parameter "utils"

    #@0
    .prologue
    .line 497
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    .line 498
    return-void
.end method

.method public showBouncer(I)V
    .registers 6
    .parameter "duration"

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 546
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mIsBouncing:Z

    #@4
    .line 547
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mFadeView:Landroid/view/View;

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mBouncerFrame:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-static {v0, v1, v2, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewHelper;->showBouncer(Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    #@d
    .line 551
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 552
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCharalayout:Landroid/widget/LinearLayout;

    #@13
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@16
    .line 554
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 555
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon2:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@1f
    .line 557
    :cond_1f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@21
    if-eqz v0, :cond_28

    #@23
    .line 558
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mHistoryAtticon3:Landroid/widget/TextView;

    #@25
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@28
    .line 560
    :cond_28
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@2a
    if-eqz v0, :cond_31

    #@2c
    .line 561
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@2e
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@31
    .line 563
    :cond_31
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@33
    if-eqz v0, :cond_3a

    #@35
    .line 564
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mMascotVoiceIcon:Landroid/widget/ImageView;

    #@37
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@3a
    .line 567
    :cond_3a
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 370
    return-void
.end method

.method public updateResources()V
    .registers 3

    #@0
    .prologue
    .line 901
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mCameraDisabled:Z

    #@2
    if-eqz v1, :cond_19

    #@4
    .line 903
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mSilentMode:Z

    #@6
    if-eqz v1, :cond_15

    #@8
    const v0, 0x20200aa

    #@b
    .line 908
    .local v0, resId:I
    :goto_b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 909
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->mDocTouchIcon:Landroid/widget/ImageView;

    #@11
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    #@14
    .line 911
    :cond_14
    return-void

    #@15
    .line 903
    .end local v0           #resId:I
    :cond_15
    const v0, 0x20200ad

    #@18
    goto :goto_b

    #@19
    .line 906
    :cond_19
    const v0, 0x20200a5

    #@1c
    .restart local v0       #resId:I
    goto :goto_b
.end method
