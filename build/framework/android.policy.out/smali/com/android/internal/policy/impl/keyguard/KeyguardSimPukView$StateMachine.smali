.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;
.super Ljava/lang/Object;
.source "KeyguardSimPukView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "StateMachine"
.end annotation


# static fields
.field static final CONFIRM_PIN:I = 0x2

.field static final DONE:I = 0x3

.field static final ENTER_PIN:I = 0x1

.field static final ENTER_PUK:I


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;


# direct methods
.method protected constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 83
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method get_state()I
    .registers 4

    #@0
    .prologue
    .line 142
    const-string v0, "KeyguardSimPukView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "get_state()   "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 143
    sget v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@1c
    return v0
.end method

.method public next()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 92
    const/4 v2, 0x0

    #@3
    .line 93
    .local v2, msg:I
    const/4 v0, 0x0

    #@4
    .line 96
    .local v0, attemptsRemaining:I
    :try_start_4
    const-string v3, "phone"

    #@6
    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v3

    #@a
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@d
    move-result-object v3

    #@e
    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->getIccPuk1RetryCount()I

    #@11
    move-result v0

    #@12
    .line 98
    const-string v3, "KeyguardSimPukView"

    #@14
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v5, "next()   "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_2a} :catch_59

    #@2a
    .line 103
    :goto_2a
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@2c
    if-nez v3, :cond_7b

    #@2e
    .line 104
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@30
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->checkPuk()Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_73

    #@36
    .line 105
    sput v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@38
    .line 107
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, "VDF"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_49

    #@44
    .line 108
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@46
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;I)I

    #@49
    .line 110
    :cond_49
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@4b
    sget v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@4d
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@50
    .line 131
    :cond_50
    :goto_50
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@52
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@54
    const/4 v4, 0x0

    #@55
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@58
    .line 132
    return-void

    #@59
    .line 99
    :catch_59
    move-exception v1

    #@5a
    .line 100
    .local v1, ex:Landroid/os/RemoteException;
    const-string v3, "KeyguardSimPukView"

    #@5c
    new-instance v4, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v5, "next() catch  "

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_2a

    #@73
    .line 112
    .end local v1           #ex:Landroid/os/RemoteException;
    :cond_73
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@75
    sget v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@77
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@7a
    goto :goto_50

    #@7b
    .line 114
    :cond_7b
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@7d
    if-ne v3, v6, :cond_99

    #@7f
    .line 115
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@81
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->checkPin()Z

    #@84
    move-result v3

    #@85
    if-eqz v3, :cond_91

    #@87
    .line 116
    sput v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@89
    .line 117
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@8b
    sget v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@8d
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@90
    goto :goto_50

    #@91
    .line 119
    :cond_91
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@93
    sget v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@95
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@98
    goto :goto_50

    #@99
    .line 121
    :cond_99
    sget v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@9b
    if-ne v3, v7, :cond_50

    #@9d
    .line 122
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@9f
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->confirmPin()Z

    #@a2
    move-result v3

    #@a3
    if-eqz v3, :cond_ae

    #@a5
    .line 123
    const/4 v3, 0x3

    #@a6
    sput v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@a8
    .line 124
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@aa
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->updateSim()V

    #@ad
    goto :goto_50

    #@ae
    .line 126
    :cond_ae
    sput v6, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@b0
    .line 127
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@b2
    sget v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@b4
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@b7
    .line 128
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@b9
    invoke-static {v3, v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@bc
    goto :goto_50
.end method

.method reset()V
    .registers 2

    #@0
    .prologue
    .line 135
    const-string v0, ""

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@4
    .line 136
    const-string v0, ""

    #@6
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPukText:Ljava/lang/String;

    #@8
    .line 137
    const/4 v0, 0x0

    #@9
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@b
    .line 138
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@f
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    #@12
    .line 139
    return-void
.end method
