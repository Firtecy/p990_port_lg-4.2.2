.class Lcom/android/internal/policy/impl/keyguard/PagedView$10;
.super Ljava/lang/Object;
.source "PagedView.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->onFlingToDelete(Landroid/graphics/PointF;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCount:I

.field private mOffset:F

.field private mStartTime:J

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;J)V
    .registers 6
    .parameter
    .parameter

    #@0
    .prologue
    .line 2428
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    iput-wide p2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->val$startTime:J

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 2429
    const/4 v0, -0x1

    #@8
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@a
    .line 2433
    iget-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->val$startTime:J

    #@c
    iput-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mStartTime:J

    #@e
    .line 2434
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 7
    .parameter "t"

    #@0
    .prologue
    .line 2438
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@2
    if-gez v0, :cond_14

    #@4
    .line 2439
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@6
    add-int/lit8 v0, v0, 0x1

    #@8
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@a
    .line 2445
    :cond_a
    :goto_a
    const/high16 v0, 0x3f80

    #@c
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mOffset:F

    #@e
    add-float/2addr v1, p1

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@12
    move-result v0

    #@13
    return v0

    #@14
    .line 2440
    :cond_14
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@16
    if-nez v0, :cond_a

    #@18
    .line 2441
    const/high16 v0, 0x3f00

    #@1a
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@1d
    move-result-wide v1

    #@1e
    iget-wide v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mStartTime:J

    #@20
    sub-long/2addr v1, v3

    #@21
    long-to-float v1, v1

    #@22
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@24
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$900(Lcom/android/internal/policy/impl/keyguard/PagedView;)I

    #@27
    move-result v2

    #@28
    int-to-float v2, v2

    #@29
    div-float/2addr v1, v2

    #@2a
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mOffset:F

    #@30
    .line 2443
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@32
    add-int/lit8 v0, v0, 0x1

    #@34
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$10;->mCount:I

    #@36
    goto :goto_a
.end method
