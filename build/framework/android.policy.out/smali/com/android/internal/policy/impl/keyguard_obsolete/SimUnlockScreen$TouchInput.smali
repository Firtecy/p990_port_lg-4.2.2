.class Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;
.super Ljava/lang/Object;
.source "SimUnlockScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchInput"
.end annotation


# instance fields
.field private mCancelButton:Landroid/widget/TextView;

.field private mEight:Landroid/widget/TextView;

.field private mFive:Landroid/widget/TextView;

.field private mFour:Landroid/widget/TextView;

.field private mNine:Landroid/widget/TextView;

.field private mOne:Landroid/widget/TextView;

.field private mSeven:Landroid/widget/TextView;

.field private mSix:Landroid/widget/TextView;

.field private mThree:Landroid/widget/TextView;

.field private mTwo:Landroid/widget/TextView;

.field private mZero:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;


# direct methods
.method private constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 318
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 319
    const v0, 0x10203b2

    #@8
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/widget/TextView;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mZero:Landroid/widget/TextView;

    #@10
    .line 320
    const v0, 0x10203a9

    #@13
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/TextView;

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mOne:Landroid/widget/TextView;

    #@1b
    .line 321
    const v0, 0x10203aa

    #@1e
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/widget/TextView;

    #@24
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mTwo:Landroid/widget/TextView;

    #@26
    .line 322
    const v0, 0x10203ab

    #@29
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Landroid/widget/TextView;

    #@2f
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mThree:Landroid/widget/TextView;

    #@31
    .line 323
    const v0, 0x10203ac

    #@34
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Landroid/widget/TextView;

    #@3a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFour:Landroid/widget/TextView;

    #@3c
    .line 324
    const v0, 0x10203ad

    #@3f
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@42
    move-result-object v0

    #@43
    check-cast v0, Landroid/widget/TextView;

    #@45
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFive:Landroid/widget/TextView;

    #@47
    .line 325
    const v0, 0x10203ae

    #@4a
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Landroid/widget/TextView;

    #@50
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSix:Landroid/widget/TextView;

    #@52
    .line 326
    const v0, 0x10203af

    #@55
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Landroid/widget/TextView;

    #@5b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSeven:Landroid/widget/TextView;

    #@5d
    .line 327
    const v0, 0x10203b0

    #@60
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@63
    move-result-object v0

    #@64
    check-cast v0, Landroid/widget/TextView;

    #@66
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mEight:Landroid/widget/TextView;

    #@68
    .line 328
    const v0, 0x10203b1

    #@6b
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@6e
    move-result-object v0

    #@6f
    check-cast v0, Landroid/widget/TextView;

    #@71
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mNine:Landroid/widget/TextView;

    #@73
    .line 329
    const v0, 0x1020283

    #@76
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@79
    move-result-object v0

    #@7a
    check-cast v0, Landroid/widget/TextView;

    #@7c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mCancelButton:Landroid/widget/TextView;

    #@7e
    .line 331
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mZero:Landroid/widget/TextView;

    #@80
    const-string v1, "0"

    #@82
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@85
    .line 332
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mOne:Landroid/widget/TextView;

    #@87
    const-string v1, "1"

    #@89
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8c
    .line 333
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mTwo:Landroid/widget/TextView;

    #@8e
    const-string v1, "2"

    #@90
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@93
    .line 334
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mThree:Landroid/widget/TextView;

    #@95
    const-string v1, "3"

    #@97
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@9a
    .line 335
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFour:Landroid/widget/TextView;

    #@9c
    const-string v1, "4"

    #@9e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@a1
    .line 336
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFive:Landroid/widget/TextView;

    #@a3
    const-string v1, "5"

    #@a5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@a8
    .line 337
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSix:Landroid/widget/TextView;

    #@aa
    const-string v1, "6"

    #@ac
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@af
    .line 338
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSeven:Landroid/widget/TextView;

    #@b1
    const-string v1, "7"

    #@b3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b6
    .line 339
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mEight:Landroid/widget/TextView;

    #@b8
    const-string v1, "8"

    #@ba
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@bd
    .line 340
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mNine:Landroid/widget/TextView;

    #@bf
    const-string v1, "9"

    #@c1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@c4
    .line 342
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mZero:Landroid/widget/TextView;

    #@c6
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@c9
    .line 343
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mOne:Landroid/widget/TextView;

    #@cb
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@ce
    .line 344
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mTwo:Landroid/widget/TextView;

    #@d0
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d3
    .line 345
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mThree:Landroid/widget/TextView;

    #@d5
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d8
    .line 346
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFour:Landroid/widget/TextView;

    #@da
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@dd
    .line 347
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFive:Landroid/widget/TextView;

    #@df
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@e2
    .line 348
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSix:Landroid/widget/TextView;

    #@e4
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@e7
    .line 349
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSeven:Landroid/widget/TextView;

    #@e9
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@ec
    .line 350
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mEight:Landroid/widget/TextView;

    #@ee
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@f1
    .line 351
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mNine:Landroid/widget/TextView;

    #@f3
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@f6
    .line 352
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mCancelButton:Landroid/widget/TextView;

    #@f8
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@fb
    .line 353
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 305
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)V

    #@3
    return-void
.end method

.method private checkDigit(Landroid/view/View;)I
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 371
    const/4 v0, -0x1

    #@1
    .line 372
    .local v0, digit:I
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mZero:Landroid/widget/TextView;

    #@3
    if-ne p1, v1, :cond_7

    #@5
    .line 373
    const/4 v0, 0x0

    #@6
    .line 393
    :cond_6
    :goto_6
    return v0

    #@7
    .line 374
    :cond_7
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mOne:Landroid/widget/TextView;

    #@9
    if-ne p1, v1, :cond_d

    #@b
    .line 375
    const/4 v0, 0x1

    #@c
    goto :goto_6

    #@d
    .line 376
    :cond_d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mTwo:Landroid/widget/TextView;

    #@f
    if-ne p1, v1, :cond_13

    #@11
    .line 377
    const/4 v0, 0x2

    #@12
    goto :goto_6

    #@13
    .line 378
    :cond_13
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mThree:Landroid/widget/TextView;

    #@15
    if-ne p1, v1, :cond_19

    #@17
    .line 379
    const/4 v0, 0x3

    #@18
    goto :goto_6

    #@19
    .line 380
    :cond_19
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFour:Landroid/widget/TextView;

    #@1b
    if-ne p1, v1, :cond_1f

    #@1d
    .line 381
    const/4 v0, 0x4

    #@1e
    goto :goto_6

    #@1f
    .line 382
    :cond_1f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mFive:Landroid/widget/TextView;

    #@21
    if-ne p1, v1, :cond_25

    #@23
    .line 383
    const/4 v0, 0x5

    #@24
    goto :goto_6

    #@25
    .line 384
    :cond_25
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSix:Landroid/widget/TextView;

    #@27
    if-ne p1, v1, :cond_2b

    #@29
    .line 385
    const/4 v0, 0x6

    #@2a
    goto :goto_6

    #@2b
    .line 386
    :cond_2b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mSeven:Landroid/widget/TextView;

    #@2d
    if-ne p1, v1, :cond_31

    #@2f
    .line 387
    const/4 v0, 0x7

    #@30
    goto :goto_6

    #@31
    .line 388
    :cond_31
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mEight:Landroid/widget/TextView;

    #@33
    if-ne p1, v1, :cond_38

    #@35
    .line 389
    const/16 v0, 0x8

    #@37
    goto :goto_6

    #@38
    .line 390
    :cond_38
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mNine:Landroid/widget/TextView;

    #@3a
    if-ne p1, v1, :cond_6

    #@3c
    .line 391
    const/16 v0, 0x9

    #@3e
    goto :goto_6
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 357
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->mCancelButton:Landroid/widget/TextView;

    #@2
    if-ne p1, v1, :cond_19

    #@4
    .line 358
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@6
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/widget/TextView;

    #@9
    move-result-object v1

    #@a
    const-string v2, ""

    #@c
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f
    .line 359
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@11
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToLockScreen()V

    #@18
    .line 368
    :cond_18
    :goto_18
    return-void

    #@19
    .line 363
    :cond_19
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->checkDigit(Landroid/view/View;)I

    #@1c
    move-result v0

    #@1d
    .line 364
    .local v0, digit:I
    if-ltz v0, :cond_18

    #@1f
    .line 365
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@21
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@24
    move-result-object v1

    #@25
    const/16 v2, 0x1388

    #@27
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@2a
    .line 366
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@2c
    invoke-static {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;I)V

    #@2f
    goto :goto_18
.end method
