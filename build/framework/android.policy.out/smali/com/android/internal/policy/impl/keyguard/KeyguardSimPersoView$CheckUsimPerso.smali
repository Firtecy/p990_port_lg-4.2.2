.class abstract Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;
.super Ljava/lang/Thread;
.source "KeyguardSimPersoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "CheckUsimPerso"
.end annotation


# instance fields
.field private final mPin:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;


# direct methods
.method protected constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "pin"

    #@0
    .prologue
    .line 287
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 288
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->mPin:Ljava/lang/String;

    #@7
    .line 289
    return-void
.end method


# virtual methods
.method abstract onUsimPersoCheckResponse(Z)V
.end method

.method public run()V
    .registers 7

    #@0
    .prologue
    .line 297
    :try_start_0
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->mPin:Ljava/lang/String;

    #@2
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 298
    .local v0, InputKey:Ljava/lang/String;
    const-string v3, "KeyguardSimPersoView"

    #@8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "onUsimPersoCheckResponse InputKey= "

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 300
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@20
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;)Landroid/content/Context;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@27
    move-result-object v3

    #@28
    const-string v4, "usim_perso_control_key"

    #@2a
    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v2

    #@32
    .line 301
    .local v2, matched:Z
    const-string v3, "KeyguardSimPersoView"

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v5, "onUsimPersoCheckResponse  matched == "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 303
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@4c
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso$1;

    #@4e
    invoke-direct {v4, p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;Z)V

    #@51
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->post(Ljava/lang/Runnable;)Z
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_54} :catch_55

    #@54
    .line 315
    .end local v0           #InputKey:Ljava/lang/String;
    .end local v2           #matched:Z
    :goto_54
    return-void

    #@55
    .line 308
    :catch_55
    move-exception v1

    #@56
    .line 309
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;

    #@58
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso$2;

    #@5a
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$CheckUsimPerso;)V

    #@5d
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->post(Ljava/lang/Runnable;)Z

    #@60
    goto :goto_54
.end method
