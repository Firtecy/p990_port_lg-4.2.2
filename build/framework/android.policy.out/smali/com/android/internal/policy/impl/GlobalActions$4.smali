.class Lcom/android/internal/policy/impl/GlobalActions$4;
.super Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/GlobalActions;->createDialog()Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/GlobalActions;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/GlobalActions;II)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 336
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$4;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    invoke-direct {p0, p2, p3}, Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;-><init>(II)V

    #@5
    return-void
.end method


# virtual methods
.method public onLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 346
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onPress()V
    .registers 4

    #@0
    .prologue
    .line 338
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 339
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    .line 340
    const/high16 v1, 0x1400

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@f
    .line 341
    const-string v1, "com.lge.homeselector"

    #@11
    const-string v2, "com.lge.homeselector.HomeSelector"

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16
    .line 342
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@1d
    .line 343
    return-void
.end method

.method public showBeforeProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 354
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showDuringKeyguard()Z
    .registers 2

    #@0
    .prologue
    .line 350
    const/4 v0, 0x1

    #@1
    return v0
.end method
