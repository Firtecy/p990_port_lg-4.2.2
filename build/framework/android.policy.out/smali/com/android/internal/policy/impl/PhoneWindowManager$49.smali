.class Lcom/android/internal/policy/impl/PhoneWindowManager$49;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8533
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 8536
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1400(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)V

    #@6
    .line 8537
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@8
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_2d

    #@e
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@10
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1100(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_2d

    #@16
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@18
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1600(Lcom/android/internal/policy/impl/PhoneWindowManager;)Landroid/os/PowerManager$WakeLock;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_2d

    #@22
    .line 8538
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@24
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@29
    move-result-wide v1

    #@2a
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V

    #@2d
    .line 8540
    :cond_2d
    return-void
.end method
