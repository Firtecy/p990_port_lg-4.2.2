.class Lcom/android/internal/policy/impl/PhoneWindowManager$32;
.super Landroid/os/UEventObserver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 7016
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$32;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 7019
    const-string v2, "WindowManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "mTouchCrackModeObserver : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 7020
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$32;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1a
    iget v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackMode:I

    #@1c
    if-nez v2, :cond_4c

    #@1e
    const-string v2, "CRACK"

    #@20
    const-string v3, "TOUCH_WINDOW_STATE"

    #@22
    invoke-virtual {p1, v3}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_4c

    #@2c
    .line 7021
    const-string v2, "WindowManager"

    #@2e
    const-string v3, "launch TouchCrackSettingsActivity"

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 7023
    :try_start_33
    new-instance v1, Landroid/content/Intent;

    #@35
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@38
    .line 7024
    .local v1, startintent:Landroid/content/Intent;
    const-string v2, "com.android.settings"

    #@3a
    const-string v3, "com.android.settings.lge.TouchCrackSettingsActivity"

    #@3c
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3f
    .line 7025
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$32;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@41
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@43
    new-instance v3, Landroid/os/UserHandle;

    #@45
    const/4 v4, -0x2

    #@46
    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    #@49
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_4c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_33 .. :try_end_4c} :catch_4d

    #@4c
    .line 7030
    .end local v1           #startintent:Landroid/content/Intent;
    :cond_4c
    :goto_4c
    return-void

    #@4d
    .line 7026
    :catch_4d
    move-exception v0

    #@4e
    .line 7027
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "WindowManager"

    #@50
    new-instance v3, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v4, " mTouchCrackModeObserver Exception : "

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    goto :goto_4c
.end method
