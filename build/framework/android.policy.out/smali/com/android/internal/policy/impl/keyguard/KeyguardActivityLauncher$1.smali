.class Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;
.super Ljava/lang/Object;
.source "KeyguardActivityLauncher.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/OnDismissAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivityWithAnimation(Landroid/content/Intent;ZLandroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

.field final synthetic val$animation:Landroid/os/Bundle;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$onStarted:Ljava/lang/Runnable;

.field final synthetic val$worker:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 189
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$intent:Landroid/content/Intent;

    #@4
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$animation:Landroid/os/Bundle;

    #@6
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$worker:Landroid/os/Handler;

    #@8
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$onStarted:Ljava/lang/Runnable;

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public onDismiss()Z
    .registers 6

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;)V

    #@5
    .line 193
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@7
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$intent:Landroid/content/Intent;

    #@9
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$animation:Landroid/os/Bundle;

    #@b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$worker:Landroid/os/Handler;

    #@d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher$1;->val$onStarted:Ljava/lang/Runnable;

    #@f
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@12
    .line 194
    const/4 v0, 0x1

    #@13
    return v0
.end method
