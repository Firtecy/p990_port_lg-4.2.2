.class public Lcom/android/internal/policy/impl/keyguard/NumPadKey;
.super Landroid/widget/Button;
.source "NumPadKey.java"


# static fields
.field private static final KEYPAD_NUMBER_FONT_SIZE:F = 23.33f

.field protected static final LOG_TAG:Ljava/lang/String; = "NumPadKey"

.field static sKlondike:[Ljava/lang/String;


# instance fields
.field mDigit:I

.field mEnableHaptics:Z

.field private mListener:Landroid/view/View$OnClickListener;

.field private mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

.field mTextView:Landroid/widget/TextView;

.field mTextViewResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 84
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 47
    const/4 v5, -0x1

    #@6
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@8
    .line 49
    const/4 v5, 0x0

    #@9
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@b
    .line 53
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;

    #@d
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;-><init>(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)V

    #@10
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mListener:Landroid/view/View$OnClickListener;

    #@12
    .line 90
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@14
    invoke-direct {v5, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;-><init>(Landroid/content/Context;)V

    #@17
    iput-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@19
    .line 91
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@1b
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1e
    move-result-object v4

    #@1f
    .line 93
    .local v4, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@21
    if-eq v4, v5, :cond_2b

    #@23
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@25
    if-eq v4, v5, :cond_2b

    #@27
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@29
    if-ne v4, v5, :cond_d8

    #@2b
    .line 94
    :cond_2b
    sget-object v5, Lcom/lge/internal/R$styleable;->NumPadKey:[I

    #@2d
    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@30
    move-result-object v0

    #@31
    .line 95
    .local v0, a:Landroid/content/res/TypedArray;
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@33
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@36
    move-result v5

    #@37
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@39
    .line 96
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3c
    move-result v5

    #@3d
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setTextViewResId(I)V

    #@40
    .line 97
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@43
    move-result-object v5

    #@44
    const-string v6, "DCM"

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_52

    #@4c
    .line 98
    const v5, 0x41baa3d7

    #@4f
    invoke-virtual {p0, v7, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setTextSize(IF)V

    #@52
    .line 107
    :cond_52
    :goto_52
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mListener:Landroid/view/View$OnClickListener;

    #@54
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@57
    .line 108
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;

    #@59
    invoke-direct {v5, p1}, Lcom/android/internal/policy/impl/keyguard/LiftToActivateListener;-><init>(Landroid/content/Context;)V

    #@5c
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    #@5f
    .line 109
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;

    #@61
    invoke-direct {v5, p1}, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;-><init>(Landroid/content/Context;)V

    #@64
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    #@67
    .line 111
    new-instance v5, Lcom/android/internal/widget/LockPatternUtils;

    #@69
    invoke-direct {v5, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@6c
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    #@6f
    move-result v5

    #@70
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mEnableHaptics:Z

    #@72
    .line 113
    new-instance v1, Landroid/text/SpannableStringBuilder;

    #@74
    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@77
    .line 114
    .local v1, builder:Landroid/text/SpannableStringBuilder;
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@79
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@80
    .line 115
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@82
    if-ltz v5, :cond_d4

    #@84
    .line 117
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@86
    if-eq v4, v5, :cond_d4

    #@88
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8a
    if-eq v4, v5, :cond_d4

    #@8c
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8e
    if-eq v4, v5, :cond_d4

    #@90
    .line 118
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->sKlondike:[Ljava/lang/String;

    #@92
    if-nez v5, :cond_a1

    #@94
    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@97
    move-result-object v5

    #@98
    const v6, 0x1070015

    #@9b
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@9e
    move-result-object v5

    #@9f
    sput-object v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->sKlondike:[Ljava/lang/String;

    #@a1
    .line 122
    :cond_a1
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->sKlondike:[Ljava/lang/String;

    #@a3
    if-eqz v5, :cond_d4

    #@a5
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->sKlondike:[Ljava/lang/String;

    #@a7
    array-length v5, v5

    #@a8
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@aa
    if-le v5, v6, :cond_d4

    #@ac
    .line 123
    sget-object v5, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->sKlondike:[Ljava/lang/String;

    #@ae
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@b0
    aget-object v2, v5, v6

    #@b2
    .line 124
    .local v2, extra:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@b5
    move-result v3

    #@b6
    .line 125
    .local v3, extraLen:I
    if-lez v3, :cond_d4

    #@b8
    .line 126
    const-string v5, " "

    #@ba
    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@bd
    .line 127
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@c0
    .line 128
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    #@c2
    const v6, 0x10302a5

    #@c5
    invoke-direct {v5, p1, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    #@c8
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    #@cb
    move-result v6

    #@cc
    sub-int/2addr v6, v3

    #@cd
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    #@d0
    move-result v7

    #@d1
    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@d4
    .line 136
    .end local v2           #extra:Ljava/lang/String;
    .end local v3           #extraLen:I
    :cond_d4
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setText(Ljava/lang/CharSequence;)V

    #@d7
    .line 137
    return-void

    #@d8
    .line 101
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #builder:Landroid/text/SpannableStringBuilder;
    :cond_d8
    sget-object v5, Lcom/android/internal/R$styleable;->NumPadKey:[I

    #@da
    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@dd
    move-result-object v0

    #@de
    .line 102
    .restart local v0       #a:Landroid/content/res/TypedArray;
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@e0
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e3
    move-result v5

    #@e4
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@e6
    .line 103
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@e9
    move-result v5

    #@ea
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->setTextViewResId(I)V

    #@ed
    goto/16 :goto_52
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2
    return-object v0
.end method


# virtual methods
.method public doHapticKeyClick()V
    .registers 3

    #@0
    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mEnableHaptics:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 159
    const/4 v0, 0x1

    #@5
    const/4 v1, 0x3

    #@6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->performHapticFeedback(II)Z

    #@9
    .line 163
    :cond_9
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 141
    invoke-super {p0}, Landroid/widget/Button;->onDetachedFromWindow()V

    #@3
    .line 144
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/ObscureSpeechDelegate;->sAnnouncedHeadset:Z

    #@6
    .line 145
    return-void
.end method

.method public setTextView(Landroid/widget/TextView;)V
    .registers 2
    .parameter "tv"

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@2
    .line 149
    return-void
.end method

.method public setTextViewResId(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 152
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@3
    .line 153
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextViewResId:I

    #@5
    .line 154
    return-void
.end method
