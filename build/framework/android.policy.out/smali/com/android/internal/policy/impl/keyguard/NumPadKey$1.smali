.class Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;
.super Ljava/lang/Object;
.source "NumPadKey.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/NumPadKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "thisView"

    #@0
    .prologue
    .line 56
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@2
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@4
    if-nez v1, :cond_26

    #@6
    .line 57
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@8
    iget v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextViewResId:I

    #@a
    if-lez v1, :cond_26

    #@c
    .line 58
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@e
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->getRootView()Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@14
    iget v2, v2, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextViewResId:I

    #@16
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    .line 59
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_26

    #@1c
    instance-of v1, v0, Landroid/widget/TextView;

    #@1e
    if-eqz v1, :cond_26

    #@20
    .line 60
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@22
    check-cast v0, Landroid/widget/TextView;

    #@24
    .end local v0           #v:Landroid/view/View;
    iput-object v0, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@26
    .line 65
    :cond_26
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@28
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@2a
    if-eqz v1, :cond_87

    #@2c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@2e
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@30
    invoke-virtual {v1}, Landroid/widget/TextView;->isEnabled()Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_87

    #@36
    .line 67
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@38
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    #@3d
    move-result v1

    #@3e
    const/4 v2, 0x7

    #@3f
    if-le v1, v2, :cond_5e

    #@41
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@43
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->access$000(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@4a
    move-result-object v1

    #@4b
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@4d
    if-eq v1, v2, :cond_5d

    #@4f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@51
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->access$000(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@58
    move-result-object v1

    #@59
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@5b
    if-ne v1, v2, :cond_5e

    #@5d
    .line 76
    :cond_5d
    :goto_5d
    return-void

    #@5e
    .line 70
    :cond_5e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@60
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@62
    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    #@65
    move-result v1

    #@66
    sget v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;->maxDigits:I

    #@68
    if-le v1, v2, :cond_78

    #@6a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@6c
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->access$000(Lcom/android/internal/policy/impl/keyguard/NumPadKey;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@73
    move-result-object v1

    #@74
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@76
    if-eq v1, v2, :cond_5d

    #@78
    .line 73
    :cond_78
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@7a
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mTextView:Landroid/widget/TextView;

    #@7c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@7e
    iget v2, v2, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->mDigit:I

    #@80
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    #@87
    .line 75
    :cond_87
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/NumPadKey$1;->this$0:Lcom/android/internal/policy/impl/keyguard/NumPadKey;

    #@89
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/NumPadKey;->doHapticKeyClick()V

    #@8c
    goto :goto_5d
.end method
