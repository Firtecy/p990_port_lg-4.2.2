.class Lcom/android/internal/policy/impl/PhoneWindowManager$38;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->showBootMessage(Ljava/lang/CharSequence;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

.field final synthetic val$msg:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 7506
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->val$msg:Ljava/lang/CharSequence;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 7508
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@5
    if-nez v1, :cond_7c

    #@7
    .line 7509
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@9
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$38$1;

    #@b
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@d
    iget-object v3, v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@f
    invoke-direct {v2, p0, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$38$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$38;Landroid/content/Context;)V

    #@12
    iput-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@14
    .line 7532
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@16
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@18
    const v2, 0x104040f

    #@1b
    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    #@1e
    .line 7533
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@20
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@22
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    #@25
    .line 7534
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@27
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@29
    const/4 v2, 0x1

    #@2a
    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@2d
    .line 7535
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2f
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@31
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@34
    move-result-object v1

    #@35
    const/16 v2, 0x7e5

    #@37
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@3a
    .line 7537
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3c
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@3e
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@41
    move-result-object v1

    #@42
    const/16 v2, 0x102

    #@44
    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    #@47
    .line 7540
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@49
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@4b
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@4e
    move-result-object v1

    #@4f
    const/high16 v2, 0x3f80

    #@51
    invoke-virtual {v1, v2}, Landroid/view/Window;->setDimAmount(F)V

    #@54
    .line 7541
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@56
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@58
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@5f
    move-result-object v0

    #@60
    .line 7542
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x5

    #@61
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@63
    .line 7543
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@65
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@67
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@6e
    .line 7544
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@70
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@72
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@75
    .line 7545
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@77
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@79
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    #@7c
    .line 7547
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    :cond_7c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@7e
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@80
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$38;->val$msg:Ljava/lang/CharSequence;

    #@82
    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@85
    .line 7548
    return-void
.end method
