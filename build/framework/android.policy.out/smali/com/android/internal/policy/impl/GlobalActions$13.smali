.class Lcom/android/internal/policy/impl/GlobalActions$13;
.super Landroid/os/Handler;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/GlobalActions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/GlobalActions;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/GlobalActions;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1037
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 1039
    iget v1, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v1, :pswitch_data_a8

    #@6
    .line 1073
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1041
    :pswitch_7
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@9
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1700(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/media/AudioManager;

    #@c
    move-result-object v1

    #@d
    if-eqz v1, :cond_6f

    #@f
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$1400()I

    #@12
    move-result v1

    #@13
    if-le v1, v4, :cond_6f

    #@15
    .line 1042
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@17
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1700(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/media/AudioManager;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$1400()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@22
    .line 1043
    const-string v1, "GlobalActions"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "mAudioManager.setRingerMode("

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$1400()I

    #@32
    move-result v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, ")"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1045
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@46
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1700(Lcom/android/internal/policy/impl/GlobalActions;)Landroid/media/AudioManager;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    #@4d
    move-result v1

    #@4e
    const/4 v2, 0x1

    #@4f
    if-ne v1, v2, :cond_6c

    #@51
    .line 1046
    const-string v1, "vibrator"

    #@53
    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@56
    move-result-object v1

    #@57
    if-eqz v1, :cond_89

    #@59
    .line 1047
    invoke-static {}, Lcom/android/internal/policy/impl/GlobalActions;->access$100()Landroid/content/Context;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, "vibrator"

    #@5f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@62
    move-result-object v0

    #@63
    check-cast v0, Landroid/os/Vibrator;

    #@65
    .line 1048
    .local v0, vibrator:Landroid/os/Vibrator;
    if-eqz v0, :cond_81

    #@67
    .line 1049
    const-wide/16 v1, 0xc8

    #@69
    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    #@6c
    .line 1058
    .end local v0           #vibrator:Landroid/os/Vibrator;
    :cond_6c
    :goto_6c
    invoke-static {v4}, Lcom/android/internal/policy/impl/GlobalActions;->access$1402(I)I

    #@6f
    .line 1061
    :cond_6f
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@71
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$900(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@74
    move-result-object v1

    #@75
    if-eqz v1, :cond_6

    #@77
    .line 1062
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@79
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$900(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$GlobalActionsDialog;->dismiss()V

    #@80
    goto :goto_6

    #@81
    .line 1051
    .restart local v0       #vibrator:Landroid/os/Vibrator;
    :cond_81
    const-string v1, "GlobalActions"

    #@83
    const-string v2, "vibrator is null. Skip vibrating."

    #@85
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_6c

    #@89
    .line 1054
    .end local v0           #vibrator:Landroid/os/Vibrator;
    :cond_89
    const-string v1, "GlobalActions"

    #@8b
    const-string v2, "VIBRATOR_SERVICE is not ready. Skip vibrating."

    #@8d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    goto :goto_6c

    #@91
    .line 1066
    :pswitch_91
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@93
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1800(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@96
    .line 1067
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@98
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1000(Lcom/android/internal/policy/impl/GlobalActions;)Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;

    #@9b
    move-result-object v1

    #@9c
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/GlobalActions$MyAdapter;->notifyDataSetChanged()V

    #@9f
    goto/16 :goto_6

    #@a1
    .line 1070
    :pswitch_a1
    iget-object v1, p0, Lcom/android/internal/policy/impl/GlobalActions$13;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@a3
    invoke-static {v1}, Lcom/android/internal/policy/impl/GlobalActions;->access$1900(Lcom/android/internal/policy/impl/GlobalActions;)V

    #@a6
    goto/16 :goto_6

    #@a8
    .line 1039
    :pswitch_data_a8
    .packed-switch 0x0
        :pswitch_7
        :pswitch_91
        :pswitch_a1
    .end packed-switch
.end method
