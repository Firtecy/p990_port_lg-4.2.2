.class Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;
.super Ljava/lang/Object;
.source "KeyguardPINView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    .line 84
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@4
    invoke-virtual {v1}, Landroid/widget/TextView;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_2a

    #@a
    .line 85
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@c
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@e
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@11
    move-result-object v0

    #@12
    .line 86
    .local v0, str:Ljava/lang/CharSequence;
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@15
    move-result v1

    #@16
    if-lez v1, :cond_2a

    #@18
    .line 87
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@1a
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1c
    const/4 v2, 0x0

    #@1d
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@20
    move-result v3

    #@21
    add-int/lit8 v3, v3, -0x1

    #@23
    invoke-interface {v0, v2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2a
    .line 90
    .end local v0           #str:Ljava/lang/CharSequence;
    :cond_2a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2c
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->doHapticKeyClick()V

    #@2f
    .line 91
    return-void
.end method
