.class Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;
.super Ljava/lang/Object;
.source "KeyguardPINView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->doHapticKeyClick()V

    #@5
    .line 68
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@7
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@9
    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 69
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPINView;->verifyPasswordAndUnlock()V

    #@14
    .line 71
    :cond_14
    return-void
.end method
