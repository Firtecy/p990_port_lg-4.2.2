.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;
.super Landroid/widget/FrameLayout;
.source "KeyguardViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewManagerHost"
.end annotation


# instance fields
.field private mKeyguardIsVisible:Z

.field mLastConfiguration:Landroid/content/res/Configuration;

.field private mShouldRecreateKeyguardViewWhenReshown:Z

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 347
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3
    .line 348
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@6
    .line 343
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@9
    .line 344
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@b
    .line 345
    new-instance v0, Landroid/content/res/Configuration;

    #@d
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@12
    .line 351
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@14
    if-eqz v0, :cond_29

    #@16
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@19
    move-result-object v0

    #@1a
    if-eqz v0, :cond_29

    #@1c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isNaviBarRemoved()Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_29

    #@26
    .line 354
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->setFitsSystemWindows(Z)V

    #@29
    .line 356
    :cond_29
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@2b
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@36
    .line 357
    return-void
.end method

.method private recreateLockScreenLocked(Landroid/content/res/Configuration;Z)V
    .registers 11
    .parameter "configs"
    .parameter "force"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 469
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@4
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@7
    move-result v1

    #@8
    .line 470
    .local v1, diff:I
    const/16 v3, 0x200

    #@a
    .line 471
    .local v3, notRecreationField:I
    and-int/lit16 v0, v1, -0x201

    #@c
    .line 472
    .local v0, changes:I
    if-nez p2, :cond_10

    #@e
    if-eqz v0, :cond_86

    #@10
    .line 473
    :cond_10
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@12
    if-eqz v4, :cond_30

    #@14
    .line 474
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@16
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@19
    move-result-object v4

    #@1a
    if-eqz v4, :cond_30

    #@1c
    .line 476
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@1e
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@21
    move-result-object v4

    #@22
    const v5, 0x20d005a

    #@25
    invoke-virtual {v4, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2b
    .line 478
    .local v2, nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    if-eqz v2, :cond_30

    #@2d
    .line 479
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->cleanUp()V

    #@30
    .line 483
    .end local v2           #nextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->getVisibility()I

    #@33
    move-result v4

    #@34
    if-nez v4, :cond_7c

    #@36
    .line 485
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    const-string v5, "recreate LockScreen because config is changed."

    #@3c
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 488
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@41
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@48
    move-result v4

    #@49
    if-eqz v4, :cond_70

    #@4b
    .line 489
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@4d
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isQuickCoverClosed()Z

    #@54
    move-result v4

    #@55
    if-nez v4, :cond_66

    #@57
    .line 490
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@59
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@5b
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Z

    #@5e
    move-result v5

    #@5f
    invoke-static {v4, v5, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;ZZLandroid/os/Bundle;)V

    #@62
    .line 509
    :goto_62
    const/4 v4, 0x0

    #@63
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@65
    .line 510
    return-void

    #@66
    .line 493
    :cond_66
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    const-string v5, "Do not recreate : Quick cover is closed"

    #@6c
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_62

    #@70
    .line 497
    :cond_70
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@72
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@74
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Z

    #@77
    move-result v5

    #@78
    invoke-static {v4, v5, v6, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;ZZLandroid/os/Bundle;)V

    #@7b
    goto :goto_62

    #@7c
    .line 501
    :cond_7c
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@7f
    move-result-object v4

    #@80
    const-string v5, "Do not recreate LockScreen : view not visible"

    #@82
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_62

    #@86
    .line 506
    :cond_86
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    const-string v5, "Do not recreate LockScreen : uimode change doesn\'t cause recreation."

    #@8c
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    goto :goto_62
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 402
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@6
    move-result-object v2

    #@7
    if-eqz v2, :cond_2f

    #@9
    .line 404
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_23

    #@f
    .line 405
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@12
    move-result v0

    #@13
    .line 406
    .local v0, keyCode:I
    const/4 v2, 0x4

    #@14
    if-ne v0, v2, :cond_23

    #@16
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@18
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->handleBackKey()Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_23

    #@22
    .line 422
    .end local v0           #keyCode:I
    :cond_22
    :goto_22
    return v1

    #@23
    .line 417
    :cond_23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@25
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->interceptMediaKey(Landroid/view/KeyEvent;)Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_22

    #@2f
    .line 422
    :cond_2f
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@32
    move-result v1

    #@33
    goto :goto_22
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 5
    .parameter "insets"

    #@0
    .prologue
    .line 361
    const-string v0, "TAG"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "bug 7643792: fitSystemWindows("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ")"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 362
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@25
    move-result v0

    #@26
    return v0
.end method

.method public isKeyguardVisible()Z
    .registers 2

    #@0
    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@2
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 8
    .parameter "newConfig"

    #@0
    .prologue
    .line 367
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 372
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@5
    monitor-enter v4

    #@6
    .line 376
    :try_start_6
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@8
    if-eqz v3, :cond_18

    #@a
    .line 377
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@c
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@f
    move-result-object v3

    #@10
    if-eqz v3, :cond_16

    #@12
    .line 378
    const/4 v3, 0x0

    #@13
    invoke-direct {p0, p1, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->recreateLockScreenLocked(Landroid/content/res/Configuration;Z)V

    #@16
    .line 397
    :cond_16
    :goto_16
    monitor-exit v4

    #@17
    .line 398
    return-void

    #@18
    .line 381
    :cond_18
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->getVisibility()I

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_16

    #@1e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@20
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@23
    move-result-object v3

    #@24
    if-eqz v3, :cond_16

    #@26
    .line 382
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@28
    if-nez v3, :cond_44

    #@2a
    .line 383
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@2c
    invoke-virtual {v3, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@2f
    move-result v1

    #@30
    .line 384
    .local v1, diff:I
    const/16 v2, 0x200

    #@32
    .line 385
    .local v2, notRecreationField:I
    and-int/lit16 v0, v1, -0x201

    #@34
    .line 386
    .local v0, changes:I
    if-eqz v0, :cond_44

    #@36
    .line 387
    const/4 v3, 0x1

    #@37
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@39
    .line 388
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@3b
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@3e
    move-result-object v3

    #@3f
    const/16 v5, 0x8

    #@41
    invoke-virtual {v3, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@44
    .line 391
    .end local v0           #changes:I
    .end local v1           #diff:I
    .end local v2           #notRecreationField:I
    :cond_44
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@46
    if-eqz v3, :cond_16

    #@48
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@4b
    move-result-object v3

    #@4c
    if-eqz v3, :cond_16

    #@4e
    .line 393
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@59
    goto :goto_16

    #@5a
    .line 397
    :catchall_5a
    move-exception v3

    #@5b
    monitor-exit v4
    :try_end_5c
    .catchall {:try_start_6 .. :try_end_5c} :catchall_5a

    #@5c
    throw v3
.end method

.method public requestRecreateKeyguardViewWhenReshown()V
    .registers 2

    #@0
    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 464
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@7
    .line 466
    :cond_7
    return-void
.end method

.method public setKeyguardIsVisible(ZZ)V
    .registers 8
    .parameter "keyguardIsVisible"
    .parameter "isInitialCondition"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 427
    if-eqz p2, :cond_9

    #@4
    .line 428
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@6
    .line 429
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@8
    .line 456
    :cond_8
    :goto_8
    return-void

    #@9
    .line 431
    :cond_9
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@b
    if-ne v1, p1, :cond_1f

    #@d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@f
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    #@11
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@1a
    move-result-object v2

    #@1b
    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    #@1d
    if-eq v1, v2, :cond_46

    #@1f
    .line 433
    :cond_1f
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mKeyguardIsVisible:Z

    #@21
    .line 434
    if-eqz p1, :cond_46

    #@23
    .line 435
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mShouldRecreateKeyguardViewWhenReshown:Z

    #@25
    if-nez v1, :cond_39

    #@27
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mLastConfiguration:Landroid/content/res/Configuration;

    #@29
    iget v1, v1, Landroid/content/res/Configuration;->seq:I

    #@2b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@34
    move-result-object v2

    #@35
    iget v2, v2, Landroid/content/res/Configuration;->seq:I

    #@37
    if-ge v1, v2, :cond_93

    #@39
    .line 438
    :cond_39
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@3b
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@42
    move-result-object v1

    #@43
    invoke-direct {p0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->recreateLockScreenLocked(Landroid/content/res/Configuration;Z)V

    #@46
    .line 447
    :cond_46
    :goto_46
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@48
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@4b
    move-result-object v1

    #@4c
    if-eqz v1, :cond_8

    #@4e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@50
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@53
    move-result-object v1

    #@54
    const-string v2, "multi_pane_challenge"

    #@56
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@59
    move-result-object v1

    #@5a
    if-eqz v1, :cond_8

    #@5c
    .line 448
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@65
    move-result-object v1

    #@66
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    #@68
    .line 449
    .local v0, orientationOfView:I
    if-ne v0, v4, :cond_8

    #@6a
    .line 450
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$000()Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    new-instance v2, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v3, "setKeyguardIsVisible: View has multi_pane_challenge, orientation = "

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v2

    #@7d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v2

    #@81
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 451
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->mContext:Landroid/content/Context;

    #@86
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@89
    move-result-object v1

    #@8a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@8d
    move-result-object v1

    #@8e
    invoke-direct {p0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->recreateLockScreenLocked(Landroid/content/res/Configuration;Z)V

    #@91
    goto/16 :goto_8

    #@93
    .line 441
    .end local v0           #orientationOfView:I
    :cond_93
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@95
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@98
    move-result-object v1

    #@99
    if-eqz v1, :cond_46

    #@9b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@9d
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@a0
    move-result-object v1

    #@a1
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->getVisibility()I

    #@a4
    move-result v1

    #@a5
    if-eqz v1, :cond_46

    #@a7
    .line 443
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ViewManagerHost;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;

    #@a9
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@ac
    move-result-object v1

    #@ad
    invoke-virtual {v1, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setVisibility(I)V

    #@b0
    goto :goto_46
.end method
