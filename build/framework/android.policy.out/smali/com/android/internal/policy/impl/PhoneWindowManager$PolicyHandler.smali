.class Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;
.super Landroid/os/Handler;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PolicyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method private constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 846
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Lcom/android/internal/policy/impl/PhoneWindowManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 846
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 849
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_104

    #@5
    .line 919
    :cond_5
    :goto_5
    return-void

    #@6
    .line 851
    :sswitch_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@8
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$300(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@b
    goto :goto_5

    #@c
    .line 854
    :sswitch_c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@e
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$400(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@11
    goto :goto_5

    #@12
    .line 857
    :sswitch_12
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@14
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16
    check-cast v0, Landroid/view/KeyEvent;

    #@18
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dispatchMediaKeyWithWakeLock(Landroid/view/KeyEvent;)V

    #@1b
    goto :goto_5

    #@1c
    .line 860
    :sswitch_1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    check-cast v0, Landroid/view/KeyEvent;

    #@22
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dispatchMediaKeyRepeatWithWakeLock(Landroid/view/KeyEvent;)V

    #@25
    goto :goto_5

    #@26
    .line 864
    :sswitch_26
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@28
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_5

    #@2e
    .line 865
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@30
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@32
    monitor-enter v1

    #@33
    .line 866
    :try_start_33
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@35
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$600(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@38
    .line 867
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3a
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@3c
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@3e
    const/4 v3, 0x1

    #@3f
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@42
    .line 868
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@44
    const/4 v2, 0x1

    #@45
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$202(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@48
    .line 869
    monitor-exit v1

    #@49
    goto :goto_5

    #@4a
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit v1
    :try_end_4c
    .catchall {:try_start_33 .. :try_end_4c} :catchall_4a

    #@4c
    throw v0

    #@4d
    .line 873
    :sswitch_4d
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4f
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_5

    #@55
    .line 874
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@57
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@59
    monitor-enter v1

    #@5a
    .line 875
    :try_start_5a
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@5c
    const/4 v2, 0x0

    #@5d
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$202(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@60
    .line 876
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@62
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@64
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@66
    const/4 v3, 0x0

    #@67
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@6a
    .line 877
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@6c
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$700(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@6f
    .line 878
    monitor-exit v1

    #@70
    goto :goto_5

    #@71
    :catchall_71
    move-exception v0

    #@72
    monitor-exit v1
    :try_end_73
    .catchall {:try_start_5a .. :try_end_73} :catchall_71

    #@73
    throw v0

    #@74
    .line 884
    :sswitch_74
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@76
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$800(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@79
    move-result v0

    #@7a
    if-eqz v0, :cond_5

    #@7c
    .line 885
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@7e
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@80
    monitor-enter v1

    #@81
    .line 886
    :try_start_81
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@83
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$600(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@86
    .line 887
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@88
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@8a
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@8c
    const/4 v3, 0x1

    #@8d
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@90
    .line 888
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@92
    const/4 v2, 0x1

    #@93
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$102(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@96
    .line 889
    monitor-exit v1

    #@97
    goto/16 :goto_5

    #@99
    :catchall_99
    move-exception v0

    #@9a
    monitor-exit v1
    :try_end_9b
    .catchall {:try_start_81 .. :try_end_9b} :catchall_99

    #@9b
    throw v0

    #@9c
    .line 893
    :sswitch_9c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@9e
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$800(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@a1
    move-result v0

    #@a2
    if-eqz v0, :cond_5

    #@a4
    .line 894
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@a6
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@a8
    monitor-enter v1

    #@a9
    .line 895
    :try_start_a9
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@ab
    const/4 v2, 0x0

    #@ac
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$102(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@af
    .line 896
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@b1
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@b3
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@b5
    const/4 v3, 0x0

    #@b6
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@b9
    .line 897
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@bb
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$700(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@be
    .line 898
    monitor-exit v1

    #@bf
    goto/16 :goto_5

    #@c1
    :catchall_c1
    move-exception v0

    #@c2
    monitor-exit v1
    :try_end_c3
    .catchall {:try_start_a9 .. :try_end_c3} :catchall_c1

    #@c3
    throw v0

    #@c4
    .line 904
    :sswitch_c4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@c6
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@c8
    monitor-enter v1

    #@c9
    .line 905
    :try_start_c9
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@cb
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$600(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@ce
    .line 906
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@d0
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@d2
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@d4
    const/4 v3, 0x1

    #@d5
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@d8
    .line 907
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@da
    const/4 v2, 0x1

    #@db
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$002(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@de
    .line 908
    monitor-exit v1

    #@df
    goto/16 :goto_5

    #@e1
    :catchall_e1
    move-exception v0

    #@e2
    monitor-exit v1
    :try_end_e3
    .catchall {:try_start_c9 .. :try_end_e3} :catchall_e1

    #@e3
    throw v0

    #@e4
    .line 911
    :sswitch_e4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@e6
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@e8
    monitor-enter v1

    #@e9
    .line 912
    :try_start_e9
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@eb
    const/4 v2, 0x0

    #@ec
    invoke-static {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$002(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I

    #@ef
    .line 913
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@f1
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@f3
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@f5
    const/4 v3, 0x0

    #@f6
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V

    #@f9
    .line 914
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@fb
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$700(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@fe
    .line 915
    monitor-exit v1

    #@ff
    goto/16 :goto_5

    #@101
    :catchall_101
    move-exception v0

    #@102
    monitor-exit v1
    :try_end_103
    .catchall {:try_start_e9 .. :try_end_103} :catchall_101

    #@103
    throw v0

    #@104
    .line 849
    :sswitch_data_104
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_c
        0x3 -> :sswitch_12
        0x4 -> :sswitch_1c
        0x65 -> :sswitch_26
        0x66 -> :sswitch_4d
        0x67 -> :sswitch_74
        0x68 -> :sswitch_9c
        0x69 -> :sswitch_c4
        0x6a -> :sswitch_e4
    .end sparse-switch
.end method
