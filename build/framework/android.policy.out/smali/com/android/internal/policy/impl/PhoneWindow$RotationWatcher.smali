.class Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;
.super Landroid/view/IRotationWatcher$Stub;
.source "PhoneWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RotationWatcher"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIsWatching:Z

.field private final mRotationChanged:Ljava/lang/Runnable;

.field private final mWindows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/internal/policy/impl/PhoneWindow;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 3781
    invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V

    #@3
    .line 3783
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mRotationChanged:Ljava/lang/Runnable;

    #@a
    .line 3788
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@11
    return-void
.end method


# virtual methods
.method public addWindow(Lcom/android/internal/policy/impl/PhoneWindow;)V
    .registers 6
    .parameter "phoneWindow"

    #@0
    .prologue
    .line 3798
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 3799
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mIsWatching:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2b

    #@5
    if-nez v1, :cond_16

    #@7
    .line 3801
    :try_start_7
    sget-object v1, Lcom/android/internal/policy/impl/PhoneWindow$WindowManagerHolder;->sWindowManager:Landroid/view/IWindowManager;

    #@9
    invoke-interface {v1, p0}, Landroid/view/IWindowManager;->watchRotation(Landroid/view/IRotationWatcher;)I

    #@c
    .line 3802
    new-instance v1, Landroid/os/Handler;

    #@e
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@11
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mHandler:Landroid/os/Handler;

    #@13
    .line 3803
    const/4 v1, 0x1

    #@14
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mIsWatching:Z
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_2b
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_16} :catch_22

    #@16
    .line 3808
    :cond_16
    :goto_16
    :try_start_16
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@18
    new-instance v3, Ljava/lang/ref/WeakReference;

    #@1a
    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1d
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20
    .line 3809
    monitor-exit v2

    #@21
    .line 3810
    return-void

    #@22
    .line 3804
    :catch_22
    move-exception v0

    #@23
    .line 3805
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "PhoneWindow"

    #@25
    const-string v3, "Couldn\'t start watching for device rotation"

    #@27
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    goto :goto_16

    #@2b
    .line 3809
    .end local v0           #ex:Landroid/os/RemoteException;
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_16 .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method dispatchRotationChanged()V
    .registers 6

    #@0
    .prologue
    .line 3828
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 3829
    const/4 v0, 0x0

    #@4
    .line 3830
    .local v0, i:I
    :goto_4
    :try_start_4
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v3

    #@a
    if-ge v0, v3, :cond_2b

    #@c
    .line 3831
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@14
    .line 3832
    .local v1, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Lcom/android/internal/policy/impl/PhoneWindow;

    #@1a
    .line 3833
    .local v2, win:Lcom/android/internal/policy/impl/PhoneWindow;
    if-eqz v2, :cond_22

    #@1c
    .line 3834
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/PhoneWindow;->onOptionsPanelRotationChanged()V

    #@1f
    .line 3835
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_4

    #@22
    .line 3837
    :cond_22
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@27
    goto :goto_4

    #@28
    .line 3840
    .end local v1           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    .end local v2           #win:Lcom/android/internal/policy/impl/PhoneWindow;
    :catchall_28
    move-exception v3

    #@29
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_4 .. :try_end_2a} :catchall_28

    #@2a
    throw v3

    #@2b
    :cond_2b
    :try_start_2b
    monitor-exit v4
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_28

    #@2c
    .line 3841
    return-void
.end method

.method public onRotationChanged(I)V
    .registers 4
    .parameter "rotation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3794
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mRotationChanged:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 3795
    return-void
.end method

.method public removeWindow(Lcom/android/internal/policy/impl/PhoneWindow;)V
    .registers 7
    .parameter "phoneWindow"

    #@0
    .prologue
    .line 3813
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 3814
    const/4 v0, 0x0

    #@4
    .line 3815
    .local v0, i:I
    :goto_4
    :try_start_4
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v3

    #@a
    if-ge v0, v3, :cond_2a

    #@c
    .line 3816
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@14
    .line 3817
    .local v1, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Lcom/android/internal/policy/impl/PhoneWindow;

    #@1a
    .line 3818
    .local v2, win:Lcom/android/internal/policy/impl/PhoneWindow;
    if-eqz v2, :cond_1e

    #@1c
    if-ne v2, p1, :cond_27

    #@1e
    .line 3819
    :cond_1e
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->mWindows:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@23
    goto :goto_4

    #@24
    .line 3824
    .end local v1           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    .end local v2           #win:Lcom/android/internal/policy/impl/PhoneWindow;
    :catchall_24
    move-exception v3

    #@25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_24

    #@26
    throw v3

    #@27
    .line 3821
    .restart local v1       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    .restart local v2       #win:Lcom/android/internal/policy/impl/PhoneWindow;
    :cond_27
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_4

    #@2a
    .line 3824
    .end local v1           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/policy/impl/PhoneWindow;>;"
    .end local v2           #win:Lcom/android/internal/policy/impl/PhoneWindow;
    :cond_2a
    :try_start_2a
    monitor-exit v4
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_24

    #@2b
    .line 3825
    return-void
.end method
