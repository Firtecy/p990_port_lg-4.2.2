.class public Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
.source "KeyguardHostView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$UserSwitcherCallback;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$TransportCallback;
    }
.end annotation


# static fields
.field static final APPWIDGET_HOST_ID:I = 0x4b455947

.field public static DEBUG:Z = false

.field private static final ENABLE_MENU_KEY_FILE:Ljava/lang/String; = "/data/local/enable_menu_key"

.field private static final TAG:Ljava/lang/String; = "KeyguardHostView"

.field private static final carrier:Ljava/lang/String;

.field private static final country:Ljava/lang/String;


# instance fields
.field private final MAX_WIDGETS:I

.field protected halfWrongPasswordDialog:Landroid/app/AlertDialog;

.field private final mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

.field private mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

.field private mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private mAppWidgetToShow:I

.field private mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

.field private mCameraDisabled:Z

.field private final mCameraWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$Callbacks;

.field private mCheckAppWidgetConsistencyOnBootCompleted:Z

.field private mCleanupAppWidgetsOnBootCompleted:Z

.field private mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

.field private mDisabledFeatures:I

.field protected mDismissAction:Lcom/android/internal/policy/impl/keyguard/OnDismissAction;

.field private mEnableFallback:Z

.field protected mFailedAttempts:I

.field private mIsVerifyUnlockOnly:Z

.field private mKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

.field private mLgMusicPosition:I

.field private mLgMusicWidgetId:I

.field private mNextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

.field private mNullCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

.field private mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

.field mPersitentStickyWidgetLoaded:Z

.field private mQremoteId:I

.field private mQremotePosition:I

.field private mSafeModeEnabled:Z

.field private mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

.field private mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

.field private mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

.field protected mShowSecurityWhenReturn:Z

.field private mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

.field mSwitchPageRunnable:Ljava/lang/Runnable;

.field private mTempRect:Landroid/graphics/Rect;

.field private mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

.field private mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mUserId:I

.field private mUserSetupCompleted:Z

.field private mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

.field private mWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

.field private mWidgetContext:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 82
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@3
    .line 114
    const-string v0, "ro.build.target_operator"

    #@5
    const-string v1, "COM"

    #@7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->carrier:Ljava/lang/String;

    #@d
    .line 115
    const-string v0, "ro.build.target_country"

    #@f
    const-string v1, "COM"

    #@11
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->country:Ljava/lang/String;

    #@17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 159
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 160
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 10
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 163
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@6
    .line 87
    const/4 v1, 0x5

    #@7
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->MAX_WIDGETS:I

    #@9
    .line 98
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Invalid:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d
    .line 101
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCheckAppWidgetConsistencyOnBootCompleted:Z

    #@f
    .line 102
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCleanupAppWidgetsOnBootCompleted:Z

    #@11
    .line 120
    iput-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->halfWrongPasswordDialog:Landroid/app/AlertDialog;

    #@13
    .line 122
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mPersitentStickyWidgetLoaded:Z

    #@15
    .line 124
    new-instance v1, Landroid/graphics/Rect;

    #@17
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@1a
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@1c
    .line 283
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$1;

    #@1e
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@21
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@23
    .line 474
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$2;

    #@25
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@28
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@2a
    .line 534
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$3;

    #@2c
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@2f
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@31
    .line 979
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;

    #@33
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@36
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@38
    .line 1021
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$6;

    #@3a
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$6;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@3d
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mNullCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@3f
    .line 1373
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$7;

    #@41
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$7;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@44
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCameraWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$Callbacks;

    #@46
    .line 1403
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$8;

    #@48
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$8;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@4b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@4d
    .line 1658
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$11;

    #@4f
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$11;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@52
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSwitchPageRunnable:Ljava/lang/Runnable;

    #@54
    .line 2102
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicWidgetId:I

    #@56
    .line 2103
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicPosition:I

    #@58
    .line 2104
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@5a
    .line 2105
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremotePosition:I

    #@5c
    .line 166
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;

    #@5e
    invoke-direct {v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;-><init>(Landroid/content/Context;)V

    #@61
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetContext:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;

    #@63
    .line 168
    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    #@65
    invoke-direct {v1, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@68
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@6a
    .line 169
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@6c
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@6f
    move-result v1

    #@70
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserId:I

    #@72
    .line 172
    new-instance v1, Landroid/appwidget/AppWidgetHost;

    #@74
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetContext:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;

    #@76
    const v3, 0x4b455947

    #@79
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@7b
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@7e
    move-result-object v5

    #@7f
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;ILandroid/widget/RemoteViews$OnClickHandler;Landroid/os/Looper;)V

    #@82
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@84
    .line 177
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@86
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserId:I

    #@88
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetHost;->setUserId(I)V

    #@8b
    .line 178
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->cleanupAppWidgetIds()V

    #@8e
    .line 180
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@90
    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@93
    move-result-object v1

    #@94
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@96
    .line 181
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@98
    invoke-direct {v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;-><init>(Landroid/content/Context;)V

    #@9b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@9d
    .line 182
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@9f
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a2
    move-result-object v1

    #@a3
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a5
    .line 184
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@a7
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@aa
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@ac
    .line 186
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@ae
    const-string v2, "device_policy"

    #@b0
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b3
    move-result-object v0

    #@b4
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    #@b6
    .line 188
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    if-eqz v0, :cond_c4

    #@b8
    .line 189
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getDisabledFeatures(Landroid/app/admin/DevicePolicyManager;)I

    #@bb
    move-result v1

    #@bc
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDisabledFeatures:I

    #@be
    .line 190
    invoke-virtual {v0, v6}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;)Z

    #@c1
    move-result v1

    #@c2
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCameraDisabled:Z

    #@c4
    .line 193
    :cond_c4
    invoke-static {}, Lcom/android/internal/widget/LockPatternUtils;->isSafeModeEnabled()Z

    #@c7
    move-result v1

    #@c8
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@ca
    .line 197
    const/4 v1, 0x1

    #@cb
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserSetupCompleted:Z

    #@cd
    .line 200
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@cf
    if-eqz v1, :cond_d8

    #@d1
    .line 201
    const-string v1, "KeyguardHostView"

    #@d3
    const-string v2, "Keyguard widgets disabled by safe mode"

    #@d5
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 203
    :cond_d8
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDisabledFeatures:I

    #@da
    and-int/lit8 v1, v1, 0x1

    #@dc
    if-eqz v1, :cond_e5

    #@de
    .line 204
    const-string v1, "KeyguardHostView"

    #@e0
    const-string v2, "Keyguard widgets disabled by DPM"

    #@e2
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 206
    :cond_e5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDisabledFeatures:I

    #@e7
    and-int/lit8 v1, v1, 0x2

    #@e9
    if-eqz v1, :cond_f2

    #@eb
    .line 207
    const-string v1, "KeyguardHostView"

    #@ed
    const-string v2, "Keyguard secure camera disabled by DPM"

    #@ef
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    .line 209
    :cond_f2
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-static {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->isIgnoreSecurityPackage(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCheckAppWidgetConsistencyOnBootCompleted:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCheckAppWidgetConsistencyOnBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->reportFailedUnlockAttempt()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showBackupSecurityScreen()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showMDMDeletingDialog()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCleanupAppWidgetsOnBootCompleted:Z

    #@2
    return v0
.end method

.method static synthetic access$2000(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCleanupAppWidgetsOnBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->removeTransportFromWidgetPager()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2300(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showAppropriateWidgetPage()V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mNextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->cleanupAppWidgetIds()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->shouldEnableAddWidget()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/appwidget/AppWidgetHost;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showNextSecurityScreenOrFinish(Z)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mIsVerifyUnlockOnly:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private addCustomWidgetToWidgetPager(I)I
    .registers 9
    .parameter "id"

    #@0
    .prologue
    .line 2136
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@5
    move-result v4

    #@6
    add-int/lit8 v1, v4, -0x1

    #@8
    .line 2137
    .local v1, lastWidget:I
    const/4 v2, 0x0

    #@9
    .line 2138
    .local v2, position:I
    if-ltz v1, :cond_14

    #@b
    .line 2139
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@d
    invoke-virtual {v4, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isCameraPage(I)Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_46

    #@13
    move v2, v1

    #@14
    .line 2143
    :cond_14
    :goto_14
    if-eqz p1, :cond_49

    #@16
    .line 2144
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@18
    invoke-virtual {v4, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    #@1b
    move-result-object v0

    #@1c
    .line 2145
    .local v0, appWidgetInfo:Landroid/appwidget/AppWidgetProviderInfo;
    if-eqz v0, :cond_49

    #@1e
    .line 2146
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getAppWidgetHost()Landroid/appwidget/AppWidgetHost;

    #@21
    move-result-object v4

    #@22
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetContext:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;

    #@24
    invoke-virtual {v4, v5, p1, v0}, Landroid/appwidget/AppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    #@27
    move-result-object v3

    #@28
    .line 2147
    .local v3, view:Landroid/appwidget/AppWidgetHostView;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2a
    invoke-virtual {v4, v3, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addCustomWidget(Landroid/view/View;I)V

    #@2d
    .line 2148
    const-string v4, "KeyguardHostView"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "addCustomWidgetToWidgetPager: postion = "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 2152
    .end local v0           #appWidgetInfo:Landroid/appwidget/AppWidgetProviderInfo;
    .end local v2           #position:I
    .end local v3           #view:Landroid/appwidget/AppWidgetHostView;
    :goto_45
    return v2

    #@46
    .line 2139
    .restart local v2       #position:I
    :cond_46
    add-int/lit8 v2, v1, 0x1

    #@48
    goto :goto_14

    #@49
    .line 2152
    :cond_49
    const/4 v2, -0x1

    #@4a
    goto :goto_45
.end method

.method private addDefaultStatusWidget(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 1536
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@5
    move-result-object v0

    #@6
    .line 1537
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v2, 0x1090076

    #@9
    const/4 v3, 0x0

    #@a
    const/4 v4, 0x1

    #@b
    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@e
    move-result-object v1

    #@f
    .line 1538
    .local v1, statusWidget:Landroid/view/View;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@11
    invoke-virtual {v2, v1, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@14
    .line 1539
    return-void
.end method

.method private addDefaultWidgets()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1432
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@6
    move-result-object v3

    #@7
    .line 1433
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v4, 0x1090078

    #@a
    const/4 v5, 0x1

    #@b
    invoke-virtual {v3, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@e
    .line 1435
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@10
    if-nez v4, :cond_33

    #@12
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->widgetsDisabledByDpm()Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_33

    #@18
    .line 1436
    const v4, 0x1090052

    #@1b
    invoke-virtual {v3, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@1e
    move-result-object v0

    #@1f
    .line 1437
    .local v0, addWidget:Landroid/view/View;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@21
    invoke-virtual {v4, v0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@24
    .line 1438
    const v4, 0x10202b5

    #@27
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2a
    move-result-object v1

    #@2b
    .line 1439
    .local v1, addWidgetButton:Landroid/view/View;
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$9;

    #@2d
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$9;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@30
    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@33
    .line 1451
    .end local v0           #addWidget:Landroid/view/View;
    .end local v1           #addWidgetButton:Landroid/view/View;
    :cond_33
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@35
    if-nez v4, :cond_66

    #@37
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->cameraDisabledByDpm()Z

    #@3a
    move-result v4

    #@3b
    if-nez v4, :cond_66

    #@3d
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserSetupCompleted:Z

    #@3f
    if-eqz v4, :cond_66

    #@41
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->cameraDisabledByUser()Z

    #@44
    move-result v4

    #@45
    if-nez v4, :cond_66

    #@47
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@49
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4c
    move-result-object v4

    #@4d
    const/high16 v5, 0x111

    #@4f
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@52
    move-result v4

    #@53
    if-eqz v4, :cond_66

    #@55
    .line 1454
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@57
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCameraWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$Callbacks;

    #@59
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@5b
    invoke-static {v4, v5, v6}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->create(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$Callbacks;Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;)Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@5e
    move-result-object v2

    #@5f
    .line 1456
    .local v2, cameraWidget:Landroid/view/View;
    if-eqz v2, :cond_66

    #@61
    .line 1457
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@63
    invoke-virtual {v4, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;)V

    #@66
    .line 1461
    .end local v2           #cameraWidget:Landroid/view/View;
    :cond_66
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->enableUserSelectorIfNecessary()V

    #@69
    .line 1462
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->initializeTransportControl()V

    #@6c
    .line 1463
    return-void
.end method

.method private addTransportToWidgetPager()V
    .registers 5

    #@0
    .prologue
    .line 1480
    const v2, 0x1020319

    #@3
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getWidgetPosition(I)I

    #@6
    move-result v2

    #@7
    const/4 v3, -0x1

    #@8
    if-ne v2, v3, :cond_30

    #@a
    .line 1481
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@c
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->removeView(Landroid/view/View;)V

    #@f
    .line 1483
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@11
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@14
    move-result v2

    #@15
    add-int/lit8 v0, v2, -0x1

    #@17
    .line 1484
    .local v0, lastWidget:I
    const/4 v1, 0x0

    #@18
    .line 1485
    .local v1, position:I
    if-ltz v0, :cond_23

    #@1a
    .line 1486
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@1c
    invoke-virtual {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isCameraPage(I)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_31

    #@22
    move v1, v0

    #@23
    .line 1489
    :cond_23
    :goto_23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@25
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@27
    invoke-virtual {v2, v3, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2a
    .line 1490
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@2c
    const/4 v3, 0x0

    #@2d
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->setVisibility(I)V

    #@30
    .line 1492
    .end local v0           #lastWidget:I
    .end local v1           #position:I
    :cond_30
    return-void

    #@31
    .line 1486
    .restart local v0       #lastWidget:I
    .restart local v1       #position:I
    :cond_31
    add-int/lit8 v1, v0, 0x1

    #@33
    goto :goto_23
.end method

.method private addWidget(IIZ)Z
    .registers 9
    .parameter "appId"
    .parameter "pageIndex"
    .parameter "updateDbIfFailed"

    #@0
    .prologue
    .line 1356
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@2
    invoke-virtual {v2, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    #@5
    move-result-object v0

    #@6
    .line 1357
    .local v0, appWidgetInfo:Landroid/appwidget/AppWidgetProviderInfo;
    if-eqz v0, :cond_17

    #@8
    .line 1360
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getAppWidgetHost()Landroid/appwidget/AppWidgetHost;

    #@b
    move-result-object v2

    #@c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetContext:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$WidgetContext;

    #@e
    invoke-virtual {v2, v3, p1, v0}, Landroid/appwidget/AppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    #@11
    move-result-object v1

    #@12
    .line 1361
    .local v1, view:Landroid/appwidget/AppWidgetHostView;
    invoke-virtual {p0, v1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addWidget(Landroid/appwidget/AppWidgetHostView;I)V

    #@15
    .line 1362
    const/4 v2, 0x1

    #@16
    .line 1369
    .end local v1           #view:Landroid/appwidget/AppWidgetHostView;
    :goto_16
    return v2

    #@17
    .line 1364
    :cond_17
    if-eqz p3, :cond_41

    #@19
    .line 1365
    const-string v2, "KeyguardHostView"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "AppWidgetInfo for app widget id "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, " was null, deleting"

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1366
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@39
    invoke-virtual {v2, p1}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@3c
    .line 1367
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3e
    invoke-virtual {v2, p1}, Lcom/android/internal/widget/LockPatternUtils;->removeAppWidget(I)Z

    #@41
    .line 1369
    :cond_41
    const/4 v2, 0x0

    #@42
    goto :goto_16
.end method

.method private addWidgetsFromSettings()V
    .registers 6

    #@0
    .prologue
    .line 1542
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@2
    if-nez v3, :cond_a

    #@4
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->widgetsDisabledByDpm()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_b

    #@a
    .line 1565
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1546
    :cond_b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getInsertPageIndex()I

    #@e
    move-result v1

    #@f
    .line 1549
    .local v1, insertionIndex:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@11
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getAppWidgets()[I

    #@14
    move-result-object v2

    #@15
    .line 1551
    .local v2, widgets:[I
    if-nez v2, :cond_1f

    #@17
    .line 1552
    const-string v3, "KeyguardHostView"

    #@19
    const-string v4, "Problem reading widgets"

    #@1b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_a

    #@1f
    .line 1554
    :cond_1f
    array-length v3, v2

    #@20
    add-int/lit8 v0, v3, -0x1

    #@22
    .local v0, i:I
    :goto_22
    if-ltz v0, :cond_a

    #@24
    .line 1555
    aget v3, v2, v0

    #@26
    const/4 v4, -0x2

    #@27
    if-ne v3, v4, :cond_2f

    #@29
    .line 1556
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addDefaultStatusWidget(I)V

    #@2c
    .line 1554
    :goto_2c
    add-int/lit8 v0, v0, -0x1

    #@2e
    goto :goto_22

    #@2f
    .line 1561
    :cond_2f
    aget v3, v2, v0

    #@31
    const/4 v4, 0x1

    #@32
    invoke-direct {p0, v3, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addWidget(IIZ)Z

    #@35
    goto :goto_2c
.end method

.method private allocateIdCustomAppWidget(Ljava/lang/String;Ljava/lang/String;)I
    .registers 10
    .parameter "pkgName"
    .parameter "className"

    #@0
    .prologue
    .line 2109
    new-instance v1, Landroid/content/ComponentName;

    #@2
    invoke-direct {v1, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 2112
    .local v1, defaultAppWidget:Landroid/content/ComponentName;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@7
    invoke-virtual {v4}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    #@a
    move-result v0

    #@b
    .line 2113
    .local v0, appWidgetId:I
    new-instance v3, Landroid/os/Bundle;

    #@d
    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    #@10
    .line 2114
    .local v3, options:Landroid/os/Bundle;
    const-string v4, "appWidgetCategory"

    #@12
    const/4 v5, 0x2

    #@13
    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@16
    .line 2118
    :try_start_16
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@18
    invoke-virtual {v4, v0, v1, v3}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;Landroid/os/Bundle;)V
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_1b} :catch_1c

    #@1b
    .line 2125
    :goto_1b
    return v0

    #@1c
    .line 2120
    :catch_1c
    move-exception v2

    #@1d
    .line 2121
    .local v2, e:Ljava/lang/IllegalArgumentException;
    const-string v4, "KeyguardHostView"

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "Error when trying to bind custom AppWidget: "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 2122
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@37
    invoke-virtual {v4, v0}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@3a
    .line 2123
    const/4 v0, 0x0

    #@3b
    goto :goto_1b
.end method

.method private allocateIdForDefaultAppWidget()I
    .registers 9

    #@0
    .prologue
    .line 1569
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v4

    #@8
    .line 1572
    .local v4, res:Landroid/content/res/Resources;
    new-instance v2, Landroid/content/ComponentName;

    #@a
    const/high16 v5, 0x209

    #@c
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    const v6, 0x2090001

    #@13
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@16
    move-result-object v6

    #@17
    invoke-direct {v2, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 1576
    .local v2, defaultAppWidgetNexti:Landroid/content/ComponentName;
    new-instance v1, Landroid/content/ComponentName;

    #@1c
    const v5, 0x1040036

    #@1f
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    const v6, 0x1040037

    #@26
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1581
    .local v1, defaultAppWidget:Landroid/content/ComponentName;
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@2f
    invoke-virtual {v5}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    #@32
    move-result v0

    #@33
    .line 1584
    .local v0, appWidgetId:I
    :try_start_33
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@35
    if-eqz v5, :cond_3d

    #@37
    .line 1585
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@39
    invoke-virtual {v5, v0, v2}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;)V

    #@3c
    .line 1595
    :goto_3c
    return v0

    #@3d
    .line 1587
    :cond_3d
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    #@3f
    invoke-virtual {v5, v0, v1}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;)V
    :try_end_42
    .catch Ljava/lang/IllegalArgumentException; {:try_start_33 .. :try_end_42} :catch_43

    #@42
    goto :goto_3c

    #@43
    .line 1590
    :catch_43
    move-exception v3

    #@44
    .line 1591
    .local v3, e:Ljava/lang/IllegalArgumentException;
    const-string v5, "KeyguardHostView"

    #@46
    new-instance v6, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v7, "Error when trying to bind default AppWidget: "

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v6

    #@59
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 1592
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@5e
    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@61
    .line 1593
    const/4 v0, 0x0

    #@62
    goto :goto_3c
.end method

.method private cameraDisabledByDpm()Z
    .registers 2

    #@0
    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCameraDisabled:Z

    #@2
    if-nez v0, :cond_a

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDisabledFeatures:I

    #@6
    and-int/lit8 v0, v0, 0x2

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private cameraDisabledByUser()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 417
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string v3, "show_camera_widget"

    #@9
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v0

    #@d
    .line 419
    .local v0, user_selection:I
    if-eq v0, v1, :cond_10

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private cleanupAppWidgetIds()V
    .registers 9

    #@0
    .prologue
    .line 214
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@5
    move-result-object v5

    #@6
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->hasBootCompleted()Z

    #@9
    move-result v5

    #@a
    if-nez v5, :cond_10

    #@c
    .line 215
    const/4 v5, 0x1

    #@d
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCleanupAppWidgetsOnBootCompleted:Z

    #@f
    .line 243
    :cond_f
    return-void

    #@10
    .line 223
    :cond_10
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->getAppWidgets()[I

    #@15
    move-result-object v2

    #@16
    .line 224
    .local v2, appWidgetIdsInKeyguardSettings:[I
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@18
    invoke-virtual {v5}, Landroid/appwidget/AppWidgetHost;->getAppWidgetIds()[I

    #@1b
    move-result-object v1

    #@1c
    .line 225
    .local v1, appWidgetIdsBoundToHost:[I
    const/4 v4, 0x0

    #@1d
    .local v4, i:I
    :goto_1d
    array-length v5, v1

    #@1e
    if-ge v4, v5, :cond_f

    #@20
    .line 226
    aget v0, v1, v4

    #@22
    .line 227
    .local v0, appWidgetId:I
    invoke-static {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->contains([II)Z

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_6b

    #@28
    .line 228
    const-string v5, "KeyguardHostView"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Found a appWidgetId that\'s not being used by keyguard, deleting id "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 230
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@42
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isUsingQuickCoverWindow()Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_6e

    #@4c
    .line 231
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@4e
    if-eqz v5, :cond_6b

    #@50
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@53
    move-result-object v5

    #@54
    if-eqz v5, :cond_6b

    #@56
    .line 232
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getQuickCoverWidgetIds()[I

    #@5d
    move-result-object v3

    #@5e
    .line 233
    .local v3, appWidgetIdsInQuickCoverWindow:[I
    if-eqz v3, :cond_6b

    #@60
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->contains([II)Z

    #@63
    move-result v5

    #@64
    if-nez v5, :cond_6b

    #@66
    .line 235
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@68
    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@6b
    .line 225
    .end local v3           #appWidgetIdsInQuickCoverWindow:[I
    :cond_6b
    :goto_6b
    add-int/lit8 v4, v4, 0x1

    #@6d
    goto :goto_1d

    #@6e
    .line 239
    :cond_6e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@70
    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@73
    goto :goto_6b
.end method

.method private static contains([II)Z
    .registers 7
    .parameter "array"
    .parameter "target"

    #@0
    .prologue
    .line 246
    move-object v0, p0

    #@1
    .local v0, arr$:[I
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_e

    #@5
    aget v3, v0, v1

    #@7
    .line 247
    .local v3, value:I
    if-ne v3, p1, :cond_b

    #@9
    .line 248
    const/4 v4, 0x1

    #@a
    .line 251
    .end local v3           #value:I
    :goto_a
    return v4

    #@b
    .line 246
    .restart local v3       #value:I
    :cond_b
    add-int/lit8 v1, v1, 0x1

    #@d
    goto :goto_3

    #@e
    .line 251
    .end local v3           #value:I
    :cond_e
    const/4 v4, 0x0

    #@f
    goto :goto_a
.end method

.method private enableUserSelectorIfNecessary()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 1800
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    #@4
    move-result v6

    #@5
    if-nez v6, :cond_8

    #@7
    .line 1876
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1803
    :cond_8
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@a
    const-string v7, "user"

    #@c
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v4

    #@10
    check-cast v4, Landroid/os/UserManager;

    #@12
    .line 1804
    .local v4, um:Landroid/os/UserManager;
    if-nez v4, :cond_24

    #@14
    .line 1805
    new-instance v3, Ljava/lang/Throwable;

    #@16
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@19
    .line 1806
    .local v3, t:Ljava/lang/Throwable;
    invoke-virtual {v3}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    #@1c
    .line 1807
    const-string v6, "KeyguardHostView"

    #@1e
    const-string v7, "user service is null."

    #@20
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_7

    #@24
    .line 1812
    .end local v3           #t:Ljava/lang/Throwable;
    :cond_24
    invoke-virtual {v4, v8}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    #@27
    move-result-object v5

    #@28
    .line 1813
    .local v5, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-nez v5, :cond_3a

    #@2a
    .line 1814
    new-instance v3, Ljava/lang/Throwable;

    #@2c
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@2f
    .line 1815
    .restart local v3       #t:Ljava/lang/Throwable;
    invoke-virtual {v3}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    #@32
    .line 1816
    const-string v6, "KeyguardHostView"

    #@34
    const-string v7, "list of users is null."

    #@36
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_7

    #@3a
    .line 1820
    .end local v3           #t:Ljava/lang/Throwable;
    :cond_3a
    const v6, 0x10202ca

    #@3d
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@40
    move-result-object v2

    #@41
    .line 1821
    .local v2, multiUserView:Landroid/view/View;
    if-nez v2, :cond_53

    #@43
    .line 1822
    new-instance v3, Ljava/lang/Throwable;

    #@45
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@48
    .line 1823
    .restart local v3       #t:Ljava/lang/Throwable;
    invoke-virtual {v3}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    #@4b
    .line 1824
    const-string v6, "KeyguardHostView"

    #@4d
    const-string v7, "can\'t find user_selector in layout."

    #@4f
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@52
    goto :goto_7

    #@53
    .line 1828
    .end local v3           #t:Ljava/lang/Throwable;
    :cond_53
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@56
    move-result v6

    #@57
    if-le v6, v8, :cond_7

    #@59
    .line 1829
    instance-of v6, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;

    #@5b
    if-eqz v6, :cond_70

    #@5d
    move-object v1, v2

    #@5e
    .line 1830
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;

    #@60
    .line 1832
    .local v1, multiUser:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;
    const/4 v6, 0x0

    #@61
    invoke-virtual {v1, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;->setVisibility(I)V

    #@64
    .line 1833
    invoke-virtual {v1, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;->addUsers(Ljava/util/Collection;)V

    #@67
    .line 1834
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$12;

    #@69
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$12;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@6c
    .line 1865
    .local v0, callback:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$UserSwitcherCallback;
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;->setCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$UserSwitcherCallback;)V

    #@6f
    goto :goto_7

    #@70
    .line 1867
    .end local v0           #callback:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$UserSwitcherCallback;
    .end local v1           #multiUser:Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;
    :cond_70
    new-instance v3, Ljava/lang/Throwable;

    #@72
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@75
    .line 1868
    .restart local v3       #t:Ljava/lang/Throwable;
    invoke-virtual {v3}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    #@78
    .line 1869
    if-nez v2, :cond_82

    #@7a
    .line 1870
    const-string v6, "KeyguardHostView"

    #@7c
    const-string v7, "could not find the user_selector."

    #@7e
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    goto :goto_7

    #@82
    .line 1872
    :cond_82
    const-string v6, "KeyguardHostView"

    #@84
    const-string v7, "user_selector is the wrong type."

    #@86
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@89
    goto/16 :goto_7
.end method

.method private findCameraPage()Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;
    .registers 3

    #@0
    .prologue
    .line 1760
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_1e

    #@a
    .line 1761
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@c
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isCameraPage(I)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_1b

    #@12
    .line 1762
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@14
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@1a
    .line 1765
    :goto_1a
    return-object v1

    #@1b
    .line 1760
    :cond_1b
    add-int/lit8 v0, v0, -0x1

    #@1d
    goto :goto_8

    #@1e
    .line 1765
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1a
.end method

.method private getAppWidgetHost()Landroid/appwidget/AppWidgetHost;
    .registers 2

    #@0
    .prologue
    .line 467
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@2
    return-object v0
.end method

.method private getAppropriateWidgetPage(Z)I
    .registers 8
    .parameter "isMusicPlaying"

    #@0
    .prologue
    .line 1774
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@2
    if-eqz v3, :cond_22

    #@4
    .line 1775
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@6
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@9
    move-result v0

    #@a
    .line 1776
    .local v0, childCount:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_1f

    #@d
    .line 1777
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@f
    invoke-virtual {v3, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContentAppWidgetId()I

    #@16
    move-result v3

    #@17
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@19
    if-ne v3, v4, :cond_1c

    #@1b
    .line 1796
    .end local v0           #childCount:I
    .end local v1           #i:I
    :goto_1b
    return v1

    #@1c
    .line 1776
    .restart local v0       #childCount:I
    .restart local v1       #i:I
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_b

    #@1f
    .line 1782
    :cond_1f
    const/4 v3, 0x0

    #@20
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@22
    .line 1785
    .end local v0           #childCount:I
    .end local v1           #i:I
    :cond_22
    if-eqz p1, :cond_38

    #@24
    .line 1786
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@26
    if-eqz v3, :cond_2f

    #@28
    const-string v3, "KeyguardHostView"

    #@2a
    const-string v4, "Music playing, show transport"

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1787
    :cond_2f
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@31
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@33
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageIndex(Landroid/view/View;)I

    #@36
    move-result v1

    #@37
    goto :goto_1b

    #@38
    .line 1791
    :cond_38
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@3a
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@3d
    move-result v3

    #@3e
    add-int/lit8 v2, v3, -0x1

    #@40
    .line 1792
    .local v2, rightMost:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@42
    invoke-virtual {v3, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isCameraPage(I)Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_4a

    #@48
    .line 1793
    add-int/lit8 v2, v2, -0x1

    #@4a
    .line 1795
    :cond_4a
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@4c
    if-eqz v3, :cond_66

    #@4e
    const-string v3, "KeyguardHostView"

    #@50
    new-instance v4, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v5, "Show right-most page "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    :cond_66
    move v1, v2

    #@67
    .line 1796
    goto :goto_1b
.end method

.method private getDisabledFeatures(Landroid/app/admin/DevicePolicyManager;)I
    .registers 5
    .parameter "dpm"

    #@0
    .prologue
    .line 398
    const/4 v1, 0x0

    #@1
    .line 399
    .local v1, disabledFeatures:I
    if-eqz p1, :cond_e

    #@3
    .line 400
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@5
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@8
    move-result v0

    #@9
    .line 401
    .local v0, currentUser:I
    const/4 v2, 0x0

    #@a
    invoke-virtual {p1, v2, v0}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    #@d
    move-result v1

    #@e
    .line 403
    .end local v0           #currentUser:I
    :cond_e
    return v1
.end method

.method private getInsertPageIndex()I
    .registers 5

    #@0
    .prologue
    .line 1525
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    const v3, 0x10202b4

    #@5
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->findViewById(I)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    .line 1526
    .local v0, addWidget:Landroid/view/View;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@b
    invoke-virtual {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->indexOfChild(Landroid/view/View;)I

    #@e
    move-result v1

    #@f
    .line 1527
    .local v1, insertionIndex:I
    if-gez v1, :cond_13

    #@11
    .line 1528
    const/4 v1, 0x0

    #@12
    .line 1532
    :goto_12
    return v1

    #@13
    .line 1530
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_12
.end method

.method private getLayoutIdFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)I
    .registers 4
    .parameter "securityMode"

    #@0
    .prologue
    .line 1327
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_4a

    #@b
    .line 1351
    const/4 v0, 0x0

    #@c
    :goto_c
    return v0

    #@d
    .line 1329
    :pswitch_d
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@f
    if-eqz v0, :cond_15

    #@11
    .line 1330
    const v0, 0x2030011

    #@14
    goto :goto_c

    #@15
    .line 1332
    :cond_15
    const v0, 0x1090070

    #@18
    goto :goto_c

    #@19
    .line 1334
    :pswitch_19
    const v0, 0x1090060

    #@1c
    goto :goto_c

    #@1d
    .line 1335
    :pswitch_1d
    const v0, 0x1090061

    #@20
    goto :goto_c

    #@21
    .line 1336
    :pswitch_21
    const v0, 0x109005f

    #@24
    goto :goto_c

    #@25
    .line 1337
    :pswitch_25
    const v0, 0x1090055

    #@28
    goto :goto_c

    #@29
    .line 1338
    :pswitch_29
    const v0, 0x1090051

    #@2c
    goto :goto_c

    #@2d
    .line 1340
    :pswitch_2d
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@2f
    if-eqz v0, :cond_35

    #@31
    .line 1341
    const v0, 0x203000f

    #@34
    goto :goto_c

    #@35
    .line 1343
    :cond_35
    const v0, 0x203000c

    #@38
    goto :goto_c

    #@39
    .line 1345
    :pswitch_39
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@3b
    if-eqz v0, :cond_41

    #@3d
    .line 1346
    const v0, 0x2030010

    #@40
    goto :goto_c

    #@41
    .line 1348
    :cond_41
    const v0, 0x203000d

    #@44
    goto :goto_c

    #@45
    .line 1349
    :pswitch_45
    const v0, 0x203000b

    #@48
    goto :goto_c

    #@49
    .line 1327
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_19
        :pswitch_1d
        :pswitch_21
        :pswitch_29
        :pswitch_25
        :pswitch_2d
        :pswitch_39
        :pswitch_45
        :pswitch_d
    .end packed-switch
.end method

.method private getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    .registers 16
    .parameter "securityMode"

    #@0
    .prologue
    .line 1086
    const-string v11, "KeyguardHostView"

    #@2
    new-instance v12, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v13, "[kjj]getSecurityView_showSecurityScreen("

    #@9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v12

    #@d
    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v12

    #@11
    const-string v13, ")"

    #@13
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v12

    #@17
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v12

    #@1b
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1087
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityViewIdForMode(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)I

    #@21
    move-result v6

    #@22
    .line 1088
    .local v6, securityViewIdForMode:I
    const/4 v10, 0x0

    #@23
    .line 1089
    .local v10, view:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@25
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@28
    move-result v2

    #@29
    .line 1090
    .local v2, children:I
    const/4 v1, 0x0

    #@2a
    .local v1, child:I
    :goto_2a
    if-ge v1, v2, :cond_40

    #@2c
    .line 1091
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@2e
    invoke-virtual {v11, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@31
    move-result-object v11

    #@32
    invoke-virtual {v11}, Landroid/view/View;->getId()I

    #@35
    move-result v11

    #@36
    if-ne v11, v6, :cond_a7

    #@38
    .line 1092
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@3a
    invoke-virtual {v11, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@3d
    move-result-object v10

    #@3e
    .end local v10           #view:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    check-cast v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@40
    .line 1096
    .restart local v10       #view:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_40
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getLayoutIdFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)I

    #@43
    move-result v4

    #@44
    .line 1097
    .local v4, layoutId:I
    if-nez v10, :cond_91

    #@46
    if-eqz v4, :cond_91

    #@48
    .line 1098
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4a
    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@4d
    move-result-object v3

    #@4e
    .line 1099
    .local v3, inflater:Landroid/view/LayoutInflater;
    sget-boolean v11, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@50
    if-eqz v11, :cond_6a

    #@52
    const-string v11, "KeyguardHostView"

    #@54
    new-instance v12, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v13, "inflating id = "

    #@5b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v12

    #@5f
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v12

    #@63
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v12

    #@67
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 1100
    :cond_6a
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@6c
    const/4 v12, 0x0

    #@6d
    invoke-virtual {v3, v4, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@70
    move-result-object v8

    #@71
    .line 1101
    .local v8, v:Landroid/view/View;
    sget-boolean v11, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@73
    if-eqz v11, :cond_86

    #@75
    .line 1102
    const v11, 0x10202b6

    #@78
    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7b
    move-result-object v11

    #@7c
    check-cast v11, Landroid/view/ViewStub;

    #@7e
    move-object v9, v11

    #@7f
    check-cast v9, Landroid/view/ViewStub;

    #@81
    .line 1103
    .local v9, vStub:Landroid/view/ViewStub;
    if-eqz v9, :cond_86

    #@83
    .line 1104
    invoke-virtual {v9}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    #@86
    .line 1107
    .end local v9           #vStub:Landroid/view/ViewStub;
    :cond_86
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@88
    invoke-virtual {v11, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->addView(Landroid/view/View;)V

    #@8b
    .line 1108
    invoke-direct {p0, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->updateSecurityView(Landroid/view/View;)V

    #@8e
    move-object v10, v8

    #@8f
    .line 1109
    check-cast v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@91
    .line 1112
    .end local v3           #inflater:Landroid/view/LayoutInflater;
    .end local v8           #v:Landroid/view/View;
    :cond_91
    sget-boolean v11, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@93
    if-eqz v11, :cond_aa

    #@95
    .line 1113
    instance-of v11, v10, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@97
    if-eqz v11, :cond_a6

    #@99
    move-object v5, v10

    #@9a
    .line 1114
    check-cast v5, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@9c
    .line 1115
    .local v5, nextiselectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    const v11, 0x20d0067

    #@9f
    invoke-virtual {v5, v11}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@a2
    move-result-object v0

    #@a3
    .line 1116
    .local v0, carrierText:Landroid/view/View;
    invoke-virtual {v5, v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->setCarrierArea(Landroid/view/View;)V

    #@a6
    .line 1126
    .end local v0           #carrierText:Landroid/view/View;
    .end local v5           #nextiselectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;
    :cond_a6
    :goto_a6
    return-object v10

    #@a7
    .line 1090
    .end local v4           #layoutId:I
    :cond_a7
    add-int/lit8 v1, v1, 0x1

    #@a9
    goto :goto_2a

    #@aa
    .line 1119
    .restart local v4       #layoutId:I
    :cond_aa
    instance-of v11, v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

    #@ac
    if-eqz v11, :cond_a6

    #@ae
    move-object v7, v10

    #@af
    .line 1120
    check-cast v7, Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

    #@b1
    .line 1121
    .local v7, selectorView:Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;
    const v11, 0x10202bf

    #@b4
    invoke-virtual {v7, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;->findViewById(I)Landroid/view/View;

    #@b7
    move-result-object v0

    #@b8
    .line 1122
    .restart local v0       #carrierText:Landroid/view/View;
    invoke-virtual {v7, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;->setCarrierArea(Landroid/view/View;)V

    #@bb
    goto :goto_a6
.end method

.method private getSecurityViewIdForMode(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)I
    .registers 4
    .parameter "securityMode"

    #@0
    .prologue
    .line 1299
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_4a

    #@b
    .line 1323
    const/4 v0, 0x0

    #@c
    :goto_c
    return v0

    #@d
    .line 1301
    :pswitch_d
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@f
    if-eqz v0, :cond_15

    #@11
    .line 1302
    const v0, 0x20d005a

    #@14
    goto :goto_c

    #@15
    .line 1304
    :cond_15
    const v0, 0x102030b

    #@18
    goto :goto_c

    #@19
    .line 1306
    :pswitch_19
    const v0, 0x10202d0

    #@1c
    goto :goto_c

    #@1d
    .line 1307
    :pswitch_1d
    const v0, 0x10202d2

    #@20
    goto :goto_c

    #@21
    .line 1308
    :pswitch_21
    const v0, 0x10202cd

    #@24
    goto :goto_c

    #@25
    .line 1309
    :pswitch_25
    const v0, 0x10202ba

    #@28
    goto :goto_c

    #@29
    .line 1310
    :pswitch_29
    const v0, 0x10202b0

    #@2c
    goto :goto_c

    #@2d
    .line 1312
    :pswitch_2d
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@2f
    if-eqz v0, :cond_35

    #@31
    .line 1313
    const v0, 0x1020330

    #@34
    goto :goto_c

    #@35
    .line 1315
    :cond_35
    const v0, 0x102030d

    #@38
    goto :goto_c

    #@39
    .line 1317
    :pswitch_39
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@3b
    if-eqz v0, :cond_41

    #@3d
    .line 1318
    const v0, 0x1020331

    #@40
    goto :goto_c

    #@41
    .line 1320
    :cond_41
    const v0, 0x102030e

    #@44
    goto :goto_c

    #@45
    .line 1321
    :pswitch_45
    const v0, 0x20d0053

    #@48
    goto :goto_c

    #@49
    .line 1299
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_19
        :pswitch_1d
        :pswitch_21
        :pswitch_29
        :pswitch_25
        :pswitch_2d
        :pswitch_39
        :pswitch_45
        :pswitch_d
    .end packed-switch
.end method

.method private getWidgetPosition(I)I
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 328
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 329
    .local v0, children:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1d

    #@9
    .line 330
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@b
    invoke-virtual {v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    #@16
    move-result v2

    #@17
    if-ne v2, p1, :cond_1a

    #@19
    .line 334
    .end local v1           #i:I
    :goto_19
    return v1

    #@1a
    .line 329
    .restart local v1       #i:I
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_7

    #@1d
    .line 334
    :cond_1d
    const/4 v1, -0x1

    #@1e
    goto :goto_19
.end method

.method private initializeTransportControl()V
    .registers 3

    #@0
    .prologue
    .line 1495
    const v0, 0x1020319

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@9
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@b
    .line 1497
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@d
    const/16 v1, 0x8

    #@f
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->setVisibility(I)V

    #@12
    .line 1501
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@14
    if-eqz v0, :cond_20

    #@16
    .line 1502
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@18
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$10;

    #@1a
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$10;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@1d
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$TransportCallback;)V

    #@20
    .line 1522
    :cond_20
    return-void
.end method

.method private static isIgnoreSecurityPackage(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 273
    const-string v0, "com.android.calendar"

    #@3
    .line 274
    .local v0, CALENDAR_PACKAGE_NAME:Ljava/lang/String;
    const-string v1, "com.lge."

    #@5
    .line 276
    .local v1, LGE_PACKAGE_PREFIX:Ljava/lang/String;
    if-nez p0, :cond_8

    #@7
    .line 280
    :cond_7
    :goto_7
    return v2

    #@8
    :cond_8
    const-string v3, "com.lge."

    #@a
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_18

    #@10
    const-string v3, "com.android.calendar"

    #@12
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_7

    #@18
    :cond_18
    const/4 v2, 0x1

    #@19
    goto :goto_7
.end method

.method private isKrOTAStart()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    .line 2075
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v1

    #@b
    const-string v2, "skt_ota_usim_download"

    #@d
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v1

    #@11
    if-lez v1, :cond_4f

    #@13
    .line 2076
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@15
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@17
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_FINISH_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@19
    if-ne v1, v2, :cond_4e

    #@1b
    .line 2077
    const-string v1, "KeyguardHostView"

    #@1d
    const-string v2, "goToUnlockScreen == USIM_PERSO_FINISH_LOCK"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2078
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@24
    if-eqz v1, :cond_2d

    #@26
    .line 2079
    const-string v1, "KeyguardHostView"

    #@28
    const-string v2, "Call SKTUsimDownloadActivity after completing usim perso "

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2081
    :cond_2d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v1

    #@33
    const-string v2, "skt_ota_usim_download"

    #@35
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@38
    .line 2083
    new-instance v0, Landroid/content/Intent;

    #@3a
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@3d
    .line 2084
    .local v0, otaIntent:Landroid/content/Intent;
    const-string v1, "com.lge.ota"

    #@3f
    const-string v2, "com.lge.ota.SKTUsimDownloadActivity"

    #@41
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@44
    .line 2085
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@47
    .line 2086
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@4e
    .line 2100
    .end local v0           #otaIntent:Landroid/content/Intent;
    :cond_4e
    :goto_4e
    return-void

    #@4f
    .line 2088
    :cond_4f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@51
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isKTUSIMRegiRequires()Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_4e

    #@5b
    .line 2089
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@5d
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@5f
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_FINISH_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@61
    if-ne v1, v2, :cond_4e

    #@63
    .line 2090
    const-string v1, "KeyguardHostView"

    #@65
    const-string v2, "goToUnlockScreen == USIM_PERSO_FINISH_LOCK"

    #@67
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 2091
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@6c
    if-eqz v1, :cond_75

    #@6e
    .line 2092
    const-string v1, "KeyguardHostView"

    #@70
    const-string v2, "[OTA] startKTFOTA"

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 2094
    :cond_75
    new-instance v0, Landroid/content/Intent;

    #@77
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@7a
    .line 2095
    .restart local v0       #otaIntent:Landroid/content/Intent;
    const-string v1, "com.lge.ota"

    #@7c
    const-string v2, "com.lge.ota.KTNoUSIMActivityForLockScreen"

    #@7e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@81
    .line 2096
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@84
    .line 2097
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@8b
    goto :goto_4e
.end method

.method private isSecure()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1239
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@4
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@7
    move-result-object v0

    #@8
    .line 1240
    .local v0, mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@a
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@d
    move-result v4

    #@e
    aget v3, v3, v4

    #@10
    packed-switch v3, :pswitch_data_46

    #@13
    .line 1259
    :pswitch_13
    new-instance v1, Ljava/lang/IllegalStateException;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Unknown security mode "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1

    #@2c
    .line 1242
    :pswitch_2c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2e
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@31
    move-result v1

    #@32
    .line 1257
    :cond_32
    :goto_32
    :pswitch_32
    return v1

    #@33
    .line 1245
    :pswitch_33
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@35
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@38
    move-result v1

    #@39
    goto :goto_32

    #@3a
    .line 1253
    :pswitch_3a
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@3c
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@3e
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_DOING_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@40
    if-eq v3, v4, :cond_32

    #@42
    move v1, v2

    #@43
    goto :goto_32

    #@44
    :pswitch_44
    move v1, v2

    #@45
    .line 1257
    goto :goto_32

    #@46
    .line 1240
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_33
        :pswitch_33
        :pswitch_32
        :pswitch_13
        :pswitch_32
        :pswitch_32
        :pswitch_3a
        :pswitch_44
    .end packed-switch
.end method

.method private numWidgets()I
    .registers 5

    #@0
    .prologue
    .line 1421
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 1422
    .local v0, childCount:I
    const/4 v2, 0x0

    #@7
    .line 1423
    .local v2, widgetCount:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_17

    #@a
    .line 1424
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@c
    invoke-virtual {v3, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isWidgetPage(I)Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_14

    #@12
    .line 1425
    add-int/lit8 v2, v2, 0x1

    #@14
    .line 1423
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_8

    #@17
    .line 1428
    :cond_17
    return v2
.end method

.method private removeCustomWidget(II)Z
    .registers 4
    .parameter "appId"
    .parameter "pageIndex"

    #@0
    .prologue
    .line 2129
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v0, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->removeViewAt(I)V

    #@5
    .line 2130
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@7
    invoke-virtual {v0, p1}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@a
    .line 2131
    const/4 v0, 0x1

    #@b
    return v0
.end method

.method private removeTransportFromWidgetPager()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1466
    const v2, 0x1020319

    #@4
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getWidgetPosition(I)I

    #@7
    move-result v0

    #@8
    .line 1467
    .local v0, page:I
    const/4 v2, -0x1

    #@9
    if-eq v0, v2, :cond_24

    #@b
    .line 1468
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@d
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@f
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->removeWidget(Landroid/view/View;)V

    #@12
    .line 1471
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@14
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addView(Landroid/view/View;)V

    #@17
    .line 1472
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@19
    const/16 v3, 0x8

    #@1b
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->setVisibility(I)V

    #@1e
    .line 1473
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@20
    invoke-virtual {v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setTransportState(I)V

    #@23
    .line 1474
    const/4 v1, 0x1

    #@24
    .line 1476
    :cond_24
    return v1
.end method

.method private reportFailedUnlockAttempt()V
    .registers 15

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 754
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v5

    #@8
    .line 755
    .local v5, monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getFailedUnlockAttempts()I

    #@b
    move-result v11

    #@c
    add-int/lit8 v1, v11, 0x1

    #@e
    .line 757
    .local v1, failedAttempts:I
    sget-boolean v11, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@10
    if-eqz v11, :cond_2a

    #@12
    const-string v11, "KeyguardHostView"

    #@14
    new-instance v12, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v13, "reportFailedPatternAttempt: #"

    #@1b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v12

    #@1f
    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v12

    #@23
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v12

    #@27
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 759
    :cond_2a
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2c
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2f
    move-result-object v4

    #@30
    .line 760
    .local v4, mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v11, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@32
    if-ne v4, v11, :cond_b6

    #@34
    move v8, v9

    #@35
    .line 762
    .local v8, usingPattern:Z
    :goto_35
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@37
    invoke-virtual {v11}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3a
    move-result-object v11

    #@3b
    const/4 v12, 0x0

    #@3c
    iget-object v13, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3e
    invoke-virtual {v13}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@41
    move-result v13

    #@42
    invoke-virtual {v11, v12, v13}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    #@45
    move-result v2

    #@46
    .line 765
    .local v2, failedAttemptsBeforeWipe:I
    const/16 v0, 0xf

    #@48
    .line 768
    .local v0, failedAttemptWarning:I
    if-lez v2, :cond_b9

    #@4a
    sub-int v6, v2, v1

    #@4c
    .line 772
    .local v6, remainingBeforeWipe:I
    :goto_4c
    const/4 v7, 0x0

    #@4d
    .line 775
    .local v7, showTimeout:Z
    const/4 v3, 0x0

    #@4e
    .line 776
    .local v3, isSupportEASScenario:Z
    sget-boolean v11, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@50
    if-eqz v11, :cond_62

    #@52
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@55
    move-result-object v11

    #@56
    if-eqz v11, :cond_62

    #@58
    .line 777
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@5b
    move-result-object v11

    #@5c
    const-string v12, "config_feature_eas_scenario"

    #@5e
    invoke-virtual {v11, v12}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@61
    move-result v3

    #@62
    .line 780
    :cond_62
    sget-boolean v11, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@64
    if-eqz v11, :cond_88

    #@66
    .line 781
    const-string v11, "KeyguardHostView"

    #@68
    new-instance v12, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v13, "isSupportEASScenario= "

    #@6f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v12

    #@73
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@76
    move-result-object v12

    #@77
    const-string v13, " failedAttemptsBeforeWipe= #"

    #@79
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v12

    #@7d
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v12

    #@81
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v12

    #@85
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 784
    :cond_88
    if-eqz v3, :cond_f0

    #@8a
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@8c
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8f
    move-result-object v11

    #@90
    sget-object v12, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@92
    if-ne v11, v12, :cond_f0

    #@94
    if-lez v2, :cond_f0

    #@96
    .line 790
    div-int/lit8 v10, v2, 0x2

    #@98
    if-ne v10, v6, :cond_bd

    #@9a
    .line 792
    const-string v9, "KeyguardHostView"

    #@9c
    const-string v10, "unlock attempts remain half times; so the user should input confirm string!"

    #@9e
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 793
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@a3
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@a5
    invoke-virtual {p0, v9, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showHalfWrongPasswordDialog(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;Landroid/content/Context;)V

    #@a8
    .line 834
    :cond_a8
    :goto_a8
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->reportFailedUnlockAttempt()V

    #@ab
    .line 835
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@ad
    invoke-virtual {v9}, Lcom/android/internal/widget/LockPatternUtils;->reportFailedPasswordAttempt()V

    #@b0
    .line 836
    if-eqz v7, :cond_b5

    #@b2
    .line 837
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showTimeoutDialog()V

    #@b5
    .line 839
    :cond_b5
    return-void

    #@b6
    .end local v0           #failedAttemptWarning:I
    .end local v2           #failedAttemptsBeforeWipe:I
    .end local v3           #isSupportEASScenario:Z
    .end local v6           #remainingBeforeWipe:I
    .end local v7           #showTimeout:Z
    .end local v8           #usingPattern:Z
    :cond_b6
    move v8, v10

    #@b7
    .line 760
    goto/16 :goto_35

    #@b9
    .line 768
    .restart local v0       #failedAttemptWarning:I
    .restart local v2       #failedAttemptsBeforeWipe:I
    .restart local v8       #usingPattern:Z
    :cond_b9
    const v6, 0x7fffffff

    #@bc
    goto :goto_4c

    #@bd
    .line 794
    .restart local v3       #isSupportEASScenario:Z
    .restart local v6       #remainingBeforeWipe:I
    .restart local v7       #showTimeout:Z
    :cond_bd
    if-ne v6, v9, :cond_ca

    #@bf
    .line 796
    const-string v9, "KeyguardHostView"

    #@c1
    const-string v10, "unlock attempts remain only 1 time!"

    #@c3
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 797
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showAlmostAtWipeDialog()V

    #@c9
    goto :goto_a8

    #@ca
    .line 798
    :cond_ca
    if-nez v6, :cond_d7

    #@cc
    .line 800
    const-string v9, "KeyguardHostView"

    #@ce
    const-string v10, "Too many unlock attempts; device will be wiped!"

    #@d0
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d3
    .line 801
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showWipeDialog(I)V

    #@d6
    goto :goto_a8

    #@d7
    .line 803
    :cond_d7
    const-string v9, "KeyguardHostView"

    #@d9
    new-instance v10, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    const-string v11, "remainingBeforeWipe= #"

    #@e0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v10

    #@e4
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v10

    #@e8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v10

    #@ec
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto :goto_a8

    #@f0
    .line 807
    :cond_f0
    const/4 v11, 0x5

    #@f1
    if-ge v6, v11, :cond_104

    #@f3
    .line 812
    if-lez v6, :cond_f9

    #@f5
    .line 813
    invoke-direct {p0, v1, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showAlmostAtWipeDialog(II)V

    #@f8
    goto :goto_a8

    #@f9
    .line 816
    :cond_f9
    const-string v9, "KeyguardHostView"

    #@fb
    const-string v10, "Too many unlock attempts; device will be wiped!"

    #@fd
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@100
    .line 817
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showWipeDialog(I)V

    #@103
    goto :goto_a8

    #@104
    .line 820
    :cond_104
    rem-int/lit8 v11, v1, 0x5

    #@106
    if-nez v11, :cond_118

    #@108
    move v7, v9

    #@109
    .line 822
    :goto_109
    if-eqz v8, :cond_a8

    #@10b
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mEnableFallback:Z

    #@10d
    if-eqz v10, :cond_a8

    #@10f
    .line 823
    const/16 v10, 0xf

    #@111
    if-ne v1, v10, :cond_11a

    #@113
    .line 824
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showAlmostAtAccountLoginDialog()V

    #@116
    .line 825
    const/4 v7, 0x0

    #@117
    goto :goto_a8

    #@118
    :cond_118
    move v7, v10

    #@119
    .line 820
    goto :goto_109

    #@11a
    .line 826
    :cond_11a
    const/16 v10, 0x14

    #@11c
    if-lt v1, v10, :cond_a8

    #@11e
    .line 827
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@120
    invoke-virtual {v10, v9}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@123
    .line 828
    sget-object v9, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@125
    invoke-direct {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@128
    .line 830
    const/4 v7, 0x0

    #@129
    goto/16 :goto_a8
.end method

.method private shouldEnableAddWidget()Z
    .registers 3

    #@0
    .prologue
    .line 394
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->numWidgets()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x5

    #@5
    if-ge v0, v1, :cond_d

    #@7
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserSetupCompleted:Z

    #@9
    if-eqz v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method private shouldEnableMenuKey()Z
    .registers 7

    #@0
    .prologue
    .line 1893
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v3

    #@4
    .line 1894
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x1110025

    #@7
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v0

    #@b
    .line 1896
    .local v0, configDisabled:Z
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    #@e
    move-result v2

    #@f
    .line 1897
    .local v2, isTestHarness:Z
    new-instance v4, Ljava/io/File;

    #@11
    const-string v5, "/data/local/enable_menu_key"

    #@13
    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@19
    move-result v1

    #@1a
    .line 1898
    .local v1, fileOverride:Z
    if-eqz v0, :cond_20

    #@1c
    if-nez v2, :cond_20

    #@1e
    if-eqz v1, :cond_22

    #@20
    :cond_20
    const/4 v4, 0x1

    #@21
    :goto_21
    return v4

    #@22
    :cond_22
    const/4 v4, 0x0

    #@23
    goto :goto_21
.end method

.method private showAlmostAtAccountLoginDialog()V
    .registers 9

    #@0
    .prologue
    .line 745
    const/16 v2, 0x1e

    #@2
    .line 746
    .local v2, timeoutInSeconds:I
    const/16 v0, 0xf

    #@4
    .line 748
    .local v0, count:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@6
    const v4, 0x104056a

    #@9
    const/4 v5, 0x3

    #@a
    new-array v5, v5, [Ljava/lang/Object;

    #@c
    const/4 v6, 0x0

    #@d
    const/16 v7, 0xf

    #@f
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v7

    #@13
    aput-object v7, v5, v6

    #@15
    const/4 v6, 0x1

    #@16
    const/4 v7, 0x5

    #@17
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v7

    #@1b
    aput-object v7, v5, v6

    #@1d
    const/4 v6, 0x2

    #@1e
    const/16 v7, 0x1e

    #@20
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v7

    #@24
    aput-object v7, v5, v6

    #@26
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 750
    .local v1, message:Ljava/lang/String;
    const/4 v3, 0x0

    #@2b
    invoke-direct {p0, v3, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 751
    return-void
.end method

.method private showAlmostAtWipeDialog(II)V
    .registers 10
    .parameter "attempts"
    .parameter "remaining"

    #@0
    .prologue
    .line 686
    const/16 v1, 0x1e

    #@2
    .line 687
    .local v1, timeoutInSeconds:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4
    const v3, 0x1040568

    #@7
    const/4 v4, 0x2

    #@8
    new-array v4, v4, [Ljava/lang/Object;

    #@a
    const/4 v5, 0x0

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v6

    #@f
    aput-object v6, v4, v5

    #@11
    const/4 v5, 0x1

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 689
    .local v0, message:Ljava/lang/String;
    const/4 v2, 0x0

    #@1d
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 690
    return-void
.end method

.method private showAppropriateWidgetPage()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v5, 0x2

    #@2
    .line 1738
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@4
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->getTransportState()I

    #@7
    move-result v2

    #@8
    .line 1739
    .local v2, state:I
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTransportControl:Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardTransportControlView;->isMusicPlaying()Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_12

    #@10
    if-ne v2, v5, :cond_31

    #@12
    :cond_12
    move v0, v3

    #@13
    .line 1741
    .local v0, isMusicPlaying:Z
    :goto_13
    if-eqz v0, :cond_33

    #@15
    .line 1742
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@17
    invoke-virtual {v3, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setTransportState(I)V

    #@1a
    .line 1743
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addTransportToWidgetPager()V

    #@1d
    .line 1749
    :cond_1d
    :goto_1d
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@1f
    if-eqz v3, :cond_27

    #@21
    .line 1750
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showLgMusicWidgetPage()V

    #@24
    .line 1751
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showQremoteWidgetPage()V

    #@27
    .line 1755
    :cond_27
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getAppropriateWidgetPage(Z)I

    #@2a
    move-result v1

    #@2b
    .line 1756
    .local v1, pageToShow:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2d
    invoke-virtual {v3, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setCurrentPage(I)V

    #@30
    .line 1757
    return-void

    #@31
    .line 1739
    .end local v0           #isMusicPlaying:Z
    .end local v1           #pageToShow:I
    :cond_31
    const/4 v0, 0x0

    #@32
    goto :goto_13

    #@33
    .line 1744
    .restart local v0       #isMusicPlaying:Z
    :cond_33
    if-ne v2, v5, :cond_1d

    #@35
    .line 1745
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@37
    invoke-virtual {v4, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setTransportState(I)V

    #@3a
    goto :goto_1d
.end method

.method private showBackupSecurityScreen()V
    .registers 4

    #@0
    .prologue
    .line 880
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    const-string v1, "KeyguardHostView"

    #@6
    const-string v2, "showBackupSecurity()"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 881
    :cond_b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getBackupSecurityMode(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@12
    move-result-object v0

    #@13
    .line 882
    .local v0, backup:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@16
    .line 883
    return-void
.end method

.method private showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 650
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@e
    move-result-object v1

    #@f
    const v2, 0x104000a

    #@12
    const/4 v3, 0x0

    #@13
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1a
    move-result-object v0

    #@1b
    .line 655
    .local v0, dialog:Landroid/app/AlertDialog;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@1d
    instance-of v1, v1, Landroid/app/Activity;

    #@1f
    if-nez v1, :cond_2a

    #@21
    .line 656
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@24
    move-result-object v1

    #@25
    const/16 v2, 0x7d9

    #@27
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@2a
    .line 658
    :cond_2a
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2d
    .line 659
    return-void
.end method

.method private showLgMusicWidgetPage()V
    .registers 6

    #@0
    .prologue
    .line 2156
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isLgMusicplaying()Z

    #@9
    move-result v0

    #@a
    .line 2157
    .local v0, isLgMusicPlaying:Z
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@c
    if-eqz v2, :cond_32

    #@e
    .line 2158
    const-string v2, "KeyguardHostView"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "showLgMusicWidgetPage: isLgMusicPlaying = "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", mLgMusicWidgetId = "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicWidgetId:I

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 2161
    :cond_32
    if-eqz v0, :cond_5c

    #@34
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicWidgetId:I

    #@36
    if-nez v2, :cond_5c

    #@38
    .line 2162
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3f
    move-result-object v1

    #@40
    .line 2163
    .local v1, res:Landroid/content/res/Resources;
    const v2, 0x2090002

    #@43
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    const v3, 0x2090003

    #@4a
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->allocateIdCustomAppWidget(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    move-result v2

    #@52
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicWidgetId:I

    #@54
    .line 2165
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicWidgetId:I

    #@56
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addCustomWidgetToWidgetPager(I)I

    #@59
    move-result v2

    #@5a
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mLgMusicPosition:I

    #@5c
    .line 2167
    .end local v1           #res:Landroid/content/res/Resources;
    :cond_5c
    return-void
.end method

.method private showMDMDeletingDialog()V
    .registers 5

    #@0
    .prologue
    const v3, 0x10403ea

    #@3
    .line 735
    new-instance v0, Landroid/app/ProgressDialog;

    #@5
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@7
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@a
    .line 736
    .local v0, dialog:Landroid/app/ProgressDialog;
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@d
    move-result-object v1

    #@e
    const/16 v2, 0x7d9

    #@10
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@13
    .line 737
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@1c
    .line 738
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@25
    .line 739
    const/4 v1, 0x0

    #@26
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@29
    .line 740
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    #@2c
    .line 741
    return-void
.end method

.method private showMDMDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 717
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@e
    move-result-object v1

    #@f
    const v2, 0x104000a

    #@12
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$4;

    #@14
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$4;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@17
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1e
    move-result-object v0

    #@1f
    .line 727
    .local v0, dialog:Landroid/app/AlertDialog;
    const/4 v1, 0x0

    #@20
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    #@23
    .line 728
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@26
    move-result-object v1

    #@27
    const/16 v2, 0x7d9

    #@29
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@2c
    .line 729
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2f
    .line 730
    return-void
.end method

.method private showNextSecurityScreenOrFinish(Z)V
    .registers 10
    .parameter "authenticated"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 898
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@4
    if-eqz v3, :cond_24

    #@6
    const-string v3, "KeyguardHostView"

    #@8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v5, "showNextSecurityScreenOrFinish("

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    const-string v5, ")"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 899
    :cond_24
    const/4 v1, 0x0

    #@25
    .line 900
    .local v1, finish:Z
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@27
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@29
    if-ne v3, v4, :cond_8d

    #@2b
    .line 901
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2d
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@30
    move-result-object v2

    #@31
    .line 903
    .local v2, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@33
    invoke-virtual {v3, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getAlternateFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@36
    move-result-object v2

    #@37
    .line 904
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@39
    if-ne v3, v2, :cond_89

    #@3b
    .line 905
    const/4 v1, 0x1

    #@3c
    .line 947
    .end local v2           #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :goto_3c
    if-eqz v1, :cond_115

    #@3e
    .line 950
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@40
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@47
    .line 953
    const-string v3, "KR"

    #@49
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->country:Ljava/lang/String;

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v3

    #@4f
    if-eqz v3, :cond_6f

    #@51
    const-string v3, "SKT"

    #@53
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->carrier:Ljava/lang/String;

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v3

    #@59
    if-nez v3, :cond_65

    #@5b
    const-string v3, "KT"

    #@5d
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->carrier:Ljava/lang/String;

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v3

    #@63
    if-eqz v3, :cond_6f

    #@65
    .line 955
    :cond_65
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->isKrOTAStart()V

    #@68
    .line 956
    const-string v3, "KeyguardHostView"

    #@6a
    const-string v4, "isKrOTAStart();"

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 962
    :cond_6f
    const/4 v0, 0x0

    #@70
    .line 963
    .local v0, deferKeyguardDone:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDismissAction:Lcom/android/internal/policy/impl/keyguard/OnDismissAction;

    #@72
    if-eqz v3, :cond_7d

    #@74
    .line 964
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDismissAction:Lcom/android/internal/policy/impl/keyguard/OnDismissAction;

    #@76
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard/OnDismissAction;->onDismiss()Z

    #@79
    move-result v0

    #@7a
    .line 965
    const/4 v3, 0x0

    #@7b
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDismissAction:Lcom/android/internal/policy/impl/keyguard/OnDismissAction;

    #@7d
    .line 967
    :cond_7d
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@7f
    if-eqz v3, :cond_88

    #@81
    .line 968
    if-eqz v0, :cond_10e

    #@83
    .line 969
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@85
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDonePending()V

    #@88
    .line 977
    .end local v0           #deferKeyguardDone:Z
    :cond_88
    :goto_88
    return-void

    #@89
    .line 907
    .restart local v2       #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_89
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@8c
    goto :goto_3c

    #@8d
    .line 909
    .end local v2           #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_8d
    if-eqz p1, :cond_109

    #@8f
    .line 910
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@91
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@93
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@96
    move-result v4

    #@97
    aget v3, v3, v4

    #@99
    packed-switch v3, :pswitch_data_11c

    #@9c
    .line 940
    const-string v3, "KeyguardHostView"

    #@9e
    new-instance v4, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v5, "Bad security screen "

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    const-string v5, ", fail safe"

    #@b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v4

    #@b9
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 941
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@bf
    goto/16 :goto_3c

    #@c1
    .line 916
    :pswitch_c1
    const/4 v1, 0x1

    #@c2
    .line 917
    goto/16 :goto_3c

    #@c4
    .line 923
    :pswitch_c4
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@c6
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@c9
    move-result-object v2

    #@ca
    .line 924
    .restart local v2       #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@cc
    if-eq v2, v3, :cond_106

    #@ce
    .line 926
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d0
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d2
    if-ne v3, v4, :cond_101

    #@d4
    const-string v3, "KR"

    #@d6
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->country:Ljava/lang/String;

    #@d8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@db
    move-result v3

    #@dc
    if-eqz v3, :cond_101

    #@de
    const-string v3, "SKT"

    #@e0
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->carrier:Ljava/lang/String;

    #@e2
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v3

    #@e6
    if-nez v3, :cond_f2

    #@e8
    const-string v3, "KT"

    #@ea
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->carrier:Ljava/lang/String;

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v3

    #@f0
    if-eqz v3, :cond_101

    #@f2
    .line 927
    :cond_f2
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@f5
    .line 928
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->isKrOTAStart()V

    #@f8
    .line 929
    const-string v3, "KeyguardHostView"

    #@fa
    const-string v4, "isKrOTAStart();"

    #@fc
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    goto/16 :goto_3c

    #@101
    .line 932
    :cond_101
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@104
    goto/16 :goto_3c

    #@106
    .line 935
    :cond_106
    const/4 v1, 0x1

    #@107
    .line 937
    goto/16 :goto_3c

    #@109
    .line 945
    .end local v2           #securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_109
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@10c
    goto/16 :goto_3c

    #@10e
    .line 971
    .restart local v0       #deferKeyguardDone:Z
    :cond_10e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@110
    invoke-interface {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDone(Z)V

    #@113
    goto/16 :goto_88

    #@115
    .line 975
    .end local v0           #deferKeyguardDone:Z
    :cond_115
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@117
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->showBouncer(Z)V

    #@11a
    goto/16 :goto_88

    #@11c
    .line 910
    :pswitch_data_11c
    .packed-switch 0x1
        :pswitch_c1
        :pswitch_c1
        :pswitch_c1
        :pswitch_c1
        :pswitch_c1
        :pswitch_c4
        :pswitch_c4
        :pswitch_c4
    .end packed-switch
.end method

.method private showQremoteWidgetPage()V
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 2170
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@3
    if-eqz v1, :cond_27

    #@5
    .line 2171
    const-string v1, "KeyguardHostView"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "showQremoteWidgetPage: isQremote = "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@14
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected()Z

    #@1b
    move-result v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 2173
    :cond_27
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@29
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected()Z

    #@30
    move-result v1

    #@31
    if-eqz v1, :cond_5c

    #@33
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@35
    if-nez v1, :cond_5c

    #@37
    .line 2175
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getContext()Landroid/content/Context;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v0

    #@3f
    .line 2176
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x2090004

    #@42
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    const v2, 0x2090005

    #@49
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-direct {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->allocateIdCustomAppWidget(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    move-result v1

    #@51
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@53
    .line 2178
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@55
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addCustomWidgetToWidgetPager(I)I

    #@58
    move-result v1

    #@59
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremotePosition:I

    #@5b
    .line 2184
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_5b
    :goto_5b
    return-void

    #@5c
    .line 2179
    :cond_5c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@5e
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isHomeWifiConnected()Z

    #@65
    move-result v1

    #@66
    if-nez v1, :cond_5b

    #@68
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@6a
    if-eqz v1, :cond_5b

    #@6c
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremotePosition:I

    #@6e
    if-eq v1, v4, :cond_5b

    #@70
    .line 2180
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@72
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremotePosition:I

    #@74
    invoke-direct {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->removeCustomWidget(II)Z

    #@77
    .line 2181
    const/4 v1, 0x0

    #@78
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremoteId:I

    #@7a
    .line 2182
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mQremotePosition:I

    #@7c
    goto :goto_5b
.end method

.method private showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V
    .registers 15
    .parameter "securityMode"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 1136
    const-string v10, "KeyguardHostView"

    #@4
    new-instance v11, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v12, "[kjj]showSecurityScreen("

    #@b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v11

    #@f
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v11

    #@13
    const-string v12, ")"

    #@15
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v11

    #@19
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v11

    #@1d
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1138
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@22
    if-ne p1, v10, :cond_25

    #@24
    .line 1189
    :goto_24
    return-void

    #@25
    .line 1140
    :cond_25
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@27
    invoke-direct {p0, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@2a
    move-result-object v6

    #@2b
    .line 1141
    .local v6, oldView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@2e
    move-result-object v5

    #@2f
    .line 1144
    .local v5, newView:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getResources()Landroid/content/res/Resources;

    #@32
    move-result-object v10

    #@33
    const v11, 0x111000c

    #@36
    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@39
    move-result v1

    #@3a
    .line 1146
    .local v1, fullScreenEnabled:Z
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@3c
    if-eq p1, v10, :cond_46

    #@3e
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@40
    if-eq p1, v10, :cond_46

    #@42
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@44
    if-ne p1, v10, :cond_c3

    #@46
    :cond_46
    move v3, v9

    #@47
    .line 1149
    .local v3, isSimOrAccount:Z
    :goto_47
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@49
    if-eqz v3, :cond_c5

    #@4b
    if-eqz v1, :cond_c5

    #@4d
    const/16 v10, 0x8

    #@4f
    :goto_4f
    invoke-virtual {v11, v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setVisibility(I)V

    #@52
    .line 1152
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@54
    if-eqz v10, :cond_5d

    #@56
    .line 1153
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@58
    if-nez v1, :cond_c7

    #@5a
    :goto_5a
    invoke-virtual {v10, v9}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setChallengeInteractive(Z)V

    #@5d
    .line 1157
    :cond_5d
    if-eqz v6, :cond_67

    #@5f
    .line 1158
    invoke-interface {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onPause()V

    #@62
    .line 1159
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mNullCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@64
    invoke-interface {v6, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V

    #@67
    .line 1161
    :cond_67
    const/4 v8, 0x2

    #@68
    invoke-interface {v5, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onResume(I)V

    #@6b
    .line 1162
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@6d
    invoke-interface {v5, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V

    #@70
    .line 1164
    invoke-interface {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->needsInput()Z

    #@73
    move-result v4

    #@74
    .line 1165
    .local v4, needsInput:Z
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@76
    if-eqz v8, :cond_7d

    #@78
    .line 1166
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@7a
    invoke-interface {v8, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNeedsInput(Z)V

    #@7d
    .line 1170
    :cond_7d
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@7f
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@82
    move-result v0

    #@83
    .line 1172
    .local v0, childCount:I
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@85
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@87
    const v10, 0x10a002b

    #@8a
    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@8d
    move-result-object v9

    #@8e
    invoke-virtual {v8, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    #@91
    .line 1174
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@93
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@95
    const v10, 0x10a002c

    #@98
    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v8, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    #@9f
    .line 1176
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityViewIdForMode(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)I

    #@a2
    move-result v7

    #@a3
    .line 1177
    .local v7, securityViewIdForMode:I
    const/4 v2, 0x0

    #@a4
    .local v2, i:I
    :goto_a4
    if-ge v2, v0, :cond_b7

    #@a6
    .line 1178
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@a8
    invoke-virtual {v8, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@ab
    move-result-object v8

    #@ac
    invoke-virtual {v8}, Landroid/view/View;->getId()I

    #@af
    move-result v8

    #@b0
    if-ne v8, v7, :cond_c9

    #@b2
    .line 1179
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@b4
    invoke-virtual {v8, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->setDisplayedChild(I)V

    #@b7
    .line 1184
    :cond_b7
    sget-object v8, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@b9
    if-ne p1, v8, :cond_bf

    #@bb
    .line 1186
    const/4 v8, 0x0

    #@bc
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->setOnDismissAction(Lcom/android/internal/policy/impl/keyguard/OnDismissAction;)V

    #@bf
    .line 1188
    :cond_bf
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@c1
    goto/16 :goto_24

    #@c3
    .end local v0           #childCount:I
    .end local v2           #i:I
    .end local v3           #isSimOrAccount:Z
    .end local v4           #needsInput:Z
    .end local v7           #securityViewIdForMode:I
    :cond_c3
    move v3, v8

    #@c4
    .line 1146
    goto :goto_47

    #@c5
    .restart local v3       #isSimOrAccount:Z
    :cond_c5
    move v10, v8

    #@c6
    .line 1149
    goto :goto_4f

    #@c7
    :cond_c7
    move v9, v8

    #@c8
    .line 1153
    goto :goto_5a

    #@c9
    .line 1177
    .restart local v0       #childCount:I
    .restart local v2       #i:I
    .restart local v4       #needsInput:Z
    .restart local v7       #securityViewIdForMode:I
    :cond_c9
    add-int/lit8 v2, v2, 0x1

    #@cb
    goto :goto_a4
.end method

.method private showTimeoutDialog()V
    .registers 8

    #@0
    .prologue
    .line 662
    const/16 v2, 0x1e

    #@2
    .line 663
    .local v2, timeoutInSeconds:I
    const/4 v1, 0x0

    #@3
    .line 665
    .local v1, messageId:I
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$18;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@5
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@7
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@a
    move-result-object v4

    #@b
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@e
    move-result v4

    #@f
    aget v3, v3, v4

    #@11
    packed-switch v3, :pswitch_data_48

    #@14
    .line 677
    :goto_14
    if-eqz v1, :cond_3b

    #@16
    .line 678
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@18
    const/4 v4, 0x2

    #@19
    new-array v4, v4, [Ljava/lang/Object;

    #@1b
    const/4 v5, 0x0

    #@1c
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@1e
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getFailedUnlockAttempts()I

    #@25
    move-result v6

    #@26
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v6

    #@2a
    aput-object v6, v4, v5

    #@2c
    const/4 v5, 0x1

    #@2d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v6

    #@31
    aput-object v6, v4, v5

    #@33
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 681
    .local v0, message:Ljava/lang/String;
    const/4 v3, 0x0

    #@38
    invoke-direct {p0, v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 683
    .end local v0           #message:Ljava/lang/String;
    :cond_3b
    return-void

    #@3c
    .line 667
    :pswitch_3c
    const v1, 0x1040567

    #@3f
    .line 668
    goto :goto_14

    #@40
    .line 670
    :pswitch_40
    const v1, 0x1040565

    #@43
    .line 671
    goto :goto_14

    #@44
    .line 673
    :pswitch_44
    const v1, 0x1040566

    #@47
    goto :goto_14

    #@48
    .line 665
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_40
        :pswitch_44
    .end packed-switch
.end method

.method private showWipeDialog(I)V
    .registers 9
    .parameter "attempts"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 701
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@3
    const v2, 0x1040569

    #@6
    const/4 v3, 0x1

    #@7
    new-array v3, v3, [Ljava/lang/Object;

    #@9
    const/4 v4, 0x0

    #@a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v5

    #@e
    aput-object v5, v3, v4

    #@10
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 703
    .local v0, message:Ljava/lang/String;
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@16
    if-eqz v1, :cond_24

    #@18
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1a
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_24

    #@20
    .line 705
    invoke-direct {p0, v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showMDMDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 713
    :goto_23
    return-void

    #@24
    .line 707
    :cond_24
    invoke-direct {p0, v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    goto :goto_23
.end method

.method private updateSecurityView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 430
    instance-of v1, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3
    if-eqz v1, :cond_22

    #@5
    move-object v0, p1

    #@6
    .line 431
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@8
    .line 432
    .local v0, ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@a
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V

    #@d
    .line 433
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@12
    .line 434
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@14
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->isBouncing()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1e

    #@1a
    .line 435
    invoke-interface {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->showBouncer(I)V

    #@1d
    .line 442
    .end local v0           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :goto_1d
    return-void

    #@1e
    .line 437
    .restart local v0       #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_1e
    invoke-interface {v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->hideBouncer(I)V

    #@21
    goto :goto_1d

    #@22
    .line 440
    .end local v0           #ksv:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
    :cond_22
    const-string v1, "KeyguardHostView"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "View "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, " is not a KeyguardSecurityView"

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_1d
.end method

.method private updateSecurityViews()V
    .registers 4

    #@0
    .prologue
    .line 423
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 424
    .local v0, children:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 425
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@b
    invoke-virtual {v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->updateSecurityView(Landroid/view/View;)V

    #@12
    .line 424
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_7

    #@15
    .line 427
    :cond_15
    return-void
.end method

.method private widgetsDisabledByDpm()Z
    .registers 2

    #@0
    .prologue
    .line 407
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDisabledFeatures:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method


# virtual methods
.method addWidget(Landroid/appwidget/AppWidgetHostView;I)V
    .registers 4
    .parameter "view"
    .parameter "pageIndex"

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@5
    .line 472
    return-void
.end method

.method public checkAppWidgetConsistency()V
    .registers 11

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 1600
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v9

    #@8
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->hasBootCompleted()Z

    #@b
    move-result v9

    #@c
    if-nez v9, :cond_11

    #@e
    .line 1601
    iput-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCheckAppWidgetConsistencyOnBootCompleted:Z

    #@10
    .line 1656
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1604
    :cond_11
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@13
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@16
    move-result v2

    #@17
    .line 1605
    .local v2, childCount:I
    const/4 v6, 0x0

    #@18
    .line 1606
    .local v6, widgetPageExists:Z
    const/4 v3, 0x0

    #@19
    .local v3, i:I
    :goto_19
    if-ge v3, v2, :cond_24

    #@1b
    .line 1607
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@1d
    invoke-virtual {v9, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isWidgetPage(I)Z

    #@20
    move-result v9

    #@21
    if-eqz v9, :cond_59

    #@23
    .line 1608
    const/4 v6, 0x1

    #@24
    .line 1612
    :cond_24
    if-nez v6, :cond_10

    #@26
    .line 1613
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getInsertPageIndex()I

    #@29
    move-result v4

    #@2a
    .line 1615
    .local v4, insertPageIndex:I
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->widgetsDisabledByDpm()Z

    #@2d
    move-result v9

    #@2e
    if-nez v9, :cond_5c

    #@30
    move v5, v7

    #@31
    .line 1616
    .local v5, userAddedWidgetsEnabled:Z
    :goto_31
    const/4 v0, 0x0

    #@32
    .line 1618
    .local v0, addedDefaultAppWidget:Z
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@34
    if-nez v9, :cond_42

    #@36
    .line 1619
    if-eqz v5, :cond_5e

    #@38
    .line 1620
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->allocateIdForDefaultAppWidget()I

    #@3b
    move-result v1

    #@3c
    .line 1621
    .local v1, appWidgetId:I
    if-eqz v1, :cond_42

    #@3e
    .line 1622
    invoke-direct {p0, v1, v4, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addWidget(IIZ)Z

    #@41
    move-result v0

    #@42
    .line 1646
    .end local v1           #appWidgetId:I
    :cond_42
    :goto_42
    if-nez v0, :cond_47

    #@44
    .line 1647
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addDefaultStatusWidget(I)V

    #@47
    .line 1651
    :cond_47
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSafeModeEnabled:Z

    #@49
    if-nez v7, :cond_10

    #@4b
    if-eqz v5, :cond_10

    #@4d
    .line 1652
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@4f
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@51
    invoke-virtual {v8, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@54
    move-result-object v8

    #@55
    invoke-virtual {v7, v8, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->onAddView(Landroid/view/View;I)V

    #@58
    goto :goto_10

    #@59
    .line 1606
    .end local v0           #addedDefaultAppWidget:Z
    .end local v4           #insertPageIndex:I
    .end local v5           #userAddedWidgetsEnabled:Z
    :cond_59
    add-int/lit8 v3, v3, 0x1

    #@5b
    goto :goto_19

    #@5c
    .restart local v4       #insertPageIndex:I
    :cond_5c
    move v5, v8

    #@5d
    .line 1615
    goto :goto_31

    #@5e
    .line 1627
    .restart local v0       #addedDefaultAppWidget:Z
    .restart local v5       #userAddedWidgetsEnabled:Z
    :cond_5e
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@60
    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternUtils;->getFallbackAppWidgetId()I

    #@63
    move-result v1

    #@64
    .line 1628
    .restart local v1       #appWidgetId:I
    if-nez v1, :cond_71

    #@66
    .line 1629
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->allocateIdForDefaultAppWidget()I

    #@69
    move-result v1

    #@6a
    .line 1630
    if-eqz v1, :cond_71

    #@6c
    .line 1631
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@6e
    invoke-virtual {v7, v1}, Lcom/android/internal/widget/LockPatternUtils;->writeFallbackAppWidgetId(I)V

    #@71
    .line 1634
    :cond_71
    if-eqz v1, :cond_42

    #@73
    .line 1635
    invoke-direct {p0, v1, v4, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addWidget(IIZ)Z

    #@76
    move-result v0

    #@77
    .line 1636
    if-nez v0, :cond_42

    #@79
    .line 1637
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@7b
    invoke-virtual {v7, v1}, Landroid/appwidget/AppWidgetHost;->deleteAppWidgetId(I)V

    #@7e
    .line 1638
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@80
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/LockPatternUtils;->writeFallbackAppWidgetId(I)V

    #@83
    goto :goto_42
.end method

.method public cleanUp()V
    .registers 4

    #@0
    .prologue
    .line 1880
    const-string v0, "KeyguardHostView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "hostView cleanUp() state1:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1881
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1c
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@1f
    move-result-object v0

    #@20
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onPause()V

    #@23
    .line 1882
    return-void
.end method

.method public clearAppWidgetToShow()V
    .registers 2

    #@0
    .prologue
    .line 1229
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@3
    .line 1230
    return-void
.end method

.method public clearLockPattern()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 616
    const-string v3, "KeyguardHostView"

    #@4
    const-string v4, "clearLockPattern()"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 617
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@b
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_21

    #@11
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@13
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_21

    #@19
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1b
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isOMADM()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_2f

    #@21
    :cond_21
    move v0, v2

    #@22
    .line 619
    .local v0, isLockEnabled:Z
    :goto_22
    if-eqz v0, :cond_2e

    #@24
    .line 621
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@26
    invoke-virtual {v3, v1}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@29
    .line 622
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2b
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Z)V

    #@2e
    .line 624
    :cond_2e
    return-void

    #@2f
    .end local v0           #isLockEnabled:Z
    :cond_2f
    move v0, v1

    #@30
    .line 617
    goto :goto_22
.end method

.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 1933
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showNextSecurityScreenOrFinish(Z)V

    #@4
    .line 1934
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 321
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 322
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 323
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@9
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDoneDrawing()V

    #@c
    .line 325
    :cond_c
    return-void
.end method

.method public expirePasswordReset()V
    .registers 3

    #@0
    .prologue
    .line 1954
    const-string v0, "LGMDM"

    #@2
    const-string v1, "password expired, and password change, and lockscreen reset"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1956
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showNextSecurityScreenOrFinish(Z)V

    #@b
    .line 1957
    return-void
.end method

.method public getUserActivityTimeout()J
    .registers 3

    #@0
    .prologue
    .line 528
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 529
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getUserActivityTimeout()J

    #@9
    move-result-wide v0

    #@a
    .line 531
    :goto_a
    return-wide v0

    #@b
    :cond_b
    const-wide/16 v0, -0x1

    #@d
    goto :goto_a
.end method

.method public goToUserSwitcher()V
    .registers 3

    #@0
    .prologue
    .line 1904
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@2
    const v1, 0x10202cc

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getWidgetPosition(I)I

    #@8
    move-result v1

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setCurrentPage(I)V

    #@c
    .line 1905
    return-void
.end method

.method public goToWidget(I)V
    .registers 3
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 1908
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@2
    .line 1909
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSwitchPageRunnable:Ljava/lang/Runnable;

    #@4
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@7
    .line 1910
    return-void
.end method

.method public handleBackKey()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1922
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@3
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@5
    if-eq v1, v2, :cond_d

    #@7
    .line 1923
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@9
    invoke-interface {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@c
    .line 1924
    const/4 v0, 0x1

    #@d
    .line 1926
    :cond_d
    return v0
.end method

.method public handleMenuKey()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1914
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->shouldEnableMenuKey()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_b

    #@7
    .line 1915
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showNextSecurityScreenOrFinish(Z)V

    #@a
    .line 1916
    const/4 v0, 0x1

    #@b
    .line 1918
    :cond_b
    return v0
.end method

.method public hideSKTLocked()V
    .registers 3

    #@0
    .prologue
    .line 644
    const-string v0, "KeyguardHostView"

    #@2
    const-string v1, "[SKT Lock&Wipe] hideSKTLocked()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 645
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->clearLockPattern()V

    #@a
    .line 646
    return-void
.end method

.method isMusicPage(I)Z
    .registers 3
    .parameter "pageIndex"

    #@0
    .prologue
    .line 1769
    if-ltz p1, :cond_d

    #@2
    const v0, 0x1020319

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getWidgetPosition(I)I

    #@8
    move-result v0

    #@9
    if-ne p1, v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public needRecreateMe()Z
    .registers 2

    #@0
    .prologue
    .line 2071
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 454
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onAttachedToWindow()V

    #@3
    .line 455
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserId:I

    #@7
    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetHost;->startListeningAsUser(I)V

    #@a
    .line 456
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@12
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@15
    .line 457
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 461
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onDetachedFromWindow()V

    #@3
    .line 462
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUserId:I

    #@7
    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetHost;->stopListeningAsUser(I)V

    #@a
    .line 463
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@12
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@15
    .line 464
    return-void
.end method

.method protected onFinishInflate()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 341
    const v2, 0x10202c3

    #@4
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@7
    move-result-object v1

    #@8
    .line 342
    .local v1, deleteDropTarget:Landroid/view/View;
    const v2, 0x10202c4

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@11
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@13
    .line 343
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@15
    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setVisibility(I)V

    #@18
    .line 344
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@1a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mWidgetCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setCallbacks(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;)V

    #@1f
    .line 345
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@21
    invoke-virtual {v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setDeleteDropTarget(Landroid/view/View;)V

    #@24
    .line 346
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@26
    const/high16 v3, 0x3f00

    #@28
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setMinScale(F)V

    #@2b
    .line 348
    const v2, 0x10202c7

    #@2e
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@34
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@36
    .line 349
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@38
    if-eqz v2, :cond_41

    #@3a
    .line 350
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@3c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@3e
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setOnChallengeScrolledListener(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$OnChallengeScrolledListener;)V

    #@41
    .line 352
    :cond_41
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@43
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@45
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setViewStateManager(Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;)V

    #@48
    .line 353
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@4a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@4c
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@4f
    .line 355
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@51
    if-eqz v2, :cond_dc

    #@53
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSlidingChallengeLayout:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@55
    .line 357
    .local v0, challenge:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;
    :goto_55
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@57
    invoke-interface {v0, v2}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;->setOnBouncerStateChangedListener(Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;)V

    #@5a
    .line 358
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@5c
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;->getBouncerAnimationDuration()I

    #@5f
    move-result v3

    #@60
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setBouncerAnimationDuration(I)V

    #@63
    .line 359
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@65
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@67
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setPagedView(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;)V

    #@6a
    .line 360
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@6c
    invoke-virtual {v2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setChallengeLayout(Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;)V

    #@6f
    .line 361
    const v2, 0x10202c6

    #@72
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@75
    move-result-object v2

    #@76
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@78
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@7a
    .line 362
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@7c
    if-eqz v2, :cond_e8

    #@7e
    .line 363
    const v2, 0x20d005a

    #@81
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@84
    move-result-object v2

    #@85
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@87
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mNextiKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@89
    .line 368
    :goto_89
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8b
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@8d
    if-eq v2, v3, :cond_9b

    #@8f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@91
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@93
    if-eq v2, v3, :cond_9b

    #@95
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@97
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@99
    if-ne v2, v3, :cond_a0

    #@9b
    .line 369
    :cond_9b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@9d
    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->setPadding(IIII)V

    #@a0
    .line 372
    :cond_a0
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@a2
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@a4
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setSecurityViewContainer(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;)V

    #@a7
    .line 374
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@a9
    instance-of v2, v2, Landroid/app/Activity;

    #@ab
    if-nez v2, :cond_b7

    #@ad
    .line 375
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSystemUiVisibility()I

    #@b0
    move-result v2

    #@b1
    const/high16 v3, 0x40

    #@b3
    or-int/2addr v2, v3

    #@b4
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->setSystemUiVisibility(I)V

    #@b7
    .line 378
    :cond_b7
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addDefaultWidgets()V

    #@ba
    .line 380
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->addWidgetsFromSettings()V

    #@bd
    .line 381
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->shouldEnableAddWidget()Z

    #@c0
    move-result v2

    #@c1
    if-nez v2, :cond_c8

    #@c3
    .line 382
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@c5
    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setAddWidgetEnabled(Z)V

    #@c8
    .line 384
    :cond_c8
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->checkAppWidgetConsistency()V

    #@cb
    .line 385
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSwitchPageRunnable:Ljava/lang/Runnable;

    #@cd
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    #@d0
    .line 387
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@d2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->showUsabilityHints()V

    #@d5
    .line 389
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@d8
    .line 390
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->updateSecurityViews()V

    #@db
    .line 391
    return-void

    #@dc
    .line 355
    .end local v0           #challenge:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;
    :cond_dc
    const v2, 0x10202c2

    #@df
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@e2
    move-result-object v2

    #@e3
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;

    #@e5
    move-object v0, v2

    #@e6
    goto/16 :goto_55

    #@e8
    .line 365
    .restart local v0       #challenge:Lcom/android/internal/policy/impl/keyguard/ChallengeLayout;
    :cond_e8
    const v2, 0x102030b

    #@eb
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@ee
    move-result-object v2

    #@ef
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

    #@f1
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mKeyguardSelectorView:Lcom/android/internal/policy/impl/keyguard/KeyguardSelectorView;

    #@f3
    goto :goto_89
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1710
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    const-string v1, "KeyguardHostView"

    #@6
    const-string v2, "onRestoreInstanceState"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1711
    :cond_b
    instance-of v1, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;

    #@d
    if-nez v1, :cond_13

    #@f
    .line 1712
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@12
    .line 1720
    :goto_12
    return-void

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 1715
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;

    #@16
    .line 1716
    .local v0, ss:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@19
    move-result-object v1

    #@1a
    invoke-super {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1d
    .line 1717
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@1f
    iget v2, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;->transportState:I

    #@21
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->setTransportState(I)V

    #@24
    .line 1718
    iget v1, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;->appWidgetToShow:I

    #@26
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@28
    .line 1719
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSwitchPageRunnable:Ljava/lang/Runnable;

    #@2a
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->post(Ljava/lang/Runnable;)Z

    #@2d
    goto :goto_12
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 5

    #@0
    .prologue
    .line 1700
    sget-boolean v2, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v2, :cond_b

    #@4
    const-string v2, "KeyguardHostView"

    #@6
    const-string v3, "onSaveInstanceState"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1701
    :cond_b
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onSaveInstanceState()Landroid/os/Parcelable;

    #@e
    move-result-object v1

    #@f
    .line 1702
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;

    #@11
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@14
    .line 1703
    .local v0, ss:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@16
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->getTransportState()I

    #@19
    move-result v2

    #@1a
    iput v2, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;->transportState:I

    #@1c
    .line 1704
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetToShow:I

    #@1e
    iput v2, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$SavedState;->appWidgetToShow:I

    #@20
    .line 1705
    return-object v0
.end method

.method public onScreenTurnedOff()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1210
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@3
    if-eqz v1, :cond_28

    #@5
    const-string v1, "KeyguardHostView"

    #@7
    const-string v2, "screen off, instance %s at %s"

    #@9
    const/4 v3, 0x2

    #@a
    new-array v3, v3, [Ljava/lang/Object;

    #@c
    const/4 v4, 0x0

    #@d
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@10
    move-result v5

    #@11
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    aput-object v5, v3, v4

    #@17
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1a
    move-result-wide v4

    #@1b
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1e
    move-result-object v4

    #@1f
    aput-object v4, v3, v6

    #@21
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1214
    :cond_28
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2a
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    #@31
    .line 1217
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->clearAppWidgetToShow()V

    #@34
    .line 1218
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->checkAppWidgetConsistency()V

    #@37
    .line 1219
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@3a
    .line 1220
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@3c
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@3f
    move-result-object v1

    #@40
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onPause()V

    #@43
    .line 1221
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findCameraPage()Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@46
    move-result-object v0

    #@47
    .line 1222
    .local v0, cameraPage:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;
    if-eqz v0, :cond_4c

    #@49
    .line 1223
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->onScreenTurnedOff()V

    #@4c
    .line 1225
    :cond_4c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->clearFocus()V

    #@4f
    .line 1226
    return-void
.end method

.method public onScreenTurnedOn()V
    .registers 4

    #@0
    .prologue
    .line 1193
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v0, :cond_24

    #@4
    const-string v0, "KeyguardHostView"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "screen on, instance "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@14
    move-result v2

    #@15
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1194
    :cond_24
    const/4 v0, 0x0

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@28
    .line 1195
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mCurrentSecuritySelection:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2a
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getSecurityView(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;

    #@2d
    move-result-object v0

    #@2e
    const/4 v1, 0x1

    #@2f
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;->onResume(I)V

    #@32
    .line 1200
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->requestLayout()V

    #@35
    .line 1202
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@37
    if-eqz v0, :cond_3e

    #@39
    .line 1203
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@3b
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->showUsabilityHints()V

    #@3e
    .line 1205
    :cond_3e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->requestFocus()Z

    #@41
    .line 1206
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "ev"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 310
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@4
    move-result v0

    #@5
    .line 311
    .local v0, result:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 312
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@e
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@11
    .line 313
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@13
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@15
    int-to-float v2, v2

    #@16
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@18
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@1a
    int-to-float v3, v3

    #@1b
    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@1e
    .line 314
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityViewContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@20
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_28

    #@26
    if-eqz v0, :cond_39

    #@28
    :cond_28
    const/4 v0, 0x1

    #@29
    .line 315
    :goto_29
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@2b
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@2d
    neg-int v1, v1

    #@2e
    int-to-float v1, v1

    #@2f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mTempRect:Landroid/graphics/Rect;

    #@31
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@33
    neg-int v2, v2

    #@34
    int-to-float v2, v2

    #@35
    invoke-virtual {p1, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@38
    .line 316
    return v0

    #@39
    :cond_39
    move v0, v1

    #@3a
    .line 314
    goto :goto_29
.end method

.method public onUserActivityTimeoutChanged()V
    .registers 2

    #@0
    .prologue
    .line 519
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 520
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->onUserActivityTimeoutChanged()V

    #@9
    .line 522
    :cond_9
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 6
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 1724
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onWindowFocusChanged(Z)V

    #@3
    .line 1725
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@5
    if-eqz v1, :cond_23

    #@7
    const-string v2, "KeyguardHostView"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Window is "

    #@10
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    if-eqz p1, :cond_41

    #@16
    const-string v1, "focused"

    #@18
    :goto_18
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1726
    :cond_23
    if-eqz p1, :cond_40

    #@25
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mShowSecurityWhenReturn:Z

    #@27
    if-eqz v1, :cond_40

    #@29
    .line 1727
    const v1, 0x10202c7

    #@2c
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@32
    .line 1729
    .local v0, slider:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
    if-eqz v0, :cond_3d

    #@34
    .line 1730
    const/high16 v1, 0x3f80

    #@36
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->setHandleAlpha(F)V

    #@39
    .line 1731
    const/4 v1, 0x1

    #@3a
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->showChallenge(Z)V

    #@3d
    .line 1733
    :cond_3d
    const/4 v1, 0x0

    #@3e
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mShowSecurityWhenReturn:Z

    #@40
    .line 1735
    .end local v0           #slider:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
    :cond_40
    return-void

    #@41
    .line 1725
    :cond_41
    const-string v1, "unfocused"

    #@43
    goto :goto_18
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 1073
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mIsVerifyUnlockOnly:Z

    #@3
    .line 1074
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mAppWidgetContainer:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;

    #@5
    const v1, 0x1020310

    #@8
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getWidgetPosition(I)I

    #@b
    move-result v1

    #@c
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setCurrentPage(I)V

    #@f
    .line 1075
    return-void
.end method

.method protected setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 3
    .parameter "utils"

    #@0
    .prologue
    .line 446
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@3
    .line 447
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@5
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V

    #@8
    .line 449
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->updateSecurityViews()V

    #@b
    .line 450
    return-void
.end method

.method protected setOnDismissAction(Lcom/android/internal/policy/impl/keyguard/OnDismissAction;)V
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 1082
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mDismissAction:Lcom/android/internal/policy/impl/keyguard/OnDismissAction;

    #@2
    .line 1083
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 1234
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "KeyguardHostView"

    #@6
    const-string v1, "show()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1235
    :cond_b
    const/4 v0, 0x0

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showPrimarySecurityScreen(Z)V

    #@f
    .line 1236
    return-void
.end method

.method protected showAlmostAtWipeDialog()V
    .registers 5

    #@0
    .prologue
    .line 694
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2
    const v3, 0x209015b

    #@5
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 695
    .local v1, title:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@b
    const v3, 0x209015c

    #@e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 696
    .local v0, message:Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 697
    return-void
.end method

.method public showAssistant()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1937
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "search"

    #@5
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/app/SearchManager;

    #@b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@d
    const/4 v3, -0x2

    #@e
    invoke-virtual {v0, v2, v3}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@11
    move-result-object v1

    #@12
    .line 1940
    .local v1, intent:Landroid/content/Intent;
    if-nez v1, :cond_15

    #@14
    .line 1950
    :goto_14
    return-void

    #@15
    .line 1942
    :cond_15
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@17
    const v2, 0x10a0027

    #@1a
    const v3, 0x10a0028

    #@1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->getHandler()Landroid/os/Handler;

    #@20
    move-result-object v5

    #@21
    invoke-static {v0, v2, v3, v5, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    #@24
    move-result-object v6

    #@25
    .line 1946
    .local v6, opts:Landroid/app/ActivityOptions;
    const/high16 v0, 0x1000

    #@27
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2a
    .line 1948
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mActivityLauncher:Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@2c
    const/4 v2, 0x0

    #@2d
    invoke-virtual {v6}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    #@30
    move-result-object v3

    #@31
    move-object v5, v4

    #@32
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchActivityWithAnimation(Landroid/content/Intent;ZLandroid/os/Bundle;Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@35
    goto :goto_14
.end method

.method protected showHalfWrongPasswordDialog(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;Landroid/content/Context;)V
    .registers 16
    .parameter "mCallback"
    .parameter "mContext"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    .line 1962
    const-string v9, "layout_inflater"

    #@3
    invoke-virtual {p2, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v6

    #@7
    check-cast v6, Landroid/view/LayoutInflater;

    #@9
    .line 1963
    .local v6, inflater:Landroid/view/LayoutInflater;
    const v10, 0x203000a

    #@c
    const v9, 0x20d004e

    #@f
    invoke-virtual {p0, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->findViewById(I)Landroid/view/View;

    #@12
    move-result-object v9

    #@13
    check-cast v9, Landroid/view/ViewGroup;

    #@15
    invoke-virtual {v6, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@18
    move-result-object v7

    #@19
    .line 1965
    .local v7, layout:Landroid/view/View;
    const v9, 0x20d0051

    #@1c
    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Landroid/widget/EditText;

    #@22
    .line 1966
    .local v3, entercode:Landroid/widget/EditText;
    const v9, 0x20d0052

    #@25
    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Landroid/widget/Button;

    #@2b
    .line 1967
    .local v2, btnDialogOk:Landroid/widget/Button;
    const v9, 0x20d0050

    #@2e
    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v5

    #@32
    check-cast v5, Landroid/widget/ImageView;

    #@34
    .line 1969
    .local v5, image:Landroid/widget/ImageView;
    invoke-static {p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@37
    move-result-object v9

    #@38
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInputHalfFailedAttempts()Z

    #@3b
    move-result v9

    #@3c
    if-eqz v9, :cond_46

    #@3e
    .line 1970
    const-string v9, "KeyguardHostView"

    #@40
    const-string v10, "the user already inputs the half failed attempts string."

    #@42
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 2066
    :goto_45
    return-void

    #@46
    .line 1974
    :cond_46
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->halfWrongPasswordDialog:Landroid/app/AlertDialog;

    #@48
    if-eqz v9, :cond_6b

    #@4a
    .line 1975
    const-string v9, "KeyguardHostView"

    #@4c
    new-instance v10, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v11, "halfWrongPasswordDialog is "

    #@53
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v10

    #@57
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->halfWrongPasswordDialog:Landroid/app/AlertDialog;

    #@59
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v10

    #@5d
    const-string v11, ". so it does not need to create one."

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v10

    #@63
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v10

    #@67
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_45

    #@6b
    .line 1979
    :cond_6b
    new-instance v8, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$13;

    #@6d
    invoke-direct {v8, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$13;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@70
    .line 1991
    .local v8, watcher:Landroid/text/TextWatcher;
    const v9, 0x2020408

    #@73
    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    #@76
    .line 1992
    invoke-virtual {v3, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@79
    .line 1996
    new-instance v4, Landroid/view/GestureDetector;

    #@7b
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$14;

    #@7d
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$14;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@80
    invoke-direct {v4, p2, v9}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@83
    .line 2019
    .local v4, gestureDetector:Landroid/view/GestureDetector;
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$15;

    #@85
    invoke-direct {v9, p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$15;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Landroid/view/GestureDetector;)V

    #@88
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@8b
    .line 2025
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@8d
    invoke-direct {v0, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@90
    .line 2026
    .local v0, aDialog:Landroid/app/AlertDialog$Builder;
    const v9, 0x2090159

    #@93
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@96
    .line 2027
    const v9, 0x209015a

    #@99
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@9c
    .line 2028
    const/4 v9, 0x0

    #@9d
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@a0
    .line 2029
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@a3
    .line 2031
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@a6
    move-result-object v1

    #@a7
    .line 2032
    .local v1, ad:Landroid/app/AlertDialog;
    const-string v9, "KeyguardHostView"

    #@a9
    new-instance v10, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v11, "ad = "

    #@b0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v10

    #@b8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v10

    #@bc
    invoke-static {v9, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 2033
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@c2
    move-result-object v9

    #@c3
    const/16 v10, 0x7d9

    #@c5
    invoke-virtual {v9, v10}, Landroid/view/Window;->setType(I)V

    #@c8
    .line 2034
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@cb
    move-result-object v9

    #@cc
    const v10, 0x111000f

    #@cf
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@d2
    move-result v9

    #@d3
    if-nez v9, :cond_dc

    #@d5
    .line 2035
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@d8
    move-result-object v9

    #@d9
    invoke-virtual {v9, v12, v12}, Landroid/view/Window;->setFlags(II)V

    #@dc
    .line 2039
    :cond_dc
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$16;

    #@de
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$16;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V

    #@e1
    invoke-virtual {v1, v9}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@e4
    .line 2047
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@e7
    .line 2048
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->halfWrongPasswordDialog:Landroid/app/AlertDialog;

    #@e9
    .line 2050
    new-instance v9, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;

    #@eb
    invoke-direct {v9, p0, v3, v1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$17;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;Landroid/widget/EditText;Landroid/app/AlertDialog;Landroid/content/Context;)V

    #@ee
    invoke-virtual {v2, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@f1
    goto/16 :goto_45
.end method

.method public showNextSecurityScreenIfPresent()Z
    .registers 3

    #@0
    .prologue
    .line 886
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@5
    move-result-object v0

    #@6
    .line 888
    .local v0, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@8
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getAlternateFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@b
    move-result-object v0

    #@c
    .line 889
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@e
    if-ne v1, v0, :cond_12

    #@10
    .line 890
    const/4 v1, 0x0

    #@11
    .line 893
    :goto_11
    return v1

    #@12
    .line 892
    :cond_12
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@15
    .line 893
    const/4 v1, 0x1

    #@16
    goto :goto_11
.end method

.method showPrimarySecurityScreen(Z)V
    .registers 6
    .parameter "turningOff"

    #@0
    .prologue
    .line 847
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@5
    move-result-object v0

    #@6
    .line 848
    .local v0, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@8
    if-eqz v1, :cond_28

    #@a
    const-string v1, "KeyguardHostView"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "showPrimarySecurityScreen(turningOff="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ")"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 849
    :cond_28
    if-nez p1, :cond_3c

    #@2a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mContext:Landroid/content/Context;

    #@2c
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isAlternateUnlockEnabled()Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_3c

    #@36
    .line 853
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@38
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getAlternateFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@3b
    move-result-object v0

    #@3c
    .line 855
    :cond_3c
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@3f
    .line 859
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@41
    if-eqz v1, :cond_63

    #@43
    .line 861
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@45
    if-eq v0, v1, :cond_4f

    #@47
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@49
    if-eq v0, v1, :cond_4f

    #@4b
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@4d
    if-ne v0, v1, :cond_64

    #@4f
    :cond_4f
    const-string v1, "vold.decrypt"

    #@51
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    const-string v2, "1"

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v1

    #@5b
    if-eqz v1, :cond_64

    #@5d
    .line 865
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@5f
    const/4 v2, 0x0

    #@60
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNaviHidden(Z)V

    #@63
    .line 872
    :cond_63
    :goto_63
    return-void

    #@64
    .line 868
    :cond_64
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@66
    const/4 v2, 0x1

    #@67
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->setNaviHidden(Z)V

    #@6a
    goto :goto_63
.end method

.method public showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 629
    const-string v0, "KeyguardHostView"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[SKT Lock&Wipe] showSKTLocked("

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ", "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, ")"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 631
    if-ne v3, p1, :cond_45

    #@35
    .line 633
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->clearLockPattern()V

    #@38
    .line 634
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3a
    const/high16 v1, 0x2

    #@3c
    invoke-virtual {v0, p2, v1}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;I)V

    #@3f
    .line 635
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@41
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLockPatternEnabled(Z)V

    #@44
    .line 641
    :goto_44
    return-void

    #@45
    .line 639
    :cond_45
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@47
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->setPermanentlyLocked(Z)V

    #@4a
    goto :goto_44
.end method

.method public userActivity()V
    .registers 2

    #@0
    .prologue
    .line 513
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 514
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->userActivity()V

    #@9
    .line 516
    :cond_9
    return-void
.end method

.method public verifyUnlock()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1279
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mSecurityModel:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@6
    move-result-object v0

    #@7
    .line 1280
    .local v0, securityMode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@9
    if-ne v0, v1, :cond_15

    #@b
    .line 1281
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 1282
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@11
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDone(Z)V

    #@14
    .line 1296
    :cond_14
    :goto_14
    return-void

    #@15
    .line 1284
    :cond_15
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@17
    if-eq v0, v1, :cond_2c

    #@19
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->PIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1b
    if-eq v0, v1, :cond_2c

    #@1d
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1f
    if-eq v0, v1, :cond_2c

    #@21
    .line 1288
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@23
    if-eqz v1, :cond_14

    #@25
    .line 1289
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@27
    const/4 v2, 0x0

    #@28
    invoke-interface {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDone(Z)V

    #@2b
    goto :goto_14

    #@2c
    .line 1293
    :cond_2c
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->mIsVerifyUnlockOnly:Z

    #@2e
    .line 1294
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@31
    goto :goto_14
.end method

.method public wakeWhenReadyTq(I)V
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 1265
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "KeyguardHostView"

    #@6
    const-string v1, "onWakeKey"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1266
    :cond_b
    const/16 v0, 0x52

    #@d
    if-ne p1, v0, :cond_2f

    #@f
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->isSecure()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_2f

    #@15
    .line 1267
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@17
    if-eqz v0, :cond_20

    #@19
    const-string v0, "KeyguardHostView"

    #@1b
    const-string v1, "switching screens to unlock screen because wake key was MENU"

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1268
    :cond_20
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@22
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->showSecurityScreen(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)V

    #@25
    .line 1272
    :cond_25
    :goto_25
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@27
    if-eqz v0, :cond_2e

    #@29
    .line 1273
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    #@2b
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->wakeUp()V

    #@2e
    .line 1275
    :cond_2e
    return-void

    #@2f
    .line 1270
    :cond_2f
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;->DEBUG:Z

    #@31
    if-eqz v0, :cond_25

    #@33
    const-string v0, "KeyguardHostView"

    #@35
    const-string v1, "poking wake lock immediately"

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_25
.end method
