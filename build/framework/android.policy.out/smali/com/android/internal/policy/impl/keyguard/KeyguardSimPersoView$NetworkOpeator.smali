.class public final enum Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;
.super Ljava/lang/Enum;
.source "KeyguardSimPersoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkOpeator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

.field public static final enum TELECOM_KT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

.field public static final enum TELECOM_LGT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

.field public static final enum TELECOM_NONE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

.field public static final enum TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 88
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@6
    const-string v1, "TELECOM_NONE"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_NONE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@d
    .line 89
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@f
    const-string v1, "TELECOM_LGT"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_LGT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@16
    .line 90
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@18
    const-string v1, "TELECOM_KT"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_KT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@1f
    .line 91
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@21
    const-string v1, "TELECOM_SKT"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@28
    .line 87
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@2b
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_NONE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_LGT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_KT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->TELECOM_SKT:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 87
    const-class v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;
    .registers 1

    #@0
    .prologue
    .line 87
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPersoView$NetworkOpeator;

    #@8
    return-object v0
.end method
