.class Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;
.super Landroid/content/BroadcastReceiver;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 392
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 25
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 395
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v9

    #@4
    .line 398
    .local v9, action:Ljava/lang/String;
    const-string v3, "android.intent.action.TIME_TICK"

    #@6
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_1c

    #@c
    const-string v3, "android.intent.action.TIME_SET"

    #@e
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_1c

    #@14
    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    #@16
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3a

    #@1c
    .line 401
    :cond_1c
    move-object/from16 v0, p0

    #@1e
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@20
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@23
    move-result-object v3

    #@24
    move-object/from16 v0, p0

    #@26
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@28
    move-object/from16 v18, v0

    #@2a
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@2d
    move-result-object v18

    #@2e
    const/16 v19, 0x12d

    #@30
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@33
    move-result-object v18

    #@34
    move-object/from16 v0, v18

    #@36
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@39
    .line 514
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 402
    :cond_3a
    const-string v3, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@3c
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_bd

    #@42
    .line 404
    const-string v3, "subscription"

    #@44
    const/16 v18, 0x0

    #@46
    move-object/from16 v0, p2

    #@48
    move/from16 v1, v18

    #@4a
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4d
    move-result v17

    #@4e
    .line 405
    .local v17, subscription:I
    const-string v3, "KeyguardUpdateMonitor"

    #@50
    new-instance v18, Ljava/lang/StringBuilder;

    #@52
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v19, "Received SPN update on sub :"

    #@57
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v18

    #@5b
    move-object/from16 v0, v18

    #@5d
    move/from16 v1, v17

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v18

    #@63
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v18

    #@67
    move-object/from16 v0, v18

    #@69
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 407
    move-object/from16 v0, p0

    #@6e
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@70
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$900(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)[Ljava/lang/CharSequence;

    #@73
    move-result-object v3

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@78
    move-object/from16 v18, v0

    #@7a
    move-object/from16 v0, v18

    #@7c
    move-object/from16 v1, p2

    #@7e
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@81
    move-result-object v18

    #@82
    aput-object v18, v3, v17

    #@84
    .line 408
    move-object/from16 v0, p0

    #@86
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@88
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)[Ljava/lang/CharSequence;

    #@8b
    move-result-object v3

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@90
    move-object/from16 v18, v0

    #@92
    move-object/from16 v0, v18

    #@94
    move-object/from16 v1, p2

    #@96
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1200(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@99
    move-result-object v18

    #@9a
    aput-object v18, v3, v17

    #@9c
    .line 409
    move-object/from16 v0, p0

    #@9e
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a0
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@a3
    move-result-object v3

    #@a4
    const/16 v18, 0x12f

    #@a6
    move/from16 v0, v18

    #@a8
    invoke-virtual {v3, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@ab
    move-result-object v12

    #@ac
    .line 410
    .local v12, msg:Landroid/os/Message;
    move/from16 v0, v17

    #@ae
    iput v0, v12, Landroid/os/Message;->arg1:I

    #@b0
    .line 411
    move-object/from16 v0, p0

    #@b2
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@b4
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@b7
    move-result-object v3

    #@b8
    invoke-virtual {v3, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@bb
    goto/16 :goto_39

    #@bd
    .line 412
    .end local v12           #msg:Landroid/os/Message;
    .end local v17           #subscription:I
    :cond_bd
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    #@bf
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2
    move-result v3

    #@c3
    if-eqz v3, :cond_146

    #@c5
    .line 413
    const-string v3, "status"

    #@c7
    const/16 v18, 0x1

    #@c9
    move-object/from16 v0, p2

    #@cb
    move/from16 v1, v18

    #@cd
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@d0
    move-result v4

    #@d1
    .line 414
    .local v4, status:I
    const-string v3, "plugged"

    #@d3
    const/16 v18, 0x0

    #@d5
    move-object/from16 v0, p2

    #@d7
    move/from16 v1, v18

    #@d9
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@dc
    move-result v6

    #@dd
    .line 415
    .local v6, plugged:I
    const-string v3, "level"

    #@df
    const/16 v18, 0x0

    #@e1
    move-object/from16 v0, p2

    #@e3
    move/from16 v1, v18

    #@e5
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@e8
    move-result v5

    #@e9
    .line 416
    .local v5, level:I
    const-string v3, "health"

    #@eb
    const/16 v18, 0x1

    #@ed
    move-object/from16 v0, p2

    #@ef
    move/from16 v1, v18

    #@f1
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@f4
    move-result v7

    #@f5
    .line 418
    .local v7, health:I
    const-string v3, "temperature"

    #@f7
    const/16 v18, 0x0

    #@f9
    move-object/from16 v0, p2

    #@fb
    move/from16 v1, v18

    #@fd
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@100
    move-result v8

    #@101
    .line 419
    .local v8, temperature:I
    move-object/from16 v0, p0

    #@103
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@105
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z

    #@108
    move-result v3

    #@109
    if-eqz v3, :cond_122

    #@10b
    .line 420
    move-object/from16 v0, p0

    #@10d
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@10f
    const-string v18, "charging_current"

    #@111
    const/16 v19, 0x0

    #@113
    move-object/from16 v0, p2

    #@115
    move-object/from16 v1, v18

    #@117
    move/from16 v2, v19

    #@119
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@11c
    move-result v18

    #@11d
    move/from16 v0, v18

    #@11f
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1402(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;I)I

    #@122
    .line 422
    :cond_122
    move-object/from16 v0, p0

    #@124
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@126
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@129
    move-result-object v18

    #@12a
    const/16 v19, 0x12e

    #@12c
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;

    #@12e
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$BatteryStatus;-><init>(IIIII)V

    #@131
    move-object/from16 v0, v18

    #@133
    move/from16 v1, v19

    #@135
    invoke-virtual {v0, v1, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@138
    move-result-object v12

    #@139
    .line 425
    .restart local v12       #msg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@13b
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@13d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@140
    move-result-object v3

    #@141
    invoke-virtual {v3, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@144
    goto/16 :goto_39

    #@146
    .line 426
    .end local v4           #status:I
    .end local v5           #level:I
    .end local v6           #plugged:I
    .end local v7           #health:I
    .end local v8           #temperature:I
    .end local v12           #msg:Landroid/os/Message;
    :cond_146
    const-string v3, "android.intent.action.SIM_STATE_CHANGED"

    #@148
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14b
    move-result v3

    #@14c
    if-eqz v3, :cond_1fc

    #@14e
    .line 428
    const-string v3, "KeyguardUpdateMonitor"

    #@150
    new-instance v18, Ljava/lang/StringBuilder;

    #@152
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v19, "action "

    #@157
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v18

    #@15b
    move-object/from16 v0, v18

    #@15d
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v18

    #@161
    const-string v19, " state"

    #@163
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v18

    #@167
    const-string v19, "ss"

    #@169
    move-object/from16 v0, p2

    #@16b
    move-object/from16 v1, v19

    #@16d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@170
    move-result-object v19

    #@171
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v18

    #@175
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@178
    move-result-object v18

    #@179
    move-object/from16 v0, v18

    #@17b
    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    .line 431
    const-string v3, "ss"

    #@180
    move-object/from16 v0, p2

    #@182
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@185
    move-result-object v15

    #@186
    .line 432
    .local v15, stateExtra:Ljava/lang/String;
    const-string v3, "LOADED"

    #@188
    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18b
    move-result v3

    #@18c
    if-eqz v3, :cond_1d9

    #@18e
    .line 433
    const-string v3, "subscription"

    #@190
    const/16 v18, 0x0

    #@192
    move-object/from16 v0, p2

    #@194
    move/from16 v1, v18

    #@196
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@199
    move-result v16

    #@19a
    .line 434
    .local v16, subs:I
    const-string v3, "KeyguardUpdateMonitor"

    #@19c
    new-instance v18, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v19, "Received ACTION_SIM_STATE_CHANGED, INTENT_VALUE_ICC_LOADED on sub :"

    #@1a3
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v18

    #@1a7
    move-object/from16 v0, v18

    #@1a9
    move/from16 v1, v16

    #@1ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v18

    #@1af
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b2
    move-result-object v18

    #@1b3
    move-object/from16 v0, v18

    #@1b5
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b8
    .line 435
    move-object/from16 v0, p0

    #@1ba
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1bc
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@1bf
    move-result-object v3

    #@1c0
    const/16 v18, 0x12f

    #@1c2
    move/from16 v0, v18

    #@1c4
    invoke-virtual {v3, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1c7
    move-result-object v12

    #@1c8
    .line 436
    .restart local v12       #msg:Landroid/os/Message;
    move/from16 v0, v16

    #@1ca
    iput v0, v12, Landroid/os/Message;->arg1:I

    #@1cc
    .line 437
    move-object/from16 v0, p0

    #@1ce
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1d0
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@1d3
    move-result-object v3

    #@1d4
    invoke-virtual {v3, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1d7
    goto/16 :goto_39

    #@1d9
    .line 439
    .end local v12           #msg:Landroid/os/Message;
    .end local v16           #subs:I
    :cond_1d9
    move-object/from16 v0, p0

    #@1db
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1dd
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@1e0
    move-result-object v3

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1e5
    move-object/from16 v18, v0

    #@1e7
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@1ea
    move-result-object v18

    #@1eb
    const/16 v19, 0x130

    #@1ed
    invoke-static/range {p2 .. p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->fromIntent(Landroid/content/Intent;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;

    #@1f0
    move-result-object v20

    #@1f1
    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1f4
    move-result-object v18

    #@1f5
    move-object/from16 v0, v18

    #@1f7
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1fa
    goto/16 :goto_39

    #@1fc
    .line 441
    .end local v15           #stateExtra:Ljava/lang/String;
    :cond_1fc
    const-string v3, "android.media.RINGER_MODE_CHANGED"

    #@1fe
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@201
    move-result v3

    #@202
    if-eqz v3, :cond_233

    #@204
    .line 442
    move-object/from16 v0, p0

    #@206
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@208
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@20b
    move-result-object v3

    #@20c
    move-object/from16 v0, p0

    #@20e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@210
    move-object/from16 v18, v0

    #@212
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@215
    move-result-object v18

    #@216
    const/16 v19, 0x131

    #@218
    const-string v20, "android.media.EXTRA_RINGER_MODE"

    #@21a
    const/16 v21, -0x1

    #@21c
    move-object/from16 v0, p2

    #@21e
    move-object/from16 v1, v20

    #@220
    move/from16 v2, v21

    #@222
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@225
    move-result v20

    #@226
    const/16 v21, 0x0

    #@228
    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@22b
    move-result-object v18

    #@22c
    move-object/from16 v0, v18

    #@22e
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@231
    goto/16 :goto_39

    #@233
    .line 444
    :cond_233
    const-string v3, "android.intent.action.PHONE_STATE"

    #@235
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@238
    move-result v3

    #@239
    if-eqz v3, :cond_266

    #@23b
    .line 445
    const-string v3, "state"

    #@23d
    move-object/from16 v0, p2

    #@23f
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@242
    move-result-object v14

    #@243
    .line 446
    .local v14, state:Ljava/lang/String;
    move-object/from16 v0, p0

    #@245
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@247
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@24a
    move-result-object v3

    #@24b
    move-object/from16 v0, p0

    #@24d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@24f
    move-object/from16 v18, v0

    #@251
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@254
    move-result-object v18

    #@255
    const/16 v19, 0x132

    #@257
    move-object/from16 v0, v18

    #@259
    move/from16 v1, v19

    #@25b
    invoke-virtual {v0, v1, v14}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@25e
    move-result-object v18

    #@25f
    move-object/from16 v0, v18

    #@261
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@264
    goto/16 :goto_39

    #@266
    .line 447
    .end local v14           #state:Ljava/lang/String;
    :cond_266
    const-string v3, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    #@268
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26b
    move-result v3

    #@26c
    if-eqz v3, :cond_28d

    #@26e
    .line 449
    move-object/from16 v0, p0

    #@270
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@272
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@275
    move-result-object v3

    #@276
    move-object/from16 v0, p0

    #@278
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@27a
    move-object/from16 v18, v0

    #@27c
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@27f
    move-result-object v18

    #@280
    const/16 v19, 0x135

    #@282
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@285
    move-result-object v18

    #@286
    move-object/from16 v0, v18

    #@288
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@28b
    goto/16 :goto_39

    #@28d
    .line 450
    :cond_28d
    const-string v3, "android.intent.action.USER_REMOVED"

    #@28f
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@292
    move-result v3

    #@293
    if-eqz v3, :cond_2c4

    #@295
    .line 451
    move-object/from16 v0, p0

    #@297
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@299
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@29c
    move-result-object v3

    #@29d
    move-object/from16 v0, p0

    #@29f
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2a1
    move-object/from16 v18, v0

    #@2a3
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@2a6
    move-result-object v18

    #@2a7
    const/16 v19, 0x137

    #@2a9
    const-string v20, "android.intent.extra.user_handle"

    #@2ab
    const/16 v21, 0x0

    #@2ad
    move-object/from16 v0, p2

    #@2af
    move-object/from16 v1, v20

    #@2b1
    move/from16 v2, v21

    #@2b3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2b6
    move-result v20

    #@2b7
    const/16 v21, 0x0

    #@2b9
    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@2bc
    move-result-object v18

    #@2bd
    move-object/from16 v0, v18

    #@2bf
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2c2
    goto/16 :goto_39

    #@2c4
    .line 453
    :cond_2c4
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@2c6
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c9
    move-result v3

    #@2ca
    if-eqz v3, :cond_2eb

    #@2cc
    .line 454
    move-object/from16 v0, p0

    #@2ce
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2d0
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@2d3
    move-result-object v3

    #@2d4
    move-object/from16 v0, p0

    #@2d6
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2d8
    move-object/from16 v18, v0

    #@2da
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@2dd
    move-result-object v18

    #@2de
    const/16 v19, 0x139

    #@2e0
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2e3
    move-result-object v18

    #@2e4
    move-object/from16 v0, v18

    #@2e6
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2e9
    goto/16 :goto_39

    #@2eb
    .line 455
    :cond_2eb
    move-object/from16 v0, p0

    #@2ed
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2ef
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1500(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Ljava/lang/String;

    #@2f2
    move-result-object v3

    #@2f3
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f6
    move-result v3

    #@2f7
    if-eqz v3, :cond_318

    #@2f9
    .line 456
    move-object/from16 v0, p0

    #@2fb
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2fd
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@300
    move-result-object v3

    #@301
    move-object/from16 v0, p0

    #@303
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@305
    move-object/from16 v18, v0

    #@307
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@30a
    move-result-object v18

    #@30b
    const/16 v19, 0x13a

    #@30d
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@310
    move-result-object v18

    #@311
    move-object/from16 v0, v18

    #@313
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@316
    goto/16 :goto_39

    #@318
    .line 458
    :cond_318
    const-string v3, "android.intent.action.HDMI_PLUGGED"

    #@31a
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31d
    move-result v3

    #@31e
    if-eqz v3, :cond_34f

    #@320
    .line 459
    const-string v3, "state"

    #@322
    const/16 v18, 0x0

    #@324
    move-object/from16 v0, p2

    #@326
    move/from16 v1, v18

    #@328
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@32b
    move-result v10

    #@32c
    .line 461
    .local v10, connected:Z
    move-object/from16 v0, p0

    #@32e
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@330
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@333
    move-result-object v3

    #@334
    move-object/from16 v0, p0

    #@336
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@338
    move-object/from16 v18, v0

    #@33a
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@33d
    move-result-object v18

    #@33e
    const/16 v19, 0x13b

    #@340
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@343
    move-result-object v20

    #@344
    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@347
    move-result-object v18

    #@348
    move-object/from16 v0, v18

    #@34a
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@34d
    goto/16 :goto_39

    #@34f
    .line 466
    .end local v10           #connected:Z
    :cond_34f
    const-string v3, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@351
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@354
    move-result v3

    #@355
    if-eqz v3, :cond_39c

    #@357
    .line 467
    move-object/from16 v0, p0

    #@359
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@35b
    const-string v18, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@35d
    const/16 v19, 0x0

    #@35f
    move-object/from16 v0, p2

    #@361
    move-object/from16 v1, v18

    #@363
    move/from16 v2, v19

    #@365
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@368
    move-result v18

    #@369
    move/from16 v0, v18

    #@36b
    iput v0, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCoverState:I

    #@36d
    .line 468
    move-object/from16 v0, p0

    #@36f
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@371
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@374
    move-result-object v3

    #@375
    move-object/from16 v0, p0

    #@377
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@379
    move-object/from16 v18, v0

    #@37b
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@37e
    move-result-object v18

    #@37f
    const/16 v19, 0x13c

    #@381
    move-object/from16 v0, p0

    #@383
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@385
    move-object/from16 v20, v0

    #@387
    move-object/from16 v0, v20

    #@389
    iget v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mCoverState:I

    #@38b
    move/from16 v20, v0

    #@38d
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@390
    move-result-object v20

    #@391
    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@394
    move-result-object v18

    #@395
    move-object/from16 v0, v18

    #@397
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@39a
    goto/16 :goto_39

    #@39c
    .line 473
    :cond_39c
    const-string v3, "com.lge.intent.action.LG_DUAL_SCREEN_STATUS_CHANGED"

    #@39e
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a1
    move-result v3

    #@3a2
    if-eqz v3, :cond_3e3

    #@3a4
    .line 475
    const/4 v11, 0x0

    #@3a5
    .line 476
    .local v11, currentEnable:Z
    const-string v3, "Status"

    #@3a7
    const/16 v18, 0x0

    #@3a9
    move-object/from16 v0, p2

    #@3ab
    move/from16 v1, v18

    #@3ad
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3b0
    move-result v4

    #@3b1
    .line 477
    .restart local v4       #status:I
    const/4 v3, 0x4

    #@3b2
    if-ne v4, v3, :cond_3e1

    #@3b4
    .line 478
    const/4 v11, 0x1

    #@3b5
    .line 485
    :goto_3b5
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1600()Z

    #@3b8
    move-result v3

    #@3b9
    if-eq v3, v11, :cond_39

    #@3bb
    .line 486
    invoke-static {v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1602(Z)Z

    #@3be
    .line 487
    move-object/from16 v0, p0

    #@3c0
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3c2
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@3c5
    move-result-object v3

    #@3c6
    move-object/from16 v0, p0

    #@3c8
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3ca
    move-object/from16 v18, v0

    #@3cc
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@3cf
    move-result-object v18

    #@3d0
    const/16 v19, 0x13d

    #@3d2
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@3d5
    move-result-object v20

    #@3d6
    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3d9
    move-result-object v18

    #@3da
    move-object/from16 v0, v18

    #@3dc
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3df
    goto/16 :goto_39

    #@3e1
    .line 480
    :cond_3e1
    const/4 v11, 0x0

    #@3e2
    goto :goto_3b5

    #@3e3
    .line 493
    .end local v4           #status:I
    .end local v11           #currentEnable:Z
    :cond_3e3
    const-string v3, "com.lge.qremote.settings.action.HomeWiFiConnected"

    #@3e5
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e8
    move-result v3

    #@3e9
    if-eqz v3, :cond_42a

    #@3eb
    .line 494
    const-string v3, "KeyguardUpdateMonitor"

    #@3ed
    const-string v18, "Q remote wifi changed!"

    #@3ef
    move-object/from16 v0, v18

    #@3f1
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f4
    .line 495
    move-object/from16 v0, p0

    #@3f6
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3f8
    const-string v18, "is_connected"

    #@3fa
    const/16 v19, 0x0

    #@3fc
    move-object/from16 v0, p2

    #@3fe
    move-object/from16 v1, v18

    #@400
    move/from16 v2, v19

    #@402
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@405
    move-result v18

    #@406
    move/from16 v0, v18

    #@408
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1702(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z

    #@40b
    .line 496
    move-object/from16 v0, p0

    #@40d
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@40f
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@412
    move-result-object v3

    #@413
    move-object/from16 v0, p0

    #@415
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@417
    move-object/from16 v18, v0

    #@419
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@41c
    move-result-object v18

    #@41d
    const/16 v19, 0x191

    #@41f
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@422
    move-result-object v18

    #@423
    move-object/from16 v0, v18

    #@425
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@428
    goto/16 :goto_39

    #@42a
    .line 497
    :cond_42a
    const-string v3, "android.net.wifi.STATE_CHANGE"

    #@42c
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42f
    move-result v3

    #@430
    if-eqz v3, :cond_47c

    #@432
    .line 498
    const-string v3, "networkInfo"

    #@434
    move-object/from16 v0, p2

    #@436
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@439
    move-result-object v13

    #@43a
    check-cast v13, Landroid/net/NetworkInfo;

    #@43c
    .line 499
    .local v13, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v13, :cond_39

    #@43e
    .line 500
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@441
    move-result-object v3

    #@442
    sget-object v18, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@444
    move-object/from16 v0, v18

    #@446
    if-ne v3, v0, :cond_39

    #@448
    .line 501
    move-object/from16 v0, p0

    #@44a
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@44c
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1700(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Z

    #@44f
    move-result v3

    #@450
    if-eqz v3, :cond_39

    #@452
    .line 502
    move-object/from16 v0, p0

    #@454
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@456
    const/16 v18, 0x0

    #@458
    move/from16 v0, v18

    #@45a
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1702(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z

    #@45d
    .line 503
    move-object/from16 v0, p0

    #@45f
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@461
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@464
    move-result-object v3

    #@465
    move-object/from16 v0, p0

    #@467
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@469
    move-object/from16 v18, v0

    #@46b
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@46e
    move-result-object v18

    #@46f
    const/16 v19, 0x191

    #@471
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@474
    move-result-object v18

    #@475
    move-object/from16 v0, v18

    #@477
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@47a
    goto/16 :goto_39

    #@47c
    .line 508
    .end local v13           #networkInfo:Landroid/net/NetworkInfo;
    :cond_47c
    const-string v3, "com.lge.music.playstatechanged"

    #@47e
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@481
    move-result v3

    #@482
    if-nez v3, :cond_494

    #@484
    const-string v3, "com.lge.music.metachanged"

    #@486
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@489
    move-result v3

    #@48a
    if-nez v3, :cond_494

    #@48c
    const-string v3, "com.lge.music.playbackcomplete"

    #@48e
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@491
    move-result v3

    #@492
    if-eqz v3, :cond_39

    #@494
    .line 510
    :cond_494
    move-object/from16 v0, p0

    #@496
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@498
    const-string v18, "playing"

    #@49a
    const/16 v19, 0x0

    #@49c
    move-object/from16 v0, p2

    #@49e
    move-object/from16 v1, v18

    #@4a0
    move/from16 v2, v19

    #@4a2
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@4a5
    move-result v18

    #@4a6
    move/from16 v0, v18

    #@4a8
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$1802(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;Z)Z

    #@4ab
    .line 511
    move-object/from16 v0, p0

    #@4ad
    iget-object v3, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@4af
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@4b2
    move-result-object v3

    #@4b3
    move-object/from16 v0, p0

    #@4b5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@4b7
    move-object/from16 v18, v0

    #@4b9
    invoke-static/range {v18 .. v18}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->access$800(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;)Landroid/os/Handler;

    #@4bc
    move-result-object v18

    #@4bd
    const/16 v19, 0x192

    #@4bf
    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4c2
    move-result-object v18

    #@4c3
    move-object/from16 v0, v18

    #@4c5
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@4c8
    goto/16 :goto_39
.end method
