.class public Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;
.super Ljava/lang/Object;
.source "LgeKeyguardPackageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IEffectSurfaceDrawingCompleteListener;,
        Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field public static final CAPP_LOCKSCREEN:Z = false

.field private static final DBG:Z = true

.field private static final DEFAULT_LOCKSCREEN_PACKAGE_NAME:Ljava/lang/String; = "com.lge.lockscreen"

.field public static final EFFECT_VIEW_TAG:Ljava/lang/String; = "EffectSurfaceView"

.field private static final LOCKSCREEN_FACTORY_CLASS_NAME:Ljava/lang/String; = "LgeKeyguardHostViewFactoryImpl"

.field private static final LOCKSCREE_HOSTVIEW_LAYOUT_NAME:Ljava/lang/String; = "lge_keyguard_host_view"

.field private static final MSG_PACKAGE_ADDED:I = 0x64

.field private static final MSG_PACKAGE_REMOVED:I = 0x65

.field private static final MSG_PACKAGE_REPLACED:I = 0x66

.field private static final QUICK_COVER_WINDOW_HOSTVIEW_LAYOUT_NAME:Ljava/lang/String; = "lge_quick_cover_window_host_view"

.field private static final TAG:Ljava/lang/String; = "LgeKeyguardPackageManager"

.field private static mInstance:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;


# instance fields
.field private final mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

.field private mHostViewID:I

.field private mHostViewLayoutID:I

.field private mIsKeyguardEffectEnabled:Z

.field private mIsNaviRemoved:Z

.field private mIsPatternEffectEnabled:Z

.field private mIsScreenDecoTransparent:Z

.field private mIsScreenRotationEnabled:Z

.field private mIsSupportForgotPinPassword:Z

.field private mIsSupportQuickCover:Z

.field private mIsSupportQuickCoverWindow:Z

.field private mIsUseHardwareAcceleration:Z

.field private mPackageContext:Landroid/content/Context;

.field private mPackageName:Ljava/lang/String;

.field private mQuickCoverWindowHostViewID:I

.field private mQuickCoverWindowHostViewLayoutID:I

.field private mShouldShowSwipeBeforeSecure:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@2
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@4
    .line 93
    const/4 v0, 0x0

    #@5
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mInstance:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 108
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 51
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageName:Ljava/lang/String;

    #@7
    .line 52
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@9
    .line 54
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@b
    .line 56
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@d
    .line 57
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@f
    .line 59
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewLayoutID:I

    #@11
    .line 60
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@13
    .line 62
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenRotationEnabled:Z

    #@15
    .line 63
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsUseHardwareAcceleration:Z

    #@17
    .line 64
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsKeyguardEffectEnabled:Z

    #@19
    .line 65
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mShouldShowSwipeBeforeSecure:Z

    #@1b
    .line 66
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenDecoTransparent:Z

    #@1d
    .line 67
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportForgotPinPassword:Z

    #@1f
    .line 68
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsPatternEffectEnabled:Z

    #@21
    .line 72
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsNaviRemoved:Z

    #@23
    .line 74
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCoverWindow:Z

    #@25
    .line 75
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCover:Z

    #@27
    .line 76
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$1;

    #@29
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$1;-><init>(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)V

    #@2c
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHandler:Landroid/os/Handler;

    #@2e
    .line 224
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@31
    move-result-object v1

    #@32
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@34
    .line 109
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mContext:Landroid/content/Context;

    #@36
    .line 111
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->loadLockScreenPackage(Landroid/content/Context;)Z

    #@39
    .line 113
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@3b
    if-eqz v1, :cond_42

    #@3d
    .line 114
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@3f
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onCreate()V

    #@42
    .line 117
    :cond_42
    new-instance v0, Landroid/content/IntentFilter;

    #@44
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@47
    .line 118
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    #@49
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4c
    .line 119
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    #@4e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@51
    .line 120
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    #@53
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@56
    .line 121
    const-string v1, "package"

    #@58
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@5b
    .line 123
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;

    #@5d
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$2;-><init>(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)V

    #@60
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@63
    .line 160
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->handlePackageAdded(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->handlePackageRemoved(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->handlePackageReplaced(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public static getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;
    .registers 1

    #@0
    .prologue
    .line 105
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mInstance:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@2
    return-object v0
.end method

.method private handlePackageAdded(Ljava/lang/String;)V
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 285
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 286
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@a
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onDestory()V

    #@d
    .line 289
    :cond_d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mContext:Landroid/content/Context;

    #@f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->loadLockScreenPackage(Landroid/content/Context;)Z

    #@12
    .line 291
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 292
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@18
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onCreate()V

    #@1b
    .line 295
    :cond_1b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->notifyPackageStateChanged()V

    #@1e
    .line 296
    return-void
.end method

.method private handlePackageRemoved(Ljava/lang/String;)V
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 299
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 300
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@8
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onDestory()V

    #@b
    .line 303
    :cond_b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageName:Ljava/lang/String;

    #@d
    .line 304
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@f
    .line 305
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@11
    .line 306
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@13
    .line 307
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@15
    .line 308
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->notifyPackageStateChanged()V

    #@18
    .line 309
    return-void
.end method

.method private handlePackageReplaced(Ljava/lang/String;)V
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 313
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onDestory()V

    #@9
    .line 316
    :cond_9
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mContext:Landroid/content/Context;

    #@b
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->loadLockScreenPackage(Landroid/content/Context;)Z

    #@e
    .line 318
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 319
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@14
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onCreate()V

    #@17
    .line 321
    :cond_17
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->notifyPackageStateChanged()V

    #@1a
    .line 322
    return-void
.end method

.method public static initialize(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mInstance:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 97
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@6
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mInstance:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@b
    .line 99
    :cond_b
    return-void
.end method

.method private loadLockScreenPackage(Landroid/content/Context;)Z
    .registers 15
    .parameter "context"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 164
    :try_start_3
    const-string v8, "com.lge.lockscreen"

    #@5
    const/4 v11, 0x3

    #@6
    invoke-virtual {p1, v8, v11}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@9
    move-result-object v8

    #@a
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@c
    .line 169
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@f
    move-result-object v7

    #@10
    .line 170
    .local v7, packageManager:Landroid/content/pm/PackageManager;
    const-string v8, "com.lge.lockscreen"

    #@12
    const/4 v11, 0x0

    #@13
    invoke-virtual {v7, v8, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@16
    move-result-object v6

    #@17
    .line 172
    .local v6, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@19
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageName:Ljava/lang/String;

    #@1b
    .line 174
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v8}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@20
    move-result-object v2

    #@21
    .line 175
    .local v2, classLoader:Ljava/lang/ClassLoader;
    const-string v8, "com.lge.lockscreen.LgeKeyguardHostViewFactoryImpl"

    #@23
    const/4 v11, 0x1

    #@24
    invoke-static {v8, v11, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@27
    move-result-object v5

    #@28
    .line 178
    .local v5, lockScreenFactroyclass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v8, 0x2

    #@29
    new-array v0, v8, [Ljava/lang/Class;

    #@2b
    .line 179
    .local v0, argClasses:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    const/4 v8, 0x0

    #@2c
    const-class v11, Landroid/content/Context;

    #@2e
    aput-object v11, v0, v8

    #@30
    .line 180
    const/4 v8, 0x1

    #@31
    const-class v11, Landroid/content/Context;

    #@33
    aput-object v11, v0, v8

    #@35
    .line 181
    invoke-virtual {v5, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@38
    move-result-object v4

    #@39
    .line 183
    .local v4, localConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    const/4 v8, 0x2

    #@3a
    new-array v1, v8, [Ljava/lang/Object;

    #@3c
    .line 184
    .local v1, argObjects:[Ljava/lang/Object;
    const/4 v8, 0x0

    #@3d
    aput-object p1, v1, v8

    #@3f
    .line 185
    const/4 v8, 0x1

    #@40
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@42
    aput-object v11, v1, v8

    #@44
    .line 187
    invoke-virtual {v4, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-result-object v8

    #@48
    check-cast v8, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@4a
    iput-object v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@4c
    .line 190
    const-string v8, "lge_keyguard_host_view"

    #@4e
    const-string v11, "layout"

    #@50
    invoke-virtual {p0, v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    move-result v8

    #@54
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@56
    .line 191
    const-string v8, "lge_keyguard_host_view"

    #@58
    const-string v11, "id"

    #@5a
    invoke-virtual {p0, v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    move-result v8

    #@5e
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@60
    .line 193
    const-string v8, "lge_quick_cover_window_host_view"

    #@62
    const-string v11, "layout"

    #@64
    invoke-virtual {p0, v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    move-result v8

    #@68
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewLayoutID:I

    #@6a
    .line 194
    const-string v8, "lge_quick_cover_window_host_view"

    #@6c
    const-string v11, "id"

    #@6e
    invoke-virtual {p0, v8, v11}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getResId(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    move-result v8

    #@72
    iput v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@74
    .line 196
    const-string v8, "config_feature_enable_screen_rotation"

    #@76
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@79
    move-result v8

    #@7a
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenRotationEnabled:Z

    #@7c
    .line 197
    const-string v8, "config_feature_low_tier"

    #@7e
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@81
    move-result v8

    #@82
    if-nez v8, :cond_c9

    #@84
    move v8, v9

    #@85
    :goto_85
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsUseHardwareAcceleration:Z

    #@87
    .line 198
    const-string v8, "config_feature_lockscreen_effect"

    #@89
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@8c
    move-result v8

    #@8d
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsKeyguardEffectEnabled:Z

    #@8f
    .line 199
    const-string v8, "config_feature_show_swipe_before_security"

    #@91
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@94
    move-result v8

    #@95
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mShouldShowSwipeBeforeSecure:Z

    #@97
    .line 200
    const-string v8, "config_feature_transparent_screen_deco"

    #@99
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@9c
    move-result v8

    #@9d
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenDecoTransparent:Z

    #@9f
    .line 201
    const-string v8, "config_feature_forgot_pin_password"

    #@a1
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@a4
    move-result v8

    #@a5
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportForgotPinPassword:Z

    #@a7
    .line 202
    const-string v8, "config_feature_pattern_effect"

    #@a9
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@ac
    move-result v8

    #@ad
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsPatternEffectEnabled:Z

    #@af
    .line 206
    const-string v8, "config_feature_remove_navibar"

    #@b1
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@b4
    move-result v8

    #@b5
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsNaviRemoved:Z

    #@b7
    .line 208
    const-string v8, "config_feature_quick_cover_window"

    #@b9
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@bc
    move-result v8

    #@bd
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCoverWindow:Z

    #@bf
    .line 209
    const-string v8, "config_feature_quick_cover"

    #@c1
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@c4
    move-result v8

    #@c5
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCover:Z
    :try_end_c7
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_c7} :catch_cb

    #@c7
    move v8, v9

    #@c8
    .line 220
    .end local v0           #argClasses:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .end local v1           #argObjects:[Ljava/lang/Object;
    .end local v2           #classLoader:Ljava/lang/ClassLoader;
    .end local v4           #localConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .end local v5           #lockScreenFactroyclass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v6           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v7           #packageManager:Landroid/content/pm/PackageManager;
    :goto_c8
    return v8

    #@c9
    .restart local v0       #argClasses:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .restart local v1       #argObjects:[Ljava/lang/Object;
    .restart local v2       #classLoader:Ljava/lang/ClassLoader;
    .restart local v4       #localConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .restart local v5       #lockScreenFactroyclass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .restart local v6       #packageInfo:Landroid/content/pm/PackageInfo;
    .restart local v7       #packageManager:Landroid/content/pm/PackageManager;
    :cond_c9
    move v8, v10

    #@ca
    .line 197
    goto :goto_85

    #@cb
    .line 211
    .end local v0           #argClasses:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .end local v1           #argObjects:[Ljava/lang/Object;
    .end local v2           #classLoader:Ljava/lang/ClassLoader;
    .end local v4           #localConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .end local v5           #lockScreenFactroyclass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v6           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v7           #packageManager:Landroid/content/pm/PackageManager;
    :catch_cb
    move-exception v3

    #@cc
    .line 213
    .local v3, e:Ljava/lang/Exception;
    const-string v8, "LgeKeyguardPackageManager"

    #@ce
    new-instance v9, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v11, "fail to load a lock screen package!"

    #@d5
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v9

    #@d9
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v9

    #@dd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v9

    #@e1
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e4
    .line 215
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageName:Ljava/lang/String;

    #@e6
    .line 216
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@e8
    .line 217
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@ea
    .line 218
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@ec
    .line 219
    iput v10, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@ee
    move v8, v10

    #@ef
    .line 220
    goto :goto_c8
.end method

.method private notifyPackageStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 275
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 276
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;

    #@17
    .line 277
    .local v0, cb:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;
    if-eqz v0, :cond_1c

    #@19
    .line 278
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;->onKeyguardPackageStateChanged()V

    #@1c
    .line 275
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_1

    #@1f
    .line 281
    .end local v0           #cb:Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;
    :cond_1f
    return-void
.end method


# virtual methods
.method public createKeyguardHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    .registers 13
    .parameter "context"
    .parameter "parent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 411
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@3
    if-eqz v6, :cond_7a

    #@5
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@7
    if-eqz v6, :cond_7a

    #@9
    .line 412
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@b
    if-eqz v6, :cond_11

    #@d
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@f
    if-nez v6, :cond_39

    #@11
    .line 413
    :cond_11
    const-string v6, "LgeKeyguardPackageManager"

    #@13
    new-instance v8, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v9, "fail to createKeyguardHostView "

    #@1a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v8

    #@1e
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@20
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    const-string v9, ","

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@2c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    move-object v6, v7

    #@38
    .line 435
    :goto_38
    return-object v6

    #@39
    .line 419
    :cond_39
    :try_start_39
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@3b
    invoke-virtual {p2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@3e
    move-result-object v4

    #@3f
    .line 420
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_44

    #@41
    .line 421
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@44
    .line 423
    :cond_44
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@46
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@49
    move-result-object v2

    #@4a
    .line 424
    .local v2, inflater:Landroid/view/LayoutInflater;
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewLayoutID:I

    #@4c
    const/4 v8, 0x0

    #@4d
    invoke-virtual {v2, v6, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@50
    move-result-object v5

    #@51
    .line 425
    .local v5, view:Landroid/view/View;
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@53
    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@56
    move-result-object v6

    #@57
    check-cast v6, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_59} :catch_5a

    #@59
    goto :goto_38

    #@5a
    .line 426
    .end local v2           #inflater:Landroid/view/LayoutInflater;
    .end local v4           #v:Landroid/view/View;
    .end local v5           #view:Landroid/view/View;
    :catch_5a
    move-exception v0

    #@5b
    .line 427
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LgeKeyguardPackageManager"

    #@5d
    const-string v8, "fail to createKeyguardHostView"

    #@5f
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 428
    new-instance v3, Ljava/io/StringWriter;

    #@64
    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    #@67
    .line 429
    .local v3, sw:Ljava/io/StringWriter;
    new-instance v6, Ljava/io/PrintWriter;

    #@69
    invoke-direct {v6, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@6c
    invoke-virtual {v0, v6}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    #@6f
    .line 430
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 431
    .local v1, exceptionAsStrting:Ljava/lang/String;
    const-string v6, "LgeKeyguardPackageManager"

    #@75
    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    move-object v6, v7

    #@79
    .line 432
    goto :goto_38

    #@7a
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #exceptionAsStrting:Ljava/lang/String;
    .end local v3           #sw:Ljava/io/StringWriter;
    :cond_7a
    move-object v6, v7

    #@7b
    .line 435
    goto :goto_38
.end method

.method public createQuickCoverHostView(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    .registers 13
    .parameter "context"
    .parameter "parent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 440
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@3
    if-eqz v6, :cond_7a

    #@5
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@7
    if-eqz v6, :cond_7a

    #@9
    .line 441
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewLayoutID:I

    #@b
    if-eqz v6, :cond_11

    #@d
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@f
    if-nez v6, :cond_39

    #@11
    .line 442
    :cond_11
    const-string v6, "LgeKeyguardPackageManager"

    #@13
    new-instance v8, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v9, "fail to createQuickCoverHostView "

    #@1a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v8

    #@1e
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewLayoutID:I

    #@20
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    const-string v9, ","

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@2c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    move-object v6, v7

    #@38
    .line 464
    :goto_38
    return-object v6

    #@39
    .line 448
    :cond_39
    :try_start_39
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@3b
    invoke-virtual {p2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@3e
    move-result-object v4

    #@3f
    .line 449
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_44

    #@41
    .line 450
    invoke-virtual {p2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@44
    .line 452
    :cond_44
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@46
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@49
    move-result-object v2

    #@4a
    .line 453
    .local v2, inflater:Landroid/view/LayoutInflater;
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewLayoutID:I

    #@4c
    const/4 v8, 0x0

    #@4d
    invoke-virtual {v2, v6, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@50
    move-result-object v5

    #@51
    .line 454
    .local v5, view:Landroid/view/View;
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mQuickCoverWindowHostViewID:I

    #@53
    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@56
    move-result-object v6

    #@57
    check-cast v6, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_59} :catch_5a

    #@59
    goto :goto_38

    #@5a
    .line 455
    .end local v2           #inflater:Landroid/view/LayoutInflater;
    .end local v4           #v:Landroid/view/View;
    .end local v5           #view:Landroid/view/View;
    :catch_5a
    move-exception v0

    #@5b
    .line 456
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LgeKeyguardPackageManager"

    #@5d
    const-string v8, "fail to createQuickCoverHostView"

    #@5f
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 457
    new-instance v3, Ljava/io/StringWriter;

    #@64
    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    #@67
    .line 458
    .local v3, sw:Ljava/io/StringWriter;
    new-instance v6, Ljava/io/PrintWriter;

    #@69
    invoke-direct {v6, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@6c
    invoke-virtual {v0, v6}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    #@6f
    .line 459
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 460
    .local v1, exceptionAsStrting:Ljava/lang/String;
    const-string v6, "LgeKeyguardPackageManager"

    #@75
    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    move-object v6, v7

    #@79
    .line 461
    goto :goto_38

    #@7a
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #exceptionAsStrting:Ljava/lang/String;
    .end local v3           #sw:Ljava/io/StringWriter;
    :cond_7a
    move-object v6, v7

    #@7b
    .line 464
    goto :goto_38
.end method

.method public destroyKeyguardHostView(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .registers 5
    .parameter "context"
    .parameter "parent"

    #@0
    .prologue
    .line 529
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewID:I

    #@2
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 530
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_11

    #@8
    move-object v1, v0

    #@9
    .line 531
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->cleanUp()V

    #@e
    .line 532
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@11
    .line 534
    :cond_11
    return-void
.end method

.method public getBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 253
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v0

    #@a
    .line 255
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getFeatureString(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "featureName"

    #@0
    .prologue
    .line 512
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@2
    if-eqz v4, :cond_35

    #@4
    .line 514
    :try_start_4
    const-string v3, "default"

    #@6
    .line 515
    .local v3, ret:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    .line 516
    .local v2, res:Landroid/content/res/Resources;
    const-string v4, "string"

    #@e
    const-string v5, "com.lge.lockscreen"

    #@10
    invoke-virtual {v2, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@13
    move-result v1

    #@14
    .line 518
    .local v1, id:I
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_17} :catch_19

    #@17
    move-result-object v3

    #@18
    .line 525
    .end local v1           #id:I
    .end local v2           #res:Landroid/content/res/Resources;
    .end local v3           #ret:Ljava/lang/String;
    :goto_18
    return-object v3

    #@19
    .line 520
    :catch_19
    move-exception v0

    #@1a
    .line 521
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "LgeKeyguardPackageManager"

    #@1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v6, "fail to get feature\'s value : "

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 522
    const-string v3, "default"

    #@34
    goto :goto_18

    #@35
    .line 525
    .end local v0           #e:Ljava/lang/Exception;
    :cond_35
    const-string v3, "default"

    #@37
    goto :goto_18
.end method

.method public getPackageContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 493
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getQuickCoverWidgetIds()[I
    .registers 2

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 260
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->getQuickCoverWidgetIds()[I

    #@9
    move-result-object v0

    #@a
    .line 262
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getResId(Ljava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "name"
    .parameter "defType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 497
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@3
    if-eqz v3, :cond_11

    #@5
    .line 499
    :try_start_5
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@7
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v2

    #@b
    .line 500
    .local v2, res:Landroid/content/res/Resources;
    const-string v3, "com.lge.lockscreen"

    #@d
    invoke-virtual {v2, p1, p2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_10} :catch_12

    #@10
    move-result v1

    #@11
    .line 508
    .end local v2           #res:Landroid/content/res/Resources;
    :cond_11
    :goto_11
    return v1

    #@12
    .line 503
    :catch_12
    move-exception v0

    #@13
    .line 504
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LgeKeyguardPackageManager"

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "fail to get resource "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_11
.end method

.method public getStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .registers 9
    .parameter "arrayName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 476
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@3
    if-eqz v4, :cond_17

    #@5
    .line 479
    :try_start_5
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@7
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v2

    #@b
    .line 480
    .local v2, res:Landroid/content/res/Resources;
    const-string v4, "array"

    #@d
    const-string v5, "com.lge.lockscreen"

    #@f
    invoke-virtual {v2, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@12
    move-result v1

    #@13
    .line 482
    .local v1, id:I
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_16} :catch_18

    #@16
    move-result-object v3

    #@17
    .line 489
    .end local v1           #id:I
    .end local v2           #res:Landroid/content/res/Resources;
    :cond_17
    :goto_17
    return-object v3

    #@18
    .line 484
    :catch_18
    move-exception v0

    #@19
    .line 485
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "LgeKeyguardPackageManager"

    #@1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v6, "fail to get resource "

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_17
.end method

.method public getVersionNumber()I
    .registers 3

    #@0
    .prologue
    .line 345
    const/4 v0, -0x1

    #@1
    .line 347
    .local v0, version:I
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@3
    if-eqz v1, :cond_b

    #@5
    .line 348
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@7
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->getVersionNumber()I

    #@a
    move-result v0

    #@b
    .line 351
    :cond_b
    return v0
.end method

.method public isFeatureEnabled(Ljava/lang/String;)Z
    .registers 10
    .parameter "featureName"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 325
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@3
    if-eqz v5, :cond_34

    #@5
    .line 327
    const/4 v3, 0x0

    #@6
    .line 328
    .local v3, ret:Z
    :try_start_6
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mPackageContext:Landroid/content/Context;

    #@8
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    .line 329
    .local v2, res:Landroid/content/res/Resources;
    const-string v5, "bool"

    #@e
    const-string v6, "com.lge.lockscreen"

    #@10
    invoke-virtual {v2, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@13
    move-result v1

    #@14
    .line 331
    .local v1, id:I
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_17} :catch_19

    #@17
    move-result v3

    #@18
    .line 338
    .end local v1           #id:I
    .end local v2           #res:Landroid/content/res/Resources;
    .end local v3           #ret:Z
    :goto_18
    return v3

    #@19
    .line 333
    .restart local v3       #ret:Z
    :catch_19
    move-exception v0

    #@1a
    .line 334
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LgeKeyguardPackageManager"

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "fail to get resource "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    move v3, v4

    #@33
    .line 335
    goto :goto_18

    #@34
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #ret:Z
    :cond_34
    move v3, v4

    #@35
    .line 338
    goto :goto_18
.end method

.method public isHardwareAccelerated()Z
    .registers 2

    #@0
    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsUseHardwareAcceleration:Z

    #@2
    return v0
.end method

.method public isKeyguardEffectEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsKeyguardEffectEnabled:Z

    #@2
    return v0
.end method

.method public isNaviBarRemoved()Z
    .registers 2

    #@0
    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsNaviRemoved:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 396
    const/4 v0, 0x1

    #@5
    .line 398
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isPatternEffectEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsPatternEffectEnabled:Z

    #@2
    return v0
.end method

.method public isScreenDecoTransparent()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 371
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenDecoTransparent:Z

    #@4
    if-eqz v4, :cond_1b

    #@6
    .line 373
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    .line 374
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v1, 0x1

    #@d
    .line 375
    .local v1, transparentSystemBarEnabled:Z
    if-eqz v0, :cond_18

    #@f
    .line 376
    const-string v4, "navigation_bar_option"

    #@11
    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14
    move-result v4

    #@15
    if-ne v4, v2, :cond_19

    #@17
    move v1, v2

    #@18
    .line 381
    .end local v0           #resolver:Landroid/content/ContentResolver;
    .end local v1           #transparentSystemBarEnabled:Z
    :cond_18
    :goto_18
    return v1

    #@19
    .restart local v0       #resolver:Landroid/content/ContentResolver;
    .restart local v1       #transparentSystemBarEnabled:Z
    :cond_19
    move v1, v3

    #@1a
    .line 376
    goto :goto_18

    #@1b
    .end local v0           #resolver:Landroid/content/ContentResolver;
    .end local v1           #transparentSystemBarEnabled:Z
    :cond_1b
    move v1, v3

    #@1c
    .line 381
    goto :goto_18
.end method

.method public isScreenRotationEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsScreenRotationEnabled:Z

    #@2
    return v0
.end method

.method public isSupportForgotPinPassword()Z
    .registers 2

    #@0
    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportForgotPinPassword:Z

    #@2
    return v0
.end method

.method public isSupportQuickCover()Z
    .registers 2

    #@0
    .prologue
    .line 407
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCover:Z

    #@2
    return v0
.end method

.method public isSupportQuickCoverWindow()Z
    .registers 2

    #@0
    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mIsSupportQuickCoverWindow:Z

    #@2
    return v0
.end method

.method public notifyKeyguardDone()V
    .registers 2

    #@0
    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 470
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->onKeyguardDone()V

    #@9
    .line 472
    :cond_9
    return-void
.end method

.method registerPackageStateChangedCallback(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IKeyguardPackageStateChanged;)V
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    .line 266
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1b

    #@9
    .line 267
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@11
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    if-ne v1, p1, :cond_18

    #@17
    .line 272
    :goto_17
    return-void

    #@18
    .line 266
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 271
    :cond_1b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mCallbacks:Ljava/util/ArrayList;

    #@1d
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@1f
    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@22
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    goto :goto_17
.end method

.method public shouldShowSwipeBeforeSecure()Z
    .registers 2

    #@0
    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mShouldShowSwipeBeforeSecure:Z

    #@2
    return v0
.end method

.method public waitForEffectSurfaceDrawn(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IEffectSurfaceDrawingCompleteListener;)Z
    .registers 3
    .parameter "drawingCompleteListener"

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 246
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->mHostViewFactory:Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard/ILgeKeyguardHostViewFactory;->waitForEffectSurfaceDrawn(Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager$IEffectSurfaceDrawingCompleteListener;)Z

    #@9
    move-result v0

    #@a
    .line 248
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method
