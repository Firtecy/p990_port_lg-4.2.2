.class public Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;
.source "MSimKeyguardSimPukView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MSimStateMachine"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;


# direct methods
.method protected constructor <init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 38
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)V

    #@5
    return-void
.end method


# virtual methods
.method public next()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    .line 40
    const/4 v0, 0x0

    #@3
    .line 41
    .local v0, msg:I
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@5
    if-nez v1, :cond_30

    #@7
    .line 42
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->checkPuk()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2c

    #@f
    .line 43
    sput v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@11
    .line 44
    const v0, 0x1040555

    #@14
    .line 66
    :cond_14
    :goto_14
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@16
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1c
    .line 67
    if-eqz v0, :cond_2b

    #@1e
    .line 68
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@20
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@22
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@24
    invoke-virtual {v2, v0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@27
    move-result-object v2

    #@28
    invoke-interface {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@2b
    .line 70
    :cond_2b
    return-void

    #@2c
    .line 46
    :cond_2c
    const v0, 0x104055a

    #@2f
    goto :goto_14

    #@30
    .line 48
    :cond_30
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@32
    if-ne v1, v3, :cond_46

    #@34
    .line 49
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@36
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->checkPin()Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_42

    #@3c
    .line 50
    sput v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@3e
    .line 51
    const v0, 0x1040556

    #@41
    goto :goto_14

    #@42
    .line 53
    :cond_42
    const v0, 0x1040559

    #@45
    goto :goto_14

    #@46
    .line 55
    :cond_46
    sget v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@48
    if-ne v1, v2, :cond_14

    #@4a
    .line 56
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@4c
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->confirmPin()Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_5e

    #@52
    .line 57
    const/4 v1, 0x3

    #@53
    sput v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@55
    .line 58
    const v0, 0x104032d

    #@58
    .line 60
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@5a
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->updateSim()V

    #@5d
    goto :goto_14

    #@5e
    .line 62
    :cond_5e
    sput v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@60
    .line 63
    const v0, 0x104055c

    #@63
    goto :goto_14
.end method

.method reset()V
    .registers 4

    #@0
    .prologue
    .line 73
    const-string v0, ""

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@4
    .line 74
    const-string v0, ""

    #@6
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mPukText:Ljava/lang/String;

    #@8
    .line 75
    const/4 v0, 0x0

    #@9
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->state:I

    #@b
    .line 76
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@11
    const v2, 0x1040554

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@17
    move-result-object v1

    #@18
    const/4 v2, 0x1

    #@19
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@1c
    .line 78
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@1e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@20
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    #@23
    .line 79
    return-void
.end method
