.class Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;
.super Ljava/lang/Object;
.source "SimUnlockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->onSimLockChangedResponse(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 218
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@2
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@4
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/app/ProgressDialog;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_15

    #@a
    .line 221
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@c
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/app/ProgressDialog;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    #@15
    .line 223
    :cond_15
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->val$success:Z

    #@17
    if-eqz v0, :cond_3b

    #@19
    .line 226
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@1b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@1d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->reportSimUnlocked()V

    #@24
    .line 227
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@26
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@28
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2b
    move-result-object v0

    #@2c
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToUnlockScreen()V

    #@2f
    .line 233
    :goto_2f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@31
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@33
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@36
    move-result-object v0

    #@37
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@3a
    .line 234
    return-void

    #@3b
    .line 229
    :cond_3b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@3d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@3f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/widget/TextView;

    #@42
    move-result-object v0

    #@43
    const v1, 0x1040303

    #@46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@49
    .line 230
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@4b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@4d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/widget/TextView;

    #@50
    move-result-object v0

    #@51
    const-string v1, ""

    #@53
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@56
    .line 231
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@58
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@5a
    const/4 v1, 0x0

    #@5b
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;I)I

    #@5e
    goto :goto_2f
.end method
