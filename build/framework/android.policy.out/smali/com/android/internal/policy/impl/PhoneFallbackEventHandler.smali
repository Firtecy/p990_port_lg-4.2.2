.class public Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;
.super Ljava/lang/Object;
.source "PhoneFallbackEventHandler.java"

# interfaces
.implements Landroid/view/FallbackEventHandler;


# static fields
.field private static final DEBUG:Z

.field private static TAG:Ljava/lang/String;


# instance fields
.field mAudioManager:Landroid/media/AudioManager;

.field mContext:Landroid/content/Context;

.field mKeyguardManager:Landroid/app/KeyguardManager;

.field mSearchManager:Landroid/app/SearchManager;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 43
    const-string v0, "PhoneFallbackEventHandler"

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@5
    .line 56
    return-void
.end method

.method private handleMediaKeyEvent(Landroid/view/KeyEvent;)V
    .registers 7
    .parameter "keyEvent"

    #@0
    .prologue
    .line 303
    const-string v2, "audio"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    .line 305
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_2a

    #@c
    .line 307
    :try_start_c
    invoke-interface {v0, p1}, Landroid/media/IAudioService;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_10

    #@f
    .line 314
    :goto_f
    return-void

    #@10
    .line 308
    :catch_10
    move-exception v1

    #@11
    .line 309
    .local v1, e:Landroid/os/RemoteException;
    sget-object v2, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "dispatchMediaKeyEvent threw exception "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_f

    #@2a
    .line 312
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_2a
    sget-object v2, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    #@2c
    const-string v3, "Unable to find IAudioService for media key event."

    #@2e
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_f
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3
    move-result v0

    #@4
    .line 69
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v1

    #@8
    .line 71
    .local v1, keyCode:I
    if-nez v0, :cond_f

    #@a
    .line 72
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v2

    #@e
    .line 74
    :goto_e
    return v2

    #@f
    :cond_f
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@12
    move-result v2

    #@13
    goto :goto_e
.end method

.method getAudioManager()Landroid/media/AudioManager;
    .registers 3

    #@0
    .prologue
    .line 292
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 293
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "audio"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/media/AudioManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    #@10
    .line 295
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    #@12
    return-object v0
.end method

.method getKeyguardManager()Landroid/app/KeyguardManager;
    .registers 3

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 286
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "keyguard"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/KeyguardManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@10
    .line 288
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@12
    return-object v0
.end method

.method getSearchManager()Landroid/app/SearchManager;
    .registers 3

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 271
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "search"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/SearchManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    #@10
    .line 273
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    #@12
    return-object v0
.end method

.method getTelephonyManager()Landroid/telephony/TelephonyManager;
    .registers 3

    #@0
    .prologue
    .line 277
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 278
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "phone"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@10
    .line 281
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@12
    return-object v0
.end method

.method onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 16
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v12, 0x1

    #@5
    .line 83
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@7
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@a
    move-result-object v10

    #@b
    .line 85
    .local v10, dispatcher:Landroid/view/KeyEvent$DispatcherState;
    sparse-switch p1, :sswitch_data_112

    #@e
    .line 186
    :cond_e
    :goto_e
    return v6

    #@f
    .line 89
    :sswitch_f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getAudioManager()Landroid/media/AudioManager;

    #@12
    move-result-object v0

    #@13
    const/high16 v2, -0x8000

    #@15
    invoke-virtual {v0, p2, v2}, Landroid/media/AudioManager;->handleKeyDown(Landroid/view/KeyEvent;I)V

    #@18
    move v6, v12

    #@19
    .line 90
    goto :goto_e

    #@1a
    .line 99
    :sswitch_1a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_26

    #@24
    move v6, v12

    #@25
    .line 100
    goto :goto_e

    #@26
    .line 110
    :cond_26
    :sswitch_26
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    #@29
    move v6, v12

    #@2a
    .line 111
    goto :goto_e

    #@2b
    .line 115
    :sswitch_2b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@32
    move-result v0

    #@33
    if-nez v0, :cond_e

    #@35
    if-eqz v10, :cond_e

    #@37
    .line 118
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@3a
    move-result v0

    #@3b
    if-nez v0, :cond_42

    #@3d
    .line 119
    invoke-virtual {v10, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@40
    :cond_40
    :goto_40
    move v6, v12

    #@41
    .line 133
    goto :goto_e

    #@42
    .line 120
    :cond_42
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_40

    #@48
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_40

    #@4e
    .line 121
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V

    #@51
    .line 122
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@53
    invoke-virtual {v0, v6}, Landroid/view/View;->performHapticFeedback(I)Z

    #@56
    .line 124
    new-instance v1, Landroid/content/Intent;

    #@58
    const-string v0, "android.intent.action.VOICE_COMMAND"

    #@5a
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5d
    .line 125
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@60
    .line 127
    :try_start_60
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->sendCloseSystemWindows()V

    #@63
    .line 128
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@65
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_68
    .catch Landroid/content/ActivityNotFoundException; {:try_start_60 .. :try_end_68} :catch_69

    #@68
    goto :goto_40

    #@69
    .line 129
    :catch_69
    move-exception v11

    #@6a
    .line 130
    .local v11, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->startCallActivity()V

    #@6d
    goto :goto_40

    #@6e
    .line 137
    .end local v1           #intent:Landroid/content/Intent;
    .end local v11           #e:Landroid/content/ActivityNotFoundException;
    :sswitch_6e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@75
    move-result v0

    #@76
    if-nez v0, :cond_e

    #@78
    if-eqz v10, :cond_e

    #@7a
    .line 140
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@7d
    move-result v0

    #@7e
    if-nez v0, :cond_85

    #@80
    .line 141
    invoke-virtual {v10, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@83
    :cond_83
    :goto_83
    move v6, v12

    #@84
    .line 152
    goto :goto_e

    #@85
    .line 142
    :cond_85
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    #@88
    move-result v0

    #@89
    if-eqz v0, :cond_83

    #@8b
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    #@8e
    move-result v0

    #@8f
    if-eqz v0, :cond_83

    #@91
    .line 143
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V

    #@94
    .line 144
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@96
    invoke-virtual {v0, v6}, Landroid/view/View;->performHapticFeedback(I)Z

    #@99
    .line 145
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->sendCloseSystemWindows()V

    #@9c
    .line 147
    new-instance v1, Landroid/content/Intent;

    #@9e
    const-string v0, "android.intent.action.CAMERA_BUTTON"

    #@a0
    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@a3
    .line 148
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v0, "android.intent.extra.KEY_EVENT"

    #@a5
    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@a8
    .line 149
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@aa
    sget-object v2, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    #@ac
    move-object v4, v3

    #@ad
    move-object v5, v3

    #@ae
    move-object v7, v3

    #@af
    move-object v8, v3

    #@b0
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@b3
    goto :goto_83

    #@b4
    .line 156
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_b4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@bb
    move-result v0

    #@bc
    if-nez v0, :cond_e

    #@be
    if-eqz v10, :cond_e

    #@c0
    .line 159
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@c3
    move-result v0

    #@c4
    if-nez v0, :cond_cb

    #@c6
    .line 160
    invoke-virtual {v10, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@c9
    goto/16 :goto_e

    #@cb
    .line 161
    :cond_cb
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    #@ce
    move-result v0

    #@cf
    if-eqz v0, :cond_e

    #@d1
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    #@d4
    move-result v0

    #@d5
    if-eqz v0, :cond_e

    #@d7
    .line 162
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@d9
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@e0
    move-result-object v9

    #@e1
    .line 163
    .local v9, config:Landroid/content/res/Configuration;
    iget v0, v9, Landroid/content/res/Configuration;->keyboard:I

    #@e3
    if-eq v0, v12, :cond_ea

    #@e5
    iget v0, v9, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@e7
    const/4 v2, 0x2

    #@e8
    if-ne v0, v2, :cond_e

    #@ea
    .line 166
    :cond_ea
    new-instance v1, Landroid/content/Intent;

    #@ec
    const-string v0, "android.intent.action.SEARCH_LONG_PRESS"

    #@ee
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@f1
    .line 167
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@f4
    .line 169
    :try_start_f4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@f6
    const/4 v2, 0x0

    #@f7
    invoke-virtual {v0, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    #@fa
    .line 170
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->sendCloseSystemWindows()V

    #@fd
    .line 171
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getSearchManager()Landroid/app/SearchManager;

    #@100
    move-result-object v0

    #@101
    invoke-virtual {v0}, Landroid/app/SearchManager;->stopSearch()V

    #@104
    .line 172
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@106
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@109
    .line 176
    invoke-virtual {v10, p2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V
    :try_end_10c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_f4 .. :try_end_10c} :catch_10f

    #@10c
    move v6, v12

    #@10d
    .line 177
    goto/16 :goto_e

    #@10f
    .line 178
    :catch_10f
    move-exception v0

    #@110
    goto/16 :goto_e

    #@112
    .line 85
    :sswitch_data_112
    .sparse-switch
        0x5 -> :sswitch_2b
        0x18 -> :sswitch_f
        0x19 -> :sswitch_f
        0x1b -> :sswitch_6e
        0x4f -> :sswitch_26
        0x54 -> :sswitch_b4
        0x55 -> :sswitch_1a
        0x56 -> :sswitch_26
        0x57 -> :sswitch_26
        0x58 -> :sswitch_26
        0x59 -> :sswitch_26
        0x5a -> :sswitch_26
        0x5b -> :sswitch_26
        0x7e -> :sswitch_1a
        0x7f -> :sswitch_1a
        0x82 -> :sswitch_26
        0xa4 -> :sswitch_f
    .end sparse-switch
.end method

.method onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 193
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@3
    invoke-virtual {v3}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@6
    move-result-object v1

    #@7
    .line 194
    .local v1, dispatcher:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v1, :cond_c

    #@9
    .line 195
    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@c
    .line 198
    :cond_c
    sparse-switch p1, :sswitch_data_62

    #@f
    .line 247
    :cond_f
    const/4 v2, 0x0

    #@10
    :cond_10
    :goto_10
    return v2

    #@11
    .line 202
    :sswitch_11
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_10

    #@17
    .line 203
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@19
    const-string v4, "audio"

    #@1b
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/media/AudioManager;

    #@21
    .line 205
    .local v0, audioManager:Landroid/media/AudioManager;
    if-eqz v0, :cond_10

    #@23
    .line 206
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getAudioManager()Landroid/media/AudioManager;

    #@26
    move-result-object v3

    #@27
    const/high16 v4, -0x8000

    #@29
    invoke-virtual {v3, p2, v4}, Landroid/media/AudioManager;->handleKeyUp(Landroid/view/KeyEvent;I)V

    #@2c
    goto :goto_10

    #@2d
    .line 223
    .end local v0           #audioManager:Landroid/media/AudioManager;
    :sswitch_2d
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    #@30
    goto :goto_10

    #@31
    .line 228
    :sswitch_31
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@38
    move-result v3

    #@39
    if-nez v3, :cond_f

    #@3b
    .line 231
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_10

    #@41
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@44
    move-result v3

    #@45
    if-nez v3, :cond_10

    #@47
    goto :goto_10

    #@48
    .line 238
    :sswitch_48
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@4f
    move-result v3

    #@50
    if-nez v3, :cond_f

    #@52
    .line 241
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@55
    move-result v3

    #@56
    if-eqz v3, :cond_10

    #@58
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@5b
    move-result v3

    #@5c
    if-nez v3, :cond_10

    #@5e
    .line 242
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->startCallActivity()V

    #@61
    goto :goto_10

    #@62
    .line 198
    :sswitch_data_62
    .sparse-switch
        0x5 -> :sswitch_48
        0x18 -> :sswitch_11
        0x19 -> :sswitch_11
        0x1b -> :sswitch_31
        0x4f -> :sswitch_2d
        0x55 -> :sswitch_2d
        0x56 -> :sswitch_2d
        0x57 -> :sswitch_2d
        0x58 -> :sswitch_2d
        0x59 -> :sswitch_2d
        0x5a -> :sswitch_2d
        0x5b -> :sswitch_2d
        0x7e -> :sswitch_2d
        0x7f -> :sswitch_2d
        0x82 -> :sswitch_2d
        0xa4 -> :sswitch_11
    .end sparse-switch
.end method

.method public preDispatchKeyEvent(Landroid/view/KeyEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->getAudioManager()Landroid/media/AudioManager;

    #@3
    move-result-object v0

    #@4
    const/high16 v1, -0x8000

    #@6
    invoke-virtual {v0, p1, v1}, Landroid/media/AudioManager;->preDispatchKeyEvent(Landroid/view/KeyEvent;I)V

    #@9
    .line 64
    return-void
.end method

.method sendCloseSystemWindows()V
    .registers 3

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V

    #@6
    .line 300
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    #@2
    .line 60
    return-void
.end method

.method startCallActivity()V
    .registers 5

    #@0
    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->sendCloseSystemWindows()V

    #@3
    .line 259
    new-instance v1, Landroid/content/Intent;

    #@5
    const-string v2, "android.intent.action.DIAL"

    #@7
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a
    .line 261
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@c
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@f
    .line 263
    :try_start_f
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_14
    .catch Landroid/content/ActivityNotFoundException; {:try_start_f .. :try_end_14} :catch_15

    #@14
    .line 267
    :goto_14
    return-void

    #@15
    .line 264
    :catch_15
    move-exception v0

    #@16
    .line 265
    .local v0, e:Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/android/internal/policy/impl/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    #@18
    const-string v3, "No activity found for android.intent.action.CALL_BUTTON."

    #@1a
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_14
.end method
