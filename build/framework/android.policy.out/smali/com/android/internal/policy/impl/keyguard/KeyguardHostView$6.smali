.class Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$6;
.super Ljava/lang/Object;
.source "KeyguardHostView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1021
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public dismiss(Z)V
    .registers 2
    .parameter "securityVerified"

    #@0
    .prologue
    .line 1055
    return-void
.end method

.method public forgotPattern(Z)V
    .registers 2
    .parameter "isPermanentlyLocked"

    #@0
    .prologue
    .line 1066
    return-void
.end method

.method public getFailedAttempts()I
    .registers 2

    #@0
    .prologue
    .line 1050
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isVerifyUnlockOnly()Z
    .registers 2

    #@0
    .prologue
    .line 1045
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public reportFailedUnlockAttempt()V
    .registers 1

    #@0
    .prologue
    .line 1041
    return-void
.end method

.method public reportSuccessfulUnlockAttempt()V
    .registers 1

    #@0
    .prologue
    .line 1037
    return-void
.end method

.method public setOnDismissAction(Lcom/android/internal/policy/impl/keyguard/OnDismissAction;)V
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 1033
    return-void
.end method

.method public showBackupSecurity()V
    .registers 1

    #@0
    .prologue
    .line 1029
    return-void
.end method

.method public showDialogFailedUnlockAttempt(I)V
    .registers 2
    .parameter "remainingBeforeWipe"

    #@0
    .prologue
    .line 1060
    return-void
.end method

.method public userActivity(J)V
    .registers 3
    .parameter "timeout"

    #@0
    .prologue
    .line 1025
    return-void
.end method
