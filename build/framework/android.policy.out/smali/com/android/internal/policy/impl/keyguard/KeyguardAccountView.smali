.class public Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;
.super Landroid/widget/LinearLayout;
.source "KeyguardAccountView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final AWAKE_POKE_MILLIS:I = 0x7530

.field private static final LOCK_PATTERN_CLASS:Ljava/lang/String; = "com.android.settings.ChooseLockGeneric"

.field private static final LOCK_PATTERN_PACKAGE:Ljava/lang/String; = "com.android.settings"


# instance fields
.field private mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

.field private mCheckingDialog:Landroid/app/ProgressDialog;

.field public mEnableFallback:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLogin:Landroid/widget/EditText;

.field private mOk:Landroid/widget/Button;

.field private mPassword:Landroid/widget/EditText;

.field private mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 76
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 81
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->getContext()Landroid/content/Context;

    #@8
    move-result-object v1

    #@9
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@e
    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mPassword:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->postOnCheckPasswordResult(Z)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/app/Dialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->getProgressDialog()Landroid/app/Dialog;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method private asyncCheckPassword()V
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 262
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@3
    const-wide/16 v4, 0x7530

    #@5
    invoke-interface {v0, v4, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@8
    .line 263
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@a
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    .line 264
    .local v7, login:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mPassword:Landroid/widget/EditText;

    #@14
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v8

    #@1c
    .line 265
    .local v8, password:Ljava/lang/String;
    invoke-direct {p0, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->findIntendedAccount(Ljava/lang/String;)Landroid/accounts/Account;

    #@1f
    move-result-object v1

    #@20
    .line 266
    .local v1, account:Landroid/accounts/Account;
    if-nez v1, :cond_27

    #@22
    .line 267
    const/4 v0, 0x0

    #@23
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->postOnCheckPasswordResult(Z)V

    #@26
    .line 296
    :goto_26
    return-void

    #@27
    .line 270
    :cond_27
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->getProgressDialog()Landroid/app/Dialog;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@2e
    .line 271
    new-instance v2, Landroid/os/Bundle;

    #@30
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@33
    .line 272
    .local v2, options:Landroid/os/Bundle;
    const-string v0, "password"

    #@35
    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 273
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mContext:Landroid/content/Context;

    #@3a
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@3d
    move-result-object v0

    #@3e
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$2;

    #@40
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;)V

    #@43
    new-instance v6, Landroid/os/UserHandle;

    #@45
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@47
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@4a
    move-result v5

    #@4b
    invoke-direct {v6, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@4e
    move-object v5, v3

    #@4f
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->confirmCredentialsAsUser(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;Landroid/os/UserHandle;)Landroid/accounts/AccountManagerFuture;

    #@52
    goto :goto_26
.end method

.method private findIntendedAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .registers 16
    .parameter "username"

    #@0
    .prologue
    .line 225
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@5
    move-result-object v10

    #@6
    const-string v11, "com.google"

    #@8
    new-instance v12, Landroid/os/UserHandle;

    #@a
    iget-object v13, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@c
    invoke-virtual {v13}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@f
    move-result v13

    #@10
    invoke-direct {v12, v13}, Landroid/os/UserHandle;-><init>(I)V

    #@13
    invoke-virtual {v10, v11, v12}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    #@16
    move-result-object v2

    #@17
    .line 232
    .local v2, accounts:[Landroid/accounts/Account;
    const/4 v4, 0x0

    #@18
    .line 233
    .local v4, bestAccount:Landroid/accounts/Account;
    const/4 v5, 0x0

    #@19
    .line 234
    .local v5, bestScore:I
    move-object v3, v2

    #@1a
    .local v3, arr$:[Landroid/accounts/Account;
    array-length v8, v3

    #@1b
    .local v8, len$:I
    const/4 v7, 0x0

    #@1c
    .local v7, i$:I
    :goto_1c
    if-ge v7, v8, :cond_68

    #@1e
    aget-object v0, v3, v7

    #@20
    .line 235
    .local v0, a:Landroid/accounts/Account;
    const/4 v9, 0x0

    #@21
    .line 236
    .local v9, score:I
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@23
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v10

    #@27
    if-eqz v10, :cond_31

    #@29
    .line 237
    const/4 v9, 0x4

    #@2a
    .line 251
    :cond_2a
    :goto_2a
    if-le v9, v5, :cond_64

    #@2c
    .line 252
    move-object v4, v0

    #@2d
    .line 253
    move v5, v9

    #@2e
    .line 234
    :cond_2e
    :goto_2e
    add-int/lit8 v7, v7, 0x1

    #@30
    goto :goto_1c

    #@31
    .line 238
    :cond_31
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@33
    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@36
    move-result v10

    #@37
    if-eqz v10, :cond_3b

    #@39
    .line 239
    const/4 v9, 0x3

    #@3a
    goto :goto_2a

    #@3b
    .line 240
    :cond_3b
    const/16 v10, 0x40

    #@3d
    invoke-virtual {p1, v10}, Ljava/lang/String;->indexOf(I)I

    #@40
    move-result v10

    #@41
    if-gez v10, :cond_2a

    #@43
    .line 241
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@45
    const/16 v11, 0x40

    #@47
    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I

    #@4a
    move-result v6

    #@4b
    .line 242
    .local v6, i:I
    if-ltz v6, :cond_2a

    #@4d
    .line 243
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@4f
    const/4 v11, 0x0

    #@50
    invoke-virtual {v10, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    .line 244
    .local v1, aUsername:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v10

    #@58
    if-eqz v10, :cond_5c

    #@5a
    .line 245
    const/4 v9, 0x2

    #@5b
    goto :goto_2a

    #@5c
    .line 246
    :cond_5c
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5f
    move-result v10

    #@60
    if-eqz v10, :cond_2a

    #@62
    .line 247
    const/4 v9, 0x1

    #@63
    goto :goto_2a

    #@64
    .line 254
    .end local v1           #aUsername:Ljava/lang/String;
    .end local v6           #i:I
    :cond_64
    if-ne v9, v5, :cond_2e

    #@66
    .line 255
    const/4 v4, 0x0

    #@67
    goto :goto_2e

    #@68
    .line 258
    .end local v0           #a:Landroid/accounts/Account;
    .end local v9           #score:I
    :cond_68
    return-object v4
.end method

.method private getProgressDialog()Landroid/app/Dialog;
    .registers 4

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@2
    if-nez v0, :cond_32

    #@4
    .line 300
    new-instance v0, Landroid/app/ProgressDialog;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@d
    .line 301
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mContext:Landroid/content/Context;

    #@11
    const v2, 0x1040564

    #@14
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@1b
    .line 303
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@1d
    const/4 v1, 0x1

    #@1e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@21
    .line 304
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@27
    .line 305
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@29
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@2c
    move-result-object v0

    #@2d
    const/16 v1, 0x7d9

    #@2f
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@32
    .line 308
    :cond_32
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@34
    return-object v0
.end method

.method private postOnCheckPasswordResult(Z)V
    .registers 4
    .parameter "success"

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;Z)V

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 192
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 116
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 119
    return-void
.end method

.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 150
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 151
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCheckingDialog:Landroid/app/ProgressDialog;

    #@7
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    #@a
    .line 153
    :cond_a
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@c
    .line 154
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@e
    .line 155
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1d

    #@6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x4

    #@b
    if-ne v0, v1, :cond_1d

    #@d
    .line 198
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1b

    #@15
    .line 199
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@17
    const/4 v1, 0x0

    #@18
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@1b
    .line 203
    :cond_1b
    const/4 v0, 0x1

    #@1c
    .line 205
    :goto_1c
    return v0

    #@1d
    :cond_1d
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@20
    move-result v0

    #@21
    goto :goto_1c
.end method

.method public getCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    return-object v0
.end method

.method public hideBouncer(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 331
    return-void
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 135
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    const-wide/16 v1, 0x0

    #@4
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@7
    .line 159
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mOk:Landroid/widget/Button;

    #@9
    if-ne p1, v0, :cond_e

    #@b
    .line 160
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->asyncCheckPassword()V

    #@e
    .line 162
    :cond_e
    return-void
.end method

.method protected onFinishInflate()V
    .registers 5

    #@0
    .prologue
    .line 86
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    #@3
    .line 88
    const v0, 0x10202b1

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/widget/EditText;

    #@c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@e
    .line 89
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@10
    const/4 v1, 0x1

    #@11
    new-array v1, v1, [Landroid/text/InputFilter;

    #@13
    const/4 v2, 0x0

    #@14
    new-instance v3, Landroid/text/LoginFilter$UsernameFilterGeneric;

    #@16
    invoke-direct {v3}, Landroid/text/LoginFilter$UsernameFilterGeneric;-><init>()V

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@1e
    .line 90
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@20
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@23
    .line 92
    const v0, 0x10202b2

    #@26
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->findViewById(I)Landroid/view/View;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Landroid/widget/EditText;

    #@2c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mPassword:Landroid/widget/EditText;

    #@2e
    .line 93
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mPassword:Landroid/widget/EditText;

    #@30
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@33
    .line 95
    const v0, 0x10202b3

    #@36
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->findViewById(I)Landroid/view/View;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/widget/Button;

    #@3c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mOk:Landroid/widget/Button;

    #@3e
    .line 96
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mOk:Landroid/widget/Button;

    #@40
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@43
    .line 98
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$Helper;

    #@45
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$Helper;-><init>(Landroid/view/View;)V

    #@48
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@4a
    .line 99
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->reset()V

    #@4d
    .line 100
    return-void
.end method

.method public onPause()V
    .registers 1

    #@0
    .prologue
    .line 314
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onResume(I)V
    .registers 2
    .parameter "reason"

    #@0
    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->reset()V

    #@3
    .line 319
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 8
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 123
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@6
    const-wide/16 v1, 0x7530

    #@8
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@b
    .line 125
    :cond_b
    return-void
.end method

.method public reset()V
    .registers 5

    #@0
    .prologue
    .line 140
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@2
    const-string v2, ""

    #@4
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@7
    .line 141
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mPassword:Landroid/widget/EditText;

    #@9
    const-string v2, ""

    #@b
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@e
    .line 142
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLogin:Landroid/widget/EditText;

    #@10
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    #@13
    .line 143
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@15
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@18
    move-result v0

    #@19
    .line 144
    .local v0, permLocked:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@1b
    if-eqz v0, :cond_28

    #@1d
    const v1, 0x104055d

    #@20
    move v2, v1

    #@21
    :goto_21
    if-eqz v0, :cond_2d

    #@23
    const/4 v1, 0x1

    #@24
    :goto_24
    invoke-interface {v3, v2, v1}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(IZ)V

    #@27
    .line 146
    return-void

    #@28
    .line 144
    :cond_28
    const v1, 0x104055e

    #@2b
    move v2, v1

    #@2c
    goto :goto_21

    #@2d
    :cond_2d
    const/4 v1, 0x0

    #@2e
    goto :goto_24
.end method

.method public setKeyguardCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@2
    .line 104
    return-void
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 2
    .parameter "utils"

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAccountView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    .line 108
    return-void
.end method

.method public showBouncer(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 327
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 323
    return-void
.end method
