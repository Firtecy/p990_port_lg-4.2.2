.class Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;
.super Ljava/lang/Object;
.source "MSimKeyguardSimPukView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->onSimLockChangedResponse(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 142
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 144
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@3
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@5
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 145
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@f
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    #@12
    .line 147
    :cond_12
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->val$success:Z

    #@14
    if-eqz v0, :cond_27

    #@16
    .line 148
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@18
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@1a
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@1c
    invoke-interface {v0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@1f
    .line 154
    :goto_1f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@21
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@23
    const/4 v1, 0x0

    #@24
    iput-boolean v1, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->mCheckInProgress:Z

    #@26
    .line 155
    return-void

    #@27
    .line 150
    :cond_27
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@29
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@2b
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@2d
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->reset()V

    #@30
    .line 151
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@32
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@34
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@36
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1$1;->this$1:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@38
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;

    #@3a
    const v2, 0x104055b

    #@3d
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getSecurityMessageDisplay(I)Ljava/lang/CharSequence;

    #@40
    move-result-object v1

    #@41
    invoke-interface {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(Ljava/lang/CharSequence;Z)V

    #@44
    goto :goto_1f
.end method
