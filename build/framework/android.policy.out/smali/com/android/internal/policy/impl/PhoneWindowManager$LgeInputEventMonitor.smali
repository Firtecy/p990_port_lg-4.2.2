.class public Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;
.super Landroid/view/InputEventReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LgeInputEventMonitor"
.end annotation


# instance fields
.field final TILT:I

.field mPalmCover:Lcom/lge/gesture/LgeGesture;

.field mPalmSwipe:Lcom/lge/gesture/LgeGesture;

.field mSlideAside:Lcom/lge/gesture/LgeGesture;

.field mToolType:I

.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

.field tryCount:I


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 7
    .parameter
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 592
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4
    .line 593
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@7
    .line 585
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mToolType:I

    #@9
    .line 586
    const/4 v0, 0x7

    #@a
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->TILT:I

    #@c
    .line 587
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@e
    .line 588
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@10
    .line 589
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@12
    .line 590
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@14
    .line 594
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 17
    .parameter "event"

    #@0
    .prologue
    .line 607
    const/4 v2, 0x0

    #@1
    .line 608
    .local v2, handled:Z
    const/4 v10, 0x0

    #@2
    .line 609
    .local v10, pointerCount:I
    const/4 v4, 0x0

    #@3
    .line 610
    .local v4, hasPalm:Z
    const/4 v5, 0x0

    #@4
    .line 611
    .local v5, hasPalmCover:Z
    const/4 v6, 0x0

    #@5
    .line 612
    .local v6, hasTilt:Z
    const/4 v3, 0x0

    #@6
    .line 614
    .local v3, hasFinger:Z
    :try_start_6
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@8
    invoke-virtual {v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardOn()Z

    #@b
    move-result v11

    #@c
    if-nez v11, :cond_9b

    #@e
    move-object/from16 v0, p1

    #@10
    instance-of v11, v0, Landroid/view/MotionEvent;

    #@12
    if-eqz v11, :cond_9b

    #@14
    invoke-virtual/range {p1 .. p1}, Landroid/view/InputEvent;->getSource()I

    #@17
    move-result v11

    #@18
    and-int/lit8 v11, v11, 0x2

    #@1a
    if-eqz v11, :cond_9b

    #@1c
    .line 616
    move-object/from16 v0, p1

    #@1e
    check-cast v0, Landroid/view/MotionEvent;

    #@20
    move-object v9, v0

    #@21
    .line 617
    .local v9, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v9}, Landroid/view/MotionEvent;->getPointerCount()I

    #@24
    move-result v10

    #@25
    .line 618
    const/4 v7, 0x0

    #@26
    .local v7, i:I
    :goto_26
    if-ge v7, v10, :cond_50

    #@28
    .line 619
    if-nez v3, :cond_35

    #@2a
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    #@2d
    move-result v11

    #@2e
    const/4 v12, 0x1

    #@2f
    if-ne v11, v12, :cond_35

    #@31
    const/4 v3, 0x1

    #@32
    .line 618
    :cond_32
    :goto_32
    add-int/lit8 v7, v7, 0x1

    #@34
    goto :goto_26

    #@35
    .line 620
    :cond_35
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    #@38
    move-result v11

    #@39
    const/4 v12, 0x6

    #@3a
    if-ne v11, v12, :cond_3e

    #@3c
    const/4 v5, 0x1

    #@3d
    goto :goto_32

    #@3e
    .line 621
    :cond_3e
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    #@41
    move-result v11

    #@42
    const/4 v12, 0x5

    #@43
    if-ne v11, v12, :cond_47

    #@45
    const/4 v4, 0x1

    #@46
    goto :goto_32

    #@47
    .line 622
    :cond_47
    invoke-virtual {v9, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    #@4a
    move-result v11

    #@4b
    const/4 v12, 0x7

    #@4c
    if-ne v11, v12, :cond_32

    #@4e
    const/4 v6, 0x1

    #@4f
    goto :goto_32

    #@50
    .line 625
    :cond_50
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@52
    invoke-static {v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$000(Lcom/android/internal/policy/impl/PhoneWindowManager;)I

    #@55
    move-result v11

    #@56
    const/4 v12, 0x1

    #@57
    if-ne v11, v12, :cond_70

    #@59
    if-eqz v3, :cond_70

    #@5b
    .line 626
    const-string v11, "kids"

    #@5d
    const-string v12, "service.plushome.currenthome"

    #@5f
    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v12

    #@63
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v8

    #@67
    .line 627
    .local v8, kidsModeOn:Z
    if-nez v8, :cond_70

    #@69
    .line 628
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@6b
    move-object/from16 v0, p1

    #@6d
    invoke-interface {v11, v0}, Lcom/lge/gesture/LgeGesture;->PlayWithInputEvent(Landroid/view/InputEvent;)V

    #@70
    .line 631
    .end local v8           #kidsModeOn:Z
    :cond_70
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@72
    invoke-static {v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$100(Lcom/android/internal/policy/impl/PhoneWindowManager;)I

    #@75
    move-result v11

    #@76
    const/4 v12, 0x1

    #@77
    if-ne v11, v12, :cond_84

    #@79
    if-nez v3, :cond_7d

    #@7b
    if-eqz v4, :cond_84

    #@7d
    .line 632
    :cond_7d
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@7f
    move-object/from16 v0, p1

    #@81
    invoke-interface {v11, v0}, Lcom/lge/gesture/LgeGesture;->PlayWithInputEvent(Landroid/view/InputEvent;)V

    #@84
    .line 634
    :cond_84
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@86
    invoke-static {v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$200(Lcom/android/internal/policy/impl/PhoneWindowManager;)I

    #@89
    move-result v11

    #@8a
    const/4 v12, 0x1

    #@8b
    if-ne v11, v12, :cond_9a

    #@8d
    if-nez v3, :cond_93

    #@8f
    if-nez v4, :cond_93

    #@91
    if-eqz v5, :cond_9a

    #@93
    .line 635
    :cond_93
    iget-object v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@95
    move-object/from16 v0, p1

    #@97
    invoke-interface {v11, v0}, Lcom/lge/gesture/LgeGesture;->PlayWithInputEvent(Landroid/view/InputEvent;)V
    :try_end_9a
    .catchall {:try_start_6 .. :try_end_9a} :catchall_d7

    #@9a
    .line 637
    :cond_9a
    const/4 v2, 0x1

    #@9b
    .line 641
    .end local v7           #i:I
    .end local v9           #motionEvent:Landroid/view/MotionEvent;
    :cond_9b
    iget v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@9d
    const/4 v12, 0x2

    #@9e
    if-le v11, v12, :cond_a9

    #@a0
    .line 642
    const/4 v11, 0x0

    #@a1
    iput v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@a3
    .line 643
    move-object/from16 v0, p1

    #@a5
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@a8
    .line 653
    :goto_a8
    return-void

    #@a9
    .line 646
    :cond_a9
    :try_start_a9
    move-object/from16 v0, p1

    #@ab
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->finishInputEvent(Landroid/view/InputEvent;Z)V
    :try_end_ae
    .catch Ljava/lang/RuntimeException; {:try_start_a9 .. :try_end_ae} :catch_af

    #@ae
    goto :goto_a8

    #@af
    .line 647
    :catch_af
    move-exception v1

    #@b0
    .line 648
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v11, "WindowManager"

    #@b2
    new-instance v12, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v13, "failed finishInputEvent / tryCount("

    #@b9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v12

    #@bd
    iget v13, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@bf
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v12

    #@c3
    const-string v13, ") -> "

    #@c5
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v12

    #@c9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v12

    #@cd
    invoke-static {v11, v12, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d0
    .line 649
    iget v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@d2
    add-int/lit8 v11, v11, 0x1

    #@d4
    iput v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@d6
    goto :goto_a8

    #@d7
    .line 641
    .end local v1           #e:Ljava/lang/RuntimeException;
    :catchall_d7
    move-exception v11

    #@d8
    iget v12, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@da
    const/4 v13, 0x2

    #@db
    if-le v12, v13, :cond_e6

    #@dd
    .line 642
    const/4 v12, 0x0

    #@de
    iput v12, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@e0
    .line 643
    move-object/from16 v0, p1

    #@e2
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@e5
    .line 650
    :goto_e5
    throw v11

    #@e6
    .line 646
    :cond_e6
    :try_start_e6
    move-object/from16 v0, p1

    #@e8
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->finishInputEvent(Landroid/view/InputEvent;Z)V
    :try_end_eb
    .catch Ljava/lang/RuntimeException; {:try_start_e6 .. :try_end_eb} :catch_ec

    #@eb
    goto :goto_e5

    #@ec
    .line 647
    :catch_ec
    move-exception v1

    #@ed
    .line 648
    .restart local v1       #e:Ljava/lang/RuntimeException;
    const-string v12, "WindowManager"

    #@ef
    new-instance v13, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v14, "failed finishInputEvent / tryCount("

    #@f6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v13

    #@fa
    iget v14, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@fc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v13

    #@100
    const-string v14, ") -> "

    #@102
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v13

    #@106
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v13

    #@10a
    invoke-static {v12, v13, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10d
    .line 649
    iget v12, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@10f
    add-int/lit8 v12, v12, 0x1

    #@111
    iput v12, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->tryCount:I

    #@113
    goto :goto_e5
.end method

.method setLgeGesture(Lcom/lge/gesture/LgeGestureFactory$GestureType;Z)V
    .registers 6
    .parameter "type"
    .parameter "enable"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 597
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@3
    if-ne p1, v0, :cond_13

    #@5
    .line 598
    if-eqz p2, :cond_38

    #@7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@9
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@b
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@d
    invoke-static {v0, v2}, Lcom/lge/gesture/LgeGestureFactory;->getInstance(Landroid/content/Context;Lcom/lge/gesture/LgeGestureFactory$GestureType;)Lcom/lge/gesture/LgeGesture;

    #@10
    move-result-object v0

    #@11
    :goto_11
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@13
    .line 599
    :cond_13
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@15
    if-ne p1, v0, :cond_25

    #@17
    .line 600
    if-eqz p2, :cond_3a

    #@19
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1d
    sget-object v2, Lcom/lge/gesture/LgeGestureFactory;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@1f
    invoke-static {v0, v2}, Lcom/lge/gesture/LgeGestureFactory;->getInstance(Landroid/content/Context;Lcom/lge/gesture/LgeGestureFactory$GestureType;)Lcom/lge/gesture/LgeGesture;

    #@22
    move-result-object v0

    #@23
    :goto_23
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@25
    .line 601
    :cond_25
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@27
    if-ne p1, v0, :cond_37

    #@29
    .line 602
    if-eqz p2, :cond_35

    #@2b
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2d
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2f
    sget-object v1, Lcom/lge/gesture/LgeGestureFactory;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@31
    invoke-static {v0, v1}, Lcom/lge/gesture/LgeGestureFactory;->getInstance(Landroid/content/Context;Lcom/lge/gesture/LgeGestureFactory$GestureType;)Lcom/lge/gesture/LgeGesture;

    #@34
    move-result-object v1

    #@35
    :cond_35
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@37
    .line 603
    :cond_37
    return-void

    #@38
    :cond_38
    move-object v0, v1

    #@39
    .line 598
    goto :goto_11

    #@3a
    :cond_3a
    move-object v0, v1

    #@3b
    .line 600
    goto :goto_23
.end method
