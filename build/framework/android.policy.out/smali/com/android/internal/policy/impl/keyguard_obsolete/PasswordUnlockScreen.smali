.class public Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;
.super Landroid/widget/LinearLayout;
.source "PasswordUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static final MINIMUM_PASSWORD_LENGTH_BEFORE_REPORT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PasswordUnlockScreen"


# instance fields
.field private final mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private final mCreationHardKeyboardHidden:I

.field private final mCreationOrientation:I

.field private final mIsAlpha:Z

.field private final mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

.field private final mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private final mPasswordEntry:Landroid/widget/EditText;

.field private mResuming:Z

.field private final mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

.field private final mUseSystemIME:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V
    .registers 20
    .parameter "context"
    .parameter "configuration"
    .parameter "lockPatternUtils"
    .parameter "updateMonitor"
    .parameter "callback"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 78
    const/4 v1, 0x1

    #@4
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mUseSystemIME:Z

    #@6
    .line 90
    move-object/from16 v0, p2

    #@8
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@a
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    #@c
    .line 91
    move-object/from16 v0, p2

    #@e
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    #@10
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationOrientation:I

    #@12
    .line 92
    move-object/from16 v0, p4

    #@14
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@16
    .line 93
    move-object/from16 v0, p5

    #@18
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1a
    .line 94
    move-object/from16 v0, p3

    #@1c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1e
    .line 96
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@21
    move-result-object v9

    #@22
    .line 97
    .local v9, layoutInflater:Landroid/view/LayoutInflater;
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationOrientation:I

    #@24
    const/4 v2, 0x2

    #@25
    if-eq v1, v2, :cond_104

    #@27
    .line 98
    const v1, 0x1090065

    #@2a
    const/4 v2, 0x1

    #@2b
    invoke-virtual {v9, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@2e
    .line 103
    :goto_2e
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@30
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@32
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@34
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@36
    const/4 v6, 0x1

    #@37
    move-object v2, p0

    #@38
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;-><init>(Landroid/view/View;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Z)V

    #@3b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@3d
    .line 106
    invoke-virtual/range {p3 .. p3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@40
    move-result v12

    #@41
    .line 107
    .local v12, quality:I
    const/high16 v1, 0x4

    #@43
    if-eq v1, v12, :cond_4d

    #@45
    const/high16 v1, 0x5

    #@47
    if-eq v1, v12, :cond_4d

    #@49
    const/high16 v1, 0x6

    #@4b
    if-ne v1, v12, :cond_10d

    #@4d
    :cond_4d
    const/4 v1, 0x1

    #@4e
    :goto_4e
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mIsAlpha:Z

    #@50
    .line 111
    const v1, 0x102022c

    #@53
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    #@56
    move-result-object v1

    #@57
    check-cast v1, Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@59
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@5b
    .line 112
    const v1, 0x10202ce

    #@5e
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    #@61
    move-result-object v1

    #@62
    check-cast v1, Landroid/widget/EditText;

    #@64
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@66
    .line 113
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@68
    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    #@6b
    .line 115
    new-instance v1, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@6d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@6f
    const/4 v3, 0x0

    #@70
    invoke-direct {v1, p1, v2, p0, v3}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;-><init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;Z)V

    #@73
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@75
    .line 116
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@77
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@79
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    #@7c
    move-result v2

    #@7d
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setEnableHaptics(Z)V

    #@80
    .line 117
    const/4 v7, 0x0

    #@81
    .line 118
    .local v7, imeOrDeleteButtonVisible:Z
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mIsAlpha:Z

    #@83
    if-eqz v1, :cond_110

    #@85
    .line 120
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@87
    const/4 v2, 0x0

    #@88
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setKeyboardMode(I)V

    #@8b
    .line 121
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@8d
    const/16 v2, 0x8

    #@8f
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    #@92
    .line 143
    :cond_92
    :goto_92
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@94
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    #@97
    .line 146
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mIsAlpha:Z

    #@99
    if-eqz v1, :cond_13b

    #@9b
    .line 147
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@9d
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@a4
    .line 148
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@a6
    const/16 v2, 0x81

    #@a8
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    #@ab
    .line 161
    :goto_ab
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@ad
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$2;

    #@af
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)V

    #@b2
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@b5
    .line 166
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@b7
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$3;

    #@b9
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)V

    #@bc
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@bf
    .line 181
    const v1, 0x10202cf

    #@c2
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    #@c5
    move-result-object v13

    #@c6
    .line 182
    .local v13, switchImeButton:Landroid/view/View;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->getContext()Landroid/content/Context;

    #@c9
    move-result-object v1

    #@ca
    const-string v2, "input_method"

    #@cc
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@cf
    move-result-object v8

    #@d0
    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    #@d2
    .line 184
    .local v8, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mIsAlpha:Z

    #@d4
    if-eqz v1, :cond_ec

    #@d6
    if-eqz v13, :cond_ec

    #@d8
    const/4 v1, 0x0

    #@d9
    invoke-direct {p0, v8, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->hasMultipleEnabledIMEsOrSubtypes(Landroid/view/inputmethod/InputMethodManager;Z)Z

    #@dc
    move-result v1

    #@dd
    if-eqz v1, :cond_ec

    #@df
    .line 185
    const/4 v1, 0x0

    #@e0
    invoke-virtual {v13, v1}, Landroid/view/View;->setVisibility(I)V

    #@e3
    .line 186
    const/4 v7, 0x1

    #@e4
    .line 187
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$4;

    #@e6
    invoke-direct {v1, p0, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$4;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;Landroid/view/inputmethod/InputMethodManager;)V

    #@e9
    invoke-virtual {v13, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@ec
    .line 197
    :cond_ec
    if-nez v7, :cond_103

    #@ee
    .line 198
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@f0
    invoke-virtual {v1}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@f3
    move-result-object v10

    #@f4
    .line 199
    .local v10, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v1, v10, Landroid/view/ViewGroup$MarginLayoutParams;

    #@f6
    if-eqz v1, :cond_103

    #@f8
    move-object v1, v10

    #@f9
    .line 200
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@fb
    const/4 v2, 0x0

    #@fc
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@fe
    .line 201
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@100
    invoke-virtual {v1, v10}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@103
    .line 204
    .end local v10           #params:Landroid/view/ViewGroup$LayoutParams;
    :cond_103
    return-void

    #@104
    .line 100
    .end local v7           #imeOrDeleteButtonVisible:Z
    .end local v8           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v12           #quality:I
    .end local v13           #switchImeButton:Landroid/view/View;
    :cond_104
    const v1, 0x1090064

    #@107
    const/4 v2, 0x1

    #@108
    invoke-virtual {v9, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@10b
    goto/16 :goto_2e

    #@10d
    .line 107
    .restart local v12       #quality:I
    :cond_10d
    const/4 v1, 0x0

    #@10e
    goto/16 :goto_4e

    #@110
    .line 124
    .restart local v7       #imeOrDeleteButtonVisible:Z
    :cond_110
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@112
    const/4 v2, 0x1

    #@113
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setKeyboardMode(I)V

    #@116
    .line 125
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@118
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    #@11a
    const/4 v3, 0x1

    #@11b
    if-ne v1, v3, :cond_139

    #@11d
    const/4 v1, 0x4

    #@11e
    :goto_11e
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    #@121
    .line 130
    const v1, 0x10202f7

    #@124
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    #@127
    move-result-object v11

    #@128
    .line 131
    .local v11, pinDelete:Landroid/view/View;
    if-eqz v11, :cond_92

    #@12a
    .line 132
    const/4 v1, 0x0

    #@12b
    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    #@12e
    .line 133
    const/4 v7, 0x1

    #@12f
    .line 134
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$1;

    #@131
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)V

    #@134
    invoke-virtual {v11, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@137
    goto/16 :goto_92

    #@139
    .line 125
    .end local v11           #pinDelete:Landroid/view/View;
    :cond_139
    const/4 v1, 0x0

    #@13a
    goto :goto_11e

    #@13b
    .line 153
    :cond_13b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@13d
    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    #@140
    move-result-object v2

    #@141
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@144
    .line 154
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@146
    const/16 v2, 0x12

    #@148
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    #@14b
    goto/16 :goto_ab
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/widget/PasswordEntryKeyboardHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mResuming:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;)Lcom/android/internal/widget/PasswordEntryKeyboardView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@2
    return-object v0
.end method

.method private handleAttemptLockout(J)V
    .registers 11
    .parameter "elapsedRealtimeDeadline"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 320
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    #@6
    .line 321
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setEnabled(Z)V

    #@b
    .line 322
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v6

    #@f
    .line 323
    .local v6, elapsedRealtime:J
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;

    #@11
    sub-long v2, p1, v6

    #@13
    const-wide/16 v4, 0x3e8

    #@15
    move-object v1, p0

    #@16
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;JJ)V

    #@19
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen$5;->start()Landroid/os/CountDownTimer;

    #@1c
    .line 341
    return-void
.end method

.method private hasMultipleEnabledIMEsOrSubtypes(Landroid/view/inputmethod/InputMethodManager;Z)Z
    .registers 15
    .parameter "imm"
    .parameter "shouldIncludeAuxiliarySubtypes"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 215
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    #@5
    move-result-object v1

    #@6
    .line 218
    .local v1, enabledImis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    const/4 v2, 0x0

    #@7
    .line 220
    .local v2, filteredImisCount:I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v3

    #@b
    :cond_b
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v11

    #@f
    if-eqz v11, :cond_50

    #@11
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Landroid/view/inputmethod/InputMethodInfo;

    #@17
    .line 222
    .local v5, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-le v2, v10, :cond_1a

    #@19
    .line 248
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :goto_19
    return v10

    #@1a
    .line 223
    .restart local v5       #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_1a
    invoke-virtual {p1, v5, v10}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@1d
    move-result-object v8

    #@1e
    .line 226
    .local v8, subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    #@21
    move-result v11

    #@22
    if-eqz v11, :cond_27

    #@24
    .line 227
    add-int/lit8 v2, v2, 0x1

    #@26
    .line 228
    goto :goto_b

    #@27
    .line 231
    :cond_27
    const/4 v0, 0x0

    #@28
    .line 232
    .local v0, auxCount:I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v4

    #@2c
    .local v4, i$:Ljava/util/Iterator;
    :cond_2c
    :goto_2c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v11

    #@30
    if-eqz v11, :cond_41

    #@32
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v7

    #@36
    check-cast v7, Landroid/view/inputmethod/InputMethodSubtype;

    #@38
    .line 233
    .local v7, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@3b
    move-result v11

    #@3c
    if-eqz v11, :cond_2c

    #@3e
    .line 234
    add-int/lit8 v0, v0, 0x1

    #@40
    goto :goto_2c

    #@41
    .line 237
    .end local v7           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_41
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@44
    move-result v11

    #@45
    sub-int v6, v11, v0

    #@47
    .line 242
    .local v6, nonAuxCount:I
    if-gtz v6, :cond_4d

    #@49
    if-eqz p2, :cond_b

    #@4b
    if-le v0, v10, :cond_b

    #@4d
    .line 243
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    .line 244
    goto :goto_b

    #@50
    .line 248
    .end local v0           #auxCount:I
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v6           #nonAuxCount:I
    .end local v8           #subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_50
    if-gt v2, v10, :cond_5d

    #@52
    const/4 v11, 0x0

    #@53
    invoke-virtual {p1, v11, v9}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@56
    move-result-object v11

    #@57
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@5a
    move-result v11

    #@5b
    if-le v11, v10, :cond_5e

    #@5d
    :cond_5d
    move v9, v10

    #@5e
    :cond_5e
    move v10, v9

    #@5f
    goto :goto_19
.end method

.method private verifyPasswordAndUnlock()V
    .registers 7

    #@0
    .prologue
    const v5, 0x1040314

    #@3
    .line 294
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@5
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .line 295
    .local v2, entry:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_35

    #@15
    .line 296
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->keyguardDone(Z)V

    #@1b
    .line 297
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1d
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    #@20
    .line 298
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@22
    const/4 v4, 0x0

    #@23
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@26
    .line 299
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, v2}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@2d
    .line 315
    :cond_2d
    :goto_2d
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@2f
    const-string v4, ""

    #@31
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@34
    .line 316
    return-void

    #@35
    .line 300
    :cond_35
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@38
    move-result v3

    #@39
    const/4 v4, 0x3

    #@3a
    if-le v3, v4, :cond_60

    #@3c
    .line 303
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@3e
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportFailedUnlockAttempt()V

    #@41
    .line 304
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@43
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getFailedAttempts()I

    #@46
    move-result v3

    #@47
    rem-int/lit8 v3, v3, 0x5

    #@49
    if-nez v3, :cond_54

    #@4b
    .line 306
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@4d
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->setLockoutAttemptDeadline()J

    #@50
    move-result-wide v0

    #@51
    .line 307
    .local v0, deadline:J
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->handleAttemptLockout(J)V

    #@54
    .line 309
    .end local v0           #deadline:J
    :cond_54
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@56
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    #@58
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@5f
    goto :goto_2d

    #@60
    .line 311
    :cond_60
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@63
    move-result v3

    #@64
    if-lez v3, :cond_2d

    #@66
    .line 312
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@68
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    #@6a
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@71
    goto :goto_2d
.end method


# virtual methods
.method public cleanUp()V
    .registers 2

    #@0
    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@5
    .line 291
    return-void
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mIsAlpha:Z

    #@2
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 351
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    #@3
    .line 352
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@a
    move-result-object v0

    #@b
    .line 353
    .local v0, config:Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    #@d
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationOrientation:I

    #@f
    if-ne v1, v2, :cond_17

    #@11
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@13
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    #@15
    if-eq v1, v2, :cond_1c

    #@17
    .line 355
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@19
    invoke-interface {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@1c
    .line 357
    :cond_1c
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 362
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 363
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    #@5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationOrientation:I

    #@7
    if-ne v0, v1, :cond_f

    #@9
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@b
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    #@d
    if-eq v0, v1, :cond_14

    #@f
    .line 365
    :cond_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@11
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@14
    .line 367
    :cond_14
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    #@0
    .prologue
    .line 376
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x6

    #@3
    if-eq p2, v0, :cond_8

    #@5
    const/4 v0, 0x5

    #@6
    if-ne p2, v0, :cond_d

    #@8
    .line 378
    :cond_8
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->verifyPasswordAndUnlock()V

    #@b
    .line 379
    const/4 v0, 0x1

    #@c
    .line 381
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 345
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@5
    .line 346
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public onKeyboardChange(Z)V
    .registers 4
    .parameter "isKeyboardOpen"

    #@0
    .prologue
    .line 371
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x4

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    #@8
    .line 372
    return-void

    #@9
    .line 371
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onPause()V

    #@5
    .line 268
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onResume()V
    .registers 5

    #@0
    .prologue
    .line 272
    const/4 v2, 0x1

    #@1
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mResuming:Z

    #@3
    .line 274
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@5
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onResume()V

    #@8
    .line 277
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@a
    const-string v3, ""

    #@c
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@f
    .line 278
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    #@11
    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    #@14
    .line 281
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@16
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getLockoutAttemptDeadline()J

    #@19
    move-result-wide v0

    #@1a
    .line 282
    .local v0, deadline:J
    const-wide/16 v2, 0x0

    #@1c
    cmp-long v2, v0, v2

    #@1e
    if-eqz v2, :cond_23

    #@20
    .line 283
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->handleAttemptLockout(J)V

    #@23
    .line 285
    :cond_23
    const/4 v2, 0x0

    #@24
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;->mResuming:Z

    #@26
    .line 286
    return-void
.end method
