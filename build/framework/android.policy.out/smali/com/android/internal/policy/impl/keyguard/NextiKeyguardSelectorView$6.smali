.class Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;
.super Landroid/database/ContentObserver;
.source "NextiKeyguardSelectorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 816
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 12
    .parameter "selfChange"

    #@0
    .prologue
    .line 818
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    #@3
    .line 829
    const/4 v6, 0x0

    #@4
    .line 830
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v8, -0x1

    #@5
    .line 832
    .local v8, missedcount:I
    :try_start_5
    new-instance v9, Ljava/lang/StringBuilder;

    #@7
    const-string v0, "type="

    #@9
    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@c
    .line 833
    .local v9, where:Ljava/lang/StringBuilder;
    const/4 v0, 0x3

    #@d
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    .line 834
    const-string v0, " AND new=1"

    #@12
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 835
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->getContext()Landroid/content/Context;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1e
    move-result-object v0

    #@1f
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@21
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$1500()[Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    const/4 v4, 0x0

    #@2a
    const-string v5, "date DESC"

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v6

    #@30
    .line 839
    if-eqz v6, :cond_40

    #@32
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@35
    move-result v8

    #@36
    .line 843
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@39
    .line 845
    if-ltz v8, :cond_40

    #@3b
    .line 847
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView$6;->this$0:Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;

    #@3d
    invoke-static {v0, v8}, Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;->access$600(Lcom/android/internal/policy/impl/keyguard/NextiKeyguardSelectorView;I)V
    :try_end_40
    .catchall {:try_start_5 .. :try_end_40} :catchall_69
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_40} :catch_46

    #@40
    .line 855
    :cond_40
    if-eqz v6, :cond_45

    #@42
    .line 856
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@45
    .line 860
    .end local v9           #where:Ljava/lang/StringBuilder;
    :cond_45
    :goto_45
    return-void

    #@46
    .line 850
    :catch_46
    move-exception v7

    #@47
    .line 851
    .local v7, e:Ljava/lang/Exception;
    :try_start_47
    const-string v0, "SecuritySelectorView"

    #@49
    new-instance v1, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v2, "KeyguardSelectorView.ContentObserver.onChange(): ContentObserver Exception Occurred: "

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_63
    .catchall {:try_start_47 .. :try_end_63} :catchall_69

    #@63
    .line 855
    if-eqz v6, :cond_45

    #@65
    .line 856
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@68
    goto :goto_45

    #@69
    .line 855
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_69
    move-exception v0

    #@6a
    if-eqz v6, :cond_6f

    #@6c
    .line 856
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6f
    :cond_6f
    throw v0
.end method
