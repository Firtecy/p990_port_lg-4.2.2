.class final Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "KeepHidingNavInputEventReceiver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;
    }
.end annotation


# instance fields
.field private mSingleTapChecker:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

.field needShowingNav:Z

.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 7
    .parameter
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    .line 4254
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    .line 4255
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@5
    .line 4259
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->needShowingNav:Z

    #@8
    .line 4260
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

    #@a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->mSingleTapChecker:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

    #@f
    .line 4256
    const-string v0, "WindowManager"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "new KeepHidingNavInputEventReceiver of "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 4257
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 4263
    const/4 v1, 0x0

    #@2
    .line 4264
    .local v1, handled:Z
    const-string v3, "WindowManager"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "KeepHidingNavInputEventReceiver.onInputEvent for "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 4266
    :try_start_1a
    instance-of v3, p1, Landroid/view/MotionEvent;

    #@1c
    if-eqz v3, :cond_4c

    #@1e
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@21
    move-result v3

    #@22
    and-int/lit8 v3, v3, 0x2

    #@24
    if-eqz v3, :cond_4c

    #@26
    .line 4268
    move-object v0, p1

    #@27
    check-cast v0, Landroid/view/MotionEvent;

    #@29
    move-object v2, v0

    #@2a
    .line 4270
    .local v2, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_50

    #@30
    .line 4271
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->mSingleTapChecker:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

    #@32
    invoke-virtual {v3, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->init(Landroid/view/MotionEvent;)V

    #@35
    .line 4279
    :cond_35
    :goto_35
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->needShowingNav:Z

    #@37
    if-eqz v3, :cond_67

    #@39
    .line 4280
    const-string v3, "WindowManager"

    #@3b
    const-string v4, "show navigation bar"

    #@3d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 4281
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@42
    invoke-static {v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2800(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@45
    .line 4282
    const/4 v3, 0x0

    #@46
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->mSingleTapChecker:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

    #@48
    .line 4283
    const/4 v3, 0x0

    #@49
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->needShowingNav:Z
    :try_end_4b
    .catchall {:try_start_1a .. :try_end_4b} :catchall_62

    #@4b
    .line 4287
    :goto_4b
    const/4 v1, 0x1

    #@4c
    .line 4290
    .end local v2           #motionEvent:Landroid/view/MotionEvent;
    :cond_4c
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@4f
    .line 4292
    return-void

    #@50
    .line 4273
    .restart local v2       #motionEvent:Landroid/view/MotionEvent;
    :cond_50
    :try_start_50
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    #@53
    move-result v3

    #@54
    if-ne v3, v6, :cond_35

    #@56
    .line 4274
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->mSingleTapChecker:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;

    #@58
    invoke-virtual {v3, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver$SingleTapChecker;->isSingleTapUp(Landroid/view/MotionEvent;)Z

    #@5b
    move-result v3

    #@5c
    if-eqz v3, :cond_35

    #@5e
    .line 4275
    const/4 v3, 0x1

    #@5f
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->needShowingNav:Z
    :try_end_61
    .catchall {:try_start_50 .. :try_end_61} :catchall_62

    #@61
    goto :goto_35

    #@62
    .line 4290
    .end local v2           #motionEvent:Landroid/view/MotionEvent;
    :catchall_62
    move-exception v3

    #@63
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@66
    throw v3

    #@67
    .line 4286
    .restart local v2       #motionEvent:Landroid/view/MotionEvent;
    :cond_67
    :try_start_67
    const-string v3, "WindowManager"

    #@69
    const-string v4, "touch ignored"

    #@6b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6e
    .catchall {:try_start_67 .. :try_end_6e} :catchall_62

    #@6e
    goto :goto_4b
.end method
