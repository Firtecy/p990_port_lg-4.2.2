.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;
.super Ljava/lang/Object;
.source "KeyguardSimPukView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->onSimLockChangedResponse(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;Z)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 615
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->val$success:Z

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 617
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@4
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@6
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@8
    if-eqz v3, :cond_13

    #@a
    .line 618
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@c
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@e
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@10
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->hide()V

    #@13
    .line 620
    :cond_13
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->val$success:Z

    #@15
    if-eqz v3, :cond_5a

    #@17
    .line 622
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@19
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@1b
    const/4 v4, 0x3

    #@1c
    invoke-static {v3, v4, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@1f
    .line 623
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@21
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@23
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->getContext()Landroid/content/Context;

    #@26
    move-result-object v3

    #@27
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->reportSimUnlocked()V

    #@2e
    .line 625
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@30
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@32
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@34
    const/4 v4, 0x1

    #@35
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->dismiss(Z)V

    #@38
    .line 655
    :goto_38
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@3a
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@3c
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@3e
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;->reset()V

    #@41
    .line 656
    const-string v3, "KeyguardSimPukView"

    #@43
    const-string v4, "[KYC] CheckSimPuk - mCallback.userActivity(0)"

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 657
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@4a
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@4c
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;

    #@4e
    const-wide/16 v4, 0x0

    #@50
    invoke-interface {v3, v4, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityCallback;->userActivity(J)V

    #@53
    .line 659
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@55
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@57
    iput-boolean v6, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mCheckInProgress:Z

    #@59
    .line 660
    return-void

    #@5a
    .line 630
    :cond_5a
    :try_start_5a
    const-string v3, "phone"

    #@5c
    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5f
    move-result-object v3

    #@60
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@63
    move-result-object v3

    #@64
    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->getIccPuk1RetryCount()I

    #@67
    move-result v0

    #@68
    .line 632
    .local v0, attemptsRemaining:I
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    const-string v4, "VDF"

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v3

    #@72
    if-eqz v3, :cond_b1

    #@74
    .line 633
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@76
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@78
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)I

    #@7b
    move-result v3

    #@7c
    if-ne v3, v0, :cond_a0

    #@7e
    .line 634
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@80
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@82
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$500(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;)Landroid/content/Context;

    #@85
    move-result-object v3

    #@86
    const v4, 0x1040303

    #@89
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8c
    move-result-object v2

    #@8d
    .line 635
    .local v2, message:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@8f
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@91
    const/4 v4, 0x0

    #@92
    invoke-static {v3, v4, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_95
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_95} :catch_96

    #@95
    goto :goto_38

    #@96
    .line 648
    .end local v0           #attemptsRemaining:I
    .end local v2           #message:Ljava/lang/String;
    :catch_96
    move-exception v1

    #@97
    .line 649
    .local v1, ex:Landroid/os/RemoteException;
    const/4 v0, 0x0

    #@98
    .line 650
    .restart local v0       #attemptsRemaining:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@9a
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@9c
    invoke-static {v3, v6, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@9f
    goto :goto_38

    #@a0
    .line 638
    .end local v1           #ex:Landroid/os/RemoteException;
    :cond_a0
    :try_start_a0
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@a2
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@a4
    const/4 v4, 0x0

    #@a5
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@a8
    .line 639
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@aa
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@ac
    const/4 v4, 0x0

    #@ad
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@b0
    goto :goto_38

    #@b1
    .line 643
    :cond_b1
    const-string v3, "KeyguardSimPukView"

    #@b3
    const-string v4, "[FCMANIA18] CheckSimPuk"

    #@b5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 644
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@ba
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@bc
    const/4 v4, 0x0

    #@bd
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V

    #@c0
    .line 645
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;

    #@c2
    iget-object v3, v3, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$8;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;

    #@c4
    const/4 v4, 0x0

    #@c5
    invoke-static {v3, v4, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;II)V
    :try_end_c8
    .catch Landroid/os/RemoteException; {:try_start_a0 .. :try_end_c8} :catch_96

    #@c8
    goto/16 :goto_38
.end method
