.class public Lcom/android/internal/policy/impl/keyguard/EmergencyButton;
.super Landroid/widget/Button;
.source "EmergencyButton.java"


# static fields
.field private static final ACTION_EMERGENCY_DIAL:Ljava/lang/String; = "com.android.phone.EmergencyDialer.DIAL"

.field private static final EMERGENCY_CALL_TIMEOUT:I = 0x2710

.field private static countrycode:Ljava/lang/String;


# instance fields
.field private emergency_status:Z

.field mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mServiceState:Landroid/telephony/ServiceState;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 57
    const-string v0, "ro.build.target_country"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->countrycode:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 82
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 56
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->emergency_status:Z

    #@6
    .line 63
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$1;-><init>(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@d
    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->updateEmergencyCallButton(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->emergency_status:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private updateEmergencyCallButton(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 10
    .parameter "simState"
    .parameter "phoneState"

    #@0
    .prologue
    .line 190
    const/4 v3, 0x0

    #@1
    .line 191
    .local v3, enabled:Z
    const/4 v5, 0x0

    #@2
    .line 192
    .local v5, showIcon:Z
    const/4 v0, 0x2

    #@3
    if-ne p2, v0, :cond_24

    #@5
    .line 193
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "AU"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_22

    #@11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->checkIsDialEmergency()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_22

    #@17
    .line 194
    const/4 v3, 0x0

    #@18
    .line 239
    :cond_18
    :goto_18
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1a
    sget-boolean v4, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->USE_UPPER_CASE:Z

    #@1c
    move-object v1, p0

    #@1d
    move v2, p2

    #@1e
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;IZZZ)V

    #@21
    .line 241
    return-void

    #@22
    .line 197
    :cond_22
    const/4 v3, 0x1

    #@23
    goto :goto_18

    #@24
    .line 198
    :cond_24
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isEmergencyCallCapable()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_18

    #@2c
    .line 199
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@2e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimLocked()Z

    #@35
    move-result v6

    #@36
    .line 200
    .local v6, simLocked:Z
    if-eqz v6, :cond_78

    #@38
    .line 202
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3a
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isEmergencyCallEnabledWhileSimLocked()Z

    #@3d
    move-result v3

    #@3e
    .line 203
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    const-string v1, "VZW"

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v0

    #@48
    if-eqz v0, :cond_55

    #@4a
    .line 204
    const/4 v5, 0x0

    #@4b
    .line 233
    :goto_4b
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@4d
    if-eqz v0, :cond_18

    #@4f
    .line 234
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@51
    if-ne p1, v0, :cond_18

    #@53
    .line 235
    const/4 v3, 0x0

    #@54
    goto :goto_18

    #@55
    .line 206
    :cond_55
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    const-string v1, "AU"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_76

    #@61
    .line 207
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mServiceState:Landroid/telephony/ServiceState;

    #@63
    if-eqz v0, :cond_6d

    #@65
    .line 209
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mServiceState:Landroid/telephony/ServiceState;

    #@67
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    #@6a
    move-result v0

    #@6b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->emergency_status:Z

    #@6d
    .line 212
    :cond_6d
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->emergency_status:Z

    #@6f
    if-eqz v0, :cond_74

    #@71
    .line 214
    const/4 v5, 0x1

    #@72
    .line 215
    const/4 v3, 0x1

    #@73
    goto :goto_4b

    #@74
    .line 219
    :cond_74
    const/4 v3, 0x0

    #@75
    goto :goto_4b

    #@76
    .line 223
    :cond_76
    const/4 v5, 0x1

    #@77
    goto :goto_4b

    #@78
    .line 225
    :cond_78
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@7a
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_DOING_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@7c
    if-ne v0, v1, :cond_81

    #@7e
    .line 226
    const/4 v3, 0x1

    #@7f
    .line 227
    const/4 v5, 0x1

    #@80
    goto :goto_4b

    #@81
    .line 231
    :cond_81
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@83
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@86
    move-result v3

    #@87
    goto :goto_4b
.end method


# virtual methods
.method protected checkIsDialEmergency()Z
    .registers 5

    #@0
    .prologue
    .line 158
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    .line 159
    .local v0, nPinTextLength:I
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@8
    .line 160
    .local v1, szEmergencyText:Ljava/lang/String;
    const/4 v2, 0x3

    #@9
    if-ne v0, v2, :cond_2f

    #@b
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->countrycode:Ljava/lang/String;

    #@d
    const-string v3, "AU"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_2f

    #@15
    const-string v2, "112"

    #@17
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v2

    #@1b
    if-nez v2, :cond_2d

    #@1d
    const-string v2, "911"

    #@1f
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_2d

    #@25
    const-string v2, "000"

    #@27
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_2f

    #@2d
    :cond_2d
    const/4 v2, 0x1

    #@2e
    :goto_2e
    return v2

    #@2f
    :cond_2f
    const/4 v2, 0x0

    #@30
    goto :goto_2e
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 91
    invoke-super {p0}, Landroid/widget/Button;->onAttachedToWindow()V

    #@3
    .line 92
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 93
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/Button;->onDetachedFromWindow()V

    #@3
    .line 98
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 99
    return-void
.end method

.method protected onFinishInflate()V
    .registers 7

    #@0
    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/Button;->onFinishInflate()V

    #@3
    .line 108
    new-instance v4, Lcom/android/internal/widget/LockPatternUtils;

    #@5
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@7
    invoke-direct {v4, v5}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@a
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@c
    .line 109
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@e
    const-string v5, "power"

    #@10
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Landroid/os/PowerManager;

    #@16
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mPowerManager:Landroid/os/PowerManager;

    #@18
    .line 110
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, "DCM"

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v4

    #@22
    if-nez v4, :cond_30

    #@24
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    const-string v5, "AU"

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_6f

    #@30
    .line 112
    :cond_30
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$2;

    #@32
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$2;-><init>(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)V

    #@35
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@38
    .line 125
    :goto_38
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@3a
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPhoneState()I

    #@41
    move-result v1

    #@42
    .line 126
    .local v1, phoneState:I
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@44
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@4b
    move-result-object v2

    #@4c
    .line 127
    .local v2, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-direct {p0, v2, v1}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->updateEmergencyCallButton(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@4f
    .line 130
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@51
    const-string v5, "phone"

    #@53
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@56
    move-result-object v3

    #@57
    check-cast v3, Landroid/telephony/TelephonyManager;

    #@59
    .line 131
    .local v3, telephonyManager:Landroid/telephony/TelephonyManager;
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;

    #@5b
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$4;-><init>(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)V

    #@5e
    .line 146
    .local v0, listener:Landroid/telephony/PhoneStateListener;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    const-string v5, "AU"

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v4

    #@68
    if-eqz v4, :cond_6e

    #@6a
    .line 147
    const/4 v4, 0x1

    #@6b
    invoke-virtual {v3, v0, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@6e
    .line 151
    :cond_6e
    return-void

    #@6f
    .line 118
    .end local v0           #listener:Landroid/telephony/PhoneStateListener;
    .end local v1           #phoneState:I
    .end local v2           #simState:Lcom/android/internal/telephony/IccCardConstants$State;
    .end local v3           #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_6f
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$3;

    #@71
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton$3;-><init>(Lcom/android/internal/policy/impl/keyguard/EmergencyButton;)V

    #@74
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@77
    goto :goto_38
.end method

.method public takeEmergencyCallAction()V
    .registers 7

    #@0
    .prologue
    .line 167
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mPowerManager:Landroid/os/PowerManager;

    #@2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v3

    #@6
    const/4 v5, 0x1

    #@7
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@a
    .line 168
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@11
    move-result v2

    #@12
    const/4 v3, 0x2

    #@13
    if-ne v2, v3, :cond_1b

    #@15
    .line 170
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@17
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->resumeCall()Z

    #@1a
    .line 187
    :goto_1a
    return-void

    #@1b
    .line 172
    :cond_1b
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->countrycode:Ljava/lang/String;

    #@1d
    const-string v3, "AU"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_59

    #@25
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->checkIsDialEmergency()Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_59

    #@2b
    .line 174
    new-instance v1, Landroid/content/Intent;

    #@2d
    const-string v2, "android.intent.action.CALL_EMERGENCY"

    #@2f
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 175
    .local v1, intent:Landroid/content/Intent;
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mTempString:Ljava/lang/String;

    #@34
    .line 176
    .local v0, digits:Ljava/lang/CharSequence;
    new-instance v2, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v3, "tel:"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@4e
    .line 177
    const/high16 v2, 0x1000

    #@50
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@53
    .line 178
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->mContext:Landroid/content/Context;

    #@55
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@58
    goto :goto_1a

    #@59
    .line 182
    .end local v0           #digits:Ljava/lang/CharSequence;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_59
    new-instance v1, Landroid/content/Intent;

    #@5b
    const-string v2, "com.android.phone.EmergencyDialer.DIAL"

    #@5d
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@60
    .line 183
    .restart local v1       #intent:Landroid/content/Intent;
    const/high16 v2, 0x1080

    #@62
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@65
    .line 185
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/EmergencyButton;->getContext()Landroid/content/Context;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@6c
    goto :goto_1a
.end method
