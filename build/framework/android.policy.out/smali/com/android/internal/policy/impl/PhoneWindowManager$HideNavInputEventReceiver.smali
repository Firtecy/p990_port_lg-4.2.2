.class final Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "HideNavInputEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    .line 4436
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    .line 4437
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@5
    .line 4438
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 4442
    const/4 v1, 0x0

    #@1
    .line 4444
    .local v1, handled:Z
    :try_start_1
    instance-of v3, p1, Landroid/view/MotionEvent;

    #@3
    if-eqz v3, :cond_1c

    #@5
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@8
    move-result v3

    #@9
    and-int/lit8 v3, v3, 0x2

    #@b
    if-eqz v3, :cond_1c

    #@d
    .line 4446
    move-object v0, p1

    #@e
    check-cast v0, Landroid/view/MotionEvent;

    #@10
    move-object v2, v0

    #@11
    .line 4447
    .local v2, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getAction()I

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_1c

    #@17
    .line 4448
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@19
    invoke-static {v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$2800(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_20

    #@1c
    .line 4452
    .end local v2           #motionEvent:Landroid/view/MotionEvent;
    :cond_1c
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@1f
    .line 4454
    return-void

    #@20
    .line 4452
    :catchall_20
    move-exception v3

    #@21
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@24
    throw v3
.end method
