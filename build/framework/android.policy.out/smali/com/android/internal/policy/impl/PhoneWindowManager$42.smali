.class Lcom/android/internal/policy/impl/PhoneWindowManager$42;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->showGoHomeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8221
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 8224
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@4
    if-eqz v1, :cond_11

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@8
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@a
    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 8263
    :cond_10
    :goto_10
    return-void

    #@11
    .line 8228
    :cond_11
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@13
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@15
    if-eqz v1, :cond_21

    #@17
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@19
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@1b
    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_10

    #@21
    .line 8232
    :cond_21
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@23
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@25
    if-nez v1, :cond_76

    #@27
    .line 8233
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@29
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2b
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2d
    const/4 v2, 0x2

    #@2e
    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@31
    .line 8235
    .local v0, ab:Landroid/app/AlertDialog$Builder;
    const v1, 0x2090263

    #@34
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@37
    .line 8236
    const v1, 0x2090265

    #@3a
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@3d
    .line 8237
    const v1, 0x1080027

    #@40
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@43
    .line 8238
    const v1, 0x2090267

    #@46
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$42$1;

    #@48
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$42$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$42;)V

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@4e
    .line 8244
    const v1, 0x2090268

    #@51
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$42$2;

    #@53
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$42$2;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$42;)V

    #@56
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@59
    .line 8252
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$42$3;

    #@5b
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$42$3;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$42;)V

    #@5e
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@61
    .line 8258
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@63
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@66
    move-result-object v2

    #@67
    iput-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@69
    .line 8259
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@6b
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@6d
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@70
    move-result-object v1

    #@71
    const/16 v2, 0x7d8

    #@73
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@76
    .line 8262
    .end local v0           #ab:Landroid/app/AlertDialog$Builder;
    :cond_76
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$42;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@78
    iget-object v1, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog:Landroid/app/AlertDialog;

    #@7a
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@7d
    goto :goto_10
.end method
