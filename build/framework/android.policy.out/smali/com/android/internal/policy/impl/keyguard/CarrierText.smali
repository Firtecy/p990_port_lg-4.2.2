.class public Lcom/android/internal/policy/impl/keyguard/CarrierText;
.super Landroid/widget/TextView;
.source "CarrierText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/CarrierText$2;,
        Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    }
.end annotation


# static fields
.field private static mSeparator:Ljava/lang/CharSequence;


# instance fields
.field private mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 73
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 39
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;-><init>(Lcom/android/internal/policy/impl/keyguard/CarrierText;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@a
    .line 78
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@13
    .line 79
    return-void
.end method

.method private static concatenate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 7
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 263
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_2c

    #@8
    move v0, v2

    #@9
    .line 264
    .local v0, plmnValid:Z
    :goto_9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_2e

    #@f
    move v1, v2

    #@10
    .line 265
    .local v1, spnValid:Z
    :goto_10
    if-eqz v0, :cond_30

    #@12
    if-eqz v1, :cond_30

    #@14
    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    sget-object v3, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mSeparator:Ljava/lang/CharSequence;

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object p0

    #@2b
    .line 272
    .end local p0
    :cond_2b
    :goto_2b
    return-object p0

    #@2c
    .end local v0           #plmnValid:Z
    .end local v1           #spnValid:Z
    .restart local p0
    :cond_2c
    move v0, v3

    #@2d
    .line 263
    goto :goto_9

    #@2e
    .restart local v0       #plmnValid:Z
    :cond_2e
    move v1, v3

    #@2f
    .line 264
    goto :goto_10

    #@30
    .line 267
    .restart local v1       #spnValid:Z
    :cond_30
    if-nez v0, :cond_2b

    #@32
    .line 269
    if-eqz v1, :cond_36

    #@34
    move-object p0, p1

    #@35
    .line 270
    goto :goto_2b

    #@36
    .line 272
    :cond_36
    const-string p0, ""

    #@38
    goto :goto_2b
.end method

.method private getCarrierHelpTextForSimState(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 8
    .parameter "simState"
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    .line 279
    .local v0, carrierHelpTextId:I
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getStatusForIccState(Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@4
    move-result-object v1

    #@5
    .line 280
    .local v1, status:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$2;->$SwitchMap$com$android$internal$policy$impl$keyguard$CarrierText$StatusMode:[I

    #@7
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->ordinal()I

    #@a
    move-result v3

    #@b
    aget v2, v2, v3

    #@d
    packed-switch v2, :pswitch_data_28

    #@10
    .line 303
    :goto_10
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@15
    move-result-object v2

    #@16
    return-object v2

    #@17
    .line 282
    :pswitch_17
    const v0, 0x104030e

    #@1a
    .line 283
    goto :goto_10

    #@1b
    .line 286
    :pswitch_1b
    const v0, 0x104031d

    #@1e
    .line 287
    goto :goto_10

    #@1f
    .line 290
    :pswitch_1f
    const v0, 0x104031f

    #@22
    .line 291
    goto :goto_10

    #@23
    .line 294
    :pswitch_23
    const v0, 0x104031c

    #@26
    .line 295
    goto :goto_10

    #@27
    .line 280
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x3
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
    .end packed-switch
.end method

.method private getStatusForIccState(Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    .registers 5
    .parameter "simState"

    #@0
    .prologue
    .line 228
    if-nez p1, :cond_5

    #@2
    .line 229
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->Normal:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@4
    .line 259
    :goto_4
    return-object v1

    #@5
    .line 232
    :cond_5
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@7
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_2c

    #@11
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@13
    if-eq p1, v1, :cond_19

    #@15
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@17
    if-ne p1, v1, :cond_2c

    #@19
    :cond_19
    const/4 v0, 0x1

    #@1a
    .line 238
    .local v0, missingAndNotProvisioned:Z
    :goto_1a
    if-eqz v0, :cond_1e

    #@1c
    sget-object p1, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1e
    .line 239
    :cond_1e
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$2;->$SwitchMap$com$android$internal$telephony$IccCardConstants$State:[I

    #@20
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    #@23
    move-result v2

    #@24
    aget v1, v1, v2

    #@26
    packed-switch v1, :pswitch_data_4a

    #@29
    .line 259
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@2b
    goto :goto_4

    #@2c
    .line 232
    .end local v0           #missingAndNotProvisioned:Z
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_1a

    #@2e
    .line 241
    .restart local v0       #missingAndNotProvisioned:Z
    :pswitch_2e
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@30
    goto :goto_4

    #@31
    .line 243
    :pswitch_31
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->PersoLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@33
    goto :goto_4

    #@34
    .line 245
    :pswitch_34
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimNotReady:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@36
    goto :goto_4

    #@37
    .line 247
    :pswitch_37
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@39
    goto :goto_4

    #@3a
    .line 249
    :pswitch_3a
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPukLocked:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@3c
    goto :goto_4

    #@3d
    .line 251
    :pswitch_3d
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->Normal:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@3f
    goto :goto_4

    #@40
    .line 253
    :pswitch_40
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimPermDisabled:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@42
    goto :goto_4

    #@43
    .line 255
    :pswitch_43
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimMissing:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@45
    goto :goto_4

    #@46
    .line 257
    :pswitch_46
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->SimIOError:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@48
    goto :goto_4

    #@49
    .line 239
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3a
        :pswitch_3d
        :pswitch_40
        :pswitch_43
        :pswitch_46
    .end packed-switch
.end method

.method private makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "simMessage"
    .parameter "emergencyCallMessage"

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->isEmergencyCallCapable()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_c

    #@8
    .line 218
    invoke-static {p1, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->concatenate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@b
    move-result-object p1

    #@c
    .line 220
    .end local p1
    :cond_c
    return-object p1
.end method


# virtual methods
.method protected getCarrierTextForSimState(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 11
    .parameter "simState"
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    const v6, 0x104032c

    #@3
    const v5, 0x1040329

    #@6
    const v4, 0x104031a

    #@9
    .line 128
    const/4 v0, 0x0

    #@a
    .line 129
    .local v0, carrierText:Ljava/lang/CharSequence;
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getStatusForIccState(Lcom/android/internal/telephony/IccCardConstants$State;)Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;

    #@d
    move-result-object v1

    #@e
    .line 130
    .local v1, status:Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/CarrierText$2;->$SwitchMap$com$android$internal$policy$impl$keyguard$CarrierText$StatusMode:[I

    #@10
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/CarrierText$StatusMode;->ordinal()I

    #@13
    move-result v3

    #@14
    aget v2, v2, v3

    #@16
    packed-switch v2, :pswitch_data_e6

    #@19
    .line 205
    :goto_19
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v2

    #@21
    const-string v3, "airplane_mode_on"

    #@23
    const/4 v4, 0x0

    #@24
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_39

    #@2a
    .line 206
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@31
    move-result-object v2

    #@32
    const v3, 0x104010b

    #@35
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@38
    move-result-object v0

    #@39
    .line 209
    :cond_39
    return-object v0

    #@3a
    .line 132
    :pswitch_3a
    invoke-static {p2, p3}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->concatenate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@3d
    move-result-object v0

    #@3e
    .line 133
    goto :goto_19

    #@3f
    .line 136
    :pswitch_3f
    const/4 v0, 0x0

    #@40
    .line 137
    goto :goto_19

    #@41
    .line 140
    :pswitch_41
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@43
    if-eqz v2, :cond_4c

    #@45
    .line 141
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@47
    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4a
    move-result-object v0

    #@4b
    goto :goto_19

    #@4c
    .line 143
    :cond_4c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@4e
    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@51
    move-result-object v2

    #@52
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@55
    move-result-object v0

    #@56
    .line 146
    goto :goto_19

    #@57
    .line 153
    :pswitch_57
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@59
    if-eqz v2, :cond_64

    #@5b
    .line 154
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@62
    move-result-object v0

    #@63
    goto :goto_19

    #@64
    .line 156
    :cond_64
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@6b
    move-result-object v2

    #@6c
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@6f
    move-result-object v0

    #@70
    .line 160
    goto :goto_19

    #@71
    .line 163
    :pswitch_71
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@73
    if-eqz v2, :cond_81

    #@75
    .line 164
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@78
    move-result-object v2

    #@79
    const v3, 0x209026f

    #@7c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@7f
    move-result-object v0

    #@80
    goto :goto_19

    #@81
    .line 167
    :cond_81
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@84
    move-result-object v2

    #@85
    const v3, 0x104031e

    #@88
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@8b
    move-result-object v0

    #@8c
    .line 170
    goto :goto_19

    #@8d
    .line 173
    :pswitch_8d
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@8f
    if-eqz v2, :cond_9a

    #@91
    .line 174
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@98
    move-result-object v0

    #@99
    goto :goto_19

    #@9a
    .line 176
    :cond_9a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a1
    move-result-object v2

    #@a2
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@a5
    move-result-object v0

    #@a6
    .line 180
    goto/16 :goto_19

    #@a8
    .line 183
    :pswitch_a8
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@aa
    if-eqz v2, :cond_b6

    #@ac
    .line 184
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@af
    move-result-object v2

    #@b0
    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@b3
    move-result-object v0

    #@b4
    goto/16 :goto_19

    #@b6
    .line 186
    :cond_b6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@b9
    move-result-object v2

    #@ba
    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@bd
    move-result-object v2

    #@be
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@c1
    move-result-object v0

    #@c2
    .line 190
    goto/16 :goto_19

    #@c4
    .line 193
    :pswitch_c4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@c7
    move-result-object v2

    #@c8
    const v3, 0x104032a

    #@cb
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ce
    move-result-object v2

    #@cf
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@d2
    move-result-object v0

    #@d3
    .line 196
    goto/16 :goto_19

    #@d5
    .line 199
    :pswitch_d5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getContext()Landroid/content/Context;

    #@d8
    move-result-object v2

    #@d9
    const v3, 0x1040326

    #@dc
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@df
    move-result-object v2

    #@e0
    invoke-direct {p0, v2, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->makeCarrierStringOnEmergencyCapable(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@e3
    move-result-object v0

    #@e4
    goto/16 :goto_19

    #@e6
    .line 130
    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_3f
        :pswitch_41
        :pswitch_57
        :pswitch_71
        :pswitch_8d
        :pswitch_a8
        :pswitch_c4
        :pswitch_d5
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 104
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    #@3
    .line 105
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isMultiSimEnabled:Z

    #@5
    if-eqz v0, :cond_8

    #@7
    .line 109
    :goto_7
    return-void

    #@8
    .line 108
    :cond_8
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@10
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@13
    goto :goto_7
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 113
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    #@3
    .line 114
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 115
    return-void
.end method

.method protected onFinishInflate()V
    .registers 3

    #@0
    .prologue
    .line 92
    invoke-super {p0}, Landroid/widget/TextView;->onFinishInflate()V

    #@3
    .line 93
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v0

    #@7
    const v1, 0x104056b

    #@a
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mSeparator:Ljava/lang/CharSequence;

    #@10
    .line 94
    const/4 v0, 0x1

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->setSelected(Z)V

    #@14
    .line 95
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@16
    if-eqz v0, :cond_21

    #@18
    .line 97
    const-string v0, "/system/fonts/Roboto-Regular.ttf"

    #@1a
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->setTypeface(Landroid/graphics/Typeface;)V

    #@21
    .line 100
    :cond_21
    return-void
.end method

.method protected updateCarrierText(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 6
    .parameter "simState"
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 82
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->getCarrierTextForSimState(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    .line 83
    .local v0, text:Ljava/lang/CharSequence;
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->USE_UPPER_CASE:Z

    #@6
    if-eqz v1, :cond_18

    #@8
    .line 84
    if-eqz v0, :cond_16

    #@a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    :goto_12
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->setText(Ljava/lang/CharSequence;)V

    #@15
    .line 88
    :goto_15
    return-void

    #@16
    .line 84
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_12

    #@18
    .line 86
    :cond_18
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->setText(Ljava/lang/CharSequence;)V

    #@1b
    goto :goto_15
.end method
