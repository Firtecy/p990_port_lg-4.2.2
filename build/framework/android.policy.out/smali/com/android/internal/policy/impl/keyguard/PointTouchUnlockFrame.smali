.class public Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;
.super Landroid/widget/FrameLayout;
.source "PointTouchUnlockFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;
    }
.end annotation


# static fields
.field private static final BUTTON_DRAWING_STATE_BLINK:I = 0x1

.field private static final BUTTON_DRAWING_STATE_IDLE:I = 0x0

.field private static final BUTTON_DRAWING_STATE_SHINING:I = 0x2

.field private static final VIBRATE_LONG:J = 0x28L

.field private static final VIBRATE_SHORT:J = 0x1eL


# instance fields
.field private DEBUG_ON:Z

.field private TAG:Ljava/lang/String;

.field private mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private mBlinkingField:Landroid/widget/ImageView;

.field private mButtonDrawingState:I

.field private mFlashTap:Landroid/widget/ImageView;

.field private mIsButtonPressed:Z

.field private mIsUnlocked:Z

.field private mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

.field private mPadLock:Landroid/widget/ImageView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 30
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->DEBUG_ON:Z

    #@7
    .line 31
    new-instance v0, Ljava/lang/String;

    #@9
    const-string v1, "PointTouchUnlockFrame"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@10
    .line 32
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@12
    .line 33
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@14
    .line 34
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mFlashTap:Landroid/widget/ImageView;

    #@16
    .line 35
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@18
    .line 36
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@1a
    .line 37
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@1c
    .line 38
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

    #@1e
    .line 39
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@20
    .line 40
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mVibrator:Landroid/os/Vibrator;

    #@22
    .line 41
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPowerManager:Landroid/os/PowerManager;

    #@24
    .line 47
    const-string v0, "PointTouchUnlockFrame"

    #@26
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@29
    .line 49
    const-string v0, "PointTouchUnlockFrame"

    #@2b
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@2e
    .line 50
    return-void
.end method

.method private isButtonPressed(FF)Z
    .registers 7
    .parameter "positionX"
    .parameter "positionY"

    #@0
    .prologue
    .line 119
    const-string v2, "isButtonPressed"

    #@2
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 121
    const/4 v0, 0x0

    #@6
    .line 122
    .local v0, pressed:Z
    new-instance v1, Landroid/graphics/Rect;

    #@8
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@b
    .line 124
    .local v1, rect:Landroid/graphics/Rect;
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->getDrawingRect(Landroid/graphics/Rect;)V

    #@e
    .line 125
    float-to-int v2, p1

    #@f
    float-to-int v3, p2

    #@10
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_17

    #@16
    .line 126
    const/4 v0, 0x1

    #@17
    .line 129
    :cond_17
    const-string v2, "isButtonPressed"

    #@19
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@1c
    .line 130
    return v0
.end method

.method private onButtonPressed()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 253
    const-string v0, "onButtonPressed"

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@6
    .line 255
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@8
    .line 256
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->stopButtonBlink()V

    #@b
    .line 257
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonShining(Z)V

    #@e
    .line 258
    const-wide/16 v0, 0x1e

    #@10
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->vibrate(J)V

    #@13
    .line 259
    const-string v0, "onButtonPressed"

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@18
    .line 260
    return-void
.end method

.method private onButtonReleased(Z)V
    .registers 4
    .parameter "haspressed"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 263
    const-string v0, "onButtonReleased"

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@6
    .line 265
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@8
    .line 267
    const/4 v0, 0x1

    #@9
    if-ne p1, v0, :cond_14

    #@b
    .line 268
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->unLockScreen()V

    #@e
    .line 274
    :goto_e
    const-string v0, "onButtonReleased"

    #@10
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@13
    .line 275
    return-void

    #@14
    .line 270
    :cond_14
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonShining(Z)V

    #@17
    .line 271
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->startButtonBlink()V

    #@1a
    goto :goto_e
.end method

.method private setButtonDisabled()V
    .registers 3

    #@0
    .prologue
    .line 242
    const-string v0, "setButtonDisabled"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 243
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_13

    #@a
    .line 244
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->stopButtonBlink()V

    #@d
    .line 249
    :cond_d
    :goto_d
    const-string v0, "setButtonDisabled"

    #@f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@12
    .line 250
    return-void

    #@13
    .line 245
    :cond_13
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@15
    const/4 v1, 0x2

    #@16
    if-ne v0, v1, :cond_d

    #@18
    .line 246
    const/4 v0, 0x0

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonShining(Z)V

    #@1c
    goto :goto_d
.end method

.method private setButtonShining(Z)V
    .registers 5
    .parameter "shining"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    const/4 v1, 0x0

    #@2
    .line 220
    const-string v0, "setButtonShining"

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@7
    .line 222
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@9
    if-eqz v0, :cond_29

    #@b
    .line 224
    const/4 v0, 0x1

    #@c
    if-ne p1, v0, :cond_21

    #@e
    .line 225
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mFlashTap:Landroid/widget/ImageView;

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@13
    .line 226
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@15
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@18
    .line 227
    const/4 v0, 0x2

    #@19
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@1b
    .line 238
    :goto_1b
    const-string v0, "setButtonShining"

    #@1d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@20
    .line 239
    return-void

    #@21
    .line 229
    :cond_21
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mFlashTap:Landroid/widget/ImageView;

    #@23
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@26
    .line 230
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@28
    goto :goto_1b

    #@29
    .line 234
    :cond_29
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@2b
    const-string v1, "PointTouchUnlockFrame.setButtonShining(): mBlinkingField is missing"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_1b
.end method

.method private startButtonBlink()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 158
    const-string v0, "startButtonBlink"

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@7
    .line 160
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPowerManager:Landroid/os/PowerManager;

    #@9
    if-nez v0, :cond_19

    #@b
    .line 161
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->getContext()Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    const-string v1, "power"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/os/PowerManager;

    #@17
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPowerManager:Landroid/os/PowerManager;

    #@19
    .line 163
    :cond_19
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPowerManager:Landroid/os/PowerManager;

    #@1b
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@1e
    move-result v0

    #@1f
    if-eq v0, v3, :cond_43

    #@21
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v1, "mPowerManager.isScreenOn()="

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPowerManager:Landroid/os/PowerManager;

    #@2e
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    #@31
    move-result v1

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceLog(Ljava/lang/String;)V

    #@3d
    .line 165
    const-string v0, "startButtonBlink"

    #@3f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@42
    .line 199
    :goto_42
    return-void

    #@43
    .line 169
    :cond_43
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@45
    if-eqz v0, :cond_80

    #@47
    .line 171
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@49
    const/4 v1, 0x2

    #@4a
    if-ne v0, v1, :cond_4f

    #@4c
    .line 173
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonShining(Z)V

    #@4f
    .line 176
    :cond_4f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@51
    if-nez v0, :cond_65

    #@53
    .line 177
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@55
    const v1, 0x2040007

    #@58
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    #@5b
    .line 178
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@5d
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@60
    move-result-object v0

    #@61
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    #@63
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@65
    .line 184
    :cond_65
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@67
    if-eqz v0, :cond_7a

    #@69
    .line 186
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@6b
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    #@6e
    .line 188
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@70
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@73
    .line 189
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@75
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    #@78
    .line 190
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@7a
    .line 198
    :cond_7a
    :goto_7a
    const-string v0, "startButtonBlink"

    #@7c
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@7f
    goto :goto_42

    #@80
    .line 194
    :cond_80
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@82
    const-string v1, "PointTouchUnlockFrame.startButtonBlink(): mBlinkingField is missing"

    #@84
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    goto :goto_7a
.end method

.method private stopButtonBlink()V
    .registers 3

    #@0
    .prologue
    .line 202
    const-string v0, "stopButtonBlink"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 204
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@7
    if-eqz v0, :cond_23

    #@9
    .line 205
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@b
    if-eqz v0, :cond_1b

    #@d
    .line 206
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkAnimation:Landroid/graphics/drawable/AnimationDrawable;

    #@f
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    #@12
    .line 207
    const/4 v0, 0x0

    #@13
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mButtonDrawingState:I

    #@15
    .line 216
    :goto_15
    const-string v0, "stopButtonBlink"

    #@17
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@1a
    .line 217
    return-void

    #@1b
    .line 209
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@1d
    const-string v1, "PointTouchUnlockFrame.stopButtonBlink(): mBlinkAnimation is missing"

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_15

    #@23
    .line 213
    :cond_23
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@25
    const-string v1, "PointTouchUnlockFrame.stopButtonBlink(): mBlinkingField is missing"

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_15
.end method

.method private traceIn(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "   <<<<< IN"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceLog(Ljava/lang/String;)V

    #@16
    .line 336
    return-void
.end method

.method private traceLog(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->DEBUG_ON:Z

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_1d

    #@5
    .line 330
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "PointTouchUnlockFrame."

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 332
    :cond_1d
    return-void
.end method

.method private traceOut(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "   >>>>> OUT"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceLog(Ljava/lang/String;)V

    #@16
    .line 340
    return-void
.end method

.method private unLockScreen()V
    .registers 3

    #@0
    .prologue
    .line 278
    const-string v0, "unLockScreen"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 279
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@7
    if-eqz v0, :cond_2d

    #@9
    .line 280
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@b
    const v1, 0x2020184

    #@e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@11
    .line 285
    :goto_11
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@14
    .line 287
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

    #@16
    if-eqz v0, :cond_35

    #@18
    .line 288
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

    #@1a
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;->onScreenTimeOutExtended()V

    #@1d
    .line 289
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

    #@1f
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;->onUnlocked()V

    #@22
    .line 295
    :goto_22
    const-wide/16 v0, 0x28

    #@24
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->vibrate(J)V

    #@27
    .line 297
    const-string v0, "unLockScreen"

    #@29
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@2c
    .line 298
    return-void

    #@2d
    .line 282
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@2f
    const-string v1, "PointTouchUnlockFrame.unLockScreen(): mPadLock is missing"

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_11

    #@35
    .line 292
    :cond_35
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->TAG:Ljava/lang/String;

    #@37
    const-string v1, "PointTouchUnlockFrame.unLockScreen(): mOnTriggerListener is missing"

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_22
.end method

.method private declared-synchronized vibrate(J)V
    .registers 5
    .parameter "duration"

    #@0
    .prologue
    .line 304
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mVibrator:Landroid/os/Vibrator;

    #@3
    if-nez v0, :cond_13

    #@5
    .line 305
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->getContext()Landroid/content/Context;

    #@8
    move-result-object v0

    #@9
    const-string v1, "vibrator"

    #@b
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/os/Vibrator;

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mVibrator:Landroid/os/Vibrator;

    #@13
    .line 308
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mVibrator:Landroid/os/Vibrator;

    #@15
    invoke-virtual {v0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1a

    #@18
    .line 309
    monitor-exit p0

    #@19
    return-void

    #@1a
    .line 304
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit p0

    #@1c
    throw v0
.end method


# virtual methods
.method public initFrame()V
    .registers 3

    #@0
    .prologue
    .line 134
    const-string v0, "initFrame"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 136
    const v0, 0x20d0070

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/widget/ImageView;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@10
    .line 137
    const v0, 0x20d006f

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/ImageView;

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mBlinkingField:Landroid/widget/ImageView;

    #@1b
    .line 138
    const v0, 0x20d006e

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->findViewById(I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/widget/ImageView;

    #@24
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mFlashTap:Landroid/widget/ImageView;

    #@26
    .line 140
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mFlashTap:Landroid/widget/ImageView;

    #@28
    const/4 v1, 0x4

    #@29
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@2c
    .line 141
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@2e
    if-eqz v0, :cond_38

    #@30
    .line 142
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@32
    const v1, 0x2020183

    #@35
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@38
    .line 144
    :cond_38
    const/4 v0, 0x0

    #@39
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@3b
    .line 146
    const-string v0, "initFrame"

    #@3d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@40
    .line 147
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 323
    const-string v0, "onPause"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 324
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonDisabled()V

    #@8
    .line 325
    const-string v0, "onPause"

    #@a
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@d
    .line 326
    return-void
.end method

.method public onResume()V
    .registers 3

    #@0
    .prologue
    .line 312
    const-string v0, "onResume"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 313
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 314
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@b
    const v1, 0x2020183

    #@e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@11
    .line 316
    :cond_11
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@14
    .line 317
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->startButtonBlink()V

    #@17
    .line 319
    const-string v0, "onResume"

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@1c
    .line 320
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 76
    const-string v4, "onTouchEvent"

    #@3
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@6
    .line 78
    const/4 v1, 0x0

    #@7
    .line 80
    .local v1, bret:Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v0

    #@b
    .line 81
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@e
    move-result v2

    #@f
    .line 82
    .local v2, positionX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v3

    #@13
    .line 84
    .local v3, positionY:F
    packed-switch v0, :pswitch_data_42

    #@16
    .line 109
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v1

    #@1a
    .line 113
    :goto_1a
    const-string v4, "onTouchEvent"

    #@1c
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@1f
    .line 114
    return v1

    #@20
    .line 86
    :pswitch_20
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@22
    if-nez v4, :cond_27

    #@24
    .line 87
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onButtonPressed()V

    #@27
    .line 89
    :cond_27
    const/4 v1, 0x1

    #@28
    .line 90
    goto :goto_1a

    #@29
    .line 93
    :pswitch_29
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@2b
    if-ne v4, v5, :cond_30

    #@2d
    .line 94
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onButtonReleased(Z)V

    #@30
    .line 96
    :cond_30
    const/4 v1, 0x1

    #@31
    .line 97
    goto :goto_1a

    #@32
    .line 100
    :pswitch_32
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@34
    if-ne v4, v5, :cond_40

    #@36
    .line 101
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->isButtonPressed(FF)Z

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_40

    #@3c
    .line 102
    const/4 v4, 0x0

    #@3d
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onButtonReleased(Z)V

    #@40
    .line 105
    :cond_40
    const/4 v1, 0x1

    #@41
    .line 106
    goto :goto_1a

    #@42
    .line 84
    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_20
        :pswitch_29
        :pswitch_32
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasWindowFocus"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 54
    const-string v0, "onWindowFocusChanged"

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@7
    .line 56
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    #@a
    .line 58
    if-ne p1, v1, :cond_23

    #@c
    .line 59
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 60
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mPadLock:Landroid/widget/ImageView;

    #@12
    const v1, 0x2020183

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@18
    .line 62
    :cond_18
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsUnlocked:Z

    #@1a
    .line 63
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->startButtonBlink()V

    #@1d
    .line 71
    :goto_1d
    const-string v0, "onWindowFocusChanged"

    #@1f
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@22
    .line 72
    return-void

    #@23
    .line 65
    :cond_23
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mIsButtonPressed:Z

    #@25
    if-ne v0, v1, :cond_2a

    #@27
    .line 66
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->onButtonReleased(Z)V

    #@2a
    .line 68
    :cond_2a
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->setButtonDisabled()V

    #@2d
    goto :goto_1d
.end method

.method public setOnTriggerListener(Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 150
    const-string v0, "setOnTriggerListener"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceIn(Ljava/lang/String;)V

    #@5
    .line 152
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->mOnTriggerListener:Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame$OnTriggerListener;

    #@7
    .line 154
    const-string v0, "setOnTriggerListener"

    #@9
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/PointTouchUnlockFrame;->traceOut(Ljava/lang/String;)V

    #@c
    .line 155
    return-void
.end method
