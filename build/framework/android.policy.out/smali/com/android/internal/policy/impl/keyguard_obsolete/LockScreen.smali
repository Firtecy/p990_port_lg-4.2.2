.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;
.super Landroid/widget/LinearLayout;
.source "LockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;
    }
.end annotation


# static fields
.field private static final ASSIST_ICON_METADATA_NAME:Ljava/lang/String; = "com.android.systemui.action_assist_icon"

.field private static final DBG:Z = false

.field private static final ENABLE_MENU_KEY_FILE:Ljava/lang/String; = "/data/local/enable_menu_key"

.field private static final ON_RESUME_PING_DELAY:I = 0x1f4

.field private static final STAY_ON_WHILE_GRABBED_TIMEOUT:I = 0x7530

.field private static final TAG:Ljava/lang/String; = "LockScreen"

.field private static final WAIT_FOR_ANIMATION_TIMEOUT:I


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private mCameraDisabled:Z

.field private mCreationOrientation:I

.field private mEnableMenuKeyInLockScreen:Z

.field private mEnableRingSilenceFallback:Z

.field private final mHasVibrator:Z

.field mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private final mOnResumePing:Ljava/lang/Runnable;

.field private mSearchDisabled:Z

.field private mSilentMode:Z

.field private mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

.field private mUnlockWidget:Landroid/view/View;

.field private mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

.field private mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V
    .registers 15
    .parameter "context"
    .parameter "configuration"
    .parameter "lockPatternUtils"
    .parameter "updateMonitor"
    .parameter "callback"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 448
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@5
    .line 71
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mEnableRingSilenceFallback:Z

    #@7
    .line 88
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$1;

    #@9
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@e
    .line 595
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$3;

    #@10
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@13
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mOnResumePing:Ljava/lang/Runnable;

    #@15
    .line 449
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@17
    .line 450
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@19
    .line 451
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1b
    .line 452
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->shouldEnableMenuKey()Z

    #@1e
    move-result v0

    #@1f
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mEnableMenuKeyInLockScreen:Z

    #@21
    .line 453
    iget v0, p2, Landroid/content/res/Configuration;->orientation:I

    #@23
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCreationOrientation:I

    #@25
    .line 461
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@28
    move-result-object v6

    #@29
    .line 463
    .local v6, inflater:Landroid/view/LayoutInflater;
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCreationOrientation:I

    #@2b
    const/4 v1, 0x2

    #@2c
    if-eq v0, v1, :cond_7d

    #@2e
    .line 464
    const v0, 0x109006c

    #@31
    invoke-virtual {v6, v0, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@34
    .line 469
    :goto_34
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@36
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@38
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3a
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@3c
    move-object v1, p0

    #@3d
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;-><init>(Landroid/view/View;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Z)V

    #@40
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@42
    .line 472
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->setFocusable(Z)V

    #@45
    .line 473
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->setFocusableInTouchMode(Z)V

    #@48
    .line 474
    const/high16 v0, 0x6

    #@4a
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->setDescendantFocusability(I)V

    #@4d
    .line 476
    const-string v0, "vibrator"

    #@4f
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@52
    move-result-object v7

    #@53
    check-cast v7, Landroid/os/Vibrator;

    #@55
    .line 477
    .local v7, vibrator:Landroid/os/Vibrator;
    if-nez v7, :cond_84

    #@57
    :goto_57
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mHasVibrator:Z

    #@59
    .line 478
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@5b
    const-string v1, "audio"

    #@5d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@60
    move-result-object v0

    #@61
    check-cast v0, Landroid/media/AudioManager;

    #@63
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mAudioManager:Landroid/media/AudioManager;

    #@65
    .line 479
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->isSilentMode()Z

    #@68
    move-result v0

    #@69
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@6b
    .line 480
    const v0, 0x1020307

    #@6e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->findViewById(I)Landroid/view/View;

    #@71
    move-result-object v0

    #@72
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidget:Landroid/view/View;

    #@74
    .line 481
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidget:Landroid/view/View;

    #@76
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->createUnlockMethods(Landroid/view/View;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@79
    move-result-object v0

    #@7a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@7c
    .line 485
    return-void

    #@7d
    .line 466
    .end local v7           #vibrator:Landroid/os/Vibrator;
    :cond_7d
    const v0, 0x109006d

    #@80
    invoke-virtual {v6, v0, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@83
    goto :goto_34

    #@84
    .line 477
    .restart local v7       #vibrator:Landroid/os/Vibrator;
    :cond_84
    invoke-virtual {v7}, Landroid/os/Vibrator;->hasVibrator()Z

    #@87
    move-result v5

    #@88
    goto :goto_57
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSearchDisabled:Z

    #@2
    return v0
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->updateTargets()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->toggleRingMode()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->isSilentMode()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->requestUnlockScreen()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCameraDisabled:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mEnableRingSilenceFallback:Z

    #@2
    return v0
.end method

.method private createUnlockMethods(Landroid/view/View;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;
    .registers 12
    .parameter "unlockWidget"

    #@0
    .prologue
    .line 488
    instance-of v6, p1, Lcom/android/internal/widget/SlidingTab;

    #@2
    if-eqz v6, :cond_2a

    #@4
    move-object v3, p1

    #@5
    .line 489
    check-cast v3, Lcom/android/internal/widget/SlidingTab;

    #@7
    .line 490
    .local v3, slidingTabView:Lcom/android/internal/widget/SlidingTab;
    const/4 v6, 0x1

    #@8
    const/4 v7, 0x0

    #@9
    invoke-virtual {v3, v6, v7}, Lcom/android/internal/widget/SlidingTab;->setHoldAfterTrigger(ZZ)V

    #@c
    .line 491
    const v6, 0x104033f

    #@f
    invoke-virtual {v3, v6}, Lcom/android/internal/widget/SlidingTab;->setLeftHintText(I)V

    #@12
    .line 492
    const v6, 0x10802dd

    #@15
    const v7, 0x10803b5

    #@18
    const v8, 0x1080396

    #@1b
    const v9, 0x10803a9

    #@1e
    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/android/internal/widget/SlidingTab;->setLeftTabResources(IIII)V

    #@21
    .line 497
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;

    #@23
    invoke-direct {v2, p0, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/SlidingTab;)V

    #@26
    .line 498
    .local v2, slidingTabMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/SlidingTab;->setOnTriggerListener(Lcom/android/internal/widget/SlidingTab$OnTriggerListener;)V

    #@29
    .line 509
    .end local v2           #slidingTabMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;
    .end local v3           #slidingTabView:Lcom/android/internal/widget/SlidingTab;
    :goto_29
    return-object v2

    #@2a
    .line 500
    :cond_2a
    instance-of v6, p1, Lcom/android/internal/widget/WaveView;

    #@2c
    if-eqz v6, :cond_3b

    #@2e
    move-object v4, p1

    #@2f
    .line 501
    check-cast v4, Lcom/android/internal/widget/WaveView;

    #@31
    .line 502
    .local v4, waveView:Lcom/android/internal/widget/WaveView;
    new-instance v5, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;

    #@33
    invoke-direct {v5, p0, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/WaveView;)V

    #@36
    .line 503
    .local v5, waveViewMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/WaveView;->setOnTriggerListener(Lcom/android/internal/widget/WaveView$OnTriggerListener;)V

    #@39
    move-object v2, v5

    #@3a
    .line 504
    goto :goto_29

    #@3b
    .line 505
    .end local v4           #waveView:Lcom/android/internal/widget/WaveView;
    .end local v5           #waveViewMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$WaveViewMethods;
    :cond_3b
    instance-of v6, p1, Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@3d
    if-eqz v6, :cond_4c

    #@3f
    move-object v0, p1

    #@40
    .line 506
    check-cast v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@42
    .line 507
    .local v0, glowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;

    #@44
    invoke-direct {v1, p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@47
    .line 508
    .local v1, glowPadViewMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setOnTriggerListener(Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;)V

    #@4a
    move-object v2, v1

    #@4b
    .line 509
    goto :goto_29

    #@4c
    .line 511
    .end local v0           #glowPadView:Lcom/android/internal/widget/multiwaveview/GlowPadView;
    .end local v1           #glowPadViewMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;
    :cond_4c
    new-instance v6, Ljava/lang/IllegalStateException;

    #@4e
    new-instance v7, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v8, "Unrecognized unlock widget: "

    #@55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@64
    throw v6
.end method

.method private isSilentMode()Z
    .registers 3

    #@0
    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mAudioManager:Landroid/media/AudioManager;

    #@2
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-eq v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private requestUnlockScreen()V
    .registers 4

    #@0
    .prologue
    .line 403
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$2;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@5
    const-wide/16 v1, 0x0

    #@7
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    #@a
    .line 408
    return-void
.end method

.method private shouldEnableMenuKey()Z
    .registers 7

    #@0
    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v3

    #@4
    .line 431
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x1110025

    #@7
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v0

    #@b
    .line 432
    .local v0, configDisabled:Z
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    #@e
    move-result v2

    #@f
    .line 433
    .local v2, isTestHarness:Z
    new-instance v4, Ljava/io/File;

    #@11
    const-string v5, "/data/local/enable_menu_key"

    #@13
    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@19
    move-result v1

    #@1a
    .line 434
    .local v1, fileOverride:Z
    if-eqz v0, :cond_20

    #@1c
    if-nez v2, :cond_20

    #@1e
    if-eqz v1, :cond_22

    #@20
    :cond_20
    const/4 v4, 0x1

    #@21
    :goto_21
    return v4

    #@22
    :cond_22
    const/4 v4, 0x0

    #@23
    goto :goto_21
.end method

.method private toggleRingMode()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 412
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@4
    if-nez v0, :cond_17

    #@6
    move v0, v1

    #@7
    :goto_7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@9
    .line 413
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSilentMode:Z

    #@b
    if-eqz v0, :cond_1b

    #@d
    .line 414
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mAudioManager:Landroid/media/AudioManager;

    #@f
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mHasVibrator:Z

    #@11
    if-eqz v3, :cond_19

    #@13
    :goto_13
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@16
    .line 420
    :goto_16
    return-void

    #@17
    :cond_17
    move v0, v2

    #@18
    .line 412
    goto :goto_7

    #@19
    :cond_19
    move v1, v2

    #@1a
    .line 414
    goto :goto_13

    #@1b
    .line 418
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mAudioManager:Landroid/media/AudioManager;

    #@1d
    const/4 v1, 0x2

    #@1e
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@21
    goto :goto_16
.end method

.method private updateTargets()V
    .registers 11

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 516
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@4
    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@7
    move-result-object v5

    #@8
    const/4 v8, 0x0

    #@9
    invoke-virtual {v5, v8}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;)Z

    #@c
    move-result v1

    #@d
    .line 518
    .local v1, disabledByAdmin:Z
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@f
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isSimLocked()Z

    #@12
    move-result v2

    #@13
    .line 519
    .local v2, disabledBySimState:Z
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@15
    instance-of v5, v5, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;

    #@17
    if-eqz v5, :cond_6a

    #@19
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@1b
    check-cast v5, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;

    #@1d
    const v8, 0x10802e6

    #@20
    invoke-virtual {v5, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->isTargetPresent(I)Z

    #@23
    move-result v0

    #@24
    .line 523
    .local v0, cameraTargetPresent:Z
    :goto_24
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@26
    instance-of v5, v5, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;

    #@28
    if-eqz v5, :cond_6c

    #@2a
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@2c
    check-cast v5, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;

    #@2e
    const v8, 0x108029a

    #@31
    invoke-virtual {v5, v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$GlowPadViewMethods;->isTargetPresent(I)Z

    #@34
    move-result v4

    #@35
    .line 528
    .local v4, searchTargetPresent:Z
    :goto_35
    if-eqz v1, :cond_6e

    #@37
    .line 529
    const-string v5, "LockScreen"

    #@39
    const-string v8, "Camera disabled by Device Policy"

    #@3b
    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 533
    :cond_3e
    :goto_3e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@40
    const-string v8, "search"

    #@42
    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v5

    #@46
    check-cast v5, Landroid/app/SearchManager;

    #@48
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mContext:Landroid/content/Context;

    #@4a
    const/4 v9, -0x2

    #@4b
    invoke-virtual {v5, v8, v9}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@4e
    move-result-object v5

    #@4f
    if-eqz v5, :cond_78

    #@51
    move v3, v7

    #@52
    .line 536
    .local v3, searchActionAvailable:Z
    :goto_52
    if-nez v1, :cond_58

    #@54
    if-nez v2, :cond_58

    #@56
    if-nez v0, :cond_7a

    #@58
    :cond_58
    move v5, v7

    #@59
    :goto_59
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCameraDisabled:Z

    #@5b
    .line 537
    if-nez v2, :cond_61

    #@5d
    if-eqz v3, :cond_61

    #@5f
    if-nez v4, :cond_62

    #@61
    :cond_61
    move v6, v7

    #@62
    :cond_62
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mSearchDisabled:Z

    #@64
    .line 538
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@66
    invoke-interface {v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;->updateResources()V

    #@69
    .line 539
    return-void

    #@6a
    .end local v0           #cameraTargetPresent:Z
    .end local v3           #searchActionAvailable:Z
    .end local v4           #searchTargetPresent:Z
    :cond_6a
    move v0, v6

    #@6b
    .line 519
    goto :goto_24

    #@6c
    .restart local v0       #cameraTargetPresent:Z
    :cond_6c
    move v4, v6

    #@6d
    .line 523
    goto :goto_35

    #@6e
    .line 530
    .restart local v4       #searchTargetPresent:Z
    :cond_6e
    if-eqz v2, :cond_3e

    #@70
    .line 531
    const-string v5, "LockScreen"

    #@72
    const-string v8, "Camera disabled by Sim State"

    #@74
    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_3e

    #@78
    :cond_78
    move v3, v6

    #@79
    .line 533
    goto :goto_52

    #@7a
    .restart local v3       #searchActionAvailable:Z
    :cond_7a
    move v5, v6

    #@7b
    .line 536
    goto :goto_59
.end method


# virtual methods
.method public cleanUp()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 613
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@3
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@5
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@8
    .line 614
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@a
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;->cleanUp()V

    #@d
    .line 615
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    .line 616
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@11
    .line 617
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@13
    .line 618
    return-void
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 585
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 562
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    #@3
    .line 568
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->updateConfiguration()V

    #@6
    .line 569
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 574
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 580
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->updateConfiguration()V

    #@6
    .line 581
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 547
    const/16 v0, 0x52

    #@2
    if-ne p1, v0, :cond_d

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mEnableMenuKeyInLockScreen:Z

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 548
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@a
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToUnlockScreen()V

    #@d
    .line 550
    :cond_d
    const/4 v0, 0x0

    #@e
    return v0
.end method

.method public onPause()V
    .registers 3

    #@0
    .prologue
    .line 590
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@4
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@7
    .line 591
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onPause()V

    #@c
    .line 592
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUnlockWidgetMethods:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;->reset(Z)V

    #@12
    .line 593
    return-void
.end method

.method public onResume()V
    .registers 4

    #@0
    .prologue
    .line 605
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@4
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;)V

    #@7
    .line 607
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onResume()V

    #@c
    .line 608
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mOnResumePing:Ljava/lang/Runnable;

    #@e
    const-wide/16 v1, 0x1f4

    #@10
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    #@13
    .line 609
    return-void
.end method

.method updateConfiguration()V
    .registers 4

    #@0
    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    .line 555
    .local v0, newConfig:Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    #@a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCreationOrientation:I

    #@c
    if-eq v1, v2, :cond_13

    #@e
    .line 556
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@10
    invoke-interface {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@13
    .line 558
    :cond_13
    return-void
.end method
