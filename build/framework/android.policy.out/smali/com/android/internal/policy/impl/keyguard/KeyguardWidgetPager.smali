.class public Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;
.super Lcom/android/internal/policy/impl/keyguard/PagedView;
.source "KeyguardWidgetPager.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/android/internal/policy/impl/keyguard/ChallengeLayout$OnBouncerStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;
    }
.end annotation


# static fields
.field private static CAMERA_DISTANCE:F = 0.0f

.field public static final CHILDREN_OUTLINE_FADE_IN_DURATION:I = 0x64

.field public static final CHILDREN_OUTLINE_FADE_OUT_DURATION:I = 0x177

.field private static final CUSTOM_WIDGET_USER_ACTIVITY_TIMEOUT:J = 0x7530L

.field protected static OVERSCROLL_MAX_ROTATION:F = 0.0f

.field private static final PERFORM_OVERSCROLL_ROTATION:Z = true

.field private static final TAG:Ljava/lang/String; = "KeyguardWidgetPager"


# instance fields
.field private BOUNCER_SCALE_FACTOR:F

.field private mAddWidgetView:Landroid/view/View;

.field private final mBackgroundWorkerHandler:Landroid/os/Handler;

.field private final mBackgroundWorkerThread:Landroid/os/HandlerThread;

.field private mBouncerZoomInOutDuration:I

.field private mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

.field private mCenterSmallWidgetsVertically:Z

.field protected mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

.field private mHasMeasure:Z

.field private mLastHeightMeasureSpec:I

.field private mLastWidthMeasureSpec:I

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mPage:I

.field protected mScreenCenter:I

.field protected mShowingInitialHints:Z

.field protected mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

.field private mWidgetToResetAfterFadeOut:I

.field mZInterpolator:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;

.field showHintsAfterLayout:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 50
    const v0, 0x461c4000

    #@3
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->CAMERA_DISTANCE:F

    #@5
    .line 51
    const/high16 v0, 0x41f0

    #@7
    sput v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->OVERSCROLL_MAX_ROTATION:F

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 93
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v1, v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 89
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 49
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;

    #@6
    const/high16 v1, 0x3f00

    #@8
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;-><init>(F)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mZInterpolator:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$ZInterpolator;

    #@d
    .line 62
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mHasMeasure:Z

    #@f
    .line 63
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->showHintsAfterLayout:Z

    #@11
    .line 69
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mPage:I

    #@13
    .line 73
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mShowingInitialHints:Z

    #@15
    .line 81
    const/16 v0, 0xfa

    #@17
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBouncerZoomInOutDuration:I

    #@19
    .line 82
    const v0, 0x3f2b851f

    #@1c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->BOUNCER_SCALE_FACTOR:F

    #@1e
    .line 98
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getImportantForAccessibility()I

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_28

    #@24
    .line 99
    const/4 v0, 0x1

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setImportantForAccessibility(I)V

    #@28
    .line 102
    :cond_28
    invoke-virtual {p0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setPageSwitchListener(Lcom/android/internal/policy/impl/keyguard/PagedView$PageSwitchListener;)V

    #@2b
    .line 104
    new-instance v0, Landroid/os/HandlerThread;

    #@2d
    const-string v1, "KeyguardWidgetPager Worker"

    #@2f
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@32
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerThread:Landroid/os/HandlerThread;

    #@34
    .line 105
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerThread:Landroid/os/HandlerThread;

    #@36
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@39
    .line 106
    new-instance v0, Landroid/os/Handler;

    #@3b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerThread:Landroid/os/HandlerThread;

    #@3d
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@40
    move-result-object v1

    #@41
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@44
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerHandler:Landroid/os/Handler;

    #@46
    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mWidgetToResetAfterFadeOut:I

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mWidgetToResetAfterFadeOut:I

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->updateWidgetFramesImportantForAccessibility()V

    #@3
    return-void
.end method

.method private captureUserInteraction(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getCurrentPage()I

    #@3
    move-result v1

    #@4
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@7
    move-result-object v0

    #@8
    .line 218
    .local v0, currentWidgetPage:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v0, :cond_12

    #@a
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->onUserInteraction(Landroid/view/MotionEvent;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method private enforceKeyguardWidgetFrame(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 400
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 401
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "KeyguardWidgetPager children must be KeyguardWidgetFrames"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 404
    :cond_c
    return-void
.end method

.method private updatePageAlphaValues(I)V
    .registers 2
    .parameter "screenCenter"

    #@0
    .prologue
    .line 482
    return-void
.end method

.method private updateWidgetFrameImportantForAccessibility(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)V
    .registers 4
    .parameter "frame"

    #@0
    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContentAlpha()F

    #@3
    move-result v0

    #@4
    const/4 v1, 0x0

    #@5
    cmpg-float v0, v0, v1

    #@7
    if-gtz v0, :cond_e

    #@9
    .line 193
    const/4 v0, 0x2

    #@a
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setImportantForAccessibility(I)V

    #@d
    .line 197
    :goto_d
    return-void

    #@e
    .line 195
    :cond_e
    const/4 v0, 0x1

    #@f
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setImportantForAccessibility(I)V

    #@12
    goto :goto_d
.end method

.method private updateWidgetFramesImportantForAccessibility()V
    .registers 4

    #@0
    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getPageCount()I

    #@3
    move-result v2

    #@4
    .line 185
    .local v2, pageCount:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v2, :cond_11

    #@7
    .line 186
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@a
    move-result-object v0

    #@b
    .line 187
    .local v0, frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->updateWidgetFrameImportantForAccessibility(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)V

    #@e
    .line 185
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 189
    .end local v0           #frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_11
    return-void
.end method

.method private userActivity()V
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 201
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;->onUserActivityTimeoutChanged()V

    #@9
    .line 202
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@b
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;->userActivity()V

    #@e
    .line 204
    :cond_e
    return-void
.end method


# virtual methods
.method public addCustomWidget(Landroid/view/View;I)V
    .registers 5
    .parameter "widget"
    .parameter "pageIndex"

    #@0
    .prologue
    .line 352
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@3
    move-result-object v0

    #@4
    .line 353
    .local v0, fr:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v0, :cond_a

    #@6
    .line 354
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setCustomWidget(Z)V

    #@a
    .line 356
    :cond_a
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .registers 3
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 365
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->enforceKeyguardWidgetFrame(Landroid/view/View;)V

    #@3
    .line 366
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->addView(Landroid/view/View;I)V

    #@6
    .line 367
    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .registers 4
    .parameter "child"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 375
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->enforceKeyguardWidgetFrame(Landroid/view/View;)V

    #@3
    .line 376
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->addView(Landroid/view/View;II)V

    #@6
    .line 377
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 395
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->enforceKeyguardWidgetFrame(Landroid/view/View;)V

    #@3
    .line 396
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/PagedView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@6
    .line 397
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 385
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->enforceKeyguardWidgetFrame(Landroid/view/View;)V

    #@3
    .line 386
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@6
    .line 387
    return-void
.end method

.method public addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .registers 15
    .parameter "widget"
    .parameter "pageIndex"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, -0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 298
    instance-of v7, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@5
    if-nez v7, :cond_71

    #@7
    .line 299
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@9
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getContext()Landroid/content/Context;

    #@c
    move-result-object v7

    #@d
    invoke-direct {v3, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;-><init>(Landroid/content/Context;)V

    #@10
    .line 300
    .local v3, frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    #@12
    invoke-direct {v5, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@15
    .line 302
    .local v5, lp:Landroid/widget/FrameLayout$LayoutParams;
    const/16 v7, 0x30

    #@17
    iput v7, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@19
    .line 306
    invoke-virtual {p1, v11, v11, v11, v11}, Landroid/view/View;->setPadding(IIII)V

    #@1c
    .line 307
    invoke-virtual {v3, p1, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1f
    .line 310
    instance-of v7, p1, Landroid/appwidget/AppWidgetHostView;

    #@21
    if-eqz v7, :cond_33

    #@23
    move-object v0, p1

    #@24
    .line 311
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@26
    .line 312
    .local v0, awhv:Landroid/appwidget/AppWidgetHostView;
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHostView;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    #@29
    move-result-object v4

    #@2a
    .line 313
    .local v4, info:Landroid/appwidget/AppWidgetProviderInfo;
    iget v7, v4, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@2c
    and-int/lit8 v7, v7, 0x2

    #@2e
    if-eqz v7, :cond_65

    #@30
    .line 314
    invoke-virtual {v3, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetLockedSmall(Z)V

    #@33
    .line 327
    .end local v0           #awhv:Landroid/appwidget/AppWidgetHostView;
    .end local v4           #info:Landroid/appwidget/AppWidgetProviderInfo;
    .end local v5           #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_33
    :goto_33
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    #@35
    invoke-direct {v6, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@38
    .line 329
    .local v6, pageLp:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@3b
    .line 330
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerHandler:Landroid/os/Handler;

    #@3d
    invoke-virtual {v3, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWorkerHandler(Landroid/os/Handler;)V

    #@40
    .line 332
    if-ne p2, v8, :cond_75

    #@42
    .line 333
    invoke-virtual {p0, v3, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@45
    .line 339
    :goto_45
    if-ne p1, v3, :cond_79

    #@47
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@4a
    move-result-object v1

    #@4b
    .line 340
    .local v1, content:Landroid/view/View;
    :goto_4b
    if-eqz v1, :cond_61

    #@4d
    .line 341
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@4f
    const v8, 0x104034a

    #@52
    new-array v9, v9, [Ljava/lang/Object;

    #@54
    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    #@57
    move-result-object v10

    #@58
    aput-object v10, v9, v11

    #@5a
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    .line 344
    .local v2, contentDescription:Ljava/lang/String;
    invoke-virtual {v3, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentDescription(Ljava/lang/CharSequence;)V

    #@61
    .line 346
    .end local v2           #contentDescription:Ljava/lang/String;
    :cond_61
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->updateWidgetFrameImportantForAccessibility(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)V

    #@64
    .line 347
    return-object v3

    #@65
    .line 317
    .end local v1           #content:Landroid/view/View;
    .end local v6           #pageLp:Landroid/view/ViewGroup$LayoutParams;
    .restart local v0       #awhv:Landroid/appwidget/AppWidgetHostView;
    .restart local v4       #info:Landroid/appwidget/AppWidgetProviderInfo;
    .restart local v5       #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_65
    invoke-virtual {v3, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetLockedSmall(Z)V

    #@68
    .line 318
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCenterSmallWidgetsVertically:Z

    #@6a
    if-eqz v7, :cond_33

    #@6c
    .line 319
    const/16 v7, 0x11

    #@6e
    iput v7, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@70
    goto :goto_33

    #@71
    .end local v0           #awhv:Landroid/appwidget/AppWidgetHostView;
    .end local v3           #frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v4           #info:Landroid/appwidget/AppWidgetProviderInfo;
    .end local v5           #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_71
    move-object v3, p1

    #@72
    .line 324
    check-cast v3, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@74
    .restart local v3       #frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    goto :goto_33

    #@75
    .line 335
    .restart local v6       #pageLp:Landroid/view/ViewGroup$LayoutParams;
    :cond_75
    invoke-virtual {p0, v3, p2, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@78
    goto :goto_45

    #@79
    :cond_79
    move-object v1, p1

    #@7a
    .line 339
    goto :goto_4b
.end method

.method public addWidget(Landroid/view/View;)V
    .registers 3
    .parameter "widget"

    #@0
    .prologue
    .line 251
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addWidget(Landroid/view/View;I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@4
    .line 252
    return-void
.end method

.method animateOutlinesAndSidePages(Z)V
    .registers 3
    .parameter "show"

    #@0
    .prologue
    .line 666
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->animateOutlinesAndSidePages(ZI)V

    #@4
    .line 667
    return-void
.end method

.method animateOutlinesAndSidePages(ZI)V
    .registers 16
    .parameter "show"
    .parameter "duration"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 678
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@4
    if-eqz v9, :cond_e

    #@6
    .line 679
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@8
    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->cancel()V

    #@b
    .line 680
    const/4 v9, 0x0

    #@c
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@e
    .line 682
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@11
    move-result v4

    #@12
    .line 684
    .local v4, count:I
    new-instance v2, Ljava/util/ArrayList;

    #@14
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@17
    .line 686
    .local v2, anims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    const/4 v9, -0x1

    #@18
    if-ne p2, v9, :cond_1e

    #@1a
    .line 687
    if-eqz p1, :cond_54

    #@1c
    const/16 p2, 0x64

    #@1e
    .line 691
    :cond_1e
    :goto_1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getNextPage()I

    #@21
    move-result v5

    #@22
    .line 692
    .local v5, curPage:I
    const/4 v8, 0x0

    #@23
    .local v8, i:I
    :goto_23
    if-ge v8, v4, :cond_62

    #@25
    .line 694
    if-eqz p1, :cond_57

    #@27
    .line 695
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@29
    invoke-virtual {p0, v9, v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getAlphaForPage(IIZ)F

    #@2c
    move-result v6

    #@2d
    .line 701
    .local v6, finalContentAlpha:F
    :goto_2d
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@30
    move-result-object v3

    #@31
    .line 703
    .local v3, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    const-string v9, "contentAlpha"

    #@33
    new-array v10, v11, [F

    #@35
    aput v6, v10, v12

    #@37
    invoke-static {v9, v10}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@3a
    move-result-object v1

    #@3b
    .line 704
    .local v1, alpha:Landroid/animation/PropertyValuesHolder;
    new-array v9, v11, [Landroid/animation/PropertyValuesHolder;

    #@3d
    aput-object v1, v9, v12

    #@3f
    invoke-static {v3, v9}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@42
    move-result-object v0

    #@43
    .line 705
    .local v0, a:Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@46
    .line 707
    if-eqz p1, :cond_60

    #@48
    iget v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@4a
    invoke-virtual {p0, v9, v8, v11}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getOutlineAlphaForPage(IIZ)F

    #@4d
    move-result v7

    #@4e
    .line 708
    .local v7, finalOutlineAlpha:F
    :goto_4e
    invoke-virtual {v3, p0, p1, v7, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->fadeFrame(Ljava/lang/Object;ZFI)V

    #@51
    .line 692
    add-int/lit8 v8, v8, 0x1

    #@53
    goto :goto_23

    #@54
    .line 687
    .end local v0           #a:Landroid/animation/ObjectAnimator;
    .end local v1           #alpha:Landroid/animation/PropertyValuesHolder;
    .end local v3           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v5           #curPage:I
    .end local v6           #finalContentAlpha:F
    .end local v7           #finalOutlineAlpha:F
    .end local v8           #i:I
    :cond_54
    const/16 p2, 0x177

    #@56
    goto :goto_1e

    #@57
    .line 696
    .restart local v5       #curPage:I
    .restart local v8       #i:I
    :cond_57
    if-nez p1, :cond_5e

    #@59
    if-ne v8, v5, :cond_5e

    #@5b
    .line 697
    const/high16 v6, 0x3f80

    #@5d
    .restart local v6       #finalContentAlpha:F
    goto :goto_2d

    #@5e
    .line 699
    .end local v6           #finalContentAlpha:F
    :cond_5e
    const/4 v6, 0x0

    #@5f
    .restart local v6       #finalContentAlpha:F
    goto :goto_2d

    #@60
    .line 707
    .restart local v0       #a:Landroid/animation/ObjectAnimator;
    .restart local v1       #alpha:Landroid/animation/PropertyValuesHolder;
    .restart local v3       #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_60
    const/4 v7, 0x0

    #@61
    goto :goto_4e

    #@62
    .line 711
    .end local v0           #a:Landroid/animation/ObjectAnimator;
    .end local v1           #alpha:Landroid/animation/PropertyValuesHolder;
    .end local v3           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v6           #finalContentAlpha:F
    :cond_62
    new-instance v9, Landroid/animation/AnimatorSet;

    #@64
    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    #@67
    iput-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@69
    .line 712
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@6b
    invoke-virtual {v9, v2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    #@6e
    .line 714
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@70
    int-to-long v10, p2

    #@71
    invoke-virtual {v9, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@74
    .line 715
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@76
    new-instance v10, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$3;

    #@78
    invoke-direct {v10, p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;Z)V

    #@7b
    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@7e
    .line 738
    iget-object v9, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mChildrenOutlineFadeAnimation:Landroid/animation/AnimatorSet;

    #@80
    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    #@83
    .line 739
    return-void
.end method

.method backgroundAlphaInterpolator(F)F
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 478
    const/high16 v0, 0x3f80

    #@2
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method boundByReorderablePages(Z[I)V
    .registers 7
    .parameter "isReordering"
    .parameter "range"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 567
    if-eqz p1, :cond_2e

    #@4
    .line 569
    :goto_4
    aget v0, p2, v3

    #@6
    aget v1, p2, v2

    #@8
    if-lt v0, v1, :cond_19

    #@a
    aget v0, p2, v3

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isWidgetPage(I)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_19

    #@12
    .line 570
    aget v0, p2, v3

    #@14
    add-int/lit8 v0, v0, -0x1

    #@16
    aput v0, p2, v3

    #@18
    goto :goto_4

    #@19
    .line 572
    :cond_19
    :goto_19
    aget v0, p2, v2

    #@1b
    aget v1, p2, v3

    #@1d
    if-gt v0, v1, :cond_2e

    #@1f
    aget v0, p2, v2

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isWidgetPage(I)Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_2e

    #@27
    .line 573
    aget v0, p2, v2

    #@29
    add-int/lit8 v0, v0, 0x1

    #@2b
    aput v0, p2, v2

    #@2d
    goto :goto_19

    #@2e
    .line 576
    :cond_2e
    return-void
.end method

.method protected disablePageContentLayers()V
    .registers 4

    #@0
    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 448
    .local v0, children:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 449
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->disableHardwareLayersForContent()V

    #@e
    .line 448
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 451
    :cond_11
    return-void
.end method

.method protected enablePageContentLayers()V
    .registers 4

    #@0
    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 441
    .local v0, children:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 442
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->enableHardwareLayersForContent()V

    #@e
    .line 441
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 444
    :cond_11
    return-void
.end method

.method public getAlphaForPage(IIZ)F
    .registers 6
    .parameter "screenCenter"
    .parameter "index"
    .parameter "showSidePages"

    #@0
    .prologue
    const/high16 v0, 0x3f80

    #@2
    .line 485
    if-eqz p3, :cond_5

    #@4
    .line 488
    :cond_4
    :goto_4
    return v0

    #@5
    :cond_5
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@7
    if-eq p2, v1, :cond_4

    #@9
    const/4 v0, 0x0

    #@a
    goto :goto_4
.end method

.method public getOutlineAlphaForPage(IIZ)F
    .registers 6
    .parameter "screenCenter"
    .parameter "index"
    .parameter "showSidePages"

    #@0
    .prologue
    .line 493
    if-eqz p3, :cond_b

    #@2
    .line 494
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getAlphaForPage(IIZ)F

    #@5
    move-result v0

    #@6
    const v1, 0x3f19999a

    #@9
    mul-float/2addr v0, v1

    #@a
    .line 497
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getUserActivityTimeout()J
    .registers 6

    #@0
    .prologue
    .line 226
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mPage:I

    #@2
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getPageAt(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 227
    .local v0, page:Landroid/view/View;
    instance-of v3, v0, Landroid/view/ViewGroup;

    #@8
    if-eqz v3, :cond_1d

    #@a
    move-object v1, v0

    #@b
    .line 228
    check-cast v1, Landroid/view/ViewGroup;

    #@d
    .line 229
    .local v1, vg:Landroid/view/ViewGroup;
    const/4 v3, 0x0

    #@e
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v2

    #@12
    .line 230
    .local v2, view:Landroid/view/View;
    instance-of v3, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;

    #@14
    if-nez v3, :cond_1d

    #@16
    instance-of v3, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardMultiUserSelectorView;

    #@18
    if-nez v3, :cond_1d

    #@1a
    .line 232
    const-wide/16 v3, 0x7530

    #@1c
    .line 235
    .end local v1           #vg:Landroid/view/ViewGroup;
    .end local v2           #view:Landroid/view/View;
    :goto_1c
    return-wide v3

    #@1d
    :cond_1d
    const-wide/16 v3, -0x1

    #@1f
    goto :goto_1c
.end method

.method public getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 408
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@6
    return-object v0
.end method

.method public getWidgetPageIndex(Landroid/view/View;)I
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 770
    instance-of v0, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 771
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->indexOfChild(Landroid/view/View;)I

    #@7
    move-result v0

    #@8
    .line 774
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->indexOfChild(Landroid/view/View;)I

    #@12
    move-result v0

    #@13
    goto :goto_8
.end method

.method public getWidgetToResetOnPageFadeOut()I
    .registers 2

    #@0
    .prologue
    .line 674
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mWidgetToResetAfterFadeOut:I

    #@2
    return v0
.end method

.method hideOutlinesAndSidePages()V
    .registers 2

    #@0
    .prologue
    .line 600
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->animateOutlinesAndSidePages(Z)V

    #@4
    .line 601
    return-void
.end method

.method isAddPage(I)Z
    .registers 5
    .parameter "pageIndex"

    #@0
    .prologue
    .line 865
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 866
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_11

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    #@9
    move-result v1

    #@a
    const v2, 0x10202b4

    #@d
    if-ne v1, v2, :cond_11

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method isCameraPage(I)Z
    .registers 4
    .parameter "pageIndex"

    #@0
    .prologue
    .line 870
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 871
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_c

    #@6
    instance-of v1, v0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@8
    if-eqz v1, :cond_c

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method protected isOverScrollChild(IF)Z
    .registers 9
    .parameter "index"
    .parameter "scrollProgress"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 502
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@5
    if-ltz v3, :cond_d

    #@7
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mOverScrollX:I

    #@9
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mMaxScrollX:I

    #@b
    if-le v3, v4, :cond_23

    #@d
    :cond_d
    move v0, v1

    #@e
    .line 503
    .local v0, isInOverscroll:Z
    :goto_e
    if-eqz v0, :cond_25

    #@10
    if-nez p1, :cond_16

    #@12
    cmpg-float v3, p2, v5

    #@14
    if-ltz v3, :cond_22

    #@16
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@19
    move-result v3

    #@1a
    add-int/lit8 v3, v3, -0x1

    #@1c
    if-ne p1, v3, :cond_25

    #@1e
    cmpl-float v3, p2, v5

    #@20
    if-lez v3, :cond_25

    #@22
    :cond_22
    :goto_22
    return v1

    #@23
    .end local v0           #isInOverscroll:Z
    :cond_23
    move v0, v2

    #@24
    .line 502
    goto :goto_e

    #@25
    .restart local v0       #isInOverscroll:Z
    :cond_25
    move v1, v2

    #@26
    .line 503
    goto :goto_22
.end method

.method public isWidgetPage(I)Z
    .registers 6
    .parameter "pageIndex"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 544
    if-ltz p1, :cond_9

    #@3
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@6
    move-result v3

    #@7
    if-lt p1, v3, :cond_a

    #@9
    .line 559
    :cond_9
    :goto_9
    return v2

    #@a
    .line 547
    :cond_a
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    .line 548
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_9

    #@10
    instance-of v3, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@12
    if-eqz v3, :cond_9

    #@14
    move-object v0, v1

    #@15
    .line 549
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@17
    .line 552
    .local v0, kwf:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->isCustomWidget()Z

    #@1a
    move-result v3

    #@1b
    if-nez v3, :cond_9

    #@1d
    .line 557
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContentAppWidgetId()I

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_9

    #@23
    const/4 v2, 0x1

    #@24
    goto :goto_9
.end method

.method public onAddView(Landroid/view/View;I)V
    .registers 7
    .parameter "v"
    .parameter "index"

    #@0
    .prologue
    .line 275
    move-object v2, p1

    #@1
    check-cast v2, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContentAppWidgetId()I

    #@6
    move-result v0

    #@7
    .line 276
    .local v0, appWidgetId:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mTempVisiblePagesRange:[I

    #@9
    array-length v2, v2

    #@a
    new-array v1, v2, [I

    #@c
    .line 277
    .local v1, pagesRange:[I
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getVisiblePages([I)V

    #@f
    .line 278
    const/4 v2, 0x1

    #@10
    invoke-virtual {p0, v2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->boundByReorderablePages(Z[I)V

    #@13
    .line 279
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@15
    if-eqz v2, :cond_1c

    #@17
    .line 280
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@19
    invoke-interface {v2, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;->onAddView(Landroid/view/View;)V

    #@1c
    .line 284
    :cond_1c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerHandler:Landroid/os/Handler;

    #@1e
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$2;

    #@20
    invoke-direct {v3, p0, v0, p2, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;II[I)V

    #@23
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@26
    .line 290
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 627
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onAttachedToWindow()V

    #@3
    .line 628
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mHasMeasure:Z

    #@6
    .line 629
    return-void
.end method

.method public onBouncerStateChanged(Z)V
    .registers 2
    .parameter "bouncerActive"

    #@0
    .prologue
    .line 787
    if-eqz p1, :cond_6

    #@2
    .line 788
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->zoomOutToBouncer()V

    #@5
    .line 792
    :goto_5
    return-void

    #@6
    .line 790
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->zoomInFromBouncer()V

    #@9
    goto :goto_5
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 111
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onDetachedFromWindow()V

    #@3
    .line 114
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerThread:Landroid/os/HandlerThread;

    #@5
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@8
    .line 115
    return-void
.end method

.method protected onEndReordering()V
    .registers 1

    #@0
    .prologue
    .line 591
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onEndReordering()V

    #@3
    .line 592
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->hideOutlinesAndSidePages()V

    #@6
    .line 593
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->captureUserInteraction(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 6
    .parameter "v"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 744
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@4
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->isChallengeShowing()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_1c

    #@a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@c
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->isChallengeOverlapping()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_1c

    #@12
    move v0, v1

    #@13
    .line 746
    .local v0, isChallengeOverlapping:Z
    :goto_13
    if-nez v0, :cond_1e

    #@15
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->startReordering()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_1e

    #@1b
    .line 749
    :goto_1b
    return v1

    #@1c
    .end local v0           #isChallengeOverlapping:Z
    :cond_1c
    move v0, v2

    #@1d
    .line 744
    goto :goto_13

    #@1e
    .restart local v0       #isChallengeOverlapping:Z
    :cond_1e
    move v1, v2

    #@1f
    .line 749
    goto :goto_1b
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 632
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLastWidthMeasureSpec:I

    #@2
    .line 633
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLastHeightMeasureSpec:I

    #@4
    .line 635
    const/4 v4, -0x1

    #@5
    .line 636
    .local v4, maxChallengeTop:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getParent()Landroid/view/ViewParent;

    #@8
    move-result-object v5

    #@9
    check-cast v5, Landroid/view/View;

    #@b
    .line 637
    .local v5, parent:Landroid/view/View;
    const/4 v0, 0x0

    #@c
    .line 641
    .local v0, challengeShowing:Z
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@f
    move-result-object v8

    #@10
    instance-of v8, v8, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@12
    if-eqz v8, :cond_46

    #@14
    .line 642
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@17
    move-result-object v6

    #@18
    check-cast v6, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@1a
    .line 643
    .local v6, scl:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->getMaxChallengeTop()I

    #@1d
    move-result v7

    #@1e
    .line 647
    .local v7, top:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getPaddingTop()I

    #@21
    move-result v8

    #@22
    sub-int v4, v7, v8

    #@24
    .line 648
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->isChallengeShowing()Z

    #@27
    move-result v0

    #@28
    .line 650
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@2b
    move-result v1

    #@2c
    .line 651
    .local v1, count:I
    const/4 v3, 0x0

    #@2d
    .local v3, i:I
    :goto_2d
    if-ge v3, v1, :cond_46

    #@2f
    .line 652
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@32
    move-result-object v2

    #@33
    .line 653
    .local v2, frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setMaxChallengeTop(I)V

    #@36
    .line 656
    if-eqz v0, :cond_43

    #@38
    iget v8, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@3a
    if-ne v3, v8, :cond_43

    #@3c
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mHasMeasure:Z

    #@3e
    if-nez v8, :cond_43

    #@40
    .line 657
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->shrinkWidget()V

    #@43
    .line 651
    :cond_43
    add-int/lit8 v3, v3, 0x1

    #@45
    goto :goto_2d

    #@46
    .line 661
    .end local v1           #count:I
    .end local v2           #frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v3           #i:I
    .end local v6           #scl:Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
    .end local v7           #top:I
    :cond_46
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onMeasure(II)V

    #@49
    .line 662
    const/4 v8, 0x1

    #@4a
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mHasMeasure:Z

    #@4c
    .line 663
    return-void
.end method

.method protected onPageBeginMoving()V
    .registers 2

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 418
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->onPageBeginMoving()V

    #@9
    .line 420
    :cond_9
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isReordering(Z)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_13

    #@10
    .line 421
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->showOutlinesAndSidePages()V

    #@13
    .line 423
    :cond_13
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->userActivity()V

    #@16
    .line 424
    return-void
.end method

.method protected onPageEndMoving()V
    .registers 2

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 429
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->onPageEndMoving()V

    #@9
    .line 434
    :cond_9
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isReordering(Z)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_13

    #@10
    .line 435
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->hideOutlinesAndSidePages()V

    #@13
    .line 437
    :cond_13
    return-void
.end method

.method public onPageSwitched(Landroid/view/View;I)V
    .registers 12
    .parameter "newPage"
    .parameter "newPageIndex"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 134
    const/4 v4, 0x0

    #@2
    .line 135
    .local v4, showingStatusWidget:Z
    instance-of v6, p1, Landroid/view/ViewGroup;

    #@4
    if-eqz v6, :cond_12

    #@6
    move-object v5, p1

    #@7
    .line 136
    check-cast v5, Landroid/view/ViewGroup;

    #@9
    .line 137
    .local v5, vg:Landroid/view/ViewGroup;
    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v6

    #@d
    instance-of v6, v6, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;

    #@f
    if-eqz v6, :cond_12

    #@11
    .line 138
    const/4 v4, 0x1

    #@12
    .line 143
    .end local v5           #vg:Landroid/view/ViewGroup;
    :cond_12
    if-eqz v4, :cond_6a

    #@14
    .line 144
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getSystemUiVisibility()I

    #@17
    move-result v6

    #@18
    const/high16 v7, 0x80

    #@1a
    or-int/2addr v6, v7

    #@1b
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setSystemUiVisibility(I)V

    #@1e
    .line 150
    :goto_1e
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mPage:I

    #@20
    if-eq v6, p2, :cond_60

    #@22
    .line 151
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mPage:I

    #@24
    .line 152
    .local v2, oldPageIndex:I
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mPage:I

    #@26
    .line 153
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->userActivity()V

    #@29
    .line 154
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2c
    move-result-object v3

    #@2d
    .line 155
    .local v3, oldWidgetPage:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v3, :cond_32

    #@2f
    .line 156
    invoke-virtual {v3, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->onActive(Z)V

    #@32
    .line 158
    :cond_32
    invoke-virtual {p0, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@35
    move-result-object v1

    #@36
    .line 159
    .local v1, newWidgetPage:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    if-eqz v1, :cond_3f

    #@38
    .line 160
    const/4 v6, 0x1

    #@39
    invoke-virtual {v1, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->onActive(Z)V

    #@3c
    .line 161
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->requestAccessibilityFocus()Z

    #@3f
    .line 163
    :cond_3f
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mParent:Landroid/view/ViewParent;

    #@41
    if-eqz v6, :cond_60

    #@43
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mContext:Landroid/content/Context;

    #@45
    invoke-static {v6}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_60

    #@4f
    .line 164
    const/16 v6, 0x1000

    #@51
    invoke-static {v6}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@54
    move-result-object v0

    #@55
    .line 166
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@58
    .line 167
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@5b
    .line 168
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mParent:Landroid/view/ViewParent;

    #@5d
    invoke-interface {v6, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@60
    .line 171
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #newWidgetPage:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    .end local v2           #oldPageIndex:I
    .end local v3           #oldWidgetPage:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_60
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@62
    if-eqz v6, :cond_69

    #@64
    .line 172
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@66
    invoke-virtual {v6, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->onPageSwitched(Landroid/view/View;I)V

    #@69
    .line 174
    :cond_69
    return-void

    #@6a
    .line 146
    :cond_6a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getSystemUiVisibility()I

    #@6d
    move-result v6

    #@6e
    const v7, -0x800001

    #@71
    and-int/2addr v6, v7

    #@72
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setSystemUiVisibility(I)V

    #@75
    goto :goto_1e
.end method

.method public onPageSwitching(Landroid/view/View;I)V
    .registers 4
    .parameter "newPage"
    .parameter "newPageIndex"

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 128
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@6
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;->onPageSwitching(Landroid/view/View;I)V

    #@9
    .line 130
    :cond_9
    return-void
.end method

.method public onRemoveView(Landroid/view/View;Z)V
    .registers 6
    .parameter "v"
    .parameter "deletePermanently"

    #@0
    .prologue
    .line 255
    move-object v1, p1

    #@1
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContentAppWidgetId()I

    #@6
    move-result v0

    #@7
    .line 256
    .local v0, appWidgetId:I
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 257
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@d
    invoke-interface {v1, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;->onRemoveView(Landroid/view/View;Z)V

    #@10
    .line 259
    :cond_10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBackgroundWorkerHandler:Landroid/os/Handler;

    #@12
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$1;

    #@14
    invoke-direct {v2, p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;I)V

    #@17
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1a
    .line 265
    return-void
.end method

.method public onRemoveViewAnimationCompleted()V
    .registers 2

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 270
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@6
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;->onRemoveViewAnimationCompleted()V

    #@9
    .line 272
    :cond_9
    return-void
.end method

.method protected onStartReordering()V
    .registers 1

    #@0
    .prologue
    .line 584
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onStartReordering()V

    #@3
    .line 585
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->enablePageContentLayers()V

    #@6
    .line 586
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->reorderStarting()V

    #@9
    .line 587
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 208
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->captureUserInteraction(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected onUnhandledTap(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "ev"

    #@0
    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->showPagingFeedback()V

    #@3
    .line 413
    return-void
.end method

.method protected overScroll(F)V
    .registers 2
    .parameter "amount"

    #@0
    .prologue
    .line 474
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->acceleratedOverScroll(F)V

    #@3
    .line 475
    return-void
.end method

.method public removeWidget(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    .line 753
    instance-of v2, p1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@2
    if-eqz v2, :cond_8

    #@4
    .line 754
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->removeView(Landroid/view/View;)V

    #@7
    .line 767
    :goto_7
    return-void

    #@8
    .line 758
    :cond_8
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageIndex(Landroid/view/View;)I

    #@b
    move-result v1

    #@c
    .line 759
    .local v1, pos:I
    const/4 v2, -0x1

    #@d
    if-eq v1, v2, :cond_1c

    #@f
    .line 760
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@15
    .line 761
    .local v0, frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->removeView(Landroid/view/View;)V

    #@18
    .line 762
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->removeView(Landroid/view/View;)V

    #@1b
    goto :goto_7

    #@1c
    .line 764
    .end local v0           #frame:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_1c
    const-string v2, "KeyguardWidgetPager"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "removeWidget() can\'t find:"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_7
.end method

.method protected reorderStarting()V
    .registers 1

    #@0
    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->showOutlinesAndSidePages()V

    #@3
    .line 580
    return-void
.end method

.method protected screenScrolled(I)V
    .registers 12
    .parameter "screenCenter"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    .line 509
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mScreenCenter:I

    #@4
    .line 510
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->updatePageAlphaValues(I)V

    #@7
    .line 511
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@b
    move-result v6

    #@c
    if-ge v1, v6, :cond_74

    #@e
    .line 512
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@11
    move-result-object v5

    #@12
    .line 513
    .local v5, v:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDragView:Landroid/view/View;

    #@14
    if-ne v5, v6, :cond_19

    #@16
    .line 511
    :cond_16
    :goto_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_8

    #@19
    .line 514
    :cond_19
    if-eqz v5, :cond_16

    #@1b
    .line 515
    invoke-virtual {p0, p1, v5, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getScrollProgress(ILandroid/view/View;I)F

    #@1e
    move-result v4

    #@1f
    .line 517
    .local v4, scrollProgress:F
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mDensity:F

    #@21
    sget v8, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->CAMERA_DISTANCE:F

    #@23
    mul-float/2addr v6, v8

    #@24
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setCameraDistance(F)V

    #@27
    .line 519
    invoke-virtual {p0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isOverScrollChild(IF)Z

    #@2a
    move-result v6

    #@2b
    if-eqz v6, :cond_63

    #@2d
    .line 520
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredWidth()I

    #@30
    move-result v6

    #@31
    div-int/lit8 v6, v6, 0x2

    #@33
    int-to-float v2, v6

    #@34
    .line 521
    .local v2, pivotX:F
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredHeight()I

    #@37
    move-result v6

    #@38
    div-int/lit8 v6, v6, 0x2

    #@3a
    int-to-float v3, v6

    #@3b
    .line 522
    .local v3, pivotY:F
    invoke-virtual {v5, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setPivotX(F)V

    #@3e
    .line 523
    invoke-virtual {v5, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setPivotY(F)V

    #@41
    .line 524
    sget v6, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->OVERSCROLL_MAX_ROTATION:F

    #@43
    neg-float v6, v6

    #@44
    mul-float/2addr v6, v4

    #@45
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setRotationY(F)V

    #@48
    .line 525
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@4b
    move-result v8

    #@4c
    cmpg-float v6, v4, v9

    #@4e
    if-gez v6, :cond_61

    #@50
    const/4 v6, 0x1

    #@51
    :goto_51
    invoke-virtual {v5, v8, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setOverScrollAmount(FZ)V

    #@54
    .line 531
    .end local v2           #pivotX:F
    .end local v3           #pivotY:F
    :goto_54
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getAlpha()F

    #@57
    move-result v0

    #@58
    .line 534
    .local v0, alpha:F
    cmpl-float v6, v0, v9

    #@5a
    if-nez v6, :cond_6a

    #@5c
    .line 535
    const/4 v6, 0x4

    #@5d
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setVisibility(I)V

    #@60
    goto :goto_16

    #@61
    .end local v0           #alpha:F
    .restart local v2       #pivotX:F
    .restart local v3       #pivotY:F
    :cond_61
    move v6, v7

    #@62
    .line 525
    goto :goto_51

    #@63
    .line 527
    .end local v2           #pivotX:F
    .end local v3           #pivotY:F
    :cond_63
    invoke-virtual {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setRotationY(F)V

    #@66
    .line 528
    invoke-virtual {v5, v9, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setOverScrollAmount(FZ)V

    #@69
    goto :goto_54

    #@6a
    .line 536
    .restart local v0       #alpha:F
    :cond_6a
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getVisibility()I

    #@6d
    move-result v6

    #@6e
    if-eqz v6, :cond_16

    #@70
    .line 537
    invoke-virtual {v5, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setVisibility(I)V

    #@73
    goto :goto_16

    #@74
    .line 541
    .end local v0           #alpha:F
    .end local v4           #scrollProgress:F
    .end local v5           #v:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_74
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 178
    const/16 v0, 0x1000

    #@2
    if-ne p1, v0, :cond_a

    #@4
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isPageMoving()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 179
    :cond_a
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->sendAccessibilityEvent(I)V

    #@d
    .line 181
    :cond_d
    return-void
.end method

.method setAddWidgetEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 847
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mAddWidgetView:Landroid/view/View;

    #@2
    if-eqz v1, :cond_1e

    #@4
    if-eqz p1, :cond_1e

    #@6
    .line 848
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mAddWidgetView:Landroid/view/View;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->addView(Landroid/view/View;I)V

    #@c
    .line 851
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLastWidthMeasureSpec:I

    #@e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLastHeightMeasureSpec:I

    #@10
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->measure(II)V

    #@13
    .line 853
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@15
    add-int/lit8 v1, v1, 0x1

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->setCurrentPage(I)V

    #@1a
    .line 854
    const/4 v1, 0x0

    #@1b
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mAddWidgetView:Landroid/view/View;

    #@1d
    .line 862
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 855
    :cond_1e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mAddWidgetView:Landroid/view/View;

    #@20
    if-nez v1, :cond_1d

    #@22
    if-nez p1, :cond_1d

    #@24
    .line 856
    const v1, 0x10202b4

    #@27
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->findViewById(I)Landroid/view/View;

    #@2a
    move-result-object v0

    #@2b
    .line 857
    .local v0, addWidget:Landroid/view/View;
    if-eqz v0, :cond_1d

    #@2d
    .line 858
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mAddWidgetView:Landroid/view/View;

    #@2f
    .line 859
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->removeView(Landroid/view/View;)V

    #@32
    goto :goto_1d
.end method

.method setBouncerAnimationDuration(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 795
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBouncerZoomInOutDuration:I

    #@2
    .line 796
    return-void
.end method

.method public setCallbacks(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;)V
    .registers 2
    .parameter "callbacks"

    #@0
    .prologue
    .line 239
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager$Callbacks;

    #@2
    .line 240
    return-void
.end method

.method setCurrentPage(I)V
    .registers 2
    .parameter "currentPage"

    #@0
    .prologue
    .line 620
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->setCurrentPage(I)V

    #@3
    .line 621
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->showOutlinesAndSidePages()V

    #@6
    .line 622
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->updateWidgetFramesImportantForAccessibility()V

    #@9
    .line 623
    return-void
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 122
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    .line 123
    return-void
.end method

.method protected setPageHoveringOverDeleteDropTarget(IZ)V
    .registers 4
    .parameter "viewIndex"
    .parameter "isHovering"

    #@0
    .prologue
    .line 780
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@3
    move-result-object v0

    #@4
    .line 781
    .local v0, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    invoke-virtual {v0, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setIsHoveringOverDeleteDropTarget(Z)V

    #@7
    .line 782
    return-void
.end method

.method public setViewStateManager(Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;)V
    .registers 2
    .parameter "viewStateManager"

    #@0
    .prologue
    .line 118
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mViewStateManager:Lcom/android/internal/policy/impl/keyguard/KeyguardViewStateManager;

    #@2
    .line 119
    return-void
.end method

.method public setWidgetToResetOnPageFadeOut(I)V
    .registers 2
    .parameter "widget"

    #@0
    .prologue
    .line 670
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mWidgetToResetAfterFadeOut:I

    #@2
    .line 671
    return-void
.end method

.method protected shouldSetTopAlignedPivotForWidget(I)Z
    .registers 3
    .parameter "childIndex"

    #@0
    .prologue
    .line 876
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->isCameraPage(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->shouldSetTopAlignedPivotForWidget(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public showInitialPageHints()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 604
    const/4 v3, 0x1

    #@2
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mShowingInitialHints:Z

    #@4
    .line 605
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getChildCount()I

    #@7
    move-result v1

    #@8
    .line 606
    .local v1, count:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v1, :cond_28

    #@b
    .line 607
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getWidgetPageAt(I)Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@e
    move-result-object v0

    #@f
    .line 608
    .local v0, child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mCurrentPage:I

    #@11
    if-eq v2, v3, :cond_1f

    #@13
    .line 609
    const v3, 0x3f19999a

    #@16
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@19
    .line 610
    invoke-virtual {v0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentAlpha(F)V

    #@1c
    .line 606
    :goto_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_9

    #@1f
    .line 612
    :cond_1f
    invoke-virtual {v0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@22
    .line 613
    const/high16 v3, 0x3f80

    #@24
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setContentAlpha(F)V

    #@27
    goto :goto_1c

    #@28
    .line 616
    .end local v0           #child:Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
    :cond_28
    return-void
.end method

.method showOutlinesAndSidePages()V
    .registers 2

    #@0
    .prologue
    .line 596
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->animateOutlinesAndSidePages(Z)V

    #@4
    .line 597
    return-void
.end method

.method public showPagingFeedback()V
    .registers 1

    #@0
    .prologue
    .line 223
    return-void
.end method

.method zoomInFromBouncer()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/high16 v5, 0x3f80

    #@4
    .line 800
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@6
    if-eqz v1, :cond_15

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@a
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_15

    #@10
    .line 801
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@12
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    #@15
    .line 803
    :cond_15
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getCurrentPage()I

    #@18
    move-result v1

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getPageAt(I)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    .line 804
    .local v0, currentPage:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    #@20
    move-result v1

    #@21
    cmpg-float v1, v1, v5

    #@23
    if-ltz v1, :cond_2d

    #@25
    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    #@28
    move-result v1

    #@29
    cmpg-float v1, v1, v5

    #@2b
    if-gez v1, :cond_6d

    #@2d
    .line 805
    :cond_2d
    new-instance v1, Landroid/animation/AnimatorSet;

    #@2f
    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    #@32
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@34
    .line 806
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@36
    const/4 v2, 0x2

    #@37
    new-array v2, v2, [Landroid/animation/Animator;

    #@39
    const-string v3, "scaleX"

    #@3b
    new-array v4, v7, [F

    #@3d
    aput v5, v4, v6

    #@3f
    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@42
    move-result-object v3

    #@43
    aput-object v3, v2, v6

    #@45
    const-string v3, "scaleY"

    #@47
    new-array v4, v7, [F

    #@49
    aput v5, v4, v6

    #@4b
    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@4e
    move-result-object v3

    #@4f
    aput-object v3, v2, v7

    #@51
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@54
    .line 809
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@56
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBouncerZoomInOutDuration:I

    #@58
    int-to-long v2, v2

    #@59
    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@5c
    .line 810
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@5e
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    #@60
    const/high16 v3, 0x3fc0

    #@62
    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@65
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@68
    .line 811
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@6a
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    #@6d
    .line 813
    :cond_6d
    instance-of v1, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@6f
    if-eqz v1, :cond_76

    #@71
    .line 814
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@73
    .end local v0           #currentPage:Landroid/view/View;
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->onBouncerShowing(Z)V

    #@76
    .line 816
    :cond_76
    return-void
.end method

.method zoomOutToBouncer()V
    .registers 10

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v7, 0x1

    #@5
    .line 820
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@7
    if-eqz v2, :cond_16

    #@9
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@b
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_16

    #@11
    .line 821
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@13
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    #@16
    .line 823
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getCurrentPage()I

    #@19
    move-result v0

    #@1a
    .line 824
    .local v0, curPage:I
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->getPageAt(I)Landroid/view/View;

    #@1d
    move-result-object v1

    #@1e
    .line 825
    .local v1, currentPage:Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->shouldSetTopAlignedPivotForWidget(I)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_34

    #@24
    .line 826
    invoke-virtual {v1, v3}, Landroid/view/View;->setPivotY(F)V

    #@27
    .line 829
    invoke-virtual {v1, v3}, Landroid/view/View;->setPivotX(F)V

    #@2a
    .line 830
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    #@2d
    move-result v2

    #@2e
    div-int/lit8 v2, v2, 0x2

    #@30
    int-to-float v2, v2

    #@31
    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotX(F)V

    #@34
    .line 832
    :cond_34
    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    #@37
    move-result v2

    #@38
    cmpg-float v2, v2, v4

    #@3a
    if-ltz v2, :cond_88

    #@3c
    invoke-virtual {v1}, Landroid/view/View;->getScaleY()F

    #@3f
    move-result v2

    #@40
    cmpg-float v2, v2, v4

    #@42
    if-ltz v2, :cond_88

    #@44
    .line 833
    new-instance v2, Landroid/animation/AnimatorSet;

    #@46
    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    #@49
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@4b
    .line 834
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@4d
    const/4 v3, 0x2

    #@4e
    new-array v3, v3, [Landroid/animation/Animator;

    #@50
    const-string v4, "scaleX"

    #@52
    new-array v5, v7, [F

    #@54
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->BOUNCER_SCALE_FACTOR:F

    #@56
    aput v6, v5, v8

    #@58
    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@5b
    move-result-object v4

    #@5c
    aput-object v4, v3, v8

    #@5e
    const-string v4, "scaleY"

    #@60
    new-array v5, v7, [F

    #@62
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->BOUNCER_SCALE_FACTOR:F

    #@64
    aput v6, v5, v8

    #@66
    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@69
    move-result-object v4

    #@6a
    aput-object v4, v3, v7

    #@6c
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@6f
    .line 837
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@71
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetPager;->mBouncerZoomInOutDuration:I

    #@73
    int-to-long v3, v3

    #@74
    invoke-virtual {v2, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@77
    .line 838
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@79
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    #@7b
    const/high16 v4, 0x3fc0

    #@7d
    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@80
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@83
    .line 839
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/PagedView;->mZoomInOutAnim:Landroid/animation/AnimatorSet;

    #@85
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    #@88
    .line 841
    :cond_88
    instance-of v2, v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@8a
    if-eqz v2, :cond_91

    #@8c
    .line 842
    check-cast v1, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;

    #@8e
    .end local v1           #currentPage:Landroid/view/View;
    invoke-virtual {v1, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->onBouncerShowing(Z)V

    #@91
    .line 844
    :cond_91
    return-void
.end method
