.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;
.super Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;
.source "KeyguardStatusViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 621
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onPhoneStateChanged(I)V
    .registers 3
    .parameter "phoneState"

    #@0
    .prologue
    .line 647
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    iput p1, v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->mPhoneState:I

    #@4
    .line 648
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@6
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;I)V

    #@9
    .line 649
    return-void
.end method

.method public onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 6
    .parameter "status"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 624
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@3
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_f

    #@9
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isBatteryLow()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_46

    #@f
    :cond_f
    const/4 v1, 0x1

    #@10
    :goto_10
    invoke-static {v3, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$402(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Z)Z

    #@13
    .line 625
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@15
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@18
    move-result v3

    #@19
    invoke-static {v1, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$502(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Z)Z

    #@1c
    .line 626
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@1e
    iget v3, p1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@20
    invoke-static {v1, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;I)I

    #@23
    .line 627
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@25
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isCharged()Z

    #@28
    move-result v3

    #@29
    iput-boolean v3, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->mBatteryCharged:Z

    #@2b
    .line 628
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2d
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isBatteryLow()Z

    #@30
    move-result v3

    #@31
    iput-boolean v3, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->mBatteryIsLow:Z

    #@33
    .line 629
    new-instance v0, Llibcore/util/MutableInt;

    #@35
    invoke-direct {v0, v2}, Llibcore/util/MutableInt;-><init>(I)V

    #@38
    .line 630
    .local v0, tmpIcon:Llibcore/util/MutableInt;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@3a
    const/16 v2, 0xf

    #@3c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@3e
    invoke-static {v3, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Llibcore/util/MutableInt;)Ljava/lang/CharSequence;

    #@41
    move-result-object v3

    #@42
    invoke-static {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;ILjava/lang/CharSequence;)V

    #@45
    .line 631
    return-void

    #@46
    .end local v0           #tmpIcon:Llibcore/util/MutableInt;
    :cond_46
    move v1, v2

    #@47
    .line 624
    goto :goto_10
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 640
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$802(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@5
    .line 641
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@7
    invoke-static {v0, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$902(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@a
    .line 642
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@e
    iget-object v1, v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@13
    .line 643
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 3
    .parameter "simState"

    #@0
    .prologue
    .line 653
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@5
    .line 654
    return-void
.end method

.method public onTimeChanged()V
    .registers 2

    #@0
    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->refreshDate()V

    #@5
    .line 636
    return-void
.end method
