.class public Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;
.source "MSimKeyguardSimPukView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimCheckSimPuk;,
        Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const-string v0, "MSimKeyguardSimPukView"

    #@2
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 88
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$MSimStateMachine;-><init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView;->mStateMachine:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPukView$StateMachine;

    #@a
    .line 89
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 34
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method


# virtual methods
.method protected getSecurityMessageDisplay(I)Ljava/lang/CharSequence;
    .registers 7
    .parameter "resId"

    #@0
    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    const v1, 0x1040573

    #@7
    const/4 v2, 0x2

    #@8
    new-array v2, v2, [Ljava/lang/Object;

    #@a
    const/4 v3, 0x0

    #@b
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@d
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPukLockedSubscription()I

    #@14
    move-result v4

    #@15
    add-int/lit8 v4, v4, 0x1

    #@17
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v4

    #@1b
    aput-object v4, v2, v3

    #@1d
    const/4 v3, 0x1

    #@1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getContext()Landroid/content/Context;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@29
    move-result-object v4

    #@2a
    aput-object v4, v2, v3

    #@2c
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    return-object v0
.end method

.method protected updateSim()V
    .registers 5

    #@0
    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->getSimUnlockProgressDialog()Landroid/app/Dialog;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@7
    .line 134
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->TAG:Ljava/lang/String;

    #@9
    const-string v1, "updateSim()"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 136
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->mCheckInProgress:Z

    #@10
    if-nez v0, :cond_2b

    #@12
    .line 137
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->mCheckInProgress:Z

    #@15
    .line 139
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;

    #@17
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->mPukText:Ljava/lang/String;

    #@19
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;->mPinText:Ljava/lang/String;

    #@1b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mContext:Landroid/content/Context;

    #@1d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPukLockedSubscription()I

    #@24
    move-result v3

    #@25
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView;Ljava/lang/String;Ljava/lang/String;I)V

    #@28
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/MSimKeyguardSimPukView$1;->start()V

    #@2b
    .line 160
    :cond_2b
    return-void
.end method
