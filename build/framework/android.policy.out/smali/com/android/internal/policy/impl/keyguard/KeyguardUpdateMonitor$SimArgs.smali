.class Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimArgs"
.end annotation


# instance fields
.field public final simState:Lcom/android/internal/telephony/IccCardConstants$State;

.field public subscription:I


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 3
    .parameter "state"
    .parameter "sub"

    #@0
    .prologue
    .line 528
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 529
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@5
    .line 530
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->subscription:I

    #@7
    .line 531
    return-void
.end method

.method static fromIntent(Landroid/content/Intent;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;
    .registers 9
    .parameter "intent"

    #@0
    .prologue
    .line 536
    const-string v5, "android.intent.action.SIM_STATE_CHANGED"

    #@2
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v5

    #@a
    if-nez v5, :cond_14

    #@c
    .line 537
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v6, "only handles intent ACTION_SIM_STATE_CHANGED"

    #@10
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v5

    #@14
    .line 539
    :cond_14
    const-string v5, "subscription"

    #@16
    const/4 v6, 0x0

    #@17
    invoke-virtual {p0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1a
    move-result v4

    #@1b
    .line 540
    .local v4, subscription:I
    const-string v5, "KeyguardUpdateMonitor"

    #@1d
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v7, "ACTION_SIM_STATE_CHANGED intent received on sub = "

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 542
    const-string v5, "ss"

    #@35
    invoke-virtual {p0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    .line 543
    .local v3, stateExtra:Ljava/lang/String;
    const-string v5, "ABSENT"

    #@3b
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v5

    #@3f
    if-eqz v5, :cond_5a

    #@41
    .line 544
    const-string v5, "reason"

    #@43
    invoke-virtual {p0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .line 547
    .local v0, absentReason:Ljava/lang/String;
    const-string v5, "PERM_DISABLED"

    #@49
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_57

    #@4f
    .line 549
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@51
    .line 579
    .end local v0           #absentReason:Ljava/lang/String;
    .local v2, state:Lcom/android/internal/telephony/IccCardConstants$State;
    :goto_51
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;

    #@53
    invoke-direct {v5, v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;-><init>(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    #@56
    return-object v5

    #@57
    .line 551
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    .restart local v0       #absentReason:Ljava/lang/String;
    :cond_57
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@59
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@5a
    .line 553
    .end local v0           #absentReason:Ljava/lang/String;
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_5a
    const-string v5, "READY"

    #@5c
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v5

    #@60
    if-eqz v5, :cond_65

    #@62
    .line 554
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@64
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@65
    .line 555
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_65
    const-string v5, "LOCKED"

    #@67
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v5

    #@6b
    if-eqz v5, :cond_a2

    #@6d
    .line 556
    const-string v5, "reason"

    #@6f
    invoke-virtual {p0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 558
    .local v1, lockedReason:Ljava/lang/String;
    const-string v5, "PIN"

    #@75
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v5

    #@79
    if-eqz v5, :cond_7e

    #@7b
    .line 559
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7d
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@7e
    .line 560
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_7e
    const-string v5, "PUK"

    #@80
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v5

    #@84
    if-eqz v5, :cond_89

    #@86
    .line 561
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@88
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@89
    .line 562
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_89
    const-string v5, "PERM_DISABLED"

    #@8b
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v5

    #@8f
    if-eqz v5, :cond_94

    #@91
    .line 563
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@93
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@94
    .line 564
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_94
    const-string v5, "PERSO"

    #@96
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v5

    #@9a
    if-eqz v5, :cond_9f

    #@9c
    .line 565
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@9e
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@9f
    .line 567
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_9f
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a1
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@a2
    .line 569
    .end local v1           #lockedReason:Ljava/lang/String;
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_a2
    const-string v5, "CARD_IO_ERROR"

    #@a4
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v5

    #@a8
    if-eqz v5, :cond_ad

    #@aa
    .line 570
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    #@ac
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@ad
    .line 571
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_ad
    const-string v5, "LOADED"

    #@af
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v5

    #@b3
    if-nez v5, :cond_bd

    #@b5
    const-string v5, "IMSI"

    #@b7
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v5

    #@bb
    if-eqz v5, :cond_c0

    #@bd
    .line 575
    :cond_bd
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@bf
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51

    #@c0
    .line 577
    .end local v2           #state:Lcom/android/internal/telephony/IccCardConstants$State;
    :cond_c0
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@c2
    .restart local v2       #state:Lcom/android/internal/telephony/IccCardConstants$State;
    goto :goto_51
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 583
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
