.class public Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEBUG_SIM_STATES:Z = false

.field private static final FAILED_BIOMETRIC_UNLOCK_ATTEMPTS_BEFORE_BACKUP:I = 0x3

.field private static final LOW_BATTERY_THRESHOLD:I = 0x14

.field private static final MSG_BATTERY_UPDATE:I = 0x12e

.field private static final MSG_CARRIER_INFO_UPDATE:I = 0x12f

.field private static final MSG_CLOCK_VISIBILITY_CHANGED:I = 0x133

.field private static final MSG_DEVICE_PROVISIONED:I = 0x134

.field protected static final MSG_DPM_STATE_CHANGED:I = 0x135

.field private static final MSG_PHONE_STATE_CHANGED:I = 0x132

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x131

.field private static final MSG_SIM_STATE_CHANGE:I = 0x130

.field private static final MSG_TIME_UPDATE:I = 0x12d

.field protected static final MSG_USER_REMOVED:I = 0x137

.field protected static final MSG_USER_SWITCHED:I = 0x136

.field private static final TAG:Ljava/lang/String; = "KeyguardUpdateMonitor"


# instance fields
.field private mBatteryStatus:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mClockVisible:Z

.field private mContentObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private mDeviceProvisioned:Z

.field private mFailedAttempts:I

.field private mFailedBiometricUnlockAttempts:I

.field private final mHandler:Landroid/os/Handler;

.field private mPhoneState:I

.field private mRingMode:I

.field private mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

.field private mTelephonyPlmn:Ljava/lang/CharSequence;

.field private mTelephonySpn:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 286
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 82
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@9
    .line 92
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@b
    .line 93
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@d
    .line 97
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@13
    .line 100
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$1;

    #@15
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V

    #@18
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@1a
    .line 141
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;

    #@1c
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V

    #@1f
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@21
    .line 287
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@23
    .line 289
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v1

    #@29
    const-string v4, "device_provisioned"

    #@2b
    invoke-static {v1, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_90

    #@31
    move v1, v2

    #@32
    :goto_32
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@34
    .line 294
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@36
    if-nez v1, :cond_3b

    #@38
    .line 295
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->watchForDeviceProvisioning()V

    #@3b
    .line 299
    :cond_3b
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3d
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3f
    .line 300
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@41
    const/16 v4, 0x64

    #@43
    invoke-direct {v1, v2, v4, v3, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;-><init>(IIII)V

    #@46
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@48
    .line 301
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    #@4b
    move-result-object v1

    #@4c
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonyPlmn:Ljava/lang/CharSequence;

    #@4e
    .line 304
    new-instance v0, Landroid/content/IntentFilter;

    #@50
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@53
    .line 305
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    #@55
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@58
    .line 306
    const-string v1, "android.intent.action.TIME_SET"

    #@5a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5d
    .line 307
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    #@5f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@62
    .line 308
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    #@64
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@67
    .line 309
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@69
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6c
    .line 310
    const-string v1, "android.intent.action.PHONE_STATE"

    #@6e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@71
    .line 311
    const-string v1, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@73
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@76
    .line 312
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    #@78
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@7b
    .line 313
    const-string v1, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    #@7d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@80
    .line 314
    const-string v1, "android.intent.action.USER_SWITCHED"

    #@82
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@85
    .line 315
    const-string v1, "android.intent.action.USER_REMOVED"

    #@87
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8a
    .line 316
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@8c
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@8f
    .line 317
    return-void

    #@90
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_90
    move v1, v3

    #@91
    .line 289
    goto :goto_32
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleTimeUpdate()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleBatteryUpdate(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleCarrierInfoUpdate()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleClockVisibilityChanged()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonyPlmn:Ljava/lang/CharSequence;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$802(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonySpn:Ljava/lang/CharSequence;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getDefaultPlmn()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 523
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x104030b

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method private getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 512
    const-string v1, "showPlmn"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_17

    #@9
    .line 513
    const-string v1, "plmn"

    #@b
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 514
    .local v0, plmn:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@11
    .line 516
    .end local v0           #plmn:Ljava/lang/String;
    :goto_11
    return-object v0

    #@12
    .line 514
    .restart local v0       #plmn:Ljava/lang/String;
    :cond_12
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    #@15
    move-result-object v0

    #@16
    goto :goto_11

    #@17
    .line 516
    .end local v0           #plmn:Ljava/lang/String;
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_11
.end method

.method private getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 531
    const-string v1, "showSpn"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_12

    #@9
    .line 532
    const-string v1, "spn"

    #@b
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 533
    .local v0, spn:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@11
    .line 537
    .end local v0           #spn:Ljava/lang/String;
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private handleBatteryUpdate(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 433
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@2
    invoke-static {v2, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isBatteryUpdateInteresting(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)Z

    #@5
    move-result v0

    #@6
    .line 434
    .local v0, batteryUpdateInteresting:Z
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@8
    .line 435
    if-eqz v0, :cond_21

    #@a
    .line 436
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v2

    #@11
    if-ge v1, v2, :cond_21

    #@13
    .line 437
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@1b
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V

    #@1e
    .line 436
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_b

    #@21
    .line 440
    .end local v1           #i:I
    :cond_21
    return-void
.end method

.method private handleCarrierInfoUpdate()V
    .registers 5

    #@0
    .prologue
    .line 449
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1b

    #@9
    .line 450
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonyPlmn:Ljava/lang/CharSequence;

    #@13
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonySpn:Ljava/lang/CharSequence;

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@18
    .line 449
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 452
    :cond_1b
    return-void
.end method

.method private handleClockVisibilityChanged()V
    .registers 3

    #@0
    .prologue
    .line 478
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 479
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onClockVisibilityChanged()V

    #@14
    .line 478
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 481
    :cond_17
    return-void
.end method

.method private handleSimStateChange(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;)V
    .registers 5
    .parameter "simArgs"

    #@0
    .prologue
    .line 458
    iget-object v1, p1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    .line 465
    .local v1, state:Lcom/android/internal/telephony/IccCardConstants$State;
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4
    if-eq v1, v2, :cond_23

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    if-eq v1, v2, :cond_23

    #@a
    .line 466
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@c
    .line 467
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v2

    #@13
    if-ge v0, v2, :cond_23

    #@15
    .line 468
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@1d
    invoke-virtual {v2, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@20
    .line 467
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_d

    #@23
    .line 471
    .end local v0           #i:I
    :cond_23
    return-void
.end method

.method private handleTimeUpdate()V
    .registers 3

    #@0
    .prologue
    .line 423
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 424
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onTimeChanged()V

    #@14
    .line 423
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 426
    :cond_17
    return-void
.end method

.method private static isBatteryUpdateInteresting(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)Z
    .registers 9
    .parameter "old"
    .parameter "current"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 484
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@5
    move-result v0

    #@6
    .line 485
    .local v0, nowPluggedIn:Z
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isPluggedIn()Z

    #@9
    move-result v2

    #@a
    .line 486
    .local v2, wasPluggedIn:Z
    if-ne v2, v3, :cond_1a

    #@c
    if-ne v0, v3, :cond_1a

    #@e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@10
    iget v6, p1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->status:I

    #@12
    if-eq v5, v6, :cond_1a

    #@14
    move v1, v3

    #@15
    .line 491
    .local v1, stateChangedWhilePluggedIn:Z
    :goto_15
    if-ne v2, v0, :cond_19

    #@17
    if-eqz v1, :cond_1c

    #@19
    .line 504
    :cond_19
    :goto_19
    return v3

    #@1a
    .end local v1           #stateChangedWhilePluggedIn:Z
    :cond_1a
    move v1, v4

    #@1b
    .line 486
    goto :goto_15

    #@1c
    .line 496
    .restart local v1       #stateChangedWhilePluggedIn:Z
    :cond_1c
    if-eqz v0, :cond_24

    #@1e
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@20
    iget v6, p1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@22
    if-ne v5, v6, :cond_19

    #@24
    .line 501
    :cond_24
    if-nez v0, :cond_32

    #@26
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->isBatteryLow()Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_32

    #@2c
    iget v5, p1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@2e
    iget v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;->level:I

    #@30
    if-ne v5, v6, :cond_19

    #@32
    :cond_32
    move v3, v4

    #@33
    .line 504
    goto :goto_19
.end method

.method private watchForDeviceProvisioning()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 320
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$3;

    #@3
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@5
    invoke-direct {v1, p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Landroid/os/Handler;)V

    #@8
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    #@a
    .line 333
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string v2, "device_provisioned"

    #@12
    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    #@18
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1b
    .line 339
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v1

    #@21
    const-string v2, "device_provisioned"

    #@23
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_2a

    #@29
    const/4 v0, 0x1

    #@2a
    .line 341
    .local v0, provisioned:Z
    :cond_2a
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2c
    if-eq v0, v1, :cond_41

    #@2e
    .line 342
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@30
    .line 343
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@32
    if-eqz v1, :cond_41

    #@34
    .line 344
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@36
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@38
    const/16 v3, 0x134

    #@3a
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@41
    .line 347
    :cond_41
    return-void
.end method


# virtual methods
.method public clearFailedAttempts()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 613
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@3
    .line 614
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@5
    .line 615
    return-void
.end method

.method public getFailedAttempts()I
    .registers 2

    #@0
    .prologue
    .line 609
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@2
    return v0
.end method

.method public getMaxBiometricUnlockAttemptsReached()Z
    .registers 3

    #@0
    .prologue
    .line 634
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@2
    const/4 v1, 0x3

    #@3
    if-lt v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getPhoneState()I
    .registers 2

    #@0
    .prologue
    .line 626
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@2
    return v0
.end method

.method public getSimState()Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 2

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    return-object v0
.end method

.method public getTelephonyPlmn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 593
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonyPlmn:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTelephonySpn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 597
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonySpn:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method protected handleDevicePolicyManagerStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 353
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 354
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onDevicePolicyManagerStateChanged()V

    #@14
    .line 353
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 356
    :cond_17
    return-void
.end method

.method protected handleDeviceProvisioned()V
    .registers 4

    #@0
    .prologue
    .line 380
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 381
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onDeviceProvisioned()V

    #@14
    .line 380
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 383
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    #@19
    if-eqz v1, :cond_29

    #@1b
    .line 385
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    #@23
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@26
    .line 386
    const/4 v1, 0x0

    #@27
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    #@29
    .line 388
    :cond_29
    return-void
.end method

.method protected handlePhoneStateChanged(Ljava/lang/String;)V
    .registers 5
    .parameter "newState"

    #@0
    .prologue
    .line 395
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@2
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_24

    #@8
    .line 396
    const/4 v1, 0x0

    #@9
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@b
    .line 402
    :cond_b
    :goto_b
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v1

    #@12
    if-ge v0, v1, :cond_3c

    #@14
    .line 403
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@1c
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@1e
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onPhoneStateChanged(I)V

    #@21
    .line 402
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_c

    #@24
    .line 397
    .end local v0           #i:I
    :cond_24
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@26
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_30

    #@2c
    .line 398
    const/4 v1, 0x2

    #@2d
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@2f
    goto :goto_b

    #@30
    .line 399
    :cond_30
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@32
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_b

    #@38
    .line 400
    const/4 v1, 0x1

    #@39
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@3b
    goto :goto_b

    #@3c
    .line 405
    .restart local v0       #i:I
    :cond_3c
    return-void
.end method

.method protected handleRingerModeChange(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 412
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mRingMode:I

    #@2
    .line 413
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    if-ge v0, v1, :cond_19

    #@b
    .line 414
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@13
    invoke-virtual {v1, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRingerModeChanged(I)V

    #@16
    .line 413
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_3

    #@19
    .line 416
    :cond_19
    return-void
.end method

.method protected handleUserRemoved(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 371
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 372
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onUserRemoved(I)V

    #@14
    .line 371
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 374
    :cond_17
    return-void
.end method

.method protected handleUserSwitched(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 362
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_17

    #@9
    .line 363
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@11
    invoke-virtual {v1, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onUserSwitched(I)V

    #@14
    .line 362
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_1

    #@17
    .line 365
    :cond_17
    return-void
.end method

.method public isClockVisible()Z
    .registers 2

    #@0
    .prologue
    .line 622
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mClockVisible:Z

    #@2
    return v0
.end method

.method public isDeviceProvisioned()Z
    .registers 2

    #@0
    .prologue
    .line 605
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mDeviceProvisioned:Z

    #@2
    return v0
.end method

.method public isSimLocked()Z
    .registers 3

    #@0
    .prologue
    .line 638
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4
    if-eq v0, v1, :cond_12

    #@6
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a
    if-eq v0, v1, :cond_12

    #@c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@e
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    if-ne v0, v1, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public registerCallback(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 555
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_2e

    #@8
    .line 556
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 558
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;

    #@f
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRefreshBatteryInfo(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$BatteryStatus;)V

    #@12
    .line 559
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onTimeChanged()V

    #@15
    .line 560
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mRingMode:I

    #@17
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRingerModeChanged(I)V

    #@1a
    .line 561
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mPhoneState:I

    #@1c
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onPhoneStateChanged(I)V

    #@1f
    .line 562
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonyPlmn:Ljava/lang/CharSequence;

    #@21
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mTelephonySpn:Ljava/lang/CharSequence;

    #@23
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@26
    .line 563
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onClockVisibilityChanged()V

    #@29
    .line 564
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2b
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@2e
    .line 569
    :cond_2e
    return-void
.end method

.method public removeCallback(Ljava/lang/Object;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 546
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mCallbacks:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 547
    return-void
.end method

.method public reportClockVisible(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 572
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mClockVisible:Z

    #@2
    .line 573
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    #@4
    const/16 v1, 0x133

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 574
    return-void
.end method

.method public reportFailedAttempt()V
    .registers 2

    #@0
    .prologue
    .line 618
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedAttempts:I

    #@6
    .line 619
    return-void
.end method

.method public reportFailedBiometricUnlockAttempt()V
    .registers 2

    #@0
    .prologue
    .line 630
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    #@6
    .line 631
    return-void
.end method

.method public reportSimUnlocked()V
    .registers 3

    #@0
    .prologue
    .line 589
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;

    #@2
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;-><init>(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor$SimArgs;)V

    #@a
    .line 590
    return-void
.end method
