.class final enum Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;
.super Ljava/lang/Enum;
.source "KeyguardSimPinView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PinLockState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

.field public static final enum PIN_CHECKPIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

.field public static final enum PIN_CREATE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

.field public static final enum PIN_RESUME:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 101
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@5
    const-string v1, "PIN_CREATE"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_CREATE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@c
    .line 102
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@e
    const-string v1, "PIN_RESUME"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_RESUME:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@15
    .line 103
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@17
    const-string v1, "PIN_CHECKPIN"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_CHECKPIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@1e
    .line 100
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@21
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_CREATE:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_RESUME:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->PIN_CHECKPIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 100
    const-class v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;
    .registers 1

    #@0
    .prologue
    .line 100
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$PinLockState;

    #@8
    return-object v0
.end method
