.class Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;
.super Ljava/lang/Object;
.source "KeyguardSimPinView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 217
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 220
    const-string v0, "KeyguardSimPinView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LGE] mSimUnlockProgressDialog status="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@f
    iget-object v2, v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 221
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@1e
    iget-object v0, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@20
    if-nez v0, :cond_2c

    #@22
    .line 222
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@24
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->doHapticKeyClick()V

    #@27
    .line 223
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView$2;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;

    #@29
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSimPinView;->verifyPasswordAndUnlock()V

    #@2c
    .line 225
    :cond_2c
    return-void
.end method
