.class public Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;
.super Landroid/widget/LinearLayout;
.source "KeyguardLinearLayout.java"


# instance fields
.field mTopChild:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 30
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 27
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;->mTopChild:I

    #@6
    .line 39
    return-void
.end method


# virtual methods
.method public setTopChild(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v0

    #@4
    .line 43
    .local v0, top:I
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;->mTopChild:I

    #@6
    .line 44
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardLinearLayout;->invalidate()V

    #@9
    .line 45
    return-void
.end method
