.class Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;
.super Ljava/lang/Object;
.source "KeyguardPatternView.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountAnalyzer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccountIndex:I

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mAccounts:[Landroid/accounts/Account;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;


# direct methods
.method private constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;Landroid/accounts/AccountManager;)V
    .registers 6
    .parameter
    .parameter "accountManager"

    #@0
    .prologue
    .line 302
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 303
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountManager:Landroid/accounts/AccountManager;

    #@7
    .line 304
    const-string v0, "com.google"

    #@9
    new-instance v1, Landroid/os/UserHandle;

    #@b
    invoke-static {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;)Lcom/android/internal/widget/LockPatternUtils;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@12
    move-result v2

    #@13
    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    #@16
    invoke-virtual {p2, v0, v1}, Landroid/accounts/AccountManager;->getAccountsByTypeAsUser(Ljava/lang/String;Landroid/os/UserHandle;)[Landroid/accounts/Account;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@1c
    .line 306
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;Landroid/accounts/AccountManager;Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 297
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;Landroid/accounts/AccountManager;)V

    #@3
    return-void
.end method

.method private next()V
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 311
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;

    #@3
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;->access$1000(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_10

    #@9
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@d
    array-length v1, v1

    #@e
    if-lt v0, v1, :cond_11

    #@10
    .line 318
    :cond_10
    :goto_10
    return-void

    #@11
    .line 316
    :cond_11
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountManager:Landroid/accounts/AccountManager;

    #@13
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccounts:[Landroid/accounts/Account;

    #@15
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@17
    aget-object v1, v1, v3

    #@19
    new-instance v6, Landroid/os/UserHandle;

    #@1b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;

    #@1d
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;->access$400(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;)Lcom/android/internal/widget/LockPatternUtils;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@24
    move-result v3

    #@25
    invoke-direct {v6, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@28
    move-object v3, v2

    #@29
    move-object v4, p0

    #@2a
    move-object v5, v2

    #@2b
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->confirmCredentialsAsUser(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;Landroid/os/UserHandle;)Landroid/accounts/AccountManagerFuture;

    #@2e
    goto :goto_10
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 328
    .local p1, future:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/os/Bundle;

    #@6
    .line 329
    .local v0, result:Landroid/os/Bundle;
    const-string v1, "intent"

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v1

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 330
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;->access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;Z)Z
    :try_end_14
    .catchall {:try_start_0 .. :try_end_14} :catchall_3f
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_14} :catch_1e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_14} :catch_29
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_14} :catch_34

    #@14
    .line 339
    :cond_14
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@16
    add-int/lit8 v1, v1, 0x1

    #@18
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@1a
    .line 340
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@1d
    .line 342
    .end local v0           #result:Landroid/os/Bundle;
    :goto_1d
    return-void

    #@1e
    .line 332
    :catch_1e
    move-exception v1

    #@1f
    .line 339
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@21
    add-int/lit8 v1, v1, 0x1

    #@23
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@25
    .line 340
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@28
    goto :goto_1d

    #@29
    .line 334
    :catch_29
    move-exception v1

    #@2a
    .line 339
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@30
    .line 340
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@33
    goto :goto_1d

    #@34
    .line 336
    :catch_34
    move-exception v1

    #@35
    .line 339
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@37
    add-int/lit8 v1, v1, 0x1

    #@39
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@3b
    .line 340
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@3e
    goto :goto_1d

    #@3f
    .line 339
    :catchall_3f
    move-exception v1

    #@40
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@42
    add-int/lit8 v2, v2, 0x1

    #@44
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@46
    .line 340
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@49
    throw v1
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 321
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;->access$1002(Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;Z)Z

    #@6
    .line 322
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->mAccountIndex:I

    #@8
    .line 323
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$AccountAnalyzer;->next()V

    #@b
    .line 324
    return-void
.end method
