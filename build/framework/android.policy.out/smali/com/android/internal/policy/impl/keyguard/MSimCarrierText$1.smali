.class Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.source "MSimCarrierText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPlmn:[Ljava/lang/CharSequence;

.field private mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

.field private mSpn:[Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public initialize()V
    .registers 3

    #@0
    .prologue
    .line 46
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@7
    move-result v0

    #@8
    .line 47
    .local v0, numPhones:I
    new-array v1, v0, [Ljava/lang/CharSequence;

    #@a
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mPlmn:[Ljava/lang/CharSequence;

    #@c
    .line 48
    new-array v1, v0, [Ljava/lang/CharSequence;

    #@e
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSpn:[Ljava/lang/CharSequence;

    #@10
    .line 49
    new-array v1, v0, [Lcom/android/internal/telephony/IccCardConstants$State;

    #@12
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@14
    .line 50
    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .registers 8
    .parameter "plmn"
    .parameter "spn"
    .parameter "sub"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mPlmn:[Ljava/lang/CharSequence;

    #@2
    aput-object p1, v0, p3

    #@4
    .line 55
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSpn:[Ljava/lang/CharSequence;

    #@6
    aput-object p2, v0, p3

    #@8
    .line 56
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;

    #@a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mPlmn:[Ljava/lang/CharSequence;

    #@e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSpn:[Ljava/lang/CharSequence;

    #@10
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->updateCarrierText([Lcom/android/internal/telephony/IccCardConstants$State;[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    #@13
    .line 57
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V
    .registers 7
    .parameter "simState"
    .parameter "sub"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    aput-object p1, v0, p2

    #@4
    .line 62
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mPlmn:[Ljava/lang/CharSequence;

    #@a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;->mSpn:[Ljava/lang/CharSequence;

    #@c
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->updateCarrierText([Lcom/android/internal/telephony/IccCardConstants$State;[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    #@f
    .line 63
    return-void
.end method
