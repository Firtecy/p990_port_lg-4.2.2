.class Lcom/android/internal/policy/impl/PhoneWindowManager$40;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->updateSystemUiVisibilityLw()I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

.field final synthetic val$needsMenu:Z

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;IZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 7928
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    iput p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->val$visibility:I

    #@4
    iput-boolean p3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->val$needsMenu:Z

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 7931
    :try_start_0
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@5
    move-result-object v1

    #@6
    .line 7932
    .local v1, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_13

    #@8
    .line 7933
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->val$visibility:I

    #@a
    const/4 v3, -0x1

    #@b
    invoke-interface {v1, v2, v3}, Lcom/android/internal/statusbar/IStatusBarService;->setSystemUiVisibility(II)V

    #@e
    .line 7934
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->val$needsMenu:Z

    #@10
    invoke-interface {v1, v2}, Lcom/android/internal/statusbar/IStatusBarService;->topAppWindowChanged(Z)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_13} :catch_14

    #@13
    .line 7940
    .end local v1           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 7936
    :catch_14
    move-exception v0

    #@15
    .line 7938
    .local v0, e:Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$40;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@17
    const/4 v3, 0x0

    #@18
    iput-object v3, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@1a
    goto :goto_13
.end method
