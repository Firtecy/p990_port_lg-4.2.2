.class public Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;
.super Ljava/lang/Object;
.source "FaceUnlock.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "FULLockscreen"


# instance fields
.field private final BACKUP_LOCK_TIMEOUT:I

.field private final MSG_CANCEL:I

.field private final MSG_HIDE_FACE_UNLOCK_VIEW:I

.field private final MSG_POKE_WAKELOCK:I

.field private final MSG_REPORT_FAILED_ATTEMPT:I

.field private final MSG_SERVICE_CONNECTED:I

.field private final MSG_SERVICE_DISCONNECTED:I

.field private final MSG_SHOW_FACE_UNLOCK_VIEW:I

.field private final MSG_UNLOCK:I

.field private final SERVICE_STARTUP_VIEW_TIMEOUT:I

.field private mBoundToService:Z

.field private mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private final mFaceUnlockCallback:Lcom/android/internal/policy/IFaceLockCallback;

.field private mFaceUnlockView:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private volatile mIsRunning:Z

.field mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mService:Lcom/android/internal/policy/IFaceLockInterface;

.field private mServiceRunning:Z

.field private final mServiceRunningLock:Ljava/lang/Object;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V
    .registers 7
    .parameter "context"
    .parameter "updateMonitor"
    .parameter "lockPatternUtils"
    .parameter "keyguardScreenCallback"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 48
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@6
    .line 51
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunningLock:Ljava/lang/Object;

    #@d
    .line 53
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@f
    .line 57
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_SHOW_FACE_UNLOCK_VIEW:I

    #@11
    .line 58
    const/4 v0, 0x1

    #@12
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_HIDE_FACE_UNLOCK_VIEW:I

    #@14
    .line 59
    const/4 v0, 0x2

    #@15
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_SERVICE_CONNECTED:I

    #@17
    .line 60
    const/4 v0, 0x3

    #@18
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_SERVICE_DISCONNECTED:I

    #@1a
    .line 61
    const/4 v0, 0x4

    #@1b
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_UNLOCK:I

    #@1d
    .line 62
    const/4 v0, 0x5

    #@1e
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_CANCEL:I

    #@20
    .line 63
    const/4 v0, 0x6

    #@21
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_REPORT_FAILED_ATTEMPT:I

    #@23
    .line 65
    const/16 v0, 0x8

    #@25
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->MSG_POKE_WAKELOCK:I

    #@27
    .line 70
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@29
    .line 75
    const/16 v0, 0xbb8

    #@2b
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->SERVICE_STARTUP_VIEW_TIMEOUT:I

    #@2d
    .line 79
    const/16 v0, 0x1388

    #@2f
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->BACKUP_LOCK_TIMEOUT:I

    #@31
    .line 432
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;

    #@33
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)V

    #@36
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mConnection:Landroid/content/ServiceConnection;

    #@38
    .line 504
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;

    #@3a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)V

    #@3d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockCallback:Lcom/android/internal/policy/IFaceLockCallback;

    #@3f
    .line 89
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mContext:Landroid/content/Context;

    #@41
    .line 90
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@43
    .line 91
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@45
    .line 92
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@47
    .line 93
    new-instance v0, Landroid/os/Handler;

    #@49
    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    #@4c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@4e
    .line 94
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;Lcom/android/internal/policy/IFaceLockInterface;)Lcom/android/internal/policy/IFaceLockInterface;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 38
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private removeDisplayMessages()V
    .registers 3

    #@0
    .prologue
    .line 425
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 426
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 427
    return-void
.end method

.method private startUi(Landroid/os/IBinder;IIII)V
    .registers 15
    .parameter "windowToken"
    .parameter "x"
    .parameter "y"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 458
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunningLock:Ljava/lang/Object;

    #@2
    monitor-enter v8

    #@3
    .line 459
    :try_start_3
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@5
    if-nez v0, :cond_45

    #@7
    .line 460
    const-string v0, "FULLockscreen"

    #@9
    const-string v1, "Starting Face Unlock"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_42

    #@e
    .line 462
    :try_start_e
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakLivelinessEnabled()Z

    #@15
    move-result v6

    #@16
    move-object v1, p1

    #@17
    move v2, p2

    #@18
    move v3, p3

    #@19
    move v4, p4

    #@1a
    move v5, p5

    #@1b
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/policy/IFaceLockInterface;->startUi(Landroid/os/IBinder;IIIIZ)V
    :try_end_1e
    .catchall {:try_start_e .. :try_end_1e} :catchall_42
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_1e} :catch_23

    #@1e
    .line 468
    const/4 v0, 0x1

    #@1f
    :try_start_1f
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@21
    .line 472
    :goto_21
    monitor-exit v8

    #@22
    .line 473
    :goto_22
    return-void

    #@23
    .line 464
    :catch_23
    move-exception v7

    #@24
    .line 465
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "FULLockscreen"

    #@26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "Caught exception starting Face Unlock: "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v7}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 466
    monitor-exit v8

    #@41
    goto :goto_22

    #@42
    .line 472
    .end local v7           #e:Landroid/os/RemoteException;
    :catchall_42
    move-exception v0

    #@43
    monitor-exit v8
    :try_end_44
    .catchall {:try_start_1f .. :try_end_44} :catchall_42

    #@44
    throw v0

    #@45
    .line 470
    :cond_45
    :try_start_45
    const-string v0, "FULLockscreen"

    #@47
    const-string v1, "startUi() attempted while running"

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4c
    .catchall {:try_start_45 .. :try_end_4c} :catchall_42

    #@4c
    goto :goto_21
.end method

.method private stopUi()V
    .registers 6

    #@0
    .prologue
    .line 483
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunningLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 484
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@5
    if-eqz v1, :cond_16

    #@7
    .line 485
    const-string v1, "FULLockscreen"

    #@9
    const-string v3, "Stopping Face Unlock"

    #@b
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_36

    #@e
    .line 487
    :try_start_e
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@10
    invoke-interface {v1}, Lcom/android/internal/policy/IFaceLockInterface;->stopUi()V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_36
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_18

    #@13
    .line 491
    :goto_13
    const/4 v1, 0x0

    #@14
    :try_start_14
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@16
    .line 498
    :cond_16
    monitor-exit v2

    #@17
    .line 499
    return-void

    #@18
    .line 488
    :catch_18
    move-exception v0

    #@19
    .line 489
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "FULLockscreen"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "Caught exception stopping Face Unlock: "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_13

    #@36
    .line 498
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_36
    move-exception v1

    #@37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_14 .. :try_end_38} :catchall_36

    #@38
    throw v1
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 216
    :try_start_4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockCallback:Lcom/android/internal/policy/IFaceLockCallback;

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/policy/IFaceLockInterface;->unregisterCallback(Lcom/android/internal/policy/IFaceLockCallback;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_b} :catch_12

    #@b
    .line 220
    :goto_b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->stopUi()V

    #@e
    .line 221
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@11
    .line 223
    :cond_11
    return-void

    #@12
    .line 217
    :catch_12
    move-exception v0

    #@13
    goto :goto_b
.end method

.method public getQuality()I
    .registers 2

    #@0
    .prologue
    .line 229
    const v0, 0x8000

    #@3
    return v0
.end method

.method handleCancel()V
    .registers 3

    #@0
    .prologue
    .line 382
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 383
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@a
    .line 387
    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->stop()Z

    #@d
    .line 388
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@f
    const/16 v1, 0x1388

    #@11
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@14
    .line 389
    return-void

    #@15
    .line 385
    :cond_15
    const-string v0, "FULLockscreen"

    #@17
    const-string v1, "mFaceUnlockView is null in handleCancel()"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_a
.end method

.method handleHideFaceUnlockView()V
    .registers 3

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 292
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@a
    .line 296
    :goto_a
    return-void

    #@b
    .line 294
    :cond_b
    const-string v0, "FULLockscreen"

    #@d
    const-string v1, "mFaceUnlockView is null in handleHideFaceUnlockView()"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_a
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 239
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_32

    #@5
    .line 268
    :pswitch_5
    const-string v0, "FULLockscreen"

    #@7
    const-string v1, "Unhandled message"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 269
    const/4 v0, 0x0

    #@d
    .line 271
    :goto_d
    return v0

    #@e
    .line 241
    :pswitch_e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleShowFaceUnlockView()V

    #@11
    .line 271
    :goto_11
    const/4 v0, 0x1

    #@12
    goto :goto_d

    #@13
    .line 244
    :pswitch_13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleHideFaceUnlockView()V

    #@16
    goto :goto_11

    #@17
    .line 247
    :pswitch_17
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleServiceConnected()V

    #@1a
    goto :goto_11

    #@1b
    .line 250
    :pswitch_1b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleServiceDisconnected()V

    #@1e
    goto :goto_11

    #@1f
    .line 253
    :pswitch_1f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleUnlock()V

    #@22
    goto :goto_11

    #@23
    .line 256
    :pswitch_23
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleCancel()V

    #@26
    goto :goto_11

    #@27
    .line 259
    :pswitch_27
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handleReportFailedAttempt()V

    #@2a
    goto :goto_11

    #@2b
    .line 265
    :pswitch_2b
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@2d
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->handlePokeWakelock(I)V

    #@30
    goto :goto_11

    #@31
    .line 239
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_e
        :pswitch_13
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
        :pswitch_27
        :pswitch_5
        :pswitch_2b
    .end packed-switch
.end method

.method handlePokeWakelock(I)V
    .registers 3
    .parameter "millis"

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@5
    .line 418
    return-void
.end method

.method handleReportFailedAttempt()V
    .registers 2

    #@0
    .prologue
    .line 396
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->reportFailedBiometricUnlockAttempt()V

    #@5
    .line 397
    return-void
.end method

.method handleServiceConnected()V
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 303
    const-string v0, "FULLockscreen"

    #@3
    const-string v2, "handleServiceConnected()"

    #@5
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 310
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@a
    if-nez v0, :cond_14

    #@c
    .line 311
    const-string v0, "FULLockscreen"

    #@e
    const-string v2, "Dropping startUi() in handleServiceConnected() because no longer bound"

    #@10
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 342
    :cond_13
    :goto_13
    return-void

    #@14
    .line 316
    :cond_14
    :try_start_14
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@16
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockCallback:Lcom/android/internal/policy/IFaceLockCallback;

    #@18
    invoke-interface {v0, v2}, Lcom/android/internal/policy/IFaceLockInterface;->registerCallback(Lcom/android/internal/policy/IFaceLockCallback;)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_1b} :catch_4a

    #@1b
    .line 325
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@1d
    if-eqz v0, :cond_13

    #@1f
    .line 326
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@21
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@24
    move-result-object v1

    #@25
    .line 327
    .local v1, windowToken:Landroid/os/IBinder;
    if-eqz v1, :cond_6f

    #@27
    .line 331
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@29
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@2c
    .line 334
    const/4 v0, 0x2

    #@2d
    new-array v7, v0, [I

    #@2f
    .line 335
    .local v7, position:[I
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@31
    invoke-virtual {v0, v7}, Landroid/view/View;->getLocationInWindow([I)V

    #@34
    .line 336
    aget v2, v7, v4

    #@36
    const/4 v0, 0x1

    #@37
    aget v3, v7, v0

    #@39
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@3b
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@3e
    move-result v4

    #@3f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@41
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@44
    move-result v5

    #@45
    move-object v0, p0

    #@46
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->startUi(Landroid/os/IBinder;IIII)V

    #@49
    goto :goto_13

    #@4a
    .line 317
    .end local v1           #windowToken:Landroid/os/IBinder;
    .end local v7           #position:[I
    :catch_4a
    move-exception v6

    #@4b
    .line 318
    .local v6, e:Landroid/os/RemoteException;
    const-string v0, "FULLockscreen"

    #@4d
    new-instance v2, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v3, "Caught exception connecting to Face Unlock: "

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v6}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 319
    const/4 v0, 0x0

    #@68
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@6a
    .line 320
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@6c
    .line 321
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@6e
    goto :goto_13

    #@6f
    .line 339
    .end local v6           #e:Landroid/os/RemoteException;
    .restart local v1       #windowToken:Landroid/os/IBinder;
    :cond_6f
    const-string v0, "FULLockscreen"

    #@71
    const-string v2, "windowToken is null in handleServiceConnected()"

    #@73
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_13
.end method

.method handleServiceDisconnected()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 349
    const-string v0, "FULLockscreen"

    #@3
    const-string v1, "handleServiceDisconnected()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 352
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunningLock:Ljava/lang/Object;

    #@a
    monitor-enter v1

    #@b
    .line 353
    const/4 v0, 0x0

    #@c
    :try_start_c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@e
    .line 354
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mServiceRunning:Z

    #@11
    .line 355
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_17

    #@12
    .line 356
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@14
    .line 357
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@16
    .line 358
    return-void

    #@17
    .line 355
    :catchall_17
    move-exception v0

    #@18
    :try_start_18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method handleShowFaceUnlockView()V
    .registers 3

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 280
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@a
    .line 284
    :goto_a
    return-void

    #@b
    .line 282
    :cond_b
    const-string v0, "FULLockscreen"

    #@d
    const-string v1, "mFaceUnlockView is null in handleShowFaceUnlockView()"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_a
.end method

.method handleUnlock()V
    .registers 3

    #@0
    .prologue
    .line 366
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->removeDisplayMessages()V

    #@3
    .line 367
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@5
    if-eqz v0, :cond_1c

    #@7
    .line 368
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@d
    .line 372
    :goto_d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->stop()Z

    #@10
    .line 373
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->keyguardDone(Z)V

    #@16
    .line 374
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@18
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    #@1b
    .line 375
    return-void

    #@1c
    .line 370
    :cond_1c
    const-string v0, "FULLockscreen"

    #@1e
    const-string v1, "mFaceUnlockView is null in handleUnlock()"

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_d
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 138
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->removeDisplayMessages()V

    #@3
    .line 139
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x1

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@9
    .line 140
    return-void
.end method

.method public initializeView(Landroid/view/View;)V
    .registers 4
    .parameter "biometricUnlockView"

    #@0
    .prologue
    .line 102
    const-string v0, "FULLockscreen"

    #@2
    const-string v1, "initializeView()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 103
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@9
    .line 104
    return-void
.end method

.method public isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@2
    return v0
.end method

.method public show(J)V
    .registers 5
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@9
    move-result-object v1

    #@a
    if-eq v0, v1, :cond_13

    #@c
    .line 120
    const-string v0, "FULLockscreen"

    #@e
    const-string v1, "show() called off of the UI thread"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 123
    :cond_13
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->removeDisplayMessages()V

    #@16
    .line 124
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 125
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockView:Landroid/view/View;

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@20
    .line 127
    :cond_20
    const-wide/16 v0, 0x0

    #@22
    cmp-long v0, p1, v0

    #@24
    if-lez v0, :cond_2c

    #@26
    .line 128
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@28
    const/4 v1, 0x1

    #@29
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2c
    .line 130
    :cond_2c
    return-void
.end method

.method public start()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 149
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@6
    move-result-object v0

    #@7
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@a
    move-result-object v1

    #@b
    if-eq v0, v1, :cond_14

    #@d
    .line 150
    const-string v0, "FULLockscreen"

    #@f
    const-string v1, "start() called off of the UI thread"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 153
    :cond_14
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@16
    if-eqz v0, :cond_1f

    #@18
    .line 154
    const-string v0, "FULLockscreen"

    #@1a
    const-string v1, "start() called when already running"

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 160
    :cond_1f
    const-wide/16 v0, 0xbb8

    #@21
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->show(J)V

    #@24
    .line 161
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@26
    if-nez v0, :cond_4c

    #@28
    .line 162
    const-string v0, "FULLockscreen"

    #@2a
    const-string v1, "Binding to Face Unlock service"

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 163
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mContext:Landroid/content/Context;

    #@31
    new-instance v1, Landroid/content/Intent;

    #@33
    const-class v2, Lcom/android/internal/policy/IFaceLockInterface;

    #@35
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mConnection:Landroid/content/ServiceConnection;

    #@3e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@40
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@43
    move-result v3

    #@44
    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@47
    .line 167
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@49
    .line 172
    :goto_49
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@4b
    .line 173
    return v4

    #@4c
    .line 169
    :cond_4c
    const-string v0, "FULLockscreen"

    #@4e
    const-string v1, "Attempt to bind to Face Unlock when already bound"

    #@50
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_49
.end method

.method public stop()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 181
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@6
    move-result-object v1

    #@7
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@a
    move-result-object v2

    #@b
    if-eq v1, v2, :cond_14

    #@d
    .line 182
    const-string v1, "FULLockscreen"

    #@f
    const-string v2, "stop() called off of the UI thread"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 185
    :cond_14
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@16
    .line 186
    .local v0, mWasRunning:Z
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->stopUi()V

    #@19
    .line 188
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@1b
    if-eqz v1, :cond_38

    #@1d
    .line 189
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@1f
    if-eqz v1, :cond_28

    #@21
    .line 191
    :try_start_21
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mService:Lcom/android/internal/policy/IFaceLockInterface;

    #@23
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mFaceUnlockCallback:Lcom/android/internal/policy/IFaceLockCallback;

    #@25
    invoke-interface {v1, v2}, Lcom/android/internal/policy/IFaceLockInterface;->unregisterCallback(Lcom/android/internal/policy/IFaceLockCallback;)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_28} :catch_3b

    #@28
    .line 196
    :cond_28
    :goto_28
    const-string v1, "FULLockscreen"

    #@2a
    const-string v2, "Unbinding from Face Unlock service"

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 197
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mContext:Landroid/content/Context;

    #@31
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mConnection:Landroid/content/ServiceConnection;

    #@33
    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@36
    .line 198
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mBoundToService:Z

    #@38
    .line 205
    :cond_38
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;->mIsRunning:Z

    #@3a
    .line 206
    return v0

    #@3b
    .line 192
    :catch_3b
    move-exception v1

    #@3c
    goto :goto_28
.end method
