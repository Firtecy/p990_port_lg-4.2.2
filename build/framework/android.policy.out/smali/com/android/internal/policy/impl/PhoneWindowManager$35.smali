.class Lcom/android/internal/policy/impl/PhoneWindowManager$35;
.super Landroid/os/IRemoteCallback$Stub;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->waitForKeyguardWindowDrawn(Landroid/os/IBinder;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

.field final synthetic val$screenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 7102
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$35;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$35;->val$screenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;

    #@4
    invoke-direct {p0}, Landroid/os/IRemoteCallback$Stub;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public sendResult(Landroid/os/Bundle;)V
    .registers 4
    .parameter "data"

    #@0
    .prologue
    .line 7105
    const-string v0, "WindowManager"

    #@2
    const-string v1, "Lock screen displayed!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 7106
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$35;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$35;->val$screenOnListener:Landroid/view/WindowManagerPolicy$ScreenOnListener;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$3600(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@e
    .line 7107
    return-void
.end method
