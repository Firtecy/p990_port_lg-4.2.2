.class final enum Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;
.super Ljava/lang/Enum;
.source "KeyguardPatternView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "FooterMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

.field public static final enum ForgotLockPattern:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

.field public static final enum Normal:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

.field public static final enum VerifyUnlocked:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 93
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@5
    const-string v1, "Normal"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->Normal:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@c
    .line 94
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@e
    const-string v1, "ForgotLockPattern"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->ForgotLockPattern:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@15
    .line 95
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@17
    const-string v1, "VerifyUnlocked"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->VerifyUnlocked:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@1e
    .line 92
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@21
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->Normal:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->ForgotLockPattern:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->VerifyUnlocked:Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 92
    const-class v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;
    .registers 1

    #@0
    .prologue
    .line 92
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->$VALUES:[Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/policy/impl/keyguard/KeyguardPatternView$FooterMode;

    #@8
    return-object v0
.end method
