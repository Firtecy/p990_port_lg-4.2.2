.class Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;
.super Ljava/lang/Object;
.source "KeyguardHostView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/OnDismissAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;->onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;

.field final synthetic val$fillInIntent:Landroid/content/Intent;

.field final synthetic val$pendingIntent:Landroid/app/PendingIntent;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 985
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->this$1:Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5;

    #@2
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$view:Landroid/view/View;

    #@4
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$pendingIntent:Landroid/app/PendingIntent;

    #@6
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$fillInIntent:Landroid/content/Intent;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public onDismiss()Z
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 989
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$view:Landroid/view/View;

    #@3
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    .line 990
    .local v0, context:Landroid/content/Context;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$view:Landroid/view/View;

    #@9
    const/4 v2, 0x0

    #@a
    const/4 v3, 0x0

    #@b
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$view:Landroid/view/View;

    #@d
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@10
    move-result v4

    #@11
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$view:Landroid/view/View;

    #@13
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@16
    move-result v5

    #@17
    invoke-static {v1, v2, v3, v4, v5}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    #@1a
    move-result-object v8

    #@1b
    .line 993
    .local v8, opts:Landroid/app/ActivityOptions;
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$pendingIntent:Landroid/app/PendingIntent;

    #@1d
    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardHostView$5$1;->val$fillInIntent:Landroid/content/Intent;

    #@23
    const/high16 v3, 0x1000

    #@25
    const/high16 v4, 0x1000

    #@27
    const/4 v5, 0x0

    #@28
    invoke-virtual {v8}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual/range {v0 .. v6}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V
    :try_end_2f
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_1 .. :try_end_2f} :catch_30
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_2f} :catch_39

    #@2f
    .line 1003
    .end local v0           #context:Landroid/content/Context;
    .end local v8           #opts:Landroid/app/ActivityOptions;
    :goto_2f
    return v9

    #@30
    .line 997
    :catch_30
    move-exception v7

    #@31
    .line 998
    .local v7, e:Landroid/content/IntentSender$SendIntentException;
    const-string v1, "KeyguardHostView"

    #@33
    const-string v2, "Cannot send pending intent: "

    #@35
    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_2f

    #@39
    .line 999
    .end local v7           #e:Landroid/content/IntentSender$SendIntentException;
    :catch_39
    move-exception v7

    #@3a
    .line 1000
    .local v7, e:Ljava/lang/Exception;
    const-string v1, "KeyguardHostView"

    #@3c
    const-string v2, "Cannot send pending intent due to unknown exception: "

    #@3e
    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    goto :goto_2f
.end method
