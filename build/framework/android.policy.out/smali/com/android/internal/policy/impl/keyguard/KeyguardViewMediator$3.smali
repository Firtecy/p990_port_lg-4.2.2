.class Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;
.super Landroid/content/BroadcastReceiver;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1536
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 1539
    const-string v6, "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"

    #@5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@8
    move-result-object v7

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v6

    #@d
    if-eqz v6, :cond_76

    #@f
    .line 1540
    const-string v6, "seq"

    #@11
    invoke-virtual {p2, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14
    move-result v2

    #@15
    .line 1541
    .local v2, sequence:I
    const-string v5, "KeyguardViewMediator"

    #@17
    new-instance v6, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v7, "received DELAYED_KEYGUARD_ACTION with seq = "

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    const-string v7, ", mDelayedShowingSequence = "

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2e
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)I

    #@31
    move-result v7

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1543
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@3f
    monitor-enter v6

    #@40
    .line 1544
    :try_start_40
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@42
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)I

    #@45
    move-result v5

    #@46
    if-ne v5, v2, :cond_65

    #@48
    .line 1546
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@4a
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@4d
    move-result v5

    #@4e
    if-eqz v5, :cond_67

    #@50
    .line 1548
    const-string v5, "KeyguardViewMediator"

    #@52
    const-string v7, "LockTimerState is changed, and reset is called."

    #@54
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1550
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@59
    sget v7, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@5b
    const/4 v8, 0x1

    #@5c
    invoke-static {v5, v7, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2400(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;IZ)V

    #@5f
    .line 1551
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@61
    const/4 v7, 0x0

    #@62
    invoke-static {v5, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2500(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@65
    .line 1561
    :cond_65
    :goto_65
    monitor-exit v6

    #@66
    .line 1668
    .end local v2           #sequence:I
    :cond_66
    :goto_66
    return-void

    #@67
    .line 1554
    .restart local v2       #sequence:I
    :cond_67
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@69
    const/4 v7, 0x1

    #@6a
    invoke-static {v5, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2602(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@6d
    .line 1555
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6f
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@72
    goto :goto_65

    #@73
    .line 1561
    :catchall_73
    move-exception v5

    #@74
    monitor-exit v6
    :try_end_75
    .catchall {:try_start_40 .. :try_end_75} :catchall_73

    #@75
    throw v5

    #@76
    .line 1563
    .end local v2           #sequence:I
    :cond_76
    const-string v6, "android.intent.action.SIM_STATE_CHANGED"

    #@78
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v6

    #@80
    if-eqz v6, :cond_1cf

    #@82
    .line 1565
    const-string v6, "KeyguardViewMediator"

    #@84
    const-string v7, "mBroadCastReceiver() intent.action : ACTION_SIM_STATE_CHANGED"

    #@86
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 1566
    const-string v6, "ss"

    #@8b
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    .line 1567
    .local v4, stateExtra:Ljava/lang/String;
    const-string v6, "KeyguardViewMediator"

    #@91
    new-instance v7, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v8, "mBroadCastReceiver() stateExtra : "

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v7

    #@a0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v7

    #@a4
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 1569
    const-string v6, "LOADED"

    #@a9
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v6

    #@ad
    if-eqz v6, :cond_1c4

    #@af
    .line 1570
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@b1
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@b4
    move-result-object v6

    #@b5
    invoke-virtual {v6, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setUsimStateLoaded(Z)V

    #@b8
    .line 1571
    const-string v6, "KR"

    #@ba
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2700()Ljava/lang/String;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v6

    #@c2
    if-eqz v6, :cond_192

    #@c4
    const-string v6, "SKT"

    #@c6
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$800()Ljava/lang/String;

    #@c9
    move-result-object v7

    #@ca
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cd
    move-result v6

    #@ce
    if-nez v6, :cond_dc

    #@d0
    const-string v6, "KT"

    #@d2
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$800()Ljava/lang/String;

    #@d5
    move-result-object v7

    #@d6
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d9
    move-result v6

    #@da
    if-eqz v6, :cond_192

    #@dc
    .line 1573
    :cond_dc
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@de
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@e1
    move-result-object v6

    #@e2
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e5
    move-result-object v6

    #@e6
    const-string v7, "usim_perso_locked"

    #@e8
    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@eb
    move-result v6

    #@ec
    if-ne v6, v0, :cond_10c

    #@ee
    .line 1574
    .local v0, locked:Z
    :goto_ee
    if-eqz v0, :cond_158

    #@f0
    sget-object v6, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@f2
    sget-object v7, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_FINISH_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@f4
    if-eq v6, v7, :cond_158

    #@f6
    .line 1576
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@f8
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2800(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@fb
    move-result v6

    #@fc
    if-nez v6, :cond_10e

    #@fe
    .line 1578
    const-string v5, "KeyguardViewMediator"

    #@100
    const-string v6, "[SKT OTA] isMatchedIMSI() : showLocked()"

    #@102
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 1579
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@107
    invoke-static {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@10a
    goto/16 :goto_66

    #@10c
    .end local v0           #locked:Z
    :cond_10c
    move v0, v5

    #@10d
    .line 1573
    goto :goto_ee

    #@10e
    .line 1581
    .restart local v0       #locked:Z
    :cond_10e
    new-instance v3, Landroid/content/Intent;

    #@110
    const-string v6, "android.intent.action.SIM_UNLOCKED"

    #@112
    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@115
    .line 1582
    .local v3, simUnlockIntent:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@117
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@11a
    move-result-object v6

    #@11b
    invoke-virtual {v6, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@11e
    .line 1584
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@120
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@123
    move-result-object v6

    #@124
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@127
    move-result-object v6

    #@128
    const-string v7, "skt_ota_usim_download"

    #@12a
    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12d
    move-result v5

    #@12e
    if-lez v5, :cond_13e

    #@130
    .line 1586
    const-string v5, "KeyguardViewMediator"

    #@132
    const-string v6, "[SKT OTA] directly run : Normal USIM_PERSO_FINISH_LOCK"

    #@134
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    .line 1587
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@139
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@13c
    goto/16 :goto_66

    #@13e
    .line 1588
    :cond_13e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@140
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@143
    move-result-object v5

    #@144
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isKTUSIMRegiRequires()Z

    #@147
    move-result v5

    #@148
    if-eqz v5, :cond_66

    #@14a
    .line 1590
    const-string v5, "KeyguardViewMediator"

    #@14c
    const-string v6, "[KTF OTA] : call KTNoUSIMActivityForLockScreen.java"

    #@14e
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 1591
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@153
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@156
    goto/16 :goto_66

    #@158
    .line 1595
    .end local v3           #simUnlockIntent:Landroid/content/Intent;
    :cond_158
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@15a
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Landroid/content/Context;

    #@15d
    move-result-object v6

    #@15e
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@161
    move-result-object v6

    #@162
    const-string v7, "skt_ota_usim_download"

    #@164
    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@167
    move-result v5

    #@168
    if-lez v5, :cond_178

    #@16a
    .line 1597
    const-string v5, "KeyguardViewMediator"

    #@16c
    const-string v6, "[SKT OTA] directly run : Normal"

    #@16e
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@171
    .line 1598
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@173
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2900(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@176
    goto/16 :goto_66

    #@178
    .line 1599
    :cond_178
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@17a
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@17d
    move-result-object v5

    #@17e
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isKTUSIMRegiRequires()Z

    #@181
    move-result v5

    #@182
    if-eqz v5, :cond_66

    #@184
    .line 1601
    const-string v5, "KeyguardViewMediator"

    #@186
    const-string v6, "[KTF OTA] : call KTNoUSIMActivityForLockScreen.java"

    #@188
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18b
    .line 1602
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@18d
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3000(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@190
    goto/16 :goto_66

    #@192
    .line 1605
    .end local v0           #locked:Z
    :cond_192
    const-string v5, "KR"

    #@194
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$2700()Ljava/lang/String;

    #@197
    move-result-object v6

    #@198
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19b
    move-result v5

    #@19c
    if-eqz v5, :cond_66

    #@19e
    const-string v5, "LGU"

    #@1a0
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$800()Ljava/lang/String;

    #@1a3
    move-result-object v6

    #@1a4
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a7
    move-result v5

    #@1a8
    if-eqz v5, :cond_66

    #@1aa
    .line 1606
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1ac
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1af
    move-result-object v5

    #@1b0
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isLGTUSIMRegiRequires()Z

    #@1b3
    move-result v5

    #@1b4
    if-eqz v5, :cond_66

    #@1b6
    .line 1608
    const-string v5, "KeyguardViewMediator"

    #@1b8
    const-string v6, "[LGU+ OTA] : call LGTNoUSIMActivityForLockScreen.java"

    #@1ba
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    .line 1609
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1bf
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@1c2
    goto/16 :goto_66

    #@1c4
    .line 1613
    :cond_1c4
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1c6
    invoke-static {v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@1c9
    move-result-object v6

    #@1ca
    invoke-virtual {v6, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setUsimStateLoaded(Z)V

    #@1cd
    goto/16 :goto_66

    #@1cf
    .line 1619
    .end local v4           #stateExtra:Ljava/lang/String;
    :cond_1cf
    const-string v6, "com.lge.intent.action.LTE_MISSING_PHONE"

    #@1d1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1d4
    move-result-object v7

    #@1d5
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d8
    move-result v6

    #@1d9
    if-nez v6, :cond_1e7

    #@1db
    const-string v6, "android.intent.action.LTE_EMM_REJECT"

    #@1dd
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1e0
    move-result-object v7

    #@1e1
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e4
    move-result v6

    #@1e5
    if-eqz v6, :cond_243

    #@1e7
    .line 1621
    :cond_1e7
    sget-boolean v6, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@1e9
    if-eqz v6, :cond_66

    #@1eb
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1ee
    move-result-object v6

    #@1ef
    if-eqz v6, :cond_66

    #@1f1
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@1f4
    move-result-object v6

    #@1f5
    const-string v7, "config_feature_attach_reject_lgu"

    #@1f7
    invoke-virtual {v6, v7}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isFeatureEnabled(Ljava/lang/String;)Z

    #@1fa
    move-result v6

    #@1fb
    if-eqz v6, :cond_66

    #@1fd
    .line 1623
    const-string v6, "rejectCode"

    #@1ff
    invoke-virtual {p2, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@202
    move-result v1

    #@203
    .line 1624
    .local v1, mRejectNum:I
    const-string v6, "KeyguardViewMediator"

    #@205
    new-instance v7, Ljava/lang/StringBuilder;

    #@207
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20a
    const-string v8, "mRejectNum : "

    #@20c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v7

    #@210
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@213
    move-result-object v7

    #@214
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@217
    move-result-object v7

    #@218
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21b
    .line 1625
    const/4 v6, 0x5

    #@21c
    if-eq v1, v6, :cond_221

    #@21e
    const/4 v6, 0x6

    #@21f
    if-ne v1, v6, :cond_235

    #@221
    .line 1626
    :cond_221
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@223
    invoke-static {v5, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@226
    .line 1627
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@228
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@22b
    move-result v5

    #@22c
    if-nez v5, :cond_66

    #@22e
    .line 1628
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@230
    invoke-static {v5, v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1300(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Landroid/os/Bundle;)V

    #@233
    goto/16 :goto_66

    #@235
    .line 1630
    :cond_235
    if-nez v1, :cond_66

    #@237
    .line 1631
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@239
    invoke-static {v6, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$1202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@23c
    .line 1632
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@23e
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@241
    goto/16 :goto_66

    #@243
    .line 1637
    .end local v1           #mRejectNum:I
    :cond_243
    const-string v6, "android.intent.action.OMADM_DEVICE_LOCK_MSG"

    #@245
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@248
    move-result-object v7

    #@249
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24c
    move-result v6

    #@24d
    if-eqz v6, :cond_25d

    #@24f
    .line 1638
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@251
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setOMADM()V

    #@254
    .line 1639
    const-string v5, "KeyguardViewMediator"

    #@256
    const-string v6, "OMADM intent received"

    #@258
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25b
    goto/16 :goto_66

    #@25d
    .line 1647
    :cond_25d
    const-string v6, "com.lge.lollipop.action.UNLOCKSCREEN"

    #@25f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@262
    move-result-object v7

    #@263
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@266
    move-result v6

    #@267
    if-eqz v6, :cond_27f

    #@269
    .line 1648
    const-string v5, "KeyguardViewMediator"

    #@26b
    const-string v6, "Lollipop intent received!"

    #@26d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@270
    .line 1649
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@272
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@275
    move-result v5

    #@276
    if-nez v5, :cond_66

    #@278
    .line 1650
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@27a
    invoke-virtual {v5, v0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->keyguardDone(ZZ)V

    #@27d
    goto/16 :goto_66

    #@27f
    .line 1655
    :cond_27f
    const-string v6, "com.lge.mdm.intent.action.LOCKOUT_FOR_SWIPE"

    #@281
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@284
    move-result-object v7

    #@285
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@288
    move-result v6

    #@289
    if-eqz v6, :cond_66

    #@28b
    .line 1656
    const-string v6, "KeyguardViewMediator"

    #@28d
    const-string v7, "intent.action.LOCKOUT_FOR_SWIPE intent received"

    #@28f
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@292
    .line 1657
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@294
    monitor-enter v6

    #@295
    .line 1658
    :try_start_295
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@297
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)V

    #@29a
    .line 1659
    monitor-exit v6
    :try_end_29b
    .catchall {:try_start_295 .. :try_end_29b} :catchall_2df

    #@29b
    .line 1661
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@29d
    invoke-static {v6, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@2a0
    .line 1662
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@2a2
    if-eqz v5, :cond_66

    #@2a4
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2a7
    move-result-object v5

    #@2a8
    if-eqz v5, :cond_66

    #@2aa
    .line 1663
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2ac
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2af
    move-result-object v6

    #@2b0
    invoke-interface {v6, v9}, Lcom/lge/cappuccino/IMdm;->getLockoutNow(Landroid/content/ComponentName;)Z

    #@2b3
    move-result v6

    #@2b4
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3202(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;Z)Z

    #@2b7
    .line 1664
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2b9
    invoke-static {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@2bc
    move-result v5

    #@2bd
    if-eqz v5, :cond_66

    #@2bf
    const-string v5, "KeyguardViewMediator"

    #@2c1
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c6
    const-string v7, "ACTION_LOCKOUT_FOR_SWIPE : lockoutMode is "

    #@2c8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cb
    move-result-object v6

    #@2cc
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$3;->this$0:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2ce
    invoke-static {v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->access$3200(Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;)Z

    #@2d1
    move-result v7

    #@2d2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d5
    move-result-object v6

    #@2d6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d9
    move-result-object v6

    #@2da
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2dd
    goto/16 :goto_66

    #@2df
    .line 1659
    :catchall_2df
    move-exception v5

    #@2e0
    :try_start_2e0
    monitor-exit v6
    :try_end_2e1
    .catchall {:try_start_2e0 .. :try_end_2e1} :catchall_2df

    #@2e1
    throw v5
.end method
