.class Lcom/android/internal/policy/impl/keyguard/ClockView$FormatChangeObserver;
.super Landroid/database/ContentObserver;
.source "ClockView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/ClockView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FormatChangeObserver"
.end annotation


# instance fields
.field private mClock:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/internal/policy/impl/keyguard/ClockView;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/keyguard/ClockView;)V
    .registers 3
    .parameter "clock"

    #@0
    .prologue
    .line 125
    new-instance v0, Landroid/os/Handler;

    #@2
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@5
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@8
    .line 126
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@a
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$FormatChangeObserver;->mClock:Ljava/lang/ref/WeakReference;

    #@f
    .line 127
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/ClockView;->getContext()Landroid/content/Context;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$FormatChangeObserver;->mContext:Landroid/content/Context;

    #@15
    .line 128
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 4
    .parameter "selfChange"

    #@0
    .prologue
    .line 131
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$FormatChangeObserver;->mClock:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@8
    .line 132
    .local v0, digitalClock:Lcom/android/internal/policy/impl/keyguard/ClockView;
    if-eqz v0, :cond_11

    #@a
    .line 133
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/ClockView;->access$200(Lcom/android/internal/policy/impl/keyguard/ClockView;)V

    #@d
    .line 134
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/ClockView;->updateTime()V

    #@10
    .line 142
    :goto_10
    return-void

    #@11
    .line 137
    :cond_11
    :try_start_11
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$FormatChangeObserver;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_1a
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_10

    #@1b
    .line 138
    :catch_1b
    move-exception v1

    #@1c
    goto :goto_10
.end method
