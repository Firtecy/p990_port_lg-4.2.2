.class Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;
.super Ljava/lang/Object;
.source "GlobalActions.java"

# interfaces
.implements Lcom/android/internal/policy/impl/GlobalActions$Action;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/GlobalActions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SilentModeTriStateAction"
.end annotation


# instance fields
.field private final ITEM_IDS:[I

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/os/Handler;)V
    .registers 5
    .parameter "context"
    .parameter "audioManager"
    .parameter "handler"

    #@0
    .prologue
    .line 899
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 892
    const/4 v0, 0x3

    #@4
    new-array v0, v0, [I

    #@6
    fill-array-data v0, :array_12

    #@9
    iput-object v0, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->ITEM_IDS:[I

    #@b
    .line 900
    iput-object p2, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mAudioManager:Landroid/media/AudioManager;

    #@d
    .line 901
    iput-object p3, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mHandler:Landroid/os/Handler;

    #@f
    .line 902
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mContext:Landroid/content/Context;

    #@11
    .line 903
    return-void

    #@12
    .line 892
    :array_12
    .array-data 0x4
        0x92t 0x2t 0x2t 0x1t
        0x93t 0x2t 0x2t 0x1t
        0x94t 0x2t 0x2t 0x1t
    .end array-data
.end method

.method private indexToRingerMode(I)I
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 912
    return p1
.end method

.method private ringerModeToIndex(I)I
    .registers 2
    .parameter "ringerMode"

    #@0
    .prologue
    .line 907
    return p1
.end method


# virtual methods
.method public create(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 11
    .parameter "context"
    .parameter "convertView"
    .parameter "parent"
    .parameter "inflater"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 917
    const v4, 0x1090045

    #@4
    invoke-virtual {p4, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@7
    move-result-object v3

    #@8
    .line 919
    .local v3, v:Landroid/view/View;
    iget-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mAudioManager:Landroid/media/AudioManager;

    #@a
    invoke-virtual {v4}, Landroid/media/AudioManager;->getRingerMode()I

    #@d
    move-result v4

    #@e
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->ringerModeToIndex(I)I

    #@11
    move-result v2

    #@12
    .line 920
    .local v2, selectedIndex:I
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    const/4 v4, 0x3

    #@14
    if-ge v0, v4, :cond_33

    #@16
    .line 921
    iget-object v4, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->ITEM_IDS:[I

    #@18
    aget v4, v4, v0

    #@1a
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1d
    move-result-object v1

    #@1e
    .line 922
    .local v1, itemView:Landroid/view/View;
    if-ne v2, v0, :cond_31

    #@20
    const/4 v4, 0x1

    #@21
    :goto_21
    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    #@24
    .line 924
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@2b
    .line 925
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2e
    .line 920
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_13

    #@31
    :cond_31
    move v4, v5

    #@32
    .line 922
    goto :goto_21

    #@33
    .line 927
    .end local v1           #itemView:Landroid/view/View;
    :cond_33
    return-object v3
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 946
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 9
    .parameter "v"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 953
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@4
    move-result-object v3

    #@5
    instance-of v3, v3, Ljava/lang/Integer;

    #@7
    if-nez v3, :cond_a

    #@9
    .line 975
    :goto_9
    return-void

    #@a
    .line 955
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@d
    move-result-object v3

    #@e
    check-cast v3, Ljava/lang/Integer;

    #@10
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@13
    move-result v3

    #@14
    invoke-static {v3}, Lcom/android/internal/policy/impl/GlobalActions;->access$1402(I)I

    #@17
    .line 958
    :try_start_17
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mContext:Landroid/content/Context;

    #@19
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, "quiet_mode_status"

    #@1f
    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@22
    move-result v3

    #@23
    const/4 v4, 0x1

    #@24
    if-ne v3, v4, :cond_67

    #@26
    .line 959
    const/4 v3, -0x1

    #@27
    invoke-static {v3}, Lcom/android/internal/policy/impl/GlobalActions;->access$1402(I)I

    #@2a
    .line 960
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mHandler:Landroid/os/Handler;

    #@2c
    const/4 v4, 0x0

    #@2d
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@30
    .line 961
    new-instance v2, Landroid/content/Intent;

    #@32
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@35
    .line 962
    .local v2, in:Landroid/content/Intent;
    const-string v3, "com.lge.settings.QUIET_MODE_ASK_SOUNDPROFILE"

    #@37
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@3a
    .line 963
    const/high16 v3, 0x3000

    #@3c
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_3f
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_17 .. :try_end_3f} :catch_5f

    #@3f
    .line 965
    :try_start_3f
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mContext:Landroid/content/Context;

    #@41
    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_44
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3f .. :try_end_44} :catch_45
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3f .. :try_end_44} :catch_5f

    #@44
    goto :goto_9

    #@45
    .line 966
    :catch_45
    move-exception v1

    #@46
    .line 967
    .local v1, ex:Landroid/content/ActivityNotFoundException;
    :try_start_46
    const-string v3, "GlobalActions"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "QmodeAlertActivity "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5e
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_46 .. :try_end_5e} :catch_5f

    #@5e
    goto :goto_9

    #@5f
    .line 971
    .end local v1           #ex:Landroid/content/ActivityNotFoundException;
    .end local v2           #in:Landroid/content/Intent;
    :catch_5f
    move-exception v0

    #@60
    .line 972
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, "GlobalActions"

    #@62
    const-string v4, "SettingNotFoundException - getDBQuietModeState()"

    #@64
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 974
    .end local v0           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_67
    iget-object v3, p0, Lcom/android/internal/policy/impl/GlobalActions$SilentModeTriStateAction;->mHandler:Landroid/os/Handler;

    #@69
    const-wide/16 v4, 0x12c

    #@6b
    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@6e
    goto :goto_9
.end method

.method public onLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 934
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onPress()V
    .registers 1

    #@0
    .prologue
    .line 931
    return-void
.end method

.method public showBeforeProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 942
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showDuringKeyguard()Z
    .registers 2

    #@0
    .prologue
    .line 938
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method willCreate()V
    .registers 1

    #@0
    .prologue
    .line 950
    return-void
.end method
