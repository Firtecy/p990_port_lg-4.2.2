.class final Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$1;
.super Landroid/util/FloatProperty;
.source "SlidingChallengeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/FloatProperty",
        "<",
        "Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    invoke-direct {p0, p1}, Landroid/util/FloatProperty;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public get(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)Ljava/lang/Float;
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 152
    iget v0, p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAlpha:F

    #@2
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 143
    check-cast p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$1;->get(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;)Ljava/lang/Float;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public setValue(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;F)V
    .registers 3
    .parameter "view"
    .parameter "value"

    #@0
    .prologue
    .line 146
    iput p2, p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->mHandleAlpha:F

    #@2
    .line 147
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;->invalidate()V

    #@5
    .line 148
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;F)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 143
    check-cast p1, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;

    #@2
    .end local p1
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout$1;->setValue(Lcom/android/internal/policy/impl/keyguard/SlidingChallengeLayout;F)V

    #@5
    return-void
.end method
