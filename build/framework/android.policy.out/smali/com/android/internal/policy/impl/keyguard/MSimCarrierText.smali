.class public Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;
.super Lcom/android/internal/policy/impl/keyguard/CarrierText;
.source "MSimCarrierText.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MSimCarrierText"


# instance fields
.field private mMSimCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/CarrierText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 39
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText$1;-><init>(Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->mMSimCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@a
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->mMSimCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;->initialize()V

    #@f
    .line 73
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 98
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->onAttachedToWindow()V

    #@3
    .line 99
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->mMSimCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 100
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 104
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->onDetachedFromWindow()V

    #@3
    .line 105
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->mMSimCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 106
    return-void
.end method

.method protected onFinishInflate()V
    .registers 3

    #@0
    .prologue
    .line 92
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->onFinishInflate()V

    #@3
    .line 93
    const-string v0, "MSimCarrierText"

    #@5
    const-string v1, "onFinishInflate!!!"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 94
    return-void
.end method

.method protected updateCarrierText([Lcom/android/internal/telephony/IccCardConstants$State;[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V
    .registers 11
    .parameter "simState"
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 76
    const-string v2, ""

    #@2
    .line 77
    .local v2, text:Ljava/lang/CharSequence;
    const/4 v1, 0x0

    #@3
    .local v1, i:I
    :goto_3
    array-length v3, p1

    #@4
    if-ge v1, v3, :cond_40

    #@6
    .line 78
    aget-object v3, p1, v1

    #@8
    aget-object v4, p2, v1

    #@a
    aget-object v5, p3, v1

    #@c
    invoke-virtual {p0, v3, v4, v5}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->getCarrierTextForSimState(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@f
    move-result-object v0

    #@10
    .line 79
    .local v0, displayText:Ljava/lang/CharSequence;
    sget-boolean v3, Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager;->USE_UPPER_CASE:Z

    #@12
    if-eqz v3, :cond_1e

    #@14
    .line 80
    if-eqz v0, :cond_28

    #@16
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .line 82
    :cond_1e
    :goto_1e
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_2b

    #@24
    move-object v2, v0

    #@25
    .line 77
    :goto_25
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_3

    #@28
    .line 80
    :cond_28
    const-string v0, ""

    #@2a
    goto :goto_1e

    #@2b
    .line 82
    :cond_2b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v3

    #@2f
    const v4, 0x1040574

    #@32
    const/4 v5, 0x2

    #@33
    new-array v5, v5, [Ljava/lang/Object;

    #@35
    const/4 v6, 0x0

    #@36
    aput-object v2, v5, v6

    #@38
    const/4 v6, 0x1

    #@39
    aput-object v0, v5, v6

    #@3b
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    goto :goto_25

    #@40
    .line 86
    .end local v0           #displayText:Ljava/lang/CharSequence;
    :cond_40
    const-string v3, "MSimCarrierText"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "updateCarrierText: text = "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 87
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/keyguard/MSimCarrierText;->setText(Ljava/lang/CharSequence;)V

    #@5b
    .line 88
    return-void
.end method
