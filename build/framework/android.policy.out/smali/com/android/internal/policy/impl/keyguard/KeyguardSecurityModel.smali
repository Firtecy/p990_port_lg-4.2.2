.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;
.super Ljava/lang/Object;
.source "KeyguardSecurityModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$1;,
        Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    }
.end annotation


# static fields
.field private static final carrier:Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field protected final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 52
    const-string v0, "ro.build.target_operator"

    #@2
    const-string v1, "COM"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->carrier:Ljava/lang/String;

    #@a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 49
    const-string v0, "KeyguardSecurityModel"

    #@6
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@8
    .line 51
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@a
    .line 53
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@c
    .line 54
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@e
    .line 58
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@10
    .line 59
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    #@12
    invoke-direct {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@15
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@17
    .line 60
    return-void
.end method

.method private isBiometricUnlockSuppressed()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 86
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v1

    #@8
    .line 87
    .local v1, monitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getFailedUnlockAttempts()I

    #@b
    move-result v4

    #@c
    const/4 v5, 0x5

    #@d
    if-lt v4, v5, :cond_26

    #@f
    move v0, v2

    #@10
    .line 89
    .local v0, backupIsTimedOut:Z
    :goto_10
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getMaxBiometricUnlockAttemptsReached()Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_24

    #@16
    if-nez v0, :cond_24

    #@18
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isAlternateUnlockEnabled()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_24

    #@1e
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getPhoneState()I

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_25

    #@24
    :cond_24
    move v3, v2

    #@25
    :cond_25
    return v3

    #@26
    .end local v0           #backupIsTimedOut:Z
    :cond_26
    move v0, v3

    #@27
    .line 87
    goto :goto_10
.end method

.method private isSupportForgotPinPassword()Z
    .registers 3

    #@0
    .prologue
    .line 96
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    .line 97
    .local v0, res:Landroid/content/res/Resources;
    sget-boolean v1, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->CAPP_LOCKSCREEN:Z

    #@8
    if-eqz v1, :cond_1c

    #@a
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@d
    move-result-object v1

    #@e
    if-eqz v1, :cond_1c

    #@10
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->getInstance()Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/LgeKeyguardPackageManager;->isSupportForgotPinPassword()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    const/4 v1, 0x1

    #@1b
    :goto_1b
    return v1

    #@1c
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_1b
.end method

.method private isUsimPersoLockScreenMode()Z
    .registers 16

    #@0
    .prologue
    .line 240
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@2
    new-instance v13, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v14, "[isUsimPersoLockScreenMode] mTelephonyManager(1)= "

    #@9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v13

    #@d
    iget-object v14, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v13

    #@13
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v13

    #@17
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 241
    const/4 v12, 0x0

    #@1b
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1d
    .line 242
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@1f
    invoke-static {v12}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@22
    move-result-object v9

    #@23
    .line 243
    .local v9, mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@25
    const-string v13, "phone"

    #@27
    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v12

    #@2b
    check-cast v12, Landroid/telephony/TelephonyManager;

    #@2d
    iput-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2f
    .line 244
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@31
    new-instance v13, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v14, "[isUsimPersoLockScreenMode] mTelephonyManager(2)= "

    #@38
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v13

    #@3c
    iget-object v14, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@3e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v13

    #@42
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v13

    #@46
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 246
    const/4 v0, 0x0

    #@4a
    .line 247
    .local v0, PersoState1:Ljava/lang/String;
    const/4 v1, 0x0

    #@4b
    .line 248
    .local v1, PersoState2:Ljava/lang/String;
    const/4 v3, 0x0

    #@4c
    .line 249
    .local v3, getSubscriberId1:Ljava/lang/String;
    const/4 v4, 0x0

    #@4d
    .line 250
    .local v4, getSubscriberId2:Ljava/lang/String;
    const/4 v5, 0x0

    #@4e
    .line 251
    .local v5, getUsimPersoEfImsi1:Ljava/lang/String;
    const/4 v6, 0x0

    #@4f
    .line 252
    .local v6, getUsimPersoEfImsi2:Ljava/lang/String;
    const/4 v8, 0x0

    #@50
    .line 254
    .local v8, locked:Z
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@52
    const-string v13, "[isUsimPersoLockScreenMode] START "

    #@54
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 258
    :try_start_57
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@59
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5c
    move-result-object v12

    #@5d
    const-string v13, "usim_perso_locked"

    #@5f
    const/4 v14, 0x0

    #@60
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_63
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_63} :catch_9e

    #@63
    move-result v12

    #@64
    const/4 v13, 0x1

    #@65
    if-ne v12, v13, :cond_9c

    #@67
    const/4 v8, 0x1

    #@68
    .line 266
    :goto_68
    const/4 v10, 0x1

    #@69
    .line 279
    .local v10, matched:Z
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@6b
    const-string v13, "isKTUSIMDownloadRequires()"

    #@6d
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 281
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimType()I

    #@73
    move-result v11

    #@74
    .line 282
    .local v11, usimType:I
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getUsimIsEmpty()I

    #@77
    move-result v7

    #@78
    .line 286
    .local v7, isUsimEmpty:I
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@7a
    new-instance v13, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v14, "[isUsimPersoLockScreenMode] mUpdateMonitor.mUsimPersoFinishState= "

    #@81
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v13

    #@85
    sget-object v14, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@87
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v13

    #@8b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v13

    #@8f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 287
    sget-object v12, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@94
    sget-object v13, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->mUsimPersoFinishState:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@96
    sget-object v13, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;->USIM_PERSO_FINISH_LOCK:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor$UsimPersoFinishState;

    #@98
    if-ne v12, v13, :cond_a9

    #@9a
    .line 289
    const/4 v12, 0x0

    #@9b
    .line 339
    .end local v7           #isUsimEmpty:I
    .end local v10           #matched:Z
    .end local v11           #usimType:I
    :goto_9b
    return v12

    #@9c
    .line 258
    :cond_9c
    const/4 v8, 0x0

    #@9d
    goto :goto_68

    #@9e
    .line 259
    :catch_9e
    move-exception v2

    #@9f
    .line 260
    .local v2, e:Ljava/lang/Exception;
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@a1
    const-string v13, "can not read [isUsimPersoLockScreenMode] DB value"

    #@a3
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 261
    const/4 v8, 0x0

    #@a7
    .line 262
    const/4 v12, 0x0

    #@a8
    goto :goto_9b

    #@a9
    .line 293
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v7       #isUsimEmpty:I
    .restart local v10       #matched:Z
    .restart local v11       #usimType:I
    :cond_a9
    if-eqz v8, :cond_172

    #@ab
    .line 294
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@ad
    const-string v13, "[isUsimPersoLockScreenMode] Setting is locked-TRUE"

    #@af
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 296
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@b4
    invoke-virtual {v12}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    .line 301
    const/4 v12, 0x1

    #@b9
    if-ne v11, v12, :cond_d2

    #@bb
    const/4 v12, 0x2

    #@bc
    if-ne v7, v12, :cond_d2

    #@be
    if-eqz v3, :cond_c6

    #@c0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@c3
    move-result v12

    #@c4
    if-nez v12, :cond_d2

    #@c6
    .line 302
    :cond_c6
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@c8
    const-string v13, "[isUsimPersoLockScreenMode]  RETURN LOCKMODE TRUE"

    #@ca
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 303
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setUsimPersoLocking()V

    #@d0
    .line 304
    const/4 v12, 0x1

    #@d1
    goto :goto_9b

    #@d2
    .line 308
    :cond_d2
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@d4
    new-instance v13, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v14, "[isUsimPersoLockScreenMode] getSubscriberId1(Read) = "

    #@db
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v13

    #@df
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v13

    #@e3
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v13

    #@e7
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 309
    if-eqz v3, :cond_f2

    #@ec
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@ef
    move-result v12

    #@f0
    if-nez v12, :cond_115

    #@f2
    .line 310
    :cond_f2
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@f4
    const-string v13, "[isUsimPersoLockScreenMode] getSubscriberId NULL"

    #@f6
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    .line 315
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@fb
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fe
    move-result-object v12

    #@ff
    const-string v13, "skt_ota_usim_download"

    #@101
    const/4 v14, 0x0

    #@102
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@105
    move-result v12

    #@106
    if-lez v12, :cond_113

    #@108
    const/4 v12, 0x1

    #@109
    if-ne v11, v12, :cond_113

    #@10b
    const/4 v12, 0x2

    #@10c
    if-ne v7, v12, :cond_113

    #@10e
    .line 316
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setUsimPersoLocking()V

    #@111
    .line 317
    const/4 v12, 0x1

    #@112
    goto :goto_9b

    #@113
    .line 321
    :cond_113
    const/4 v12, 0x0

    #@114
    goto :goto_9b

    #@115
    .line 323
    :cond_115
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@118
    move-result-object v4

    #@119
    .line 324
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@11b
    new-instance v13, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v14, "[isUsimPersoLockScreenMode] getSubscriberId2 = "

    #@122
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v13

    #@126
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v13

    #@12a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v13

    #@12e
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@131
    .line 325
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@133
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@136
    move-result-object v12

    #@137
    const-string v13, "usim_perso_imsi"

    #@139
    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@13c
    move-result-object v12

    #@13d
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@140
    move-result v10

    #@141
    .line 326
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@143
    new-instance v13, Ljava/lang/StringBuilder;

    #@145
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@148
    const-string v14, "[isUsimPersoLockScreenMode] matched = "

    #@14a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v13

    #@14e
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@151
    move-result-object v13

    #@152
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v13

    #@156
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@159
    .line 328
    if-eqz v10, :cond_165

    #@15b
    .line 329
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@15d
    const-string v13, "[isUsimPersoLockScreenMode]  IMSI matched !!- RETURN LOCKMODE FALSE"

    #@15f
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@162
    .line 330
    const/4 v12, 0x0

    #@163
    goto/16 :goto_9b

    #@165
    .line 332
    :cond_165
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@167
    const-string v13, "[isUsimPersoLockScreenMode] IMSI  NOT matched!!- RETURN LOCKMODE TRUE"

    #@169
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    .line 334
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setUsimPersoLocking()V

    #@16f
    .line 335
    const/4 v12, 0x1

    #@170
    goto/16 :goto_9b

    #@172
    .line 338
    :cond_172
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@174
    const-string v13, "[isUsimPersoLockScreenMode] Setting is unlocked-FALSE"

    #@176
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@179
    .line 339
    const/4 v12, 0x0

    #@17a
    goto/16 :goto_9b
.end method


# virtual methods
.method public getAlternateFor(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->isBiometricUnlockEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1a

    #@6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->isBiometricUnlockSuppressed()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_1a

    #@c
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@e
    if-eq p1, v0, :cond_18

    #@10
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->PIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@12
    if-eq p1, v0, :cond_18

    #@14
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@16
    if-ne p1, v0, :cond_1a

    #@18
    .line 215
    :cond_18
    sget-object p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Biometric:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@1a
    .line 217
    .end local p1
    :cond_1a
    return-object p1
.end method

.method public getBackupSecurityMode(Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;)Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 228
    sget-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$1;->$SwitchMap$com$android$internal$policy$impl$keyguard$KeyguardSecurityModel$SecurityMode:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_14

    #@b
    .line 234
    .end local p1
    :goto_b
    return-object p1

    #@c
    .line 230
    .restart local p1
    :pswitch_c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f
    move-result-object p1

    #@10
    goto :goto_b

    #@11
    .line 232
    :pswitch_11
    sget-object p1, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@13
    goto :goto_b

    #@14
    .line 228
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_c
        :pswitch_11
    .end packed-switch
.end method

.method public getSecurityMode()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .registers 16

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 105
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v12}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@7
    move-result-object v9

    #@8
    .line 106
    .local v9, updateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;
    invoke-virtual {v9}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@b
    move-result-object v8

    #@c
    .line 113
    .local v8, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v12

    #@10
    invoke-virtual {v12}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@13
    move-result v6

    #@14
    .line 114
    .local v6, numPhones:I
    const/4 v1, 0x0

    #@15
    .local v1, i:I
    :goto_15
    if-ge v1, v6, :cond_23

    #@17
    .line 115
    invoke-virtual {v9, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    #@1a
    move-result-object v8

    #@1b
    .line 120
    sget-object v12, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1d
    if-eq v8, v12, :cond_23

    #@1f
    sget-object v12, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@21
    if-ne v8, v12, :cond_2d

    #@23
    .line 126
    :cond_23
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@25
    .line 127
    .local v4, mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    sget-object v12, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@27
    if-ne v8, v12, :cond_30

    #@29
    .line 128
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPin:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@2b
    :cond_2b
    :goto_2b
    move-object v5, v4

    #@2c
    .line 198
    .end local v4           #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .local v5, mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :goto_2c
    return-object v5

    #@2d
    .line 114
    .end local v5           #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_15

    #@30
    .line 129
    .restart local v4       #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_30
    sget-object v12, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@32
    if-ne v8, v12, :cond_3f

    #@34
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@36
    invoke-virtual {v12}, Lcom/android/internal/widget/LockPatternUtils;->isPukUnlockScreenEnable()Z

    #@39
    move-result v12

    #@3a
    if-eqz v12, :cond_3f

    #@3c
    .line 131
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@3e
    goto :goto_2b

    #@3f
    .line 134
    :cond_3f
    const-string v12, "SKT"

    #@41
    sget-object v13, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->carrier:Ljava/lang/String;

    #@43
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v12

    #@47
    if-nez v12, :cond_53

    #@49
    const-string v12, "KT"

    #@4b
    sget-object v13, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->carrier:Ljava/lang/String;

    #@4d
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v12

    #@51
    if-eqz v12, :cond_94

    #@53
    .line 135
    :cond_53
    const/4 v3, 0x0

    #@54
    .line 137
    .local v3, locked:Z
    :try_start_54
    iget-object v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@56
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@59
    move-result-object v12

    #@5a
    const-string v13, "usim_perso_locked"

    #@5c
    const/4 v14, 0x0

    #@5d
    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@60
    move-result v12

    #@61
    if-ne v12, v10, :cond_88

    #@63
    move v3, v10

    #@64
    .line 138
    :goto_64
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@66
    new-instance v12, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v13, "locked="

    #@6d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v12

    #@71
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@74
    move-result-object v12

    #@75
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v12

    #@79
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_7c} :catch_8a

    #@7c
    .line 143
    :goto_7c
    if-eqz v3, :cond_94

    #@7e
    .line 144
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->isUsimPersoLockScreenMode()Z

    #@81
    move-result v11

    #@82
    if-ne v11, v10, :cond_94

    #@84
    .line 145
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->UsimPerso:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@86
    move-object v5, v4

    #@87
    .line 146
    .end local v4           #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .restart local v5       #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    goto :goto_2c

    #@88
    .end local v5           #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    .restart local v4       #mode:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;
    :cond_88
    move v3, v11

    #@89
    .line 137
    goto :goto_64

    #@8a
    .line 139
    :catch_8a
    move-exception v0

    #@8b
    .line 140
    .local v0, e:Ljava/lang/Exception;
    iget-object v11, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->TAG:Ljava/lang/String;

    #@8d
    const-string v12, "can not read [USIM_PERSO_LOCKED] DB value"

    #@8f
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 141
    const/4 v3, 0x0

    #@93
    goto :goto_7c

    #@94
    .line 151
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #locked:Z
    :cond_94
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@96
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@99
    move-result v7

    #@9a
    .line 152
    .local v7, security:I
    sparse-switch v7, :sswitch_data_12e

    #@9d
    .line 183
    new-instance v10, Ljava/lang/IllegalStateException;

    #@9f
    new-instance v11, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v12, "Unknown unlock mode:"

    #@a6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v11

    #@aa
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v11

    #@ae
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v11

    #@b2
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b5
    throw v10

    #@b6
    .line 154
    :sswitch_b6
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@b8
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@bb
    move-result v10

    #@bc
    if-eqz v10, :cond_f6

    #@be
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->PIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@c0
    .line 157
    :goto_c0
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->isSupportForgotPinPassword()Z

    #@c3
    move-result v10

    #@c4
    if-eqz v10, :cond_d0

    #@c6
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@c8
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@cb
    move-result v10

    #@cc
    if-eqz v10, :cond_d0

    #@ce
    .line 158
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d0
    .line 186
    :cond_d0
    :goto_d0
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->PIN:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d2
    if-eq v4, v10, :cond_dc

    #@d4
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@d6
    if-eq v4, v10, :cond_dc

    #@d8
    sget-object v10, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@da
    if-ne v4, v10, :cond_2b

    #@dc
    .line 188
    :cond_dc
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mContext:Landroid/content/Context;

    #@de
    invoke-static {v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@e1
    move-result-object v10

    #@e2
    invoke-virtual {v10}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getLockTimerState()I

    #@e5
    move-result v2

    #@e6
    .line 190
    .local v2, lockTimerState:I
    sget v10, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_DISABLE:I

    #@e8
    if-eq v2, v10, :cond_2b

    #@ea
    sget v10, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_NONE:I

    #@ec
    if-eq v2, v10, :cond_2b

    #@ee
    sget v10, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->LOCKTIMER_STATE_END:I

    #@f0
    if-eq v2, v10, :cond_2b

    #@f2
    .line 193
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f4
    goto/16 :goto_2b

    #@f6
    .line 154
    .end local v2           #lockTimerState:I
    :cond_f6
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@f8
    goto :goto_c0

    #@f9
    .line 165
    :sswitch_f9
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@fb
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@fe
    move-result v10

    #@ff
    if-eqz v10, :cond_114

    #@101
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Password:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@103
    .line 168
    :goto_103
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->isSupportForgotPinPassword()Z

    #@106
    move-result v10

    #@107
    if-eqz v10, :cond_d0

    #@109
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@10b
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@10e
    move-result v10

    #@10f
    if-eqz v10, :cond_d0

    #@111
    .line 169
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@113
    goto :goto_d0

    #@114
    .line 165
    :cond_114
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->None:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@116
    goto :goto_103

    #@117
    .line 176
    :sswitch_117
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@119
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@11c
    move-result v10

    #@11d
    if-eqz v10, :cond_d0

    #@11f
    .line 177
    iget-object v10, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@121
    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@124
    move-result v10

    #@125
    if-eqz v10, :cond_12a

    #@127
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Account:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@129
    :goto_129
    goto :goto_d0

    #@12a
    :cond_12a
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;->Pattern:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel$SecurityMode;

    #@12c
    goto :goto_129

    #@12d
    .line 152
    nop

    #@12e
    :sswitch_data_12e
    .sparse-switch
        0x0 -> :sswitch_117
        0x10000 -> :sswitch_117
        0x20000 -> :sswitch_b6
        0x40000 -> :sswitch_f9
        0x50000 -> :sswitch_f9
        0x60000 -> :sswitch_f9
    .end sparse-switch
.end method

.method isBiometricUnlockEnabled()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 72
    const-string v1, "SPR"

    #@3
    sget-object v2, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->carrier:Ljava/lang/String;

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_14

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isOMADM()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_14

    #@13
    .line 76
    :cond_13
    :goto_13
    return v0

    #@14
    :cond_14
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@16
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_13

    #@1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1e
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakInstalled()Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_13

    #@24
    const/4 v0, 0x1

    #@25
    goto :goto_13
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 2
    .parameter "utils"

    #@0
    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityModel;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    .line 65
    return-void
.end method
