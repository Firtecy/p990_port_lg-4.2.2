.class Lcom/android/internal/policy/impl/keyguard/PagedView$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->zoomOut()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2084
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 5
    .parameter "animation"

    #@0
    .prologue
    .line 2088
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_34

    #@8
    .line 2089
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@a
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@12
    .line 2090
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@14
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$500(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    #@1b
    move-result-object v0

    #@1c
    const/high16 v1, 0x3f80

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    #@21
    move-result-object v0

    #@22
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$5;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@24
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$600(Lcom/android/internal/policy/impl/keyguard/PagedView;)J

    #@27
    move-result-wide v1

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    #@2b
    move-result-object v0

    #@2c
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/PagedView$5$1;

    #@2e
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/PagedView$5$1;-><init>(Lcom/android/internal/policy/impl/keyguard/PagedView$5;)V

    #@31
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    #@34
    .line 2099
    :cond_34
    return-void
.end method
