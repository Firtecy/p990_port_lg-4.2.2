.class public Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;
.source "KeyguardPasswordView.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityView;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;


# instance fields
.field mImm:Landroid/view/inputmethod/InputMethodManager;

.field private final mShowImeAtScreenOn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 52
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v0

    #@7
    const v1, 0x1110003

    #@a
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@d
    move-result v0

    #@e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mShowImeAtScreenOn:Z

    #@10
    .line 59
    return-void
.end method

.method private hasMultipleEnabledIMEsOrSubtypes(Landroid/view/inputmethod/InputMethodManager;Z)Z
    .registers 15
    .parameter "imm"
    .parameter "shouldIncludeAuxiliarySubtypes"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 161
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    #@5
    move-result-object v1

    #@6
    .line 164
    .local v1, enabledImis:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    const/4 v2, 0x0

    #@7
    .line 166
    .local v2, filteredImisCount:I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v3

    #@b
    :cond_b
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v11

    #@f
    if-eqz v11, :cond_50

    #@11
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Landroid/view/inputmethod/InputMethodInfo;

    #@17
    .line 168
    .local v5, imi:Landroid/view/inputmethod/InputMethodInfo;
    if-le v2, v10, :cond_1a

    #@19
    .line 194
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    :goto_19
    return v10

    #@1a
    .line 169
    .restart local v5       #imi:Landroid/view/inputmethod/InputMethodInfo;
    :cond_1a
    invoke-virtual {p1, v5, v10}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@1d
    move-result-object v8

    #@1e
    .line 172
    .local v8, subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    #@21
    move-result v11

    #@22
    if-eqz v11, :cond_27

    #@24
    .line 173
    add-int/lit8 v2, v2, 0x1

    #@26
    .line 174
    goto :goto_b

    #@27
    .line 177
    :cond_27
    const/4 v0, 0x0

    #@28
    .line 178
    .local v0, auxCount:I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v4

    #@2c
    .local v4, i$:Ljava/util/Iterator;
    :cond_2c
    :goto_2c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v11

    #@30
    if-eqz v11, :cond_41

    #@32
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v7

    #@36
    check-cast v7, Landroid/view/inputmethod/InputMethodSubtype;

    #@38
    .line 179
    .local v7, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@3b
    move-result v11

    #@3c
    if-eqz v11, :cond_2c

    #@3e
    .line 180
    add-int/lit8 v0, v0, 0x1

    #@40
    goto :goto_2c

    #@41
    .line 183
    .end local v7           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_41
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@44
    move-result v11

    #@45
    sub-int v6, v11, v0

    #@47
    .line 188
    .local v6, nonAuxCount:I
    if-gtz v6, :cond_4d

    #@49
    if-eqz p2, :cond_b

    #@4b
    if-le v0, v10, :cond_b

    #@4d
    .line 189
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    .line 190
    goto :goto_b

    #@50
    .line 194
    .end local v0           #auxCount:I
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v5           #imi:Landroid/view/inputmethod/InputMethodInfo;
    .end local v6           #nonAuxCount:I
    .end local v8           #subtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_50
    if-gt v2, v10, :cond_5d

    #@52
    const/4 v11, 0x0

    #@53
    invoke-virtual {p1, v11, v9}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@56
    move-result-object v11

    #@57
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@5a
    move-result v11

    #@5b
    if-le v11, v10, :cond_5e

    #@5d
    :cond_5d
    move v9, v10

    #@5e
    :cond_5e
    move v10, v9

    #@5f
    goto :goto_19
.end method


# virtual methods
.method protected getPasswordTextViewId()I
    .registers 2

    #@0
    .prologue
    .line 68
    const v0, 0x10202ce

    #@3
    return v0
.end method

.method public getWrongPasswordStringId()I
    .registers 2

    #@0
    .prologue
    .line 206
    const v0, 0x104054d

    #@3
    return v0
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 73
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onFinishInflate()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 93
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onFinishInflate()V

    #@4
    .line 95
    const/4 v0, 0x0

    #@5
    .line 97
    .local v0, imeOrDeleteButtonVisible:Z
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->getContext()Landroid/content/Context;

    #@8
    move-result-object v4

    #@9
    const-string v5, "input_method"

    #@b
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    #@11
    iput-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@13
    .line 100
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@15
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@1c
    .line 101
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@1e
    const/16 v5, 0x81

    #@20
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setInputType(I)V

    #@23
    .line 105
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@25
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$1;

    #@27
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;)V

    #@2a
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2d
    .line 111
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@2f
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$2;

    #@31
    invoke-direct {v5, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;)V

    #@34
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@37
    .line 125
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@39
    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    #@3c
    .line 128
    const v4, 0x10202cf

    #@3f
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->findViewById(I)Landroid/view/View;

    #@42
    move-result-object v3

    #@43
    .line 129
    .local v3, switchImeButton:Landroid/view/View;
    if-eqz v3, :cond_59

    #@45
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@47
    invoke-direct {p0, v4, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->hasMultipleEnabledIMEsOrSubtypes(Landroid/view/inputmethod/InputMethodManager;Z)Z

    #@4a
    move-result v4

    #@4b
    if-eqz v4, :cond_59

    #@4d
    .line 130
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    #@50
    .line 131
    const/4 v0, 0x1

    #@51
    .line 132
    new-instance v4, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$3;

    #@53
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;)V

    #@56
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@59
    .line 142
    :cond_59
    if-nez v0, :cond_70

    #@5b
    .line 143
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@5d
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@60
    move-result-object v2

    #@61
    .line 144
    .local v2, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    #@63
    if-eqz v4, :cond_70

    #@65
    move-object v1, v2

    #@66
    .line 145
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    #@68
    .line 146
    .local v1, mlp:Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    #@6b
    .line 147
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@6d
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@70
    .line 150
    .end local v1           #mlp:Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2           #params:Landroid/view/ViewGroup$LayoutParams;
    :cond_70
    return-void
.end method

.method public onPause()V
    .registers 4

    #@0
    .prologue
    .line 87
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onPause()V

    #@3
    .line 88
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->getWindowToken()Landroid/os/IBinder;

    #@8
    move-result-object v1

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@d
    .line 89
    return-void
.end method

.method public onResume(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 78
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->onResume(I)V

    #@4
    .line 79
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    #@9
    .line 80
    if-ne p1, v2, :cond_f

    #@b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mShowImeAtScreenOn:Z

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 81
    :cond_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardPasswordView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@11
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@16
    .line 83
    :cond_16
    return-void
.end method

.method protected resetState()V
    .registers 4

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mSecurityMessageDisplay:Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;

    #@2
    const v1, 0x1040553

    #@5
    const/4 v2, 0x0

    #@6
    invoke-interface {v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/SecurityMessageDisplay;->setMessage(IZ)V

    #@9
    .line 63
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardAbsKeyInputView;->mPasswordEntry:Landroid/widget/TextView;

    #@b
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@f
    .line 64
    return-void
.end method

.method public showUsabilityHint()V
    .registers 1

    #@0
    .prologue
    .line 202
    return-void
.end method
