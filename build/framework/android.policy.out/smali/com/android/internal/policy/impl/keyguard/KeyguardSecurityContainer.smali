.class public Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;
.super Landroid/widget/FrameLayout;
.source "KeyguardSecurityContainer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 16
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v1, v1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 12
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 21
    return-void
.end method


# virtual methods
.method getFlipper()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;
    .registers 4

    #@0
    .prologue
    .line 24
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getChildCount()I

    #@4
    move-result v2

    #@5
    if-ge v1, v2, :cond_15

    #@7
    .line 25
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 26
    .local v0, child:Landroid/view/View;
    instance-of v2, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@d
    if-eqz v2, :cond_12

    #@f
    .line 27
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@11
    .line 30
    .end local v0           #child:Landroid/view/View;
    :goto_11
    return-object v0

    #@12
    .line 24
    .restart local v0       #child:Landroid/view/View;
    :cond_12
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_1

    #@15
    .line 30
    .end local v0           #child:Landroid/view/View;
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_11
.end method

.method public hideBouncer(I)V
    .registers 3
    .parameter "duration"

    #@0
    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getFlipper()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@3
    move-result-object v0

    #@4
    .line 42
    .local v0, flipper:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;
    if-eqz v0, :cond_9

    #@6
    .line 43
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->hideBouncer(I)V

    #@9
    .line 45
    :cond_9
    return-void
.end method

.method public showBouncer(I)V
    .registers 3
    .parameter "duration"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityContainer;->getFlipper()Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;

    #@3
    move-result-object v0

    #@4
    .line 35
    .local v0, flipper:Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;
    if-eqz v0, :cond_9

    #@6
    .line 36
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardSecurityViewFlipper;->showBouncer(I)V

    #@9
    .line 38
    :cond_9
    return-void
.end method
