.class Lcom/android/internal/policy/impl/PhoneWindowManager$25;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->takeScreenshot()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 6059
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 10
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 6062
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    iget-object v5, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotLock:Ljava/lang/Object;

    #@4
    monitor-enter v5

    #@5
    .line 6063
    :try_start_5
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@7
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@9
    if-eq v4, p0, :cond_d

    #@b
    .line 6064
    monitor-exit v5

    #@c
    .line 6092
    :goto_c
    return-void

    #@d
    .line 6066
    :cond_d
    new-instance v1, Landroid/os/Messenger;

    #@f
    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    #@12
    .line 6067
    .local v1, messenger:Landroid/os/Messenger;
    const/4 v4, 0x0

    #@13
    const/4 v6, 0x1

    #@14
    invoke-static {v4, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@17
    move-result-object v2

    #@18
    .line 6068
    .local v2, msg:Landroid/os/Message;
    move-object v3, p0

    #@19
    .line 6069
    .local v3, myConn:Landroid/content/ServiceConnection;
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;

    #@1b
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@1d
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@1f
    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@22
    move-result-object v4

    #@23
    invoke-direct {v0, p0, v4, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$25$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager$25;Landroid/os/Looper;Landroid/content/ServiceConnection;)V

    #@26
    .line 6081
    .local v0, h:Landroid/os/Handler;
    new-instance v4, Landroid/os/Messenger;

    #@28
    invoke-direct {v4, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@2b
    iput-object v4, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2d
    .line 6082
    const/4 v4, 0x0

    #@2e
    iput v4, v2, Landroid/os/Message;->arg2:I

    #@30
    iput v4, v2, Landroid/os/Message;->arg1:I

    #@32
    .line 6083
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@34
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@36
    if-eqz v4, :cond_45

    #@38
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3a
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@3c
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_45

    #@42
    .line 6084
    const/4 v4, 0x1

    #@43
    iput v4, v2, Landroid/os/Message;->arg1:I

    #@45
    .line 6085
    :cond_45
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@47
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@49
    if-eqz v4, :cond_58

    #@4b
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$25;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4d
    iget-object v4, v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@4f
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@52
    move-result v4

    #@53
    if-eqz v4, :cond_58

    #@55
    .line 6086
    const/4 v4, 0x1

    #@56
    iput v4, v2, Landroid/os/Message;->arg2:I
    :try_end_58
    .catchall {:try_start_5 .. :try_end_58} :catchall_5d

    #@58
    .line 6088
    :cond_58
    :try_start_58
    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_5b
    .catchall {:try_start_58 .. :try_end_5b} :catchall_5d
    .catch Landroid/os/RemoteException; {:try_start_58 .. :try_end_5b} :catch_60

    #@5b
    .line 6091
    :goto_5b
    :try_start_5b
    monitor-exit v5

    #@5c
    goto :goto_c

    #@5d
    .end local v0           #h:Landroid/os/Handler;
    .end local v1           #messenger:Landroid/os/Messenger;
    .end local v2           #msg:Landroid/os/Message;
    .end local v3           #myConn:Landroid/content/ServiceConnection;
    :catchall_5d
    move-exception v4

    #@5e
    monitor-exit v5
    :try_end_5f
    .catchall {:try_start_5b .. :try_end_5f} :catchall_5d

    #@5f
    throw v4

    #@60
    .line 6089
    .restart local v0       #h:Landroid/os/Handler;
    .restart local v1       #messenger:Landroid/os/Messenger;
    .restart local v2       #msg:Landroid/os/Message;
    .restart local v3       #myConn:Landroid/content/ServiceConnection;
    :catch_60
    move-exception v4

    #@61
    goto :goto_5b
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 6094
    return-void
.end method
