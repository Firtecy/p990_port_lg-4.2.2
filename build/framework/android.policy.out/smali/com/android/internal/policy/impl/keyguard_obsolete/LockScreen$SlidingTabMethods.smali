.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;
.super Ljava/lang/Object;
.source "LockScreen.java"

# interfaces
.implements Lcom/android/internal/widget/SlidingTab$OnTriggerListener;
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$UnlockWidgetCommonMethods;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SlidingTabMethods"
.end annotation


# instance fields
.field private final mSlidingTab:Lcom/android/internal/widget/SlidingTab;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Lcom/android/internal/widget/SlidingTab;)V
    .registers 3
    .parameter
    .parameter "slidingTab"

    #@0
    .prologue
    .line 137
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 138
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@7
    .line 139
    return-void
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/SlidingTab;->setOnTriggerListener(Lcom/android/internal/widget/SlidingTab$OnTriggerListener;)V

    #@6
    .line 203
    return-void
.end method

.method public getTargetPosition(I)I
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 198
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@2
    return-object v0
.end method

.method public onGrabbedStateChange(Landroid/view/View;I)V
    .registers 5
    .parameter "v"
    .parameter "grabbedState"

    #@0
    .prologue
    .line 169
    const/4 v0, 0x2

    #@1
    if-ne p2, v0, :cond_1e

    #@3
    .line 170
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@5
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@7
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@a
    move-result v1

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$002(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;Z)Z

    #@e
    .line 171
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@12
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_2a

    #@18
    const v0, 0x1040340

    #@1b
    :goto_1b
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/SlidingTab;->setRightHintText(I)V

    #@1e
    .line 177
    :cond_1e
    if-eqz p2, :cond_29

    #@20
    .line 178
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@22
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@25
    move-result-object v0

    #@26
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@29
    .line 180
    :cond_29
    return-void

    #@2a
    .line 171
    :cond_2a
    const v0, 0x1040341

    #@2d
    goto :goto_1b
.end method

.method public onTrigger(Landroid/view/View;I)V
    .registers 4
    .parameter "v"
    .parameter "whichHandle"

    #@0
    .prologue
    .line 159
    const/4 v0, 0x1

    #@1
    if-ne p2, v0, :cond_d

    #@3
    .line 160
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToUnlockScreen()V

    #@c
    .line 165
    :cond_c
    :goto_c
    return-void

    #@d
    .line 161
    :cond_d
    const/4 v0, 0x2

    #@e
    if-ne p2, v0, :cond_c

    #@10
    .line 162
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@12
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)V

    #@15
    .line 163
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@17
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1a
    move-result-object v0

    #@1b
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@1e
    goto :goto_c
.end method

.method public ping()V
    .registers 1

    #@0
    .prologue
    .line 191
    return-void
.end method

.method public reset(Z)V
    .registers 3
    .parameter "animate"

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab;->reset(Z)V

    #@5
    .line 188
    return-void
.end method

.method public setEnabled(IZ)V
    .registers 3
    .parameter "resourceId"
    .parameter "enabled"

    #@0
    .prologue
    .line 195
    return-void
.end method

.method public updateResources()V
    .registers 7

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 142
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@3
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_49

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@b
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Landroid/media/AudioManager;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    #@12
    move-result v1

    #@13
    if-ne v1, v0, :cond_49

    #@15
    .line 145
    .local v0, vibe:Z
    :goto_15
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->mSlidingTab:Lcom/android/internal/widget/SlidingTab;

    #@17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@19
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_4f

    #@1f
    if-eqz v0, :cond_4b

    #@21
    const v1, 0x10802de

    #@24
    :goto_24
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@26
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_53

    #@2c
    const v2, 0x10803b7

    #@2f
    :goto_2f
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@31
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@34
    move-result v3

    #@35
    if-eqz v3, :cond_57

    #@37
    const v3, 0x10803a0

    #@3a
    :goto_3a
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen$SlidingTabMethods;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@3c
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_5b

    #@42
    const v4, 0x10803b3

    #@45
    :goto_45
    invoke-virtual {v5, v1, v2, v3, v4}, Lcom/android/internal/widget/SlidingTab;->setRightTabResources(IIII)V

    #@48
    .line 155
    return-void

    #@49
    .line 142
    .end local v0           #vibe:Z
    :cond_49
    const/4 v0, 0x0

    #@4a
    goto :goto_15

    #@4b
    .line 145
    .restart local v0       #vibe:Z
    :cond_4b
    const v1, 0x10802db

    #@4e
    goto :goto_24

    #@4f
    :cond_4f
    const v1, 0x10802dc

    #@52
    goto :goto_24

    #@53
    :cond_53
    const v2, 0x10803b4

    #@56
    goto :goto_2f

    #@57
    :cond_57
    const v3, 0x108039f

    #@5a
    goto :goto_3a

    #@5b
    :cond_5b
    const v4, 0x10803b2

    #@5e
    goto :goto_45
.end method
