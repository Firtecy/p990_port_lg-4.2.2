.class Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;
.super Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;
.source "KeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 256
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClockVisibilityChanged()V
    .registers 2

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@5
    .line 292
    return-void
.end method

.method public onDeviceProvisioned()V
    .registers 4

    #@0
    .prologue
    .line 296
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@8
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Landroid/content/Intent;

    #@b
    move-result-object v1

    #@c
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@11
    .line 297
    return-void
.end method

.method onPhoneStateChanged(I)V
    .registers 4
    .parameter "phoneState"

    #@0
    .prologue
    .line 273
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    monitor-enter v1

    #@3
    .line 274
    if-nez p1, :cond_1a

    #@5
    :try_start_5
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@7
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_1a

    #@d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 284
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@17
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@1a
    .line 286
    :cond_1a
    monitor-exit v1

    #@1b
    .line 287
    return-void

    #@1c
    .line 286
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_5 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 4
    .parameter "simState"

    #@0
    .prologue
    .line 303
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$4;->$SwitchMap$com$android$internal$telephony$IccCardConstants$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_76

    #@b
    .line 354
    :goto_b
    return-void

    #@c
    .line 308
    :pswitch_c
    monitor-enter p0

    #@d
    .line 309
    :try_start_d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_26

    #@19
    .line 310
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isShowing()Z

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_2b

    #@21
    .line 314
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@23
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@26
    .line 319
    :cond_26
    :goto_26
    monitor-exit p0

    #@27
    goto :goto_b

    #@28
    :catchall_28
    move-exception v0

    #@29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_d .. :try_end_2a} :catchall_28

    #@2a
    throw v0

    #@2b
    .line 316
    :cond_2b
    :try_start_2b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2d
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    :try_end_30
    .catchall {:try_start_2b .. :try_end_30} :catchall_28

    #@30
    goto :goto_26

    #@31
    .line 323
    :pswitch_31
    monitor-enter p0

    #@32
    .line 324
    :try_start_32
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@34
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isShowing()Z

    #@37
    move-result v0

    #@38
    if-nez v0, :cond_44

    #@3a
    .line 327
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@3c
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@3f
    .line 331
    :goto_3f
    monitor-exit p0

    #@40
    goto :goto_b

    #@41
    :catchall_41
    move-exception v0

    #@42
    monitor-exit p0
    :try_end_43
    .catchall {:try_start_32 .. :try_end_43} :catchall_41

    #@43
    throw v0

    #@44
    .line 329
    :cond_44
    :try_start_44
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@46
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_41

    #@49
    goto :goto_3f

    #@4a
    .line 334
    :pswitch_4a
    monitor-enter p0

    #@4b
    .line 335
    :try_start_4b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@4d
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isShowing()Z

    #@50
    move-result v0

    #@51
    if-nez v0, :cond_5d

    #@53
    .line 338
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@55
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@58
    .line 344
    :goto_58
    monitor-exit p0

    #@59
    goto :goto_b

    #@5a
    :catchall_5a
    move-exception v0

    #@5b
    monitor-exit p0
    :try_end_5c
    .catchall {:try_start_4b .. :try_end_5c} :catchall_5a

    #@5c
    throw v0

    #@5d
    .line 342
    :cond_5d
    :try_start_5d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@5f
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V
    :try_end_62
    .catchall {:try_start_5d .. :try_end_62} :catchall_5a

    #@62
    goto :goto_58

    #@63
    .line 347
    :pswitch_63
    monitor-enter p0

    #@64
    .line 348
    :try_start_64
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@66
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->isShowing()Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_71

    #@6c
    .line 349
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@6e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@71
    .line 351
    :cond_71
    monitor-exit p0

    #@72
    goto :goto_b

    #@73
    :catchall_73
    move-exception v0

    #@74
    monitor-exit p0
    :try_end_75
    .catchall {:try_start_64 .. :try_end_75} :catchall_73

    #@75
    throw v0

    #@76
    .line 303
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_c
        :pswitch_c
        :pswitch_31
        :pswitch_31
        :pswitch_4a
        :pswitch_63
    .end packed-switch
.end method

.method public onUserRemoved(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->removeUser(I)V

    #@9
    .line 269
    return-void
.end method

.method public onUserSwitched(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)Lcom/android/internal/widget/LockPatternUtils;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setCurrentUser(I)V

    #@9
    .line 261
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@b
    monitor-enter v1

    #@c
    .line 262
    :try_start_c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator$1;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;

    #@e
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewMediator;)V

    #@11
    .line 263
    monitor-exit v1

    #@12
    .line 264
    return-void

    #@13
    .line 263
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_c .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method
