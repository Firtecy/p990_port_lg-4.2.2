.class Lcom/android/internal/policy/impl/keyguard/CarrierText$1;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;
.source "CarrierText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/CarrierText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPlmn:Ljava/lang/CharSequence;

.field private mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

.field private mSpn:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/CarrierText;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/CarrierText;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/CarrierText;

    #@2
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "plmn"
    .parameter "spn"

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mPlmn:Ljava/lang/CharSequence;

    #@2
    .line 47
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSpn:Ljava/lang/CharSequence;

    #@4
    .line 48
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/CarrierText;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mPlmn:Ljava/lang/CharSequence;

    #@a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSpn:Ljava/lang/CharSequence;

    #@c
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->updateCarrierText(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@f
    .line 49
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 6
    .parameter "simState"

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    .line 54
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->this$0:Lcom/android/internal/policy/impl/keyguard/CarrierText;

    #@4
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mPlmn:Ljava/lang/CharSequence;

    #@8
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/CarrierText$1;->mSpn:Ljava/lang/CharSequence;

    #@a
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/CarrierText;->updateCarrierText(Lcom/android/internal/telephony/IccCardConstants$State;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@d
    .line 55
    return-void
.end method
