.class Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "LockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FastBitmapDrawable"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mOpacity:I


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "bitmap"

    #@0
    .prologue
    .line 1156
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 1157
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@5
    .line 1158
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_11

    #@d
    const/4 v0, -0x3

    #@e
    :goto_e
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mOpacity:I

    #@10
    .line 1159
    return-void

    #@11
    .line 1158
    :cond_11
    const/4 v0, -0x1

    #@12
    goto :goto_e
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 1163
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    #@9
    move-result v1

    #@a
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@c
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    #@f
    move-result v2

    #@10
    sub-int/2addr v1, v2

    #@11
    div-int/lit8 v1, v1, 0x2

    #@13
    int-to-float v1, v1

    #@14
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->getBounds()Landroid/graphics/Rect;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    #@1b
    move-result v2

    #@1c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@1e
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    #@21
    move-result v3

    #@22
    sub-int/2addr v2, v3

    #@23
    int-to-float v2, v2

    #@24
    const/4 v3, 0x0

    #@25
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@28
    .line 1168
    return-void
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 1190
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 1185
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 1200
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 1195
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 1172
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;->mOpacity:I

    #@2
    return v0
.end method

.method public setAlpha(I)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 1177
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter "cf"

    #@0
    .prologue
    .line 1181
    return-void
.end method
