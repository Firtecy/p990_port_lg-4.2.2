.class public Lcom/android/internal/policy/impl/RecentApplicationsBackground;
.super Landroid/widget/LinearLayout;
.source "RecentApplicationsBackground.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RecentApplicationsBackground"


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBackgroundSizeChanged:Z

.field private mTmp0:Landroid/graphics/Rect;

.field private mTmp1:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 45
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->init()V

    #@7
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 40
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mTmp0:Landroid/graphics/Rect;

    #@a
    .line 41
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mTmp1:Landroid/graphics/Rect;

    #@11
    .line 50
    invoke-direct {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->init()V

    #@14
    .line 51
    return-void
.end method

.method private getChildBounds(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "r"

    #@0
    .prologue
    .line 145
    const v3, 0x7fffffff

    #@3
    iput v3, p1, Landroid/graphics/Rect;->top:I

    #@5
    iput v3, p1, Landroid/graphics/Rect;->left:I

    #@7
    .line 146
    const/high16 v3, -0x8000

    #@9
    iput v3, p1, Landroid/graphics/Rect;->right:I

    #@b
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    #@d
    .line 147
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getChildCount()I

    #@10
    move-result v0

    #@11
    .line 148
    .local v0, N:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_51

    #@14
    .line 149
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getChildAt(I)Landroid/view/View;

    #@17
    move-result-object v2

    #@18
    .line 150
    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_4e

    #@1e
    .line 151
    iget v3, p1, Landroid/graphics/Rect;->left:I

    #@20
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    #@23
    move-result v4

    #@24
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@27
    move-result v3

    #@28
    iput v3, p1, Landroid/graphics/Rect;->left:I

    #@2a
    .line 152
    iget v3, p1, Landroid/graphics/Rect;->top:I

    #@2c
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@2f
    move-result v4

    #@30
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@33
    move-result v3

    #@34
    iput v3, p1, Landroid/graphics/Rect;->top:I

    #@36
    .line 153
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@38
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    #@3b
    move-result v4

    #@3c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@3f
    move-result v3

    #@40
    iput v3, p1, Landroid/graphics/Rect;->right:I

    #@42
    .line 154
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@44
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    #@47
    move-result v4

    #@48
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@4b
    move-result v3

    #@4c
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    #@4e
    .line 148
    :cond_4e
    add-int/lit8 v1, v1, 0x1

    #@50
    goto :goto_12

    #@51
    .line 157
    .end local v2           #v:Landroid/view/View;
    :cond_51
    return-void
.end method

.method private init()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 54
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getBackground()Landroid/graphics/drawable/Drawable;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@7
    .line 55
    const/4 v0, 0x0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 56
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->setPadding(IIII)V

    #@e
    .line 57
    const/16 v0, 0x11

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->setGravity(I)V

    #@13
    .line 58
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 91
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@3
    .line 92
    .local v0, background:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_2b

    #@5
    .line 93
    iget-boolean v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackgroundSizeChanged:Z

    #@7
    if-eqz v7, :cond_2b

    #@9
    .line 94
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackgroundSizeChanged:Z

    #@b
    .line 95
    iget-object v3, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mTmp0:Landroid/graphics/Rect;

    #@d
    .line 96
    .local v3, chld:Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mTmp1:Landroid/graphics/Rect;

    #@f
    .line 97
    .local v1, bkg:Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v7, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@14
    .line 98
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getChildBounds(Landroid/graphics/Rect;)V

    #@17
    .line 101
    iget v7, v3, Landroid/graphics/Rect;->top:I

    #@19
    iget v8, v1, Landroid/graphics/Rect;->top:I

    #@1b
    sub-int v6, v7, v8

    #@1d
    .line 102
    .local v6, top:I
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    #@1f
    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    #@21
    add-int v2, v7, v8

    #@23
    .line 113
    .local v2, bottom:I
    const/4 v4, 0x0

    #@24
    .line 114
    .local v4, left:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getRight()I

    #@27
    move-result v5

    #@28
    .line 116
    .local v5, right:I
    invoke-virtual {v0, v4, v6, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2b
    .line 119
    .end local v1           #bkg:Landroid/graphics/Rect;
    .end local v2           #bottom:I
    .end local v3           #chld:Landroid/graphics/Rect;
    .end local v4           #left:I
    .end local v5           #right:I
    .end local v6           #top:I
    :cond_2b
    iget-object v7, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2d
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@30
    .line 126
    const/16 v7, 0xbf

    #@32
    invoke-virtual {p1, v7, v9, v9, v9}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    #@35
    .line 128
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    #@38
    .line 129
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    .line 83
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_11

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 84
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->getDrawableState()[I

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@11
    .line 86
    :cond_11
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    #@14
    .line 87
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/LinearLayout;->jumpDrawablesToCurrentState()V

    #@3
    .line 77
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 78
    :cond_c
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 133
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    #@3
    .line 134
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@8
    .line 135
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->setWillNotDraw(Z)V

    #@c
    .line 136
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 140
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    #@3
    .line 141
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@9
    .line 142
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->setWillNotDraw(Z)V

    #@4
    .line 63
    iget v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mLeft:I

    #@6
    if-ne v0, p1, :cond_14

    #@8
    iget v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mRight:I

    #@a
    if-ne v0, p3, :cond_14

    #@c
    iget v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mTop:I

    #@e
    if-ne v0, p2, :cond_14

    #@10
    iget v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBottom:I

    #@12
    if-eq v0, p4, :cond_17

    #@14
    .line 64
    :cond_14
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackgroundSizeChanged:Z

    #@17
    .line 66
    :cond_17
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->setFrame(IIII)Z

    #@1a
    move-result v0

    #@1b
    return v0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/RecentApplicationsBackground;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq p1, v0, :cond_a

    #@4
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
