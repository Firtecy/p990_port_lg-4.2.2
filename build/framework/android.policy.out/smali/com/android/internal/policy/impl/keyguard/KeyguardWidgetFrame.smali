.class public Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;
.super Landroid/widget/FrameLayout;
.source "KeyguardWidgetFrame.java"


# static fields
.field static final ENABLE_HOVER_OVER_DELETE_DROP_TARGET_OVERLAY:Z = true

.field static final HOVER_OVER_DELETE_DROP_TARGET_OVERLAY_COLOR:I = -0x66010000

.field static final OUTLINE_ALPHA_MULTIPLIER:F = 0.6f

.field private static final sAddBlendMode:Landroid/graphics/PorterDuffXfermode;


# instance fields
.field private isCustomWidget:Z

.field private mBackgroundAlpha:F

.field private mBackgroundAlphaMultiplier:F

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mBackgroundRect:Landroid/graphics/Rect;

.field private mBgAlphaController:Ljava/lang/Object;

.field private mContentAlpha:F

.field private mForegroundAlpha:I

.field private mForegroundGradient:Landroid/graphics/LinearGradient;

.field private final mForegroundRect:Landroid/graphics/Rect;

.field private mFrameFade:Landroid/animation/Animator;

.field private mFrameHeight:I

.field private mFrameStrokeAdjustment:I

.field private mGradientColor:I

.field private mGradientPaint:Landroid/graphics/Paint;

.field private mIsHoveringOverDeleteDropTarget:Z

.field private mIsSmall:Z

.field mLeftToRight:Z

.field private mLeftToRightGradient:Landroid/graphics/LinearGradient;

.field private mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

.field private mMaxChallengeTop:I

.field private mOverScrollAmount:F

.field private mPerformAppWidgetSizeUpdateOnBootComplete:Z

.field private mRightToLeftGradient:Landroid/graphics/LinearGradient;

.field private mSmallFrameHeight:I

.field private mSmallWidgetHeight:I

.field private mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field private mWidgetLockedSmall:Z

.field private mWorkerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 43
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    #@2
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    #@4
    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@7
    sput-object v0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->sAddBlendMode:Landroid/graphics/PorterDuffXfermode;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 97
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 101
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 56
    new-instance v3, Landroid/graphics/Paint;

    #@6
    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    #@9
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientPaint:Landroid/graphics/Paint;

    #@b
    .line 57
    const/4 v3, 0x1

    #@c
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLeftToRight:Z

    #@e
    .line 59
    const/4 v3, 0x0

    #@f
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mOverScrollAmount:F

    #@11
    .line 60
    new-instance v3, Landroid/graphics/Rect;

    #@13
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@16
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@18
    .line 61
    iput v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundAlpha:I

    #@1a
    .line 64
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@1c
    .line 69
    const/high16 v3, 0x3f80

    #@1e
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlphaMultiplier:F

    #@20
    .line 71
    new-instance v3, Landroid/graphics/Rect;

    #@22
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@25
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundRect:Landroid/graphics/Rect;

    #@27
    .line 77
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWidgetLockedSmall:Z

    #@29
    .line 78
    const/4 v3, -0x1

    #@2a
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mMaxChallengeTop:I

    #@2c
    .line 137
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame$1;

    #@2e
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)V

    #@31
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@33
    .line 529
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->isCustomWidget:Z

    #@35
    .line 103
    new-instance v3, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@37
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;-><init>(Landroid/view/View;)V

    #@3a
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@3c
    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3f
    move-result-object v2

    #@40
    .line 108
    .local v2, res:Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@43
    move-result-object v3

    #@44
    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    #@46
    .line 109
    .local v0, density:F
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@49
    move-result-object v3

    #@4a
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    #@4c
    const/high16 v4, 0x4100

    #@4e
    mul-float/2addr v3, v4

    #@4f
    float-to-int v1, v3

    #@50
    .line 110
    .local v1, padding:I
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setPadding(IIII)V

    #@53
    .line 112
    const/high16 v3, 0x4000

    #@55
    mul-float/2addr v3, v0

    #@56
    float-to-int v3, v3

    #@57
    add-int/lit8 v3, v3, 0x2

    #@59
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@5b
    .line 116
    const v3, 0x1050078

    #@5e
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@61
    move-result v3

    #@62
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@64
    .line 118
    const v3, 0x10803ca

    #@67
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@6a
    move-result-object v3

    #@6b
    iput-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@6d
    .line 119
    const v3, 0x1060048

    #@70
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    #@73
    move-result v3

    #@74
    iput v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientColor:I

    #@76
    .line 120
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientPaint:Landroid/graphics/Paint;

    #@78
    sget-object v4, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->sAddBlendMode:Landroid/graphics/PorterDuffXfermode;

    #@7a
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@7d
    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mPerformAppWidgetSizeUpdateOnBootComplete:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mPerformAppWidgetSizeUpdateOnBootComplete:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->performAppWidgetSizeCallbacksIfNecessary()V

    #@3
    return-void
.end method

.method private drawGradientOverlay(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientPaint:Landroid/graphics/Paint;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundGradient:Landroid/graphics/LinearGradient;

    #@4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@7
    .line 214
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientPaint:Landroid/graphics/Paint;

    #@9
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundAlpha:I

    #@b
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    #@e
    .line 215
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientPaint:Landroid/graphics/Paint;

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@15
    .line 216
    return-void
.end method

.method private drawHoveringOverDeleteOverlay(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsHoveringOverDeleteDropTarget:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 220
    const/high16 v0, -0x6601

    #@6
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    #@9
    .line 222
    :cond_9
    return-void
.end method

.method private performAppWidgetSizeCallbacksIfNecessary()V
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 474
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@4
    move-result-object v7

    #@5
    .line 475
    .local v7, content:Landroid/view/View;
    instance-of v1, v7, Landroid/appwidget/AppWidgetHostView;

    #@7
    if-nez v1, :cond_a

    #@9
    .line 491
    :goto_9
    return-void

    #@a
    .line 477
    :cond_a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->hasBootCompleted()Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_19

    #@16
    .line 478
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mPerformAppWidgetSizeUpdateOnBootComplete:Z

    #@18
    goto :goto_9

    #@19
    :cond_19
    move-object v0, v7

    #@1a
    .line 485
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@1c
    .line 486
    .local v0, awhv:Landroid/appwidget/AppWidgetHostView;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@23
    move-result-object v1

    #@24
    iget v8, v1, Landroid/util/DisplayMetrics;->density:F

    #@26
    .line 488
    .local v8, density:F
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    #@29
    move-result v1

    #@2a
    int-to-float v1, v1

    #@2b
    div-float/2addr v1, v8

    #@2c
    float-to-int v2, v1

    #@2d
    .line 489
    .local v2, width:I
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    #@30
    move-result v1

    #@31
    int-to-float v1, v1

    #@32
    div-float/2addr v1, v8

    #@33
    float-to-int v3, v1

    #@34
    .line 490
    .local v3, height:I
    const/4 v1, 0x0

    #@35
    move v4, v2

    #@36
    move v5, v3

    #@37
    invoke-virtual/range {v0 .. v6}, Landroid/appwidget/AppWidgetHostView;->updateAppWidgetSize(Landroid/os/Bundle;IIIIZ)V

    #@3a
    goto :goto_9
.end method

.method private setWidgetHeight(I)V
    .registers 6
    .parameter "height"

    #@0
    .prologue
    .line 335
    const/4 v1, 0x0

    #@1
    .line 336
    .local v1, needLayout:Z
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@4
    move-result-object v2

    #@5
    .line 337
    .local v2, widget:Landroid/view/View;
    if-eqz v2, :cond_14

    #@7
    .line 338
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    #@d
    .line 339
    .local v0, lp:Landroid/widget/FrameLayout$LayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@f
    if-eq v3, p1, :cond_14

    #@11
    .line 340
    const/4 v1, 0x1

    #@12
    .line 341
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@14
    .line 344
    .end local v0           #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_14
    if-eqz v1, :cond_19

    #@16
    .line 345
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->requestLayout()V

    #@19
    .line 347
    :cond_19
    return-void
.end method

.method private updateGradient()V
    .registers 16

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 442
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLeftToRight:Z

    #@4
    if-eqz v0, :cond_2f

    #@6
    move v1, v2

    #@7
    .line 443
    .local v1, x0:F
    :goto_7
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLeftToRight:Z

    #@9
    if-eqz v0, :cond_37

    #@b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@d
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@10
    move-result v0

    #@11
    int-to-float v3, v0

    #@12
    .line 444
    .local v3, x1:F
    :goto_12
    new-instance v0, Landroid/graphics/LinearGradient;

    #@14
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientColor:I

    #@16
    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@18
    move v4, v2

    #@19
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    #@1c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLeftToRightGradient:Landroid/graphics/LinearGradient;

    #@1e
    .line 446
    new-instance v7, Landroid/graphics/LinearGradient;

    #@20
    iget v12, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mGradientColor:I

    #@22
    sget-object v14, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@24
    move v8, v3

    #@25
    move v9, v2

    #@26
    move v10, v1

    #@27
    move v11, v2

    #@28
    move v13, v6

    #@29
    invoke-direct/range {v7 .. v14}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    #@2c
    iput-object v7, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mRightToLeftGradient:Landroid/graphics/LinearGradient;

    #@2e
    .line 448
    return-void

    #@2f
    .line 442
    .end local v1           #x0:F
    .end local v3           #x1:F
    :cond_2f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@31
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@34
    move-result v0

    #@35
    int-to-float v1, v0

    #@36
    goto :goto_7

    #@37
    .restart local v1       #x0:F
    :cond_37
    move v3, v2

    #@38
    .line 443
    goto :goto_12
.end method


# virtual methods
.method public adjustFrame(I)V
    .registers 4
    .parameter "challengeTop"

    #@0
    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getPaddingBottom()I

    #@3
    move-result v1

    #@4
    add-int v0, p1, v1

    #@6
    .line 368
    .local v0, frameHeight:I
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setFrameHeight(I)V

    #@9
    .line 369
    return-void
.end method

.method public cancelLongPress()V
    .registers 2

    #@0
    .prologue
    .line 207
    invoke-super {p0}, Landroid/widget/FrameLayout;->cancelLongPress()V

    #@3
    .line 208
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->cancelLongPress()V

    #@8
    .line 209
    return-void
.end method

.method public disableHardwareLayers()V
    .registers 3

    #@0
    .prologue
    .line 277
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setLayerType(ILandroid/graphics/Paint;)V

    #@5
    .line 278
    return-void
.end method

.method public disableHardwareLayersForContent()V
    .registers 4

    #@0
    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 267
    .local v0, widget:Landroid/view/View;
    if-eqz v0, :cond_b

    #@6
    .line 268
    const/4 v1, 0x0

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    #@b
    .line 270
    :cond_b
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 237
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@3
    .line 239
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->drawBg(Landroid/graphics/Canvas;)V

    #@6
    .line 240
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@9
    .line 241
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->drawGradientOverlay(Landroid/graphics/Canvas;)V

    #@c
    .line 243
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->drawHoveringOverDeleteOverlay(Landroid/graphics/Canvas;)V

    #@f
    .line 244
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@12
    .line 246
    return-void
.end method

.method protected drawBg(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 225
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlpha:F

    #@2
    const/4 v2, 0x0

    #@3
    cmpl-float v1, v1, v2

    #@5
    if-lez v1, :cond_1d

    #@7
    .line 226
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    .line 228
    .local v0, bg:Landroid/graphics/drawable/Drawable;
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlpha:F

    #@b
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlphaMultiplier:F

    #@d
    mul-float/2addr v1, v2

    #@e
    const/high16 v2, 0x437f

    #@10
    mul-float/2addr v1, v2

    #@11
    float-to-int v1, v1

    #@12
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@15
    .line 229
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundRect:Landroid/graphics/Rect;

    #@17
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@1a
    .line 230
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@1d
    .line 232
    .end local v0           #bg:Landroid/graphics/drawable/Drawable;
    :cond_1d
    return-void
.end method

.method public enableHardwareLayers()V
    .registers 3

    #@0
    .prologue
    .line 273
    const/4 v0, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setLayerType(ILandroid/graphics/Paint;)V

    #@5
    .line 274
    return-void
.end method

.method public enableHardwareLayersForContent()V
    .registers 4

    #@0
    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 255
    .local v0, widget:Landroid/view/View;
    if-eqz v0, :cond_b

    #@6
    .line 256
    const/4 v1, 0x2

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    #@b
    .line 258
    :cond_b
    return-void
.end method

.method public fadeFrame(Ljava/lang/Object;ZFI)V
    .registers 10
    .parameter "caller"
    .parameter "takeControl"
    .parameter "alpha"
    .parameter "duration"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 423
    if-eqz p2, :cond_6

    #@4
    .line 424
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBgAlphaController:Ljava/lang/Object;

    #@6
    .line 427
    :cond_6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBgAlphaController:Ljava/lang/Object;

    #@8
    if-eq v1, p1, :cond_f

    #@a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBgAlphaController:Ljava/lang/Object;

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 439
    :goto_e
    return-void

    #@f
    .line 431
    :cond_f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@11
    if-eqz v1, :cond_1b

    #@13
    .line 432
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@15
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    #@18
    .line 433
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@1b
    .line 435
    :cond_1b
    const-string v1, "backgroundAlpha"

    #@1d
    new-array v2, v4, [F

    #@1f
    aput p3, v2, v3

    #@21
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@24
    move-result-object v0

    #@25
    .line 436
    .local v0, bgAlpha:Landroid/animation/PropertyValuesHolder;
    new-array v1, v4, [Landroid/animation/PropertyValuesHolder;

    #@27
    aput-object v0, v1, v3

    #@29
    invoke-static {p0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@2f
    .line 437
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@31
    int-to-long v2, p4

    #@32
    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@35
    .line 438
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameFade:Landroid/animation/Animator;

    #@37
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    #@3a
    goto :goto_e
.end method

.method public getBackgroundAlpha()F
    .registers 2

    #@0
    .prologue
    .line 296
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlpha:F

    #@2
    return v0
.end method

.method public getBackgroundAlphaMultiplier()F
    .registers 2

    #@0
    .prologue
    .line 307
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlphaMultiplier:F

    #@2
    return v0
.end method

.method public getContent()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 281
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getChildAt(I)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getContentAlpha()F
    .registers 2

    #@0
    .prologue
    .line 318
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mContentAlpha:F

    #@2
    return v0
.end method

.method public getContentAppWidgetId()I
    .registers 3

    #@0
    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 286
    .local v0, content:Landroid/view/View;
    instance-of v1, v0, Landroid/appwidget/AppWidgetHostView;

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 287
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@a
    .end local v0           #content:Landroid/view/View;
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHostView;->getAppWidgetId()I

    #@d
    move-result v1

    #@e
    .line 291
    :goto_e
    return v1

    #@f
    .line 288
    .restart local v0       #content:Landroid/view/View;
    :cond_f
    instance-of v1, v0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;

    #@11
    if-eqz v1, :cond_1a

    #@13
    .line 289
    check-cast v0, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;

    #@15
    .end local v0           #content:Landroid/view/View;
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardStatusView;->getAppWidgetId()I

    #@18
    move-result v1

    #@19
    goto :goto_e

    #@1a
    .line 291
    .restart local v0       #content:Landroid/view/View;
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_e
.end method

.method public getSmallFrameHeight()I
    .registers 2

    #@0
    .prologue
    .line 381
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallFrameHeight:I

    #@2
    return v0
.end method

.method public getWorkerHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWorkerHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public hideFrame(Ljava/lang/Object;)V
    .registers 5
    .parameter "caller"

    #@0
    .prologue
    .line 414
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    const/16 v2, 0x177

    #@4
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->fadeFrame(Ljava/lang/Object;ZFI)V

    #@7
    .line 415
    return-void
.end method

.method public isCustomWidget()Z
    .registers 2

    #@0
    .prologue
    .line 535
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->isCustomWidget:Z

    #@2
    return v0
.end method

.method public isSmall()Z
    .registers 2

    #@0
    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@2
    return v0
.end method

.method public onActive(Z)V
    .registers 2
    .parameter "isActive"

    #@0
    .prologue
    .line 510
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 133
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    #@3
    .line 134
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@e
    .line 135
    return-void
.end method

.method public onBouncerShowing(Z)V
    .registers 2
    .parameter "showing"

    #@0
    .prologue
    .line 519
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 125
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    #@3
    .line 126
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->cancelLongPress()V

    #@6
    .line 127
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mContext:Landroid/content/Context;

    #@8
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mUpdateMonitorCallbacks:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@e
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->removeCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@11
    .line 129
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_1c

    #@7
    .line 176
    :goto_7
    :pswitch_7
    const/4 v0, 0x0

    #@8
    return v0

    #@9
    .line 163
    :pswitch_9
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->postCheckForLongPress(Landroid/view/MotionEvent;)V

    #@e
    goto :goto_7

    #@f
    .line 166
    :pswitch_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@11
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->onMove(Landroid/view/MotionEvent;)V

    #@14
    goto :goto_7

    #@15
    .line 171
    :pswitch_15
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->cancelLongPress()V

    #@1a
    goto :goto_7

    #@1b
    .line 161
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_9
        :pswitch_15
        :pswitch_f
        :pswitch_15
        :pswitch_7
        :pswitch_15
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .registers 3
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 469
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@3
    .line 470
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->performAppWidgetSizeCallbacksIfNecessary()V

    #@6
    .line 471
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 12
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 452
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    #@4
    .line 454
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@6
    if-nez v0, :cond_a

    #@8
    .line 455
    iput p2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@a
    .line 460
    :cond_a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@c
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@e
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@10
    iget v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@12
    sub-int v3, p1, v3

    #@14
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@16
    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v4

    #@1a
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@1c
    sub-int/2addr v4, v5

    #@1d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@20
    .line 463
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredWidth()I

    #@25
    move-result v1

    #@26
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@28
    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result v2

    #@2c
    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@2f
    .line 464
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->updateGradient()V

    #@32
    .line 465
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@35
    .line 466
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 183
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_16

    #@7
    .line 196
    :goto_7
    :pswitch_7
    const/4 v0, 0x1

    #@8
    return v0

    #@9
    .line 185
    :pswitch_9
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->onMove(Landroid/view/MotionEvent;)V

    #@e
    goto :goto_7

    #@f
    .line 190
    :pswitch_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLongPressHelper:Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/CheckLongPressHelper;->cancelLongPress()V

    #@14
    goto :goto_7

    #@15
    .line 183
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_f
        :pswitch_9
        :pswitch_f
        :pswitch_7
        :pswitch_f
    .end packed-switch
.end method

.method public onUserInteraction(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 514
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "disallowIntercept"

    #@0
    .prologue
    .line 201
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    #@3
    .line 202
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->cancelLongPress()V

    #@6
    .line 203
    return-void
.end method

.method public resetSize()V
    .registers 2

    #@0
    .prologue
    .line 396
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@3
    .line 397
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWidgetLockedSmall:Z

    #@5
    if-nez v0, :cond_b

    #@7
    .line 398
    const/4 v0, -0x1

    #@8
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetHeight(I)V

    #@b
    .line 400
    :cond_b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredHeight()I

    #@e
    move-result v0

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setFrameHeight(I)V

    #@12
    .line 401
    return-void
.end method

.method public setBackgroundAlpha(F)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 311
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlpha:F

    #@2
    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 312
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlpha:F

    #@a
    .line 313
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@d
    .line 315
    :cond_d
    return-void
.end method

.method public setBackgroundAlphaMultiplier(F)V
    .registers 3
    .parameter "multiplier"

    #@0
    .prologue
    .line 300
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlphaMultiplier:F

    #@2
    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 301
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundAlphaMultiplier:F

    #@a
    .line 302
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@d
    .line 304
    :cond_d
    return-void
.end method

.method public setContentAlpha(F)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 322
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mContentAlpha:F

    #@2
    .line 323
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getContent()Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 324
    .local v0, content:Landroid/view/View;
    if-eqz v0, :cond_b

    #@8
    .line 325
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    #@b
    .line 327
    :cond_b
    return-void
.end method

.method public setCustomWidget(Z)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 532
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->isCustomWidget:Z

    #@2
    .line 533
    return-void
.end method

.method public setFrameHeight(I)V
    .registers 8
    .parameter "height"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 404
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@3
    .line 405
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mBackgroundRect:Landroid/graphics/Rect;

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredWidth()I

    #@8
    move-result v1

    #@9
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredHeight()I

    #@e
    move-result v3

    #@f
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v2

    #@13
    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@16
    .line 406
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundRect:Landroid/graphics/Rect;

    #@18
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@1a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@1c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredWidth()I

    #@1f
    move-result v3

    #@20
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@22
    sub-int/2addr v3, v4

    #@23
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getMeasuredHeight()I

    #@26
    move-result v4

    #@27
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameHeight:I

    #@29
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@2c
    move-result v4

    #@2d
    iget v5, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mFrameStrokeAdjustment:I

    #@2f
    sub-int/2addr v4, v5

    #@30
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@33
    .line 409
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->updateGradient()V

    #@36
    .line 410
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@39
    .line 411
    return-void
.end method

.method setIsHoveringOverDeleteDropTarget(Z)V
    .registers 3
    .parameter "isHovering"

    #@0
    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsHoveringOverDeleteDropTarget:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 151
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsHoveringOverDeleteDropTarget:Z

    #@6
    .line 152
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@9
    .line 155
    :cond_9
    return-void
.end method

.method public setMaxChallengeTop(I)V
    .registers 4
    .parameter "top"

    #@0
    .prologue
    .line 350
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mMaxChallengeTop:I

    #@2
    if-eq v1, p1, :cond_27

    #@4
    const/4 v0, 0x1

    #@5
    .line 351
    .local v0, dirty:Z
    :goto_5
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mMaxChallengeTop:I

    #@7
    .line 352
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getPaddingTop()I

    #@a
    move-result v1

    #@b
    sub-int v1, p1, v1

    #@d
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@f
    .line 353
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->getPaddingBottom()I

    #@12
    move-result v1

    #@13
    add-int/2addr v1, p1

    #@14
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallFrameHeight:I

    #@16
    .line 354
    if-eqz v0, :cond_29

    #@18
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@1a
    if-eqz v1, :cond_29

    #@1c
    .line 355
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@1e
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetHeight(I)V

    #@21
    .line 356
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallFrameHeight:I

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setFrameHeight(I)V

    #@26
    .line 360
    :cond_26
    :goto_26
    return-void

    #@27
    .line 350
    .end local v0           #dirty:Z
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_5

    #@29
    .line 357
    .restart local v0       #dirty:Z
    :cond_29
    if-eqz v0, :cond_26

    #@2b
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWidgetLockedSmall:Z

    #@2d
    if-eqz v1, :cond_26

    #@2f
    .line 358
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@31
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetHeight(I)V

    #@34
    goto :goto_26
.end method

.method setOverScrollAmount(FZ)V
    .registers 6
    .parameter "r"
    .parameter "left"

    #@0
    .prologue
    .line 494
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mOverScrollAmount:F

    #@2
    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_30

    #@8
    .line 495
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mOverScrollAmount:F

    #@a
    .line 496
    if-eqz p2, :cond_31

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mLeftToRightGradient:Landroid/graphics/LinearGradient;

    #@e
    :goto_e
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundGradient:Landroid/graphics/LinearGradient;

    #@10
    .line 497
    const/high16 v1, 0x3f00

    #@12
    mul-float/2addr v1, p1

    #@13
    const/high16 v2, 0x437f

    #@15
    mul-float/2addr v1, v2

    #@16
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mForegroundAlpha:I

    #@1c
    .line 501
    const v1, 0x3f19999a

    #@1f
    const v2, 0x3ecccccc

    #@22
    mul-float/2addr v2, p1

    #@23
    add-float/2addr v1, v2

    #@24
    const/high16 v2, 0x3f80

    #@26
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    #@29
    move-result v0

    #@2a
    .line 503
    .local v0, bgAlpha:F
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setBackgroundAlpha(F)V

    #@2d
    .line 504
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->invalidate()V

    #@30
    .line 506
    .end local v0           #bgAlpha:F
    :cond_30
    return-void

    #@31
    .line 496
    :cond_31
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mRightToLeftGradient:Landroid/graphics/LinearGradient;

    #@33
    goto :goto_e
.end method

.method public setWidgetLockedSmall(Z)V
    .registers 3
    .parameter "locked"

    #@0
    .prologue
    .line 389
    if-eqz p1, :cond_7

    #@2
    .line 390
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetHeight(I)V

    #@7
    .line 392
    :cond_7
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWidgetLockedSmall:Z

    #@9
    .line 393
    return-void
.end method

.method public setWorkerHandler(Landroid/os/Handler;)V
    .registers 2
    .parameter "workerHandler"

    #@0
    .prologue
    .line 522
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mWorkerHandler:Landroid/os/Handler;

    #@2
    .line 523
    return-void
.end method

.method public showFrame(Ljava/lang/Object;)V
    .registers 5
    .parameter "caller"

    #@0
    .prologue
    .line 418
    const/4 v0, 0x1

    #@1
    const v1, 0x3f19999a

    #@4
    const/16 v2, 0x64

    #@6
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->fadeFrame(Ljava/lang/Object;ZFI)V

    #@9
    .line 420
    return-void
.end method

.method public shrinkWidget()V
    .registers 2

    #@0
    .prologue
    .line 385
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->shrinkWidget(Z)V

    #@4
    .line 386
    return-void
.end method

.method public shrinkWidget(Z)V
    .registers 3
    .parameter "alsoShrinkFrame"

    #@0
    .prologue
    .line 372
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mIsSmall:Z

    #@3
    .line 373
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallWidgetHeight:I

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setWidgetHeight(I)V

    #@8
    .line 375
    if-eqz p1, :cond_f

    #@a
    .line 376
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->mSmallFrameHeight:I

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardWidgetFrame;->setFrameHeight(I)V

    #@f
    .line 378
    :cond_f
    return-void
.end method
