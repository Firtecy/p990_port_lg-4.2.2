.class Lcom/android/internal/policy/impl/PhoneWindowManager$48;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8511
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 8530
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 8514
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4
    invoke-static {v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$3900(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_46

    #@a
    .line 8515
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    #@c
    aget v0, v4, v3

    #@e
    .line 8516
    .local v0, distance:F
    const/4 v4, 0x0

    #@f
    cmpl-float v4, v0, v4

    #@11
    if-lez v4, :cond_47

    #@13
    move v1, v2

    #@14
    .line 8517
    .local v1, positive:Z
    :goto_14
    if-eqz v1, :cond_49

    #@16
    .line 8518
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@18
    invoke-static {v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1502(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@1b
    .line 8522
    :cond_1b
    :goto_1b
    const-string v2, "WindowManager"

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Proximity Near : "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2a
    invoke-static {v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z

    #@2d
    move-result v4

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 8523
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3b
    iget-object v2, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@3d
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3f
    iget-object v3, v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyPress:Ljava/lang/Runnable;

    #@41
    const-wide/16 v4, 0x2bc

    #@43
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@46
    .line 8525
    .end local v0           #distance:F
    .end local v1           #positive:Z
    :cond_46
    return-void

    #@47
    .restart local v0       #distance:F
    :cond_47
    move v1, v3

    #@48
    .line 8516
    goto :goto_14

    #@49
    .line 8519
    .restart local v1       #positive:Z
    :cond_49
    const-string v3, "sys.allautotest.run"

    #@4b
    const-string v4, "false"

    #@4d
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    const-string v4, "false"

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v3

    #@57
    if-eqz v3, :cond_1b

    #@59
    .line 8520
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@5b
    invoke-static {v3, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->access$1502(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z

    #@5e
    goto :goto_1b
.end method
