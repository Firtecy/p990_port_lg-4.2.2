.class abstract Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;
.super Ljava/lang/Thread;
.source "SimUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "CheckSimPin"
.end annotation


# instance fields
.field private final mPin:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;


# direct methods
.method protected constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "pin"

    #@0
    .prologue
    .line 150
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 151
    iput-object p2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;->mPin:Ljava/lang/String;

    #@7
    .line 152
    return-void
.end method


# virtual methods
.method abstract onSimLockChangedResponse(Z)V
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 159
    :try_start_0
    const-string v2, "phone"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v2

    #@a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;->mPin:Ljava/lang/String;

    #@c
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->supplyPin(Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    .line 161
    .local v1, result:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@12
    new-instance v3, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin$1;

    #@14
    invoke-direct {v3, p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;Z)V

    #@17
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->post(Ljava/lang/Runnable;)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_1a} :catch_1b

    #@1a
    .line 173
    .end local v1           #result:Z
    :goto_1a
    return-void

    #@1b
    .line 166
    :catch_1b
    move-exception v0

    #@1c
    .line 167
    .local v0, e:Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@1e
    new-instance v3, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin$2;

    #@20
    invoke-direct {v3, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;)V

    #@23
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->post(Ljava/lang/Runnable;)Z

    #@26
    goto :goto_1a
.end method
