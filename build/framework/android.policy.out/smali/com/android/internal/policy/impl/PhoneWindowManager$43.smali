.class Lcom/android/internal/policy/impl/PhoneWindowManager$43;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;->showGoHomeDialog2()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8273
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$43;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 15
    .parameter "dialog"
    .parameter "whichButton"

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v10, 0x2

    #@3
    .line 8275
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$43;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@5
    iget-object v7, v7, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v7

    #@b
    const-string v8, "go_home"

    #@d
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@10
    .line 8278
    new-instance v2, Landroid/content/Intent;

    #@12
    const-string v7, "com.lge.setup_wizard.AUTORUNON"

    #@14
    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 8279
    .local v2, intent_autorun:Landroid/content/Intent;
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$43;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@19
    iget-object v7, v7, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1e
    .line 8283
    new-instance v3, Landroid/content/ComponentName;

    #@20
    const-string v7, "com.android.LGSetupWizard"

    #@22
    const-string v8, "com.android.LGSetupWizard.SetupStart"

    #@24
    invoke-direct {v3, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 8285
    .local v3, name:Landroid/content/ComponentName;
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$43;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@29
    iget-object v7, v7, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@2e
    move-result-object v4

    #@2f
    .line 8287
    .local v4, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {v4, v3, v10, v11}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    #@32
    .line 8290
    const-string v7, "WindowManager"

    #@34
    new-instance v8, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v9, "LGSetupWizardDisable"

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v8

    #@47
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 8293
    new-instance v0, Landroid/content/Intent;

    #@4c
    const-string v7, "android.intent.action.MAIN"

    #@4e
    const/4 v8, 0x0

    #@4f
    invoke-direct {v0, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@52
    .line 8294
    .local v0, Homeintent:Landroid/content/Intent;
    const-string v7, "android.intent.category.HOME"

    #@54
    invoke-virtual {v0, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@57
    .line 8295
    const/high16 v7, 0x1400

    #@59
    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5c
    .line 8297
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$43;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@5e
    iget-object v7, v7, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@60
    invoke-virtual {v7, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@63
    .line 8300
    const-string v5, "com.android.LGSetupWizard"

    #@65
    .line 8301
    .local v5, setupWizard:Ljava/lang/String;
    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@67
    .line 8303
    .local v6, version:Ljava/lang/String;
    :try_start_67
    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    #@6a
    move-result v7

    #@6b
    if-eq v7, v10, :cond_81

    #@6d
    .line 8304
    const-string v7, "4.1"

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@72
    move-result v7

    #@73
    if-nez v7, :cond_82

    #@75
    .line 8305
    const/4 v7, 0x2

    #@76
    const/4 v8, 0x1

    #@77
    invoke-virtual {v4, v5, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    #@7a
    .line 8309
    const-string v7, "WindowManager"

    #@7c
    const-string v8, "######LGSetupWizard COMPONENT_ENABLED_STATE_DISABLED######"

    #@7e
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 8322
    :cond_81
    :goto_81
    return-void

    #@82
    .line 8312
    :cond_82
    const/4 v7, 0x2

    #@83
    const/4 v8, 0x0

    #@84
    invoke-virtual {v4, v5, v7, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    #@87
    .line 8314
    const-string v7, "WindowManager"

    #@89
    const-string v8, "haksung_temp LGSetupWizard COMPONENT_ENABLED_STATE_DISABLED"

    #@8b
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8e
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_81

    #@8f
    .line 8318
    :catch_8f
    move-exception v1

    #@90
    .line 8319
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "WindowManager"

    #@92
    const-string v8, "cannot disable setup wizard"

    #@94
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_81
.end method
