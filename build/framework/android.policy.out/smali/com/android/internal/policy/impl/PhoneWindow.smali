.class public Lcom/android/internal/policy/impl/PhoneWindow;
.super Landroid/view/Window;
.source "PhoneWindow.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuBuilder$Callback;
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;,
        Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;,
        Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;,
        Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;,
        Lcom/android/internal/policy/impl/PhoneWindow$DecorView;,
        Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;,
        Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;,
        Lcom/android/internal/policy/impl/PhoneWindow$WindowManagerHolder;
    }
.end annotation


# static fields
.field private static final ACTION_BAR_TAG:Ljava/lang/String; = "android:ActionBar"

.field private static final FOCUSED_ID_TAG:Ljava/lang/String; = "android:focusedViewId"

.field private static final PANELS_TAG:Ljava/lang/String; = "android:Panels"

.field private static final SWEEP_OPEN_MENU:Z = false

.field private static final TAG:Ljava/lang/String; = "PhoneWindow"

.field private static final VIEWS_TAG:Ljava/lang/String; = "android:views"

.field static final sRotationWatcher:Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;


# instance fields
.field private mActionBar:Lcom/android/internal/widget/ActionBarView;

.field private mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

.field private mAlwaysReadCloseOnTouchAttr:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mBackgroundResource:I

.field private mCircularProgressBar:Landroid/widget/ProgressBar;

.field private mClosingActionMenu:Z

.field private mContentParent:Landroid/view/ViewGroup;

.field mContext:Landroid/content/Context;

.field private mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

.field final mContextMenuCallback:Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;

.field private mContextMenuHelper:Lcom/android/internal/view/menu/MenuDialogHelper;

.field private mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

.field private mDrawables:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

.field mFixedHeightMajor:Landroid/util/TypedValue;

.field mFixedHeightMinor:Landroid/util/TypedValue;

.field mFixedWidthMajor:Landroid/util/TypedValue;

.field mFixedWidthMinor:Landroid/util/TypedValue;

.field private mForceNotFullscreen:Z

.field private mFrameResource:I

.field private mGestureScanner:Landroid/view/GestureDetector;

.field private mHorizontalProgressBar:Landroid/widget/ProgressBar;

.field private mInvalidatePanelMenuFeatures:I

.field private mInvalidatePanelMenuPosted:Z

.field private final mInvalidatePanelMenuRunnable:Ljava/lang/Runnable;

.field private mIsFloating:Z

.field private mIsFocused:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLeftIconView:Landroid/widget/ImageView;

.field final mMaxHeightMajor:Landroid/util/TypedValue;

.field final mMaxHeightMinor:Landroid/util/TypedValue;

.field final mMinWidthMajor:Landroid/util/TypedValue;

.field final mMinWidthMinor:Landroid/util/TypedValue;

.field private mPanelChordingKey:I

.field private mPanelMayLongPress:Z

.field private mPanelMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

.field private mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

.field private mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

.field private mPrevFlag:I

.field private mRightIconView:Landroid/widget/ImageView;

.field private mSetFlagsLocked:Z

.field mStatusbarReceiver:Landroid/content/BroadcastReceiver;

.field mTakeInputQueueCallback:Landroid/view/InputQueue$Callback;

.field mTakeSurfaceCallback:Landroid/view/SurfaceHolder$Callback2;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTextColor:I

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleColor:I

.field private mTitleView:Landroid/widget/TextView;

.field private mUiOptions:I

.field private mVolumeControlStreamType:I

.field private mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 254
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;

    #@2
    invoke-direct {v0}, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindow;->sRotationWatcher:Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 257
    invoke-direct {p0, p1}, Landroid/view/Window;-><init>(Landroid/content/Context;)V

    #@5
    .line 134
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;

    #@7
    const/4 v1, 0x6

    #@8
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;I)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuCallback:Lcom/android/internal/policy/impl/PhoneWindow$DialogMenuCallback;

    #@d
    .line 136
    new-instance v0, Landroid/util/TypedValue;

    #@f
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMajor:Landroid/util/TypedValue;

    #@14
    .line 137
    new-instance v0, Landroid/util/TypedValue;

    #@16
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMinor:Landroid/util/TypedValue;

    #@1b
    .line 140
    new-instance v0, Landroid/util/TypedValue;

    #@1d
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@22
    .line 141
    new-instance v0, Landroid/util/TypedValue;

    #@24
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@29
    .line 144
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@2b
    .line 145
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPrevFlag:I

    #@2d
    .line 146
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mSetFlagsLocked:Z

    #@2f
    .line 201
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@31
    .line 205
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mFrameResource:I

    #@33
    .line 207
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTextColor:I

    #@35
    .line 209
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@37
    .line 211
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleColor:I

    #@39
    .line 213
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mAlwaysReadCloseOnTouchAttr:Z

    #@3b
    .line 219
    const/high16 v0, -0x8000

    #@3d
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mVolumeControlStreamType:I

    #@3f
    .line 225
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@41
    .line 228
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mUiOptions:I

    #@43
    .line 233
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFocused:Z

    #@45
    .line 237
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$1;

    #@47
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindow$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@4a
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuRunnable:Ljava/lang/Runnable;

    #@4c
    .line 272
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$2;

    #@4e
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindow$2;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@51
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mStatusbarReceiver:Landroid/content/BroadcastReceiver;

    #@53
    .line 258
    new-instance v0, Landroid/view/GestureDetector;

    #@55
    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    #@58
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mGestureScanner:Landroid/view/GestureDetector;

    #@5a
    .line 259
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@5d
    move-result-object v0

    #@5e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@60
    .line 260
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContext:Landroid/content/Context;

    #@62
    .line 261
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/PhoneWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuFeatures:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/PhoneWindow;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuFeatures:I

    #@2
    return p1
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/policy/impl/PhoneWindow;->performPanelShortcut(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/PhoneWindow;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuPosted:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/PhoneWindow;)Landroid/telephony/TelephonyManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/internal/policy/impl/PhoneWindow;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/PhoneWindow;IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/widget/ActionBarView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/view/menu/ContextMenuBuilder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/view/menu/ContextMenuBuilder;)Lcom/android/internal/view/menu/ContextMenuBuilder;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@2
    return-object p1
.end method

.method static synthetic access$1502(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/view/menu/MenuDialogHelper;)Lcom/android/internal/view/menu/MenuDialogHelper;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuHelper:Lcom/android/internal/view/menu/MenuDialogHelper;

    #@2
    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/PhoneWindow;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->setDefaultWindowFormat(I)V

    #@3
    return-void
.end method

.method static synthetic access$1902(Lcom/android/internal/policy/impl/PhoneWindow;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/PhoneWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@2
    return v0
.end method

.method static synthetic access$2002(Lcom/android/internal/policy/impl/PhoneWindow;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFocused:Z

    #@2
    return p1
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/PhoneWindow;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanelsAfterRestore()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/internal/policy/impl/PhoneWindow;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->dismissContextMenu()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/PhoneWindow;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/PhoneWindow;ILcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/Menu;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindow;->callOnPanelClosed(ILcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/Menu;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/PhoneWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/PhoneWindow;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@2
    return-object v0
.end method

.method private callOnPanelClosed(ILcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/Menu;)V
    .registers 6
    .parameter "featureId"
    .parameter "panel"
    .parameter "menu"

    #@0
    .prologue
    .line 3431
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@3
    move-result-object v0

    #@4
    .line 3432
    .local v0, cb:Landroid/view/Window$Callback;
    if-nez v0, :cond_7

    #@6
    .line 3457
    :cond_6
    :goto_6
    return-void

    #@7
    .line 3436
    :cond_7
    if-nez p3, :cond_1a

    #@9
    .line 3438
    if-nez p2, :cond_16

    #@b
    .line 3439
    if-ltz p1, :cond_16

    #@d
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@f
    array-length v1, v1

    #@10
    if-ge p1, v1, :cond_16

    #@12
    .line 3440
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@14
    aget-object p2, v1, p1

    #@16
    .line 3444
    :cond_16
    if-eqz p2, :cond_1a

    #@18
    .line 3446
    iget-object p3, p2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1a
    .line 3451
    :cond_1a
    if-eqz p2, :cond_20

    #@1c
    iget-boolean v1, p2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@1e
    if-eqz v1, :cond_6

    #@20
    .line 3454
    :cond_20
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@23
    move-result v1

    #@24
    if-nez v1, :cond_6

    #@26
    .line 3455
    invoke-interface {v0, p1, p3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    #@29
    goto :goto_6
.end method

.method private static clearMenuViews(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)V
    .registers 2
    .parameter "st"

    #@0
    .prologue
    .line 627
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@3
    .line 630
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@6
    .line 632
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->clearMenuPresenters()V

    #@9
    .line 633
    return-void
.end method

.method private declared-synchronized closeContextMenu()V
    .registers 2

    #@0
    .prologue
    .line 1034
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 1035
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ContextMenuBuilder;->close()V

    #@a
    .line 1036
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->dismissContextMenu()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 1038
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 1034
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method private declared-synchronized dismissContextMenu()V
    .registers 2

    #@0
    .prologue
    .line 1045
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@4
    .line 1047
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuHelper:Lcom/android/internal/view/menu/MenuDialogHelper;

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 1048
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuHelper:Lcom/android/internal/view/menu/MenuDialogHelper;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuDialogHelper;->dismiss()V

    #@d
    .line 1049
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenuHelper:Lcom/android/internal/view/menu/MenuDialogHelper;
    :try_end_10
    .catchall {:try_start_2 .. :try_end_10} :catchall_12

    #@10
    .line 1051
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 1045
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method private getCircularProgressBar(Z)Landroid/widget/ProgressBar;
    .registers 4
    .parameter "shouldInstallDecor"

    #@0
    .prologue
    .line 3382
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3383
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@6
    .line 3392
    :goto_6
    return-object v0

    #@7
    .line 3385
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    if-nez v0, :cond_10

    #@b
    if-eqz p1, :cond_10

    #@d
    .line 3386
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@10
    .line 3388
    :cond_10
    const v0, 0x102036a

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/ProgressBar;

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@1b
    .line 3389
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@1d
    if-eqz v0, :cond_25

    #@1f
    .line 3390
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@21
    const/4 v1, 0x4

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@25
    .line 3392
    :cond_25
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mCircularProgressBar:Landroid/widget/ProgressBar;

    #@27
    goto :goto_6
.end method

.method private getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    .registers 9
    .parameter "featureId"
    .parameter "required"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3222
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@4
    move-result v3

    #@5
    const/4 v4, 0x1

    #@6
    shl-int/2addr v4, p1

    #@7
    and-int/2addr v3, v4

    #@8
    if-nez v3, :cond_16

    #@a
    .line 3223
    if-nez p2, :cond_e

    #@c
    .line 3224
    const/4 v2, 0x0

    #@d
    .line 3242
    :cond_d
    :goto_d
    return-object v2

    #@e
    .line 3226
    :cond_e
    new-instance v3, Ljava/lang/RuntimeException;

    #@10
    const-string v4, "The feature has not been requested"

    #@12
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v3

    #@16
    .line 3230
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDrawables:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@18
    .local v0, ar:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    if-eqz v0, :cond_1d

    #@1a
    array-length v3, v0

    #@1b
    if-gt v3, p1, :cond_2a

    #@1d
    .line 3231
    :cond_1d
    add-int/lit8 v3, p1, 0x1

    #@1f
    new-array v1, v3, [Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@21
    .line 3232
    .local v1, nar:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    if-eqz v0, :cond_27

    #@23
    .line 3233
    array-length v3, v0

    #@24
    invoke-static {v0, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 3235
    :cond_27
    move-object v0, v1

    #@28
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDrawables:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@2a
    .line 3238
    .end local v1           #nar:[Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    :cond_2a
    aget-object v2, v0, p1

    #@2c
    .line 3239
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    if-nez v2, :cond_d

    #@2e
    .line 3240
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@30
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    invoke-direct {v2, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;-><init>(I)V

    #@33
    .restart local v2       #st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    aput-object v2, v0, p1

    #@35
    goto :goto_d
.end method

.method private getHorizontalProgressBar(Z)Landroid/widget/ProgressBar;
    .registers 4
    .parameter "shouldInstallDecor"

    #@0
    .prologue
    .line 3396
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3397
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@6
    .line 3406
    :goto_6
    return-object v0

    #@7
    .line 3399
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    if-nez v0, :cond_10

    #@b
    if-eqz p1, :cond_10

    #@d
    .line 3400
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@10
    .line 3402
    :cond_10
    const v0, 0x102036b

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/ProgressBar;

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@1b
    .line 3403
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@1d
    if-eqz v0, :cond_25

    #@1f
    .line 3404
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@21
    const/4 v1, 0x4

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@25
    .line 3406
    :cond_25
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mHorizontalProgressBar:Landroid/widget/ProgressBar;

    #@27
    goto :goto_6
.end method

.method private getKeyguardManager()Landroid/app/KeyguardManager;
    .registers 3

    #@0
    .prologue
    .line 1683
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@2
    if-nez v0, :cond_12

    #@4
    .line 1684
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@7
    move-result-object v0

    #@8
    const-string v1, "keyguard"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/app/KeyguardManager;

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@12
    .line 1687
    :cond_12
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@14
    return-object v0
.end method

.method private getLeftIconView()Landroid/widget/ImageView;
    .registers 2

    #@0
    .prologue
    .line 3372
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLeftIconView:Landroid/widget/ImageView;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3373
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLeftIconView:Landroid/widget/ImageView;

    #@6
    .line 3378
    :goto_6
    return-object v0

    #@7
    .line 3375
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    if-nez v0, :cond_e

    #@b
    .line 3376
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@e
    .line 3378
    :cond_e
    const v0, 0x102024b

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/widget/ImageView;

    #@17
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLeftIconView:Landroid/widget/ImageView;

    #@19
    goto :goto_6
.end method

.method private getOptionsPanelGravity()I
    .registers 4

    #@0
    .prologue
    .line 1230
    :try_start_0
    sget-object v1, Lcom/android/internal/policy/impl/PhoneWindow$WindowManagerHolder;->sWindowManager:Landroid/view/IWindowManager;

    #@2
    invoke-interface {v1}, Landroid/view/IWindowManager;->getPreferredOptionsPanelGravity()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1233
    :goto_6
    return v1

    #@7
    .line 1231
    :catch_7
    move-exception v0

    #@8
    .line 1232
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "PhoneWindow"

    #@a
    const-string v2, "Couldn\'t getOptionsPanelGravity; using default"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 1233
    const/16 v1, 0x51

    #@11
    goto :goto_6
.end method

.method private getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    .registers 4
    .parameter "featureId"
    .parameter "required"

    #@0
    .prologue
    .line 3254
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZLcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private getPanelState(IZLcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    .registers 10
    .parameter "featureId"
    .parameter "required"
    .parameter "convertPanelState"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3269
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@4
    move-result v3

    #@5
    const/4 v4, 0x1

    #@6
    shl-int/2addr v4, p1

    #@7
    and-int/2addr v3, v4

    #@8
    if-nez v3, :cond_16

    #@a
    .line 3270
    if-nez p2, :cond_e

    #@c
    .line 3271
    const/4 v2, 0x0

    #@d
    .line 3291
    :cond_d
    :goto_d
    return-object v2

    #@e
    .line 3273
    :cond_e
    new-instance v3, Ljava/lang/RuntimeException;

    #@10
    const-string v4, "The feature has not been requested"

    #@12
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v3

    #@16
    .line 3277
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@18
    .local v0, ar:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v0, :cond_1d

    #@1a
    array-length v3, v0

    #@1b
    if-gt v3, p1, :cond_2a

    #@1d
    .line 3278
    :cond_1d
    add-int/lit8 v3, p1, 0x1

    #@1f
    new-array v1, v3, [Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@21
    .line 3279
    .local v1, nar:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v0, :cond_27

    #@23
    .line 3280
    array-length v3, v0

    #@24
    invoke-static {v0, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 3282
    :cond_27
    move-object v0, v1

    #@28
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@2a
    .line 3285
    .end local v1           #nar:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_2a
    aget-object v2, v0, p1

    #@2c
    .line 3286
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez v2, :cond_d

    #@2e
    .line 3287
    if-eqz p3, :cond_34

    #@30
    move-object v2, p3

    #@31
    :goto_31
    aput-object v2, v0, p1

    #@33
    goto :goto_d

    #@34
    :cond_34
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@36
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    invoke-direct {v2, p1}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;-><init>(I)V

    #@39
    goto :goto_31
.end method

.method private getRightIconView()Landroid/widget/ImageView;
    .registers 2

    #@0
    .prologue
    .line 3410
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mRightIconView:Landroid/widget/ImageView;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3411
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mRightIconView:Landroid/widget/ImageView;

    #@6
    .line 3416
    :goto_6
    return-object v0

    #@7
    .line 3413
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    if-nez v0, :cond_e

    #@b
    .line 3414
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@e
    .line 3416
    :cond_e
    const v0, 0x102024d

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/widget/ImageView;

    #@17
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mRightIconView:Landroid/widget/ImageView;

    #@19
    goto :goto_6
.end method

.method private hideProgressBars(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V
    .registers 8
    .parameter "horizontalProgressBar"
    .parameter "spinnyProgressBar"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    .line 1512
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@4
    move-result v1

    #@5
    .line 1513
    .local v1, features:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@8
    move-result-object v2

    #@9
    const v3, 0x10a0001

    #@c
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@f
    move-result-object v0

    #@10
    .line 1514
    .local v0, anim:Landroid/view/animation/Animation;
    const-wide/16 v2, 0x3e8

    #@12
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    #@15
    .line 1515
    and-int/lit8 v2, v1, 0x20

    #@17
    if-eqz v2, :cond_25

    #@19
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getVisibility()I

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_25

    #@1f
    .line 1517
    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    #@22
    .line 1518
    invoke-virtual {p2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@25
    .line 1520
    :cond_25
    and-int/lit8 v2, v1, 0x4

    #@27
    if-eqz v2, :cond_35

    #@29
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getVisibility()I

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_35

    #@2f
    .line 1522
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    #@32
    .line 1523
    invoke-virtual {p1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@35
    .line 1525
    :cond_35
    return-void
.end method

.method private installDecor()V
    .registers 11

    #@0
    .prologue
    const/16 v9, 0x8

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 3123
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@6
    if-nez v6, :cond_29

    #@8
    .line 3124
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->generateDecor()Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@b
    move-result-object v6

    #@c
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@e
    .line 3125
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@10
    const/high16 v8, 0x4

    #@12
    invoke-virtual {v6, v8}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setDescendantFocusability(I)V

    #@15
    .line 3126
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@17
    invoke-virtual {v6, v4}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setIsRootNamespace(Z)V

    #@1a
    .line 3127
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuPosted:Z

    #@1c
    if-nez v6, :cond_29

    #@1e
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuFeatures:I

    #@20
    if-eqz v6, :cond_29

    #@22
    .line 3128
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@24
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuRunnable:Ljava/lang/Runnable;

    #@26
    invoke-virtual {v6, v8}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@29
    .line 3131
    :cond_29
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2b
    if-nez v6, :cond_76

    #@2d
    .line 3132
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2f
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->generateLayout(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Landroid/view/ViewGroup;

    #@32
    move-result-object v6

    #@33
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@35
    .line 3135
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@37
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->makeOptionalFitsSystemWindows()V

    #@3a
    .line 3137
    const v6, 0x1020016

    #@3d
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@40
    move-result-object v6

    #@41
    check-cast v6, Landroid/widget/TextView;

    #@43
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@45
    .line 3138
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@47
    if-eqz v6, :cond_85

    #@49
    .line 3139
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@4b
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@4d
    invoke-virtual {v7}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getLayoutDirection()I

    #@50
    move-result v7

    #@51
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLayoutDirection(I)V

    #@54
    .line 3140
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@57
    move-result v6

    #@58
    and-int/lit8 v6, v6, 0x2

    #@5a
    if-eqz v6, :cond_7d

    #@5c
    .line 3141
    const v6, 0x102024e

    #@5f
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@62
    move-result-object v5

    #@63
    .line 3142
    .local v5, titleContainer:Landroid/view/View;
    if-eqz v5, :cond_77

    #@65
    .line 3143
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    #@68
    .line 3147
    :goto_68
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@6a
    instance-of v6, v6, Landroid/widget/FrameLayout;

    #@6c
    if-eqz v6, :cond_76

    #@6e
    .line 3148
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@70
    check-cast v6, Landroid/widget/FrameLayout;

    #@72
    const/4 v7, 0x0

    #@73
    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    #@76
    .line 3209
    .end local v5           #titleContainer:Landroid/view/View;
    :cond_76
    :goto_76
    return-void

    #@77
    .line 3145
    .restart local v5       #titleContainer:Landroid/view/View;
    :cond_77
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@79
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    #@7c
    goto :goto_68

    #@7d
    .line 3151
    .end local v5           #titleContainer:Landroid/view/View;
    :cond_7d
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@7f
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@81
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@84
    goto :goto_76

    #@85
    .line 3154
    :cond_85
    const v6, 0x102036d

    #@88
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@8b
    move-result-object v6

    #@8c
    check-cast v6, Lcom/android/internal/widget/ActionBarView;

    #@8e
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@90
    .line 3155
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@92
    if-eqz v6, :cond_76

    #@94
    .line 3156
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@96
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@99
    move-result-object v8

    #@9a
    invoke-virtual {v6, v8}, Lcom/android/internal/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    #@9d
    .line 3157
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@9f
    invoke-virtual {v6}, Lcom/android/internal/widget/ActionBarView;->getTitle()Ljava/lang/CharSequence;

    #@a2
    move-result-object v6

    #@a3
    if-nez v6, :cond_ac

    #@a5
    .line 3158
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@a7
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@a9
    invoke-virtual {v6, v8}, Lcom/android/internal/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    #@ac
    .line 3160
    :cond_ac
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@af
    move-result v1

    #@b0
    .line 3161
    .local v1, localFeatures:I
    and-int/lit8 v6, v1, 0x4

    #@b2
    if-eqz v6, :cond_b9

    #@b4
    .line 3162
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@b6
    invoke-virtual {v6}, Lcom/android/internal/widget/ActionBarView;->initProgress()V

    #@b9
    .line 3164
    :cond_b9
    and-int/lit8 v6, v1, 0x20

    #@bb
    if-eqz v6, :cond_c2

    #@bd
    .line 3165
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@bf
    invoke-virtual {v6}, Lcom/android/internal/widget/ActionBarView;->initIndeterminateProgress()V

    #@c2
    .line 3168
    :cond_c2
    const/4 v2, 0x0

    #@c3
    .line 3169
    .local v2, splitActionBar:Z
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mUiOptions:I

    #@c5
    and-int/lit8 v6, v6, 0x1

    #@c7
    if-eqz v6, :cond_112

    #@c9
    .line 3171
    .local v4, splitWhenNarrow:Z
    :goto_c9
    if-eqz v4, :cond_114

    #@cb
    .line 3172
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@ce
    move-result-object v6

    #@cf
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d2
    move-result-object v6

    #@d3
    const v7, 0x1110006

    #@d6
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@d9
    move-result v2

    #@da
    .line 3178
    :goto_da
    const v6, 0x102036f

    #@dd
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@e0
    move-result-object v3

    #@e1
    check-cast v3, Lcom/android/internal/widget/ActionBarContainer;

    #@e3
    .line 3180
    .local v3, splitView:Lcom/android/internal/widget/ActionBarContainer;
    if-eqz v3, :cond_11f

    #@e5
    .line 3181
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@e7
    invoke-virtual {v6, v3}, Lcom/android/internal/widget/ActionBarView;->setSplitView(Lcom/android/internal/widget/ActionBarContainer;)V

    #@ea
    .line 3182
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@ec
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/ActionBarView;->setSplitActionBar(Z)V

    #@ef
    .line 3183
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@f1
    invoke-virtual {v6, v4}, Lcom/android/internal/widget/ActionBarView;->setSplitWhenNarrow(Z)V

    #@f4
    .line 3185
    const v6, 0x102036e

    #@f7
    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@fa
    move-result-object v0

    #@fb
    check-cast v0, Lcom/android/internal/widget/ActionBarContextView;

    #@fd
    .line 3187
    .local v0, cab:Lcom/android/internal/widget/ActionBarContextView;
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/ActionBarContextView;->setSplitView(Lcom/android/internal/widget/ActionBarContainer;)V

    #@100
    .line 3188
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarContextView;->setSplitActionBar(Z)V

    #@103
    .line 3189
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/ActionBarContextView;->setSplitWhenNarrow(Z)V

    #@106
    .line 3197
    .end local v0           #cab:Lcom/android/internal/widget/ActionBarContextView;
    :cond_106
    :goto_106
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@108
    new-instance v7, Lcom/android/internal/policy/impl/PhoneWindow$3;

    #@10a
    invoke-direct {v7, p0}, Lcom/android/internal/policy/impl/PhoneWindow$3;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@10d
    invoke-virtual {v6, v7}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->post(Ljava/lang/Runnable;)Z

    #@110
    goto/16 :goto_76

    #@112
    .end local v3           #splitView:Lcom/android/internal/widget/ActionBarContainer;
    .end local v4           #splitWhenNarrow:Z
    :cond_112
    move v4, v7

    #@113
    .line 3169
    goto :goto_c9

    #@114
    .line 3175
    .restart local v4       #splitWhenNarrow:Z
    :cond_114
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowStyle()Landroid/content/res/TypedArray;

    #@117
    move-result-object v6

    #@118
    const/16 v8, 0x16

    #@11a
    invoke-virtual {v6, v8, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@11d
    move-result v2

    #@11e
    goto :goto_da

    #@11f
    .line 3190
    .restart local v3       #splitView:Lcom/android/internal/widget/ActionBarContainer;
    :cond_11f
    if-eqz v2, :cond_106

    #@121
    .line 3191
    const-string v6, "PhoneWindow"

    #@123
    const-string v7, "Requested split action bar with incompatible window decor! Ignoring request."

    #@125
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    goto :goto_106
.end method

.method private launchDefaultSearch()Z
    .registers 3

    #@0
    .prologue
    .line 3466
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@3
    move-result-object v0

    #@4
    .line 3467
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_c

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    .line 3468
    :cond_c
    const/4 v1, 0x0

    #@d
    .line 3471
    :goto_d
    return v1

    #@e
    .line 3470
    :cond_e
    const-string v1, "search"

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->sendCloseSystemWindows(Ljava/lang/String;)V

    #@13
    .line 3471
    invoke-interface {v0}, Landroid/view/Window$Callback;->onSearchRequested()Z

    #@16
    move-result v1

    #@17
    goto :goto_d
.end method

.method private loadImageURI(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter "uri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3213
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@c
    move-result-object v2

    #@d
    const/4 v3, 0x0

    #@e
    invoke-static {v2, v3}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_11} :catch_13

    #@11
    move-result-object v1

    #@12
    .line 3218
    :goto_12
    return-object v1

    #@13
    .line 3215
    :catch_13
    move-exception v0

    #@14
    .line 3216
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "PhoneWindow"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Unable to open content: "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method private openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V
    .registers 21
    .parameter "st"
    .parameter "event"

    #@0
    .prologue
    .line 651
    move-object/from16 v0, p1

    #@2
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@4
    if-nez v4, :cond_c

    #@6
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_d

    #@c
    .line 771
    :cond_c
    :goto_c
    return-void

    #@d
    .line 657
    :cond_d
    move-object/from16 v0, p1

    #@f
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@11
    if-nez v4, :cond_36

    #@13
    .line 658
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@16
    move-result-object v13

    #@17
    .line 659
    .local v13, context:Landroid/content/Context;
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@1e
    move-result-object v12

    #@1f
    .line 660
    .local v12, config:Landroid/content/res/Configuration;
    iget v4, v12, Landroid/content/res/Configuration;->screenLayout:I

    #@21
    and-int/lit8 v4, v4, 0xf

    #@23
    const/4 v5, 0x4

    #@24
    if-ne v4, v5, :cond_53

    #@26
    const/4 v15, 0x1

    #@27
    .line 662
    .local v15, isXLarge:Z
    :goto_27
    invoke-virtual {v13}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@2a
    move-result-object v4

    #@2b
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@2d
    const/16 v5, 0xb

    #@2f
    if-lt v4, v5, :cond_55

    #@31
    const/4 v14, 0x1

    #@32
    .line 665
    .local v14, isHoneycombApp:Z
    :goto_32
    if-eqz v15, :cond_36

    #@34
    if-nez v14, :cond_c

    #@36
    .line 670
    .end local v12           #config:Landroid/content/res/Configuration;
    .end local v13           #context:Landroid/content/Context;
    .end local v14           #isHoneycombApp:Z
    .end local v15           #isXLarge:Z
    :cond_36
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@39
    move-result-object v11

    #@3a
    .line 671
    .local v11, cb:Landroid/view/Window$Callback;
    if-eqz v11, :cond_57

    #@3c
    move-object/from16 v0, p1

    #@3e
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@40
    move-object/from16 v0, p1

    #@42
    iget-object v5, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@44
    invoke-interface {v11, v4, v5}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    #@47
    move-result v4

    #@48
    if-nez v4, :cond_57

    #@4a
    .line 673
    const/4 v4, 0x1

    #@4b
    move-object/from16 v0, p0

    #@4d
    move-object/from16 v1, p1

    #@4f
    invoke-virtual {v0, v1, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@52
    goto :goto_c

    #@53
    .line 660
    .end local v11           #cb:Landroid/view/Window$Callback;
    .restart local v12       #config:Landroid/content/res/Configuration;
    .restart local v13       #context:Landroid/content/Context;
    :cond_53
    const/4 v15, 0x0

    #@54
    goto :goto_27

    #@55
    .line 662
    .restart local v15       #isXLarge:Z
    :cond_55
    const/4 v14, 0x0

    #@56
    goto :goto_32

    #@57
    .line 677
    .end local v12           #config:Landroid/content/res/Configuration;
    .end local v13           #context:Landroid/content/Context;
    .end local v15           #isXLarge:Z
    .restart local v11       #cb:Landroid/view/Window$Callback;
    :cond_57
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowManager()Landroid/view/WindowManager;

    #@5a
    move-result-object v17

    #@5b
    .line 678
    .local v17, wm:Landroid/view/WindowManager;
    if-eqz v17, :cond_c

    #@5d
    .line 683
    invoke-virtual/range {p0 .. p2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@60
    move-result v4

    #@61
    if-eqz v4, :cond_c

    #@63
    .line 687
    const/4 v3, -0x2

    #@64
    .line 688
    .local v3, width:I
    move-object/from16 v0, p1

    #@66
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@68
    if-eqz v4, :cond_70

    #@6a
    move-object/from16 v0, p1

    #@6c
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@6e
    if-eqz v4, :cond_163

    #@70
    .line 689
    :cond_70
    move-object/from16 v0, p1

    #@72
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@74
    if-nez v4, :cond_144

    #@76
    .line 691
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/policy/impl/PhoneWindow;->initializePanelDecor(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z

    #@79
    move-result v4

    #@7a
    if-eqz v4, :cond_c

    #@7c
    move-object/from16 v0, p1

    #@7e
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@80
    if-eqz v4, :cond_c

    #@82
    .line 699
    :cond_82
    :goto_82
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/policy/impl/PhoneWindow;->initializePanelContent(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z

    #@85
    move-result v4

    #@86
    if-eqz v4, :cond_c

    #@88
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->hasPanelItems()Z

    #@8b
    move-result v4

    #@8c
    if-eqz v4, :cond_c

    #@8e
    .line 703
    move-object/from16 v0, p1

    #@90
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@92
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@95
    move-result-object v2

    #@96
    .line 704
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    if-nez v2, :cond_9f

    #@98
    .line 705
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@9a
    .end local v2           #lp:Landroid/view/ViewGroup$LayoutParams;
    const/4 v4, -0x2

    #@9b
    const/4 v5, -0x2

    #@9c
    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@9f
    .line 709
    .restart local v2       #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_9f
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@a1
    const/4 v5, -0x1

    #@a2
    if-ne v4, v5, :cond_15d

    #@a4
    .line 712
    move-object/from16 v0, p1

    #@a6
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->fullBackground:I

    #@a8
    .line 713
    .local v10, backgroundResId:I
    const/4 v3, -0x1

    #@a9
    .line 718
    :goto_a9
    move-object/from16 v0, p1

    #@ab
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@ad
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@b0
    move-result-object v5

    #@b1
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v4, v5}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setWindowBackground(Landroid/graphics/drawable/Drawable;)V

    #@bc
    .line 721
    move-object/from16 v0, p1

    #@be
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@c0
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@c3
    move-result-object v16

    #@c4
    .line 722
    .local v16, shownPanelParent:Landroid/view/ViewParent;
    if-eqz v16, :cond_d7

    #@c6
    move-object/from16 v0, v16

    #@c8
    instance-of v4, v0, Landroid/view/ViewGroup;

    #@ca
    if-eqz v4, :cond_d7

    #@cc
    .line 723
    check-cast v16, Landroid/view/ViewGroup;

    #@ce
    .end local v16           #shownPanelParent:Landroid/view/ViewParent;
    move-object/from16 v0, p1

    #@d0
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@d2
    move-object/from16 v0, v16

    #@d4
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@d7
    .line 725
    :cond_d7
    move-object/from16 v0, p1

    #@d9
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@db
    move-object/from16 v0, p1

    #@dd
    iget-object v5, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@df
    invoke-virtual {v4, v5, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@e2
    .line 731
    move-object/from16 v0, p1

    #@e4
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@e6
    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    #@e9
    move-result v4

    #@ea
    if-nez v4, :cond_f3

    #@ec
    .line 732
    move-object/from16 v0, p1

    #@ee
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@f0
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    #@f3
    .line 745
    .end local v2           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v10           #backgroundResId:I
    :cond_f3
    :goto_f3
    const/4 v4, 0x1

    #@f4
    move-object/from16 v0, p1

    #@f6
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@f8
    .line 746
    const/4 v4, 0x0

    #@f9
    move-object/from16 v0, p1

    #@fb
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@fd
    .line 748
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    #@ff
    const/4 v4, -0x2

    #@100
    move-object/from16 v0, p1

    #@102
    iget v5, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->x:I

    #@104
    move-object/from16 v0, p1

    #@106
    iget v6, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->y:I

    #@108
    const/16 v7, 0x3eb

    #@10a
    const/high16 v8, 0x82

    #@10c
    move-object/from16 v0, p1

    #@10e
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@110
    iget v9, v9, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->mDefaultOpacity:I

    #@112
    invoke-direct/range {v2 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    #@115
    .line 756
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@117
    or-int/lit8 v4, v4, 0x2

    #@119
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@11b
    .line 757
    const v4, 0x3f19999a

    #@11e
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@120
    .line 760
    move-object/from16 v0, p1

    #@122
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isCompact:Z

    #@124
    if-eqz v4, :cond_183

    #@126
    .line 761
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getOptionsPanelGravity()I

    #@129
    move-result v4

    #@12a
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@12c
    .line 762
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindow;->sRotationWatcher:Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;

    #@12e
    move-object/from16 v0, p0

    #@130
    invoke-virtual {v4, v0}, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->addWindow(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@133
    .line 767
    :goto_133
    move-object/from16 v0, p1

    #@135
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->windowAnimations:I

    #@137
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@139
    .line 769
    move-object/from16 v0, p1

    #@13b
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@13d
    move-object/from16 v0, v17

    #@13f
    invoke-interface {v0, v4, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@142
    goto/16 :goto_c

    #@144
    .line 693
    .end local v2           #lp:Landroid/view/WindowManager$LayoutParams;
    :cond_144
    move-object/from16 v0, p1

    #@146
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@148
    if-eqz v4, :cond_82

    #@14a
    move-object/from16 v0, p1

    #@14c
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@14e
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getChildCount()I

    #@151
    move-result v4

    #@152
    if-lez v4, :cond_82

    #@154
    .line 695
    move-object/from16 v0, p1

    #@156
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@158
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->removeAllViews()V

    #@15b
    goto/16 :goto_82

    #@15d
    .line 716
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_15d
    move-object/from16 v0, p1

    #@15f
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->background:I

    #@161
    .restart local v10       #backgroundResId:I
    goto/16 :goto_a9

    #@163
    .line 734
    .end local v2           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v10           #backgroundResId:I
    :cond_163
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInListMode()Z

    #@166
    move-result v4

    #@167
    if-nez v4, :cond_16b

    #@169
    .line 735
    const/4 v3, -0x1

    #@16a
    goto :goto_f3

    #@16b
    .line 736
    :cond_16b
    move-object/from16 v0, p1

    #@16d
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@16f
    if-eqz v4, :cond_f3

    #@171
    .line 739
    move-object/from16 v0, p1

    #@173
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@175
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@178
    move-result-object v2

    #@179
    .line 740
    .restart local v2       #lp:Landroid/view/ViewGroup$LayoutParams;
    if-eqz v2, :cond_f3

    #@17b
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@17d
    const/4 v5, -0x1

    #@17e
    if-ne v4, v5, :cond_f3

    #@180
    .line 741
    const/4 v3, -0x1

    #@181
    goto/16 :goto_f3

    #@183
    .line 764
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    :cond_183
    move-object/from16 v0, p1

    #@185
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->gravity:I

    #@187
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@189
    goto :goto_133
.end method

.method private openPanelsAfterRestore()V
    .registers 5

    #@0
    .prologue
    .line 1930
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@2
    .line 1932
    .local v1, panels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez v1, :cond_5

    #@4
    .line 1950
    :cond_4
    return-void

    #@5
    .line 1937
    :cond_5
    array-length v3, v1

    #@6
    add-int/lit8 v0, v3, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_4

    #@a
    .line 1938
    aget-object v2, v1, v0

    #@c
    .line 1942
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v2, :cond_21

    #@e
    .line 1943
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->applyFrozenState()V

    #@11
    .line 1944
    iget-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@13
    if-nez v3, :cond_21

    #@15
    iget-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->wasLastOpen:Z

    #@17
    if-eqz v3, :cond_21

    #@19
    .line 1945
    iget-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->wasLastExpanded:Z

    #@1b
    iput-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@1d
    .line 1946
    const/4 v3, 0x0

    #@1e
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V

    #@21
    .line 1937
    :cond_21
    add-int/lit8 v0, v0, -0x1

    #@23
    goto :goto_8
.end method

.method private performPanelShortcut(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z
    .registers 8
    .parameter "st"
    .parameter "keyCode"
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1060
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_9

    #@7
    if-nez p1, :cond_b

    #@9
    .line 1061
    :cond_9
    const/4 v0, 0x0

    #@a
    .line 1083
    :cond_a
    :goto_a
    return v0

    #@b
    .line 1064
    :cond_b
    const/4 v0, 0x0

    #@c
    .line 1068
    .local v0, handled:Z
    iget-boolean v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@e
    if-nez v1, :cond_16

    #@10
    invoke-virtual {p0, p1, p3}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_20

    #@16
    :cond_16
    iget-object v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@18
    if-eqz v1, :cond_20

    #@1a
    .line 1070
    iget-object v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1c
    invoke-virtual {v1, p2, p3, p4}, Lcom/android/internal/view/menu/MenuBuilder;->performShortcut(ILandroid/view/KeyEvent;I)Z

    #@1f
    move-result v0

    #@20
    .line 1073
    :cond_20
    if-eqz v0, :cond_a

    #@22
    .line 1075
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@24
    .line 1078
    and-int/lit8 v1, p4, 0x1

    #@26
    if-nez v1, :cond_a

    #@28
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@2a
    if-nez v1, :cond_a

    #@2c
    .line 1079
    invoke-virtual {p0, p1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@2f
    goto :goto_a
.end method

.method private reopenMenu(Z)V
    .registers 9
    .parameter "toggleMenuMode"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 1135
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@6
    if-eqz v5, :cond_66

    #@8
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@a
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->isOverflowReserved()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_66

    #@10
    .line 1136
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@13
    move-result-object v0

    #@14
    .line 1137
    .local v0, cb:Landroid/view/Window$Callback;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@16
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->isOverflowMenuShowing()Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_1e

    #@1c
    if-nez p1, :cond_4f

    #@1e
    .line 1138
    :cond_1e
    if-eqz v0, :cond_4e

    #@20
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_4e

    #@26
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@28
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->getVisibility()I

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_4e

    #@2e
    .line 1139
    invoke-direct {p0, v4, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@31
    move-result-object v2

    #@32
    .line 1143
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-object v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@34
    if-eqz v3, :cond_4e

    #@36
    iget-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshMenuContent:Z

    #@38
    if-nez v3, :cond_4e

    #@3a
    iget-object v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@3c
    iget-object v5, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@3e
    invoke-interface {v0, v4, v3, v5}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_4e

    #@44
    .line 1145
    iget-object v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@46
    invoke-interface {v0, v6, v3}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    #@49
    .line 1146
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@4b
    invoke-virtual {v3}, Lcom/android/internal/widget/ActionBarView;->showOverflowMenu()Z

    #@4e
    .line 1171
    .end local v0           #cb:Landroid/view/Window$Callback;
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_4e
    :goto_4e
    return-void

    #@4f
    .line 1150
    .restart local v0       #cb:Landroid/view/Window$Callback;
    :cond_4f
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@51
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->hideOverflowMenu()Z

    #@54
    .line 1151
    if-eqz v0, :cond_4e

    #@56
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@59
    move-result v5

    #@5a
    if-nez v5, :cond_4e

    #@5c
    .line 1152
    invoke-direct {p0, v4, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@5f
    move-result-object v2

    #@60
    .line 1153
    .restart local v2       #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-object v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@62
    invoke-interface {v0, v6, v3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    #@65
    goto :goto_4e

    #@66
    .line 1159
    .end local v0           #cb:Landroid/view/Window$Callback;
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_66
    invoke-direct {p0, v4, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@69
    move-result-object v2

    #@6a
    .line 1162
    .restart local v2       #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz p1, :cond_7f

    #@6c
    iget-boolean v5, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@6e
    if-nez v5, :cond_7d

    #@70
    move v1, v3

    #@71
    .line 1164
    .local v1, newExpandedMode:Z
    :goto_71
    iput-boolean v3, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@73
    .line 1165
    invoke-virtual {p0, v2, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@76
    .line 1168
    iput-boolean v1, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@78
    .line 1170
    const/4 v3, 0x0

    #@79
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V

    #@7c
    goto :goto_4e

    #@7d
    .end local v1           #newExpandedMode:Z
    :cond_7d
    move v1, v4

    #@7e
    .line 1162
    goto :goto_71

    #@7f
    :cond_7f
    iget-boolean v1, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@81
    goto :goto_71
.end method

.method private restorePanelState(Landroid/util/SparseArray;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1906
    .local p1, icicles:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    #@3
    move-result v3

    #@4
    add-int/lit8 v1, v3, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_23

    #@8
    .line 1907
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@b
    move-result v0

    #@c
    .line 1908
    .local v0, curFeatureId:I
    const/4 v3, 0x0

    #@d
    invoke-direct {p0, v0, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@10
    move-result-object v2

    #@11
    .line 1909
    .local v2, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez v2, :cond_16

    #@13
    .line 1906
    :goto_13
    add-int/lit8 v1, v1, -0x1

    #@15
    goto :goto_6

    #@16
    .line 1914
    :cond_16
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Landroid/os/Parcelable;

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1f
    .line 1915
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->invalidatePanelMenu(I)V

    #@22
    goto :goto_13

    #@23
    .line 1922
    .end local v0           #curFeatureId:I
    .end local v2           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_23
    return-void
.end method

.method private savePanelState(Landroid/util/SparseArray;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1886
    .local p1, icicles:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@2
    .line 1887
    .local v1, panels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez v1, :cond_5

    #@4
    .line 1896
    :cond_4
    return-void

    #@5
    .line 1891
    :cond_5
    array-length v2, v1

    #@6
    add-int/lit8 v0, v2, -0x1

    #@8
    .local v0, curFeatureId:I
    :goto_8
    if-ltz v0, :cond_4

    #@a
    .line 1892
    aget-object v2, v1, v0

    #@c
    if-eqz v2, :cond_17

    #@e
    .line 1893
    aget-object v2, v1, v0

    #@10
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->onSaveInstanceState()Landroid/os/Parcelable;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@17
    .line 1891
    :cond_17
    add-int/lit8 v0, v0, -0x1

    #@19
    goto :goto_8
.end method

.method private setForceMode(Z)V
    .registers 7
    .parameter "forceNotFull"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 283
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mSetFlagsLocked:Z

    #@4
    .line 284
    if-eqz p1, :cond_34

    #@6
    .line 285
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@8
    .line 286
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@b
    move-result-object v0

    #@c
    .line 287
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPrevFlag:I

    #@10
    .line 288
    const/16 v2, 0x400

    #@12
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->clearFlags(I)V

    #@15
    .line 289
    const/16 v2, 0x200

    #@17
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->addFlags(I)V

    #@1a
    .line 290
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mStatusbarReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    if-eqz v2, :cond_31

    #@1e
    .line 291
    new-instance v1, Landroid/content/IntentFilter;

    #@20
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@23
    .line 292
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "com.lge.statusbar.collapsed"

    #@25
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@28
    .line 293
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mStatusbarReceiver:Landroid/content/BroadcastReceiver;

    #@2e
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@31
    .line 307
    .end local v1           #filter:Landroid/content/IntentFilter;
    :cond_31
    :goto_31
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mSetFlagsLocked:Z

    #@33
    .line 308
    return-void

    #@34
    .line 296
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_34
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@37
    move-result-object v0

    #@38
    .line 297
    .restart local v0       #attrs:Landroid/view/WindowManager$LayoutParams;
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPrevFlag:I

    #@3a
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@3c
    .line 298
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@3f
    .line 299
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@41
    .line 301
    :try_start_41
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mStatusbarReceiver:Landroid/content/BroadcastReceiver;

    #@43
    if-eqz v2, :cond_31

    #@45
    .line 302
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@48
    move-result-object v2

    #@49
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mStatusbarReceiver:Landroid/content/BroadcastReceiver;

    #@4b
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_4e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_41 .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_31

    #@4f
    .line 304
    :catch_4f
    move-exception v2

    #@50
    goto :goto_31
.end method

.method private showProgressBars(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V
    .registers 7
    .parameter "horizontalProgressBar"
    .parameter "spinnyProgressBar"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1499
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@4
    move-result v0

    #@5
    .line 1500
    .local v0, features:I
    and-int/lit8 v1, v0, 0x20

    #@7
    if-eqz v1, :cond_13

    #@9
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getVisibility()I

    #@c
    move-result v1

    #@d
    const/4 v2, 0x4

    #@e
    if-ne v1, v2, :cond_13

    #@10
    .line 1502
    invoke-virtual {p2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@13
    .line 1505
    :cond_13
    and-int/lit8 v1, v0, 0x4

    #@15
    if-eqz v1, :cond_22

    #@17
    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getProgress()I

    #@1a
    move-result v1

    #@1b
    const/16 v2, 0x2710

    #@1d
    if-ge v1, v2, :cond_22

    #@1f
    .line 1507
    invoke-virtual {p1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@22
    .line 1509
    :cond_22
    return-void
.end method

.method private updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V
    .registers 8
    .parameter "featureId"
    .parameter "st"
    .parameter "fromResume"

    #@0
    .prologue
    .line 3315
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3346
    :cond_4
    :goto_4
    return-void

    #@5
    .line 3319
    :cond_5
    const/4 v2, 0x1

    #@6
    shl-int v1, v2, p1

    #@8
    .line 3321
    .local v1, featureMask:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@b
    move-result v2

    #@c
    and-int/2addr v2, v1

    #@d
    if-nez v2, :cond_11

    #@f
    if-eqz p3, :cond_4

    #@11
    .line 3325
    :cond_11
    const/4 v0, 0x0

    #@12
    .line 3326
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz p2, :cond_1e

    #@14
    .line 3327
    iget-object v0, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->child:Landroid/graphics/drawable/Drawable;

    #@16
    .line 3328
    if-nez v0, :cond_1a

    #@18
    .line 3329
    iget-object v0, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->local:Landroid/graphics/drawable/Drawable;

    #@1a
    .line 3330
    :cond_1a
    if-nez v0, :cond_1e

    #@1c
    .line 3331
    iget-object v0, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->def:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 3333
    :cond_1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@21
    move-result v2

    #@22
    and-int/2addr v2, v1

    #@23
    if-nez v2, :cond_3b

    #@25
    .line 3334
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@28
    move-result-object v2

    #@29
    if-eqz v2, :cond_4

    #@2b
    .line 3335
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isActive()Z

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_33

    #@31
    if-eqz p3, :cond_4

    #@33
    .line 3336
    :cond_33
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, p1, v0}, Landroid/view/Window;->setChildDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@3a
    goto :goto_4

    #@3b
    .line 3339
    :cond_3b
    if-eqz p2, :cond_4

    #@3d
    iget-object v2, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->cur:Landroid/graphics/drawable/Drawable;

    #@3f
    if-ne v2, v0, :cond_47

    #@41
    iget v2, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->curAlpha:I

    #@43
    iget v3, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->alpha:I

    #@45
    if-eq v2, v3, :cond_4

    #@47
    .line 3342
    :cond_47
    iput-object v0, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->cur:Landroid/graphics/drawable/Drawable;

    #@49
    .line 3343
    iget v2, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->alpha:I

    #@4b
    iput v2, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->curAlpha:I

    #@4d
    .line 3344
    iget v2, p2, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->alpha:I

    #@4f
    invoke-virtual {p0, p1, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->onDrawableChanged(ILandroid/graphics/drawable/Drawable;I)V

    #@52
    goto :goto_4
.end method

.method private updateInt(IIZ)V
    .registers 6
    .parameter "featureId"
    .parameter "value"
    .parameter "fromResume"

    #@0
    .prologue
    .line 3352
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 3369
    :cond_4
    :goto_4
    return-void

    #@5
    .line 3356
    :cond_5
    const/4 v1, 0x1

    #@6
    shl-int v0, v1, p1

    #@8
    .line 3358
    .local v0, featureMask:I
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@b
    move-result v1

    #@c
    and-int/2addr v1, v0

    #@d
    if-nez v1, :cond_11

    #@f
    if-eqz p3, :cond_4

    #@11
    .line 3362
    :cond_11
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@14
    move-result v1

    #@15
    and-int/2addr v1, v0

    #@16
    if-nez v1, :cond_26

    #@18
    .line 3363
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@1b
    move-result-object v1

    #@1c
    if-eqz v1, :cond_4

    #@1e
    .line 3364
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p1, p2}, Landroid/view/Window;->setChildInt(II)V

    #@25
    goto :goto_4

    #@26
    .line 3367
    :cond_26
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->onIntChanged(II)V

    #@29
    goto :goto_4
.end method

.method private updateProgressBars(I)V
    .registers 12
    .parameter "value"

    #@0
    .prologue
    const/16 v9, 0x8

    #@2
    const/16 v8, 0x2710

    #@4
    const/4 v7, 0x1

    #@5
    const/4 v5, 0x0

    #@6
    .line 1454
    invoke-direct {p0, v7}, Lcom/android/internal/policy/impl/PhoneWindow;->getCircularProgressBar(Z)Landroid/widget/ProgressBar;

    #@9
    move-result-object v0

    #@a
    .line 1455
    .local v0, circularProgressBar:Landroid/widget/ProgressBar;
    invoke-direct {p0, v7}, Lcom/android/internal/policy/impl/PhoneWindow;->getHorizontalProgressBar(Z)Landroid/widget/ProgressBar;

    #@d
    move-result-object v2

    #@e
    .line 1457
    .local v2, horizontalProgressBar:Landroid/widget/ProgressBar;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@11
    move-result v1

    #@12
    .line 1458
    .local v1, features:I
    const/4 v6, -0x1

    #@13
    if-ne p1, v6, :cond_33

    #@15
    .line 1459
    and-int/lit8 v6, v1, 0x4

    #@17
    if-eqz v6, :cond_29

    #@19
    .line 1460
    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getProgress()I

    #@1c
    move-result v3

    #@1d
    .line 1461
    .local v3, level:I
    invoke-virtual {v2}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    #@20
    move-result v6

    #@21
    if-nez v6, :cond_25

    #@23
    if-ge v3, v8, :cond_31

    #@25
    :cond_25
    move v4, v5

    #@26
    .line 1463
    .local v4, visibility:I
    :goto_26
    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@29
    .line 1465
    .end local v3           #level:I
    .end local v4           #visibility:I
    :cond_29
    and-int/lit8 v6, v1, 0x20

    #@2b
    if-eqz v6, :cond_30

    #@2d
    .line 1466
    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@30
    .line 1496
    :cond_30
    :goto_30
    return-void

    #@31
    .line 1461
    .restart local v3       #level:I
    :cond_31
    const/4 v4, 0x4

    #@32
    goto :goto_26

    #@33
    .line 1468
    .end local v3           #level:I
    :cond_33
    const/4 v6, -0x2

    #@34
    if-ne p1, v6, :cond_45

    #@36
    .line 1469
    and-int/lit8 v5, v1, 0x4

    #@38
    if-eqz v5, :cond_3d

    #@3a
    .line 1470
    invoke-virtual {v2, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@3d
    .line 1472
    :cond_3d
    and-int/lit8 v5, v1, 0x20

    #@3f
    if-eqz v5, :cond_30

    #@41
    .line 1473
    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@44
    goto :goto_30

    #@45
    .line 1475
    :cond_45
    const/4 v6, -0x3

    #@46
    if-ne p1, v6, :cond_4c

    #@48
    .line 1476
    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    #@4b
    goto :goto_30

    #@4c
    .line 1477
    :cond_4c
    const/4 v6, -0x4

    #@4d
    if-ne p1, v6, :cond_53

    #@4f
    .line 1478
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    #@52
    goto :goto_30

    #@53
    .line 1479
    :cond_53
    if-ltz p1, :cond_66

    #@55
    if-gt p1, v8, :cond_66

    #@57
    .line 1483
    add-int/lit8 v5, p1, 0x0

    #@59
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@5c
    .line 1485
    if-ge p1, v8, :cond_62

    #@5e
    .line 1486
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->showProgressBars(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V

    #@61
    goto :goto_30

    #@62
    .line 1488
    :cond_62
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->hideProgressBars(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V

    #@65
    goto :goto_30

    #@66
    .line 1490
    :cond_66
    const/16 v5, 0x4e20

    #@68
    if-gt v5, p1, :cond_30

    #@6a
    const/16 v5, 0x7530

    #@6c
    if-gt p1, v5, :cond_30

    #@6e
    .line 1491
    add-int/lit16 v5, p1, -0x4e20

    #@70
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    #@73
    .line 1493
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->showProgressBars(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V

    #@76
    goto :goto_30
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 416
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2
    if-nez v1, :cond_7

    #@4
    .line 417
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@7
    .line 419
    :cond_7
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    invoke-virtual {v1, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@c
    .line 420
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@f
    move-result-object v0

    #@10
    .line 421
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1b

    #@12
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_1b

    #@18
    .line 422
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    #@1b
    .line 424
    :cond_1b
    return-void
.end method

.method public alwaysReadCloseOnTouchAttr()V
    .registers 2

    #@0
    .prologue
    .line 3119
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mAlwaysReadCloseOnTouchAttr:Z

    #@3
    .line 3120
    return-void
.end method

.method checkCloseActionMenu(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    #@0
    .prologue
    .line 840
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mClosingActionMenu:Z

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 851
    :goto_4
    return-void

    #@5
    .line 844
    :cond_5
    const/4 v1, 0x1

    #@6
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mClosingActionMenu:Z

    #@8
    .line 845
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@a
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarView;->dismissPopupMenus()V

    #@d
    .line 846
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@10
    move-result-object v0

    #@11
    .line 847
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1e

    #@13
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1e

    #@19
    .line 848
    const/16 v1, 0x8

    #@1b
    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    #@1e
    .line 850
    :cond_1e
    const/4 v1, 0x0

    #@1f
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mClosingActionMenu:Z

    #@21
    goto :goto_4
.end method

.method public final closeAllPanels()V
    .registers 7

    #@0
    .prologue
    .line 1012
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowManager()Landroid/view/WindowManager;

    #@3
    move-result-object v4

    #@4
    .line 1013
    .local v4, wm:Landroid/view/ViewManager;
    if-nez v4, :cond_7

    #@6
    .line 1027
    :goto_6
    return-void

    #@7
    .line 1017
    :cond_7
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@9
    .line 1018
    .local v3, panels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v3, :cond_1a

    #@b
    array-length v0, v3

    #@c
    .line 1019
    .local v0, N:I
    :goto_c
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_1c

    #@f
    .line 1020
    aget-object v2, v3, v1

    #@11
    .line 1021
    .local v2, panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v2, :cond_17

    #@13
    .line 1022
    const/4 v5, 0x1

    #@14
    invoke-virtual {p0, v2, v5}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@17
    .line 1019
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_d

    #@1a
    .line 1018
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_c

    #@1c
    .line 1026
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_1c
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->closeContextMenu()V

    #@1f
    goto :goto_6
.end method

.method public final closePanel(I)V
    .registers 4
    .parameter "featureId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 775
    if-nez p1, :cond_15

    #@3
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@5
    if-eqz v0, :cond_15

    #@7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->isOverflowReserved()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_15

    #@f
    .line 777
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->hideOverflowMenu()Z

    #@14
    .line 783
    :goto_14
    return-void

    #@15
    .line 778
    :cond_15
    const/4 v0, 0x6

    #@16
    if-ne p1, v0, :cond_1c

    #@18
    .line 779
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->closeContextMenu()V

    #@1b
    goto :goto_14

    #@1c
    .line 781
    :cond_1c
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@23
    goto :goto_14
.end method

.method public final closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V
    .registers 7
    .parameter "st"
    .parameter "doCallback"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 798
    if-eqz p2, :cond_1a

    #@4
    iget v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@6
    if-nez v1, :cond_1a

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@a
    if-eqz v1, :cond_1a

    #@c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@e
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarView;->isOverflowMenuShowing()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1a

    #@14
    .line 800
    iget-object v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@16
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->checkCloseActionMenu(Landroid/view/Menu;)V

    #@19
    .line 837
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 804
    :cond_1a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowManager()Landroid/view/WindowManager;

    #@1d
    move-result-object v0

    #@1e
    .line 805
    .local v0, wm:Landroid/view/ViewManager;
    if-eqz v0, :cond_3d

    #@20
    iget-boolean v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@22
    if-eqz v1, :cond_3d

    #@24
    .line 806
    iget-object v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@26
    if-eqz v1, :cond_36

    #@28
    .line 807
    iget-object v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2a
    invoke-interface {v0, v1}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    #@2d
    .line 809
    iget-boolean v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isCompact:Z

    #@2f
    if-eqz v1, :cond_36

    #@31
    .line 810
    sget-object v1, Lcom/android/internal/policy/impl/PhoneWindow;->sRotationWatcher:Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;

    #@33
    invoke-virtual {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindow$RotationWatcher;->removeWindow(Lcom/android/internal/policy/impl/PhoneWindow;)V

    #@36
    .line 814
    :cond_36
    if-eqz p2, :cond_3d

    #@38
    .line 815
    iget v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@3a
    invoke-direct {p0, v1, p1, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->callOnPanelClosed(ILcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/Menu;)V

    #@3d
    .line 819
    :cond_3d
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@3f
    .line 820
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@41
    .line 821
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@43
    .line 824
    iput-object v3, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@45
    .line 826
    iget-boolean v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@47
    if-eqz v1, :cond_4e

    #@49
    .line 829
    const/4 v1, 0x1

    #@4a
    iput-boolean v1, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@4c
    .line 830
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@4e
    .line 833
    :cond_4e
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@50
    if-ne v1, p1, :cond_19

    #@52
    .line 834
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@54
    .line 835
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@56
    goto :goto_19
.end method

.method doInvalidatePanelMenu(I)V
    .registers 7
    .parameter "featureId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 874
    invoke-direct {p0, p1, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@5
    move-result-object v1

    #@6
    .line 875
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    const/4 v0, 0x0

    #@7
    .line 876
    .local v0, savedActionViewStates:Landroid/os/Bundle;
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@9
    if-eqz v2, :cond_27

    #@b
    .line 877
    new-instance v0, Landroid/os/Bundle;

    #@d
    .end local v0           #savedActionViewStates:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@10
    .line 878
    .restart local v0       #savedActionViewStates:Landroid/os/Bundle;
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@12
    invoke-virtual {v2, v0}, Lcom/android/internal/view/menu/MenuBuilder;->saveActionViewStates(Landroid/os/Bundle;)V

    #@15
    .line 879
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    #@18
    move-result v2

    #@19
    if-lez v2, :cond_1d

    #@1b
    .line 880
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->frozenActionViewState:Landroid/os/Bundle;

    #@1d
    .line 883
    :cond_1d
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1f
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@22
    .line 884
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@24
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->clear()V

    #@27
    .line 886
    :cond_27
    iput-boolean v4, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshMenuContent:Z

    #@29
    .line 887
    iput-boolean v4, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    #@2b
    .line 890
    const/16 v2, 0x8

    #@2d
    if-eq p1, v2, :cond_31

    #@2f
    if-nez p1, :cond_41

    #@31
    :cond_31
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@33
    if-eqz v2, :cond_41

    #@35
    .line 892
    invoke-direct {p0, v3, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@38
    move-result-object v1

    #@39
    .line 893
    if-eqz v1, :cond_41

    #@3b
    .line 894
    iput-boolean v3, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@3d
    .line 895
    const/4 v2, 0x0

    #@3e
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@41
    .line 898
    :cond_41
    return-void
.end method

.method public findMenuPanel(Landroid/view/Menu;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    .registers 7
    .parameter "menu"

    #@0
    .prologue
    .line 1108
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@2
    .line 1109
    .local v3, panels:[Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v3, :cond_11

    #@4
    array-length v0, v3

    #@5
    .line 1110
    .local v0, N:I
    :goto_5
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    if-ge v1, v0, :cond_16

    #@8
    .line 1111
    aget-object v2, v3, v1

    #@a
    .line 1112
    .local v2, panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v2, :cond_13

    #@c
    iget-object v4, v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@e
    if-ne v4, p1, :cond_13

    #@10
    .line 1116
    .end local v2           #panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :goto_10
    return-object v2

    #@11
    .line 1109
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_5

    #@13
    .line 1110
    .restart local v0       #N:I
    .restart local v1       #i:I
    .restart local v2       #panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_6

    #@16
    .line 1116
    .end local v2           #panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_10
.end method

.method protected generateDecor()Lcom/android/internal/policy/impl/PhoneWindow$DecorView;
    .registers 4

    #@0
    .prologue
    .line 2836
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const/4 v2, -0x1

    #@7
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;Landroid/content/Context;I)V

    #@a
    return-object v0
.end method

.method protected generateLayout(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Landroid/view/ViewGroup;
    .registers 24
    .parameter "decor"

    #@0
    .prologue
    .line 2857
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowStyle()Landroid/content/res/TypedArray;

    #@3
    move-result-object v3

    #@4
    .line 2869
    .local v3, a:Landroid/content/res/TypedArray;
    const/16 v19, 0x4

    #@6
    const/16 v20, 0x0

    #@8
    move/from16 v0, v19

    #@a
    move/from16 v1, v20

    #@c
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f
    move-result v19

    #@10
    move/from16 v0, v19

    #@12
    move-object/from16 v1, p0

    #@14
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@16
    .line 2870
    const v19, 0x10100

    #@19
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@1c
    move-result v20

    #@1d
    xor-int/lit8 v20, v20, -0x1

    #@1f
    and-int v8, v19, v20

    #@21
    .line 2872
    .local v8, flagsToUpdate:I
    move-object/from16 v0, p0

    #@23
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@25
    move/from16 v19, v0

    #@27
    if-eqz v19, :cond_3b3

    #@29
    .line 2873
    const/16 v19, -0x2

    #@2b
    const/16 v20, -0x2

    #@2d
    move-object/from16 v0, p0

    #@2f
    move/from16 v1, v19

    #@31
    move/from16 v2, v20

    #@33
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->setLayout(II)V

    #@36
    .line 2874
    const/16 v19, 0x0

    #@38
    move-object/from16 v0, p0

    #@3a
    move/from16 v1, v19

    #@3c
    invoke-virtual {v0, v1, v8}, Lcom/android/internal/policy/impl/PhoneWindow;->setFlags(II)V

    #@3f
    .line 2879
    :goto_3f
    const/16 v19, 0x3

    #@41
    const/16 v20, 0x0

    #@43
    move/from16 v0, v19

    #@45
    move/from16 v1, v20

    #@47
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4a
    move-result v19

    #@4b
    if-eqz v19, :cond_3bf

    #@4d
    .line 2880
    const/16 v19, 0x1

    #@4f
    move-object/from16 v0, p0

    #@51
    move/from16 v1, v19

    #@53
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->requestFeature(I)Z

    #@56
    .line 2886
    :cond_56
    :goto_56
    const/16 v19, 0x11

    #@58
    const/16 v20, 0x0

    #@5a
    move/from16 v0, v19

    #@5c
    move/from16 v1, v20

    #@5e
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@61
    move-result v19

    #@62
    if-eqz v19, :cond_6d

    #@64
    .line 2887
    const/16 v19, 0x9

    #@66
    move-object/from16 v0, p0

    #@68
    move/from16 v1, v19

    #@6a
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->requestFeature(I)Z

    #@6d
    .line 2890
    :cond_6d
    const/16 v19, 0x10

    #@6f
    const/16 v20, 0x0

    #@71
    move/from16 v0, v19

    #@73
    move/from16 v1, v20

    #@75
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@78
    move-result v19

    #@79
    if-eqz v19, :cond_84

    #@7b
    .line 2891
    const/16 v19, 0xa

    #@7d
    move-object/from16 v0, p0

    #@7f
    move/from16 v1, v19

    #@81
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->requestFeature(I)Z

    #@84
    .line 2894
    :cond_84
    const/16 v19, 0x9

    #@86
    const/16 v20, 0x0

    #@88
    move/from16 v0, v19

    #@8a
    move/from16 v1, v20

    #@8c
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@8f
    move-result v19

    #@90
    if-eqz v19, :cond_a9

    #@92
    .line 2895
    const/16 v19, 0x400

    #@94
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@97
    move-result v20

    #@98
    xor-int/lit8 v20, v20, -0x1

    #@9a
    move/from16 v0, v20

    #@9c
    and-int/lit16 v0, v0, 0x400

    #@9e
    move/from16 v20, v0

    #@a0
    move-object/from16 v0, p0

    #@a2
    move/from16 v1, v19

    #@a4
    move/from16 v2, v20

    #@a6
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->setFlags(II)V

    #@a9
    .line 2898
    :cond_a9
    const/16 v19, 0xe

    #@ab
    const/16 v20, 0x0

    #@ad
    move/from16 v0, v19

    #@af
    move/from16 v1, v20

    #@b1
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@b4
    move-result v19

    #@b5
    if-eqz v19, :cond_cc

    #@b7
    .line 2899
    const/high16 v19, 0x10

    #@b9
    const/high16 v20, 0x10

    #@bb
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@be
    move-result v21

    #@bf
    xor-int/lit8 v21, v21, -0x1

    #@c1
    and-int v20, v20, v21

    #@c3
    move-object/from16 v0, p0

    #@c5
    move/from16 v1, v19

    #@c7
    move/from16 v2, v20

    #@c9
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->setFlags(II)V

    #@cc
    .line 2902
    :cond_cc
    const/16 v20, 0x12

    #@ce
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@d1
    move-result-object v19

    #@d2
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@d5
    move-result-object v19

    #@d6
    move-object/from16 v0, v19

    #@d8
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@da
    move/from16 v19, v0

    #@dc
    const/16 v21, 0xb

    #@de
    move/from16 v0, v19

    #@e0
    move/from16 v1, v21

    #@e2
    if-lt v0, v1, :cond_3d8

    #@e4
    const/16 v19, 0x1

    #@e6
    :goto_e6
    move/from16 v0, v20

    #@e8
    move/from16 v1, v19

    #@ea
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ed
    move-result v19

    #@ee
    if-eqz v19, :cond_105

    #@f0
    .line 2905
    const/high16 v19, 0x80

    #@f2
    const/high16 v20, 0x80

    #@f4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@f7
    move-result v21

    #@f8
    xor-int/lit8 v21, v21, -0x1

    #@fa
    and-int v20, v20, v21

    #@fc
    move-object/from16 v0, p0

    #@fe
    move/from16 v1, v19

    #@100
    move/from16 v2, v20

    #@102
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->setFlags(II)V

    #@105
    .line 2908
    :cond_105
    const/16 v19, 0x13

    #@107
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMajor:Landroid/util/TypedValue;

    #@10b
    move-object/from16 v20, v0

    #@10d
    move/from16 v0, v19

    #@10f
    move-object/from16 v1, v20

    #@111
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@114
    .line 2909
    const/16 v19, 0x14

    #@116
    move-object/from16 v0, p0

    #@118
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMinWidthMinor:Landroid/util/TypedValue;

    #@11a
    move-object/from16 v20, v0

    #@11c
    move/from16 v0, v19

    #@11e
    move-object/from16 v1, v20

    #@120
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@123
    .line 2912
    const/16 v19, 0x17

    #@125
    move-object/from16 v0, p0

    #@127
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@129
    move-object/from16 v20, v0

    #@12b
    move/from16 v0, v19

    #@12d
    move-object/from16 v1, v20

    #@12f
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@132
    .line 2913
    const/16 v19, 0x18

    #@134
    move-object/from16 v0, p0

    #@136
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@138
    move-object/from16 v20, v0

    #@13a
    move/from16 v0, v19

    #@13c
    move-object/from16 v1, v20

    #@13e
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@141
    .line 2916
    const/16 v19, 0x19

    #@143
    move/from16 v0, v19

    #@145
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@148
    move-result v19

    #@149
    if-eqz v19, :cond_16d

    #@14b
    .line 2917
    move-object/from16 v0, p0

    #@14d
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMajor:Landroid/util/TypedValue;

    #@14f
    move-object/from16 v19, v0

    #@151
    if-nez v19, :cond_15e

    #@153
    new-instance v19, Landroid/util/TypedValue;

    #@155
    invoke-direct/range {v19 .. v19}, Landroid/util/TypedValue;-><init>()V

    #@158
    move-object/from16 v0, v19

    #@15a
    move-object/from16 v1, p0

    #@15c
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMajor:Landroid/util/TypedValue;

    #@15e
    .line 2918
    :cond_15e
    const/16 v19, 0x19

    #@160
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMajor:Landroid/util/TypedValue;

    #@164
    move-object/from16 v20, v0

    #@166
    move/from16 v0, v19

    #@168
    move-object/from16 v1, v20

    #@16a
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@16d
    .line 2921
    :cond_16d
    const/16 v19, 0x1b

    #@16f
    move/from16 v0, v19

    #@171
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@174
    move-result v19

    #@175
    if-eqz v19, :cond_199

    #@177
    .line 2922
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMinor:Landroid/util/TypedValue;

    #@17b
    move-object/from16 v19, v0

    #@17d
    if-nez v19, :cond_18a

    #@17f
    new-instance v19, Landroid/util/TypedValue;

    #@181
    invoke-direct/range {v19 .. v19}, Landroid/util/TypedValue;-><init>()V

    #@184
    move-object/from16 v0, v19

    #@186
    move-object/from16 v1, p0

    #@188
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMinor:Landroid/util/TypedValue;

    #@18a
    .line 2923
    :cond_18a
    const/16 v19, 0x1b

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedWidthMinor:Landroid/util/TypedValue;

    #@190
    move-object/from16 v20, v0

    #@192
    move/from16 v0, v19

    #@194
    move-object/from16 v1, v20

    #@196
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@199
    .line 2926
    :cond_199
    const/16 v19, 0x1c

    #@19b
    move/from16 v0, v19

    #@19d
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@1a0
    move-result v19

    #@1a1
    if-eqz v19, :cond_1c5

    #@1a3
    .line 2927
    move-object/from16 v0, p0

    #@1a5
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMajor:Landroid/util/TypedValue;

    #@1a7
    move-object/from16 v19, v0

    #@1a9
    if-nez v19, :cond_1b6

    #@1ab
    new-instance v19, Landroid/util/TypedValue;

    #@1ad
    invoke-direct/range {v19 .. v19}, Landroid/util/TypedValue;-><init>()V

    #@1b0
    move-object/from16 v0, v19

    #@1b2
    move-object/from16 v1, p0

    #@1b4
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMajor:Landroid/util/TypedValue;

    #@1b6
    .line 2928
    :cond_1b6
    const/16 v19, 0x1c

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMajor:Landroid/util/TypedValue;

    #@1bc
    move-object/from16 v20, v0

    #@1be
    move/from16 v0, v19

    #@1c0
    move-object/from16 v1, v20

    #@1c2
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@1c5
    .line 2931
    :cond_1c5
    const/16 v19, 0x1a

    #@1c7
    move/from16 v0, v19

    #@1c9
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@1cc
    move-result v19

    #@1cd
    if-eqz v19, :cond_1f1

    #@1cf
    .line 2932
    move-object/from16 v0, p0

    #@1d1
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMinor:Landroid/util/TypedValue;

    #@1d3
    move-object/from16 v19, v0

    #@1d5
    if-nez v19, :cond_1e2

    #@1d7
    new-instance v19, Landroid/util/TypedValue;

    #@1d9
    invoke-direct/range {v19 .. v19}, Landroid/util/TypedValue;-><init>()V

    #@1dc
    move-object/from16 v0, v19

    #@1de
    move-object/from16 v1, p0

    #@1e0
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMinor:Landroid/util/TypedValue;

    #@1e2
    .line 2933
    :cond_1e2
    const/16 v19, 0x1a

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFixedHeightMinor:Landroid/util/TypedValue;

    #@1e8
    move-object/from16 v20, v0

    #@1ea
    move/from16 v0, v19

    #@1ec
    move-object/from16 v1, v20

    #@1ee
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@1f1
    .line 2937
    :cond_1f1
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@1f4
    move-result-object v5

    #@1f5
    .line 2938
    .local v5, context:Landroid/content/Context;
    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@1f8
    move-result-object v19

    #@1f9
    move-object/from16 v0, v19

    #@1fb
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@1fd
    move/from16 v18, v0

    #@1ff
    .line 2939
    .local v18, targetSdk:I
    const/16 v19, 0xb

    #@201
    move/from16 v0, v18

    #@203
    move/from16 v1, v19

    #@205
    if-ge v0, v1, :cond_3dc

    #@207
    const/16 v16, 0x1

    #@209
    .line 2940
    .local v16, targetPreHoneycomb:Z
    :goto_209
    const/16 v19, 0xe

    #@20b
    move/from16 v0, v18

    #@20d
    move/from16 v1, v19

    #@20f
    if-ge v0, v1, :cond_3e0

    #@211
    const/16 v17, 0x1

    #@213
    .line 2941
    .local v17, targetPreIcs:Z
    :goto_213
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@216
    move-result-object v19

    #@217
    const v20, 0x111000a

    #@21a
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@21d
    move-result v15

    #@21e
    .line 2943
    .local v15, targetHcNeedsOptions:Z
    const/16 v19, 0x8

    #@220
    move-object/from16 v0, p0

    #@222
    move/from16 v1, v19

    #@224
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->hasFeature(I)Z

    #@227
    move-result v19

    #@228
    if-eqz v19, :cond_236

    #@22a
    const/16 v19, 0x1

    #@22c
    move-object/from16 v0, p0

    #@22e
    move/from16 v1, v19

    #@230
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->hasFeature(I)Z

    #@233
    move-result v19

    #@234
    if-eqz v19, :cond_3e4

    #@236
    :cond_236
    const/4 v11, 0x1

    #@237
    .line 2945
    .local v11, noActionBar:Z
    :goto_237
    if-nez v16, :cond_23f

    #@239
    if-eqz v17, :cond_3e7

    #@23b
    if-eqz v15, :cond_3e7

    #@23d
    if-eqz v11, :cond_3e7

    #@23f
    .line 2946
    :cond_23f
    const/high16 v19, 0x800

    #@241
    move-object/from16 v0, p0

    #@243
    move/from16 v1, v19

    #@245
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->addFlags(I)V

    #@248
    .line 2951
    :goto_248
    move-object/from16 v0, p0

    #@24a
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mAlwaysReadCloseOnTouchAttr:Z

    #@24c
    move/from16 v19, v0

    #@24e
    if-nez v19, :cond_266

    #@250
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@253
    move-result-object v19

    #@254
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@257
    move-result-object v19

    #@258
    move-object/from16 v0, v19

    #@25a
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@25c
    move/from16 v19, v0

    #@25e
    const/16 v20, 0xb

    #@260
    move/from16 v0, v19

    #@262
    move/from16 v1, v20

    #@264
    if-lt v0, v1, :cond_27d

    #@266
    .line 2953
    :cond_266
    const/16 v19, 0x15

    #@268
    const/16 v20, 0x0

    #@26a
    move/from16 v0, v19

    #@26c
    move/from16 v1, v20

    #@26e
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@271
    move-result v19

    #@272
    if-eqz v19, :cond_27d

    #@274
    .line 2956
    const/16 v19, 0x1

    #@276
    move-object/from16 v0, p0

    #@278
    move/from16 v1, v19

    #@27a
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->setCloseOnTouchOutsideIfNotSet(Z)V

    #@27d
    .line 2960
    :cond_27d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@280
    move-result-object v12

    #@281
    .line 2962
    .local v12, params:Landroid/view/WindowManager$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->hasSoftInputMode()Z

    #@284
    move-result v19

    #@285
    if-nez v19, :cond_299

    #@287
    .line 2963
    const/16 v19, 0xd

    #@289
    iget v0, v12, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@28b
    move/from16 v20, v0

    #@28d
    move/from16 v0, v19

    #@28f
    move/from16 v1, v20

    #@291
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@294
    move-result v19

    #@295
    move/from16 v0, v19

    #@297
    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@299
    .line 2968
    :cond_299
    const/16 v19, 0xb

    #@29b
    move-object/from16 v0, p0

    #@29d
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@29f
    move/from16 v20, v0

    #@2a1
    move/from16 v0, v19

    #@2a3
    move/from16 v1, v20

    #@2a5
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2a8
    move-result v19

    #@2a9
    if-eqz v19, :cond_2d3

    #@2ab
    .line 2971
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@2ae
    move-result v19

    #@2af
    and-int/lit8 v19, v19, 0x2

    #@2b1
    if-nez v19, :cond_2bd

    #@2b3
    .line 2972
    iget v0, v12, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2b5
    move/from16 v19, v0

    #@2b7
    or-int/lit8 v19, v19, 0x2

    #@2b9
    move/from16 v0, v19

    #@2bb
    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2bd
    .line 2974
    :cond_2bd
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->haveDimAmount()Z

    #@2c0
    move-result v19

    #@2c1
    if-nez v19, :cond_2d3

    #@2c3
    .line 2975
    const/16 v19, 0x0

    #@2c5
    const/high16 v20, 0x3f00

    #@2c7
    move/from16 v0, v19

    #@2c9
    move/from16 v1, v20

    #@2cb
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@2ce
    move-result v19

    #@2cf
    move/from16 v0, v19

    #@2d1
    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@2d3
    .line 2980
    :cond_2d3
    iget v0, v12, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@2d5
    move/from16 v19, v0

    #@2d7
    if-nez v19, :cond_2e9

    #@2d9
    .line 2981
    const/16 v19, 0x8

    #@2db
    const/16 v20, 0x0

    #@2dd
    move/from16 v0, v19

    #@2df
    move/from16 v1, v20

    #@2e1
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2e4
    move-result v19

    #@2e5
    move/from16 v0, v19

    #@2e7
    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@2e9
    .line 2987
    :cond_2e9
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@2ec
    move-result-object v19

    #@2ed
    if-nez v19, :cond_33d

    #@2ef
    .line 2988
    move-object/from16 v0, p0

    #@2f1
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@2f3
    move-object/from16 v19, v0

    #@2f5
    if-nez v19, :cond_32b

    #@2f7
    .line 2989
    move-object/from16 v0, p0

    #@2f9
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@2fb
    move/from16 v19, v0

    #@2fd
    if-nez v19, :cond_311

    #@2ff
    .line 2990
    const/16 v19, 0x1

    #@301
    const/16 v20, 0x0

    #@303
    move/from16 v0, v19

    #@305
    move/from16 v1, v20

    #@307
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@30a
    move-result v19

    #@30b
    move/from16 v0, v19

    #@30d
    move-object/from16 v1, p0

    #@30f
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@311
    .line 2993
    :cond_311
    move-object/from16 v0, p0

    #@313
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFrameResource:I

    #@315
    move/from16 v19, v0

    #@317
    if-nez v19, :cond_32b

    #@319
    .line 2994
    const/16 v19, 0x2

    #@31b
    const/16 v20, 0x0

    #@31d
    move/from16 v0, v19

    #@31f
    move/from16 v1, v20

    #@321
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@324
    move-result v19

    #@325
    move/from16 v0, v19

    #@327
    move-object/from16 v1, p0

    #@329
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mFrameResource:I

    #@32b
    .line 3002
    :cond_32b
    const/16 v19, 0x7

    #@32d
    const/high16 v20, -0x100

    #@32f
    move/from16 v0, v19

    #@331
    move/from16 v1, v20

    #@333
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@336
    move-result v19

    #@337
    move/from16 v0, v19

    #@339
    move-object/from16 v1, p0

    #@33b
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mTextColor:I

    #@33d
    .line 3008
    :cond_33d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLocalFeatures()I

    #@340
    move-result v7

    #@341
    .line 3010
    .local v7, features:I
    and-int/lit8 v19, v7, 0x18

    #@343
    if-eqz v19, :cond_3f7

    #@345
    .line 3011
    move-object/from16 v0, p0

    #@347
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@349
    move/from16 v19, v0

    #@34b
    if-eqz v19, :cond_3f2

    #@34d
    .line 3012
    new-instance v14, Landroid/util/TypedValue;

    #@34f
    invoke-direct {v14}, Landroid/util/TypedValue;-><init>()V

    #@352
    .line 3013
    .local v14, res:Landroid/util/TypedValue;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@355
    move-result-object v19

    #@356
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@359
    move-result-object v19

    #@35a
    const v20, 0x10103e7

    #@35d
    const/16 v21, 0x1

    #@35f
    move-object/from16 v0, v19

    #@361
    move/from16 v1, v20

    #@363
    move/from16 v2, v21

    #@365
    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@368
    .line 3015
    iget v10, v14, Landroid/util/TypedValue;->resourceId:I

    #@36a
    .line 3020
    .end local v14           #res:Landroid/util/TypedValue;
    .local v10, layoutResource:I
    :goto_36a
    const/16 v19, 0x8

    #@36c
    move-object/from16 v0, p0

    #@36e
    move/from16 v1, v19

    #@370
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->removeFeature(I)V

    #@373
    .line 3067
    :goto_373
    move-object/from16 v0, p0

    #@375
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@377
    move-object/from16 v19, v0

    #@379
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->startChanging()V

    #@37c
    .line 3069
    move-object/from16 v0, p0

    #@37e
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@380
    move-object/from16 v19, v0

    #@382
    const/16 v20, 0x0

    #@384
    move-object/from16 v0, v19

    #@386
    move-object/from16 v1, v20

    #@388
    invoke-virtual {v0, v10, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@38b
    move-result-object v9

    #@38c
    .line 3070
    .local v9, in:Landroid/view/View;
    new-instance v19, Landroid/view/ViewGroup$LayoutParams;

    #@38e
    const/16 v20, -0x1

    #@390
    const/16 v21, -0x1

    #@392
    invoke-direct/range {v19 .. v21}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@395
    move-object/from16 v0, p1

    #@397
    move-object/from16 v1, v19

    #@399
    invoke-virtual {v0, v9, v1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@39c
    .line 3072
    const v19, 0x1020002

    #@39f
    move-object/from16 v0, p0

    #@3a1
    move/from16 v1, v19

    #@3a3
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@3a6
    move-result-object v4

    #@3a7
    check-cast v4, Landroid/view/ViewGroup;

    #@3a9
    .line 3073
    .local v4, contentParent:Landroid/view/ViewGroup;
    if-nez v4, :cond_496

    #@3ab
    .line 3074
    new-instance v19, Ljava/lang/RuntimeException;

    #@3ad
    const-string v20, "Window couldn\'t find content container view"

    #@3af
    invoke-direct/range {v19 .. v20}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3b2
    throw v19

    #@3b3
    .line 2876
    .end local v4           #contentParent:Landroid/view/ViewGroup;
    .end local v5           #context:Landroid/content/Context;
    .end local v7           #features:I
    .end local v9           #in:Landroid/view/View;
    .end local v10           #layoutResource:I
    .end local v11           #noActionBar:Z
    .end local v12           #params:Landroid/view/WindowManager$LayoutParams;
    .end local v15           #targetHcNeedsOptions:Z
    .end local v16           #targetPreHoneycomb:Z
    .end local v17           #targetPreIcs:Z
    .end local v18           #targetSdk:I
    :cond_3b3
    const v19, 0x10100

    #@3b6
    move-object/from16 v0, p0

    #@3b8
    move/from16 v1, v19

    #@3ba
    invoke-virtual {v0, v1, v8}, Lcom/android/internal/policy/impl/PhoneWindow;->setFlags(II)V

    #@3bd
    goto/16 :goto_3f

    #@3bf
    .line 2881
    :cond_3bf
    const/16 v19, 0xf

    #@3c1
    const/16 v20, 0x0

    #@3c3
    move/from16 v0, v19

    #@3c5
    move/from16 v1, v20

    #@3c7
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3ca
    move-result v19

    #@3cb
    if-eqz v19, :cond_56

    #@3cd
    .line 2883
    const/16 v19, 0x8

    #@3cf
    move-object/from16 v0, p0

    #@3d1
    move/from16 v1, v19

    #@3d3
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->requestFeature(I)Z

    #@3d6
    goto/16 :goto_56

    #@3d8
    .line 2902
    :cond_3d8
    const/16 v19, 0x0

    #@3da
    goto/16 :goto_e6

    #@3dc
    .line 2939
    .restart local v5       #context:Landroid/content/Context;
    .restart local v18       #targetSdk:I
    :cond_3dc
    const/16 v16, 0x0

    #@3de
    goto/16 :goto_209

    #@3e0
    .line 2940
    .restart local v16       #targetPreHoneycomb:Z
    :cond_3e0
    const/16 v17, 0x0

    #@3e2
    goto/16 :goto_213

    #@3e4
    .line 2943
    .restart local v15       #targetHcNeedsOptions:Z
    .restart local v17       #targetPreIcs:Z
    :cond_3e4
    const/4 v11, 0x0

    #@3e5
    goto/16 :goto_237

    #@3e7
    .line 2948
    .restart local v11       #noActionBar:Z
    :cond_3e7
    const/high16 v19, 0x800

    #@3e9
    move-object/from16 v0, p0

    #@3eb
    move/from16 v1, v19

    #@3ed
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->clearFlags(I)V

    #@3f0
    goto/16 :goto_248

    #@3f2
    .line 3017
    .restart local v7       #features:I
    .restart local v12       #params:Landroid/view/WindowManager$LayoutParams;
    :cond_3f2
    const v10, 0x10900c0

    #@3f5
    .restart local v10       #layoutResource:I
    goto/16 :goto_36a

    #@3f7
    .line 3022
    .end local v10           #layoutResource:I
    :cond_3f7
    and-int/lit8 v19, v7, 0x24

    #@3f9
    if-eqz v19, :cond_406

    #@3fb
    and-int/lit16 v0, v7, 0x100

    #@3fd
    move/from16 v19, v0

    #@3ff
    if-nez v19, :cond_406

    #@401
    .line 3026
    const v10, 0x10900bc

    #@404
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@406
    .line 3028
    .end local v10           #layoutResource:I
    :cond_406
    and-int/lit16 v0, v7, 0x80

    #@408
    move/from16 v19, v0

    #@40a
    if-eqz v19, :cond_440

    #@40c
    .line 3031
    move-object/from16 v0, p0

    #@40e
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@410
    move/from16 v19, v0

    #@412
    if-eqz v19, :cond_43c

    #@414
    .line 3032
    new-instance v14, Landroid/util/TypedValue;

    #@416
    invoke-direct {v14}, Landroid/util/TypedValue;-><init>()V

    #@419
    .line 3033
    .restart local v14       #res:Landroid/util/TypedValue;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@41c
    move-result-object v19

    #@41d
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@420
    move-result-object v19

    #@421
    const v20, 0x10103e8

    #@424
    const/16 v21, 0x1

    #@426
    move-object/from16 v0, v19

    #@428
    move/from16 v1, v20

    #@42a
    move/from16 v2, v21

    #@42c
    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@42f
    .line 3035
    iget v10, v14, Landroid/util/TypedValue;->resourceId:I

    #@431
    .line 3040
    .end local v14           #res:Landroid/util/TypedValue;
    .restart local v10       #layoutResource:I
    :goto_431
    const/16 v19, 0x8

    #@433
    move-object/from16 v0, p0

    #@435
    move/from16 v1, v19

    #@437
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->removeFeature(I)V

    #@43a
    goto/16 :goto_373

    #@43c
    .line 3037
    .end local v10           #layoutResource:I
    :cond_43c
    const v10, 0x10900bb

    #@43f
    .restart local v10       #layoutResource:I
    goto :goto_431

    #@440
    .line 3041
    .end local v10           #layoutResource:I
    :cond_440
    and-int/lit8 v19, v7, 0x2

    #@442
    if-nez v19, :cond_486

    #@444
    .line 3044
    move-object/from16 v0, p0

    #@446
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@448
    move/from16 v19, v0

    #@44a
    if-eqz v19, :cond_46b

    #@44c
    .line 3045
    new-instance v14, Landroid/util/TypedValue;

    #@44e
    invoke-direct {v14}, Landroid/util/TypedValue;-><init>()V

    #@451
    .line 3046
    .restart local v14       #res:Landroid/util/TypedValue;
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@454
    move-result-object v19

    #@455
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@458
    move-result-object v19

    #@459
    const v20, 0x10103e9

    #@45c
    const/16 v21, 0x1

    #@45e
    move-object/from16 v0, v19

    #@460
    move/from16 v1, v20

    #@462
    move/from16 v2, v21

    #@464
    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@467
    .line 3048
    iget v10, v14, Landroid/util/TypedValue;->resourceId:I

    #@469
    .line 3049
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@46b
    .end local v10           #layoutResource:I
    .end local v14           #res:Landroid/util/TypedValue;
    :cond_46b
    and-int/lit16 v0, v7, 0x100

    #@46d
    move/from16 v19, v0

    #@46f
    if-eqz v19, :cond_481

    #@471
    .line 3050
    and-int/lit16 v0, v7, 0x200

    #@473
    move/from16 v19, v0

    #@475
    if-eqz v19, :cond_47c

    #@477
    .line 3051
    const v10, 0x10900ba

    #@47a
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@47c
    .line 3053
    .end local v10           #layoutResource:I
    :cond_47c
    const v10, 0x10900b9

    #@47f
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@481
    .line 3056
    .end local v10           #layoutResource:I
    :cond_481
    const v10, 0x10900bf

    #@484
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@486
    .line 3059
    .end local v10           #layoutResource:I
    :cond_486
    and-int/lit16 v0, v7, 0x400

    #@488
    move/from16 v19, v0

    #@48a
    if-eqz v19, :cond_491

    #@48c
    .line 3060
    const v10, 0x10900be

    #@48f
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@491
    .line 3063
    .end local v10           #layoutResource:I
    :cond_491
    const v10, 0x10900bd

    #@494
    .restart local v10       #layoutResource:I
    goto/16 :goto_373

    #@496
    .line 3077
    .restart local v4       #contentParent:Landroid/view/ViewGroup;
    .restart local v9       #in:Landroid/view/View;
    :cond_496
    and-int/lit8 v19, v7, 0x20

    #@498
    if-eqz v19, :cond_4ad

    #@49a
    .line 3078
    const/16 v19, 0x0

    #@49c
    move-object/from16 v0, p0

    #@49e
    move/from16 v1, v19

    #@4a0
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getCircularProgressBar(Z)Landroid/widget/ProgressBar;

    #@4a3
    move-result-object v13

    #@4a4
    .line 3079
    .local v13, progress:Landroid/widget/ProgressBar;
    if-eqz v13, :cond_4ad

    #@4a6
    .line 3080
    const/16 v19, 0x1

    #@4a8
    move/from16 v0, v19

    #@4aa
    invoke-virtual {v13, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    #@4ad
    .line 3086
    .end local v13           #progress:Landroid/widget/ProgressBar;
    :cond_4ad
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContainer()Landroid/view/Window;

    #@4b0
    move-result-object v19

    #@4b1
    if-nez v19, :cond_538

    #@4b3
    .line 3087
    move-object/from16 v0, p0

    #@4b5
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@4b7
    .line 3088
    .local v6, drawable:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    #@4b9
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@4bb
    move/from16 v19, v0

    #@4bd
    if-eqz v19, :cond_4d1

    #@4bf
    .line 3089
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@4c2
    move-result-object v19

    #@4c3
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4c6
    move-result-object v19

    #@4c7
    move-object/from16 v0, p0

    #@4c9
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@4cb
    move/from16 v20, v0

    #@4cd
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4d0
    move-result-object v6

    #@4d1
    .line 3091
    :cond_4d1
    move-object/from16 v0, p0

    #@4d3
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@4d5
    move-object/from16 v19, v0

    #@4d7
    move-object/from16 v0, v19

    #@4d9
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setWindowBackground(Landroid/graphics/drawable/Drawable;)V

    #@4dc
    .line 3092
    const/4 v6, 0x0

    #@4dd
    .line 3093
    move-object/from16 v0, p0

    #@4df
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFrameResource:I

    #@4e1
    move/from16 v19, v0

    #@4e3
    if-eqz v19, :cond_4f7

    #@4e5
    .line 3094
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@4e8
    move-result-object v19

    #@4e9
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4ec
    move-result-object v19

    #@4ed
    move-object/from16 v0, p0

    #@4ef
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mFrameResource:I

    #@4f1
    move/from16 v20, v0

    #@4f3
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4f6
    move-result-object v6

    #@4f7
    .line 3096
    :cond_4f7
    move-object/from16 v0, p0

    #@4f9
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@4fb
    move-object/from16 v19, v0

    #@4fd
    move-object/from16 v0, v19

    #@4ff
    invoke-virtual {v0, v6}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setWindowFrame(Landroid/graphics/drawable/Drawable;)V

    #@502
    .line 3102
    move-object/from16 v0, p0

    #@504
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleColor:I

    #@506
    move/from16 v19, v0

    #@508
    if-nez v19, :cond_516

    #@50a
    .line 3103
    move-object/from16 v0, p0

    #@50c
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTextColor:I

    #@50e
    move/from16 v19, v0

    #@510
    move/from16 v0, v19

    #@512
    move-object/from16 v1, p0

    #@514
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleColor:I

    #@516
    .line 3106
    :cond_516
    move-object/from16 v0, p0

    #@518
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@51a
    move-object/from16 v19, v0

    #@51c
    if-eqz v19, :cond_52b

    #@51e
    .line 3107
    move-object/from16 v0, p0

    #@520
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@522
    move-object/from16 v19, v0

    #@524
    move-object/from16 v0, p0

    #@526
    move-object/from16 v1, v19

    #@528
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->setTitle(Ljava/lang/CharSequence;)V

    #@52b
    .line 3109
    :cond_52b
    move-object/from16 v0, p0

    #@52d
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleColor:I

    #@52f
    move/from16 v19, v0

    #@531
    move-object/from16 v0, p0

    #@533
    move/from16 v1, v19

    #@535
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->setTitleColor(I)V

    #@538
    .line 3112
    .end local v6           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_538
    move-object/from16 v0, p0

    #@53a
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@53c
    move-object/from16 v19, v0

    #@53e
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->finishChanging()V

    #@541
    .line 3114
    return-object v4
.end method

.method getAudioManager()Landroid/media/AudioManager;
    .registers 3

    #@0
    .prologue
    .line 1691
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mAudioManager:Landroid/media/AudioManager;

    #@2
    if-nez v0, :cond_12

    #@4
    .line 1692
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@7
    move-result-object v0

    #@8
    const-string v1, "audio"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/media/AudioManager;

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mAudioManager:Landroid/media/AudioManager;

    #@12
    .line 1694
    :cond_12
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mAudioManager:Landroid/media/AudioManager;

    #@14
    return-object v0
.end method

.method public getCurrentFocus()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->findFocus()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final getDecorView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1777
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 1778
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@7
    .line 1780
    :cond_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@9
    return-object v0
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 453
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@2
    return-object v0
.end method

.method public getVolumeControlStream()I
    .registers 2

    #@0
    .prologue
    .line 3482
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mVolumeControlStreamType:I

    #@2
    return v0
.end method

.method protected initializePanelContent(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z
    .registers 8
    .parameter "st"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1261
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@4
    if-eqz v2, :cond_c

    #@6
    .line 1262
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@8
    iput-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@a
    move v2, v3

    #@b
    .line 1288
    :goto_b
    return v2

    #@c
    .line 1266
    :cond_c
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@e
    if-nez v2, :cond_12

    #@10
    move v2, v4

    #@11
    .line 1267
    goto :goto_b

    #@12
    .line 1270
    :cond_12
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

    #@14
    if-nez v2, :cond_1e

    #@16
    .line 1271
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

    #@18
    const/4 v5, 0x0

    #@19
    invoke-direct {v2, p0, v5}, Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$1;)V

    #@1c
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

    #@1e
    .line 1274
    :cond_1e
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInListMode()Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_41

    #@24
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@27
    move-result-object v2

    #@28
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

    #@2a
    invoke-virtual {p1, v2, v5}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->getListMenuView(Landroid/content/Context;Lcom/android/internal/view/menu/MenuPresenter$Callback;)Lcom/android/internal/view/menu/MenuView;

    #@2d
    move-result-object v1

    #@2e
    .local v1, menuView:Lcom/android/internal/view/menu/MenuView;
    :goto_2e
    move-object v2, v1

    #@2f
    .line 1278
    check-cast v2, Landroid/view/View;

    #@31
    iput-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@33
    .line 1280
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    #@35
    if-eqz v2, :cond_4c

    #@37
    .line 1282
    invoke-interface {v1}, Lcom/android/internal/view/menu/MenuView;->getWindowAnimations()I

    #@3a
    move-result v0

    #@3b
    .line 1283
    .local v0, defaultAnimations:I
    if-eqz v0, :cond_3f

    #@3d
    .line 1284
    iput v0, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->windowAnimations:I

    #@3f
    :cond_3f
    move v2, v3

    #@40
    .line 1286
    goto :goto_b

    #@41
    .line 1274
    .end local v0           #defaultAnimations:I
    .end local v1           #menuView:Lcom/android/internal/view/menu/MenuView;
    :cond_41
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@44
    move-result-object v2

    #@45
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$PanelMenuPresenterCallback;

    #@47
    invoke-virtual {p1, v2, v5}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->getIconMenuView(Landroid/content/Context;Lcom/android/internal/view/menu/MenuPresenter$Callback;)Lcom/android/internal/view/menu/MenuView;

    #@4a
    move-result-object v1

    #@4b
    goto :goto_2e

    #@4c
    .restart local v1       #menuView:Lcom/android/internal/view/menu/MenuView;
    :cond_4c
    move v2, v4

    #@4d
    .line 1288
    goto :goto_b
.end method

.method protected initializePanelDecor(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z
    .registers 5
    .parameter "st"

    #@0
    .prologue
    .line 1215
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    iget v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@8
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;Landroid/content/Context;I)V

    #@b
    iput-object v0, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@d
    .line 1216
    const/16 v0, 0x51

    #@f
    iput v0, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->gravity:I

    #@11
    .line 1217
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->setStyle(Landroid/content/Context;)V

    #@18
    .line 1219
    const/4 v0, 0x1

    #@19
    return v0
.end method

.method protected initializePanelMenu(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z
    .registers 11
    .parameter "st"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 1183
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@4
    move-result-object v0

    #@5
    .line 1186
    .local v0, context:Landroid/content/Context;
    iget v6, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@7
    if-eqz v6, :cond_f

    #@9
    iget v6, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@b
    const/16 v7, 0x8

    #@d
    if-ne v6, v7, :cond_32

    #@f
    :cond_f
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@11
    if-eqz v6, :cond_32

    #@13
    .line 1188
    new-instance v4, Landroid/util/TypedValue;

    #@15
    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    #@18
    .line 1189
    .local v4, outValue:Landroid/util/TypedValue;
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@1b
    move-result-object v2

    #@1c
    .line 1190
    .local v2, currentTheme:Landroid/content/res/Resources$Theme;
    const v6, 0x1010397

    #@1f
    invoke-virtual {v2, v6, v4, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@22
    .line 1192
    iget v5, v4, Landroid/util/TypedValue;->resourceId:I

    #@24
    .line 1194
    .local v5, targetThemeRes:I
    if-eqz v5, :cond_32

    #@26
    invoke-virtual {v0}, Landroid/content/Context;->getThemeResId()I

    #@29
    move-result v6

    #@2a
    if-eq v6, v5, :cond_32

    #@2c
    .line 1195
    new-instance v1, Landroid/view/ContextThemeWrapper;

    #@2e
    invoke-direct {v1, v0, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    #@31
    .end local v0           #context:Landroid/content/Context;
    .local v1, context:Landroid/content/Context;
    move-object v0, v1

    #@32
    .line 1199
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #currentTheme:Landroid/content/res/Resources$Theme;
    .end local v4           #outValue:Landroid/util/TypedValue;
    .end local v5           #targetThemeRes:I
    .restart local v0       #context:Landroid/content/Context;
    :cond_32
    new-instance v3, Lcom/android/internal/view/menu/MenuBuilder;

    #@34
    invoke-direct {v3, v0}, Lcom/android/internal/view/menu/MenuBuilder;-><init>(Landroid/content/Context;)V

    #@37
    .line 1201
    .local v3, menu:Lcom/android/internal/view/menu/MenuBuilder;
    invoke-virtual {v3, p0}, Lcom/android/internal/view/menu/MenuBuilder;->setCallback(Lcom/android/internal/view/menu/MenuBuilder$Callback;)V

    #@3a
    .line 1202
    invoke-virtual {p1, v3}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->setMenu(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@3d
    .line 1204
    return v8
.end method

.method public invalidatePanelMenu(I)V
    .registers 5
    .parameter "featureId"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 865
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuFeatures:I

    #@3
    shl-int v1, v2, p1

    #@5
    or-int/2addr v0, v1

    #@6
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuFeatures:I

    #@8
    .line 867
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuPosted:Z

    #@a
    if-nez v0, :cond_19

    #@c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@e
    if-eqz v0, :cond_19

    #@10
    .line 868
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@12
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuRunnable:Ljava/lang/Runnable;

    #@14
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@17
    .line 869
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mInvalidatePanelMenuPosted:Z

    #@19
    .line 871
    :cond_19
    return-void
.end method

.method public isFloating()Z
    .registers 2

    #@0
    .prologue
    .line 442
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFloating:Z

    #@2
    return v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 3308
    invoke-direct {p0, v2, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@5
    move-result-object v0

    #@6
    .line 3309
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8
    if-eqz v3, :cond_13

    #@a
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@c
    invoke-virtual {v3, p1, p2}, Lcom/android/internal/view/menu/MenuBuilder;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_13

    #@12
    :goto_12
    return v1

    #@13
    :cond_13
    move v1, v2

    #@14
    goto :goto_12
.end method

.method protected onActive()V
    .registers 1

    #@0
    .prologue
    .line 1773
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "newConfig"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 582
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@3
    if-nez v1, :cond_13

    #@5
    .line 583
    invoke-direct {p0, v2, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@8
    move-result-object v0

    #@9
    .line 584
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v0, :cond_13

    #@b
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@d
    if-eqz v1, :cond_13

    #@f
    .line 585
    iget-boolean v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@11
    if-eqz v1, :cond_14

    #@13
    .line 620
    .end local v0           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 616
    .restart local v0       #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_14
    invoke-static {v0}, Lcom/android/internal/policy/impl/PhoneWindow;->clearMenuViews(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)V

    #@17
    goto :goto_13
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    #@0
    .prologue
    .line 336
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onDrawableChanged(ILandroid/graphics/drawable/Drawable;I)V
    .registers 6
    .parameter "featureId"
    .parameter "drawable"
    .parameter "alpha"

    #@0
    .prologue
    .line 1399
    const/4 v1, 0x3

    #@1
    if-ne p1, v1, :cond_14

    #@3
    .line 1400
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getLeftIconView()Landroid/widget/ImageView;

    #@6
    move-result-object v0

    #@7
    .line 1407
    .local v0, view:Landroid/widget/ImageView;
    :goto_7
    if-eqz p2, :cond_1c

    #@9
    .line 1408
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@c
    .line 1409
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 1410
    const/4 v1, 0x0

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@13
    .line 1414
    .end local v0           #view:Landroid/widget/ImageView;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1401
    :cond_14
    const/4 v1, 0x4

    #@15
    if-ne p1, v1, :cond_13

    #@17
    .line 1402
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getRightIconView()Landroid/widget/ImageView;

    #@1a
    move-result-object v0

    #@1b
    .restart local v0       #view:Landroid/widget/ImageView;
    goto :goto_7

    #@1c
    .line 1412
    :cond_1c
    const/16 v1, 0x8

    #@1e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@21
    goto :goto_13
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 8
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 312
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@3
    if-nez v1, :cond_28

    #@5
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getForcedWindowFlags()I

    #@8
    move-result v1

    #@9
    xor-int/lit8 v1, v1, -0x1

    #@b
    and-int/lit16 v1, v1, 0x400

    #@d
    if-nez v1, :cond_28

    #@f
    .line 313
    cmpl-float v1, p4, p3

    #@11
    if-lez v1, :cond_28

    #@13
    const v1, 0x451c4000

    #@16
    cmpl-float v1, p4, v1

    #@18
    if-lez v1, :cond_28

    #@1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    #@1d
    move-result v1

    #@1e
    const/high16 v2, 0x4248

    #@20
    cmpg-float v1, v1, v2

    #@22
    if-gez v1, :cond_28

    #@24
    .line 314
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@27
    .line 318
    :goto_27
    return v0

    #@28
    :cond_28
    const/4 v0, 0x0

    #@29
    goto :goto_27
.end method

.method protected onIntChanged(II)V
    .registers 5
    .parameter "featureId"
    .parameter "value"

    #@0
    .prologue
    .line 1424
    const/4 v1, 0x2

    #@1
    if-eq p1, v1, :cond_6

    #@3
    const/4 v1, 0x5

    #@4
    if-ne p1, v1, :cond_a

    #@6
    .line 1425
    :cond_6
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->updateProgressBars(I)V

    #@9
    .line 1432
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1426
    :cond_a
    const/4 v1, 0x7

    #@b
    if-ne p1, v1, :cond_9

    #@d
    .line 1427
    const v1, 0x102024e

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->findViewById(I)Landroid/view/View;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/widget/FrameLayout;

    #@16
    .line 1428
    .local v0, titleContainer:Landroid/widget/FrameLayout;
    if-eqz v0, :cond_9

    #@18
    .line 1429
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@1a
    invoke-virtual {v1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1d
    goto :goto_9
.end method

.method protected onKeyDown(IILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "featureId"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 1645
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@4
    if-eqz v3, :cond_10

    #@6
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@8
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@b
    move-result-object v0

    #@c
    .line 1650
    .local v0, dispatcher:Landroid/view/KeyEvent$DispatcherState;
    :goto_c
    sparse-switch p2, :sswitch_data_34

    #@f
    .line 1679
    .end local p1
    :cond_f
    :goto_f
    return v1

    #@10
    .line 1645
    .end local v0           #dispatcher:Landroid/view/KeyEvent$DispatcherState;
    .restart local p1
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_c

    #@12
    .line 1658
    .restart local v0       #dispatcher:Landroid/view/KeyEvent$DispatcherState;
    :sswitch_12
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAudioManager()Landroid/media/AudioManager;

    #@15
    move-result-object v1

    #@16
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mVolumeControlStreamType:I

    #@18
    invoke-virtual {v1, p3, v3}, Landroid/media/AudioManager;->handleKeyDown(Landroid/view/KeyEvent;I)V

    #@1b
    move v1, v2

    #@1c
    .line 1659
    goto :goto_f

    #@1d
    .line 1663
    :sswitch_1d
    if-gez p1, :cond_20

    #@1f
    move p1, v1

    #@20
    .end local p1
    :cond_20
    invoke-virtual {p0, p1, p3}, Lcom/android/internal/policy/impl/PhoneWindow;->onKeyDownPanel(ILandroid/view/KeyEvent;)Z

    #@23
    move v1, v2

    #@24
    .line 1664
    goto :goto_f

    #@25
    .line 1668
    .restart local p1
    :sswitch_25
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@28
    move-result v3

    #@29
    if-gtz v3, :cond_f

    #@2b
    .line 1669
    if-ltz p1, :cond_f

    #@2d
    .line 1671
    if-eqz v0, :cond_32

    #@2f
    .line 1672
    invoke-virtual {v0, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 1674
    goto :goto_f

    #@34
    .line 1650
    :sswitch_data_34
    .sparse-switch
        0x4 -> :sswitch_25
        0x18 -> :sswitch_12
        0x19 -> :sswitch_12
        0x52 -> :sswitch_1d
        0xa4 -> :sswitch_12
    .end sparse-switch
.end method

.method public final onKeyDownPanel(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "featureId"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 907
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v0

    #@6
    .line 909
    .local v0, keyCode:I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_2f

    #@c
    .line 911
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@e
    .line 912
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@10
    .line 914
    invoke-direct {p0, p1, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@13
    move-result-object v1

    #@14
    .line 915
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-boolean v3, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@16
    if-nez v3, :cond_2e

    #@18
    .line 917
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@23
    move-result-object v2

    #@24
    iget v2, v2, Landroid/content/res/Configuration;->keyboard:I

    #@26
    if-ne v2, v4, :cond_2a

    #@28
    .line 919
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@2a
    .line 921
    :cond_2a
    invoke-virtual {p0, v1, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@2d
    move-result v2

    #@2e
    .line 931
    .end local v1           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_2e
    :goto_2e
    return v2

    #@2f
    .line 923
    :cond_2f
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@31
    if-eqz v3, :cond_2e

    #@33
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@35
    if-ne v3, v0, :cond_2e

    #@37
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    #@3a
    move-result v3

    #@3b
    and-int/lit16 v3, v3, 0x80

    #@3d
    if-eqz v3, :cond_2e

    #@3f
    .line 927
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@41
    .line 928
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@43
    goto :goto_2e
.end method

.method protected onKeyUp(IILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "featureId"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1704
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@4
    if-eqz v4, :cond_15

    #@6
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@8
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@b
    move-result-object v0

    #@c
    .line 1706
    .local v0, dispatcher:Landroid/view/KeyEvent$DispatcherState;
    :goto_c
    if-eqz v0, :cond_11

    #@e
    .line 1707
    invoke-virtual {v0, p3}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@11
    .line 1712
    :cond_11
    sparse-switch p2, :sswitch_data_72

    #@14
    .line 1768
    .end local p1
    :cond_14
    :goto_14
    return v2

    #@15
    .line 1704
    .end local v0           #dispatcher:Landroid/view/KeyEvent$DispatcherState;
    .restart local p1
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_c

    #@17
    .line 1721
    .restart local v0       #dispatcher:Landroid/view/KeyEvent$DispatcherState;
    :sswitch_17
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    #@1a
    move-result v2

    #@1b
    and-int/lit8 v2, v2, 0x20

    #@1d
    if-nez v2, :cond_28

    #@1f
    .line 1722
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAudioManager()Landroid/media/AudioManager;

    #@22
    move-result-object v2

    #@23
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mVolumeControlStreamType:I

    #@25
    invoke-virtual {v2, p3, v4}, Landroid/media/AudioManager;->handleKeyUp(Landroid/view/KeyEvent;I)V

    #@28
    :cond_28
    move v2, v3

    #@29
    .line 1726
    goto :goto_14

    #@2a
    .line 1730
    :sswitch_2a
    if-gez p1, :cond_2d

    #@2c
    move p1, v2

    #@2d
    .end local p1
    :cond_2d
    invoke-virtual {p0, p1, p3}, Lcom/android/internal/policy/impl/PhoneWindow;->onKeyUpPanel(ILandroid/view/KeyEvent;)V

    #@30
    move v2, v3

    #@31
    .line 1732
    goto :goto_14

    #@32
    .line 1736
    .restart local p1
    :sswitch_32
    if-ltz p1, :cond_14

    #@34
    .line 1737
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_14

    #@3a
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    #@3d
    move-result v4

    #@3e
    if-nez v4, :cond_14

    #@40
    .line 1738
    if-nez p1, :cond_51

    #@42
    .line 1739
    invoke-direct {p0, p1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@45
    move-result-object v1

    #@46
    .line 1740
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v1, :cond_51

    #@48
    iget-boolean v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    #@4a
    if-eqz v2, :cond_51

    #@4c
    .line 1743
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->reopenMenu(Z)V

    #@4f
    move v2, v3

    #@50
    .line 1744
    goto :goto_14

    #@51
    .line 1747
    .end local v1           #st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :cond_51
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(I)V

    #@54
    move v2, v3

    #@55
    .line 1748
    goto :goto_14

    #@56
    .line 1758
    :sswitch_56
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getKeyguardManager()Landroid/app/KeyguardManager;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    #@5d
    move-result v4

    #@5e
    if-nez v4, :cond_14

    #@60
    .line 1761
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    #@63
    move-result v2

    #@64
    if-eqz v2, :cond_6f

    #@66
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    #@69
    move-result v2

    #@6a
    if-nez v2, :cond_6f

    #@6c
    .line 1762
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->launchDefaultSearch()Z

    #@6f
    :cond_6f
    move v2, v3

    #@70
    .line 1764
    goto :goto_14

    #@71
    .line 1712
    nop

    #@72
    :sswitch_data_72
    .sparse-switch
        0x4 -> :sswitch_32
        0x18 -> :sswitch_17
        0x19 -> :sswitch_17
        0x52 -> :sswitch_2a
        0x54 -> :sswitch_56
        0xa4 -> :sswitch_17
    .end sparse-switch
.end method

.method public final onKeyUpPanel(ILandroid/view/KeyEvent;)V
    .registers 11
    .parameter "featureId"
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 941
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@4
    if-eqz v5, :cond_1c

    #@6
    .line 942
    iput v7, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelChordingKey:I

    #@8
    .line 943
    iput-boolean v7, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPanelMayLongPress:Z

    #@a
    .line 945
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@d
    move-result v5

    #@e
    if-nez v5, :cond_1c

    #@10
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@12
    if-eqz v5, :cond_1d

    #@14
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@16
    invoke-static {v5}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->access$500(Lcom/android/internal/policy/impl/PhoneWindow$DecorView;)Landroid/view/ActionMode;

    #@19
    move-result-object v5

    #@1a
    if-eqz v5, :cond_1d

    #@1c
    .line 1008
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 949
    :cond_1d
    const/4 v2, 0x0

    #@1e
    .line 950
    .local v2, playSoundEffect:Z
    invoke-direct {p0, p1, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@21
    move-result-object v4

    #@22
    .line 952
    .local v4, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez p1, :cond_7c

    #@24
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@26
    if-eqz v5, :cond_7c

    #@28
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@2a
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->isOverflowReserved()Z

    #@2d
    move-result v5

    #@2e
    if-eqz v5, :cond_7c

    #@30
    .line 954
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@32
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->getVisibility()I

    #@35
    move-result v5

    #@36
    if-nez v5, :cond_52

    #@38
    .line 955
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@3a
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->isOverflowMenuShowing()Z

    #@3d
    move-result v5

    #@3e
    if-nez v5, :cond_75

    #@40
    .line 956
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@43
    move-result v5

    #@44
    if-nez v5, :cond_52

    #@46
    invoke-virtual {p0, v4, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_52

    #@4c
    .line 957
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@4e
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->showOverflowMenu()Z

    #@51
    move-result v2

    #@52
    .line 995
    :cond_52
    :goto_52
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContext:Landroid/content/Context;

    #@54
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@57
    move-result-object v5

    #@58
    const v6, 0x111003c

    #@5b
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@5e
    move-result v1

    #@5f
    .line 998
    .local v1, mHasNavigationBar:Z
    if-eqz v2, :cond_1c

    #@61
    if-nez v1, :cond_1c

    #@63
    .line 999
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@66
    move-result-object v5

    #@67
    const-string v6, "audio"

    #@69
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6c
    move-result-object v0

    #@6d
    check-cast v0, Landroid/media/AudioManager;

    #@6f
    .line 1001
    .local v0, audioManager:Landroid/media/AudioManager;
    if-eqz v0, :cond_b2

    #@71
    .line 1002
    invoke-virtual {v0, v7}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@74
    goto :goto_1c

    #@75
    .line 960
    .end local v0           #audioManager:Landroid/media/AudioManager;
    .end local v1           #mHasNavigationBar:Z
    :cond_75
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@77
    invoke-virtual {v5}, Lcom/android/internal/widget/ActionBarView;->hideOverflowMenu()Z

    #@7a
    move-result v2

    #@7b
    goto :goto_52

    #@7c
    .line 964
    :cond_7c
    iget-boolean v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@7e
    if-nez v5, :cond_84

    #@80
    iget-boolean v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@82
    if-eqz v5, :cond_8a

    #@84
    .line 968
    :cond_84
    iget-boolean v2, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@86
    .line 971
    invoke-virtual {p0, v4, v6}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@89
    goto :goto_52

    #@8a
    .line 973
    :cond_8a
    iget-boolean v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@8c
    if-eqz v5, :cond_52

    #@8e
    .line 974
    const/4 v3, 0x1

    #@8f
    .line 975
    .local v3, show:Z
    iget-boolean v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshMenuContent:Z

    #@91
    if-eqz v5, :cond_99

    #@93
    .line 978
    iput-boolean v7, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@95
    .line 979
    invoke-virtual {p0, v4, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@98
    move-result v3

    #@99
    .line 983
    :cond_99
    if-eqz v3, :cond_52

    #@9b
    iget-object v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@9d
    if-eqz v5, :cond_52

    #@9f
    iget-object v5, v4, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@a1
    invoke-virtual {v5}, Lcom/android/internal/view/menu/MenuBuilder;->hasNonActionItems()Z

    #@a4
    move-result v5

    #@a5
    if-eqz v5, :cond_52

    #@a7
    .line 986
    const v5, 0xc351

    #@aa
    invoke-static {v5, v7}, Landroid/util/EventLog;->writeEvent(II)I

    #@ad
    .line 989
    invoke-direct {p0, v4, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V

    #@b0
    .line 991
    const/4 v2, 0x1

    #@b1
    goto :goto_52

    #@b2
    .line 1004
    .end local v3           #show:Z
    .restart local v0       #audioManager:Landroid/media/AudioManager;
    .restart local v1       #mHasNavigationBar:Z
    :cond_b2
    const-string v5, "PhoneWindow"

    #@b4
    const-string v6, "Couldn\'t get audio manager"

    #@b6
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto/16 :goto_1c
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    #@0
    .prologue
    .line 322
    return-void
.end method

.method public onMenuItemSelected(Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 1120
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@3
    move-result-object v0

    #@4
    .line 1121
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1d

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_1d

    #@c
    .line 1122
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuBuilder;->getRootMenu()Lcom/android/internal/view/menu/MenuBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->findMenuPanel(Landroid/view/Menu;)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@13
    move-result-object v1

    #@14
    .line 1123
    .local v1, panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-eqz v1, :cond_1d

    #@16
    .line 1124
    iget v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@18
    invoke-interface {v0, v2, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    #@1b
    move-result v2

    #@1c
    .line 1127
    .end local v1           #panel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    :goto_1c
    return v2

    #@1d
    :cond_1d
    const/4 v2, 0x0

    #@1e
    goto :goto_1c
.end method

.method public onMenuModeChange(Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 1131
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->reopenMenu(Z)V

    #@4
    .line 1132
    return-void
.end method

.method onOptionsPanelRotationChanged()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1238
    invoke-direct {p0, v3, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@4
    move-result-object v1

    #@5
    .line 1239
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    if-nez v1, :cond_8

    #@7
    .line 1250
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1241
    :cond_8
    iget-object v3, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@a
    if-eqz v3, :cond_29

    #@c
    iget-object v3, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@e
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Landroid/view/WindowManager$LayoutParams;

    #@14
    move-object v0, v3

    #@15
    .line 1243
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    :goto_15
    if-eqz v0, :cond_7

    #@17
    .line 1244
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getOptionsPanelGravity()I

    #@1a
    move-result v3

    #@1b
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1d
    .line 1245
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getWindowManager()Landroid/view/WindowManager;

    #@20
    move-result-object v2

    #@21
    .line 1246
    .local v2, wm:Landroid/view/ViewManager;
    if-eqz v2, :cond_7

    #@23
    .line 1247
    iget-object v3, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@25
    invoke-interface {v2, v3, v0}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@28
    goto :goto_7

    #@29
    .line 1241
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    .end local v2           #wm:Landroid/view/ViewManager;
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_15
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter "e1"
    .parameter "e2"
    .parameter "distanceX"
    .parameter "distanceY"

    #@0
    .prologue
    .line 325
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    #@0
    .prologue
    .line 329
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    #@0
    .prologue
    .line 332
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final openPanel(ILandroid/view/KeyEvent;)V
    .registers 4
    .parameter "featureId"
    .parameter "event"

    #@0
    .prologue
    .line 637
    if-nez p1, :cond_1c

    #@2
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@4
    if-eqz v0, :cond_1c

    #@6
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->isOverflowReserved()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_1c

    #@e
    .line 639
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@10
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->getVisibility()I

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1b

    #@16
    .line 640
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->showOverflowMenu()Z

    #@1b
    .line 645
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 643
    :cond_1c
    const/4 v0, 0x1

    #@1d
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@20
    move-result-object v0

    #@21
    invoke-direct {p0, v0, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V

    #@24
    goto :goto_1b
.end method

.method public final peekDecorView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1785
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    return-object v0
.end method

.method public performContextMenuIdentifierAction(II)Z
    .registers 4
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    .line 1294
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContextMenu:Lcom/android/internal/view/menu/ContextMenuBuilder;

    #@6
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/view/menu/ContextMenuBuilder;->performIdentifierAction(II)Z

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public performPanelIdentifierAction(III)Z
    .registers 9
    .parameter "featureId"
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1089
    invoke-direct {p0, p1, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@5
    move-result-object v1

    #@6
    .line 1090
    .local v1, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    new-instance v2, Landroid/view/KeyEvent;

    #@8
    const/16 v3, 0x52

    #@a
    invoke-direct {v2, v0, v3}, Landroid/view/KeyEvent;-><init>(II)V

    #@d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_14

    #@13
    .line 1104
    :cond_13
    :goto_13
    return v0

    #@14
    .line 1093
    :cond_14
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@16
    if-eqz v2, :cond_13

    #@18
    .line 1097
    iget-object v2, v1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1a
    invoke-virtual {v2, p2, p3}, Lcom/android/internal/view/menu/MenuBuilder;->performIdentifierAction(II)Z

    #@1d
    move-result v0

    #@1e
    .line 1100
    .local v0, res:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@20
    if-nez v2, :cond_13

    #@22
    .line 1101
    invoke-virtual {p0, v1, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@25
    goto :goto_13
.end method

.method public performPanelShortcut(IILandroid/view/KeyEvent;I)Z
    .registers 6
    .parameter "featureId"
    .parameter "keyCode"
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    .line 1055
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@4
    move-result-object v0

    #@5
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/internal/policy/impl/PhoneWindow;->performPanelShortcut(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final preparePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)Z
    .registers 11
    .parameter "st"
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 484
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_a

    #@9
    .line 576
    :cond_9
    :goto_9
    return v4

    #@a
    .line 489
    :cond_a
    iget-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@c
    if-eqz v2, :cond_10

    #@e
    move v4, v3

    #@f
    .line 490
    goto :goto_9

    #@10
    .line 493
    :cond_10
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@12
    if-eqz v2, :cond_1d

    #@14
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@16
    if-eq v2, p1, :cond_1d

    #@18
    .line 495
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@1a
    invoke-virtual {p0, v2, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@1d
    .line 498
    :cond_1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@20
    move-result-object v0

    #@21
    .line 500
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_2b

    #@23
    .line 501
    iget v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@25
    invoke-interface {v0, v2}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    iput-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@2b
    .line 504
    :cond_2b
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@2d
    if-nez v2, :cond_ce

    #@2f
    .line 506
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@31
    if-eqz v2, :cond_37

    #@33
    iget-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshMenuContent:Z

    #@35
    if-eqz v2, :cond_7f

    #@37
    .line 507
    :cond_37
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@39
    if-nez v2, :cond_45

    #@3b
    .line 508
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->initializePanelMenu(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;)Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_9

    #@41
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@43
    if-eqz v2, :cond_9

    #@45
    .line 513
    :cond_45
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@47
    if-eqz v2, :cond_5d

    #@49
    .line 514
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@4b
    if-nez v2, :cond_54

    #@4d
    .line 515
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@4f
    invoke-direct {v2, p0, v7}, Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;-><init>(Lcom/android/internal/policy/impl/PhoneWindow;Lcom/android/internal/policy/impl/PhoneWindow$1;)V

    #@52
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@54
    .line 517
    :cond_54
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@56
    iget-object v5, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@58
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@5a
    invoke-virtual {v2, v5, v6}, Lcom/android/internal/widget/ActionBarView;->setMenu(Landroid/view/Menu;Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@5d
    .line 524
    :cond_5d
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@5f
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@62
    .line 525
    if-eqz v0, :cond_6e

    #@64
    iget v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@66
    iget-object v5, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@68
    invoke-interface {v0, v2, v5}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    #@6b
    move-result v2

    #@6c
    if-nez v2, :cond_7d

    #@6e
    .line 527
    :cond_6e
    invoke-virtual {p1, v7}, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->setMenu(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@71
    .line 529
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@73
    if-eqz v2, :cond_9

    #@75
    .line 531
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@77
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@79
    invoke-virtual {v2, v7, v3}, Lcom/android/internal/widget/ActionBarView;->setMenu(Landroid/view/Menu;Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@7c
    goto :goto_9

    #@7d
    .line 537
    :cond_7d
    iput-boolean v4, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->refreshMenuContent:Z

    #@7f
    .line 544
    :cond_7f
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@81
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@84
    .line 548
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->frozenActionViewState:Landroid/os/Bundle;

    #@86
    if-eqz v2, :cond_91

    #@88
    .line 549
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8a
    iget-object v5, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->frozenActionViewState:Landroid/os/Bundle;

    #@8c
    invoke-virtual {v2, v5}, Lcom/android/internal/view/menu/MenuBuilder;->restoreActionViewStates(Landroid/os/Bundle;)V

    #@8f
    .line 550
    iput-object v7, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->frozenActionViewState:Landroid/os/Bundle;

    #@91
    .line 553
    :cond_91
    iget v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->featureId:I

    #@93
    iget-object v5, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    #@95
    iget-object v6, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@97
    invoke-interface {v0, v2, v5, v6}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    #@9a
    move-result v2

    #@9b
    if-nez v2, :cond_af

    #@9d
    .line 554
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@9f
    if-eqz v2, :cond_a8

    #@a1
    .line 557
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@a3
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionMenuPresenterCallback:Lcom/android/internal/policy/impl/PhoneWindow$ActionMenuPresenterCallback;

    #@a5
    invoke-virtual {v2, v7, v3}, Lcom/android/internal/widget/ActionBarView;->setMenu(Landroid/view/Menu;Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@a8
    .line 559
    :cond_a8
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@aa
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->startDispatchingItemsChanged()V

    #@ad
    goto/16 :goto_9

    #@af
    .line 564
    :cond_af
    if-eqz p2, :cond_d7

    #@b1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    #@b4
    move-result v2

    #@b5
    :goto_b5
    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@b8
    move-result-object v1

    #@b9
    .line 566
    .local v1, kmap:Landroid/view/KeyCharacterMap;
    invoke-virtual {v1}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    #@bc
    move-result v2

    #@bd
    if-eq v2, v3, :cond_d9

    #@bf
    move v2, v3

    #@c0
    :goto_c0
    iput-boolean v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->qwertyMode:Z

    #@c2
    .line 567
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@c4
    iget-boolean v5, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->qwertyMode:Z

    #@c6
    invoke-virtual {v2, v5}, Lcom/android/internal/view/menu/MenuBuilder;->setQwertyMode(Z)V

    #@c9
    .line 568
    iget-object v2, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    #@cb
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->startDispatchingItemsChanged()V

    #@ce
    .line 572
    .end local v1           #kmap:Landroid/view/KeyCharacterMap;
    :cond_ce
    iput-boolean v3, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isPrepared:Z

    #@d0
    .line 573
    iput-boolean v4, p1, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isHandled:Z

    #@d2
    .line 574
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPreparedPanel:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@d4
    move v4, v3

    #@d5
    .line 576
    goto/16 :goto_9

    #@d7
    .line 564
    :cond_d7
    const/4 v2, -0x1

    #@d8
    goto :goto_b5

    #@d9
    .restart local v1       #kmap:Landroid/view/KeyCharacterMap;
    :cond_d9
    move v2, v4

    #@da
    .line 566
    goto :goto_c0
.end method

.method public requestFeature(I)Z
    .registers 6
    .parameter "featureId"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x7

    #@3
    .line 346
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@5
    if-eqz v1, :cond_f

    #@7
    .line 347
    new-instance v1, Landroid/util/AndroidRuntimeException;

    #@9
    const-string v2, "requestFeature() must be called before adding content"

    #@b
    invoke-direct {v1, v2}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 349
    :cond_f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@12
    move-result v0

    #@13
    .line 350
    .local v0, features:I
    const/16 v1, 0x41

    #@15
    if-eq v0, v1, :cond_21

    #@17
    if-ne p1, v2, :cond_21

    #@19
    .line 353
    new-instance v1, Landroid/util/AndroidRuntimeException;

    #@1b
    const-string v2, "You cannot combine custom titles with other title features"

    #@1d
    invoke-direct {v1, v2}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1

    #@21
    .line 355
    :cond_21
    and-int/lit16 v1, v0, 0x80

    #@23
    if-eqz v1, :cond_33

    #@25
    if-eq p1, v2, :cond_33

    #@27
    const/16 v1, 0xa

    #@29
    if-eq p1, v1, :cond_33

    #@2b
    .line 359
    new-instance v1, Landroid/util/AndroidRuntimeException;

    #@2d
    const-string v2, "You cannot combine custom titles with other title features"

    #@2f
    invoke-direct {v1, v2}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v1

    #@33
    .line 361
    :cond_33
    and-int/lit8 v1, v0, 0x2

    #@35
    if-eqz v1, :cond_3b

    #@37
    if-ne p1, v3, :cond_3b

    #@39
    .line 362
    const/4 v1, 0x0

    #@3a
    .line 368
    :goto_3a
    return v1

    #@3b
    .line 364
    :cond_3b
    and-int/lit16 v1, v0, 0x100

    #@3d
    if-eqz v1, :cond_45

    #@3f
    const/4 v1, 0x1

    #@40
    if-ne p1, v1, :cond_45

    #@42
    .line 366
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->removeFeature(I)V

    #@45
    .line 368
    :cond_45
    invoke-super {p0, p1}, Landroid/view/Window;->requestFeature(I)Z

    #@48
    move-result v1

    #@49
    goto :goto_3a
.end method

.method public restoreHierarchyState(Landroid/os/Bundle;)V
    .registers 10
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 1837
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@3
    if-nez v5, :cond_6

    #@5
    .line 1876
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1841
    :cond_6
    const-string v5, "android:views"

    #@8
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@b
    move-result-object v4

    #@c
    .line 1843
    .local v4, savedStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    if-eqz v4, :cond_13

    #@e
    .line 1844
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@10
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->restoreHierarchyState(Landroid/util/SparseArray;)V

    #@13
    .line 1848
    :cond_13
    const-string v5, "android:focusedViewId"

    #@15
    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@18
    move-result v1

    #@19
    .line 1849
    .local v1, focusedViewId:I
    if-eq v1, v6, :cond_26

    #@1b
    .line 1850
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@1d
    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@20
    move-result-object v2

    #@21
    .line 1851
    .local v2, needsFocus:Landroid/view/View;
    if-eqz v2, :cond_43

    #@23
    .line 1852
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    #@26
    .line 1861
    .end local v2           #needsFocus:Landroid/view/View;
    :cond_26
    :goto_26
    const-string v5, "android:Panels"

    #@28
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@2b
    move-result-object v3

    #@2c
    .line 1862
    .local v3, panelStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    if-eqz v3, :cond_31

    #@2e
    .line 1863
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->restorePanelState(Landroid/util/SparseArray;)V

    #@31
    .line 1866
    :cond_31
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@33
    if-eqz v5, :cond_5

    #@35
    .line 1867
    const-string v5, "android:ActionBar"

    #@37
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@3a
    move-result-object v0

    #@3b
    .line 1869
    .local v0, actionBarStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    if-eqz v0, :cond_62

    #@3d
    .line 1870
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@3f
    invoke-virtual {v5, v0}, Lcom/android/internal/widget/ActionBarView;->restoreHierarchyState(Landroid/util/SparseArray;)V

    #@42
    goto :goto_5

    #@43
    .line 1854
    .end local v0           #actionBarStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    .end local v3           #panelStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    .restart local v2       #needsFocus:Landroid/view/View;
    :cond_43
    const-string v5, "PhoneWindow"

    #@45
    new-instance v6, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v7, "Previously focused view reported id "

    #@4c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    const-string v7, " during save, but can\'t be found during restore."

    #@56
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    goto :goto_26

    #@62
    .line 1872
    .end local v2           #needsFocus:Landroid/view/View;
    .restart local v0       #actionBarStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    .restart local v3       #panelStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    :cond_62
    const-string v5, "PhoneWindow"

    #@64
    const-string v6, "Missing saved instance states for action bar views! State will not be restored."

    #@66
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_5
.end method

.method public saveHierarchyState()Landroid/os/Bundle;
    .registers 8

    #@0
    .prologue
    .line 1796
    new-instance v2, Landroid/os/Bundle;

    #@2
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 1797
    .local v2, outState:Landroid/os/Bundle;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@7
    if-nez v5, :cond_a

    #@9
    .line 1831
    :cond_9
    :goto_9
    return-object v2

    #@a
    .line 1801
    :cond_a
    new-instance v4, Landroid/util/SparseArray;

    #@c
    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    #@f
    .line 1802
    .local v4, states:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@11
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->saveHierarchyState(Landroid/util/SparseArray;)V

    #@14
    .line 1803
    const-string v5, "android:views"

    #@16
    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@19
    .line 1806
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@1b
    invoke-virtual {v5}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    #@1e
    move-result-object v1

    #@1f
    .line 1807
    .local v1, focusedView:Landroid/view/View;
    if-eqz v1, :cond_31

    #@21
    .line 1808
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    #@24
    move-result v5

    #@25
    const/4 v6, -0x1

    #@26
    if-eq v5, v6, :cond_31

    #@28
    .line 1809
    const-string v5, "android:focusedViewId"

    #@2a
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    #@2d
    move-result v6

    #@2e
    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@31
    .line 1819
    :cond_31
    new-instance v3, Landroid/util/SparseArray;

    #@33
    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    #@36
    .line 1820
    .local v3, panelStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/PhoneWindow;->savePanelState(Landroid/util/SparseArray;)V

    #@39
    .line 1821
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@3c
    move-result v5

    #@3d
    if-lez v5, :cond_44

    #@3f
    .line 1822
    const-string v5, "android:Panels"

    #@41
    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@44
    .line 1825
    :cond_44
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@46
    if-eqz v5, :cond_9

    #@48
    .line 1826
    new-instance v0, Landroid/util/SparseArray;

    #@4a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@4d
    .line 1827
    .local v0, actionBarStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@4f
    invoke-virtual {v5, v0}, Lcom/android/internal/widget/ActionBarView;->saveHierarchyState(Landroid/util/SparseArray;)V

    #@52
    .line 1828
    const-string v5, "android:ActionBar"

    #@54
    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@57
    goto :goto_9
.end method

.method sendCloseSystemWindows()V
    .registers 3

    #@0
    .prologue
    .line 3912
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V

    #@8
    .line 3913
    return-void
.end method

.method sendCloseSystemWindows(Ljava/lang/String;)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 3916
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V

    #@7
    .line 3917
    return-void
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    #@0
    .prologue
    .line 1299
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@6
    if-eqz v0, :cond_16

    #@8
    .line 1300
    :cond_8
    const/4 v0, 0x0

    #@9
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundResource:I

    #@b
    .line 1301
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    .line 1302
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 1303
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@13
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setWindowBackground(Landroid/graphics/drawable/Drawable;)V

    #@16
    .line 1306
    :cond_16
    return-void
.end method

.method public final setChildDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "featureId"
    .parameter "drawable"

    #@0
    .prologue
    .line 3296
    const/4 v1, 0x1

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@4
    move-result-object v0

    #@5
    .line 3297
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iput-object p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->child:Landroid/graphics/drawable/Drawable;

    #@7
    .line 3298
    const/4 v1, 0x0

    #@8
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@b
    .line 3299
    return-void
.end method

.method public final setChildInt(II)V
    .registers 4
    .parameter "featureId"
    .parameter "value"

    #@0
    .prologue
    .line 3303
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->updateInt(IIZ)V

    #@4
    .line 3304
    return-void
.end method

.method public final setContainer(Landroid/view/Window;)V
    .registers 2
    .parameter "container"

    #@0
    .prologue
    .line 341
    invoke-super {p0, p1}, Landroid/view/Window;->setContainer(Landroid/view/Window;)V

    #@3
    .line 342
    return-void
.end method

.method public setContentView(I)V
    .registers 5
    .parameter "layoutResID"

    #@0
    .prologue
    .line 383
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2
    if-nez v1, :cond_1e

    #@4
    .line 384
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@7
    .line 388
    :goto_7
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@9
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@b
    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@e
    .line 389
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@11
    move-result-object v0

    #@12
    .line 390
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1d

    #@14
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_1d

    #@1a
    .line 391
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    #@1d
    .line 393
    :cond_1d
    return-void

    #@1e
    .line 386
    .end local v0           #cb:Landroid/view/Window$Callback;
    :cond_1e
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@20
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    #@23
    goto :goto_7
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 397
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@6
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@9
    .line 398
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 402
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@2
    if-nez v1, :cond_1c

    #@4
    .line 403
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->installDecor()V

    #@7
    .line 407
    :goto_7
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@9
    invoke-virtual {v1, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@c
    .line 408
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getCallback()Landroid/view/Window$Callback;

    #@f
    move-result-object v0

    #@10
    .line 409
    .local v0, cb:Landroid/view/Window$Callback;
    if-eqz v0, :cond_1b

    #@12
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->isDestroyed()Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_1b

    #@18
    .line 410
    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    #@1b
    .line 412
    :cond_1b
    return-void

    #@1c
    .line 405
    .end local v0           #cb:Landroid/view/Window$Callback;
    :cond_1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mContentParent:Landroid/view/ViewGroup;

    #@1e
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    #@21
    goto :goto_7
.end method

.method protected final setFeatureDefaultDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "featureId"
    .parameter "drawable"

    #@0
    .prologue
    .line 1359
    const/4 v1, 0x1

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@4
    move-result-object v0

    #@5
    .line 1360
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->def:Landroid/graphics/drawable/Drawable;

    #@7
    if-eq v1, p2, :cond_f

    #@9
    .line 1361
    iput-object p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->def:Landroid/graphics/drawable/Drawable;

    #@b
    .line 1362
    const/4 v1, 0x0

    #@c
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@f
    .line 1364
    :cond_f
    return-void
.end method

.method public final setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "featureId"
    .parameter "drawable"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1340
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@5
    move-result-object v0

    #@6
    .line 1341
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iput v2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->resid:I

    #@8
    .line 1342
    const/4 v1, 0x0

    #@9
    iput-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->uri:Landroid/net/Uri;

    #@b
    .line 1343
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->local:Landroid/graphics/drawable/Drawable;

    #@d
    if-eq v1, p2, :cond_14

    #@f
    .line 1344
    iput-object p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->local:Landroid/graphics/drawable/Drawable;

    #@11
    .line 1345
    invoke-direct {p0, p1, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@14
    .line 1347
    :cond_14
    return-void
.end method

.method public setFeatureDrawableAlpha(II)V
    .registers 5
    .parameter "featureId"
    .parameter "alpha"

    #@0
    .prologue
    .line 1351
    const/4 v1, 0x1

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@4
    move-result-object v0

    #@5
    .line 1352
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iget v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->alpha:I

    #@7
    if-eq v1, p2, :cond_f

    #@9
    .line 1353
    iput p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->alpha:I

    #@b
    .line 1354
    const/4 v1, 0x0

    #@c
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@f
    .line 1356
    :cond_f
    return-void
.end method

.method public final setFeatureDrawableResource(II)V
    .registers 6
    .parameter "featureId"
    .parameter "resId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1310
    if-eqz p2, :cond_23

    #@3
    .line 1311
    const/4 v1, 0x1

    #@4
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@7
    move-result-object v0

    #@8
    .line 1312
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iget v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->resid:I

    #@a
    if-eq v1, p2, :cond_22

    #@c
    .line 1313
    iput p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->resid:I

    #@e
    .line 1314
    iput-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->uri:Landroid/net/Uri;

    #@10
    .line 1315
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getContext()Landroid/content/Context;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->local:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 1316
    const/4 v1, 0x0

    #@1f
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@22
    .line 1321
    .end local v0           #st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    :cond_22
    :goto_22
    return-void

    #@23
    .line 1319
    :cond_23
    invoke-virtual {p0, p1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@26
    goto :goto_22
.end method

.method public final setFeatureDrawableUri(ILandroid/net/Uri;)V
    .registers 6
    .parameter "featureId"
    .parameter "uri"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1325
    if-eqz p2, :cond_22

    #@3
    .line 1326
    const/4 v1, 0x1

    #@4
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@7
    move-result-object v0

    #@8
    .line 1327
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->uri:Landroid/net/Uri;

    #@a
    if-eqz v1, :cond_14

    #@c
    iget-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->uri:Landroid/net/Uri;

    #@e
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_21

    #@14
    .line 1328
    :cond_14
    iput v2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->resid:I

    #@16
    .line 1329
    iput-object p2, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->uri:Landroid/net/Uri;

    #@18
    .line 1330
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->loadImageURI(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;->local:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 1331
    invoke-direct {p0, p1, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@21
    .line 1336
    .end local v0           #st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    :cond_21
    :goto_21
    return-void

    #@22
    .line 1334
    :cond_22
    const/4 v1, 0x0

    #@23
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@26
    goto :goto_21
.end method

.method protected setFeatureFromAttrs(ILandroid/content/res/TypedArray;II)V
    .registers 9
    .parameter "featureId"
    .parameter "attrs"
    .parameter "drawableAttr"
    .parameter "alphaAttr"

    #@0
    .prologue
    .line 2841
    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v1

    #@4
    .line 2842
    .local v1, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_c

    #@6
    .line 2843
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindow;->requestFeature(I)Z

    #@9
    .line 2844
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->setFeatureDefaultDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@c
    .line 2846
    :cond_c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getFeatures()I

    #@f
    move-result v2

    #@10
    const/4 v3, 0x1

    #@11
    shl-int/2addr v3, p1

    #@12
    and-int/2addr v2, v3

    #@13
    if-eqz v2, :cond_1f

    #@15
    .line 2847
    const/4 v2, -0x1

    #@16
    invoke-virtual {p2, p4, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@19
    move-result v0

    #@1a
    .line 2848
    .local v0, alpha:I
    if-ltz v0, :cond_1f

    #@1c
    .line 2849
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setFeatureDrawableAlpha(II)V

    #@1f
    .line 2852
    .end local v0           #alpha:I
    :cond_1f
    return-void
.end method

.method public final setFeatureInt(II)V
    .registers 4
    .parameter "featureId"
    .parameter "value"

    #@0
    .prologue
    .line 1370
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->updateInt(IIZ)V

    #@4
    .line 1371
    return-void
.end method

.method public setFlags(II)V
    .registers 5
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@2
    if-eqz v0, :cond_16

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mSetFlagsLocked:Z

    #@6
    if-nez v0, :cond_16

    #@8
    .line 266
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPrevFlag:I

    #@a
    xor-int/lit8 v1, p2, -0x1

    #@c
    and-int/2addr v0, v1

    #@d
    and-int v1, p1, p2

    #@f
    or-int/2addr v0, v1

    #@10
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mPrevFlag:I

    #@12
    .line 267
    const/4 v0, 0x0

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@16
    .line 269
    :cond_16
    invoke-super {p0, p1, p2}, Landroid/view/Window;->setFlags(II)V

    #@19
    .line 270
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 459
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@9
    .line 463
    :cond_9
    :goto_9
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitle:Ljava/lang/CharSequence;

    #@b
    .line 464
    return-void

    #@c
    .line 460
    :cond_c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@e
    if-eqz v0, :cond_9

    #@10
    .line 461
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mActionBar:Lcom/android/internal/widget/ActionBarView;

    #@12
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    #@15
    goto :goto_9
.end method

.method public setTitleColor(I)V
    .registers 3
    .parameter "textColor"

    #@0
    .prologue
    .line 468
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 469
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    #@9
    .line 471
    :cond_9
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTitleColor:I

    #@b
    .line 472
    return-void
.end method

.method public setUiOptions(I)V
    .registers 2
    .parameter "uiOptions"

    #@0
    .prologue
    .line 373
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mUiOptions:I

    #@2
    .line 374
    return-void
.end method

.method public setUiOptions(II)V
    .registers 5
    .parameter "uiOptions"
    .parameter "mask"

    #@0
    .prologue
    .line 378
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mUiOptions:I

    #@2
    xor-int/lit8 v1, p2, -0x1

    #@4
    and-int/2addr v0, v1

    #@5
    and-int v1, p1, p2

    #@7
    or-int/2addr v0, v1

    #@8
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mUiOptions:I

    #@a
    .line 379
    return-void
.end method

.method public setVolumeControlStream(I)V
    .registers 2
    .parameter "streamType"

    #@0
    .prologue
    .line 3477
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mVolumeControlStreamType:I

    #@2
    .line 3478
    return-void
.end method

.method public superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1620
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public superDispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1540
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_16

    #@7
    .line 1541
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@9
    if-eqz v0, :cond_f

    #@b
    .line 1542
    const/4 v0, 0x0

    #@c
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@f
    .line 1552
    :cond_f
    :goto_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@11
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->superDispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@14
    move-result v0

    #@15
    return v0

    #@16
    .line 1544
    :cond_16
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@19
    move-result v0

    #@1a
    const/16 v1, 0x53

    #@1c
    if-ne v0, v1, :cond_f

    #@1e
    .line 1545
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@21
    move-result-object v0

    #@22
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@24
    and-int/lit16 v0, v0, 0x400

    #@26
    if-eqz v0, :cond_f

    #@28
    const-string v0, "kids"

    #@2a
    const-string v1, "service.plushome.currenthome"

    #@2c
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_f

    #@36
    .line 1548
    const/4 v0, 0x1

    #@37
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@3a
    goto :goto_f
.end method

.method public superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1557
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public superDispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1563
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mGestureScanner:Landroid/view/GestureDetector;

    #@4
    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_17

    #@a
    .line 1564
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@d
    move-result v2

    #@e
    if-ne v2, v3, :cond_17

    #@10
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mForceNotFullscreen:Z

    #@12
    if-eqz v2, :cond_17

    #@14
    .line 1565
    invoke-direct {p0, v4}, Lcom/android/internal/policy/impl/PhoneWindow;->setForceMode(Z)V

    #@17
    .line 1569
    :cond_17
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@19
    if-eqz v2, :cond_30

    #@1b
    .line 1570
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindow;->getAppTokenOfWindow()Landroid/os/IBinder;

    #@1e
    move-result-object v0

    #@1f
    .line 1573
    .local v0, appToken:Landroid/os/IBinder;
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mWindowManagerService:Landroid/view/IWindowManager;

    #@21
    if-nez v2, :cond_29

    #@23
    .line 1574
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@26
    move-result-object v2

    #@27
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mWindowManagerService:Landroid/view/IWindowManager;

    #@29
    .line 1576
    :cond_29
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@2c
    move-result v2

    #@2d
    sparse-switch v2, :sswitch_data_74

    #@30
    .line 1610
    .end local v0           #appToken:Landroid/os/IBinder;
    :cond_30
    :goto_30
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@32
    invoke-virtual {v2, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->superDispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@35
    move-result v2

    #@36
    return v2

    #@37
    .line 1579
    .restart local v0       #appToken:Landroid/os/IBinder;
    :sswitch_37
    if-eqz v0, :cond_4d

    #@39
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFocused:Z

    #@3b
    if-nez v2, :cond_4d

    #@3d
    .line 1582
    :try_start_3d
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mWindowManagerService:Landroid/view/IWindowManager;

    #@3f
    const/4 v3, 0x1

    #@40
    invoke-interface {v2, v0, v3}, Landroid/view/IWindowManager;->sendSplitWindowFocusChanged(Landroid/os/IBinder;Z)V
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_43} :catch_44

    #@43
    goto :goto_30

    #@44
    .line 1583
    :catch_44
    move-exception v1

    #@45
    .line 1584
    .local v1, ex:Landroid/os/RemoteException;
    const-string v2, "SplitWindow"

    #@47
    const-string v3, "RemoteException on WindowManagerService"

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_30

    #@4d
    .line 1589
    .end local v1           #ex:Landroid/os/RemoteException;
    :cond_4d
    :try_start_4d
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mWindowManagerService:Landroid/view/IWindowManager;

    #@4f
    const/4 v3, 0x0

    #@50
    const/4 v4, 0x1

    #@51
    invoke-interface {v2, v3, v4}, Landroid/view/IWindowManager;->sendSplitWindowFocusChanged(Landroid/os/IBinder;Z)V
    :try_end_54
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_54} :catch_55

    #@54
    goto :goto_30

    #@55
    .line 1590
    :catch_55
    move-exception v1

    #@56
    .line 1591
    .restart local v1       #ex:Landroid/os/RemoteException;
    const-string v2, "SplitWindow"

    #@58
    const-string v3, "RemoteException on WindowManagerService"

    #@5a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_30

    #@5e
    .line 1598
    .end local v1           #ex:Landroid/os/RemoteException;
    :sswitch_5e
    if-eqz v0, :cond_30

    #@60
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mIsFocused:Z

    #@62
    if-eqz v2, :cond_30

    #@64
    .line 1601
    :try_start_64
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mWindowManagerService:Landroid/view/IWindowManager;

    #@66
    const/4 v3, 0x0

    #@67
    invoke-interface {v2, v0, v3}, Landroid/view/IWindowManager;->sendSplitWindowFocusChanged(Landroid/os/IBinder;Z)V
    :try_end_6a
    .catch Landroid/os/RemoteException; {:try_start_64 .. :try_end_6a} :catch_6b

    #@6a
    goto :goto_30

    #@6b
    .line 1602
    :catch_6b
    move-exception v1

    #@6c
    .line 1603
    .restart local v1       #ex:Landroid/os/RemoteException;
    const-string v2, "SplitWindow"

    #@6e
    const-string v3, "RemoteException on WindowManagerService"

    #@70
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_30

    #@74
    .line 1576
    :sswitch_data_74
    .sparse-switch
        0x0 -> :sswitch_37
        0x4 -> :sswitch_5e
    .end sparse-switch
.end method

.method public superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1615
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public takeInputQueue(Landroid/view/InputQueue$Callback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 437
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTakeInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@2
    .line 438
    return-void
.end method

.method public takeKeyEvents(Z)V
    .registers 3
    .parameter "get"

    #@0
    .prologue
    .line 1534
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mDecor:Lcom/android/internal/policy/impl/PhoneWindow$DecorView;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindow$DecorView;->setFocusable(Z)V

    #@5
    .line 1535
    return-void
.end method

.method public takeSurface(Landroid/view/SurfaceHolder$Callback2;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 433
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindow;->mTakeSurfaceCallback:Landroid/view/SurfaceHolder$Callback2;

    #@2
    .line 434
    return-void
.end method

.method public final togglePanel(ILandroid/view/KeyEvent;)V
    .registers 6
    .parameter "featureId"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 855
    invoke-direct {p0, p1, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->getPanelState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;

    #@4
    move-result-object v0

    #@5
    .line 856
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;
    iget-boolean v1, v0, Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;->isOpen:Z

    #@7
    if-eqz v1, :cond_d

    #@9
    .line 857
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindow;->closePanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Z)V

    #@c
    .line 861
    :goto_c
    return-void

    #@d
    .line 859
    :cond_d
    invoke-direct {p0, v0, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->openPanel(Lcom/android/internal/policy/impl/PhoneWindow$PanelFeatureState;Landroid/view/KeyEvent;)V

    #@10
    goto :goto_c
.end method

.method protected final updateDrawable(IZ)V
    .registers 5
    .parameter "featureId"
    .parameter "fromActive"

    #@0
    .prologue
    .line 1383
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindow;->getDrawableState(IZ)Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;

    #@4
    move-result-object v0

    #@5
    .line 1384
    .local v0, st:Lcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;
    if-eqz v0, :cond_a

    #@7
    .line 1385
    invoke-direct {p0, p1, v0, p2}, Lcom/android/internal/policy/impl/PhoneWindow;->updateDrawable(ILcom/android/internal/policy/impl/PhoneWindow$DrawableFeatureState;Z)V

    #@a
    .line 1387
    :cond_a
    return-void
.end method
