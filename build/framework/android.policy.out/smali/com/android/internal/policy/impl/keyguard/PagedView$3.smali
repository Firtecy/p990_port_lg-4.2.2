.class Lcom/android/internal/policy/impl/keyguard/PagedView$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/PagedView;->hideScrollingIndicator(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private cancelled:Z

.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/PagedView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1989
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@2
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    #@5
    .line 1990
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$3;->cancelled:Z

    #@8
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter "animation"

    #@0
    .prologue
    .line 1993
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$3;->cancelled:Z

    #@3
    .line 1994
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 1997
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$3;->cancelled:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 1998
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/PagedView$3;->this$0:Lcom/android/internal/policy/impl/keyguard/PagedView;

    #@6
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/PagedView;->access$300(Lcom/android/internal/policy/impl/keyguard/PagedView;)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x4

    #@b
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@e
    .line 2000
    :cond_e
    return-void
.end method
