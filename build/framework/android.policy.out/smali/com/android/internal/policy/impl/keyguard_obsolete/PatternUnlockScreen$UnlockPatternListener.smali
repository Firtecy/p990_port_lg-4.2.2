.class Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;
.super Ljava/lang/Object;
.source "PatternUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/widget/LockPatternView$OnPatternListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnlockPatternListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;


# direct methods
.method private constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 320
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)V

    #@3
    return-void
.end method


# virtual methods
.method public onPatternCellAdded(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 333
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-le v0, v1, :cond_13

    #@7
    .line 334
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@9
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@c
    move-result-object v0

    #@d
    const/16 v1, 0x1b58

    #@f
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@12
    .line 339
    :goto_12
    return-void

    #@13
    .line 337
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@15
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@18
    move-result-object v0

    #@19
    const/16 v1, 0x7d0

    #@1b
    invoke-interface {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@1e
    goto :goto_12
.end method

.method public onPatternCleared()V
    .registers 1

    #@0
    .prologue
    .line 328
    return-void
.end method

.method public onPatternDetected(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    const/4 v6, 0x1

    #@1
    .line 342
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@3
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPattern(Ljava/util/List;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_4a

    #@d
    .line 343
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@f
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;

    #@12
    move-result-object v3

    #@13
    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@15
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    #@18
    .line 345
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@1a
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, ""

    #@20
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@23
    .line 346
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@25
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->updateStatusLines(Z)V

    #@2c
    .line 347
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2e
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@31
    move-result-object v3

    #@32
    invoke-interface {v3, v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->keyguardDone(Z)V

    #@35
    .line 348
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@37
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@3a
    move-result-object v3

    #@3b
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    #@3e
    .line 349
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@41
    move-result-object v3

    #@42
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v3, v4}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@49
    .line 381
    :cond_49
    :goto_49
    return-void

    #@4a
    .line 351
    :cond_4a
    const/4 v2, 0x0

    #@4b
    .line 352
    .local v2, reportFailedAttempt:Z
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@4e
    move-result v3

    #@4f
    const/4 v4, 0x2

    #@50
    if-le v3, v4, :cond_5d

    #@52
    .line 353
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@54
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@57
    move-result-object v3

    #@58
    const/16 v4, 0x1b58

    #@5a
    invoke-interface {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock(I)V

    #@5d
    .line 355
    :cond_5d
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@5f
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;

    #@62
    move-result-object v3

    #@63
    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@65
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    #@68
    .line 356
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@6b
    move-result v3

    #@6c
    const/4 v4, 0x4

    #@6d
    if-lt v3, v4, :cond_7a

    #@6f
    .line 357
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@71
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$608(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I

    #@74
    .line 358
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@76
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$708(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I

    #@79
    .line 359
    const/4 v2, 0x1

    #@7a
    .line 361
    :cond_7a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@7c
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)I

    #@7f
    move-result v3

    #@80
    const/4 v4, 0x5

    #@81
    if-lt v3, v4, :cond_9e

    #@83
    .line 363
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@85
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternUtils;

    #@88
    move-result-object v3

    #@89
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->setLockoutAttemptDeadline()J

    #@8c
    move-result-wide v0

    #@8d
    .line 364
    .local v0, deadline:J
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@8f
    invoke-static {v3, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;J)V

    #@92
    .line 377
    .end local v0           #deadline:J
    :goto_92
    if-eqz v2, :cond_49

    #@94
    .line 378
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@96
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@99
    move-result-object v3

    #@9a
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->reportFailedUnlockAttempt()V

    #@9d
    goto :goto_49

    #@9e
    .line 367
    :cond_9e
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@a0
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@a3
    move-result-object v3

    #@a4
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@a6
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->getContext()Landroid/content/Context;

    #@a9
    move-result-object v4

    #@aa
    const v5, 0x1040313

    #@ad
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b0
    move-result-object v4

    #@b1
    invoke-virtual {v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->setInstructionText(Ljava/lang/String;)V

    #@b4
    .line 369
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@b6
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@b9
    move-result-object v3

    #@ba
    invoke-virtual {v3, v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->updateStatusLines(Z)V

    #@bd
    .line 370
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@bf
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;

    #@c2
    move-result-object v3

    #@c3
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@c5
    invoke-static {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Ljava/lang/Runnable;

    #@c8
    move-result-object v4

    #@c9
    const-wide/16 v5, 0x7d0

    #@cb
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/internal/widget/LockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@ce
    goto :goto_92
.end method

.method public onPatternStart()V
    .registers 3

    #@0
    .prologue
    .line 324
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@2
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Lcom/android/internal/widget/LockPatternView;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@8
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;)Ljava/lang/Runnable;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@f
    .line 325
    return-void
.end method
