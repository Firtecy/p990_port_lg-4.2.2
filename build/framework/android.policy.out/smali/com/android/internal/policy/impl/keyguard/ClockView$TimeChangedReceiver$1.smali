.class Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;
.super Ljava/lang/Object;
.source "ClockView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver;

.field final synthetic val$clock:Lcom/android/internal/policy/impl/keyguard/ClockView;

.field final synthetic val$timezoneChanged:Z


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver;ZLcom/android/internal/policy/impl/keyguard/ClockView;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->this$0:Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver;

    #@2
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->val$timezoneChanged:Z

    #@4
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->val$clock:Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->val$timezoneChanged:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 77
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->val$clock:Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@6
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@9
    move-result-object v1

    #@a
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/keyguard/ClockView;->access$002(Lcom/android/internal/policy/impl/keyguard/ClockView;Ljava/util/Calendar;)Ljava/util/Calendar;

    #@d
    .line 79
    :cond_d
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/ClockView$TimeChangedReceiver$1;->val$clock:Lcom/android/internal/policy/impl/keyguard/ClockView;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/ClockView;->updateTime()V

    #@12
    .line 80
    return-void
.end method
