.class public Landroid/widget/DatePicker;
.super Landroid/widget/FrameLayout;
.source "DatePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/DatePicker$SavedState;,
        Landroid/widget/DatePicker$OnDateChangedListener;
    }
.end annotation


# static fields
.field private static final DATE_FORMAT:Ljava/lang/String; = "MM/dd/yyyy"

.field private static final DEFAULT_CALENDAR_VIEW_SHOWN:Z = true

.field private static final DEFAULT_ENABLED_STATE:Z = true

.field private static final DEFAULT_END_YEAR:I = 0x7f4

.field private static final DEFAULT_SPINNERS_SHOWN:Z = true

.field private static final DEFAULT_START_YEAR:I = 0x76e

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mCalendarView:Landroid/widget/CalendarView;

.field private mCurrentDate:Ljava/util/Calendar;

.field private mCurrentLocale:Ljava/util/Locale;

.field private final mDateFormat:Ljava/text/DateFormat;

.field private final mDaySpinner:Landroid/widget/NumberPicker;

.field private final mDaySpinnerInput:Landroid/widget/EditText;

.field private mIsEnabled:Z

.field private mIsShowNumberOnlyAboutMonth:Z

.field private mMaxDate:Ljava/util/Calendar;

.field private mMinDate:Ljava/util/Calendar;

.field private mMonthOffset:I

.field private final mMonthSpinner:Landroid/widget/NumberPicker;

.field private final mMonthSpinnerInput:Landroid/widget/EditText;

.field private mNumberOfMonths:I

.field private mOnDateChangedListener:Landroid/widget/DatePicker$OnDateChangedListener;

.field private mShortMonths:[Ljava/lang/String;

.field private final mSpinners:Landroid/widget/LinearLayout;

.field private mTempDate:Ljava/util/Calendar;

.field private final mYearSpinner:Landroid/widget/NumberPicker;

.field private final mYearSpinnerInput:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 73
    const-class v0, Landroid/widget/DatePicker;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/DatePicker;->LOG_TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 154
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 158
    const v0, 0x101035c

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 159
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 21
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 162
    invoke-direct/range {p0 .. p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 109
    new-instance v13, Ljava/text/SimpleDateFormat;

    #@5
    const-string v14, "MM/dd/yyyy"

    #@7
    invoke-direct {v13, v14}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@a
    move-object/from16 v0, p0

    #@c
    iput-object v13, v0, Landroid/widget/DatePicker;->mDateFormat:Ljava/text/DateFormat;

    #@e
    .line 121
    const/4 v13, 0x1

    #@f
    move-object/from16 v0, p0

    #@11
    iput-boolean v13, v0, Landroid/widget/DatePicker;->mIsEnabled:Z

    #@13
    .line 129
    const/4 v13, 0x0

    #@14
    move-object/from16 v0, p0

    #@16
    iput-boolean v13, v0, Landroid/widget/DatePicker;->mIsShowNumberOnlyAboutMonth:Z

    #@18
    .line 133
    const/4 v13, 0x0

    #@19
    move-object/from16 v0, p0

    #@1b
    iput v13, v0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@1d
    .line 165
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@20
    move-result-object v13

    #@21
    move-object/from16 v0, p0

    #@23
    invoke-direct {v0, v13}, Landroid/widget/DatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    #@26
    .line 167
    sget-object v13, Lcom/android/internal/R$styleable;->DatePicker:[I

    #@28
    const/4 v14, 0x0

    #@29
    move-object/from16 v0, p1

    #@2b
    move-object/from16 v1, p2

    #@2d
    move/from16 v2, p3

    #@2f
    invoke-virtual {v0, v1, v13, v2, v14}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@32
    move-result-object v3

    #@33
    .line 169
    .local v3, attributesArray:Landroid/content/res/TypedArray;
    const/4 v13, 0x4

    #@34
    const/4 v14, 0x1

    #@35
    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@38
    move-result v11

    #@39
    .line 171
    .local v11, spinnersShown:Z
    const/4 v13, 0x5

    #@3a
    const/4 v14, 0x1

    #@3b
    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3e
    move-result v4

    #@3f
    .line 173
    .local v4, calendarViewShown:Z
    const/4 v13, 0x0

    #@40
    const/16 v14, 0x76e

    #@42
    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    #@45
    move-result v12

    #@46
    .line 175
    .local v12, startYear:I
    const/4 v13, 0x1

    #@47
    const/16 v14, 0x7f4

    #@49
    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4c
    move-result v5

    #@4d
    .line 176
    .local v5, endYear:I
    const/4 v13, 0x2

    #@4e
    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v9

    #@52
    .line 177
    .local v9, minDate:Ljava/lang/String;
    const/4 v13, 0x3

    #@53
    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    .line 178
    .local v8, maxDate:Ljava/lang/String;
    const/4 v13, 0x6

    #@58
    const v14, 0x1090037

    #@5b
    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5e
    move-result v7

    #@5f
    .line 180
    .local v7, layoutResourceId:I
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@62
    .line 182
    const-string/jumbo v13, "layout_inflater"

    #@65
    move-object/from16 v0, p1

    #@67
    invoke-virtual {v0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6a
    move-result-object v6

    #@6b
    check-cast v6, Landroid/view/LayoutInflater;

    #@6d
    .line 184
    .local v6, inflater:Landroid/view/LayoutInflater;
    const/4 v13, 0x1

    #@6e
    move-object/from16 v0, p0

    #@70
    invoke-virtual {v6, v7, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@73
    .line 186
    new-instance v10, Landroid/widget/DatePicker$1;

    #@75
    move-object/from16 v0, p0

    #@77
    invoke-direct {v10, v0}, Landroid/widget/DatePicker$1;-><init>(Landroid/widget/DatePicker;)V

    #@7a
    .line 224
    .local v10, onChangeListener:Landroid/widget/NumberPicker$OnValueChangeListener;
    const v13, 0x1020288

    #@7d
    move-object/from16 v0, p0

    #@7f
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    #@82
    move-result-object v13

    #@83
    check-cast v13, Landroid/widget/LinearLayout;

    #@85
    move-object/from16 v0, p0

    #@87
    iput-object v13, v0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@89
    .line 227
    const v13, 0x102028c

    #@8c
    move-object/from16 v0, p0

    #@8e
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    #@91
    move-result-object v13

    #@92
    check-cast v13, Landroid/widget/CalendarView;

    #@94
    move-object/from16 v0, p0

    #@96
    iput-object v13, v0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@98
    .line 228
    move-object/from16 v0, p0

    #@9a
    iget-object v13, v0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@9c
    new-instance v14, Landroid/widget/DatePicker$2;

    #@9e
    move-object/from16 v0, p0

    #@a0
    invoke-direct {v14, v0}, Landroid/widget/DatePicker$2;-><init>(Landroid/widget/DatePicker;)V

    #@a3
    invoke-virtual {v13, v14}, Landroid/widget/CalendarView;->setOnDateChangeListener(Landroid/widget/CalendarView$OnDateChangeListener;)V

    #@a6
    .line 237
    const v13, 0x102028a

    #@a9
    move-object/from16 v0, p0

    #@ab
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    #@ae
    move-result-object v13

    #@af
    check-cast v13, Landroid/widget/NumberPicker;

    #@b1
    move-object/from16 v0, p0

    #@b3
    iput-object v13, v0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@b5
    .line 238
    move-object/from16 v0, p0

    #@b7
    iget-object v13, v0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@b9
    invoke-static {}, Landroid/widget/NumberPicker;->getTwoDigitFormatter()Landroid/widget/NumberPicker$Formatter;

    #@bc
    move-result-object v14

    #@bd
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    #@c0
    .line 239
    move-object/from16 v0, p0

    #@c2
    iget-object v13, v0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@c4
    const-wide/16 v14, 0x64

    #@c6
    invoke-virtual {v13, v14, v15}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    #@c9
    .line 240
    move-object/from16 v0, p0

    #@cb
    iget-object v13, v0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@cd
    invoke-virtual {v13, v10}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@d0
    .line 241
    move-object/from16 v0, p0

    #@d2
    iget-object v13, v0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@d4
    const v14, 0x1020349

    #@d7
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@da
    move-result-object v13

    #@db
    check-cast v13, Landroid/widget/EditText;

    #@dd
    move-object/from16 v0, p0

    #@df
    iput-object v13, v0, Landroid/widget/DatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    #@e1
    .line 244
    const v13, 0x1020289

    #@e4
    move-object/from16 v0, p0

    #@e6
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    #@e9
    move-result-object v13

    #@ea
    check-cast v13, Landroid/widget/NumberPicker;

    #@ec
    move-object/from16 v0, p0

    #@ee
    iput-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@f0
    .line 246
    move-object/from16 v0, p0

    #@f2
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget v14, v0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@f8
    add-int/lit8 v14, v14, 0x0

    #@fa
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@fd
    .line 247
    move-object/from16 v0, p0

    #@ff
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@101
    move-object/from16 v0, p0

    #@103
    iget v14, v0, Landroid/widget/DatePicker;->mNumberOfMonths:I

    #@105
    add-int/lit8 v14, v14, -0x1

    #@107
    move-object/from16 v0, p0

    #@109
    iget v15, v0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@10b
    add-int/2addr v14, v15

    #@10c
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@10f
    .line 249
    move-object/from16 v0, p0

    #@111
    iget-boolean v13, v0, Landroid/widget/DatePicker;->mIsShowNumberOnlyAboutMonth:Z

    #@113
    if-nez v13, :cond_120

    #@115
    .line 250
    move-object/from16 v0, p0

    #@117
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@119
    move-object/from16 v0, p0

    #@11b
    iget-object v14, v0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@11d
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@120
    .line 253
    :cond_120
    move-object/from16 v0, p0

    #@122
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@124
    const-wide/16 v14, 0xc8

    #@126
    invoke-virtual {v13, v14, v15}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    #@129
    .line 254
    move-object/from16 v0, p0

    #@12b
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@12d
    invoke-virtual {v13, v10}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@130
    .line 255
    move-object/from16 v0, p0

    #@132
    iget-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@134
    const v14, 0x1020349

    #@137
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@13a
    move-result-object v13

    #@13b
    check-cast v13, Landroid/widget/EditText;

    #@13d
    move-object/from16 v0, p0

    #@13f
    iput-object v13, v0, Landroid/widget/DatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    #@141
    .line 258
    const v13, 0x102028b

    #@144
    move-object/from16 v0, p0

    #@146
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    #@149
    move-result-object v13

    #@14a
    check-cast v13, Landroid/widget/NumberPicker;

    #@14c
    move-object/from16 v0, p0

    #@14e
    iput-object v13, v0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@150
    .line 259
    move-object/from16 v0, p0

    #@152
    iget-object v13, v0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@154
    const-wide/16 v14, 0x64

    #@156
    invoke-virtual {v13, v14, v15}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    #@159
    .line 260
    move-object/from16 v0, p0

    #@15b
    iget-object v13, v0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@15d
    invoke-virtual {v13, v10}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@160
    .line 261
    move-object/from16 v0, p0

    #@162
    iget-object v13, v0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@164
    const v14, 0x1020349

    #@167
    invoke-virtual {v13, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@16a
    move-result-object v13

    #@16b
    check-cast v13, Landroid/widget/EditText;

    #@16d
    move-object/from16 v0, p0

    #@16f
    iput-object v13, v0, Landroid/widget/DatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    #@171
    .line 265
    if-nez v11, :cond_21e

    #@173
    if-nez v4, :cond_21e

    #@175
    .line 266
    const/4 v13, 0x1

    #@176
    move-object/from16 v0, p0

    #@178
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    #@17b
    .line 273
    :goto_17b
    move-object/from16 v0, p0

    #@17d
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@17f
    invoke-virtual {v13}, Ljava/util/Calendar;->clear()V

    #@182
    .line 274
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@185
    move-result v13

    #@186
    if-nez v13, :cond_22a

    #@188
    .line 275
    move-object/from16 v0, p0

    #@18a
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@18c
    move-object/from16 v0, p0

    #@18e
    invoke-direct {v0, v9, v13}, Landroid/widget/DatePicker;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@191
    move-result v13

    #@192
    if-nez v13, :cond_19d

    #@194
    .line 276
    move-object/from16 v0, p0

    #@196
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@198
    const/4 v14, 0x0

    #@199
    const/4 v15, 0x1

    #@19a
    invoke-virtual {v13, v12, v14, v15}, Ljava/util/Calendar;->set(III)V

    #@19d
    .line 281
    :cond_19d
    :goto_19d
    move-object/from16 v0, p0

    #@19f
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@1a1
    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    #@1a4
    move-result-wide v13

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    invoke-virtual {v0, v13, v14}, Landroid/widget/DatePicker;->setMinDate(J)V

    #@1aa
    .line 284
    move-object/from16 v0, p0

    #@1ac
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@1ae
    invoke-virtual {v13}, Ljava/util/Calendar;->clear()V

    #@1b1
    .line 285
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b4
    move-result v13

    #@1b5
    if-nez v13, :cond_235

    #@1b7
    .line 286
    move-object/from16 v0, p0

    #@1b9
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    invoke-direct {v0, v8, v13}, Landroid/widget/DatePicker;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@1c0
    move-result v13

    #@1c1
    if-nez v13, :cond_1ce

    #@1c3
    .line 287
    move-object/from16 v0, p0

    #@1c5
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@1c7
    const/16 v14, 0xb

    #@1c9
    const/16 v15, 0x1f

    #@1cb
    invoke-virtual {v13, v5, v14, v15}, Ljava/util/Calendar;->set(III)V

    #@1ce
    .line 292
    :cond_1ce
    :goto_1ce
    move-object/from16 v0, p0

    #@1d0
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@1d2
    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    #@1d5
    move-result-wide v13

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    invoke-virtual {v0, v13, v14}, Landroid/widget/DatePicker;->setMaxDate(J)V

    #@1db
    .line 295
    move-object/from16 v0, p0

    #@1dd
    iget-object v13, v0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1df
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1e2
    move-result-wide v14

    #@1e3
    invoke-virtual {v13, v14, v15}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@1e6
    .line 296
    move-object/from16 v0, p0

    #@1e8
    iget-object v13, v0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1ea
    const/4 v14, 0x1

    #@1eb
    invoke-virtual {v13, v14}, Ljava/util/Calendar;->get(I)I

    #@1ee
    move-result v13

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    iget-object v14, v0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1f3
    const/4 v15, 0x2

    #@1f4
    invoke-virtual {v14, v15}, Ljava/util/Calendar;->get(I)I

    #@1f7
    move-result v14

    #@1f8
    move-object/from16 v0, p0

    #@1fa
    iget-object v15, v0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1fc
    const/16 v16, 0x5

    #@1fe
    invoke-virtual/range {v15 .. v16}, Ljava/util/Calendar;->get(I)I

    #@201
    move-result v15

    #@202
    const/16 v16, 0x0

    #@204
    move-object/from16 v0, p0

    #@206
    move-object/from16 v1, v16

    #@208
    invoke-virtual {v0, v13, v14, v15, v1}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    #@20b
    .line 300
    invoke-direct/range {p0 .. p0}, Landroid/widget/DatePicker;->reorderSpinners()V

    #@20e
    .line 303
    invoke-direct/range {p0 .. p0}, Landroid/widget/DatePicker;->setContentDescriptions()V

    #@211
    .line 306
    invoke-virtual/range {p0 .. p0}, Landroid/widget/DatePicker;->getImportantForAccessibility()I

    #@214
    move-result v13

    #@215
    if-nez v13, :cond_21d

    #@217
    .line 307
    const/4 v13, 0x1

    #@218
    move-object/from16 v0, p0

    #@21a
    invoke-virtual {v0, v13}, Landroid/widget/DatePicker;->setImportantForAccessibility(I)V

    #@21d
    .line 309
    :cond_21d
    return-void

    #@21e
    .line 268
    :cond_21e
    move-object/from16 v0, p0

    #@220
    invoke-virtual {v0, v11}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    #@223
    .line 269
    move-object/from16 v0, p0

    #@225
    invoke-virtual {v0, v4}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    #@228
    goto/16 :goto_17b

    #@22a
    .line 279
    :cond_22a
    move-object/from16 v0, p0

    #@22c
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@22e
    const/4 v14, 0x0

    #@22f
    const/4 v15, 0x1

    #@230
    invoke-virtual {v13, v12, v14, v15}, Ljava/util/Calendar;->set(III)V

    #@233
    goto/16 :goto_19d

    #@235
    .line 290
    :cond_235
    move-object/from16 v0, p0

    #@237
    iget-object v13, v0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@239
    const/16 v14, 0xb

    #@23b
    const/16 v15, 0x1f

    #@23d
    invoke-virtual {v13, v5, v14, v15}, Ljava/util/Calendar;->set(III)V

    #@240
    goto :goto_1ce
.end method

.method static synthetic access$000(Landroid/widget/DatePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateInputState()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/DatePicker;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/DatePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/DatePicker;->notifyDateChanged()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/DatePicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/DatePicker;III)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/DatePicker;->setDate(III)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/widget/DatePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/widget/DatePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@3
    return-void
.end method

.method private getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;
    .registers 6
    .parameter "oldCalendar"
    .parameter "locale"

    #@0
    .prologue
    .line 529
    if-nez p1, :cond_7

    #@2
    .line 530
    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    #@5
    move-result-object v2

    #@6
    .line 535
    :goto_6
    return-object v2

    #@7
    .line 532
    :cond_7
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@a
    move-result-wide v0

    #@b
    .line 533
    .local v0, currentTimeMillis:J
    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    #@e
    move-result-object v2

    #@f
    .line 534
    .local v2, newCalendar:Ljava/util/Calendar;
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@12
    goto :goto_6
.end method

.method private isNewDate(III)Z
    .registers 7
    .parameter "year"
    .parameter "month"
    .parameter "dayOfMonth"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 641
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@3
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v1

    #@7
    if-ne v1, p1, :cond_1b

    #@9
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@b
    const/4 v2, 0x2

    #@c
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@f
    move-result v1

    #@10
    if-ne v1, p3, :cond_1b

    #@12
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@14
    const/4 v2, 0x5

    #@15
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@18
    move-result v1

    #@19
    if-eq v1, p2, :cond_1c

    #@1b
    :cond_1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method private notifyDateChanged()V
    .registers 5

    #@0
    .prologue
    .line 744
    const/4 v0, 0x4

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/DatePicker;->sendAccessibilityEvent(I)V

    #@4
    .line 745
    iget-object v0, p0, Landroid/widget/DatePicker;->mOnDateChangedListener:Landroid/widget/DatePicker$OnDateChangedListener;

    #@6
    if-eqz v0, :cond_19

    #@8
    .line 746
    iget-object v0, p0, Landroid/widget/DatePicker;->mOnDateChangedListener:Landroid/widget/DatePicker$OnDateChangedListener;

    #@a
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getYear()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getMonth()I

    #@11
    move-result v2

    #@12
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    #@15
    move-result v3

    #@16
    invoke-interface {v0, p0, v1, v2, v3}, Landroid/widget/DatePicker$OnDateChangedListener;->onDateChanged(Landroid/widget/DatePicker;III)V

    #@19
    .line 748
    :cond_19
    return-void
.end method

.method private parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z
    .registers 7
    .parameter "date"
    .parameter "outDate"

    #@0
    .prologue
    .line 632
    :try_start_0
    iget-object v1, p0, Landroid/widget/DatePicker;->mDateFormat:Ljava/text/DateFormat;

    #@2
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_9
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    .line 633
    const/4 v1, 0x1

    #@a
    .line 636
    :goto_a
    return v1

    #@b
    .line 634
    :catch_b
    move-exception v0

    #@c
    .line 635
    .local v0, e:Ljava/text/ParseException;
    sget-object v1, Landroid/widget/DatePicker;->LOG_TAG:Ljava/lang/String;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Date: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " not in format: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, "MM/dd/yyyy"

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 636
    const/4 v1, 0x0

    #@31
    goto :goto_a
.end method

.method private reorderSpinners()V
    .registers 6

    #@0
    .prologue
    .line 545
    iget-object v3, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    #@5
    .line 546
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getContext()Landroid/content/Context;

    #@8
    move-result-object v3

    #@9
    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    #@c
    move-result-object v1

    #@d
    .line 547
    .local v1, order:[C
    array-length v2, v1

    #@e
    .line 548
    .local v2, spinnerCount:I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    if-ge v0, v2, :cond_45

    #@11
    .line 549
    aget-char v3, v1, v0

    #@13
    sparse-switch v3, :sswitch_data_46

    #@16
    .line 563
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@18
    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@1b
    throw v3

    #@1c
    .line 551
    :sswitch_1c
    iget-object v3, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@1e
    iget-object v4, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@20
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@23
    .line 552
    iget-object v3, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@25
    invoke-direct {p0, v3, v2, v0}, Landroid/widget/DatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    #@28
    .line 548
    :goto_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_f

    #@2b
    .line 555
    :sswitch_2b
    iget-object v3, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@2d
    iget-object v4, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@2f
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@32
    .line 556
    iget-object v3, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@34
    invoke-direct {p0, v3, v2, v0}, Landroid/widget/DatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    #@37
    goto :goto_28

    #@38
    .line 559
    :sswitch_38
    iget-object v3, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@3a
    iget-object v4, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@3c
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@3f
    .line 560
    iget-object v3, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@41
    invoke-direct {p0, v3, v2, v0}, Landroid/widget/DatePicker;->setImeOptions(Landroid/widget/NumberPicker;II)V

    #@44
    goto :goto_28

    #@45
    .line 566
    :cond_45
    return-void

    #@46
    .line 549
    :sswitch_data_46
    .sparse-switch
        0x4d -> :sswitch_2b
        0x64 -> :sswitch_1c
        0x79 -> :sswitch_38
    .end sparse-switch
.end method

.method private setContentDescriptions()V
    .registers 5

    #@0
    .prologue
    const v3, 0x102034a

    #@3
    const v2, 0x1020348

    #@6
    .line 770
    iget-object v0, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@8
    const v1, 0x10404ef

    #@b
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@e
    .line 772
    iget-object v0, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@10
    const v1, 0x10404f0

    #@13
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@16
    .line 775
    iget-object v0, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@18
    const v1, 0x10404ed

    #@1b
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@1e
    .line 777
    iget-object v0, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@20
    const v1, 0x10404ee

    #@23
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@26
    .line 780
    iget-object v0, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@28
    const v1, 0x10404f1

    #@2b
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@2e
    .line 782
    iget-object v0, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@30
    const v1, 0x10404f2

    #@33
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/DatePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@36
    .line 784
    return-void
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .registers 8
    .parameter "locale"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 488
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentLocale:Ljava/util/Locale;

    #@4
    invoke-virtual {p1, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_b

    #@a
    .line 520
    :goto_a
    return-void

    #@b
    .line 492
    :cond_b
    iput-object p1, p0, Landroid/widget/DatePicker;->mCurrentLocale:Ljava/util/Locale;

    #@d
    .line 494
    iget-object v1, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@f
    invoke-direct {p0, v1, p1}, Landroid/widget/DatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@12
    move-result-object v1

    #@13
    iput-object v1, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@15
    .line 495
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@17
    invoke-direct {p0, v1, p1}, Landroid/widget/DatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@1d
    .line 496
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@1f
    invoke-direct {p0, v1, p1}, Landroid/widget/DatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@25
    .line 497
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@27
    invoke-direct {p0, v1, p1}, Landroid/widget/DatePicker;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@2a
    move-result-object v1

    #@2b
    iput-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2d
    .line 499
    iget-object v1, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@2f
    const/4 v2, 0x2

    #@30
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@33
    move-result v1

    #@34
    add-int/lit8 v1, v1, 0x1

    #@36
    iput v1, p0, Landroid/widget/DatePicker;->mNumberOfMonths:I

    #@38
    .line 500
    iget v1, p0, Landroid/widget/DatePicker;->mNumberOfMonths:I

    #@3a
    new-array v1, v1, [Ljava/lang/String;

    #@3c
    iput-object v1, p0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@3e
    .line 501
    const/4 v0, 0x0

    #@3f
    .local v0, i:I
    :goto_3f
    iget v1, p0, Landroid/widget/DatePicker;->mNumberOfMonths:I

    #@41
    if-ge v0, v1, :cond_52

    #@43
    .line 502
    iget-object v1, p0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@45
    add-int/lit8 v2, v0, 0x0

    #@47
    const/16 v3, 0x14

    #@49
    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    aput-object v2, v1, v0

    #@4f
    .line 501
    add-int/lit8 v0, v0, 0x1

    #@51
    goto :goto_3f

    #@52
    .line 512
    :cond_52
    iget-object v1, p0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@54
    array-length v1, v1

    #@55
    if-lez v1, :cond_68

    #@57
    iget-object v1, p0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@59
    aget-object v1, v1, v4

    #@5b
    const-string v2, "1"

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@60
    move-result v1

    #@61
    if-eqz v1, :cond_68

    #@63
    .line 513
    iput-boolean v5, p0, Landroid/widget/DatePicker;->mIsShowNumberOnlyAboutMonth:Z

    #@65
    .line 514
    iput v5, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@67
    goto :goto_a

    #@68
    .line 516
    :cond_68
    iput-boolean v4, p0, Landroid/widget/DatePicker;->mIsShowNumberOnlyAboutMonth:Z

    #@6a
    .line 517
    iput v4, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@6c
    goto :goto_a
.end method

.method private setDate(III)V
    .registers 7
    .parameter "year"
    .parameter "month"
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 647
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    #@5
    .line 648
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@7
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@9
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_1b

    #@f
    .line 649
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@11
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@13
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@16
    move-result-wide v1

    #@17
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@1a
    .line 653
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 650
    :cond_1b
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1d
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@1f
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_1a

    #@25
    .line 651
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@27
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@29
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2c
    move-result-wide v1

    #@2d
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@30
    goto :goto_1a
.end method

.method private setImeOptions(Landroid/widget/NumberPicker;II)V
    .registers 7
    .parameter "spinner"
    .parameter "spinnerCount"
    .parameter "spinnerIndex"

    #@0
    .prologue
    .line 759
    add-int/lit8 v2, p2, -0x1

    #@2
    if-ge p3, v2, :cond_12

    #@4
    .line 760
    const/4 v0, 0x5

    #@5
    .line 764
    .local v0, imeOptions:I
    :goto_5
    const v2, 0x1020349

    #@8
    invoke-virtual {p1, v2}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/widget/TextView;

    #@e
    .line 765
    .local v1, input:Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setImeOptions(I)V

    #@11
    .line 766
    return-void

    #@12
    .line 762
    .end local v0           #imeOptions:I
    .end local v1           #input:Landroid/widget/TextView;
    :cond_12
    const/4 v0, 0x6

    #@13
    .restart local v0       #imeOptions:I
    goto :goto_5
.end method

.method private trySetContentDescription(Landroid/view/View;II)V
    .registers 6
    .parameter "root"
    .parameter "viewId"
    .parameter "contDescResId"

    #@0
    .prologue
    .line 787
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 788
    .local v0, target:Landroid/view/View;
    if-eqz v0, :cond_f

    #@6
    .line 789
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    #@f
    .line 791
    :cond_f
    return-void
.end method

.method private updateCalendarView()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 716
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@3
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@5
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@8
    move-result-wide v1

    #@9
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/CalendarView;->setDate(JZZ)V

    #@c
    .line 717
    return-void
.end method

.method private updateInputState()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 799
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@4
    move-result-object v0

    #@5
    .line 800
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1b

    #@7
    .line 801
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    #@9
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1c

    #@f
    .line 802
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinnerInput:Landroid/widget/EditText;

    #@11
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@14
    .line 803
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getWindowToken()Landroid/os/IBinder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1b
    .line 812
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 804
    :cond_1c
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_31

    #@24
    .line 805
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinnerInput:Landroid/widget/EditText;

    #@26
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@29
    .line 806
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getWindowToken()Landroid/os/IBinder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@30
    goto :goto_1b

    #@31
    .line 807
    :cond_31
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    #@33
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_1b

    #@39
    .line 808
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinnerInput:Landroid/widget/EditText;

    #@3b
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@3e
    .line 809
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getWindowToken()Landroid/os/IBinder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@45
    goto :goto_1b
.end method

.method private updateSpinners()V
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v7, 0x2

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v5, 0x5

    #@4
    const/4 v4, 0x1

    #@5
    .line 657
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@7
    iget-object v2, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@9
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_af

    #@f
    .line 658
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@11
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@13
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    #@16
    move-result v2

    #@17
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@1a
    .line 659
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@1c
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@1e
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@21
    move-result v2

    #@22
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@25
    .line 660
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@27
    invoke-virtual {v1, v6}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@2a
    .line 661
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@2c
    invoke-virtual {v1, v3}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@2f
    .line 663
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@31
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@33
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    #@36
    move-result v2

    #@37
    iget v3, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@39
    add-int/2addr v2, v3

    #@3a
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@3d
    .line 664
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@3f
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@41
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@44
    move-result v2

    #@45
    iget v3, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@47
    add-int/2addr v2, v3

    #@48
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@4b
    .line 666
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@4d
    invoke-virtual {v1, v6}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@50
    .line 690
    :goto_50
    iget-boolean v1, p0, Landroid/widget/DatePicker;->mIsShowNumberOnlyAboutMonth:Z

    #@52
    if-nez v1, :cond_6f

    #@54
    .line 693
    iget-object v1, p0, Landroid/widget/DatePicker;->mShortMonths:[Ljava/lang/String;

    #@56
    iget-object v2, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@58
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getMinValue()I

    #@5b
    move-result v2

    #@5c
    iget-object v3, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@5e
    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getMaxValue()I

    #@61
    move-result v3

    #@62
    add-int/lit8 v3, v3, 0x1

    #@64
    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    #@67
    move-result-object v0

    #@68
    check-cast v0, [Ljava/lang/String;

    #@6a
    .line 695
    .local v0, displayedValues:[Ljava/lang/String;
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@6c
    invoke-virtual {v1, v0}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@6f
    .line 700
    .end local v0           #displayedValues:[Ljava/lang/String;
    :cond_6f
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@71
    iget-object v2, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@73
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    #@76
    move-result v2

    #@77
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@7a
    .line 701
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@7c
    iget-object v2, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@7e
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    #@81
    move-result v2

    #@82
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@85
    .line 702
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@87
    invoke-virtual {v1, v6}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@8a
    .line 705
    iget-object v1, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@8c
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@8e
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    #@91
    move-result v2

    #@92
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setValue(I)V

    #@95
    .line 707
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@97
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@99
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    #@9c
    move-result v2

    #@9d
    iget v3, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@9f
    add-int/2addr v2, v3

    #@a0
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setValue(I)V

    #@a3
    .line 709
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@a5
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@a7
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    #@aa
    move-result v2

    #@ab
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setValue(I)V

    #@ae
    .line 710
    return-void

    #@af
    .line 667
    :cond_af
    iget-object v1, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@b1
    iget-object v2, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@b3
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v1

    #@b7
    if-eqz v1, :cond_fc

    #@b9
    .line 668
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@bb
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@bd
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMinimum(I)I

    #@c0
    move-result v2

    #@c1
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@c4
    .line 669
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@c6
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@c8
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    #@cb
    move-result v2

    #@cc
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@cf
    .line 670
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@d1
    invoke-virtual {v1, v6}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@d4
    .line 671
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@d6
    invoke-virtual {v1, v3}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@d9
    .line 673
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@db
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@dd
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->getActualMinimum(I)I

    #@e0
    move-result v2

    #@e1
    iget v3, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@e3
    add-int/2addr v2, v3

    #@e4
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@e7
    .line 674
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@e9
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@eb
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    #@ee
    move-result v2

    #@ef
    iget v3, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@f1
    add-int/2addr v2, v3

    #@f2
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@f5
    .line 676
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@f7
    invoke-virtual {v1, v6}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@fa
    goto/16 :goto_50

    #@fc
    .line 678
    :cond_fc
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@fe
    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@101
    .line 679
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@103
    iget-object v2, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@105
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@108
    move-result v2

    #@109
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@10c
    .line 680
    iget-object v1, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@10e
    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@111
    .line 681
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@113
    invoke-virtual {v1, v3}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@116
    .line 683
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@118
    iget v2, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@11a
    add-int/lit8 v2, v2, 0x0

    #@11c
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@11f
    .line 684
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@121
    iget v2, p0, Landroid/widget/DatePicker;->mMonthOffset:I

    #@123
    add-int/lit8 v2, v2, 0xb

    #@125
    invoke-virtual {v1, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@128
    .line 686
    iget-object v1, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@12a
    invoke-virtual {v1, v4}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@12d
    goto/16 :goto_50
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 403
    invoke-virtual {p0, p1}, Landroid/widget/DatePicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 404
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 588
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Landroid/widget/DatePicker;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    #@3
    .line 589
    return-void
.end method

.method public getCalendarView()Landroid/widget/CalendarView;
    .registers 2

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2
    return-object v0
.end method

.method public getCalendarViewShown()Z
    .registers 2

    #@0
    .prologue
    .line 442
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2
    invoke-virtual {v0}, Landroid/widget/CalendarView;->isShown()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDayOfMonth()I
    .registers 3

    #@0
    .prologue
    .line 737
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x5

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getMaxDate()J
    .registers 3

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2
    invoke-virtual {v0}, Landroid/widget/CalendarView;->getMaxDate()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getMinDate()J
    .registers 3

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2
    invoke-virtual {v0}, Landroid/widget/CalendarView;->getMinDate()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getMonth()I
    .registers 3

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getSpinnersShown()Z
    .registers 2

    #@0
    .prologue
    .line 470
    iget-object v0, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getYear()I
    .registers 3

    #@0
    .prologue
    .line 723
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V
    .registers 5
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"
    .parameter "onDateChangedListener"

    #@0
    .prologue
    .line 618
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/DatePicker;->setDate(III)V

    #@3
    .line 619
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@6
    .line 620
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@9
    .line 621
    iput-object p4, p0, Landroid/widget/DatePicker;->mOnDateChangedListener:Landroid/widget/DatePicker$OnDateChangedListener;

    #@b
    .line 622
    return-void
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 398
    iget-boolean v0, p0, Landroid/widget/DatePicker;->mIsEnabled:Z

    #@2
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 431
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 432
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5
    invoke-direct {p0, v0}, Landroid/widget/DatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    #@8
    .line 433
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 419
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 420
    const-class v0, Landroid/widget/DatePicker;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 421
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 425
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 426
    const-class v0, Landroid/widget/DatePicker;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 427
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 409
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 411
    const/16 v0, 0x14

    #@5
    .line 412
    .local v0, flags:I
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@7
    iget-object v3, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@9
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    #@c
    move-result-wide v3

    #@d
    const/16 v5, 0x14

    #@f
    invoke-static {v2, v3, v4, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 414
    .local v1, selectedDateUtterance:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@16
    move-result-object v2

    #@17
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1a
    .line 415
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 599
    move-object v0, p1

    #@1
    check-cast v0, Landroid/widget/DatePicker$SavedState;

    #@3
    .line 600
    .local v0, ss:Landroid/widget/DatePicker$SavedState;
    invoke-virtual {v0}, Landroid/widget/DatePicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 601
    invoke-static {v0}, Landroid/widget/DatePicker$SavedState;->access$1200(Landroid/widget/DatePicker$SavedState;)I

    #@d
    move-result v1

    #@e
    invoke-static {v0}, Landroid/widget/DatePicker$SavedState;->access$1300(Landroid/widget/DatePicker$SavedState;)I

    #@11
    move-result v2

    #@12
    invoke-static {v0}, Landroid/widget/DatePicker$SavedState;->access$1400(Landroid/widget/DatePicker$SavedState;)I

    #@15
    move-result v3

    #@16
    invoke-direct {p0, v1, v2, v3}, Landroid/widget/DatePicker;->setDate(III)V

    #@19
    .line 602
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@1c
    .line 603
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@1f
    .line 604
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 7

    #@0
    .prologue
    .line 593
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 594
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Landroid/widget/DatePicker$SavedState;

    #@6
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getYear()I

    #@9
    move-result v2

    #@a
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getMonth()I

    #@d
    move-result v3

    #@e
    invoke-virtual {p0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    #@11
    move-result v4

    #@12
    const/4 v5, 0x0

    #@13
    invoke-direct/range {v0 .. v5}, Landroid/widget/DatePicker$SavedState;-><init>(Landroid/os/Parcelable;IIILandroid/widget/DatePicker$1;)V

    #@16
    return-object v0
.end method

.method public setCalendarViewShown(Z)V
    .registers 4
    .parameter "shown"

    #@0
    .prologue
    .line 461
    iget-object v1, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/CalendarView;->setVisibility(I)V

    #@8
    .line 462
    return-void

    #@9
    .line 461
    :cond_9
    const/16 v0, 0x8

    #@b
    goto :goto_5
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 385
    iget-boolean v0, p0, Landroid/widget/DatePicker;->mIsEnabled:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 394
    :goto_4
    return-void

    #@5
    .line 388
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    #@8
    .line 389
    iget-object v0, p0, Landroid/widget/DatePicker;->mDaySpinner:Landroid/widget/NumberPicker;

    #@a
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@d
    .line 390
    iget-object v0, p0, Landroid/widget/DatePicker;->mMonthSpinner:Landroid/widget/NumberPicker;

    #@f
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@12
    .line 391
    iget-object v0, p0, Landroid/widget/DatePicker;->mYearSpinner:Landroid/widget/NumberPicker;

    #@14
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@17
    .line 392
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@19
    invoke-virtual {v0, p1}, Landroid/widget/CalendarView;->setEnabled(Z)V

    #@1c
    .line 393
    iput-boolean p1, p0, Landroid/widget/DatePicker;->mIsEnabled:Z

    #@1e
    goto :goto_4
.end method

.method public setMaxDate(J)V
    .registers 7
    .parameter "maxDate"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x1

    #@2
    .line 369
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@4
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@7
    .line 370
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@9
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    #@c
    move-result v0

    #@d
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@f
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@12
    move-result v1

    #@13
    if-ne v0, v1, :cond_24

    #@15
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@17
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    #@1a
    move-result v0

    #@1b
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@1d
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v1

    #@21
    if-eq v0, v1, :cond_24

    #@23
    .line 381
    :goto_23
    return-void

    #@24
    .line 374
    :cond_24
    iget-object v0, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@26
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@29
    .line 375
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2b
    invoke-virtual {v0, p1, p2}, Landroid/widget/CalendarView;->setMaxDate(J)V

    #@2e
    .line 376
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@30
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@32
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_46

    #@38
    .line 377
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@3a
    iget-object v1, p0, Landroid/widget/DatePicker;->mMaxDate:Ljava/util/Calendar;

    #@3c
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3f
    move-result-wide v1

    #@40
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@43
    .line 378
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@46
    .line 380
    :cond_46
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@49
    goto :goto_23
.end method

.method public setMinDate(J)V
    .registers 7
    .parameter "minDate"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x1

    #@2
    .line 333
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@4
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@7
    .line 334
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@9
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    #@c
    move-result v0

    #@d
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@f
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@12
    move-result v1

    #@13
    if-ne v0, v1, :cond_24

    #@15
    iget-object v0, p0, Landroid/widget/DatePicker;->mTempDate:Ljava/util/Calendar;

    #@17
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    #@1a
    move-result v0

    #@1b
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@1d
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v1

    #@21
    if-eq v0, v1, :cond_24

    #@23
    .line 345
    :goto_23
    return-void

    #@24
    .line 338
    :cond_24
    iget-object v0, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@26
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@29
    .line 339
    iget-object v0, p0, Landroid/widget/DatePicker;->mCalendarView:Landroid/widget/CalendarView;

    #@2b
    invoke-virtual {v0, p1, p2}, Landroid/widget/CalendarView;->setMinDate(J)V

    #@2e
    .line 340
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@30
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@32
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_46

    #@38
    .line 341
    iget-object v0, p0, Landroid/widget/DatePicker;->mCurrentDate:Ljava/util/Calendar;

    #@3a
    iget-object v1, p0, Landroid/widget/DatePicker;->mMinDate:Ljava/util/Calendar;

    #@3c
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3f
    move-result-wide v1

    #@40
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@43
    .line 342
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@46
    .line 344
    :cond_46
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@49
    goto :goto_23
.end method

.method public setSpinnersShown(Z)V
    .registers 4
    .parameter "shown"

    #@0
    .prologue
    .line 479
    iget-object v1, p0, Landroid/widget/DatePicker;->mSpinners:Landroid/widget/LinearLayout;

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@8
    .line 480
    return-void

    #@9
    .line 479
    :cond_9
    const/16 v0, 0x8

    #@b
    goto :goto_5
.end method

.method public updateDate(III)V
    .registers 5
    .parameter "year"
    .parameter "month"
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 576
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/DatePicker;->isNewDate(III)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 583
    :goto_6
    return-void

    #@7
    .line 579
    :cond_7
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/DatePicker;->setDate(III)V

    #@a
    .line 580
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateSpinners()V

    #@d
    .line 581
    invoke-direct {p0}, Landroid/widget/DatePicker;->updateCalendarView()V

    #@10
    .line 582
    invoke-direct {p0}, Landroid/widget/DatePicker;->notifyDateChanged()V

    #@13
    goto :goto_6
.end method
