.class Landroid/widget/AbsListView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "AbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/widget/AbsListView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field checkIdState:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field checkState:Landroid/util/SparseBooleanArray;

.field checkedItemCount:I

.field filter:Ljava/lang/String;

.field firstId:J

.field height:I

.field inActionMode:Z

.field position:I

.field selectedId:J

.field viewTop:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1627
    new-instance v0, Landroid/widget/AbsListView$SavedState$1;

    #@2
    invoke-direct {v0}, Landroid/widget/AbsListView$SavedState$1;-><init>()V

    #@5
    sput-object v0, Landroid/widget/AbsListView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 9
    .parameter "in"

    #@0
    .prologue
    .line 1573
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    #@3
    .line 1574
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@6
    move-result-wide v5

    #@7
    iput-wide v5, p0, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@9
    .line 1575
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@c
    move-result-wide v5

    #@d
    iput-wide v5, p0, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@f
    .line 1576
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v5

    #@13
    iput v5, p0, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@15
    .line 1577
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v5

    #@19
    iput v5, p0, Landroid/widget/AbsListView$SavedState;->position:I

    #@1b
    .line 1578
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v5

    #@1f
    iput v5, p0, Landroid/widget/AbsListView$SavedState;->height:I

    #@21
    .line 1579
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    iput-object v5, p0, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@27
    .line 1580
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_60

    #@2d
    const/4 v5, 0x1

    #@2e
    :goto_2e
    iput-boolean v5, p0, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@30
    .line 1581
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v5

    #@34
    iput v5, p0, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@36
    .line 1582
    invoke-virtual {p1}, Landroid/os/Parcel;->readSparseBooleanArray()Landroid/util/SparseBooleanArray;

    #@39
    move-result-object v5

    #@3a
    iput-object v5, p0, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@3c
    .line 1583
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v0

    #@40
    .line 1584
    .local v0, N:I
    if-lez v0, :cond_62

    #@42
    .line 1585
    new-instance v5, Landroid/util/LongSparseArray;

    #@44
    invoke-direct {v5}, Landroid/util/LongSparseArray;-><init>()V

    #@47
    iput-object v5, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@49
    .line 1586
    const/4 v1, 0x0

    #@4a
    .local v1, i:I
    :goto_4a
    if-ge v1, v0, :cond_62

    #@4c
    .line 1587
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@4f
    move-result-wide v2

    #@50
    .line 1588
    .local v2, key:J
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v4

    #@54
    .line 1589
    .local v4, value:I
    iget-object v5, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@56
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v5, v2, v3, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@5d
    .line 1586
    add-int/lit8 v1, v1, 0x1

    #@5f
    goto :goto_4a

    #@60
    .line 1580
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #key:J
    .end local v4           #value:I
    :cond_60
    const/4 v5, 0x0

    #@61
    goto :goto_2e

    #@62
    .line 1592
    .restart local v0       #N:I
    :cond_62
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/widget/AbsListView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1550
    invoke-direct {p0, p1}, Landroid/widget/AbsListView$SavedState;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .registers 2
    .parameter "superState"

    #@0
    .prologue
    .line 1566
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    #@3
    .line 1567
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1616
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "AbsListView.SavedState{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " selectedId="

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget-wide v1, p0, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " firstId="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget-wide v1, p0, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@2b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " viewTop="

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    iget v1, p0, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " position="

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget v1, p0, Landroid/widget/AbsListView$SavedState;->position:I

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, " height="

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget v1, p0, Landroid/widget/AbsListView$SavedState;->height:I

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, " filter="

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget-object v1, p0, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string v1, " checkState="

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    iget-object v1, p0, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    const-string/jumbo v1, "}"

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1596
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    #@4
    .line 1597
    iget-wide v4, p0, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@6
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@9
    .line 1598
    iget-wide v4, p0, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@b
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@e
    .line 1599
    iget v2, p0, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@10
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1600
    iget v2, p0, Landroid/widget/AbsListView$SavedState;->position:I

    #@15
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 1601
    iget v2, p0, Landroid/widget/AbsListView$SavedState;->height:I

    #@1a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 1602
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@1f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@22
    .line 1603
    iget-boolean v2, p0, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@24
    if-eqz v2, :cond_60

    #@26
    const/4 v2, 0x1

    #@27
    :goto_27
    int-to-byte v2, v2

    #@28
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@2b
    .line 1604
    iget v2, p0, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@2d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 1605
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@32
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeSparseBooleanArray(Landroid/util/SparseBooleanArray;)V

    #@35
    .line 1606
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@37
    if-eqz v2, :cond_62

    #@39
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@3b
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    #@3e
    move-result v0

    #@3f
    .line 1607
    .local v0, N:I
    :goto_3f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    .line 1608
    const/4 v1, 0x0

    #@43
    .local v1, i:I
    :goto_43
    if-ge v1, v0, :cond_64

    #@45
    .line 1609
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@47
    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    #@4a
    move-result-wide v2

    #@4b
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@4e
    .line 1610
    iget-object v2, p0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@50
    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@53
    move-result-object v2

    #@54
    check-cast v2, Ljava/lang/Integer;

    #@56
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@59
    move-result v2

    #@5a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 1608
    add-int/lit8 v1, v1, 0x1

    #@5f
    goto :goto_43

    #@60
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_60
    move v2, v3

    #@61
    .line 1603
    goto :goto_27

    #@62
    :cond_62
    move v0, v3

    #@63
    .line 1606
    goto :goto_3f

    #@64
    .line 1612
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_64
    return-void
.end method
