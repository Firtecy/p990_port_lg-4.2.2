.class public final enum Landroid/widget/ImageView$ScaleType;
.super Ljava/lang/Enum;
.source "ImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScaleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/widget/ImageView$ScaleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/widget/ImageView$ScaleType;

.field public static final enum CENTER:Landroid/widget/ImageView$ScaleType;

.field public static final enum CENTER_CROP:Landroid/widget/ImageView$ScaleType;

.field public static final enum CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

.field public static final enum FIT_CENTER:Landroid/widget/ImageView$ScaleType;

.field public static final enum FIT_END:Landroid/widget/ImageView$ScaleType;

.field public static final enum FIT_START:Landroid/widget/ImageView$ScaleType;

.field public static final enum FIT_XY:Landroid/widget/ImageView$ScaleType;

.field public static final enum MATRIX:Landroid/widget/ImageView$ScaleType;


# instance fields
.field final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 463
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@7
    const-string v1, "MATRIX"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    #@e
    .line 468
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@10
    const-string v1, "FIT_XY"

    #@12
    invoke-direct {v0, v1, v5, v5}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    #@17
    .line 473
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@19
    const-string v1, "FIT_START"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    #@20
    .line 479
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@22
    const-string v1, "FIT_CENTER"

    #@24
    invoke-direct {v0, v1, v7, v7}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    #@29
    .line 484
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@2b
    const-string v1, "FIT_END"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    #@32
    .line 489
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@34
    const-string v1, "CENTER"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@3d
    .line 497
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@3f
    const-string v1, "CENTER_CROP"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    #@48
    .line 505
    new-instance v0, Landroid/widget/ImageView$ScaleType;

    #@4a
    const-string v1, "CENTER_INSIDE"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageView$ScaleType;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    #@53
    .line 457
    const/16 v0, 0x8

    #@55
    new-array v0, v0, [Landroid/widget/ImageView$ScaleType;

    #@57
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    #@59
    aput-object v1, v0, v4

    #@5b
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    #@5d
    aput-object v1, v0, v5

    #@5f
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    #@61
    aput-object v1, v0, v6

    #@63
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    #@65
    aput-object v1, v0, v7

    #@67
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    #@69
    aput-object v1, v0, v8

    #@6b
    const/4 v1, 0x5

    #@6c
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@6e
    aput-object v2, v0, v1

    #@70
    const/4 v1, 0x6

    #@71
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    #@73
    aput-object v2, v0, v1

    #@75
    const/4 v1, 0x7

    #@76
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    #@78
    aput-object v2, v0, v1

    #@7a
    sput-object v0, Landroid/widget/ImageView$ScaleType;->$VALUES:[Landroid/widget/ImageView$ScaleType;

    #@7c
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "ni"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 507
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 508
    iput p3, p0, Landroid/widget/ImageView$ScaleType;->nativeInt:I

    #@5
    .line 509
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/widget/ImageView$ScaleType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 457
    const-class v0, Landroid/widget/ImageView$ScaleType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/widget/ImageView$ScaleType;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/widget/ImageView$ScaleType;
    .registers 1

    #@0
    .prologue
    .line 457
    sget-object v0, Landroid/widget/ImageView$ScaleType;->$VALUES:[Landroid/widget/ImageView$ScaleType;

    #@2
    invoke-virtual {v0}, [Landroid/widget/ImageView$ScaleType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/widget/ImageView$ScaleType;

    #@8
    return-object v0
.end method
