.class Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
.super Ljava/lang/Object;
.source "RemoteViewsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViewsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RemoteViewsMetaData"
.end annotation


# instance fields
.field count:I

.field hasStableIds:Z

.field mFirstView:Landroid/widget/RemoteViews;

.field mFirstViewHeight:I

.field private final mTypeIdIndexMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mUserLoadingView:Landroid/widget/RemoteViews;

.field viewTypeCount:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 412
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 410
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@a
    .line 413
    invoke-virtual {p0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->reset()V

    #@d
    .line 414
    return-void
.end method

.method static synthetic access$1900(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 397
    invoke-direct/range {p0 .. p6}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->createLoadingView(ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private createLoadingView(ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .registers 19
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "lock"
    .parameter "layoutInflater"
    .parameter "handler"

    #@0
    .prologue
    .line 469
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    .line 470
    .local v1, context:Landroid/content/Context;
    new-instance v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@6
    invoke-direct {v6, v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;-><init>(Landroid/content/Context;)V

    #@9
    .line 473
    .local v6, layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    monitor-enter p4

    #@a
    .line 474
    const/4 v2, 0x0

    #@b
    .line 476
    .local v2, customLoadingViewAvailable:Z
    :try_start_b
    iget-object v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mUserLoadingView:Landroid/widget/RemoteViews;
    :try_end_d
    .catchall {:try_start_b .. :try_end_d} :catchall_7d

    #@d
    if-eqz v9, :cond_2b

    #@f
    .line 479
    :try_start_f
    iget-object v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mUserLoadingView:Landroid/widget/RemoteViews;

    #@11
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@14
    move-result-object v10

    #@15
    move-object/from16 v0, p6

    #@17
    invoke-virtual {v9, v10, p3, v0}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;

    #@1a
    move-result-object v8

    #@1b
    .line 481
    .local v8, loadingView:Landroid/view/View;
    const v9, 0x1020253

    #@1e
    new-instance v10, Ljava/lang/Integer;

    #@20
    const/4 v11, 0x0

    #@21
    invoke-direct {v10, v11}, Ljava/lang/Integer;-><init>(I)V

    #@24
    invoke-virtual {v8, v9, v10}, Landroid/view/View;->setTagInternal(ILjava/lang/Object;)V

    #@27
    .line 483
    invoke-virtual {v6, v8}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;->addView(Landroid/view/View;)V
    :try_end_2a
    .catchall {:try_start_f .. :try_end_2a} :catchall_7d
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_2a} :catch_74

    #@2a
    .line 484
    const/4 v2, 0x1

    #@2b
    .line 490
    .end local v8           #loadingView:Landroid/view/View;
    :cond_2b
    :goto_2b
    if-nez v2, :cond_72

    #@2d
    .line 493
    :try_start_2d
    iget v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I
    :try_end_2f
    .catchall {:try_start_2d .. :try_end_2f} :catchall_7d

    #@2f
    if-gez v9, :cond_55

    #@31
    .line 495
    :try_start_31
    iget-object v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;

    #@33
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@36
    move-result-object v10

    #@37
    move-object/from16 v0, p6

    #@39
    invoke-virtual {v9, v10, p3, v0}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;

    #@3c
    move-result-object v5

    #@3d
    .line 496
    .local v5, firstView:Landroid/view/View;
    const/4 v9, 0x0

    #@3e
    const/4 v10, 0x0

    #@3f
    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@42
    move-result v9

    #@43
    const/4 v10, 0x0

    #@44
    const/4 v11, 0x0

    #@45
    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@48
    move-result v10

    #@49
    invoke-virtual {v5, v9, v10}, Landroid/view/View;->measure(II)V

    #@4c
    .line 499
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@4f
    move-result v9

    #@50
    iput v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I

    #@52
    .line 500
    const/4 v9, 0x0

    #@53
    iput-object v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;
    :try_end_55
    .catchall {:try_start_31 .. :try_end_55} :catchall_7d
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_55} :catch_80

    #@55
    .line 511
    .end local v5           #firstView:Landroid/view/View;
    :cond_55
    :goto_55
    const v9, 0x10900b4

    #@58
    const/4 v10, 0x0

    #@59
    :try_start_59
    move-object/from16 v0, p5

    #@5b
    invoke-virtual {v0, v9, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@5e
    move-result-object v7

    #@5f
    check-cast v7, Landroid/widget/TextView;

    #@61
    .line 514
    .local v7, loadingTextView:Landroid/widget/TextView;
    iget v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I

    #@63
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setHeight(I)V

    #@66
    .line 515
    new-instance v9, Ljava/lang/Integer;

    #@68
    const/4 v10, 0x0

    #@69
    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    #@6c
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    #@6f
    .line 517
    invoke-virtual {v6, v7}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;->addView(Landroid/view/View;)V

    #@72
    .line 519
    .end local v7           #loadingTextView:Landroid/widget/TextView;
    :cond_72
    monitor-exit p4

    #@73
    .line 521
    return-object v6

    #@74
    .line 485
    :catch_74
    move-exception v4

    #@75
    .line 486
    .local v4, e:Ljava/lang/Exception;
    const-string v9, "RemoteViewsAdapter"

    #@77
    const-string v10, "Error inflating custom loading view, using default loadingview instead"

    #@79
    invoke-static {v9, v10, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7c
    goto :goto_2b

    #@7d
    .line 519
    .end local v4           #e:Ljava/lang/Exception;
    :catchall_7d
    move-exception v9

    #@7e
    monitor-exit p4
    :try_end_7f
    .catchall {:try_start_59 .. :try_end_7f} :catchall_7d

    #@7f
    throw v9

    #@80
    .line 501
    :catch_80
    move-exception v4

    #@81
    .line 502
    .restart local v4       #e:Ljava/lang/Exception;
    :try_start_81
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@84
    move-result-object v9

    #@85
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@88
    move-result-object v9

    #@89
    iget v3, v9, Landroid/util/DisplayMetrics;->density:F

    #@8b
    .line 503
    .local v3, density:F
    const/high16 v9, 0x4248

    #@8d
    mul-float/2addr v9, v3

    #@8e
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@91
    move-result v9

    #@92
    iput v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I

    #@94
    .line 505
    const/4 v9, 0x0

    #@95
    iput-object v9, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;

    #@97
    .line 506
    const-string v9, "RemoteViewsAdapter"

    #@99
    new-instance v10, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v11, "Error inflating first RemoteViews"

    #@a0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v10

    #@a8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v10

    #@ac
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_af
    .catchall {:try_start_81 .. :try_end_af} :catchall_7d

    #@af
    goto :goto_55
.end method


# virtual methods
.method public getMappedViewType(I)I
    .registers 6
    .parameter "typeId"

    #@0
    .prologue
    .line 446
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1d

    #@c
    .line 447
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Ljava/lang/Integer;

    #@18
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@1b
    move-result v0

    #@1c
    .line 452
    :goto_1c
    return v0

    #@1d
    .line 450
    :cond_1d
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    #@22
    move-result v1

    #@23
    add-int/lit8 v0, v1, 0x1

    #@25
    .line 451
    .local v0, incrementalTypeId:I
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    goto :goto_1c
.end method

.method public isViewTypeInRange(I)Z
    .registers 4
    .parameter "typeId"

    #@0
    .prologue
    .line 457
    invoke-virtual {p0, p1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->getMappedViewType(I)I

    #@3
    move-result v0

    #@4
    .line 458
    .local v0, mappedType:I
    iget v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@6
    if-lt v0, v1, :cond_a

    #@8
    .line 459
    const/4 v1, 0x0

    #@9
    .line 461
    :goto_9
    return v1

    #@a
    :cond_a
    const/4 v1, 0x1

    #@b
    goto :goto_9
.end method

.method public reset()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v0, 0x0

    #@3
    .line 426
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@5
    .line 429
    iput v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@7
    .line 430
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->hasStableIds:Z

    #@9
    .line 431
    iput-object v2, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mUserLoadingView:Landroid/widget/RemoteViews;

    #@b
    .line 432
    iput-object v2, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;

    #@d
    .line 433
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I

    #@f
    .line 434
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mTypeIdIndexMap:Ljava/util/HashMap;

    #@11
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@14
    .line 435
    return-void
.end method

.method public set(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 417
    monitor-enter p1

    #@1
    .line 418
    :try_start_1
    iget v0, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@3
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@5
    .line 419
    iget v0, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@7
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@9
    .line 420
    iget-boolean v0, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->hasStableIds:Z

    #@b
    iput-boolean v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->hasStableIds:Z

    #@d
    .line 421
    iget-object v0, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mUserLoadingView:Landroid/widget/RemoteViews;

    #@f
    iget-object v1, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->setLoadingViewTemplates(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    #@14
    .line 422
    monitor-exit p1

    #@15
    .line 423
    return-void

    #@16
    .line 422
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p1
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public setLoadingViewTemplates(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V
    .registers 4
    .parameter "loadingView"
    .parameter "firstView"

    #@0
    .prologue
    .line 438
    iput-object p1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mUserLoadingView:Landroid/widget/RemoteViews;

    #@2
    .line 439
    if-eqz p2, :cond_9

    #@4
    .line 440
    iput-object p2, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstView:Landroid/widget/RemoteViews;

    #@6
    .line 441
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->mFirstViewHeight:I

    #@9
    .line 443
    :cond_9
    return-void
.end method
