.class Landroid/widget/RemoteViews$SetDrawableParameters;
.super Landroid/widget/RemoteViews$Action;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetDrawableParameters"
.end annotation


# static fields
.field public static final TAG:I = 0x3


# instance fields
.field alpha:I

.field colorFilter:I

.field filterMode:Landroid/graphics/PorterDuff$Mode;

.field level:I

.field targetBackground:Z

.field final synthetic this$0:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViews;IZIILandroid/graphics/PorterDuff$Mode;I)V
    .registers 9
    .parameter
    .parameter "id"
    .parameter "targetBackground"
    .parameter "alpha"
    .parameter "colorFilter"
    .parameter "mode"
    .parameter "level"

    #@0
    .prologue
    .line 652
    iput-object p1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 653
    iput p2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    .line 654
    iput-boolean p3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->targetBackground:Z

    #@a
    .line 655
    iput p4, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->alpha:I

    #@c
    .line 656
    iput p5, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->colorFilter:I

    #@e
    .line 657
    iput-object p6, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@10
    .line 658
    iput p7, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->level:I

    #@12
    .line 659
    return-void
.end method

.method public constructor <init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V
    .registers 8
    .parameter
    .parameter "parcel"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 661
    iput-object p1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->this$0:Landroid/widget/RemoteViews;

    #@5
    invoke-direct {p0, v4}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@8
    .line 662
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v1

    #@c
    iput v1, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@e
    .line 663
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_3d

    #@14
    move v1, v2

    #@15
    :goto_15
    iput-boolean v1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->targetBackground:Z

    #@17
    .line 664
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v1

    #@1b
    iput v1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->alpha:I

    #@1d
    .line 665
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v1

    #@21
    iput v1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->colorFilter:I

    #@23
    .line 666
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_3f

    #@29
    move v0, v2

    #@2a
    .line 667
    .local v0, hasMode:Z
    :goto_2a
    if-eqz v0, :cond_41

    #@2c
    .line 668
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v1}, Landroid/graphics/PorterDuff$Mode;->valueOf(Ljava/lang/String;)Landroid/graphics/PorterDuff$Mode;

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@36
    .line 672
    :goto_36
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v1

    #@3a
    iput v1, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->level:I

    #@3c
    .line 673
    return-void

    #@3d
    .end local v0           #hasMode:Z
    :cond_3d
    move v1, v3

    #@3e
    .line 663
    goto :goto_15

    #@3f
    :cond_3f
    move v0, v3

    #@40
    .line 666
    goto :goto_2a

    #@41
    .line 670
    .restart local v0       #hasMode:Z
    :cond_41
    iput-object v4, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@43
    goto :goto_36
.end method


# virtual methods
.method public apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 10
    .parameter "root"
    .parameter "rootParent"
    .parameter "handler"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 692
    iget v3, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@3
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v1

    #@7
    .line 693
    .local v1, target:Landroid/view/View;
    if-nez v1, :cond_a

    #@9
    .line 716
    :cond_9
    :goto_9
    return-void

    #@a
    .line 696
    :cond_a
    const/4 v2, 0x0

    #@b
    .line 697
    .local v2, targetDrawable:Landroid/graphics/drawable/Drawable;
    iget-boolean v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->targetBackground:Z

    #@d
    if-eqz v3, :cond_37

    #@f
    .line 698
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    #@12
    move-result-object v2

    #@13
    .line 704
    :cond_13
    :goto_13
    if-eqz v2, :cond_9

    #@15
    .line 706
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->alpha:I

    #@17
    if-eq v3, v5, :cond_1e

    #@19
    .line 707
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->alpha:I

    #@1b
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@1e
    .line 709
    :cond_1e
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->colorFilter:I

    #@20
    if-eq v3, v5, :cond_2d

    #@22
    iget-object v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@24
    if-eqz v3, :cond_2d

    #@26
    .line 710
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->colorFilter:I

    #@28
    iget-object v4, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@2a
    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    #@2d
    .line 712
    :cond_2d
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->level:I

    #@2f
    if-eq v3, v5, :cond_9

    #@31
    .line 713
    iget v3, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->level:I

    #@33
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@36
    goto :goto_9

    #@37
    .line 699
    :cond_37
    instance-of v3, v1, Landroid/widget/ImageView;

    #@39
    if-eqz v3, :cond_13

    #@3b
    move-object v0, v1

    #@3c
    .line 700
    check-cast v0, Landroid/widget/ImageView;

    #@3e
    .line 701
    .local v0, imageView:Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@41
    move-result-object v2

    #@42
    goto :goto_13
.end method

.method public getActionName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 719
    const-string v0, "SetDrawableParameters"

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 676
    const/4 v0, 0x3

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 677
    iget v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 678
    iget-boolean v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->targetBackground:Z

    #@d
    if-eqz v0, :cond_33

    #@f
    move v0, v1

    #@10
    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 679
    iget v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->alpha:I

    #@15
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 680
    iget v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->colorFilter:I

    #@1a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 681
    iget-object v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@1f
    if-eqz v0, :cond_35

    #@21
    .line 682
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 683
    iget-object v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->filterMode:Landroid/graphics/PorterDuff$Mode;

    #@26
    invoke-virtual {v0}, Landroid/graphics/PorterDuff$Mode;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 687
    :goto_2d
    iget v0, p0, Landroid/widget/RemoteViews$SetDrawableParameters;->level:I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 688
    return-void

    #@33
    :cond_33
    move v0, v2

    #@34
    .line 678
    goto :goto_10

    #@35
    .line 685
    :cond_35
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_2d
.end method
