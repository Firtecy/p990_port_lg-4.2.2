.class Landroid/widget/PopupWindow$PopupViewContainer;
.super Landroid/widget/FrameLayout;
.source "PopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/PopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupViewContainer"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PopupWindow.PopupViewContainer"


# instance fields
.field final synthetic this$0:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/widget/PopupWindow;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 1615
    iput-object p1, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@2
    .line 1616
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@5
    .line 1617
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1633
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@4
    move-result v2

    #@5
    const/4 v3, 0x4

    #@6
    if-ne v2, v3, :cond_4c

    #@8
    .line 1634
    invoke-virtual {p0}, Landroid/widget/PopupWindow$PopupViewContainer;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@b
    move-result-object v2

    #@c
    if-nez v2, :cond_13

    #@e
    .line 1635
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@11
    move-result v1

    #@12
    .line 1654
    :cond_12
    :goto_12
    return v1

    #@13
    .line 1638
    :cond_13
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_29

    #@19
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_29

    #@1f
    .line 1640
    invoke-virtual {p0}, Landroid/widget/PopupWindow$PopupViewContainer;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@22
    move-result-object v0

    #@23
    .line 1641
    .local v0, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_12

    #@25
    .line 1642
    invoke-virtual {v0, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@28
    goto :goto_12

    #@29
    .line 1645
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_29
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@2c
    move-result v2

    #@2d
    if-ne v2, v1, :cond_47

    #@2f
    .line 1646
    invoke-virtual {p0}, Landroid/widget/PopupWindow$PopupViewContainer;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@32
    move-result-object v0

    #@33
    .line 1647
    .restart local v0       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_47

    #@35
    invoke-virtual {v0, p1}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_47

    #@3b
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    #@3e
    move-result v2

    #@3f
    if-nez v2, :cond_47

    #@41
    .line 1648
    iget-object v2, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@43
    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    #@46
    goto :goto_12

    #@47
    .line 1652
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_47
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@4a
    move-result v1

    #@4b
    goto :goto_12

    #@4c
    .line 1654
    :cond_4c
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@4f
    move-result v1

    #@50
    goto :goto_12
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 1660
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@2
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$800(Landroid/widget/PopupWindow;)Landroid/view/View$OnTouchListener;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_16

    #@8
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@a
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$800(Landroid/widget/PopupWindow;)Landroid/view/View$OnTouchListener;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    .line 1661
    const/4 v0, 0x1

    #@15
    .line 1663
    :goto_15
    return v0

    #@16
    :cond_16
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_15
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 1697
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@2
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$1000(Landroid/widget/PopupWindow;)I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_17

    #@9
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@b
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$1100(Landroid/widget/PopupWindow;)I

    #@e
    move-result v0

    #@f
    const/4 v1, 0x1

    #@10
    if-ne v0, v1, :cond_17

    #@12
    .line 1699
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@14
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@17
    .line 1701
    :cond_17
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 1621
    iget-object v1, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@2
    invoke-static {v1}, Landroid/widget/PopupWindow;->access$600(Landroid/widget/PopupWindow;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_16

    #@8
    .line 1623
    add-int/lit8 v1, p1, 0x1

    #@a
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    #@d
    move-result-object v0

    #@e
    .line 1624
    .local v0, drawableState:[I
    invoke-static {}, Landroid/widget/PopupWindow;->access$700()[I

    #@11
    move-result-object v1

    #@12
    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    #@15
    .line 1627
    .end local v0           #drawableState:[I
    :goto_15
    return-object v0

    #@16
    :cond_16
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    #@19
    move-result-object v0

    #@1a
    goto :goto_15
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1668
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@4
    move-result v3

    #@5
    float-to-int v0, v3

    #@6
    .line 1669
    .local v0, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@9
    move-result v3

    #@a
    float-to-int v1, v3

    #@b
    .line 1671
    .local v1, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_27

    #@11
    if-ltz v0, :cond_21

    #@13
    invoke-virtual {p0}, Landroid/widget/PopupWindow$PopupViewContainer;->getWidth()I

    #@16
    move-result v3

    #@17
    if-ge v0, v3, :cond_21

    #@19
    if-ltz v1, :cond_21

    #@1b
    invoke-virtual {p0}, Landroid/widget/PopupWindow$PopupViewContainer;->getHeight()I

    #@1e
    move-result v3

    #@1f
    if-lt v1, v3, :cond_27

    #@21
    .line 1673
    :cond_21
    iget-object v3, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@23
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->dismiss()V

    #@26
    .line 1679
    :goto_26
    return v2

    #@27
    .line 1675
    :cond_27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@2a
    move-result v3

    #@2b
    const/4 v4, 0x4

    #@2c
    if-ne v3, v4, :cond_34

    #@2e
    .line 1676
    iget-object v3, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@30
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->dismiss()V

    #@33
    goto :goto_26

    #@34
    .line 1679
    :cond_34
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@37
    move-result v2

    #@38
    goto :goto_26
.end method

.method public sendAccessibilityEvent(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 1686
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@2
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$900(Landroid/widget/PopupWindow;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 1687
    iget-object v0, p0, Landroid/widget/PopupWindow$PopupViewContainer;->this$0:Landroid/widget/PopupWindow;

    #@a
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$900(Landroid/widget/PopupWindow;)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@11
    .line 1691
    :goto_11
    return-void

    #@12
    .line 1689
    :cond_12
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    #@15
    goto :goto_11
.end method
