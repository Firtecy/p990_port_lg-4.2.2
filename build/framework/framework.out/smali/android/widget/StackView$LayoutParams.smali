.class Landroid/widget/StackView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "StackView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/StackView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LayoutParams"
.end annotation


# instance fields
.field private final globalInvalidateRect:Landroid/graphics/Rect;

.field horizontalOffset:I

.field private final invalidateRect:Landroid/graphics/Rect;

.field private final invalidateRectf:Landroid/graphics/RectF;

.field mView:Landroid/view/View;

.field private final parentRect:Landroid/graphics/Rect;

.field final synthetic this$0:Landroid/widget/StackView;

.field verticalOffset:I


# direct methods
.method constructor <init>(Landroid/widget/StackView;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1286
    iput-object p1, p0, Landroid/widget/StackView$LayoutParams;->this$0:Landroid/widget/StackView;

    #@3
    .line 1287
    invoke-direct {p0, p2, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@6
    .line 1272
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->parentRect:Landroid/graphics/Rect;

    #@d
    .line 1273
    new-instance v0, Landroid/graphics/Rect;

    #@f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@14
    .line 1274
    new-instance v0, Landroid/graphics/RectF;

    #@16
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@19
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@1b
    .line 1275
    new-instance v0, Landroid/graphics/Rect;

    #@1d
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@20
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@22
    .line 1288
    iput v1, p0, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@24
    .line 1289
    iput v1, p0, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@26
    .line 1290
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@28
    .line 1291
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2a
    .line 1292
    return-void
.end method

.method constructor <init>(Landroid/widget/StackView;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter "view"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1277
    iput-object p1, p0, Landroid/widget/StackView$LayoutParams;->this$0:Landroid/widget/StackView;

    #@3
    .line 1278
    invoke-direct {p0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@6
    .line 1272
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->parentRect:Landroid/graphics/Rect;

    #@d
    .line 1273
    new-instance v0, Landroid/graphics/Rect;

    #@f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@14
    .line 1274
    new-instance v0, Landroid/graphics/RectF;

    #@16
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@19
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@1b
    .line 1275
    new-instance v0, Landroid/graphics/Rect;

    #@1d
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@20
    iput-object v0, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@22
    .line 1279
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@24
    .line 1280
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@26
    .line 1281
    iput v1, p0, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@28
    .line 1282
    iput v1, p0, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@2a
    .line 1283
    iput-object p2, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@2c
    .line 1284
    return-void
.end method


# virtual methods
.method getInvalidateRect()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 1322
    iget-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 11
    .parameter "v"
    .parameter "r"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1296
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@6
    .line 1297
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@8
    iget-object v3, p0, Landroid/widget/StackView$LayoutParams;->this$0:Landroid/widget/StackView;

    #@a
    invoke-virtual {v3}, Landroid/widget/StackView;->getWidth()I

    #@d
    move-result v3

    #@e
    iget-object v4, p0, Landroid/widget/StackView$LayoutParams;->this$0:Landroid/widget/StackView;

    #@10
    invoke-virtual {v4}, Landroid/widget/StackView;->getHeight()I

    #@13
    move-result v4

    #@14
    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    #@17
    .line 1298
    move-object v1, p1

    #@18
    .line 1299
    .local v1, p:Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@1b
    move-result-object v2

    #@1c
    if-eqz v2, :cond_26

    #@1e
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@21
    move-result-object v2

    #@22
    instance-of v2, v2, Landroid/view/View;

    #@24
    if-nez v2, :cond_27

    #@26
    .line 1319
    :cond_26
    :goto_26
    return-void

    #@27
    .line 1301
    :cond_27
    const/4 v0, 0x1

    #@28
    .line 1302
    .local v0, firstPass:Z
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->parentRect:Landroid/graphics/Rect;

    #@2a
    invoke-virtual {v2, v5, v5, v5, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@2d
    .line 1304
    :goto_2d
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@30
    move-result-object v2

    #@31
    if-eqz v2, :cond_98

    #@33
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@36
    move-result-object v2

    #@37
    instance-of v2, v2, Landroid/view/View;

    #@39
    if-eqz v2, :cond_98

    #@3b
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->parentRect:Landroid/graphics/Rect;

    #@3d
    iget-object v3, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@3f
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@42
    move-result v2

    #@43
    if-nez v2, :cond_98

    #@45
    .line 1305
    if-nez v0, :cond_5e

    #@47
    .line 1306
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@49
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    #@4c
    move-result v3

    #@4d
    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    #@50
    move-result v4

    #@51
    sub-int/2addr v3, v4

    #@52
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@55
    move-result v4

    #@56
    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    #@59
    move-result v5

    #@5a
    sub-int/2addr v4, v5

    #@5b
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    #@5e
    .line 1309
    :cond_5e
    const/4 v0, 0x0

    #@5f
    .line 1310
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@62
    move-result-object v1

    #@63
    .end local v1           #p:Landroid/view/View;
    check-cast v1, Landroid/view/View;

    #@65
    .line 1311
    .restart local v1       #p:Landroid/view/View;
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->parentRect:Landroid/graphics/Rect;

    #@67
    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    #@6a
    move-result v3

    #@6b
    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    #@6e
    move-result v4

    #@6f
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    #@72
    move-result v5

    #@73
    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    #@76
    move-result v6

    #@77
    add-int/2addr v5, v6

    #@78
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    #@7b
    move-result v6

    #@7c
    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    #@7f
    move-result v7

    #@80
    add-int/2addr v6, v7

    #@81
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    #@84
    .line 1313
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@86
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@88
    iget-object v3, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@8a
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@8c
    iget-object v4, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@8e
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@90
    iget-object v5, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@92
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@94
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->invalidate(IIII)V

    #@97
    goto :goto_2d

    #@98
    .line 1317
    :cond_98
    iget-object v2, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@9a
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@9c
    iget-object v3, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@9e
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@a0
    iget-object v4, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@a2
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@a4
    iget-object v5, p0, Landroid/widget/StackView$LayoutParams;->globalInvalidateRect:Landroid/graphics/Rect;

    #@a6
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@a8
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->invalidate(IIII)V

    #@ab
    goto/16 :goto_26
.end method

.method resetInvalidateRect()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1326
    iget-object v0, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@6
    .line 1327
    return-void
.end method

.method public setHorizontalOffset(I)V
    .registers 3
    .parameter "newHorizontalOffset"

    #@0
    .prologue
    .line 1335
    iget v0, p0, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/widget/StackView$LayoutParams;->setOffsets(II)V

    #@5
    .line 1336
    return-void
.end method

.method public setOffsets(II)V
    .registers 18
    .parameter "newHorizontalOffset"
    .parameter "newVerticalOffset"

    #@0
    .prologue
    .line 1339
    iget v9, p0, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@2
    sub-int v2, p1, v9

    #@4
    .line 1340
    .local v2, horizontalOffsetDelta:I
    move/from16 v0, p1

    #@6
    iput v0, p0, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@8
    .line 1341
    iget v9, p0, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@a
    sub-int v6, p2, v9

    #@c
    .line 1342
    .local v6, verticalOffsetDelta:I
    move/from16 v0, p2

    #@e
    iput v0, p0, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@10
    .line 1344
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@12
    if-eqz v9, :cond_bb

    #@14
    .line 1345
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@16
    invoke-virtual {v9}, Landroid/view/View;->requestLayout()V

    #@19
    .line 1346
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@1b
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    #@1e
    move-result v9

    #@1f
    add-int/2addr v9, v2

    #@20
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@22
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    #@25
    move-result v10

    #@26
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@29
    move-result v3

    #@2a
    .line 1347
    .local v3, left:I
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@2c
    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    #@2f
    move-result v9

    #@30
    add-int/2addr v9, v2

    #@31
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@33
    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    #@36
    move-result v10

    #@37
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@3a
    move-result v4

    #@3b
    .line 1348
    .local v4, right:I
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@3d
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    #@40
    move-result v9

    #@41
    add-int/2addr v9, v6

    #@42
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@44
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@47
    move-result v10

    #@48
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@4b
    move-result v5

    #@4c
    .line 1349
    .local v5, top:I
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@4e
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    #@51
    move-result v9

    #@52
    add-int/2addr v9, v6

    #@53
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@55
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@58
    move-result v10

    #@59
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@5c
    move-result v1

    #@5d
    .line 1351
    .local v1, bottom:I
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@5f
    int-to-float v10, v3

    #@60
    int-to-float v11, v5

    #@61
    int-to-float v12, v4

    #@62
    int-to-float v13, v1

    #@63
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;->set(FFFF)V

    #@66
    .line 1353
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@68
    iget v9, v9, Landroid/graphics/RectF;->left:F

    #@6a
    neg-float v7, v9

    #@6b
    .line 1354
    .local v7, xoffset:F
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@6d
    iget v9, v9, Landroid/graphics/RectF;->top:F

    #@6f
    neg-float v8, v9

    #@70
    .line 1355
    .local v8, yoffset:F
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@72
    invoke-virtual {v9, v7, v8}, Landroid/graphics/RectF;->offset(FF)V

    #@75
    .line 1356
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@77
    invoke-virtual {v9}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@7a
    move-result-object v9

    #@7b
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@7d
    invoke-virtual {v9, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@80
    .line 1357
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@82
    neg-float v10, v7

    #@83
    neg-float v11, v8

    #@84
    invoke-virtual {v9, v10, v11}, Landroid/graphics/RectF;->offset(FF)V

    #@87
    .line 1359
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@89
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@8b
    iget v10, v10, Landroid/graphics/RectF;->left:F

    #@8d
    float-to-double v10, v10

    #@8e
    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    #@91
    move-result-wide v10

    #@92
    double-to-int v10, v10

    #@93
    iget-object v11, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@95
    iget v11, v11, Landroid/graphics/RectF;->top:F

    #@97
    float-to-double v11, v11

    #@98
    invoke-static {v11, v12}, Ljava/lang/Math;->floor(D)D

    #@9b
    move-result-wide v11

    #@9c
    double-to-int v11, v11

    #@9d
    iget-object v12, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@9f
    iget v12, v12, Landroid/graphics/RectF;->right:F

    #@a1
    float-to-double v12, v12

    #@a2
    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    #@a5
    move-result-wide v12

    #@a6
    double-to-int v12, v12

    #@a7
    iget-object v13, p0, Landroid/widget/StackView$LayoutParams;->invalidateRectf:Landroid/graphics/RectF;

    #@a9
    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    #@ab
    float-to-double v13, v13

    #@ac
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    #@af
    move-result-wide v13

    #@b0
    double-to-int v13, v13

    #@b1
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@b4
    .line 1364
    iget-object v9, p0, Landroid/widget/StackView$LayoutParams;->mView:Landroid/view/View;

    #@b6
    iget-object v10, p0, Landroid/widget/StackView$LayoutParams;->invalidateRect:Landroid/graphics/Rect;

    #@b8
    invoke-virtual {p0, v9, v10}, Landroid/widget/StackView$LayoutParams;->invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/Rect;)V

    #@bb
    .line 1366
    .end local v1           #bottom:I
    .end local v3           #left:I
    .end local v4           #right:I
    .end local v5           #top:I
    .end local v7           #xoffset:F
    .end local v8           #yoffset:F
    :cond_bb
    return-void
.end method

.method public setVerticalOffset(I)V
    .registers 3
    .parameter "newVerticalOffset"

    #@0
    .prologue
    .line 1331
    iget v0, p0, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/widget/StackView$LayoutParams;->setOffsets(II)V

    #@5
    .line 1332
    return-void
.end method
