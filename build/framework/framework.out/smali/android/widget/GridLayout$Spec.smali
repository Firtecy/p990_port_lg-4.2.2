.class public Landroid/widget/GridLayout$Spec;
.super Ljava/lang/Object;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/GridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spec"
.end annotation


# static fields
.field static final UNDEFINED:Landroid/widget/GridLayout$Spec;


# instance fields
.field final alignment:Landroid/widget/GridLayout$Alignment;

.field final span:Landroid/widget/GridLayout$Interval;

.field final startDefined:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2347
    const/high16 v0, -0x8000

    #@2
    invoke-static {v0}, Landroid/widget/GridLayout;->spec(I)Landroid/widget/GridLayout$Spec;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@8
    return-void
.end method

.method private constructor <init>(ZIILandroid/widget/GridLayout$Alignment;)V
    .registers 7
    .parameter "startDefined"
    .parameter "start"
    .parameter "size"
    .parameter "alignment"

    #@0
    .prologue
    .line 2360
    new-instance v0, Landroid/widget/GridLayout$Interval;

    #@2
    add-int v1, p2, p3

    #@4
    invoke-direct {v0, p2, v1}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@7
    invoke-direct {p0, p1, v0, p4}, Landroid/widget/GridLayout$Spec;-><init>(ZLandroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$Alignment;)V

    #@a
    .line 2361
    return-void
.end method

.method synthetic constructor <init>(ZIILandroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 2346
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/GridLayout$Spec;-><init>(ZIILandroid/widget/GridLayout$Alignment;)V

    #@3
    return-void
.end method

.method private constructor <init>(ZLandroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$Alignment;)V
    .registers 4
    .parameter "startDefined"
    .parameter "span"
    .parameter "alignment"

    #@0
    .prologue
    .line 2353
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2354
    iput-boolean p1, p0, Landroid/widget/GridLayout$Spec;->startDefined:Z

    #@5
    .line 2355
    iput-object p2, p0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@7
    .line 2356
    iput-object p3, p0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@9
    .line 2357
    return-void
.end method


# virtual methods
.method final copyWriteAlignment(Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;
    .registers 5
    .parameter "alignment"

    #@0
    .prologue
    .line 2368
    new-instance v0, Landroid/widget/GridLayout$Spec;

    #@2
    iget-boolean v1, p0, Landroid/widget/GridLayout$Spec;->startDefined:Z

    #@4
    iget-object v2, p0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@6
    invoke-direct {v0, v1, v2, p1}, Landroid/widget/GridLayout$Spec;-><init>(ZLandroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$Alignment;)V

    #@9
    return-object v0
.end method

.method final copyWriteSpan(Landroid/widget/GridLayout$Interval;)Landroid/widget/GridLayout$Spec;
    .registers 5
    .parameter "span"

    #@0
    .prologue
    .line 2364
    new-instance v0, Landroid/widget/GridLayout$Spec;

    #@2
    iget-boolean v1, p0, Landroid/widget/GridLayout$Spec;->startDefined:Z

    #@4
    iget-object v2, p0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@6
    invoke-direct {v0, v1, p1, v2}, Landroid/widget/GridLayout$Spec;-><init>(ZLandroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$Alignment;)V

    #@9
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "that"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2387
    if-ne p0, p1, :cond_5

    #@4
    .line 2404
    :cond_4
    :goto_4
    return v1

    #@5
    .line 2390
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    .line 2391
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 2394
    check-cast v0, Landroid/widget/GridLayout$Spec;

    #@16
    .line 2396
    .local v0, spec:Landroid/widget/GridLayout$Spec;
    iget-object v3, p0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@18
    iget-object v4, v0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_22

    #@20
    move v1, v2

    #@21
    .line 2397
    goto :goto_4

    #@22
    .line 2400
    :cond_22
    iget-object v3, p0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@24
    iget-object v4, v0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@26
    invoke-virtual {v3, v4}, Landroid/widget/GridLayout$Interval;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_4

    #@2c
    move v1, v2

    #@2d
    .line 2401
    goto :goto_4
.end method

.method final getFlexibility()I
    .registers 3

    #@0
    .prologue
    .line 2372
    iget-object v0, p0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@2
    sget-object v1, Landroid/widget/GridLayout;->UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x2

    #@9
    goto :goto_7
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 2409
    iget-object v1, p0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@2
    invoke-virtual {v1}, Landroid/widget/GridLayout$Interval;->hashCode()I

    #@5
    move-result v0

    #@6
    .line 2410
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget-object v2, p0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@a
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    #@d
    move-result v2

    #@e
    add-int v0, v1, v2

    #@10
    .line 2411
    return v0
.end method
