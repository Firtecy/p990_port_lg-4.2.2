.class public abstract Landroid/widget/CursorAdapter;
.super Landroid/widget/BaseAdapter;
.source "CursorAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/widget/CursorFilter$CursorFilterClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/CursorAdapter$1;,
        Landroid/widget/CursorAdapter$MyDataSetObserver;,
        Landroid/widget/CursorAdapter$ChangeObserver;
    }
.end annotation


# static fields
.field public static final FLAG_AUTO_REQUERY:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_REGISTER_CONTENT_OBSERVER:I = 0x2


# instance fields
.field protected mAutoRequery:Z

.field protected mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

.field protected mContext:Landroid/content/Context;

.field protected mCursor:Landroid/database/Cursor;

.field protected mCursorFilter:Landroid/widget/CursorFilter;

.field protected mDataSetObserver:Landroid/database/DataSetObserver;

.field protected mDataValid:Z

.field protected mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

.field protected mRowIDColumn:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 4
    .parameter "context"
    .parameter "c"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 115
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@3
    .line 116
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    #@7
    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
    .registers 4
    .parameter "context"
    .parameter "c"
    .parameter "flags"

    #@0
    .prologue
    .line 144
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@3
    .line 145
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    #@6
    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .registers 5
    .parameter "context"
    .parameter "c"
    .parameter "autoRequery"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@3
    .line 132
    if-eqz p3, :cond_a

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    #@9
    .line 133
    return-void

    #@a
    .line 132
    :cond_a
    const/4 v0, 0x2

    #@b
    goto :goto_6
.end method


# virtual methods
.method public abstract bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    .line 309
    invoke-virtual {p0, p1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    .line 310
    .local v0, old:Landroid/database/Cursor;
    if-eqz v0, :cond_9

    #@6
    .line 311
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@9
    .line 313
    :cond_9
    return-void
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    .line 361
    if-nez p1, :cond_5

    #@2
    const-string v0, ""

    #@4
    :goto_4
    return-object v0

    #@5
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    goto :goto_4
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 195
    iget-boolean v0, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 196
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@a
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    #@d
    move-result v0

    #@e
    .line 198
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getCursor()Landroid/database/Cursor;
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@2
    return-object v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 256
    iget-boolean v1, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@2
    if-eqz v1, :cond_1d

    #@4
    .line 257
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@9
    .line 259
    if-nez p2, :cond_1b

    #@b
    .line 260
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@d
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@f
    invoke-virtual {p0, v1, v2, p3}, Landroid/widget/CursorAdapter;->newDropDownView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    .line 264
    .local v0, v:Landroid/view/View;
    :goto_13
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@15
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@17
    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/CursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    #@1a
    .line 267
    .end local v0           #v:Landroid/view/View;
    :goto_1a
    return-object v0

    #@1b
    .line 262
    :cond_1b
    move-object v0, p2

    #@1c
    .restart local v0       #v:Landroid/view/View;
    goto :goto_13

    #@1d
    .line 267
    .end local v0           #v:Landroid/view/View;
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1a
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    #@0
    .prologue
    .line 398
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursorFilter:Landroid/widget/CursorFilter;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 399
    new-instance v0, Landroid/widget/CursorFilter;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/CursorFilter;-><init>(Landroid/widget/CursorFilter$CursorFilterClient;)V

    #@9
    iput-object v0, p0, Landroid/widget/CursorAdapter;->mCursorFilter:Landroid/widget/CursorFilter;

    #@b
    .line 401
    :cond_b
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursorFilter:Landroid/widget/CursorFilter;

    #@d
    return-object v0
.end method

.method public getFilterQueryProvider()Landroid/widget/FilterQueryProvider;
    .registers 2

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    #@2
    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 206
    iget-boolean v0, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 207
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@a
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@d
    .line 208
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@f
    .line 210
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public getItemId(I)J
    .registers 5
    .parameter "position"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 218
    iget-boolean v2, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@4
    if-eqz v2, :cond_1a

    #@6
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@8
    if-eqz v2, :cond_1a

    #@a
    .line 219
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@c
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1a

    #@12
    .line 220
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@14
    iget v1, p0, Landroid/widget/CursorAdapter;->mRowIDColumn:I

    #@16
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@19
    move-result-wide v0

    #@1a
    .line 225
    :cond_1a
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 238
    iget-boolean v1, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@2
    if-nez v1, :cond_d

    #@4
    .line 239
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v2, "this should only be called when the cursor is valid"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 241
    :cond_d
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@f
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_2e

    #@15
    .line 242
    new-instance v1, Ljava/lang/IllegalStateException;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "couldn\'t move cursor to position "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v1

    #@2e
    .line 245
    :cond_2e
    if-nez p2, :cond_40

    #@30
    .line 246
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@32
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@34
    invoke-virtual {p0, v1, v2, p3}, Landroid/widget/CursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    #@37
    move-result-object v0

    #@38
    .line 250
    .local v0, v:Landroid/view/View;
    :goto_38
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@3a
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@3c
    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/CursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    #@3f
    .line 251
    return-object v0

    #@40
    .line 248
    .end local v0           #v:Landroid/view/View;
    :cond_40
    move-object v0, p2

    #@41
    .restart local v0       #v:Landroid/view/View;
    goto :goto_38
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 231
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method init(Landroid/content/Context;Landroid/database/Cursor;I)V
    .registers 8
    .parameter "context"
    .parameter "c"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v0, 0x1

    #@3
    .line 158
    and-int/lit8 v2, p3, 0x1

    #@5
    if-ne v2, v0, :cond_45

    #@7
    .line 159
    or-int/lit8 p3, p3, 0x2

    #@9
    .line 160
    iput-boolean v0, p0, Landroid/widget/CursorAdapter;->mAutoRequery:Z

    #@b
    .line 164
    :goto_b
    if-eqz p2, :cond_48

    #@d
    .line 165
    .local v0, cursorPresent:Z
    :goto_d
    iput-object p2, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@f
    .line 166
    iput-boolean v0, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@11
    .line 167
    iput-object p1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@13
    .line 168
    if-eqz v0, :cond_4a

    #@15
    const-string v1, "_id"

    #@17
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1a
    move-result v1

    #@1b
    :goto_1b
    iput v1, p0, Landroid/widget/CursorAdapter;->mRowIDColumn:I

    #@1d
    .line 169
    and-int/lit8 v1, p3, 0x2

    #@1f
    const/4 v2, 0x2

    #@20
    if-ne v1, v2, :cond_4c

    #@22
    .line 170
    new-instance v1, Landroid/widget/CursorAdapter$ChangeObserver;

    #@24
    invoke-direct {v1, p0}, Landroid/widget/CursorAdapter$ChangeObserver;-><init>(Landroid/widget/CursorAdapter;)V

    #@27
    iput-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@29
    .line 171
    new-instance v1, Landroid/widget/CursorAdapter$MyDataSetObserver;

    #@2b
    invoke-direct {v1, p0, v3}, Landroid/widget/CursorAdapter$MyDataSetObserver;-><init>(Landroid/widget/CursorAdapter;Landroid/widget/CursorAdapter$1;)V

    #@2e
    iput-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@30
    .line 177
    :goto_30
    if-eqz v0, :cond_44

    #@32
    .line 178
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@34
    if-eqz v1, :cond_3b

    #@36
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@38
    invoke-interface {p2, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    #@3b
    .line 179
    :cond_3b
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@3d
    if-eqz v1, :cond_44

    #@3f
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@41
    invoke-interface {p2, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@44
    .line 181
    :cond_44
    return-void

    #@45
    .line 162
    .end local v0           #cursorPresent:Z
    :cond_45
    iput-boolean v1, p0, Landroid/widget/CursorAdapter;->mAutoRequery:Z

    #@47
    goto :goto_b

    #@48
    :cond_48
    move v0, v1

    #@49
    .line 164
    goto :goto_d

    #@4a
    .line 168
    .restart local v0       #cursorPresent:Z
    :cond_4a
    const/4 v1, -0x1

    #@4b
    goto :goto_1b

    #@4c
    .line 173
    :cond_4c
    iput-object v3, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@4e
    .line 174
    iput-object v3, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@50
    goto :goto_30
.end method

.method protected init(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .registers 5
    .parameter "context"
    .parameter "c"
    .parameter "autoRequery"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 154
    if-eqz p3, :cond_7

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    #@6
    .line 155
    return-void

    #@7
    .line 154
    :cond_7
    const/4 v0, 0x2

    #@8
    goto :goto_3
.end method

.method public newDropDownView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    #@0
    .prologue
    .line 290
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public abstract newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected onContentChanged()V
    .registers 2

    #@0
    .prologue
    .line 441
    iget-boolean v0, p0, Landroid/widget/CursorAdapter;->mAutoRequery:Z

    #@2
    if-eqz v0, :cond_18

    #@4
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@6
    if-eqz v0, :cond_18

    #@8
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@a
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    .line 443
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@12
    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    #@15
    move-result v0

    #@16
    iput-boolean v0, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@18
    .line 445
    :cond_18
    return-void
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .registers 3
    .parameter "constraint"

    #@0
    .prologue
    .line 390
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 391
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    #@6
    invoke-interface {v0, p1}, Landroid/widget/FilterQueryProvider;->runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    #@9
    move-result-object v0

    #@a
    .line 394
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@d
    goto :goto_a
.end method

.method public setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V
    .registers 2
    .parameter "filterQueryProvider"

    #@0
    .prologue
    .line 430
    iput-object p1, p0, Landroid/widget/CursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    #@2
    .line 431
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 4
    .parameter "newCursor"

    #@0
    .prologue
    .line 326
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@2
    if-ne p1, v1, :cond_6

    #@4
    .line 327
    const/4 v0, 0x0

    #@5
    .line 348
    :goto_5
    return-object v0

    #@6
    .line 329
    :cond_6
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@8
    .line 330
    .local v0, oldCursor:Landroid/database/Cursor;
    if-eqz v0, :cond_1c

    #@a
    .line 331
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@c
    if-eqz v1, :cond_13

    #@e
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@10
    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@13
    .line 332
    :cond_13
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@15
    if-eqz v1, :cond_1c

    #@17
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@19
    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@1c
    .line 334
    :cond_1c
    iput-object p1, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@1e
    .line 335
    if-eqz p1, :cond_41

    #@20
    .line 336
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@22
    if-eqz v1, :cond_29

    #@24
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mChangeObserver:Landroid/widget/CursorAdapter$ChangeObserver;

    #@26
    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    #@29
    .line 337
    :cond_29
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@2b
    if-eqz v1, :cond_32

    #@2d
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@2f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@32
    .line 338
    :cond_32
    const-string v1, "_id"

    #@34
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@37
    move-result v1

    #@38
    iput v1, p0, Landroid/widget/CursorAdapter;->mRowIDColumn:I

    #@3a
    .line 339
    const/4 v1, 0x1

    #@3b
    iput-boolean v1, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@3d
    .line 341
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V

    #@40
    goto :goto_5

    #@41
    .line 343
    :cond_41
    const/4 v1, -0x1

    #@42
    iput v1, p0, Landroid/widget/CursorAdapter;->mRowIDColumn:I

    #@44
    .line 344
    const/4 v1, 0x0

    #@45
    iput-boolean v1, p0, Landroid/widget/CursorAdapter;->mDataValid:Z

    #@47
    .line 346
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->notifyDataSetInvalidated()V

    #@4a
    goto :goto_5
.end method
