.class Landroid/widget/Editor$ActionPopupWindow$1;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/Editor$ActionPopupWindow;->hideCliptrayOpenCueAfterDelay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/Editor$ActionPopupWindow;


# direct methods
.method constructor <init>(Landroid/widget/Editor$ActionPopupWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3776
    iput-object p1, p0, Landroid/widget/Editor$ActionPopupWindow$1;->this$1:Landroid/widget/Editor$ActionPopupWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 3778
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow$1;->this$1:Landroid/widget/Editor$ActionPopupWindow;

    #@2
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$3002(Landroid/widget/Editor;Z)Z

    #@8
    .line 3779
    const-string v0, "Cliptray_editer"

    #@a
    const-string v1, "isHideCliptrayOpenCueAfterDelay=false"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 3780
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow$1;->this$1:Landroid/widget/Editor$ActionPopupWindow;

    #@11
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@13
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@15
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@18
    move-result v0

    #@19
    const/16 v1, 0x8

    #@1b
    if-ne v0, v1, :cond_38

    #@1d
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow$1;->this$1:Landroid/widget/Editor$ActionPopupWindow;

    #@1f
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@21
    invoke-virtual {v0}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_38

    #@27
    .line 3781
    const-string v0, "Cliptray_editer"

    #@29
    const-string/jumbo v1, "mClipManager.getVisibility() == View.GONE, hideCliptraycue()"

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 3782
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow$1;->this$1:Landroid/widget/Editor$ActionPopupWindow;

    #@31
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@33
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@35
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptraycue()V

    #@38
    .line 3784
    :cond_38
    return-void
.end method
