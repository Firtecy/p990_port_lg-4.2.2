.class Landroid/widget/ExpandableListConnector;
.super Landroid/widget/BaseAdapter;
.source "ExpandableListConnector.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ExpandableListConnector$PositionMetadata;,
        Landroid/widget/ExpandableListConnector$GroupMetadata;,
        Landroid/widget/ExpandableListConnector$MyDataSetObserver;
    }
.end annotation


# instance fields
.field private final mDataSetObserver:Landroid/database/DataSetObserver;

.field private mExpGroupMetadataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ExpandableListConnector$GroupMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field private mMaxExpGroupCount:I

.field private mTotalExpChildrenCount:I


# direct methods
.method public constructor <init>(Landroid/widget/ExpandableListAdapter;)V
    .registers 3
    .parameter "expandableListAdapter"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@3
    .line 67
    const v0, 0x7fffffff

    #@6
    iput v0, p0, Landroid/widget/ExpandableListConnector;->mMaxExpGroupCount:I

    #@8
    .line 70
    new-instance v0, Landroid/widget/ExpandableListConnector$MyDataSetObserver;

    #@a
    invoke-direct {v0, p0}, Landroid/widget/ExpandableListConnector$MyDataSetObserver;-><init>(Landroid/widget/ExpandableListConnector;)V

    #@d
    iput-object v0, p0, Landroid/widget/ExpandableListConnector;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@f
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@16
    .line 78
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->setExpandableListAdapter(Landroid/widget/ExpandableListAdapter;)V

    #@19
    .line 79
    return-void
.end method

.method static synthetic access$000(Landroid/widget/ExpandableListConnector;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListConnector;->refreshExpGroupMetadataList(ZZ)V

    #@3
    return-void
.end method

.method private refreshExpGroupMetadataList(ZZ)V
    .registers 16
    .parameter "forceChildrenCountRefresh"
    .parameter "syncGroupPositions"

    #@0
    .prologue
    const/4 v12, -0x1

    #@1
    .line 520
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@3
    .line 521
    .local v2, egml:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ExpandableListConnector$GroupMetadata;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v3

    #@7
    .line 522
    .local v3, egmlSize:I
    const/4 v0, 0x0

    #@8
    .line 525
    .local v0, curFlPos:I
    const/4 v9, 0x0

    #@9
    iput v9, p0, Landroid/widget/ExpandableListConnector;->mTotalExpChildrenCount:I

    #@b
    .line 527
    if-eqz p2, :cond_38

    #@d
    .line 529
    const/4 v8, 0x0

    #@e
    .line 531
    .local v8, positionsChanged:Z
    add-int/lit8 v5, v3, -0x1

    #@10
    .local v5, i:I
    :goto_10
    if-ltz v5, :cond_33

    #@12
    .line 532
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@18
    .line 533
    .local v1, curGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget-wide v9, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gId:J

    #@1a
    iget v11, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@1c
    invoke-virtual {p0, v9, v10, v11}, Landroid/widget/ExpandableListConnector;->findGroupPosition(JI)I

    #@1f
    move-result v7

    #@20
    .line 534
    .local v7, newGPos:I
    iget v9, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@22
    if-eq v7, v9, :cond_30

    #@24
    .line 535
    if-ne v7, v12, :cond_2b

    #@26
    .line 537
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@29
    .line 538
    add-int/lit8 v3, v3, -0x1

    #@2b
    .line 541
    :cond_2b
    iput v7, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@2d
    .line 542
    if-nez v8, :cond_30

    #@2f
    const/4 v8, 0x1

    #@30
    .line 531
    :cond_30
    add-int/lit8 v5, v5, -0x1

    #@32
    goto :goto_10

    #@33
    .line 546
    .end local v1           #curGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    .end local v7           #newGPos:I
    :cond_33
    if-eqz v8, :cond_38

    #@35
    .line 548
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@38
    .line 553
    .end local v5           #i:I
    .end local v8           #positionsChanged:Z
    :cond_38
    const/4 v6, 0x0

    #@39
    .line 554
    .local v6, lastGPos:I
    const/4 v5, 0x0

    #@3a
    .restart local v5       #i:I
    :goto_3a
    if-ge v5, v3, :cond_6a

    #@3c
    .line 556
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v1

    #@40
    check-cast v1, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@42
    .line 562
    .restart local v1       #curGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget v9, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@44
    if-eq v9, v12, :cond_48

    #@46
    if-eqz p1, :cond_63

    #@48
    .line 563
    :cond_48
    iget-object v9, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@4a
    iget v10, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@4c
    invoke-interface {v9, v10}, Landroid/widget/ExpandableListAdapter;->getChildrenCount(I)I

    #@4f
    move-result v4

    #@50
    .line 572
    .local v4, gChildrenCount:I
    :goto_50
    iget v9, p0, Landroid/widget/ExpandableListConnector;->mTotalExpChildrenCount:I

    #@52
    add-int/2addr v9, v4

    #@53
    iput v9, p0, Landroid/widget/ExpandableListConnector;->mTotalExpChildrenCount:I

    #@55
    .line 579
    iget v9, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@57
    sub-int/2addr v9, v6

    #@58
    add-int/2addr v0, v9

    #@59
    .line 580
    iget v6, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@5b
    .line 583
    iput v0, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@5d
    .line 584
    add-int/2addr v0, v4

    #@5e
    .line 585
    iput v0, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@60
    .line 554
    add-int/lit8 v5, v5, 0x1

    #@62
    goto :goto_3a

    #@63
    .line 568
    .end local v4           #gChildrenCount:I
    :cond_63
    iget v9, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@65
    iget v10, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@67
    sub-int v4, v9, v10

    #@69
    .restart local v4       #gChildrenCount:I
    goto :goto_50

    #@6a
    .line 587
    .end local v1           #curGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    .end local v4           #gChildrenCount:I
    :cond_6a
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 370
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    invoke-interface {v0}, Landroid/widget/ExpandableListAdapter;->areAllItemsEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method collapseGroup(I)Z
    .registers 7
    .parameter "groupPos"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 595
    const/4 v3, 0x2

    #@2
    invoke-static {v3, p1, v4, v4}, Landroid/widget/ExpandableListPosition;->obtain(IIII)Landroid/widget/ExpandableListPosition;

    #@5
    move-result-object v0

    #@6
    .line 597
    .local v0, elGroupPos:Landroid/widget/ExpandableListPosition;
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@9
    move-result-object v1

    #@a
    .line 598
    .local v1, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    invoke-virtual {v0}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@d
    .line 599
    if-nez v1, :cond_11

    #@f
    const/4 v2, 0x0

    #@10
    .line 603
    :goto_10
    return v2

    #@11
    .line 601
    :cond_11
    invoke-virtual {p0, v1}, Landroid/widget/ExpandableListConnector;->collapseGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z

    #@14
    move-result v2

    #@15
    .line 602
    .local v2, retValue:Z
    invoke-virtual {v1}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@18
    goto :goto_10
.end method

.method collapseGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z
    .registers 5
    .parameter "posMetadata"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 616
    iget-object v1, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 630
    :goto_5
    return v0

    #@6
    .line 619
    :cond_6
    iget-object v1, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@8
    iget-object v2, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@a
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@d
    .line 622
    invoke-direct {p0, v0, v0}, Landroid/widget/ExpandableListConnector;->refreshExpGroupMetadataList(ZZ)V

    #@10
    .line 625
    invoke-virtual {p0}, Landroid/widget/ExpandableListConnector;->notifyDataSetChanged()V

    #@13
    .line 628
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@15
    iget-object v1, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@17
    iget v1, v1, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@19
    invoke-interface {v0, v1}, Landroid/widget/ExpandableListAdapter;->onGroupCollapsed(I)V

    #@1c
    .line 630
    const/4 v0, 0x1

    #@1d
    goto :goto_5
.end method

.method expandGroup(I)Z
    .registers 7
    .parameter "groupPos"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 638
    const/4 v3, 0x2

    #@2
    invoke-static {v3, p1, v4, v4}, Landroid/widget/ExpandableListPosition;->obtain(IIII)Landroid/widget/ExpandableListPosition;

    #@5
    move-result-object v0

    #@6
    .line 640
    .local v0, elGroupPos:Landroid/widget/ExpandableListPosition;
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@9
    move-result-object v1

    #@a
    .line 641
    .local v1, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    invoke-virtual {v0}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@d
    .line 642
    invoke-virtual {p0, v1}, Landroid/widget/ExpandableListConnector;->expandGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z

    #@10
    move-result v2

    #@11
    .line 643
    .local v2, retValue:Z
    invoke-virtual {v1}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@14
    .line 644
    return v2
.end method

.method expandGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z
    .registers 10
    .parameter "posMetadata"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 652
    iget-object v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@4
    iget v4, v4, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@6
    if-gez v4, :cond_10

    #@8
    .line 654
    new-instance v3, Ljava/lang/RuntimeException;

    #@a
    const-string v4, "Need group"

    #@c
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v3

    #@10
    .line 657
    :cond_10
    iget v4, p0, Landroid/widget/ExpandableListConnector;->mMaxExpGroupCount:I

    #@12
    if-nez v4, :cond_15

    #@14
    .line 696
    :cond_14
    :goto_14
    return v3

    #@15
    .line 660
    :cond_15
    iget-object v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@17
    if-nez v4, :cond_14

    #@19
    .line 663
    iget-object v4, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v4

    #@1f
    iget v5, p0, Landroid/widget/ExpandableListConnector;->mMaxExpGroupCount:I

    #@21
    if-lt v4, v5, :cond_40

    #@23
    .line 667
    iget-object v4, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@2b
    .line 669
    .local v0, collapsedGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget-object v4, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@30
    move-result v1

    #@31
    .line 671
    .local v1, collapsedIndex:I
    iget v4, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@33
    invoke-virtual {p0, v4}, Landroid/widget/ExpandableListConnector;->collapseGroup(I)Z

    #@36
    .line 674
    iget v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupInsertIndex:I

    #@38
    if-le v4, v1, :cond_40

    #@3a
    .line 675
    iget v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupInsertIndex:I

    #@3c
    add-int/lit8 v4, v4, -0x1

    #@3e
    iput v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupInsertIndex:I

    #@40
    .line 679
    .end local v0           #collapsedGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    .end local v1           #collapsedIndex:I
    :cond_40
    iget-object v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@42
    iget v4, v4, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@44
    iget-object v5, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@46
    iget-object v6, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@48
    iget v6, v6, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@4a
    invoke-interface {v5, v6}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    #@4d
    move-result-wide v5

    #@4e
    invoke-static {v7, v7, v4, v5, v6}, Landroid/widget/ExpandableListConnector$GroupMetadata;->obtain(IIIJ)Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@51
    move-result-object v2

    #@52
    .line 685
    .local v2, expandedGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget-object v4, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@54
    iget v5, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupInsertIndex:I

    #@56
    invoke-virtual {v4, v5, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@59
    .line 688
    invoke-direct {p0, v3, v3}, Landroid/widget/ExpandableListConnector;->refreshExpGroupMetadataList(ZZ)V

    #@5c
    .line 691
    invoke-virtual {p0}, Landroid/widget/ExpandableListConnector;->notifyDataSetChanged()V

    #@5f
    .line 694
    iget-object v3, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@61
    iget v4, v2, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@63
    invoke-interface {v3, v4}, Landroid/widget/ExpandableListAdapter;->onGroupExpanded(I)V

    #@66
    .line 696
    const/4 v3, 0x1

    #@67
    goto :goto_14
.end method

.method findGroupPosition(JI)I
    .registers 20
    .parameter "groupIdToMatch"
    .parameter "seedGroupPosition"

    #@0
    .prologue
    .line 779
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@4
    invoke-interface {v12}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    #@7
    move-result v2

    #@8
    .line 781
    .local v2, count:I
    if-nez v2, :cond_c

    #@a
    .line 782
    const/4 v12, -0x1

    #@b
    .line 851
    :goto_b
    return v12

    #@c
    .line 786
    :cond_c
    const-wide/high16 v12, -0x8000

    #@e
    cmp-long v12, p1, v12

    #@10
    if-nez v12, :cond_14

    #@12
    .line 787
    const/4 v12, -0x1

    #@13
    goto :goto_b

    #@14
    .line 791
    :cond_14
    const/4 v12, 0x0

    #@15
    move/from16 v0, p3

    #@17
    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    #@1a
    move-result p3

    #@1b
    .line 792
    add-int/lit8 v12, v2, -0x1

    #@1d
    move/from16 v0, p3

    #@1f
    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    #@22
    move-result p3

    #@23
    .line 794
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26
    move-result-wide v12

    #@27
    const-wide/16 v14, 0x64

    #@29
    add-long v3, v12, v14

    #@2b
    .line 799
    .local v3, endTime:J
    move/from16 v5, p3

    #@2d
    .line 802
    .local v5, first:I
    move/from16 v8, p3

    #@2f
    .line 805
    .local v8, last:I
    const/4 v9, 0x0

    #@30
    .line 815
    .local v9, next:Z
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ExpandableListConnector;->getAdapter()Landroid/widget/ExpandableListAdapter;

    #@33
    move-result-object v1

    #@34
    .line 816
    .local v1, adapter:Landroid/widget/ExpandableListAdapter;
    if-nez v1, :cond_43

    #@36
    .line 817
    const/4 v12, -0x1

    #@37
    goto :goto_b

    #@38
    .line 835
    .local v6, hitFirst:Z
    .local v7, hitLast:Z
    .local v10, rowId:J
    :cond_38
    if-nez v6, :cond_3e

    #@3a
    if-eqz v9, :cond_6a

    #@3c
    if-nez v7, :cond_6a

    #@3e
    .line 837
    :cond_3e
    add-int/lit8 v8, v8, 0x1

    #@40
    .line 838
    move/from16 p3, v8

    #@42
    .line 840
    const/4 v9, 0x0

    #@43
    .line 820
    .end local v6           #hitFirst:Z
    .end local v7           #hitLast:Z
    .end local v10           #rowId:J
    :cond_43
    :goto_43
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@46
    move-result-wide v12

    #@47
    cmp-long v12, v12, v3

    #@49
    if-gtz v12, :cond_64

    #@4b
    .line 821
    move/from16 v0, p3

    #@4d
    invoke-interface {v1, v0}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    #@50
    move-result-wide v10

    #@51
    .line 822
    .restart local v10       #rowId:J
    cmp-long v12, v10, p1

    #@53
    if-nez v12, :cond_58

    #@55
    move/from16 v12, p3

    #@57
    .line 824
    goto :goto_b

    #@58
    .line 827
    :cond_58
    add-int/lit8 v12, v2, -0x1

    #@5a
    if-ne v8, v12, :cond_66

    #@5c
    const/4 v7, 0x1

    #@5d
    .line 828
    .restart local v7       #hitLast:Z
    :goto_5d
    if-nez v5, :cond_68

    #@5f
    const/4 v6, 0x1

    #@60
    .line 830
    .restart local v6       #hitFirst:Z
    :goto_60
    if-eqz v7, :cond_38

    #@62
    if-eqz v6, :cond_38

    #@64
    .line 851
    .end local v6           #hitFirst:Z
    .end local v7           #hitLast:Z
    .end local v10           #rowId:J
    :cond_64
    const/4 v12, -0x1

    #@65
    goto :goto_b

    #@66
    .line 827
    .restart local v10       #rowId:J
    :cond_66
    const/4 v7, 0x0

    #@67
    goto :goto_5d

    #@68
    .line 828
    .restart local v7       #hitLast:Z
    :cond_68
    const/4 v6, 0x0

    #@69
    goto :goto_60

    #@6a
    .line 841
    .restart local v6       #hitFirst:Z
    :cond_6a
    if-nez v7, :cond_70

    #@6c
    if-nez v9, :cond_43

    #@6e
    if-nez v6, :cond_43

    #@70
    .line 843
    :cond_70
    add-int/lit8 v5, v5, -0x1

    #@72
    .line 844
    move/from16 p3, v5

    #@74
    .line 846
    const/4 v9, 0x1

    #@75
    goto :goto_43
.end method

.method getAdapter()Landroid/widget/ExpandableListAdapter;
    .registers 2

    #@0
    .prologue
    .line 725
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    return-object v0
.end method

.method public getCount()I
    .registers 3

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    invoke-interface {v0}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/widget/ExpandableListConnector;->mTotalExpChildrenCount:I

    #@8
    add-int/2addr v0, v1

    #@9
    return v0
.end method

.method getExpandedGroupMetadataList()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ExpandableListConnector$GroupMetadata;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 3

    #@0
    .prologue
    .line 729
    invoke-virtual {p0}, Landroid/widget/ExpandableListConnector;->getAdapter()Landroid/widget/ExpandableListAdapter;

    #@3
    move-result-object v0

    #@4
    .line 730
    .local v0, adapter:Landroid/widget/ExpandableListAdapter;
    instance-of v1, v0, Landroid/widget/Filterable;

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 731
    check-cast v0, Landroid/widget/Filterable;

    #@a
    .end local v0           #adapter:Landroid/widget/ExpandableListAdapter;
    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@d
    move-result-object v1

    #@e
    .line 733
    :goto_e
    return-object v1

    #@f
    .restart local v0       #adapter:Landroid/widget/ExpandableListAdapter;
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;
    .registers 27
    .parameter "pos"

    #@0
    .prologue
    .line 259
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@4
    move-object/from16 v20, v0

    #@6
    .line 260
    .local v20, egml:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ExpandableListConnector$GroupMetadata;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v23

    #@a
    .line 263
    .local v23, numExpGroups:I
    const/4 v13, 0x0

    #@b
    .line 264
    .local v13, leftExpGroupIndex:I
    add-int/lit8 v19, v23, -0x1

    #@d
    .line 265
    .local v19, rightExpGroupIndex:I
    const/16 v22, 0x0

    #@f
    .line 268
    .local v22, midExpGroupIndex:I
    if-nez v23, :cond_109

    #@11
    .line 274
    move-object/from16 v0, p1

    #@13
    iget v2, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@15
    move-object/from16 v0, p1

    #@17
    iget v3, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@19
    move-object/from16 v0, p1

    #@1b
    iget v4, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@1d
    move-object/from16 v0, p1

    #@1f
    iget v5, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@21
    const/4 v6, 0x0

    #@22
    const/4 v7, 0x0

    #@23
    invoke-static/range {v2 .. v7}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@26
    move-result-object v2

    #@27
    move/from16 v7, v22

    #@29
    .line 364
    .end local v22           #midExpGroupIndex:I
    .local v7, midExpGroupIndex:I
    :goto_29
    return-object v2

    #@2a
    .line 283
    :cond_2a
    :goto_2a
    move/from16 v0, v19

    #@2c
    if-gt v13, v0, :cond_97

    #@2e
    .line 284
    sub-int v2, v19, v13

    #@30
    div-int/lit8 v2, v2, 0x2

    #@32
    add-int v7, v2, v13

    #@34
    .line 285
    move-object/from16 v0, v20

    #@36
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v6

    #@3a
    check-cast v6, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@3c
    .line 287
    .local v6, midExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    move-object/from16 v0, p1

    #@3e
    iget v2, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@40
    iget v3, v6, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@42
    if-le v2, v3, :cond_47

    #@44
    .line 291
    add-int/lit8 v13, v7, 0x1

    #@46
    goto :goto_2a

    #@47
    .line 292
    :cond_47
    move-object/from16 v0, p1

    #@49
    iget v2, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@4b
    iget v3, v6, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@4d
    if-ge v2, v3, :cond_52

    #@4f
    .line 296
    add-int/lit8 v19, v7, -0x1

    #@51
    goto :goto_2a

    #@52
    .line 297
    :cond_52
    move-object/from16 v0, p1

    #@54
    iget v2, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@56
    iget v3, v6, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@58
    if-ne v2, v3, :cond_2a

    #@5a
    .line 302
    move-object/from16 v0, p1

    #@5c
    iget v2, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@5e
    const/4 v3, 0x2

    #@5f
    if-ne v2, v3, :cond_74

    #@61
    .line 304
    iget v2, v6, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@63
    move-object/from16 v0, p1

    #@65
    iget v3, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@67
    move-object/from16 v0, p1

    #@69
    iget v4, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@6b
    move-object/from16 v0, p1

    #@6d
    iget v5, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@6f
    invoke-static/range {v2 .. v7}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@72
    move-result-object v2

    #@73
    goto :goto_29

    #@74
    .line 306
    :cond_74
    move-object/from16 v0, p1

    #@76
    iget v2, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@78
    const/4 v3, 0x1

    #@79
    if-ne v2, v3, :cond_95

    #@7b
    .line 308
    iget v2, v6, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@7d
    move-object/from16 v0, p1

    #@7f
    iget v3, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@81
    add-int/2addr v2, v3

    #@82
    add-int/lit8 v2, v2, 0x1

    #@84
    move-object/from16 v0, p1

    #@86
    iget v3, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@88
    move-object/from16 v0, p1

    #@8a
    iget v4, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@8c
    move-object/from16 v0, p1

    #@8e
    iget v5, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@90
    invoke-static/range {v2 .. v7}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@93
    move-result-object v2

    #@94
    goto :goto_29

    #@95
    .line 312
    :cond_95
    const/4 v2, 0x0

    #@96
    goto :goto_29

    #@97
    .line 321
    .end local v6           #midExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_97
    move-object/from16 v0, p1

    #@99
    iget v2, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@9b
    const/4 v3, 0x2

    #@9c
    if-eq v2, v3, :cond_a0

    #@9e
    .line 323
    const/4 v2, 0x0

    #@9f
    goto :goto_29

    #@a0
    .line 331
    :cond_a0
    if-le v13, v7, :cond_ce

    #@a2
    .line 342
    add-int/lit8 v2, v13, -0x1

    #@a4
    move-object/from16 v0, v20

    #@a6
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a9
    move-result-object v21

    #@aa
    check-cast v21, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@ac
    .line 343
    .local v21, leftExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    move-object/from16 v0, v21

    #@ae
    iget v2, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@b0
    move-object/from16 v0, p1

    #@b2
    iget v3, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@b4
    move-object/from16 v0, v21

    #@b6
    iget v4, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@b8
    sub-int/2addr v3, v4

    #@b9
    add-int v8, v2, v3

    #@bb
    .line 347
    .local v8, flPos:I
    move-object/from16 v0, p1

    #@bd
    iget v9, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@bf
    move-object/from16 v0, p1

    #@c1
    iget v10, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@c3
    move-object/from16 v0, p1

    #@c5
    iget v11, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@c7
    const/4 v12, 0x0

    #@c8
    invoke-static/range {v8 .. v13}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@cb
    move-result-object v2

    #@cc
    goto/16 :goto_29

    #@ce
    .line 349
    .end local v8           #flPos:I
    .end local v21           #leftExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_ce
    move/from16 v0, v19

    #@d0
    if-ge v0, v7, :cond_106

    #@d2
    .line 357
    add-int/lit8 v19, v19, 0x1

    #@d4
    move-object/from16 v0, v20

    #@d6
    move/from16 v1, v19

    #@d8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v24

    #@dc
    check-cast v24, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@de
    .line 358
    .local v24, rightExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    move-object/from16 v0, v24

    #@e0
    iget v2, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@e2
    move-object/from16 v0, v24

    #@e4
    iget v3, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@e6
    move-object/from16 v0, p1

    #@e8
    iget v4, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@ea
    sub-int/2addr v3, v4

    #@eb
    sub-int v8, v2, v3

    #@ed
    .line 361
    .restart local v8       #flPos:I
    move-object/from16 v0, p1

    #@ef
    iget v15, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@f1
    move-object/from16 v0, p1

    #@f3
    iget v0, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@f5
    move/from16 v16, v0

    #@f7
    move-object/from16 v0, p1

    #@f9
    iget v0, v0, Landroid/widget/ExpandableListPosition;->childPos:I

    #@fb
    move/from16 v17, v0

    #@fd
    const/16 v18, 0x0

    #@ff
    move v14, v8

    #@100
    invoke-static/range {v14 .. v19}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@103
    move-result-object v2

    #@104
    goto/16 :goto_29

    #@106
    .line 364
    .end local v8           #flPos:I
    .end local v24           #rightExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_106
    const/4 v2, 0x0

    #@107
    goto/16 :goto_29

    #@109
    .end local v7           #midExpGroupIndex:I
    .restart local v22       #midExpGroupIndex:I
    :cond_109
    move/from16 v7, v22

    #@10b
    .end local v22           #midExpGroupIndex:I
    .restart local v7       #midExpGroupIndex:I
    goto/16 :goto_2a
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 7
    .parameter "flatListPos"

    #@0
    .prologue
    .line 401
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@3
    move-result-object v0

    #@4
    .line 404
    .local v0, posMetadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v2, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@6
    iget v2, v2, Landroid/widget/ExpandableListPosition;->type:I

    #@8
    const/4 v3, 0x2

    #@9
    if-ne v2, v3, :cond_19

    #@b
    .line 405
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@d
    iget-object v3, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@f
    iget v3, v3, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@11
    invoke-interface {v2, v3}, Landroid/widget/ExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    .line 415
    .local v1, retValue:Ljava/lang/Object;
    :goto_15
    invoke-virtual {v0}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@18
    .line 417
    return-object v1

    #@19
    .line 407
    .end local v1           #retValue:Ljava/lang/Object;
    :cond_19
    iget-object v2, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@1b
    iget v2, v2, Landroid/widget/ExpandableListPosition;->type:I

    #@1d
    const/4 v3, 0x1

    #@1e
    if-ne v2, v3, :cond_2f

    #@20
    .line 408
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@22
    iget-object v3, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@24
    iget v3, v3, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@26
    iget-object v4, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@28
    iget v4, v4, Landroid/widget/ExpandableListPosition;->childPos:I

    #@2a
    invoke-interface {v2, v3, v4}, Landroid/widget/ExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    .restart local v1       #retValue:Ljava/lang/Object;
    goto :goto_15

    #@2f
    .line 412
    .end local v1           #retValue:Ljava/lang/Object;
    :cond_2f
    new-instance v2, Ljava/lang/RuntimeException;

    #@31
    const-string v3, "Flat list position is of unknown type"

    #@33
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@36
    throw v2
.end method

.method public getItemId(I)J
    .registers 12
    .parameter "flatListPos"

    #@0
    .prologue
    .line 421
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@3
    move-result-object v4

    #@4
    .line 422
    .local v4, posMetadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v7, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@6
    iget-object v8, v4, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@8
    iget v8, v8, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@a
    invoke-interface {v7, v8}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    #@d
    move-result-wide v2

    #@e
    .line 425
    .local v2, groupId:J
    iget-object v7, v4, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@10
    iget v7, v7, Landroid/widget/ExpandableListPosition;->type:I

    #@12
    const/4 v8, 0x2

    #@13
    if-ne v7, v8, :cond_1f

    #@15
    .line 426
    iget-object v7, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@17
    invoke-interface {v7, v2, v3}, Landroid/widget/ExpandableListAdapter;->getCombinedGroupId(J)J

    #@1a
    move-result-wide v5

    #@1b
    .line 436
    .local v5, retValue:J
    :goto_1b
    invoke-virtual {v4}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@1e
    .line 438
    return-wide v5

    #@1f
    .line 427
    .end local v5           #retValue:J
    :cond_1f
    iget-object v7, v4, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@21
    iget v7, v7, Landroid/widget/ExpandableListPosition;->type:I

    #@23
    const/4 v8, 0x1

    #@24
    if-ne v7, v8, :cond_3b

    #@26
    .line 428
    iget-object v7, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@28
    iget-object v8, v4, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@2a
    iget v8, v8, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@2c
    iget-object v9, v4, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@2e
    iget v9, v9, Landroid/widget/ExpandableListPosition;->childPos:I

    #@30
    invoke-interface {v7, v8, v9}, Landroid/widget/ExpandableListAdapter;->getChildId(II)J

    #@33
    move-result-wide v0

    #@34
    .line 430
    .local v0, childId:J
    iget-object v7, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@36
    invoke-interface {v7, v2, v3, v0, v1}, Landroid/widget/ExpandableListAdapter;->getCombinedChildId(JJ)J

    #@39
    move-result-wide v5

    #@3a
    .line 431
    .restart local v5       #retValue:J
    goto :goto_1b

    #@3b
    .line 433
    .end local v0           #childId:J
    .end local v5           #retValue:J
    :cond_3b
    new-instance v7, Ljava/lang/RuntimeException;

    #@3d
    const-string v8, "Flat list position is of unknown type"

    #@3f
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42
    throw v7
.end method

.method public getItemViewType(I)I
    .registers 9
    .parameter "flatListPos"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 465
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@4
    move-result-object v2

    #@5
    .line 466
    .local v2, metadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v3, v2, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@7
    .line 469
    .local v3, pos:Landroid/widget/ExpandableListPosition;
    iget-object v5, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@9
    instance-of v5, v5, Landroid/widget/HeterogeneousExpandableList;

    #@b
    if-eqz v5, :cond_2e

    #@d
    .line 470
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@f
    check-cast v0, Landroid/widget/HeterogeneousExpandableList;

    #@11
    .line 472
    .local v0, adapter:Landroid/widget/HeterogeneousExpandableList;
    iget v5, v3, Landroid/widget/ExpandableListPosition;->type:I

    #@13
    if-ne v5, v6, :cond_1f

    #@15
    .line 473
    iget v5, v3, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@17
    invoke-interface {v0, v5}, Landroid/widget/HeterogeneousExpandableList;->getGroupType(I)I

    #@1a
    move-result v4

    #@1b
    .line 486
    .end local v0           #adapter:Landroid/widget/HeterogeneousExpandableList;
    .local v4, retValue:I
    :goto_1b
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@1e
    .line 488
    return v4

    #@1f
    .line 475
    .end local v4           #retValue:I
    .restart local v0       #adapter:Landroid/widget/HeterogeneousExpandableList;
    :cond_1f
    iget v5, v3, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@21
    iget v6, v3, Landroid/widget/ExpandableListPosition;->childPos:I

    #@23
    invoke-interface {v0, v5, v6}, Landroid/widget/HeterogeneousExpandableList;->getChildType(II)I

    #@26
    move-result v1

    #@27
    .line 476
    .local v1, childType:I
    invoke-interface {v0}, Landroid/widget/HeterogeneousExpandableList;->getGroupTypeCount()I

    #@2a
    move-result v5

    #@2b
    add-int v4, v5, v1

    #@2d
    .restart local v4       #retValue:I
    goto :goto_1b

    #@2e
    .line 479
    .end local v0           #adapter:Landroid/widget/HeterogeneousExpandableList;
    .end local v1           #childType:I
    .end local v4           #retValue:I
    :cond_2e
    iget v5, v3, Landroid/widget/ExpandableListPosition;->type:I

    #@30
    if-ne v5, v6, :cond_34

    #@32
    .line 480
    const/4 v4, 0x0

    #@33
    .restart local v4       #retValue:I
    goto :goto_1b

    #@34
    .line 482
    .end local v4           #retValue:I
    :cond_34
    const/4 v4, 0x1

    #@35
    .restart local v4       #retValue:I
    goto :goto_1b
.end method

.method getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;
    .registers 22
    .parameter "flPos"

    #@0
    .prologue
    .line 109
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@4
    .line 110
    .local v13, egml:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ExpandableListConnector$GroupMetadata;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v17

    #@8
    .line 113
    .local v17, numExpGroups:I
    const/4 v15, 0x0

    #@9
    .line 114
    .local v15, leftExpGroupIndex:I
    add-int/lit8 v19, v17, -0x1

    #@b
    .line 115
    .local v19, rightExpGroupIndex:I
    const/16 v16, 0x0

    #@d
    .line 118
    .local v16, midExpGroupIndex:I
    if-nez v17, :cond_ab

    #@f
    .line 124
    const/4 v2, 0x2

    #@10
    const/4 v4, -0x1

    #@11
    const/4 v5, 0x0

    #@12
    const/4 v6, 0x0

    #@13
    move/from16 v1, p1

    #@15
    move/from16 v3, p1

    #@17
    invoke-static/range {v1 .. v6}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@1a
    move-result-object v1

    #@1b
    move/from16 v6, v16

    #@1d
    .line 243
    .end local v16           #midExpGroupIndex:I
    .local v6, midExpGroupIndex:I
    :goto_1d
    return-object v1

    #@1e
    .line 140
    :cond_1e
    :goto_1e
    move/from16 v0, v19

    #@20
    if-gt v15, v0, :cond_67

    #@22
    .line 141
    sub-int v1, v19, v15

    #@24
    div-int/lit8 v1, v1, 0x2

    #@26
    add-int v6, v1, v15

    #@28
    .line 144
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v5

    #@2c
    check-cast v5, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@2e
    .line 146
    .local v5, midExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget v1, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@30
    move/from16 v0, p1

    #@32
    if-le v0, v1, :cond_37

    #@34
    .line 151
    add-int/lit8 v15, v6, 0x1

    #@36
    goto :goto_1e

    #@37
    .line 152
    :cond_37
    iget v1, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@39
    move/from16 v0, p1

    #@3b
    if-ge v0, v1, :cond_40

    #@3d
    .line 157
    add-int/lit8 v19, v6, -0x1

    #@3f
    goto :goto_1e

    #@40
    .line 158
    :cond_40
    iget v1, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@42
    move/from16 v0, p1

    #@44
    if-ne v0, v1, :cond_51

    #@46
    .line 163
    const/4 v2, 0x2

    #@47
    iget v3, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@49
    const/4 v4, -0x1

    #@4a
    move/from16 v1, p1

    #@4c
    invoke-static/range {v1 .. v6}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@4f
    move-result-object v1

    #@50
    goto :goto_1d

    #@51
    .line 165
    :cond_51
    iget v1, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@53
    move/from16 v0, p1

    #@55
    if-gt v0, v1, :cond_1e

    #@57
    .line 175
    iget v1, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@59
    add-int/lit8 v1, v1, 0x1

    #@5b
    sub-int v4, p1, v1

    #@5d
    .line 176
    .local v4, childPos:I
    const/4 v2, 0x1

    #@5e
    iget v3, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@60
    move/from16 v1, p1

    #@62
    invoke-static/range {v1 .. v6}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@65
    move-result-object v1

    #@66
    goto :goto_1d

    #@67
    .line 192
    .end local v4           #childPos:I
    .end local v5           #midExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_67
    const/4 v12, 0x0

    #@68
    .line 195
    .local v12, insertPosition:I
    const/4 v9, 0x0

    #@69
    .line 202
    .local v9, groupPos:I
    if-le v15, v6, :cond_86

    #@6b
    .line 210
    add-int/lit8 v1, v15, -0x1

    #@6d
    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@70
    move-result-object v14

    #@71
    check-cast v14, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@73
    .line 212
    .local v14, leftExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    move v12, v15

    #@74
    .line 218
    iget v1, v14, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@76
    sub-int v1, p1, v1

    #@78
    iget v2, v14, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@7a
    add-int v9, v1, v2

    #@7c
    .line 243
    .end local v14           #leftExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :goto_7c
    const/4 v8, 0x2

    #@7d
    const/4 v10, -0x1

    #@7e
    const/4 v11, 0x0

    #@7f
    move/from16 v7, p1

    #@81
    invoke-static/range {v7 .. v12}, Landroid/widget/ExpandableListConnector$PositionMetadata;->obtain(IIIILandroid/widget/ExpandableListConnector$GroupMetadata;I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@84
    move-result-object v1

    #@85
    goto :goto_1d

    #@86
    .line 220
    :cond_86
    move/from16 v0, v19

    #@88
    if-ge v0, v6, :cond_a3

    #@8a
    .line 227
    add-int/lit8 v19, v19, 0x1

    #@8c
    move/from16 v0, v19

    #@8e
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@91
    move-result-object v18

    #@92
    check-cast v18, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@94
    .line 229
    .local v18, rightExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    move/from16 v12, v19

    #@96
    .line 237
    move-object/from16 v0, v18

    #@98
    iget v1, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@9a
    move-object/from16 v0, v18

    #@9c
    iget v2, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@9e
    sub-int v2, v2, p1

    #@a0
    sub-int v9, v1, v2

    #@a2
    .line 238
    goto :goto_7c

    #@a3
    .line 240
    .end local v18           #rightExpGm:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_a3
    new-instance v1, Ljava/lang/RuntimeException;

    #@a5
    const-string v2, "Unknown state"

    #@a7
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@aa
    throw v1

    #@ab
    .end local v6           #midExpGroupIndex:I
    .end local v9           #groupPos:I
    .end local v12           #insertPosition:I
    .restart local v16       #midExpGroupIndex:I
    :cond_ab
    move/from16 v6, v16

    #@ad
    .end local v16           #midExpGroupIndex:I
    .restart local v6       #midExpGroupIndex:I
    goto/16 :goto_1e
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .parameter "flatListPos"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 442
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@4
    move-result-object v6

    #@5
    .line 445
    .local v6, posMetadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v0, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@7
    iget v0, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@9
    const/4 v1, 0x2

    #@a
    if-ne v0, v1, :cond_1e

    #@c
    .line 446
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@e
    iget-object v1, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@10
    iget v1, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@12
    invoke-virtual {v6}, Landroid/widget/ExpandableListConnector$PositionMetadata;->isExpanded()Z

    #@15
    move-result v2

    #@16
    invoke-interface {v0, v1, v2, p2, p3}, Landroid/widget/ExpandableListAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@19
    move-result-object v7

    #@1a
    .line 458
    .local v7, retValue:Landroid/view/View;
    :goto_1a
    invoke-virtual {v6}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@1d
    .line 460
    return-object v7

    #@1e
    .line 448
    .end local v7           #retValue:Landroid/view/View;
    :cond_1e
    iget-object v0, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@20
    iget v0, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@22
    if-ne v0, v3, :cond_3d

    #@24
    .line 449
    iget-object v0, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@26
    iget v0, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@28
    if-ne v0, p1, :cond_3b

    #@2a
    .line 451
    .local v3, isLastChild:Z
    :goto_2a
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2c
    iget-object v1, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@2e
    iget v1, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@30
    iget-object v2, v6, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@32
    iget v2, v2, Landroid/widget/ExpandableListPosition;->childPos:I

    #@34
    move-object v4, p2

    #@35
    move-object v5, p3

    #@36
    invoke-interface/range {v0 .. v5}, Landroid/widget/ExpandableListAdapter;->getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@39
    move-result-object v7

    #@3a
    .line 453
    .restart local v7       #retValue:Landroid/view/View;
    goto :goto_1a

    #@3b
    .line 449
    .end local v3           #isLastChild:Z
    .end local v7           #retValue:Landroid/view/View;
    :cond_3b
    const/4 v3, 0x0

    #@3c
    goto :goto_2a

    #@3d
    .line 455
    :cond_3d
    new-instance v0, Ljava/lang/RuntimeException;

    #@3f
    const-string v1, "Flat list position is of unknown type"

    #@41
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@44
    throw v0
.end method

.method public getViewTypeCount()I
    .registers 4

    #@0
    .prologue
    .line 493
    iget-object v1, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    instance-of v1, v1, Landroid/widget/HeterogeneousExpandableList;

    #@4
    if-eqz v1, :cond_14

    #@6
    .line 494
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@8
    check-cast v0, Landroid/widget/HeterogeneousExpandableList;

    #@a
    .line 496
    .local v0, adapter:Landroid/widget/HeterogeneousExpandableList;
    invoke-interface {v0}, Landroid/widget/HeterogeneousExpandableList;->getGroupTypeCount()I

    #@d
    move-result v1

    #@e
    invoke-interface {v0}, Landroid/widget/HeterogeneousExpandableList;->getChildTypeCount()I

    #@11
    move-result v2

    #@12
    add-int/2addr v1, v2

    #@13
    .line 498
    .end local v0           #adapter:Landroid/widget/HeterogeneousExpandableList;
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x2

    #@15
    goto :goto_13
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 504
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    invoke-interface {v0}, Landroid/widget/ExpandableListAdapter;->hasStableIds()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmpty()Z
    .registers 3

    #@0
    .prologue
    .line 763
    invoke-virtual {p0}, Landroid/widget/ExpandableListConnector;->getAdapter()Landroid/widget/ExpandableListAdapter;

    #@3
    move-result-object v0

    #@4
    .line 764
    .local v0, adapter:Landroid/widget/ExpandableListAdapter;
    if-eqz v0, :cond_b

    #@6
    invoke-interface {v0}, Landroid/widget/ExpandableListAdapter;->isEmpty()Z

    #@9
    move-result v1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x1

    #@c
    goto :goto_a
.end method

.method public isEnabled(I)Z
    .registers 8
    .parameter "flatListPos"

    #@0
    .prologue
    .line 375
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@3
    move-result-object v0

    #@4
    .line 376
    .local v0, metadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v1, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@6
    .line 379
    .local v1, pos:Landroid/widget/ExpandableListPosition;
    iget v3, v1, Landroid/widget/ExpandableListPosition;->type:I

    #@8
    const/4 v4, 0x1

    #@9
    if-ne v3, v4, :cond_19

    #@b
    .line 380
    iget-object v3, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@d
    iget v4, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@f
    iget v5, v1, Landroid/widget/ExpandableListPosition;->childPos:I

    #@11
    invoke-interface {v3, v4, v5}, Landroid/widget/ExpandableListAdapter;->isChildSelectable(II)Z

    #@14
    move-result v2

    #@15
    .line 386
    .local v2, retValue:Z
    :goto_15
    invoke-virtual {v0}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@18
    .line 388
    return v2

    #@19
    .line 383
    .end local v2           #retValue:Z
    :cond_19
    const/4 v2, 0x1

    #@1a
    .restart local v2       #retValue:Z
    goto :goto_15
.end method

.method public isGroupExpanded(I)Z
    .registers 5
    .parameter "groupPosition"

    #@0
    .prologue
    .line 706
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_1b

    #@a
    .line 707
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@12
    .line 709
    .local v0, groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;
    iget v2, v0, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@14
    if-ne v2, p1, :cond_18

    #@16
    .line 710
    const/4 v2, 0x1

    #@17
    .line 714
    .end local v0           #groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :goto_17
    return v2

    #@18
    .line 706
    .restart local v0       #groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_18
    add-int/lit8 v1, v1, -0x1

    #@1a
    goto :goto_8

    #@1b
    .line 714
    .end local v0           #groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;
    :cond_1b
    const/4 v2, 0x0

    #@1c
    goto :goto_17
.end method

.method public setExpandableListAdapter(Landroid/widget/ExpandableListAdapter;)V
    .registers 4
    .parameter "expandableListAdapter"

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 88
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@6
    iget-object v1, p0, Landroid/widget/ExpandableListConnector;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@8
    invoke-interface {v0, v1}, Landroid/widget/ExpandableListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@b
    .line 91
    :cond_b
    iput-object p1, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@d
    .line 92
    iget-object v0, p0, Landroid/widget/ExpandableListConnector;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@f
    invoke-interface {p1, v0}, Landroid/widget/ExpandableListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@12
    .line 93
    return-void
.end method

.method setExpandedGroupMetadataList(Ljava/util/ArrayList;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ExpandableListConnector$GroupMetadata;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 743
    .local p1, expandedGroupMetadataList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ExpandableListConnector$GroupMetadata;>;"
    if-eqz p1, :cond_6

    #@2
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@4
    if-nez v2, :cond_7

    #@6
    .line 759
    :cond_6
    :goto_6
    return-void

    #@7
    .line 749
    :cond_7
    iget-object v2, p0, Landroid/widget/ExpandableListConnector;->mExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    #@9
    invoke-interface {v2}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    #@c
    move-result v1

    #@d
    .line 750
    .local v1, numGroups:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v2

    #@11
    add-int/lit8 v0, v2, -0x1

    #@13
    .local v0, i:I
    :goto_13
    if-ltz v0, :cond_22

    #@15
    .line 751
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@1b
    iget v2, v2, Landroid/widget/ExpandableListConnector$GroupMetadata;->gPos:I

    #@1d
    if-ge v2, v1, :cond_6

    #@1f
    .line 750
    add-int/lit8 v0, v0, -0x1

    #@21
    goto :goto_13

    #@22
    .line 757
    :cond_22
    iput-object p1, p0, Landroid/widget/ExpandableListConnector;->mExpGroupMetadataList:Ljava/util/ArrayList;

    #@24
    .line 758
    const/4 v2, 0x1

    #@25
    const/4 v3, 0x0

    #@26
    invoke-direct {p0, v2, v3}, Landroid/widget/ExpandableListConnector;->refreshExpGroupMetadataList(ZZ)V

    #@29
    goto :goto_6
.end method

.method public setMaxExpGroupCount(I)V
    .registers 2
    .parameter "maxExpGroupCount"

    #@0
    .prologue
    .line 721
    iput p1, p0, Landroid/widget/ExpandableListConnector;->mMaxExpGroupCount:I

    #@2
    .line 722
    return-void
.end method
