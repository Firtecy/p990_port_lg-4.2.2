.class Landroid/widget/AbsListView$FlingRunnable;
.super Ljava/lang/Object;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlingRunnable"
.end annotation


# static fields
.field private static final FLYWHEEL_TIMEOUT:I = 0x28


# instance fields
.field private isFirstFling:Z

.field private final mCheckFlywheel:Ljava/lang/Runnable;

.field private mLastFlingY:I

.field private final mScroller:Landroid/widget/OverScroller;

.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 4149
    iput-object p1, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4123
    new-instance v0, Landroid/widget/AbsListView$FlingRunnable$1;

    #@7
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$FlingRunnable$1;-><init>(Landroid/widget/AbsListView$FlingRunnable;)V

    #@a
    iput-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mCheckFlywheel:Ljava/lang/Runnable;

    #@c
    .line 4150
    new-instance v0, Landroid/widget/OverScroller;

    #@e
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@11
    move-result-object v1

    #@12
    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    #@15
    iput-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@17
    .line 4152
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@19
    if-eqz v0, :cond_1e

    #@1b
    .line 4153
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Landroid/widget/AbsListView$FlingRunnable;->isFirstFling:Z

    #@1e
    .line 4155
    :cond_1e
    return-void
.end method

.method static synthetic access$700(Landroid/widget/AbsListView$FlingRunnable;)Landroid/widget/OverScroller;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 4110
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@2
    return-object v0
.end method


# virtual methods
.method edgeReached(I)V
    .registers 8
    .parameter "delta"

    #@0
    .prologue
    .line 4207
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@2
    iget-object v3, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@4
    invoke-static {v3}, Landroid/widget/AbsListView;->access$1700(Landroid/widget/AbsListView;)I

    #@7
    move-result v3

    #@8
    const/4 v4, 0x0

    #@9
    iget-object v5, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@b
    iget v5, v5, Landroid/widget/AbsListView;->mOverflingDistance:I

    #@d
    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/OverScroller;->notifyVerticalEdgeReached(III)V

    #@10
    .line 4208
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@12
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getOverScrollMode()I

    #@15
    move-result v0

    #@16
    .line 4209
    .local v0, overscrollMode:I
    if-eqz v0, :cond_23

    #@18
    const/4 v2, 0x1

    #@19
    if-ne v0, v2, :cond_4f

    #@1b
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1d
    invoke-static {v2}, Landroid/widget/AbsListView;->access$1800(Landroid/widget/AbsListView;)Z

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_4f

    #@23
    .line 4211
    :cond_23
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@25
    const/4 v3, 0x6

    #@26
    iput v3, v2, Landroid/widget/AbsListView;->mTouchMode:I

    #@28
    .line 4212
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@2a
    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@2d
    move-result v2

    #@2e
    float-to-int v1, v2

    #@2f
    .line 4213
    .local v1, vel:I
    if-lez p1, :cond_45

    #@31
    .line 4214
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@33
    invoke-static {v2}, Landroid/widget/AbsListView;->access$1900(Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@3a
    .line 4224
    .end local v1           #vel:I
    :cond_3a
    :goto_3a
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@3c
    invoke-virtual {v2}, Landroid/widget/AbsListView;->invalidate()V

    #@3f
    .line 4225
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@41
    invoke-virtual {v2, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@44
    .line 4226
    return-void

    #@45
    .line 4216
    .restart local v1       #vel:I
    :cond_45
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@47
    invoke-static {v2}, Landroid/widget/AbsListView;->access$2000(Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@4e
    goto :goto_3a

    #@4f
    .line 4219
    .end local v1           #vel:I
    :cond_4f
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@51
    const/4 v3, -0x1

    #@52
    iput v3, v2, Landroid/widget/AbsListView;->mTouchMode:I

    #@54
    .line 4220
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@56
    iget-object v2, v2, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@58
    if-eqz v2, :cond_3a

    #@5a
    .line 4221
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@5c
    iget-object v2, v2, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@5e
    invoke-virtual {v2}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@61
    goto :goto_3a
.end method

.method endFling()V
    .registers 3

    #@0
    .prologue
    .line 4238
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2
    const/4 v1, -0x1

    #@3
    iput v1, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@5
    .line 4240
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 4241
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/widget/AbsListView$FlingRunnable;->isFirstFling:Z

    #@c
    .line 4244
    :cond_c
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@e
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@11
    .line 4245
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@13
    iget-object v1, p0, Landroid/widget/AbsListView$FlingRunnable;->mCheckFlywheel:Ljava/lang/Runnable;

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@18
    .line 4247
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@1e
    .line 4248
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@20
    invoke-static {v0}, Landroid/widget/AbsListView;->access$2100(Landroid/widget/AbsListView;)V

    #@23
    .line 4249
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@25
    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    #@28
    .line 4251
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2a
    invoke-static {v0}, Landroid/widget/AbsListView;->access$1400(Landroid/widget/AbsListView;)Landroid/os/StrictMode$Span;

    #@2d
    move-result-object v0

    #@2e
    if-eqz v0, :cond_3f

    #@30
    .line 4252
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@32
    invoke-static {v0}, Landroid/widget/AbsListView;->access$1400(Landroid/widget/AbsListView;)Landroid/os/StrictMode$Span;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Landroid/os/StrictMode$Span;->finish()V

    #@39
    .line 4253
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@3b
    const/4 v1, 0x0

    #@3c
    invoke-static {v0, v1}, Landroid/widget/AbsListView;->access$1402(Landroid/widget/AbsListView;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@3f
    .line 4255
    :cond_3f
    return-void
.end method

.method flywheelTouch()V
    .registers 5

    #@0
    .prologue
    .line 4258
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2
    iget-object v1, p0, Landroid/widget/AbsListView$FlingRunnable;->mCheckFlywheel:Ljava/lang/Runnable;

    #@4
    const-wide/16 v2, 0x28

    #@6
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@9
    .line 4259
    return-void
.end method

.method public run()V
    .registers 31

    #@0
    .prologue
    .line 4262
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@4
    iget v2, v2, Landroid/widget/AbsListView;->mTouchMode:I

    #@6
    packed-switch v2, :pswitch_data_1f4

    #@9
    .line 4264
    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@c
    .line 4389
    :cond_c
    :goto_c
    return-void

    #@d
    .line 4268
    :pswitch_d
    move-object/from16 v0, p0

    #@f
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@11
    invoke-virtual {v2}, Landroid/widget/OverScroller;->isFinished()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_21

    #@17
    .line 4269
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@19
    if-eqz v2, :cond_c

    #@1b
    .line 4270
    const/4 v2, 0x1

    #@1c
    move-object/from16 v0, p0

    #@1e
    iput-boolean v2, v0, Landroid/widget/AbsListView$FlingRunnable;->isFirstFling:Z

    #@20
    goto :goto_c

    #@21
    .line 4276
    :cond_21
    :pswitch_21
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@25
    iget-boolean v2, v2, Landroid/widget/AdapterView;->mDataChanged:Z

    #@27
    if-eqz v2, :cond_30

    #@29
    .line 4277
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2d
    invoke-virtual {v2}, Landroid/widget/AbsListView;->layoutChildren()V

    #@30
    .line 4280
    :cond_30
    move-object/from16 v0, p0

    #@32
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@34
    iget v2, v2, Landroid/widget/AdapterView;->mItemCount:I

    #@36
    if-eqz v2, :cond_42

    #@38
    move-object/from16 v0, p0

    #@3a
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@3c
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChildCount()I

    #@3f
    move-result v2

    #@40
    if-nez v2, :cond_46

    #@42
    .line 4281
    :cond_42
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@45
    goto :goto_c

    #@46
    .line 4285
    :cond_46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@4a
    move-object/from16 v27, v0

    #@4c
    .line 4286
    .local v27, scroller:Landroid/widget/OverScroller;
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    #@4f
    move-result v23

    #@50
    .line 4287
    .local v23, more:Z
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->getCurrY()I

    #@53
    move-result v29

    #@54
    .line 4291
    .local v29, y:I
    move-object/from16 v0, p0

    #@56
    iget v2, v0, Landroid/widget/AbsListView$FlingRunnable;->mLastFlingY:I

    #@58
    sub-int v20, v2, v29

    #@5a
    .line 4294
    .local v20, delta:I
    if-lez v20, :cond_ff

    #@5c
    .line 4296
    move-object/from16 v0, p0

    #@5e
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@60
    move-object/from16 v0, p0

    #@62
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@64
    iget v3, v3, Landroid/widget/AdapterView;->mFirstPosition:I

    #@66
    iput v3, v2, Landroid/widget/AbsListView;->mMotionPosition:I

    #@68
    .line 4297
    move-object/from16 v0, p0

    #@6a
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@6c
    const/4 v3, 0x0

    #@6d
    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@70
    move-result-object v21

    #@71
    .line 4298
    .local v21, firstView:Landroid/view/View;
    move-object/from16 v0, p0

    #@73
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@75
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTop()I

    #@78
    move-result v3

    #@79
    iput v3, v2, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@7b
    .line 4301
    move-object/from16 v0, p0

    #@7d
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@7f
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getHeight()I

    #@82
    move-result v2

    #@83
    move-object/from16 v0, p0

    #@85
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@87
    invoke-static {v3}, Landroid/widget/AbsListView;->access$2200(Landroid/widget/AbsListView;)I

    #@8a
    move-result v3

    #@8b
    sub-int/2addr v2, v3

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@90
    invoke-static {v3}, Landroid/widget/AbsListView;->access$2300(Landroid/widget/AbsListView;)I

    #@93
    move-result v3

    #@94
    sub-int/2addr v2, v3

    #@95
    add-int/lit8 v2, v2, -0x1

    #@97
    move/from16 v0, v20

    #@99
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    #@9c
    move-result v20

    #@9d
    .line 4315
    .end local v21           #firstView:Landroid/view/View;
    :goto_9d
    move-object/from16 v0, p0

    #@9f
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@a5
    iget v3, v3, Landroid/widget/AbsListView;->mMotionPosition:I

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget-object v5, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@ab
    iget v5, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@ad
    sub-int/2addr v3, v5

    #@ae
    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@b1
    move-result-object v24

    #@b2
    .line 4316
    .local v24, motionView:Landroid/view/View;
    const/16 v26, 0x0

    #@b4
    .line 4317
    .local v26, oldTop:I
    if-eqz v24, :cond_ba

    #@b6
    .line 4318
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getTop()I

    #@b9
    move-result v26

    #@ba
    .line 4322
    :cond_ba
    move-object/from16 v0, p0

    #@bc
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@be
    move/from16 v0, v20

    #@c0
    move/from16 v1, v20

    #@c2
    invoke-virtual {v2, v0, v1}, Landroid/widget/AbsListView;->trackMotionScroll(II)Z

    #@c5
    move-result v15

    #@c6
    .line 4323
    .local v15, atEdge:Z
    if-eqz v15, :cond_150

    #@c8
    if-eqz v20, :cond_150

    #@ca
    const/16 v16, 0x1

    #@cc
    .line 4324
    .local v16, atEnd:Z
    :goto_cc
    if-eqz v16, :cond_154

    #@ce
    .line 4325
    if-eqz v24, :cond_f4

    #@d0
    .line 4327
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getTop()I

    #@d3
    move-result v2

    #@d4
    sub-int v2, v2, v26

    #@d6
    sub-int v2, v20, v2

    #@d8
    neg-int v4, v2

    #@d9
    .line 4328
    .local v4, overshoot:I
    move-object/from16 v0, p0

    #@db
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@dd
    const/4 v3, 0x0

    #@de
    const/4 v5, 0x0

    #@df
    move-object/from16 v0, p0

    #@e1
    iget-object v6, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@e3
    invoke-static {v6}, Landroid/widget/AbsListView;->access$2600(Landroid/widget/AbsListView;)I

    #@e6
    move-result v6

    #@e7
    const/4 v7, 0x0

    #@e8
    const/4 v8, 0x0

    #@e9
    const/4 v9, 0x0

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v10, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@ee
    iget v10, v10, Landroid/widget/AbsListView;->mOverflingDistance:I

    #@f0
    const/4 v11, 0x0

    #@f1
    invoke-static/range {v2 .. v11}, Landroid/widget/AbsListView;->access$2700(Landroid/widget/AbsListView;IIIIIIIIZ)Z

    #@f4
    .line 4331
    .end local v4           #overshoot:I
    :cond_f4
    if-eqz v23, :cond_c

    #@f6
    .line 4332
    move-object/from16 v0, p0

    #@f8
    move/from16 v1, v20

    #@fa
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView$FlingRunnable;->edgeReached(I)V

    #@fd
    goto/16 :goto_c

    #@ff
    .line 4304
    .end local v15           #atEdge:Z
    .end local v16           #atEnd:Z
    .end local v24           #motionView:Landroid/view/View;
    .end local v26           #oldTop:I
    :cond_ff
    move-object/from16 v0, p0

    #@101
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@103
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChildCount()I

    #@106
    move-result v2

    #@107
    add-int/lit8 v25, v2, -0x1

    #@109
    .line 4305
    .local v25, offsetToLast:I
    move-object/from16 v0, p0

    #@10b
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@10d
    move-object/from16 v0, p0

    #@10f
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@111
    iget v3, v3, Landroid/widget/AdapterView;->mFirstPosition:I

    #@113
    add-int v3, v3, v25

    #@115
    iput v3, v2, Landroid/widget/AbsListView;->mMotionPosition:I

    #@117
    .line 4307
    move-object/from16 v0, p0

    #@119
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@11b
    move/from16 v0, v25

    #@11d
    invoke-virtual {v2, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@120
    move-result-object v22

    #@121
    .line 4308
    .local v22, lastView:Landroid/view/View;
    move-object/from16 v0, p0

    #@123
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@125
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getTop()I

    #@128
    move-result v3

    #@129
    iput v3, v2, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@12b
    .line 4311
    move-object/from16 v0, p0

    #@12d
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@12f
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getHeight()I

    #@132
    move-result v2

    #@133
    move-object/from16 v0, p0

    #@135
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@137
    invoke-static {v3}, Landroid/widget/AbsListView;->access$2400(Landroid/widget/AbsListView;)I

    #@13a
    move-result v3

    #@13b
    sub-int/2addr v2, v3

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget-object v3, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@140
    invoke-static {v3}, Landroid/widget/AbsListView;->access$2500(Landroid/widget/AbsListView;)I

    #@143
    move-result v3

    #@144
    sub-int/2addr v2, v3

    #@145
    add-int/lit8 v2, v2, -0x1

    #@147
    neg-int v2, v2

    #@148
    move/from16 v0, v20

    #@14a
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    #@14d
    move-result v20

    #@14e
    goto/16 :goto_9d

    #@150
    .line 4323
    .end local v22           #lastView:Landroid/view/View;
    .end local v25           #offsetToLast:I
    .restart local v15       #atEdge:Z
    .restart local v24       #motionView:Landroid/view/View;
    .restart local v26       #oldTop:I
    :cond_150
    const/16 v16, 0x0

    #@152
    goto/16 :goto_cc

    #@154
    .line 4337
    .restart local v16       #atEnd:Z
    :cond_154
    if-eqz v23, :cond_172

    #@156
    if-nez v16, :cond_172

    #@158
    .line 4338
    if-eqz v15, :cond_161

    #@15a
    move-object/from16 v0, p0

    #@15c
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@15e
    invoke-virtual {v2}, Landroid/widget/AbsListView;->invalidate()V

    #@161
    .line 4339
    :cond_161
    move/from16 v0, v29

    #@163
    move-object/from16 v1, p0

    #@165
    iput v0, v1, Landroid/widget/AbsListView$FlingRunnable;->mLastFlingY:I

    #@167
    .line 4340
    move-object/from16 v0, p0

    #@169
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@16b
    move-object/from16 v0, p0

    #@16d
    invoke-virtual {v2, v0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@170
    goto/16 :goto_c

    #@172
    .line 4342
    :cond_172
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@175
    goto/16 :goto_c

    #@177
    .line 4360
    .end local v15           #atEdge:Z
    .end local v16           #atEnd:Z
    .end local v20           #delta:I
    .end local v23           #more:Z
    .end local v24           #motionView:Landroid/view/View;
    .end local v26           #oldTop:I
    .end local v27           #scroller:Landroid/widget/OverScroller;
    .end local v29           #y:I
    :pswitch_177
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@17b
    move-object/from16 v27, v0

    #@17d
    .line 4361
    .restart local v27       #scroller:Landroid/widget/OverScroller;
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    #@180
    move-result v2

    #@181
    if-eqz v2, :cond_1ee

    #@183
    .line 4362
    move-object/from16 v0, p0

    #@185
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@187
    invoke-static {v2}, Landroid/widget/AbsListView;->access$2800(Landroid/widget/AbsListView;)I

    #@18a
    move-result v9

    #@18b
    .line 4363
    .local v9, scrollY:I
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->getCurrY()I

    #@18e
    move-result v19

    #@18f
    .line 4364
    .local v19, currY:I
    sub-int v7, v19, v9

    #@191
    .line 4365
    .local v7, deltaY:I
    move-object/from16 v0, p0

    #@193
    iget-object v5, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@195
    const/4 v6, 0x0

    #@196
    const/4 v8, 0x0

    #@197
    const/4 v10, 0x0

    #@198
    const/4 v11, 0x0

    #@199
    const/4 v12, 0x0

    #@19a
    move-object/from16 v0, p0

    #@19c
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@19e
    iget v13, v2, Landroid/widget/AbsListView;->mOverflingDistance:I

    #@1a0
    const/4 v14, 0x0

    #@1a1
    invoke-static/range {v5 .. v14}, Landroid/widget/AbsListView;->access$2900(Landroid/widget/AbsListView;IIIIIIIIZ)Z

    #@1a4
    move-result v2

    #@1a5
    if-eqz v2, :cond_1dc

    #@1a7
    .line 4367
    if-gtz v9, :cond_1d1

    #@1a9
    if-lez v19, :cond_1d1

    #@1ab
    const/16 v17, 0x1

    #@1ad
    .line 4368
    .local v17, crossDown:Z
    :goto_1ad
    if-ltz v9, :cond_1d4

    #@1af
    if-gez v19, :cond_1d4

    #@1b1
    const/16 v18, 0x1

    #@1b3
    .line 4369
    .local v18, crossUp:Z
    :goto_1b3
    if-nez v17, :cond_1b7

    #@1b5
    if-eqz v18, :cond_1d7

    #@1b7
    .line 4370
    :cond_1b7
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@1ba
    move-result v2

    #@1bb
    float-to-int v0, v2

    #@1bc
    move/from16 v28, v0

    #@1be
    .line 4371
    .local v28, velocity:I
    if-eqz v18, :cond_1c5

    #@1c0
    move/from16 v0, v28

    #@1c2
    neg-int v0, v0

    #@1c3
    move/from16 v28, v0

    #@1c5
    .line 4374
    :cond_1c5
    invoke-virtual/range {v27 .. v27}, Landroid/widget/OverScroller;->abortAnimation()V

    #@1c8
    .line 4375
    move-object/from16 v0, p0

    #@1ca
    move/from16 v1, v28

    #@1cc
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView$FlingRunnable;->start(I)V

    #@1cf
    goto/16 :goto_c

    #@1d1
    .line 4367
    .end local v17           #crossDown:Z
    .end local v18           #crossUp:Z
    .end local v28           #velocity:I
    :cond_1d1
    const/16 v17, 0x0

    #@1d3
    goto :goto_1ad

    #@1d4
    .line 4368
    .restart local v17       #crossDown:Z
    :cond_1d4
    const/16 v18, 0x0

    #@1d6
    goto :goto_1b3

    #@1d7
    .line 4377
    .restart local v18       #crossUp:Z
    :cond_1d7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView$FlingRunnable;->startSpringback()V

    #@1da
    goto/16 :goto_c

    #@1dc
    .line 4380
    .end local v17           #crossDown:Z
    .end local v18           #crossUp:Z
    :cond_1dc
    move-object/from16 v0, p0

    #@1de
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1e0
    invoke-virtual {v2}, Landroid/widget/AbsListView;->invalidate()V

    #@1e3
    .line 4381
    move-object/from16 v0, p0

    #@1e5
    iget-object v2, v0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    invoke-virtual {v2, v0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@1ec
    goto/16 :goto_c

    #@1ee
    .line 4384
    .end local v7           #deltaY:I
    .end local v9           #scrollY:I
    .end local v19           #currY:I
    :cond_1ee
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@1f1
    goto/16 :goto_c

    #@1f3
    .line 4262
    nop

    #@1f4
    :pswitch_data_1f4
    .packed-switch 0x3
        :pswitch_d
        :pswitch_21
        :pswitch_9
        :pswitch_177
    .end packed-switch
.end method

.method start(I)V
    .registers 11
    .parameter "initialVelocity"

    #@0
    .prologue
    const v6, 0x7fffffff

    #@3
    const/4 v1, 0x0

    #@4
    .line 4158
    if-gez p1, :cond_41

    #@6
    move v2, v6

    #@7
    .line 4159
    .local v2, initialY:I
    :goto_7
    iput v2, p0, Landroid/widget/AbsListView$FlingRunnable;->mLastFlingY:I

    #@9
    .line 4160
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@b
    const/4 v3, 0x0

    #@c
    invoke-virtual {v0, v3}, Landroid/widget/OverScroller;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@f
    .line 4161
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@11
    move v3, v1

    #@12
    move v4, p1

    #@13
    move v5, v1

    #@14
    move v7, v1

    #@15
    move v8, v6

    #@16
    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    #@19
    .line 4163
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1b
    const/4 v3, 0x4

    #@1c
    iput v3, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@1e
    .line 4165
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@20
    if-eqz v0, :cond_43

    #@22
    .line 4166
    iget-boolean v0, p0, Landroid/widget/AbsListView$FlingRunnable;->isFirstFling:Z

    #@24
    if-eqz v0, :cond_2d

    #@26
    .line 4167
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@28
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@2b
    .line 4168
    iput-boolean v1, p0, Landroid/widget/AbsListView$FlingRunnable;->isFirstFling:Z

    #@2d
    .line 4181
    :cond_2d
    :goto_2d
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2f
    invoke-static {v0}, Landroid/widget/AbsListView;->access$1400(Landroid/widget/AbsListView;)Landroid/os/StrictMode$Span;

    #@32
    move-result-object v0

    #@33
    if-nez v0, :cond_40

    #@35
    .line 4182
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@37
    const-string v1, "AbsListView-fling"

    #@39
    invoke-static {v1}, Landroid/os/StrictMode;->enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/widget/AbsListView;->access$1402(Landroid/widget/AbsListView;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@40
    .line 4184
    :cond_40
    return-void

    #@41
    .end local v2           #initialY:I
    :cond_41
    move v2, v1

    #@42
    .line 4158
    goto :goto_7

    #@43
    .line 4171
    .restart local v2       #initialY:I
    :cond_43
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@45
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@48
    goto :goto_2d
.end method

.method startOverfling(I)V
    .registers 13
    .parameter "initialVelocity"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4198
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {v0, v2}, Landroid/widget/OverScroller;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@7
    .line 4199
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@9
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@b
    invoke-static {v2}, Landroid/widget/AbsListView;->access$1600(Landroid/widget/AbsListView;)I

    #@e
    move-result v2

    #@f
    const/high16 v7, -0x8000

    #@11
    const v8, 0x7fffffff

    #@14
    iget-object v3, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@16
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getHeight()I

    #@19
    move-result v10

    #@1a
    move v3, v1

    #@1b
    move v4, p1

    #@1c
    move v5, v1

    #@1d
    move v6, v1

    #@1e
    move v9, v1

    #@1f
    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@22
    .line 4201
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@24
    const/4 v1, 0x6

    #@25
    iput v1, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@27
    .line 4202
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@29
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    #@2c
    .line 4203
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2e
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@31
    .line 4204
    return-void
.end method

.method startScroll(IIZ)V
    .registers 10
    .parameter "distance"
    .parameter "duration"
    .parameter "linear"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4229
    if-gez p1, :cond_24

    #@3
    const v2, 0x7fffffff

    #@6
    .line 4230
    .local v2, initialY:I
    :goto_6
    iput v2, p0, Landroid/widget/AbsListView$FlingRunnable;->mLastFlingY:I

    #@8
    .line 4231
    iget-object v3, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@a
    if-eqz p3, :cond_26

    #@c
    sget-object v0, Landroid/widget/AbsListView;->sLinearInterpolator:Landroid/view/animation/Interpolator;

    #@e
    :goto_e
    invoke-virtual {v3, v0}, Landroid/widget/OverScroller;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@11
    .line 4232
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@13
    move v3, v1

    #@14
    move v4, p1

    #@15
    move v5, p2

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    #@19
    .line 4233
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1b
    const/4 v1, 0x4

    #@1c
    iput v1, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@1e
    .line 4234
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@20
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@23
    .line 4235
    return-void

    #@24
    .end local v2           #initialY:I
    :cond_24
    move v2, v1

    #@25
    .line 4229
    goto :goto_6

    #@26
    .line 4231
    .restart local v2       #initialY:I
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_e
.end method

.method startSpringback()V
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4187
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->mScroller:Landroid/widget/OverScroller;

    #@3
    iget-object v2, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@5
    invoke-static {v2}, Landroid/widget/AbsListView;->access$1500(Landroid/widget/AbsListView;)I

    #@8
    move-result v2

    #@9
    move v3, v1

    #@a
    move v4, v1

    #@b
    move v5, v1

    #@c
    move v6, v1

    #@d
    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_23

    #@13
    .line 4188
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@15
    const/4 v1, 0x6

    #@16
    iput v1, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@18
    .line 4189
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1a
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    #@1d
    .line 4190
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@1f
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@22
    .line 4195
    :goto_22
    return-void

    #@23
    .line 4192
    :cond_23
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@25
    const/4 v2, -0x1

    #@26
    iput v2, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@28
    .line 4193
    iget-object v0, p0, Landroid/widget/AbsListView$FlingRunnable;->this$0:Landroid/widget/AbsListView;

    #@2a
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@2d
    goto :goto_22
.end method
