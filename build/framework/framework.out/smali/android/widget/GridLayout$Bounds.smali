.class Landroid/widget/GridLayout$Bounds;
.super Ljava/lang/Object;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/GridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Bounds"
.end annotation


# instance fields
.field public after:I

.field public before:I

.field public flexibility:I


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2187
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2188
    invoke-virtual {p0}, Landroid/widget/GridLayout$Bounds;->reset()V

    #@6
    .line 2189
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/GridLayout$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2182
    invoke-direct {p0}, Landroid/widget/GridLayout$Bounds;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method protected getOffset(Landroid/widget/GridLayout;Landroid/view/View;Landroid/widget/GridLayout$Alignment;IZ)I
    .registers 8
    .parameter "gl"
    .parameter "c"
    .parameter "a"
    .parameter "size"
    .parameter "horizontal"

    #@0
    .prologue
    .line 2212
    iget v0, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@2
    invoke-virtual {p1}, Landroid/widget/GridLayout;->getLayoutMode()I

    #@5
    move-result v1

    #@6
    invoke-virtual {p3, p2, p4, v1}, Landroid/widget/GridLayout$Alignment;->getAlignmentValue(Landroid/view/View;II)I

    #@9
    move-result v1

    #@a
    sub-int/2addr v0, v1

    #@b
    return v0
.end method

.method protected include(II)V
    .registers 4
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 2198
    iget v0, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@2
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@8
    .line 2199
    iget v0, p0, Landroid/widget/GridLayout$Bounds;->after:I

    #@a
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/widget/GridLayout$Bounds;->after:I

    #@10
    .line 2200
    return-void
.end method

.method protected final include(Landroid/widget/GridLayout;Landroid/view/View;Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Axis;)V
    .registers 11
    .parameter "gl"
    .parameter "c"
    .parameter "spec"
    .parameter "axis"

    #@0
    .prologue
    .line 2216
    iget v4, p0, Landroid/widget/GridLayout$Bounds;->flexibility:I

    #@2
    invoke-virtual {p3}, Landroid/widget/GridLayout$Spec;->getFlexibility()I

    #@5
    move-result v5

    #@6
    and-int/2addr v4, v5

    #@7
    iput v4, p0, Landroid/widget/GridLayout$Bounds;->flexibility:I

    #@9
    .line 2217
    iget-boolean v2, p4, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@b
    .line 2218
    .local v2, horizontal:Z
    invoke-virtual {p1, p2, v2}, Landroid/widget/GridLayout;->getMeasurementIncludingMargin(Landroid/view/View;Z)I

    #@e
    move-result v3

    #@f
    .line 2219
    .local v3, size:I
    iget-object v4, p3, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@11
    invoke-virtual {p1, v4, v2}, Landroid/widget/GridLayout;->getAlignment(Landroid/widget/GridLayout$Alignment;Z)Landroid/widget/GridLayout$Alignment;

    #@14
    move-result-object v0

    #@15
    .line 2221
    .local v0, alignment:Landroid/widget/GridLayout$Alignment;
    invoke-virtual {p1}, Landroid/widget/GridLayout;->getLayoutMode()I

    #@18
    move-result v4

    #@19
    invoke-virtual {v0, p2, v3, v4}, Landroid/widget/GridLayout$Alignment;->getAlignmentValue(Landroid/view/View;II)I

    #@1c
    move-result v1

    #@1d
    .line 2222
    .local v1, before:I
    sub-int v4, v3, v1

    #@1f
    invoke-virtual {p0, v1, v4}, Landroid/widget/GridLayout$Bounds;->include(II)V

    #@22
    .line 2223
    return-void
.end method

.method protected reset()V
    .registers 2

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    .line 2192
    iput v0, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@4
    .line 2193
    iput v0, p0, Landroid/widget/GridLayout$Bounds;->after:I

    #@6
    .line 2194
    const/4 v0, 0x2

    #@7
    iput v0, p0, Landroid/widget/GridLayout$Bounds;->flexibility:I

    #@9
    .line 2195
    return-void
.end method

.method protected size(Z)I
    .registers 4
    .parameter "min"

    #@0
    .prologue
    .line 2203
    if-nez p1, :cond_e

    #@2
    .line 2204
    iget v0, p0, Landroid/widget/GridLayout$Bounds;->flexibility:I

    #@4
    invoke-static {v0}, Landroid/widget/GridLayout;->canStretch(I)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_e

    #@a
    .line 2205
    const v0, 0x186a0

    #@d
    .line 2208
    :goto_d
    return v0

    #@e
    :cond_e
    iget v0, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@10
    iget v1, p0, Landroid/widget/GridLayout$Bounds;->after:I

    #@12
    add-int/2addr v0, v1

    #@13
    goto :goto_d
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2227
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Bounds{before="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/widget/GridLayout$Bounds;->before:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", after="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/widget/GridLayout$Bounds;->after:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const/16 v1, 0x7d

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
