.class public Landroid/widget/ScrollBarDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "ScrollBarDrawable.java"


# instance fields
.field private mAlwaysDrawHorizontalTrack:Z

.field private mAlwaysDrawVerticalTrack:Z

.field private mChanged:Z

.field private mExtent:I

.field private mHorizontalThumb:Landroid/graphics/drawable/Drawable;

.field private mHorizontalTrack:Landroid/graphics/drawable/Drawable;

.field private mOffset:I

.field private mRange:I

.field private mRangeChanged:Z

.field private final mTempBounds:Landroid/graphics/Rect;

.field private mVertical:Z

.field private mVerticalThumb:Landroid/graphics/drawable/Drawable;

.field private mVerticalTrack:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 42
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/ScrollBarDrawable;->mTempBounds:Landroid/graphics/Rect;

    #@a
    .line 47
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 20
    .parameter "canvas"

    #@0
    .prologue
    .line 102
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Landroid/widget/ScrollBarDrawable;->mVertical:Z

    #@4
    move/from16 v17, v0

    #@6
    .line 103
    .local v17, vertical:Z
    move-object/from16 v0, p0

    #@8
    iget v11, v0, Landroid/widget/ScrollBarDrawable;->mExtent:I

    #@a
    .line 104
    .local v11, extent:I
    move-object/from16 v0, p0

    #@c
    iget v14, v0, Landroid/widget/ScrollBarDrawable;->mRange:I

    #@e
    .line 106
    .local v14, range:I
    const/4 v10, 0x1

    #@f
    .line 107
    .local v10, drawTrack:Z
    const/4 v9, 0x1

    #@10
    .line 108
    .local v9, drawThumb:Z
    if-lez v11, :cond_14

    #@12
    if-gt v14, v11, :cond_1b

    #@14
    .line 109
    :cond_14
    if-eqz v17, :cond_36

    #@16
    move-object/from16 v0, p0

    #@18
    iget-boolean v10, v0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawVerticalTrack:Z

    #@1a
    .line 110
    :goto_1a
    const/4 v9, 0x0

    #@1b
    .line 113
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollBarDrawable;->getBounds()Landroid/graphics/Rect;

    #@1e
    move-result-object v13

    #@1f
    .line 114
    .local v13, r:Landroid/graphics/Rect;
    iget v3, v13, Landroid/graphics/Rect;->left:I

    #@21
    int-to-float v4, v3

    #@22
    iget v3, v13, Landroid/graphics/Rect;->top:I

    #@24
    int-to-float v5, v3

    #@25
    iget v3, v13, Landroid/graphics/Rect;->right:I

    #@27
    int-to-float v6, v3

    #@28
    iget v3, v13, Landroid/graphics/Rect;->bottom:I

    #@2a
    int-to-float v7, v3

    #@2b
    sget-object v8, Landroid/graphics/Canvas$EdgeType;->AA:Landroid/graphics/Canvas$EdgeType;

    #@2d
    move-object/from16 v3, p1

    #@2f
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_3b

    #@35
    .line 151
    :cond_35
    :goto_35
    return-void

    #@36
    .line 109
    .end local v13           #r:Landroid/graphics/Rect;
    :cond_36
    move-object/from16 v0, p0

    #@38
    iget-boolean v10, v0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawHorizontalTrack:Z

    #@3a
    goto :goto_1a

    #@3b
    .line 117
    .restart local v13       #r:Landroid/graphics/Rect;
    :cond_3b
    if-eqz v10, :cond_46

    #@3d
    .line 118
    move-object/from16 v0, p0

    #@3f
    move-object/from16 v1, p1

    #@41
    move/from16 v2, v17

    #@43
    invoke-virtual {v0, v1, v13, v2}, Landroid/widget/ScrollBarDrawable;->drawTrack(Landroid/graphics/Canvas;Landroid/graphics/Rect;Z)V

    #@46
    .line 121
    :cond_46
    if-eqz v9, :cond_35

    #@48
    .line 122
    if-eqz v17, :cond_84

    #@4a
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    #@4d
    move-result v15

    #@4e
    .line 123
    .local v15, size:I
    :goto_4e
    if-eqz v17, :cond_89

    #@50
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    #@53
    move-result v16

    #@54
    .line 124
    .local v16, thickness:I
    :goto_54
    int-to-float v3, v15

    #@55
    int-to-float v4, v11

    #@56
    mul-float/2addr v3, v4

    #@57
    int-to-float v4, v14

    #@58
    div-float/2addr v3, v4

    #@59
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@5c
    move-result v7

    #@5d
    .line 128
    .local v7, length:I
    mul-int/lit8 v12, v16, 0x2

    #@5f
    .line 129
    .local v12, minLength:I
    if-ge v7, v12, :cond_62

    #@61
    .line 130
    move v7, v12

    #@62
    .line 133
    :cond_62
    sub-int v3, v15, v7

    #@64
    int-to-float v3, v3

    #@65
    move-object/from16 v0, p0

    #@67
    iget v4, v0, Landroid/widget/ScrollBarDrawable;->mOffset:I

    #@69
    int-to-float v4, v4

    #@6a
    mul-float/2addr v3, v4

    #@6b
    sub-int v4, v14, v11

    #@6d
    int-to-float v4, v4

    #@6e
    div-float/2addr v3, v4

    #@6f
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@72
    move-result v6

    #@73
    .line 145
    .local v6, offset:I
    add-int v3, v6, v7

    #@75
    if-le v3, v15, :cond_79

    #@77
    .line 146
    sub-int v6, v15, v7

    #@79
    :cond_79
    move-object/from16 v3, p0

    #@7b
    move-object/from16 v4, p1

    #@7d
    move-object v5, v13

    #@7e
    move/from16 v8, v17

    #@80
    .line 149
    invoke-virtual/range {v3 .. v8}, Landroid/widget/ScrollBarDrawable;->drawThumb(Landroid/graphics/Canvas;Landroid/graphics/Rect;IIZ)V

    #@83
    goto :goto_35

    #@84
    .line 122
    .end local v6           #offset:I
    .end local v7           #length:I
    .end local v12           #minLength:I
    .end local v15           #size:I
    .end local v16           #thickness:I
    :cond_84
    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    #@87
    move-result v15

    #@88
    goto :goto_4e

    #@89
    .line 123
    .restart local v15       #size:I
    :cond_89
    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    #@8c
    move-result v16

    #@8d
    goto :goto_54
.end method

.method protected drawThumb(Landroid/graphics/Canvas;Landroid/graphics/Rect;IIZ)V
    .registers 13
    .parameter "canvas"
    .parameter "bounds"
    .parameter "offset"
    .parameter "length"
    .parameter "vertical"

    #@0
    .prologue
    .line 175
    iget-object v2, p0, Landroid/widget/ScrollBarDrawable;->mTempBounds:Landroid/graphics/Rect;

    #@2
    .line 176
    .local v2, thumbRect:Landroid/graphics/Rect;
    iget-boolean v3, p0, Landroid/widget/ScrollBarDrawable;->mRangeChanged:Z

    #@4
    if-nez v3, :cond_a

    #@6
    iget-boolean v3, p0, Landroid/widget/ScrollBarDrawable;->mChanged:Z

    #@8
    if-eqz v3, :cond_2a

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    .line 177
    .local v0, changed:Z
    :goto_b
    if-eqz v0, :cond_1d

    #@d
    .line 178
    if-eqz p5, :cond_2c

    #@f
    .line 179
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@11
    iget v4, p2, Landroid/graphics/Rect;->top:I

    #@13
    add-int/2addr v4, p3

    #@14
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@16
    iget v6, p2, Landroid/graphics/Rect;->top:I

    #@18
    add-int/2addr v6, p3

    #@19
    add-int/2addr v6, p4

    #@1a
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    #@1d
    .line 187
    :cond_1d
    :goto_1d
    if-eqz p5, :cond_3b

    #@1f
    .line 188
    iget-object v1, p0, Landroid/widget/ScrollBarDrawable;->mVerticalThumb:Landroid/graphics/drawable/Drawable;

    #@21
    .line 189
    .local v1, thumb:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_26

    #@23
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@26
    .line 190
    :cond_26
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@29
    .line 196
    :goto_29
    return-void

    #@2a
    .line 176
    .end local v0           #changed:Z
    .end local v1           #thumb:Landroid/graphics/drawable/Drawable;
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_b

    #@2c
    .line 182
    .restart local v0       #changed:Z
    :cond_2c
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@2e
    add-int/2addr v3, p3

    #@2f
    iget v4, p2, Landroid/graphics/Rect;->top:I

    #@31
    iget v5, p2, Landroid/graphics/Rect;->left:I

    #@33
    add-int/2addr v5, p3

    #@34
    add-int/2addr v5, p4

    #@35
    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    #@37
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    #@3a
    goto :goto_1d

    #@3b
    .line 192
    :cond_3b
    iget-object v1, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalThumb:Landroid/graphics/drawable/Drawable;

    #@3d
    .line 193
    .restart local v1       #thumb:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_42

    #@3f
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@42
    .line 194
    :cond_42
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@45
    goto :goto_29
.end method

.method protected drawTrack(Landroid/graphics/Canvas;Landroid/graphics/Rect;Z)V
    .registers 6
    .parameter "canvas"
    .parameter "bounds"
    .parameter "vertical"

    #@0
    .prologue
    .line 161
    if-eqz p3, :cond_11

    #@2
    .line 162
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@4
    .line 166
    .local v0, track:Landroid/graphics/drawable/Drawable;
    :goto_4
    if-eqz v0, :cond_10

    #@6
    .line 167
    iget-boolean v1, p0, Landroid/widget/ScrollBarDrawable;->mChanged:Z

    #@8
    if-eqz v1, :cond_d

    #@a
    .line 168
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@d
    .line 170
    :cond_d
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@10
    .line 172
    :cond_10
    return-void

    #@11
    .line 164
    .end local v0           #track:Landroid/graphics/drawable/Drawable;
    :cond_11
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@13
    .restart local v0       #track:Landroid/graphics/drawable/Drawable;
    goto :goto_4
.end method

.method public getAlwaysDrawHorizontalTrack()Z
    .registers 2

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawHorizontalTrack:Z

    #@2
    return v0
.end method

.method public getAlwaysDrawVerticalTrack()Z
    .registers 2

    #@0
    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawVerticalTrack:Z

    #@2
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 254
    const/4 v0, -0x3

    #@1
    return v0
.end method

.method public getSize(Z)I
    .registers 3
    .parameter "vertical"

    #@0
    .prologue
    .line 219
    if-eqz p1, :cond_10

    #@2
    .line 220
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v0, :cond_d

    #@6
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@8
    :goto_8
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@b
    move-result v0

    #@c
    .line 223
    :goto_c
    return v0

    #@d
    .line 220
    :cond_d
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalThumb:Landroid/graphics/drawable/Drawable;

    #@f
    goto :goto_8

    #@10
    .line 223
    :cond_10
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@12
    if-eqz v0, :cond_1b

    #@14
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@16
    :goto_16
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@19
    move-result v0

    #@1a
    goto :goto_c

    #@1b
    :cond_1b
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalThumb:Landroid/graphics/drawable/Drawable;

    #@1d
    goto :goto_16
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@3
    .line 156
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/ScrollBarDrawable;->mChanged:Z

    #@6
    .line 157
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 231
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@9
    .line 233
    :cond_9
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalThumb:Landroid/graphics/drawable/Drawable;

    #@b
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@e
    .line 234
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 235
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@14
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@17
    .line 237
    :cond_17
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalThumb:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@1c
    .line 238
    return-void
.end method

.method public setAlwaysDrawHorizontalTrack(Z)V
    .registers 2
    .parameter "alwaysDrawTrack"

    #@0
    .prologue
    .line 56
    iput-boolean p1, p0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawHorizontalTrack:Z

    #@2
    .line 57
    return-void
.end method

.method public setAlwaysDrawVerticalTrack(Z)V
    .registers 2
    .parameter "alwaysDrawTrack"

    #@0
    .prologue
    .line 66
    iput-boolean p1, p0, Landroid/widget/ScrollBarDrawable;->mAlwaysDrawVerticalTrack:Z

    #@2
    .line 67
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 243
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@9
    .line 245
    :cond_9
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mVerticalThumb:Landroid/graphics/drawable/Drawable;

    #@b
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@e
    .line 246
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 247
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@14
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@17
    .line 249
    :cond_17
    iget-object v0, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalThumb:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@1c
    .line 250
    return-void
.end method

.method public setHorizontalThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "thumb"

    #@0
    .prologue
    .line 209
    if-eqz p1, :cond_4

    #@2
    .line 210
    iput-object p1, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalThumb:Landroid/graphics/drawable/Drawable;

    #@4
    .line 212
    :cond_4
    return-void
.end method

.method public setHorizontalTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "track"

    #@0
    .prologue
    .line 215
    iput-object p1, p0, Landroid/widget/ScrollBarDrawable;->mHorizontalTrack:Landroid/graphics/drawable/Drawable;

    #@2
    .line 216
    return-void
.end method

.method public setParameters(IIIZ)V
    .registers 7
    .parameter "range"
    .parameter "offset"
    .parameter "extent"
    .parameter "vertical"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 86
    iget-boolean v0, p0, Landroid/widget/ScrollBarDrawable;->mVertical:Z

    #@3
    if-eq v0, p4, :cond_7

    #@5
    .line 87
    iput-boolean v1, p0, Landroid/widget/ScrollBarDrawable;->mChanged:Z

    #@7
    .line 90
    :cond_7
    iget v0, p0, Landroid/widget/ScrollBarDrawable;->mRange:I

    #@9
    if-ne v0, p1, :cond_13

    #@b
    iget v0, p0, Landroid/widget/ScrollBarDrawable;->mOffset:I

    #@d
    if-ne v0, p2, :cond_13

    #@f
    iget v0, p0, Landroid/widget/ScrollBarDrawable;->mExtent:I

    #@11
    if-eq v0, p3, :cond_15

    #@13
    .line 91
    :cond_13
    iput-boolean v1, p0, Landroid/widget/ScrollBarDrawable;->mRangeChanged:Z

    #@15
    .line 94
    :cond_15
    iput p1, p0, Landroid/widget/ScrollBarDrawable;->mRange:I

    #@17
    .line 95
    iput p2, p0, Landroid/widget/ScrollBarDrawable;->mOffset:I

    #@19
    .line 96
    iput p3, p0, Landroid/widget/ScrollBarDrawable;->mExtent:I

    #@1b
    .line 97
    iput-boolean p4, p0, Landroid/widget/ScrollBarDrawable;->mVertical:Z

    #@1d
    .line 98
    return-void
.end method

.method public setVerticalThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "thumb"

    #@0
    .prologue
    .line 199
    if-eqz p1, :cond_4

    #@2
    .line 200
    iput-object p1, p0, Landroid/widget/ScrollBarDrawable;->mVerticalThumb:Landroid/graphics/drawable/Drawable;

    #@4
    .line 202
    :cond_4
    return-void
.end method

.method public setVerticalTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "track"

    #@0
    .prologue
    .line 205
    iput-object p1, p0, Landroid/widget/ScrollBarDrawable;->mVerticalTrack:Landroid/graphics/drawable/Drawable;

    #@2
    .line 206
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ScrollBarDrawable: range="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/widget/ScrollBarDrawable;->mRange:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " offset="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/widget/ScrollBarDrawable;->mOffset:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " extent="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/widget/ScrollBarDrawable;->mExtent:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget-boolean v0, p0, Landroid/widget/ScrollBarDrawable;->mVertical:Z

    #@2b
    if-eqz v0, :cond_38

    #@2d
    const-string v0, " V"

    #@2f
    :goto_2f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    return-object v0

    #@38
    :cond_38
    const-string v0, " H"

    #@3a
    goto :goto_2f
.end method
