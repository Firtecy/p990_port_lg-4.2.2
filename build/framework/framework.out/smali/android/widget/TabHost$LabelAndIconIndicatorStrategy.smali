.class Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;
.super Ljava/lang/Object;
.source "TabHost.java"

# interfaces
.implements Landroid/widget/TabHost$IndicatorStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TabHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelAndIconIndicatorStrategy"
.end annotation


# instance fields
.field private final mIcon:Landroid/graphics/drawable/Drawable;

.field private final mLabel:Ljava/lang/CharSequence;

.field final synthetic this$0:Landroid/widget/TabHost;


# direct methods
.method private constructor <init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter
    .parameter "label"
    .parameter "icon"

    #@0
    .prologue
    .line 623
    iput-object p1, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 624
    iput-object p2, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mLabel:Ljava/lang/CharSequence;

    #@7
    .line 625
    iput-object p3, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mIcon:Landroid/graphics/drawable/Drawable;

    #@9
    .line 626
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/widget/TabHost$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 618
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;-><init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@3
    return-void
.end method


# virtual methods
.method public createIndicatorView()Landroid/view/View;
    .registers 11

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 629
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@4
    invoke-virtual {v8}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    .line 630
    .local v1, context:Landroid/content/Context;
    const-string/jumbo v8, "layout_inflater"

    #@b
    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    check-cast v4, Landroid/view/LayoutInflater;

    #@11
    .line 632
    .local v4, inflater:Landroid/view/LayoutInflater;
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@13
    invoke-static {v8}, Landroid/widget/TabHost;->access$900(Landroid/widget/TabHost;)I

    #@16
    move-result v8

    #@17
    iget-object v9, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@19
    invoke-static {v9}, Landroid/widget/TabHost;->access$1000(Landroid/widget/TabHost;)Landroid/widget/TabWidget;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {v4, v8, v9, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@20
    move-result-object v5

    #@21
    .line 636
    .local v5, tabIndicator:Landroid/view/View;
    const v8, 0x1020016

    #@24
    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@27
    move-result-object v6

    #@28
    check-cast v6, Landroid/widget/TextView;

    #@2a
    .line 637
    .local v6, tv:Landroid/widget/TextView;
    const v8, 0x1020006

    #@2d
    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@30
    move-result-object v3

    #@31
    check-cast v3, Landroid/widget/ImageView;

    #@33
    .line 640
    .local v3, iconView:Landroid/widget/ImageView;
    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    #@36
    move-result v8

    #@37
    const/16 v9, 0x8

    #@39
    if-ne v8, v9, :cond_77

    #@3b
    move v2, v0

    #@3c
    .line 641
    .local v2, exclusive:Z
    :goto_3c
    if-eqz v2, :cond_46

    #@3e
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mLabel:Ljava/lang/CharSequence;

    #@40
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@43
    move-result v8

    #@44
    if-eqz v8, :cond_79

    #@46
    .line 643
    .local v0, bindIcon:Z
    :cond_46
    :goto_46
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mLabel:Ljava/lang/CharSequence;

    #@48
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4b
    .line 645
    if-eqz v0, :cond_59

    #@4d
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4f
    if-eqz v8, :cond_59

    #@51
    .line 646
    iget-object v8, p0, Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;->mIcon:Landroid/graphics/drawable/Drawable;

    #@53
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@56
    .line 647
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    #@59
    .line 650
    :cond_59
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5c
    move-result-object v7

    #@5d
    iget v7, v7, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@5f
    const/4 v8, 0x4

    #@60
    if-gt v7, v8, :cond_76

    #@62
    .line 652
    const v7, 0x10805bb

    #@65
    invoke-virtual {v5, v7}, Landroid/view/View;->setBackgroundResource(I)V

    #@68
    .line 653
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6b
    move-result-object v7

    #@6c
    const v8, 0x106007d

    #@6f
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    #@76
    .line 656
    :cond_76
    return-object v5

    #@77
    .end local v0           #bindIcon:Z
    .end local v2           #exclusive:Z
    :cond_77
    move v2, v7

    #@78
    .line 640
    goto :goto_3c

    #@79
    .restart local v2       #exclusive:Z
    :cond_79
    move v0, v7

    #@7a
    .line 641
    goto :goto_46
.end method
