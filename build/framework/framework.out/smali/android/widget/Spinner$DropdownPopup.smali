.class Landroid/widget/Spinner$DropdownPopup;
.super Landroid/widget/ListPopupWindow;
.source "Spinner.java"

# interfaces
.implements Landroid/widget/Spinner$SpinnerPopup;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Spinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DropdownPopup"
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mHintText:Ljava/lang/CharSequence;

.field final synthetic this$0:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/widget/Spinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleRes"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 913
    iput-object p1, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@3
    .line 914
    invoke-direct {p0, p2, p3, v1, p4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@6
    .line 916
    invoke-virtual {p0, p1}, Landroid/widget/Spinner$DropdownPopup;->setAnchorView(Landroid/view/View;)V

    #@9
    .line 917
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Landroid/widget/Spinner$DropdownPopup;->setModal(Z)V

    #@d
    .line 918
    invoke-virtual {p0, v1}, Landroid/widget/Spinner$DropdownPopup;->setPromptPosition(I)V

    #@10
    .line 919
    new-instance v0, Landroid/widget/Spinner$DropdownPopup$1;

    #@12
    invoke-direct {v0, p0, p1}, Landroid/widget/Spinner$DropdownPopup$1;-><init>(Landroid/widget/Spinner$DropdownPopup;Landroid/widget/Spinner;)V

    #@15
    invoke-virtual {p0, v0}, Landroid/widget/Spinner$DropdownPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@18
    .line 928
    return-void
.end method

.method static synthetic access$100(Landroid/widget/Spinner$DropdownPopup;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 909
    iget-object v0, p0, Landroid/widget/Spinner$DropdownPopup;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method


# virtual methods
.method public getHintText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 937
    iget-object v0, p0, Landroid/widget/Spinner$DropdownPopup;->mHintText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 2
    .parameter "adapter"

    #@0
    .prologue
    .line 932
    invoke-super {p0, p1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@3
    .line 933
    iput-object p1, p0, Landroid/widget/Spinner$DropdownPopup;->mAdapter:Landroid/widget/ListAdapter;

    #@5
    .line 934
    return-void
.end method

.method public setPromptText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "hintText"

    #@0
    .prologue
    .line 942
    iput-object p1, p0, Landroid/widget/Spinner$DropdownPopup;->mHintText:Ljava/lang/CharSequence;

    #@2
    .line 943
    return-void
.end method

.method public show()V
    .registers 14

    #@0
    .prologue
    .line 947
    invoke-virtual {p0}, Landroid/widget/Spinner$DropdownPopup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    .line 948
    .local v0, background:Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    #@5
    .line 949
    .local v3, hOffset:I
    if-eqz v0, :cond_ce

    #@7
    .line 950
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@9
    invoke-static {v10}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@c
    move-result-object v10

    #@d
    invoke-virtual {v0, v10}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@10
    .line 951
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@12
    invoke-virtual {v10}, Landroid/widget/Spinner;->isLayoutRtl()Z

    #@15
    move-result v10

    #@16
    if-eqz v10, :cond_c3

    #@18
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@1a
    invoke-static {v10}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@1d
    move-result-object v10

    #@1e
    iget v3, v10, Landroid/graphics/Rect;->right:I

    #@20
    .line 956
    :goto_20
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@22
    invoke-virtual {v10}, Landroid/widget/Spinner;->getPaddingLeft()I

    #@25
    move-result v6

    #@26
    .line 957
    .local v6, spinnerPaddingLeft:I
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@28
    invoke-virtual {v10}, Landroid/widget/Spinner;->getPaddingRight()I

    #@2b
    move-result v7

    #@2c
    .line 958
    .local v7, spinnerPaddingRight:I
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@2e
    invoke-virtual {v10}, Landroid/widget/Spinner;->getWidth()I

    #@31
    move-result v8

    #@32
    .line 959
    .local v8, spinnerWidth:I
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@34
    iget v10, v10, Landroid/widget/Spinner;->mDropDownWidth:I

    #@36
    const/4 v11, -0x2

    #@37
    if-ne v10, v11, :cond_e1

    #@39
    .line 960
    iget-object v11, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@3b
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->mAdapter:Landroid/widget/ListAdapter;

    #@3d
    check-cast v10, Landroid/widget/SpinnerAdapter;

    #@3f
    invoke-virtual {p0}, Landroid/widget/Spinner$DropdownPopup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@42
    move-result-object v12

    #@43
    invoke-virtual {v11, v10, v12}, Landroid/widget/Spinner;->measureContentWidth(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    #@46
    move-result v1

    #@47
    .line 962
    .local v1, contentWidth:I
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@49
    invoke-static {v10}, Landroid/widget/Spinner;->access$300(Landroid/widget/Spinner;)Landroid/content/Context;

    #@4c
    move-result-object v10

    #@4d
    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@50
    move-result-object v10

    #@51
    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@54
    move-result-object v10

    #@55
    iget v10, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@57
    iget-object v11, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@59
    invoke-static {v11}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@5c
    move-result-object v11

    #@5d
    iget v11, v11, Landroid/graphics/Rect;->left:I

    #@5f
    sub-int/2addr v10, v11

    #@60
    iget-object v11, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@62
    invoke-static {v11}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@65
    move-result-object v11

    #@66
    iget v11, v11, Landroid/graphics/Rect;->right:I

    #@68
    sub-int v2, v10, v11

    #@6a
    .line 964
    .local v2, contentWidthLimit:I
    if-le v1, v2, :cond_6d

    #@6c
    .line 965
    move v1, v2

    #@6d
    .line 967
    :cond_6d
    sub-int v10, v8, v6

    #@6f
    sub-int/2addr v10, v7

    #@70
    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    #@73
    move-result v10

    #@74
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setContentWidth(I)V

    #@77
    .line 975
    .end local v1           #contentWidth:I
    .end local v2           #contentWidthLimit:I
    :goto_77
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@79
    invoke-virtual {v10}, Landroid/widget/Spinner;->isLayoutRtl()Z

    #@7c
    move-result v10

    #@7d
    if-eqz v10, :cond_f7

    #@7f
    .line 976
    sub-int v10, v8, v7

    #@81
    invoke-virtual {p0}, Landroid/widget/Spinner$DropdownPopup;->getWidth()I

    #@84
    move-result v11

    #@85
    sub-int/2addr v10, v11

    #@86
    add-int/2addr v3, v10

    #@87
    .line 980
    :goto_87
    invoke-virtual {p0, v3}, Landroid/widget/Spinner$DropdownPopup;->setHorizontalOffset(I)V

    #@8a
    .line 981
    const/4 v10, 0x2

    #@8b
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setInputMethodMode(I)V

    #@8e
    .line 982
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    #@91
    .line 983
    invoke-virtual {p0}, Landroid/widget/Spinner$DropdownPopup;->getListView()Landroid/widget/ListView;

    #@94
    move-result-object v10

    #@95
    const/4 v11, 0x1

    #@96
    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@99
    .line 984
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@9b
    invoke-virtual {v10}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@9e
    move-result v10

    #@9f
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setSelection(I)V

    #@a2
    .line 989
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@a4
    invoke-virtual {v10}, Landroid/widget/Spinner;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@a7
    move-result-object v9

    #@a8
    .line 990
    .local v9, vto:Landroid/view/ViewTreeObserver;
    if-eqz v9, :cond_c2

    #@aa
    .line 991
    new-instance v4, Landroid/widget/Spinner$DropdownPopup$2;

    #@ac
    invoke-direct {v4, p0}, Landroid/widget/Spinner$DropdownPopup$2;-><init>(Landroid/widget/Spinner$DropdownPopup;)V

    #@af
    .line 999
    .local v4, layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    invoke-virtual {v9, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@b2
    .line 1001
    new-instance v5, Landroid/widget/Spinner$DropdownPopup$3;

    #@b4
    invoke-direct {v5, p0}, Landroid/widget/Spinner$DropdownPopup$3;-><init>(Landroid/widget/Spinner$DropdownPopup;)V

    #@b7
    .line 1009
    .local v5, scrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;
    invoke-virtual {v9, v5}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@ba
    .line 1011
    new-instance v10, Landroid/widget/Spinner$DropdownPopup$4;

    #@bc
    invoke-direct {v10, p0, v4, v5}, Landroid/widget/Spinner$DropdownPopup$4;-><init>(Landroid/widget/Spinner$DropdownPopup;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@bf
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    #@c2
    .line 1021
    .end local v4           #layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .end local v5           #scrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;
    :cond_c2
    return-void

    #@c3
    .line 951
    .end local v6           #spinnerPaddingLeft:I
    .end local v7           #spinnerPaddingRight:I
    .end local v8           #spinnerWidth:I
    .end local v9           #vto:Landroid/view/ViewTreeObserver;
    :cond_c3
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@c5
    invoke-static {v10}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@c8
    move-result-object v10

    #@c9
    iget v10, v10, Landroid/graphics/Rect;->left:I

    #@cb
    neg-int v3, v10

    #@cc
    goto/16 :goto_20

    #@ce
    .line 953
    :cond_ce
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@d0
    invoke-static {v10}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@d3
    move-result-object v10

    #@d4
    iget-object v11, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@d6
    invoke-static {v11}, Landroid/widget/Spinner;->access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;

    #@d9
    move-result-object v11

    #@da
    const/4 v12, 0x0

    #@db
    iput v12, v11, Landroid/graphics/Rect;->right:I

    #@dd
    iput v12, v10, Landroid/graphics/Rect;->left:I

    #@df
    goto/16 :goto_20

    #@e1
    .line 969
    .restart local v6       #spinnerPaddingLeft:I
    .restart local v7       #spinnerPaddingRight:I
    .restart local v8       #spinnerWidth:I
    :cond_e1
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@e3
    iget v10, v10, Landroid/widget/Spinner;->mDropDownWidth:I

    #@e5
    const/4 v11, -0x1

    #@e6
    if-ne v10, v11, :cond_ef

    #@e8
    .line 970
    sub-int v10, v8, v6

    #@ea
    sub-int/2addr v10, v7

    #@eb
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setContentWidth(I)V

    #@ee
    goto :goto_77

    #@ef
    .line 972
    :cond_ef
    iget-object v10, p0, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@f1
    iget v10, v10, Landroid/widget/Spinner;->mDropDownWidth:I

    #@f3
    invoke-virtual {p0, v10}, Landroid/widget/Spinner$DropdownPopup;->setContentWidth(I)V

    #@f6
    goto :goto_77

    #@f7
    .line 978
    :cond_f7
    add-int/2addr v3, v6

    #@f8
    goto :goto_87
.end method
