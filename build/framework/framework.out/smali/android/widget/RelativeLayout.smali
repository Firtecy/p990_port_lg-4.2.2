.class public Landroid/widget/RelativeLayout;
.super Landroid/view/ViewGroup;
.source "RelativeLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RelativeLayout$1;,
        Landroid/widget/RelativeLayout$DependencyGraph;,
        Landroid/widget/RelativeLayout$LayoutParams;,
        Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;
    }
.end annotation


# static fields
.field public static final ABOVE:I = 0x2

.field public static final ALIGN_BASELINE:I = 0x4

.field public static final ALIGN_BOTTOM:I = 0x8

.field public static final ALIGN_END:I = 0x13

.field public static final ALIGN_LEFT:I = 0x5

.field public static final ALIGN_PARENT_BOTTOM:I = 0xc

.field public static final ALIGN_PARENT_END:I = 0x15

.field public static final ALIGN_PARENT_LEFT:I = 0x9

.field public static final ALIGN_PARENT_RIGHT:I = 0xb

.field public static final ALIGN_PARENT_START:I = 0x14

.field public static final ALIGN_PARENT_TOP:I = 0xa

.field public static final ALIGN_RIGHT:I = 0x7

.field public static final ALIGN_START:I = 0x12

.field public static final ALIGN_TOP:I = 0x6

.field public static final BELOW:I = 0x3

.field public static final CENTER_HORIZONTAL:I = 0xe

.field public static final CENTER_IN_PARENT:I = 0xd

.field public static final CENTER_VERTICAL:I = 0xf

.field private static final DEBUG_GRAPH:Z = false

.field public static final END_OF:I = 0x11

.field public static final LEFT_OF:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "RelativeLayout"

.field public static final RIGHT_OF:I = 0x1

.field private static final RULES_HORIZONTAL:[I = null

.field private static final RULES_VERTICAL:[I = null

.field public static final START_OF:I = 0x10

.field public static final TRUE:I = -0x1

.field private static final VERB_COUNT:I = 0x16


# instance fields
.field private mBaselineView:Landroid/view/View;

.field private final mContentBounds:Landroid/graphics/Rect;

.field private mDirtyHierarchy:Z

.field private final mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

.field private mGravity:I

.field private mHasBaselineAlignedChild:Z

.field private mIgnoreGravity:I

.field private final mSelfBounds:Landroid/graphics/Rect;

.field private mSortedHorizontalChildren:[Landroid/view/View;

.field private mSortedVerticalChildren:[Landroid/view/View;

.field private mTopToBottomLeftToRightSet:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 182
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_12

    #@6
    sput-object v0, Landroid/widget/RelativeLayout;->RULES_VERTICAL:[I

    #@8
    .line 186
    const/16 v0, 0x8

    #@a
    new-array v0, v0, [I

    #@c
    fill-array-data v0, :array_20

    #@f
    sput-object v0, Landroid/widget/RelativeLayout;->RULES_HORIZONTAL:[I

    #@11
    return-void

    #@12
    .line 182
    :array_12
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    #@20
    .line 186
    :array_20
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x12t 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 206
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@5
    .line 190
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@7
    .line 193
    const v0, 0x800033

    #@a
    iput v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@c
    .line 194
    new-instance v0, Landroid/graphics/Rect;

    #@e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mContentBounds:Landroid/graphics/Rect;

    #@13
    .line 195
    new-instance v0, Landroid/graphics/Rect;

    #@15
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@1a
    .line 198
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@1c
    .line 201
    new-array v0, v2, [Landroid/view/View;

    #@1e
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@20
    .line 202
    new-array v0, v2, [Landroid/view/View;

    #@22
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@24
    .line 203
    new-instance v0, Landroid/widget/RelativeLayout$DependencyGraph;

    #@26
    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$DependencyGraph;-><init>(Landroid/widget/RelativeLayout$1;)V

    #@29
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@2b
    .line 207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 210
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 190
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@7
    .line 193
    const v0, 0x800033

    #@a
    iput v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@c
    .line 194
    new-instance v0, Landroid/graphics/Rect;

    #@e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mContentBounds:Landroid/graphics/Rect;

    #@13
    .line 195
    new-instance v0, Landroid/graphics/Rect;

    #@15
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@1a
    .line 198
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@1c
    .line 201
    new-array v0, v2, [Landroid/view/View;

    #@1e
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@20
    .line 202
    new-array v0, v2, [Landroid/view/View;

    #@22
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@24
    .line 203
    new-instance v0, Landroid/widget/RelativeLayout$DependencyGraph;

    #@26
    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$DependencyGraph;-><init>(Landroid/widget/RelativeLayout$1;)V

    #@29
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@2b
    .line 211
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@2e
    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 215
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 190
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@7
    .line 193
    const v0, 0x800033

    #@a
    iput v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@c
    .line 194
    new-instance v0, Landroid/graphics/Rect;

    #@e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mContentBounds:Landroid/graphics/Rect;

    #@13
    .line 195
    new-instance v0, Landroid/graphics/Rect;

    #@15
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@1a
    .line 198
    iput-object v1, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@1c
    .line 201
    new-array v0, v2, [Landroid/view/View;

    #@1e
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@20
    .line 202
    new-array v0, v2, [Landroid/view/View;

    #@22
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@24
    .line 203
    new-instance v0, Landroid/widget/RelativeLayout$DependencyGraph;

    #@26
    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$DependencyGraph;-><init>(Landroid/widget/RelativeLayout$1;)V

    #@29
    iput-object v0, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@2b
    .line 216
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@2e
    .line 217
    return-void
.end method

.method static synthetic access$1000()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$1100()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$1200()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$1300()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$1400()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$700()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$800()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$900()Z
    .registers 1

    #@0
    .prologue
    .line 71
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method private alignBaseline(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V
    .registers 13
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v9, -0x1

    #@2
    .line 627
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getLayoutDirection()I

    #@5
    move-result v4

    #@6
    .line 628
    .local v4, layoutDirection:I
    invoke-virtual {p2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@9
    move-result-object v7

    #@a
    .line 629
    .local v7, rules:[I
    invoke-direct {p0, v7, v8}, Landroid/widget/RelativeLayout;->getRelatedViewBaseline([II)I

    #@d
    move-result v0

    #@e
    .line 631
    .local v0, anchorBaseline:I
    if-eq v0, v9, :cond_38

    #@10
    .line 632
    invoke-direct {p0, v7, v8}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@13
    move-result-object v1

    #@14
    .line 633
    .local v1, anchorParams:Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v1, :cond_38

    #@16
    .line 634
    invoke-static {v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@19
    move-result v8

    #@1a
    add-int v6, v8, v0

    #@1c
    .line 635
    .local v6, offset:I
    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    #@1f
    move-result v2

    #@20
    .line 636
    .local v2, baseline:I
    if-eq v2, v9, :cond_23

    #@22
    .line 637
    sub-int/2addr v6, v2

    #@23
    .line 639
    :cond_23
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@26
    move-result v8

    #@27
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@2a
    move-result v9

    #@2b
    sub-int v3, v8, v9

    #@2d
    .line 640
    .local v3, height:I
    invoke-static {p2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@30
    .line 641
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@33
    move-result v8

    #@34
    add-int/2addr v8, v3

    #@35
    invoke-static {p2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@38
    .line 645
    .end local v1           #anchorParams:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2           #baseline:I
    .end local v3           #height:I
    .end local v6           #offset:I
    :cond_38
    iget-object v8, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@3a
    if-nez v8, :cond_3f

    #@3c
    .line 646
    iput-object p1, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@3e
    .line 653
    :cond_3e
    :goto_3e
    return-void

    #@3f
    .line 648
    :cond_3f
    iget-object v8, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@41
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@44
    move-result-object v5

    #@45
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    #@47
    .line 649
    .local v5, lp:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@4a
    move-result v8

    #@4b
    invoke-static {v5}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@4e
    move-result v9

    #@4f
    if-lt v8, v9, :cond_65

    #@51
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@54
    move-result v8

    #@55
    invoke-static {v5}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@58
    move-result v9

    #@59
    if-ne v8, v9, :cond_3e

    #@5b
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@5e
    move-result v8

    #@5f
    invoke-static {v5}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@62
    move-result v9

    #@63
    if-ge v8, v9, :cond_3e

    #@65
    .line 650
    :cond_65
    iput-object p1, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@67
    goto :goto_3e
.end method

.method private applyHorizontalSizeRules(Landroid/widget/RelativeLayout$LayoutParams;I)V
    .registers 12
    .parameter "childParams"
    .parameter "myWidth"

    #@0
    .prologue
    const/4 v8, 0x7

    #@1
    const/4 v7, 0x5

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, -0x1

    #@5
    .line 840
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getLayoutDirection()I

    #@8
    move-result v1

    #@9
    .line 841
    .local v1, layoutDirection:I
    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@c
    move-result-object v2

    #@d
    .line 848
    .local v2, rules:[I
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@10
    .line 849
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@13
    .line 851
    invoke-direct {p0, v2, v4}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@16
    move-result-object v0

    #@17
    .line 852
    .local v0, anchorParams:Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v0, :cond_7a

    #@19
    .line 853
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1c
    move-result v3

    #@1d
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1f
    iget v5, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@21
    add-int/2addr v4, v5

    #@22
    sub-int/2addr v3, v4

    #@23
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@26
    .line 863
    :cond_26
    :goto_26
    invoke-direct {p0, v2, v6}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@29
    move-result-object v0

    #@2a
    .line 864
    if-eqz v0, :cond_8f

    #@2c
    .line 865
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@2f
    move-result v3

    #@30
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@32
    iget v5, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@34
    add-int/2addr v4, v5

    #@35
    add-int/2addr v3, v4

    #@36
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@39
    .line 871
    :cond_39
    :goto_39
    invoke-direct {p0, v2, v7}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@3c
    move-result-object v0

    #@3d
    .line 872
    if-eqz v0, :cond_a0

    #@3f
    .line 873
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@42
    move-result v3

    #@43
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@45
    add-int/2addr v3, v4

    #@46
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@49
    .line 878
    :cond_49
    :goto_49
    invoke-direct {p0, v2, v8}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@4c
    move-result-object v0

    #@4d
    .line 879
    if-eqz v0, :cond_b1

    #@4f
    .line 880
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@52
    move-result v3

    #@53
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@55
    sub-int/2addr v3, v4

    #@56
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@59
    .line 889
    :cond_59
    :goto_59
    const/16 v3, 0x9

    #@5b
    aget v3, v2, v3

    #@5d
    if-eqz v3, :cond_67

    #@5f
    .line 890
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@61
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@63
    add-int/2addr v3, v4

    #@64
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@67
    .line 893
    :cond_67
    const/16 v3, 0xb

    #@69
    aget v3, v2, v3

    #@6b
    if-eqz v3, :cond_79

    #@6d
    .line 894
    if-ltz p2, :cond_79

    #@6f
    .line 895
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@71
    sub-int v3, p2, v3

    #@73
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@75
    sub-int/2addr v3, v4

    #@76
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@79
    .line 900
    :cond_79
    return-void

    #@7a
    .line 855
    :cond_7a
    iget-boolean v3, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@7c
    if-eqz v3, :cond_26

    #@7e
    aget v3, v2, v4

    #@80
    if-eqz v3, :cond_26

    #@82
    .line 856
    if-ltz p2, :cond_26

    #@84
    .line 857
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@86
    sub-int v3, p2, v3

    #@88
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@8a
    sub-int/2addr v3, v4

    #@8b
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@8e
    goto :goto_26

    #@8f
    .line 867
    :cond_8f
    iget-boolean v3, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@91
    if-eqz v3, :cond_39

    #@93
    aget v3, v2, v6

    #@95
    if-eqz v3, :cond_39

    #@97
    .line 868
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@99
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@9b
    add-int/2addr v3, v4

    #@9c
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@9f
    goto :goto_39

    #@a0
    .line 874
    :cond_a0
    iget-boolean v3, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@a2
    if-eqz v3, :cond_49

    #@a4
    aget v3, v2, v7

    #@a6
    if-eqz v3, :cond_49

    #@a8
    .line 875
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@aa
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@ac
    add-int/2addr v3, v4

    #@ad
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@b0
    goto :goto_49

    #@b1
    .line 881
    :cond_b1
    iget-boolean v3, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@b3
    if-eqz v3, :cond_59

    #@b5
    aget v3, v2, v8

    #@b7
    if-eqz v3, :cond_59

    #@b9
    .line 882
    if-ltz p2, :cond_59

    #@bb
    .line 883
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@bd
    sub-int v3, p2, v3

    #@bf
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@c1
    sub-int/2addr v3, v4

    #@c2
    invoke-static {p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@c5
    goto :goto_59
.end method

.method private applyVerticalSizeRules(Landroid/widget/RelativeLayout$LayoutParams;I)V
    .registers 11
    .parameter "childParams"
    .parameter "myHeight"

    #@0
    .prologue
    const/16 v7, 0x8

    #@2
    const/4 v6, 0x6

    #@3
    const/4 v5, 0x3

    #@4
    const/4 v3, 0x2

    #@5
    const/4 v2, -0x1

    #@6
    .line 903
    invoke-virtual {p1}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    #@9
    move-result-object v1

    #@a
    .line 906
    .local v1, rules:[I
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@d
    .line 907
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@10
    .line 909
    invoke-direct {p0, v1, v3}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@13
    move-result-object v0

    #@14
    .line 910
    .local v0, anchorParams:Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v0, :cond_7f

    #@16
    .line 911
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@19
    move-result v2

    #@1a
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1c
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@1e
    add-int/2addr v3, v4

    #@1f
    sub-int/2addr v2, v3

    #@20
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@23
    .line 921
    :cond_23
    :goto_23
    invoke-direct {p0, v1, v5}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@26
    move-result-object v0

    #@27
    .line 922
    if-eqz v0, :cond_94

    #@29
    .line 923
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@2c
    move-result v2

    #@2d
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@2f
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@31
    add-int/2addr v3, v4

    #@32
    add-int/2addr v2, v3

    #@33
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@36
    .line 929
    :cond_36
    :goto_36
    invoke-direct {p0, v1, v6}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@39
    move-result-object v0

    #@3a
    .line 930
    if-eqz v0, :cond_a5

    #@3c
    .line 931
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@3f
    move-result v2

    #@40
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@42
    add-int/2addr v2, v3

    #@43
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@46
    .line 936
    :cond_46
    :goto_46
    invoke-direct {p0, v1, v7}, Landroid/widget/RelativeLayout;->getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;

    #@49
    move-result-object v0

    #@4a
    .line 937
    if-eqz v0, :cond_b6

    #@4c
    .line 938
    invoke-static {v0}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@4f
    move-result v2

    #@50
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@52
    sub-int/2addr v2, v3

    #@53
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@56
    .line 947
    :cond_56
    :goto_56
    const/16 v2, 0xa

    #@58
    aget v2, v1, v2

    #@5a
    if-eqz v2, :cond_64

    #@5c
    .line 948
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@5e
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@60
    add-int/2addr v2, v3

    #@61
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@64
    .line 951
    :cond_64
    const/16 v2, 0xc

    #@66
    aget v2, v1, v2

    #@68
    if-eqz v2, :cond_76

    #@6a
    .line 952
    if-ltz p2, :cond_76

    #@6c
    .line 953
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@6e
    sub-int v2, p2, v2

    #@70
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@72
    sub-int/2addr v2, v3

    #@73
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@76
    .line 959
    :cond_76
    const/4 v2, 0x4

    #@77
    aget v2, v1, v2

    #@79
    if-eqz v2, :cond_7e

    #@7b
    .line 960
    const/4 v2, 0x1

    #@7c
    iput-boolean v2, p0, Landroid/widget/RelativeLayout;->mHasBaselineAlignedChild:Z

    #@7e
    .line 962
    :cond_7e
    return-void

    #@7f
    .line 913
    :cond_7f
    iget-boolean v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@81
    if-eqz v2, :cond_23

    #@83
    aget v2, v1, v3

    #@85
    if-eqz v2, :cond_23

    #@87
    .line 914
    if-ltz p2, :cond_23

    #@89
    .line 915
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@8b
    sub-int v2, p2, v2

    #@8d
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@8f
    sub-int/2addr v2, v3

    #@90
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@93
    goto :goto_23

    #@94
    .line 925
    :cond_94
    iget-boolean v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@96
    if-eqz v2, :cond_36

    #@98
    aget v2, v1, v5

    #@9a
    if-eqz v2, :cond_36

    #@9c
    .line 926
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@9e
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@a0
    add-int/2addr v2, v3

    #@a1
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@a4
    goto :goto_36

    #@a5
    .line 932
    :cond_a5
    iget-boolean v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@a7
    if-eqz v2, :cond_46

    #@a9
    aget v2, v1, v6

    #@ab
    if-eqz v2, :cond_46

    #@ad
    .line 933
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@af
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@b1
    add-int/2addr v2, v3

    #@b2
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@b5
    goto :goto_46

    #@b6
    .line 939
    :cond_b6
    iget-boolean v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@b8
    if-eqz v2, :cond_56

    #@ba
    aget v2, v1, v7

    #@bc
    if-eqz v2, :cond_56

    #@be
    .line 940
    if-ltz p2, :cond_56

    #@c0
    .line 941
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@c2
    sub-int v2, p2, v2

    #@c4
    iget v3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@c6
    sub-int/2addr v2, v3

    #@c7
    invoke-static {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@ca
    goto :goto_56
.end method

.method private centerHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V
    .registers 7
    .parameter "child"
    .parameter "params"
    .parameter "myWidth"

    #@0
    .prologue
    .line 1005
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3
    move-result v0

    #@4
    .line 1006
    .local v0, childWidth:I
    sub-int v2, p3, v0

    #@6
    div-int/lit8 v1, v2, 0x2

    #@8
    .line 1008
    .local v1, left:I
    invoke-static {p2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@b
    .line 1009
    add-int v2, v1, v0

    #@d
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@10
    .line 1010
    return-void
.end method

.method private centerVertical(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V
    .registers 7
    .parameter "child"
    .parameter "params"
    .parameter "myHeight"

    #@0
    .prologue
    .line 1013
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@3
    move-result v0

    #@4
    .line 1014
    .local v0, childHeight:I
    sub-int v2, p3, v0

    #@6
    div-int/lit8 v1, v2, 0x2

    #@8
    .line 1016
    .local v1, top:I
    invoke-static {p2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@b
    .line 1017
    add-int v2, v1, v0

    #@d
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@10
    .line 1018
    return-void
.end method

.method private getChildMeasureSpec(IIIIIIII)I
    .registers 15
    .parameter "childStart"
    .parameter "childEnd"
    .parameter "childSize"
    .parameter "startMargin"
    .parameter "endMargin"
    .parameter "startPadding"
    .parameter "endPadding"
    .parameter "mySize"

    #@0
    .prologue
    .line 713
    const/4 v0, 0x0

    #@1
    .line 714
    .local v0, childSpecMode:I
    const/4 v1, 0x0

    #@2
    .line 717
    .local v1, childSpecSize:I
    move v4, p1

    #@3
    .line 718
    .local v4, tempStart:I
    move v3, p2

    #@4
    .line 722
    .local v3, tempEnd:I
    if-gez v4, :cond_8

    #@6
    .line 723
    add-int v4, p6, p4

    #@8
    .line 725
    :cond_8
    if-gez v3, :cond_e

    #@a
    .line 726
    sub-int v5, p8, p7

    #@c
    sub-int v3, v5, p5

    #@e
    .line 730
    :cond_e
    sub-int v2, v3, v4

    #@10
    .line 732
    .local v2, maxAvailable:I
    if-ltz p1, :cond_1c

    #@12
    if-ltz p2, :cond_1c

    #@14
    .line 734
    const/high16 v0, 0x4000

    #@16
    .line 735
    move v1, v2

    #@17
    .line 770
    :cond_17
    :goto_17
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1a
    move-result v5

    #@1b
    return v5

    #@1c
    .line 737
    :cond_1c
    if-ltz p3, :cond_29

    #@1e
    .line 739
    const/high16 v0, 0x4000

    #@20
    .line 741
    if-ltz v2, :cond_27

    #@22
    .line 743
    invoke-static {v2, p3}, Ljava/lang/Math;->min(II)I

    #@25
    move-result v1

    #@26
    goto :goto_17

    #@27
    .line 746
    :cond_27
    move v1, p3

    #@28
    goto :goto_17

    #@29
    .line 748
    :cond_29
    const/4 v5, -0x1

    #@2a
    if-ne p3, v5, :cond_30

    #@2c
    .line 751
    const/high16 v0, 0x4000

    #@2e
    .line 752
    move v1, v2

    #@2f
    goto :goto_17

    #@30
    .line 753
    :cond_30
    const/4 v5, -0x2

    #@31
    if-ne p3, v5, :cond_17

    #@33
    .line 757
    if-ltz v2, :cond_39

    #@35
    .line 759
    const/high16 v0, -0x8000

    #@37
    .line 760
    move v1, v2

    #@38
    goto :goto_17

    #@39
    .line 764
    :cond_39
    const/4 v0, 0x0

    #@3a
    .line 765
    const/4 v1, 0x0

    #@3b
    goto :goto_17
.end method

.method private getRelatedView([II)Landroid/view/View;
    .registers 9
    .parameter "rules"
    .parameter "relation"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 965
    aget v0, p1, p2

    #@3
    .line 966
    .local v0, id:I
    if-eqz v0, :cond_42

    #@5
    .line 967
    iget-object v3, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@7
    invoke-static {v3}, Landroid/widget/RelativeLayout$DependencyGraph;->access$500(Landroid/widget/RelativeLayout$DependencyGraph;)Landroid/util/SparseArray;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@11
    .line 968
    .local v1, node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    if-nez v1, :cond_15

    #@13
    move-object v2, v4

    #@14
    .line 982
    .end local v1           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_14
    :goto_14
    return-object v2

    #@15
    .line 969
    .restart local v1       #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_15
    iget-object v2, v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@17
    .line 972
    .local v2, v:Landroid/view/View;
    :goto_17
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@1a
    move-result v3

    #@1b
    const/16 v5, 0x8

    #@1d
    if-ne v3, v5, :cond_14

    #@1f
    .line 973
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@22
    move-result-object v3

    #@23
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    #@25
    invoke-virtual {v2}, Landroid/view/View;->getLayoutDirection()I

    #@28
    move-result v5

    #@29
    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@2c
    move-result-object p1

    #@2d
    .line 974
    iget-object v3, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@2f
    invoke-static {v3}, Landroid/widget/RelativeLayout$DependencyGraph;->access$500(Landroid/widget/RelativeLayout$DependencyGraph;)Landroid/util/SparseArray;

    #@32
    move-result-object v3

    #@33
    aget v5, p1, p2

    #@35
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    .end local v1           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@3b
    .line 975
    .restart local v1       #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    if-nez v1, :cond_3f

    #@3d
    move-object v2, v4

    #@3e
    goto :goto_14

    #@3f
    .line 976
    :cond_3f
    iget-object v2, v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@41
    goto :goto_17

    #@42
    .end local v1           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    .end local v2           #v:Landroid/view/View;
    :cond_42
    move-object v2, v4

    #@43
    .line 982
    goto :goto_14
.end method

.method private getRelatedViewBaseline([II)I
    .registers 5
    .parameter "rules"
    .parameter "relation"

    #@0
    .prologue
    .line 997
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;->getRelatedView([II)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 998
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_b

    #@6
    .line 999
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@9
    move-result v1

    #@a
    .line 1001
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, -0x1

    #@c
    goto :goto_a
.end method

.method private getRelatedViewParams([II)Landroid/widget/RelativeLayout$LayoutParams;
    .registers 6
    .parameter "rules"
    .parameter "relation"

    #@0
    .prologue
    .line 986
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;->getRelatedView([II)Landroid/view/View;

    #@3
    move-result-object v1

    #@4
    .line 987
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_15

    #@6
    .line 988
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9
    move-result-object v0

    #@a
    .line 989
    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Landroid/widget/RelativeLayout$LayoutParams;

    #@c
    if-eqz v2, :cond_15

    #@e
    .line 990
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    #@14
    .line 993
    .end local v0           #params:Landroid/view/ViewGroup$LayoutParams;
    :goto_14
    return-object v2

    #@15
    :cond_15
    const/4 v2, 0x0

    #@16
    goto :goto_14
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 220
    sget-object v1, Lcom/android/internal/R$styleable;->RelativeLayout:[I

    #@2
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v0

    #@6
    .line 221
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    #@7
    const/4 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@b
    move-result v1

    #@c
    iput v1, p0, Landroid/widget/RelativeLayout;->mIgnoreGravity:I

    #@e
    .line 222
    const/4 v1, 0x0

    #@f
    iget v2, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@14
    move-result v1

    #@15
    iput v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@17
    .line 223
    sget-boolean v1, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@19
    if-eqz v1, :cond_30

    #@1b
    .line 224
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@1d
    and-int/lit8 v1, v1, 0x3

    #@1f
    const/4 v2, 0x3

    #@20
    if-eq v1, v2, :cond_29

    #@22
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@24
    and-int/lit8 v1, v1, 0x5

    #@26
    const/4 v2, 0x5

    #@27
    if-ne v1, v2, :cond_30

    #@29
    .line 225
    :cond_29
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@2b
    const/high16 v2, 0x80

    #@2d
    or-int/2addr v1, v2

    #@2e
    iput v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@30
    .line 228
    :cond_30
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 229
    return-void
.end method

.method private measureChild(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V
    .registers 16
    .parameter "child"
    .parameter "params"
    .parameter "myWidth"
    .parameter "myHeight"

    #@0
    .prologue
    .line 666
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@3
    move-result v1

    #@4
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@7
    move-result v2

    #@8
    iget v3, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@a
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@c
    iget v5, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@e
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@10
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@12
    move-object v0, p0

    #@13
    move v8, p3

    #@14
    invoke-direct/range {v0 .. v8}, Landroid/widget/RelativeLayout;->getChildMeasureSpec(IIIIIIII)I

    #@17
    move-result v10

    #@18
    .line 671
    .local v10, childWidthMeasureSpec:I
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1b
    move-result v1

    #@1c
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1f
    move-result v2

    #@20
    iget v3, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@22
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@24
    iget v5, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@26
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@28
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@2a
    move-object v0, p0

    #@2b
    move v8, p4

    #@2c
    invoke-direct/range {v0 .. v8}, Landroid/widget/RelativeLayout;->getChildMeasureSpec(IIIIIIII)I

    #@2f
    move-result v9

    #@30
    .line 676
    .local v9, childHeightMeasureSpec:I
    invoke-virtual {p1, v10, v9}, Landroid/view/View;->measure(II)V

    #@33
    .line 677
    return-void
.end method

.method private measureChildHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V
    .registers 16
    .parameter "child"
    .parameter "params"
    .parameter "myWidth"
    .parameter "myHeight"

    #@0
    .prologue
    .line 680
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@3
    move-result v1

    #@4
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@7
    move-result v2

    #@8
    iget v3, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@a
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@c
    iget v5, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@e
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@10
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@12
    move-object v0, p0

    #@13
    move v8, p3

    #@14
    invoke-direct/range {v0 .. v8}, Landroid/widget/RelativeLayout;->getChildMeasureSpec(IIIIIIII)I

    #@17
    move-result v10

    #@18
    .line 686
    .local v10, childWidthMeasureSpec:I
    iget v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1a
    const/4 v1, -0x1

    #@1b
    if-ne v0, v1, :cond_27

    #@1d
    .line 687
    const/high16 v0, 0x4000

    #@1f
    invoke-static {p4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@22
    move-result v9

    #@23
    .line 691
    .local v9, childHeightMeasureSpec:I
    :goto_23
    invoke-virtual {p1, v10, v9}, Landroid/view/View;->measure(II)V

    #@26
    .line 692
    return-void

    #@27
    .line 689
    .end local v9           #childHeightMeasureSpec:I
    :cond_27
    const/high16 v0, -0x8000

    #@29
    invoke-static {p4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2c
    move-result v9

    #@2d
    .restart local v9       #childHeightMeasureSpec:I
    goto :goto_23
.end method

.method private positionChildHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z
    .registers 10
    .parameter "child"
    .parameter "params"
    .parameter "myWidth"
    .parameter "wrapContent"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 776
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getLayoutDirection()I

    #@4
    move-result v0

    #@5
    .line 777
    .local v0, layoutDirection:I
    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@8
    move-result-object v1

    #@9
    .line 779
    .local v1, rules:[I
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@c
    move-result v3

    #@d
    if-gez v3, :cond_28

    #@f
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@12
    move-result v3

    #@13
    if-ltz v3, :cond_28

    #@15
    .line 781
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@18
    move-result v3

    #@19
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@1c
    move-result v4

    #@1d
    sub-int/2addr v3, v4

    #@1e
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@21
    .line 807
    :cond_21
    :goto_21
    const/16 v3, 0x15

    #@23
    aget v3, v1, v3

    #@25
    if-eqz v3, :cond_a7

    #@27
    :goto_27
    return v2

    #@28
    .line 782
    :cond_28
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@2b
    move-result v3

    #@2c
    if-ltz v3, :cond_41

    #@2e
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@31
    move-result v3

    #@32
    if-gez v3, :cond_41

    #@34
    .line 784
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@37
    move-result v3

    #@38
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3b
    move-result v4

    #@3c
    add-int/2addr v3, v4

    #@3d
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@40
    goto :goto_21

    #@41
    .line 785
    :cond_41
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@44
    move-result v3

    #@45
    if-gez v3, :cond_21

    #@47
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@4a
    move-result v3

    #@4b
    if-gez v3, :cond_21

    #@4d
    .line 787
    const/16 v3, 0xd

    #@4f
    aget v3, v1, v3

    #@51
    if-nez v3, :cond_59

    #@53
    const/16 v3, 0xe

    #@55
    aget v3, v1, v3

    #@57
    if-eqz v3, :cond_74

    #@59
    .line 788
    :cond_59
    if-nez p4, :cond_5f

    #@5b
    .line 789
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->centerHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@5e
    goto :goto_27

    #@5f
    .line 791
    :cond_5f
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@61
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@63
    add-int/2addr v3, v4

    #@64
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@67
    .line 792
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@6a
    move-result v3

    #@6b
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@6e
    move-result v4

    #@6f
    add-int/2addr v3, v4

    #@70
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@73
    goto :goto_27

    #@74
    .line 798
    :cond_74
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->isLayoutRtl()Z

    #@77
    move-result v3

    #@78
    if-eqz v3, :cond_91

    #@7a
    .line 799
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@7c
    sub-int v3, p3, v3

    #@7e
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@80
    sub-int/2addr v3, v4

    #@81
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@84
    .line 800
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@87
    move-result v3

    #@88
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@8b
    move-result v4

    #@8c
    sub-int/2addr v3, v4

    #@8d
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@90
    goto :goto_21

    #@91
    .line 802
    :cond_91
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@93
    iget v4, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@95
    add-int/2addr v3, v4

    #@96
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@99
    .line 803
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@9c
    move-result v3

    #@9d
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@a0
    move-result v4

    #@a1
    add-int/2addr v3, v4

    #@a2
    invoke-static {p2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@a5
    goto/16 :goto_21

    #@a7
    .line 807
    :cond_a7
    const/4 v2, 0x0

    #@a8
    goto/16 :goto_27
.end method

.method private positionChildVertical(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z
    .registers 9
    .parameter "child"
    .parameter "params"
    .parameter "myHeight"
    .parameter "wrapContent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 813
    invoke-virtual {p2}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    #@4
    move-result-object v0

    #@5
    .line 815
    .local v0, rules:[I
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@8
    move-result v2

    #@9
    if-gez v2, :cond_24

    #@b
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@e
    move-result v2

    #@f
    if-ltz v2, :cond_24

    #@11
    .line 817
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@14
    move-result v2

    #@15
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@18
    move-result v3

    #@19
    sub-int/2addr v2, v3

    #@1a
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@1d
    .line 836
    :cond_1d
    :goto_1d
    const/16 v2, 0xc

    #@1f
    aget v2, v0, v2

    #@21
    if-eqz v2, :cond_85

    #@23
    :goto_23
    return v1

    #@24
    .line 818
    :cond_24
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@27
    move-result v2

    #@28
    if-ltz v2, :cond_3d

    #@2a
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@2d
    move-result v2

    #@2e
    if-gez v2, :cond_3d

    #@30
    .line 820
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@33
    move-result v2

    #@34
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@37
    move-result v3

    #@38
    add-int/2addr v2, v3

    #@39
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@3c
    goto :goto_1d

    #@3d
    .line 821
    :cond_3d
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@40
    move-result v2

    #@41
    if-gez v2, :cond_1d

    #@43
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@46
    move-result v2

    #@47
    if-gez v2, :cond_1d

    #@49
    .line 823
    const/16 v2, 0xd

    #@4b
    aget v2, v0, v2

    #@4d
    if-nez v2, :cond_55

    #@4f
    const/16 v2, 0xf

    #@51
    aget v2, v0, v2

    #@53
    if-eqz v2, :cond_70

    #@55
    .line 824
    :cond_55
    if-nez p4, :cond_5b

    #@57
    .line 825
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->centerVertical(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@5a
    goto :goto_23

    #@5b
    .line 827
    :cond_5b
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@5d
    iget v3, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@5f
    add-int/2addr v2, v3

    #@60
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@63
    .line 828
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@66
    move-result v2

    #@67
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@6a
    move-result v3

    #@6b
    add-int/2addr v2, v3

    #@6c
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@6f
    goto :goto_23

    #@70
    .line 832
    :cond_70
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@72
    iget v3, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@74
    add-int/2addr v2, v3

    #@75
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@78
    .line 833
    invoke-static {p2}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@7b
    move-result v2

    #@7c
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@7f
    move-result v3

    #@80
    add-int/2addr v2, v3

    #@81
    invoke-static {p2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@84
    goto :goto_1d

    #@85
    .line 836
    :cond_85
    const/4 v1, 0x0

    #@86
    goto :goto_23
.end method

.method private sortChildren()V
    .registers 7

    #@0
    .prologue
    .line 333
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 334
    .local v1, count:I
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@6
    array-length v4, v4

    #@7
    if-eq v4, v1, :cond_d

    #@9
    new-array v4, v1, [Landroid/view/View;

    #@b
    iput-object v4, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@d
    .line 335
    :cond_d
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@f
    array-length v4, v4

    #@10
    if-eq v4, v1, :cond_16

    #@12
    new-array v4, v1, [Landroid/view/View;

    #@14
    iput-object v4, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@16
    .line 337
    :cond_16
    iget-object v2, p0, Landroid/widget/RelativeLayout;->mGraph:Landroid/widget/RelativeLayout$DependencyGraph;

    #@18
    .line 338
    .local v2, graph:Landroid/widget/RelativeLayout$DependencyGraph;
    invoke-virtual {v2}, Landroid/widget/RelativeLayout$DependencyGraph;->clear()V

    #@1b
    .line 340
    const/4 v3, 0x0

    #@1c
    .local v3, i:I
    :goto_1c
    if-ge v3, v1, :cond_28

    #@1e
    .line 341
    invoke-virtual {p0, v3}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    .line 342
    .local v0, child:Landroid/view/View;
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$DependencyGraph;->add(Landroid/view/View;)V

    #@25
    .line 340
    add-int/lit8 v3, v3, 0x1

    #@27
    goto :goto_1c

    #@28
    .line 352
    .end local v0           #child:Landroid/view/View;
    :cond_28
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@2a
    sget-object v5, Landroid/widget/RelativeLayout;->RULES_VERTICAL:[I

    #@2c
    invoke-virtual {v2, v4, v5}, Landroid/widget/RelativeLayout$DependencyGraph;->getSortedViews([Landroid/view/View;[I)V

    #@2f
    .line 353
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@31
    sget-object v5, Landroid/widget/RelativeLayout;->RULES_HORIZONTAL:[I

    #@33
    invoke-virtual {v2, v4, v5}, Landroid/widget/RelativeLayout$DependencyGraph;->getSortedViews([Landroid/view/View;[I)V

    #@36
    .line 365
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1054
    instance-of v0, p1, Landroid/widget/RelativeLayout$LayoutParams;

    #@2
    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 1064
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@2
    if-nez v4, :cond_11

    #@4
    .line 1065
    new-instance v4, Ljava/util/TreeSet;

    #@6
    new-instance v5, Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;

    #@8
    const/4 v6, 0x0

    #@9
    invoke-direct {v5, p0, v6}, Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;-><init>(Landroid/widget/RelativeLayout;Landroid/widget/RelativeLayout$1;)V

    #@c
    invoke-direct {v4, v5}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    #@f
    iput-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@11
    .line 1069
    :cond_11
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getChildCount()I

    #@15
    move-result v0

    #@16
    .local v0, count:I
    :goto_16
    if-ge v1, v0, :cond_24

    #@18
    .line 1070
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@1a
    invoke-virtual {p0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@1d
    move-result-object v5

    #@1e
    invoke-interface {v4, v5}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    #@21
    .line 1069
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_16

    #@24
    .line 1073
    :cond_24
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@26
    invoke-interface {v4}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v2

    #@2a
    .local v2, i$:Ljava/util/Iterator;
    :cond_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_49

    #@30
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    check-cast v3, Landroid/view/View;

    #@36
    .line 1074
    .local v3, view:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_2a

    #@3c
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_2a

    #@42
    .line 1076
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@44
    invoke-interface {v4}, Ljava/util/SortedSet;->clear()V

    #@47
    .line 1077
    const/4 v4, 0x1

    #@48
    .line 1082
    .end local v3           #view:Landroid/view/View;
    :goto_48
    return v4

    #@49
    .line 1081
    :cond_49
    iget-object v4, p0, Landroid/widget/RelativeLayout;->mTopToBottomLeftToRightSet:Ljava/util/SortedSet;

    #@4b
    invoke-interface {v4}, Ljava/util/SortedSet;->clear()V

    #@4e
    .line 1082
    const/4 v4, 0x0

    #@4f
    goto :goto_48
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 1048
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    #@6
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 70
    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1059
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 1038
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public getBaseline()I
    .registers 2

    #@0
    .prologue
    .line 323
    iget-object v0, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/RelativeLayout;->mBaselineView:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public getGravity()I
    .registers 2

    #@0
    .prologue
    .line 263
    iget v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@2
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1087
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1088
    const-class v0, Landroid/widget/RelativeLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1089
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1093
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1094
    const-class v0, Landroid/widget/RelativeLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1095
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 14
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1024
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 1026
    .local v1, count:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_2f

    #@7
    .line 1027
    invoke-virtual {p0, v2}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 1028
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@e
    move-result v4

    #@f
    const/16 v5, 0x8

    #@11
    if-eq v4, v5, :cond_2c

    #@13
    .line 1029
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    #@19
    .line 1031
    .local v3, st:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1c
    move-result v4

    #@1d
    invoke-static {v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@20
    move-result v5

    #@21
    invoke-static {v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@24
    move-result v6

    #@25
    invoke-static {v3}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@28
    move-result v7

    #@29
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    #@2c
    .line 1026
    .end local v3           #st:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_5

    #@2f
    .line 1034
    .end local v0           #child:Landroid/view/View;
    :cond_2f
    return-void
.end method

.method protected onMeasure(II)V
    .registers 47
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 371
    move-object/from16 v0, p0

    #@2
    iget-boolean v4, v0, Landroid/widget/RelativeLayout;->mDirtyHierarchy:Z

    #@4
    if-eqz v4, :cond_e

    #@6
    .line 372
    const/4 v4, 0x0

    #@7
    move-object/from16 v0, p0

    #@9
    iput-boolean v4, v0, Landroid/widget/RelativeLayout;->mDirtyHierarchy:Z

    #@b
    .line 373
    invoke-direct/range {p0 .. p0}, Landroid/widget/RelativeLayout;->sortChildren()V

    #@e
    .line 376
    :cond_e
    const/16 v29, -0x1

    #@10
    .line 377
    .local v29, myWidth:I
    const/16 v28, -0x1

    #@12
    .line 379
    .local v28, myHeight:I
    const/16 v40, 0x0

    #@14
    .line 380
    .local v40, width:I
    const/16 v18, 0x0

    #@16
    .line 382
    .local v18, height:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@19
    move-result v41

    #@1a
    .line 383
    .local v41, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@1d
    move-result v19

    #@1e
    .line 384
    .local v19, heightMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@21
    move-result v42

    #@22
    .line 385
    .local v42, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@25
    move-result v20

    #@26
    .line 388
    .local v20, heightSize:I
    if-eqz v41, :cond_2a

    #@28
    .line 389
    move/from16 v29, v42

    #@2a
    .line 392
    :cond_2a
    if-eqz v19, :cond_2e

    #@2c
    .line 393
    move/from16 v28, v20

    #@2e
    .line 396
    :cond_2e
    const/high16 v4, 0x4000

    #@30
    move/from16 v0, v41

    #@32
    if-ne v0, v4, :cond_36

    #@34
    .line 397
    move/from16 v40, v29

    #@36
    .line 400
    :cond_36
    const/high16 v4, 0x4000

    #@38
    move/from16 v0, v19

    #@3a
    if-ne v0, v4, :cond_3e

    #@3c
    .line 401
    move/from16 v18, v28

    #@3e
    .line 404
    :cond_3e
    const/4 v4, 0x0

    #@3f
    move-object/from16 v0, p0

    #@41
    iput-boolean v4, v0, Landroid/widget/RelativeLayout;->mHasBaselineAlignedChild:Z

    #@43
    .line 406
    const/16 v24, 0x0

    #@45
    .line 407
    .local v24, ignore:Landroid/view/View;
    move-object/from16 v0, p0

    #@47
    iget v4, v0, Landroid/widget/RelativeLayout;->mGravity:I

    #@49
    const v5, 0x800007

    #@4c
    and-int v17, v4, v5

    #@4e
    .line 408
    .local v17, gravity:I
    const v4, 0x800003

    #@51
    move/from16 v0, v17

    #@53
    if-eq v0, v4, :cond_102

    #@55
    if-eqz v17, :cond_102

    #@57
    const/16 v21, 0x1

    #@59
    .line 409
    .local v21, horizontalGravity:Z
    :goto_59
    move-object/from16 v0, p0

    #@5b
    iget v4, v0, Landroid/widget/RelativeLayout;->mGravity:I

    #@5d
    and-int/lit8 v17, v4, 0x70

    #@5f
    .line 410
    const/16 v4, 0x30

    #@61
    move/from16 v0, v17

    #@63
    if-eq v0, v4, :cond_106

    #@65
    if-eqz v17, :cond_106

    #@67
    const/16 v36, 0x1

    #@69
    .line 412
    .local v36, verticalGravity:Z
    :goto_69
    const v27, 0x7fffffff

    #@6c
    .line 413
    .local v27, left:I
    const v35, 0x7fffffff

    #@6f
    .line 414
    .local v35, top:I
    const/high16 v33, -0x8000

    #@71
    .line 415
    .local v33, right:I
    const/high16 v10, -0x8000

    #@73
    .line 417
    .local v10, bottom:I
    const/16 v30, 0x0

    #@75
    .line 418
    .local v30, offsetHorizontalAxis:Z
    const/16 v31, 0x0

    #@77
    .line 420
    .local v31, offsetVerticalAxis:Z
    if-nez v21, :cond_7b

    #@79
    if-eqz v36, :cond_8c

    #@7b
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    iget v4, v0, Landroid/widget/RelativeLayout;->mIgnoreGravity:I

    #@7f
    const/4 v5, -0x1

    #@80
    if-eq v4, v5, :cond_8c

    #@82
    .line 421
    move-object/from16 v0, p0

    #@84
    iget v4, v0, Landroid/widget/RelativeLayout;->mIgnoreGravity:I

    #@86
    move-object/from16 v0, p0

    #@88
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    #@8b
    move-result-object v24

    #@8c
    .line 424
    :cond_8c
    const/high16 v4, 0x4000

    #@8e
    move/from16 v0, v41

    #@90
    if-eq v0, v4, :cond_10a

    #@92
    const/16 v26, 0x1

    #@94
    .line 425
    .local v26, isWrapContentWidth:Z
    :goto_94
    const/high16 v4, 0x4000

    #@96
    move/from16 v0, v19

    #@98
    if-eq v0, v4, :cond_10d

    #@9a
    const/16 v25, 0x1

    #@9c
    .line 427
    .local v25, isWrapContentHeight:Z
    :goto_9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Landroid/widget/RelativeLayout;->mSortedHorizontalChildren:[Landroid/view/View;

    #@a0
    move-object/from16 v38, v0

    #@a2
    .line 428
    .local v38, views:[Landroid/view/View;
    move-object/from16 v0, v38

    #@a4
    array-length v0, v0

    #@a5
    move/from16 v16, v0

    #@a7
    .line 431
    .local v16, count:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->isLayoutRtl()Z

    #@aa
    move-result v4

    #@ab
    if-eqz v4, :cond_120

    #@ad
    const/4 v4, -0x1

    #@ae
    move/from16 v0, v29

    #@b0
    if-eq v0, v4, :cond_b4

    #@b2
    if-eqz v26, :cond_120

    #@b4
    .line 432
    :cond_b4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getPaddingStart()I

    #@b7
    move-result v4

    #@b8
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getPaddingEnd()I

    #@bb
    move-result v5

    #@bc
    add-int v39, v4, v5

    #@be
    .line 433
    .local v39, w:I
    const/4 v4, 0x0

    #@bf
    const/4 v5, 0x0

    #@c0
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c3
    move-result v15

    #@c4
    .line 434
    .local v15, childWidthMeasureSpec:I
    const/16 v23, 0x0

    #@c6
    .local v23, i:I
    :goto_c6
    move/from16 v0, v23

    #@c8
    move/from16 v1, v16

    #@ca
    if-ge v0, v1, :cond_119

    #@cc
    .line 435
    aget-object v11, v38, v23

    #@ce
    .line 436
    .local v11, child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@d1
    move-result v4

    #@d2
    const/16 v5, 0x8

    #@d4
    if-eq v4, v5, :cond_ff

    #@d6
    .line 437
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@d9
    move-result-object v32

    #@da
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@dc
    .line 442
    .local v32, params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, v32

    #@de
    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@e0
    const/4 v5, -0x1

    #@e1
    if-ne v4, v5, :cond_110

    #@e3
    .line 443
    const/high16 v4, 0x4000

    #@e5
    move/from16 v0, v28

    #@e7
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@ea
    move-result v13

    #@eb
    .line 447
    .local v13, childHeightMeasureSpec:I
    :goto_eb
    invoke-virtual {v11, v15, v13}, Landroid/view/View;->measure(II)V

    #@ee
    .line 449
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    #@f1
    move-result v4

    #@f2
    add-int v39, v39, v4

    #@f4
    .line 450
    move-object/from16 v0, v32

    #@f6
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@f8
    move-object/from16 v0, v32

    #@fa
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@fc
    add-int/2addr v4, v5

    #@fd
    add-int v39, v39, v4

    #@ff
    .line 434
    .end local v13           #childHeightMeasureSpec:I
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_ff
    add-int/lit8 v23, v23, 0x1

    #@101
    goto :goto_c6

    #@102
    .line 408
    .end local v10           #bottom:I
    .end local v11           #child:Landroid/view/View;
    .end local v15           #childWidthMeasureSpec:I
    .end local v16           #count:I
    .end local v21           #horizontalGravity:Z
    .end local v23           #i:I
    .end local v25           #isWrapContentHeight:Z
    .end local v26           #isWrapContentWidth:Z
    .end local v27           #left:I
    .end local v30           #offsetHorizontalAxis:Z
    .end local v31           #offsetVerticalAxis:Z
    .end local v33           #right:I
    .end local v35           #top:I
    .end local v36           #verticalGravity:Z
    .end local v38           #views:[Landroid/view/View;
    .end local v39           #w:I
    :cond_102
    const/16 v21, 0x0

    #@104
    goto/16 :goto_59

    #@106
    .line 410
    .restart local v21       #horizontalGravity:Z
    :cond_106
    const/16 v36, 0x0

    #@108
    goto/16 :goto_69

    #@10a
    .line 424
    .restart local v10       #bottom:I
    .restart local v27       #left:I
    .restart local v30       #offsetHorizontalAxis:Z
    .restart local v31       #offsetVerticalAxis:Z
    .restart local v33       #right:I
    .restart local v35       #top:I
    .restart local v36       #verticalGravity:Z
    :cond_10a
    const/16 v26, 0x0

    #@10c
    goto :goto_94

    #@10d
    .line 425
    .restart local v26       #isWrapContentWidth:Z
    :cond_10d
    const/16 v25, 0x0

    #@10f
    goto :goto_9c

    #@110
    .line 445
    .restart local v11       #child:Landroid/view/View;
    .restart local v15       #childWidthMeasureSpec:I
    .restart local v16       #count:I
    .restart local v23       #i:I
    .restart local v25       #isWrapContentHeight:Z
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v38       #views:[Landroid/view/View;
    .restart local v39       #w:I
    :cond_110
    const/high16 v4, -0x8000

    #@112
    move/from16 v0, v28

    #@114
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@117
    move-result v13

    #@118
    .restart local v13       #childHeightMeasureSpec:I
    goto :goto_eb

    #@119
    .line 453
    .end local v11           #child:Landroid/view/View;
    .end local v13           #childHeightMeasureSpec:I
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_119
    const/4 v4, -0x1

    #@11a
    move/from16 v0, v29

    #@11c
    if-ne v0, v4, :cond_15f

    #@11e
    .line 455
    move/from16 v29, v39

    #@120
    .line 465
    .end local v15           #childWidthMeasureSpec:I
    .end local v23           #i:I
    .end local v39           #w:I
    :cond_120
    :goto_120
    const/16 v23, 0x0

    #@122
    .restart local v23       #i:I
    :goto_122
    move/from16 v0, v23

    #@124
    move/from16 v1, v16

    #@126
    if-ge v0, v1, :cond_16a

    #@128
    .line 466
    aget-object v11, v38, v23

    #@12a
    .line 467
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@12d
    move-result v4

    #@12e
    const/16 v5, 0x8

    #@130
    if-eq v4, v5, :cond_15c

    #@132
    .line 468
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@135
    move-result-object v32

    #@136
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@138
    .line 470
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    #@13a
    move-object/from16 v1, v32

    #@13c
    move/from16 v2, v29

    #@13e
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout;->applyHorizontalSizeRules(Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@141
    .line 471
    move-object/from16 v0, p0

    #@143
    move-object/from16 v1, v32

    #@145
    move/from16 v2, v29

    #@147
    move/from16 v3, v28

    #@149
    invoke-direct {v0, v11, v1, v2, v3}, Landroid/widget/RelativeLayout;->measureChildHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V

    #@14c
    .line 472
    move-object/from16 v0, p0

    #@14e
    move-object/from16 v1, v32

    #@150
    move/from16 v2, v29

    #@152
    move/from16 v3, v26

    #@154
    invoke-direct {v0, v11, v1, v2, v3}, Landroid/widget/RelativeLayout;->positionChildHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z

    #@157
    move-result v4

    #@158
    if-eqz v4, :cond_15c

    #@15a
    .line 473
    const/16 v30, 0x1

    #@15c
    .line 465
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_15c
    add-int/lit8 v23, v23, 0x1

    #@15e
    goto :goto_122

    #@15f
    .line 459
    .end local v11           #child:Landroid/view/View;
    .restart local v15       #childWidthMeasureSpec:I
    .restart local v39       #w:I
    :cond_15f
    if-lez v39, :cond_120

    #@161
    .line 460
    move/from16 v0, v29

    #@163
    move/from16 v1, v39

    #@165
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@168
    move-result v29

    #@169
    goto :goto_120

    #@16a
    .line 478
    .end local v15           #childWidthMeasureSpec:I
    .end local v39           #w:I
    :cond_16a
    move-object/from16 v0, p0

    #@16c
    iget-object v0, v0, Landroid/widget/RelativeLayout;->mSortedVerticalChildren:[Landroid/view/View;

    #@16e
    move-object/from16 v38, v0

    #@170
    .line 479
    move-object/from16 v0, v38

    #@172
    array-length v0, v0

    #@173
    move/from16 v16, v0

    #@175
    .line 481
    const/16 v23, 0x0

    #@177
    :goto_177
    move/from16 v0, v23

    #@179
    move/from16 v1, v16

    #@17b
    if-ge v0, v1, :cond_213

    #@17d
    .line 482
    aget-object v11, v38, v23

    #@17f
    .line 483
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@182
    move-result v4

    #@183
    const/16 v5, 0x8

    #@185
    if-eq v4, v5, :cond_20f

    #@187
    .line 484
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@18a
    move-result-object v32

    #@18b
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@18d
    .line 486
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    #@18f
    move-object/from16 v1, v32

    #@191
    move/from16 v2, v28

    #@193
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout;->applyVerticalSizeRules(Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@196
    .line 487
    move-object/from16 v0, p0

    #@198
    move-object/from16 v1, v32

    #@19a
    move/from16 v2, v29

    #@19c
    move/from16 v3, v28

    #@19e
    invoke-direct {v0, v11, v1, v2, v3}, Landroid/widget/RelativeLayout;->measureChild(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;II)V

    #@1a1
    .line 488
    move-object/from16 v0, p0

    #@1a3
    move-object/from16 v1, v32

    #@1a5
    move/from16 v2, v28

    #@1a7
    move/from16 v3, v25

    #@1a9
    invoke-direct {v0, v11, v1, v2, v3}, Landroid/widget/RelativeLayout;->positionChildVertical(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;IZ)Z

    #@1ac
    move-result v4

    #@1ad
    if-eqz v4, :cond_1b1

    #@1af
    .line 489
    const/16 v31, 0x1

    #@1b1
    .line 492
    :cond_1b1
    if-eqz v26, :cond_1bd

    #@1b3
    .line 493
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1b6
    move-result v4

    #@1b7
    move/from16 v0, v40

    #@1b9
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@1bc
    move-result v40

    #@1bd
    .line 496
    :cond_1bd
    if-eqz v25, :cond_1c9

    #@1bf
    .line 497
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1c2
    move-result v4

    #@1c3
    move/from16 v0, v18

    #@1c5
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@1c8
    move-result v18

    #@1c9
    .line 500
    :cond_1c9
    move-object/from16 v0, v24

    #@1cb
    if-ne v11, v0, :cond_1cf

    #@1cd
    if-eqz v36, :cond_1ed

    #@1cf
    .line 501
    :cond_1cf
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1d2
    move-result v4

    #@1d3
    move-object/from16 v0, v32

    #@1d5
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1d7
    sub-int/2addr v4, v5

    #@1d8
    move/from16 v0, v27

    #@1da
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@1dd
    move-result v27

    #@1de
    .line 502
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1e1
    move-result v4

    #@1e2
    move-object/from16 v0, v32

    #@1e4
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1e6
    sub-int/2addr v4, v5

    #@1e7
    move/from16 v0, v35

    #@1e9
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@1ec
    move-result v35

    #@1ed
    .line 505
    :cond_1ed
    move-object/from16 v0, v24

    #@1ef
    if-ne v11, v0, :cond_1f3

    #@1f1
    if-eqz v21, :cond_20f

    #@1f3
    .line 506
    :cond_1f3
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@1f6
    move-result v4

    #@1f7
    move-object/from16 v0, v32

    #@1f9
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1fb
    add-int/2addr v4, v5

    #@1fc
    move/from16 v0, v33

    #@1fe
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@201
    move-result v33

    #@202
    .line 507
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@205
    move-result v4

    #@206
    move-object/from16 v0, v32

    #@208
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@20a
    add-int/2addr v4, v5

    #@20b
    invoke-static {v10, v4}, Ljava/lang/Math;->max(II)I

    #@20e
    move-result v10

    #@20f
    .line 481
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_20f
    add-int/lit8 v23, v23, 0x1

    #@211
    goto/16 :goto_177

    #@213
    .line 512
    .end local v11           #child:Landroid/view/View;
    :cond_213
    move-object/from16 v0, p0

    #@215
    iget-boolean v4, v0, Landroid/widget/RelativeLayout;->mHasBaselineAlignedChild:Z

    #@217
    if-eqz v4, :cond_287

    #@219
    .line 513
    const/16 v23, 0x0

    #@21b
    :goto_21b
    move/from16 v0, v23

    #@21d
    move/from16 v1, v16

    #@21f
    if-ge v0, v1, :cond_287

    #@221
    .line 514
    move-object/from16 v0, p0

    #@223
    move/from16 v1, v23

    #@225
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@228
    move-result-object v11

    #@229
    .line 515
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@22c
    move-result v4

    #@22d
    const/16 v5, 0x8

    #@22f
    if-eq v4, v5, :cond_284

    #@231
    .line 516
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@234
    move-result-object v32

    #@235
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@237
    .line 517
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    #@239
    move-object/from16 v1, v32

    #@23b
    invoke-direct {v0, v11, v1}, Landroid/widget/RelativeLayout;->alignBaseline(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;)V

    #@23e
    .line 519
    move-object/from16 v0, v24

    #@240
    if-ne v11, v0, :cond_244

    #@242
    if-eqz v36, :cond_262

    #@244
    .line 520
    :cond_244
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@247
    move-result v4

    #@248
    move-object/from16 v0, v32

    #@24a
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@24c
    sub-int/2addr v4, v5

    #@24d
    move/from16 v0, v27

    #@24f
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@252
    move-result v27

    #@253
    .line 521
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@256
    move-result v4

    #@257
    move-object/from16 v0, v32

    #@259
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@25b
    sub-int/2addr v4, v5

    #@25c
    move/from16 v0, v35

    #@25e
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@261
    move-result v35

    #@262
    .line 524
    :cond_262
    move-object/from16 v0, v24

    #@264
    if-ne v11, v0, :cond_268

    #@266
    if-eqz v21, :cond_284

    #@268
    .line 525
    :cond_268
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$100(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@26b
    move-result v4

    #@26c
    move-object/from16 v0, v32

    #@26e
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@270
    add-int/2addr v4, v5

    #@271
    move/from16 v0, v33

    #@273
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@276
    move-result v33

    #@277
    .line 526
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$200(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@27a
    move-result v4

    #@27b
    move-object/from16 v0, v32

    #@27d
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@27f
    add-int/2addr v4, v5

    #@280
    invoke-static {v10, v4}, Ljava/lang/Math;->max(II)I

    #@283
    move-result v10

    #@284
    .line 513
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_284
    add-int/lit8 v23, v23, 0x1

    #@286
    goto :goto_21b

    #@287
    .line 532
    .end local v11           #child:Landroid/view/View;
    :cond_287
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getLayoutDirection()I

    #@28a
    move-result v9

    #@28b
    .line 534
    .local v9, layoutDirection:I
    if-eqz v26, :cond_318

    #@28d
    .line 537
    move-object/from16 v0, p0

    #@28f
    iget v4, v0, Landroid/view/View;->mPaddingRight:I

    #@291
    add-int v40, v40, v4

    #@293
    .line 539
    move-object/from16 v0, p0

    #@295
    iget-object v4, v0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@297
    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@299
    if-ltz v4, :cond_2a7

    #@29b
    .line 540
    move-object/from16 v0, p0

    #@29d
    iget-object v4, v0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@29f
    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2a1
    move/from16 v0, v40

    #@2a3
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@2a6
    move-result v40

    #@2a7
    .line 543
    :cond_2a7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getSuggestedMinimumWidth()I

    #@2aa
    move-result v4

    #@2ab
    move/from16 v0, v40

    #@2ad
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@2b0
    move-result v40

    #@2b1
    .line 544
    move/from16 v0, v40

    #@2b3
    move/from16 v1, p1

    #@2b5
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout;->resolveSize(II)I

    #@2b8
    move-result v40

    #@2b9
    .line 546
    if-eqz v30, :cond_318

    #@2bb
    .line 547
    const/16 v23, 0x0

    #@2bd
    :goto_2bd
    move/from16 v0, v23

    #@2bf
    move/from16 v1, v16

    #@2c1
    if-ge v0, v1, :cond_318

    #@2c3
    .line 548
    move-object/from16 v0, p0

    #@2c5
    move/from16 v1, v23

    #@2c7
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@2ca
    move-result-object v11

    #@2cb
    .line 549
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@2ce
    move-result v4

    #@2cf
    const/16 v5, 0x8

    #@2d1
    if-eq v4, v5, :cond_2f4

    #@2d3
    .line 550
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2d6
    move-result-object v32

    #@2d7
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@2d9
    .line 551
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, v32

    #@2db
    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@2de
    move-result-object v34

    #@2df
    .line 552
    .local v34, rules:[I
    const/16 v4, 0xd

    #@2e1
    aget v4, v34, v4

    #@2e3
    if-nez v4, :cond_2eb

    #@2e5
    const/16 v4, 0xe

    #@2e7
    aget v4, v34, v4

    #@2e9
    if-eqz v4, :cond_2f7

    #@2eb
    .line 553
    :cond_2eb
    move-object/from16 v0, p0

    #@2ed
    move-object/from16 v1, v32

    #@2ef
    move/from16 v2, v40

    #@2f1
    invoke-direct {v0, v11, v1, v2}, Landroid/widget/RelativeLayout;->centerHorizontal(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@2f4
    .line 547
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v34           #rules:[I
    :cond_2f4
    :goto_2f4
    add-int/lit8 v23, v23, 0x1

    #@2f6
    goto :goto_2bd

    #@2f7
    .line 554
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v34       #rules:[I
    :cond_2f7
    const/16 v4, 0xb

    #@2f9
    aget v4, v34, v4

    #@2fb
    if-eqz v4, :cond_2f4

    #@2fd
    .line 555
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    #@300
    move-result v14

    #@301
    .line 556
    .local v14, childWidth:I
    move-object/from16 v0, p0

    #@303
    iget v4, v0, Landroid/view/View;->mPaddingRight:I

    #@305
    sub-int v4, v40, v4

    #@307
    sub-int/2addr v4, v14

    #@308
    move-object/from16 v0, v32

    #@30a
    invoke-static {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@30d
    .line 557
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$300(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@310
    move-result v4

    #@311
    add-int/2addr v4, v14

    #@312
    move-object/from16 v0, v32

    #@314
    invoke-static {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@317
    goto :goto_2f4

    #@318
    .line 564
    .end local v11           #child:Landroid/view/View;
    .end local v14           #childWidth:I
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v34           #rules:[I
    :cond_318
    if-eqz v25, :cond_3a5

    #@31a
    .line 567
    move-object/from16 v0, p0

    #@31c
    iget v4, v0, Landroid/view/View;->mPaddingBottom:I

    #@31e
    add-int v18, v18, v4

    #@320
    .line 569
    move-object/from16 v0, p0

    #@322
    iget-object v4, v0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@324
    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@326
    if-ltz v4, :cond_334

    #@328
    .line 570
    move-object/from16 v0, p0

    #@32a
    iget-object v4, v0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@32c
    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@32e
    move/from16 v0, v18

    #@330
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@333
    move-result v18

    #@334
    .line 573
    :cond_334
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getSuggestedMinimumHeight()I

    #@337
    move-result v4

    #@338
    move/from16 v0, v18

    #@33a
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@33d
    move-result v18

    #@33e
    .line 574
    move/from16 v0, v18

    #@340
    move/from16 v1, p2

    #@342
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout;->resolveSize(II)I

    #@345
    move-result v18

    #@346
    .line 576
    if-eqz v31, :cond_3a5

    #@348
    .line 577
    const/16 v23, 0x0

    #@34a
    :goto_34a
    move/from16 v0, v23

    #@34c
    move/from16 v1, v16

    #@34e
    if-ge v0, v1, :cond_3a5

    #@350
    .line 578
    move-object/from16 v0, p0

    #@352
    move/from16 v1, v23

    #@354
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@357
    move-result-object v11

    #@358
    .line 579
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@35b
    move-result v4

    #@35c
    const/16 v5, 0x8

    #@35e
    if-eq v4, v5, :cond_381

    #@360
    .line 580
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@363
    move-result-object v32

    #@364
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@366
    .line 581
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, v32

    #@368
    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout$LayoutParams;->getRules(I)[I

    #@36b
    move-result-object v34

    #@36c
    .line 582
    .restart local v34       #rules:[I
    const/16 v4, 0xd

    #@36e
    aget v4, v34, v4

    #@370
    if-nez v4, :cond_378

    #@372
    const/16 v4, 0xf

    #@374
    aget v4, v34, v4

    #@376
    if-eqz v4, :cond_384

    #@378
    .line 583
    :cond_378
    move-object/from16 v0, p0

    #@37a
    move-object/from16 v1, v32

    #@37c
    move/from16 v2, v18

    #@37e
    invoke-direct {v0, v11, v1, v2}, Landroid/widget/RelativeLayout;->centerVertical(Landroid/view/View;Landroid/widget/RelativeLayout$LayoutParams;I)V

    #@381
    .line 577
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v34           #rules:[I
    :cond_381
    :goto_381
    add-int/lit8 v23, v23, 0x1

    #@383
    goto :goto_34a

    #@384
    .line 584
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v34       #rules:[I
    :cond_384
    const/16 v4, 0xc

    #@386
    aget v4, v34, v4

    #@388
    if-eqz v4, :cond_381

    #@38a
    .line 585
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    #@38d
    move-result v12

    #@38e
    .line 586
    .local v12, childHeight:I
    move-object/from16 v0, p0

    #@390
    iget v4, v0, Landroid/view/View;->mPaddingBottom:I

    #@392
    sub-int v4, v18, v4

    #@394
    sub-int/2addr v4, v12

    #@395
    move-object/from16 v0, v32

    #@397
    invoke-static {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@39a
    .line 587
    invoke-static/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->access$400(Landroid/widget/RelativeLayout$LayoutParams;)I

    #@39d
    move-result v4

    #@39e
    add-int/2addr v4, v12

    #@39f
    move-object/from16 v0, v32

    #@3a1
    invoke-static {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@3a4
    goto :goto_381

    #@3a5
    .line 594
    .end local v11           #child:Landroid/view/View;
    .end local v12           #childHeight:I
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v34           #rules:[I
    :cond_3a5
    if-nez v21, :cond_3a9

    #@3a7
    if-eqz v36, :cond_428

    #@3a9
    .line 595
    :cond_3a9
    move-object/from16 v0, p0

    #@3ab
    iget-object v7, v0, Landroid/widget/RelativeLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@3ad
    .line 596
    .local v7, selfBounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@3af
    iget v4, v0, Landroid/view/View;->mPaddingLeft:I

    #@3b1
    move-object/from16 v0, p0

    #@3b3
    iget v5, v0, Landroid/view/View;->mPaddingTop:I

    #@3b5
    move-object/from16 v0, p0

    #@3b7
    iget v6, v0, Landroid/view/View;->mPaddingRight:I

    #@3b9
    sub-int v6, v40, v6

    #@3bb
    move-object/from16 v0, p0

    #@3bd
    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    #@3bf
    move/from16 v43, v0

    #@3c1
    sub-int v43, v18, v43

    #@3c3
    move/from16 v0, v43

    #@3c5
    invoke-virtual {v7, v4, v5, v6, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@3c8
    .line 599
    move-object/from16 v0, p0

    #@3ca
    iget-object v8, v0, Landroid/widget/RelativeLayout;->mContentBounds:Landroid/graphics/Rect;

    #@3cc
    .line 600
    .local v8, contentBounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@3ce
    iget v4, v0, Landroid/widget/RelativeLayout;->mGravity:I

    #@3d0
    sub-int v5, v33, v27

    #@3d2
    sub-int v6, v10, v35

    #@3d4
    invoke-static/range {v4 .. v9}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    #@3d7
    .line 603
    iget v4, v8, Landroid/graphics/Rect;->left:I

    #@3d9
    sub-int v22, v4, v27

    #@3db
    .line 604
    .local v22, horizontalOffset:I
    iget v4, v8, Landroid/graphics/Rect;->top:I

    #@3dd
    sub-int v37, v4, v35

    #@3df
    .line 605
    .local v37, verticalOffset:I
    if-nez v22, :cond_3e3

    #@3e1
    if-eqz v37, :cond_428

    #@3e3
    .line 606
    :cond_3e3
    const/16 v23, 0x0

    #@3e5
    :goto_3e5
    move/from16 v0, v23

    #@3e7
    move/from16 v1, v16

    #@3e9
    if-ge v0, v1, :cond_428

    #@3eb
    .line 607
    move-object/from16 v0, p0

    #@3ed
    move/from16 v1, v23

    #@3ef
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    #@3f2
    move-result-object v11

    #@3f3
    .line 608
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@3f6
    move-result v4

    #@3f7
    const/16 v5, 0x8

    #@3f9
    if-eq v4, v5, :cond_425

    #@3fb
    move-object/from16 v0, v24

    #@3fd
    if-eq v11, v0, :cond_425

    #@3ff
    .line 609
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@402
    move-result-object v32

    #@403
    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    #@405
    .line 610
    .restart local v32       #params:Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v21, :cond_415

    #@407
    .line 611
    move-object/from16 v0, v32

    #@409
    move/from16 v1, v22

    #@40b
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$312(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@40e
    .line 612
    move-object/from16 v0, v32

    #@410
    move/from16 v1, v22

    #@412
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$112(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@415
    .line 614
    :cond_415
    if-eqz v36, :cond_425

    #@417
    .line 615
    move-object/from16 v0, v32

    #@419
    move/from16 v1, v37

    #@41b
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$412(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@41e
    .line 616
    move-object/from16 v0, v32

    #@420
    move/from16 v1, v37

    #@422
    invoke-static {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->access$212(Landroid/widget/RelativeLayout$LayoutParams;I)I

    #@425
    .line 606
    .end local v32           #params:Landroid/widget/RelativeLayout$LayoutParams;
    :cond_425
    add-int/lit8 v23, v23, 0x1

    #@427
    goto :goto_3e5

    #@428
    .line 623
    .end local v7           #selfBounds:Landroid/graphics/Rect;
    .end local v8           #contentBounds:Landroid/graphics/Rect;
    .end local v11           #child:Landroid/view/View;
    .end local v22           #horizontalOffset:I
    .end local v37           #verticalOffset:I
    :cond_428
    move-object/from16 v0, p0

    #@42a
    move/from16 v1, v40

    #@42c
    move/from16 v2, v18

    #@42e
    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->setMeasuredDimension(II)V

    #@431
    .line 624
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 328
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@3
    .line 329
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/RelativeLayout;->mDirtyHierarchy:Z

    #@6
    .line 330
    return-void
.end method

.method public setGravity(I)V
    .registers 4
    .parameter "gravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 284
    iget v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@2
    if-eq v0, p1, :cond_2e

    #@4
    .line 285
    const v0, 0x800007

    #@7
    and-int/2addr v0, p1

    #@8
    if-nez v0, :cond_e

    #@a
    .line 286
    const v0, 0x800003

    #@d
    or-int/2addr p1, v0

    #@e
    .line 289
    :cond_e
    and-int/lit8 v0, p1, 0x70

    #@10
    if-nez v0, :cond_14

    #@12
    .line 290
    or-int/lit8 p1, p1, 0x30

    #@14
    .line 292
    :cond_14
    sget-boolean v0, Landroid/widget/RelativeLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@16
    if-eqz v0, :cond_29

    #@18
    .line 293
    iget v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@1a
    and-int/lit8 v0, v0, 0x3

    #@1c
    const/4 v1, 0x3

    #@1d
    if-eq v0, v1, :cond_26

    #@1f
    iget v0, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@21
    and-int/lit8 v0, v0, 0x5

    #@23
    const/4 v1, 0x5

    #@24
    if-ne v0, v1, :cond_29

    #@26
    .line 294
    :cond_26
    const/high16 v0, 0x80

    #@28
    or-int/2addr p1, v0

    #@29
    .line 298
    :cond_29
    iput p1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@2b
    .line 299
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    #@2e
    .line 301
    :cond_2e
    return-void
.end method

.method public setHorizontalGravity(I)V
    .registers 5
    .parameter "horizontalGravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const v2, 0x800007

    #@3
    .line 305
    and-int v0, p1, v2

    #@5
    .line 306
    .local v0, gravity:I
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@7
    and-int/2addr v1, v2

    #@8
    if-eq v1, v0, :cond_16

    #@a
    .line 307
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@c
    const v2, -0x800008

    #@f
    and-int/2addr v1, v2

    #@10
    or-int/2addr v1, v0

    #@11
    iput v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@13
    .line 308
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    #@16
    .line 310
    :cond_16
    return-void
.end method

.method public setIgnoreGravity(I)V
    .registers 2
    .parameter "viewId"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 249
    iput p1, p0, Landroid/widget/RelativeLayout;->mIgnoreGravity:I

    #@2
    .line 250
    return-void
.end method

.method public setVerticalGravity(I)V
    .registers 4
    .parameter "verticalGravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 314
    and-int/lit8 v0, p1, 0x70

    #@2
    .line 315
    .local v0, gravity:I
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@4
    and-int/lit8 v1, v1, 0x70

    #@6
    if-eq v1, v0, :cond_12

    #@8
    .line 316
    iget v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@a
    and-int/lit8 v1, v1, -0x71

    #@c
    or-int/2addr v1, v0

    #@d
    iput v1, p0, Landroid/widget/RelativeLayout;->mGravity:I

    #@f
    .line 317
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    #@12
    .line 319
    :cond_12
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 233
    const/4 v0, 0x0

    #@1
    return v0
.end method
