.class Landroid/widget/RemoteViews$TextViewDrawableAction;
.super Landroid/widget/RemoteViews$Action;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextViewDrawableAction"
.end annotation


# static fields
.field public static final TAG:I = 0xb


# instance fields
.field d1:I

.field d2:I

.field d3:I

.field d4:I

.field isRelative:Z

.field final synthetic this$0:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViews;IZIIII)V
    .registers 9
    .parameter
    .parameter "viewId"
    .parameter "isRelative"
    .parameter "d1"
    .parameter "d2"
    .parameter "d3"
    .parameter "d4"

    #@0
    .prologue
    .line 1267
    iput-object p1, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 1311
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@9
    .line 1268
    iput p2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@b
    .line 1269
    iput-boolean p3, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@d
    .line 1270
    iput p4, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d1:I

    #@f
    .line 1271
    iput p5, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d2:I

    #@11
    .line 1272
    iput p6, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d3:I

    #@13
    .line 1273
    iput p7, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d4:I

    #@15
    .line 1274
    return-void
.end method

.method public constructor <init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V
    .registers 5
    .parameter
    .parameter "parcel"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1276
    iput-object p1, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->this$0:Landroid/widget/RemoteViews;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v1}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@7
    .line 1311
    iput-boolean v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@9
    .line 1277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v1

    #@d
    iput v1, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@f
    .line 1278
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_16

    #@15
    const/4 v0, 0x1

    #@16
    :cond_16
    iput-boolean v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@18
    .line 1279
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d1:I

    #@1e
    .line 1280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d2:I

    #@24
    .line 1281
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d3:I

    #@2a
    .line 1282
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d4:I

    #@30
    .line 1283
    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 10
    .parameter "root"
    .parameter "rootParent"
    .parameter "handler"

    #@0
    .prologue
    .line 1297
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 1298
    .local v0, context:Landroid/content/Context;
    iget v2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@6
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/widget/TextView;

    #@c
    .line 1299
    .local v1, target:Landroid/widget/TextView;
    if-nez v1, :cond_f

    #@e
    .line 1305
    :goto_e
    return-void

    #@f
    .line 1300
    :cond_f
    iget-boolean v2, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@11
    if-eqz v2, :cond_1f

    #@13
    .line 1301
    iget v2, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d1:I

    #@15
    iget v3, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d2:I

    #@17
    iget v4, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d3:I

    #@19
    iget v5, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d4:I

    #@1b
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    #@1e
    goto :goto_e

    #@1f
    .line 1303
    :cond_1f
    iget v2, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d1:I

    #@21
    iget v3, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d2:I

    #@23
    iget v4, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d3:I

    #@25
    iget v5, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d4:I

    #@27
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@2a
    goto :goto_e
.end method

.method public getActionName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1308
    const-string v0, "TextViewDrawableAction"

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 1286
    const/16 v0, 0xb

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 1287
    iget v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 1288
    iget-boolean v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->isRelative:Z

    #@c
    if-eqz v0, :cond_27

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 1289
    iget v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d1:I

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 1290
    iget v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d2:I

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1291
    iget v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d3:I

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 1292
    iget v0, p0, Landroid/widget/RemoteViews$TextViewDrawableAction;->d4:I

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 1293
    return-void

    #@27
    .line 1288
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_f
.end method
