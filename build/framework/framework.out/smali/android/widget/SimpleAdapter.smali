.class public Landroid/widget/SimpleAdapter;
.super Landroid/widget/BaseAdapter;
.source "SimpleAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SimpleAdapter$1;,
        Landroid/widget/SimpleAdapter$SimpleFilter;,
        Landroid/widget/SimpleAdapter$ViewBinder;
    }
.end annotation


# instance fields
.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private mDropDownResource:I

.field private mFilter:Landroid/widget/SimpleAdapter$SimpleFilter;

.field private mFrom:[Ljava/lang/String;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mResource:I

.field private mTo:[I

.field private mUnfilteredData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private mViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V
    .registers 7
    .parameter "context"
    .parameter
    .parameter "resource"
    .parameter "from"
    .parameter "to"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 80
    .local p2, data:Ljava/util/List;,"Ljava/util/List<+Ljava/util/Map<Ljava/lang/String;*>;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@3
    .line 81
    iput-object p2, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@5
    .line 82
    iput p3, p0, Landroid/widget/SimpleAdapter;->mDropDownResource:I

    #@7
    iput p3, p0, Landroid/widget/SimpleAdapter;->mResource:I

    #@9
    .line 83
    iput-object p4, p0, Landroid/widget/SimpleAdapter;->mFrom:[Ljava/lang/String;

    #@b
    .line 84
    iput-object p5, p0, Landroid/widget/SimpleAdapter;->mTo:[I

    #@d
    .line 85
    const-string/jumbo v0, "layout_inflater"

    #@10
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/view/LayoutInflater;

    #@16
    iput-object v0, p0, Landroid/widget/SimpleAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@18
    .line 86
    return-void
.end method

.method static synthetic access$100(Landroid/widget/SimpleAdapter;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mUnfilteredData:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/widget/SimpleAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Landroid/widget/SimpleAdapter;->mUnfilteredData:Ljava/util/ArrayList;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/widget/SimpleAdapter;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Landroid/widget/SimpleAdapter;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Landroid/widget/SimpleAdapter;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mTo:[I

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/SimpleAdapter;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mFrom:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private bindView(ILandroid/view/View;)V
    .registers 16
    .parameter "position"
    .parameter "view"

    #@0
    .prologue
    .line 147
    iget-object v10, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@2
    invoke-interface {v10, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v4

    #@6
    check-cast v4, Ljava/util/Map;

    #@8
    .line 148
    .local v4, dataSet:Ljava/util/Map;
    if-nez v4, :cond_b

    #@a
    .line 201
    :cond_a
    return-void

    #@b
    .line 152
    :cond_b
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    #@d
    .line 153
    .local v0, binder:Landroid/widget/SimpleAdapter$ViewBinder;
    iget-object v5, p0, Landroid/widget/SimpleAdapter;->mFrom:[Ljava/lang/String;

    #@f
    .line 154
    .local v5, from:[Ljava/lang/String;
    iget-object v8, p0, Landroid/widget/SimpleAdapter;->mTo:[I

    #@11
    .line 155
    .local v8, to:[I
    array-length v2, v8

    #@12
    .line 157
    .local v2, count:I
    const/4 v6, 0x0

    #@13
    .local v6, i:I
    :goto_13
    if-ge v6, v2, :cond_a

    #@15
    .line 158
    aget v10, v8, v6

    #@17
    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v9

    #@1b
    .line 159
    .local v9, v:Landroid/view/View;
    if-eqz v9, :cond_47

    #@1d
    .line 160
    aget-object v10, v5, v6

    #@1f
    invoke-interface {v4, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v3

    #@23
    .line 161
    .local v3, data:Ljava/lang/Object;
    if-nez v3, :cond_4a

    #@25
    const-string v7, ""

    #@27
    .line 162
    .local v7, text:Ljava/lang/String;
    :goto_27
    if-nez v7, :cond_2b

    #@29
    .line 163
    const-string v7, ""

    #@2b
    .line 166
    :cond_2b
    const/4 v1, 0x0

    #@2c
    .line 167
    .local v1, bound:Z
    if-eqz v0, :cond_32

    #@2e
    .line 168
    invoke-interface {v0, v9, v3, v7}, Landroid/widget/SimpleAdapter$ViewBinder;->setViewValue(Landroid/view/View;Ljava/lang/Object;Ljava/lang/String;)Z

    #@31
    move-result v1

    #@32
    .line 171
    :cond_32
    if-nez v1, :cond_47

    #@34
    .line 172
    instance-of v10, v9, Landroid/widget/Checkable;

    #@36
    if-eqz v10, :cond_87

    #@38
    .line 173
    instance-of v10, v3, Ljava/lang/Boolean;

    #@3a
    if-eqz v10, :cond_4f

    #@3c
    .line 174
    check-cast v9, Landroid/widget/Checkable;

    #@3e
    .end local v9           #v:Landroid/view/View;
    check-cast v3, Ljava/lang/Boolean;

    #@40
    .end local v3           #data:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    #@43
    move-result v10

    #@44
    invoke-interface {v9, v10}, Landroid/widget/Checkable;->setChecked(Z)V

    #@47
    .line 157
    .end local v1           #bound:Z
    .end local v7           #text:Ljava/lang/String;
    :cond_47
    :goto_47
    add-int/lit8 v6, v6, 0x1

    #@49
    goto :goto_13

    #@4a
    .line 161
    .restart local v3       #data:Ljava/lang/Object;
    .restart local v9       #v:Landroid/view/View;
    :cond_4a
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    goto :goto_27

    #@4f
    .line 175
    .restart local v1       #bound:Z
    .restart local v7       #text:Ljava/lang/String;
    :cond_4f
    instance-of v10, v9, Landroid/widget/TextView;

    #@51
    if-eqz v10, :cond_59

    #@53
    .line 178
    check-cast v9, Landroid/widget/TextView;

    #@55
    .end local v9           #v:Landroid/view/View;
    invoke-virtual {p0, v9, v7}, Landroid/widget/SimpleAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;)V

    #@58
    goto :goto_47

    #@59
    .line 180
    .restart local v9       #v:Landroid/view/View;
    :cond_59
    new-instance v11, Ljava/lang/IllegalStateException;

    #@5b
    new-instance v10, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@63
    move-result-object v12

    #@64
    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@67
    move-result-object v12

    #@68
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v10

    #@6c
    const-string v12, " should be bound to a Boolean, not a "

    #@6e
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v12

    #@72
    if-nez v3, :cond_82

    #@74
    const-string v10, "<unknown type>"

    #@76
    :goto_76
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v10

    #@7a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v10

    #@7e
    invoke-direct {v11, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@81
    throw v11

    #@82
    :cond_82
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@85
    move-result-object v10

    #@86
    goto :goto_76

    #@87
    .line 184
    :cond_87
    instance-of v10, v9, Landroid/widget/TextView;

    #@89
    if-eqz v10, :cond_91

    #@8b
    .line 187
    check-cast v9, Landroid/widget/TextView;

    #@8d
    .end local v9           #v:Landroid/view/View;
    invoke-virtual {p0, v9, v7}, Landroid/widget/SimpleAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;)V

    #@90
    goto :goto_47

    #@91
    .line 188
    .restart local v9       #v:Landroid/view/View;
    :cond_91
    instance-of v10, v9, Landroid/widget/ImageView;

    #@93
    if-eqz v10, :cond_ab

    #@95
    .line 189
    instance-of v10, v3, Ljava/lang/Integer;

    #@97
    if-eqz v10, :cond_a5

    #@99
    .line 190
    check-cast v9, Landroid/widget/ImageView;

    #@9b
    .end local v9           #v:Landroid/view/View;
    check-cast v3, Ljava/lang/Integer;

    #@9d
    .end local v3           #data:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@a0
    move-result v10

    #@a1
    invoke-virtual {p0, v9, v10}, Landroid/widget/SimpleAdapter;->setViewImage(Landroid/widget/ImageView;I)V

    #@a4
    goto :goto_47

    #@a5
    .line 192
    .restart local v3       #data:Ljava/lang/Object;
    .restart local v9       #v:Landroid/view/View;
    :cond_a5
    check-cast v9, Landroid/widget/ImageView;

    #@a7
    .end local v9           #v:Landroid/view/View;
    invoke-virtual {p0, v9, v7}, Landroid/widget/SimpleAdapter;->setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    #@aa
    goto :goto_47

    #@ab
    .line 195
    .restart local v9       #v:Landroid/view/View;
    :cond_ab
    new-instance v10, Ljava/lang/IllegalStateException;

    #@ad
    new-instance v11, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b5
    move-result-object v12

    #@b6
    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b9
    move-result-object v12

    #@ba
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v11

    #@be
    const-string v12, " is not a "

    #@c0
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v11

    #@c4
    const-string v12, " view that can be bounds by this SimpleAdapter"

    #@c6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v11

    #@ca
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v11

    #@ce
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d1
    throw v10
.end method

.method private createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .registers 8
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "resource"

    #@0
    .prologue
    .line 120
    if-nez p2, :cond_d

    #@2
    .line 121
    iget-object v1, p0, Landroid/widget/SimpleAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, p4, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    .line 126
    .local v0, v:Landroid/view/View;
    :goto_9
    invoke-direct {p0, p1, v0}, Landroid/widget/SimpleAdapter;->bindView(ILandroid/view/View;)V

    #@c
    .line 128
    return-object v0

    #@d
    .line 123
    .end local v0           #v:Landroid/view/View;
    :cond_d
    move-object v0, p2

    #@e
    .restart local v0       #v:Landroid/view/View;
    goto :goto_9
.end method


# virtual methods
.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 143
    iget v0, p0, Landroid/widget/SimpleAdapter;->mDropDownResource:I

    #@2
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/SimpleAdapter;->createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 3

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mFilter:Landroid/widget/SimpleAdapter$SimpleFilter;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 282
    new-instance v0, Landroid/widget/SimpleAdapter$SimpleFilter;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/widget/SimpleAdapter$SimpleFilter;-><init>(Landroid/widget/SimpleAdapter;Landroid/widget/SimpleAdapter$1;)V

    #@a
    iput-object v0, p0, Landroid/widget/SimpleAdapter;->mFilter:Landroid/widget/SimpleAdapter$SimpleFilter;

    #@c
    .line 284
    :cond_c
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mFilter:Landroid/widget/SimpleAdapter$SimpleFilter;

    #@e
    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mData:Ljava/util/List;

    #@2
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 107
    int-to-long v0, p1

    #@1
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/widget/SimpleAdapter;->mResource:I

    #@2
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/SimpleAdapter;->createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getViewBinder()Landroid/widget/SimpleAdapter$ViewBinder;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/widget/SimpleAdapter;->mViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    #@2
    return-object v0
.end method

.method public setDropDownViewResource(I)V
    .registers 2
    .parameter "resource"

    #@0
    .prologue
    .line 138
    iput p1, p0, Landroid/widget/SimpleAdapter;->mDropDownResource:I

    #@2
    .line 139
    return-void
.end method

.method public setViewBinder(Landroid/widget/SimpleAdapter$ViewBinder;)V
    .registers 2
    .parameter "viewBinder"

    #@0
    .prologue
    .line 223
    iput-object p1, p0, Landroid/widget/SimpleAdapter;->mViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    #@2
    .line 224
    return-void
.end method

.method public setViewImage(Landroid/widget/ImageView;I)V
    .registers 3
    .parameter "v"
    .parameter "value"

    #@0
    .prologue
    .line 240
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    #@3
    .line 241
    return-void
.end method

.method public setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .registers 5
    .parameter "v"
    .parameter "value"

    #@0
    .prologue
    .line 262
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 266
    :goto_7
    return-void

    #@8
    .line 263
    :catch_8
    move-exception v0

    #@9
    .line 264
    .local v0, nfe:Ljava/lang/NumberFormatException;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    #@10
    goto :goto_7
.end method

.method public setViewText(Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 3
    .parameter "v"
    .parameter "text"

    #@0
    .prologue
    .line 277
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@3
    .line 278
    return-void
.end method
