.class final Landroid/widget/AbsListView$CheckForTap;
.super Ljava/lang/Object;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CheckForTap"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2985
    iput-object p1, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    .line 2987
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@4
    iget v4, v4, Landroid/widget/AbsListView;->mTouchMode:I

    #@6
    if-nez v4, :cond_9a

    #@8
    .line 2988
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@a
    iput v7, v4, Landroid/widget/AbsListView;->mTouchMode:I

    #@c
    .line 2989
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@e
    iget-object v5, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@10
    iget v5, v5, Landroid/widget/AbsListView;->mMotionPosition:I

    #@12
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@14
    iget v6, v6, Landroid/widget/AdapterView;->mFirstPosition:I

    #@16
    sub-int/2addr v5, v6

    #@17
    invoke-virtual {v4, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1a
    move-result-object v0

    #@1b
    .line 2990
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_9a

    #@1d
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    #@20
    move-result v4

    #@21
    if-nez v4, :cond_9a

    #@23
    .line 2991
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@25
    const/4 v5, 0x0

    #@26
    iput v5, v4, Landroid/widget/AbsListView;->mLayoutMode:I

    #@28
    .line 2993
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@2a
    iget-boolean v4, v4, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2c
    if-nez v4, :cond_a6

    #@2e
    .line 2994
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    #@31
    .line 2995
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@33
    invoke-virtual {v4, v7}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@36
    .line 2996
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@38
    invoke-virtual {v4}, Landroid/widget/AbsListView;->layoutChildren()V

    #@3b
    .line 2997
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@3d
    iget-object v5, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@3f
    iget v5, v5, Landroid/widget/AbsListView;->mMotionPosition:I

    #@41
    invoke-virtual {v4, v5, v0}, Landroid/widget/AbsListView;->positionSelector(ILandroid/view/View;)V

    #@44
    .line 2998
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@46
    invoke-virtual {v4}, Landroid/widget/AbsListView;->refreshDrawableState()V

    #@49
    .line 3000
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@4c
    move-result v3

    #@4d
    .line 3001
    .local v3, longPressTimeout:I
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@4f
    invoke-virtual {v4}, Landroid/widget/AbsListView;->isLongClickable()Z

    #@52
    move-result v2

    #@53
    .line 3003
    .local v2, longClickable:Z
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@55
    iget-object v4, v4, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@57
    if-eqz v4, :cond_6e

    #@59
    .line 3004
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@5b
    iget-object v4, v4, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@5d
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    #@60
    move-result-object v1

    #@61
    .line 3005
    .local v1, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_6e

    #@63
    instance-of v4, v1, Landroid/graphics/drawable/TransitionDrawable;

    #@65
    if-eqz v4, :cond_6e

    #@67
    .line 3006
    if-eqz v2, :cond_9b

    #@69
    .line 3007
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    #@6b
    .end local v1           #d:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    #@6e
    .line 3014
    :cond_6e
    :goto_6e
    if-eqz v2, :cond_a1

    #@70
    .line 3015
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@72
    invoke-static {v4}, Landroid/widget/AbsListView;->access$500(Landroid/widget/AbsListView;)Landroid/widget/AbsListView$CheckForLongPress;

    #@75
    move-result-object v4

    #@76
    if-nez v4, :cond_85

    #@78
    .line 3016
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@7a
    new-instance v5, Landroid/widget/AbsListView$CheckForLongPress;

    #@7c
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@7e
    const/4 v7, 0x0

    #@7f
    invoke-direct {v5, v6, v7}, Landroid/widget/AbsListView$CheckForLongPress;-><init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V

    #@82
    invoke-static {v4, v5}, Landroid/widget/AbsListView;->access$502(Landroid/widget/AbsListView;Landroid/widget/AbsListView$CheckForLongPress;)Landroid/widget/AbsListView$CheckForLongPress;

    #@85
    .line 3018
    :cond_85
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@87
    invoke-static {v4}, Landroid/widget/AbsListView;->access$500(Landroid/widget/AbsListView;)Landroid/widget/AbsListView$CheckForLongPress;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4}, Landroid/widget/AbsListView$CheckForLongPress;->rememberWindowAttachCount()V

    #@8e
    .line 3019
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@90
    iget-object v5, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@92
    invoke-static {v5}, Landroid/widget/AbsListView;->access$500(Landroid/widget/AbsListView;)Landroid/widget/AbsListView$CheckForLongPress;

    #@95
    move-result-object v5

    #@96
    int-to-long v6, v3

    #@97
    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@9a
    .line 3028
    .end local v0           #child:Landroid/view/View;
    .end local v2           #longClickable:Z
    .end local v3           #longPressTimeout:I
    :cond_9a
    :goto_9a
    return-void

    #@9b
    .line 3009
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #d:Landroid/graphics/drawable/Drawable;
    .restart local v2       #longClickable:Z
    .restart local v3       #longPressTimeout:I
    :cond_9b
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    #@9d
    .end local v1           #d:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    #@a0
    goto :goto_6e

    #@a1
    .line 3021
    :cond_a1
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@a3
    iput v8, v4, Landroid/widget/AbsListView;->mTouchMode:I

    #@a5
    goto :goto_9a

    #@a6
    .line 3024
    .end local v2           #longClickable:Z
    .end local v3           #longPressTimeout:I
    :cond_a6
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForTap;->this$0:Landroid/widget/AbsListView;

    #@a8
    iput v8, v4, Landroid/widget/AbsListView;->mTouchMode:I

    #@aa
    goto :goto_9a
.end method
