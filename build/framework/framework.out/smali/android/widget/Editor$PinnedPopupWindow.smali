.class abstract Landroid/widget/Editor$PinnedPopupWindow;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Landroid/widget/Editor$TextViewPositionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "PinnedPopupWindow"
.end annotation


# instance fields
.field protected mContentView:Landroid/view/ViewGroup;

.field protected mPopupWindow:Landroid/widget/PopupWindow;

.field mPositionX:I

.field mPositionY:I

.field protected mStatusBarHeight:I

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;)V
    .registers 6
    .parameter

    #@0
    .prologue
    const/4 v3, -0x2

    #@1
    .line 2545
    iput-object p1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 2546
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->createPopupWindow()V

    #@9
    .line 2549
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@b
    if-eqz v1, :cond_20

    #@d
    .line 2550
    invoke-static {p1}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_3f

    #@13
    .line 2551
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@15
    const/16 v2, 0x7d2

    #@17
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@1a
    .line 2552
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@1c
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBaseAppType(I)V

    #@20
    .line 2560
    :cond_20
    :goto_20
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@22
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@25
    .line 2561
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@27
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@2a
    .line 2563
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->initContentView()V

    #@2d
    .line 2565
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@2f
    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@32
    .line 2567
    .local v0, wrapContent:Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@34
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@37
    .line 2569
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@39
    iget-object v2, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@3b
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@3e
    .line 2570
    return-void

    #@3f
    .line 2553
    .end local v0           #wrapContent:Landroid/view/ViewGroup$LayoutParams;
    :cond_3f
    instance-of v1, p0, Landroid/widget/Editor$EasyEditPopupWindow;

    #@41
    if-eqz v1, :cond_4b

    #@43
    .line 2554
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@45
    const/16 v2, 0x3ea

    #@47
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@4a
    goto :goto_20

    #@4b
    .line 2556
    :cond_4b
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@4d
    const/16 v2, 0x3e8

    #@4f
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@52
    goto :goto_20
.end method

.method private computeLocalPosition()V
    .registers 7

    #@0
    .prologue
    .line 2595
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->measureContent()V

    #@3
    .line 2596
    iget-object v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    #@8
    move-result v2

    #@9
    .line 2597
    .local v2, width:I
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->getTextOffset()I

    #@c
    move-result v1

    #@d
    .line 2598
    .local v1, offset:I
    iget-object v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@f
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@1a
    move-result v3

    #@1b
    int-to-float v4, v2

    #@1c
    const/high16 v5, 0x4000

    #@1e
    div-float/2addr v4, v5

    #@1f
    sub-float/2addr v3, v4

    #@20
    float-to-int v3, v3

    #@21
    iput v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@23
    .line 2599
    iget v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@25
    iget-object v4, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@27
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@2e
    move-result v4

    #@2f
    add-int/2addr v3, v4

    #@30
    iput v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@32
    .line 2601
    iget-object v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@34
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@3f
    move-result v0

    #@40
    .line 2602
    .local v0, line:I
    invoke-virtual {p0, v0}, Landroid/widget/Editor$PinnedPopupWindow;->getVerticalLocalPosition(I)I

    #@43
    move-result v3

    #@44
    iput v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionY:I

    #@46
    .line 2603
    iget v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionY:I

    #@48
    iget-object v4, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@4a
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v4}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@51
    move-result v4

    #@52
    add-int/2addr v3, v4

    #@53
    iput v3, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionY:I

    #@55
    .line 2604
    return-void
.end method

.method private updatePosition(II)V
    .registers 20
    .parameter "parentPositionX"
    .parameter "parentPositionY"

    #@0
    .prologue
    .line 2607
    move-object/from16 v0, p0

    #@2
    iget v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@4
    add-int v6, p1, v14

    #@6
    .line 2608
    .local v6, positionX:I
    move-object/from16 v0, p0

    #@8
    iget v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionY:I

    #@a
    add-int v7, p2, v14

    #@c
    .line 2610
    .local v7, positionY:I
    move-object/from16 v0, p0

    #@e
    invoke-virtual {v0, v7}, Landroid/widget/Editor$PinnedPopupWindow;->clipVertically(I)I

    #@11
    move-result v7

    #@12
    .line 2612
    sget-boolean v14, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@14
    if-eqz v14, :cond_10d

    #@16
    move-object/from16 v0, p0

    #@18
    instance-of v14, v0, Landroid/widget/Editor$ActionPopupWindow;

    #@1a
    if-eqz v14, :cond_10d

    #@1c
    .line 2613
    move-object/from16 v0, p0

    #@1e
    move/from16 v1, p2

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/Editor$PinnedPopupWindow;->clipVertically(I)I

    #@23
    move-result v7

    #@24
    .line 2619
    :goto_24
    move-object/from16 v0, p0

    #@26
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@28
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2b
    move-result-object v14

    #@2c
    invoke-virtual {v14}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@2f
    move-result-object v14

    #@30
    invoke-virtual {v14}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@33
    move-result-object v2

    #@34
    .line 2620
    .local v2, displayMetrics:Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    #@36
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@38
    invoke-virtual {v14}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    #@3b
    move-result v11

    #@3c
    .line 2621
    .local v11, width:I
    iget v14, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@3e
    sub-int/2addr v14, v11

    #@3f
    invoke-static {v14, v6}, Ljava/lang/Math;->min(II)I

    #@42
    move-result v6

    #@43
    .line 2622
    const/4 v14, 0x0

    #@44
    invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I

    #@47
    move-result v6

    #@48
    .line 2624
    sget-boolean v14, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4a
    if-eqz v14, :cond_fa

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@50
    invoke-static {v14}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@53
    move-result v14

    #@54
    if-eqz v14, :cond_fa

    #@56
    move-object/from16 v0, p0

    #@58
    instance-of v14, v0, Landroid/widget/Editor$ActionPopupWindow;

    #@5a
    if-eqz v14, :cond_fa

    #@5c
    .line 2625
    move-object/from16 v0, p0

    #@5e
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@60
    if-eqz v14, :cond_71

    #@62
    .line 2626
    move-object/from16 v0, p0

    #@64
    instance-of v14, v0, Landroid/widget/Editor$EasyEditPopupWindow;

    #@66
    if-eqz v14, :cond_115

    #@68
    .line 2627
    move-object/from16 v0, p0

    #@6a
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@6c
    const/16 v15, 0x3ea

    #@6e
    invoke-virtual {v14, v15}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@71
    .line 2633
    :cond_71
    :goto_71
    const/4 v14, 0x2

    #@72
    new-array v8, v14, [I

    #@74
    .line 2634
    .local v8, screenLocation:[I
    move-object/from16 v0, p0

    #@76
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@78
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7b
    move-result-object v14

    #@7c
    invoke-virtual {v14, v8}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    #@7f
    .line 2635
    const/4 v14, 0x2

    #@80
    new-array v3, v14, [I

    #@82
    .line 2636
    .local v3, drawingLocation:[I
    move-object/from16 v0, p0

    #@84
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@86
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@89
    move-result-object v14

    #@8a
    invoke-virtual {v14, v3}, Landroid/widget/TextView;->getLocationInWindow([I)V

    #@8d
    .line 2637
    const/4 v14, 0x0

    #@8e
    aget v14, v8, v14

    #@90
    const/4 v15, 0x0

    #@91
    aget v15, v3, v15

    #@93
    sub-int v12, v14, v15

    #@95
    .line 2638
    .local v12, winX:I
    const/4 v14, 0x1

    #@96
    aget v14, v8, v14

    #@98
    const/4 v15, 0x1

    #@99
    aget v15, v3, v15

    #@9b
    sub-int v13, v14, v15

    #@9d
    .line 2639
    .local v13, winY:I
    move-object/from16 v0, p0

    #@9f
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@a1
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@a4
    move-result-object v14

    #@a5
    invoke-virtual {v14}, Landroid/widget/TextView;->getSelectionEnd()I

    #@a8
    move-result v4

    #@a9
    .line 2640
    .local v4, offsetEnd:I
    move-object/from16 v0, p0

    #@ab
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@ad
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@b0
    move-result-object v14

    #@b1
    invoke-virtual {v14}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@b4
    move-result-object v14

    #@b5
    invoke-virtual {v14, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@b8
    move-result v14

    #@b9
    float-to-int v14, v14

    #@ba
    move-object/from16 v0, p0

    #@bc
    iget-object v15, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@be
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c1
    move-result-object v15

    #@c2
    invoke-virtual {v15}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@c5
    move-result v15

    #@c6
    add-int v5, v14, v15

    #@c8
    .line 2642
    .local v5, offsetPosition:I
    move-object/from16 v0, p0

    #@ca
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@cc
    invoke-static {v14}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@cf
    move-result-object v14

    #@d0
    invoke-virtual {v14}, Landroid/widget/TextView;->getWidth()I

    #@d3
    move-result v10

    #@d4
    .line 2643
    .local v10, viewWidth:I
    const/high16 v9, 0x3f80

    #@d6
    .line 2644
    .local v9, sf:F
    int-to-float v14, v5

    #@d7
    int-to-float v15, v10

    #@d8
    const v16, 0x3ea8f5c3

    #@db
    mul-float v15, v15, v16

    #@dd
    cmpg-float v14, v14, v15

    #@df
    if-gez v14, :cond_128

    #@e1
    const/high16 v9, 0x3e80

    #@e3
    .line 2647
    :goto_e3
    int-to-float v14, v5

    #@e4
    int-to-float v15, v11

    #@e5
    mul-float/2addr v15, v9

    #@e6
    sub-float/2addr v14, v15

    #@e7
    float-to-int v14, v14

    #@e8
    move-object/from16 v0, p0

    #@ea
    iput v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@ec
    .line 2648
    move-object/from16 v0, p0

    #@ee
    iget v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPositionX:I

    #@f0
    add-int v6, p1, v14

    #@f2
    .line 2649
    add-int/2addr v6, v12

    #@f3
    .line 2650
    move-object/from16 v0, p0

    #@f5
    iget v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mStatusBarHeight:I

    #@f7
    sub-int v14, v13, v14

    #@f9
    add-int/2addr v7, v14

    #@fa
    .line 2654
    .end local v3           #drawingLocation:[I
    .end local v4           #offsetEnd:I
    .end local v5           #offsetPosition:I
    .end local v8           #screenLocation:[I
    .end local v9           #sf:F
    .end local v10           #viewWidth:I
    .end local v12           #winX:I
    .end local v13           #winY:I
    :cond_fa
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor$PinnedPopupWindow;->isShowing()Z

    #@fd
    move-result v14

    #@fe
    if-eqz v14, :cond_139

    #@100
    .line 2655
    move-object/from16 v0, p0

    #@102
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@104
    const/4 v15, -0x1

    #@105
    const/16 v16, -0x1

    #@107
    move/from16 v0, v16

    #@109
    invoke-virtual {v14, v6, v7, v15, v0}, Landroid/widget/PopupWindow;->update(IIII)V

    #@10c
    .line 2660
    :goto_10c
    return-void

    #@10d
    .line 2615
    .end local v2           #displayMetrics:Landroid/util/DisplayMetrics;
    .end local v11           #width:I
    :cond_10d
    move-object/from16 v0, p0

    #@10f
    invoke-virtual {v0, v7}, Landroid/widget/Editor$PinnedPopupWindow;->clipVertically(I)I

    #@112
    move-result v7

    #@113
    goto/16 :goto_24

    #@115
    .line 2629
    .restart local v2       #displayMetrics:Landroid/util/DisplayMetrics;
    .restart local v11       #width:I
    :cond_115
    move-object/from16 v0, p0

    #@117
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@119
    const/16 v15, 0x7d2

    #@11b
    invoke-virtual {v14, v15}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@11e
    .line 2630
    move-object/from16 v0, p0

    #@120
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@122
    const/4 v15, 0x1

    #@123
    invoke-virtual {v14, v15}, Landroid/widget/PopupWindow;->setBaseAppType(I)V

    #@126
    goto/16 :goto_71

    #@128
    .line 2645
    .restart local v3       #drawingLocation:[I
    .restart local v4       #offsetEnd:I
    .restart local v5       #offsetPosition:I
    .restart local v8       #screenLocation:[I
    .restart local v9       #sf:F
    .restart local v10       #viewWidth:I
    .restart local v12       #winX:I
    .restart local v13       #winY:I
    :cond_128
    int-to-float v14, v5

    #@129
    int-to-float v15, v10

    #@12a
    const v16, 0x3f28f5c3

    #@12d
    mul-float v15, v15, v16

    #@12f
    cmpl-float v14, v14, v15

    #@131
    if-lez v14, :cond_136

    #@133
    const/high16 v9, 0x3f40

    #@135
    goto :goto_e3

    #@136
    .line 2646
    :cond_136
    const/high16 v9, 0x3f00

    #@138
    goto :goto_e3

    #@139
    .line 2657
    .end local v3           #drawingLocation:[I
    .end local v4           #offsetEnd:I
    .end local v5           #offsetPosition:I
    .end local v8           #screenLocation:[I
    .end local v9           #sf:F
    .end local v10           #viewWidth:I
    .end local v12           #winX:I
    .end local v13           #winY:I
    :cond_139
    move-object/from16 v0, p0

    #@13b
    iget-object v14, v0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@13d
    move-object/from16 v0, p0

    #@13f
    iget-object v15, v0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@141
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@144
    move-result-object v15

    #@145
    const/16 v16, 0x0

    #@147
    move/from16 v0, v16

    #@149
    invoke-virtual {v14, v15, v0, v6, v7}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@14c
    goto :goto_10c
.end method


# virtual methods
.method protected abstract clipVertically(I)I
.end method

.method protected abstract createPopupWindow()V
.end method

.method protected abstract getTextOffset()I
.end method

.method protected abstract getVerticalLocalPosition(I)I
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    .line 2663
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@5
    .line 2664
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@7
    invoke-static {v0}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Landroid/widget/Editor$PositionListener;->removeSubscriber(Landroid/widget/Editor$TextViewPositionListener;)V

    #@e
    .line 2665
    return-void
.end method

.method protected abstract initContentView()V
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 2693
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected measureContent()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, -0x8000

    #@2
    .line 2582
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@f
    move-result-object v0

    #@10
    .line 2583
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@12
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@14
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@17
    move-result v2

    #@18
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@1a
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1d
    move-result v3

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->measure(II)V

    #@21
    .line 2588
    return-void
.end method

.method public show()V
    .registers 4

    #@0
    .prologue
    .line 2573
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v1}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, p0, v2}, Landroid/widget/Editor$PositionListener;->addSubscriber(Landroid/widget/Editor$TextViewPositionListener;Z)V

    #@a
    .line 2575
    invoke-direct {p0}, Landroid/widget/Editor$PinnedPopupWindow;->computeLocalPosition()V

    #@d
    .line 2577
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@f
    invoke-static {v1}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@12
    move-result-object v0

    #@13
    .line 2578
    .local v0, positionListener:Landroid/widget/Editor$PositionListener;
    invoke-virtual {v0}, Landroid/widget/Editor$PositionListener;->getPositionX()I

    #@16
    move-result v1

    #@17
    invoke-virtual {v0}, Landroid/widget/Editor$PositionListener;->getPositionY()I

    #@1a
    move-result v2

    #@1b
    invoke-direct {p0, v1, v2}, Landroid/widget/Editor$PinnedPopupWindow;->updatePosition(II)V

    #@1e
    .line 2579
    return-void
.end method

.method public updatePosition(IIZZ)V
    .registers 7
    .parameter "parentPositionX"
    .parameter "parentPositionY"
    .parameter "parentPositionChanged"
    .parameter "parentScrolled"

    #@0
    .prologue
    .line 2671
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-nez v0, :cond_23

    #@4
    .line 2672
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->isShowing()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1f

    #@a
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@c
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->getTextOffset()I

    #@f
    move-result v1

    #@10
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1f

    #@16
    .line 2673
    if-eqz p4, :cond_1b

    #@18
    invoke-direct {p0}, Landroid/widget/Editor$PinnedPopupWindow;->computeLocalPosition()V

    #@1b
    .line 2674
    :cond_1b
    invoke-direct {p0, p1, p2}, Landroid/widget/Editor$PinnedPopupWindow;->updatePosition(II)V

    #@1e
    .line 2690
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 2676
    :cond_1f
    invoke-virtual {p0}, Landroid/widget/Editor$PinnedPopupWindow;->hide()V

    #@22
    goto :goto_1e

    #@23
    .line 2679
    :cond_23
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@25
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@27
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionStart()I

    #@2e
    move-result v1

    #@2f
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@32
    move-result v0

    #@33
    if-nez v0, :cond_47

    #@35
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@37
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@39
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@40
    move-result v1

    #@41
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_52

    #@47
    .line 2680
    :cond_47
    if-nez p4, :cond_4b

    #@49
    if-eqz p3, :cond_4e

    #@4b
    :cond_4b
    invoke-direct {p0}, Landroid/widget/Editor$PinnedPopupWindow;->computeLocalPosition()V

    #@4e
    .line 2681
    :cond_4e
    invoke-direct {p0, p1, p2}, Landroid/widget/Editor$PinnedPopupWindow;->updatePosition(II)V

    #@51
    goto :goto_1e

    #@52
    .line 2684
    :cond_52
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@54
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@57
    .line 2685
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@59
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    #@60
    move-result v0

    #@61
    if-nez v0, :cond_1e

    #@63
    .line 2686
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->this$0:Landroid/widget/Editor;

    #@65
    invoke-static {v0}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@68
    move-result-object v0

    #@69
    invoke-virtual {v0, p0}, Landroid/widget/Editor$PositionListener;->removeSubscriber(Landroid/widget/Editor$TextViewPositionListener;)V

    #@6c
    goto :goto_1e
.end method
