.class Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
.super Ljava/lang/Object;
.source "RemoteViewsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViewsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RemoteViewsIndexMetaData"
.end annotation


# instance fields
.field itemId:J

.field typeId:I


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViews;J)V
    .registers 4
    .parameter "v"
    .parameter "itemId"

    #@0
    .prologue
    .line 532
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 533
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->set(Landroid/widget/RemoteViews;J)V

    #@6
    .line 534
    return-void
.end method


# virtual methods
.method public set(Landroid/widget/RemoteViews;J)V
    .registers 5
    .parameter "v"
    .parameter "id"

    #@0
    .prologue
    .line 537
    iput-wide p2, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->itemId:J

    #@2
    .line 538
    if-eqz p1, :cond_b

    #@4
    .line 539
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->typeId:I

    #@a
    .line 543
    :goto_a
    return-void

    #@b
    .line 541
    :cond_b
    const/4 v0, 0x0

    #@c
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->typeId:I

    #@e
    goto :goto_a
.end method
