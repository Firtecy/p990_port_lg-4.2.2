.class public Landroid/widget/ListPopupWindow;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ListPopupWindow$PopupScrollListener;,
        Landroid/widget/ListPopupWindow$PopupTouchInterceptor;,
        Landroid/widget/ListPopupWindow$ResizePopupRunnable;,
        Landroid/widget/ListPopupWindow$ListSelectorHider;,
        Landroid/widget/ListPopupWindow$PopupDataSetObserver;,
        Landroid/widget/ListPopupWindow$DropDownListView;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final EXPAND_LIST_TIMEOUT:I = 0xfa

.field public static final INPUT_METHOD_FROM_FOCUSABLE:I = 0x0

.field public static final INPUT_METHOD_NEEDED:I = 0x1

.field public static final INPUT_METHOD_NOT_NEEDED:I = 0x2

.field public static final MATCH_PARENT:I = -0x1

.field public static final POSITION_PROMPT_ABOVE:I = 0x0

.field public static final POSITION_PROMPT_BELOW:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ListPopupWindow"

.field public static final WRAP_CONTENT:I = -0x2


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mContext:Landroid/content/Context;

.field private mDropDownAlwaysVisible:Z

.field private mDropDownAnchorView:Landroid/view/View;

.field private mDropDownHeight:I

.field private mDropDownHorizontalOffset:I

.field private mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

.field private mDropDownListHighlight:Landroid/graphics/drawable/Drawable;

.field private mDropDownVerticalOffset:I

.field private mDropDownVerticalOffsetSet:Z

.field private mDropDownWidth:I

.field private mForceIgnoreOutsideTouch:Z

.field private mHandler:Landroid/os/Handler;

.field private final mHideSelector:Landroid/widget/ListPopupWindow$ListSelectorHider;

.field private mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mLayoutDirection:I

.field mListItemExpandMaximum:I

.field private mModal:Z

.field private mObserver:Landroid/database/DataSetObserver;

.field private mPopup:Landroid/widget/PopupWindow;

.field private mPromptPosition:I

.field private mPromptView:Landroid/view/View;

.field private final mResizePopupRunnable:Landroid/widget/ListPopupWindow$ResizePopupRunnable;

.field private final mScrollListener:Landroid/widget/ListPopupWindow$PopupScrollListener;

.field private mShowDropDownRunnable:Ljava/lang/Runnable;

.field private mTempRect:Landroid/graphics/Rect;

.field private final mTouchInterceptor:Landroid/widget/ListPopupWindow$PopupTouchInterceptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 162
    const/4 v0, 0x0

    #@1
    const v1, 0x10102ff

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@8
    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 173
    const v0, 0x10102ff

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@7
    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@4
    .line 186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"
    .parameter "defStyleRes"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 197
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 64
    iput v1, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@8
    .line 65
    iput v1, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@a
    .line 70
    iput-boolean v3, p0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@c
    .line 71
    iput-boolean v3, p0, Landroid/widget/ListPopupWindow;->mForceIgnoreOutsideTouch:Z

    #@e
    .line 72
    const v1, 0x7fffffff

    #@11
    iput v1, p0, Landroid/widget/ListPopupWindow;->mListItemExpandMaximum:I

    #@13
    .line 75
    iput v3, p0, Landroid/widget/ListPopupWindow;->mPromptPosition:I

    #@15
    .line 86
    new-instance v1, Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@17
    invoke-direct {v1, p0, v2}, Landroid/widget/ListPopupWindow$ResizePopupRunnable;-><init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V

    #@1a
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mResizePopupRunnable:Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@1c
    .line 87
    new-instance v1, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;

    #@1e
    invoke-direct {v1, p0, v2}, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;-><init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V

    #@21
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mTouchInterceptor:Landroid/widget/ListPopupWindow$PopupTouchInterceptor;

    #@23
    .line 88
    new-instance v1, Landroid/widget/ListPopupWindow$PopupScrollListener;

    #@25
    invoke-direct {v1, p0, v2}, Landroid/widget/ListPopupWindow$PopupScrollListener;-><init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V

    #@28
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mScrollListener:Landroid/widget/ListPopupWindow$PopupScrollListener;

    #@2a
    .line 89
    new-instance v1, Landroid/widget/ListPopupWindow$ListSelectorHider;

    #@2c
    invoke-direct {v1, p0, v2}, Landroid/widget/ListPopupWindow$ListSelectorHider;-><init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V

    #@2f
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mHideSelector:Landroid/widget/ListPopupWindow$ListSelectorHider;

    #@31
    .line 92
    new-instance v1, Landroid/os/Handler;

    #@33
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@36
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;

    #@38
    .line 94
    new-instance v1, Landroid/graphics/Rect;

    #@3a
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@3d
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@3f
    .line 198
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    #@41
    .line 199
    new-instance v1, Landroid/widget/PopupWindow;

    #@43
    invoke-direct {v1, p1, p2, p3, p4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@46
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@48
    .line 200
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@4a
    const/4 v2, 0x1

    #@4b
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@4e
    .line 202
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    #@50
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@57
    move-result-object v1

    #@58
    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5a
    .line 203
    .local v0, locale:Ljava/util/Locale;
    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    #@5d
    move-result v1

    #@5e
    iput v1, p0, Landroid/widget/ListPopupWindow;->mLayoutDirection:I

    #@60
    .line 204
    return-void
.end method

.method static synthetic access$600(Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow$DropDownListView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/ListPopupWindow;)Landroid/widget/PopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow$ResizePopupRunnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mResizePopupRunnable:Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/ListPopupWindow;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private buildDropDown()I
    .registers 22

    #@0
    .prologue
    .line 973
    const/16 v17, 0x0

    #@2
    .line 975
    .local v17, otherHeights:I
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@6
    if-nez v1, :cond_161

    #@8
    .line 976
    move-object/from16 v0, p0

    #@a
    iget-object v8, v0, Landroid/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    #@c
    .line 984
    .local v8, context:Landroid/content/Context;
    new-instance v1, Landroid/widget/ListPopupWindow$1;

    #@e
    move-object/from16 v0, p0

    #@10
    invoke-direct {v1, v0}, Landroid/widget/ListPopupWindow$1;-><init>(Landroid/widget/ListPopupWindow;)V

    #@13
    move-object/from16 v0, p0

    #@15
    iput-object v1, v0, Landroid/widget/ListPopupWindow;->mShowDropDownRunnable:Ljava/lang/Runnable;

    #@17
    .line 994
    new-instance v3, Landroid/widget/ListPopupWindow$DropDownListView;

    #@19
    move-object/from16 v0, p0

    #@1b
    iget-boolean v1, v0, Landroid/widget/ListPopupWindow;->mModal:Z

    #@1d
    if-nez v1, :cond_14e

    #@1f
    const/4 v1, 0x1

    #@20
    :goto_20
    invoke-direct {v3, v8, v1}, Landroid/widget/ListPopupWindow$DropDownListView;-><init>(Landroid/content/Context;Z)V

    #@23
    move-object/from16 v0, p0

    #@25
    iput-object v3, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@27
    .line 995
    move-object/from16 v0, p0

    #@29
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownListHighlight:Landroid/graphics/drawable/Drawable;

    #@2b
    if-eqz v1, :cond_38

    #@2d
    .line 996
    move-object/from16 v0, p0

    #@2f
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@31
    move-object/from16 v0, p0

    #@33
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mDropDownListHighlight:Landroid/graphics/drawable/Drawable;

    #@35
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    #@38
    .line 998
    :cond_38
    move-object/from16 v0, p0

    #@3a
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@3c
    move-object/from16 v0, p0

    #@3e
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@40
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@43
    .line 999
    move-object/from16 v0, p0

    #@45
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@47
    move-object/from16 v0, p0

    #@49
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@4b
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@4e
    .line 1000
    move-object/from16 v0, p0

    #@50
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@52
    const/4 v3, 0x1

    #@53
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setFocusable(Z)V

    #@56
    .line 1001
    move-object/from16 v0, p0

    #@58
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@5a
    const/4 v3, 0x1

    #@5b
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setFocusableInTouchMode(Z)V

    #@5e
    .line 1002
    move-object/from16 v0, p0

    #@60
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@62
    new-instance v3, Landroid/widget/ListPopupWindow$2;

    #@64
    move-object/from16 v0, p0

    #@66
    invoke-direct {v3, v0}, Landroid/widget/ListPopupWindow$2;-><init>(Landroid/widget/ListPopupWindow;)V

    #@69
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    #@6c
    .line 1018
    move-object/from16 v0, p0

    #@6e
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@70
    move-object/from16 v0, p0

    #@72
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mScrollListener:Landroid/widget/ListPopupWindow$PopupScrollListener;

    #@74
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    #@77
    .line 1020
    move-object/from16 v0, p0

    #@79
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@7b
    if-eqz v1, :cond_88

    #@7d
    .line 1021
    move-object/from16 v0, p0

    #@7f
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@85
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    #@88
    .line 1024
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget-object v9, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@8c
    .line 1026
    .local v9, dropDownView:Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    #@8e
    iget-object v13, v0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@90
    .line 1027
    .local v13, hintView:Landroid/view/View;
    if-eqz v13, :cond_e9

    #@92
    .line 1030
    new-instance v11, Landroid/widget/LinearLayout;

    #@94
    invoke-direct {v11, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@97
    .line 1031
    .local v11, hintContainer:Landroid/widget/LinearLayout;
    const/4 v1, 0x1

    #@98
    invoke-virtual {v11, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@9b
    .line 1033
    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    #@9d
    const/4 v1, -0x1

    #@9e
    const/4 v3, 0x0

    #@9f
    const/high16 v4, 0x3f80

    #@a1
    invoke-direct {v12, v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@a4
    .line 1037
    .local v12, hintParams:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    #@a6
    iget v1, v0, Landroid/widget/ListPopupWindow;->mPromptPosition:I

    #@a8
    packed-switch v1, :pswitch_data_1fc

    #@ab
    .line 1049
    const-string v1, "ListPopupWindow"

    #@ad
    new-instance v3, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v4, "Invalid hint position "

    #@b4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v3

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget v4, v0, Landroid/widget/ListPopupWindow;->mPromptPosition:I

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v3

    #@c4
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 1055
    :goto_c7
    move-object/from16 v0, p0

    #@c9
    iget v1, v0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@cb
    const/high16 v3, -0x8000

    #@cd
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@d0
    move-result v20

    #@d1
    .line 1056
    .local v20, widthSpec:I
    const/4 v10, 0x0

    #@d2
    .line 1057
    .local v10, heightSpec:I
    move/from16 v0, v20

    #@d4
    invoke-virtual {v13, v0, v10}, Landroid/view/View;->measure(II)V

    #@d7
    .line 1059
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@da
    move-result-object v12

    #@db
    .end local v12           #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    #@dd
    .line 1060
    .restart local v12       #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    #@e0
    move-result v1

    #@e1
    iget v3, v12, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@e3
    add-int/2addr v1, v3

    #@e4
    iget v3, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@e6
    add-int v17, v1, v3

    #@e8
    .line 1063
    move-object v9, v11

    #@e9
    .line 1066
    .end local v10           #heightSpec:I
    .end local v11           #hintContainer:Landroid/widget/LinearLayout;
    .end local v12           #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v20           #widthSpec:I
    :cond_e9
    move-object/from16 v0, p0

    #@eb
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@ed
    invoke-virtual {v1, v9}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@f0
    .line 1080
    .end local v8           #context:Landroid/content/Context;
    .end local v13           #hintView:Landroid/view/View;
    :cond_f0
    :goto_f0
    const/16 v18, 0x0

    #@f2
    .line 1081
    .local v18, padding:I
    move-object/from16 v0, p0

    #@f4
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@f6
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@f9
    move-result-object v7

    #@fa
    .line 1082
    .local v7, background:Landroid/graphics/drawable/Drawable;
    if-eqz v7, :cond_186

    #@fc
    .line 1083
    move-object/from16 v0, p0

    #@fe
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@100
    invoke-virtual {v7, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@103
    .line 1084
    move-object/from16 v0, p0

    #@105
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@107
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@109
    move-object/from16 v0, p0

    #@10b
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@10d
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@10f
    add-int v18, v1, v3

    #@111
    .line 1088
    move-object/from16 v0, p0

    #@113
    iget-boolean v1, v0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffsetSet:Z

    #@115
    if-nez v1, :cond_122

    #@117
    .line 1089
    move-object/from16 v0, p0

    #@119
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@11b
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@11d
    neg-int v1, v1

    #@11e
    move-object/from16 v0, p0

    #@120
    iput v1, v0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@122
    .line 1096
    :cond_122
    :goto_122
    move-object/from16 v0, p0

    #@124
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@126
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    #@129
    move-result v1

    #@12a
    const/4 v3, 0x2

    #@12b
    if-ne v1, v3, :cond_18e

    #@12d
    const/4 v14, 0x1

    #@12e
    .line 1098
    .local v14, ignoreBottomDecorations:Z
    :goto_12e
    move-object/from16 v0, p0

    #@130
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@132
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@135
    move-result-object v3

    #@136
    move-object/from16 v0, p0

    #@138
    iget v4, v0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@13a
    invoke-virtual {v1, v3, v4, v14}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;IZ)I

    #@13d
    move-result v16

    #@13e
    .line 1101
    .local v16, maxHeight:I
    move-object/from16 v0, p0

    #@140
    iget-boolean v1, v0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@142
    if-nez v1, :cond_14b

    #@144
    move-object/from16 v0, p0

    #@146
    iget v1, v0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@148
    const/4 v3, -0x1

    #@149
    if-ne v1, v3, :cond_190

    #@14b
    .line 1102
    :cond_14b
    add-int v1, v16, v18

    #@14d
    .line 1129
    :goto_14d
    return v1

    #@14e
    .line 994
    .end local v7           #background:Landroid/graphics/drawable/Drawable;
    .end local v9           #dropDownView:Landroid/view/ViewGroup;
    .end local v14           #ignoreBottomDecorations:Z
    .end local v16           #maxHeight:I
    .end local v18           #padding:I
    .restart local v8       #context:Landroid/content/Context;
    :cond_14e
    const/4 v1, 0x0

    #@14f
    goto/16 :goto_20

    #@151
    .line 1039
    .restart local v9       #dropDownView:Landroid/view/ViewGroup;
    .restart local v11       #hintContainer:Landroid/widget/LinearLayout;
    .restart local v12       #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    .restart local v13       #hintView:Landroid/view/View;
    :pswitch_151
    invoke-virtual {v11, v9, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@154
    .line 1040
    invoke-virtual {v11, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@157
    goto/16 :goto_c7

    #@159
    .line 1044
    :pswitch_159
    invoke-virtual {v11, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@15c
    .line 1045
    invoke-virtual {v11, v9, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@15f
    goto/16 :goto_c7

    #@161
    .line 1068
    .end local v8           #context:Landroid/content/Context;
    .end local v9           #dropDownView:Landroid/view/ViewGroup;
    .end local v11           #hintContainer:Landroid/widget/LinearLayout;
    .end local v12           #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v13           #hintView:Landroid/view/View;
    :cond_161
    move-object/from16 v0, p0

    #@163
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@165
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    #@168
    move-result-object v9

    #@169
    check-cast v9, Landroid/view/ViewGroup;

    #@16b
    .line 1069
    .restart local v9       #dropDownView:Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    #@16d
    iget-object v0, v0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@16f
    move-object/from16 v19, v0

    #@171
    .line 1070
    .local v19, view:Landroid/view/View;
    if-eqz v19, :cond_f0

    #@173
    .line 1071
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@176
    move-result-object v12

    #@177
    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    #@179
    .line 1073
    .restart local v12       #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    #@17c
    move-result v1

    #@17d
    iget v3, v12, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@17f
    add-int/2addr v1, v3

    #@180
    iget v3, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@182
    add-int v17, v1, v3

    #@184
    goto/16 :goto_f0

    #@186
    .line 1092
    .end local v12           #hintParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v19           #view:Landroid/view/View;
    .restart local v7       #background:Landroid/graphics/drawable/Drawable;
    .restart local v18       #padding:I
    :cond_186
    move-object/from16 v0, p0

    #@188
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@18a
    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    #@18d
    goto :goto_122

    #@18e
    .line 1096
    :cond_18e
    const/4 v14, 0x0

    #@18f
    goto :goto_12e

    #@190
    .line 1106
    .restart local v14       #ignoreBottomDecorations:Z
    .restart local v16       #maxHeight:I
    :cond_190
    move-object/from16 v0, p0

    #@192
    iget v1, v0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@194
    packed-switch v1, :pswitch_data_204

    #@197
    .line 1120
    move-object/from16 v0, p0

    #@199
    iget v1, v0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@19b
    const/high16 v3, 0x4000

    #@19d
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1a0
    move-result v2

    #@1a1
    .line 1123
    .local v2, childWidthSpec:I
    :goto_1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@1a5
    const/4 v3, 0x0

    #@1a6
    const/4 v4, -0x1

    #@1a7
    sub-int v5, v16, v17

    #@1a9
    const/4 v6, -0x1

    #@1aa
    invoke-virtual/range {v1 .. v6}, Landroid/widget/ListPopupWindow$DropDownListView;->measureHeightOfChildren(IIIII)I

    #@1ad
    move-result v15

    #@1ae
    .line 1127
    .local v15, listContent:I
    if-lez v15, :cond_1b2

    #@1b0
    add-int v17, v17, v18

    #@1b2
    .line 1129
    :cond_1b2
    add-int v1, v15, v17

    #@1b4
    goto :goto_14d

    #@1b5
    .line 1108
    .end local v2           #childWidthSpec:I
    .end local v15           #listContent:I
    :pswitch_1b5
    move-object/from16 v0, p0

    #@1b7
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    #@1b9
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1bc
    move-result-object v1

    #@1bd
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1c0
    move-result-object v1

    #@1c1
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@1c7
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v4, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@1cd
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@1cf
    add-int/2addr v3, v4

    #@1d0
    sub-int/2addr v1, v3

    #@1d1
    const/high16 v3, -0x8000

    #@1d3
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1d6
    move-result v2

    #@1d7
    .line 1112
    .restart local v2       #childWidthSpec:I
    goto :goto_1a1

    #@1d8
    .line 1114
    .end local v2           #childWidthSpec:I
    :pswitch_1d8
    move-object/from16 v0, p0

    #@1da
    iget-object v1, v0, Landroid/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    #@1dc
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1df
    move-result-object v1

    #@1e0
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1e3
    move-result-object v1

    #@1e4
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v3, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@1ea
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v4, v0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@1f0
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@1f2
    add-int/2addr v3, v4

    #@1f3
    sub-int/2addr v1, v3

    #@1f4
    const/high16 v3, 0x4000

    #@1f6
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1f9
    move-result v2

    #@1fa
    .line 1118
    .restart local v2       #childWidthSpec:I
    goto :goto_1a1

    #@1fb
    .line 1037
    nop

    #@1fc
    :pswitch_data_1fc
    .packed-switch 0x0
        :pswitch_159
        :pswitch_151
    .end packed-switch

    #@204
    .line 1106
    :pswitch_data_204
    .packed-switch -0x2
        :pswitch_1b5
        :pswitch_1d8
    .end packed-switch
.end method

.method private removePromptView()V
    .registers 4

    #@0
    .prologue
    .line 637
    iget-object v2, p0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@2
    if-eqz v2, :cond_16

    #@4
    .line 638
    iget-object v2, p0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@6
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@9
    move-result-object v1

    #@a
    .line 639
    .local v1, parent:Landroid/view/ViewParent;
    instance-of v2, v1, Landroid/view/ViewGroup;

    #@c
    if-eqz v2, :cond_16

    #@e
    move-object v0, v1

    #@f
    .line 640
    check-cast v0, Landroid/view/ViewGroup;

    #@11
    .line 641
    .local v0, group:Landroid/view/ViewGroup;
    iget-object v2, p0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@13
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@16
    .line 644
    .end local v0           #group:Landroid/view/ViewGroup;
    .end local v1           #parent:Landroid/view/ViewParent;
    :cond_16
    return-void
.end method


# virtual methods
.method public clearListSelection()V
    .registers 3

    #@0
    .prologue
    .line 693
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@2
    .line 694
    .local v0, list:Landroid/widget/ListPopupWindow$DropDownListView;
    if-eqz v0, :cond_e

    #@4
    .line 696
    const/4 v1, 0x1

    #@5
    invoke-static {v0, v1}, Landroid/widget/ListPopupWindow$DropDownListView;->access$502(Landroid/widget/ListPopupWindow$DropDownListView;Z)Z

    #@8
    .line 697
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->hideSelector()V

    #@b
    .line 698
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->requestLayout()V

    #@e
    .line 700
    :cond_e
    return-void
.end method

.method public dismiss()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 620
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@3
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@6
    .line 621
    invoke-direct {p0}, Landroid/widget/ListPopupWindow;->removePromptView()V

    #@9
    .line 622
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@b
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@e
    .line 623
    iput-object v1, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@10
    .line 624
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;

    #@12
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mResizePopupRunnable:Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@17
    .line 625
    return-void
.end method

.method public getAnchorView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownAnchorView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getAnimationStyle()I
    .registers 2

    #@0
    .prologue
    .line 374
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getAnimationStyle()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 346
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 469
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@2
    return v0
.end method

.method public getHorizontalOffset()I
    .registers 2

    #@0
    .prologue
    .line 400
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHorizontalOffset:I

    #@2
    return v0
.end method

.method public getInputMethodMode()I
    .registers 2

    #@0
    .prologue
    .line 668
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 791
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@2
    return-object v0
.end method

.method public getPromptPosition()I
    .registers 2

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/widget/ListPopupWindow;->mPromptPosition:I

    #@2
    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 741
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 742
    const/4 v0, 0x0

    #@7
    .line 744
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@a
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItem()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    goto :goto_7
.end method

.method public getSelectedItemId()J
    .registers 3

    #@0
    .prologue
    .line 767
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 768
    const-wide/high16 v0, -0x8000

    #@8
    .line 770
    :goto_8
    return-wide v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@b
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItemId()J

    #@e
    move-result-wide v0

    #@f
    goto :goto_8
.end method

.method public getSelectedItemPosition()I
    .registers 2

    #@0
    .prologue
    .line 754
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 755
    const/4 v0, -0x1

    #@7
    .line 757
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@a
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItemPosition()I

    #@d
    move-result v0

    #@e
    goto :goto_7
.end method

.method public getSelectedView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 780
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 781
    const/4 v0, 0x0

    #@7
    .line 783
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@a
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedView()Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    goto :goto_7
.end method

.method public getSoftInputMode()I
    .registers 2

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getSoftInputMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getVerticalOffset()I
    .registers 2

    #@0
    .prologue
    .line 416
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffsetSet:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 417
    const/4 v0, 0x0

    #@5
    .line 419
    :goto_5
    return v0

    #@6
    :cond_6
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@8
    goto :goto_5
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 436
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@2
    return v0
.end method

.method public isDropDownAlwaysVisible()Z
    .registers 2

    #@0
    .prologue
    .line 306
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@2
    return v0
.end method

.method public isInputMethodNotNeeded()Z
    .registers 3

    #@0
    .prologue
    .line 714
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isModal()Z
    .registers 2

    #@0
    .prologue
    .line 271
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow;->mModal:Z

    #@2
    return v0
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 16
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/16 v12, 0x14

    #@2
    const/16 v11, 0x13

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x1

    #@6
    .line 816
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@9
    move-result v9

    #@a
    if-eqz v9, :cond_93

    #@c
    .line 822
    const/16 v9, 0x3e

    #@e
    if-eq p1, v9, :cond_93

    #@10
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@12
    invoke-virtual {v9}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItemPosition()I

    #@15
    move-result v9

    #@16
    if-gez v9, :cond_20

    #@18
    const/16 v9, 0x42

    #@1a
    if-eq p1, v9, :cond_93

    #@1c
    const/16 v9, 0x17

    #@1e
    if-eq p1, v9, :cond_93

    #@20
    .line 826
    :cond_20
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@22
    invoke-virtual {v9}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItemPosition()I

    #@25
    move-result v4

    #@26
    .line 829
    .local v4, curIndex:I
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@28
    invoke-virtual {v9}, Landroid/widget/PopupWindow;->isAboveAnchor()Z

    #@2b
    move-result v9

    #@2c
    if-nez v9, :cond_5f

    #@2e
    move v2, v7

    #@2f
    .line 831
    .local v2, below:Z
    :goto_2f
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@31
    .line 834
    .local v0, adapter:Landroid/widget/ListAdapter;
    const v5, 0x7fffffff

    #@34
    .line 835
    .local v5, firstItem:I
    const/high16 v6, -0x8000

    #@36
    .line 837
    .local v6, lastItem:I
    if-eqz v0, :cond_47

    #@38
    .line 838
    invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    #@3b
    move-result v1

    #@3c
    .line 839
    .local v1, allEnabled:Z
    if-eqz v1, :cond_61

    #@3e
    move v5, v8

    #@3f
    .line 841
    :goto_3f
    if-eqz v1, :cond_68

    #@41
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@44
    move-result v9

    #@45
    add-int/lit8 v6, v9, -0x1

    #@47
    .line 845
    .end local v1           #allEnabled:Z
    :cond_47
    :goto_47
    if-eqz v2, :cond_4d

    #@49
    if-ne p1, v11, :cond_4d

    #@4b
    if-le v4, v5, :cond_53

    #@4d
    :cond_4d
    if-nez v2, :cond_75

    #@4f
    if-ne p1, v12, :cond_75

    #@51
    if-lt v4, v6, :cond_75

    #@53
    .line 849
    :cond_53
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->clearListSelection()V

    #@56
    .line 850
    iget-object v8, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@58
    invoke-virtual {v8, v7}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@5b
    .line 851
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->show()V

    #@5e
    .line 897
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v2           #below:Z
    .end local v4           #curIndex:I
    .end local v5           #firstItem:I
    .end local v6           #lastItem:I
    :goto_5e
    :sswitch_5e
    return v7

    #@5f
    .restart local v4       #curIndex:I
    :cond_5f
    move v2, v8

    #@60
    .line 829
    goto :goto_2f

    #@61
    .line 839
    .restart local v0       #adapter:Landroid/widget/ListAdapter;
    .restart local v1       #allEnabled:Z
    .restart local v2       #below:Z
    .restart local v5       #firstItem:I
    .restart local v6       #lastItem:I
    :cond_61
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@63
    invoke-virtual {v9, v8, v7}, Landroid/widget/ListPopupWindow$DropDownListView;->lookForSelectablePosition(IZ)I

    #@66
    move-result v5

    #@67
    goto :goto_3f

    #@68
    .line 841
    :cond_68
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@6a
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@6d
    move-result v10

    #@6e
    add-int/lit8 v10, v10, -0x1

    #@70
    invoke-virtual {v9, v10, v8}, Landroid/widget/ListPopupWindow$DropDownListView;->lookForSelectablePosition(IZ)I

    #@73
    move-result v6

    #@74
    goto :goto_47

    #@75
    .line 856
    .end local v1           #allEnabled:Z
    :cond_75
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@77
    invoke-static {v9, v8}, Landroid/widget/ListPopupWindow$DropDownListView;->access$502(Landroid/widget/ListPopupWindow$DropDownListView;Z)Z

    #@7a
    .line 859
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@7c
    invoke-virtual {v9, p1, p2}, Landroid/widget/ListPopupWindow$DropDownListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@7f
    move-result v3

    #@80
    .line 862
    .local v3, consumed:Z
    if-eqz v3, :cond_95

    #@82
    .line 865
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@84
    const/4 v10, 0x2

    #@85
    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@88
    .line 870
    iget-object v9, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@8a
    invoke-virtual {v9}, Landroid/widget/ListPopupWindow$DropDownListView;->requestFocusFromTouch()Z

    #@8d
    .line 871
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->show()V

    #@90
    .line 873
    sparse-switch p1, :sswitch_data_a4

    #@93
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v2           #below:Z
    .end local v3           #consumed:Z
    .end local v4           #curIndex:I
    .end local v5           #firstItem:I
    .end local v6           #lastItem:I
    :cond_93
    move v7, v8

    #@94
    .line 897
    goto :goto_5e

    #@95
    .line 883
    .restart local v0       #adapter:Landroid/widget/ListAdapter;
    .restart local v2       #below:Z
    .restart local v3       #consumed:Z
    .restart local v4       #curIndex:I
    .restart local v5       #firstItem:I
    .restart local v6       #lastItem:I
    :cond_95
    if-eqz v2, :cond_9c

    #@97
    if-ne p1, v12, :cond_9c

    #@99
    .line 886
    if-ne v4, v6, :cond_93

    #@9b
    goto :goto_5e

    #@9c
    .line 889
    :cond_9c
    if-nez v2, :cond_93

    #@9e
    if-ne p1, v11, :cond_93

    #@a0
    if-ne v4, v5, :cond_93

    #@a2
    goto :goto_5e

    #@a3
    .line 873
    nop

    #@a4
    :sswitch_data_a4
    .sparse-switch
        0x13 -> :sswitch_5e
        0x14 -> :sswitch_5e
        0x17 -> :sswitch_5e
        0x42 -> :sswitch_5e
    .end sparse-switch
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 941
    const/4 v3, 0x4

    #@2
    if-ne p1, v3, :cond_41

    #@4
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_41

    #@a
    .line 944
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownAnchorView:Landroid/view/View;

    #@c
    .line 945
    .local v0, anchorView:Landroid/view/View;
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_22

    #@12
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_22

    #@18
    .line 946
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@1b
    move-result-object v1

    #@1c
    .line 947
    .local v1, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v1, :cond_21

    #@1e
    .line 948
    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@21
    .line 962
    .end local v0           #anchorView:Landroid/view/View;
    .end local v1           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_21
    :goto_21
    return v2

    #@22
    .line 951
    .restart local v0       #anchorView:Landroid/view/View;
    :cond_22
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@25
    move-result v3

    #@26
    if-ne v3, v2, :cond_41

    #@28
    .line 952
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@2b
    move-result-object v1

    #@2c
    .line 953
    .restart local v1       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v1, :cond_31

    #@2e
    .line 954
    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@31
    .line 956
    :cond_31
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@34
    move-result v3

    #@35
    if-eqz v3, :cond_41

    #@37
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@3a
    move-result v3

    #@3b
    if-nez v3, :cond_41

    #@3d
    .line 957
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@40
    goto :goto_21

    #@41
    .line 962
    .end local v0           #anchorView:Landroid/view/View;
    .end local v1           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_41
    const/4 v2, 0x0

    #@42
    goto :goto_21
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 911
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1e

    #@6
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@8
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow$DropDownListView;->getSelectedItemPosition()I

    #@b
    move-result v1

    #@c
    if-ltz v1, :cond_1e

    #@e
    .line 912
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@10
    invoke-virtual {v1, p1, p2}, Landroid/widget/ListPopupWindow$DropDownListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@13
    move-result v0

    #@14
    .line 913
    .local v0, consumed:Z
    if-eqz v0, :cond_19

    #@16
    .line 914
    sparse-switch p1, :sswitch_data_20

    #@19
    .line 926
    .end local v0           #consumed:Z
    :cond_19
    :goto_19
    return v0

    #@1a
    .line 920
    .restart local v0       #consumed:Z
    :sswitch_1a
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@1d
    goto :goto_19

    #@1e
    .line 926
    .end local v0           #consumed:Z
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_19

    #@20
    .line 914
    :sswitch_data_20
    .sparse-switch
        0x17 -> :sswitch_1a
        0x42 -> :sswitch_1a
    .end sparse-switch
.end method

.method public performItemClick(I)Z
    .registers 9
    .parameter "position"

    #@0
    .prologue
    .line 725
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_26

    #@6
    .line 726
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@8
    if-eqz v0, :cond_24

    #@a
    .line 727
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@c
    .line 728
    .local v1, list:Landroid/widget/ListPopupWindow$DropDownListView;
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow$DropDownListView;->getFirstVisiblePosition()I

    #@f
    move-result v0

    #@10
    sub-int v0, p1, v0

    #@12
    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v2

    #@16
    .line 729
    .local v2, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow$DropDownListView;->getAdapter()Landroid/widget/ListAdapter;

    #@19
    move-result-object v6

    #@1a
    .line 730
    .local v6, adapter:Landroid/widget/ListAdapter;
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@1c
    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@1f
    move-result-wide v4

    #@20
    move v3, p1

    #@21
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    #@24
    .line 732
    .end local v1           #list:Landroid/widget/ListPopupWindow$DropDownListView;
    .end local v2           #child:Landroid/view/View;
    .end local v6           #adapter:Landroid/widget/ListAdapter;
    :cond_24
    const/4 v0, 0x1

    #@25
    .line 734
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method public postShow()V
    .registers 3

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mShowDropDownRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 525
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 4
    .parameter "adapter"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mObserver:Landroid/database/DataSetObserver;

    #@2
    if-nez v0, :cond_23

    #@4
    .line 214
    new-instance v0, Landroid/widget/ListPopupWindow$PopupDataSetObserver;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/widget/ListPopupWindow$PopupDataSetObserver;-><init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V

    #@a
    iput-object v0, p0, Landroid/widget/ListPopupWindow;->mObserver:Landroid/database/DataSetObserver;

    #@c
    .line 218
    :cond_c
    :goto_c
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@e
    .line 219
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 220
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mObserver:Landroid/database/DataSetObserver;

    #@14
    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@17
    .line 223
    :cond_17
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@19
    if-eqz v0, :cond_22

    #@1b
    .line 224
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@1d
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@1f
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow$DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@22
    .line 226
    :cond_22
    return-void

    #@23
    .line 215
    :cond_23
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@25
    if-eqz v0, :cond_c

    #@27
    .line 216
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mAdapter:Landroid/widget/ListAdapter;

    #@29
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mObserver:Landroid/database/DataSetObserver;

    #@2b
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@2e
    goto :goto_c
.end method

.method public setAnchorView(Landroid/view/View;)V
    .registers 2
    .parameter "anchor"

    #@0
    .prologue
    .line 393
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mDropDownAnchorView:Landroid/view/View;

    #@2
    .line 394
    return-void
.end method

.method public setAnimationStyle(I)V
    .registers 3
    .parameter "animationStyle"

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    #@5
    .line 365
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 356
    return-void
.end method

.method public setContentWidth(I)V
    .registers 5
    .parameter "width"

    #@0
    .prologue
    .line 456
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    .line 457
    .local v0, popupBackground:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1a

    #@8
    .line 458
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@d
    .line 459
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@f
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@11
    iget-object v2, p0, Landroid/widget/ListPopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@13
    iget v2, v2, Landroid/graphics/Rect;->right:I

    #@15
    add-int/2addr v1, v2

    #@16
    add-int/2addr v1, p1

    #@17
    iput v1, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@19
    .line 463
    :goto_19
    return-void

    #@1a
    .line 461
    :cond_1a
    invoke-virtual {p0, p1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    #@1d
    goto :goto_19
.end method

.method public setDropDownAlwaysVisible(Z)V
    .registers 2
    .parameter "dropDownAlwaysVisible"

    #@0
    .prologue
    .line 297
    iput-boolean p1, p0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@2
    .line 298
    return-void
.end method

.method public setForceIgnoreOutsideTouch(Z)V
    .registers 2
    .parameter "forceIgnoreOutsideTouch"

    #@0
    .prologue
    .line 282
    iput-boolean p1, p0, Landroid/widget/ListPopupWindow;->mForceIgnoreOutsideTouch:Z

    #@2
    .line 283
    return-void
.end method

.method public setHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 478
    iput p1, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@2
    .line 479
    return-void
.end method

.method public setHorizontalOffset(I)V
    .registers 2
    .parameter "offset"

    #@0
    .prologue
    .line 409
    iput p1, p0, Landroid/widget/ListPopupWindow;->mDropDownHorizontalOffset:I

    #@2
    .line 410
    return-void
.end method

.method public setInputMethodMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 659
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@5
    .line 660
    return-void
.end method

.method setListItemExpandMax(I)V
    .registers 2
    .parameter "max"

    #@0
    .prologue
    .line 801
    iput p1, p0, Landroid/widget/ListPopupWindow;->mListItemExpandMaximum:I

    #@2
    .line 802
    return-void
.end method

.method public setListSelector(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "selector"

    #@0
    .prologue
    .line 339
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mDropDownListHighlight:Landroid/graphics/drawable/Drawable;

    #@2
    .line 340
    return-void
.end method

.method public setModal(Z)V
    .registers 3
    .parameter "modal"

    #@0
    .prologue
    .line 261
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/ListPopupWindow;->mModal:Z

    #@3
    .line 262
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@5
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    #@8
    .line 263
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 633
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    #@5
    .line 634
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter "clickListener"

    #@0
    .prologue
    .line 489
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    .line 490
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 2
    .parameter "selectedListener"

    #@0
    .prologue
    .line 500
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    .line 501
    return-void
.end method

.method public setPromptPosition(I)V
    .registers 2
    .parameter "position"

    #@0
    .prologue
    .line 238
    iput p1, p0, Landroid/widget/ListPopupWindow;->mPromptPosition:I

    #@2
    .line 239
    return-void
.end method

.method public setPromptView(Landroid/view/View;)V
    .registers 3
    .parameter "prompt"

    #@0
    .prologue
    .line 510
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    .line 511
    .local v0, showing:Z
    if-eqz v0, :cond_9

    #@6
    .line 512
    invoke-direct {p0}, Landroid/widget/ListPopupWindow;->removePromptView()V

    #@9
    .line 514
    :cond_9
    iput-object p1, p0, Landroid/widget/ListPopupWindow;->mPromptView:Landroid/view/View;

    #@b
    .line 515
    if-eqz v0, :cond_10

    #@d
    .line 516
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->show()V

    #@10
    .line 518
    :cond_10
    return-void
.end method

.method public setSelection(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 678
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@2
    .line 679
    .local v0, list:Landroid/widget/ListPopupWindow$DropDownListView;
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_1b

    #@8
    if-eqz v0, :cond_1b

    #@a
    .line 680
    const/4 v1, 0x0

    #@b
    invoke-static {v0, v1}, Landroid/widget/ListPopupWindow$DropDownListView;->access$502(Landroid/widget/ListPopupWindow$DropDownListView;Z)Z

    #@e
    .line 681
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow$DropDownListView;->setSelection(I)V

    #@11
    .line 682
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->getChoiceMode()I

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_1b

    #@17
    .line 683
    const/4 v1, 0x1

    #@18
    invoke-virtual {v0, p1, v1}, Landroid/widget/ListPopupWindow$DropDownListView;->setItemChecked(IZ)V

    #@1b
    .line 686
    :cond_1b
    return-void
.end method

.method public setSoftInputMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setSoftInputMode(I)V

    #@5
    .line 321
    return-void
.end method

.method public setVerticalOffset(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 428
    iput p1, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@2
    .line 429
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffsetSet:Z

    #@5
    .line 430
    return-void
.end method

.method public setWidth(I)V
    .registers 2
    .parameter "width"

    #@0
    .prologue
    .line 446
    iput p1, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@2
    .line 447
    return-void
.end method

.method public show()V
    .registers 11

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v9, -0x2

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, -0x1

    #@4
    .line 532
    invoke-direct {p0}, Landroid/widget/ListPopupWindow;->buildDropDown()I

    #@7
    move-result v6

    #@8
    .line 534
    .local v6, height:I
    const/4 v4, 0x0

    #@9
    .line 535
    .local v4, widthSpec:I
    const/4 v5, 0x0

    #@a
    .line 537
    .local v5, heightSpec:I
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->isInputMethodNotNeeded()Z

    #@d
    move-result v7

    #@e
    .line 538
    .local v7, noInputMethod:Z
    iget-object v8, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@10
    if-nez v7, :cond_51

    #@12
    move v0, v1

    #@13
    :goto_13
    invoke-virtual {v8, v0}, Landroid/widget/PopupWindow;->setAllowScrollingAnchorParent(Z)V

    #@16
    .line 540
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@18
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_7f

    #@1e
    .line 541
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@20
    if-ne v0, v3, :cond_53

    #@22
    .line 544
    const/4 v4, -0x1

    #@23
    .line 551
    :goto_23
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@25
    if-ne v0, v3, :cond_74

    #@27
    .line 554
    if-eqz v7, :cond_63

    #@29
    move v5, v6

    #@2a
    .line 555
    :goto_2a
    if-eqz v7, :cond_67

    #@2c
    .line 556
    iget-object v8, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@2e
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@30
    if-ne v0, v3, :cond_65

    #@32
    move v0, v3

    #@33
    :goto_33
    invoke-virtual {v8, v0, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    #@36
    .line 571
    :goto_36
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@38
    iget-boolean v3, p0, Landroid/widget/ListPopupWindow;->mForceIgnoreOutsideTouch:Z

    #@3a
    if-nez v3, :cond_7d

    #@3c
    iget-boolean v3, p0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@3e
    if-nez v3, :cond_7d

    #@40
    :goto_40
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    #@43
    .line 573
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@45
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@48
    move-result-object v1

    #@49
    iget v2, p0, Landroid/widget/ListPopupWindow;->mDropDownHorizontalOffset:I

    #@4b
    iget v3, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@4d
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    #@50
    .line 614
    :cond_50
    :goto_50
    return-void

    #@51
    :cond_51
    move v0, v2

    #@52
    .line 538
    goto :goto_13

    #@53
    .line 545
    :cond_53
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@55
    if-ne v0, v9, :cond_60

    #@57
    .line 546
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@5e
    move-result v4

    #@5f
    goto :goto_23

    #@60
    .line 548
    :cond_60
    iget v4, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@62
    goto :goto_23

    #@63
    :cond_63
    move v5, v3

    #@64
    .line 554
    goto :goto_2a

    #@65
    :cond_65
    move v0, v2

    #@66
    .line 556
    goto :goto_33

    #@67
    .line 560
    :cond_67
    iget-object v8, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@69
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@6b
    if-ne v0, v3, :cond_72

    #@6d
    move v0, v3

    #@6e
    :goto_6e
    invoke-virtual {v8, v0, v3}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    #@71
    goto :goto_36

    #@72
    :cond_72
    move v0, v2

    #@73
    goto :goto_6e

    #@74
    .line 565
    :cond_74
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@76
    if-ne v0, v9, :cond_7a

    #@78
    .line 566
    move v5, v6

    #@79
    goto :goto_36

    #@7a
    .line 568
    :cond_7a
    iget v5, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@7c
    goto :goto_36

    #@7d
    :cond_7d
    move v1, v2

    #@7e
    .line 571
    goto :goto_40

    #@7f
    .line 576
    :cond_7f
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@81
    if-ne v0, v3, :cond_d5

    #@83
    .line 577
    const/4 v4, -0x1

    #@84
    .line 586
    :goto_84
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@86
    if-ne v0, v3, :cond_ef

    #@88
    .line 587
    const/4 v5, -0x1

    #@89
    .line 596
    :goto_89
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@8b
    invoke-virtual {v0, v4, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    #@8e
    .line 597
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@90
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClipToScreenEnabled(Z)V

    #@93
    .line 601
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@95
    iget-boolean v8, p0, Landroid/widget/ListPopupWindow;->mForceIgnoreOutsideTouch:Z

    #@97
    if-nez v8, :cond_101

    #@99
    iget-boolean v8, p0, Landroid/widget/ListPopupWindow;->mDropDownAlwaysVisible:Z

    #@9b
    if-nez v8, :cond_101

    #@9d
    :goto_9d
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    #@a0
    .line 602
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@a2
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mTouchInterceptor:Landroid/widget/ListPopupWindow$PopupTouchInterceptor;

    #@a4
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    #@a7
    .line 603
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@a9
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@ac
    move-result-object v1

    #@ad
    iget v2, p0, Landroid/widget/ListPopupWindow;->mDropDownHorizontalOffset:I

    #@af
    iget v8, p0, Landroid/widget/ListPopupWindow;->mDropDownVerticalOffset:I

    #@b1
    invoke-virtual {v0, v1, v2, v8}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    #@b4
    .line 605
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@b6
    invoke-virtual {v0, v3}, Landroid/widget/ListPopupWindow$DropDownListView;->setSelection(I)V

    #@b9
    .line 607
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow;->mModal:Z

    #@bb
    if-eqz v0, :cond_c5

    #@bd
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mDropDownList:Landroid/widget/ListPopupWindow$DropDownListView;

    #@bf
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow$DropDownListView;->isInTouchMode()Z

    #@c2
    move-result v0

    #@c3
    if-eqz v0, :cond_c8

    #@c5
    .line 608
    :cond_c5
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->clearListSelection()V

    #@c8
    .line 610
    :cond_c8
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow;->mModal:Z

    #@ca
    if-nez v0, :cond_50

    #@cc
    .line 611
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;

    #@ce
    iget-object v1, p0, Landroid/widget/ListPopupWindow;->mHideSelector:Landroid/widget/ListPopupWindow$ListSelectorHider;

    #@d0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@d3
    goto/16 :goto_50

    #@d5
    .line 579
    :cond_d5
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@d7
    if-ne v0, v9, :cond_e7

    #@d9
    .line 580
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@db
    invoke-virtual {p0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@de
    move-result-object v8

    #@df
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    #@e2
    move-result v8

    #@e3
    invoke-virtual {v0, v8}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@e6
    goto :goto_84

    #@e7
    .line 582
    :cond_e7
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@e9
    iget v8, p0, Landroid/widget/ListPopupWindow;->mDropDownWidth:I

    #@eb
    invoke-virtual {v0, v8}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@ee
    goto :goto_84

    #@ef
    .line 589
    :cond_ef
    iget v0, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@f1
    if-ne v0, v9, :cond_f9

    #@f3
    .line 590
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@f5
    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@f8
    goto :goto_89

    #@f9
    .line 592
    :cond_f9
    iget-object v0, p0, Landroid/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;

    #@fb
    iget v8, p0, Landroid/widget/ListPopupWindow;->mDropDownHeight:I

    #@fd
    invoke-virtual {v0, v8}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@100
    goto :goto_89

    #@101
    :cond_101
    move v1, v2

    #@102
    .line 601
    goto :goto_9d
.end method
