.class Landroid/widget/TextView$CharWrapper;
.super Ljava/lang/Object;
.source "TextView.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Landroid/text/GetChars;
.implements Landroid/text/GraphicsOperations;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CharWrapper"
.end annotation


# instance fields
.field private mChars:[C

.field private mLength:I

.field private mStart:I


# direct methods
.method public constructor <init>([CII)V
    .registers 4
    .parameter "chars"
    .parameter "start"
    .parameter "len"

    #@0
    .prologue
    .line 9174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 9175
    iput-object p1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@5
    .line 9176
    iput p2, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@7
    .line 9177
    iput p3, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@9
    .line 9178
    return-void
.end method

.method static synthetic access$102(Landroid/widget/TextView$CharWrapper;[C)[C
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 9170
    iput-object p1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    return-object p1
.end method


# virtual methods
.method public charAt(I)C
    .registers 4
    .parameter "off"

    #@0
    .prologue
    .line 9191
    iget-object v0, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    iget v1, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@4
    add-int/2addr v1, p1

    #@5
    aget-char v0, v0, v1

    #@7
    return v0
.end method

.method public drawText(Landroid/graphics/Canvas;IIFFLandroid/graphics/Paint;)V
    .registers 14
    .parameter "c"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "p"

    #@0
    .prologue
    .line 9217
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@4
    add-int v2, p2, v0

    #@6
    sub-int v3, p3, p2

    #@8
    move-object v0, p1

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    move-object v6, p6

    #@c
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    #@f
    .line 9218
    return-void
.end method

.method public drawTextRun(Landroid/graphics/Canvas;IIIIFFILandroid/graphics/Paint;)V
    .registers 20
    .parameter "c"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "x"
    .parameter "y"
    .parameter "flags"
    .parameter "p"

    #@0
    .prologue
    .line 9222
    sub-int v3, p3, p2

    #@2
    .line 9223
    .local v3, count:I
    sub-int v5, p5, p4

    #@4
    .line 9224
    .local v5, contextCount:I
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@6
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@8
    add-int v2, p2, v0

    #@a
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@c
    add-int v4, p4, v0

    #@e
    move-object v0, p1

    #@f
    move/from16 v6, p6

    #@11
    move/from16 v7, p7

    #@13
    move/from16 v8, p8

    #@15
    move-object/from16 v9, p9

    #@17
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFILandroid/graphics/Paint;)V

    #@1a
    .line 9226
    return-void
.end method

.method public getChars(II[CI)V
    .registers 8
    .parameter "start"
    .parameter "end"
    .parameter "buf"
    .parameter "off"

    #@0
    .prologue
    .line 9208
    if-ltz p1, :cond_c

    #@2
    if-ltz p2, :cond_c

    #@4
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@6
    if-gt p1, v0, :cond_c

    #@8
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@a
    if-le p2, v0, :cond_29

    #@c
    .line 9209
    :cond_c
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 9212
    :cond_29
    iget-object v0, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2b
    iget v1, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@2d
    add-int/2addr v1, p1

    #@2e
    sub-int v2, p2, p1

    #@30
    invoke-static {v0, v1, p3, p4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@33
    .line 9213
    return-void
.end method

.method public getTextRunAdvances(IIIII[FILandroid/graphics/Paint;)F
    .registers 18
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"
    .parameter "p"

    #@0
    .prologue
    .line 9239
    sub-int v3, p2, p1

    #@2
    .line 9240
    .local v3, count:I
    sub-int v5, p4, p3

    #@4
    .line 9241
    .local v5, contextCount:I
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@6
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@8
    add-int v2, p1, v0

    #@a
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@c
    add-int v4, p3, v0

    #@e
    move-object/from16 v0, p8

    #@10
    move v6, p5

    #@11
    move-object v7, p6

    #@12
    move/from16 v8, p7

    #@14
    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FI)F

    #@17
    move-result v0

    #@18
    return v0
.end method

.method public getTextRunAdvances(IIIII[FILandroid/graphics/Paint;I)F
    .registers 20
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"
    .parameter "p"
    .parameter "reserved"

    #@0
    .prologue
    .line 9249
    sub-int v3, p2, p1

    #@2
    .line 9250
    .local v3, count:I
    sub-int v5, p4, p3

    #@4
    .line 9251
    .local v5, contextCount:I
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@6
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@8
    add-int v2, p1, v0

    #@a
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@c
    add-int v4, p3, v0

    #@e
    move-object/from16 v0, p8

    #@10
    move v6, p5

    #@11
    move-object/from16 v7, p6

    #@13
    move/from16 v8, p7

    #@15
    move/from16 v9, p9

    #@17
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@1a
    move-result v0

    #@1b
    return v0
.end method

.method public getTextRunCursor(IIIIILandroid/graphics/Paint;)I
    .registers 14
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "offset"
    .parameter "cursorOpt"
    .parameter "p"

    #@0
    .prologue
    .line 9258
    sub-int v3, p2, p1

    #@2
    .line 9259
    .local v3, contextCount:I
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@4
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@6
    add-int v2, p1, v0

    #@8
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@a
    add-int v5, p4, v0

    #@c
    move-object v0, p6

    #@d
    move v4, p3

    #@e
    move v6, p5

    #@f
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor([CIIIII)I

    #@12
    move-result v0

    #@13
    return v0
.end method

.method public getTextWidths(II[FLandroid/graphics/Paint;)I
    .registers 8
    .parameter "start"
    .parameter "end"
    .parameter "widths"
    .parameter "p"

    #@0
    .prologue
    .line 9233
    iget-object v0, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    iget v1, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@4
    add-int/2addr v1, p1

    #@5
    sub-int v2, p2, p1

    #@7
    invoke-virtual {p4, v0, v1, v2, p3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public length()I
    .registers 2

    #@0
    .prologue
    .line 9187
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@2
    return v0
.end method

.method public measureText(IILandroid/graphics/Paint;)F
    .registers 7
    .parameter "start"
    .parameter "end"
    .parameter "p"

    #@0
    .prologue
    .line 9229
    iget-object v0, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    iget v1, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@4
    add-int/2addr v1, p1

    #@5
    sub-int v2, p2, p1

    #@7
    invoke-virtual {p3, v0, v1, v2}, Landroid/graphics/Paint;->measureText([CII)F

    #@a
    move-result v0

    #@b
    return v0
.end method

.method set([CII)V
    .registers 4
    .parameter "chars"
    .parameter "start"
    .parameter "len"

    #@0
    .prologue
    .line 9181
    iput-object p1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2
    .line 9182
    iput p2, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@4
    .line 9183
    iput p3, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@6
    .line 9184
    return-void
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 7
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 9200
    if-ltz p1, :cond_c

    #@2
    if-ltz p2, :cond_c

    #@4
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@6
    if-gt p1, v0, :cond_c

    #@8
    iget v0, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@a
    if-le p2, v0, :cond_29

    #@c
    .line 9201
    :cond_c
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 9204
    :cond_29
    new-instance v0, Ljava/lang/String;

    #@2b
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@2d
    iget v2, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@2f
    add-int/2addr v2, p1

    #@30
    sub-int v3, p2, p1

    #@32
    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    #@35
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 9196
    new-instance v0, Ljava/lang/String;

    #@2
    iget-object v1, p0, Landroid/widget/TextView$CharWrapper;->mChars:[C

    #@4
    iget v2, p0, Landroid/widget/TextView$CharWrapper;->mStart:I

    #@6
    iget v3, p0, Landroid/widget/TextView$CharWrapper;->mLength:I

    #@8
    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    #@b
    return-object v0
.end method
