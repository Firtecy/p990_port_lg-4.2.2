.class public Landroid/widget/FastScroller$ScrollFade;
.super Ljava/lang/Object;
.source "FastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/FastScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScrollFade"
.end annotation


# static fields
.field static final ALPHA_MAX:I = 0xd0

.field static final FADE_DURATION:J = 0xc8L


# instance fields
.field mFadeDuration:J

.field mStartTime:J

.field final synthetic this$0:Landroid/widget/FastScroller;


# direct methods
.method public constructor <init>(Landroid/widget/FastScroller;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 834
    iput-object p1, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method getAlpha()I
    .registers 10

    #@0
    .prologue
    const-wide/16 v7, 0xd0

    #@2
    .line 848
    iget-object v3, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@4
    invoke-virtual {v3}, Landroid/widget/FastScroller;->getState()I

    #@7
    move-result v3

    #@8
    const/4 v4, 0x4

    #@9
    if-eq v3, v4, :cond_e

    #@b
    .line 849
    const/16 v0, 0xd0

    #@d
    .line 858
    :goto_d
    return v0

    #@e
    .line 852
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@11
    move-result-wide v1

    #@12
    .line 853
    .local v1, now:J
    iget-wide v3, p0, Landroid/widget/FastScroller$ScrollFade;->mStartTime:J

    #@14
    iget-wide v5, p0, Landroid/widget/FastScroller$ScrollFade;->mFadeDuration:J

    #@16
    add-long/2addr v3, v5

    #@17
    cmp-long v3, v1, v3

    #@19
    if-lez v3, :cond_1d

    #@1b
    .line 854
    const/4 v0, 0x0

    #@1c
    .local v0, alpha:I
    goto :goto_d

    #@1d
    .line 856
    .end local v0           #alpha:I
    :cond_1d
    iget-wide v3, p0, Landroid/widget/FastScroller$ScrollFade;->mStartTime:J

    #@1f
    sub-long v3, v1, v3

    #@21
    mul-long/2addr v3, v7

    #@22
    iget-wide v5, p0, Landroid/widget/FastScroller$ScrollFade;->mFadeDuration:J

    #@24
    div-long/2addr v3, v5

    #@25
    sub-long v3, v7, v3

    #@27
    long-to-int v0, v3

    #@28
    .restart local v0       #alpha:I
    goto :goto_d
.end method

.method public run()V
    .registers 3

    #@0
    .prologue
    .line 862
    iget-object v0, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/FastScroller;->getState()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x4

    #@7
    if-eq v0, v1, :cond_d

    #@9
    .line 863
    invoke-virtual {p0}, Landroid/widget/FastScroller$ScrollFade;->startFade()V

    #@c
    .line 872
    :goto_c
    return-void

    #@d
    .line 867
    :cond_d
    invoke-virtual {p0}, Landroid/widget/FastScroller$ScrollFade;->getAlpha()I

    #@10
    move-result v0

    #@11
    if-lez v0, :cond_1b

    #@13
    .line 868
    iget-object v0, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@15
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@17
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    #@1a
    goto :goto_c

    #@1b
    .line 870
    :cond_1b
    iget-object v0, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@1d
    const/4 v1, 0x0

    #@1e
    invoke-virtual {v0, v1}, Landroid/widget/FastScroller;->setState(I)V

    #@21
    goto :goto_c
.end method

.method startFade()V
    .registers 3

    #@0
    .prologue
    .line 842
    const-wide/16 v0, 0xc8

    #@2
    iput-wide v0, p0, Landroid/widget/FastScroller$ScrollFade;->mFadeDuration:J

    #@4
    .line 843
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v0

    #@8
    iput-wide v0, p0, Landroid/widget/FastScroller$ScrollFade;->mStartTime:J

    #@a
    .line 844
    iget-object v0, p0, Landroid/widget/FastScroller$ScrollFade;->this$0:Landroid/widget/FastScroller;

    #@c
    const/4 v1, 0x4

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/FastScroller;->setState(I)V

    #@10
    .line 845
    return-void
.end method
