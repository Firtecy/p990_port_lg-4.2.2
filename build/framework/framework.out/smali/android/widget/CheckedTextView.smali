.class public Landroid/widget/CheckedTextView;
.super Landroid/widget/TextView;
.source "CheckedTextView.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private mBasePadding:I

.field private mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

.field private mCheckMarkResource:I

.field private mCheckMarkWidth:I

.field private mChecked:Z

.field private mNeedRequestlayout:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 49
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x10100a0

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Landroid/widget/CheckedTextView;->CHECKED_STATE_SET:[I

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 58
    const v0, 0x10103c8

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 64
    sget-object v3, Lcom/android/internal/R$styleable;->CheckedTextView:[I

    #@6
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 67
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x1

    #@b
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v2

    #@f
    .line 68
    .local v2, d:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_14

    #@11
    .line 69
    invoke-virtual {p0, v2}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    #@14
    .line 72
    :cond_14
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@17
    move-result v1

    #@18
    .line 73
    .local v1, checked:Z
    invoke-virtual {p0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    #@1b
    .line 75
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1e
    .line 76
    return-void
.end method

.method private setBasePadding(Z)V
    .registers 3
    .parameter "isLayoutRtl"

    #@0
    .prologue
    .line 205
    if-eqz p1, :cond_7

    #@2
    .line 206
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@4
    iput v0, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@6
    .line 210
    :goto_6
    return-void

    #@7
    .line 208
    :cond_7
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@9
    iput v0, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@b
    goto :goto_6
.end method

.method private updatePadding()V
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 188
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->resetPaddingToInitialValues()V

    #@5
    .line 189
    iget-object v3, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@7
    if-eqz v3, :cond_2a

    #@9
    iget v3, p0, Landroid/widget/CheckedTextView;->mCheckMarkWidth:I

    #@b
    iget v4, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@d
    add-int v0, v3, v4

    #@f
    .line 191
    .local v0, newPadding:I
    :goto_f
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->isLayoutRtl()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_2f

    #@15
    .line 192
    iget-boolean v3, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@17
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@19
    if-eq v4, v0, :cond_2d

    #@1b
    :goto_1b
    or-int/2addr v1, v3

    #@1c
    iput-boolean v1, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@1e
    .line 193
    iput v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@20
    .line 198
    :goto_20
    iget-boolean v1, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 199
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->requestLayout()V

    #@27
    .line 200
    iput-boolean v2, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@29
    .line 202
    :cond_29
    return-void

    #@2a
    .line 189
    .end local v0           #newPadding:I
    :cond_2a
    iget v0, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@2c
    goto :goto_f

    #@2d
    .restart local v0       #newPadding:I
    :cond_2d
    move v1, v2

    #@2e
    .line 192
    goto :goto_1b

    #@2f
    .line 195
    :cond_2f
    iget-boolean v3, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@31
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@33
    if-eq v4, v0, :cond_3b

    #@35
    :goto_35
    or-int/2addr v1, v3

    #@36
    iput-boolean v1, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@38
    .line 196
    iput v0, p0, Landroid/view/View;->mPaddingRight:I

    #@3a
    goto :goto_20

    #@3b
    :cond_3b
    move v1, v2

    #@3c
    .line 195
    goto :goto_35
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 261
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    #@3
    .line 263
    iget-object v1, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v1, :cond_13

    #@7
    .line 264
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getDrawableState()[I

    #@a
    move-result-object v0

    #@b
    .line 267
    .local v0, myDrawableState:[I
    iget-object v1, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@10
    .line 269
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->invalidate()V

    #@13
    .line 271
    .end local v0           #myDrawableState:[I
    :cond_13
    return-void
.end method

.method public getCheckMarkDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method protected internalSetPadding(IIII)V
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->internalSetPadding(IIII)V

    #@3
    .line 178
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->isLayoutRtl()Z

    #@6
    move-result v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/CheckedTextView;->setBasePadding(Z)V

    #@a
    .line 179
    return-void
.end method

.method public isChecked()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 84
    iget-boolean v0, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@2
    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 252
    add-int/lit8 v1, p1, 0x1

    #@2
    invoke-super {p0, v1}, Landroid/widget/TextView;->onCreateDrawableState(I)[I

    #@5
    move-result-object v0

    #@6
    .line 253
    .local v0, drawableState:[I
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->isChecked()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 254
    sget-object v1, Landroid/widget/CheckedTextView;->CHECKED_STATE_SET:[I

    #@e
    invoke-static {v0, v1}, Landroid/widget/CheckedTextView;->mergeDrawableStates([I[I)[I

    #@11
    .line 256
    :cond_11
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    .line 214
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 216
    iget-object v1, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 217
    .local v1, checkMarkDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_2e

    #@7
    .line 218
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getGravity()I

    #@a
    move-result v10

    #@b
    and-int/lit8 v7, v10, 0x70

    #@d
    .line 219
    .local v7, verticalGravity:I
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@10
    move-result v2

    #@11
    .line 221
    .local v2, height:I
    const/4 v9, 0x0

    #@12
    .line 223
    .local v9, y:I
    sparse-switch v7, :sswitch_data_48

    #@15
    .line 232
    :goto_15
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->isLayoutRtl()Z

    #@18
    move-result v3

    #@19
    .line 233
    .local v3, isLayoutRtl:Z
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getWidth()I

    #@1c
    move-result v8

    #@1d
    .line 234
    .local v8, width:I
    move v6, v9

    #@1e
    .line 235
    .local v6, top:I
    add-int v0, v6, v2

    #@20
    .line 238
    .local v0, bottom:I
    if-eqz v3, :cond_3e

    #@22
    .line 239
    iget v4, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@24
    .line 240
    .local v4, left:I
    iget v10, p0, Landroid/widget/CheckedTextView;->mCheckMarkWidth:I

    #@26
    add-int v5, v4, v10

    #@28
    .line 245
    .local v5, right:I
    :goto_28
    invoke-virtual {v1, v4, v6, v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2b
    .line 246
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@2e
    .line 248
    .end local v0           #bottom:I
    .end local v2           #height:I
    .end local v3           #isLayoutRtl:Z
    .end local v4           #left:I
    .end local v5           #right:I
    .end local v6           #top:I
    .end local v7           #verticalGravity:I
    .end local v8           #width:I
    .end local v9           #y:I
    :cond_2e
    return-void

    #@2f
    .line 225
    .restart local v2       #height:I
    .restart local v7       #verticalGravity:I
    .restart local v9       #y:I
    :sswitch_2f
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getHeight()I

    #@32
    move-result v10

    #@33
    sub-int v9, v10, v2

    #@35
    .line 226
    goto :goto_15

    #@36
    .line 228
    :sswitch_36
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getHeight()I

    #@39
    move-result v10

    #@3a
    sub-int/2addr v10, v2

    #@3b
    div-int/lit8 v9, v10, 0x2

    #@3d
    goto :goto_15

    #@3e
    .line 242
    .restart local v0       #bottom:I
    .restart local v3       #isLayoutRtl:Z
    .restart local v6       #top:I
    .restart local v8       #width:I
    :cond_3e
    iget v10, p0, Landroid/widget/CheckedTextView;->mBasePadding:I

    #@40
    sub-int v5, v8, v10

    #@42
    .line 243
    .restart local v5       #right:I
    iget v10, p0, Landroid/widget/CheckedTextView;->mCheckMarkWidth:I

    #@44
    sub-int v4, v5, v10

    #@46
    .restart local v4       #left:I
    goto :goto_28

    #@47
    .line 223
    nop

    #@48
    :sswitch_data_48
    .sparse-switch
        0x10 -> :sswitch_36
        0x50 -> :sswitch_2f
    .end sparse-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 275
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 276
    const-class v0, Landroid/widget/CheckedTextView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 277
    iget-boolean v0, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@e
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    #@11
    .line 278
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 282
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 283
    const-class v0, Landroid/widget/CheckedTextView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 284
    const/4 v0, 0x1

    #@d
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    #@10
    .line 285
    iget-boolean v0, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@12
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    #@15
    .line 286
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 2
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/widget/TextView;->onRtlPropertiesChanged(I)V

    #@3
    .line 184
    invoke-direct {p0}, Landroid/widget/CheckedTextView;->updatePadding()V

    #@6
    .line 185
    return-void
.end method

.method public setCheckMarkDrawable(I)V
    .registers 5
    .parameter "resid"

    #@0
    .prologue
    .line 113
    if-eqz p1, :cond_7

    #@2
    iget v1, p0, Landroid/widget/CheckedTextView;->mCheckMarkResource:I

    #@4
    if-ne p1, v1, :cond_7

    #@6
    .line 124
    :goto_6
    return-void

    #@7
    .line 117
    :cond_7
    iput p1, p0, Landroid/widget/CheckedTextView;->mCheckMarkResource:I

    #@9
    .line 119
    const/4 v0, 0x0

    #@a
    .line 120
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iget v1, p0, Landroid/widget/CheckedTextView;->mCheckMarkResource:I

    #@c
    if-eqz v1, :cond_18

    #@e
    .line 121
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getResources()Landroid/content/res/Resources;

    #@11
    move-result-object v1

    #@12
    iget v2, p0, Landroid/widget/CheckedTextView;->mCheckMarkResource:I

    #@14
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    .line 123
    :cond_18
    invoke-virtual {p0, v0}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1b
    goto :goto_6
.end method

.method public setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "d"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 137
    iget-object v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 138
    iget-object v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@c
    .line 139
    iget-object v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {p0, v0}, Landroid/widget/CheckedTextView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 141
    :cond_11
    iget-object v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    if-eq p1, v0, :cond_45

    #@15
    move v0, v1

    #@16
    :goto_16
    iput-boolean v0, p0, Landroid/widget/CheckedTextView;->mNeedRequestlayout:Z

    #@18
    .line 142
    if-eqz p1, :cond_49

    #@1a
    .line 143
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@1d
    .line 144
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getVisibility()I

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_47

    #@23
    :goto_23
    invoke-virtual {p1, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@26
    .line 145
    sget-object v0, Landroid/widget/CheckedTextView;->CHECKED_STATE_SET:[I

    #@28
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@2b
    .line 146
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2e
    move-result v0

    #@2f
    invoke-virtual {p0, v0}, Landroid/widget/CheckedTextView;->setMinHeight(I)V

    #@32
    .line 148
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@35
    move-result v0

    #@36
    iput v0, p0, Landroid/widget/CheckedTextView;->mCheckMarkWidth:I

    #@38
    .line 149
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getDrawableState()[I

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@3f
    .line 153
    :goto_3f
    iput-object p1, p0, Landroid/widget/CheckedTextView;->mCheckMarkDrawable:Landroid/graphics/drawable/Drawable;

    #@41
    .line 155
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->resolvePadding()V

    #@44
    .line 156
    return-void

    #@45
    :cond_45
    move v0, v2

    #@46
    .line 141
    goto :goto_16

    #@47
    :cond_47
    move v1, v2

    #@48
    .line 144
    goto :goto_23

    #@49
    .line 151
    :cond_49
    iput v2, p0, Landroid/widget/CheckedTextView;->mCheckMarkWidth:I

    #@4b
    goto :goto_3f
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    #@0
    .prologue
    .line 93
    iget-boolean v0, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@2
    if-eq v0, p1, :cond_c

    #@4
    .line 94
    iput-boolean p1, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@6
    .line 95
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->refreshDrawableState()V

    #@9
    .line 96
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->notifyAccessibilityStateChanged()V

    #@c
    .line 98
    :cond_c
    return-void
.end method

.method public toggle()V
    .registers 2

    #@0
    .prologue
    .line 79
    iget-boolean v0, p0, Landroid/widget/CheckedTextView;->mChecked:Z

    #@2
    if-nez v0, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    #@8
    .line 80
    return-void

    #@9
    .line 79
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method
