.class Landroid/widget/ListPopupWindow$PopupTouchInterceptor;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupTouchInterceptor"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/ListPopupWindow;


# direct methods
.method private constructor <init>(Landroid/widget/ListPopupWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1273
    iput-object p1, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ListPopupWindow;Landroid/widget/ListPopupWindow$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1273
    invoke-direct {p0, p1}, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;-><init>(Landroid/widget/ListPopupWindow;)V

    #@3
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    .line 1275
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    .line 1276
    .local v0, action:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@7
    move-result v3

    #@8
    float-to-int v1, v3

    #@9
    .line 1277
    .local v1, x:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    #@c
    move-result v3

    #@d
    float-to-int v2, v3

    #@e
    .line 1279
    .local v2, y:I
    if-nez v0, :cond_53

    #@10
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@12
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$700(Landroid/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    #@15
    move-result-object v3

    #@16
    if-eqz v3, :cond_53

    #@18
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@1a
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$700(Landroid/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_53

    #@24
    if-ltz v1, :cond_53

    #@26
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@28
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$700(Landroid/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    #@2f
    move-result v3

    #@30
    if-ge v1, v3, :cond_53

    #@32
    if-ltz v2, :cond_53

    #@34
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@36
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$700(Landroid/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getHeight()I

    #@3d
    move-result v3

    #@3e
    if-ge v2, v3, :cond_53

    #@40
    .line 1282
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@42
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$900(Landroid/widget/ListPopupWindow;)Landroid/os/Handler;

    #@45
    move-result-object v3

    #@46
    iget-object v4, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@48
    invoke-static {v4}, Landroid/widget/ListPopupWindow;->access$800(Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@4b
    move-result-object v4

    #@4c
    const-wide/16 v5, 0xfa

    #@4e
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@51
    .line 1286
    :cond_51
    :goto_51
    const/4 v3, 0x0

    #@52
    return v3

    #@53
    .line 1283
    :cond_53
    const/4 v3, 0x1

    #@54
    if-ne v0, v3, :cond_51

    #@56
    .line 1284
    iget-object v3, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@58
    invoke-static {v3}, Landroid/widget/ListPopupWindow;->access$900(Landroid/widget/ListPopupWindow;)Landroid/os/Handler;

    #@5b
    move-result-object v3

    #@5c
    iget-object v4, p0, Landroid/widget/ListPopupWindow$PopupTouchInterceptor;->this$0:Landroid/widget/ListPopupWindow;

    #@5e
    invoke-static {v4}, Landroid/widget/ListPopupWindow;->access$800(Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow$ResizePopupRunnable;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@65
    goto :goto_51
.end method
