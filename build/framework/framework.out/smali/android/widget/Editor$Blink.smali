.class Landroid/widget/Editor$Blink;
.super Landroid/os/Handler;
.source "Editor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Blink"
.end annotation


# instance fields
.field private mCancelled:Z

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method private constructor <init>(Landroid/widget/Editor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2106
    iput-object p1, p0, Landroid/widget/Editor$Blink;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2106
    invoke-direct {p0, p1}, Landroid/widget/Editor$Blink;-><init>(Landroid/widget/Editor;)V

    #@3
    return-void
.end method


# virtual methods
.method cancel()V
    .registers 2

    #@0
    .prologue
    .line 2129
    iget-boolean v0, p0, Landroid/widget/Editor$Blink;->mCancelled:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 2130
    invoke-virtual {p0, p0}, Landroid/widget/Editor$Blink;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 2131
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/widget/Editor$Blink;->mCancelled:Z

    #@a
    .line 2133
    :cond_a
    return-void
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 2110
    iget-boolean v0, p0, Landroid/widget/Editor$Blink;->mCancelled:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 2126
    :cond_4
    :goto_4
    return-void

    #@5
    .line 2114
    :cond_5
    invoke-virtual {p0, p0}, Landroid/widget/Editor$Blink;->removeCallbacks(Ljava/lang/Runnable;)V

    #@8
    .line 2118
    iget-object v0, p0, Landroid/widget/Editor$Blink;->this$0:Landroid/widget/Editor;

    #@a
    invoke-static {v0}, Landroid/widget/Editor;->access$700(Landroid/widget/Editor;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_4

    #@10
    iget-object v0, p0, Landroid/widget/Editor$Blink;->this$0:Landroid/widget/Editor;

    #@12
    invoke-static {v0}, Landroid/widget/Editor;->access$800(Landroid/widget/Editor;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_4

    #@18
    .line 2120
    iget-object v0, p0, Landroid/widget/Editor$Blink;->this$0:Landroid/widget/Editor;

    #@1a
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@21
    move-result-object v0

    #@22
    if-eqz v0, :cond_2d

    #@24
    .line 2121
    iget-object v0, p0, Landroid/widget/Editor$Blink;->this$0:Landroid/widget/Editor;

    #@26
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidateCursorPath()V

    #@2d
    .line 2124
    :cond_2d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@30
    move-result-wide v0

    #@31
    const-wide/16 v2, 0x1f4

    #@33
    add-long/2addr v0, v2

    #@34
    invoke-virtual {p0, p0, v0, v1}, Landroid/widget/Editor$Blink;->postAtTime(Ljava/lang/Runnable;J)Z

    #@37
    goto :goto_4
.end method

.method uncancel()V
    .registers 2

    #@0
    .prologue
    .line 2136
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/Editor$Blink;->mCancelled:Z

    #@3
    .line 2137
    return-void
.end method
