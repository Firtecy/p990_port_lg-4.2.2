.class Landroid/widget/AbsListView$3;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "AbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/AbsListView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;Landroid/view/inputmethod/InputConnection;Z)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 5803
    iput-object p1, p0, Landroid/widget/AbsListView$3;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    #@5
    return-void
.end method


# virtual methods
.method public performEditorAction(I)Z
    .registers 6
    .parameter "editorAction"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5815
    const/4 v2, 0x6

    #@2
    if-ne p1, v2, :cond_1e

    #@4
    .line 5816
    iget-object v2, p0, Landroid/widget/AbsListView$3;->this$0:Landroid/widget/AbsListView;

    #@6
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    const-string v3, "input_method"

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@12
    .line 5819
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1d

    #@14
    .line 5820
    iget-object v2, p0, Landroid/widget/AbsListView$3;->this$0:Landroid/widget/AbsListView;

    #@16
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getWindowToken()Landroid/os/IBinder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1d
    .line 5822
    :cond_1d
    const/4 v1, 0x1

    #@1e
    .line 5824
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_1e
    return v1
.end method

.method public reportFullscreenMode(Z)Z
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 5808
    iget-object v0, p0, Landroid/widget/AbsListView$3;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-static {v0}, Landroid/widget/AbsListView;->access$3400(Landroid/widget/AbsListView;)Landroid/view/inputmethod/InputConnection;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/view/inputmethod/InputConnection;->reportFullscreenMode(Z)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 5832
    iget-object v0, p0, Landroid/widget/AbsListView$3;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-static {v0}, Landroid/widget/AbsListView;->access$3400(Landroid/widget/AbsListView;)Landroid/view/inputmethod/InputConnection;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method
