.class Landroid/widget/VideoView$4;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/VideoView;


# direct methods
.method constructor <init>(Landroid/widget/VideoView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 367
    iput-object p1, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 11
    .parameter "mp"
    .parameter "framework_err"
    .parameter "impl_err"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, -0x1

    #@2
    .line 369
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@4
    invoke-static {v2}, Landroid/widget/VideoView;->access$1400(Landroid/widget/VideoView;)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Error: "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, ","

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 370
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@2a
    invoke-static {v2, v5}, Landroid/widget/VideoView;->access$202(Landroid/widget/VideoView;I)I

    #@2d
    .line 371
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@2f
    invoke-static {v2, v5}, Landroid/widget/VideoView;->access$1202(Landroid/widget/VideoView;I)I

    #@32
    .line 372
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@34
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@37
    move-result-object v2

    #@38
    if-eqz v2, :cond_43

    #@3a
    .line 373
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@3c
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Landroid/widget/MediaController;->hide()V

    #@43
    .line 377
    :cond_43
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@45
    invoke-static {v2}, Landroid/widget/VideoView;->access$1500(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    #@48
    move-result-object v2

    #@49
    if-eqz v2, :cond_5e

    #@4b
    .line 378
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@4d
    invoke-static {v2}, Landroid/widget/VideoView;->access$1500(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    #@50
    move-result-object v2

    #@51
    iget-object v3, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@53
    invoke-static {v3}, Landroid/widget/VideoView;->access$700(Landroid/widget/VideoView;)Landroid/media/MediaPlayer;

    #@56
    move-result-object v3

    #@57
    invoke-interface {v2, v3, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    #@5a
    move-result v2

    #@5b
    if-eqz v2, :cond_5e

    #@5d
    .line 414
    :cond_5d
    :goto_5d
    return v6

    #@5e
    .line 388
    :cond_5e
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@60
    invoke-virtual {v2}, Landroid/widget/VideoView;->getWindowToken()Landroid/os/IBinder;

    #@63
    move-result-object v2

    #@64
    if-eqz v2, :cond_5d

    #@66
    .line 389
    iget-object v2, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@68
    invoke-static {v2}, Landroid/widget/VideoView;->access$1600(Landroid/widget/VideoView;)Landroid/content/Context;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6f
    move-result-object v1

    #@70
    .line 392
    .local v1, r:Landroid/content/res/Resources;
    const/16 v2, 0xc8

    #@72
    if-ne p2, v2, :cond_9b

    #@74
    .line 393
    const v0, 0x1040015

    #@77
    .line 398
    .local v0, messageId:I
    :goto_77
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@79
    iget-object v3, p0, Landroid/widget/VideoView$4;->this$0:Landroid/widget/VideoView;

    #@7b
    invoke-static {v3}, Landroid/widget/VideoView;->access$1700(Landroid/widget/VideoView;)Landroid/content/Context;

    #@7e
    move-result-object v3

    #@7f
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@82
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@85
    move-result-object v2

    #@86
    const v3, 0x1040010

    #@89
    new-instance v4, Landroid/widget/VideoView$4$1;

    #@8b
    invoke-direct {v4, p0}, Landroid/widget/VideoView$4$1;-><init>(Landroid/widget/VideoView$4;)V

    #@8e
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@91
    move-result-object v2

    #@92
    const/4 v3, 0x0

    #@93
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@96
    move-result-object v2

    #@97
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@9a
    goto :goto_5d

    #@9b
    .line 395
    .end local v0           #messageId:I
    :cond_9b
    const v0, 0x1040011

    #@9e
    .restart local v0       #messageId:I
    goto :goto_77
.end method
