.class Landroid/widget/RemoteViews$SetOnClickPendingIntent$1;
.super Ljava/lang/Object;
.source "RemoteViews.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/RemoteViews$SetOnClickPendingIntent;->apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/RemoteViews$SetOnClickPendingIntent;

.field final synthetic val$handler:Landroid/widget/RemoteViews$OnClickHandler;


# direct methods
.method constructor <init>(Landroid/widget/RemoteViews$SetOnClickPendingIntent;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 604
    iput-object p1, p0, Landroid/widget/RemoteViews$SetOnClickPendingIntent$1;->this$1:Landroid/widget/RemoteViews$SetOnClickPendingIntent;

    #@2
    iput-object p2, p0, Landroid/widget/RemoteViews$SetOnClickPendingIntent$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "v"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/high16 v6, 0x3f00

    #@4
    .line 608
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@f
    move-result-object v4

    #@10
    iget v0, v4, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@12
    .line 610
    .local v0, appScale:F
    const/4 v4, 0x2

    #@13
    new-array v2, v4, [I

    #@15
    .line 611
    .local v2, pos:[I
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    #@18
    .line 613
    new-instance v3, Landroid/graphics/Rect;

    #@1a
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@1d
    .line 614
    .local v3, rect:Landroid/graphics/Rect;
    aget v4, v2, v5

    #@1f
    int-to-float v4, v4

    #@20
    mul-float/2addr v4, v0

    #@21
    add-float/2addr v4, v6

    #@22
    float-to-int v4, v4

    #@23
    iput v4, v3, Landroid/graphics/Rect;->left:I

    #@25
    .line 615
    aget v4, v2, v7

    #@27
    int-to-float v4, v4

    #@28
    mul-float/2addr v4, v0

    #@29
    add-float/2addr v4, v6

    #@2a
    float-to-int v4, v4

    #@2b
    iput v4, v3, Landroid/graphics/Rect;->top:I

    #@2d
    .line 616
    aget v4, v2, v5

    #@2f
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@32
    move-result v5

    #@33
    add-int/2addr v4, v5

    #@34
    int-to-float v4, v4

    #@35
    mul-float/2addr v4, v0

    #@36
    add-float/2addr v4, v6

    #@37
    float-to-int v4, v4

    #@38
    iput v4, v3, Landroid/graphics/Rect;->right:I

    #@3a
    .line 617
    aget v4, v2, v7

    #@3c
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@3f
    move-result v5

    #@40
    add-int/2addr v4, v5

    #@41
    int-to-float v4, v4

    #@42
    mul-float/2addr v4, v0

    #@43
    add-float/2addr v4, v6

    #@44
    float-to-int v4, v4

    #@45
    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    #@47
    .line 619
    new-instance v1, Landroid/content/Intent;

    #@49
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@4c
    .line 620
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@4f
    .line 621
    iget-object v4, p0, Landroid/widget/RemoteViews$SetOnClickPendingIntent$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@51
    iget-object v5, p0, Landroid/widget/RemoteViews$SetOnClickPendingIntent$1;->this$1:Landroid/widget/RemoteViews$SetOnClickPendingIntent;

    #@53
    iget-object v5, v5, Landroid/widget/RemoteViews$SetOnClickPendingIntent;->pendingIntent:Landroid/app/PendingIntent;

    #@55
    invoke-virtual {v4, p1, v5, v1}, Landroid/widget/RemoteViews$OnClickHandler;->onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    #@58
    .line 622
    return-void
.end method
