.class public final Landroid/widget/ActivityChooserModel$HistoricalRecord;
.super Ljava/lang/Object;
.source "ActivityChooserModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ActivityChooserModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HistoricalRecord"
.end annotation


# instance fields
.field public final activity:Landroid/content/ComponentName;

.field public final time:J

.field public final weight:F


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;JF)V
    .registers 5
    .parameter "activityName"
    .parameter "time"
    .parameter "weight"

    #@0
    .prologue
    .line 809
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 810
    iput-object p1, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@5
    .line 811
    iput-wide p2, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@7
    .line 812
    iput p4, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@9
    .line 813
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JF)V
    .registers 6
    .parameter "activityName"
    .parameter "time"
    .parameter "weight"

    #@0
    .prologue
    .line 799
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2, p3, p4}, Landroid/widget/ActivityChooserModel$HistoricalRecord;-><init>(Landroid/content/ComponentName;JF)V

    #@7
    .line 800
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 827
    if-ne p0, p1, :cond_5

    #@4
    .line 850
    :cond_4
    :goto_4
    return v1

    #@5
    .line 830
    :cond_5
    if-nez p1, :cond_9

    #@7
    move v1, v2

    #@8
    .line 831
    goto :goto_4

    #@9
    .line 833
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@10
    move-result-object v4

    #@11
    if-eq v3, v4, :cond_15

    #@13
    move v1, v2

    #@14
    .line 834
    goto :goto_4

    #@15
    :cond_15
    move-object v0, p1

    #@16
    .line 836
    check-cast v0, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@18
    .line 837
    .local v0, other:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    iget-object v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@1a
    if-nez v3, :cond_22

    #@1c
    .line 838
    iget-object v3, v0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@1e
    if-eqz v3, :cond_2e

    #@20
    move v1, v2

    #@21
    .line 839
    goto :goto_4

    #@22
    .line 841
    :cond_22
    iget-object v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@24
    iget-object v4, v0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@26
    invoke-virtual {v3, v4}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_2e

    #@2c
    move v1, v2

    #@2d
    .line 842
    goto :goto_4

    #@2e
    .line 844
    :cond_2e
    iget-wide v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@30
    iget-wide v5, v0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@32
    cmp-long v3, v3, v5

    #@34
    if-eqz v3, :cond_38

    #@36
    move v1, v2

    #@37
    .line 845
    goto :goto_4

    #@38
    .line 847
    :cond_38
    iget v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@3a
    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    #@3d
    move-result v3

    #@3e
    iget v4, v0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@40
    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    #@43
    move-result v4

    #@44
    if-eq v3, v4, :cond_4

    #@46
    move v1, v2

    #@47
    .line 848
    goto :goto_4
.end method

.method public hashCode()I
    .registers 9

    #@0
    .prologue
    .line 817
    const/16 v0, 0x1f

    #@2
    .line 818
    .local v0, prime:I
    const/4 v1, 0x1

    #@3
    .line 819
    .local v1, result:I
    iget-object v2, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@5
    if-nez v2, :cond_22

    #@7
    const/4 v2, 0x0

    #@8
    :goto_8
    add-int/lit8 v1, v2, 0x1f

    #@a
    .line 820
    mul-int/lit8 v2, v1, 0x1f

    #@c
    iget-wide v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@e
    iget-wide v5, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@10
    const/16 v7, 0x20

    #@12
    ushr-long/2addr v5, v7

    #@13
    xor-long/2addr v3, v5

    #@14
    long-to-int v3, v3

    #@15
    add-int v1, v2, v3

    #@17
    .line 821
    mul-int/lit8 v2, v1, 0x1f

    #@19
    iget v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@1b
    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    #@1e
    move-result v3

    #@1f
    add-int v1, v2, v3

    #@21
    .line 822
    return v1

    #@22
    .line 819
    :cond_22
    iget-object v2, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@24
    invoke-virtual {v2}, Landroid/content/ComponentName;->hashCode()I

    #@27
    move-result v2

    #@28
    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 855
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 856
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 857
    const-string v1, "; activity:"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    .line 858
    const-string v1, "; time:"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget-wide v2, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@1d
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@20
    .line 859
    const-string v1, "; weight:"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    new-instance v2, Ljava/math/BigDecimal;

    #@28
    iget v3, p0, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@2a
    float-to-double v3, v3

    #@2b
    invoke-direct {v2, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    .line 860
    const-string v1, "]"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 861
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    return-object v1
.end method
