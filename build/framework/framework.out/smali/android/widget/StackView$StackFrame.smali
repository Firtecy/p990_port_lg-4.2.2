.class Landroid/widget/StackView$StackFrame;
.super Landroid/widget/FrameLayout;
.source "StackView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/StackView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StackFrame"
.end annotation


# instance fields
.field sliderAnimator:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field

.field transformAnimator:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 478
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 479
    return-void
.end method


# virtual methods
.method cancelSliderAnimator()Z
    .registers 3

    #@0
    .prologue
    .line 501
    iget-object v1, p0, Landroid/widget/StackView$StackFrame;->sliderAnimator:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 502
    iget-object v1, p0, Landroid/widget/StackView$StackFrame;->sliderAnimator:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@c
    .line 503
    .local v0, oa:Landroid/animation/ObjectAnimator;
    if-eqz v0, :cond_13

    #@e
    .line 504
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    #@11
    .line 505
    const/4 v1, 0x1

    #@12
    .line 508
    .end local v0           #oa:Landroid/animation/ObjectAnimator;
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method cancelTransformAnimator()Z
    .registers 3

    #@0
    .prologue
    .line 490
    iget-object v1, p0, Landroid/widget/StackView$StackFrame;->transformAnimator:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 491
    iget-object v1, p0, Landroid/widget/StackView$StackFrame;->transformAnimator:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@c
    .line 492
    .local v0, oa:Landroid/animation/ObjectAnimator;
    if-eqz v0, :cond_13

    #@e
    .line 493
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    #@11
    .line 494
    const/4 v1, 0x1

    #@12
    .line 497
    .end local v0           #oa:Landroid/animation/ObjectAnimator;
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method setSliderAnimator(Landroid/animation/ObjectAnimator;)V
    .registers 3
    .parameter "oa"

    #@0
    .prologue
    .line 486
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@2
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@5
    iput-object v0, p0, Landroid/widget/StackView$StackFrame;->sliderAnimator:Ljava/lang/ref/WeakReference;

    #@7
    .line 487
    return-void
.end method

.method setTransformAnimator(Landroid/animation/ObjectAnimator;)V
    .registers 3
    .parameter "oa"

    #@0
    .prologue
    .line 482
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@2
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@5
    iput-object v0, p0, Landroid/widget/StackView$StackFrame;->transformAnimator:Ljava/lang/ref/WeakReference;

    #@7
    .line 483
    return-void
.end method
