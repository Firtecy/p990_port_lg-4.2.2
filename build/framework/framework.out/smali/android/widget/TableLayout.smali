.class public Landroid/widget/TableLayout;
.super Landroid/widget/LinearLayout;
.source "TableLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TableLayout$1;,
        Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;,
        Landroid/widget/TableLayout$LayoutParams;
    }
.end annotation


# instance fields
.field private mCollapsedColumns:Landroid/util/SparseBooleanArray;

.field private mInitialized:Z

.field private mMaxWidths:[I

.field private mPassThroughListener:Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;

.field private mShrinkAllColumns:Z

.field private mShrinkableColumns:Landroid/util/SparseBooleanArray;

.field private mStretchAllColumns:Z

.field private mStretchableColumns:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 94
    invoke-direct {p0}, Landroid/widget/TableLayout;->initTableLayout()V

    #@6
    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v7, 0x2a

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 105
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@7
    .line 107
    sget-object v4, Lcom/android/internal/R$styleable;->TableLayout:[I

    #@9
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 109
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    .line 110
    .local v3, stretchedColumns:Ljava/lang/String;
    if-eqz v3, :cond_1b

    #@13
    .line 111
    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v4

    #@17
    if-ne v4, v7, :cond_3d

    #@19
    .line 112
    iput-boolean v6, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@1b
    .line 118
    :cond_1b
    :goto_1b
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 119
    .local v2, shrinkedColumns:Ljava/lang/String;
    if-eqz v2, :cond_29

    #@21
    .line 120
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v4

    #@25
    if-ne v4, v7, :cond_44

    #@27
    .line 121
    iput-boolean v6, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@29
    .line 127
    :cond_29
    :goto_29
    const/4 v4, 0x2

    #@2a
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 128
    .local v1, collapsedColumns:Ljava/lang/String;
    if-eqz v1, :cond_36

    #@30
    .line 129
    invoke-static {v1}, Landroid/widget/TableLayout;->parseColumns(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    #@33
    move-result-object v4

    #@34
    iput-object v4, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@36
    .line 132
    :cond_36
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@39
    .line 133
    invoke-direct {p0}, Landroid/widget/TableLayout;->initTableLayout()V

    #@3c
    .line 134
    return-void

    #@3d
    .line 114
    .end local v1           #collapsedColumns:Ljava/lang/String;
    .end local v2           #shrinkedColumns:Ljava/lang/String;
    :cond_3d
    invoke-static {v3}, Landroid/widget/TableLayout;->parseColumns(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    #@40
    move-result-object v4

    #@41
    iput-object v4, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@43
    goto :goto_1b

    #@44
    .line 123
    .restart local v2       #shrinkedColumns:Ljava/lang/String;
    :cond_44
    invoke-static {v2}, Landroid/widget/TableLayout;->parseColumns(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    #@47
    move-result-object v4

    #@48
    iput-object v4, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@4a
    goto :goto_29
.end method

.method static synthetic access$200(Landroid/widget/TableLayout;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/TableLayout;->trackCollapsedColumns(Landroid/view/View;)V

    #@3
    return-void
.end method

.method private findLargestCells(I)V
    .registers 23
    .parameter "widthMeasureSpec"

    #@0
    .prologue
    .line 486
    const/4 v7, 0x1

    #@1
    .line 495
    .local v7, firstRow:Z
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TableLayout;->getChildCount()I

    #@4
    move-result v5

    #@5
    .line 496
    .local v5, count:I
    const/4 v8, 0x0

    #@6
    .local v8, i:I
    :goto_6
    if-ge v8, v5, :cond_d5

    #@8
    .line 497
    move-object/from16 v0, p0

    #@a
    invoke-virtual {v0, v8}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v4

    #@e
    .line 498
    .local v4, child:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@11
    move-result v17

    #@12
    const/16 v18, 0x8

    #@14
    move/from16 v0, v17

    #@16
    move/from16 v1, v18

    #@18
    if-ne v0, v1, :cond_1d

    #@1a
    .line 496
    :cond_1a
    :goto_1a
    add-int/lit8 v8, v8, 0x1

    #@1c
    goto :goto_6

    #@1d
    .line 502
    :cond_1d
    instance-of v0, v4, Landroid/widget/TableRow;

    #@1f
    move/from16 v17, v0

    #@21
    if-eqz v17, :cond_1a

    #@23
    move-object v15, v4

    #@24
    .line 503
    check-cast v15, Landroid/widget/TableRow;

    #@26
    .line 505
    .local v15, row:Landroid/widget/TableRow;
    invoke-virtual {v15}, Landroid/widget/TableRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@29
    move-result-object v10

    #@2a
    .line 506
    .local v10, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    const/16 v17, -0x2

    #@2c
    move/from16 v0, v17

    #@2e
    iput v0, v10, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@30
    .line 508
    move/from16 v0, p1

    #@32
    invoke-virtual {v15, v0}, Landroid/widget/TableRow;->getColumnsWidths(I)[I

    #@35
    move-result-object v16

    #@36
    .line 509
    .local v16, widths:[I
    move-object/from16 v0, v16

    #@38
    array-length v13, v0

    #@39
    .line 511
    .local v13, newLength:I
    if-eqz v7, :cond_73

    #@3b
    .line 512
    move-object/from16 v0, p0

    #@3d
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@3f
    move-object/from16 v17, v0

    #@41
    if-eqz v17, :cond_52

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@47
    move-object/from16 v17, v0

    #@49
    move-object/from16 v0, v17

    #@4b
    array-length v0, v0

    #@4c
    move/from16 v17, v0

    #@4e
    move/from16 v0, v17

    #@50
    if-eq v0, v13, :cond_5c

    #@52
    .line 513
    :cond_52
    new-array v0, v13, [I

    #@54
    move-object/from16 v17, v0

    #@56
    move-object/from16 v0, v17

    #@58
    move-object/from16 v1, p0

    #@5a
    iput-object v0, v1, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@5c
    .line 515
    :cond_5c
    const/16 v17, 0x0

    #@5e
    move-object/from16 v0, p0

    #@60
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@62
    move-object/from16 v18, v0

    #@64
    const/16 v19, 0x0

    #@66
    move-object/from16 v0, v16

    #@68
    move/from16 v1, v17

    #@6a
    move-object/from16 v2, v18

    #@6c
    move/from16 v3, v19

    #@6e
    invoke-static {v0, v1, v2, v3, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@71
    .line 516
    const/4 v7, 0x0

    #@72
    goto :goto_1a

    #@73
    .line 518
    :cond_73
    move-object/from16 v0, p0

    #@75
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@77
    move-object/from16 v17, v0

    #@79
    move-object/from16 v0, v17

    #@7b
    array-length v11, v0

    #@7c
    .line 519
    .local v11, length:I
    sub-int v6, v13, v11

    #@7e
    .line 522
    .local v6, difference:I
    if-lez v6, :cond_bd

    #@80
    .line 523
    move-object/from16 v0, p0

    #@82
    iget-object v14, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@84
    .line 524
    .local v14, oldMaxWidths:[I
    new-array v0, v13, [I

    #@86
    move-object/from16 v17, v0

    #@88
    move-object/from16 v0, v17

    #@8a
    move-object/from16 v1, p0

    #@8c
    iput-object v0, v1, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@8e
    .line 525
    const/16 v17, 0x0

    #@90
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@94
    move-object/from16 v18, v0

    #@96
    const/16 v19, 0x0

    #@98
    array-length v0, v14

    #@99
    move/from16 v20, v0

    #@9b
    move/from16 v0, v17

    #@9d
    move-object/from16 v1, v18

    #@9f
    move/from16 v2, v19

    #@a1
    move/from16 v3, v20

    #@a3
    invoke-static {v14, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a6
    .line 527
    array-length v0, v14

    #@a7
    move/from16 v17, v0

    #@a9
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@ad
    move-object/from16 v18, v0

    #@af
    array-length v0, v14

    #@b0
    move/from16 v19, v0

    #@b2
    move-object/from16 v0, v16

    #@b4
    move/from16 v1, v17

    #@b6
    move-object/from16 v2, v18

    #@b8
    move/from16 v3, v19

    #@ba
    invoke-static {v0, v1, v2, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@bd
    .line 535
    .end local v14           #oldMaxWidths:[I
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v12, v0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@c1
    .line 536
    .local v12, maxWidths:[I
    invoke-static {v11, v13}, Ljava/lang/Math;->min(II)I

    #@c4
    move-result v11

    #@c5
    .line 537
    const/4 v9, 0x0

    #@c6
    .local v9, j:I
    :goto_c6
    if-ge v9, v11, :cond_1a

    #@c8
    .line 538
    aget v17, v12, v9

    #@ca
    aget v18, v16, v9

    #@cc
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    #@cf
    move-result v17

    #@d0
    aput v17, v12, v9

    #@d2
    .line 537
    add-int/lit8 v9, v9, 0x1

    #@d4
    goto :goto_c6

    #@d5
    .line 543
    .end local v4           #child:Landroid/view/View;
    .end local v6           #difference:I
    .end local v9           #j:I
    .end local v10           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v11           #length:I
    .end local v12           #maxWidths:[I
    .end local v13           #newLength:I
    .end local v15           #row:Landroid/widget/TableRow;
    .end local v16           #widths:[I
    :cond_d5
    return-void
.end method

.method private initTableLayout()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 177
    iget-object v0, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 178
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@7
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@a
    iput-object v0, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@c
    .line 180
    :cond_c
    iget-object v0, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@e
    if-nez v0, :cond_17

    #@10
    .line 181
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@12
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@15
    iput-object v0, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@17
    .line 183
    :cond_17
    iget-object v0, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@19
    if-nez v0, :cond_22

    #@1b
    .line 184
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@1d
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@20
    iput-object v0, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@22
    .line 189
    :cond_22
    invoke-virtual {p0, v2}, Landroid/widget/TableLayout;->setOrientation(I)V

    #@25
    .line 191
    new-instance v0, Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;

    #@27
    const/4 v1, 0x0

    #@28
    invoke-direct {v0, p0, v1}, Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;-><init>(Landroid/widget/TableLayout;Landroid/widget/TableLayout$1;)V

    #@2b
    iput-object v0, p0, Landroid/widget/TableLayout;->mPassThroughListener:Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;

    #@2d
    .line 194
    iget-object v0, p0, Landroid/widget/TableLayout;->mPassThroughListener:Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;

    #@2f
    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@32
    .line 196
    iput-boolean v2, p0, Landroid/widget/TableLayout;->mInitialized:Z

    #@34
    .line 197
    return-void
.end method

.method private mutateColumnsWidth(Landroid/util/SparseBooleanArray;ZII)V
    .registers 17
    .parameter "columns"
    .parameter "allColumns"
    .parameter "size"
    .parameter "totalWidth"

    #@0
    .prologue
    .line 582
    const/4 v8, 0x0

    #@1
    .line 583
    .local v8, skipped:I
    iget-object v6, p0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@3
    .line 584
    .local v6, maxWidths:[I
    array-length v5, v6

    #@4
    .line 585
    .local v5, length:I
    if-eqz p2, :cond_20

    #@6
    move v2, v5

    #@7
    .line 586
    .local v2, count:I
    :goto_7
    sub-int v9, p3, p4

    #@9
    .line 587
    .local v9, totalExtraSpace:I
    div-int v3, v9, v2

    #@b
    .line 591
    .local v3, extraSpace:I
    invoke-virtual {p0}, Landroid/widget/TableLayout;->getChildCount()I

    #@e
    move-result v7

    #@f
    .line 592
    .local v7, nbChildren:I
    const/4 v4, 0x0

    #@10
    .local v4, i:I
    :goto_10
    if-ge v4, v7, :cond_25

    #@12
    .line 593
    invoke-virtual {p0, v4}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 594
    .local v0, child:Landroid/view/View;
    instance-of v10, v0, Landroid/widget/TableRow;

    #@18
    if-eqz v10, :cond_1d

    #@1a
    .line 595
    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    #@1d
    .line 592
    :cond_1d
    add-int/lit8 v4, v4, 0x1

    #@1f
    goto :goto_10

    #@20
    .line 585
    .end local v0           #child:Landroid/view/View;
    .end local v2           #count:I
    .end local v3           #extraSpace:I
    .end local v4           #i:I
    .end local v7           #nbChildren:I
    .end local v9           #totalExtraSpace:I
    :cond_20
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    #@23
    move-result v2

    #@24
    goto :goto_7

    #@25
    .line 599
    .restart local v2       #count:I
    .restart local v3       #extraSpace:I
    .restart local v4       #i:I
    .restart local v7       #nbChildren:I
    .restart local v9       #totalExtraSpace:I
    :cond_25
    if-nez p2, :cond_41

    #@27
    .line 600
    const/4 v4, 0x0

    #@28
    :goto_28
    if-ge v4, v2, :cond_4c

    #@2a
    .line 601
    invoke-virtual {p1, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@2d
    move-result v1

    #@2e
    .line 602
    .local v1, column:I
    invoke-virtual {p1, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@31
    move-result v10

    #@32
    if-eqz v10, :cond_3b

    #@34
    .line 603
    if-ge v1, v5, :cond_3e

    #@36
    .line 604
    aget v10, v6, v1

    #@38
    add-int/2addr v10, v3

    #@39
    aput v10, v6, v1

    #@3b
    .line 600
    :cond_3b
    :goto_3b
    add-int/lit8 v4, v4, 0x1

    #@3d
    goto :goto_28

    #@3e
    .line 606
    :cond_3e
    add-int/lit8 v8, v8, 0x1

    #@40
    goto :goto_3b

    #@41
    .line 611
    .end local v1           #column:I
    :cond_41
    const/4 v4, 0x0

    #@42
    :goto_42
    if-ge v4, v2, :cond_75

    #@44
    .line 612
    aget v10, v6, v4

    #@46
    add-int/2addr v10, v3

    #@47
    aput v10, v6, v4

    #@49
    .line 611
    add-int/lit8 v4, v4, 0x1

    #@4b
    goto :goto_42

    #@4c
    .line 619
    :cond_4c
    if-lez v8, :cond_75

    #@4e
    if-ge v8, v2, :cond_75

    #@50
    .line 621
    mul-int v10, v8, v3

    #@52
    sub-int v11, v2, v8

    #@54
    div-int v3, v10, v11

    #@56
    .line 622
    const/4 v4, 0x0

    #@57
    :goto_57
    if-ge v4, v2, :cond_75

    #@59
    .line 623
    invoke-virtual {p1, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@5c
    move-result v1

    #@5d
    .line 624
    .restart local v1       #column:I
    invoke-virtual {p1, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@60
    move-result v10

    #@61
    if-eqz v10, :cond_6c

    #@63
    if-ge v1, v5, :cond_6c

    #@65
    .line 625
    aget v10, v6, v1

    #@67
    if-le v3, v10, :cond_6f

    #@69
    .line 626
    const/4 v10, 0x0

    #@6a
    aput v10, v6, v1

    #@6c
    .line 622
    :cond_6c
    :goto_6c
    add-int/lit8 v4, v4, 0x1

    #@6e
    goto :goto_57

    #@6f
    .line 628
    :cond_6f
    aget v10, v6, v1

    #@71
    add-int/2addr v10, v3

    #@72
    aput v10, v6, v1

    #@74
    goto :goto_6c

    #@75
    .line 633
    .end local v1           #column:I
    :cond_75
    return-void
.end method

.method private static parseColumns(Ljava/lang/String;)Landroid/util/SparseBooleanArray;
    .registers 10
    .parameter "sequence"

    #@0
    .prologue
    .line 151
    new-instance v4, Landroid/util/SparseBooleanArray;

    #@2
    invoke-direct {v4}, Landroid/util/SparseBooleanArray;-><init>()V

    #@5
    .line 152
    .local v4, columns:Landroid/util/SparseBooleanArray;
    const-string v8, "\\s*,\\s*"

    #@7
    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@a
    move-result-object v7

    #@b
    .line 153
    .local v7, pattern:Ljava/util/regex/Pattern;
    invoke-virtual {v7, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 155
    .local v1, columnDefs:[Ljava/lang/String;
    move-object v0, v1

    #@10
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@11
    .local v6, len$:I
    const/4 v5, 0x0

    #@12
    .local v5, i$:I
    :goto_12
    if-ge v5, v6, :cond_23

    #@14
    aget-object v2, v0, v5

    #@16
    .line 157
    .local v2, columnIdentifier:Ljava/lang/String;
    :try_start_16
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@19
    move-result v3

    #@1a
    .line 159
    .local v3, columnIndex:I
    if-ltz v3, :cond_20

    #@1c
    .line 162
    const/4 v8, 0x1

    #@1d
    invoke-virtual {v4, v3, v8}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_20
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_20} :catch_24

    #@20
    .line 155
    .end local v3           #columnIndex:I
    :cond_20
    :goto_20
    add-int/lit8 v5, v5, 0x1

    #@22
    goto :goto_12

    #@23
    .line 169
    .end local v2           #columnIdentifier:Ljava/lang/String;
    :cond_23
    return-object v4

    #@24
    .line 164
    .restart local v2       #columnIdentifier:Ljava/lang/String;
    :catch_24
    move-exception v8

    #@25
    goto :goto_20
.end method

.method private requestRowsLayout()V
    .registers 4

    #@0
    .prologue
    .line 210
    iget-boolean v2, p0, Landroid/widget/TableLayout;->mInitialized:Z

    #@2
    if-eqz v2, :cond_15

    #@4
    .line 211
    invoke-virtual {p0}, Landroid/widget/TableLayout;->getChildCount()I

    #@7
    move-result v0

    #@8
    .line 212
    .local v0, count:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_15

    #@b
    .line 213
    invoke-virtual {p0, v1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    #@12
    .line 212
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_9

    #@15
    .line 216
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_15
    return-void
.end method

.method private shrinkAndStretchColumns(I)V
    .registers 10
    .parameter "widthMeasureSpec"

    #@0
    .prologue
    .line 557
    iget-object v6, p0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@2
    if-nez v6, :cond_5

    #@4
    .line 578
    :cond_4
    :goto_4
    return-void

    #@5
    .line 562
    :cond_5
    const/4 v4, 0x0

    #@6
    .line 563
    .local v4, totalWidth:I
    iget-object v0, p0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@8
    .local v0, arr$:[I
    array-length v2, v0

    #@9
    .local v2, len$:I
    const/4 v1, 0x0

    #@a
    .local v1, i$:I
    :goto_a
    if-ge v1, v2, :cond_12

    #@c
    aget v5, v0, v1

    #@e
    .line 564
    .local v5, width:I
    add-int/2addr v4, v5

    #@f
    .line 563
    add-int/lit8 v1, v1, 0x1

    #@11
    goto :goto_a

    #@12
    .line 567
    .end local v5           #width:I
    :cond_12
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@15
    move-result v6

    #@16
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    #@18
    sub-int/2addr v6, v7

    #@19
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@1b
    sub-int v3, v6, v7

    #@1d
    .line 569
    .local v3, size:I
    if-le v4, v3, :cond_33

    #@1f
    iget-boolean v6, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@21
    if-nez v6, :cond_2b

    #@23
    iget-object v6, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@25
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    #@28
    move-result v6

    #@29
    if-lez v6, :cond_33

    #@2b
    .line 572
    :cond_2b
    iget-object v6, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@2d
    iget-boolean v7, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@2f
    invoke-direct {p0, v6, v7, v3, v4}, Landroid/widget/TableLayout;->mutateColumnsWidth(Landroid/util/SparseBooleanArray;ZII)V

    #@32
    goto :goto_4

    #@33
    .line 573
    :cond_33
    if-ge v4, v3, :cond_4

    #@35
    iget-boolean v6, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@37
    if-nez v6, :cond_41

    #@39
    iget-object v6, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@3b
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    #@3e
    move-result v6

    #@3f
    if-lez v6, :cond_4

    #@41
    .line 576
    :cond_41
    iget-object v6, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@43
    iget-boolean v7, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@45
    invoke-direct {p0, v6, v7, v3, v4}, Landroid/widget/TableLayout;->mutateColumnsWidth(Landroid/util/SparseBooleanArray;ZII)V

    #@48
    goto :goto_4
.end method

.method private trackCollapsedColumns(Landroid/view/View;)V
    .registers 9
    .parameter "child"

    #@0
    .prologue
    .line 380
    instance-of v6, p1, Landroid/widget/TableRow;

    #@2
    if-eqz v6, :cond_20

    #@4
    move-object v5, p1

    #@5
    .line 381
    check-cast v5, Landroid/widget/TableRow;

    #@7
    .line 382
    .local v5, row:Landroid/widget/TableRow;
    iget-object v0, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@9
    .line 383
    .local v0, collapsedColumns:Landroid/util/SparseBooleanArray;
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    #@c
    move-result v2

    #@d
    .line 384
    .local v2, count:I
    const/4 v3, 0x0

    #@e
    .local v3, i:I
    :goto_e
    if-ge v3, v2, :cond_20

    #@10
    .line 385
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@13
    move-result v1

    #@14
    .line 386
    .local v1, columnIndex:I
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@17
    move-result v4

    #@18
    .line 390
    .local v4, isCollapsed:Z
    if-eqz v4, :cond_1d

    #@1a
    .line 391
    invoke-virtual {v5, v1, v4}, Landroid/widget/TableRow;->setColumnCollapsed(IZ)V

    #@1d
    .line 384
    :cond_1d
    add-int/lit8 v3, v3, 0x1

    #@1f
    goto :goto_e

    #@20
    .line 395
    .end local v0           #collapsedColumns:Landroid/util/SparseBooleanArray;
    .end local v1           #columnIndex:I
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #isCollapsed:Z
    .end local v5           #row:Landroid/widget/TableRow;
    :cond_20
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 402
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@3
    .line 403
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@6
    .line 404
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .registers 3
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 411
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    #@3
    .line 412
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@6
    .line 413
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 429
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 430
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@6
    .line 431
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 420
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 421
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@6
    .line 422
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 658
    instance-of v0, p1, Landroid/widget/TableLayout$LayoutParams;

    #@2
    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/widget/TableLayout;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 650
    new-instance v0, Landroid/widget/TableLayout$LayoutParams;

    #@2
    invoke-direct {v0}, Landroid/widget/TableLayout$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 74
    invoke-virtual {p0, p1}, Landroid/widget/TableLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 74
    invoke-virtual {p0, p1}, Landroid/widget/TableLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 74
    invoke-virtual {p0, p1}, Landroid/widget/TableLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 666
    new-instance v0, Landroid/widget/TableLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/TableLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 640
    new-instance v0, Landroid/widget/TableLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/TableLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/TableLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public isColumnCollapsed(I)Z
    .registers 3
    .parameter "columnIndex"

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isColumnShrinkable(I)Z
    .registers 3
    .parameter "columnIndex"

    #@0
    .prologue
    .line 366
    iget-boolean v0, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@2
    if-nez v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@6
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isColumnStretchable(I)Z
    .registers 3
    .parameter "columnIndex"

    #@0
    .prologue
    .line 339
    iget-boolean v0, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@2
    if-nez v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@6
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isShrinkAllColumns()Z
    .registers 2

    #@0
    .prologue
    .line 241
    iget-boolean v0, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@2
    return v0
.end method

.method public isStretchAllColumns()Z
    .registers 2

    #@0
    .prologue
    .line 263
    iget-boolean v0, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@2
    return v0
.end method

.method measureChildBeforeLayout(Landroid/view/View;IIIII)V
    .registers 9
    .parameter "child"
    .parameter "childIndex"
    .parameter "widthMeasureSpec"
    .parameter "totalWidth"
    .parameter "heightMeasureSpec"
    .parameter "totalHeight"

    #@0
    .prologue
    .line 460
    instance-of v0, p1, Landroid/widget/TableRow;

    #@2
    if-eqz v0, :cond_c

    #@4
    move-object v0, p1

    #@5
    .line 461
    check-cast v0, Landroid/widget/TableRow;

    #@7
    iget-object v1, p0, Landroid/widget/TableLayout;->mMaxWidths:[I

    #@9
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setColumnsWidthConstraints([I)V

    #@c
    .line 464
    :cond_c
    invoke-super/range {p0 .. p6}, Landroid/widget/LinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    #@f
    .line 466
    return-void
.end method

.method measureVertical(II)V
    .registers 3
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 473
    invoke-direct {p0, p1}, Landroid/widget/TableLayout;->findLargestCells(I)V

    #@3
    .line 474
    invoke-direct {p0, p1}, Landroid/widget/TableLayout;->shrinkAndStretchColumns(I)V

    #@6
    .line 476
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->measureVertical(II)V

    #@9
    .line 477
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 671
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 672
    const-class v0, Landroid/widget/TableLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 673
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 677
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 678
    const-class v0, Landroid/widget/TableLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 679
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 448
    invoke-virtual {p0}, Landroid/widget/TableLayout;->layoutVertical()V

    #@3
    .line 449
    return-void
.end method

.method protected onMeasure(II)V
    .registers 3
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 439
    invoke-virtual {p0, p1, p2}, Landroid/widget/TableLayout;->measureVertical(II)V

    #@3
    .line 440
    return-void
.end method

.method public requestLayout()V
    .registers 4

    #@0
    .prologue
    .line 223
    iget-boolean v2, p0, Landroid/widget/TableLayout;->mInitialized:Z

    #@2
    if-eqz v2, :cond_15

    #@4
    .line 224
    invoke-virtual {p0}, Landroid/widget/TableLayout;->getChildCount()I

    #@7
    move-result v0

    #@8
    .line 225
    .local v0, count:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_15

    #@b
    .line 226
    invoke-virtual {p0, v1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    #@12
    .line 225
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_9

    #@15
    .line 230
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_15
    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@18
    .line 231
    return-void
.end method

.method public setColumnCollapsed(IZ)V
    .registers 7
    .parameter "columnIndex"
    .parameter "isCollapsed"

    #@0
    .prologue
    .line 292
    iget-object v3, p0, Landroid/widget/TableLayout;->mCollapsedColumns:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {v3, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@5
    .line 294
    invoke-virtual {p0}, Landroid/widget/TableLayout;->getChildCount()I

    #@8
    move-result v0

    #@9
    .line 295
    .local v0, count:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_1c

    #@c
    .line 296
    invoke-virtual {p0, v1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v2

    #@10
    .line 297
    .local v2, view:Landroid/view/View;
    instance-of v3, v2, Landroid/widget/TableRow;

    #@12
    if-eqz v3, :cond_19

    #@14
    .line 298
    check-cast v2, Landroid/widget/TableRow;

    #@16
    .end local v2           #view:Landroid/view/View;
    invoke-virtual {v2, p1, p2}, Landroid/widget/TableRow;->setColumnCollapsed(IZ)V

    #@19
    .line 295
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_a

    #@1c
    .line 302
    :cond_1c
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@1f
    .line 303
    return-void
.end method

.method public setColumnShrinkable(IZ)V
    .registers 4
    .parameter "columnIndex"
    .parameter "isShrinkable"

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Landroid/widget/TableLayout;->mShrinkableColumns:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@5
    .line 356
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@8
    .line 357
    return-void
.end method

.method public setColumnStretchable(IZ)V
    .registers 4
    .parameter "columnIndex"
    .parameter "isStretchable"

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Landroid/widget/TableLayout;->mStretchableColumns:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@5
    .line 329
    invoke-direct {p0}, Landroid/widget/TableLayout;->requestRowsLayout()V

    #@8
    .line 330
    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/widget/TableLayout;->mPassThroughListener:Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;

    #@2
    invoke-static {v0, p1}, Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;->access$102(Landroid/widget/TableLayout$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@5
    .line 207
    return-void
.end method

.method public setShrinkAllColumns(Z)V
    .registers 2
    .parameter "shrinkAllColumns"

    #@0
    .prologue
    .line 252
    iput-boolean p1, p0, Landroid/widget/TableLayout;->mShrinkAllColumns:Z

    #@2
    .line 253
    return-void
.end method

.method public setStretchAllColumns(Z)V
    .registers 2
    .parameter "stretchAllColumns"

    #@0
    .prologue
    .line 274
    iput-boolean p1, p0, Landroid/widget/TableLayout;->mStretchAllColumns:Z

    #@2
    .line 275
    return-void
.end method
