.class Landroid/widget/RemoteViews$ReflectionAction;
.super Landroid/widget/RemoteViews$Action;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReflectionAction"
.end annotation


# static fields
.field static final BITMAP:I = 0xc

.field static final BOOLEAN:I = 0x1

.field static final BUNDLE:I = 0xd

.field static final BYTE:I = 0x2

.field static final CHAR:I = 0x8

.field static final CHAR_SEQUENCE:I = 0xa

.field static final DOUBLE:I = 0x7

.field static final FLOAT:I = 0x6

.field static final INT:I = 0x4

.field static final INTENT:I = 0xe

.field static final LONG:I = 0x5

.field static final SHORT:I = 0x3

.field static final STRING:I = 0x9

.field static final TAG:I = 0x2

.field static final URI:I = 0xb


# instance fields
.field methodName:Ljava/lang/String;

.field final synthetic this$0:Landroid/widget/RemoteViews;

.field type:I

.field value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V
    .registers 7
    .parameter
    .parameter "viewId"
    .parameter "methodName"
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 935
    iput-object p1, p0, Landroid/widget/RemoteViews$ReflectionAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 936
    iput p2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    .line 937
    iput-object p3, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@a
    .line 938
    iput p4, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@c
    .line 939
    iput-object p5, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@e
    .line 940
    return-void
.end method

.method constructor <init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V
    .registers 5
    .parameter
    .parameter "in"

    #@0
    .prologue
    .line 942
    iput-object p1, p0, Landroid/widget/RemoteViews$ReflectionAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 943
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@c
    .line 944
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@12
    .line 945
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@18
    .line 954
    iget v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@1a
    packed-switch v0, :pswitch_data_c4

    #@1d
    .line 1006
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 956
    :pswitch_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_2c

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@2b
    goto :goto_1d

    #@2c
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_25

    #@2e
    .line 959
    :pswitch_2e
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@31
    move-result v0

    #@32
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@38
    goto :goto_1d

    #@39
    .line 962
    :pswitch_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    int-to-short v0, v0

    #@3e
    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@44
    goto :goto_1d

    #@45
    .line 965
    :pswitch_45
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v0

    #@49
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v0

    #@4d
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@4f
    goto :goto_1d

    #@50
    .line 968
    :pswitch_50
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@53
    move-result-wide v0

    #@54
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@57
    move-result-object v0

    #@58
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@5a
    goto :goto_1d

    #@5b
    .line 971
    :pswitch_5b
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@5e
    move-result v0

    #@5f
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@62
    move-result-object v0

    #@63
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@65
    goto :goto_1d

    #@66
    .line 974
    :pswitch_66
    invoke-virtual {p2}, Landroid/os/Parcel;->readDouble()D

    #@69
    move-result-wide v0

    #@6a
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@6d
    move-result-object v0

    #@6e
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@70
    goto :goto_1d

    #@71
    .line 977
    :pswitch_71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@74
    move-result v0

    #@75
    int-to-char v0, v0

    #@76
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@79
    move-result-object v0

    #@7a
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@7c
    goto :goto_1d

    #@7d
    .line 980
    :pswitch_7d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@80
    move-result-object v0

    #@81
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@83
    goto :goto_1d

    #@84
    .line 983
    :pswitch_84
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@86
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@89
    move-result-object v0

    #@8a
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@8c
    goto :goto_1d

    #@8d
    .line 986
    :pswitch_8d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v0

    #@91
    if-eqz v0, :cond_1d

    #@93
    .line 987
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@95
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@98
    move-result-object v0

    #@99
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@9b
    goto :goto_1d

    #@9c
    .line 991
    :pswitch_9c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9f
    move-result v0

    #@a0
    if-eqz v0, :cond_1d

    #@a2
    .line 992
    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a4
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a7
    move-result-object v0

    #@a8
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@aa
    goto/16 :goto_1d

    #@ac
    .line 996
    :pswitch_ac
    invoke-virtual {p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@af
    move-result-object v0

    #@b0
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@b2
    goto/16 :goto_1d

    #@b4
    .line 999
    :pswitch_b4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b7
    move-result v0

    #@b8
    if-eqz v0, :cond_1d

    #@ba
    .line 1000
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bc
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bf
    move-result-object v0

    #@c0
    iput-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@c2
    goto/16 :goto_1d

    #@c4
    .line 954
    :pswitch_data_c4
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2e
        :pswitch_39
        :pswitch_45
        :pswitch_50
        :pswitch_5b
        :pswitch_66
        :pswitch_71
        :pswitch_7d
        :pswitch_84
        :pswitch_8d
        :pswitch_9c
        :pswitch_ac
        :pswitch_b4
    .end packed-switch
.end method

.method private getParameterType()Ljava/lang/Class;
    .registers 2

    #@0
    .prologue
    .line 1079
    iget v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@2
    packed-switch v0, :pswitch_data_32

    #@5
    .line 1109
    const/4 v0, 0x0

    #@6
    :goto_6
    return-object v0

    #@7
    .line 1081
    :pswitch_7
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@9
    goto :goto_6

    #@a
    .line 1083
    :pswitch_a
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    #@c
    goto :goto_6

    #@d
    .line 1085
    :pswitch_d
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    #@f
    goto :goto_6

    #@10
    .line 1087
    :pswitch_10
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@12
    goto :goto_6

    #@13
    .line 1089
    :pswitch_13
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    #@15
    goto :goto_6

    #@16
    .line 1091
    :pswitch_16
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@18
    goto :goto_6

    #@19
    .line 1093
    :pswitch_19
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@1b
    goto :goto_6

    #@1c
    .line 1095
    :pswitch_1c
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    #@1e
    goto :goto_6

    #@1f
    .line 1097
    :pswitch_1f
    const-class v0, Ljava/lang/String;

    #@21
    goto :goto_6

    #@22
    .line 1099
    :pswitch_22
    const-class v0, Ljava/lang/CharSequence;

    #@24
    goto :goto_6

    #@25
    .line 1101
    :pswitch_25
    const-class v0, Landroid/net/Uri;

    #@27
    goto :goto_6

    #@28
    .line 1103
    :pswitch_28
    const-class v0, Landroid/graphics/Bitmap;

    #@2a
    goto :goto_6

    #@2b
    .line 1105
    :pswitch_2b
    const-class v0, Landroid/os/Bundle;

    #@2d
    goto :goto_6

    #@2e
    .line 1107
    :pswitch_2e
    const-class v0, Landroid/content/Intent;

    #@30
    goto :goto_6

    #@31
    .line 1079
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_7
        :pswitch_a
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_19
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
    .end packed-switch
.end method


# virtual methods
.method public apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 14
    .parameter "root"
    .parameter "rootParent"
    .parameter "handler"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1115
    iget v6, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@3
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v5

    #@7
    .line 1116
    .local v5, view:Landroid/view/View;
    if-nez v5, :cond_a

    #@9
    .line 1157
    :goto_9
    return-void

    #@a
    .line 1119
    :cond_a
    instance-of v6, v5, Landroid/widget/TextView;

    #@c
    if-eqz v6, :cond_14

    #@e
    move-object v2, v5

    #@f
    .line 1120
    check-cast v2, Landroid/widget/TextView;

    #@11
    .line 1121
    .local v2, mTitle:Landroid/widget/TextView;
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setMarqueeAlwaysEnable(Z)V

    #@14
    .line 1124
    .end local v2           #mTitle:Landroid/widget/TextView;
    :cond_14
    invoke-direct {p0}, Landroid/widget/RemoteViews$ReflectionAction;->getParameterType()Ljava/lang/Class;

    #@17
    move-result-object v4

    #@18
    .line 1125
    .local v4, param:Ljava/lang/Class;
    if-nez v4, :cond_35

    #@1a
    .line 1126
    new-instance v6, Landroid/widget/RemoteViews$ActionException;

    #@1c
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v8, "bad type: "

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    iget v8, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-direct {v6, v7}, Landroid/widget/RemoteViews$ActionException;-><init>(Ljava/lang/String;)V

    #@34
    throw v6

    #@35
    .line 1129
    :cond_35
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@38
    move-result-object v1

    #@39
    .line 1132
    .local v1, klass:Ljava/lang/Class;
    :try_start_39
    iget-object v6, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@3b
    const/4 v7, 0x1

    #@3c
    new-array v7, v7, [Ljava/lang/Class;

    #@3e
    const/4 v8, 0x0

    #@3f
    invoke-direct {p0}, Landroid/widget/RemoteViews$ReflectionAction;->getParameterType()Ljava/lang/Class;

    #@42
    move-result-object v9

    #@43
    aput-object v9, v7, v8

    #@45
    invoke-virtual {v1, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_48
    .catch Ljava/lang/NoSuchMethodException; {:try_start_39 .. :try_end_48} :catch_8f

    #@48
    move-result-object v3

    #@49
    .line 1139
    .local v3, method:Ljava/lang/reflect/Method;
    const-class v6, Landroid/view/RemotableViewMethod;

    #@4b
    invoke-virtual {v3, v6}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@4e
    move-result v6

    #@4f
    if-nez v6, :cond_ce

    #@51
    .line 1140
    new-instance v6, Landroid/widget/RemoteViews$ActionException;

    #@53
    new-instance v7, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string/jumbo v8, "view: "

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    const-string v8, " can\'t use method with RemoteViews: "

    #@69
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    iget-object v8, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    const-string v8, "("

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v7

    #@81
    const-string v8, ")"

    #@83
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v7

    #@87
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v7

    #@8b
    invoke-direct {v6, v7}, Landroid/widget/RemoteViews$ActionException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v6

    #@8f
    .line 1134
    .end local v3           #method:Ljava/lang/reflect/Method;
    :catch_8f
    move-exception v0

    #@90
    .line 1135
    .local v0, ex:Ljava/lang/NoSuchMethodException;
    new-instance v6, Landroid/widget/RemoteViews$ActionException;

    #@92
    new-instance v7, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string/jumbo v8, "view: "

    #@9a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@a1
    move-result-object v8

    #@a2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    const-string v8, " doesn\'t have method: "

    #@a8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v7

    #@ac
    iget-object v8, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@ae
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v7

    #@b2
    const-string v8, "("

    #@b4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v7

    #@b8
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@bb
    move-result-object v8

    #@bc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v7

    #@c0
    const-string v8, ")"

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v7

    #@ca
    invoke-direct {v6, v7}, Landroid/widget/RemoteViews$ActionException;-><init>(Ljava/lang/String;)V

    #@cd
    throw v6

    #@ce
    .line 1152
    .end local v0           #ex:Ljava/lang/NoSuchMethodException;
    .restart local v3       #method:Ljava/lang/reflect/Method;
    :cond_ce
    const/4 v6, 0x1

    #@cf
    :try_start_cf
    new-array v6, v6, [Ljava/lang/Object;

    #@d1
    const/4 v7, 0x0

    #@d2
    iget-object v8, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@d4
    aput-object v8, v6, v7

    #@d6
    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d9
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_d9} :catch_db

    #@d9
    goto/16 :goto_9

    #@db
    .line 1154
    :catch_db
    move-exception v0

    #@dc
    .line 1155
    .local v0, ex:Ljava/lang/Exception;
    new-instance v6, Landroid/widget/RemoteViews$ActionException;

    #@de
    invoke-direct {v6, v0}, Landroid/widget/RemoteViews$ActionException;-><init>(Ljava/lang/Exception;)V

    #@e1
    throw v6
.end method

.method public getActionName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1171
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ReflectionAction"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    iget v1, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public mergeBehavior()I
    .registers 3

    #@0
    .prologue
    .line 1161
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@2
    const-string/jumbo v1, "smoothScrollBy"

    #@5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_d

    #@b
    .line 1162
    const/4 v0, 0x1

    #@c
    .line 1164
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1009
    const/4 v0, 0x2

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1010
    iget v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 1011
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->methodName:Ljava/lang/String;

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 1012
    iget v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 1021
    iget v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->type:I

    #@17
    packed-switch v0, :pswitch_data_dc

    #@1a
    .line 1076
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 1023
    :pswitch_1b
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@1d
    check-cast v0, Ljava/lang/Boolean;

    #@1f
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_2a

    #@25
    move v0, v1

    #@26
    :goto_26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_1a

    #@2a
    :cond_2a
    move v0, v2

    #@2b
    goto :goto_26

    #@2c
    .line 1026
    :pswitch_2c
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@2e
    check-cast v0, Ljava/lang/Byte;

    #@30
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    #@33
    move-result v0

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@37
    goto :goto_1a

    #@38
    .line 1029
    :pswitch_38
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@3a
    check-cast v0, Ljava/lang/Short;

    #@3c
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    #@3f
    move-result v0

    #@40
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    goto :goto_1a

    #@44
    .line 1032
    :pswitch_44
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@46
    check-cast v0, Ljava/lang/Integer;

    #@48
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@4b
    move-result v0

    #@4c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    goto :goto_1a

    #@50
    .line 1035
    :pswitch_50
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@52
    check-cast v0, Ljava/lang/Long;

    #@54
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@57
    move-result-wide v0

    #@58
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5b
    goto :goto_1a

    #@5c
    .line 1038
    :pswitch_5c
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@5e
    check-cast v0, Ljava/lang/Float;

    #@60
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@63
    move-result v0

    #@64
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@67
    goto :goto_1a

    #@68
    .line 1041
    :pswitch_68
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@6a
    check-cast v0, Ljava/lang/Double;

    #@6c
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@6f
    move-result-wide v0

    #@70
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@73
    goto :goto_1a

    #@74
    .line 1044
    :pswitch_74
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@76
    check-cast v0, Ljava/lang/Character;

    #@78
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    #@7b
    move-result v0

    #@7c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_1a

    #@80
    .line 1047
    :pswitch_80
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@82
    check-cast v0, Ljava/lang/String;

    #@84
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@87
    goto :goto_1a

    #@88
    .line 1050
    :pswitch_88
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@8a
    check-cast v0, Ljava/lang/CharSequence;

    #@8c
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@8f
    goto :goto_1a

    #@90
    .line 1053
    :pswitch_90
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@92
    if-eqz v0, :cond_a4

    #@94
    :goto_94
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@97
    .line 1054
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@99
    if-eqz v0, :cond_1a

    #@9b
    .line 1055
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@9d
    check-cast v0, Landroid/net/Uri;

    #@9f
    invoke-virtual {v0, p1, p2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@a2
    goto/16 :goto_1a

    #@a4
    :cond_a4
    move v1, v2

    #@a5
    .line 1053
    goto :goto_94

    #@a6
    .line 1059
    :pswitch_a6
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@a8
    if-eqz v0, :cond_ba

    #@aa
    :goto_aa
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@ad
    .line 1060
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@af
    if-eqz v0, :cond_1a

    #@b1
    .line 1061
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@b3
    check-cast v0, Landroid/graphics/Bitmap;

    #@b5
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@b8
    goto/16 :goto_1a

    #@ba
    :cond_ba
    move v1, v2

    #@bb
    .line 1059
    goto :goto_aa

    #@bc
    .line 1065
    :pswitch_bc
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@be
    check-cast v0, Landroid/os/Bundle;

    #@c0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@c3
    goto/16 :goto_1a

    #@c5
    .line 1068
    :pswitch_c5
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@c7
    if-eqz v0, :cond_d9

    #@c9
    :goto_c9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@cc
    .line 1069
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@ce
    if-eqz v0, :cond_1a

    #@d0
    .line 1070
    iget-object v0, p0, Landroid/widget/RemoteViews$ReflectionAction;->value:Ljava/lang/Object;

    #@d2
    check-cast v0, Landroid/content/Intent;

    #@d4
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@d7
    goto/16 :goto_1a

    #@d9
    :cond_d9
    move v1, v2

    #@da
    .line 1068
    goto :goto_c9

    #@db
    .line 1021
    nop

    #@dc
    :pswitch_data_dc
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_2c
        :pswitch_38
        :pswitch_44
        :pswitch_50
        :pswitch_5c
        :pswitch_68
        :pswitch_74
        :pswitch_80
        :pswitch_88
        :pswitch_90
        :pswitch_a6
        :pswitch_bc
        :pswitch_c5
    .end packed-switch
.end method
