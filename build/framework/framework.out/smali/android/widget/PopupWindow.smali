.class public Landroid/widget/PopupWindow;
.super Ljava/lang/Object;
.source "PopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/PopupWindow$PopupViewContainer;,
        Landroid/widget/PopupWindow$OnDismissListener;
    }
.end annotation


# static fields
.field private static final ABOVE_ANCHOR_STATE_SET:[I = null

.field public static final INPUT_METHOD_FROM_FOCUSABLE:I = 0x0

.field public static final INPUT_METHOD_NEEDED:I = 0x1

.field public static final INPUT_METHOD_NOT_NEEDED:I = 0x2

.field public static final POPUP_BASE_NORMAL_APP:I = 0x0

.field public static final POPUP_BASE_QSLIDE:I = 0x1


# instance fields
.field private mAboveAnchor:Z

.field private mAboveAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mAllowScrollingAnchorParent:Z

.field private mAnchor:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mAnchorXoff:I

.field private mAnchorYoff:I

.field private mAnimationStyle:I

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBaseAppType:I

.field private mBelowAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mClipToScreen:Z

.field private mClippingEnabled:Z

.field private mContentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mDrawingLocation:[I

.field private mFocusable:Z

.field private mHeight:I

.field private mHeightMode:I

.field private mIgnoreCheekPress:Z

.field private mInputMethodMode:I

.field private mIsDropdown:Z

.field private mIsShowing:Z

.field private mLastHeight:I

.field private mLastWidth:I

.field private mLayoutInScreen:Z

.field private mLayoutInsetDecor:Z

.field private mNotTouchModal:Z

.field private mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private mOnScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private mOutsideTouchable:Z

.field private mPopupHeight:I

.field private mPopupView:Landroid/view/View;

.field private mPopupViewInitialLayoutDirectionInherited:Z

.field private mPopupWidth:I

.field private mScreenLocation:[I

.field private mSoftInputMode:I

.field private mSplitTouchEnabled:I

.field private mTempRect:Landroid/graphics/Rect;

.field private mTouchInterceptor:Landroid/view/View$OnTouchListener;

.field private mTouchable:Z

.field private mWidth:I

.field private mWidthMode:I

.field private mWindowLayoutType:I

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 131
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x10100aa

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Landroid/widget/PopupWindow;->ABOVE_ANCHOR_STATE_SET:[I

    #@b
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 267
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v0, v1, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    #@5
    .line 268
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 294
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    #@4
    .line 295
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 180
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 189
    const v0, 0x1010076

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 190
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 198
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@4
    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"
    .parameter "defStyleRes"

    #@0
    .prologue
    .line 206
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 91
    const/4 v7, 0x0

    #@4
    iput v7, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@6
    .line 92
    const/4 v7, 0x1

    #@7
    iput v7, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@9
    .line 93
    const/4 v7, 0x1

    #@a
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mTouchable:Z

    #@c
    .line 94
    const/4 v7, 0x0

    #@d
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mOutsideTouchable:Z

    #@f
    .line 95
    const/4 v7, 0x1

    #@10
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mClippingEnabled:Z

    #@12
    .line 96
    const/4 v7, -0x1

    #@13
    iput v7, p0, Landroid/widget/PopupWindow;->mSplitTouchEnabled:I

    #@15
    .line 99
    const/4 v7, 0x1

    #@16
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mAllowScrollingAnchorParent:Z

    #@18
    .line 100
    const/4 v7, 0x0

    #@19
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mLayoutInsetDecor:Z

    #@1b
    .line 115
    const/4 v7, 0x2

    #@1c
    new-array v7, v7, [I

    #@1e
    iput-object v7, p0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@20
    .line 116
    const/4 v7, 0x2

    #@21
    new-array v7, v7, [I

    #@23
    iput-object v7, p0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@25
    .line 117
    new-instance v7, Landroid/graphics/Rect;

    #@27
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@2a
    iput-object v7, p0, Landroid/widget/PopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@2c
    .line 124
    const/16 v7, 0x3e8

    #@2e
    iput v7, p0, Landroid/widget/PopupWindow;->mWindowLayoutType:I

    #@30
    .line 127
    const/4 v7, 0x0

    #@31
    iput-boolean v7, p0, Landroid/widget/PopupWindow;->mIgnoreCheekPress:Z

    #@33
    .line 129
    const/4 v7, -0x1

    #@34
    iput v7, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@36
    .line 136
    new-instance v7, Landroid/widget/PopupWindow$1;

    #@38
    invoke-direct {v7, p0}, Landroid/widget/PopupWindow$1;-><init>(Landroid/widget/PopupWindow;)V

    #@3b
    iput-object v7, p0, Landroid/widget/PopupWindow;->mOnScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@3d
    .line 164
    const/4 v7, 0x0

    #@3e
    iput v7, p0, Landroid/widget/PopupWindow;->mBaseAppType:I

    #@40
    .line 207
    iput-object p1, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@42
    .line 208
    const-string/jumbo v7, "window"

    #@45
    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@48
    move-result-object v7

    #@49
    check-cast v7, Landroid/view/WindowManager;

    #@4b
    iput-object v7, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@4d
    .line 210
    sget-object v7, Lcom/android/internal/R$styleable;->PopupWindow:[I

    #@4f
    invoke-virtual {p1, p2, v7, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@52
    move-result-object v0

    #@53
    .line 214
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v7, 0x0

    #@54
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@57
    move-result-object v7

    #@58
    iput-object v7, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5a
    .line 216
    const/4 v7, 0x1

    #@5b
    const/4 v8, -0x1

    #@5c
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5f
    move-result v2

    #@60
    .line 217
    .local v2, animStyle:I
    const v7, 0x10301ee

    #@63
    if-ne v2, v7, :cond_66

    #@65
    const/4 v2, -0x1

    #@66
    .end local v2           #animStyle:I
    :cond_66
    iput v2, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@68
    .line 229
    iget-object v7, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@6a
    instance-of v7, v7, Landroid/graphics/drawable/StateListDrawable;

    #@6c
    if-eqz v7, :cond_95

    #@6e
    .line 230
    iget-object v3, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@70
    check-cast v3, Landroid/graphics/drawable/StateListDrawable;

    #@72
    .line 233
    .local v3, background:Landroid/graphics/drawable/StateListDrawable;
    sget-object v7, Landroid/widget/PopupWindow;->ABOVE_ANCHOR_STATE_SET:[I

    #@74
    invoke-virtual {v3, v7}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    #@77
    move-result v1

    #@78
    .line 237
    .local v1, aboveAnchorStateIndex:I
    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    #@7b
    move-result v5

    #@7c
    .line 238
    .local v5, count:I
    const/4 v4, -0x1

    #@7d
    .line 239
    .local v4, belowAnchorStateIndex:I
    const/4 v6, 0x0

    #@7e
    .local v6, i:I
    :goto_7e
    if-ge v6, v5, :cond_83

    #@80
    .line 240
    if-eq v6, v1, :cond_99

    #@82
    .line 241
    move v4, v6

    #@83
    .line 248
    :cond_83
    const/4 v7, -0x1

    #@84
    if-eq v1, v7, :cond_9c

    #@86
    const/4 v7, -0x1

    #@87
    if-eq v4, v7, :cond_9c

    #@89
    .line 249
    invoke-virtual {v3, v1}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@8c
    move-result-object v7

    #@8d
    iput-object v7, p0, Landroid/widget/PopupWindow;->mAboveAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@8f
    .line 250
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@92
    move-result-object v7

    #@93
    iput-object v7, p0, Landroid/widget/PopupWindow;->mBelowAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@95
    .line 257
    .end local v1           #aboveAnchorStateIndex:I
    .end local v3           #background:Landroid/graphics/drawable/StateListDrawable;
    .end local v4           #belowAnchorStateIndex:I
    .end local v5           #count:I
    .end local v6           #i:I
    :cond_95
    :goto_95
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@98
    .line 258
    return-void

    #@99
    .line 239
    .restart local v1       #aboveAnchorStateIndex:I
    .restart local v3       #background:Landroid/graphics/drawable/StateListDrawable;
    .restart local v4       #belowAnchorStateIndex:I
    .restart local v5       #count:I
    .restart local v6       #i:I
    :cond_99
    add-int/lit8 v6, v6, 0x1

    #@9b
    goto :goto_7e

    #@9c
    .line 252
    :cond_9c
    const/4 v7, 0x0

    #@9d
    iput-object v7, p0, Landroid/widget/PopupWindow;->mBelowAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@9f
    .line 253
    const/4 v7, 0x0

    #@a0
    iput-object v7, p0, Landroid/widget/PopupWindow;->mAboveAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@a2
    goto :goto_95
.end method

.method public constructor <init>(Landroid/view/View;)V
    .registers 3
    .parameter "contentView"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 280
    invoke-direct {p0, p1, v0, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    #@4
    .line 281
    return-void
.end method

.method public constructor <init>(Landroid/view/View;II)V
    .registers 5
    .parameter "contentView"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 310
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    #@4
    .line 311
    return-void
.end method

.method public constructor <init>(Landroid/view/View;IIZ)V
    .registers 9
    .parameter "contentView"
    .parameter "width"
    .parameter "height"
    .parameter "focusable"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v2, -0x1

    #@2
    const/4 v0, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 325
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 91
    iput v1, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@9
    .line 92
    iput v0, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@b
    .line 93
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mTouchable:Z

    #@d
    .line 94
    iput-boolean v1, p0, Landroid/widget/PopupWindow;->mOutsideTouchable:Z

    #@f
    .line 95
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mClippingEnabled:Z

    #@11
    .line 96
    iput v2, p0, Landroid/widget/PopupWindow;->mSplitTouchEnabled:I

    #@13
    .line 99
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mAllowScrollingAnchorParent:Z

    #@15
    .line 100
    iput-boolean v1, p0, Landroid/widget/PopupWindow;->mLayoutInsetDecor:Z

    #@17
    .line 115
    new-array v0, v3, [I

    #@19
    iput-object v0, p0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@1b
    .line 116
    new-array v0, v3, [I

    #@1d
    iput-object v0, p0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@1f
    .line 117
    new-instance v0, Landroid/graphics/Rect;

    #@21
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@24
    iput-object v0, p0, Landroid/widget/PopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@26
    .line 124
    const/16 v0, 0x3e8

    #@28
    iput v0, p0, Landroid/widget/PopupWindow;->mWindowLayoutType:I

    #@2a
    .line 127
    iput-boolean v1, p0, Landroid/widget/PopupWindow;->mIgnoreCheekPress:Z

    #@2c
    .line 129
    iput v2, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@2e
    .line 136
    new-instance v0, Landroid/widget/PopupWindow$1;

    #@30
    invoke-direct {v0, p0}, Landroid/widget/PopupWindow$1;-><init>(Landroid/widget/PopupWindow;)V

    #@33
    iput-object v0, p0, Landroid/widget/PopupWindow;->mOnScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@35
    .line 164
    iput v1, p0, Landroid/widget/PopupWindow;->mBaseAppType:I

    #@37
    .line 326
    if-eqz p1, :cond_4c

    #@39
    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@3f
    .line 328
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@41
    const-string/jumbo v1, "window"

    #@44
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@47
    move-result-object v0

    #@48
    check-cast v0, Landroid/view/WindowManager;

    #@4a
    iput-object v0, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@4c
    .line 330
    :cond_4c
    invoke-virtual {p0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@4f
    .line 331
    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@52
    .line 332
    invoke-virtual {p0, p3}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@55
    .line 333
    invoke-virtual {p0, p4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    #@58
    .line 334
    return-void
.end method

.method static synthetic access$000(Landroid/widget/PopupWindow;)Ljava/lang/ref/WeakReference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/widget/PopupWindow;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/PopupWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Landroid/widget/PopupWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/PopupWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/widget/PopupWindow;->mAnchorXoff:I

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/PopupWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/widget/PopupWindow;->mAnchorYoff:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/widget/PopupWindow;Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/PopupWindow;->findDropDownPosition(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Landroid/widget/PopupWindow;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;->updateAboveAnchor(Z)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/PopupWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@2
    return v0
.end method

.method static synthetic access$700()[I
    .registers 1

    #@0
    .prologue
    .line 57
    sget-object v0, Landroid/widget/PopupWindow;->ABOVE_ANCHOR_STATE_SET:[I

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/PopupWindow;)Landroid/view/View$OnTouchListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/widget/PopupWindow;->mTouchInterceptor:Landroid/view/View$OnTouchListener;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/PopupWindow;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method private computeAnimationResource()I
    .registers 3

    #@0
    .prologue
    .line 1118
    iget v0, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_17

    #@5
    .line 1119
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mIsDropdown:Z

    #@7
    if-eqz v0, :cond_15

    #@9
    .line 1120
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@b
    if-eqz v0, :cond_11

    #@d
    const v0, 0x10301e8

    #@10
    .line 1126
    :goto_10
    return v0

    #@11
    .line 1120
    :cond_11
    const v0, 0x10301e7

    #@14
    goto :goto_10

    #@15
    .line 1124
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_10

    #@17
    .line 1126
    :cond_17
    iget v0, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@19
    goto :goto_10
.end method

.method private computeFlags(I)I
    .registers 5
    .parameter "curFlags"

    #@0
    .prologue
    const/high16 v2, 0x2

    #@2
    .line 1074
    const v0, -0x868219

    #@5
    and-int/2addr p1, v0

    #@6
    .line 1082
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mIgnoreCheekPress:Z

    #@8
    if-eqz v0, :cond_e

    #@a
    .line 1083
    const v0, 0x8000

    #@d
    or-int/2addr p1, v0

    #@e
    .line 1085
    :cond_e
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mFocusable:Z

    #@10
    if-nez v0, :cond_4a

    #@12
    .line 1086
    or-int/lit8 p1, p1, 0x8

    #@14
    .line 1087
    iget v0, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@16
    const/4 v1, 0x1

    #@17
    if-ne v0, v1, :cond_1a

    #@19
    .line 1088
    or-int/2addr p1, v2

    #@1a
    .line 1093
    :cond_1a
    :goto_1a
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mTouchable:Z

    #@1c
    if-nez v0, :cond_20

    #@1e
    .line 1094
    or-int/lit8 p1, p1, 0x10

    #@20
    .line 1096
    :cond_20
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mOutsideTouchable:Z

    #@22
    if-eqz v0, :cond_27

    #@24
    .line 1097
    const/high16 v0, 0x4

    #@26
    or-int/2addr p1, v0

    #@27
    .line 1099
    :cond_27
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mClippingEnabled:Z

    #@29
    if-nez v0, :cond_2d

    #@2b
    .line 1100
    or-int/lit16 p1, p1, 0x200

    #@2d
    .line 1102
    :cond_2d
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isSplitTouchEnabled()Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_36

    #@33
    .line 1103
    const/high16 v0, 0x80

    #@35
    or-int/2addr p1, v0

    #@36
    .line 1105
    :cond_36
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mLayoutInScreen:Z

    #@38
    if-eqz v0, :cond_3c

    #@3a
    .line 1106
    or-int/lit16 p1, p1, 0x100

    #@3c
    .line 1108
    :cond_3c
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mLayoutInsetDecor:Z

    #@3e
    if-eqz v0, :cond_43

    #@40
    .line 1109
    const/high16 v0, 0x1

    #@42
    or-int/2addr p1, v0

    #@43
    .line 1111
    :cond_43
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mNotTouchModal:Z

    #@45
    if-eqz v0, :cond_49

    #@47
    .line 1112
    or-int/lit8 p1, p1, 0x20

    #@49
    .line 1114
    :cond_49
    return p1

    #@4a
    .line 1090
    :cond_4a
    iget v0, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@4c
    const/4 v1, 0x2

    #@4d
    if-ne v0, v1, :cond_1a

    #@4f
    .line 1091
    or-int/2addr p1, v2

    #@50
    goto :goto_1a
.end method

.method private createPopupLayout(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;
    .registers 5
    .parameter "token"

    #@0
    .prologue
    .line 1042
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@2
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@5
    .line 1047
    .local v0, p:Landroid/view/WindowManager$LayoutParams;
    const v1, 0x800033

    #@8
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@a
    .line 1048
    iget v1, p0, Landroid/widget/PopupWindow;->mWidth:I

    #@c
    iput v1, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@e
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@10
    .line 1049
    iget v1, p0, Landroid/widget/PopupWindow;->mHeight:I

    #@12
    iput v1, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@14
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@16
    .line 1050
    iget-object v1, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@18
    if-eqz v1, :cond_58

    #@1a
    .line 1051
    iget-object v1, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@1c
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@1f
    move-result v1

    #@20
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@22
    .line 1055
    :goto_22
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@24
    invoke-direct {p0, v1}, Landroid/widget/PopupWindow;->computeFlags(I)I

    #@27
    move-result v1

    #@28
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2a
    .line 1056
    iget v1, p0, Landroid/widget/PopupWindow;->mWindowLayoutType:I

    #@2c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2e
    .line 1057
    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@30
    .line 1058
    iget v1, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@32
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@34
    .line 1060
    iget v1, p0, Landroid/widget/PopupWindow;->mBaseAppType:I

    #@36
    packed-switch v1, :pswitch_data_7c

    #@39
    .line 1066
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "PopupWindow:"

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@47
    move-result v2

    #@48
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@57
    .line 1070
    :goto_57
    return-object v0

    #@58
    .line 1053
    :cond_58
    const/4 v1, -0x3

    #@59
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@5b
    goto :goto_22

    #@5c
    .line 1062
    :pswitch_5c
    new-instance v1, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v2, "Floating:PopupWindow:"

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@6a
    move-result v2

    #@6b
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@7a
    goto :goto_57

    #@7b
    .line 1060
    nop

    #@7c
    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_5c
    .end packed-switch
.end method

.method private findDropDownPosition(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z
    .registers 23
    .parameter "anchor"
    .parameter "p"
    .parameter "xoff"
    .parameter "yoff"

    #@0
    .prologue
    .line 1146
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    #@3
    move-result v1

    #@4
    .line 1147
    .local v1, anchorHeight:I
    move-object/from16 v0, p0

    #@6
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-virtual {v0, v15}, Landroid/view/View;->getLocationInWindow([I)V

    #@d
    .line 1148
    move-object/from16 v0, p0

    #@f
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@11
    const/16 v16, 0x0

    #@13
    aget v15, v15, v16

    #@15
    add-int v15, v15, p3

    #@17
    move-object/from16 v0, p2

    #@19
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1b
    .line 1149
    move-object/from16 v0, p0

    #@1d
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@1f
    const/16 v16, 0x1

    #@21
    aget v15, v15, v16

    #@23
    add-int/2addr v15, v1

    #@24
    add-int v15, v15, p4

    #@26
    move-object/from16 v0, p2

    #@28
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@2a
    .line 1152
    const/4 v5, 0x0

    #@2b
    .line 1154
    .local v5, onTop:Z
    const v15, 0x800033

    #@2e
    move-object/from16 v0, p2

    #@30
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@32
    .line 1156
    move-object/from16 v0, p0

    #@34
    iget-object v15, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@36
    move-object/from16 v0, p1

    #@38
    invoke-virtual {v0, v15}, Landroid/view/View;->getLocationOnScreen([I)V

    #@3b
    .line 1157
    new-instance v2, Landroid/graphics/Rect;

    #@3d
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@40
    .line 1158
    .local v2, displayFrame:Landroid/graphics/Rect;
    move-object/from16 v0, p1

    #@42
    invoke-virtual {v0, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    #@45
    .line 1160
    move-object/from16 v0, p0

    #@47
    iget-object v15, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@49
    const/16 v16, 0x1

    #@4b
    aget v15, v15, v16

    #@4d
    add-int/2addr v15, v1

    #@4e
    add-int v10, v15, p4

    #@50
    .line 1162
    .local v10, screenY:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@53
    move-result-object v9

    #@54
    .line 1163
    .local v9, root:Landroid/view/View;
    move-object/from16 v0, p0

    #@56
    iget v15, v0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@58
    add-int/2addr v15, v10

    #@59
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    #@5b
    move/from16 v16, v0

    #@5d
    move/from16 v0, v16

    #@5f
    if-gt v15, v0, :cond_75

    #@61
    move-object/from16 v0, p2

    #@63
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@65
    move-object/from16 v0, p0

    #@67
    iget v0, v0, Landroid/widget/PopupWindow;->mPopupWidth:I

    #@69
    move/from16 v16, v0

    #@6b
    add-int v15, v15, v16

    #@6d
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    #@70
    move-result v16

    #@71
    sub-int v15, v15, v16

    #@73
    if-lez v15, :cond_127

    #@75
    .line 1168
    :cond_75
    move-object/from16 v0, p0

    #@77
    iget-boolean v15, v0, Landroid/widget/PopupWindow;->mAllowScrollingAnchorParent:Z

    #@79
    if-eqz v15, :cond_a7

    #@7b
    .line 1169
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollX()I

    #@7e
    move-result v11

    #@7f
    .line 1170
    .local v11, scrollX:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollY()I

    #@82
    move-result v12

    #@83
    .line 1171
    .local v12, scrollY:I
    new-instance v7, Landroid/graphics/Rect;

    #@85
    move-object/from16 v0, p0

    #@87
    iget v15, v0, Landroid/widget/PopupWindow;->mPopupWidth:I

    #@89
    add-int/2addr v15, v11

    #@8a
    add-int v15, v15, p3

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget v0, v0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@90
    move/from16 v16, v0

    #@92
    add-int v16, v16, v12

    #@94
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    #@97
    move-result v17

    #@98
    add-int v16, v16, v17

    #@9a
    add-int v16, v16, p4

    #@9c
    move/from16 v0, v16

    #@9e
    invoke-direct {v7, v11, v12, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    #@a1
    .line 1173
    .local v7, r:Landroid/graphics/Rect;
    const/4 v15, 0x1

    #@a2
    move-object/from16 v0, p1

    #@a4
    invoke-virtual {v0, v7, v15}, Landroid/view/View;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    #@a7
    .line 1178
    .end local v7           #r:Landroid/graphics/Rect;
    .end local v11           #scrollX:I
    .end local v12           #scrollY:I
    :cond_a7
    move-object/from16 v0, p0

    #@a9
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@ab
    move-object/from16 v0, p1

    #@ad
    invoke-virtual {v0, v15}, Landroid/view/View;->getLocationInWindow([I)V

    #@b0
    .line 1179
    move-object/from16 v0, p0

    #@b2
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@b4
    const/16 v16, 0x0

    #@b6
    aget v15, v15, v16

    #@b8
    add-int v15, v15, p3

    #@ba
    move-object/from16 v0, p2

    #@bc
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@be
    .line 1180
    move-object/from16 v0, p0

    #@c0
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@c2
    const/16 v16, 0x1

    #@c4
    aget v15, v15, v16

    #@c6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    #@c9
    move-result v16

    #@ca
    add-int v15, v15, v16

    #@cc
    add-int v15, v15, p4

    #@ce
    move-object/from16 v0, p2

    #@d0
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@d2
    .line 1183
    move-object/from16 v0, p0

    #@d4
    iget-object v15, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@d6
    move-object/from16 v0, p1

    #@d8
    invoke-virtual {v0, v15}, Landroid/view/View;->getLocationOnScreen([I)V

    #@db
    .line 1185
    iget v15, v2, Landroid/graphics/Rect;->bottom:I

    #@dd
    move-object/from16 v0, p0

    #@df
    iget-object v0, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@e1
    move-object/from16 v16, v0

    #@e3
    const/16 v17, 0x1

    #@e5
    aget v16, v16, v17

    #@e7
    sub-int v15, v15, v16

    #@e9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    #@ec
    move-result v16

    #@ed
    sub-int v15, v15, v16

    #@ef
    sub-int v15, v15, p4

    #@f1
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@f5
    move-object/from16 v16, v0

    #@f7
    const/16 v17, 0x1

    #@f9
    aget v16, v16, v17

    #@fb
    sub-int v16, v16, p4

    #@fd
    iget v0, v2, Landroid/graphics/Rect;->top:I

    #@ff
    move/from16 v17, v0

    #@101
    sub-int v16, v16, v17

    #@103
    move/from16 v0, v16

    #@105
    if-ge v15, v0, :cond_1ab

    #@107
    const/4 v5, 0x1

    #@108
    .line 1187
    :goto_108
    if-eqz v5, :cond_1ae

    #@10a
    .line 1188
    const v15, 0x800053

    #@10d
    move-object/from16 v0, p2

    #@10f
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@111
    .line 1189
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    #@114
    move-result v15

    #@115
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@119
    move-object/from16 v16, v0

    #@11b
    const/16 v17, 0x1

    #@11d
    aget v16, v16, v17

    #@11f
    sub-int v15, v15, v16

    #@121
    add-int v15, v15, p4

    #@123
    move-object/from16 v0, p2

    #@125
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@127
    .line 1196
    :cond_127
    :goto_127
    new-instance v13, Landroid/graphics/Rect;

    #@129
    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    #@12c
    .line 1197
    .local v13, splitWindowFrame:Landroid/graphics/Rect;
    const/4 v4, 0x0

    #@12d
    .line 1200
    .local v4, isSplitWindow:Z
    move-object/from16 v0, p0

    #@12f
    iget-boolean v15, v0, Landroid/widget/PopupWindow;->mClipToScreen:Z

    #@131
    if-eqz v15, :cond_1e4

    #@133
    .line 1201
    iget v15, v2, Landroid/graphics/Rect;->right:I

    #@135
    iget v0, v2, Landroid/graphics/Rect;->left:I

    #@137
    move/from16 v16, v0

    #@139
    sub-int v3, v15, v16

    #@13b
    .line 1204
    .local v3, displayFrameWidth:I
    move-object/from16 v0, p1

    #@13d
    invoke-virtual {v0, v13}, Landroid/view/View;->isWindowSplit(Landroid/graphics/Rect;)Z

    #@140
    move-result v4

    #@141
    .line 1206
    move-object/from16 v0, p2

    #@143
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@145
    move-object/from16 v0, p2

    #@147
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@149
    move/from16 v16, v0

    #@14b
    add-int v8, v15, v16

    #@14d
    .line 1207
    .local v8, right:I
    if-le v8, v3, :cond_15b

    #@14f
    .line 1208
    move-object/from16 v0, p2

    #@151
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@153
    sub-int v16, v8, v3

    #@155
    sub-int v15, v15, v16

    #@157
    move-object/from16 v0, p2

    #@159
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@15b
    .line 1210
    :cond_15b
    move-object/from16 v0, p2

    #@15d
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@15f
    iget v0, v2, Landroid/graphics/Rect;->left:I

    #@161
    move/from16 v16, v0

    #@163
    move/from16 v0, v16

    #@165
    if-ge v15, v0, :cond_17f

    #@167
    .line 1211
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@169
    if-eqz v15, :cond_16d

    #@16b
    if-nez v4, :cond_1c4

    #@16d
    .line 1212
    :cond_16d
    iget v15, v2, Landroid/graphics/Rect;->left:I

    #@16f
    move-object/from16 v0, p2

    #@171
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@173
    .line 1213
    move-object/from16 v0, p2

    #@175
    iget v15, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@177
    invoke-static {v15, v3}, Ljava/lang/Math;->min(II)I

    #@17a
    move-result v15

    #@17b
    move-object/from16 v0, p2

    #@17d
    iput v15, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@17f
    .line 1219
    :cond_17f
    :goto_17f
    if-eqz v5, :cond_1d3

    #@181
    .line 1220
    move-object/from16 v0, p0

    #@183
    iget-object v15, v0, Landroid/widget/PopupWindow;->mScreenLocation:[I

    #@185
    const/16 v16, 0x1

    #@187
    aget v15, v15, v16

    #@189
    add-int v15, v15, p4

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget v0, v0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@18f
    move/from16 v16, v0

    #@191
    sub-int v6, v15, v16

    #@193
    .line 1221
    .local v6, popupTop:I
    if-gez v6, :cond_19e

    #@195
    .line 1222
    move-object/from16 v0, p2

    #@197
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@199
    add-int/2addr v15, v6

    #@19a
    move-object/from16 v0, p2

    #@19c
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@19e
    .line 1241
    .end local v3           #displayFrameWidth:I
    .end local v6           #popupTop:I
    .end local v8           #right:I
    :cond_19e
    :goto_19e
    move-object/from16 v0, p2

    #@1a0
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1a2
    const/high16 v16, 0x1000

    #@1a4
    or-int v15, v15, v16

    #@1a6
    move-object/from16 v0, p2

    #@1a8
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1aa
    .line 1243
    return v5

    #@1ab
    .line 1185
    .end local v4           #isSplitWindow:Z
    .end local v13           #splitWindowFrame:Landroid/graphics/Rect;
    :cond_1ab
    const/4 v5, 0x0

    #@1ac
    goto/16 :goto_108

    #@1ae
    .line 1191
    :cond_1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v15, v0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@1b2
    const/16 v16, 0x1

    #@1b4
    aget v15, v15, v16

    #@1b6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    #@1b9
    move-result v16

    #@1ba
    add-int v15, v15, v16

    #@1bc
    add-int v15, v15, p4

    #@1be
    move-object/from16 v0, p2

    #@1c0
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@1c2
    goto/16 :goto_127

    #@1c4
    .line 1215
    .restart local v3       #displayFrameWidth:I
    .restart local v4       #isSplitWindow:Z
    .restart local v8       #right:I
    .restart local v13       #splitWindowFrame:Landroid/graphics/Rect;
    :cond_1c4
    move-object/from16 v0, p2

    #@1c6
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1c8
    iget v0, v13, Landroid/graphics/Rect;->left:I

    #@1ca
    move/from16 v16, v0

    #@1cc
    add-int v15, v15, v16

    #@1ce
    move-object/from16 v0, p2

    #@1d0
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1d2
    goto :goto_17f

    #@1d3
    .line 1226
    :cond_1d3
    move-object/from16 v0, p2

    #@1d5
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@1d7
    iget v0, v2, Landroid/graphics/Rect;->top:I

    #@1d9
    move/from16 v16, v0

    #@1db
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    #@1de
    move-result v15

    #@1df
    move-object/from16 v0, p2

    #@1e1
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@1e3
    goto :goto_19e

    #@1e4
    .line 1229
    .end local v3           #displayFrameWidth:I
    .end local v8           #right:I
    :cond_1e4
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@1e6
    if-eqz v15, :cond_19e

    #@1e8
    .line 1230
    move-object/from16 v0, p1

    #@1ea
    invoke-virtual {v0, v13}, Landroid/view/View;->isWindowSplit(Landroid/graphics/Rect;)Z

    #@1ed
    move-result v4

    #@1ee
    .line 1231
    if-eqz v4, :cond_19e

    #@1f0
    .line 1232
    move-object/from16 v0, p0

    #@1f2
    iget-object v15, v0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@1f4
    if-nez v15, :cond_231

    #@1f6
    const/4 v14, 0x0

    #@1f7
    .line 1233
    .local v14, statusBarHeight:I
    :goto_1f7
    const-string v15, "SplitWindow"

    #@1f9
    new-instance v16, Ljava/lang/StringBuilder;

    #@1fb
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1fe
    const-string v17, "check displayFrame on popup case.. displayFrame "

    #@200
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v16

    #@204
    move-object/from16 v0, v16

    #@206
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v16

    #@20a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v16

    #@20e
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@211
    .line 1234
    move-object/from16 v0, p2

    #@213
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@215
    iget v0, v13, Landroid/graphics/Rect;->left:I

    #@217
    move/from16 v16, v0

    #@219
    add-int v15, v15, v16

    #@21b
    move-object/from16 v0, p2

    #@21d
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@21f
    .line 1235
    move-object/from16 v0, p2

    #@221
    iget v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@223
    iget v0, v13, Landroid/graphics/Rect;->top:I

    #@225
    move/from16 v16, v0

    #@227
    sub-int v16, v16, v14

    #@229
    add-int v15, v15, v16

    #@22b
    move-object/from16 v0, p2

    #@22d
    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@22f
    goto/16 :goto_19e

    #@231
    .line 1232
    .end local v14           #statusBarHeight:I
    :cond_231
    move-object/from16 v0, p0

    #@233
    iget-object v15, v0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@235
    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@238
    move-result-object v15

    #@239
    const v16, 0x105000c

    #@23c
    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@23f
    move-result v14

    #@240
    goto :goto_1f7
.end method

.method private invokePopup(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "p"

    #@0
    .prologue
    .line 1015
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1016
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@c
    .line 1018
    :cond_c
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@e
    iget-boolean v1, p0, Landroid/widget/PopupWindow;->mLayoutInsetDecor:Z

    #@10
    invoke-virtual {v0, v1}, Landroid/view/View;->setFitsSystemWindows(Z)V

    #@13
    .line 1019
    invoke-direct {p0}, Landroid/widget/PopupWindow;->setLayoutDirectionFromAnchor()V

    #@16
    .line 1020
    iget-object v0, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@18
    iget-object v1, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@1a
    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1d
    .line 1021
    return-void
.end method

.method private preparePopup(Landroid/view/WindowManager$LayoutParams;)V
    .registers 8
    .parameter "p"

    #@0
    .prologue
    .line 974
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@2
    if-eqz v4, :cond_c

    #@4
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@6
    if-eqz v4, :cond_c

    #@8
    iget-object v4, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@a
    if-nez v4, :cond_14

    #@c
    .line 975
    :cond_c
    new-instance v4, Ljava/lang/IllegalStateException;

    #@e
    const-string v5, "You must specify a valid content view by calling setContentView() before attempting to show the popup."

    #@10
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v4

    #@14
    .line 979
    :cond_14
    iget-object v4, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v4, :cond_55

    #@18
    .line 980
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@1a
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1d
    move-result-object v1

    #@1e
    .line 981
    .local v1, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    const/4 v0, -0x1

    #@1f
    .line 982
    .local v0, height:I
    if-eqz v1, :cond_27

    #@21
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@23
    const/4 v5, -0x2

    #@24
    if-ne v4, v5, :cond_27

    #@26
    .line 984
    const/4 v0, -0x2

    #@27
    .line 989
    :cond_27
    new-instance v3, Landroid/widget/PopupWindow$PopupViewContainer;

    #@29
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@2b
    invoke-direct {v3, p0, v4}, Landroid/widget/PopupWindow$PopupViewContainer;-><init>(Landroid/widget/PopupWindow;Landroid/content/Context;)V

    #@2e
    .line 990
    .local v3, popupViewContainer:Landroid/widget/PopupWindow$PopupViewContainer;
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    #@30
    const/4 v4, -0x1

    #@31
    invoke-direct {v2, v4, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@34
    .line 993
    .local v2, listParams:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@36
    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow$PopupViewContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@39
    .line 994
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@3b
    invoke-virtual {v3, v4, v2}, Landroid/widget/PopupWindow$PopupViewContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@3e
    .line 996
    iput-object v3, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@40
    .line 1000
    .end local v0           #height:I
    .end local v1           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v2           #listParams:Landroid/widget/FrameLayout$LayoutParams;
    .end local v3           #popupViewContainer:Landroid/widget/PopupWindow$PopupViewContainer;
    :goto_40
    iget-object v4, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@42
    invoke-virtual {v4}, Landroid/view/View;->getRawLayoutDirection()I

    #@45
    move-result v4

    #@46
    const/4 v5, 0x2

    #@47
    if-ne v4, v5, :cond_5a

    #@49
    const/4 v4, 0x1

    #@4a
    :goto_4a
    iput-boolean v4, p0, Landroid/widget/PopupWindow;->mPopupViewInitialLayoutDirectionInherited:Z

    #@4c
    .line 1002
    iget v4, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@4e
    iput v4, p0, Landroid/widget/PopupWindow;->mPopupWidth:I

    #@50
    .line 1003
    iget v4, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@52
    iput v4, p0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@54
    .line 1004
    return-void

    #@55
    .line 998
    :cond_55
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@57
    iput-object v4, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@59
    goto :goto_40

    #@5a
    .line 1000
    :cond_5a
    const/4 v4, 0x0

    #@5b
    goto :goto_4a
.end method

.method private registerForScrollChanged(Landroid/view/View;II)V
    .registers 6
    .parameter "anchor"
    .parameter "xoff"
    .parameter "yoff"

    #@0
    .prologue
    .line 1600
    invoke-direct {p0}, Landroid/widget/PopupWindow;->unregisterForScrollChanged()V

    #@3
    .line 1602
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v1, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@a
    .line 1603
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@d
    move-result-object v0

    #@e
    .line 1604
    .local v0, vto:Landroid/view/ViewTreeObserver;
    if-eqz v0, :cond_15

    #@10
    .line 1605
    iget-object v1, p0, Landroid/widget/PopupWindow;->mOnScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@12
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@15
    .line 1608
    :cond_15
    iput p2, p0, Landroid/widget/PopupWindow;->mAnchorXoff:I

    #@17
    .line 1609
    iput p3, p0, Landroid/widget/PopupWindow;->mAnchorYoff:I

    #@19
    .line 1610
    return-void
.end method

.method private setLayoutDirectionFromAnchor()V
    .registers 4

    #@0
    .prologue
    .line 1024
    iget-object v1, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 1025
    iget-object v1, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/view/View;

    #@c
    .line 1026
    .local v0, anchor:Landroid/view/View;
    if-eqz v0, :cond_1b

    #@e
    iget-boolean v1, p0, Landroid/widget/PopupWindow;->mPopupViewInitialLayoutDirectionInherited:Z

    #@10
    if-eqz v1, :cond_1b

    #@12
    .line 1027
    iget-object v1, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@14
    invoke-virtual {v0}, Landroid/view/View;->getLayoutDirection()I

    #@17
    move-result v2

    #@18
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutDirection(I)V

    #@1b
    .line 1030
    .end local v0           #anchor:Landroid/view/View;
    :cond_1b
    return-void
.end method

.method private unregisterForScrollChanged()V
    .registers 5

    #@0
    .prologue
    .line 1587
    iget-object v1, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@2
    .line 1588
    .local v1, anchorRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/view/View;>;"
    const/4 v0, 0x0

    #@3
    .line 1589
    .local v0, anchor:Landroid/view/View;
    if-eqz v1, :cond_b

    #@5
    .line 1590
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    .end local v0           #anchor:Landroid/view/View;
    check-cast v0, Landroid/view/View;

    #@b
    .line 1592
    .restart local v0       #anchor:Landroid/view/View;
    :cond_b
    if-eqz v0, :cond_16

    #@d
    .line 1593
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@10
    move-result-object v2

    #@11
    .line 1594
    .local v2, vto:Landroid/view/ViewTreeObserver;
    iget-object v3, p0, Landroid/widget/PopupWindow;->mOnScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@13
    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@16
    .line 1596
    .end local v2           #vto:Landroid/view/ViewTreeObserver;
    :cond_16
    const/4 v3, 0x0

    #@17
    iput-object v3, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@19
    .line 1597
    return-void
.end method

.method private update(Landroid/view/View;ZIIZII)V
    .registers 20
    .parameter "anchor"
    .parameter "updateLocation"
    .parameter "xoff"
    .parameter "yoff"
    .parameter "updateDimension"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1535
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_a

    #@6
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@8
    if-nez v1, :cond_b

    #@a
    .line 1574
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1539
    :cond_b
    iget-object v8, p0, Landroid/widget/PopupWindow;->mAnchor:Ljava/lang/ref/WeakReference;

    #@d
    .line 1540
    .local v8, oldAnchor:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/view/View;>;"
    if-eqz p2, :cond_6e

    #@f
    iget v1, p0, Landroid/widget/PopupWindow;->mAnchorXoff:I

    #@11
    if-ne v1, p3, :cond_19

    #@13
    iget v1, p0, Landroid/widget/PopupWindow;->mAnchorYoff:I

    #@15
    move/from16 v0, p4

    #@17
    if-eq v1, v0, :cond_6e

    #@19
    :cond_19
    const/4 v7, 0x1

    #@1a
    .line 1541
    .local v7, needsUpdate:Z
    :goto_1a
    if-eqz v8, :cond_28

    #@1c
    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    if-ne v1, p1, :cond_28

    #@22
    if-eqz v7, :cond_70

    #@24
    iget-boolean v1, p0, Landroid/widget/PopupWindow;->mIsDropdown:Z

    #@26
    if-nez v1, :cond_70

    #@28
    .line 1542
    :cond_28
    move/from16 v0, p4

    #@2a
    invoke-direct {p0, p1, p3, v0}, Landroid/widget/PopupWindow;->registerForScrollChanged(Landroid/view/View;II)V

    #@2d
    .line 1549
    :cond_2d
    :goto_2d
    iget-object v1, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@2f
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@32
    move-result-object v9

    #@33
    check-cast v9, Landroid/view/WindowManager$LayoutParams;

    #@35
    .line 1551
    .local v9, p:Landroid/view/WindowManager$LayoutParams;
    if-eqz p5, :cond_49

    #@37
    .line 1552
    const/4 v1, -0x1

    #@38
    move/from16 v0, p6

    #@3a
    if-ne v0, v1, :cond_79

    #@3c
    .line 1553
    iget v0, p0, Landroid/widget/PopupWindow;->mPopupWidth:I

    #@3e
    move/from16 p6, v0

    #@40
    .line 1557
    :goto_40
    const/4 v1, -0x1

    #@41
    move/from16 v0, p7

    #@43
    if-ne v0, v1, :cond_7e

    #@45
    .line 1558
    iget v0, p0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@47
    move/from16 p7, v0

    #@49
    .line 1564
    :cond_49
    :goto_49
    iget v10, v9, Landroid/view/WindowManager$LayoutParams;->x:I

    #@4b
    .line 1565
    .local v10, x:I
    iget v11, v9, Landroid/view/WindowManager$LayoutParams;->y:I

    #@4d
    .line 1567
    .local v11, y:I
    if-eqz p2, :cond_83

    #@4f
    .line 1568
    move/from16 v0, p4

    #@51
    invoke-direct {p0, p1, v9, p3, v0}, Landroid/widget/PopupWindow;->findDropDownPosition(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    #@54
    move-result v1

    #@55
    invoke-direct {p0, v1}, Landroid/widget/PopupWindow;->updateAboveAnchor(Z)V

    #@58
    .line 1573
    :goto_58
    iget v2, v9, Landroid/view/WindowManager$LayoutParams;->x:I

    #@5a
    iget v3, v9, Landroid/view/WindowManager$LayoutParams;->y:I

    #@5c
    iget v1, v9, Landroid/view/WindowManager$LayoutParams;->x:I

    #@5e
    if-ne v10, v1, :cond_64

    #@60
    iget v1, v9, Landroid/view/WindowManager$LayoutParams;->y:I

    #@62
    if-eq v11, v1, :cond_8f

    #@64
    :cond_64
    const/4 v6, 0x1

    #@65
    :goto_65
    move-object v1, p0

    #@66
    move/from16 v4, p6

    #@68
    move/from16 v5, p7

    #@6a
    invoke-virtual/range {v1 .. v6}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    #@6d
    goto :goto_a

    #@6e
    .line 1540
    .end local v7           #needsUpdate:Z
    .end local v9           #p:Landroid/view/WindowManager$LayoutParams;
    .end local v10           #x:I
    .end local v11           #y:I
    :cond_6e
    const/4 v7, 0x0

    #@6f
    goto :goto_1a

    #@70
    .line 1543
    .restart local v7       #needsUpdate:Z
    :cond_70
    if-eqz v7, :cond_2d

    #@72
    .line 1545
    iput p3, p0, Landroid/widget/PopupWindow;->mAnchorXoff:I

    #@74
    .line 1546
    move/from16 v0, p4

    #@76
    iput v0, p0, Landroid/widget/PopupWindow;->mAnchorYoff:I

    #@78
    goto :goto_2d

    #@79
    .line 1555
    .restart local v9       #p:Landroid/view/WindowManager$LayoutParams;
    :cond_79
    move/from16 v0, p6

    #@7b
    iput v0, p0, Landroid/widget/PopupWindow;->mPopupWidth:I

    #@7d
    goto :goto_40

    #@7e
    .line 1560
    :cond_7e
    move/from16 v0, p7

    #@80
    iput v0, p0, Landroid/widget/PopupWindow;->mPopupHeight:I

    #@82
    goto :goto_49

    #@83
    .line 1570
    .restart local v10       #x:I
    .restart local v11       #y:I
    :cond_83
    iget v1, p0, Landroid/widget/PopupWindow;->mAnchorXoff:I

    #@85
    iget v2, p0, Landroid/widget/PopupWindow;->mAnchorYoff:I

    #@87
    invoke-direct {p0, p1, v9, v1, v2}, Landroid/widget/PopupWindow;->findDropDownPosition(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    #@8a
    move-result v1

    #@8b
    invoke-direct {p0, v1}, Landroid/widget/PopupWindow;->updateAboveAnchor(Z)V

    #@8e
    goto :goto_58

    #@8f
    .line 1573
    :cond_8f
    const/4 v6, 0x0

    #@90
    goto :goto_65
.end method

.method private updateAboveAnchor(Z)V
    .registers 4
    .parameter "aboveAnchor"

    #@0
    .prologue
    .line 930
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@2
    if-eq p1, v0, :cond_19

    #@4
    .line 931
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@6
    .line 933
    iget-object v0, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_19

    #@a
    .line 937
    iget-object v0, p0, Landroid/widget/PopupWindow;->mAboveAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    if-eqz v0, :cond_22

    #@e
    .line 938
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@10
    if-eqz v0, :cond_1a

    #@12
    .line 939
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@14
    iget-object v1, p0, Landroid/widget/PopupWindow;->mAboveAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@19
    .line 948
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 941
    :cond_1a
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@1c
    iget-object v1, p0, Landroid/widget/PopupWindow;->mBelowAnchorBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@21
    goto :goto_19

    #@22
    .line 944
    :cond_22
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@24
    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    #@27
    goto :goto_19
.end method


# virtual methods
.method public dismiss()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1327
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_38

    #@7
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@9
    if-eqz v0, :cond_38

    #@b
    .line 1328
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mIsShowing:Z

    #@e
    .line 1330
    invoke-direct {p0}, Landroid/widget/PopupWindow;->unregisterForScrollChanged()V

    #@11
    .line 1333
    :try_start_11
    iget-object v0, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@13
    iget-object v1, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@15
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_39
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_18} :catch_5c

    #@18
    .line 1343
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@1a
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@1c
    if-eq v0, v1, :cond_2d

    #@1e
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@20
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@22
    if-eqz v0, :cond_2d

    #@24
    .line 1344
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@26
    check-cast v0, Landroid/view/ViewGroup;

    #@28
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@2a
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@2d
    .line 1346
    :cond_2d
    iput-object v3, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@2f
    .line 1348
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@31
    if-eqz v0, :cond_38

    #@33
    .line 1349
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@35
    :goto_35
    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    #@38
    .line 1353
    :cond_38
    return-void

    #@39
    .line 1343
    :catchall_39
    move-exception v0

    #@3a
    move-object v1, v0

    #@3b
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@3d
    iget-object v2, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@3f
    if-eq v0, v2, :cond_50

    #@41
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@43
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@45
    if-eqz v0, :cond_50

    #@47
    .line 1344
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@49
    check-cast v0, Landroid/view/ViewGroup;

    #@4b
    iget-object v2, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@4d
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@50
    .line 1346
    :cond_50
    iput-object v3, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@52
    .line 1348
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@54
    if-eqz v0, :cond_5b

    #@56
    .line 1349
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@58
    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    #@5b
    .line 1343
    :cond_5b
    throw v1

    #@5c
    .line 1335
    :catch_5c
    move-exception v0

    #@5d
    .line 1343
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@5f
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@61
    if-eq v0, v1, :cond_72

    #@63
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@65
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@67
    if-eqz v0, :cond_72

    #@69
    .line 1344
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@6b
    check-cast v0, Landroid/view/ViewGroup;

    #@6d
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@6f
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@72
    .line 1346
    :cond_72
    iput-object v3, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@74
    .line 1348
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@76
    if-eqz v0, :cond_38

    #@78
    .line 1349
    iget-object v0, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@7a
    goto :goto_35
.end method

.method public getAnimationStyle()I
    .registers 2

    #@0
    .prologue
    .line 361
    iget v0, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@2
    return v0
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 342
    iget-object v0, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getContentView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 405
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 770
    iget v0, p0, Landroid/widget/PopupWindow;->mHeight:I

    #@2
    return v0
.end method

.method public getInputMethodMode()I
    .registers 2

    #@0
    .prologue
    .line 480
    iget v0, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@2
    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;)I
    .registers 3
    .parameter "anchor"

    #@0
    .prologue
    .line 1257
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;I)I
    .registers 4
    .parameter "anchor"
    .parameter "yOffset"

    #@0
    .prologue
    .line 1272
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;IZ)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getMaxAvailableHeight(Landroid/view/View;IZ)I
    .registers 14
    .parameter "anchor"
    .parameter "yOffset"
    .parameter "ignoreBottomDecorations"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 1293
    new-instance v2, Landroid/graphics/Rect;

    #@3
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@6
    .line 1294
    .local v2, displayFrame:Landroid/graphics/Rect;
    invoke-virtual {p1, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    #@9
    .line 1296
    iget-object v0, p0, Landroid/widget/PopupWindow;->mDrawingLocation:[I

    #@b
    .line 1297
    .local v0, anchorPos:[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    #@e
    .line 1299
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    #@10
    .line 1300
    .local v1, bottomEdge:I
    if-eqz p3, :cond_20

    #@12
    .line 1301
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v5

    #@1a
    .line 1302
    .local v5, res:Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1d
    move-result-object v7

    #@1e
    iget v1, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    #@20
    .line 1304
    .end local v5           #res:Landroid/content/res/Resources;
    :cond_20
    aget v7, v0, v9

    #@22
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@25
    move-result v8

    #@26
    add-int/2addr v7, v8

    #@27
    sub-int v7, v1, v7

    #@29
    sub-int v3, v7, p2

    #@2b
    .line 1305
    .local v3, distanceToBottom:I
    aget v7, v0, v9

    #@2d
    iget v8, v2, Landroid/graphics/Rect;->top:I

    #@2f
    sub-int/2addr v7, v8

    #@30
    add-int v4, v7, p2

    #@32
    .line 1308
    .local v4, distanceToTop:I
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@35
    move-result v6

    #@36
    .line 1309
    .local v6, returnedHeight:I
    iget-object v7, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@38
    if-eqz v7, :cond_4b

    #@3a
    .line 1310
    iget-object v7, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@3c
    iget-object v8, p0, Landroid/widget/PopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@3e
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@41
    .line 1311
    iget-object v7, p0, Landroid/widget/PopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@43
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@45
    iget-object v8, p0, Landroid/widget/PopupWindow;->mTempRect:Landroid/graphics/Rect;

    #@47
    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    #@49
    add-int/2addr v7, v8

    #@4a
    sub-int/2addr v6, v7

    #@4b
    .line 1316
    :cond_4b
    return v6
.end method

.method public getSoftInputMode()I
    .registers 2

    #@0
    .prologue
    .line 521
    iget v0, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@2
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 796
    iget v0, p0, Landroid/widget/PopupWindow;->mWidth:I

    #@2
    return v0
.end method

.method public getWindowLayoutType()I
    .registers 2

    #@0
    .prologue
    .line 723
    iget v0, p0, Landroid/widget/PopupWindow;->mWindowLayoutType:I

    #@2
    return v0
.end method

.method public isAboveAnchor()Z
    .registers 2

    #@0
    .prologue
    .line 962
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mAboveAnchor:Z

    #@2
    return v0
.end method

.method public isClippingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 595
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mClippingEnabled:Z

    #@2
    return v0
.end method

.method public isFocusable()Z
    .registers 2

    #@0
    .prologue
    .line 451
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mFocusable:Z

    #@2
    return v0
.end method

.method public isLayoutInScreenEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 676
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mLayoutInScreen:Z

    #@2
    return v0
.end method

.method public isOutsideTouchable()Z
    .registers 2

    #@0
    .prologue
    .line 563
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mOutsideTouchable:Z

    #@2
    return v0
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 820
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mIsShowing:Z

    #@2
    return v0
.end method

.method public isSplitTouchEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 645
    iget v2, p0, Landroid/widget/PopupWindow;->mSplitTouchEnabled:I

    #@4
    if-gez v2, :cond_19

    #@6
    iget-object v2, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@8
    if-eqz v2, :cond_19

    #@a
    .line 646
    iget-object v2, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@f
    move-result-object v2

    #@10
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@12
    const/16 v3, 0xb

    #@14
    if-lt v2, v3, :cond_17

    #@16
    .line 648
    :cond_16
    :goto_16
    return v0

    #@17
    :cond_17
    move v0, v1

    #@18
    .line 646
    goto :goto_16

    #@19
    .line 648
    :cond_19
    iget v2, p0, Landroid/widget/PopupWindow;->mSplitTouchEnabled:I

    #@1b
    if-eq v2, v0, :cond_16

    #@1d
    move v0, v1

    #@1e
    goto :goto_16
.end method

.method public isTouchable()Z
    .registers 2

    #@0
    .prologue
    .line 532
    iget-boolean v0, p0, Landroid/widget/PopupWindow;->mTouchable:Z

    #@2
    return v0
.end method

.method setAllowScrollingAnchorParent(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 634
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mAllowScrollingAnchorParent:Z

    #@2
    .line 635
    return-void
.end method

.method public setAnimationStyle(I)V
    .registers 2
    .parameter "animationStyle"

    #@0
    .prologue
    .line 394
    iput p1, p0, Landroid/widget/PopupWindow;->mAnimationStyle:I

    #@2
    .line 395
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "background"

    #@0
    .prologue
    .line 352
    iput-object p1, p0, Landroid/widget/PopupWindow;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    .line 353
    return-void
.end method

.method public setBaseAppType(I)V
    .registers 2
    .parameter "appType"

    #@0
    .prologue
    .line 171
    iput p1, p0, Landroid/widget/PopupWindow;->mBaseAppType:I

    #@2
    .line 172
    return-void
.end method

.method public setClipToScreenEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 623
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mClipToScreen:Z

    #@2
    .line 624
    if-nez p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    #@8
    .line 625
    return-void

    #@9
    .line 624
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setClippingEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 613
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mClippingEnabled:Z

    #@2
    .line 614
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 4
    .parameter "contentView"

    #@0
    .prologue
    .line 420
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 433
    :cond_6
    :goto_6
    return-void

    #@7
    .line 424
    :cond_7
    iput-object p1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@9
    .line 426
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@b
    if-nez v0, :cond_19

    #@d
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@f
    if-eqz v0, :cond_19

    #@11
    .line 427
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@13
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@19
    .line 430
    :cond_19
    iget-object v0, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@1b
    if-nez v0, :cond_6

    #@1d
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@1f
    if-eqz v0, :cond_6

    #@21
    .line 431
    iget-object v0, p0, Landroid/widget/PopupWindow;->mContext:Landroid/content/Context;

    #@23
    const-string/jumbo v1, "window"

    #@26
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Landroid/view/WindowManager;

    #@2c
    iput-object v0, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@2e
    goto :goto_6
.end method

.method public setFocusable(Z)V
    .registers 2
    .parameter "focusable"

    #@0
    .prologue
    .line 471
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mFocusable:Z

    #@2
    .line 472
    return-void
.end method

.method public setHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 785
    iput p1, p0, Landroid/widget/PopupWindow;->mHeight:I

    #@2
    .line 786
    return-void
.end method

.method public setIgnoreCheekPress()V
    .registers 2

    #@0
    .prologue
    .line 376
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mIgnoreCheekPress:Z

    #@3
    .line 377
    return-void
.end method

.method public setInputMethodMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 497
    iput p1, p0, Landroid/widget/PopupWindow;->mInputMethodMode:I

    #@2
    .line 498
    return-void
.end method

.method public setLayoutInScreenEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 688
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mLayoutInScreen:Z

    #@2
    .line 689
    return-void
.end method

.method public setLayoutInsetDecor(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 704
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mLayoutInsetDecor:Z

    #@2
    .line 705
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .registers 2
    .parameter "onDismissListener"

    #@0
    .prologue
    .line 1361
    iput-object p1, p0, Landroid/widget/PopupWindow;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@2
    .line 1362
    return-void
.end method

.method public setOutsideTouchable(Z)V
    .registers 2
    .parameter "touchable"

    #@0
    .prologue
    .line 584
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mOutsideTouchable:Z

    #@2
    .line 585
    return-void
.end method

.method public setSoftInputMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 511
    iput p1, p0, Landroid/widget/PopupWindow;->mSoftInputMode:I

    #@2
    .line 512
    return-void
.end method

.method public setSplitTouchEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 665
    if-eqz p1, :cond_6

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    iput v0, p0, Landroid/widget/PopupWindow;->mSplitTouchEnabled:I

    #@5
    .line 666
    return-void

    #@6
    .line 665
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_3
.end method

.method public setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 440
    iput-object p1, p0, Landroid/widget/PopupWindow;->mTouchInterceptor:Landroid/view/View$OnTouchListener;

    #@2
    .line 441
    return-void
.end method

.method public setTouchModal(Z)V
    .registers 3
    .parameter "touchModal"

    #@0
    .prologue
    .line 732
    if-nez p1, :cond_6

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    iput-boolean v0, p0, Landroid/widget/PopupWindow;->mNotTouchModal:Z

    #@5
    .line 733
    return-void

    #@6
    .line 732
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_3
.end method

.method public setTouchable(Z)V
    .registers 2
    .parameter "touchable"

    #@0
    .prologue
    .line 551
    iput-boolean p1, p0, Landroid/widget/PopupWindow;->mTouchable:Z

    #@2
    .line 552
    return-void
.end method

.method public setWidth(I)V
    .registers 2
    .parameter "width"

    #@0
    .prologue
    .line 811
    iput p1, p0, Landroid/widget/PopupWindow;->mWidth:I

    #@2
    .line 812
    return-void
.end method

.method public setWindowLayoutMode(II)V
    .registers 3
    .parameter "widthSpec"
    .parameter "heightSpec"

    #@0
    .prologue
    .line 758
    iput p1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@2
    .line 759
    iput p2, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@4
    .line 760
    return-void
.end method

.method public setWindowLayoutType(I)V
    .registers 2
    .parameter "layoutType"

    #@0
    .prologue
    .line 715
    iput p1, p0, Landroid/widget/PopupWindow;->mWindowLayoutType:I

    #@2
    .line 716
    return-void
.end method

.method public showAsDropDown(Landroid/view/View;)V
    .registers 3
    .parameter "anchor"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 889
    invoke-virtual {p0, p1, v0, v0}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    #@4
    .line 890
    return-void
.end method

.method public showAsDropDown(Landroid/view/View;II)V
    .registers 7
    .parameter "anchor"
    .parameter "xoff"
    .parameter "yoff"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 907
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_b

    #@7
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@9
    if-nez v1, :cond_c

    #@b
    .line 927
    :cond_b
    :goto_b
    return-void

    #@c
    .line 911
    :cond_c
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/PopupWindow;->registerForScrollChanged(Landroid/view/View;II)V

    #@f
    .line 913
    iput-boolean v2, p0, Landroid/widget/PopupWindow;->mIsShowing:Z

    #@11
    .line 914
    iput-boolean v2, p0, Landroid/widget/PopupWindow;->mIsDropdown:Z

    #@13
    .line 916
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v1}, Landroid/widget/PopupWindow;->createPopupLayout(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;

    #@1a
    move-result-object v0

    #@1b
    .line 917
    .local v0, p:Landroid/view/WindowManager$LayoutParams;
    invoke-direct {p0, v0}, Landroid/widget/PopupWindow;->preparePopup(Landroid/view/WindowManager$LayoutParams;)V

    #@1e
    .line 919
    invoke-direct {p0, p1, v0, p2, p3}, Landroid/widget/PopupWindow;->findDropDownPosition(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    #@21
    move-result v1

    #@22
    invoke-direct {p0, v1}, Landroid/widget/PopupWindow;->updateAboveAnchor(Z)V

    #@25
    .line 921
    iget v1, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@27
    if-gez v1, :cond_2f

    #@29
    iget v1, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@2b
    iput v1, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@2d
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2f
    .line 922
    :cond_2f
    iget v1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@31
    if-gez v1, :cond_39

    #@33
    iget v1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@35
    iput v1, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@37
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@39
    .line 924
    :cond_39
    invoke-direct {p0}, Landroid/widget/PopupWindow;->computeAnimationResource()I

    #@3c
    move-result v1

    #@3d
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@3f
    .line 926
    invoke-direct {p0, v0}, Landroid/widget/PopupWindow;->invokePopup(Landroid/view/WindowManager$LayoutParams;)V

    #@42
    goto :goto_b
.end method

.method public showAtLocation(Landroid/os/IBinder;III)V
    .registers 7
    .parameter "token"
    .parameter "gravity"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 853
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_a

    #@6
    iget-object v1, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@8
    if-nez v1, :cond_b

    #@a
    .line 875
    :cond_a
    :goto_a
    return-void

    #@b
    .line 857
    :cond_b
    invoke-direct {p0}, Landroid/widget/PopupWindow;->unregisterForScrollChanged()V

    #@e
    .line 859
    const/4 v1, 0x1

    #@f
    iput-boolean v1, p0, Landroid/widget/PopupWindow;->mIsShowing:Z

    #@11
    .line 860
    const/4 v1, 0x0

    #@12
    iput-boolean v1, p0, Landroid/widget/PopupWindow;->mIsDropdown:Z

    #@14
    .line 862
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;->createPopupLayout(Landroid/os/IBinder;)Landroid/view/WindowManager$LayoutParams;

    #@17
    move-result-object v0

    #@18
    .line 863
    .local v0, p:Landroid/view/WindowManager$LayoutParams;
    invoke-direct {p0}, Landroid/widget/PopupWindow;->computeAnimationResource()I

    #@1b
    move-result v1

    #@1c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@1e
    .line 865
    invoke-direct {p0, v0}, Landroid/widget/PopupWindow;->preparePopup(Landroid/view/WindowManager$LayoutParams;)V

    #@21
    .line 866
    if-nez p2, :cond_26

    #@23
    .line 867
    const p2, 0x800033

    #@26
    .line 869
    :cond_26
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@28
    .line 870
    iput p3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@2a
    .line 871
    iput p4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@2c
    .line 872
    iget v1, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@2e
    if-gez v1, :cond_36

    #@30
    iget v1, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@32
    iput v1, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@34
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@36
    .line 873
    :cond_36
    iget v1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@38
    if-gez v1, :cond_40

    #@3a
    iget v1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@3c
    iput v1, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@3e
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@40
    .line 874
    :cond_40
    invoke-direct {p0, v0}, Landroid/widget/PopupWindow;->invokePopup(Landroid/view/WindowManager$LayoutParams;)V

    #@43
    goto :goto_a
.end method

.method public showAtLocation(Landroid/view/View;III)V
    .registers 6
    .parameter "parent"
    .parameter "gravity"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 838
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2, p3, p4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/os/IBinder;III)V

    #@7
    .line 839
    return-void
.end method

.method public update()V
    .registers 7

    #@0
    .prologue
    .line 1372
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_a

    #@6
    iget-object v4, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@8
    if-nez v4, :cond_b

    #@a
    .line 1397
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1376
    :cond_b
    iget-object v4, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@d
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    #@13
    .line 1379
    .local v2, p:Landroid/view/WindowManager$LayoutParams;
    const/4 v3, 0x0

    #@14
    .line 1381
    .local v3, update:Z
    invoke-direct {p0}, Landroid/widget/PopupWindow;->computeAnimationResource()I

    #@17
    move-result v0

    #@18
    .line 1382
    .local v0, newAnim:I
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@1a
    if-eq v0, v4, :cond_1f

    #@1c
    .line 1383
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@1e
    .line 1384
    const/4 v3, 0x1

    #@1f
    .line 1387
    :cond_1f
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@21
    invoke-direct {p0, v4}, Landroid/widget/PopupWindow;->computeFlags(I)I

    #@24
    move-result v1

    #@25
    .line 1388
    .local v1, newFlags:I
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@27
    if-eq v1, v4, :cond_2c

    #@29
    .line 1389
    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2b
    .line 1390
    const/4 v3, 0x1

    #@2c
    .line 1393
    :cond_2c
    if-eqz v3, :cond_a

    #@2e
    .line 1394
    invoke-direct {p0}, Landroid/widget/PopupWindow;->setLayoutDirectionFromAnchor()V

    #@31
    .line 1395
    iget-object v4, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@33
    iget-object v5, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@35
    invoke-interface {v4, v5, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@38
    goto :goto_a
.end method

.method public update(II)V
    .registers 10
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1408
    iget-object v0, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5
    move-result-object v6

    #@6
    check-cast v6, Landroid/view/WindowManager$LayoutParams;

    #@8
    .line 1410
    .local v6, p:Landroid/view/WindowManager$LayoutParams;
    iget v1, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    #@a
    iget v2, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    #@c
    const/4 v5, 0x0

    #@d
    move-object v0, p0

    #@e
    move v3, p1

    #@f
    move v4, p2

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    #@13
    .line 1411
    return-void
.end method

.method public update(IIII)V
    .registers 11
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1425
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    #@9
    .line 1426
    return-void
.end method

.method public update(IIIIZ)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "force"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 1442
    if-eq p3, v7, :cond_8

    #@3
    .line 1443
    iput p3, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@5
    .line 1444
    invoke-virtual {p0, p3}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@8
    .line 1447
    :cond_8
    if-eq p4, v7, :cond_f

    #@a
    .line 1448
    iput p4, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@c
    .line 1449
    invoke-virtual {p0, p4}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@f
    .line 1452
    :cond_f
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@12
    move-result v6

    #@13
    if-eqz v6, :cond_19

    #@15
    iget-object v6, p0, Landroid/widget/PopupWindow;->mContentView:Landroid/view/View;

    #@17
    if-nez v6, :cond_1a

    #@19
    .line 1498
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 1456
    :cond_1a
    iget-object v6, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@1c
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@22
    .line 1458
    .local v4, p:Landroid/view/WindowManager$LayoutParams;
    move v5, p5

    #@23
    .line 1460
    .local v5, update:Z
    iget v6, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@25
    if-gez v6, :cond_78

    #@27
    iget v1, p0, Landroid/widget/PopupWindow;->mWidthMode:I

    #@29
    .line 1461
    .local v1, finalWidth:I
    :goto_29
    if-eq p3, v7, :cond_34

    #@2b
    iget v6, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2d
    if-eq v6, v1, :cond_34

    #@2f
    .line 1462
    iput v1, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@31
    iput v1, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@33
    .line 1463
    const/4 v5, 0x1

    #@34
    .line 1466
    :cond_34
    iget v6, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@36
    if-gez v6, :cond_7b

    #@38
    iget v0, p0, Landroid/widget/PopupWindow;->mHeightMode:I

    #@3a
    .line 1467
    .local v0, finalHeight:I
    :goto_3a
    if-eq p4, v7, :cond_45

    #@3c
    iget v6, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3e
    if-eq v6, v0, :cond_45

    #@40
    .line 1468
    iput v0, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@42
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@44
    .line 1469
    const/4 v5, 0x1

    #@45
    .line 1472
    :cond_45
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    #@47
    if-eq v6, p1, :cond_4c

    #@49
    .line 1473
    iput p1, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    #@4b
    .line 1474
    const/4 v5, 0x1

    #@4c
    .line 1477
    :cond_4c
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    #@4e
    if-eq v6, p2, :cond_53

    #@50
    .line 1478
    iput p2, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    #@52
    .line 1479
    const/4 v5, 0x1

    #@53
    .line 1482
    :cond_53
    invoke-direct {p0}, Landroid/widget/PopupWindow;->computeAnimationResource()I

    #@56
    move-result v2

    #@57
    .line 1483
    .local v2, newAnim:I
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@59
    if-eq v2, v6, :cond_5e

    #@5b
    .line 1484
    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@5d
    .line 1485
    const/4 v5, 0x1

    #@5e
    .line 1488
    :cond_5e
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@60
    invoke-direct {p0, v6}, Landroid/widget/PopupWindow;->computeFlags(I)I

    #@63
    move-result v3

    #@64
    .line 1489
    .local v3, newFlags:I
    iget v6, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@66
    if-eq v3, v6, :cond_6b

    #@68
    .line 1490
    iput v3, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@6a
    .line 1491
    const/4 v5, 0x1

    #@6b
    .line 1494
    :cond_6b
    if-eqz v5, :cond_19

    #@6d
    .line 1495
    invoke-direct {p0}, Landroid/widget/PopupWindow;->setLayoutDirectionFromAnchor()V

    #@70
    .line 1496
    iget-object v6, p0, Landroid/widget/PopupWindow;->mWindowManager:Landroid/view/WindowManager;

    #@72
    iget-object v7, p0, Landroid/widget/PopupWindow;->mPopupView:Landroid/view/View;

    #@74
    invoke-interface {v6, v7, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@77
    goto :goto_19

    #@78
    .line 1460
    .end local v0           #finalHeight:I
    .end local v1           #finalWidth:I
    .end local v2           #newAnim:I
    .end local v3           #newFlags:I
    :cond_78
    iget v1, p0, Landroid/widget/PopupWindow;->mLastWidth:I

    #@7a
    goto :goto_29

    #@7b
    .line 1466
    .restart local v1       #finalWidth:I
    :cond_7b
    iget v0, p0, Landroid/widget/PopupWindow;->mLastHeight:I

    #@7d
    goto :goto_3a
.end method

.method public update(Landroid/view/View;II)V
    .registers 12
    .parameter "anchor"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1510
    const/4 v5, 0x1

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v3, v2

    #@5
    move v4, v2

    #@6
    move v6, p2

    #@7
    move v7, p3

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/widget/PopupWindow;->update(Landroid/view/View;ZIIZII)V

    #@b
    .line 1511
    return-void
.end method

.method public update(Landroid/view/View;IIII)V
    .registers 14
    .parameter "anchor"
    .parameter "xoff"
    .parameter "yoff"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1529
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v3, p2

    #@4
    move v4, p3

    #@5
    move v5, v2

    #@6
    move v6, p4

    #@7
    move v7, p5

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/widget/PopupWindow;->update(Landroid/view/View;ZIIZII)V

    #@b
    .line 1530
    return-void
.end method
