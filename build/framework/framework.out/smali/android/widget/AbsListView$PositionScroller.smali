.class Landroid/widget/AbsListView$PositionScroller;
.super Ljava/lang/Object;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PositionScroller"
.end annotation


# static fields
.field private static final MOVE_DOWN_BOUND:I = 0x3

.field private static final MOVE_DOWN_POS:I = 0x1

.field private static final MOVE_OFFSET:I = 0x5

.field private static final MOVE_UP_BOUND:I = 0x4

.field private static final MOVE_UP_POS:I = 0x2

.field private static final SCROLL_DURATION:I = 0xc8


# instance fields
.field private mBoundPos:I

.field private final mExtraScroll:I

.field private mLastSeenPos:I

.field private mMode:I

.field private mOffsetFromTop:I

.field private mScrollDuration:I

.field private mTargetPos:I

.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 4410
    iput-object p1, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4411
    invoke-static {p1}, Landroid/widget/AbsListView;->access$3000(Landroid/widget/AbsListView;)Landroid/content/Context;

    #@8
    move-result-object v0

    #@9
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledFadingEdgeLength()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/widget/AbsListView$PositionScroller;->mExtraScroll:I

    #@13
    .line 4412
    return-void
.end method


# virtual methods
.method public run()V
    .registers 36

    #@0
    .prologue
    .line 4654
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@4
    move-object/from16 v31, v0

    #@6
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getHeight()I

    #@9
    move-result v19

    #@a
    .line 4655
    .local v19, listHeight:I
    move-object/from16 v0, p0

    #@c
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@e
    move-object/from16 v31, v0

    #@10
    move-object/from16 v0, v31

    #@12
    iget v9, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@14
    .line 4657
    .local v9, firstPos:I
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@18
    move/from16 v31, v0

    #@1a
    packed-switch v31, :pswitch_data_420

    #@1d
    .line 4829
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 4659
    :pswitch_1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@22
    move-object/from16 v31, v0

    #@24
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getChildCount()I

    #@27
    move-result v31

    #@28
    add-int/lit8 v16, v31, -0x1

    #@2a
    .line 4660
    .local v16, lastViewIndex:I
    add-int v12, v9, v16

    #@2c
    .line 4662
    .local v12, lastPos:I
    if-ltz v16, :cond_1d

    #@2e
    .line 4666
    move-object/from16 v0, p0

    #@30
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@32
    move/from16 v31, v0

    #@34
    move/from16 v0, v31

    #@36
    if-ne v12, v0, :cond_46

    #@38
    .line 4668
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@3c
    move-object/from16 v31, v0

    #@3e
    move-object/from16 v0, v31

    #@40
    move-object/from16 v1, p0

    #@42
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@45
    goto :goto_1d

    #@46
    .line 4672
    :cond_46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@4a
    move-object/from16 v31, v0

    #@4c
    move-object/from16 v0, v31

    #@4e
    move/from16 v1, v16

    #@50
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@53
    move-result-object v13

    #@54
    .line 4673
    .local v13, lastView:Landroid/view/View;
    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    #@57
    move-result v15

    #@58
    .line 4674
    .local v15, lastViewHeight:I
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    #@5b
    move-result v18

    #@5c
    .line 4675
    .local v18, lastViewTop:I
    sub-int v17, v19, v18

    #@5e
    .line 4676
    .local v17, lastViewPixelsShowing:I
    move-object/from16 v0, p0

    #@60
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@62
    move-object/from16 v31, v0

    #@64
    move-object/from16 v0, v31

    #@66
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@68
    move/from16 v31, v0

    #@6a
    add-int/lit8 v31, v31, -0x1

    #@6c
    move/from16 v0, v31

    #@6e
    if-ge v12, v0, :cond_c6

    #@70
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@74
    move-object/from16 v31, v0

    #@76
    move-object/from16 v0, v31

    #@78
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@7a
    move-object/from16 v31, v0

    #@7c
    move-object/from16 v0, v31

    #@7e
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@80
    move/from16 v31, v0

    #@82
    move-object/from16 v0, p0

    #@84
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mExtraScroll:I

    #@86
    move/from16 v32, v0

    #@88
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->max(II)I

    #@8b
    move-result v8

    #@8c
    .line 4679
    .local v8, extraScroll:I
    :goto_8c
    sub-int v31, v15, v17

    #@8e
    add-int v28, v31, v8

    #@90
    .line 4680
    .local v28, scrollBy:I
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@94
    move-object/from16 v31, v0

    #@96
    move-object/from16 v0, p0

    #@98
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@9a
    move/from16 v32, v0

    #@9c
    const/16 v33, 0x1

    #@9e
    move-object/from16 v0, v31

    #@a0
    move/from16 v1, v28

    #@a2
    move/from16 v2, v32

    #@a4
    move/from16 v3, v33

    #@a6
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@a9
    .line 4682
    move-object/from16 v0, p0

    #@ab
    iput v12, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@ad
    .line 4683
    move-object/from16 v0, p0

    #@af
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@b1
    move/from16 v31, v0

    #@b3
    move/from16 v0, v31

    #@b5
    if-ge v12, v0, :cond_1d

    #@b7
    .line 4684
    move-object/from16 v0, p0

    #@b9
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@bb
    move-object/from16 v31, v0

    #@bd
    move-object/from16 v0, v31

    #@bf
    move-object/from16 v1, p0

    #@c1
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@c4
    goto/16 :goto_1d

    #@c6
    .line 4676
    .end local v8           #extraScroll:I
    .end local v28           #scrollBy:I
    :cond_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@ca
    move-object/from16 v31, v0

    #@cc
    move-object/from16 v0, v31

    #@ce
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@d0
    move-object/from16 v31, v0

    #@d2
    move-object/from16 v0, v31

    #@d4
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    #@d6
    goto :goto_8c

    #@d7
    .line 4690
    .end local v12           #lastPos:I
    .end local v13           #lastView:Landroid/view/View;
    .end local v15           #lastViewHeight:I
    .end local v16           #lastViewIndex:I
    .end local v17           #lastViewPixelsShowing:I
    .end local v18           #lastViewTop:I
    :pswitch_d7
    const/16 v24, 0x1

    #@d9
    .line 4691
    .local v24, nextViewIndex:I
    move-object/from16 v0, p0

    #@db
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@dd
    move-object/from16 v31, v0

    #@df
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getChildCount()I

    #@e2
    move-result v5

    #@e3
    .line 4693
    .local v5, childCount:I
    move-object/from16 v0, p0

    #@e5
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@e7
    move/from16 v31, v0

    #@e9
    move/from16 v0, v31

    #@eb
    if-eq v9, v0, :cond_1d

    #@ed
    const/16 v31, 0x1

    #@ef
    move/from16 v0, v31

    #@f1
    if-le v5, v0, :cond_1d

    #@f3
    add-int v31, v9, v5

    #@f5
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@f9
    move-object/from16 v32, v0

    #@fb
    move-object/from16 v0, v32

    #@fd
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@ff
    move/from16 v32, v0

    #@101
    move/from16 v0, v31

    #@103
    move/from16 v1, v32

    #@105
    if-ge v0, v1, :cond_1d

    #@107
    .line 4697
    add-int/lit8 v21, v9, 0x1

    #@109
    .line 4699
    .local v21, nextPos:I
    move-object/from16 v0, p0

    #@10b
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@10d
    move/from16 v31, v0

    #@10f
    move/from16 v0, v21

    #@111
    move/from16 v1, v31

    #@113
    if-ne v0, v1, :cond_124

    #@115
    .line 4701
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@119
    move-object/from16 v31, v0

    #@11b
    move-object/from16 v0, v31

    #@11d
    move-object/from16 v1, p0

    #@11f
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@122
    goto/16 :goto_1d

    #@124
    .line 4705
    :cond_124
    move-object/from16 v0, p0

    #@126
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@128
    move-object/from16 v31, v0

    #@12a
    const/16 v32, 0x1

    #@12c
    invoke-virtual/range {v31 .. v32}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@12f
    move-result-object v22

    #@130
    .line 4706
    .local v22, nextView:Landroid/view/View;
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getHeight()I

    #@133
    move-result v23

    #@134
    .line 4707
    .local v23, nextViewHeight:I
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getTop()I

    #@137
    move-result v25

    #@138
    .line 4708
    .local v25, nextViewTop:I
    move-object/from16 v0, p0

    #@13a
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@13c
    move-object/from16 v31, v0

    #@13e
    move-object/from16 v0, v31

    #@140
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@142
    move-object/from16 v31, v0

    #@144
    move-object/from16 v0, v31

    #@146
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@148
    move/from16 v31, v0

    #@14a
    move-object/from16 v0, p0

    #@14c
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mExtraScroll:I

    #@14e
    move/from16 v32, v0

    #@150
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->max(II)I

    #@153
    move-result v8

    #@154
    .line 4709
    .restart local v8       #extraScroll:I
    move-object/from16 v0, p0

    #@156
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@158
    move/from16 v31, v0

    #@15a
    move/from16 v0, v21

    #@15c
    move/from16 v1, v31

    #@15e
    if-ge v0, v1, :cond_190

    #@160
    .line 4710
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@164
    move-object/from16 v31, v0

    #@166
    const/16 v32, 0x0

    #@168
    add-int v33, v23, v25

    #@16a
    sub-int v33, v33, v8

    #@16c
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->max(II)I

    #@16f
    move-result v32

    #@170
    move-object/from16 v0, p0

    #@172
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@174
    move/from16 v33, v0

    #@176
    const/16 v34, 0x1

    #@178
    invoke-virtual/range {v31 .. v34}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@17b
    .line 4713
    move/from16 v0, v21

    #@17d
    move-object/from16 v1, p0

    #@17f
    iput v0, v1, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@181
    .line 4715
    move-object/from16 v0, p0

    #@183
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@185
    move-object/from16 v31, v0

    #@187
    move-object/from16 v0, v31

    #@189
    move-object/from16 v1, p0

    #@18b
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@18e
    goto/16 :goto_1d

    #@190
    .line 4717
    :cond_190
    move/from16 v0, v25

    #@192
    if-le v0, v8, :cond_1d

    #@194
    .line 4718
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@198
    move-object/from16 v31, v0

    #@19a
    sub-int v32, v25, v8

    #@19c
    move-object/from16 v0, p0

    #@19e
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@1a0
    move/from16 v33, v0

    #@1a2
    const/16 v34, 0x1

    #@1a4
    invoke-virtual/range {v31 .. v34}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@1a7
    goto/16 :goto_1d

    #@1a9
    .line 4725
    .end local v5           #childCount:I
    .end local v8           #extraScroll:I
    .end local v21           #nextPos:I
    .end local v22           #nextView:Landroid/view/View;
    .end local v23           #nextViewHeight:I
    .end local v24           #nextViewIndex:I
    .end local v25           #nextViewTop:I
    :pswitch_1a9
    move-object/from16 v0, p0

    #@1ab
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@1ad
    move/from16 v31, v0

    #@1af
    move/from16 v0, v31

    #@1b1
    if-ne v9, v0, :cond_1c2

    #@1b3
    .line 4727
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1b7
    move-object/from16 v31, v0

    #@1b9
    move-object/from16 v0, v31

    #@1bb
    move-object/from16 v1, p0

    #@1bd
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@1c0
    goto/16 :goto_1d

    #@1c2
    .line 4731
    :cond_1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1c6
    move-object/from16 v31, v0

    #@1c8
    const/16 v32, 0x0

    #@1ca
    invoke-virtual/range {v31 .. v32}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1cd
    move-result-object v10

    #@1ce
    .line 4732
    .local v10, firstView:Landroid/view/View;
    if-eqz v10, :cond_1d

    #@1d0
    .line 4735
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@1d3
    move-result v11

    #@1d4
    .line 4736
    .local v11, firstViewTop:I
    if-lez v9, :cond_222

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mExtraScroll:I

    #@1da
    move/from16 v31, v0

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1e0
    move-object/from16 v32, v0

    #@1e2
    move-object/from16 v0, v32

    #@1e4
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1e6
    move-object/from16 v32, v0

    #@1e8
    move-object/from16 v0, v32

    #@1ea
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@1ec
    move/from16 v32, v0

    #@1ee
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->max(II)I

    #@1f1
    move-result v8

    #@1f2
    .line 4739
    .restart local v8       #extraScroll:I
    :goto_1f2
    move-object/from16 v0, p0

    #@1f4
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1f6
    move-object/from16 v31, v0

    #@1f8
    sub-int v32, v11, v8

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@1fe
    move/from16 v33, v0

    #@200
    const/16 v34, 0x1

    #@202
    invoke-virtual/range {v31 .. v34}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@205
    .line 4741
    move-object/from16 v0, p0

    #@207
    iput v9, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@209
    .line 4743
    move-object/from16 v0, p0

    #@20b
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@20d
    move/from16 v31, v0

    #@20f
    move/from16 v0, v31

    #@211
    if-le v9, v0, :cond_1d

    #@213
    .line 4744
    move-object/from16 v0, p0

    #@215
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@217
    move-object/from16 v31, v0

    #@219
    move-object/from16 v0, v31

    #@21b
    move-object/from16 v1, p0

    #@21d
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@220
    goto/16 :goto_1d

    #@222
    .line 4736
    .end local v8           #extraScroll:I
    :cond_222
    move-object/from16 v0, p0

    #@224
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@226
    move-object/from16 v31, v0

    #@228
    move-object/from16 v0, v31

    #@22a
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@22c
    move-object/from16 v31, v0

    #@22e
    move-object/from16 v0, v31

    #@230
    iget v8, v0, Landroid/graphics/Rect;->top:I

    #@232
    goto :goto_1f2

    #@233
    .line 4750
    .end local v10           #firstView:Landroid/view/View;
    .end local v11           #firstViewTop:I
    :pswitch_233
    move-object/from16 v0, p0

    #@235
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@237
    move-object/from16 v31, v0

    #@239
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getChildCount()I

    #@23c
    move-result v31

    #@23d
    add-int/lit8 v16, v31, -0x2

    #@23f
    .line 4751
    .restart local v16       #lastViewIndex:I
    if-ltz v16, :cond_1d

    #@241
    .line 4754
    add-int v12, v9, v16

    #@243
    .line 4756
    .restart local v12       #lastPos:I
    move-object/from16 v0, p0

    #@245
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@247
    move/from16 v31, v0

    #@249
    move/from16 v0, v31

    #@24b
    if-ne v12, v0, :cond_25c

    #@24d
    .line 4758
    move-object/from16 v0, p0

    #@24f
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@251
    move-object/from16 v31, v0

    #@253
    move-object/from16 v0, v31

    #@255
    move-object/from16 v1, p0

    #@257
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@25a
    goto/16 :goto_1d

    #@25c
    .line 4762
    :cond_25c
    move-object/from16 v0, p0

    #@25e
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@260
    move-object/from16 v31, v0

    #@262
    move-object/from16 v0, v31

    #@264
    move/from16 v1, v16

    #@266
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@269
    move-result-object v13

    #@26a
    .line 4763
    .restart local v13       #lastView:Landroid/view/View;
    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    #@26d
    move-result v15

    #@26e
    .line 4764
    .restart local v15       #lastViewHeight:I
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    #@271
    move-result v18

    #@272
    .line 4765
    .restart local v18       #lastViewTop:I
    sub-int v17, v19, v18

    #@274
    .line 4766
    .restart local v17       #lastViewPixelsShowing:I
    move-object/from16 v0, p0

    #@276
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@278
    move-object/from16 v31, v0

    #@27a
    move-object/from16 v0, v31

    #@27c
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@27e
    move-object/from16 v31, v0

    #@280
    move-object/from16 v0, v31

    #@282
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@284
    move/from16 v31, v0

    #@286
    move-object/from16 v0, p0

    #@288
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mExtraScroll:I

    #@28a
    move/from16 v32, v0

    #@28c
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->max(II)I

    #@28f
    move-result v8

    #@290
    .line 4767
    .restart local v8       #extraScroll:I
    move-object/from16 v0, p0

    #@292
    iput v12, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@294
    .line 4768
    move-object/from16 v0, p0

    #@296
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@298
    move/from16 v31, v0

    #@29a
    move/from16 v0, v31

    #@29c
    if-le v12, v0, :cond_2c5

    #@29e
    .line 4769
    move-object/from16 v0, p0

    #@2a0
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2a2
    move-object/from16 v31, v0

    #@2a4
    sub-int v32, v17, v8

    #@2a6
    move/from16 v0, v32

    #@2a8
    neg-int v0, v0

    #@2a9
    move/from16 v32, v0

    #@2ab
    move-object/from16 v0, p0

    #@2ad
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@2af
    move/from16 v33, v0

    #@2b1
    const/16 v34, 0x1

    #@2b3
    invoke-virtual/range {v31 .. v34}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@2b6
    .line 4770
    move-object/from16 v0, p0

    #@2b8
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2ba
    move-object/from16 v31, v0

    #@2bc
    move-object/from16 v0, v31

    #@2be
    move-object/from16 v1, p0

    #@2c0
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@2c3
    goto/16 :goto_1d

    #@2c5
    .line 4772
    :cond_2c5
    sub-int v4, v19, v8

    #@2c7
    .line 4773
    .local v4, bottom:I
    add-int v14, v18, v15

    #@2c9
    .line 4774
    .local v14, lastViewBottom:I
    if-le v4, v14, :cond_1d

    #@2cb
    .line 4775
    move-object/from16 v0, p0

    #@2cd
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2cf
    move-object/from16 v31, v0

    #@2d1
    sub-int v32, v4, v14

    #@2d3
    move/from16 v0, v32

    #@2d5
    neg-int v0, v0

    #@2d6
    move/from16 v32, v0

    #@2d8
    move-object/from16 v0, p0

    #@2da
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@2dc
    move/from16 v33, v0

    #@2de
    const/16 v34, 0x1

    #@2e0
    invoke-virtual/range {v31 .. v34}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@2e3
    goto/16 :goto_1d

    #@2e5
    .line 4782
    .end local v4           #bottom:I
    .end local v8           #extraScroll:I
    .end local v12           #lastPos:I
    .end local v13           #lastView:Landroid/view/View;
    .end local v14           #lastViewBottom:I
    .end local v15           #lastViewHeight:I
    .end local v16           #lastViewIndex:I
    .end local v17           #lastViewPixelsShowing:I
    .end local v18           #lastViewTop:I
    :pswitch_2e5
    move-object/from16 v0, p0

    #@2e7
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@2e9
    move/from16 v31, v0

    #@2eb
    move/from16 v0, v31

    #@2ed
    if-ne v0, v9, :cond_2fe

    #@2ef
    .line 4784
    move-object/from16 v0, p0

    #@2f1
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2f3
    move-object/from16 v31, v0

    #@2f5
    move-object/from16 v0, v31

    #@2f7
    move-object/from16 v1, p0

    #@2f9
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@2fc
    goto/16 :goto_1d

    #@2fe
    .line 4788
    :cond_2fe
    move-object/from16 v0, p0

    #@300
    iput v9, v0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@302
    .line 4790
    move-object/from16 v0, p0

    #@304
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@306
    move-object/from16 v31, v0

    #@308
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getChildCount()I

    #@30b
    move-result v5

    #@30c
    .line 4791
    .restart local v5       #childCount:I
    move-object/from16 v0, p0

    #@30e
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@310
    move/from16 v26, v0

    #@312
    .line 4792
    .local v26, position:I
    add-int v31, v9, v5

    #@314
    add-int/lit8 v12, v31, -0x1

    #@316
    .line 4794
    .restart local v12       #lastPos:I
    const/16 v30, 0x0

    #@318
    .line 4795
    .local v30, viewTravelCount:I
    move/from16 v0, v26

    #@31a
    if-ge v0, v9, :cond_37f

    #@31c
    .line 4796
    sub-int v31, v9, v26

    #@31e
    add-int/lit8 v30, v31, 0x1

    #@320
    .line 4802
    :cond_320
    :goto_320
    move/from16 v0, v30

    #@322
    int-to-float v0, v0

    #@323
    move/from16 v31, v0

    #@325
    int-to-float v0, v5

    #@326
    move/from16 v32, v0

    #@328
    div-float v27, v31, v32

    #@32a
    .line 4804
    .local v27, screenTravelCount:F
    invoke-static/range {v27 .. v27}, Ljava/lang/Math;->abs(F)F

    #@32d
    move-result v31

    #@32e
    const/high16 v32, 0x3f80

    #@330
    invoke-static/range {v31 .. v32}, Ljava/lang/Math;->min(FF)F

    #@333
    move-result v20

    #@334
    .line 4805
    .local v20, modifier:F
    move/from16 v0, v26

    #@336
    if-ge v0, v9, :cond_386

    #@338
    .line 4806
    move-object/from16 v0, p0

    #@33a
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@33c
    move-object/from16 v31, v0

    #@33e
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getHeight()I

    #@341
    move-result v31

    #@342
    move/from16 v0, v31

    #@344
    neg-int v0, v0

    #@345
    move/from16 v31, v0

    #@347
    move/from16 v0, v31

    #@349
    int-to-float v0, v0

    #@34a
    move/from16 v31, v0

    #@34c
    mul-float v31, v31, v20

    #@34e
    move/from16 v0, v31

    #@350
    float-to-int v6, v0

    #@351
    .line 4807
    .local v6, distance:I
    move-object/from16 v0, p0

    #@353
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@355
    move/from16 v31, v0

    #@357
    move/from16 v0, v31

    #@359
    int-to-float v0, v0

    #@35a
    move/from16 v31, v0

    #@35c
    mul-float v31, v31, v20

    #@35e
    move/from16 v0, v31

    #@360
    float-to-int v7, v0

    #@361
    .line 4808
    .local v7, duration:I
    move-object/from16 v0, p0

    #@363
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@365
    move-object/from16 v31, v0

    #@367
    const/16 v32, 0x1

    #@369
    move-object/from16 v0, v31

    #@36b
    move/from16 v1, v32

    #@36d
    invoke-virtual {v0, v6, v7, v1}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@370
    .line 4809
    move-object/from16 v0, p0

    #@372
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@374
    move-object/from16 v31, v0

    #@376
    move-object/from16 v0, v31

    #@378
    move-object/from16 v1, p0

    #@37a
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@37d
    goto/16 :goto_1d

    #@37f
    .line 4797
    .end local v6           #distance:I
    .end local v7           #duration:I
    .end local v20           #modifier:F
    .end local v27           #screenTravelCount:F
    :cond_37f
    move/from16 v0, v26

    #@381
    if-le v0, v12, :cond_320

    #@383
    .line 4798
    sub-int v30, v26, v12

    #@385
    goto :goto_320

    #@386
    .line 4810
    .restart local v20       #modifier:F
    .restart local v27       #screenTravelCount:F
    :cond_386
    move/from16 v0, v26

    #@388
    if-le v0, v12, :cond_3cc

    #@38a
    .line 4811
    move-object/from16 v0, p0

    #@38c
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@38e
    move-object/from16 v31, v0

    #@390
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView;->getHeight()I

    #@393
    move-result v31

    #@394
    move/from16 v0, v31

    #@396
    int-to-float v0, v0

    #@397
    move/from16 v31, v0

    #@399
    mul-float v31, v31, v20

    #@39b
    move/from16 v0, v31

    #@39d
    float-to-int v6, v0

    #@39e
    .line 4812
    .restart local v6       #distance:I
    move-object/from16 v0, p0

    #@3a0
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@3a2
    move/from16 v31, v0

    #@3a4
    move/from16 v0, v31

    #@3a6
    int-to-float v0, v0

    #@3a7
    move/from16 v31, v0

    #@3a9
    mul-float v31, v31, v20

    #@3ab
    move/from16 v0, v31

    #@3ad
    float-to-int v7, v0

    #@3ae
    .line 4813
    .restart local v7       #duration:I
    move-object/from16 v0, p0

    #@3b0
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@3b2
    move-object/from16 v31, v0

    #@3b4
    const/16 v32, 0x1

    #@3b6
    move-object/from16 v0, v31

    #@3b8
    move/from16 v1, v32

    #@3ba
    invoke-virtual {v0, v6, v7, v1}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@3bd
    .line 4814
    move-object/from16 v0, p0

    #@3bf
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@3c1
    move-object/from16 v31, v0

    #@3c3
    move-object/from16 v0, v31

    #@3c5
    move-object/from16 v1, p0

    #@3c7
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@3ca
    goto/16 :goto_1d

    #@3cc
    .line 4817
    .end local v6           #distance:I
    .end local v7           #duration:I
    :cond_3cc
    move-object/from16 v0, p0

    #@3ce
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@3d0
    move-object/from16 v31, v0

    #@3d2
    sub-int v32, v26, v9

    #@3d4
    invoke-virtual/range {v31 .. v32}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@3d7
    move-result-object v31

    #@3d8
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getTop()I

    #@3db
    move-result v29

    #@3dc
    .line 4818
    .local v29, targetTop:I
    move-object/from16 v0, p0

    #@3de
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mOffsetFromTop:I

    #@3e0
    move/from16 v31, v0

    #@3e2
    sub-int v6, v29, v31

    #@3e4
    .line 4819
    .restart local v6       #distance:I
    move-object/from16 v0, p0

    #@3e6
    iget v0, v0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@3e8
    move/from16 v31, v0

    #@3ea
    move/from16 v0, v31

    #@3ec
    int-to-float v0, v0

    #@3ed
    move/from16 v31, v0

    #@3ef
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    #@3f2
    move-result v32

    #@3f3
    move/from16 v0, v32

    #@3f5
    int-to-float v0, v0

    #@3f6
    move/from16 v32, v0

    #@3f8
    move-object/from16 v0, p0

    #@3fa
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@3fc
    move-object/from16 v33, v0

    #@3fe
    invoke-virtual/range {v33 .. v33}, Landroid/widget/AbsListView;->getHeight()I

    #@401
    move-result v33

    #@402
    move/from16 v0, v33

    #@404
    int-to-float v0, v0

    #@405
    move/from16 v33, v0

    #@407
    div-float v32, v32, v33

    #@409
    mul-float v31, v31, v32

    #@40b
    move/from16 v0, v31

    #@40d
    float-to-int v7, v0

    #@40e
    .line 4821
    .restart local v7       #duration:I
    move-object/from16 v0, p0

    #@410
    iget-object v0, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@412
    move-object/from16 v31, v0

    #@414
    const/16 v32, 0x1

    #@416
    move-object/from16 v0, v31

    #@418
    move/from16 v1, v32

    #@41a
    invoke-virtual {v0, v6, v7, v1}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@41d
    goto/16 :goto_1d

    #@41f
    .line 4657
    nop

    #@420
    :pswitch_data_420
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_1a9
        :pswitch_d7
        :pswitch_233
        :pswitch_2e5
    .end packed-switch
.end method

.method scrollToVisible(III)V
    .registers 21
    .parameter "targetPos"
    .parameter "boundPos"
    .parameter "duration"

    #@0
    .prologue
    .line 4600
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@4
    iget v6, v14, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6
    .line 4601
    .local v6, firstPos:I
    move-object/from16 v0, p0

    #@8
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@a
    invoke-virtual {v14}, Landroid/widget/AbsListView;->getChildCount()I

    #@d
    move-result v5

    #@e
    .line 4602
    .local v5, childCount:I
    add-int v14, v6, v5

    #@10
    add-int/lit8 v7, v14, -0x1

    #@12
    .line 4603
    .local v7, lastPos:I
    move-object/from16 v0, p0

    #@14
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@16
    iget-object v14, v14, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@18
    iget v9, v14, Landroid/graphics/Rect;->top:I

    #@1a
    .line 4604
    .local v9, paddedTop:I
    move-object/from16 v0, p0

    #@1c
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1e
    invoke-virtual {v14}, Landroid/widget/AbsListView;->getHeight()I

    #@21
    move-result v14

    #@22
    move-object/from16 v0, p0

    #@24
    iget-object v15, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@26
    iget-object v15, v15, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@28
    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    #@2a
    sub-int v8, v14, v15

    #@2c
    .line 4606
    .local v8, paddedBottom:I
    move/from16 v0, p1

    #@2e
    if-lt v0, v6, :cond_34

    #@30
    move/from16 v0, p1

    #@32
    if-le v0, v7, :cond_69

    #@34
    .line 4607
    :cond_34
    const-string v14, "AbsListView"

    #@36
    new-instance v15, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string/jumbo v16, "scrollToVisible called with targetPos "

    #@3e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v15

    #@42
    move/from16 v0, p1

    #@44
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v15

    #@48
    const-string v16, " not visible ["

    #@4a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v15

    #@4e
    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v15

    #@52
    const-string v16, ", "

    #@54
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v15

    #@58
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v15

    #@5c
    const-string v16, "]"

    #@5e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v15

    #@62
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v15

    #@66
    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 4610
    :cond_69
    move/from16 v0, p2

    #@6b
    if-lt v0, v6, :cond_71

    #@6d
    move/from16 v0, p2

    #@6f
    if-le v0, v7, :cond_73

    #@71
    .line 4612
    :cond_71
    const/16 p2, -0x1

    #@73
    .line 4615
    :cond_73
    move-object/from16 v0, p0

    #@75
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@77
    sub-int v15, p1, v6

    #@79
    invoke-virtual {v14, v15}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@7c
    move-result-object v12

    #@7d
    .line 4616
    .local v12, targetChild:Landroid/view/View;
    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    #@80
    move-result v13

    #@81
    .line 4617
    .local v13, targetTop:I
    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    #@84
    move-result v11

    #@85
    .line 4618
    .local v11, targetBottom:I
    const/4 v10, 0x0

    #@86
    .line 4620
    .local v10, scrollBy:I
    if-le v11, v8, :cond_8a

    #@88
    .line 4621
    sub-int v10, v11, v8

    #@8a
    .line 4623
    :cond_8a
    if-ge v13, v9, :cond_8e

    #@8c
    .line 4624
    sub-int v10, v13, v9

    #@8e
    .line 4627
    :cond_8e
    if-nez v10, :cond_91

    #@90
    .line 4647
    :goto_90
    return-void

    #@91
    .line 4631
    :cond_91
    if-ltz p2, :cond_b6

    #@93
    .line 4632
    move-object/from16 v0, p0

    #@95
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@97
    sub-int v15, p2, v6

    #@99
    invoke-virtual {v14, v15}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@9c
    move-result-object v3

    #@9d
    .line 4633
    .local v3, boundChild:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@a0
    move-result v4

    #@a1
    .line 4634
    .local v4, boundTop:I
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@a4
    move-result v2

    #@a5
    .line 4635
    .local v2, boundBottom:I
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@a8
    move-result v1

    #@a9
    .line 4637
    .local v1, absScroll:I
    if-gez v10, :cond_c0

    #@ab
    add-int v14, v2, v1

    #@ad
    if-le v14, v8, :cond_c0

    #@af
    .line 4639
    const/4 v14, 0x0

    #@b0
    sub-int v15, v2, v8

    #@b2
    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    #@b5
    move-result v10

    #@b6
    .line 4646
    .end local v1           #absScroll:I
    .end local v2           #boundBottom:I
    .end local v3           #boundChild:Landroid/view/View;
    .end local v4           #boundTop:I
    :cond_b6
    :goto_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v14, v0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@ba
    move/from16 v0, p3

    #@bc
    invoke-virtual {v14, v10, v0}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    #@bf
    goto :goto_90

    #@c0
    .line 4640
    .restart local v1       #absScroll:I
    .restart local v2       #boundBottom:I
    .restart local v3       #boundChild:Landroid/view/View;
    .restart local v4       #boundTop:I
    :cond_c0
    if-lez v10, :cond_b6

    #@c2
    sub-int v14, v4, v1

    #@c4
    if-ge v14, v9, :cond_b6

    #@c6
    .line 4642
    const/4 v14, 0x0

    #@c7
    sub-int v15, v4, v9

    #@c9
    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    #@cc
    move-result v10

    #@cd
    goto :goto_b6
.end method

.method start(I)V
    .registers 11
    .parameter "position"

    #@0
    .prologue
    const/16 v8, 0xc8

    #@2
    const/4 v7, -0x1

    #@3
    .line 4415
    invoke-virtual {p0}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@6
    .line 4417
    iget-object v5, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@8
    iget-boolean v5, v5, Landroid/widget/AdapterView;->mDataChanged:Z

    #@a
    if-eqz v5, :cond_16

    #@c
    .line 4419
    iget-object v5, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@e
    new-instance v6, Landroid/widget/AbsListView$PositionScroller$1;

    #@10
    invoke-direct {v6, p0, p1}, Landroid/widget/AbsListView$PositionScroller$1;-><init>(Landroid/widget/AbsListView$PositionScroller;I)V

    #@13
    iput-object v6, v5, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@15
    .line 4459
    :cond_15
    :goto_15
    return-void

    #@16
    .line 4427
    :cond_16
    iget-object v5, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@18
    invoke-virtual {v5}, Landroid/widget/AbsListView;->getChildCount()I

    #@1b
    move-result v0

    #@1c
    .line 4428
    .local v0, childCount:I
    if-eqz v0, :cond_15

    #@1e
    .line 4433
    iget-object v5, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@20
    iget v2, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@22
    .line 4434
    .local v2, firstPos:I
    add-int v5, v2, v0

    #@24
    add-int/lit8 v3, v5, -0x1

    #@26
    .line 4437
    .local v3, lastPos:I
    const/4 v5, 0x0

    #@27
    iget-object v6, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@29
    invoke-virtual {v6}, Landroid/widget/AbsListView;->getCount()I

    #@2c
    move-result v6

    #@2d
    add-int/lit8 v6, v6, -0x1

    #@2f
    invoke-static {v6, p1}, Ljava/lang/Math;->min(II)I

    #@32
    move-result v6

    #@33
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@36
    move-result v1

    #@37
    .line 4438
    .local v1, clampedPosition:I
    if-ge v1, v2, :cond_52

    #@39
    .line 4439
    sub-int v5, v2, v1

    #@3b
    add-int/lit8 v4, v5, 0x1

    #@3d
    .line 4440
    .local v4, viewTravelCount:I
    const/4 v5, 0x2

    #@3e
    iput v5, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@40
    .line 4449
    :goto_40
    if-lez v4, :cond_60

    #@42
    .line 4450
    div-int v5, v8, v4

    #@44
    iput v5, p0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@46
    .line 4454
    :goto_46
    iput v1, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@48
    .line 4455
    iput v7, p0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@4a
    .line 4456
    iput v7, p0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@4c
    .line 4458
    iget-object v5, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@4e
    invoke-virtual {v5, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@51
    goto :goto_15

    #@52
    .line 4441
    .end local v4           #viewTravelCount:I
    :cond_52
    if-le v1, v3, :cond_5c

    #@54
    .line 4442
    sub-int v5, v1, v3

    #@56
    add-int/lit8 v4, v5, 0x1

    #@58
    .line 4443
    .restart local v4       #viewTravelCount:I
    const/4 v5, 0x1

    #@59
    iput v5, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@5b
    goto :goto_40

    #@5c
    .line 4445
    .end local v4           #viewTravelCount:I
    :cond_5c
    invoke-virtual {p0, v1, v7, v8}, Landroid/widget/AbsListView$PositionScroller;->scrollToVisible(III)V

    #@5f
    goto :goto_15

    #@60
    .line 4452
    .restart local v4       #viewTravelCount:I
    :cond_60
    iput v8, p0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@62
    goto :goto_46
.end method

.method start(II)V
    .registers 14
    .parameter "position"
    .parameter "boundPosition"

    #@0
    .prologue
    .line 4462
    invoke-virtual {p0}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@3
    .line 4464
    const/4 v9, -0x1

    #@4
    if-ne p2, v9, :cond_a

    #@6
    .line 4465
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView$PositionScroller;->start(I)V

    #@9
    .line 4537
    :cond_9
    :goto_9
    return-void

    #@a
    .line 4469
    :cond_a
    iget-object v9, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@c
    iget-boolean v9, v9, Landroid/widget/AdapterView;->mDataChanged:Z

    #@e
    if-eqz v9, :cond_1a

    #@10
    .line 4471
    iget-object v9, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@12
    new-instance v10, Landroid/widget/AbsListView$PositionScroller$2;

    #@14
    invoke-direct {v10, p0, p1, p2}, Landroid/widget/AbsListView$PositionScroller$2;-><init>(Landroid/widget/AbsListView$PositionScroller;II)V

    #@17
    iput-object v10, v9, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@19
    goto :goto_9

    #@1a
    .line 4479
    :cond_1a
    iget-object v9, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1c
    invoke-virtual {v9}, Landroid/widget/AbsListView;->getChildCount()I

    #@1f
    move-result v3

    #@20
    .line 4480
    .local v3, childCount:I
    if-eqz v3, :cond_9

    #@22
    .line 4485
    iget-object v9, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@24
    iget v5, v9, Landroid/widget/AdapterView;->mFirstPosition:I

    #@26
    .line 4486
    .local v5, firstPos:I
    add-int v9, v5, v3

    #@28
    add-int/lit8 v6, v9, -0x1

    #@2a
    .line 4489
    .local v6, lastPos:I
    const/4 v9, 0x0

    #@2b
    iget-object v10, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2d
    invoke-virtual {v10}, Landroid/widget/AbsListView;->getCount()I

    #@30
    move-result v10

    #@31
    add-int/lit8 v10, v10, -0x1

    #@33
    invoke-static {v10, p1}, Ljava/lang/Math;->min(II)I

    #@36
    move-result v10

    #@37
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@3a
    move-result v4

    #@3b
    .line 4490
    .local v4, clampedPosition:I
    if-ge v4, v5, :cond_67

    #@3d
    .line 4491
    sub-int v1, v6, p2

    #@3f
    .line 4492
    .local v1, boundPosFromLast:I
    const/4 v9, 0x1

    #@40
    if-lt v1, v9, :cond_9

    #@42
    .line 4497
    sub-int v9, v5, v4

    #@44
    add-int/lit8 v7, v9, 0x1

    #@46
    .line 4498
    .local v7, posTravel:I
    add-int/lit8 v2, v1, -0x1

    #@48
    .line 4499
    .local v2, boundTravel:I
    if-ge v2, v7, :cond_62

    #@4a
    .line 4500
    move v8, v2

    #@4b
    .line 4501
    .local v8, viewTravelCount:I
    const/4 v9, 0x4

    #@4c
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@4e
    .line 4527
    .end local v1           #boundPosFromLast:I
    :goto_4e
    if-lez v8, :cond_86

    #@50
    .line 4528
    const/16 v9, 0xc8

    #@52
    div-int/2addr v9, v8

    #@53
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@55
    .line 4532
    :goto_55
    iput v4, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@57
    .line 4533
    iput p2, p0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@59
    .line 4534
    const/4 v9, -0x1

    #@5a
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@5c
    .line 4536
    iget-object v9, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@5e
    invoke-virtual {v9, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@61
    goto :goto_9

    #@62
    .line 4503
    .end local v8           #viewTravelCount:I
    .restart local v1       #boundPosFromLast:I
    :cond_62
    move v8, v7

    #@63
    .line 4504
    .restart local v8       #viewTravelCount:I
    const/4 v9, 0x2

    #@64
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@66
    goto :goto_4e

    #@67
    .line 4506
    .end local v1           #boundPosFromLast:I
    .end local v2           #boundTravel:I
    .end local v7           #posTravel:I
    .end local v8           #viewTravelCount:I
    :cond_67
    if-le v4, v6, :cond_80

    #@69
    .line 4507
    sub-int v0, p2, v5

    #@6b
    .line 4508
    .local v0, boundPosFromFirst:I
    const/4 v9, 0x1

    #@6c
    if-lt v0, v9, :cond_9

    #@6e
    .line 4513
    sub-int v9, v4, v6

    #@70
    add-int/lit8 v7, v9, 0x1

    #@72
    .line 4514
    .restart local v7       #posTravel:I
    add-int/lit8 v2, v0, -0x1

    #@74
    .line 4515
    .restart local v2       #boundTravel:I
    if-ge v2, v7, :cond_7b

    #@76
    .line 4516
    move v8, v2

    #@77
    .line 4517
    .restart local v8       #viewTravelCount:I
    const/4 v9, 0x3

    #@78
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@7a
    goto :goto_4e

    #@7b
    .line 4519
    .end local v8           #viewTravelCount:I
    :cond_7b
    move v8, v7

    #@7c
    .line 4520
    .restart local v8       #viewTravelCount:I
    const/4 v9, 0x1

    #@7d
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@7f
    goto :goto_4e

    #@80
    .line 4523
    .end local v0           #boundPosFromFirst:I
    .end local v2           #boundTravel:I
    .end local v7           #posTravel:I
    .end local v8           #viewTravelCount:I
    :cond_80
    const/16 v9, 0xc8

    #@82
    invoke-virtual {p0, v4, p2, v9}, Landroid/widget/AbsListView$PositionScroller;->scrollToVisible(III)V

    #@85
    goto :goto_9

    #@86
    .line 4530
    .restart local v2       #boundTravel:I
    .restart local v7       #posTravel:I
    .restart local v8       #viewTravelCount:I
    :cond_86
    const/16 v9, 0xc8

    #@88
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@8a
    goto :goto_55
.end method

.method startWithOffset(II)V
    .registers 4
    .parameter "position"
    .parameter "offset"

    #@0
    .prologue
    .line 4540
    const/16 v0, 0xc8

    #@2
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/AbsListView$PositionScroller;->startWithOffset(III)V

    #@5
    .line 4541
    return-void
.end method

.method startWithOffset(III)V
    .registers 14
    .parameter "position"
    .parameter "offset"
    .parameter "duration"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    .line 4544
    invoke-virtual {p0}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@4
    .line 4546
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@6
    iget-boolean v7, v7, Landroid/widget/AdapterView;->mDataChanged:Z

    #@8
    if-eqz v7, :cond_15

    #@a
    .line 4548
    move v3, p2

    #@b
    .line 4549
    .local v3, postOffset:I
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@d
    new-instance v8, Landroid/widget/AbsListView$PositionScroller$3;

    #@f
    invoke-direct {v8, p0, p1, v3, p3}, Landroid/widget/AbsListView$PositionScroller$3;-><init>(Landroid/widget/AbsListView$PositionScroller;III)V

    #@12
    iput-object v8, v7, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@14
    .line 4593
    .end local v3           #postOffset:I
    .end local p3
    :cond_14
    :goto_14
    return-void

    #@15
    .line 4557
    .restart local p3
    :cond_15
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@17
    invoke-virtual {v7}, Landroid/widget/AbsListView;->getChildCount()I

    #@1a
    move-result v0

    #@1b
    .line 4558
    .local v0, childCount:I
    if-eqz v0, :cond_14

    #@1d
    .line 4563
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@1f
    invoke-virtual {v7}, Landroid/widget/AbsListView;->getPaddingTop()I

    #@22
    move-result v7

    #@23
    add-int/2addr p2, v7

    #@24
    .line 4565
    const/4 v7, 0x0

    #@25
    iget-object v8, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@27
    invoke-virtual {v8}, Landroid/widget/AbsListView;->getCount()I

    #@2a
    move-result v8

    #@2b
    add-int/lit8 v8, v8, -0x1

    #@2d
    invoke-static {v8, p1}, Ljava/lang/Math;->min(II)I

    #@30
    move-result v8

    #@31
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@34
    move-result v7

    #@35
    iput v7, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@37
    .line 4566
    iput p2, p0, Landroid/widget/AbsListView$PositionScroller;->mOffsetFromTop:I

    #@39
    .line 4567
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mBoundPos:I

    #@3b
    .line 4568
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@3d
    .line 4569
    const/4 v7, 0x5

    #@3e
    iput v7, p0, Landroid/widget/AbsListView$PositionScroller;->mMode:I

    #@40
    .line 4571
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@42
    iget v1, v7, Landroid/widget/AdapterView;->mFirstPosition:I

    #@44
    .line 4572
    .local v1, firstPos:I
    add-int v7, v1, v0

    #@46
    add-int/lit8 v2, v7, -0x1

    #@48
    .line 4575
    .local v2, lastPos:I
    iget v7, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@4a
    if-ge v7, v1, :cond_64

    #@4c
    .line 4576
    iget v7, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@4e
    sub-int v6, v1, v7

    #@50
    .line 4587
    .local v6, viewTravelCount:I
    :goto_50
    int-to-float v7, v6

    #@51
    int-to-float v8, v0

    #@52
    div-float v4, v7, v8

    #@54
    .line 4588
    .local v4, screenTravelCount:F
    const/high16 v7, 0x3f80

    #@56
    cmpg-float v7, v4, v7

    #@58
    if-gez v7, :cond_83

    #@5a
    .end local p3
    :goto_5a
    iput p3, p0, Landroid/widget/AbsListView$PositionScroller;->mScrollDuration:I

    #@5c
    .line 4590
    iput v9, p0, Landroid/widget/AbsListView$PositionScroller;->mLastSeenPos:I

    #@5e
    .line 4592
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@60
    invoke-virtual {v7, p0}, Landroid/widget/AbsListView;->postOnAnimation(Ljava/lang/Runnable;)V

    #@63
    goto :goto_14

    #@64
    .line 4577
    .end local v4           #screenTravelCount:F
    .end local v6           #viewTravelCount:I
    .restart local p3
    :cond_64
    iget v7, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@66
    if-le v7, v2, :cond_6d

    #@68
    .line 4578
    iget v7, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@6a
    sub-int v6, v7, v2

    #@6c
    .restart local v6       #viewTravelCount:I
    goto :goto_50

    #@6d
    .line 4581
    .end local v6           #viewTravelCount:I
    :cond_6d
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@6f
    iget v8, p0, Landroid/widget/AbsListView$PositionScroller;->mTargetPos:I

    #@71
    sub-int/2addr v8, v1

    #@72
    invoke-virtual {v7, v8}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@79
    move-result v5

    #@7a
    .line 4582
    .local v5, targetTop:I
    iget-object v7, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@7c
    sub-int v8, v5, p2

    #@7e
    const/4 v9, 0x1

    #@7f
    invoke-virtual {v7, v8, p3, v9}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@82
    goto :goto_14

    #@83
    .line 4588
    .end local v5           #targetTop:I
    .restart local v4       #screenTravelCount:F
    .restart local v6       #viewTravelCount:I
    :cond_83
    int-to-float v7, p3

    #@84
    div-float/2addr v7, v4

    #@85
    float-to-int p3, v7

    #@86
    goto :goto_5a
.end method

.method stop()V
    .registers 2

    #@0
    .prologue
    .line 4650
    iget-object v0, p0, Landroid/widget/AbsListView$PositionScroller;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5
    .line 4651
    return-void
.end method
