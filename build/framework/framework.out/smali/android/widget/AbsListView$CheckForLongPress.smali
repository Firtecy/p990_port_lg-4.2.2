.class Landroid/widget/AbsListView$CheckForLongPress;
.super Landroid/widget/AbsListView$WindowRunnnable;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method private constructor <init>(Landroid/widget/AbsListView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2788
    iput-object p1, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Landroid/widget/AbsListView$WindowRunnnable;-><init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V

    #@6
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2788
    invoke-direct {p0, p1}, Landroid/widget/AbsListView$CheckForLongPress;-><init>(Landroid/widget/AbsListView;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2790
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@3
    iget v5, v6, Landroid/widget/AbsListView;->mMotionPosition:I

    #@5
    .line 2791
    .local v5, motionPosition:I
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@7
    iget-object v7, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@9
    iget v7, v7, Landroid/widget/AdapterView;->mFirstPosition:I

    #@b
    sub-int v7, v5, v7

    #@d
    invoke-virtual {v6, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 2792
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_45

    #@13
    .line 2793
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@15
    iget v4, v6, Landroid/widget/AbsListView;->mMotionPosition:I

    #@17
    .line 2794
    .local v4, longPressPosition:I
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@19
    iget-object v6, v6, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1b
    iget-object v7, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@1d
    iget v7, v7, Landroid/widget/AbsListView;->mMotionPosition:I

    #@1f
    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@22
    move-result-wide v2

    #@23
    .line 2796
    .local v2, longPressId:J
    const/4 v1, 0x0

    #@24
    .line 2797
    .local v1, handled:Z
    invoke-virtual {p0}, Landroid/widget/AbsListView$CheckForLongPress;->sameWindow()Z

    #@27
    move-result v6

    #@28
    if-eqz v6, :cond_36

    #@2a
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@2c
    iget-boolean v6, v6, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2e
    if-nez v6, :cond_36

    #@30
    .line 2798
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@32
    invoke-virtual {v6, v0, v4, v2, v3}, Landroid/widget/AbsListView;->performLongPress(Landroid/view/View;IJ)Z

    #@35
    move-result v1

    #@36
    .line 2800
    :cond_36
    if-eqz v1, :cond_46

    #@38
    .line 2801
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@3a
    const/4 v7, -0x1

    #@3b
    iput v7, v6, Landroid/widget/AbsListView;->mTouchMode:I

    #@3d
    .line 2802
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@3f
    invoke-virtual {v6, v8}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@42
    .line 2803
    invoke-virtual {v0, v8}, Landroid/view/View;->setPressed(Z)V

    #@45
    .line 2808
    .end local v1           #handled:Z
    .end local v2           #longPressId:J
    .end local v4           #longPressPosition:I
    :cond_45
    :goto_45
    return-void

    #@46
    .line 2805
    .restart local v1       #handled:Z
    .restart local v2       #longPressId:J
    .restart local v4       #longPressPosition:I
    :cond_46
    iget-object v6, p0, Landroid/widget/AbsListView$CheckForLongPress;->this$0:Landroid/widget/AbsListView;

    #@48
    const/4 v7, 0x2

    #@49
    iput v7, v6, Landroid/widget/AbsListView;->mTouchMode:I

    #@4b
    goto :goto_45
.end method
