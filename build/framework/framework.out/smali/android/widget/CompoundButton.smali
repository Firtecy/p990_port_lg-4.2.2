.class public abstract Landroid/widget/CompoundButton;
.super Landroid/widget/Button;
.source "CompoundButton.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/CompoundButton$1;,
        Landroid/widget/CompoundButton$SavedState;,
        Landroid/widget/CompoundButton$OnCheckedChangeListener;
    }
.end annotation


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private mBroadcasting:Z

.field private mButtonDrawable:Landroid/graphics/drawable/Drawable;

.field private mButtonResource:I

.field private mChecked:Z

.field private mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mOnCheckedChangeWidgetListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 55
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x10100a0

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Landroid/widget/CompoundButton;->CHECKED_STATE_SET:[I

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 60
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 70
    sget-object v3, Lcom/android/internal/R$styleable;->CompoundButton:[I

    #@6
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 74
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x1

    #@b
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v2

    #@f
    .line 75
    .local v2, d:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_14

    #@11
    .line 76
    invoke-virtual {p0, v2}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    #@14
    .line 79
    :cond_14
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@17
    move-result v1

    #@18
    .line 81
    .local v1, checked:Z
    invoke-virtual {p0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@1b
    .line 83
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1e
    .line 84
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 299
    invoke-super {p0}, Landroid/widget/Button;->drawableStateChanged()V

    #@3
    .line 301
    iget-object v1, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v1, :cond_13

    #@7
    .line 302
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getDrawableState()[I

    #@a
    move-result-object v0

    #@b
    .line 305
    .local v0, myDrawableState:[I
    iget-object v1, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@10
    .line 307
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->invalidate()V

    #@13
    .line 309
    .end local v0           #myDrawableState:[I
    :cond_13
    return-void
.end method

.method public getCompoundPaddingLeft()I
    .registers 4

    #@0
    .prologue
    .line 229
    invoke-super {p0}, Landroid/widget/Button;->getCompoundPaddingLeft()I

    #@3
    move-result v1

    #@4
    .line 230
    .local v1, padding:I
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isLayoutRtl()Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_13

    #@a
    .line 231
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    .line 232
    .local v0, buttonDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_13

    #@e
    .line 233
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@11
    move-result v2

    #@12
    add-int/2addr v1, v2

    #@13
    .line 236
    .end local v0           #buttonDrawable:Landroid/graphics/drawable/Drawable;
    :cond_13
    return v1
.end method

.method public getCompoundPaddingRight()I
    .registers 4

    #@0
    .prologue
    .line 241
    invoke-super {p0}, Landroid/widget/Button;->getCompoundPaddingRight()I

    #@3
    move-result v1

    #@4
    .line 242
    .local v1, padding:I
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isLayoutRtl()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_13

    #@a
    .line 243
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    .line 244
    .local v0, buttonDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_13

    #@e
    .line 245
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@11
    move-result v2

    #@12
    add-int/2addr v1, v2

    #@13
    .line 248
    .end local v0           #buttonDrawable:Landroid/graphics/drawable/Drawable;
    :cond_13
    return v1
.end method

.method public getHorizontalOffsetForDrawables()I
    .registers 3

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 257
    .local v0, buttonDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_9

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public isChecked()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 105
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@2
    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 318
    invoke-super {p0}, Landroid/widget/Button;->jumpDrawablesToCurrentState()V

    #@3
    .line 319
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 320
    :cond_c
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 290
    add-int/lit8 v1, p1, 0x1

    #@2
    invoke-super {p0, v1}, Landroid/widget/Button;->onCreateDrawableState(I)[I

    #@5
    move-result-object v0

    #@6
    .line 291
    .local v0, drawableState:[I
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isChecked()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 292
    sget-object v1, Landroid/widget/CompoundButton;->CHECKED_STATE_SET:[I

    #@e
    invoke-static {v0, v1}, Landroid/widget/CompoundButton;->mergeDrawableStates([I[I)[I

    #@11
    .line 294
    :cond_11
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 11
    .parameter "canvas"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/widget/Button;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 264
    iget-object v1, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 265
    .local v1, buttonDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_37

    #@7
    .line 266
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getGravity()I

    #@a
    move-result v8

    #@b
    and-int/lit8 v7, v8, 0x70

    #@d
    .line 267
    .local v7, verticalGravity:I
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@10
    move-result v2

    #@11
    .line 268
    .local v2, drawableHeight:I
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@14
    move-result v3

    #@15
    .line 270
    .local v3, drawableWidth:I
    const/4 v6, 0x0

    #@16
    .line 271
    .local v6, top:I
    sparse-switch v7, :sswitch_data_4c

    #@19
    .line 279
    :goto_19
    add-int v0, v6, v2

    #@1b
    .line 280
    .local v0, bottom:I
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isLayoutRtl()Z

    #@1e
    move-result v8

    #@1f
    if-eqz v8, :cond_47

    #@21
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getWidth()I

    #@24
    move-result v8

    #@25
    sub-int v4, v8, v3

    #@27
    .line 281
    .local v4, left:I
    :goto_27
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isLayoutRtl()Z

    #@2a
    move-result v8

    #@2b
    if-eqz v8, :cond_49

    #@2d
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getWidth()I

    #@30
    move-result v5

    #@31
    .line 283
    .local v5, right:I
    :goto_31
    invoke-virtual {v1, v4, v6, v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@34
    .line 284
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@37
    .line 286
    .end local v0           #bottom:I
    .end local v2           #drawableHeight:I
    .end local v3           #drawableWidth:I
    .end local v4           #left:I
    .end local v5           #right:I
    .end local v6           #top:I
    .end local v7           #verticalGravity:I
    :cond_37
    return-void

    #@38
    .line 273
    .restart local v2       #drawableHeight:I
    .restart local v3       #drawableWidth:I
    .restart local v6       #top:I
    .restart local v7       #verticalGravity:I
    :sswitch_38
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getHeight()I

    #@3b
    move-result v8

    #@3c
    sub-int v6, v8, v2

    #@3e
    .line 274
    goto :goto_19

    #@3f
    .line 276
    :sswitch_3f
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getHeight()I

    #@42
    move-result v8

    #@43
    sub-int/2addr v8, v2

    #@44
    div-int/lit8 v6, v8, 0x2

    #@46
    goto :goto_19

    #@47
    .line 280
    .restart local v0       #bottom:I
    :cond_47
    const/4 v4, 0x0

    #@48
    goto :goto_27

    #@49
    .restart local v4       #left:I
    :cond_49
    move v5, v3

    #@4a
    .line 281
    goto :goto_31

    #@4b
    .line 271
    nop

    #@4c
    :sswitch_data_4c
    .sparse-switch
        0x10 -> :sswitch_3f
        0x50 -> :sswitch_38
    .end sparse-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 214
    invoke-super {p0, p1}, Landroid/widget/Button;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 215
    const-class v0, Landroid/widget/CompoundButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 216
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@e
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    #@11
    .line 217
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/widget/Button;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 222
    const-class v0, Landroid/widget/CompoundButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 223
    const/4 v0, 0x1

    #@d
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    #@10
    .line 224
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@12
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    #@15
    .line 225
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 379
    move-object v0, p1

    #@1
    check-cast v0, Landroid/widget/CompoundButton$SavedState;

    #@3
    .line 381
    .local v0, ss:Landroid/widget/CompoundButton$SavedState;
    invoke-virtual {v0}, Landroid/widget/CompoundButton$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/widget/Button;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 382
    iget-boolean v1, v0, Landroid/widget/CompoundButton$SavedState;->checked:Z

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@f
    .line 383
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->requestLayout()V

    #@12
    .line 384
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 368
    const/4 v2, 0x1

    #@1
    invoke-virtual {p0, v2}, Landroid/widget/CompoundButton;->setFreezesText(Z)V

    #@4
    .line 369
    invoke-super {p0}, Landroid/widget/Button;->onSaveInstanceState()Landroid/os/Parcelable;

    #@7
    move-result-object v1

    #@8
    .line 371
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Landroid/widget/CompoundButton$SavedState;

    #@a
    invoke-direct {v0, v1}, Landroid/widget/CompoundButton$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@d
    .line 373
    .local v0, ss:Landroid/widget/CompoundButton$SavedState;
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isChecked()Z

    #@10
    move-result v2

    #@11
    iput-boolean v2, v0, Landroid/widget/CompoundButton$SavedState;->checked:Z

    #@13
    .line 374
    return-object v0
.end method

.method public performClick()Z
    .registers 2

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->toggle()V

    #@3
    .line 100
    invoke-super {p0}, Landroid/widget/Button;->performClick()Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public setButtonDrawable(I)V
    .registers 5
    .parameter "resid"

    #@0
    .prologue
    .line 177
    if-eqz p1, :cond_7

    #@2
    iget v1, p0, Landroid/widget/CompoundButton;->mButtonResource:I

    #@4
    if-ne p1, v1, :cond_7

    #@6
    .line 188
    :goto_6
    return-void

    #@7
    .line 181
    :cond_7
    iput p1, p0, Landroid/widget/CompoundButton;->mButtonResource:I

    #@9
    .line 183
    const/4 v0, 0x0

    #@a
    .line 184
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iget v1, p0, Landroid/widget/CompoundButton;->mButtonResource:I

    #@c
    if-eqz v1, :cond_18

    #@e
    .line 185
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    #@11
    move-result-object v1

    #@12
    iget v2, p0, Landroid/widget/CompoundButton;->mButtonResource:I

    #@14
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    .line 187
    :cond_18
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1b
    goto :goto_6
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "d"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 196
    if-eqz p1, :cond_36

    #@4
    .line 197
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 198
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@d
    .line 199
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@12
    .line 201
    :cond_12
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@15
    .line 202
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getDrawableState()[I

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@1c
    .line 203
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getVisibility()I

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_3a

    #@22
    const/4 v0, 0x1

    #@23
    :goto_23
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@26
    .line 204
    iput-object p1, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@28
    .line 205
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@2a
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@2d
    .line 206
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@2f
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@32
    move-result v0

    #@33
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->setMinHeight(I)V

    #@36
    .line 209
    :cond_36
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    #@39
    .line 210
    return-void

    #@3a
    :cond_3a
    move v0, v1

    #@3b
    .line 203
    goto :goto_23
.end method

.method public setChecked(Z)V
    .registers 4
    .parameter "checked"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@2
    if-eq v0, p1, :cond_10

    #@4
    .line 115
    iput-boolean p1, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@6
    .line 116
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    #@9
    .line 117
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->notifyAccessibilityStateChanged()V

    #@c
    .line 120
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mBroadcasting:Z

    #@e
    if-eqz v0, :cond_11

    #@10
    .line 134
    :cond_10
    :goto_10
    return-void

    #@11
    .line 124
    :cond_11
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/widget/CompoundButton;->mBroadcasting:Z

    #@14
    .line 125
    iget-object v0, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@16
    if-eqz v0, :cond_1f

    #@18
    .line 126
    iget-object v0, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@1a
    iget-boolean v1, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@1c
    invoke-interface {v0, p0, v1}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    #@1f
    .line 128
    :cond_1f
    iget-object v0, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeWidgetListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@21
    if-eqz v0, :cond_2a

    #@23
    .line 129
    iget-object v0, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeWidgetListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@25
    iget-boolean v1, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@27
    invoke-interface {v0, p0, v1}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    #@2a
    .line 132
    :cond_2a
    const/4 v0, 0x0

    #@2b
    iput-boolean v0, p0, Landroid/widget/CompoundButton;->mBroadcasting:Z

    #@2d
    goto :goto_10
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 143
    iput-object p1, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@2
    .line 144
    return-void
.end method

.method setOnCheckedChangeWidgetListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Landroid/widget/CompoundButton;->mOnCheckedChangeWidgetListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@2
    .line 155
    return-void
.end method

.method public toggle()V
    .registers 2

    #@0
    .prologue
    .line 87
    iget-boolean v0, p0, Landroid/widget/CompoundButton;->mChecked:Z

    #@2
    if-nez v0, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@8
    .line 88
    return-void

    #@9
    .line 87
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 313
    invoke-super {p0, p1}, Landroid/widget/Button;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    iget-object v0, p0, Landroid/widget/CompoundButton;->mButtonDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    if-ne p1, v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
