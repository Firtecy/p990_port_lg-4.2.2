.class public abstract Landroid/widget/BaseAdapter;
.super Ljava/lang/Object;
.source "BaseAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field private final mDataSetObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    new-instance v0, Landroid/database/DataSetObservable;

    #@5
    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/BaseAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@a
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 63
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 71
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getViewTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 83
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->getCount()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isEnabled(I)Z
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public notifyDataSetChanged()V
    .registers 2

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/widget/BaseAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    #@5
    .line 51
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .registers 2

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/widget/BaseAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    #@5
    .line 60
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/widget/BaseAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    #@5
    .line 39
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/widget/BaseAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    #@5
    .line 43
    return-void
.end method
