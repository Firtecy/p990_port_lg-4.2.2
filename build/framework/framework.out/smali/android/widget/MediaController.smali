.class public Landroid/widget/MediaController;
.super Landroid/widget/FrameLayout;
.source "MediaController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/MediaController$MediaPlayerControl;
    }
.end annotation


# static fields
.field private static final FADE_OUT:I = 0x1

.field private static final SHOW_PROGRESS:I = 0x2

.field private static final sDefaultTimeout:I = 0xbb8


# instance fields
.field private mAnchor:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDecor:Landroid/view/View;

.field private mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFfwdButton:Landroid/widget/ImageButton;

.field private mFfwdListener:Landroid/view/View$OnClickListener;

.field mFormatBuilder:Ljava/lang/StringBuilder;

.field mFormatter:Ljava/util/Formatter;

.field private mFromXml:Z

.field private mHandler:Landroid/os/Handler;

.field private mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mListenersSet:Z

.field private mNextButton:Landroid/widget/ImageButton;

.field private mNextListener:Landroid/view/View$OnClickListener;

.field private mPauseButton:Landroid/widget/ImageButton;

.field private mPauseListener:Landroid/view/View$OnClickListener;

.field private mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

.field private mPrevButton:Landroid/widget/ImageButton;

.field private mPrevListener:Landroid/view/View$OnClickListener;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mRewButton:Landroid/widget/ImageButton;

.field private mRewListener:Landroid/view/View$OnClickListener;

.field private mRoot:Landroid/view/View;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mShowing:Z

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUseFastForward:Z

.field private mWindow:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 123
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;Z)V

    #@4
    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 176
    new-instance v0, Landroid/widget/MediaController$1;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/MediaController$1;-><init>(Landroid/widget/MediaController;)V

    #@9
    iput-object v0, p0, Landroid/widget/MediaController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    #@b
    .line 188
    new-instance v0, Landroid/widget/MediaController$2;

    #@d
    invoke-direct {v0, p0}, Landroid/widget/MediaController$2;-><init>(Landroid/widget/MediaController;)V

    #@10
    iput-object v0, p0, Landroid/widget/MediaController;->mTouchListener:Landroid/view/View$OnTouchListener;

    #@12
    .line 377
    new-instance v0, Landroid/widget/MediaController$3;

    #@14
    invoke-direct {v0, p0}, Landroid/widget/MediaController$3;-><init>(Landroid/widget/MediaController;)V

    #@17
    iput-object v0, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@19
    .line 495
    new-instance v0, Landroid/widget/MediaController$4;

    #@1b
    invoke-direct {v0, p0}, Landroid/widget/MediaController$4;-><init>(Landroid/widget/MediaController;)V

    #@1e
    iput-object v0, p0, Landroid/widget/MediaController;->mPauseListener:Landroid/view/View$OnClickListener;

    #@20
    .line 533
    new-instance v0, Landroid/widget/MediaController$5;

    #@22
    invoke-direct {v0, p0}, Landroid/widget/MediaController$5;-><init>(Landroid/widget/MediaController;)V

    #@25
    iput-object v0, p0, Landroid/widget/MediaController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    #@27
    .line 610
    new-instance v0, Landroid/widget/MediaController$6;

    #@29
    invoke-direct {v0, p0}, Landroid/widget/MediaController$6;-><init>(Landroid/widget/MediaController;)V

    #@2c
    iput-object v0, p0, Landroid/widget/MediaController;->mRewListener:Landroid/view/View$OnClickListener;

    #@2e
    .line 621
    new-instance v0, Landroid/widget/MediaController$7;

    #@30
    invoke-direct {v0, p0}, Landroid/widget/MediaController$7;-><init>(Landroid/widget/MediaController;)V

    #@33
    iput-object v0, p0, Landroid/widget/MediaController;->mFfwdListener:Landroid/view/View$OnClickListener;

    #@35
    .line 102
    iput-object p0, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@37
    .line 103
    iput-object p1, p0, Landroid/widget/MediaController;->mContext:Landroid/content/Context;

    #@39
    .line 104
    iput-boolean v1, p0, Landroid/widget/MediaController;->mUseFastForward:Z

    #@3b
    .line 105
    iput-boolean v1, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@3d
    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 4
    .parameter "context"
    .parameter "useFastForward"

    #@0
    .prologue
    .line 115
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 176
    new-instance v0, Landroid/widget/MediaController$1;

    #@5
    invoke-direct {v0, p0}, Landroid/widget/MediaController$1;-><init>(Landroid/widget/MediaController;)V

    #@8
    iput-object v0, p0, Landroid/widget/MediaController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    #@a
    .line 188
    new-instance v0, Landroid/widget/MediaController$2;

    #@c
    invoke-direct {v0, p0}, Landroid/widget/MediaController$2;-><init>(Landroid/widget/MediaController;)V

    #@f
    iput-object v0, p0, Landroid/widget/MediaController;->mTouchListener:Landroid/view/View$OnTouchListener;

    #@11
    .line 377
    new-instance v0, Landroid/widget/MediaController$3;

    #@13
    invoke-direct {v0, p0}, Landroid/widget/MediaController$3;-><init>(Landroid/widget/MediaController;)V

    #@16
    iput-object v0, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@18
    .line 495
    new-instance v0, Landroid/widget/MediaController$4;

    #@1a
    invoke-direct {v0, p0}, Landroid/widget/MediaController$4;-><init>(Landroid/widget/MediaController;)V

    #@1d
    iput-object v0, p0, Landroid/widget/MediaController;->mPauseListener:Landroid/view/View$OnClickListener;

    #@1f
    .line 533
    new-instance v0, Landroid/widget/MediaController$5;

    #@21
    invoke-direct {v0, p0}, Landroid/widget/MediaController$5;-><init>(Landroid/widget/MediaController;)V

    #@24
    iput-object v0, p0, Landroid/widget/MediaController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    #@26
    .line 610
    new-instance v0, Landroid/widget/MediaController$6;

    #@28
    invoke-direct {v0, p0}, Landroid/widget/MediaController$6;-><init>(Landroid/widget/MediaController;)V

    #@2b
    iput-object v0, p0, Landroid/widget/MediaController;->mRewListener:Landroid/view/View$OnClickListener;

    #@2d
    .line 621
    new-instance v0, Landroid/widget/MediaController$7;

    #@2f
    invoke-direct {v0, p0}, Landroid/widget/MediaController$7;-><init>(Landroid/widget/MediaController;)V

    #@32
    iput-object v0, p0, Landroid/widget/MediaController;->mFfwdListener:Landroid/view/View$OnClickListener;

    #@34
    .line 116
    iput-object p1, p0, Landroid/widget/MediaController;->mContext:Landroid/content/Context;

    #@36
    .line 117
    iput-boolean p2, p0, Landroid/widget/MediaController;->mUseFastForward:Z

    #@38
    .line 118
    invoke-direct {p0}, Landroid/widget/MediaController;->initFloatingWindowLayout()V

    #@3b
    .line 119
    invoke-direct {p0}, Landroid/widget/MediaController;->initFloatingWindow()V

    #@3e
    .line 120
    return-void
.end method

.method static synthetic access$000(Landroid/widget/MediaController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/MediaController;->updateFloatingWindowLayout()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/MediaController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/widget/MediaController;->mShowing:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Landroid/widget/MediaController;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mCurrentTime:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/widget/MediaController;I)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/widget/MediaController;->stringForTime(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1200(Landroid/widget/MediaController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/MediaController;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mDecor:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/MediaController;)Landroid/view/WindowManager$LayoutParams;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/MediaController;)Landroid/view/WindowManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mWindowManager:Landroid/view/WindowManager;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/MediaController;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/MediaController;->setProgress()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Landroid/widget/MediaController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/widget/MediaController;->mDragging:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Landroid/widget/MediaController;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Landroid/widget/MediaController;->mDragging:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Landroid/widget/MediaController;)Landroid/widget/MediaController$MediaPlayerControl;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/MediaController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/widget/MediaController;->doPauseResume()V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/widget/MediaController;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private disableUnsupportedButtons()V
    .registers 3

    #@0
    .prologue
    .line 307
    :try_start_0
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@6
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->canPause()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_12

    #@c
    .line 308
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@12
    .line 310
    :cond_12
    iget-object v0, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@14
    if-eqz v0, :cond_24

    #@16
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@18
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->canSeekBackward()Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_24

    #@1e
    .line 311
    iget-object v0, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@20
    const/4 v1, 0x0

    #@21
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@24
    .line 313
    :cond_24
    iget-object v0, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@26
    if-eqz v0, :cond_36

    #@28
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2a
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->canSeekForward()Z

    #@2d
    move-result v0

    #@2e
    if-nez v0, :cond_36

    #@30
    .line 314
    iget-object v0, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@32
    const/4 v1, 0x0

    #@33
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V
    :try_end_36
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_36} :catch_37

    #@36
    .line 322
    :cond_36
    :goto_36
    return-void

    #@37
    .line 316
    :catch_37
    move-exception v0

    #@38
    goto :goto_36
.end method

.method private doPauseResume()V
    .registers 2

    #@0
    .prologue
    .line 514
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 515
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@a
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->pause()V

    #@d
    .line 519
    :goto_d
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@10
    .line 520
    return-void

    #@11
    .line 517
    :cond_11
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@13
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->start()V

    #@16
    goto :goto_d
.end method

.method private initControllerView(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/16 v3, 0x8

    #@3
    .line 244
    const v1, 0x1020325

    #@6
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/widget/ImageButton;

    #@c
    iput-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@e
    .line 245
    iget-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@10
    if-eqz v1, :cond_1e

    #@12
    .line 246
    iget-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@14
    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    #@17
    .line 247
    iget-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@19
    iget-object v4, p0, Landroid/widget/MediaController;->mPauseListener:Landroid/view/View$OnClickListener;

    #@1b
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@1e
    .line 250
    :cond_1e
    const v1, 0x1020326

    #@21
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Landroid/widget/ImageButton;

    #@27
    iput-object v1, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@29
    .line 251
    iget-object v1, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@2b
    if-eqz v1, :cond_42

    #@2d
    .line 252
    iget-object v1, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@2f
    iget-object v4, p0, Landroid/widget/MediaController;->mFfwdListener:Landroid/view/View$OnClickListener;

    #@31
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@34
    .line 253
    iget-boolean v1, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@36
    if-nez v1, :cond_42

    #@38
    .line 254
    iget-object v4, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@3a
    iget-boolean v1, p0, Landroid/widget/MediaController;->mUseFastForward:Z

    #@3c
    if-eqz v1, :cond_f0

    #@3e
    move v1, v2

    #@3f
    :goto_3f
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@42
    .line 258
    :cond_42
    const v1, 0x1020324

    #@45
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Landroid/widget/ImageButton;

    #@4b
    iput-object v1, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@4d
    .line 259
    iget-object v1, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@4f
    if-eqz v1, :cond_65

    #@51
    .line 260
    iget-object v1, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@53
    iget-object v4, p0, Landroid/widget/MediaController;->mRewListener:Landroid/view/View$OnClickListener;

    #@55
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@58
    .line 261
    iget-boolean v1, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@5a
    if-nez v1, :cond_65

    #@5c
    .line 262
    iget-object v1, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@5e
    iget-boolean v4, p0, Landroid/widget/MediaController;->mUseFastForward:Z

    #@60
    if-eqz v4, :cond_f3

    #@62
    :goto_62
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@65
    .line 267
    :cond_65
    const v1, 0x1020327

    #@68
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6b
    move-result-object v1

    #@6c
    check-cast v1, Landroid/widget/ImageButton;

    #@6e
    iput-object v1, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@70
    .line 268
    iget-object v1, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@72
    if-eqz v1, :cond_81

    #@74
    iget-boolean v1, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@76
    if-nez v1, :cond_81

    #@78
    iget-boolean v1, p0, Landroid/widget/MediaController;->mListenersSet:Z

    #@7a
    if-nez v1, :cond_81

    #@7c
    .line 269
    iget-object v1, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@7e
    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@81
    .line 271
    :cond_81
    const v1, 0x1020323

    #@84
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@87
    move-result-object v1

    #@88
    check-cast v1, Landroid/widget/ImageButton;

    #@8a
    iput-object v1, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@8c
    .line 272
    iget-object v1, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@8e
    if-eqz v1, :cond_9d

    #@90
    iget-boolean v1, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@92
    if-nez v1, :cond_9d

    #@94
    iget-boolean v1, p0, Landroid/widget/MediaController;->mListenersSet:Z

    #@96
    if-nez v1, :cond_9d

    #@98
    .line 273
    iget-object v1, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@9a
    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@9d
    .line 276
    :cond_9d
    const v1, 0x1020329

    #@a0
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@a3
    move-result-object v1

    #@a4
    check-cast v1, Landroid/widget/ProgressBar;

    #@a6
    iput-object v1, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@a8
    .line 277
    iget-object v1, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@aa
    if-eqz v1, :cond_c2

    #@ac
    .line 278
    iget-object v1, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@ae
    instance-of v1, v1, Landroid/widget/SeekBar;

    #@b0
    if-eqz v1, :cond_bb

    #@b2
    .line 279
    iget-object v0, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@b4
    check-cast v0, Landroid/widget/SeekBar;

    #@b6
    .line 280
    .local v0, seeker:Landroid/widget/SeekBar;
    iget-object v1, p0, Landroid/widget/MediaController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    #@b8
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@bb
    .line 282
    .end local v0           #seeker:Landroid/widget/SeekBar;
    :cond_bb
    iget-object v1, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@bd
    const/16 v2, 0x3e8

    #@bf
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    #@c2
    .line 285
    :cond_c2
    const v1, 0x1020064

    #@c5
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@c8
    move-result-object v1

    #@c9
    check-cast v1, Landroid/widget/TextView;

    #@cb
    iput-object v1, p0, Landroid/widget/MediaController;->mEndTime:Landroid/widget/TextView;

    #@cd
    .line 286
    const v1, 0x1020328

    #@d0
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@d3
    move-result-object v1

    #@d4
    check-cast v1, Landroid/widget/TextView;

    #@d6
    iput-object v1, p0, Landroid/widget/MediaController;->mCurrentTime:Landroid/widget/TextView;

    #@d8
    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    iput-object v1, p0, Landroid/widget/MediaController;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@df
    .line 288
    new-instance v1, Ljava/util/Formatter;

    #@e1
    iget-object v2, p0, Landroid/widget/MediaController;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@e3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@e6
    move-result-object v3

    #@e7
    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    #@ea
    iput-object v1, p0, Landroid/widget/MediaController;->mFormatter:Ljava/util/Formatter;

    #@ec
    .line 290
    invoke-direct {p0}, Landroid/widget/MediaController;->installPrevNextListeners()V

    #@ef
    .line 291
    return-void

    #@f0
    :cond_f0
    move v1, v3

    #@f1
    .line 254
    goto/16 :goto_3f

    #@f3
    :cond_f3
    move v2, v3

    #@f4
    .line 262
    goto/16 :goto_62
.end method

.method private initFloatingWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 127
    iget-object v0, p0, Landroid/widget/MediaController;->mContext:Landroid/content/Context;

    #@4
    const-string/jumbo v1, "window"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/view/WindowManager;

    #@d
    iput-object v0, p0, Landroid/widget/MediaController;->mWindowManager:Landroid/view/WindowManager;

    #@f
    .line 128
    iget-object v0, p0, Landroid/widget/MediaController;->mContext:Landroid/content/Context;

    #@11
    invoke-static {v0}, Lcom/android/internal/policy/PolicyManager;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@17
    .line 129
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@19
    iget-object v1, p0, Landroid/widget/MediaController;->mWindowManager:Landroid/view/WindowManager;

    #@1b
    invoke-virtual {v0, v1, v3, v3}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;)V

    #@1e
    .line 130
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@20
    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    #@23
    .line 131
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@25
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Landroid/widget/MediaController;->mDecor:Landroid/view/View;

    #@2b
    .line 132
    iget-object v0, p0, Landroid/widget/MediaController;->mDecor:Landroid/view/View;

    #@2d
    iget-object v1, p0, Landroid/widget/MediaController;->mTouchListener:Landroid/view/View$OnTouchListener;

    #@2f
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@32
    .line 133
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@34
    invoke-virtual {v0, p0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    #@37
    .line 134
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@39
    const v1, 0x106000d

    #@3c
    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    #@3f
    .line 138
    iget-object v0, p0, Landroid/widget/MediaController;->mWindow:Landroid/view/Window;

    #@41
    const/4 v1, 0x3

    #@42
    invoke-virtual {v0, v1}, Landroid/view/Window;->setVolumeControlStream(I)V

    #@45
    .line 140
    invoke-virtual {p0, v2}, Landroid/widget/MediaController;->setFocusable(Z)V

    #@48
    .line 141
    invoke-virtual {p0, v2}, Landroid/widget/MediaController;->setFocusableInTouchMode(Z)V

    #@4b
    .line 142
    const/high16 v0, 0x4

    #@4d
    invoke-virtual {p0, v0}, Landroid/widget/MediaController;->setDescendantFocusability(I)V

    #@50
    .line 143
    invoke-virtual {p0}, Landroid/widget/MediaController;->requestFocus()Z

    #@53
    .line 144
    return-void
.end method

.method private initFloatingWindowLayout()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 150
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    #@3
    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@6
    iput-object v1, p0, Landroid/widget/MediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@8
    .line 151
    iget-object v0, p0, Landroid/widget/MediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    .line 152
    .local v0, p:Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x30

    #@c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@e
    .line 153
    const/4 v1, -0x2

    #@f
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@11
    .line 154
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@13
    .line 155
    const/4 v1, -0x3

    #@14
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@16
    .line 156
    const/16 v1, 0x3e8

    #@18
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1a
    .line 157
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1c
    const v2, 0x820020

    #@1f
    or-int/2addr v1, v2

    #@20
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@22
    .line 160
    const/4 v1, 0x0

    #@23
    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@25
    .line 161
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@27
    .line 162
    return-void
.end method

.method private installPrevNextListeners()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 633
    iget-object v0, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@4
    if-eqz v0, :cond_17

    #@6
    .line 634
    iget-object v0, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@8
    iget-object v3, p0, Landroid/widget/MediaController;->mNextListener:Landroid/view/View$OnClickListener;

    #@a
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d
    .line 635
    iget-object v3, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@f
    iget-object v0, p0, Landroid/widget/MediaController;->mNextListener:Landroid/view/View$OnClickListener;

    #@11
    if-eqz v0, :cond_2c

    #@13
    move v0, v1

    #@14
    :goto_14
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@17
    .line 638
    :cond_17
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@19
    if-eqz v0, :cond_2b

    #@1b
    .line 639
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@1d
    iget-object v3, p0, Landroid/widget/MediaController;->mPrevListener:Landroid/view/View$OnClickListener;

    #@1f
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@22
    .line 640
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@24
    iget-object v3, p0, Landroid/widget/MediaController;->mPrevListener:Landroid/view/View$OnClickListener;

    #@26
    if-eqz v3, :cond_2e

    #@28
    :goto_28
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@2b
    .line 642
    :cond_2b
    return-void

    #@2c
    :cond_2c
    move v0, v2

    #@2d
    .line 635
    goto :goto_14

    #@2e
    :cond_2e
    move v1, v2

    #@2f
    .line 640
    goto :goto_28
.end method

.method private setProgress()I
    .registers 10

    #@0
    .prologue
    .line 412
    iget-object v5, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2
    if-eqz v5, :cond_8

    #@4
    iget-boolean v5, p0, Landroid/widget/MediaController;->mDragging:Z

    #@6
    if-eqz v5, :cond_a

    #@8
    .line 413
    :cond_8
    const/4 v4, 0x0

    #@9
    .line 432
    :cond_9
    :goto_9
    return v4

    #@a
    .line 415
    :cond_a
    iget-object v5, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@c
    invoke-interface {v5}, Landroid/widget/MediaController$MediaPlayerControl;->getCurrentPosition()I

    #@f
    move-result v4

    #@10
    .line 416
    .local v4, position:I
    iget-object v5, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@12
    invoke-interface {v5}, Landroid/widget/MediaController$MediaPlayerControl;->getDuration()I

    #@15
    move-result v0

    #@16
    .line 417
    .local v0, duration:I
    iget-object v5, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@18
    if-eqz v5, :cond_36

    #@1a
    .line 418
    if-lez v0, :cond_29

    #@1c
    .line 420
    const-wide/16 v5, 0x3e8

    #@1e
    int-to-long v7, v4

    #@1f
    mul-long/2addr v5, v7

    #@20
    int-to-long v7, v0

    #@21
    div-long v2, v5, v7

    #@23
    .line 421
    .local v2, pos:J
    iget-object v5, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@25
    long-to-int v6, v2

    #@26
    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@29
    .line 423
    .end local v2           #pos:J
    :cond_29
    iget-object v5, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2b
    invoke-interface {v5}, Landroid/widget/MediaController$MediaPlayerControl;->getBufferPercentage()I

    #@2e
    move-result v1

    #@2f
    .line 424
    .local v1, percent:I
    iget-object v5, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@31
    mul-int/lit8 v6, v1, 0xa

    #@33
    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    #@36
    .line 427
    .end local v1           #percent:I
    :cond_36
    iget-object v5, p0, Landroid/widget/MediaController;->mEndTime:Landroid/widget/TextView;

    #@38
    if-eqz v5, :cond_43

    #@3a
    .line 428
    iget-object v5, p0, Landroid/widget/MediaController;->mEndTime:Landroid/widget/TextView;

    #@3c
    invoke-direct {p0, v0}, Landroid/widget/MediaController;->stringForTime(I)Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@43
    .line 429
    :cond_43
    iget-object v5, p0, Landroid/widget/MediaController;->mCurrentTime:Landroid/widget/TextView;

    #@45
    if-eqz v5, :cond_9

    #@47
    .line 430
    iget-object v5, p0, Landroid/widget/MediaController;->mCurrentTime:Landroid/widget/TextView;

    #@49
    invoke-direct {p0, v4}, Landroid/widget/MediaController;->stringForTime(I)Ljava/lang/String;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@50
    goto :goto_9
.end method

.method private stringForTime(I)Ljava/lang/String;
    .registers 13
    .parameter "timeMs"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 397
    div-int/lit16 v3, p1, 0x3e8

    #@5
    .line 399
    .local v3, totalSeconds:I
    rem-int/lit8 v2, v3, 0x3c

    #@7
    .line 400
    .local v2, seconds:I
    div-int/lit8 v4, v3, 0x3c

    #@9
    rem-int/lit8 v1, v4, 0x3c

    #@b
    .line 401
    .local v1, minutes:I
    div-int/lit16 v0, v3, 0xe10

    #@d
    .line 403
    .local v0, hours:I
    iget-object v4, p0, Landroid/widget/MediaController;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@f
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    #@12
    .line 404
    if-lez v0, :cond_36

    #@14
    .line 405
    iget-object v4, p0, Landroid/widget/MediaController;->mFormatter:Ljava/util/Formatter;

    #@16
    const-string v5, "%d:%02d:%02d"

    #@18
    const/4 v6, 0x3

    #@19
    new-array v6, v6, [Ljava/lang/Object;

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v7

    #@1f
    aput-object v7, v6, v8

    #@21
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v7

    #@25
    aput-object v7, v6, v9

    #@27
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v7

    #@2b
    aput-object v7, v6, v10

    #@2d
    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 407
    :goto_35
    return-object v4

    #@36
    :cond_36
    iget-object v4, p0, Landroid/widget/MediaController;->mFormatter:Ljava/util/Formatter;

    #@38
    const-string v5, "%02d:%02d"

    #@3a
    new-array v6, v10, [Ljava/lang/Object;

    #@3c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v7

    #@40
    aput-object v7, v6, v8

    #@42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v7

    #@46
    aput-object v7, v6, v9

    #@48
    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    goto :goto_35
.end method

.method private updateFloatingWindowLayout()V
    .registers 5

    #@0
    .prologue
    .line 167
    const/4 v2, 0x2

    #@1
    new-array v0, v2, [I

    #@3
    .line 168
    .local v0, anchorPos:[I
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@5
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    #@8
    .line 170
    iget-object v1, p0, Landroid/widget/MediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@a
    .line 171
    .local v1, p:Landroid/view/WindowManager$LayoutParams;
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@c
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    #@f
    move-result v2

    #@10
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@12
    .line 172
    const/4 v2, 0x1

    #@13
    aget v2, v0, v2

    #@15
    iget-object v3, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@17
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    #@1a
    move-result v3

    #@1b
    add-int/2addr v2, v3

    #@1c
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    #@1e
    .line 173
    return-void
.end method

.method private updatePausePlay()V
    .registers 3

    #@0
    .prologue
    .line 503
    iget-object v0, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@6
    if-nez v0, :cond_9

    #@8
    .line 511
    :cond_8
    :goto_8
    return-void

    #@9
    .line 506
    :cond_9
    iget-object v0, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@b
    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 507
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@13
    const v1, 0x1080023

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    #@19
    goto :goto_8

    #@1a
    .line 509
    :cond_1a
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@1c
    const v1, 0x1080024

    #@1f
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    #@22
    goto :goto_8
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/16 v4, 0xbb8

    #@2
    const/4 v2, 0x1

    #@3
    .line 449
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@6
    move-result v0

    #@7
    .line 450
    .local v0, keyCode:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_32

    #@d
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@10
    move-result v3

    #@11
    if-nez v3, :cond_32

    #@13
    move v1, v2

    #@14
    .line 452
    .local v1, uniqueDown:Z
    :goto_14
    const/16 v3, 0x4f

    #@16
    if-eq v0, v3, :cond_20

    #@18
    const/16 v3, 0x55

    #@1a
    if-eq v0, v3, :cond_20

    #@1c
    const/16 v3, 0x3e

    #@1e
    if-ne v0, v3, :cond_34

    #@20
    .line 455
    :cond_20
    if-eqz v1, :cond_31

    #@22
    .line 456
    invoke-direct {p0}, Landroid/widget/MediaController;->doPauseResume()V

    #@25
    .line 457
    invoke-virtual {p0, v4}, Landroid/widget/MediaController;->show(I)V

    #@28
    .line 458
    iget-object v3, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@2a
    if-eqz v3, :cond_31

    #@2c
    .line 459
    iget-object v3, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@2e
    invoke-virtual {v3}, Landroid/widget/ImageButton;->requestFocus()Z

    #@31
    .line 492
    :cond_31
    :goto_31
    return v2

    #@32
    .line 450
    .end local v1           #uniqueDown:Z
    :cond_32
    const/4 v1, 0x0

    #@33
    goto :goto_14

    #@34
    .line 463
    .restart local v1       #uniqueDown:Z
    :cond_34
    const/16 v3, 0x7e

    #@36
    if-ne v0, v3, :cond_4e

    #@38
    .line 464
    if-eqz v1, :cond_31

    #@3a
    iget-object v3, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@3c
    invoke-interface {v3}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    #@3f
    move-result v3

    #@40
    if-nez v3, :cond_31

    #@42
    .line 465
    iget-object v3, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@44
    invoke-interface {v3}, Landroid/widget/MediaController$MediaPlayerControl;->start()V

    #@47
    .line 466
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@4a
    .line 467
    invoke-virtual {p0, v4}, Landroid/widget/MediaController;->show(I)V

    #@4d
    goto :goto_31

    #@4e
    .line 470
    :cond_4e
    const/16 v3, 0x56

    #@50
    if-eq v0, v3, :cond_56

    #@52
    const/16 v3, 0x7f

    #@54
    if-ne v0, v3, :cond_6c

    #@56
    .line 472
    :cond_56
    if-eqz v1, :cond_31

    #@58
    iget-object v3, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@5a
    invoke-interface {v3}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    #@5d
    move-result v3

    #@5e
    if-eqz v3, :cond_31

    #@60
    .line 473
    iget-object v3, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@62
    invoke-interface {v3}, Landroid/widget/MediaController$MediaPlayerControl;->pause()V

    #@65
    .line 474
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@68
    .line 475
    invoke-virtual {p0, v4}, Landroid/widget/MediaController;->show(I)V

    #@6b
    goto :goto_31

    #@6c
    .line 478
    :cond_6c
    const/16 v3, 0x19

    #@6e
    if-eq v0, v3, :cond_7c

    #@70
    const/16 v3, 0x18

    #@72
    if-eq v0, v3, :cond_7c

    #@74
    const/16 v3, 0xa4

    #@76
    if-eq v0, v3, :cond_7c

    #@78
    const/16 v3, 0x1b

    #@7a
    if-ne v0, v3, :cond_81

    #@7c
    .line 483
    :cond_7c
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@7f
    move-result v2

    #@80
    goto :goto_31

    #@81
    .line 484
    :cond_81
    const/4 v3, 0x4

    #@82
    if-eq v0, v3, :cond_88

    #@84
    const/16 v3, 0x52

    #@86
    if-ne v0, v3, :cond_8e

    #@88
    .line 485
    :cond_88
    if-eqz v1, :cond_31

    #@8a
    .line 486
    invoke-virtual {p0}, Landroid/widget/MediaController;->hide()V

    #@8d
    goto :goto_31

    #@8e
    .line 491
    :cond_8e
    invoke-virtual {p0, v4}, Landroid/widget/MediaController;->show(I)V

    #@91
    .line 492
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@94
    move-result v2

    #@95
    goto :goto_31
.end method

.method public hide()V
    .registers 4

    #@0
    .prologue
    .line 363
    iget-object v1, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 375
    :cond_4
    :goto_4
    return-void

    #@5
    .line 366
    :cond_5
    iget-boolean v1, p0, Landroid/widget/MediaController;->mShowing:Z

    #@7
    if-eqz v1, :cond_4

    #@9
    .line 368
    :try_start_9
    iget-object v1, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@b
    const/4 v2, 0x2

    #@c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@f
    .line 369
    iget-object v1, p0, Landroid/widget/MediaController;->mWindowManager:Landroid/view/WindowManager;

    #@11
    iget-object v2, p0, Landroid/widget/MediaController;->mDecor:Landroid/view/View;

    #@13
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_16} :catch_1a

    #@16
    .line 373
    :goto_16
    const/4 v1, 0x0

    #@17
    iput-boolean v1, p0, Landroid/widget/MediaController;->mShowing:Z

    #@19
    goto :goto_4

    #@1a
    .line 370
    :catch_1a
    move-exception v0

    #@1b
    .line 371
    .local v0, ex:Ljava/lang/IllegalArgumentException;
    const-string v1, "MediaController"

    #@1d
    const-string v2, "already removed"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_16
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 356
    iget-boolean v0, p0, Landroid/widget/MediaController;->mShowing:Z

    #@2
    return v0
.end method

.method protected makeControllerView()Landroid/view/View;
    .registers 4

    #@0
    .prologue
    .line 235
    iget-object v1, p0, Landroid/widget/MediaController;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v2, "layout_inflater"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/LayoutInflater;

    #@b
    .line 236
    .local v0, inflate:Landroid/view/LayoutInflater;
    const v1, 0x1090083

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@12
    move-result-object v1

    #@13
    iput-object v1, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@15
    .line 238
    iget-object v1, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@17
    invoke-direct {p0, v1}, Landroid/widget/MediaController;->initControllerView(Landroid/view/View;)V

    #@1a
    .line 240
    iget-object v1, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@1c
    return-object v1
.end method

.method public onFinishInflate()V
    .registers 2

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 111
    iget-object v0, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@6
    invoke-direct {p0, v0}, Landroid/widget/MediaController;->initControllerView(Landroid/view/View;)V

    #@9
    .line 112
    :cond_9
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 600
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 601
    const-class v0, Landroid/widget/MediaController;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 602
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 606
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 607
    const-class v0, Landroid/widget/MediaController;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 608
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 437
    const/16 v0, 0xbb8

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/MediaController;->show(I)V

    #@5
    .line 438
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 443
    const/16 v0, 0xbb8

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/MediaController;->show(I)V

    #@5
    .line 444
    const/4 v0, 0x0

    #@6
    return v0
.end method

.method public setAnchorView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 210
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 211
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@7
    iget-object v3, p0, Landroid/widget/MediaController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    #@9
    invoke-virtual {v2, v3}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@c
    .line 213
    :cond_c
    iput-object p1, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@e
    .line 214
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@10
    if-eqz v2, :cond_19

    #@12
    .line 215
    iget-object v2, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@14
    iget-object v3, p0, Landroid/widget/MediaController;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    #@16
    invoke-virtual {v2, v3}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@19
    .line 218
    :cond_19
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@1b
    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@1e
    .line 223
    .local v0, frameParams:Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/MediaController;->removeAllViews()V

    #@21
    .line 224
    invoke-virtual {p0}, Landroid/widget/MediaController;->makeControllerView()Landroid/view/View;

    #@24
    move-result-object v1

    #@25
    .line 225
    .local v1, v:Landroid/view/View;
    invoke-virtual {p0, v1, v0}, Landroid/widget/MediaController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@28
    .line 226
    return-void
.end method

.method public setEnabled(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 576
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 577
    iget-object v0, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@8
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@b
    .line 579
    :cond_b
    iget-object v0, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 580
    iget-object v0, p0, Landroid/widget/MediaController;->mFfwdButton:Landroid/widget/ImageButton;

    #@11
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@14
    .line 582
    :cond_14
    iget-object v0, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@16
    if-eqz v0, :cond_1d

    #@18
    .line 583
    iget-object v0, p0, Landroid/widget/MediaController;->mRewButton:Landroid/widget/ImageButton;

    #@1a
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@1d
    .line 585
    :cond_1d
    iget-object v0, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@1f
    if-eqz v0, :cond_2d

    #@21
    .line 586
    iget-object v3, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@23
    if-eqz p1, :cond_4c

    #@25
    iget-object v0, p0, Landroid/widget/MediaController;->mNextListener:Landroid/view/View$OnClickListener;

    #@27
    if-eqz v0, :cond_4c

    #@29
    move v0, v1

    #@2a
    :goto_2a
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@2d
    .line 588
    :cond_2d
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@2f
    if-eqz v0, :cond_3c

    #@31
    .line 589
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@33
    if-eqz p1, :cond_4e

    #@35
    iget-object v3, p0, Landroid/widget/MediaController;->mPrevListener:Landroid/view/View$OnClickListener;

    #@37
    if-eqz v3, :cond_4e

    #@39
    :goto_39
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@3c
    .line 591
    :cond_3c
    iget-object v0, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@3e
    if-eqz v0, :cond_45

    #@40
    .line 592
    iget-object v0, p0, Landroid/widget/MediaController;->mProgress:Landroid/widget/ProgressBar;

    #@42
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    #@45
    .line 594
    :cond_45
    invoke-direct {p0}, Landroid/widget/MediaController;->disableUnsupportedButtons()V

    #@48
    .line 595
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    #@4b
    .line 596
    return-void

    #@4c
    :cond_4c
    move v0, v2

    #@4d
    .line 586
    goto :goto_2a

    #@4e
    :cond_4e
    move v1, v2

    #@4f
    .line 589
    goto :goto_39
.end method

.method public setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V
    .registers 2
    .parameter "player"

    #@0
    .prologue
    .line 200
    iput-object p1, p0, Landroid/widget/MediaController;->mPlayer:Landroid/widget/MediaController$MediaPlayerControl;

    #@2
    .line 201
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@5
    .line 202
    return-void
.end method

.method public setPrevNextListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .registers 5
    .parameter "next"
    .parameter "prev"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 645
    iput-object p1, p0, Landroid/widget/MediaController;->mNextListener:Landroid/view/View$OnClickListener;

    #@3
    .line 646
    iput-object p2, p0, Landroid/widget/MediaController;->mPrevListener:Landroid/view/View$OnClickListener;

    #@5
    .line 647
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/widget/MediaController;->mListenersSet:Z

    #@8
    .line 649
    iget-object v0, p0, Landroid/widget/MediaController;->mRoot:Landroid/view/View;

    #@a
    if-eqz v0, :cond_29

    #@c
    .line 650
    invoke-direct {p0}, Landroid/widget/MediaController;->installPrevNextListeners()V

    #@f
    .line 652
    iget-object v0, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@11
    if-eqz v0, :cond_1c

    #@13
    iget-boolean v0, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@15
    if-nez v0, :cond_1c

    #@17
    .line 653
    iget-object v0, p0, Landroid/widget/MediaController;->mNextButton:Landroid/widget/ImageButton;

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@1c
    .line 655
    :cond_1c
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@1e
    if-eqz v0, :cond_29

    #@20
    iget-boolean v0, p0, Landroid/widget/MediaController;->mFromXml:Z

    #@22
    if-nez v0, :cond_29

    #@24
    .line 656
    iget-object v0, p0, Landroid/widget/MediaController;->mPrevButton:Landroid/widget/ImageButton;

    #@26
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    #@29
    .line 659
    :cond_29
    return-void
.end method

.method public show()V
    .registers 2

    #@0
    .prologue
    .line 298
    const/16 v0, 0xbb8

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/MediaController;->show(I)V

    #@5
    .line 299
    return-void
.end method

.method public show(I)V
    .registers 7
    .parameter "timeout"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 331
    iget-boolean v1, p0, Landroid/widget/MediaController;->mShowing:Z

    #@3
    if-nez v1, :cond_26

    #@5
    iget-object v1, p0, Landroid/widget/MediaController;->mAnchor:Landroid/view/View;

    #@7
    if-eqz v1, :cond_26

    #@9
    .line 332
    invoke-direct {p0}, Landroid/widget/MediaController;->setProgress()I

    #@c
    .line 333
    iget-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@e
    if-eqz v1, :cond_15

    #@10
    .line 334
    iget-object v1, p0, Landroid/widget/MediaController;->mPauseButton:Landroid/widget/ImageButton;

    #@12
    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    #@15
    .line 336
    :cond_15
    invoke-direct {p0}, Landroid/widget/MediaController;->disableUnsupportedButtons()V

    #@18
    .line 337
    invoke-direct {p0}, Landroid/widget/MediaController;->updateFloatingWindowLayout()V

    #@1b
    .line 338
    iget-object v1, p0, Landroid/widget/MediaController;->mWindowManager:Landroid/view/WindowManager;

    #@1d
    iget-object v2, p0, Landroid/widget/MediaController;->mDecor:Landroid/view/View;

    #@1f
    iget-object v3, p0, Landroid/widget/MediaController;->mDecorLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@21
    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@24
    .line 339
    iput-boolean v4, p0, Landroid/widget/MediaController;->mShowing:Z

    #@26
    .line 341
    :cond_26
    invoke-direct {p0}, Landroid/widget/MediaController;->updatePausePlay()V

    #@29
    .line 346
    iget-object v1, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@2b
    const/4 v2, 0x2

    #@2c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@2f
    .line 348
    iget-object v1, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@31
    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@34
    move-result-object v0

    #@35
    .line 349
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_42

    #@37
    .line 350
    iget-object v1, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@39
    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@3c
    .line 351
    iget-object v1, p0, Landroid/widget/MediaController;->mHandler:Landroid/os/Handler;

    #@3e
    int-to-long v2, p1

    #@3f
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@42
    .line 353
    :cond_42
    return-void
.end method
