.class Landroid/widget/CalendarView$ScrollStateRunnable;
.super Ljava/lang/Object;
.source "CalendarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/CalendarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrollStateRunnable"
.end annotation


# instance fields
.field private mNewState:I

.field private mView:Landroid/widget/AbsListView;

.field final synthetic this$0:Landroid/widget/CalendarView;


# direct methods
.method private constructor <init>(Landroid/widget/CalendarView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1288
    iput-object p1, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/CalendarView;Landroid/widget/CalendarView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1288
    invoke-direct {p0, p1}, Landroid/widget/CalendarView$ScrollStateRunnable;-><init>(Landroid/widget/CalendarView;)V

    #@3
    return-void
.end method


# virtual methods
.method public doScrollStateChange(Landroid/widget/AbsListView;I)V
    .registers 6
    .parameter "view"
    .parameter "scrollState"

    #@0
    .prologue
    .line 1301
    iput-object p1, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mView:Landroid/widget/AbsListView;

    #@2
    .line 1302
    iput p2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mNewState:I

    #@4
    .line 1303
    iget-object v0, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@6
    invoke-virtual {v0, p0}, Landroid/widget/CalendarView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 1304
    iget-object v0, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@b
    const-wide/16 v1, 0x28

    #@d
    invoke-virtual {v0, p0, v1, v2}, Landroid/widget/CalendarView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@10
    .line 1305
    return-void
.end method

.method public run()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x1f4

    #@2
    .line 1308
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@4
    iget v3, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mNewState:I

    #@6
    invoke-static {v2, v3}, Landroid/widget/CalendarView;->access$1002(Landroid/widget/CalendarView;I)I

    #@9
    .line 1310
    iget v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mNewState:I

    #@b
    if-nez v2, :cond_46

    #@d
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@f
    invoke-static {v2}, Landroid/widget/CalendarView;->access$1100(Landroid/widget/CalendarView;)I

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_46

    #@15
    .line 1312
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mView:Landroid/widget/AbsListView;

    #@17
    const/4 v3, 0x0

    #@18
    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v0

    #@1c
    .line 1313
    .local v0, child:Landroid/view/View;
    if-nez v0, :cond_1f

    #@1e
    .line 1327
    .end local v0           #child:Landroid/view/View;
    :goto_1e
    return-void

    #@1f
    .line 1317
    .restart local v0       #child:Landroid/view/View;
    :cond_1f
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@22
    move-result v2

    #@23
    iget-object v3, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@25
    invoke-static {v3}, Landroid/widget/CalendarView;->access$1200(Landroid/widget/CalendarView;)I

    #@28
    move-result v3

    #@29
    sub-int v1, v2, v3

    #@2b
    .line 1318
    .local v1, dist:I
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@2d
    invoke-static {v2}, Landroid/widget/CalendarView;->access$1200(Landroid/widget/CalendarView;)I

    #@30
    move-result v2

    #@31
    if-le v1, v2, :cond_46

    #@33
    .line 1319
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@35
    invoke-static {v2}, Landroid/widget/CalendarView;->access$1300(Landroid/widget/CalendarView;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_4e

    #@3b
    .line 1320
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mView:Landroid/widget/AbsListView;

    #@3d
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@40
    move-result v3

    #@41
    sub-int v3, v1, v3

    #@43
    invoke-virtual {v2, v3, v4}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    #@46
    .line 1326
    .end local v0           #child:Landroid/view/View;
    .end local v1           #dist:I
    :cond_46
    :goto_46
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->this$0:Landroid/widget/CalendarView;

    #@48
    iget v3, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mNewState:I

    #@4a
    invoke-static {v2, v3}, Landroid/widget/CalendarView;->access$1102(Landroid/widget/CalendarView;I)I

    #@4d
    goto :goto_1e

    #@4e
    .line 1322
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #dist:I
    :cond_4e
    iget-object v2, p0, Landroid/widget/CalendarView$ScrollStateRunnable;->mView:Landroid/widget/AbsListView;

    #@50
    invoke-virtual {v2, v1, v4}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    #@53
    goto :goto_46
.end method
