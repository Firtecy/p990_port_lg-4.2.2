.class public Landroid/widget/NumberPicker;
.super Landroid/widget/LinearLayout;
.source "NumberPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;,
        Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;,
        Landroid/widget/NumberPicker$CustomEditText;,
        Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;,
        Landroid/widget/NumberPicker$SetSelectionCommand;,
        Landroid/widget/NumberPicker$PressedStateHelper;,
        Landroid/widget/NumberPicker$InputTextFilter;,
        Landroid/widget/NumberPicker$Formatter;,
        Landroid/widget/NumberPicker$OnScrollListener;,
        Landroid/widget/NumberPicker$OnValueChangeListener;,
        Landroid/widget/NumberPicker$TwoDigitFormatter;
    }
.end annotation


# static fields
.field private static final DEFAULT_LAYOUT_RESOURCE_ID:I = 0x1090099

.field private static final DEFAULT_LONG_PRESS_UPDATE_INTERVAL:J = 0x12cL

.field private static final DIGIT_CHARACTERS:[C = null

.field private static final SELECTOR_ADJUSTMENT_DURATION_MILLIS:I = 0x320

.field private static final SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT:I = 0x8

.field private static final SELECTOR_MIDDLE_ITEM_INDEX:I = 0x1

.field private static final SELECTOR_WHEEL_ITEM_COUNT:I = 0x3

.field private static final SIZE_UNSPECIFIED:I = -0x1

.field private static final SNAP_SCROLL_DURATION:I = 0x12c

.field private static final TOP_AND_BOTTOM_FADING_EDGE_STRENGTH:F = 0.9f

.field private static final UNSCALED_DEFAULT_SELECTION_DIVIDERS_DISTANCE:I = 0x30

.field private static final UNSCALED_DEFAULT_SELECTION_DIVIDER_HEIGHT:I = 0x2

.field private static final sTwoDigitFormatter:Landroid/widget/NumberPicker$TwoDigitFormatter;


# instance fields
.field private mAccessibilityNodeProvider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

.field private final mAdjustScroller:Landroid/widget/Scroller;

.field private mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

.field private mBottomSelectionDividerBottom:I

.field private mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

.field private final mComputeMaxWidth:Z

.field private mCurrentScrollOffset:I

.field private final mDecrementButton:Landroid/widget/ImageButton;

.field private mDecrementVirtualButtonPressed:Z

.field private mDisplayedValues:[Ljava/lang/String;

.field private final mFlingScroller:Landroid/widget/Scroller;

.field private mFormatter:Landroid/widget/NumberPicker$Formatter;

.field private final mHasSelectorWheel:Z

.field private final mIncrementButton:Landroid/widget/ImageButton;

.field private mIncrementVirtualButtonPressed:Z

.field private mIngonreMoveEvents:Z

.field private mInitialScrollOffset:I

.field private final mInputText:Landroid/widget/EditText;

.field private mLastDownEventTime:J

.field private mLastDownEventY:F

.field private mLastDownOrMoveEventY:F

.field private mLastHoveredChildVirtualViewId:I

.field private mLongPressUpdateInterval:J

.field private final mMaxHeight:I

.field private mMaxValue:I

.field private mMaxWidth:I

.field private mMaximumFlingVelocity:I

.field private final mMinHeight:I

.field private mMinValue:I

.field private final mMinWidth:I

.field private mMinimumFlingVelocity:I

.field private mOnScrollListener:Landroid/widget/NumberPicker$OnScrollListener;

.field private mOnValueChangeListener:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private final mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

.field private mPreviousScrollerY:I

.field private mScrollState:I

.field private final mSelectionDivider:Landroid/graphics/drawable/Drawable;

.field private final mSelectionDividerHeight:I

.field private final mSelectionDividersDistance:I

.field private mSelectorElementHeight:I

.field private final mSelectorIndexToStringCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectorIndices:[I

.field private mSelectorTextGapHeight:I

.field private final mSelectorWheelPaint:Landroid/graphics/Paint;

.field private mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

.field private mShowSoftInputOnTap:Z

.field private final mSolidColor:I

.field private final mTextSize:I

.field private mTopSelectionDividerTop:I

.field private mTouchSlop:I

.field private mValue:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

.field private mWrapSelectorWheel:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 191
    new-instance v0, Landroid/widget/NumberPicker$TwoDigitFormatter;

    #@2
    invoke-direct {v0}, Landroid/widget/NumberPicker$TwoDigitFormatter;-><init>()V

    #@5
    sput-object v0, Landroid/widget/NumberPicker;->sTwoDigitFormatter:Landroid/widget/NumberPicker$TwoDigitFormatter;

    #@7
    .line 1927
    const/16 v0, 0x28

    #@9
    new-array v0, v0, [C

    #@b
    fill-array-data v0, :array_12

    #@e
    sput-object v0, Landroid/widget/NumberPicker;->DIGIT_CHARACTERS:[C

    #@10
    return-void

    #@11
    nop

    #@12
    :array_12
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x60t 0x6t
        0x61t 0x6t
        0x62t 0x6t
        0x63t 0x6t
        0x64t 0x6t
        0x65t 0x6t
        0x66t 0x6t
        0x67t 0x6t
        0x68t 0x6t
        0x69t 0x6t
        0xf0t 0x6t
        0xf1t 0x6t
        0xf2t 0x6t
        0xf3t 0x6t
        0xf4t 0x6t
        0xf5t 0x6t
        0xf6t 0x6t
        0xf7t 0x6t
        0xf8t 0x6t
        0xf9t 0x6t
        0x66t 0x9t
        0x67t 0x9t
        0x68t 0x9t
        0x69t 0x9t
        0x6at 0x9t
        0x6bt 0x9t
        0x6ct 0x9t
        0x6dt 0x9t
        0x6et 0x9t
        0x6ft 0x9t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 539
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 540
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 549
    const v0, 0x10103de

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 550
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 22
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 560
    invoke-direct/range {p0 .. p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 293
    const-wide/16 v14, 0x12c

    #@5
    move-object/from16 v0, p0

    #@7
    iput-wide v14, v0, Landroid/widget/NumberPicker;->mLongPressUpdateInterval:J

    #@9
    .line 298
    new-instance v14, Landroid/util/SparseArray;

    #@b
    invoke-direct {v14}, Landroid/util/SparseArray;-><init>()V

    #@e
    move-object/from16 v0, p0

    #@10
    iput-object v14, v0, Landroid/widget/NumberPicker;->mSelectorIndexToStringCache:Landroid/util/SparseArray;

    #@12
    .line 303
    const/4 v14, 0x3

    #@13
    new-array v14, v14, [I

    #@15
    move-object/from16 v0, p0

    #@17
    iput-object v14, v0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@19
    .line 323
    const/high16 v14, -0x8000

    #@1b
    move-object/from16 v0, p0

    #@1d
    iput v14, v0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@1f
    .line 424
    const/4 v14, 0x0

    #@20
    move-object/from16 v0, p0

    #@22
    iput v14, v0, Landroid/widget/NumberPicker;->mScrollState:I

    #@24
    .line 563
    sget-object v14, Lcom/android/internal/R$styleable;->NumberPicker:[I

    #@26
    const/4 v15, 0x0

    #@27
    move-object/from16 v0, p1

    #@29
    move-object/from16 v1, p2

    #@2b
    move/from16 v2, p3

    #@2d
    invoke-virtual {v0, v1, v14, v2, v15}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@30
    move-result-object v3

    #@31
    .line 565
    .local v3, attributesArray:Landroid/content/res/TypedArray;
    const/4 v14, 0x1

    #@32
    const v15, 0x1090099

    #@35
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@38
    move-result v10

    #@39
    .line 568
    .local v10, layoutResId:I
    const v14, 0x1090099

    #@3c
    if-eq v10, v14, :cond_bd

    #@3e
    const/4 v14, 0x1

    #@3f
    :goto_3f
    move-object/from16 v0, p0

    #@41
    iput-boolean v14, v0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@43
    .line 570
    const/4 v14, 0x0

    #@44
    const/4 v15, 0x0

    #@45
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getColor(II)I

    #@48
    move-result v14

    #@49
    move-object/from16 v0, p0

    #@4b
    iput v14, v0, Landroid/widget/NumberPicker;->mSolidColor:I

    #@4d
    .line 572
    const/4 v14, 0x2

    #@4e
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@51
    move-result-object v14

    #@52
    move-object/from16 v0, p0

    #@54
    iput-object v14, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@56
    .line 574
    const/4 v14, 0x1

    #@57
    const/high16 v15, 0x4000

    #@59
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getResources()Landroid/content/res/Resources;

    #@5c
    move-result-object v16

    #@5d
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@60
    move-result-object v16

    #@61
    invoke-static/range {v14 .. v16}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@64
    move-result v14

    #@65
    float-to-int v8, v14

    #@66
    .line 577
    .local v8, defSelectionDividerHeight:I
    const/4 v14, 0x3

    #@67
    invoke-virtual {v3, v14, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@6a
    move-result v14

    #@6b
    move-object/from16 v0, p0

    #@6d
    iput v14, v0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@6f
    .line 580
    const/4 v14, 0x1

    #@70
    const/high16 v15, 0x4240

    #@72
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getResources()Landroid/content/res/Resources;

    #@75
    move-result-object v16

    #@76
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@79
    move-result-object v16

    #@7a
    invoke-static/range {v14 .. v16}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@7d
    move-result v14

    #@7e
    float-to-int v7, v14

    #@7f
    .line 583
    .local v7, defSelectionDividerDistance:I
    const/4 v14, 0x4

    #@80
    invoke-virtual {v3, v14, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@83
    move-result v14

    #@84
    move-object/from16 v0, p0

    #@86
    iput v14, v0, Landroid/widget/NumberPicker;->mSelectionDividersDistance:I

    #@88
    .line 586
    const/4 v14, 0x5

    #@89
    const/4 v15, -0x1

    #@8a
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@8d
    move-result v14

    #@8e
    move-object/from16 v0, p0

    #@90
    iput v14, v0, Landroid/widget/NumberPicker;->mMinHeight:I

    #@92
    .line 589
    const/4 v14, 0x6

    #@93
    const/4 v15, -0x1

    #@94
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@97
    move-result v14

    #@98
    move-object/from16 v0, p0

    #@9a
    iput v14, v0, Landroid/widget/NumberPicker;->mMaxHeight:I

    #@9c
    .line 591
    move-object/from16 v0, p0

    #@9e
    iget v14, v0, Landroid/widget/NumberPicker;->mMinHeight:I

    #@a0
    const/4 v15, -0x1

    #@a1
    if-eq v14, v15, :cond_bf

    #@a3
    move-object/from16 v0, p0

    #@a5
    iget v14, v0, Landroid/widget/NumberPicker;->mMaxHeight:I

    #@a7
    const/4 v15, -0x1

    #@a8
    if-eq v14, v15, :cond_bf

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget v14, v0, Landroid/widget/NumberPicker;->mMinHeight:I

    #@ae
    move-object/from16 v0, p0

    #@b0
    iget v15, v0, Landroid/widget/NumberPicker;->mMaxHeight:I

    #@b2
    if-le v14, v15, :cond_bf

    #@b4
    .line 593
    new-instance v14, Ljava/lang/IllegalArgumentException;

    #@b6
    const-string/jumbo v15, "minHeight > maxHeight"

    #@b9
    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@bc
    throw v14

    #@bd
    .line 568
    .end local v7           #defSelectionDividerDistance:I
    .end local v8           #defSelectionDividerHeight:I
    :cond_bd
    const/4 v14, 0x0

    #@be
    goto :goto_3f

    #@bf
    .line 596
    .restart local v7       #defSelectionDividerDistance:I
    .restart local v8       #defSelectionDividerHeight:I
    :cond_bf
    const/4 v14, 0x7

    #@c0
    const/4 v15, -0x1

    #@c1
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@c4
    move-result v14

    #@c5
    move-object/from16 v0, p0

    #@c7
    iput v14, v0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@c9
    .line 599
    const/16 v14, 0x8

    #@cb
    const/4 v15, -0x1

    #@cc
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@cf
    move-result v14

    #@d0
    move-object/from16 v0, p0

    #@d2
    iput v14, v0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@d4
    .line 601
    move-object/from16 v0, p0

    #@d6
    iget v14, v0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@d8
    const/4 v15, -0x1

    #@d9
    if-eq v14, v15, :cond_f5

    #@db
    move-object/from16 v0, p0

    #@dd
    iget v14, v0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@df
    const/4 v15, -0x1

    #@e0
    if-eq v14, v15, :cond_f5

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget v14, v0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@e6
    move-object/from16 v0, p0

    #@e8
    iget v15, v0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@ea
    if-le v14, v15, :cond_f5

    #@ec
    .line 603
    new-instance v14, Ljava/lang/IllegalArgumentException;

    #@ee
    const-string/jumbo v15, "minWidth > maxWidth"

    #@f1
    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f4
    throw v14

    #@f5
    .line 606
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget v14, v0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@f9
    const/4 v15, -0x1

    #@fa
    if-ne v14, v15, :cond_263

    #@fc
    const/4 v14, 0x1

    #@fd
    :goto_fd
    move-object/from16 v0, p0

    #@ff
    iput-boolean v14, v0, Landroid/widget/NumberPicker;->mComputeMaxWidth:Z

    #@101
    .line 608
    const/16 v14, 0x9

    #@103
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@106
    move-result-object v14

    #@107
    move-object/from16 v0, p0

    #@109
    iput-object v14, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@10b
    .line 611
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@10e
    .line 613
    new-instance v14, Landroid/widget/NumberPicker$PressedStateHelper;

    #@110
    move-object/from16 v0, p0

    #@112
    invoke-direct {v14, v0}, Landroid/widget/NumberPicker$PressedStateHelper;-><init>(Landroid/widget/NumberPicker;)V

    #@115
    move-object/from16 v0, p0

    #@117
    iput-object v14, v0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@119
    .line 620
    move-object/from16 v0, p0

    #@11b
    iget-boolean v14, v0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@11d
    if-nez v14, :cond_266

    #@11f
    const/4 v14, 0x1

    #@120
    :goto_120
    move-object/from16 v0, p0

    #@122
    invoke-virtual {v0, v14}, Landroid/widget/NumberPicker;->setWillNotDraw(Z)V

    #@125
    .line 622
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getContext()Landroid/content/Context;

    #@128
    move-result-object v14

    #@129
    const-string/jumbo v15, "layout_inflater"

    #@12c
    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12f
    move-result-object v9

    #@130
    check-cast v9, Landroid/view/LayoutInflater;

    #@132
    .line 624
    .local v9, inflater:Landroid/view/LayoutInflater;
    const/4 v14, 0x1

    #@133
    move-object/from16 v0, p0

    #@135
    invoke-virtual {v9, v10, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@138
    .line 626
    new-instance v11, Landroid/widget/NumberPicker$1;

    #@13a
    move-object/from16 v0, p0

    #@13c
    invoke-direct {v11, v0}, Landroid/widget/NumberPicker$1;-><init>(Landroid/widget/NumberPicker;)V

    #@13f
    .line 638
    .local v11, onClickListener:Landroid/view/View$OnClickListener;
    new-instance v12, Landroid/widget/NumberPicker$2;

    #@141
    move-object/from16 v0, p0

    #@143
    invoke-direct {v12, v0}, Landroid/widget/NumberPicker$2;-><init>(Landroid/widget/NumberPicker;)V

    #@146
    .line 652
    .local v12, onLongClickListener:Landroid/view/View$OnLongClickListener;
    move-object/from16 v0, p0

    #@148
    iget-boolean v14, v0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@14a
    if-nez v14, :cond_269

    #@14c
    .line 653
    const v14, 0x1020348

    #@14f
    move-object/from16 v0, p0

    #@151
    invoke-virtual {v0, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@154
    move-result-object v14

    #@155
    check-cast v14, Landroid/widget/ImageButton;

    #@157
    move-object/from16 v0, p0

    #@159
    iput-object v14, v0, Landroid/widget/NumberPicker;->mIncrementButton:Landroid/widget/ImageButton;

    #@15b
    .line 654
    move-object/from16 v0, p0

    #@15d
    iget-object v14, v0, Landroid/widget/NumberPicker;->mIncrementButton:Landroid/widget/ImageButton;

    #@15f
    invoke-virtual {v14, v11}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@162
    .line 655
    move-object/from16 v0, p0

    #@164
    iget-object v14, v0, Landroid/widget/NumberPicker;->mIncrementButton:Landroid/widget/ImageButton;

    #@166
    invoke-virtual {v14, v12}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@169
    .line 661
    :goto_169
    move-object/from16 v0, p0

    #@16b
    iget-boolean v14, v0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@16d
    if-nez v14, :cond_270

    #@16f
    .line 662
    const v14, 0x102034a

    #@172
    move-object/from16 v0, p0

    #@174
    invoke-virtual {v0, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@177
    move-result-object v14

    #@178
    check-cast v14, Landroid/widget/ImageButton;

    #@17a
    move-object/from16 v0, p0

    #@17c
    iput-object v14, v0, Landroid/widget/NumberPicker;->mDecrementButton:Landroid/widget/ImageButton;

    #@17e
    .line 663
    move-object/from16 v0, p0

    #@180
    iget-object v14, v0, Landroid/widget/NumberPicker;->mDecrementButton:Landroid/widget/ImageButton;

    #@182
    invoke-virtual {v14, v11}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@185
    .line 664
    move-object/from16 v0, p0

    #@187
    iget-object v14, v0, Landroid/widget/NumberPicker;->mDecrementButton:Landroid/widget/ImageButton;

    #@189
    invoke-virtual {v14, v12}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@18c
    .line 670
    :goto_18c
    const v14, 0x1020349

    #@18f
    move-object/from16 v0, p0

    #@191
    invoke-virtual {v0, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@194
    move-result-object v14

    #@195
    check-cast v14, Landroid/widget/EditText;

    #@197
    move-object/from16 v0, p0

    #@199
    iput-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@19b
    .line 671
    move-object/from16 v0, p0

    #@19d
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@19f
    new-instance v15, Landroid/widget/NumberPicker$3;

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    invoke-direct {v15, v0}, Landroid/widget/NumberPicker$3;-><init>(Landroid/widget/NumberPicker;)V

    #@1a6
    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    #@1a9
    .line 687
    move-object/from16 v0, p0

    #@1ab
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1ad
    const/4 v15, 0x1

    #@1ae
    new-array v15, v15, [Landroid/text/InputFilter;

    #@1b0
    const/16 v16, 0x0

    #@1b2
    new-instance v17, Landroid/widget/NumberPicker$InputTextFilter;

    #@1b4
    invoke-direct/range {v17 .. v18}, Landroid/widget/NumberPicker$InputTextFilter;-><init>(Landroid/widget/NumberPicker;)V

    #@1b7
    aput-object v17, v15, v16

    #@1b9
    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@1bc
    .line 691
    move-object/from16 v0, p0

    #@1be
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1c0
    const/4 v15, 0x2

    #@1c1
    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setRawInputType(I)V

    #@1c4
    .line 692
    move-object/from16 v0, p0

    #@1c6
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1c8
    const/4 v15, 0x6

    #@1c9
    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setImeOptions(I)V

    #@1cc
    .line 695
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@1cf
    move-result-object v6

    #@1d0
    .line 696
    .local v6, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@1d3
    move-result v14

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    iput v14, v0, Landroid/widget/NumberPicker;->mTouchSlop:I

    #@1d8
    .line 697
    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@1db
    move-result v14

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iput v14, v0, Landroid/widget/NumberPicker;->mMinimumFlingVelocity:I

    #@1e0
    .line 698
    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@1e3
    move-result v14

    #@1e4
    div-int/lit8 v14, v14, 0x8

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iput v14, v0, Landroid/widget/NumberPicker;->mMaximumFlingVelocity:I

    #@1ea
    .line 700
    move-object/from16 v0, p0

    #@1ec
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1ee
    invoke-virtual {v14}, Landroid/widget/EditText;->getTextSize()F

    #@1f1
    move-result v14

    #@1f2
    float-to-int v14, v14

    #@1f3
    move-object/from16 v0, p0

    #@1f5
    iput v14, v0, Landroid/widget/NumberPicker;->mTextSize:I

    #@1f7
    .line 703
    new-instance v13, Landroid/graphics/Paint;

    #@1f9
    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    #@1fc
    .line 704
    .local v13, paint:Landroid/graphics/Paint;
    const/4 v14, 0x1

    #@1fd
    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@200
    .line 705
    sget-object v14, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@202
    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    #@205
    .line 706
    move-object/from16 v0, p0

    #@207
    iget v14, v0, Landroid/widget/NumberPicker;->mTextSize:I

    #@209
    int-to-float v14, v14

    #@20a
    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setTextSize(F)V

    #@20d
    .line 707
    move-object/from16 v0, p0

    #@20f
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@211
    invoke-virtual {v14}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    #@214
    move-result-object v14

    #@215
    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@218
    .line 708
    move-object/from16 v0, p0

    #@21a
    iget-object v14, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@21c
    invoke-virtual {v14}, Landroid/widget/EditText;->getTextColors()Landroid/content/res/ColorStateList;

    #@21f
    move-result-object v5

    #@220
    .line 709
    .local v5, colors:Landroid/content/res/ColorStateList;
    sget-object v14, Landroid/widget/NumberPicker;->ENABLED_STATE_SET:[I

    #@222
    const/4 v15, -0x1

    #@223
    invoke-virtual {v5, v14, v15}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@226
    move-result v4

    #@227
    .line 710
    .local v4, color:I
    invoke-virtual {v13, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@22a
    .line 711
    move-object/from16 v0, p0

    #@22c
    iput-object v13, v0, Landroid/widget/NumberPicker;->mSelectorWheelPaint:Landroid/graphics/Paint;

    #@22e
    .line 714
    new-instance v14, Landroid/widget/Scroller;

    #@230
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getContext()Landroid/content/Context;

    #@233
    move-result-object v15

    #@234
    const/16 v16, 0x0

    #@236
    const/16 v17, 0x1

    #@238
    invoke-direct/range {v14 .. v17}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    #@23b
    move-object/from16 v0, p0

    #@23d
    iput-object v14, v0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@23f
    .line 715
    new-instance v14, Landroid/widget/Scroller;

    #@241
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getContext()Landroid/content/Context;

    #@244
    move-result-object v15

    #@245
    new-instance v16, Landroid/view/animation/DecelerateInterpolator;

    #@247
    const/high16 v17, 0x4020

    #@249
    invoke-direct/range {v16 .. v17}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@24c
    invoke-direct/range {v14 .. v16}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    #@24f
    move-object/from16 v0, p0

    #@251
    iput-object v14, v0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@253
    .line 717
    invoke-direct/range {p0 .. p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@256
    .line 720
    invoke-virtual/range {p0 .. p0}, Landroid/widget/NumberPicker;->getImportantForAccessibility()I

    #@259
    move-result v14

    #@25a
    if-nez v14, :cond_262

    #@25c
    .line 721
    const/4 v14, 0x1

    #@25d
    move-object/from16 v0, p0

    #@25f
    invoke-virtual {v0, v14}, Landroid/widget/NumberPicker;->setImportantForAccessibility(I)V

    #@262
    .line 723
    :cond_262
    return-void

    #@263
    .line 606
    .end local v4           #color:I
    .end local v5           #colors:Landroid/content/res/ColorStateList;
    .end local v6           #configuration:Landroid/view/ViewConfiguration;
    .end local v9           #inflater:Landroid/view/LayoutInflater;
    .end local v11           #onClickListener:Landroid/view/View$OnClickListener;
    .end local v12           #onLongClickListener:Landroid/view/View$OnLongClickListener;
    .end local v13           #paint:Landroid/graphics/Paint;
    :cond_263
    const/4 v14, 0x0

    #@264
    goto/16 :goto_fd

    #@266
    .line 620
    :cond_266
    const/4 v14, 0x0

    #@267
    goto/16 :goto_120

    #@269
    .line 657
    .restart local v9       #inflater:Landroid/view/LayoutInflater;
    .restart local v11       #onClickListener:Landroid/view/View$OnClickListener;
    .restart local v12       #onLongClickListener:Landroid/view/View$OnLongClickListener;
    :cond_269
    const/4 v14, 0x0

    #@26a
    move-object/from16 v0, p0

    #@26c
    iput-object v14, v0, Landroid/widget/NumberPicker;->mIncrementButton:Landroid/widget/ImageButton;

    #@26e
    goto/16 :goto_169

    #@270
    .line 666
    :cond_270
    const/4 v14, 0x0

    #@271
    move-object/from16 v0, p0

    #@273
    iput-object v14, v0, Landroid/widget/NumberPicker;->mDecrementButton:Landroid/widget/ImageButton;

    #@275
    goto/16 :goto_18c
.end method

.method static synthetic access$000(Landroid/widget/NumberPicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 90
    invoke-direct {p0}, Landroid/widget/NumberPicker;->hideSoftInput()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/NumberPicker;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->getSelectedPos(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@2
    return v0
.end method

.method static synthetic access$1200(Landroid/widget/NumberPicker;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/NumberPicker;->postSetSelectionCommand(II)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/widget/NumberPicker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mIncrementVirtualButtonPressed:Z

    #@2
    return v0
.end method

.method static synthetic access$1302(Landroid/widget/NumberPicker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    iput-boolean p1, p0, Landroid/widget/NumberPicker;->mIncrementVirtualButtonPressed:Z

    #@2
    return p1
.end method

.method static synthetic access$1380(Landroid/widget/NumberPicker;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mIncrementVirtualButtonPressed:Z

    #@2
    xor-int/2addr v0, p1

    #@3
    int-to-byte v0, v0

    #@4
    iput-boolean v0, p0, Landroid/widget/NumberPicker;->mIncrementVirtualButtonPressed:Z

    #@6
    return v0
.end method

.method static synthetic access$1400(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@2
    return v0
.end method

.method static synthetic access$1500(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$1600(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Landroid/widget/NumberPicker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mDecrementVirtualButtonPressed:Z

    #@2
    return v0
.end method

.method static synthetic access$1702(Landroid/widget/NumberPicker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    iput-boolean p1, p0, Landroid/widget/NumberPicker;->mDecrementVirtualButtonPressed:Z

    #@2
    return p1
.end method

.method static synthetic access$1780(Landroid/widget/NumberPicker;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mDecrementVirtualButtonPressed:Z

    #@2
    xor-int/2addr v0, p1

    #@3
    int-to-byte v0, v0

    #@4
    iput-boolean v0, p0, Landroid/widget/NumberPicker;->mDecrementVirtualButtonPressed:Z

    #@6
    return v0
.end method

.method static synthetic access$1800(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$1900(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/NumberPicker;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->changeValueByOne(Z)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$2100(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$2200(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$2300(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$2400(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$2500(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$2600(Landroid/widget/NumberPicker;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-wide v0, p0, Landroid/widget/NumberPicker;->mLongPressUpdateInterval:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2700(Landroid/widget/NumberPicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 90
    invoke-direct {p0}, Landroid/widget/NumberPicker;->showSoftInput()V

    #@3
    return-void
.end method

.method static synthetic access$2802(Landroid/widget/NumberPicker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    iput-boolean p1, p0, Landroid/widget/NumberPicker;->mIngonreMoveEvents:Z

    #@2
    return p1
.end method

.method static synthetic access$2900(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/NumberPicker;ZJ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/NumberPicker;->postChangeCurrentByOneFromLongPress(ZJ)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$3100(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$3200(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$3300(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    return v0
.end method

.method static synthetic access$3400(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$3500(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$3600(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mTop:I

    #@2
    return v0
.end method

.method static synthetic access$3700(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$3800(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$3900(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/widget/NumberPicker;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->validateInputTextView(Landroid/view/View;)V

    #@3
    return-void
.end method

.method static synthetic access$4000(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$4100(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    return v0
.end method

.method static synthetic access$4200(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@2
    return v0
.end method

.method static synthetic access$4300(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$4400(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$4500(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$4600(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    return v0
.end method

.method static synthetic access$4700(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$4800(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$4900(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mTop:I

    #@2
    return v0
.end method

.method static synthetic access$5000(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$5100(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$5200(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$5300(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$5400(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$5500(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$5600(Landroid/widget/NumberPicker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$5700(Landroid/widget/NumberPicker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$5800(Landroid/widget/NumberPicker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$5900(Landroid/widget/NumberPicker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$6000(Landroid/widget/NumberPicker;Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-virtual {p0, p1}, Landroid/widget/NumberPicker;->isVisibleToUser(Landroid/graphics/Rect;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6100(Landroid/widget/NumberPicker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$6200(Landroid/widget/NumberPicker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->isVisibleToUser()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6300(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mValue:I

    #@2
    return v0
.end method

.method static synthetic access$6400(Landroid/widget/NumberPicker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@2
    return v0
.end method

.method static synthetic access$6500(Landroid/widget/NumberPicker;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->getWrappedSelectorIndex(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6600(Landroid/widget/NumberPicker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@2
    return v0
.end method

.method static synthetic access$6700(Landroid/widget/NumberPicker;I)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->formatNumber(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800()[C
    .registers 1

    #@0
    .prologue
    .line 90
    sget-object v0, Landroid/widget/NumberPicker;->DIGIT_CHARACTERS:[C

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private changeValueByOne(Z)V
    .registers 8
    .parameter "increment"

    #@0
    .prologue
    const/16 v5, 0x12c

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 1601
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@6
    if-eqz v0, :cond_37

    #@8
    .line 1602
    iget-object v0, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@a
    const/4 v2, 0x4

    #@b
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@e
    .line 1603
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@10
    invoke-direct {p0, v0}, Landroid/widget/NumberPicker;->moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1b

    #@16
    .line 1604
    iget-object v0, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@18
    invoke-direct {p0, v0}, Landroid/widget/NumberPicker;->moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z

    #@1b
    .line 1606
    :cond_1b
    iput v1, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@1d
    .line 1607
    if-eqz p1, :cond_2d

    #@1f
    .line 1608
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@21
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@23
    neg-int v4, v2

    #@24
    move v2, v1

    #@25
    move v3, v1

    #@26
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@29
    .line 1612
    :goto_29
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@2c
    .line 1620
    :goto_2c
    return-void

    #@2d
    .line 1610
    :cond_2d
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@2f
    iget v4, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@31
    move v2, v1

    #@32
    move v3, v1

    #@33
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@36
    goto :goto_29

    #@37
    .line 1614
    :cond_37
    if-eqz p1, :cond_41

    #@39
    .line 1615
    iget v0, p0, Landroid/widget/NumberPicker;->mValue:I

    #@3b
    add-int/lit8 v0, v0, 0x1

    #@3d
    invoke-direct {p0, v0, v2}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@40
    goto :goto_2c

    #@41
    .line 1617
    :cond_41
    iget v0, p0, Landroid/widget/NumberPicker;->mValue:I

    #@43
    add-int/lit8 v0, v0, -0x1

    #@45
    invoke-direct {p0, v0, v2}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@48
    goto :goto_2c
.end method

.method private decrementSelectorIndices([I)V
    .registers 5
    .parameter "selectorIndices"

    #@0
    .prologue
    .line 1730
    array-length v2, p1

    #@1
    add-int/lit8 v0, v2, -0x1

    #@3
    .local v0, i:I
    :goto_3
    if-lez v0, :cond_e

    #@5
    .line 1731
    add-int/lit8 v2, v0, -0x1

    #@7
    aget v2, p1, v2

    #@9
    aput v2, p1, v0

    #@b
    .line 1730
    add-int/lit8 v0, v0, -0x1

    #@d
    goto :goto_3

    #@e
    .line 1733
    :cond_e
    const/4 v2, 0x1

    #@f
    aget v2, p1, v2

    #@11
    add-int/lit8 v1, v2, -0x1

    #@13
    .line 1734
    .local v1, nextScrollSelectorIndex:I
    iget-boolean v2, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@15
    if-eqz v2, :cond_1d

    #@17
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@19
    if-ge v1, v2, :cond_1d

    #@1b
    .line 1735
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1d
    .line 1737
    :cond_1d
    const/4 v2, 0x0

    #@1e
    aput v1, p1, v2

    #@20
    .line 1738
    invoke-direct {p0, v1}, Landroid/widget/NumberPicker;->ensureCachedScrollSelectorValue(I)V

    #@23
    .line 1739
    return-void
.end method

.method private ensureCachedScrollSelectorValue(I)V
    .registers 6
    .parameter "selectorIndex"

    #@0
    .prologue
    .line 1746
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSelectorIndexToStringCache:Landroid/util/SparseArray;

    #@2
    .line 1747
    .local v0, cache:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Ljava/lang/String;

    #@8
    .line 1748
    .local v2, scrollSelectorValue:Ljava/lang/String;
    if-eqz v2, :cond_b

    #@a
    .line 1762
    :goto_a
    return-void

    #@b
    .line 1751
    :cond_b
    iget v3, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@d
    if-lt p1, v3, :cond_13

    #@f
    iget v3, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@11
    if-le p1, v3, :cond_19

    #@13
    .line 1752
    :cond_13
    const-string v2, ""

    #@15
    .line 1761
    :goto_15
    invoke-virtual {v0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@18
    goto :goto_a

    #@19
    .line 1754
    :cond_19
    iget-object v3, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@1b
    if-eqz v3, :cond_26

    #@1d
    .line 1755
    iget v3, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@1f
    sub-int v1, p1, v3

    #@21
    .line 1756
    .local v1, displayedValueIndex:I
    iget-object v3, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@23
    aget-object v2, v3, v1

    #@25
    .line 1757
    goto :goto_15

    #@26
    .line 1758
    .end local v1           #displayedValueIndex:I
    :cond_26
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->formatNumber(I)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    goto :goto_15
.end method

.method private ensureScrollWheelAdjusted()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2013
    iget v0, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@3
    iget v2, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@5
    sub-int v4, v0, v2

    #@7
    .line 2014
    .local v4, deltaY:I
    if-eqz v4, :cond_2c

    #@9
    .line 2015
    iput v1, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@b
    .line 2016
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@e
    move-result v0

    #@f
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@11
    div-int/lit8 v2, v2, 0x2

    #@13
    if-le v0, v2, :cond_1b

    #@15
    .line 2017
    if-lez v4, :cond_29

    #@17
    iget v0, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@19
    neg-int v0, v0

    #@1a
    :goto_1a
    add-int/2addr v4, v0

    #@1b
    .line 2019
    :cond_1b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@1d
    const/16 v5, 0x320

    #@1f
    move v2, v1

    #@20
    move v3, v1

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@24
    .line 2020
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@27
    .line 2021
    const/4 v1, 0x1

    #@28
    .line 2026
    :goto_28
    return v1

    #@29
    .line 2017
    :cond_29
    iget v0, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@2b
    goto :goto_1a

    #@2c
    .line 2024
    :cond_2c
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@2f
    goto :goto_28
.end method

.method private fling(I)V
    .registers 11
    .parameter "velocityY"

    #@0
    .prologue
    const v8, 0x7fffffff

    #@3
    const/4 v1, 0x0

    #@4
    .line 1686
    iput v1, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@6
    .line 1688
    if-lez p1, :cond_17

    #@8
    .line 1689
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@a
    move v2, v1

    #@b
    move v3, v1

    #@c
    move v4, p1

    #@d
    move v5, v1

    #@e
    move v6, v1

    #@f
    move v7, v1

    #@10
    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    #@13
    .line 1694
    :goto_13
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@16
    .line 1695
    return-void

    #@17
    .line 1691
    :cond_17
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@19
    move v2, v8

    #@1a
    move v3, v1

    #@1b
    move v4, p1

    #@1c
    move v5, v1

    #@1d
    move v6, v1

    #@1e
    move v7, v1

    #@1f
    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    #@22
    goto :goto_13
.end method

.method private formatNumber(I)Ljava/lang/String;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 1765
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFormatter:Landroid/widget/NumberPicker$Formatter;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFormatter:Landroid/widget/NumberPicker$Formatter;

    #@6
    invoke-interface {v0, p1}, Landroid/widget/NumberPicker$Formatter;->format(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-static {p1}, Landroid/widget/NumberPicker;->formatNumberWithLocale(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method private static formatNumberWithLocale(I)Ljava/lang/String;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 2596
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    const-string v1, "%d"

    #@6
    const/4 v2, 0x1

    #@7
    new-array v2, v2, [Ljava/lang/Object;

    #@9
    const/4 v3, 0x0

    #@a
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v4

    #@e
    aput-object v4, v2, v3

    #@10
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method private getSelectedPos(Ljava/lang/String;)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1880
    iget-object v1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@2
    if-nez v1, :cond_9

    #@4
    .line 1882
    :try_start_4
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_2d

    #@7
    move-result v1

    #@8
    .line 1906
    :goto_8
    return v1

    #@9
    .line 1887
    :cond_9
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    iget-object v1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@c
    array-length v1, v1

    #@d
    if-ge v0, v1, :cond_28

    #@f
    .line 1889
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@12
    move-result-object p1

    #@13
    .line 1890
    iget-object v1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@15
    aget-object v1, v1, v0

    #@17
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_25

    #@21
    .line 1891
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@23
    add-int/2addr v1, v0

    #@24
    goto :goto_8

    #@25
    .line 1887
    :cond_25
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_a

    #@28
    .line 1900
    :cond_28
    :try_start_28
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2b
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_2b} :catch_31

    #@2b
    move-result v1

    #@2c
    goto :goto_8

    #@2d
    .line 1883
    .end local v0           #i:I
    :catch_2d
    move-exception v1

    #@2e
    .line 1906
    :goto_2e
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@30
    goto :goto_8

    #@31
    .line 1901
    .restart local v0       #i:I
    :catch_31
    move-exception v1

    #@32
    goto :goto_2e
.end method

.method public static final getTwoDigitFormatter()Landroid/widget/NumberPicker$Formatter;
    .registers 1

    #@0
    .prologue
    .line 197
    sget-object v0, Landroid/widget/NumberPicker;->sTwoDigitFormatter:Landroid/widget/NumberPicker$TwoDigitFormatter;

    #@2
    return-object v0
.end method

.method private getWrappedSelectorIndex(I)I
    .registers 6
    .parameter "selectorIndex"

    #@0
    .prologue
    .line 1701
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@2
    if-le p1, v0, :cond_14

    #@4
    .line 1702
    iget v0, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@6
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@8
    sub-int v1, p1, v1

    #@a
    iget v2, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@c
    iget v3, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@e
    sub-int/2addr v2, v3

    #@f
    rem-int/2addr v1, v2

    #@10
    add-int/2addr v0, v1

    #@11
    add-int/lit8 p1, v0, -0x1

    #@13
    .line 1706
    .end local p1
    :cond_13
    :goto_13
    return p1

    #@14
    .line 1703
    .restart local p1
    :cond_14
    iget v0, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@16
    if-ge p1, v0, :cond_13

    #@18
    .line 1704
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1a
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@1c
    sub-int/2addr v1, p1

    #@1d
    iget v2, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1f
    iget v3, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@21
    sub-int/2addr v2, v3

    #@22
    rem-int/2addr v1, v2

    #@23
    sub-int/2addr v0, v1

    #@24
    add-int/lit8 p1, v0, 0x1

    #@26
    goto :goto_13
.end method

.method private hideSoftInput()V
    .registers 4

    #@0
    .prologue
    .line 1189
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 1190
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_20

    #@6
    iget-object v1, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@8
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_20

    #@e
    .line 1191
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getWindowToken()Landroid/os/IBinder;

    #@11
    move-result-object v1

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@16
    .line 1192
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@18
    if-eqz v1, :cond_20

    #@1a
    .line 1193
    iget-object v1, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1c
    const/4 v2, 0x4

    #@1d
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@20
    .line 1196
    :cond_20
    return-void
.end method

.method private incrementSelectorIndices([I)V
    .registers 5
    .parameter "selectorIndices"

    #@0
    .prologue
    .line 1714
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v2, p1

    #@2
    add-int/lit8 v2, v2, -0x1

    #@4
    if-ge v0, v2, :cond_f

    #@6
    .line 1715
    add-int/lit8 v2, v0, 0x1

    #@8
    aget v2, p1, v2

    #@a
    aput v2, p1, v0

    #@c
    .line 1714
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 1717
    :cond_f
    array-length v2, p1

    #@10
    add-int/lit8 v2, v2, -0x2

    #@12
    aget v2, p1, v2

    #@14
    add-int/lit8 v1, v2, 0x1

    #@16
    .line 1718
    .local v1, nextScrollSelectorIndex:I
    iget-boolean v2, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@18
    if-eqz v2, :cond_20

    #@1a
    iget v2, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1c
    if-le v1, v2, :cond_20

    #@1e
    .line 1719
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@20
    .line 1721
    :cond_20
    array-length v2, p1

    #@21
    add-int/lit8 v2, v2, -0x1

    #@23
    aput v1, p1, v2

    #@25
    .line 1722
    invoke-direct {p0, v1}, Landroid/widget/NumberPicker;->ensureCachedScrollSelectorValue(I)V

    #@28
    .line 1723
    return-void
.end method

.method private initializeFadingEdges()V
    .registers 3

    #@0
    .prologue
    .line 1649
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setVerticalFadingEdgeEnabled(Z)V

    #@4
    .line 1650
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@6
    iget v1, p0, Landroid/view/View;->mTop:I

    #@8
    sub-int/2addr v0, v1

    #@9
    iget v1, p0, Landroid/widget/NumberPicker;->mTextSize:I

    #@b
    sub-int/2addr v0, v1

    #@c
    div-int/lit8 v0, v0, 0x2

    #@e
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setFadingEdgeLength(I)V

    #@11
    .line 1651
    return-void
.end method

.method private initializeSelectorWheel()V
    .registers 9

    #@0
    .prologue
    .line 1623
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@3
    .line 1624
    iget-object v2, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@5
    .line 1625
    .local v2, selectorIndices:[I
    array-length v6, v2

    #@6
    iget v7, p0, Landroid/widget/NumberPicker;->mTextSize:I

    #@8
    mul-int v5, v6, v7

    #@a
    .line 1626
    .local v5, totalTextHeight:I
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@c
    iget v7, p0, Landroid/view/View;->mTop:I

    #@e
    sub-int/2addr v6, v7

    #@f
    sub-int/2addr v6, v5

    #@10
    int-to-float v4, v6

    #@11
    .line 1627
    .local v4, totalTextGapHeight:F
    array-length v6, v2

    #@12
    int-to-float v3, v6

    #@13
    .line 1628
    .local v3, textGapCount:F
    div-float v6, v4, v3

    #@15
    const/high16 v7, 0x3f00

    #@17
    add-float/2addr v6, v7

    #@18
    float-to-int v6, v6

    #@19
    iput v6, p0, Landroid/widget/NumberPicker;->mSelectorTextGapHeight:I

    #@1b
    .line 1629
    iget v6, p0, Landroid/widget/NumberPicker;->mTextSize:I

    #@1d
    iget v7, p0, Landroid/widget/NumberPicker;->mSelectorTextGapHeight:I

    #@1f
    add-int/2addr v6, v7

    #@20
    iput v6, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@22
    .line 1632
    iget-object v6, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@24
    invoke-virtual {v6}, Landroid/widget/EditText;->getBaseline()I

    #@27
    move-result v6

    #@28
    iget-object v7, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@2a
    invoke-virtual {v7}, Landroid/widget/EditText;->getTop()I

    #@2d
    move-result v7

    #@2e
    add-int v0, v6, v7

    #@30
    .line 1633
    .local v0, editTextTextPosition:I
    iget v6, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@32
    mul-int/lit8 v6, v6, 0x1

    #@34
    sub-int v6, v0, v6

    #@36
    iput v6, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@38
    .line 1635
    iget v6, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@3a
    iput v6, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@3c
    .line 1639
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3f
    move-result-object v1

    #@40
    .line 1640
    .local v1, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_4b

    #@42
    iget-object v6, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@44
    invoke-virtual {v1, v6}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@47
    move-result v6

    #@48
    if-eqz v6, :cond_4b

    #@4a
    .line 1646
    :goto_4a
    return-void

    #@4b
    .line 1643
    :cond_4b
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@4e
    goto :goto_4a
.end method

.method private initializeSelectorWheelIndices()V
    .registers 6

    #@0
    .prologue
    .line 1553
    iget-object v4, p0, Landroid/widget/NumberPicker;->mSelectorIndexToStringCache:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    #@5
    .line 1554
    iget-object v3, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@7
    .line 1555
    .local v3, selectorIndices:[I
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getValue()I

    #@a
    move-result v0

    #@b
    .line 1556
    .local v0, current:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    iget-object v4, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@e
    array-length v4, v4

    #@f
    if-ge v1, v4, :cond_27

    #@11
    .line 1557
    add-int/lit8 v4, v1, -0x1

    #@13
    add-int v2, v0, v4

    #@15
    .line 1558
    .local v2, selectorIndex:I
    iget-boolean v4, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@17
    if-eqz v4, :cond_1d

    #@19
    .line 1559
    invoke-direct {p0, v2}, Landroid/widget/NumberPicker;->getWrappedSelectorIndex(I)I

    #@1c
    move-result v2

    #@1d
    .line 1561
    :cond_1d
    aput v2, v3, v1

    #@1f
    .line 1562
    aget v4, v3, v1

    #@21
    invoke-direct {p0, v4}, Landroid/widget/NumberPicker;->ensureCachedScrollSelectorValue(I)V

    #@24
    .line 1556
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_c

    #@27
    .line 1564
    .end local v2           #selectorIndex:I
    :cond_27
    return-void
.end method

.method private makeMeasureSpec(II)I
    .registers 8
    .parameter "measureSpec"
    .parameter "maxSize"

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    .line 1511
    const/4 v2, -0x1

    #@3
    if-ne p2, v2, :cond_6

    #@5
    .line 1522
    .end local p1
    :goto_5
    :sswitch_5
    return p1

    #@6
    .line 1514
    .restart local p1
    :cond_6
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v1

    #@a
    .line 1515
    .local v1, size:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@d
    move-result v0

    #@e
    .line 1516
    .local v0, mode:I
    sparse-switch v0, :sswitch_data_38

    #@11
    .line 1524
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Unknown measure mode: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 1520
    :sswitch_2a
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    #@2d
    move-result v2

    #@2e
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@31
    move-result p1

    #@32
    goto :goto_5

    #@33
    .line 1522
    :sswitch_33
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@36
    move-result p1

    #@37
    goto :goto_5

    #@38
    .line 1516
    :sswitch_data_38
    .sparse-switch
        -0x80000000 -> :sswitch_2a
        0x0 -> :sswitch_33
        0x40000000 -> :sswitch_5
    .end sparse-switch
.end method

.method private moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z
    .registers 9
    .parameter "scroller"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 781
    invoke-virtual {p1, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@5
    .line 782
    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalY()I

    #@8
    move-result v5

    #@9
    invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrY()I

    #@c
    move-result v6

    #@d
    sub-int v0, v5, v6

    #@f
    .line 783
    .local v0, amountToScroll:I
    iget v5, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@11
    add-int/2addr v5, v0

    #@12
    iget v6, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@14
    rem-int v1, v5, v6

    #@16
    .line 784
    .local v1, futureScrollOffset:I
    iget v5, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@18
    sub-int v2, v5, v1

    #@1a
    .line 785
    .local v2, overshootAdjustment:I
    if-eqz v2, :cond_34

    #@1c
    .line 786
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    #@1f
    move-result v5

    #@20
    iget v6, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@22
    div-int/lit8 v6, v6, 0x2

    #@24
    if-le v5, v6, :cond_2b

    #@26
    .line 787
    if-lez v2, :cond_30

    #@28
    .line 788
    iget v5, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@2a
    sub-int/2addr v2, v5

    #@2b
    .line 793
    :cond_2b
    :goto_2b
    add-int/2addr v0, v2

    #@2c
    .line 794
    invoke-virtual {p0, v4, v0}, Landroid/widget/NumberPicker;->scrollBy(II)V

    #@2f
    .line 797
    :goto_2f
    return v3

    #@30
    .line 790
    :cond_30
    iget v5, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@32
    add-int/2addr v2, v5

    #@33
    goto :goto_2b

    #@34
    :cond_34
    move v3, v4

    #@35
    .line 797
    goto :goto_2f
.end method

.method private notifyChange(II)V
    .registers 5
    .parameter "previous"
    .parameter "current"

    #@0
    .prologue
    .line 1809
    iget-object v0, p0, Landroid/widget/NumberPicker;->mOnValueChangeListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1810
    iget-object v0, p0, Landroid/widget/NumberPicker;->mOnValueChangeListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    #@6
    iget v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@8
    invoke-interface {v0, p0, p1, v1}, Landroid/widget/NumberPicker$OnValueChangeListener;->onValueChange(Landroid/widget/NumberPicker;II)V

    #@b
    .line 1812
    :cond_b
    return-void
.end method

.method private onScrollStateChange(I)V
    .registers 3
    .parameter "scrollState"

    #@0
    .prologue
    .line 1673
    iget v0, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 1680
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1676
    :cond_5
    iput p1, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@7
    .line 1677
    iget-object v0, p0, Landroid/widget/NumberPicker;->mOnScrollListener:Landroid/widget/NumberPicker$OnScrollListener;

    #@9
    if-eqz v0, :cond_4

    #@b
    .line 1678
    iget-object v0, p0, Landroid/widget/NumberPicker;->mOnScrollListener:Landroid/widget/NumberPicker$OnScrollListener;

    #@d
    invoke-interface {v0, p0, p1}, Landroid/widget/NumberPicker$OnScrollListener;->onScrollStateChange(Landroid/widget/NumberPicker;I)V

    #@10
    goto :goto_4
.end method

.method private onScrollerFinished(Landroid/widget/Scroller;)V
    .registers 4
    .parameter "scroller"

    #@0
    .prologue
    .line 1657
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@2
    if-ne p1, v0, :cond_12

    #@4
    .line 1658
    invoke-direct {p0}, Landroid/widget/NumberPicker;->ensureScrollWheelAdjusted()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_d

    #@a
    .line 1659
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@d
    .line 1661
    :cond_d
    const/4 v0, 0x0

    #@e
    invoke-direct {p0, v0}, Landroid/widget/NumberPicker;->onScrollStateChange(I)V

    #@11
    .line 1667
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1663
    :cond_12
    iget v0, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@14
    const/4 v1, 0x1

    #@15
    if-eq v0, v1, :cond_11

    #@17
    .line 1664
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@1a
    goto :goto_11
.end method

.method private postBeginSoftInputOnLongPressCommand()V
    .registers 4

    #@0
    .prologue
    .line 1843
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 1844
    new-instance v0, Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;-><init>(Landroid/widget/NumberPicker;)V

    #@9
    iput-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@b
    .line 1848
    :goto_b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@d
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@10
    move-result v1

    #@11
    int-to-long v1, v1

    #@12
    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/NumberPicker;->postDelayed(Ljava/lang/Runnable;J)Z

    #@15
    .line 1849
    return-void

    #@16
    .line 1846
    :cond_16
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@18
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1b
    goto :goto_b
.end method

.method private postChangeCurrentByOneFromLongPress(ZJ)V
    .registers 5
    .parameter "increment"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 1820
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 1821
    new-instance v0, Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;-><init>(Landroid/widget/NumberPicker;)V

    #@9
    iput-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@b
    .line 1825
    :goto_b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@d
    invoke-static {v0, p1}, Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;->access$500(Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;Z)V

    #@10
    .line 1826
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@12
    invoke-virtual {p0, v0, p2, p3}, Landroid/widget/NumberPicker;->postDelayed(Ljava/lang/Runnable;J)Z

    #@15
    .line 1827
    return-void

    #@16
    .line 1823
    :cond_16
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@18
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1b
    goto :goto_b
.end method

.method private postSetSelectionCommand(II)V
    .registers 4
    .parameter "selectionStart"
    .parameter "selectionEnd"

    #@0
    .prologue
    .line 1914
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@2
    if-nez v0, :cond_1b

    #@4
    .line 1915
    new-instance v0, Landroid/widget/NumberPicker$SetSelectionCommand;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/NumberPicker$SetSelectionCommand;-><init>(Landroid/widget/NumberPicker;)V

    #@9
    iput-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@b
    .line 1919
    :goto_b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@d
    invoke-static {v0, p1}, Landroid/widget/NumberPicker$SetSelectionCommand;->access$602(Landroid/widget/NumberPicker$SetSelectionCommand;I)I

    #@10
    .line 1920
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@12
    invoke-static {v0, p2}, Landroid/widget/NumberPicker$SetSelectionCommand;->access$702(Landroid/widget/NumberPicker$SetSelectionCommand;I)I

    #@15
    .line 1921
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@17
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->post(Ljava/lang/Runnable;)Z

    #@1a
    .line 1922
    return-void

    #@1b
    .line 1917
    :cond_1b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@1d
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@20
    goto :goto_b
.end method

.method private removeAllCallbacks()V
    .registers 2

    #@0
    .prologue
    .line 1864
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1865
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 1867
    :cond_9
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 1868
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSetSelectionCommand:Landroid/widget/NumberPicker$SetSelectionCommand;

    #@f
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@12
    .line 1870
    :cond_12
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 1871
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@18
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1b
    .line 1873
    :cond_1b
    iget-object v0, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@1d
    invoke-virtual {v0}, Landroid/widget/NumberPicker$PressedStateHelper;->cancel()V

    #@20
    .line 1874
    return-void
.end method

.method private removeBeginSoftInputCommand()V
    .registers 2

    #@0
    .prologue
    .line 1855
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1856
    iget-object v0, p0, Landroid/widget/NumberPicker;->mBeginSoftInputOnLongPressCommand:Landroid/widget/NumberPicker$BeginSoftInputOnLongPressCommand;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 1858
    :cond_9
    return-void
.end method

.method private removeChangeCurrentByOneFromLongPress()V
    .registers 2

    #@0
    .prologue
    .line 1833
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1834
    iget-object v0, p0, Landroid/widget/NumberPicker;->mChangeCurrentByOneFromLongPressCommand:Landroid/widget/NumberPicker$ChangeCurrentByOneFromLongPressCommand;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 1836
    :cond_9
    return-void
.end method

.method private resolveSizeAndStateRespectingMinSize(III)I
    .registers 6
    .parameter "minSize"
    .parameter "measuredSize"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 1540
    const/4 v1, -0x1

    #@1
    if-eq p1, v1, :cond_c

    #@3
    .line 1541
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    #@6
    move-result v0

    #@7
    .line 1542
    .local v0, desiredWidth:I
    const/4 v1, 0x0

    #@8
    invoke-static {v0, p3, v1}, Landroid/widget/NumberPicker;->resolveSizeAndState(III)I

    #@b
    move-result p2

    #@c
    .line 1544
    .end local v0           #desiredWidth:I
    .end local p2
    :cond_c
    return p2
.end method

.method private setValueInternal(IZ)V
    .registers 5
    .parameter "current"
    .parameter "notifyChange"

    #@0
    .prologue
    .line 1573
    iget v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@2
    if-ne v1, p1, :cond_5

    #@4
    .line 1591
    :goto_4
    return-void

    #@5
    .line 1577
    :cond_5
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@7
    if-eqz v1, :cond_20

    #@9
    .line 1578
    invoke-direct {p0, p1}, Landroid/widget/NumberPicker;->getWrappedSelectorIndex(I)I

    #@c
    move-result p1

    #@d
    .line 1583
    :goto_d
    iget v0, p0, Landroid/widget/NumberPicker;->mValue:I

    #@f
    .line 1584
    .local v0, previous:I
    iput p1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@11
    .line 1585
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@14
    .line 1586
    if-eqz p2, :cond_19

    #@16
    .line 1587
    invoke-direct {p0, v0, p1}, Landroid/widget/NumberPicker;->notifyChange(II)V

    #@19
    .line 1589
    :cond_19
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@1c
    .line 1590
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@1f
    goto :goto_4

    #@20
    .line 1580
    .end local v0           #previous:I
    :cond_20
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@22
    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    #@25
    move-result p1

    #@26
    .line 1581
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@28
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result p1

    #@2c
    goto :goto_d
.end method

.method private showSoftInput()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1169
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v2

    #@5
    const v3, 0x111004e

    #@8
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@b
    move-result v1

    #@c
    .line 1171
    .local v1, isDisableKeypad:Z
    if-eqz v1, :cond_f

    #@e
    .line 1183
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1175
    :cond_f
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@12
    move-result-object v0

    #@13
    .line 1176
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_e

    #@15
    .line 1177
    iget-boolean v2, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@17
    if-eqz v2, :cond_1e

    #@19
    .line 1178
    iget-object v2, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1b
    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setVisibility(I)V

    #@1e
    .line 1180
    :cond_1e
    iget-object v2, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@20
    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    #@23
    .line 1181
    iget-object v2, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@25
    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@28
    goto :goto_e
.end method

.method private tryComputeMaxWidth()V
    .registers 11

    #@0
    .prologue
    .line 1202
    iget-boolean v8, p0, Landroid/widget/NumberPicker;->mComputeMaxWidth:Z

    #@2
    if-nez v8, :cond_5

    #@4
    .line 1239
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1205
    :cond_5
    const/4 v4, 0x0

    #@6
    .line 1206
    .local v4, maxTextWidth:I
    iget-object v8, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@8
    if-nez v8, :cond_4b

    #@a
    .line 1207
    const/4 v3, 0x0

    #@b
    .line 1208
    .local v3, maxDigitWidth:F
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    const/16 v8, 0x9

    #@e
    if-gt v2, v8, :cond_22

    #@10
    .line 1209
    iget-object v8, p0, Landroid/widget/NumberPicker;->mSelectorWheelPaint:Landroid/graphics/Paint;

    #@12
    invoke-static {v2}, Landroid/widget/NumberPicker;->formatNumberWithLocale(I)Ljava/lang/String;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    #@19
    move-result v1

    #@1a
    .line 1210
    .local v1, digitWidth:F
    cmpl-float v8, v1, v3

    #@1c
    if-lez v8, :cond_1f

    #@1e
    .line 1211
    move v3, v1

    #@1f
    .line 1208
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_c

    #@22
    .line 1214
    .end local v1           #digitWidth:F
    :cond_22
    const/4 v5, 0x0

    #@23
    .line 1215
    .local v5, numberOfDigits:I
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@25
    .line 1216
    .local v0, current:I
    :goto_25
    if-lez v0, :cond_2c

    #@27
    .line 1217
    add-int/lit8 v5, v5, 0x1

    #@29
    .line 1218
    div-int/lit8 v0, v0, 0xa

    #@2b
    goto :goto_25

    #@2c
    .line 1220
    :cond_2c
    int-to-float v8, v5

    #@2d
    mul-float/2addr v8, v3

    #@2e
    float-to-int v4, v8

    #@2f
    .line 1230
    .end local v0           #current:I
    .end local v3           #maxDigitWidth:F
    .end local v5           #numberOfDigits:I
    :cond_2f
    iget-object v8, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@31
    invoke-virtual {v8}, Landroid/widget/EditText;->getPaddingLeft()I

    #@34
    move-result v8

    #@35
    iget-object v9, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@37
    invoke-virtual {v9}, Landroid/widget/EditText;->getPaddingRight()I

    #@3a
    move-result v9

    #@3b
    add-int/2addr v8, v9

    #@3c
    add-int/2addr v4, v8

    #@3d
    .line 1231
    iget v8, p0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@3f
    if-eq v8, v4, :cond_4

    #@41
    .line 1232
    iget v8, p0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@43
    if-le v4, v8, :cond_64

    #@45
    .line 1233
    iput v4, p0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@47
    .line 1237
    :goto_47
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@4a
    goto :goto_4

    #@4b
    .line 1222
    .end local v2           #i:I
    :cond_4b
    iget-object v8, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@4d
    array-length v7, v8

    #@4e
    .line 1223
    .local v7, valueCount:I
    const/4 v2, 0x0

    #@4f
    .restart local v2       #i:I
    :goto_4f
    if-ge v2, v7, :cond_2f

    #@51
    .line 1224
    iget-object v8, p0, Landroid/widget/NumberPicker;->mSelectorWheelPaint:Landroid/graphics/Paint;

    #@53
    iget-object v9, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@55
    aget-object v9, v9, v2

    #@57
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    #@5a
    move-result v6

    #@5b
    .line 1225
    .local v6, textWidth:F
    int-to-float v8, v4

    #@5c
    cmpl-float v8, v6, v8

    #@5e
    if-lez v8, :cond_61

    #@60
    .line 1226
    float-to-int v4, v6

    #@61
    .line 1223
    :cond_61
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_4f

    #@64
    .line 1235
    .end local v6           #textWidth:F
    .end local v7           #valueCount:I
    :cond_64
    iget v8, p0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@66
    iput v8, p0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@68
    goto :goto_47
.end method

.method private updateInputTextView()Z
    .registers 5

    #@0
    .prologue
    .line 1794
    iget-object v1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@2
    if-nez v1, :cond_27

    #@4
    iget v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@6
    invoke-direct {p0, v1}, Landroid/widget/NumberPicker;->formatNumber(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 1796
    .local v0, text:Ljava/lang/String;
    :goto_a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_31

    #@10
    iget-object v1, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@12
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_31

    #@20
    .line 1797
    iget-object v1, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@22
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@25
    .line 1798
    const/4 v1, 0x1

    #@26
    .line 1801
    :goto_26
    return v1

    #@27
    .line 1794
    .end local v0           #text:Ljava/lang/String;
    :cond_27
    iget-object v1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@29
    iget v2, p0, Landroid/widget/NumberPicker;->mValue:I

    #@2b
    iget v3, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@2d
    sub-int/2addr v2, v3

    #@2e
    aget-object v0, v1, v2

    #@30
    goto :goto_a

    #@31
    .line 1801
    .restart local v0       #text:Ljava/lang/String;
    :cond_31
    const/4 v1, 0x0

    #@32
    goto :goto_26
.end method

.method private validateInputTextView(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 1769
    check-cast p1, Landroid/widget/TextView;

    #@2
    .end local p1
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1770
    .local v1, str:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_14

    #@10
    .line 1772
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@13
    .line 1778
    :goto_13
    return-void

    #@14
    .line 1775
    :cond_14
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-direct {p0, v2}, Landroid/widget/NumberPicker;->getSelectedPos(Ljava/lang/String;)I

    #@1b
    move-result v0

    #@1c
    .line 1776
    .local v0, current:I
    const/4 v2, 0x1

    #@1d
    invoke-direct {p0, v0, v2}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@20
    goto :goto_13
.end method


# virtual methods
.method public computeScroll()V
    .registers 5

    #@0
    .prologue
    .line 1012
    iget-object v1, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@2
    .line 1013
    .local v1, scroller:Landroid/widget/Scroller;
    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_11

    #@8
    .line 1014
    iget-object v1, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@a
    .line 1015
    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_11

    #@10
    .line 1031
    :goto_10
    return-void

    #@11
    .line 1019
    :cond_11
    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@14
    .line 1020
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    #@17
    move-result v0

    #@18
    .line 1021
    .local v0, currentScrollerY:I
    iget v2, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@1a
    if-nez v2, :cond_22

    #@1c
    .line 1022
    invoke-virtual {v1}, Landroid/widget/Scroller;->getStartY()I

    #@1f
    move-result v2

    #@20
    iput v2, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@22
    .line 1024
    :cond_22
    const/4 v2, 0x0

    #@23
    iget v3, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@25
    sub-int v3, v0, v3

    #@27
    invoke-virtual {p0, v2, v3}, Landroid/widget/NumberPicker;->scrollBy(II)V

    #@2a
    .line 1025
    iput v0, p0, Landroid/widget/NumberPicker;->mPreviousScrollerY:I

    #@2c
    .line 1026
    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_36

    #@32
    .line 1027
    invoke-direct {p0, v1}, Landroid/widget/NumberPicker;->onScrollerFinished(Landroid/widget/Scroller;)V

    #@35
    goto :goto_10

    #@36
    .line 1029
    :cond_36
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@39
    goto :goto_10
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter "event"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/16 v8, 0x100

    #@3
    const/16 v7, 0x80

    #@5
    const/16 v6, 0x40

    #@7
    const/4 v5, -0x1

    #@8
    .line 963
    iget-boolean v4, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@a
    if-nez v4, :cond_11

    #@c
    .line 964
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@f
    move-result v4

    #@10
    .line 1007
    :goto_10
    return v4

    #@11
    .line 966
    :cond_11
    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@13
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_34

    #@1d
    .line 967
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@20
    move-result v4

    #@21
    float-to-int v1, v4

    #@22
    .line 969
    .local v1, eventY:I
    iget v4, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@24
    if-ge v1, v4, :cond_36

    #@26
    .line 970
    const/4 v2, 0x3

    #@27
    .line 976
    .local v2, hoveredVirtualViewId:I
    :goto_27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@2a
    move-result v0

    #@2b
    .line 977
    .local v0, action:I
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

    #@31
    .line 979
    .local v3, provider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;
    packed-switch v0, :pswitch_data_64

    #@34
    .line 1007
    .end local v0           #action:I
    .end local v1           #eventY:I
    .end local v2           #hoveredVirtualViewId:I
    .end local v3           #provider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;
    :cond_34
    :goto_34
    :pswitch_34
    const/4 v4, 0x0

    #@35
    goto :goto_10

    #@36
    .line 971
    .restart local v1       #eventY:I
    :cond_36
    iget v4, p0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@38
    if-le v1, v4, :cond_3c

    #@3a
    .line 972
    const/4 v2, 0x1

    #@3b
    .restart local v2       #hoveredVirtualViewId:I
    goto :goto_27

    #@3c
    .line 974
    .end local v2           #hoveredVirtualViewId:I
    :cond_3c
    const/4 v2, 0x2

    #@3d
    .restart local v2       #hoveredVirtualViewId:I
    goto :goto_27

    #@3e
    .line 981
    .restart local v0       #action:I
    .restart local v3       #provider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;
    :pswitch_3e
    invoke-virtual {v3, v2, v7}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@41
    .line 983
    iput v2, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@43
    .line 984
    invoke-virtual {v3, v2, v6, v9}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->performAction(IILandroid/os/Bundle;)Z

    #@46
    goto :goto_34

    #@47
    .line 988
    :pswitch_47
    iget v4, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@49
    if-eq v4, v2, :cond_34

    #@4b
    iget v4, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@4d
    if-eq v4, v5, :cond_34

    #@4f
    .line 990
    iget v4, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@51
    invoke-virtual {v3, v4, v8}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@54
    .line 993
    invoke-virtual {v3, v2, v7}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@57
    .line 995
    iput v2, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@59
    .line 996
    invoke-virtual {v3, v2, v6, v9}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->performAction(IILandroid/os/Bundle;)Z

    #@5c
    goto :goto_34

    #@5d
    .line 1001
    :pswitch_5d
    invoke-virtual {v3, v2, v8}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@60
    .line 1003
    iput v5, p0, Landroid/widget/NumberPicker;->mLastHoveredChildVirtualViewId:I

    #@62
    goto :goto_34

    #@63
    .line 979
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x7
        :pswitch_47
        :pswitch_34
        :pswitch_3e
        :pswitch_5d
    .end packed-switch
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 939
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    .line 940
    .local v0, keyCode:I
    sparse-switch v0, :sswitch_data_10

    #@7
    .line 946
    :goto_7
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@a
    move-result v1

    #@b
    return v1

    #@c
    .line 943
    :sswitch_c
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@f
    goto :goto_7

    #@10
    .line 940
    :sswitch_data_10
    .sparse-switch
        0x17 -> :sswitch_c
        0x42 -> :sswitch_c
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 927
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v0

    #@4
    .line 928
    .local v0, action:I
    packed-switch v0, :pswitch_data_10

    #@7
    .line 934
    :goto_7
    :pswitch_7
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@a
    move-result v1

    #@b
    return v1

    #@c
    .line 931
    :pswitch_c
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@f
    goto :goto_7

    #@10
    .line 928
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_c
        :pswitch_7
        :pswitch_c
    .end packed-switch
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 951
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v0

    #@4
    .line 952
    .local v0, action:I
    packed-switch v0, :pswitch_data_10

    #@7
    .line 958
    :goto_7
    :pswitch_7
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@a
    move-result v1

    #@b
    return v1

    #@c
    .line 955
    :pswitch_c
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@f
    goto :goto_7

    #@10
    .line 952
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_c
        :pswitch_7
        :pswitch_c
    .end packed-switch
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .registers 2

    #@0
    .prologue
    .line 1494
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@2
    if-nez v0, :cond_9

    #@4
    .line 1495
    invoke-super {p0}, Landroid/widget/LinearLayout;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@7
    move-result-object v0

    #@8
    .line 1500
    :goto_8
    return-object v0

    #@9
    .line 1497
    :cond_9
    iget-object v0, p0, Landroid/widget/NumberPicker;->mAccessibilityNodeProvider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

    #@b
    if-nez v0, :cond_14

    #@d
    .line 1498
    new-instance v0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

    #@f
    invoke-direct {v0, p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;-><init>(Landroid/widget/NumberPicker;)V

    #@12
    iput-object v0, p0, Landroid/widget/NumberPicker;->mAccessibilityNodeProvider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

    #@14
    .line 1500
    :cond_14
    iget-object v0, p0, Landroid/widget/NumberPicker;->mAccessibilityNodeProvider:Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;

    #@16
    goto :goto_8
.end method

.method protected getBottomFadingEdgeStrength()F
    .registers 2

    #@0
    .prologue
    .line 1418
    const v0, 0x3f666666

    #@3
    return v0
.end method

.method public getDisplayedValues()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1382
    iget-object v0, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMaxValue()I
    .registers 2

    #@0
    .prologue
    .line 1344
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@2
    return v0
.end method

.method public getMinValue()I
    .registers 2

    #@0
    .prologue
    .line 1306
    iget v0, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@2
    return v0
.end method

.method public getSolidColor()I
    .registers 2

    #@0
    .prologue
    .line 1090
    iget v0, p0, Landroid/widget/NumberPicker;->mSolidColor:I

    #@2
    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .registers 2

    #@0
    .prologue
    .line 1413
    const v0, 0x3f666666

    #@3
    return v0
.end method

.method public getValue()I
    .registers 2

    #@0
    .prologue
    .line 1297
    iget v0, p0, Landroid/widget/NumberPicker;->mValue:I

    #@2
    return v0
.end method

.method public getWrapSelectorWheel()Z
    .registers 2

    #@0
    .prologue
    .line 1250
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@2
    return v0
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 1423
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@3
    .line 1424
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 18
    .parameter "canvas"

    #@0
    .prologue
    .line 1428
    move-object/from16 v0, p0

    #@2
    iget-boolean v11, v0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@4
    if-nez v11, :cond_a

    #@6
    .line 1429
    invoke-super/range {p0 .. p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    #@9
    .line 1481
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1432
    :cond_a
    move-object/from16 v0, p0

    #@c
    iget v11, v0, Landroid/view/View;->mRight:I

    #@e
    move-object/from16 v0, p0

    #@10
    iget v12, v0, Landroid/view/View;->mLeft:I

    #@12
    sub-int/2addr v11, v12

    #@13
    div-int/lit8 v11, v11, 0x2

    #@15
    int-to-float v9, v11

    #@16
    .line 1433
    .local v9, x:F
    move-object/from16 v0, p0

    #@18
    iget v11, v0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@1a
    int-to-float v10, v11

    #@1b
    .line 1436
    .local v10, y:F
    move-object/from16 v0, p0

    #@1d
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@1f
    if-eqz v11, :cond_7c

    #@21
    move-object/from16 v0, p0

    #@23
    iget v11, v0, Landroid/widget/NumberPicker;->mScrollState:I

    #@25
    if-nez v11, :cond_7c

    #@27
    .line 1438
    move-object/from16 v0, p0

    #@29
    iget-boolean v11, v0, Landroid/widget/NumberPicker;->mDecrementVirtualButtonPressed:Z

    #@2b
    if-eqz v11, :cond_50

    #@2d
    .line 1439
    move-object/from16 v0, p0

    #@2f
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@31
    sget-object v12, Landroid/widget/NumberPicker;->PRESSED_STATE_SET:[I

    #@33
    invoke-virtual {v11, v12}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@36
    .line 1440
    move-object/from16 v0, p0

    #@38
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@3a
    const/4 v12, 0x0

    #@3b
    const/4 v13, 0x0

    #@3c
    move-object/from16 v0, p0

    #@3e
    iget v14, v0, Landroid/view/View;->mRight:I

    #@40
    move-object/from16 v0, p0

    #@42
    iget v15, v0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@44
    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@47
    .line 1441
    move-object/from16 v0, p0

    #@49
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@4b
    move-object/from16 v0, p1

    #@4d
    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@50
    .line 1443
    :cond_50
    move-object/from16 v0, p0

    #@52
    iget-boolean v11, v0, Landroid/widget/NumberPicker;->mIncrementVirtualButtonPressed:Z

    #@54
    if-eqz v11, :cond_7c

    #@56
    .line 1444
    move-object/from16 v0, p0

    #@58
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@5a
    sget-object v12, Landroid/widget/NumberPicker;->PRESSED_STATE_SET:[I

    #@5c
    invoke-virtual {v11, v12}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@5f
    .line 1445
    move-object/from16 v0, p0

    #@61
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@63
    const/4 v12, 0x0

    #@64
    move-object/from16 v0, p0

    #@66
    iget v13, v0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@68
    move-object/from16 v0, p0

    #@6a
    iget v14, v0, Landroid/view/View;->mRight:I

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget v15, v0, Landroid/view/View;->mBottom:I

    #@70
    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@73
    .line 1447
    move-object/from16 v0, p0

    #@75
    iget-object v11, v0, Landroid/widget/NumberPicker;->mVirtualButtonPressedDrawable:Landroid/graphics/drawable/Drawable;

    #@77
    move-object/from16 v0, p1

    #@79
    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@7c
    .line 1452
    :cond_7c
    move-object/from16 v0, p0

    #@7e
    iget-object v6, v0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@80
    .line 1453
    .local v6, selectorIndices:[I
    const/4 v3, 0x0

    #@81
    .local v3, i:I
    :goto_81
    array-length v11, v6

    #@82
    if-ge v3, v11, :cond_af

    #@84
    .line 1454
    aget v5, v6, v3

    #@86
    .line 1455
    .local v5, selectorIndex:I
    move-object/from16 v0, p0

    #@88
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectorIndexToStringCache:Landroid/util/SparseArray;

    #@8a
    invoke-virtual {v11, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8d
    move-result-object v4

    #@8e
    check-cast v4, Ljava/lang/String;

    #@90
    .line 1461
    .local v4, scrollSelectorValue:Ljava/lang/String;
    const/4 v11, 0x1

    #@91
    if-ne v3, v11, :cond_9d

    #@93
    move-object/from16 v0, p0

    #@95
    iget-object v11, v0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@97
    invoke-virtual {v11}, Landroid/widget/EditText;->getVisibility()I

    #@9a
    move-result v11

    #@9b
    if-eqz v11, :cond_a6

    #@9d
    .line 1462
    :cond_9d
    move-object/from16 v0, p0

    #@9f
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectorWheelPaint:Landroid/graphics/Paint;

    #@a1
    move-object/from16 v0, p1

    #@a3
    invoke-virtual {v0, v4, v9, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@a6
    .line 1464
    :cond_a6
    move-object/from16 v0, p0

    #@a8
    iget v11, v0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@aa
    int-to-float v11, v11

    #@ab
    add-float/2addr v10, v11

    #@ac
    .line 1453
    add-int/lit8 v3, v3, 0x1

    #@ae
    goto :goto_81

    #@af
    .line 1468
    .end local v4           #scrollSelectorValue:Ljava/lang/String;
    .end local v5           #selectorIndex:I
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@b3
    if-eqz v11, :cond_9

    #@b5
    .line 1470
    move-object/from16 v0, p0

    #@b7
    iget v8, v0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@b9
    .line 1471
    .local v8, topOfTopDivider:I
    move-object/from16 v0, p0

    #@bb
    iget v11, v0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@bd
    add-int v2, v8, v11

    #@bf
    .line 1472
    .local v2, bottomOfTopDivider:I
    move-object/from16 v0, p0

    #@c1
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@c3
    const/4 v12, 0x0

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget v13, v0, Landroid/view/View;->mRight:I

    #@c8
    invoke-virtual {v11, v12, v8, v13, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@cb
    .line 1473
    move-object/from16 v0, p0

    #@cd
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@cf
    move-object/from16 v0, p1

    #@d1
    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@d4
    .line 1476
    move-object/from16 v0, p0

    #@d6
    iget v1, v0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@d8
    .line 1477
    .local v1, bottomOfBottomDivider:I
    move-object/from16 v0, p0

    #@da
    iget v11, v0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@dc
    sub-int v7, v1, v11

    #@de
    .line 1478
    .local v7, topOfBottomDivider:I
    move-object/from16 v0, p0

    #@e0
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@e2
    const/4 v12, 0x0

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget v13, v0, Landroid/view/View;->mRight:I

    #@e7
    invoke-virtual {v11, v12, v7, v13, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@ea
    .line 1479
    move-object/from16 v0, p0

    #@ec
    iget-object v11, v0, Landroid/widget/NumberPicker;->mSelectionDivider:Landroid/graphics/drawable/Drawable;

    #@ee
    move-object/from16 v0, p1

    #@f0
    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@f3
    goto/16 :goto_9
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1485
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1486
    const-class v0, Landroid/widget/NumberPicker;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1487
    const/4 v0, 0x1

    #@d
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@10
    .line 1488
    iget v0, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@12
    iget v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@14
    add-int/2addr v0, v1

    #@15
    iget v1, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@17
    mul-int/2addr v0, v1

    #@18
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    #@1b
    .line 1489
    iget v0, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1d
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@1f
    sub-int/2addr v0, v1

    #@20
    iget v1, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@22
    mul-int/2addr v0, v1

    #@23
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    #@26
    .line 1490
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 802
    iget-boolean v3, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@4
    if-eqz v3, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_e

    #@c
    :cond_c
    move v1, v2

    #@d
    .line 850
    :goto_d
    return v1

    #@e
    .line 805
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@11
    move-result v0

    #@12
    .line 806
    .local v0, action:I
    packed-switch v0, :pswitch_data_bc

    #@15
    move v1, v2

    #@16
    .line 850
    goto :goto_d

    #@17
    .line 808
    :pswitch_17
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@1a
    .line 809
    iget-object v3, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1c
    const/4 v4, 0x4

    #@1d
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setVisibility(I)V

    #@20
    .line 810
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@23
    move-result v3

    #@24
    iput v3, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@26
    iput v3, p0, Landroid/widget/NumberPicker;->mLastDownOrMoveEventY:F

    #@28
    .line 811
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@2b
    move-result-wide v3

    #@2c
    iput-wide v3, p0, Landroid/widget/NumberPicker;->mLastDownEventTime:J

    #@2e
    .line 812
    iput-boolean v2, p0, Landroid/widget/NumberPicker;->mIngonreMoveEvents:Z

    #@30
    .line 813
    iput-boolean v2, p0, Landroid/widget/NumberPicker;->mShowSoftInputOnTap:Z

    #@32
    .line 815
    iget v3, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@34
    iget v4, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@36
    int-to-float v4, v4

    #@37
    cmpg-float v3, v3, v4

    #@39
    if-gez v3, :cond_62

    #@3b
    .line 816
    iget v3, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@3d
    if-nez v3, :cond_45

    #@3f
    .line 817
    iget-object v3, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@41
    const/4 v4, 0x2

    #@42
    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker$PressedStateHelper;->buttonPressDelayed(I)V

    #@45
    .line 827
    :cond_45
    :goto_45
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getParent()Landroid/view/ViewParent;

    #@48
    move-result-object v3

    #@49
    invoke-interface {v3, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@4c
    .line 828
    iget-object v3, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@4e
    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    #@51
    move-result v3

    #@52
    if-nez v3, :cond_75

    #@54
    .line 829
    iget-object v3, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@56
    invoke-virtual {v3, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@59
    .line 830
    iget-object v3, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@5b
    invoke-virtual {v3, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@5e
    .line 831
    invoke-direct {p0, v2}, Landroid/widget/NumberPicker;->onScrollStateChange(I)V

    #@61
    goto :goto_d

    #@62
    .line 820
    :cond_62
    iget v3, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@64
    iget v4, p0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@66
    int-to-float v4, v4

    #@67
    cmpl-float v3, v3, v4

    #@69
    if-lez v3, :cond_45

    #@6b
    .line 821
    iget v3, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@6d
    if-nez v3, :cond_45

    #@6f
    .line 822
    iget-object v3, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@71
    invoke-virtual {v3, v1}, Landroid/widget/NumberPicker$PressedStateHelper;->buttonPressDelayed(I)V

    #@74
    goto :goto_45

    #@75
    .line 832
    :cond_75
    iget-object v3, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@77
    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    #@7a
    move-result v3

    #@7b
    if-nez v3, :cond_88

    #@7d
    .line 833
    iget-object v2, p0, Landroid/widget/NumberPicker;->mFlingScroller:Landroid/widget/Scroller;

    #@7f
    invoke-virtual {v2, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@82
    .line 834
    iget-object v2, p0, Landroid/widget/NumberPicker;->mAdjustScroller:Landroid/widget/Scroller;

    #@84
    invoke-virtual {v2, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@87
    goto :goto_d

    #@88
    .line 835
    :cond_88
    iget v3, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@8a
    iget v4, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@8c
    int-to-float v4, v4

    #@8d
    cmpg-float v3, v3, v4

    #@8f
    if-gez v3, :cond_9e

    #@91
    .line 836
    invoke-direct {p0}, Landroid/widget/NumberPicker;->hideSoftInput()V

    #@94
    .line 837
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@97
    move-result v3

    #@98
    int-to-long v3, v3

    #@99
    invoke-direct {p0, v2, v3, v4}, Landroid/widget/NumberPicker;->postChangeCurrentByOneFromLongPress(ZJ)V

    #@9c
    goto/16 :goto_d

    #@9e
    .line 839
    :cond_9e
    iget v2, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@a0
    iget v3, p0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@a2
    int-to-float v3, v3

    #@a3
    cmpl-float v2, v2, v3

    #@a5
    if-lez v2, :cond_b4

    #@a7
    .line 840
    invoke-direct {p0}, Landroid/widget/NumberPicker;->hideSoftInput()V

    #@aa
    .line 841
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@ad
    move-result v2

    #@ae
    int-to-long v2, v2

    #@af
    invoke-direct {p0, v1, v2, v3}, Landroid/widget/NumberPicker;->postChangeCurrentByOneFromLongPress(ZJ)V

    #@b2
    goto/16 :goto_d

    #@b4
    .line 844
    :cond_b4
    iput-boolean v1, p0, Landroid/widget/NumberPicker;->mShowSoftInputOnTap:Z

    #@b6
    .line 845
    invoke-direct {p0}, Landroid/widget/NumberPicker;->postBeginSoftInputOnLongPressCommand()V

    #@b9
    goto/16 :goto_d

    #@bb
    .line 806
    nop

    #@bc
    :pswitch_data_bc
    .packed-switch 0x0
        :pswitch_17
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 16
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 727
    iget-boolean v8, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@2
    if-nez v8, :cond_8

    #@4
    .line 728
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    #@7
    .line 752
    :cond_7
    :goto_7
    return-void

    #@8
    .line 731
    :cond_8
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getMeasuredWidth()I

    #@b
    move-result v7

    #@c
    .line 732
    .local v7, msrdWdth:I
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getMeasuredHeight()I

    #@f
    move-result v6

    #@10
    .line 735
    .local v6, msrdHght:I
    iget-object v8, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@12
    invoke-virtual {v8}, Landroid/widget/EditText;->getMeasuredWidth()I

    #@15
    move-result v3

    #@16
    .line 736
    .local v3, inptTxtMsrdWdth:I
    iget-object v8, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@18
    invoke-virtual {v8}, Landroid/widget/EditText;->getMeasuredHeight()I

    #@1b
    move-result v2

    #@1c
    .line 737
    .local v2, inptTxtMsrdHght:I
    sub-int v8, v7, v3

    #@1e
    div-int/lit8 v1, v8, 0x2

    #@20
    .line 738
    .local v1, inptTxtLeft:I
    sub-int v8, v6, v2

    #@22
    div-int/lit8 v5, v8, 0x2

    #@24
    .line 739
    .local v5, inptTxtTop:I
    add-int v4, v1, v3

    #@26
    .line 740
    .local v4, inptTxtRight:I
    add-int v0, v5, v2

    #@28
    .line 741
    .local v0, inptTxtBottom:I
    iget-object v8, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@2a
    invoke-virtual {v8, v1, v5, v4, v0}, Landroid/widget/EditText;->layout(IIII)V

    #@2d
    .line 743
    if-eqz p1, :cond_7

    #@2f
    .line 745
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheel()V

    #@32
    .line 746
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeFadingEdges()V

    #@35
    .line 747
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getHeight()I

    #@38
    move-result v8

    #@39
    iget v9, p0, Landroid/widget/NumberPicker;->mSelectionDividersDistance:I

    #@3b
    sub-int/2addr v8, v9

    #@3c
    div-int/lit8 v8, v8, 0x2

    #@3e
    iget v9, p0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@40
    sub-int/2addr v8, v9

    #@41
    iput v8, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@43
    .line 749
    iget v8, p0, Landroid/widget/NumberPicker;->mTopSelectionDividerTop:I

    #@45
    iget v9, p0, Landroid/widget/NumberPicker;->mSelectionDividerHeight:I

    #@47
    mul-int/lit8 v9, v9, 0x2

    #@49
    add-int/2addr v8, v9

    #@4a
    iget v9, p0, Landroid/widget/NumberPicker;->mSelectionDividersDistance:I

    #@4c
    add-int/2addr v8, v9

    #@4d
    iput v8, p0, Landroid/widget/NumberPicker;->mBottomSelectionDividerBottom:I

    #@4f
    goto :goto_7
.end method

.method protected onMeasure(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 756
    iget-boolean v4, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@2
    if-nez v4, :cond_8

    #@4
    .line 757
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@7
    .line 770
    :goto_7
    return-void

    #@8
    .line 761
    :cond_8
    iget v4, p0, Landroid/widget/NumberPicker;->mMaxWidth:I

    #@a
    invoke-direct {p0, p1, v4}, Landroid/widget/NumberPicker;->makeMeasureSpec(II)I

    #@d
    move-result v2

    #@e
    .line 762
    .local v2, newWidthMeasureSpec:I
    iget v4, p0, Landroid/widget/NumberPicker;->mMaxHeight:I

    #@10
    invoke-direct {p0, p2, v4}, Landroid/widget/NumberPicker;->makeMeasureSpec(II)I

    #@13
    move-result v1

    #@14
    .line 763
    .local v1, newHeightMeasureSpec:I
    invoke-super {p0, v2, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@17
    .line 765
    iget v4, p0, Landroid/widget/NumberPicker;->mMinWidth:I

    #@19
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getMeasuredWidth()I

    #@1c
    move-result v5

    #@1d
    invoke-direct {p0, v4, v5, p1}, Landroid/widget/NumberPicker;->resolveSizeAndStateRespectingMinSize(III)I

    #@20
    move-result v3

    #@21
    .line 767
    .local v3, widthSize:I
    iget v4, p0, Landroid/widget/NumberPicker;->mMinHeight:I

    #@23
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->getMeasuredHeight()I

    #@26
    move-result v5

    #@27
    invoke-direct {p0, v4, v5, p2}, Landroid/widget/NumberPicker;->resolveSizeAndStateRespectingMinSize(III)I

    #@2a
    move-result v0

    #@2b
    .line 769
    .local v0, heightSize:I
    invoke-virtual {p0, v3, v0}, Landroid/widget/NumberPicker;->setMeasuredDimension(II)V

    #@2e
    goto :goto_7
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "event"

    #@0
    .prologue
    .line 855
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@3
    move-result v10

    #@4
    if-eqz v10, :cond_a

    #@6
    iget-boolean v10, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@8
    if-nez v10, :cond_c

    #@a
    .line 856
    :cond_a
    const/4 v10, 0x0

    #@b
    .line 922
    :goto_b
    return v10

    #@c
    .line 858
    :cond_c
    iget-object v10, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@e
    if-nez v10, :cond_16

    #@10
    .line 859
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@13
    move-result-object v10

    #@14
    iput-object v10, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@16
    .line 861
    :cond_16
    iget-object v10, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@18
    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@1b
    .line 862
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@1e
    move-result v0

    #@1f
    .line 863
    .local v0, action:I
    packed-switch v0, :pswitch_data_de

    #@22
    .line 922
    :cond_22
    :goto_22
    const/4 v10, 0x1

    #@23
    goto :goto_b

    #@24
    .line 865
    :pswitch_24
    iget-boolean v10, p0, Landroid/widget/NumberPicker;->mIngonreMoveEvents:Z

    #@26
    if-nez v10, :cond_22

    #@28
    .line 868
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2b
    move-result v1

    #@2c
    .line 869
    .local v1, currentMoveY:F
    iget v10, p0, Landroid/widget/NumberPicker;->mScrollState:I

    #@2e
    const/4 v11, 0x1

    #@2f
    if-eq v10, v11, :cond_48

    #@31
    .line 870
    iget v10, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@33
    sub-float v10, v1, v10

    #@35
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@38
    move-result v10

    #@39
    float-to-int v2, v10

    #@3a
    .line 871
    .local v2, deltaDownY:I
    iget v10, p0, Landroid/widget/NumberPicker;->mTouchSlop:I

    #@3c
    if-le v2, v10, :cond_45

    #@3e
    .line 872
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeAllCallbacks()V

    #@41
    .line 873
    const/4 v10, 0x1

    #@42
    invoke-direct {p0, v10}, Landroid/widget/NumberPicker;->onScrollStateChange(I)V

    #@45
    .line 880
    .end local v2           #deltaDownY:I
    :cond_45
    :goto_45
    iput v1, p0, Landroid/widget/NumberPicker;->mLastDownOrMoveEventY:F

    #@47
    goto :goto_22

    #@48
    .line 876
    :cond_48
    iget v10, p0, Landroid/widget/NumberPicker;->mLastDownOrMoveEventY:F

    #@4a
    sub-float v10, v1, v10

    #@4c
    float-to-int v3, v10

    #@4d
    .line 877
    .local v3, deltaMoveY:I
    const/4 v10, 0x0

    #@4e
    invoke-virtual {p0, v10, v3}, Landroid/widget/NumberPicker;->scrollBy(II)V

    #@51
    .line 878
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@54
    goto :goto_45

    #@55
    .line 883
    .end local v1           #currentMoveY:F
    .end local v3           #deltaMoveY:I
    :pswitch_55
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeBeginSoftInputCommand()V

    #@58
    .line 884
    invoke-direct {p0}, Landroid/widget/NumberPicker;->removeChangeCurrentByOneFromLongPress()V

    #@5b
    .line 885
    iget-object v10, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@5d
    invoke-virtual {v10}, Landroid/widget/NumberPicker$PressedStateHelper;->cancel()V

    #@60
    .line 886
    iget-object v9, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@62
    .line 887
    .local v9, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v10, 0x3e8

    #@64
    iget v11, p0, Landroid/widget/NumberPicker;->mMaximumFlingVelocity:I

    #@66
    int-to-float v11, v11

    #@67
    invoke-virtual {v9, v10, v11}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@6a
    .line 888
    invoke-virtual {v9}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@6d
    move-result v10

    #@6e
    float-to-int v7, v10

    #@6f
    .line 889
    .local v7, initialVelocity:I
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@72
    move-result v10

    #@73
    iget v11, p0, Landroid/widget/NumberPicker;->mMinimumFlingVelocity:I

    #@75
    if-le v10, v11, :cond_87

    #@77
    .line 890
    invoke-direct {p0, v7}, Landroid/widget/NumberPicker;->fling(I)V

    #@7a
    .line 891
    const/4 v10, 0x2

    #@7b
    invoke-direct {p0, v10}, Landroid/widget/NumberPicker;->onScrollStateChange(I)V

    #@7e
    .line 918
    :goto_7e
    iget-object v10, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@80
    invoke-virtual {v10}, Landroid/view/VelocityTracker;->recycle()V

    #@83
    .line 919
    const/4 v10, 0x0

    #@84
    iput-object v10, p0, Landroid/widget/NumberPicker;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@86
    goto :goto_22

    #@87
    .line 893
    :cond_87
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@8a
    move-result v10

    #@8b
    float-to-int v6, v10

    #@8c
    .line 894
    .local v6, eventY:I
    int-to-float v10, v6

    #@8d
    iget v11, p0, Landroid/widget/NumberPicker;->mLastDownEventY:F

    #@8f
    sub-float/2addr v10, v11

    #@90
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@93
    move-result v10

    #@94
    float-to-int v3, v10

    #@95
    .line 895
    .restart local v3       #deltaMoveY:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@98
    move-result-wide v10

    #@99
    iget-wide v12, p0, Landroid/widget/NumberPicker;->mLastDownEventTime:J

    #@9b
    sub-long v4, v10, v12

    #@9d
    .line 896
    .local v4, deltaTime:J
    iget v10, p0, Landroid/widget/NumberPicker;->mTouchSlop:I

    #@9f
    if-gt v3, v10, :cond_d9

    #@a1
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@a4
    move-result v10

    #@a5
    int-to-long v10, v10

    #@a6
    cmp-long v10, v4, v10

    #@a8
    if-gez v10, :cond_d9

    #@aa
    .line 897
    iget-boolean v10, p0, Landroid/widget/NumberPicker;->mShowSoftInputOnTap:Z

    #@ac
    if-eqz v10, :cond_b9

    #@ae
    .line 898
    const/4 v10, 0x0

    #@af
    iput-boolean v10, p0, Landroid/widget/NumberPicker;->mShowSoftInputOnTap:Z

    #@b1
    .line 899
    invoke-direct {p0}, Landroid/widget/NumberPicker;->showSoftInput()V

    #@b4
    .line 916
    :cond_b4
    :goto_b4
    const/4 v10, 0x0

    #@b5
    invoke-direct {p0, v10}, Landroid/widget/NumberPicker;->onScrollStateChange(I)V

    #@b8
    goto :goto_7e

    #@b9
    .line 901
    :cond_b9
    iget v10, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@bb
    div-int v10, v6, v10

    #@bd
    add-int/lit8 v8, v10, -0x1

    #@bf
    .line 903
    .local v8, selectorIndexOffset:I
    if-lez v8, :cond_cc

    #@c1
    .line 904
    const/4 v10, 0x1

    #@c2
    invoke-direct {p0, v10}, Landroid/widget/NumberPicker;->changeValueByOne(Z)V

    #@c5
    .line 905
    iget-object v10, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@c7
    const/4 v11, 0x1

    #@c8
    invoke-virtual {v10, v11}, Landroid/widget/NumberPicker$PressedStateHelper;->buttonTapped(I)V

    #@cb
    goto :goto_b4

    #@cc
    .line 907
    :cond_cc
    if-gez v8, :cond_b4

    #@ce
    .line 908
    const/4 v10, 0x0

    #@cf
    invoke-direct {p0, v10}, Landroid/widget/NumberPicker;->changeValueByOne(Z)V

    #@d2
    .line 909
    iget-object v10, p0, Landroid/widget/NumberPicker;->mPressedStateHelper:Landroid/widget/NumberPicker$PressedStateHelper;

    #@d4
    const/4 v11, 0x2

    #@d5
    invoke-virtual {v10, v11}, Landroid/widget/NumberPicker$PressedStateHelper;->buttonTapped(I)V

    #@d8
    goto :goto_b4

    #@d9
    .line 914
    .end local v8           #selectorIndexOffset:I
    :cond_d9
    invoke-direct {p0}, Landroid/widget/NumberPicker;->ensureScrollWheelAdjusted()Z

    #@dc
    goto :goto_b4

    #@dd
    .line 863
    nop

    #@de
    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_55
        :pswitch_24
    .end packed-switch
.end method

.method public scrollBy(II)V
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1047
    iget-object v0, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@3
    .line 1048
    .local v0, selectorIndices:[I
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@5
    if-nez v1, :cond_14

    #@7
    if-lez p2, :cond_14

    #@9
    aget v1, v0, v3

    #@b
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@d
    if-gt v1, v2, :cond_14

    #@f
    .line 1050
    iget v1, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@11
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@13
    .line 1086
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1053
    :cond_14
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@16
    if-nez v1, :cond_25

    #@18
    if-gez p2, :cond_25

    #@1a
    aget v1, v0, v3

    #@1c
    iget v2, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1e
    if-lt v1, v2, :cond_25

    #@20
    .line 1055
    iget v1, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@22
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@24
    goto :goto_13

    #@25
    .line 1058
    :cond_25
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@27
    add-int/2addr v1, p2

    #@28
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@2a
    .line 1065
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@2c
    iget v2, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@2e
    sub-int/2addr v1, v2

    #@2f
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorTextGapHeight:I

    #@31
    if-le v1, v2, :cond_5a

    #@33
    .line 1066
    :cond_33
    :goto_33
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@35
    iget v2, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@37
    sub-int/2addr v1, v2

    #@38
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorTextGapHeight:I

    #@3a
    if-le v1, v2, :cond_13

    #@3c
    .line 1067
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@3e
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@40
    sub-int/2addr v1, v2

    #@41
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@43
    .line 1068
    invoke-direct {p0, v0}, Landroid/widget/NumberPicker;->decrementSelectorIndices([I)V

    #@46
    .line 1069
    aget v1, v0, v3

    #@48
    invoke-direct {p0, v1, v3}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@4b
    .line 1070
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@4d
    if-nez v1, :cond_33

    #@4f
    aget v1, v0, v3

    #@51
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@53
    if-gt v1, v2, :cond_33

    #@55
    .line 1071
    iget v1, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@57
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@59
    goto :goto_33

    #@5a
    .line 1075
    :cond_5a
    :goto_5a
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@5c
    iget v2, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@5e
    sub-int/2addr v1, v2

    #@5f
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorTextGapHeight:I

    #@61
    neg-int v2, v2

    #@62
    if-ge v1, v2, :cond_13

    #@64
    .line 1076
    iget v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@66
    iget v2, p0, Landroid/widget/NumberPicker;->mSelectorElementHeight:I

    #@68
    add-int/2addr v1, v2

    #@69
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@6b
    .line 1077
    invoke-direct {p0, v0}, Landroid/widget/NumberPicker;->incrementSelectorIndices([I)V

    #@6e
    .line 1078
    aget v1, v0, v3

    #@70
    invoke-direct {p0, v1, v3}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@73
    .line 1079
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@75
    if-nez v1, :cond_5a

    #@77
    aget v1, v0, v3

    #@79
    iget v2, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@7b
    if-lt v1, v2, :cond_5a

    #@7d
    .line 1080
    iget v1, p0, Landroid/widget/NumberPicker;->mInitialScrollOffset:I

    #@7f
    iput v1, p0, Landroid/widget/NumberPicker;->mCurrentScrollOffset:I

    #@81
    goto :goto_5a
.end method

.method public setDisplayedValues([Ljava/lang/String;)V
    .registers 4
    .parameter "displayedValues"

    #@0
    .prologue
    .line 1395
    iget-object v0, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 1409
    :goto_4
    return-void

    #@5
    .line 1398
    :cond_5
    iput-object p1, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@7
    .line 1399
    iget-object v0, p0, Landroid/widget/NumberPicker;->mDisplayedValues:[Ljava/lang/String;

    #@9
    if-eqz v0, :cond_1d

    #@b
    .line 1401
    iget-object v0, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@d
    const v1, 0x80001

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setRawInputType(I)V

    #@13
    .line 1406
    :goto_13
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@16
    .line 1407
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@19
    .line 1408
    invoke-direct {p0}, Landroid/widget/NumberPicker;->tryComputeMaxWidth()V

    #@1c
    goto :goto_4

    #@1d
    .line 1404
    :cond_1d
    iget-object v0, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@1f
    const/4 v1, 0x2

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setRawInputType(I)V

    #@23
    goto :goto_13
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1035
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    #@3
    .line 1036
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 1037
    iget-object v0, p0, Landroid/widget/NumberPicker;->mIncrementButton:Landroid/widget/ImageButton;

    #@9
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@c
    .line 1039
    :cond_c
    iget-boolean v0, p0, Landroid/widget/NumberPicker;->mHasSelectorWheel:Z

    #@e
    if-nez v0, :cond_15

    #@10
    .line 1040
    iget-object v0, p0, Landroid/widget/NumberPicker;->mDecrementButton:Landroid/widget/ImageButton;

    #@12
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@15
    .line 1042
    :cond_15
    iget-object v0, p0, Landroid/widget/NumberPicker;->mInputText:Landroid/widget/EditText;

    #@17
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    #@1a
    .line 1043
    return-void
.end method

.method public setFormatter(Landroid/widget/NumberPicker$Formatter;)V
    .registers 3
    .parameter "formatter"

    #@0
    .prologue
    .line 1123
    iget-object v0, p0, Landroid/widget/NumberPicker;->mFormatter:Landroid/widget/NumberPicker$Formatter;

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 1129
    :goto_4
    return-void

    #@5
    .line 1126
    :cond_5
    iput-object p1, p0, Landroid/widget/NumberPicker;->mFormatter:Landroid/widget/NumberPicker$Formatter;

    #@7
    .line 1127
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@a
    .line 1128
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@d
    goto :goto_4
.end method

.method public setMaxValue(I)V
    .registers 5
    .parameter "maxValue"

    #@0
    .prologue
    .line 1358
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@2
    if-ne v1, p1, :cond_5

    #@4
    .line 1374
    :goto_4
    return-void

    #@5
    .line 1361
    :cond_5
    if-gez p1, :cond_10

    #@7
    .line 1362
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v2, "maxValue must be >= 0"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 1364
    :cond_10
    iput p1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@12
    .line 1365
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@14
    iget v2, p0, Landroid/widget/NumberPicker;->mValue:I

    #@16
    if-ge v1, v2, :cond_1c

    #@18
    .line 1366
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1a
    iput v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@1c
    .line 1368
    :cond_1c
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1e
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@20
    sub-int/2addr v1, v2

    #@21
    iget-object v2, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@23
    array-length v2, v2

    #@24
    if-le v1, v2, :cond_37

    #@26
    const/4 v0, 0x1

    #@27
    .line 1369
    .local v0, wrapSelectorWheel:Z
    :goto_27
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@2a
    .line 1370
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@2d
    .line 1371
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@30
    .line 1372
    invoke-direct {p0}, Landroid/widget/NumberPicker;->tryComputeMaxWidth()V

    #@33
    .line 1373
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@36
    goto :goto_4

    #@37
    .line 1368
    .end local v0           #wrapSelectorWheel:Z
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_27
.end method

.method public setMinValue(I)V
    .registers 5
    .parameter "minValue"

    #@0
    .prologue
    .line 1320
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@2
    if-ne v1, p1, :cond_5

    #@4
    .line 1336
    :goto_4
    return-void

    #@5
    .line 1323
    :cond_5
    if-gez p1, :cond_10

    #@7
    .line 1324
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v2, "minValue must be >= 0"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 1326
    :cond_10
    iput p1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@12
    .line 1327
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@14
    iget v2, p0, Landroid/widget/NumberPicker;->mValue:I

    #@16
    if-le v1, v2, :cond_1c

    #@18
    .line 1328
    iget v1, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@1a
    iput v1, p0, Landroid/widget/NumberPicker;->mValue:I

    #@1c
    .line 1330
    :cond_1c
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@1e
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@20
    sub-int/2addr v1, v2

    #@21
    iget-object v2, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@23
    array-length v2, v2

    #@24
    if-le v1, v2, :cond_37

    #@26
    const/4 v0, 0x1

    #@27
    .line 1331
    .local v0, wrapSelectorWheel:Z
    :goto_27
    invoke-virtual {p0, v0}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    #@2a
    .line 1332
    invoke-direct {p0}, Landroid/widget/NumberPicker;->initializeSelectorWheelIndices()V

    #@2d
    .line 1333
    invoke-direct {p0}, Landroid/widget/NumberPicker;->updateInputTextView()Z

    #@30
    .line 1334
    invoke-direct {p0}, Landroid/widget/NumberPicker;->tryComputeMaxWidth()V

    #@33
    .line 1335
    invoke-virtual {p0}, Landroid/widget/NumberPicker;->invalidate()V

    #@36
    goto :goto_4

    #@37
    .line 1330
    .end local v0           #wrapSelectorWheel:Z
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_27
.end method

.method public setOnLongPressUpdateInterval(J)V
    .registers 3
    .parameter "intervalMillis"

    #@0
    .prologue
    .line 1288
    iput-wide p1, p0, Landroid/widget/NumberPicker;->mLongPressUpdateInterval:J

    #@2
    .line 1289
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/NumberPicker$OnScrollListener;)V
    .registers 2
    .parameter "onScrollListener"

    #@0
    .prologue
    .line 1108
    iput-object p1, p0, Landroid/widget/NumberPicker;->mOnScrollListener:Landroid/widget/NumberPicker$OnScrollListener;

    #@2
    .line 1109
    return-void
.end method

.method public setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V
    .registers 2
    .parameter "onValueChangedListener"

    #@0
    .prologue
    .line 1099
    iput-object p1, p0, Landroid/widget/NumberPicker;->mOnValueChangeListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    #@2
    .line 1100
    return-void
.end method

.method public setValue(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 1160
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/NumberPicker;->setValueInternal(IZ)V

    #@4
    .line 1161
    return-void
.end method

.method public setWrapSelectorWheel(Z)V
    .registers 5
    .parameter "wrapSelectorWheel"

    #@0
    .prologue
    .line 1271
    iget v1, p0, Landroid/widget/NumberPicker;->mMaxValue:I

    #@2
    iget v2, p0, Landroid/widget/NumberPicker;->mMinValue:I

    #@4
    sub-int/2addr v1, v2

    #@5
    iget-object v2, p0, Landroid/widget/NumberPicker;->mSelectorIndices:[I

    #@7
    array-length v2, v2

    #@8
    if-lt v1, v2, :cond_16

    #@a
    const/4 v0, 0x1

    #@b
    .line 1272
    .local v0, wrappingAllowed:Z
    :goto_b
    if-eqz p1, :cond_f

    #@d
    if-eqz v0, :cond_15

    #@f
    :cond_f
    iget-boolean v1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@11
    if-eq p1, v1, :cond_15

    #@13
    .line 1273
    iput-boolean p1, p0, Landroid/widget/NumberPicker;->mWrapSelectorWheel:Z

    #@15
    .line 1275
    :cond_15
    return-void

    #@16
    .line 1271
    .end local v0           #wrappingAllowed:Z
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_b
.end method
