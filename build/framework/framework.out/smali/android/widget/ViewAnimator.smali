.class public Landroid/widget/ViewAnimator;
.super Landroid/widget/FrameLayout;
.source "ViewAnimator.java"


# instance fields
.field mAnimateFirstTime:Z

.field mFirstTime:Z

.field mInAnimation:Landroid/view/animation/Animation;

.field mOutAnimation:Landroid/view/animation/Animation;

.field mWhichChild:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@4
    .line 40
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@7
    .line 41
    iput-boolean v1, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@9
    .line 43
    iput-boolean v1, p0, Landroid/widget/ViewAnimator;->mAnimateFirstTime:Z

    #@b
    .line 50
    const/4 v0, 0x0

    #@c
    invoke-direct {p0, p1, v0}, Landroid/widget/ViewAnimator;->initViewAnimator(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@f
    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 40
    iput v4, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@7
    .line 41
    iput-boolean v5, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@9
    .line 43
    iput-boolean v5, p0, Landroid/widget/ViewAnimator;->mAnimateFirstTime:Z

    #@b
    .line 56
    sget-object v3, Lcom/android/internal/R$styleable;->ViewAnimator:[I

    #@d
    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v0

    #@11
    .line 57
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@14
    move-result v2

    #@15
    .line 58
    .local v2, resource:I
    if-lez v2, :cond_1a

    #@17
    .line 59
    invoke-virtual {p0, p1, v2}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/content/Context;I)V

    #@1a
    .line 62
    :cond_1a
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1d
    move-result v2

    #@1e
    .line 63
    if-lez v2, :cond_23

    #@20
    .line 64
    invoke-virtual {p0, p1, v2}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/content/Context;I)V

    #@23
    .line 67
    :cond_23
    const/4 v3, 0x2

    #@24
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@27
    move-result v1

    #@28
    .line 68
    .local v1, flag:Z
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->setAnimateFirstView(Z)V

    #@2b
    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2e
    .line 72
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewAnimator;->initViewAnimator(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@31
    .line 73
    return-void
.end method

.method private initViewAnimator(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 80
    if-nez p2, :cond_6

    #@3
    .line 82
    iput-boolean v3, p0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@5
    .line 94
    :goto_5
    return-void

    #@6
    .line 88
    :cond_6
    sget-object v2, Lcom/android/internal/R$styleable;->FrameLayout:[I

    #@8
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 90
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f
    move-result v1

    #@10
    .line 92
    .local v1, measureAllChildren:Z
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->setMeasureAllChildren(Z)V

    #@13
    .line 93
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@16
    goto :goto_5
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 185
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@6
    move-result v0

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_1c

    #@a
    .line 186
    const/4 v0, 0x0

    #@b
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    #@e
    .line 190
    :goto_e
    if-ltz p2, :cond_1b

    #@10
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@12
    if-lt v0, p2, :cond_1b

    #@14
    .line 192
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@1b
    .line 194
    :cond_1b
    return-void

    #@1c
    .line 188
    :cond_1c
    const/16 v0, 0x8

    #@1e
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    #@21
    goto :goto_e
.end method

.method public getAnimateFirstView()Z
    .registers 2

    #@0
    .prologue
    .line 341
    iget-boolean v0, p0, Landroid/widget/ViewAnimator;->mAnimateFirstTime:Z

    #@2
    return v0
.end method

.method public getBaseline()I
    .registers 2

    #@0
    .prologue
    .line 357
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_f

    #@6
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@d
    move-result v0

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-super {p0}, Landroid/widget/FrameLayout;->getBaseline()I

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public getCurrentView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 254
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->getChildAt(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDisplayedChild()I
    .registers 2

    #@0
    .prologue
    .line 122
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@2
    return v0
.end method

.method public getInAnimation()Landroid/view/animation/Animation;
    .registers 2

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Landroid/widget/ViewAnimator;->mInAnimation:Landroid/view/animation/Animation;

    #@2
    return-object v0
.end method

.method public getOutAnimation()Landroid/view/animation/Animation;
    .registers 2

    #@0
    .prologue
    .line 290
    iget-object v0, p0, Landroid/widget/ViewAnimator;->mOutAnimation:Landroid/view/animation/Animation;

    #@2
    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 362
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 363
    const-class v0, Landroid/widget/ViewAnimator;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 364
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 368
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 369
    const-class v0, Landroid/widget/ViewAnimator;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 370
    return-void
.end method

.method public removeAllViews()V
    .registers 2

    #@0
    .prologue
    .line 198
    invoke-super {p0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@3
    .line 199
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@6
    .line 200
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@9
    .line 201
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 205
    invoke-virtual {p0, p1}, Landroid/widget/ViewAnimator;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v0

    #@4
    .line 206
    .local v0, index:I
    if-ltz v0, :cond_9

    #@6
    .line 207
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->removeViewAt(I)V

    #@9
    .line 209
    :cond_9
    return-void
.end method

.method public removeViewAt(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    #@3
    .line 214
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@6
    move-result v0

    #@7
    .line 215
    .local v0, childCount:I
    if-nez v0, :cond_10

    #@9
    .line 216
    const/4 v1, 0x0

    #@a
    iput v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@c
    .line 217
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@f
    .line 225
    :cond_f
    :goto_f
    return-void

    #@10
    .line 218
    :cond_10
    iget v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@12
    if-lt v1, v0, :cond_1a

    #@14
    .line 220
    add-int/lit8 v1, v0, -0x1

    #@16
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@19
    goto :goto_f

    #@1a
    .line 221
    :cond_1a
    iget v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@1c
    if-ne v1, p1, :cond_f

    #@1e
    .line 223
    iget v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@20
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@23
    goto :goto_f
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 228
    invoke-virtual {p0, p1}, Landroid/widget/ViewAnimator;->removeView(Landroid/view/View;)V

    #@3
    .line 229
    return-void
.end method

.method public removeViews(II)V
    .registers 5
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 232
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->removeViews(II)V

    #@3
    .line 233
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_10

    #@9
    .line 234
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@c
    .line 235
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@f
    .line 240
    :cond_f
    :goto_f
    return-void

    #@10
    .line 236
    :cond_10
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@12
    if-lt v0, p1, :cond_f

    #@14
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@16
    add-int v1, p1, p2

    #@18
    if-ge v0, v1, :cond_f

    #@1a
    .line 238
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@1c
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@1f
    goto :goto_f
.end method

.method public removeViewsInLayout(II)V
    .registers 3
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 243
    invoke-virtual {p0, p1, p2}, Landroid/widget/ViewAnimator;->removeViews(II)V

    #@3
    .line 244
    return-void
.end method

.method public setAnimateFirstView(Z)V
    .registers 2
    .parameter "animate"

    #@0
    .prologue
    .line 352
    iput-boolean p1, p0, Landroid/widget/ViewAnimator;->mAnimateFirstTime:Z

    #@2
    .line 353
    return-void
.end method

.method public setDisplayedChild(I)V
    .registers 4
    .parameter "whichChild"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 103
    iput p1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@3
    .line 104
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@6
    move-result v1

    #@7
    if-lt p1, v1, :cond_1e

    #@9
    .line 105
    iput v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@b
    .line 109
    :cond_b
    :goto_b
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getFocusedChild()Landroid/view/View;

    #@e
    move-result-object v1

    #@f
    if-eqz v1, :cond_12

    #@11
    const/4 v0, 0x1

    #@12
    .line 111
    .local v0, hasFocus:Z
    :cond_12
    iget v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@14
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->showOnly(I)V

    #@17
    .line 112
    if-eqz v0, :cond_1d

    #@19
    .line 114
    const/4 v1, 0x2

    #@1a
    invoke-virtual {p0, v1}, Landroid/widget/ViewAnimator;->requestFocus(I)Z

    #@1d
    .line 116
    :cond_1d
    return-void

    #@1e
    .line 106
    .end local v0           #hasFocus:Z
    :cond_1e
    if-gez p1, :cond_b

    #@20
    .line 107
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@23
    move-result v1

    #@24
    add-int/lit8 v1, v1, -0x1

    #@26
    iput v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@28
    goto :goto_b
.end method

.method public setInAnimation(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 315
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    #@7
    .line 316
    return-void
.end method

.method public setInAnimation(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter "inAnimation"

    #@0
    .prologue
    .line 278
    iput-object p1, p0, Landroid/widget/ViewAnimator;->mInAnimation:Landroid/view/animation/Animation;

    #@2
    .line 279
    return-void
.end method

.method public setOutAnimation(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 328
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    #@7
    .line 329
    return-void
.end method

.method public setOutAnimation(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter "outAnimation"

    #@0
    .prologue
    .line 302
    iput-object p1, p0, Landroid/widget/ViewAnimator;->mOutAnimation:Landroid/view/animation/Animation;

    #@2
    .line 303
    return-void
.end method

.method public showNext()V
    .registers 2
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 130
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@7
    .line 131
    return-void
.end method

.method showOnly(I)V
    .registers 4
    .parameter "childIndex"

    #@0
    .prologue
    .line 178
    iget-boolean v1, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    iget-boolean v1, p0, Landroid/widget/ViewAnimator;->mAnimateFirstTime:Z

    #@6
    if-eqz v1, :cond_d

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    .line 179
    .local v0, animate:Z
    :goto_9
    invoke-virtual {p0, p1, v0}, Landroid/widget/ViewAnimator;->showOnly(IZ)V

    #@c
    .line 180
    return-void

    #@d
    .line 178
    .end local v0           #animate:Z
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_9
.end method

.method showOnly(IZ)V
    .registers 9
    .parameter "childIndex"
    .parameter "animate"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 152
    invoke-virtual {p0}, Landroid/widget/ViewAnimator;->getChildCount()I

    #@4
    move-result v1

    #@5
    .line 153
    .local v1, count:I
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    if-ge v2, v1, :cond_44

    #@8
    .line 154
    invoke-virtual {p0, v2}, Landroid/widget/ViewAnimator;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 155
    .local v0, child:Landroid/view/View;
    if-ne v2, p1, :cond_21

    #@e
    .line 156
    if-eqz p2, :cond_19

    #@10
    iget-object v3, p0, Landroid/widget/ViewAnimator;->mInAnimation:Landroid/view/animation/Animation;

    #@12
    if-eqz v3, :cond_19

    #@14
    .line 157
    iget-object v3, p0, Landroid/widget/ViewAnimator;->mInAnimation:Landroid/view/animation/Animation;

    #@16
    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@19
    .line 159
    :cond_19
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    #@1c
    .line 160
    iput-boolean v5, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@1e
    .line 153
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_6

    #@21
    .line 162
    :cond_21
    if-eqz p2, :cond_38

    #@23
    iget-object v3, p0, Landroid/widget/ViewAnimator;->mOutAnimation:Landroid/view/animation/Animation;

    #@25
    if-eqz v3, :cond_38

    #@27
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_38

    #@2d
    .line 163
    iget-object v3, p0, Landroid/widget/ViewAnimator;->mOutAnimation:Landroid/view/animation/Animation;

    #@2f
    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@32
    .line 166
    :cond_32
    :goto_32
    const/16 v3, 0x8

    #@34
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@37
    goto :goto_1e

    #@38
    .line 164
    :cond_38
    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@3b
    move-result-object v3

    #@3c
    iget-object v4, p0, Landroid/widget/ViewAnimator;->mInAnimation:Landroid/view/animation/Animation;

    #@3e
    if-ne v3, v4, :cond_32

    #@40
    .line 165
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    #@43
    goto :goto_32

    #@44
    .line 169
    .end local v0           #child:Landroid/view/View;
    :cond_44
    return-void
.end method

.method public showPrevious()V
    .registers 2
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 138
    iget v0, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    #@7
    .line 139
    return-void
.end method
