.class public Landroid/widget/TableRow;
.super Landroid/widget/LinearLayout;
.source "TableRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TableRow$1;,
        Landroid/widget/TableRow$ChildrenTracker;,
        Landroid/widget/TableRow$LayoutParams;
    }
.end annotation


# instance fields
.field private mChildrenTracker:Landroid/widget/TableRow$ChildrenTracker;

.field private mColumnToChildIndex:Landroid/util/SparseIntArray;

.field private mColumnWidths:[I

.field private mConstrainedColumnWidths:[I

.field private mNumColumns:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 48
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/widget/TableRow;->mNumColumns:I

    #@6
    .line 62
    invoke-direct {p0}, Landroid/widget/TableRow;->initTableRow()V

    #@9
    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 48
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/widget/TableRow;->mNumColumns:I

    #@6
    .line 74
    invoke-direct {p0}, Landroid/widget/TableRow;->initTableRow()V

    #@9
    .line 75
    return-void
.end method

.method static synthetic access$302(Landroid/widget/TableRow;Landroid/util/SparseIntArray;)Landroid/util/SparseIntArray;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 47
    iput-object p1, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@2
    return-object p1
.end method

.method private initTableRow()V
    .registers 4

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@2
    .line 79
    .local v0, oldListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;
    new-instance v1, Landroid/widget/TableRow$ChildrenTracker;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v1, p0, v2}, Landroid/widget/TableRow$ChildrenTracker;-><init>(Landroid/widget/TableRow;Landroid/widget/TableRow$1;)V

    #@8
    iput-object v1, p0, Landroid/widget/TableRow;->mChildrenTracker:Landroid/widget/TableRow$ChildrenTracker;

    #@a
    .line 80
    if-eqz v0, :cond_11

    #@c
    .line 81
    iget-object v1, p0, Landroid/widget/TableRow;->mChildrenTracker:Landroid/widget/TableRow$ChildrenTracker;

    #@e
    invoke-static {v1, v0}, Landroid/widget/TableRow$ChildrenTracker;->access$100(Landroid/widget/TableRow$ChildrenTracker;Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@11
    .line 83
    :cond_11
    iget-object v1, p0, Landroid/widget/TableRow;->mChildrenTracker:Landroid/widget/TableRow$ChildrenTracker;

    #@13
    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@16
    .line 84
    return-void
.end method

.method private mapIndexAndColumns()V
    .registers 10

    #@0
    .prologue
    .line 155
    iget-object v8, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@2
    if-nez v8, :cond_38

    #@4
    .line 156
    const/4 v6, 0x0

    #@5
    .line 157
    .local v6, virtualCount:I
    invoke-virtual {p0}, Landroid/widget/TableRow;->getChildCount()I

    #@8
    move-result v2

    #@9
    .line 159
    .local v2, count:I
    new-instance v8, Landroid/util/SparseIntArray;

    #@b
    invoke-direct {v8}, Landroid/util/SparseIntArray;-><init>()V

    #@e
    iput-object v8, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@10
    .line 160
    iget-object v1, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@12
    .line 162
    .local v1, columnToChild:Landroid/util/SparseIntArray;
    const/4 v3, 0x0

    #@13
    .local v3, i:I
    :goto_13
    if-ge v3, v2, :cond_36

    #@15
    .line 163
    invoke-virtual {p0, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    .line 164
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Landroid/widget/TableRow$LayoutParams;

    #@1f
    .line 166
    .local v5, layoutParams:Landroid/widget/TableRow$LayoutParams;
    iget v8, v5, Landroid/widget/TableRow$LayoutParams;->column:I

    #@21
    if-lt v8, v6, :cond_25

    #@23
    .line 167
    iget v6, v5, Landroid/widget/TableRow$LayoutParams;->column:I

    #@25
    .line 170
    :cond_25
    const/4 v4, 0x0

    #@26
    .local v4, j:I
    :goto_26
    iget v8, v5, Landroid/widget/TableRow$LayoutParams;->span:I

    #@28
    if-ge v4, v8, :cond_33

    #@2a
    .line 171
    add-int/lit8 v7, v6, 0x1

    #@2c
    .end local v6           #virtualCount:I
    .local v7, virtualCount:I
    invoke-virtual {v1, v6, v3}, Landroid/util/SparseIntArray;->put(II)V

    #@2f
    .line 170
    add-int/lit8 v4, v4, 0x1

    #@31
    move v6, v7

    #@32
    .end local v7           #virtualCount:I
    .restart local v6       #virtualCount:I
    goto :goto_26

    #@33
    .line 162
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_13

    #@36
    .line 175
    .end local v0           #child:Landroid/view/View;
    .end local v4           #j:I
    .end local v5           #layoutParams:Landroid/widget/TableRow$LayoutParams;
    :cond_36
    iput v6, p0, Landroid/widget/TableRow;->mNumColumns:I

    #@38
    .line 177
    .end local v1           #columnToChild:Landroid/util/SparseIntArray;
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v6           #virtualCount:I
    :cond_38
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 371
    instance-of v0, p1, Landroid/widget/TableRow$LayoutParams;

    #@2
    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/widget/TableRow;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 363
    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    #@2
    invoke-direct {v0}, Landroid/widget/TableRow$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-virtual {p0, p1}, Landroid/widget/TableRow;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableRow$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-virtual {p0, p1}, Landroid/widget/TableRow;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-virtual {p0, p1}, Landroid/widget/TableRow;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableRow$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 379
    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/TableRow$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/TableRow$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 353
    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/TableRow;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/TableRow$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method getChildrenSkipCount(Landroid/view/View;I)I
    .registers 5
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 257
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/TableRow$LayoutParams;

    #@6
    .line 260
    .local v0, layoutParams:Landroid/widget/TableRow$LayoutParams;
    iget v1, v0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    return v1
.end method

.method getColumnsWidths(I)[I
    .registers 12
    .parameter "widthMeasureSpec"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 289
    invoke-virtual {p0}, Landroid/widget/TableRow;->getVirtualChildCount()I

    #@4
    move-result v4

    #@5
    .line 290
    .local v4, numColumns:I
    iget-object v7, p0, Landroid/widget/TableRow;->mColumnWidths:[I

    #@7
    if-eqz v7, :cond_e

    #@9
    iget-object v7, p0, Landroid/widget/TableRow;->mColumnWidths:[I

    #@b
    array-length v7, v7

    #@c
    if-eq v4, v7, :cond_12

    #@e
    .line 291
    :cond_e
    new-array v7, v4, [I

    #@10
    iput-object v7, p0, Landroid/widget/TableRow;->mColumnWidths:[I

    #@12
    .line 294
    :cond_12
    iget-object v1, p0, Landroid/widget/TableRow;->mColumnWidths:[I

    #@14
    .line 296
    .local v1, columnWidths:[I
    const/4 v2, 0x0

    #@15
    .local v2, i:I
    :goto_15
    if-ge v2, v4, :cond_61

    #@17
    .line 297
    invoke-virtual {p0, v2}, Landroid/widget/TableRow;->getVirtualChildAt(I)Landroid/view/View;

    #@1a
    move-result-object v0

    #@1b
    .line 298
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_5e

    #@1d
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@20
    move-result v7

    #@21
    const/16 v8, 0x8

    #@23
    if-eq v7, v8, :cond_5e

    #@25
    .line 299
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Landroid/widget/TableRow$LayoutParams;

    #@2b
    .line 300
    .local v3, layoutParams:Landroid/widget/TableRow$LayoutParams;
    iget v7, v3, Landroid/widget/TableRow$LayoutParams;->span:I

    #@2d
    const/4 v8, 0x1

    #@2e
    if-ne v7, v8, :cond_5b

    #@30
    .line 302
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@32
    packed-switch v7, :pswitch_data_62

    #@35
    .line 310
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@37
    const/high16 v8, 0x4000

    #@39
    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3c
    move-result v5

    #@3d
    .line 312
    .local v5, spec:I
    :goto_3d
    invoke-virtual {v0, v5, v5}, Landroid/view/View;->measure(II)V

    #@40
    .line 314
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@43
    move-result v7

    #@44
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@46
    add-int/2addr v7, v8

    #@47
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@49
    add-int v6, v7, v8

    #@4b
    .line 316
    .local v6, width:I
    aput v6, v1, v2

    #@4d
    .line 296
    .end local v3           #layoutParams:Landroid/widget/TableRow$LayoutParams;
    .end local v5           #spec:I
    .end local v6           #width:I
    :goto_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_15

    #@50
    .line 304
    .restart local v3       #layoutParams:Landroid/widget/TableRow$LayoutParams;
    :pswitch_50
    const/4 v7, -0x2

    #@51
    invoke-static {p1, v9, v7}, Landroid/widget/TableRow;->getChildMeasureSpec(III)I

    #@54
    move-result v5

    #@55
    .line 305
    .restart local v5       #spec:I
    goto :goto_3d

    #@56
    .line 307
    .end local v5           #spec:I
    :pswitch_56
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@59
    move-result v5

    #@5a
    .line 308
    .restart local v5       #spec:I
    goto :goto_3d

    #@5b
    .line 318
    .end local v5           #spec:I
    :cond_5b
    aput v9, v1, v2

    #@5d
    goto :goto_4d

    #@5e
    .line 321
    .end local v3           #layoutParams:Landroid/widget/TableRow$LayoutParams;
    :cond_5e
    aput v9, v1, v2

    #@60
    goto :goto_4d

    #@61
    .line 325
    .end local v0           #child:Landroid/view/View;
    :cond_61
    return-object v1

    #@62
    .line 302
    :pswitch_data_62
    .packed-switch -0x2
        :pswitch_50
        :pswitch_56
    .end packed-switch
.end method

.method getLocationOffset(Landroid/view/View;)I
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 268
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/TableRow$LayoutParams;

    #@6
    invoke-static {v0}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x0

    #@b
    aget v0, v0, v1

    #@d
    return v0
.end method

.method getNextLocationOffset(Landroid/view/View;)I
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 276
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/TableRow$LayoutParams;

    #@6
    invoke-static {v0}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x1

    #@b
    aget v0, v0, v1

    #@d
    return v0
.end method

.method public getVirtualChildAt(I)Landroid/view/View;
    .registers 5
    .parameter "i"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 131
    iget-object v1, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@3
    if-nez v1, :cond_8

    #@5
    .line 132
    invoke-direct {p0}, Landroid/widget/TableRow;->mapIndexAndColumns()V

    #@8
    .line 135
    :cond_8
    iget-object v1, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@a
    invoke-virtual {v1, p1, v2}, Landroid/util/SparseIntArray;->get(II)I

    #@d
    move-result v0

    #@e
    .line 136
    .local v0, deflectedIndex:I
    if-eq v0, v2, :cond_15

    #@10
    .line 137
    invoke-virtual {p0, v0}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    #@13
    move-result-object v1

    #@14
    .line 140
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public getVirtualChildCount()I
    .registers 2

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Landroid/widget/TableRow;->mColumnToChildIndex:Landroid/util/SparseIntArray;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 149
    invoke-direct {p0}, Landroid/widget/TableRow;->mapIndexAndColumns()V

    #@7
    .line 151
    :cond_7
    iget v0, p0, Landroid/widget/TableRow;->mNumColumns:I

    #@9
    return v0
.end method

.method measureChildBeforeLayout(Landroid/view/View;IIIII)V
    .registers 26
    .parameter "child"
    .parameter "childIndex"
    .parameter "widthMeasureSpec"
    .parameter "totalWidth"
    .parameter "heightMeasureSpec"
    .parameter "totalHeight"

    #@0
    .prologue
    .line 194
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Landroid/widget/TableRow;->mConstrainedColumnWidths:[I

    #@4
    if-eqz v14, :cond_ac

    #@6
    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9
    move-result-object v11

    #@a
    check-cast v11, Landroid/widget/TableRow$LayoutParams;

    #@c
    .line 197
    .local v11, lp:Landroid/widget/TableRow$LayoutParams;
    const/high16 v12, 0x4000

    #@e
    .line 198
    .local v12, measureMode:I
    const/4 v5, 0x0

    #@f
    .line 200
    .local v5, columnWidth:I
    iget v13, v11, Landroid/widget/TableRow$LayoutParams;->span:I

    #@11
    .line 201
    .local v13, span:I
    move-object/from16 v0, p0

    #@13
    iget-object v6, v0, Landroid/widget/TableRow;->mConstrainedColumnWidths:[I

    #@15
    .line 202
    .local v6, constrainedColumnWidths:[I
    const/4 v8, 0x0

    #@16
    .local v8, i:I
    :goto_16
    if-ge v8, v13, :cond_20

    #@18
    .line 203
    add-int v14, p2, v8

    #@1a
    aget v14, v6, v14

    #@1c
    add-int/2addr v5, v14

    #@1d
    .line 202
    add-int/lit8 v8, v8, 0x1

    #@1f
    goto :goto_16

    #@20
    .line 206
    :cond_20
    iget v7, v11, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@22
    .line 207
    .local v7, gravity:I
    invoke-static {v7}, Landroid/view/Gravity;->isHorizontal(I)Z

    #@25
    move-result v9

    #@26
    .line 209
    .local v9, isHorizontalGravity:Z
    if-eqz v9, :cond_2a

    #@28
    .line 210
    const/high16 v12, -0x8000

    #@2a
    .line 216
    :cond_2a
    const/4 v14, 0x0

    #@2b
    iget v15, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2d
    sub-int v15, v5, v15

    #@2f
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@31
    move/from16 v16, v0

    #@33
    sub-int v15, v15, v16

    #@35
    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    #@38
    move-result v14

    #@39
    invoke-static {v14, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3c
    move-result v4

    #@3d
    .line 219
    .local v4, childWidthMeasureSpec:I
    move-object/from16 v0, p0

    #@3f
    iget v14, v0, Landroid/view/View;->mPaddingTop:I

    #@41
    move-object/from16 v0, p0

    #@43
    iget v15, v0, Landroid/view/View;->mPaddingBottom:I

    #@45
    add-int/2addr v14, v15

    #@46
    iget v15, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@48
    add-int/2addr v14, v15

    #@49
    iget v15, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@4b
    add-int/2addr v14, v15

    #@4c
    add-int v14, v14, p6

    #@4e
    iget v15, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@50
    move/from16 v0, p5

    #@52
    invoke-static {v0, v14, v15}, Landroid/widget/TableRow;->getChildMeasureSpec(III)I

    #@55
    move-result v2

    #@56
    .line 223
    .local v2, childHeightMeasureSpec:I
    move-object/from16 v0, p1

    #@58
    invoke-virtual {v0, v4, v2}, Landroid/view/View;->measure(II)V

    #@5b
    .line 225
    if-eqz v9, :cond_9a

    #@5d
    .line 226
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    #@60
    move-result v3

    #@61
    .line 227
    .local v3, childWidth:I
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@64
    move-result-object v14

    #@65
    const/4 v15, 0x1

    #@66
    sub-int v16, v5, v3

    #@68
    aput v16, v14, v15

    #@6a
    .line 229
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TableRow;->getLayoutDirection()I

    #@6d
    move-result v10

    #@6e
    .line 230
    .local v10, layoutDirection:I
    invoke-static {v7, v10}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@71
    move-result v1

    #@72
    .line 231
    .local v1, absoluteGravity:I
    and-int/lit8 v14, v1, 0x7

    #@74
    packed-switch v14, :pswitch_data_b0

    #@77
    .line 250
    .end local v1           #absoluteGravity:I
    .end local v2           #childHeightMeasureSpec:I
    .end local v3           #childWidth:I
    .end local v4           #childWidthMeasureSpec:I
    .end local v5           #columnWidth:I
    .end local v6           #constrainedColumnWidths:[I
    .end local v7           #gravity:I
    .end local v8           #i:I
    .end local v9           #isHorizontalGravity:Z
    .end local v10           #layoutDirection:I
    .end local v11           #lp:Landroid/widget/TableRow$LayoutParams;
    .end local v12           #measureMode:I
    .end local v13           #span:I
    :goto_77
    :pswitch_77
    return-void

    #@78
    .line 236
    .restart local v1       #absoluteGravity:I
    .restart local v2       #childHeightMeasureSpec:I
    .restart local v3       #childWidth:I
    .restart local v4       #childWidthMeasureSpec:I
    .restart local v5       #columnWidth:I
    .restart local v6       #constrainedColumnWidths:[I
    .restart local v7       #gravity:I
    .restart local v8       #i:I
    .restart local v9       #isHorizontalGravity:Z
    .restart local v10       #layoutDirection:I
    .restart local v11       #lp:Landroid/widget/TableRow$LayoutParams;
    .restart local v12       #measureMode:I
    .restart local v13       #span:I
    :pswitch_78
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@7b
    move-result-object v14

    #@7c
    const/4 v15, 0x0

    #@7d
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@80
    move-result-object v16

    #@81
    const/16 v17, 0x1

    #@83
    aget v16, v16, v17

    #@85
    aput v16, v14, v15

    #@87
    goto :goto_77

    #@88
    .line 239
    :pswitch_88
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@8b
    move-result-object v14

    #@8c
    const/4 v15, 0x0

    #@8d
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@90
    move-result-object v16

    #@91
    const/16 v17, 0x1

    #@93
    aget v16, v16, v17

    #@95
    div-int/lit8 v16, v16, 0x2

    #@97
    aput v16, v14, v15

    #@99
    goto :goto_77

    #@9a
    .line 243
    .end local v1           #absoluteGravity:I
    .end local v3           #childWidth:I
    .end local v10           #layoutDirection:I
    :cond_9a
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@9d
    move-result-object v14

    #@9e
    const/4 v15, 0x0

    #@9f
    invoke-static {v11}, Landroid/widget/TableRow$LayoutParams;->access$200(Landroid/widget/TableRow$LayoutParams;)[I

    #@a2
    move-result-object v16

    #@a3
    const/16 v17, 0x1

    #@a5
    const/16 v18, 0x0

    #@a7
    aput v18, v16, v17

    #@a9
    aput v18, v14, v15

    #@ab
    goto :goto_77

    #@ac
    .line 247
    .end local v2           #childHeightMeasureSpec:I
    .end local v4           #childWidthMeasureSpec:I
    .end local v5           #columnWidth:I
    .end local v6           #constrainedColumnWidths:[I
    .end local v7           #gravity:I
    .end local v8           #i:I
    .end local v9           #isHorizontalGravity:Z
    .end local v11           #lp:Landroid/widget/TableRow$LayoutParams;
    .end local v12           #measureMode:I
    .end local v13           #span:I
    :cond_ac
    invoke-super/range {p0 .. p6}, Landroid/widget/LinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    #@af
    goto :goto_77

    #@b0
    .line 231
    :pswitch_data_b0
    .packed-switch 0x1
        :pswitch_88
        :pswitch_77
        :pswitch_77
        :pswitch_77
        :pswitch_78
    .end packed-switch
.end method

.method measureNullChild(I)I
    .registers 3
    .parameter "childIndex"

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/widget/TableRow;->mConstrainedColumnWidths:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 384
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 385
    const-class v0, Landroid/widget/TableRow;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 386
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 390
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 391
    const-class v0, Landroid/widget/TableRow;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 392
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/widget/TableRow;->layoutHorizontal()V

    #@3
    .line 124
    return-void
.end method

.method protected onMeasure(II)V
    .registers 3
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 114
    invoke-virtual {p0, p1, p2}, Landroid/widget/TableRow;->measureHorizontal(II)V

    #@3
    .line 115
    return-void
.end method

.method setColumnCollapsed(IZ)V
    .registers 5
    .parameter "columnIndex"
    .parameter "collapsed"

    #@0
    .prologue
    .line 102
    invoke-virtual {p0, p1}, Landroid/widget/TableRow;->getVirtualChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 103
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_d

    #@6
    .line 104
    if-eqz p2, :cond_e

    #@8
    const/16 v1, 0x8

    #@a
    :goto_a
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@d
    .line 106
    :cond_d
    return-void

    #@e
    .line 104
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_a
.end method

.method setColumnsWidthConstraints([I)V
    .registers 4
    .parameter "columnWidths"

    #@0
    .prologue
    .line 340
    if-eqz p1, :cond_9

    #@2
    array-length v0, p1

    #@3
    invoke-virtual {p0}, Landroid/widget/TableRow;->getVirtualChildCount()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_11

    #@9
    .line 341
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v1, "columnWidths should be >= getVirtualChildCount()"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 345
    :cond_11
    iput-object p1, p0, Landroid/widget/TableRow;->mConstrainedColumnWidths:[I

    #@13
    .line 346
    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Landroid/widget/TableRow;->mChildrenTracker:Landroid/widget/TableRow$ChildrenTracker;

    #@2
    invoke-static {v0, p1}, Landroid/widget/TableRow$ChildrenTracker;->access$100(Landroid/widget/TableRow$ChildrenTracker;Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@5
    .line 92
    return-void
.end method
