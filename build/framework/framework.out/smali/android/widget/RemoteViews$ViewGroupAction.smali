.class Landroid/widget/RemoteViews$ViewGroupAction;
.super Landroid/widget/RemoteViews$Action;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewGroupAction"
.end annotation


# static fields
.field public static final TAG:I = 0x4


# instance fields
.field nestedViews:Landroid/widget/RemoteViews;

.field final synthetic this$0:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViews;ILandroid/widget/RemoteViews;)V
    .registers 5
    .parameter
    .parameter "viewId"
    .parameter "nestedViews"

    #@0
    .prologue
    .line 1191
    iput-object p1, p0, Landroid/widget/RemoteViews$ViewGroupAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 1192
    iput p2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    .line 1193
    iput-object p3, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@a
    .line 1194
    if-eqz p3, :cond_f

    #@c
    .line 1195
    invoke-static {p1, p3}, Landroid/widget/RemoteViews;->access$300(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    #@f
    .line 1197
    :cond_f
    return-void
.end method

.method public constructor <init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V
    .registers 7
    .parameter
    .parameter "parcel"
    .parameter "bitmapCache"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1199
    iput-object p1, p0, Landroid/widget/RemoteViews$ViewGroupAction;->this$0:Landroid/widget/RemoteViews;

    #@3
    invoke-direct {p0, v2}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 1200
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v1

    #@a
    iput v1, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@c
    .line 1201
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1d

    #@12
    const/4 v0, 0x1

    #@13
    .line 1202
    .local v0, nestedViewsNull:Z
    :goto_13
    if-nez v0, :cond_1f

    #@15
    .line 1203
    new-instance v1, Landroid/widget/RemoteViews;

    #@17
    invoke-direct {v1, p2, p3, v2}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;Landroid/widget/RemoteViews$1;)V

    #@1a
    iput-object v1, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@1c
    .line 1207
    :goto_1c
    return-void

    #@1d
    .line 1201
    .end local v0           #nestedViewsNull:Z
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_13

    #@1f
    .line 1205
    .restart local v0       #nestedViewsNull:Z
    :cond_1f
    iput-object v2, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@21
    goto :goto_1c
.end method


# virtual methods
.method public apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 7
    .parameter "root"
    .parameter "rootParent"
    .parameter "handler"

    #@0
    .prologue
    .line 1223
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 1224
    .local v0, context:Landroid/content/Context;
    iget v2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@6
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/view/ViewGroup;

    #@c
    .line 1225
    .local v1, target:Landroid/view/ViewGroup;
    if-nez v1, :cond_f

    #@e
    .line 1233
    :goto_e
    return-void

    #@f
    .line 1226
    :cond_f
    iget-object v2, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@11
    if-eqz v2, :cond_1d

    #@13
    .line 1228
    iget-object v2, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@15
    invoke-virtual {v2, v0, v1, p3}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@1c
    goto :goto_e

    #@1d
    .line 1231
    :cond_1d
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    #@20
    goto :goto_e
.end method

.method public getActionName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1250
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ViewGroupAction"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    if-nez v0, :cond_1a

    #@17
    const-string v0, "Remove"

    #@19
    :goto_19
    return-object v0

    #@1a
    :cond_1a
    const-string v0, "Add"

    #@1c
    goto :goto_19
.end method

.method public mergeBehavior()I
    .registers 2

    #@0
    .prologue
    .line 1254
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V
    .registers 3
    .parameter "bitmapCache"

    #@0
    .prologue
    .line 1244
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1245
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@6
    invoke-static {v0, p1}, Landroid/widget/RemoteViews;->access$500(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$BitmapCache;)V

    #@9
    .line 1247
    :cond_9
    return-void
.end method

.method public updateMemoryUsageEstimate(Landroid/widget/RemoteViews$MemoryUsageCounter;)V
    .registers 3
    .parameter "counter"

    #@0
    .prologue
    .line 1237
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 1238
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@6
    invoke-virtual {v0}, Landroid/widget/RemoteViews;->estimateMemoryUsage()I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1, v0}, Landroid/widget/RemoteViews$MemoryUsageCounter;->increment(I)V

    #@d
    .line 1240
    :cond_d
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 1210
    const/4 v0, 0x4

    #@1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4
    .line 1211
    iget v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 1212
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@b
    if-eqz v0, :cond_17

    #@d
    .line 1213
    const/4 v0, 0x1

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1214
    iget-object v0, p0, Landroid/widget/RemoteViews$ViewGroupAction;->nestedViews:Landroid/widget/RemoteViews;

    #@13
    invoke-virtual {v0, p1, p2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 1219
    :goto_16
    return-void

    #@17
    .line 1217
    :cond_17
    const/4 v0, 0x0

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    goto :goto_16
.end method
