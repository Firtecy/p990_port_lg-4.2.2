.class Landroid/widget/TimePicker$2;
.super Ljava/lang/Object;
.source "TimePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Landroid/widget/TimePicker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .registers 7
    .parameter "spinner"
    .parameter "oldVal"
    .parameter "newVal"

    #@0
    .prologue
    const/16 v2, 0xc

    #@2
    const/16 v1, 0xb

    #@4
    .line 150
    iget-object v0, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@6
    invoke-static {v0}, Landroid/widget/TimePicker;->access$000(Landroid/widget/TimePicker;)V

    #@9
    .line 151
    iget-object v0, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@b
    invoke-virtual {v0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_2c

    #@11
    .line 152
    if-ne p2, v1, :cond_15

    #@13
    if-eq p3, v2, :cond_19

    #@15
    :cond_15
    if-ne p2, v2, :cond_2c

    #@17
    if-ne p3, v1, :cond_2c

    #@19
    .line 154
    :cond_19
    iget-object v1, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@1b
    iget-object v0, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@1d
    invoke-static {v0}, Landroid/widget/TimePicker;->access$100(Landroid/widget/TimePicker;)Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_32

    #@23
    const/4 v0, 0x1

    #@24
    :goto_24
    invoke-static {v1, v0}, Landroid/widget/TimePicker;->access$102(Landroid/widget/TimePicker;Z)Z

    #@27
    .line 155
    iget-object v0, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@29
    invoke-static {v0}, Landroid/widget/TimePicker;->access$200(Landroid/widget/TimePicker;)V

    #@2c
    .line 158
    :cond_2c
    iget-object v0, p0, Landroid/widget/TimePicker$2;->this$0:Landroid/widget/TimePicker;

    #@2e
    invoke-static {v0}, Landroid/widget/TimePicker;->access$300(Landroid/widget/TimePicker;)V

    #@31
    .line 159
    return-void

    #@32
    .line 154
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_24
.end method
