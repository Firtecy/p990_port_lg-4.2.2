.class public Landroid/widget/RemoteViewsAdapter;
.super Landroid/widget/BaseAdapter;
.source "RemoteViewsAdapter.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;,
        Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;,
        Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;,
        Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;,
        Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;,
        Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;,
        Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;,
        Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;
    }
.end annotation


# static fields
.field private static final REMOTE_VIEWS_CACHE_DURATION:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "RemoteViewsAdapter"

.field private static sCacheRemovalQueue:Landroid/os/Handler; = null

.field private static sCacheRemovalThread:Landroid/os/HandlerThread; = null

.field private static final sCachedRemoteViewsCaches:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;",
            "Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultCacheSize:I = 0x28

.field private static final sDefaultLoadingViewHeight:I = 0x32

.field private static final sDefaultMessageType:I = 0x0

.field private static final sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final sUnbindServiceDelay:I = 0x1388

.field private static final sUnbindServiceMessageType:I = 0x1


# instance fields
.field private final mAppWidgetId:I

.field private mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

.field private mCallback:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDataReady:Z

.field private final mIntent:Landroid/content/Intent;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mMainQueue:Landroid/os/Handler;

.field private mNotifyDataSetChangedAfterOnServiceConnected:Z

.field private mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

.field private mRequestedViews:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

.field private mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

.field mUserId:I

.field private mVisibleWindowLowerBound:I

.field private mVisibleWindowUpperBound:I

.field private mWorkerQueue:Landroid/os/Handler;

.field private mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 95
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@7
    .line 99
    new-instance v0, Ljava/util/HashMap;

    #@9
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c
    sput-object v0, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;)V
    .registers 8
    .parameter "context"
    .parameter "intent"
    .parameter "callback"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 821
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@4
    .line 81
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter;->mNotifyDataSetChangedAfterOnServiceConnected:Z

    #@6
    .line 112
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter;->mDataReady:Z

    #@8
    .line 822
    iput-object p1, p0, Landroid/widget/RemoteViewsAdapter;->mContext:Landroid/content/Context;

    #@a
    .line 823
    iput-object p2, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@c
    .line 824
    const-string/jumbo v1, "remoteAdapterAppWidgetId"

    #@f
    const/4 v2, -0x1

    #@10
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v1

    #@14
    iput v1, p0, Landroid/widget/RemoteViewsAdapter;->mAppWidgetId:I

    #@16
    .line 825
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@1c
    .line 826
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 827
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@22
    const-string v2, "Non-null Intent must be specified."

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 829
    :cond_28
    new-instance v1, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

    #@2a
    invoke-direct {v1, p0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@2d
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mRequestedViews:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

    #@2f
    .line 831
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@32
    move-result v1

    #@33
    const/16 v2, 0x3e8

    #@35
    if-ne v1, v2, :cond_e7

    #@37
    .line 832
    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    #@39
    invoke-direct {v1, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@3c
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@3f
    move-result v1

    #@40
    iput v1, p0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@42
    .line 837
    :goto_42
    const-string/jumbo v1, "remoteAdapterAppWidgetId"

    #@45
    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@48
    move-result v1

    #@49
    if-eqz v1, :cond_51

    #@4b
    .line 838
    const-string/jumbo v1, "remoteAdapterAppWidgetId"

    #@4e
    invoke-virtual {p2, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    #@51
    .line 842
    :cond_51
    new-instance v1, Landroid/os/HandlerThread;

    #@53
    const-string v2, "RemoteViewsCache-loader"

    #@55
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@58
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerThread:Landroid/os/HandlerThread;

    #@5a
    .line 843
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerThread:Landroid/os/HandlerThread;

    #@5c
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@5f
    .line 844
    new-instance v1, Landroid/os/Handler;

    #@61
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerThread:Landroid/os/HandlerThread;

    #@63
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@66
    move-result-object v2

    #@67
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@6a
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerQueue:Landroid/os/Handler;

    #@6c
    .line 845
    new-instance v1, Landroid/os/Handler;

    #@6e
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@71
    move-result-object v2

    #@72
    invoke-direct {v1, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    #@75
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@77
    .line 847
    sget-object v1, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalThread:Landroid/os/HandlerThread;

    #@79
    if-nez v1, :cond_96

    #@7b
    .line 848
    new-instance v1, Landroid/os/HandlerThread;

    #@7d
    const-string v2, "RemoteViewsAdapter-cachePruner"

    #@7f
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@82
    sput-object v1, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalThread:Landroid/os/HandlerThread;

    #@84
    .line 849
    sget-object v1, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalThread:Landroid/os/HandlerThread;

    #@86
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@89
    .line 850
    new-instance v1, Landroid/os/Handler;

    #@8b
    sget-object v2, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalThread:Landroid/os/HandlerThread;

    #@8d
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@90
    move-result-object v2

    #@91
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@94
    sput-object v1, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalQueue:Landroid/os/Handler;

    #@96
    .line 854
    :cond_96
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@98
    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@9b
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCallback:Ljava/lang/ref/WeakReference;

    #@9d
    .line 855
    new-instance v1, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@9f
    invoke-direct {v1, p0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@a2
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@a4
    .line 857
    new-instance v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;

    #@a6
    new-instance v1, Landroid/content/Intent$FilterComparison;

    #@a8
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@aa
    invoke-direct {v1, v2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@ad
    iget v2, p0, Landroid/widget/RemoteViewsAdapter;->mAppWidgetId:I

    #@af
    iget v3, p0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@b1
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;-><init>(Landroid/content/Intent$FilterComparison;II)V

    #@b4
    .line 860
    .local v0, key:Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
    sget-object v2, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@b6
    monitor-enter v2

    #@b7
    .line 861
    :try_start_b7
    sget-object v1, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@b9
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@bc
    move-result v1

    #@bd
    if-eqz v1, :cond_f5

    #@bf
    .line 862
    sget-object v1, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@c1
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c4
    move-result-object v1

    #@c5
    check-cast v1, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@c7
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@c9
    .line 863
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@cb
    invoke-static {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@ce
    move-result-object v3

    #@cf
    monitor-enter v3
    :try_end_d0
    .catchall {:try_start_b7 .. :try_end_d0} :catchall_f2

    #@d0
    .line 864
    :try_start_d0
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@d2
    invoke-static {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@d5
    move-result-object v1

    #@d6
    iget v1, v1, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@d8
    if-lez v1, :cond_dd

    #@da
    .line 867
    const/4 v1, 0x1

    #@db
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter;->mDataReady:Z

    #@dd
    .line 869
    :cond_dd
    monitor-exit v3
    :try_end_de
    .catchall {:try_start_d0 .. :try_end_de} :catchall_ef

    #@de
    .line 873
    :goto_de
    :try_start_de
    iget-boolean v1, p0, Landroid/widget/RemoteViewsAdapter;->mDataReady:Z

    #@e0
    if-nez v1, :cond_e5

    #@e2
    .line 874
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->requestBindService()Z

    #@e5
    .line 876
    :cond_e5
    monitor-exit v2
    :try_end_e6
    .catchall {:try_start_de .. :try_end_e6} :catchall_f2

    #@e6
    .line 877
    return-void

    #@e7
    .line 834
    .end local v0           #key:Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
    :cond_e7
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@ea
    move-result v1

    #@eb
    iput v1, p0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@ed
    goto/16 :goto_42

    #@ef
    .line 869
    .restart local v0       #key:Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
    :catchall_ef
    move-exception v1

    #@f0
    :try_start_f0
    monitor-exit v3
    :try_end_f1
    .catchall {:try_start_f0 .. :try_end_f1} :catchall_ef

    #@f1
    :try_start_f1
    throw v1

    #@f2
    .line 876
    :catchall_f2
    move-exception v1

    #@f3
    monitor-exit v2
    :try_end_f4
    .catchall {:try_start_f1 .. :try_end_f4} :catchall_f2

    #@f4
    throw v1

    #@f5
    .line 871
    :cond_f5
    :try_start_f5
    new-instance v1, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@f7
    const/16 v3, 0x28

    #@f9
    invoke-direct {v1, v3}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;-><init>(I)V

    #@fc
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    :try_end_fe
    .catchall {:try_start_f5 .. :try_end_fe} :catchall_f2

    #@fe
    goto :goto_de
.end method

.method static synthetic access$000(Landroid/widget/RemoteViewsAdapter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-boolean v0, p0, Landroid/widget/RemoteViewsAdapter;->mNotifyDataSetChangedAfterOnServiceConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/widget/RemoteViewsAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->onNotifyDataSetChanged()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Landroid/widget/RemoteViewsAdapter;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerQueue:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/widget/RemoteViewsAdapter;)Landroid/widget/RemoteViews$OnClickHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1400()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1500()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/widget/RemoteViewsAdapter;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/RemoteViewsAdapter;->updateRemoteViews(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/widget/RemoteViewsAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/widget/RemoteViewsAdapter;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mRequestedViews:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/RemoteViewsAdapter;)Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/RemoteViewsAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->updateTemporaryMetaData()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/widget/RemoteViewsAdapter;)Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/RemoteViewsAdapter;)Ljava/lang/ref/WeakReference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCallback:Ljava/lang/ref/WeakReference;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/widget/RemoteViewsAdapter;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/RemoteViewsAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->enqueueDeferredUnbindServiceMessage()V

    #@3
    return-void
.end method

.method private enqueueDeferredUnbindServiceMessage()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1388
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 1389
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@8
    const-wide/16 v1, 0x1388

    #@a
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@d
    .line 1390
    return-void
.end method

.method private getConvertViewTypeId(Landroid/view/View;)I
    .registers 5
    .parameter "convertView"

    #@0
    .prologue
    .line 1127
    const/4 v1, -0x1

    #@1
    .line 1128
    .local v1, typeId:I
    if-eqz p1, :cond_12

    #@3
    .line 1129
    const v2, 0x1020253

    #@6
    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    .line 1130
    .local v0, tag:Ljava/lang/Object;
    if-eqz v0, :cond_12

    #@c
    .line 1131
    check-cast v0, Ljava/lang/Integer;

    #@e
    .end local v0           #tag:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@11
    move-result v1

    #@12
    .line 1134
    :cond_12
    return v1
.end method

.method private getVisibleWindow(III)Ljava/util/ArrayList;
    .registers 7
    .parameter "lower"
    .parameter "upper"
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1318
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1321
    .local v1, window:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez p1, :cond_9

    #@7
    if-eqz p2, :cond_d

    #@9
    :cond_9
    if-ltz p1, :cond_d

    #@b
    if-gez p2, :cond_e

    #@d
    .line 1339
    :cond_d
    return-object v1

    #@e
    .line 1325
    :cond_e
    if-gt p1, p2, :cond_1d

    #@10
    .line 1326
    move v0, p1

    #@11
    .local v0, i:I
    :goto_11
    if-gt v0, p2, :cond_d

    #@13
    .line 1327
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 1326
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_11

    #@1d
    .line 1332
    .end local v0           #i:I
    :cond_1d
    move v0, p1

    #@1e
    .restart local v0       #i:I
    :goto_1e
    if-ge v0, p3, :cond_2a

    #@20
    .line 1333
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 1332
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_1e

    #@2a
    .line 1335
    :cond_2a
    const/4 v0, 0x0

    #@2b
    :goto_2b
    if-gt v0, p2, :cond_d

    #@2d
    .line 1336
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    .line 1335
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_2b
.end method

.method private loadNextIndexInBackground()V
    .registers 3

    #@0
    .prologue
    .line 940
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerQueue:Landroid/os/Handler;

    #@2
    new-instance v1, Landroid/widget/RemoteViewsAdapter$2;

    #@4
    invoke-direct {v1, p0}, Landroid/widget/RemoteViewsAdapter$2;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 963
    return-void
.end method

.method private onNotifyDataSetChanged()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1260
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@3
    invoke-virtual {v6}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->getRemoteViewsFactory()Lcom/android/internal/widget/IRemoteViewsFactory;

    #@6
    move-result-object v1

    #@7
    .line 1262
    .local v1, factory:Lcom/android/internal/widget/IRemoteViewsFactory;
    :try_start_7
    invoke-interface {v1}, Lcom/android/internal/widget/IRemoteViewsFactory;->onDataSetChanged()V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_48
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_a} :catch_66

    #@a
    .line 1275
    iget-object v7, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@c
    monitor-enter v7

    #@d
    .line 1276
    :try_start_d
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@f
    invoke-virtual {v6}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->reset()V

    #@12
    .line 1277
    monitor-exit v7
    :try_end_13
    .catchall {:try_start_d .. :try_end_13} :catchall_84

    #@13
    .line 1280
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->updateTemporaryMetaData()V

    #@16
    .line 1283
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@18
    invoke-virtual {v6}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getTemporaryMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@1b
    move-result-object v7

    #@1c
    monitor-enter v7

    #@1d
    .line 1284
    :try_start_1d
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@1f
    invoke-virtual {v6}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getTemporaryMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@22
    move-result-object v6

    #@23
    iget v4, v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@25
    .line 1285
    .local v4, newCount:I
    iget v6, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowLowerBound:I

    #@27
    iget v8, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowUpperBound:I

    #@29
    invoke-direct {p0, v6, v8, v4}, Landroid/widget/RemoteViewsAdapter;->getVisibleWindow(III)Ljava/util/ArrayList;

    #@2c
    move-result-object v5

    #@2d
    .line 1287
    .local v5, visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    monitor-exit v7
    :try_end_2e
    .catchall {:try_start_1d .. :try_end_2e} :catchall_87

    #@2e
    .line 1292
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@31
    move-result-object v3

    #@32
    .local v3, i$:Ljava/util/Iterator;
    :cond_32
    :goto_32
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@35
    move-result v6

    #@36
    if-eqz v6, :cond_8a

    #@38
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3b
    move-result-object v6

    #@3c
    check-cast v6, Ljava/lang/Integer;

    #@3e
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@41
    move-result v2

    #@42
    .line 1295
    .local v2, i:I
    if-ge v2, v4, :cond_32

    #@44
    .line 1296
    invoke-direct {p0, v2, v9}, Landroid/widget/RemoteViewsAdapter;->updateRemoteViews(IZ)V

    #@47
    goto :goto_32

    #@48
    .line 1263
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #newCount:I
    .end local v5           #visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_48
    move-exception v0

    #@49
    .line 1264
    .local v0, e:Landroid/os/RemoteException;
    const-string v6, "RemoteViewsAdapter"

    #@4b
    new-instance v7, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v8, "Error in updateNotifyDataSetChanged(): "

    #@52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v7

    #@62
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1315
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_65
    return-void

    #@66
    .line 1269
    :catch_66
    move-exception v0

    #@67
    .line 1270
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v6, "RemoteViewsAdapter"

    #@69
    new-instance v7, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v8, "Error in updateNotifyDataSetChanged(): "

    #@70
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v7

    #@74
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v7

    #@80
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_65

    #@84
    .line 1277
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_84
    move-exception v6

    #@85
    :try_start_85
    monitor-exit v7
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_84

    #@86
    throw v6

    #@87
    .line 1287
    :catchall_87
    move-exception v6

    #@88
    :try_start_88
    monitor-exit v7
    :try_end_89
    .catchall {:try_start_88 .. :try_end_89} :catchall_87

    #@89
    throw v6

    #@8a
    .line 1301
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #newCount:I
    .restart local v5       #visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_8a
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@8c
    new-instance v7, Landroid/widget/RemoteViewsAdapter$5;

    #@8e
    invoke-direct {v7, p0}, Landroid/widget/RemoteViewsAdapter$5;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@91
    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@94
    .line 1314
    iput-boolean v9, p0, Landroid/widget/RemoteViewsAdapter;->mNotifyDataSetChangedAfterOnServiceConnected:Z

    #@96
    goto :goto_65
.end method

.method private processException(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 7
    .parameter "method"
    .parameter "e"

    #@0
    .prologue
    .line 966
    const-string v1, "RemoteViewsAdapter"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Error in "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ": "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 970
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@28
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@2b
    move-result-object v0

    #@2c
    .line 971
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v0

    #@2d
    .line 972
    :try_start_2d
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->reset()V

    #@30
    .line 973
    monitor-exit v0
    :try_end_31
    .catchall {:try_start_2d .. :try_end_31} :catchall_45

    #@31
    .line 974
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@33
    monitor-enter v2

    #@34
    .line 975
    :try_start_34
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@36
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->reset()V

    #@39
    .line 976
    monitor-exit v2
    :try_end_3a
    .catchall {:try_start_34 .. :try_end_3a} :catchall_48

    #@3a
    .line 977
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@3c
    new-instance v2, Landroid/widget/RemoteViewsAdapter$3;

    #@3e
    invoke-direct {v2, p0}, Landroid/widget/RemoteViewsAdapter$3;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@41
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@44
    .line 983
    return-void

    #@45
    .line 973
    :catchall_45
    move-exception v1

    #@46
    :try_start_46
    monitor-exit v0
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    #@47
    throw v1

    #@48
    .line 976
    :catchall_48
    move-exception v1

    #@49
    :try_start_49
    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    throw v1
.end method

.method private requestBindService()Z
    .registers 5

    #@0
    .prologue
    .line 1394
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@2
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->isConnected()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_13

    #@8
    .line 1395
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@a
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mContext:Landroid/content/Context;

    #@c
    iget v2, p0, Landroid/widget/RemoteViewsAdapter;->mAppWidgetId:I

    #@e
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->bind(Landroid/content/Context;ILandroid/content/Intent;)V

    #@13
    .line 1399
    :cond_13
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@15
    const/4 v1, 0x1

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@19
    .line 1400
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@1b
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->isConnected()Z

    #@1e
    move-result v0

    #@1f
    return v0
.end method

.method private updateRemoteViews(IZ)V
    .registers 17
    .parameter "position"
    .parameter "notifyWhenLoaded"

    #@0
    .prologue
    .line 1015
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@2
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->getRemoteViewsFactory()Lcom/android/internal/widget/IRemoteViewsFactory;

    #@5
    move-result-object v8

    #@6
    .line 1018
    .local v8, factory:Lcom/android/internal/widget/IRemoteViewsFactory;
    const/4 v2, 0x0

    #@7
    .line 1019
    .local v2, remoteViews:Landroid/widget/RemoteViews;
    const-wide/16 v3, 0x0

    #@9
    .line 1021
    .local v3, itemId:J
    :try_start_9
    invoke-interface {v8, p1}, Lcom/android/internal/widget/IRemoteViewsFactory;->getViewAt(I)Landroid/widget/RemoteViews;

    #@c
    move-result-object v2

    #@d
    .line 1022
    new-instance v0, Landroid/os/UserHandle;

    #@f
    iget v1, p0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@11
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@14
    invoke-virtual {v2, v0}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@17
    .line 1023
    invoke-interface {v8, p1}, Lcom/android/internal/widget/IRemoteViewsFactory;->getItemId(I)J
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_1a} :catch_49
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_1a} :catch_71

    #@1a
    move-result-wide v3

    #@1b
    .line 1035
    if-nez v2, :cond_99

    #@1d
    .line 1039
    const-string v0, "RemoteViewsAdapter"

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v13, "Error in updateRemoteViews("

    #@26
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v13, "): "

    #@30
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v13, " null RemoteViews "

    #@36
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    const-string/jumbo v13, "returned from RemoteViewsFactory."

    #@3d
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1079
    :goto_48
    return-void

    #@49
    .line 1024
    :catch_49
    move-exception v7

    #@4a
    .line 1025
    .local v7, e:Landroid/os/RemoteException;
    const-string v0, "RemoteViewsAdapter"

    #@4c
    new-instance v1, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v13, "Error in updateRemoteViews("

    #@53
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    const-string v13, "): "

    #@5d
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    #@64
    move-result-object v13

    #@65
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    goto :goto_48

    #@71
    .line 1030
    .end local v7           #e:Landroid/os/RemoteException;
    :catch_71
    move-exception v7

    #@72
    .line 1031
    .local v7, e:Ljava/lang/RuntimeException;
    const-string v0, "RemoteViewsAdapter"

    #@74
    new-instance v1, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v13, "Error in updateRemoteViews("

    #@7b
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v1

    #@83
    const-string v13, "): "

    #@85
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@8c
    move-result-object v13

    #@8d
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v1

    #@95
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_48

    #@99
    .line 1044
    .end local v7           #e:Ljava/lang/RuntimeException;
    :cond_99
    invoke-virtual {v2}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@9c
    move-result v9

    #@9d
    .line 1045
    .local v9, layoutId:I
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@9f
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@a2
    move-result-object v10

    #@a3
    .line 1048
    .local v10, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v10

    #@a4
    .line 1049
    :try_start_a4
    invoke-virtual {v10, v9}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->isViewTypeInRange(I)Z

    #@a7
    move-result v12

    #@a8
    .line 1050
    .local v12, viewTypeInRange:Z
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@aa
    invoke-static {v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@ad
    move-result-object v0

    #@ae
    iget v6, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@b0
    .line 1051
    .local v6, cacheCount:I
    monitor-exit v10
    :try_end_b1
    .catchall {:try_start_a4 .. :try_end_b1} :catchall_d7

    #@b1
    .line 1052
    iget-object v13, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@b3
    monitor-enter v13

    #@b4
    .line 1053
    if-eqz v12, :cond_da

    #@b6
    .line 1054
    :try_start_b6
    iget v0, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowLowerBound:I

    #@b8
    iget v1, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowUpperBound:I

    #@ba
    invoke-direct {p0, v0, v1, v6}, Landroid/widget/RemoteViewsAdapter;->getVisibleWindow(III)Ljava/util/ArrayList;

    #@bd
    move-result-object v5

    #@be
    .line 1057
    .local v5, visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@c0
    move v1, p1

    #@c1
    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->insert(ILandroid/widget/RemoteViews;JLjava/util/ArrayList;)V

    #@c4
    .line 1061
    move-object v11, v2

    #@c5
    .line 1062
    .local v11, rv:Landroid/widget/RemoteViews;
    if-eqz p2, :cond_d1

    #@c7
    .line 1063
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@c9
    new-instance v1, Landroid/widget/RemoteViewsAdapter$4;

    #@cb
    invoke-direct {v1, p0, p1, v11}, Landroid/widget/RemoteViewsAdapter$4;-><init>(Landroid/widget/RemoteViewsAdapter;ILandroid/widget/RemoteViews;)V

    #@ce
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@d1
    .line 1078
    .end local v5           #visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11           #rv:Landroid/widget/RemoteViews;
    :cond_d1
    :goto_d1
    monitor-exit v13

    #@d2
    goto/16 :goto_48

    #@d4
    :catchall_d4
    move-exception v0

    #@d5
    monitor-exit v13
    :try_end_d6
    .catchall {:try_start_b6 .. :try_end_d6} :catchall_d4

    #@d6
    throw v0

    #@d7
    .line 1051
    .end local v6           #cacheCount:I
    .end local v12           #viewTypeInRange:Z
    :catchall_d7
    move-exception v0

    #@d8
    :try_start_d8
    monitor-exit v10
    :try_end_d9
    .catchall {:try_start_d8 .. :try_end_d9} :catchall_d7

    #@d9
    throw v0

    #@da
    .line 1075
    .restart local v6       #cacheCount:I
    .restart local v12       #viewTypeInRange:Z
    :cond_da
    :try_start_da
    const-string v0, "RemoteViewsAdapter"

    #@dc
    const-string v1, "Error: widget\'s RemoteViewsFactory returns more view types than  indicated by getViewTypeCount() "

    #@de
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e1
    .catchall {:try_start_da .. :try_end_e1} :catchall_d4

    #@e1
    goto :goto_d1
.end method

.method private updateTemporaryMetaData()V
    .registers 10

    #@0
    .prologue
    .line 986
    iget-object v8, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@2
    invoke-virtual {v8}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->getRemoteViewsFactory()Lcom/android/internal/widget/IRemoteViewsFactory;

    #@5
    move-result-object v2

    #@6
    .line 991
    .local v2, factory:Lcom/android/internal/widget/IRemoteViewsFactory;
    :try_start_6
    invoke-interface {v2}, Lcom/android/internal/widget/IRemoteViewsFactory;->hasStableIds()Z

    #@9
    move-result v4

    #@a
    .line 992
    .local v4, hasStableIds:Z
    invoke-interface {v2}, Lcom/android/internal/widget/IRemoteViewsFactory;->getViewTypeCount()I

    #@d
    move-result v7

    #@e
    .line 993
    .local v7, viewTypeCount:I
    invoke-interface {v2}, Lcom/android/internal/widget/IRemoteViewsFactory;->getCount()I

    #@11
    move-result v0

    #@12
    .line 994
    .local v0, count:I
    invoke-interface {v2}, Lcom/android/internal/widget/IRemoteViewsFactory;->getLoadingView()Landroid/widget/RemoteViews;

    #@15
    move-result-object v5

    #@16
    .line 995
    .local v5, loadingView:Landroid/widget/RemoteViews;
    const/4 v3, 0x0

    #@17
    .line 996
    .local v3, firstView:Landroid/widget/RemoteViews;
    if-lez v0, :cond_20

    #@19
    if-nez v5, :cond_20

    #@1b
    .line 997
    const/4 v8, 0x0

    #@1c
    invoke-interface {v2, v8}, Lcom/android/internal/widget/IRemoteViewsFactory;->getViewAt(I)Landroid/widget/RemoteViews;

    #@1f
    move-result-object v3

    #@20
    .line 999
    :cond_20
    iget-object v8, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@22
    invoke-virtual {v8}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getTemporaryMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@25
    move-result-object v6

    #@26
    .line 1000
    .local v6, tmpMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v6
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_27} :catch_37
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_27} :catch_3f

    #@27
    .line 1001
    :try_start_27
    iput-boolean v4, v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->hasStableIds:Z

    #@29
    .line 1003
    add-int/lit8 v8, v7, 0x1

    #@2b
    iput v8, v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@2d
    .line 1004
    iput v0, v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@2f
    .line 1005
    invoke-virtual {v6, v5, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->setLoadingViewTemplates(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    #@32
    .line 1006
    monitor-exit v6

    #@33
    .line 1012
    .end local v0           #count:I
    .end local v3           #firstView:Landroid/widget/RemoteViews;
    .end local v4           #hasStableIds:Z
    .end local v5           #loadingView:Landroid/widget/RemoteViews;
    .end local v6           #tmpMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .end local v7           #viewTypeCount:I
    :goto_33
    return-void

    #@34
    .line 1006
    .restart local v0       #count:I
    .restart local v3       #firstView:Landroid/widget/RemoteViews;
    .restart local v4       #hasStableIds:Z
    .restart local v5       #loadingView:Landroid/widget/RemoteViews;
    .restart local v6       #tmpMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .restart local v7       #viewTypeCount:I
    :catchall_34
    move-exception v8

    #@35
    monitor-exit v6
    :try_end_36
    .catchall {:try_start_27 .. :try_end_36} :catchall_34

    #@36
    :try_start_36
    throw v8
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_37} :catch_37
    .catch Ljava/lang/RuntimeException; {:try_start_36 .. :try_end_37} :catch_3f

    #@37
    .line 1007
    .end local v0           #count:I
    .end local v3           #firstView:Landroid/widget/RemoteViews;
    .end local v4           #hasStableIds:Z
    .end local v5           #loadingView:Landroid/widget/RemoteViews;
    .end local v6           #tmpMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .end local v7           #viewTypeCount:I
    :catch_37
    move-exception v1

    #@38
    .line 1008
    .local v1, e:Landroid/os/RemoteException;
    const-string/jumbo v8, "updateMetaData"

    #@3b
    invoke-direct {p0, v8, v1}, Landroid/widget/RemoteViewsAdapter;->processException(Ljava/lang/String;Ljava/lang/Exception;)V

    #@3e
    goto :goto_33

    #@3f
    .line 1009
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_3f
    move-exception v1

    #@40
    .line 1010
    .local v1, e:Ljava/lang/RuntimeException;
    const-string/jumbo v8, "updateMetaData"

    #@43
    invoke-direct {p0, v8, v1}, Landroid/widget/RemoteViewsAdapter;->processException(Ljava/lang/String;Ljava/lang/Exception;)V

    #@46
    goto :goto_33
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 882
    :try_start_0
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerThread:Landroid/os/HandlerThread;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 883
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerThread:Landroid/os/HandlerThread;

    #@6
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_d

    #@9
    .line 886
    :cond_9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    .line 888
    return-void

    #@d
    .line 886
    :catchall_d
    move-exception v0

    #@e
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@11
    throw v0
.end method

.method public getCount()I
    .registers 3

    #@0
    .prologue
    .line 1086
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@2
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@5
    move-result-object v0

    #@6
    .line 1087
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v0

    #@7
    .line 1088
    :try_start_7
    iget v1, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@9
    monitor-exit v0

    #@a
    return v1

    #@b
    .line 1089
    :catchall_b
    move-exception v1

    #@c
    monitor-exit v0
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1094
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getItemId(I)J
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 1098
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@2
    monitor-enter v2

    #@3
    .line 1099
    :try_start_3
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@5
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->containsMetaDataAt(I)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_15

    #@b
    .line 1100
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@d
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaDataAt(I)Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@10
    move-result-object v0

    #@11
    iget-wide v0, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->itemId:J

    #@13
    monitor-exit v2

    #@14
    .line 1102
    :goto_14
    return-wide v0

    #@15
    :cond_15
    const-wide/16 v0, 0x0

    #@17
    monitor-exit v2

    #@18
    goto :goto_14

    #@19
    .line 1103
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public getItemViewType(I)I
    .registers 6
    .parameter "position"

    #@0
    .prologue
    .line 1107
    const/4 v1, 0x0

    #@1
    .line 1108
    .local v1, typeId:I
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@3
    monitor-enter v3

    #@4
    .line 1109
    :try_start_4
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@6
    invoke-virtual {v2, p1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->containsMetaDataAt(I)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_22

    #@c
    .line 1110
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@e
    invoke-virtual {v2, p1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaDataAt(I)Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@11
    move-result-object v2

    #@12
    iget v1, v2, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->typeId:I

    #@14
    .line 1114
    monitor-exit v3
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_25

    #@15
    .line 1116
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@17
    invoke-virtual {v2}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@1a
    move-result-object v0

    #@1b
    .line 1117
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v0

    #@1c
    .line 1118
    :try_start_1c
    invoke-virtual {v0, v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->getMappedViewType(I)I

    #@1f
    move-result v2

    #@20
    monitor-exit v0
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_28

    #@21
    .end local v0           #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    :goto_21
    return v2

    #@22
    .line 1112
    :cond_22
    const/4 v2, 0x0

    #@23
    :try_start_23
    monitor-exit v3

    #@24
    goto :goto_21

    #@25
    .line 1114
    :catchall_25
    move-exception v2

    #@26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_23 .. :try_end_27} :catchall_25

    #@27
    throw v2

    #@28
    .line 1119
    .restart local v0       #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    :catchall_28
    move-exception v2

    #@29
    :try_start_29
    monitor-exit v0
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v2
.end method

.method public getRemoteViewsServiceIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 1082
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 28
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 1150
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@4
    move-object/from16 v23, v0

    #@6
    monitor-enter v23

    #@7
    .line 1151
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@b
    move/from16 v0, p1

    #@d
    invoke-virtual {v3, v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->containsRemoteViewAt(I)Z

    #@10
    move-result v16

    #@11
    .line 1152
    .local v16, isInCache:Z
    move-object/from16 v0, p0

    #@13
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@15
    invoke-virtual {v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->isConnected()Z

    #@18
    move-result v15

    #@19
    .line 1153
    .local v15, isConnected:Z
    const/4 v13, 0x0

    #@1a
    .line 1155
    .local v13, hasNewItems:Z
    if-eqz p2, :cond_2e

    #@1c
    move-object/from16 v0, p2

    #@1e
    instance-of v3, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@20
    if-eqz v3, :cond_2e

    #@22
    .line 1156
    move-object/from16 v0, p0

    #@24
    iget-object v4, v0, Landroid/widget/RemoteViewsAdapter;->mRequestedViews:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

    #@26
    move-object/from16 v0, p2

    #@28
    check-cast v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@2a
    move-object v3, v0

    #@2b
    invoke-virtual {v4, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;->removeView(Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;)V

    #@2e
    .line 1159
    :cond_2e
    if-nez v16, :cond_8a

    #@30
    if-nez v15, :cond_8a

    #@32
    .line 1162
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->requestBindService()Z

    #@35
    .line 1168
    :goto_35
    if-eqz v16, :cond_132

    #@37
    .line 1169
    const/4 v10, 0x0

    #@38
    .line 1170
    .local v10, convertViewChild:Landroid/view/View;
    const/4 v11, 0x0

    #@39
    .line 1171
    .local v11, convertViewTypeId:I
    const/16 v17, 0x0

    #@3b
    .line 1173
    .local v17, layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    move-object/from16 v0, p2

    #@3d
    instance-of v3, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@3f
    if-eqz v3, :cond_178

    #@41
    .line 1174
    move-object/from16 v0, p2

    #@43
    check-cast v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@45
    move-object/from16 v17, v0

    #@47
    .line 1175
    const/4 v3, 0x0

    #@48
    move-object/from16 v0, v17

    #@4a
    invoke-virtual {v0, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;->getChildAt(I)Landroid/view/View;

    #@4d
    move-result-object v10

    #@4e
    .line 1176
    move-object/from16 v0, p0

    #@50
    invoke-direct {v0, v10}, Landroid/widget/RemoteViewsAdapter;->getConvertViewTypeId(Landroid/view/View;)I

    #@53
    move-result v11

    #@54
    move-object/from16 v18, v17

    #@56
    .line 1181
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .local v18, layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :goto_56
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@59
    move-result-object v9

    #@5a
    .line 1182
    .local v9, context:Landroid/content/Context;
    move-object/from16 v0, p0

    #@5c
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@5e
    move/from16 v0, p1

    #@60
    invoke-virtual {v3, v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getRemoteViewsAt(I)Landroid/widget/RemoteViews;

    #@63
    move-result-object v21

    #@64
    .line 1183
    .local v21, rv:Landroid/widget/RemoteViews;
    move-object/from16 v0, p0

    #@66
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@68
    move/from16 v0, p1

    #@6a
    invoke-virtual {v3, v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaDataAt(I)Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@6d
    move-result-object v14

    #@6e
    .line 1184
    .local v14, indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    iget v0, v14, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->typeId:I

    #@70
    move/from16 v22, v0
    :try_end_72
    .catchall {:try_start_7 .. :try_end_72} :catchall_125

    #@72
    .line 1188
    .local v22, typeId:I
    if-eqz v18, :cond_c5

    #@74
    .line 1189
    move/from16 v0, v22

    #@76
    if-ne v11, v0, :cond_95

    #@78
    .line 1190
    :try_start_78
    move-object/from16 v0, p0

    #@7a
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@7c
    move-object/from16 v0, v21

    #@7e
    invoke-virtual {v0, v9, v10, v3}, Landroid/widget/RemoteViews;->reapply(Landroid/content/Context;Landroid/view/View;Landroid/widget/RemoteViews$OnClickHandler;)V
    :try_end_81
    .catchall {:try_start_78 .. :try_end_81} :catchall_171
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_81} :catch_cd

    #@81
    .line 1219
    if-eqz v13, :cond_86

    #@83
    :try_start_83
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@86
    .line 1191
    :cond_86
    monitor-exit v23

    #@87
    move-object/from16 v19, v18

    #@89
    .line 1235
    .end local v9           #context:Landroid/content/Context;
    .end local v10           #convertViewChild:Landroid/view/View;
    .end local v11           #convertViewTypeId:I
    .end local v14           #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .end local v18           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .end local v21           #rv:Landroid/widget/RemoteViews;
    .end local v22           #typeId:I
    :goto_89
    return-object v19

    #@8a
    .line 1165
    :cond_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@8e
    move/from16 v0, p1

    #@90
    invoke-virtual {v3, v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->queuePositionsToBePreloadedFromRequestedPosition(I)Z
    :try_end_93
    .catchall {:try_start_83 .. :try_end_93} :catchall_125

    #@93
    move-result v13

    #@94
    goto :goto_35

    #@95
    .line 1193
    .restart local v9       #context:Landroid/content/Context;
    .restart local v10       #convertViewChild:Landroid/view/View;
    .restart local v11       #convertViewTypeId:I
    .restart local v14       #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .restart local v18       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v21       #rv:Landroid/widget/RemoteViews;
    .restart local v22       #typeId:I
    :cond_95
    :try_start_95
    invoke-virtual/range {v18 .. v18}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;->removeAllViews()V
    :try_end_98
    .catchall {:try_start_95 .. :try_end_98} :catchall_171
    .catch Ljava/lang/Exception; {:try_start_95 .. :try_end_98} :catch_cd

    #@98
    move-object/from16 v17, v18

    #@9a
    .line 1199
    .end local v18           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v17       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :goto_9a
    :try_start_9a
    move-object/from16 v0, p0

    #@9c
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@9e
    move-object/from16 v0, v21

    #@a0
    move-object/from16 v1, p3

    #@a2
    invoke-virtual {v0, v9, v1, v3}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;

    #@a5
    move-result-object v20

    #@a6
    .line 1200
    .local v20, newView:Landroid/view/View;
    const v3, 0x1020253

    #@a9
    new-instance v4, Ljava/lang/Integer;

    #@ab
    move/from16 v0, v22

    #@ad
    invoke-direct {v4, v0}, Ljava/lang/Integer;-><init>(I)V

    #@b0
    move-object/from16 v0, v20

    #@b2
    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setTagInternal(ILjava/lang/Object;)V

    #@b5
    .line 1202
    move-object/from16 v0, v17

    #@b7
    move-object/from16 v1, v20

    #@b9
    invoke-virtual {v0, v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;->addView(Landroid/view/View;)V
    :try_end_bc
    .catchall {:try_start_9a .. :try_end_bc} :catchall_12b
    .catch Ljava/lang/Exception; {:try_start_9a .. :try_end_bc} :catch_175

    #@bc
    .line 1219
    if-eqz v13, :cond_c1

    #@be
    :try_start_be
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@c1
    .line 1203
    :cond_c1
    monitor-exit v23
    :try_end_c2
    .catchall {:try_start_be .. :try_end_c2} :catchall_125

    #@c2
    move-object/from16 v19, v17

    #@c4
    goto :goto_89

    #@c5
    .line 1195
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .end local v20           #newView:Landroid/view/View;
    .restart local v18       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :cond_c5
    :try_start_c5
    new-instance v17, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@c7
    move-object/from16 v0, v17

    #@c9
    invoke-direct {v0, v9}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;-><init>(Landroid/content/Context;)V
    :try_end_cc
    .catchall {:try_start_c5 .. :try_end_cc} :catchall_171
    .catch Ljava/lang/Exception; {:try_start_c5 .. :try_end_cc} :catch_cd

    #@cc
    .end local v18           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v17       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    goto :goto_9a

    #@cd
    .line 1205
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v18       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :catch_cd
    move-exception v12

    #@ce
    move-object/from16 v17, v18

    #@d0
    .line 1208
    .end local v18           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .local v12, e:Ljava/lang/Exception;
    .restart local v17       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :goto_d0
    :try_start_d0
    const-string v3, "RemoteViewsAdapter"

    #@d2
    new-instance v4, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v5, "Error inflating RemoteViews at position: "

    #@d9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v4

    #@dd
    move/from16 v0, p1

    #@df
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v4

    #@e3
    const-string v5, ", using"

    #@e5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v4

    #@e9
    const-string/jumbo v5, "loading view instead"

    #@ec
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v4

    #@f0
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v4

    #@f4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v4

    #@f8
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fb
    .line 1211
    const/16 v19, 0x0

    #@fd
    .line 1212
    .local v19, loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    move-object/from16 v0, p0

    #@ff
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@101
    invoke-virtual {v3}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@104
    move-result-object v2

    #@105
    .line 1213
    .local v2, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v2
    :try_end_106
    .catchall {:try_start_d0 .. :try_end_106} :catchall_12b

    #@106
    .line 1214
    :try_start_106
    move-object/from16 v0, p0

    #@108
    iget-object v6, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v7, v0, Landroid/widget/RemoteViewsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-object v8, v0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@112
    move/from16 v3, p1

    #@114
    move-object/from16 v4, p2

    #@116
    move-object/from16 v5, p3

    #@118
    #calls: Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->createLoadingView(ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    invoke-static/range {v2 .. v8}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->access$1900(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@11b
    move-result-object v19

    #@11c
    .line 1216
    monitor-exit v2
    :try_end_11d
    .catchall {:try_start_106 .. :try_end_11d} :catchall_128

    #@11d
    .line 1219
    if-eqz v13, :cond_122

    #@11f
    :try_start_11f
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@122
    .line 1217
    :cond_122
    monitor-exit v23

    #@123
    goto/16 :goto_89

    #@125
    .line 1237
    .end local v2           #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .end local v9           #context:Landroid/content/Context;
    .end local v10           #convertViewChild:Landroid/view/View;
    .end local v11           #convertViewTypeId:I
    .end local v12           #e:Ljava/lang/Exception;
    .end local v13           #hasNewItems:Z
    .end local v14           #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .end local v15           #isConnected:Z
    .end local v16           #isInCache:Z
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .end local v19           #loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .end local v21           #rv:Landroid/widget/RemoteViews;
    .end local v22           #typeId:I
    :catchall_125
    move-exception v3

    #@126
    monitor-exit v23
    :try_end_127
    .catchall {:try_start_11f .. :try_end_127} :catchall_125

    #@127
    throw v3

    #@128
    .line 1216
    .restart local v2       #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .restart local v9       #context:Landroid/content/Context;
    .restart local v10       #convertViewChild:Landroid/view/View;
    .restart local v11       #convertViewTypeId:I
    .restart local v12       #e:Ljava/lang/Exception;
    .restart local v13       #hasNewItems:Z
    .restart local v14       #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .restart local v15       #isConnected:Z
    .restart local v16       #isInCache:Z
    .restart local v17       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v19       #loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v21       #rv:Landroid/widget/RemoteViews;
    .restart local v22       #typeId:I
    :catchall_128
    move-exception v3

    #@129
    :try_start_129
    monitor-exit v2
    :try_end_12a
    .catchall {:try_start_129 .. :try_end_12a} :catchall_128

    #@12a
    :try_start_12a
    throw v3
    :try_end_12b
    .catchall {:try_start_12a .. :try_end_12b} :catchall_12b

    #@12b
    .line 1219
    .end local v2           #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .end local v12           #e:Ljava/lang/Exception;
    .end local v19           #loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    :catchall_12b
    move-exception v3

    #@12c
    :goto_12c
    if-eqz v13, :cond_131

    #@12e
    :try_start_12e
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@131
    :cond_131
    throw v3

    #@132
    .line 1224
    .end local v9           #context:Landroid/content/Context;
    .end local v10           #convertViewChild:Landroid/view/View;
    .end local v11           #convertViewTypeId:I
    .end local v14           #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .end local v21           #rv:Landroid/widget/RemoteViews;
    .end local v22           #typeId:I
    :cond_132
    const/16 v19, 0x0

    #@134
    .line 1225
    .restart local v19       #loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    move-object/from16 v0, p0

    #@136
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@138
    invoke-virtual {v3}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@13b
    move-result-object v2

    #@13c
    .line 1226
    .restart local v2       #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v2
    :try_end_13d
    .catchall {:try_start_12e .. :try_end_13d} :catchall_125

    #@13d
    .line 1227
    :try_start_13d
    move-object/from16 v0, p0

    #@13f
    iget-object v6, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v7, v0, Landroid/widget/RemoteViewsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    #@145
    move-object/from16 v0, p0

    #@147
    iget-object v8, v0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@149
    move/from16 v3, p1

    #@14b
    move-object/from16 v4, p2

    #@14d
    move-object/from16 v5, p3

    #@14f
    #calls: Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->createLoadingView(ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    invoke-static/range {v2 .. v8}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->access$1900(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;ILandroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;Landroid/view/LayoutInflater;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;

    #@152
    move-result-object v19

    #@153
    .line 1229
    monitor-exit v2
    :try_end_154
    .catchall {:try_start_13d .. :try_end_154} :catchall_16e

    #@154
    .line 1231
    :try_start_154
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mRequestedViews:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;

    #@158
    move/from16 v0, p1

    #@15a
    move-object/from16 v1, v19

    #@15c
    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayoutRefSet;->add(ILandroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;)V

    #@15f
    .line 1232
    move-object/from16 v0, p0

    #@161
    iget-object v3, v0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@163
    move/from16 v0, p1

    #@165
    invoke-virtual {v3, v0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->queueRequestedPositionToLoad(I)V

    #@168
    .line 1233
    invoke-direct/range {p0 .. p0}, Landroid/widget/RemoteViewsAdapter;->loadNextIndexInBackground()V

    #@16b
    .line 1235
    monitor-exit v23
    :try_end_16c
    .catchall {:try_start_154 .. :try_end_16c} :catchall_125

    #@16c
    goto/16 :goto_89

    #@16e
    .line 1229
    :catchall_16e
    move-exception v3

    #@16f
    :try_start_16f
    monitor-exit v2
    :try_end_170
    .catchall {:try_start_16f .. :try_end_170} :catchall_16e

    #@170
    :try_start_170
    throw v3
    :try_end_171
    .catchall {:try_start_170 .. :try_end_171} :catchall_125

    #@171
    .line 1219
    .end local v2           #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .end local v19           #loadingView:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v9       #context:Landroid/content/Context;
    .restart local v10       #convertViewChild:Landroid/view/View;
    .restart local v11       #convertViewTypeId:I
    .restart local v14       #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .restart local v18       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v21       #rv:Landroid/widget/RemoteViews;
    .restart local v22       #typeId:I
    :catchall_171
    move-exception v3

    #@172
    move-object/from16 v17, v18

    #@174
    .end local v18           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v17       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    goto :goto_12c

    #@175
    .line 1205
    :catch_175
    move-exception v12

    #@176
    goto/16 :goto_d0

    #@178
    .end local v9           #context:Landroid/content/Context;
    .end local v14           #indexMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .end local v21           #rv:Landroid/widget/RemoteViews;
    .end local v22           #typeId:I
    :cond_178
    move-object/from16 v18, v17

    #@17a
    .end local v17           #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    .restart local v18       #layout:Landroid/widget/RemoteViewsAdapter$RemoteViewsFrameLayout;
    goto/16 :goto_56
.end method

.method public getViewTypeCount()I
    .registers 3

    #@0
    .prologue
    .line 1241
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@2
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@5
    move-result-object v0

    #@6
    .line 1242
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v0

    #@7
    .line 1243
    :try_start_7
    iget v1, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->viewTypeCount:I

    #@9
    monitor-exit v0

    #@a
    return v1

    #@b
    .line 1244
    :catchall_b
    move-exception v1

    #@c
    monitor-exit v0
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v1
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 1372
    const/4 v0, 0x0

    #@1
    .line 1373
    .local v0, result:Z
    iget v1, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v1, :pswitch_data_1c

    #@6
    .line 1383
    :goto_6
    return v0

    #@7
    .line 1375
    :pswitch_7
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@9
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->isConnected()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1a

    #@f
    .line 1376
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@11
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter;->mContext:Landroid/content/Context;

    #@13
    iget v3, p0, Landroid/widget/RemoteViewsAdapter;->mAppWidgetId:I

    #@15
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@17
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->unbind(Landroid/content/Context;ILandroid/content/Intent;)V

    #@1a
    .line 1378
    :cond_1a
    const/4 v0, 0x1

    #@1b
    .line 1379
    goto :goto_6

    #@1c
    .line 1373
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method public hasStableIds()Z
    .registers 3

    #@0
    .prologue
    .line 1248
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@2
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@5
    move-result-object v0

    #@6
    .line 1249
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    monitor-enter v0

    #@7
    .line 1250
    :try_start_7
    iget-boolean v1, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->hasStableIds:Z

    #@9
    monitor-exit v0

    #@a
    return v1

    #@b
    .line 1251
    :catchall_b
    move-exception v1

    #@c
    monitor-exit v0
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v1
.end method

.method public isDataReady()Z
    .registers 2

    #@0
    .prologue
    .line 891
    iget-boolean v0, p0, Landroid/widget/RemoteViewsAdapter;->mDataReady:Z

    #@2
    return v0
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 1255
    invoke-virtual {p0}, Landroid/widget/RemoteViewsAdapter;->getCount()I

    #@3
    move-result v0

    #@4
    if-gtz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public notifyDataSetChanged()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1344
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 1348
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mServiceConnection:Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;

    #@8
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->isConnected()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_19

    #@e
    .line 1349
    iget-boolean v0, p0, Landroid/widget/RemoteViewsAdapter;->mNotifyDataSetChangedAfterOnServiceConnected:Z

    #@10
    if-eqz v0, :cond_13

    #@12
    .line 1364
    :goto_12
    return-void

    #@13
    .line 1353
    :cond_13
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter;->mNotifyDataSetChangedAfterOnServiceConnected:Z

    #@15
    .line 1354
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter;->requestBindService()Z

    #@18
    goto :goto_12

    #@19
    .line 1358
    :cond_19
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter;->mWorkerQueue:Landroid/os/Handler;

    #@1b
    new-instance v1, Landroid/widget/RemoteViewsAdapter$6;

    #@1d
    invoke-direct {v1, p0}, Landroid/widget/RemoteViewsAdapter$6;-><init>(Landroid/widget/RemoteViewsAdapter;)V

    #@20
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@23
    goto :goto_12
.end method

.method public saveRemoteViewsCache()V
    .registers 9

    #@0
    .prologue
    .line 899
    new-instance v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;

    #@2
    new-instance v4, Landroid/content/Intent$FilterComparison;

    #@4
    iget-object v5, p0, Landroid/widget/RemoteViewsAdapter;->mIntent:Landroid/content/Intent;

    #@6
    invoke-direct {v4, v5}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@9
    iget v5, p0, Landroid/widget/RemoteViewsAdapter;->mAppWidgetId:I

    #@b
    iget v6, p0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@d
    invoke-direct {v0, v4, v5, v6}, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;-><init>(Landroid/content/Intent$FilterComparison;II)V

    #@10
    .line 902
    .local v0, key:Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
    sget-object v5, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@12
    monitor-enter v5

    #@13
    .line 904
    :try_start_13
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@15
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_2d

    #@1b
    .line 905
    sget-object v6, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalQueue:Landroid/os/Handler;

    #@1d
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v4

    #@23
    check-cast v4, Ljava/lang/Runnable;

    #@25
    invoke-virtual {v6, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@28
    .line 906
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 909
    :cond_2d
    const/4 v1, 0x0

    #@2e
    .line 910
    .local v1, metaDataCount:I
    const/4 v2, 0x0

    #@2f
    .line 911
    .local v2, numRemoteViewsCached:I
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@31
    invoke-static {v4}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@34
    move-result-object v6

    #@35
    monitor-enter v6
    :try_end_36
    .catchall {:try_start_13 .. :try_end_36} :catchall_6e

    #@36
    .line 912
    :try_start_36
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@38
    invoke-static {v4}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@3b
    move-result-object v4

    #@3c
    iget v1, v4, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@3e
    .line 913
    monitor-exit v6
    :try_end_3f
    .catchall {:try_start_36 .. :try_end_3f} :catchall_6b

    #@3f
    .line 914
    :try_start_3f
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@41
    monitor-enter v6
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_6e

    #@42
    .line 915
    :try_start_42
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@44
    invoke-static {v4}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->access$1300(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Ljava/util/HashMap;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@4b
    move-result v2

    #@4c
    .line 916
    monitor-exit v6
    :try_end_4d
    .catchall {:try_start_42 .. :try_end_4d} :catchall_71

    #@4d
    .line 917
    if-lez v1, :cond_58

    #@4f
    if-lez v2, :cond_58

    #@51
    .line 918
    :try_start_51
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sCachedRemoteViewsCaches:Ljava/util/HashMap;

    #@53
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter;->mCache:Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;

    #@55
    invoke-virtual {v4, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    .line 921
    :cond_58
    new-instance v3, Landroid/widget/RemoteViewsAdapter$1;

    #@5a
    invoke-direct {v3, p0, v0}, Landroid/widget/RemoteViewsAdapter$1;-><init>(Landroid/widget/RemoteViewsAdapter;Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;)V

    #@5d
    .line 934
    .local v3, r:Ljava/lang/Runnable;
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sRemoteViewsCacheRemoveRunnables:Ljava/util/HashMap;

    #@5f
    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    .line 935
    sget-object v4, Landroid/widget/RemoteViewsAdapter;->sCacheRemovalQueue:Landroid/os/Handler;

    #@64
    const-wide/16 v6, 0x1388

    #@66
    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@69
    .line 936
    monitor-exit v5
    :try_end_6a
    .catchall {:try_start_51 .. :try_end_6a} :catchall_6e

    #@6a
    .line 937
    return-void

    #@6b
    .line 913
    .end local v3           #r:Ljava/lang/Runnable;
    :catchall_6b
    move-exception v4

    #@6c
    :try_start_6c
    monitor-exit v6
    :try_end_6d
    .catchall {:try_start_6c .. :try_end_6d} :catchall_6b

    #@6d
    :try_start_6d
    throw v4

    #@6e
    .line 936
    .end local v1           #metaDataCount:I
    .end local v2           #numRemoteViewsCached:I
    :catchall_6e
    move-exception v4

    #@6f
    monitor-exit v5
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_6e

    #@70
    throw v4

    #@71
    .line 916
    .restart local v1       #metaDataCount:I
    .restart local v2       #numRemoteViewsCached:I
    :catchall_71
    move-exception v4

    #@72
    :try_start_72
    monitor-exit v6
    :try_end_73
    .catchall {:try_start_72 .. :try_end_73} :catchall_71

    #@73
    :try_start_73
    throw v4
    :try_end_74
    .catchall {:try_start_73 .. :try_end_74} :catchall_6e
.end method

.method public setRemoteViewsOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 2
    .parameter "handler"

    #@0
    .prologue
    .line 895
    iput-object p1, p0, Landroid/widget/RemoteViewsAdapter;->mRemoteViewsOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@2
    .line 896
    return-void
.end method

.method public setVisibleRangeHint(II)V
    .registers 3
    .parameter "lowerBound"
    .parameter "upperBound"

    #@0
    .prologue
    .line 1143
    iput p1, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowLowerBound:I

    #@2
    .line 1144
    iput p2, p0, Landroid/widget/RemoteViewsAdapter;->mVisibleWindowUpperBound:I

    #@4
    .line 1145
    return-void
.end method

.method superNotifyDataSetChanged()V
    .registers 1

    #@0
    .prologue
    .line 1367
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    #@3
    .line 1368
    return-void
.end method
