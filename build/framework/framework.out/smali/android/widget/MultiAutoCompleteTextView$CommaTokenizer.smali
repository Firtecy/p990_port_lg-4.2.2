.class public Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;
.super Ljava/lang/Object;
.source "MultiAutoCompleteTextView.java"

# interfaces
.implements Landroid/widget/MultiAutoCompleteTextView$Tokenizer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/MultiAutoCompleteTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommaTokenizer"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 236
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public findTokenEnd(Ljava/lang/CharSequence;I)I
    .registers 7
    .parameter "text"
    .parameter "cursor"

    #@0
    .prologue
    .line 251
    move v0, p2

    #@1
    .line 252
    .local v0, i:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    .line 254
    .local v1, len:I
    :goto_5
    if-ge v0, v1, :cond_13

    #@7
    .line 255
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@a
    move-result v2

    #@b
    const/16 v3, 0x2c

    #@d
    if-ne v2, v3, :cond_10

    #@f
    .line 262
    .end local v0           #i:I
    :goto_f
    return v0

    #@10
    .line 258
    .restart local v0       #i:I
    :cond_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_5

    #@13
    :cond_13
    move v0, v1

    #@14
    .line 262
    goto :goto_f
.end method

.method public findTokenStart(Ljava/lang/CharSequence;I)I
    .registers 6
    .parameter "text"
    .parameter "cursor"

    #@0
    .prologue
    .line 238
    move v0, p2

    #@1
    .line 240
    .local v0, i:I
    :goto_1
    if-lez v0, :cond_10

    #@3
    add-int/lit8 v1, v0, -0x1

    #@5
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@8
    move-result v1

    #@9
    const/16 v2, 0x2c

    #@b
    if-eq v1, v2, :cond_10

    #@d
    .line 241
    add-int/lit8 v0, v0, -0x1

    #@f
    goto :goto_1

    #@10
    .line 243
    :cond_10
    :goto_10
    if-ge v0, p2, :cond_1d

    #@12
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@15
    move-result v1

    #@16
    const/16 v2, 0x20

    #@18
    if-ne v1, v2, :cond_1d

    #@1a
    .line 244
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_10

    #@1d
    .line 247
    :cond_1d
    return v0
.end method

.method public terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 9
    .parameter "text"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 266
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v6

    #@5
    .line 268
    .local v6, i:I
    :goto_5
    if-lez v6, :cond_14

    #@7
    add-int/lit8 v0, v6, -0x1

    #@9
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@c
    move-result v0

    #@d
    const/16 v2, 0x20

    #@f
    if-ne v0, v2, :cond_14

    #@11
    .line 269
    add-int/lit8 v6, v6, -0x1

    #@13
    goto :goto_5

    #@14
    .line 272
    :cond_14
    if-lez v6, :cond_21

    #@16
    add-int/lit8 v0, v6, -0x1

    #@18
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@1b
    move-result v0

    #@1c
    const/16 v2, 0x2c

    #@1e
    if-ne v0, v2, :cond_21

    #@20
    .line 281
    .end local p1
    :goto_20
    return-object p1

    #@21
    .line 275
    .restart local p1
    :cond_21
    instance-of v0, p1, Landroid/text/Spanned;

    #@23
    if-eqz v0, :cond_4c

    #@25
    .line 276
    new-instance v4, Landroid/text/SpannableString;

    #@27
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    const-string v2, ", "

    #@32
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@3d
    .local v4, sp:Landroid/text/SpannableString;
    move-object v0, p1

    #@3e
    .line 277
    check-cast v0, Landroid/text/Spanned;

    #@40
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@43
    move-result v2

    #@44
    const-class v3, Ljava/lang/Object;

    #@46
    move v5, v1

    #@47
    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    #@4a
    move-object p1, v4

    #@4b
    .line 279
    goto :goto_20

    #@4c
    .line 281
    .end local v4           #sp:Landroid/text/SpannableString;
    :cond_4c
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, ", "

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object p1

    #@5f
    goto :goto_20
.end method
