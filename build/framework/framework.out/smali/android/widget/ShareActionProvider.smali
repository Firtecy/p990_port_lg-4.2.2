.class public Landroid/widget/ShareActionProvider;
.super Landroid/view/ActionProvider;
.source "ShareActionProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ShareActionProvider$1;,
        Landroid/widget/ShareActionProvider$ShareAcitivityChooserModelPolicy;,
        Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;,
        Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_INITIAL_ACTIVITY_COUNT:I = 0x4

.field public static final DEFAULT_SHARE_HISTORY_FILE_NAME:Ljava/lang/String; = "share_history.xml"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMaxShownActivityCount:I

.field private mOnChooseActivityListener:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

.field private final mOnMenuItemClickListener:Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;

.field private mOnShareTargetSelectedListener:Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;

.field private mShareHistoryFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 144
    invoke-direct {p0, p1}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    #@3
    .line 111
    const/4 v0, 0x4

    #@4
    iput v0, p0, Landroid/widget/ShareActionProvider;->mMaxShownActivityCount:I

    #@6
    .line 116
    new-instance v0, Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-direct {v0, p0, v1}, Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;-><init>(Landroid/widget/ShareActionProvider;Landroid/widget/ShareActionProvider$1;)V

    #@c
    iput-object v0, p0, Landroid/widget/ShareActionProvider;->mOnMenuItemClickListener:Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;

    #@e
    .line 132
    const-string/jumbo v0, "share_history.xml"

    #@11
    iput-object v0, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@13
    .line 145
    iput-object p1, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@15
    .line 146
    return-void
.end method

.method static synthetic access$100(Landroid/widget/ShareActionProvider;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/ShareActionProvider;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/ShareActionProvider;)Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ShareActionProvider;->mOnShareTargetSelectedListener:Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;

    #@2
    return-object v0
.end method

.method private setActivityChooserPolicyIfNeeded()V
    .registers 4

    #@0
    .prologue
    .line 317
    iget-object v1, p0, Landroid/widget/ShareActionProvider;->mOnShareTargetSelectedListener:Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 325
    :goto_4
    return-void

    #@5
    .line 320
    :cond_5
    iget-object v1, p0, Landroid/widget/ShareActionProvider;->mOnChooseActivityListener:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@7
    if-nez v1, :cond_11

    #@9
    .line 321
    new-instance v1, Landroid/widget/ShareActionProvider$ShareAcitivityChooserModelPolicy;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v1, p0, v2}, Landroid/widget/ShareActionProvider$ShareAcitivityChooserModelPolicy;-><init>(Landroid/widget/ShareActionProvider;Landroid/widget/ShareActionProvider$1;)V

    #@f
    iput-object v1, p0, Landroid/widget/ShareActionProvider;->mOnChooseActivityListener:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@11
    .line 323
    :cond_11
    iget-object v1, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@13
    iget-object v2, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@15
    invoke-static {v1, v2}, Landroid/widget/ActivityChooserModel;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/ActivityChooserModel;

    #@18
    move-result-object v0

    #@19
    .line 324
    .local v0, dataModel:Landroid/widget/ActivityChooserModel;
    iget-object v1, p0, Landroid/widget/ShareActionProvider;->mOnChooseActivityListener:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/ActivityChooserModel;->setOnChooseActivityListener(Landroid/widget/ActivityChooserModel$OnChooseActivityListener;)V

    #@1e
    goto :goto_4
.end method


# virtual methods
.method public hasSubMenu()Z
    .registers 2

    #@0
    .prologue
    .line 194
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onCreateActionView()Landroid/view/View;
    .registers 8

    #@0
    .prologue
    .line 169
    iget-object v4, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@2
    iget-object v5, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@4
    invoke-static {v4, v5}, Landroid/widget/ActivityChooserModel;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/ActivityChooserModel;

    #@7
    move-result-object v1

    #@8
    .line 170
    .local v1, dataModel:Landroid/widget/ActivityChooserModel;
    new-instance v0, Landroid/widget/ActivityChooserView;

    #@a
    iget-object v4, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@c
    invoke-direct {v0, v4}, Landroid/widget/ActivityChooserView;-><init>(Landroid/content/Context;)V

    #@f
    .line 171
    .local v0, activityChooserView:Landroid/widget/ActivityChooserView;
    invoke-virtual {v0, v1}, Landroid/widget/ActivityChooserView;->setActivityChooserModel(Landroid/widget/ActivityChooserModel;)V

    #@12
    .line 174
    new-instance v3, Landroid/util/TypedValue;

    #@14
    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    #@17
    .line 175
    .local v3, outTypedValue:Landroid/util/TypedValue;
    iget-object v4, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@19
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@1c
    move-result-object v4

    #@1d
    const v5, 0x10103e1

    #@20
    const/4 v6, 0x1

    #@21
    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@24
    .line 176
    iget-object v4, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v4

    #@2a
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    #@2c
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2f
    move-result-object v2

    #@30
    .line 177
    .local v2, drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v2}, Landroid/widget/ActivityChooserView;->setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    #@33
    .line 178
    invoke-virtual {v0, p0}, Landroid/widget/ActivityChooserView;->setProvider(Landroid/view/ActionProvider;)V

    #@36
    .line 181
    const v4, 0x10404fc

    #@39
    invoke-virtual {v0, v4}, Landroid/widget/ActivityChooserView;->setDefaultActionButtonContentDescription(I)V

    #@3c
    .line 183
    const v4, 0x10404fb

    #@3f
    invoke-virtual {v0, v4}, Landroid/widget/ActivityChooserView;->setExpandActivityOverflowButtonContentDescription(I)V

    #@42
    .line 186
    return-object v0
.end method

.method public onPrepareSubMenu(Landroid/view/SubMenu;)V
    .registers 12
    .parameter "subMenu"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 203
    invoke-interface {p1}, Landroid/view/SubMenu;->clear()V

    #@4
    .line 205
    iget-object v7, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@6
    iget-object v8, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@8
    invoke-static {v7, v8}, Landroid/widget/ActivityChooserModel;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/ActivityChooserModel;

    #@b
    move-result-object v2

    #@c
    .line 206
    .local v2, dataModel:Landroid/widget/ActivityChooserModel;
    iget-object v7, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@11
    move-result-object v6

    #@12
    .line 208
    .local v6, packageManager:Landroid/content/pm/PackageManager;
    invoke-virtual {v2}, Landroid/widget/ActivityChooserModel;->getActivityCount()I

    #@15
    move-result v3

    #@16
    .line 209
    .local v3, expandedActivityCount:I
    iget v7, p0, Landroid/widget/ShareActionProvider;->mMaxShownActivityCount:I

    #@18
    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    #@1b
    move-result v1

    #@1c
    .line 212
    .local v1, collapsedActivityCount:I
    const/4 v5, 0x0

    #@1d
    .local v5, i:I
    :goto_1d
    if-ge v5, v1, :cond_3b

    #@1f
    .line 213
    invoke-virtual {v2, v5}, Landroid/widget/ActivityChooserModel;->getActivity(I)Landroid/content/pm/ResolveInfo;

    #@22
    move-result-object v0

    #@23
    .line 214
    .local v0, activity:Landroid/content/pm/ResolveInfo;
    invoke-virtual {v0, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@26
    move-result-object v7

    #@27
    invoke-interface {p1, v9, v5, v5, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v0, v6}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@2e
    move-result-object v8

    #@2f
    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    #@32
    move-result-object v7

    #@33
    iget-object v8, p0, Landroid/widget/ShareActionProvider;->mOnMenuItemClickListener:Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;

    #@35
    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    #@38
    .line 212
    add-int/lit8 v5, v5, 0x1

    #@3a
    goto :goto_1d

    #@3b
    .line 219
    .end local v0           #activity:Landroid/content/pm/ResolveInfo;
    :cond_3b
    if-ge v1, v3, :cond_69

    #@3d
    .line 221
    iget-object v7, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@3f
    const v8, 0x104052d

    #@42
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-interface {p1, v9, v1, v1, v7}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    #@49
    move-result-object v4

    #@4a
    .line 224
    .local v4, expandedSubMenu:Landroid/view/SubMenu;
    const/4 v5, 0x0

    #@4b
    :goto_4b
    if-ge v5, v3, :cond_69

    #@4d
    .line 225
    invoke-virtual {v2, v5}, Landroid/widget/ActivityChooserModel;->getActivity(I)Landroid/content/pm/ResolveInfo;

    #@50
    move-result-object v0

    #@51
    .line 226
    .restart local v0       #activity:Landroid/content/pm/ResolveInfo;
    invoke-virtual {v0, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@54
    move-result-object v7

    #@55
    invoke-interface {v4, v9, v5, v5, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v0, v6}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5c
    move-result-object v8

    #@5d
    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    #@60
    move-result-object v7

    #@61
    iget-object v8, p0, Landroid/widget/ShareActionProvider;->mOnMenuItemClickListener:Landroid/widget/ShareActionProvider$ShareMenuItemOnMenuItemClickListener;

    #@63
    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    #@66
    .line 224
    add-int/lit8 v5, v5, 0x1

    #@68
    goto :goto_4b

    #@69
    .line 231
    .end local v0           #activity:Landroid/content/pm/ResolveInfo;
    .end local v4           #expandedSubMenu:Landroid/view/SubMenu;
    :cond_69
    return-void
.end method

.method public setOnShareTargetSelectedListener(Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Landroid/widget/ShareActionProvider;->mOnShareTargetSelectedListener:Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;

    #@2
    .line 160
    invoke-direct {p0}, Landroid/widget/ShareActionProvider;->setActivityChooserPolicyIfNeeded()V

    #@5
    .line 161
    return-void
.end method

.method public setShareHistoryFileName(Ljava/lang/String;)V
    .registers 2
    .parameter "shareHistoryFile"

    #@0
    .prologue
    .line 265
    iput-object p1, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@2
    .line 266
    invoke-direct {p0}, Landroid/widget/ShareActionProvider;->setActivityChooserPolicyIfNeeded()V

    #@5
    .line 267
    return-void
.end method

.method public setShareIntent(Landroid/content/Intent;)V
    .registers 5
    .parameter "shareIntent"

    #@0
    .prologue
    .line 289
    iget-object v1, p0, Landroid/widget/ShareActionProvider;->mContext:Landroid/content/Context;

    #@2
    iget-object v2, p0, Landroid/widget/ShareActionProvider;->mShareHistoryFileName:Ljava/lang/String;

    #@4
    invoke-static {v1, v2}, Landroid/widget/ActivityChooserModel;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/ActivityChooserModel;

    #@7
    move-result-object v0

    #@8
    .line 291
    .local v0, dataModel:Landroid/widget/ActivityChooserModel;
    invoke-virtual {v0, p1}, Landroid/widget/ActivityChooserModel;->setIntent(Landroid/content/Intent;)V

    #@b
    .line 292
    return-void
.end method
