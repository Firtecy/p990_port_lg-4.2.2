.class Landroid/widget/CalendarView$WeeksAdapter;
.super Landroid/widget/BaseAdapter;
.source "CalendarView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/CalendarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeeksAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/CalendarView$WeeksAdapter$CalendarGestureListener;
    }
.end annotation


# instance fields
.field private mFocusedMonth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mSelectedDate:Ljava/util/Calendar;

.field private mSelectedWeek:I

.field private mTotalWeekCount:I

.field final synthetic this$0:Landroid/widget/CalendarView;


# direct methods
.method public constructor <init>(Landroid/widget/CalendarView;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 1350
    iput-object p1, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@2
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@5
    .line 1346
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@b
    .line 1351
    invoke-static {p1, p2}, Landroid/widget/CalendarView;->access$1402(Landroid/widget/CalendarView;Landroid/content/Context;)Landroid/content/Context;

    #@e
    .line 1352
    new-instance v0, Landroid/view/GestureDetector;

    #@10
    invoke-static {p1}, Landroid/widget/CalendarView;->access$1500(Landroid/widget/CalendarView;)Landroid/content/Context;

    #@13
    move-result-object v1

    #@14
    new-instance v2, Landroid/widget/CalendarView$WeeksAdapter$CalendarGestureListener;

    #@16
    invoke-direct {v2, p0}, Landroid/widget/CalendarView$WeeksAdapter$CalendarGestureListener;-><init>(Landroid/widget/CalendarView$WeeksAdapter;)V

    #@19
    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@1c
    iput-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mGestureDetector:Landroid/view/GestureDetector;

    #@1e
    .line 1353
    invoke-direct {p0}, Landroid/widget/CalendarView$WeeksAdapter;->init()V

    #@21
    .line 1354
    return-void
.end method

.method static synthetic access$400(Landroid/widget/CalendarView$WeeksAdapter;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1338
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/CalendarView$WeeksAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1338
    invoke-direct {p0}, Landroid/widget/CalendarView$WeeksAdapter;->init()V

    #@3
    return-void
.end method

.method private init()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x7

    #@1
    .line 1360
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@3
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@5
    invoke-static {v0, v1}, Landroid/widget/CalendarView;->access$1600(Landroid/widget/CalendarView;Ljava/util/Calendar;)I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedWeek:I

    #@b
    .line 1361
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@d
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@f
    invoke-static {v1}, Landroid/widget/CalendarView;->access$1700(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@12
    move-result-object v1

    #@13
    invoke-static {v0, v1}, Landroid/widget/CalendarView;->access$1600(Landroid/widget/CalendarView;Ljava/util/Calendar;)I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mTotalWeekCount:I

    #@19
    .line 1362
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@1b
    invoke-static {v0}, Landroid/widget/CalendarView;->access$1800(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    #@22
    move-result v0

    #@23
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@25
    invoke-static {v1}, Landroid/widget/CalendarView;->access$1900(Landroid/widget/CalendarView;)I

    #@28
    move-result v1

    #@29
    if-ne v0, v1, :cond_3d

    #@2b
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@2d
    invoke-static {v0}, Landroid/widget/CalendarView;->access$1700(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    #@34
    move-result v0

    #@35
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@37
    invoke-static {v1}, Landroid/widget/CalendarView;->access$1900(Landroid/widget/CalendarView;)I

    #@3a
    move-result v1

    #@3b
    if-eq v0, v1, :cond_43

    #@3d
    .line 1364
    :cond_3d
    iget v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mTotalWeekCount:I

    #@3f
    add-int/lit8 v0, v0, 0x1

    #@41
    iput v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mTotalWeekCount:I

    #@43
    .line 1366
    :cond_43
    return-void
.end method

.method private onDateTapped(Ljava/util/Calendar;)V
    .registers 3
    .parameter "day"

    #@0
    .prologue
    .line 1466
    invoke-virtual {p0, p1}, Landroid/widget/CalendarView$WeeksAdapter;->setSelectedDay(Ljava/util/Calendar;)V

    #@3
    .line 1467
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@5
    invoke-static {v0, p1}, Landroid/widget/CalendarView;->access$2300(Landroid/widget/CalendarView;Ljava/util/Calendar;)V

    #@8
    .line 1468
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 1393
    iget v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mTotalWeekCount:I

    #@2
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1398
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 1403
    int-to-long v0, p1

    #@1
    return-wide v0
.end method

.method public getSelectedDay()Ljava/util/Calendar;
    .registers 2

    #@0
    .prologue
    .line 1388
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const/4 v5, -0x2

    #@1
    .line 1408
    const/4 v2, 0x0

    #@2
    .line 1409
    .local v2, weekView:Landroid/widget/CalendarView$WeekView;
    if-eqz p2, :cond_18

    #@4
    move-object v2, p2

    #@5
    .line 1410
    check-cast v2, Landroid/widget/CalendarView$WeekView;

    #@7
    .line 1421
    :goto_7
    iget v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedWeek:I

    #@9
    if-ne v3, p1, :cond_35

    #@b
    iget-object v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@d
    const/4 v4, 0x7

    #@e
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    #@11
    move-result v1

    #@12
    .line 1423
    .local v1, selectedWeekDay:I
    :goto_12
    iget v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->mFocusedMonth:I

    #@14
    invoke-virtual {v2, p1, v1, v3}, Landroid/widget/CalendarView$WeekView;->init(III)V

    #@17
    .line 1425
    return-object v2

    #@18
    .line 1412
    .end local v1           #selectedWeekDay:I
    :cond_18
    new-instance v2, Landroid/widget/CalendarView$WeekView;

    #@1a
    .end local v2           #weekView:Landroid/widget/CalendarView$WeekView;
    iget-object v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@1c
    iget-object v4, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@1e
    invoke-static {v4}, Landroid/widget/CalendarView;->access$2000(Landroid/widget/CalendarView;)Landroid/content/Context;

    #@21
    move-result-object v4

    #@22
    invoke-direct {v2, v3, v4}, Landroid/widget/CalendarView$WeekView;-><init>(Landroid/widget/CalendarView;Landroid/content/Context;)V

    #@25
    .line 1413
    .restart local v2       #weekView:Landroid/widget/CalendarView$WeekView;
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    #@27
    invoke-direct {v0, v5, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    #@2a
    .line 1416
    .local v0, params:Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v2, v0}, Landroid/widget/CalendarView$WeekView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@2d
    .line 1417
    const/4 v3, 0x1

    #@2e
    invoke-virtual {v2, v3}, Landroid/widget/CalendarView$WeekView;->setClickable(Z)V

    #@31
    .line 1418
    invoke-virtual {v2, p0}, Landroid/widget/CalendarView$WeekView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@34
    goto :goto_7

    #@35
    .line 1421
    .end local v0           #params:Landroid/widget/AbsListView$LayoutParams;
    :cond_35
    const/4 v1, -0x1

    #@36
    goto :goto_12
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1443
    iget-object v2, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@3
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/widget/ListView;->isEnabled()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_57

    #@d
    iget-object v2, p0, Landroid/widget/CalendarView$WeeksAdapter;->mGestureDetector:Landroid/view/GestureDetector;

    #@f
    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_57

    #@15
    move-object v0, p1

    #@16
    .line 1444
    check-cast v0, Landroid/widget/CalendarView$WeekView;

    #@18
    .line 1446
    .local v0, weekView:Landroid/widget/CalendarView$WeekView;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@1b
    move-result v2

    #@1c
    iget-object v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@1e
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v0, v2, v3}, Landroid/widget/CalendarView$WeekView;->getDayFromLocation(FLjava/util/Calendar;)Z

    #@25
    move-result v2

    #@26
    if-nez v2, :cond_29

    #@28
    .line 1457
    .end local v0           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_28
    :goto_28
    return v1

    #@29
    .line 1451
    .restart local v0       #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_29
    iget-object v2, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@2b
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@2e
    move-result-object v2

    #@2f
    iget-object v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@31
    invoke-static {v3}, Landroid/widget/CalendarView;->access$1800(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@38
    move-result v2

    #@39
    if-nez v2, :cond_28

    #@3b
    iget-object v2, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@3d
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@40
    move-result-object v2

    #@41
    iget-object v3, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@43
    invoke-static {v3}, Landroid/widget/CalendarView;->access$1700(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@4a
    move-result v2

    #@4b
    if-nez v2, :cond_28

    #@4d
    .line 1454
    iget-object v2, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@4f
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@52
    move-result-object v2

    #@53
    invoke-direct {p0, v2}, Landroid/widget/CalendarView$WeeksAdapter;->onDateTapped(Ljava/util/Calendar;)V

    #@56
    goto :goto_28

    #@57
    .line 1457
    .end local v0           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_57
    const/4 v1, 0x0

    #@58
    goto :goto_28
.end method

.method public setFocusMonth(I)V
    .registers 3
    .parameter "month"

    #@0
    .prologue
    .line 1434
    iget v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mFocusedMonth:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 1439
    :goto_4
    return-void

    #@5
    .line 1437
    :cond_5
    iput p1, p0, Landroid/widget/CalendarView$WeeksAdapter;->mFocusedMonth:I

    #@7
    .line 1438
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeeksAdapter;->notifyDataSetChanged()V

    #@a
    goto :goto_4
.end method

.method public setSelectedDay(Ljava/util/Calendar;)V
    .registers 6
    .parameter "selectedDay"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x1

    #@2
    .line 1374
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@8
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    #@b
    move-result v1

    #@c
    if-ne v0, v1, :cond_1b

    #@e
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    #@11
    move-result v0

    #@12
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@14
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@17
    move-result v1

    #@18
    if-ne v0, v1, :cond_1b

    #@1a
    .line 1382
    :goto_1a
    return-void

    #@1b
    .line 1378
    :cond_1b
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@1d
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@20
    move-result-wide v1

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@24
    .line 1379
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->this$0:Landroid/widget/CalendarView;

    #@26
    iget-object v1, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@28
    invoke-static {v0, v1}, Landroid/widget/CalendarView;->access$1600(Landroid/widget/CalendarView;Ljava/util/Calendar;)I

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedWeek:I

    #@2e
    .line 1380
    iget-object v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mSelectedDate:Ljava/util/Calendar;

    #@30
    const/4 v1, 0x2

    #@31
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@34
    move-result v0

    #@35
    iput v0, p0, Landroid/widget/CalendarView$WeeksAdapter;->mFocusedMonth:I

    #@37
    .line 1381
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeeksAdapter;->notifyDataSetChanged()V

    #@3a
    goto :goto_1a
.end method
