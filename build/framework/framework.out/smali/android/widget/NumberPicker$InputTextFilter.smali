.class Landroid/widget/NumberPicker$InputTextFilter;
.super Landroid/text/method/NumberKeyListener;
.source "NumberPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InputTextFilter"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/NumberPicker;


# direct methods
.method constructor <init>(Landroid/widget/NumberPicker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1945
    iput-object p1, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-direct {p0}, Landroid/text/method/NumberKeyListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 20
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "dstart"
    .parameter "dend"

    #@0
    .prologue
    .line 1961
    iget-object v10, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-static {v10}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@5
    move-result-object v10

    #@6
    if-nez v10, :cond_5c

    #@8
    .line 1962
    invoke-super/range {p0 .. p6}, Landroid/text/method/NumberKeyListener;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    #@b
    move-result-object v3

    #@c
    .line 1963
    .local v3, filtered:Ljava/lang/CharSequence;
    if-nez v3, :cond_12

    #@e
    .line 1964
    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@11
    move-result-object v3

    #@12
    .line 1967
    :cond_12
    new-instance v10, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const/4 v11, 0x0

    #@18
    move-object/from16 v0, p4

    #@1a
    move/from16 v1, p5

    #@1c
    invoke-interface {v0, v11, v1}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    #@1f
    move-result-object v11

    #@20
    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@23
    move-result-object v11

    #@24
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v10

    #@28
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v10

    #@2c
    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->length()I

    #@2f
    move-result v11

    #@30
    move-object/from16 v0, p4

    #@32
    move/from16 v1, p6

    #@34
    invoke-interface {v0, v1, v11}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    #@37
    move-result-object v11

    #@38
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v10

    #@3c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    .line 1970
    .local v6, result:Ljava/lang/String;
    const-string v10, ""

    #@42
    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v10

    #@46
    if-eqz v10, :cond_49

    #@48
    .line 2000
    .end local v6           #result:Ljava/lang/String;
    :goto_48
    return-object v6

    #@49
    .line 1973
    .restart local v6       #result:Ljava/lang/String;
    :cond_49
    iget-object v10, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@4b
    invoke-static {v10, v6}, Landroid/widget/NumberPicker;->access$1000(Landroid/widget/NumberPicker;Ljava/lang/String;)I

    #@4e
    move-result v8

    #@4f
    .line 1980
    .local v8, val:I
    iget-object v10, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@51
    invoke-static {v10}, Landroid/widget/NumberPicker;->access$1100(Landroid/widget/NumberPicker;)I

    #@54
    move-result v10

    #@55
    if-le v8, v10, :cond_5a

    #@57
    .line 1981
    const-string v6, ""

    #@59
    goto :goto_48

    #@5a
    :cond_5a
    move-object v6, v3

    #@5b
    .line 1983
    goto :goto_48

    #@5c
    .line 1986
    .end local v3           #filtered:Ljava/lang/CharSequence;
    .end local v6           #result:Ljava/lang/String;
    .end local v8           #val:I
    :cond_5c
    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@5f
    move-result-object v10

    #@60
    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    .line 1987
    .restart local v3       #filtered:Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@67
    move-result v10

    #@68
    if-eqz v10, :cond_6d

    #@6a
    .line 1988
    const-string v6, ""

    #@6c
    goto :goto_48

    #@6d
    .line 1990
    :cond_6d
    new-instance v10, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const/4 v11, 0x0

    #@73
    move-object/from16 v0, p4

    #@75
    move/from16 v1, p5

    #@77
    invoke-interface {v0, v11, v1}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    #@7a
    move-result-object v11

    #@7b
    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@7e
    move-result-object v11

    #@7f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v10

    #@83
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    invoke-interface/range {p4 .. p4}, Landroid/text/Spanned;->length()I

    #@8a
    move-result v11

    #@8b
    move-object/from16 v0, p4

    #@8d
    move/from16 v1, p6

    #@8f
    invoke-interface {v0, v1, v11}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    #@92
    move-result-object v11

    #@93
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v10

    #@97
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    .line 1992
    .restart local v6       #result:Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@9e
    move-result-object v10

    #@9f
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    .line 1993
    .local v7, str:Ljava/lang/String;
    iget-object v10, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@a5
    invoke-static {v10}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@a8
    move-result-object v2

    #@a9
    .local v2, arr$:[Ljava/lang/String;
    array-length v5, v2

    #@aa
    .local v5, len$:I
    const/4 v4, 0x0

    #@ab
    .local v4, i$:I
    :goto_ab
    if-ge v4, v5, :cond_d5

    #@ad
    aget-object v8, v2, v4

    #@af
    .line 1994
    .local v8, val:Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@b2
    move-result-object v9

    #@b3
    .line 1995
    .local v9, valLowerCase:Ljava/lang/String;
    invoke-virtual {v9, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b6
    move-result v10

    #@b7
    if-eqz v10, :cond_d2

    #@b9
    .line 1996
    iget-object v10, p0, Landroid/widget/NumberPicker$InputTextFilter;->this$0:Landroid/widget/NumberPicker;

    #@bb
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@be
    move-result v11

    #@bf
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@c2
    move-result v12

    #@c3
    invoke-static {v10, v11, v12}, Landroid/widget/NumberPicker;->access$1200(Landroid/widget/NumberPicker;II)V

    #@c6
    .line 1997
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@c9
    move-result v10

    #@ca
    move/from16 v0, p5

    #@cc
    invoke-virtual {v8, v0, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@cf
    move-result-object v6

    #@d0
    goto/16 :goto_48

    #@d2
    .line 1993
    :cond_d2
    add-int/lit8 v4, v4, 0x1

    #@d4
    goto :goto_ab

    #@d5
    .line 2000
    .end local v8           #val:Ljava/lang/String;
    .end local v9           #valLowerCase:Ljava/lang/String;
    :cond_d5
    const-string v6, ""

    #@d7
    goto/16 :goto_48
.end method

.method protected getAcceptedChars()[C
    .registers 2

    #@0
    .prologue
    .line 1955
    invoke-static {}, Landroid/widget/NumberPicker;->access$800()[C

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 1950
    const/4 v0, 0x1

    #@1
    return v0
.end method
