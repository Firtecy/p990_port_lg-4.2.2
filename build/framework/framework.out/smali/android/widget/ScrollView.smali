.class public Landroid/widget/ScrollView;
.super Landroid/widget/FrameLayout;
.source "ScrollView.java"


# static fields
.field static final ANIMATED_SCROLL_GAP:I = 0xfa

.field private static COORDINATE_DIRECTION:I = 0x0

.field private static final INVALID_POINTER:I = -0x1

.field static final MAX_SCROLL_FACTOR:F = 0.5f

.field private static final TAG:Ljava/lang/String; = "ScrollView"

.field private static eventMonitor:I

.field private static mCapptouchFlickNoti:Z

.field private static moveCounter:I

.field private static toCompareY:I


# instance fields
.field private final FLICKMAXVELOCITY:I

.field private final GAP_BETEEN_LAST_BEFORE:I

.field private final LIMIT_Y_GAP:I

.field private final MARK_INVERSION:I

.field private final WEIGHTED_VELOCITY:I

.field private final X_Y_VELOCITY_GAP:I

.field private final coorDirection:[I

.field private mActivePointerId:I

.field private mChildToScrollTo:Landroid/view/View;

.field private mEdgeGlowBottom:Landroid/widget/EdgeEffect;

.field private mEdgeGlowTop:Landroid/widget/EdgeEffect;

.field private mFillViewport:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mFlingStrictSpan:Landroid/os/StrictMode$Span;

.field private mIsBeingDragged:Z

.field private mIsLayoutDirty:Z

.field private mLastMotionY:I

.field private mLastScroll:J

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOverflingDistance:I

.field private mOverscrollDistance:I

.field private mScrollStrictSpan:Landroid/os/StrictMode$Span;

.field private mScroller:Landroid/widget/OverScroller;

.field private mSmoothScrollingEnabled:Z

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 141
    sput-boolean v1, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@3
    .line 142
    const/16 v0, 0x8

    #@5
    sput v0, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@7
    .line 150
    sput v1, Landroid/widget/ScrollView;->toCompareY:I

    #@9
    .line 151
    sput v1, Landroid/widget/ScrollView;->eventMonitor:I

    #@b
    .line 152
    sput v1, Landroid/widget/ScrollView;->moveCounter:I

    #@d
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 170
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 174
    const v0, 0x1010080

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 178
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@7
    .line 77
    new-instance v1, Landroid/graphics/Rect;

    #@9
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@c
    iput-object v1, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@e
    .line 91
    iput-boolean v5, p0, Landroid/widget/ScrollView;->mIsLayoutDirty:Z

    #@10
    .line 98
    iput-object v3, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@12
    .line 105
    iput-boolean v2, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@14
    .line 122
    iput-boolean v5, p0, Landroid/widget/ScrollView;->mSmoothScrollingEnabled:Z

    #@16
    .line 135
    iput v4, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@18
    .line 143
    const/16 v1, 0x320

    #@1a
    iput v1, p0, Landroid/widget/ScrollView;->X_Y_VELOCITY_GAP:I

    #@1c
    .line 144
    const/16 v1, 0x258

    #@1e
    iput v1, p0, Landroid/widget/ScrollView;->WEIGHTED_VELOCITY:I

    #@20
    .line 145
    iput v4, p0, Landroid/widget/ScrollView;->MARK_INVERSION:I

    #@22
    .line 146
    sget v1, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@24
    new-array v1, v1, [I

    #@26
    iput-object v1, p0, Landroid/widget/ScrollView;->coorDirection:[I

    #@28
    .line 147
    const/16 v1, 0xfa0

    #@2a
    iput v1, p0, Landroid/widget/ScrollView;->FLICKMAXVELOCITY:I

    #@2c
    .line 148
    const/16 v1, 0xd

    #@2e
    iput v1, p0, Landroid/widget/ScrollView;->GAP_BETEEN_LAST_BEFORE:I

    #@30
    .line 149
    const/4 v1, 0x5

    #@31
    iput v1, p0, Landroid/widget/ScrollView;->LIMIT_Y_GAP:I

    #@33
    .line 160
    iput-object v3, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@35
    .line 161
    iput-object v3, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@37
    .line 179
    invoke-direct {p0}, Landroid/widget/ScrollView;->initScrollView()V

    #@3a
    .line 181
    sget-object v1, Lcom/android/internal/R$styleable;->ScrollView:[I

    #@3c
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@3f
    move-result-object v0

    #@40
    .line 184
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@43
    move-result v1

    #@44
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->setFillViewport(Z)V

    #@47
    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4a
    move-result-object v1

    #@4b
    const v2, 0x2060022

    #@4e
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@51
    move-result v1

    #@52
    sput-boolean v1, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@54
    .line 188
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@57
    .line 189
    return-void
.end method

.method private canScroll()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 288
    invoke-virtual {p0, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    .line 289
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_18

    #@7
    .line 290
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@a
    move-result v1

    #@b
    .line 291
    .local v1, childHeight:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@e
    move-result v3

    #@f
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@11
    add-int/2addr v4, v1

    #@12
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@14
    add-int/2addr v4, v5

    #@15
    if-ge v3, v4, :cond_18

    #@17
    const/4 v2, 0x1

    #@18
    .line 293
    .end local v1           #childHeight:I
    :cond_18
    return v2
.end method

.method private static clamp(III)I
    .registers 4
    .parameter "n"
    .parameter "my"
    .parameter "child"

    #@0
    .prologue
    .line 1785
    if-ge p1, p2, :cond_4

    #@2
    if-gez p0, :cond_6

    #@4
    .line 1801
    :cond_4
    const/4 p0, 0x0

    #@5
    .line 1811
    .end local p0
    :cond_5
    :goto_5
    return p0

    #@6
    .line 1803
    .restart local p0
    :cond_6
    add-int v0, p1, p0

    #@8
    if-le v0, p2, :cond_5

    #@a
    .line 1809
    sub-int p0, p2, p1

    #@c
    goto :goto_5
.end method

.method private doScrollY(I)V
    .registers 4
    .parameter "delta"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1286
    if-eqz p1, :cond_a

    #@3
    .line 1287
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mSmoothScrollingEnabled:Z

    #@5
    if-eqz v0, :cond_b

    #@7
    .line 1288
    invoke-virtual {p0, v1, p1}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    #@a
    .line 1293
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1290
    :cond_b
    invoke-virtual {p0, v1, p1}, Landroid/widget/ScrollView;->scrollBy(II)V

    #@e
    goto :goto_a
.end method

.method private endDrag()V
    .registers 2

    #@0
    .prologue
    .line 1703
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@3
    .line 1705
    invoke-direct {p0}, Landroid/widget/ScrollView;->recycleVelocityTracker()V

    #@6
    .line 1707
    iget-object v0, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@8
    if-eqz v0, :cond_14

    #@a
    .line 1708
    iget-object v0, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@c
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@f
    .line 1709
    iget-object v0, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@11
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@14
    .line 1712
    :cond_14
    iget-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@16
    if-eqz v0, :cond_20

    #@18
    .line 1713
    iget-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@1a
    invoke-virtual {v0}, Landroid/os/StrictMode$Span;->finish()V

    #@1d
    .line 1714
    const/4 v0, 0x0

    #@1e
    iput-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@20
    .line 1716
    :cond_20
    return-void
.end method

.method private findFocusableViewInBounds(ZII)Landroid/view/View;
    .registers 15
    .parameter "topFocus"
    .parameter "top"
    .parameter "bottom"

    #@0
    .prologue
    .line 1032
    const/4 v10, 0x2

    #@1
    invoke-virtual {p0, v10}, Landroid/widget/ScrollView;->getFocusables(I)Ljava/util/ArrayList;

    #@4
    move-result-object v2

    #@5
    .line 1033
    .local v2, focusables:Ljava/util/List;,"Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    #@6
    .line 1042
    .local v1, focusCandidate:Landroid/view/View;
    const/4 v3, 0x0

    #@7
    .line 1044
    .local v3, foundFullyContainedFocusable:Z
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 1045
    .local v0, count:I
    const/4 v4, 0x0

    #@c
    .local v4, i:I
    :goto_c
    if-ge v4, v0, :cond_52

    #@e
    .line 1046
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v5

    #@12
    check-cast v5, Landroid/view/View;

    #@14
    .line 1047
    .local v5, view:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@17
    move-result v9

    #@18
    .line 1048
    .local v9, viewTop:I
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@1b
    move-result v6

    #@1c
    .line 1050
    .local v6, viewBottom:I
    if-ge p2, v6, :cond_29

    #@1e
    if-ge v9, p3, :cond_29

    #@20
    .line 1056
    if-ge p2, v9, :cond_2c

    #@22
    if-ge v6, p3, :cond_2c

    #@24
    const/4 v8, 0x1

    #@25
    .line 1059
    .local v8, viewIsFullyContained:Z
    :goto_25
    if-nez v1, :cond_2e

    #@27
    .line 1061
    move-object v1, v5

    #@28
    .line 1062
    move v3, v8

    #@29
    .line 1045
    .end local v8           #viewIsFullyContained:Z
    :cond_29
    :goto_29
    add-int/lit8 v4, v4, 0x1

    #@2b
    goto :goto_c

    #@2c
    .line 1056
    :cond_2c
    const/4 v8, 0x0

    #@2d
    goto :goto_25

    #@2e
    .line 1064
    .restart local v8       #viewIsFullyContained:Z
    :cond_2e
    if-eqz p1, :cond_36

    #@30
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@33
    move-result v10

    #@34
    if-lt v9, v10, :cond_3e

    #@36
    :cond_36
    if-nez p1, :cond_47

    #@38
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@3b
    move-result v10

    #@3c
    if-le v6, v10, :cond_47

    #@3e
    :cond_3e
    const/4 v7, 0x1

    #@3f
    .line 1069
    .local v7, viewIsCloserToBoundary:Z
    :goto_3f
    if-eqz v3, :cond_49

    #@41
    .line 1070
    if-eqz v8, :cond_29

    #@43
    if-eqz v7, :cond_29

    #@45
    .line 1076
    move-object v1, v5

    #@46
    goto :goto_29

    #@47
    .line 1064
    .end local v7           #viewIsCloserToBoundary:Z
    :cond_47
    const/4 v7, 0x0

    #@48
    goto :goto_3f

    #@49
    .line 1079
    .restart local v7       #viewIsCloserToBoundary:Z
    :cond_49
    if-eqz v8, :cond_4e

    #@4b
    .line 1081
    move-object v1, v5

    #@4c
    .line 1082
    const/4 v3, 0x1

    #@4d
    goto :goto_29

    #@4e
    .line 1083
    :cond_4e
    if-eqz v7, :cond_29

    #@50
    .line 1088
    move-object v1, v5

    #@51
    goto :goto_29

    #@52
    .line 1095
    .end local v5           #view:Landroid/view/View;
    .end local v6           #viewBottom:I
    .end local v7           #viewIsCloserToBoundary:Z
    .end local v8           #viewIsFullyContained:Z
    .end local v9           #viewTop:I
    :cond_52
    return-object v1
.end method

.method private getFlickValue(II)I
    .registers 24
    .parameter "vectorVelocity"
    .parameter "initialXVelocity"

    #@0
    .prologue
    .line 596
    sget v18, Landroid/widget/ScrollView;->eventMonitor:I

    #@2
    add-int/lit8 v18, v18, -0x1

    #@4
    sput v18, Landroid/widget/ScrollView;->eventMonitor:I

    #@6
    .line 597
    move/from16 v0, p1

    #@8
    int-to-float v0, v0

    #@9
    move/from16 v18, v0

    #@b
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@e
    move-result v18

    #@f
    move/from16 v0, v18

    #@11
    float-to-int v15, v0

    #@12
    .line 598
    .local v15, signal:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    #@15
    move-result-object v18

    #@16
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v18

    #@1a
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1d
    move-result-object v18

    #@1e
    move-object/from16 v0, v18

    #@20
    iget v13, v0, Landroid/util/DisplayMetrics;->density:F

    #@22
    .line 601
    .local v13, mDensityScale:F
    const/4 v6, 0x0

    #@23
    .line 602
    .local v6, directionUPCount:I
    const/4 v5, 0x0

    #@24
    .line 603
    .local v5, directionDownCount:I
    const/16 v17, 0x0

    #@26
    .line 604
    .local v17, up:Z
    const/4 v7, 0x0

    #@27
    .line 608
    .local v7, down:Z
    sget v18, Landroid/widget/ScrollView;->moveCounter:I

    #@29
    sget v19, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@2b
    move/from16 v0, v18

    #@2d
    move/from16 v1, v19

    #@2f
    if-lt v0, v1, :cond_a7

    #@31
    sget v2, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@33
    .line 610
    .local v2, arrayCount:I
    :goto_33
    sget v18, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@35
    add-int/lit8 v8, v18, -0x1

    #@37
    .line 611
    .local v8, forBitOperation:I
    const/4 v14, 0x0

    #@38
    .line 612
    .local v14, rememberFirstSignal:I
    const/4 v4, 0x0

    #@39
    .line 613
    .local v4, calcFactor:I
    sget v18, Landroid/widget/ScrollView;->moveCounter:I

    #@3b
    sget v19, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@3d
    move/from16 v0, v18

    #@3f
    move/from16 v1, v19

    #@41
    if-le v0, v1, :cond_47

    #@43
    .line 616
    sget v18, Landroid/widget/ScrollView;->eventMonitor:I

    #@45
    add-int v4, v18, v2

    #@47
    .line 619
    :cond_47
    const/4 v12, 0x0

    #@48
    .line 620
    .local v12, keepSignal:Z
    const/4 v9, 0x0

    #@49
    .line 621
    .local v9, gotSignal:I
    const/16 v16, 0x0

    #@4b
    .line 622
    .local v16, sumDeltaY:I
    const/4 v11, 0x0

    #@4c
    .line 632
    .local v11, ifFindFirstSignal:Z
    const/4 v10, 0x1

    #@4d
    .local v10, i:I
    :goto_4d
    if-ge v10, v2, :cond_f7

    #@4f
    .line 634
    if-nez v4, :cond_aa

    #@51
    .line 635
    add-int/lit8 v3, v10, -0x1

    #@53
    .line 641
    .local v3, arrayIndex:I
    :goto_53
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@57
    move-object/from16 v18, v0

    #@59
    and-int v19, v3, v8

    #@5b
    aget v18, v18, v19

    #@5d
    move-object/from16 v0, p0

    #@5f
    iget-object v0, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@61
    move-object/from16 v19, v0

    #@63
    add-int/lit8 v20, v3, 0x1

    #@65
    and-int v20, v20, v8

    #@67
    aget v19, v19, v20

    #@69
    sub-int v9, v18, v19

    #@6b
    .line 642
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@6e
    move-result v18

    #@6f
    add-int v16, v16, v18

    #@71
    .line 644
    int-to-float v0, v9

    #@72
    move/from16 v18, v0

    #@74
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@77
    move-result v18

    #@78
    const/16 v19, 0x0

    #@7a
    cmpg-float v18, v18, v19

    #@7c
    if-gez v18, :cond_ad

    #@7e
    .line 645
    add-int/lit8 v5, v5, 0x1

    #@80
    .line 650
    :cond_80
    :goto_80
    if-eqz v9, :cond_c2

    #@82
    if-nez v11, :cond_c2

    #@84
    .line 651
    const/4 v11, 0x1

    #@85
    .line 652
    move v14, v9

    #@86
    .line 654
    if-gez v14, :cond_bd

    #@88
    .line 655
    const/4 v7, 0x1

    #@89
    .line 664
    :cond_89
    :goto_89
    add-int/lit8 v18, v2, -0x1

    #@8b
    move/from16 v0, v18

    #@8d
    if-ne v10, v0, :cond_a4

    #@8f
    .line 665
    add-int/lit8 v18, v2, -0x1

    #@91
    div-int v18, v16, v18

    #@93
    const/high16 v19, 0x40a0

    #@95
    mul-float v19, v19, v13

    #@97
    move/from16 v0, v19

    #@99
    float-to-int v0, v0

    #@9a
    move/from16 v19, v0

    #@9c
    move/from16 v0, v18

    #@9e
    move/from16 v1, v19

    #@a0
    if-gt v0, v1, :cond_a4

    #@a2
    .line 666
    move/from16 p2, p1

    #@a4
    .line 632
    :cond_a4
    add-int/lit8 v10, v10, 0x1

    #@a6
    goto :goto_4d

    #@a7
    .line 608
    .end local v2           #arrayCount:I
    .end local v3           #arrayIndex:I
    .end local v4           #calcFactor:I
    .end local v8           #forBitOperation:I
    .end local v9           #gotSignal:I
    .end local v10           #i:I
    .end local v11           #ifFindFirstSignal:Z
    .end local v12           #keepSignal:Z
    .end local v14           #rememberFirstSignal:I
    .end local v16           #sumDeltaY:I
    :cond_a7
    sget v2, Landroid/widget/ScrollView;->moveCounter:I

    #@a9
    goto :goto_33

    #@aa
    .line 637
    .restart local v2       #arrayCount:I
    .restart local v4       #calcFactor:I
    .restart local v8       #forBitOperation:I
    .restart local v9       #gotSignal:I
    .restart local v10       #i:I
    .restart local v11       #ifFindFirstSignal:Z
    .restart local v12       #keepSignal:Z
    .restart local v14       #rememberFirstSignal:I
    .restart local v16       #sumDeltaY:I
    :cond_aa
    add-int v3, v4, v10

    #@ac
    .restart local v3       #arrayIndex:I
    goto :goto_53

    #@ad
    .line 646
    :cond_ad
    int-to-float v0, v9

    #@ae
    move/from16 v18, v0

    #@b0
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@b3
    move-result v18

    #@b4
    const/16 v19, 0x0

    #@b6
    cmpl-float v18, v18, v19

    #@b8
    if-lez v18, :cond_80

    #@ba
    .line 647
    add-int/lit8 v6, v6, 0x1

    #@bc
    goto :goto_80

    #@bd
    .line 656
    :cond_bd
    if-lez v14, :cond_89

    #@bf
    .line 657
    const/16 v17, 0x1

    #@c1
    goto :goto_89

    #@c2
    .line 659
    :cond_c2
    if-eqz v9, :cond_89

    #@c4
    int-to-float v0, v9

    #@c5
    move/from16 v18, v0

    #@c7
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@ca
    move-result v18

    #@cb
    move/from16 v0, v18

    #@cd
    float-to-int v0, v0

    #@ce
    move/from16 v18, v0

    #@d0
    int-to-float v0, v14

    #@d1
    move/from16 v19, v0

    #@d3
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->signum(F)F

    #@d6
    move-result v19

    #@d7
    move/from16 v0, v19

    #@d9
    float-to-int v0, v0

    #@da
    move/from16 v19, v0

    #@dc
    move/from16 v0, v18

    #@de
    move/from16 v1, v19

    #@e0
    if-eq v0, v1, :cond_89

    #@e2
    .line 660
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@e5
    move-result v18

    #@e6
    const/high16 v19, 0x4150

    #@e8
    mul-float v19, v19, v13

    #@ea
    move/from16 v0, v19

    #@ec
    float-to-int v0, v0

    #@ed
    move/from16 v19, v0

    #@ef
    move/from16 v0, v18

    #@f1
    move/from16 v1, v19

    #@f3
    if-lt v0, v1, :cond_89

    #@f5
    .line 661
    const/4 v12, 0x1

    #@f6
    goto :goto_89

    #@f7
    .line 677
    .end local v3           #arrayIndex:I
    :cond_f7
    if-nez v12, :cond_101

    #@f9
    .line 678
    if-le v5, v6, :cond_174

    #@fb
    .line 679
    if-eqz p1, :cond_101

    #@fd
    if-gez v15, :cond_101

    #@ff
    .line 680
    mul-int/lit8 p1, p1, -0x1

    #@101
    .line 698
    :cond_101
    :goto_101
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    #@104
    move-result v18

    #@105
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    #@108
    move-result v19

    #@109
    move/from16 v0, v18

    #@10b
    move/from16 v1, v19

    #@10d
    if-ge v0, v1, :cond_158

    #@10f
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    #@112
    move-result v18

    #@113
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    #@116
    move-result v19

    #@117
    sub-int v18, v18, v19

    #@119
    const/16 v19, 0x320

    #@11b
    move/from16 v0, v18

    #@11d
    move/from16 v1, v19

    #@11f
    if-le v0, v1, :cond_158

    #@121
    .line 699
    move/from16 v0, p1

    #@123
    int-to-float v0, v0

    #@124
    move/from16 v18, v0

    #@126
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@129
    move-result v18

    #@12a
    move/from16 v0, v18

    #@12c
    float-to-int v15, v0

    #@12d
    .line 700
    mul-int v18, p1, p1

    #@12f
    mul-int v19, p2, p2

    #@131
    add-int v18, v18, v19

    #@133
    move/from16 v0, v18

    #@135
    int-to-double v0, v0

    #@136
    move-wide/from16 v18, v0

    #@138
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    #@13b
    move-result-wide v18

    #@13c
    move-wide/from16 v0, v18

    #@13e
    double-to-int v0, v0

    #@13f
    move/from16 p1, v0

    #@141
    .line 701
    const/high16 v18, 0x4416

    #@143
    div-float v18, v18, v13

    #@145
    move/from16 v0, v18

    #@147
    float-to-int v0, v0

    #@148
    move/from16 v18, v0

    #@14a
    add-int p1, p1, v18

    #@14c
    .line 702
    const/16 v18, 0xfa0

    #@14e
    move/from16 v0, p1

    #@150
    move/from16 v1, v18

    #@152
    if-lt v0, v1, :cond_156

    #@154
    .line 703
    const/16 p1, 0xfa0

    #@156
    .line 705
    :cond_156
    mul-int p1, p1, v15

    #@158
    .line 707
    :cond_158
    const/16 v18, 0x0

    #@15a
    sput v18, Landroid/widget/ScrollView;->eventMonitor:I

    #@15c
    .line 708
    const/16 v18, 0x0

    #@15e
    sput v18, Landroid/widget/ScrollView;->moveCounter:I

    #@160
    .line 709
    const/4 v10, 0x0

    #@161
    :goto_161
    sget v18, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@163
    move/from16 v0, v18

    #@165
    if-ge v10, v0, :cond_18d

    #@167
    .line 710
    move-object/from16 v0, p0

    #@169
    iget-object v0, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@16b
    move-object/from16 v18, v0

    #@16d
    const/16 v19, 0x0

    #@16f
    aput v19, v18, v10

    #@171
    .line 709
    add-int/lit8 v10, v10, 0x1

    #@173
    goto :goto_161

    #@174
    .line 684
    :cond_174
    if-ge v5, v6, :cond_17d

    #@176
    .line 685
    if-eqz p1, :cond_101

    #@178
    if-lez v15, :cond_101

    #@17a
    .line 686
    mul-int/lit8 p1, p1, -0x1

    #@17c
    goto :goto_101

    #@17d
    .line 691
    :cond_17d
    if-eqz v7, :cond_185

    #@17f
    if-gez v15, :cond_185

    #@181
    .line 692
    mul-int/lit8 p1, p1, -0x1

    #@183
    goto/16 :goto_101

    #@185
    .line 693
    :cond_185
    if-eqz v17, :cond_101

    #@187
    if-lez v15, :cond_101

    #@189
    .line 694
    mul-int/lit8 p1, p1, -0x1

    #@18b
    goto/16 :goto_101

    #@18d
    .line 712
    :cond_18d
    return p1
.end method

.method private getScrollRange()I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1006
    const/4 v1, 0x0

    #@2
    .line 1007
    .local v1, scrollRange:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_1f

    #@8
    .line 1008
    invoke-virtual {p0, v5}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 1009
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@13
    move-result v3

    #@14
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@16
    sub-int/2addr v3, v4

    #@17
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@19
    sub-int/2addr v3, v4

    #@1a
    sub-int/2addr v2, v3

    #@1b
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    #@1e
    move-result v1

    #@1f
    .line 1012
    .end local v0           #child:Landroid/view/View;
    :cond_1f
    return v1
.end method

.method private inChild(II)Z
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 426
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@4
    move-result v3

    #@5
    if-lez v3, :cond_28

    #@7
    .line 427
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@9
    .line 428
    .local v1, scrollY:I
    invoke-virtual {p0, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 429
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@10
    move-result v3

    #@11
    sub-int/2addr v3, v1

    #@12
    if-lt p2, v3, :cond_28

    #@14
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@17
    move-result v3

    #@18
    sub-int/2addr v3, v1

    #@19
    if-ge p2, v3, :cond_28

    #@1b
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@1e
    move-result v3

    #@1f
    if-lt p1, v3, :cond_28

    #@21
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@24
    move-result v3

    #@25
    if-ge p1, v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 434
    .end local v0           #child:Landroid/view/View;
    .end local v1           #scrollY:I
    :cond_28
    return v2
.end method

.method private initOrResetVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 438
    iget-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 439
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 443
    :goto_a
    return-void

    #@b
    .line 441
    :cond_b
    iget-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@d
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    #@10
    goto :goto_a
.end method

.method private initScrollView()V
    .registers 4

    #@0
    .prologue
    .line 236
    new-instance v1, Landroid/widget/OverScroller;

    #@2
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@b
    .line 237
    const/4 v1, 0x1

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->setFocusable(Z)V

    #@f
    .line 238
    const/high16 v1, 0x4

    #@11
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->setDescendantFocusability(I)V

    #@14
    .line 239
    const/4 v1, 0x0

    #@15
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->setWillNotDraw(Z)V

    #@18
    .line 240
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1a
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@1d
    move-result-object v0

    #@1e
    .line 241
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@21
    move-result v1

    #@22
    iput v1, p0, Landroid/widget/ScrollView;->mTouchSlop:I

    #@24
    .line 242
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@27
    move-result v1

    #@28
    iput v1, p0, Landroid/widget/ScrollView;->mMinimumVelocity:I

    #@2a
    .line 243
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/widget/ScrollView;->mMaximumVelocity:I

    #@30
    .line 244
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    #@33
    move-result v1

    #@34
    iput v1, p0, Landroid/widget/ScrollView;->mOverscrollDistance:I

    #@36
    .line 245
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    #@39
    move-result v1

    #@3a
    iput v1, p0, Landroid/widget/ScrollView;->mOverflingDistance:I

    #@3c
    .line 246
    return-void
.end method

.method private initVelocityTrackerIfNotExists()V
    .registers 2

    #@0
    .prologue
    .line 446
    iget-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 447
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 449
    :cond_a
    return-void
.end method

.method private isOffScreen(Landroid/view/View;)Z
    .registers 4
    .parameter "descendant"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1265
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ScrollView;->isWithinDeltaOfScreen(Landroid/view/View;II)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_c

    #@b
    const/4 v0, 0x1

    #@c
    :cond_c
    return v0
.end method

.method private static isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z
    .registers 5
    .parameter "child"
    .parameter "parent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1671
    if-ne p0, p1, :cond_4

    #@3
    .line 1676
    :cond_3
    :goto_3
    return v1

    #@4
    .line 1675
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@7
    move-result-object v0

    #@8
    .line 1676
    .local v0, theParent:Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@a
    if-eqz v2, :cond_14

    #@c
    check-cast v0, Landroid/view/View;

    #@e
    .end local v0           #theParent:Landroid/view/ViewParent;
    invoke-static {v0, p1}, Landroid/widget/ScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_3

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_3
.end method

.method private isWithinDeltaOfScreen(Landroid/view/View;II)Z
    .registers 6
    .parameter "descendant"
    .parameter "delta"
    .parameter "height"

    #@0
    .prologue
    .line 1273
    iget-object v0, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@5
    .line 1274
    iget-object v0, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {p0, p1, v0}, Landroid/widget/ScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@a
    .line 1276
    iget-object v0, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@e
    add-int/2addr v0, p2

    #@f
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@12
    move-result v1

    #@13
    if-lt v0, v1, :cond_23

    #@15
    iget-object v0, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@17
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@19
    sub-int/2addr v0, p2

    #@1a
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@1d
    move-result v1

    #@1e
    add-int/2addr v1, p3

    #@1f
    if-gt v0, v1, :cond_23

    #@21
    const/4 v0, 0x1

    #@22
    :goto_22
    return v0

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_22
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    .line 883
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v3

    #@4
    const v4, 0xff00

    #@7
    and-int/2addr v3, v4

    #@8
    shr-int/lit8 v2, v3, 0x8

    #@a
    .line 885
    .local v2, pointerIndex:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@d
    move-result v1

    #@e
    .line 886
    .local v1, pointerId:I
    iget v3, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@10
    if-ne v1, v3, :cond_2b

    #@12
    .line 890
    if-nez v2, :cond_2c

    #@14
    const/4 v0, 0x1

    #@15
    .line 891
    .local v0, newPointerIndex:I
    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@18
    move-result v3

    #@19
    float-to-int v3, v3

    #@1a
    iput v3, p0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@1c
    .line 892
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1f
    move-result v3

    #@20
    iput v3, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@22
    .line 893
    iget-object v3, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@24
    if-eqz v3, :cond_2b

    #@26
    .line 894
    iget-object v3, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@28
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@2b
    .line 897
    .end local v0           #newPointerIndex:I
    :cond_2b
    return-void

    #@2c
    .line 890
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_15
.end method

.method private recycleVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 453
    iget-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@9
    .line 454
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    .line 456
    :cond_c
    return-void
.end method

.method private scrollAndFocus(III)Z
    .registers 12
    .parameter "direction"
    .parameter "top"
    .parameter "bottom"

    #@0
    .prologue
    .line 1178
    const/4 v3, 0x1

    #@1
    .line 1180
    .local v3, handled:Z
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@4
    move-result v4

    #@5
    .line 1181
    .local v4, height:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@8
    move-result v1

    #@9
    .line 1182
    .local v1, containerTop:I
    add-int v0, v1, v4

    #@b
    .line 1183
    .local v0, containerBottom:I
    const/16 v7, 0x21

    #@d
    if-ne p1, v7, :cond_26

    #@f
    const/4 v6, 0x1

    #@10
    .line 1185
    .local v6, up:Z
    :goto_10
    invoke-direct {p0, v6, p2, p3}, Landroid/widget/ScrollView;->findFocusableViewInBounds(ZII)Landroid/view/View;

    #@13
    move-result-object v5

    #@14
    .line 1186
    .local v5, newFocused:Landroid/view/View;
    if-nez v5, :cond_17

    #@16
    .line 1187
    move-object v5, p0

    #@17
    .line 1190
    :cond_17
    if-lt p2, v1, :cond_28

    #@19
    if-gt p3, v0, :cond_28

    #@1b
    .line 1191
    const/4 v3, 0x0

    #@1c
    .line 1197
    :goto_1c
    invoke-virtual {p0}, Landroid/widget/ScrollView;->findFocus()Landroid/view/View;

    #@1f
    move-result-object v7

    #@20
    if-eq v5, v7, :cond_25

    #@22
    invoke-virtual {v5, p1}, Landroid/view/View;->requestFocus(I)Z

    #@25
    .line 1199
    :cond_25
    return v3

    #@26
    .line 1183
    .end local v5           #newFocused:Landroid/view/View;
    .end local v6           #up:Z
    :cond_26
    const/4 v6, 0x0

    #@27
    goto :goto_10

    #@28
    .line 1193
    .restart local v5       #newFocused:Landroid/view/View;
    .restart local v6       #up:Z
    :cond_28
    if-eqz v6, :cond_30

    #@2a
    sub-int v2, p2, v1

    #@2c
    .line 1194
    .local v2, delta:I
    :goto_2c
    invoke-direct {p0, v2}, Landroid/widget/ScrollView;->doScrollY(I)V

    #@2f
    goto :goto_1c

    #@30
    .line 1193
    .end local v2           #delta:I
    :cond_30
    sub-int v2, p3, v0

    #@32
    goto :goto_2c
.end method

.method private scrollToChild(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 1458
    iget-object v1, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@5
    .line 1461
    iget-object v1, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {p0, p1, v1}, Landroid/widget/ScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@a
    .line 1463
    iget-object v1, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@f
    move-result v0

    #@10
    .line 1465
    .local v0, scrollDelta:I
    if-eqz v0, :cond_16

    #@12
    .line 1466
    const/4 v1, 0x0

    #@13
    invoke-virtual {p0, v1, v0}, Landroid/widget/ScrollView;->scrollBy(II)V

    #@16
    .line 1468
    :cond_16
    return-void
.end method

.method private scrollToChildRect(Landroid/graphics/Rect;Z)Z
    .registers 6
    .parameter "rect"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1479
    invoke-virtual {p0, p1}, Landroid/widget/ScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@4
    move-result v0

    #@5
    .line 1480
    .local v0, delta:I
    if-eqz v0, :cond_10

    #@7
    const/4 v1, 0x1

    #@8
    .line 1481
    .local v1, scroll:Z
    :goto_8
    if-eqz v1, :cond_f

    #@a
    .line 1482
    if-eqz p2, :cond_12

    #@c
    .line 1483
    invoke-virtual {p0, v2, v0}, Landroid/widget/ScrollView;->scrollBy(II)V

    #@f
    .line 1488
    :cond_f
    :goto_f
    return v1

    #@10
    .end local v1           #scroll:Z
    :cond_10
    move v1, v2

    #@11
    .line 1480
    goto :goto_8

    #@12
    .line 1485
    .restart local v1       #scroll:Z
    :cond_12
    invoke-virtual {p0, v2, v0}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    #@15
    goto :goto_f
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 250
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "ScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 254
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@11
    .line 255
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .registers 5
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 259
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 260
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "ScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 263
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    #@11
    .line 264
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 278
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "ScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 281
    :cond_e
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 282
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 268
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 269
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "ScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 272
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 273
    return-void
.end method

.method public arrowScroll(I)Z
    .registers 13
    .parameter "direction"

    #@0
    .prologue
    const/16 v10, 0x82

    #@2
    const/4 v7, 0x0

    #@3
    .line 1211
    invoke-virtual {p0}, Landroid/widget/ScrollView;->findFocus()Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    .line 1212
    .local v0, currentFocused:Landroid/view/View;
    if-ne v0, p0, :cond_a

    #@9
    const/4 v0, 0x0

    #@a
    .line 1214
    :cond_a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v8, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@11
    move-result-object v4

    #@12
    .line 1216
    .local v4, nextFocused:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getMaxScrollAmount()I

    #@15
    move-result v3

    #@16
    .line 1218
    .local v3, maxJump:I
    if-eqz v4, :cond_57

    #@18
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@1b
    move-result v8

    #@1c
    invoke-direct {p0, v4, v3, v8}, Landroid/widget/ScrollView;->isWithinDeltaOfScreen(Landroid/view/View;II)Z

    #@1f
    move-result v8

    #@20
    if-eqz v8, :cond_57

    #@22
    .line 1219
    iget-object v7, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@24
    invoke-virtual {v4, v7}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@27
    .line 1220
    iget-object v7, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@29
    invoke-virtual {p0, v4, v7}, Landroid/widget/ScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@2c
    .line 1221
    iget-object v7, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2e
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@31
    move-result v6

    #@32
    .line 1222
    .local v6, scrollDelta:I
    invoke-direct {p0, v6}, Landroid/widget/ScrollView;->doScrollY(I)V

    #@35
    .line 1223
    invoke-virtual {v4, p1}, Landroid/view/View;->requestFocus(I)Z

    #@38
    .line 1245
    :goto_38
    if-eqz v0, :cond_55

    #@3a
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_55

    #@40
    invoke-direct {p0, v0}, Landroid/widget/ScrollView;->isOffScreen(Landroid/view/View;)Z

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_55

    #@46
    .line 1252
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getDescendantFocusability()I

    #@49
    move-result v2

    #@4a
    .line 1253
    .local v2, descendantFocusability:I
    const/high16 v7, 0x2

    #@4c
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->setDescendantFocusability(I)V

    #@4f
    .line 1254
    invoke-virtual {p0}, Landroid/widget/ScrollView;->requestFocus()Z

    #@52
    .line 1255
    invoke-virtual {p0, v2}, Landroid/widget/ScrollView;->setDescendantFocusability(I)V

    #@55
    .line 1257
    .end local v2           #descendantFocusability:I
    :cond_55
    const/4 v7, 0x1

    #@56
    :cond_56
    return v7

    #@57
    .line 1226
    .end local v6           #scrollDelta:I
    :cond_57
    move v6, v3

    #@58
    .line 1228
    .restart local v6       #scrollDelta:I
    const/16 v8, 0x21

    #@5a
    if-ne p1, v8, :cond_6f

    #@5c
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@5f
    move-result v8

    #@60
    if-ge v8, v6, :cond_6f

    #@62
    .line 1229
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@65
    move-result v6

    #@66
    .line 1239
    :cond_66
    :goto_66
    if-eqz v6, :cond_56

    #@68
    .line 1242
    if-ne p1, v10, :cond_93

    #@6a
    move v7, v6

    #@6b
    :goto_6b
    invoke-direct {p0, v7}, Landroid/widget/ScrollView;->doScrollY(I)V

    #@6e
    goto :goto_38

    #@6f
    .line 1230
    :cond_6f
    if-ne p1, v10, :cond_66

    #@71
    .line 1231
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@74
    move-result v8

    #@75
    if-lez v8, :cond_66

    #@77
    .line 1232
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    #@7e
    move-result v1

    #@7f
    .line 1233
    .local v1, daBottom:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@82
    move-result v8

    #@83
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@86
    move-result v9

    #@87
    add-int/2addr v8, v9

    #@88
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@8a
    sub-int v5, v8, v9

    #@8c
    .line 1234
    .local v5, screenBottom:I
    sub-int v8, v1, v5

    #@8e
    if-ge v8, v3, :cond_66

    #@90
    .line 1235
    sub-int v6, v1, v5

    #@92
    goto :goto_66

    #@93
    .line 1242
    .end local v1           #daBottom:I
    .end local v5           #screenBottom:I
    :cond_93
    neg-int v7, v6

    #@94
    goto :goto_6b
.end method

.method public computeScroll()V
    .registers 15

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1399
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@4
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_6e

    #@a
    .line 1416
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@c
    .line 1417
    .local v3, oldX:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@e
    .line 1418
    .local v4, oldY:I
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@10
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    #@13
    move-result v12

    #@14
    .line 1419
    .local v12, x:I
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@16
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    #@19
    move-result v13

    #@1a
    .line 1421
    .local v13, y:I
    if-ne v3, v12, :cond_1e

    #@1c
    if-eq v4, v13, :cond_51

    #@1e
    .line 1422
    :cond_1e
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@21
    move-result v6

    #@22
    .line 1423
    .local v6, range:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getOverScrollMode()I

    #@25
    move-result v11

    #@26
    .line 1424
    .local v11, overscrollMode:I
    if-eqz v11, :cond_2c

    #@28
    if-ne v11, v10, :cond_5b

    #@2a
    if-lez v6, :cond_5b

    #@2c
    .line 1427
    .local v10, canOverscroll:Z
    :cond_2c
    :goto_2c
    sub-int v1, v12, v3

    #@2e
    sub-int v2, v13, v4

    #@30
    iget v8, p0, Landroid/widget/ScrollView;->mOverflingDistance:I

    #@32
    move-object v0, p0

    #@33
    move v7, v5

    #@34
    move v9, v5

    #@35
    invoke-virtual/range {v0 .. v9}, Landroid/widget/ScrollView;->overScrollBy(IIIIIIIIZ)Z

    #@38
    .line 1429
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@3a
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@3c
    invoke-virtual {p0, v0, v1, v3, v4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    #@3f
    .line 1431
    if-eqz v10, :cond_51

    #@41
    .line 1432
    if-gez v13, :cond_5d

    #@43
    if-ltz v4, :cond_5d

    #@45
    .line 1433
    iget-object v0, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@47
    iget-object v1, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@49
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@4c
    move-result v1

    #@4d
    float-to-int v1, v1

    #@4e
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@51
    .line 1440
    .end local v6           #range:I
    .end local v10           #canOverscroll:Z
    .end local v11           #overscrollMode:I
    :cond_51
    :goto_51
    invoke-virtual {p0}, Landroid/widget/ScrollView;->awakenScrollBars()Z

    #@54
    move-result v0

    #@55
    if-nez v0, :cond_5a

    #@57
    .line 1442
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@5a
    .line 1450
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    .end local v12           #x:I
    .end local v13           #y:I
    :cond_5a
    :goto_5a
    return-void

    #@5b
    .restart local v3       #oldX:I
    .restart local v4       #oldY:I
    .restart local v6       #range:I
    .restart local v11       #overscrollMode:I
    .restart local v12       #x:I
    .restart local v13       #y:I
    :cond_5b
    move v10, v5

    #@5c
    .line 1424
    goto :goto_2c

    #@5d
    .line 1434
    .restart local v10       #canOverscroll:Z
    :cond_5d
    if-le v13, v6, :cond_51

    #@5f
    if-gt v4, v6, :cond_51

    #@61
    .line 1435
    iget-object v0, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@63
    iget-object v1, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@65
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@68
    move-result v1

    #@69
    float-to-int v1, v1

    #@6a
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@6d
    goto :goto_51

    #@6e
    .line 1445
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    .end local v6           #range:I
    .end local v10           #canOverscroll:Z
    .end local v11           #overscrollMode:I
    .end local v12           #x:I
    .end local v13           #y:I
    :cond_6e
    iget-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@70
    if-eqz v0, :cond_5a

    #@72
    .line 1446
    iget-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@74
    invoke-virtual {v0}, Landroid/os/StrictMode$Span;->finish()V

    #@77
    .line 1447
    const/4 v0, 0x0

    #@78
    iput-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@7a
    goto :goto_5a
.end method

.method protected computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I
    .registers 12
    .parameter "rect"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1500
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@4
    move-result v8

    #@5
    if-nez v8, :cond_9

    #@7
    move v6, v7

    #@8
    .line 1554
    :cond_8
    :goto_8
    return v6

    #@9
    .line 1502
    :cond_9
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@c
    move-result v3

    #@d
    .line 1503
    .local v3, height:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@10
    move-result v5

    #@11
    .line 1504
    .local v5, screenTop:I
    add-int v4, v5, v3

    #@13
    .line 1506
    .local v4, screenBottom:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getVerticalFadingEdgeLength()I

    #@16
    move-result v2

    #@17
    .line 1509
    .local v2, fadingEdge:I
    iget v8, p1, Landroid/graphics/Rect;->top:I

    #@19
    if-lez v8, :cond_1c

    #@1b
    .line 1510
    add-int/2addr v5, v2

    #@1c
    .line 1514
    :cond_1c
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@1e
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    #@25
    move-result v9

    #@26
    if-ge v8, v9, :cond_29

    #@28
    .line 1515
    sub-int/2addr v4, v2

    #@29
    .line 1518
    :cond_29
    const/4 v6, 0x0

    #@2a
    .line 1520
    .local v6, scrollYDelta:I
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@2c
    if-le v8, v4, :cond_50

    #@2e
    iget v8, p1, Landroid/graphics/Rect;->top:I

    #@30
    if-le v8, v5, :cond_50

    #@32
    .line 1525
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@35
    move-result v8

    #@36
    if-le v8, v3, :cond_4b

    #@38
    .line 1527
    iget v8, p1, Landroid/graphics/Rect;->top:I

    #@3a
    sub-int/2addr v8, v5

    #@3b
    add-int/2addr v6, v8

    #@3c
    .line 1534
    :goto_3c
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@43
    move-result v0

    #@44
    .line 1535
    .local v0, bottom:I
    sub-int v1, v0, v4

    #@46
    .line 1536
    .local v1, distanceToBottom:I
    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    #@49
    move-result v6

    #@4a
    .line 1538
    goto :goto_8

    #@4b
    .line 1530
    .end local v0           #bottom:I
    .end local v1           #distanceToBottom:I
    :cond_4b
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@4d
    sub-int/2addr v8, v4

    #@4e
    add-int/2addr v6, v8

    #@4f
    goto :goto_3c

    #@50
    .line 1538
    :cond_50
    iget v7, p1, Landroid/graphics/Rect;->top:I

    #@52
    if-ge v7, v5, :cond_8

    #@54
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    #@56
    if-ge v7, v4, :cond_8

    #@58
    .line 1543
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@5b
    move-result v7

    #@5c
    if-le v7, v3, :cond_6d

    #@5e
    .line 1545
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    #@60
    sub-int v7, v4, v7

    #@62
    sub-int/2addr v6, v7

    #@63
    .line 1552
    :goto_63
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@66
    move-result v7

    #@67
    neg-int v7, v7

    #@68
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@6b
    move-result v6

    #@6c
    goto :goto_8

    #@6d
    .line 1548
    :cond_6d
    iget v7, p1, Landroid/graphics/Rect;->top:I

    #@6f
    sub-int v7, v5, v7

    #@71
    sub-int/2addr v6, v7

    #@72
    goto :goto_63
.end method

.method protected computeVerticalScrollOffset()I
    .registers 3

    #@0
    .prologue
    .line 1365
    const/4 v0, 0x0

    #@1
    invoke-super {p0}, Landroid/widget/FrameLayout;->computeVerticalScrollOffset()I

    #@4
    move-result v1

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1345
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@4
    move-result v1

    #@5
    .line 1346
    .local v1, count:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@8
    move-result v5

    #@9
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@b
    sub-int/2addr v5, v6

    #@c
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@e
    sub-int v0, v5, v6

    #@10
    .line 1347
    .local v0, contentHeight:I
    if-nez v1, :cond_13

    #@12
    .line 1360
    .end local v0           #contentHeight:I
    :goto_12
    return v0

    #@13
    .line 1351
    .restart local v0       #contentHeight:I
    :cond_13
    invoke-virtual {p0, v7}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@1a
    move-result v3

    #@1b
    .line 1352
    .local v3, scrollRange:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@1d
    .line 1353
    .local v4, scrollY:I
    sub-int v5, v3, v0

    #@1f
    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    #@22
    move-result v2

    #@23
    .line 1354
    .local v2, overscrollBottom:I
    if-gez v4, :cond_28

    #@25
    .line 1355
    sub-int/2addr v3, v4

    #@26
    :cond_26
    :goto_26
    move v0, v3

    #@27
    .line 1360
    goto :goto_12

    #@28
    .line 1356
    :cond_28
    if-le v4, v2, :cond_26

    #@2a
    .line 1357
    sub-int v5, v4, v2

    #@2c
    add-int/2addr v3, v5

    #@2d
    goto :goto_26
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 372
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0, p1}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "canvas"

    #@0
    .prologue
    .line 1753
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    #@3
    .line 1754
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@5
    if-eqz v4, :cond_8a

    #@7
    .line 1755
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@9
    .line 1756
    .local v2, scrollY:I
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@b
    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_43

    #@11
    .line 1757
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@14
    move-result v1

    #@15
    .line 1758
    .local v1, restoreCount:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getWidth()I

    #@18
    move-result v4

    #@19
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@1b
    sub-int/2addr v4, v5

    #@1c
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@1e
    sub-int v3, v4, v5

    #@20
    .line 1760
    .local v3, width:I
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@22
    int-to-float v4, v4

    #@23
    const/4 v5, 0x0

    #@24
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    #@27
    move-result v5

    #@28
    int-to-float v5, v5

    #@29
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@2c
    .line 1761
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@2e
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@31
    move-result v5

    #@32
    invoke-virtual {v4, v3, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@35
    .line 1762
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@37
    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@3a
    move-result v4

    #@3b
    if-eqz v4, :cond_40

    #@3d
    .line 1763
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@40
    .line 1765
    :cond_40
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@43
    .line 1767
    .end local v1           #restoreCount:I
    .end local v3           #width:I
    :cond_43
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@45
    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@48
    move-result v4

    #@49
    if-nez v4, :cond_8a

    #@4b
    .line 1768
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@4e
    move-result v1

    #@4f
    .line 1769
    .restart local v1       #restoreCount:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getWidth()I

    #@52
    move-result v4

    #@53
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@55
    sub-int/2addr v4, v5

    #@56
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@58
    sub-int v3, v4, v5

    #@5a
    .line 1770
    .restart local v3       #width:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@5d
    move-result v0

    #@5e
    .line 1772
    .local v0, height:I
    neg-int v4, v3

    #@5f
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@61
    add-int/2addr v4, v5

    #@62
    int-to-float v4, v4

    #@63
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@66
    move-result v5

    #@67
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    #@6a
    move-result v5

    #@6b
    add-int/2addr v5, v0

    #@6c
    int-to-float v5, v5

    #@6d
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@70
    .line 1774
    const/high16 v4, 0x4334

    #@72
    int-to-float v5, v3

    #@73
    const/4 v6, 0x0

    #@74
    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@77
    .line 1775
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@79
    invoke-virtual {v4, v3, v0}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@7c
    .line 1776
    iget-object v4, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@7e
    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@81
    move-result v4

    #@82
    if-eqz v4, :cond_87

    #@84
    .line 1777
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@87
    .line 1779
    :cond_87
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@8a
    .line 1782
    .end local v0           #height:I
    .end local v1           #restoreCount:I
    .end local v2           #scrollY:I
    .end local v3           #width:I
    :cond_8a
    return-void
.end method

.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v4, 0x21

    #@3
    const/16 v5, 0x82

    #@5
    .line 384
    iget-object v6, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {v6}, Landroid/graphics/Rect;->setEmpty()V

    #@a
    .line 386
    invoke-direct {p0}, Landroid/widget/ScrollView;->canScroll()Z

    #@d
    move-result v6

    #@e
    if-nez v6, :cond_38

    #@10
    .line 387
    invoke-virtual {p0}, Landroid/widget/ScrollView;->isFocused()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_37

    #@16
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@19
    move-result v4

    #@1a
    const/4 v6, 0x4

    #@1b
    if-eq v4, v6, :cond_37

    #@1d
    .line 388
    invoke-virtual {p0}, Landroid/widget/ScrollView;->findFocus()Landroid/view/View;

    #@20
    move-result-object v0

    #@21
    .line 389
    .local v0, currentFocused:Landroid/view/View;
    if-ne v0, p0, :cond_24

    #@23
    const/4 v0, 0x0

    #@24
    .line 390
    :cond_24
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, p0, v0, v5}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@2b
    move-result-object v2

    #@2c
    .line 392
    .local v2, nextFocused:Landroid/view/View;
    if-eqz v2, :cond_37

    #@2e
    if-eq v2, p0, :cond_37

    #@30
    invoke-virtual {v2, v5}, Landroid/view/View;->requestFocus(I)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_37

    #@36
    const/4 v3, 0x1

    #@37
    .line 422
    .end local v0           #currentFocused:Landroid/view/View;
    .end local v2           #nextFocused:Landroid/view/View;
    :cond_37
    :goto_37
    return v3

    #@38
    .line 399
    :cond_38
    const/4 v1, 0x0

    #@39
    .line 400
    .local v1, handled:Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3c
    move-result v3

    #@3d
    if-nez v3, :cond_46

    #@3f
    .line 401
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@42
    move-result v3

    #@43
    sparse-switch v3, :sswitch_data_76

    #@46
    :cond_46
    :goto_46
    move v3, v1

    #@47
    .line 422
    goto :goto_37

    #@48
    .line 403
    :sswitch_48
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_53

    #@4e
    .line 404
    invoke-virtual {p0, v4}, Landroid/widget/ScrollView;->arrowScroll(I)Z

    #@51
    move-result v1

    #@52
    goto :goto_46

    #@53
    .line 406
    :cond_53
    invoke-virtual {p0, v4}, Landroid/widget/ScrollView;->fullScroll(I)Z

    #@56
    move-result v1

    #@57
    .line 408
    goto :goto_46

    #@58
    .line 410
    :sswitch_58
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@5b
    move-result v3

    #@5c
    if-nez v3, :cond_63

    #@5e
    .line 411
    invoke-virtual {p0, v5}, Landroid/widget/ScrollView;->arrowScroll(I)Z

    #@61
    move-result v1

    #@62
    goto :goto_46

    #@63
    .line 413
    :cond_63
    invoke-virtual {p0, v5}, Landroid/widget/ScrollView;->fullScroll(I)Z

    #@66
    move-result v1

    #@67
    .line 415
    goto :goto_46

    #@68
    .line 417
    :sswitch_68
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@6b
    move-result v3

    #@6c
    if-eqz v3, :cond_73

    #@6e
    move v3, v4

    #@6f
    :goto_6f
    invoke-virtual {p0, v3}, Landroid/widget/ScrollView;->pageScroll(I)Z

    #@72
    goto :goto_46

    #@73
    :cond_73
    move v3, v5

    #@74
    goto :goto_6f

    #@75
    .line 401
    nop

    #@76
    :sswitch_data_76
    .sparse-switch
        0x13 -> :sswitch_48
        0x14 -> :sswitch_58
        0x3e -> :sswitch_68
    .end sparse-switch
.end method

.method public fling(I)V
    .registers 15
    .parameter "velocityY"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1687
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@4
    move-result v0

    #@5
    if-lez v0, :cond_3f

    #@7
    .line 1688
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@a
    move-result v0

    #@b
    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    #@d
    sub-int/2addr v0, v1

    #@e
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@10
    sub-int v12, v0, v1

    #@12
    .line 1689
    .local v12, height:I
    invoke-virtual {p0, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@19
    move-result v11

    #@1a
    .line 1691
    .local v11, bottom:I
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@1c
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@1e
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@20
    sub-int v4, v11, v12

    #@22
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@25
    move-result v8

    #@26
    div-int/lit8 v10, v12, 0x2

    #@28
    move v4, p1

    #@29
    move v5, v3

    #@2a
    move v6, v3

    #@2b
    move v7, v3

    #@2c
    move v9, v3

    #@2d
    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@30
    .line 1694
    iget-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@32
    if-nez v0, :cond_3c

    #@34
    .line 1695
    const-string v0, "ScrollView-fling"

    #@36
    invoke-static {v0}, Landroid/os/StrictMode;->enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@3c
    .line 1698
    :cond_3c
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@3f
    .line 1700
    .end local v11           #bottom:I
    .end local v12           #height:I
    :cond_3f
    return-void
.end method

.method public fullScroll(I)Z
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1147
    const/16 v5, 0x82

    #@3
    if-ne p1, v5, :cond_41

    #@5
    const/4 v1, 0x1

    #@6
    .line 1148
    .local v1, down:Z
    :goto_6
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@9
    move-result v2

    #@a
    .line 1150
    .local v2, height:I
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    iput v4, v5, Landroid/graphics/Rect;->top:I

    #@e
    .line 1151
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@10
    iput v2, v4, Landroid/graphics/Rect;->bottom:I

    #@12
    .line 1153
    if-eqz v1, :cond_34

    #@14
    .line 1154
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@17
    move-result v0

    #@18
    .line 1155
    .local v0, count:I
    if-lez v0, :cond_34

    #@1a
    .line 1156
    add-int/lit8 v4, v0, -0x1

    #@1c
    invoke-virtual {p0, v4}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@1f
    move-result-object v3

    #@20
    .line 1157
    .local v3, view:Landroid/view/View;
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@25
    move-result v5

    #@26
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@28
    add-int/2addr v5, v6

    #@29
    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    #@2b
    .line 1158
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2d
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2f
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@31
    sub-int/2addr v5, v2

    #@32
    iput v5, v4, Landroid/graphics/Rect;->top:I

    #@34
    .line 1162
    .end local v0           #count:I
    .end local v3           #view:Landroid/view/View;
    :cond_34
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@36
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@38
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@3a
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@3c
    invoke-direct {p0, p1, v4, v5}, Landroid/widget/ScrollView;->scrollAndFocus(III)Z

    #@3f
    move-result v4

    #@40
    return v4

    #@41
    .end local v1           #down:Z
    .end local v2           #height:I
    :cond_41
    move v1, v4

    #@42
    .line 1147
    goto :goto_6
.end method

.method protected getBottomFadingEdgeStrength()F
    .registers 6

    #@0
    .prologue
    .line 212
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_8

    #@6
    .line 213
    const/4 v3, 0x0

    #@7
    .line 223
    :goto_7
    return v3

    #@8
    .line 216
    :cond_8
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getVerticalFadingEdgeLength()I

    #@b
    move-result v1

    #@c
    .line 217
    .local v1, length:I
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@f
    move-result v3

    #@10
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@12
    sub-int v0, v3, v4

    #@14
    .line 218
    .local v0, bottomEdge:I
    const/4 v3, 0x0

    #@15
    invoke-virtual {p0, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@1c
    move-result v3

    #@1d
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@1f
    sub-int/2addr v3, v4

    #@20
    sub-int v2, v3, v0

    #@22
    .line 219
    .local v2, span:I
    if-ge v2, v1, :cond_28

    #@24
    .line 220
    int-to-float v3, v2

    #@25
    int-to-float v4, v1

    #@26
    div-float/2addr v3, v4

    #@27
    goto :goto_7

    #@28
    .line 223
    :cond_28
    const/high16 v3, 0x3f80

    #@2a
    goto :goto_7
.end method

.method public getMaxScrollAmount()I
    .registers 4

    #@0
    .prologue
    .line 231
    const/high16 v0, 0x3f00

    #@2
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@4
    iget v2, p0, Landroid/view/View;->mTop:I

    #@6
    sub-int/2addr v1, v2

    #@7
    int-to-float v1, v1

    #@8
    mul-float/2addr v0, v1

    #@9
    float-to-int v0, v0

    #@a
    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .registers 4

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    .line 199
    const/4 v1, 0x0

    #@7
    .line 207
    :goto_7
    return v1

    #@8
    .line 202
    :cond_8
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getVerticalFadingEdgeLength()I

    #@b
    move-result v0

    #@c
    .line 203
    .local v0, length:I
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@e
    if-ge v1, v0, :cond_16

    #@10
    .line 204
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@12
    int-to-float v1, v1

    #@13
    int-to-float v2, v0

    #@14
    div-float/2addr v1, v2

    #@15
    goto :goto_7

    #@16
    .line 207
    :cond_16
    const/high16 v1, 0x3f80

    #@18
    goto :goto_7
.end method

.method public isFillViewport()Z
    .registers 2

    #@0
    .prologue
    .line 304
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mFillViewport:Z

    #@2
    return v0
.end method

.method public isSmoothScrollingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 327
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mSmoothScrollingEnabled:Z

    #@2
    return v0
.end method

.method protected measureChild(Landroid/view/View;II)V
    .registers 10
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "parentHeightMeasureSpec"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1370
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v2

    #@5
    .line 1375
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@7
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@9
    add-int/2addr v3, v4

    #@a
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@c
    invoke-static {p2, v3, v4}, Landroid/widget/ScrollView;->getChildMeasureSpec(III)I

    #@f
    move-result v1

    #@10
    .line 1378
    .local v1, childWidthMeasureSpec:I
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result v0

    #@14
    .line 1380
    .local v0, childHeightMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@17
    .line 1381
    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .registers 11
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "widthUsed"
    .parameter "parentHeightMeasureSpec"
    .parameter "heightUsed"

    #@0
    .prologue
    .line 1386
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v2

    #@4
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    #@6
    .line 1388
    .local v2, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@8
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    add-int/2addr v3, v4

    #@b
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@d
    add-int/2addr v3, v4

    #@e
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@10
    add-int/2addr v3, v4

    #@11
    add-int/2addr v3, p3

    #@12
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@14
    invoke-static {p2, v3, v4}, Landroid/widget/ScrollView;->getChildMeasureSpec(III)I

    #@17
    move-result v1

    #@18
    .line 1391
    .local v1, childWidthMeasureSpec:I
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1a
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@1c
    add-int/2addr v3, v4

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@21
    move-result v0

    #@22
    .line 1394
    .local v0, childHeightMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@25
    .line 1395
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1622
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    #@4
    .line 1624
    iget-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 1625
    iget-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@a
    invoke-virtual {v0}, Landroid/os/StrictMode$Span;->finish()V

    #@d
    .line 1626
    iput-object v1, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@f
    .line 1628
    :cond_f
    iget-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@11
    if-eqz v0, :cond_1a

    #@13
    .line 1629
    iget-object v0, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@15
    invoke-virtual {v0}, Landroid/os/StrictMode$Span;->finish()V

    #@18
    .line 1630
    iput-object v1, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@1a
    .line 1632
    :cond_1a
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 901
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3
    move-result v5

    #@4
    and-int/lit8 v5, v5, 0x2

    #@6
    if-eqz v5, :cond_f

    #@8
    .line 902
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@b
    move-result v5

    #@c
    packed-switch v5, :pswitch_data_42

    #@f
    .line 925
    :cond_f
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v5

    #@13
    :goto_13
    return v5

    #@14
    .line 904
    :pswitch_14
    iget-boolean v5, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@16
    if-nez v5, :cond_f

    #@18
    .line 905
    const/16 v5, 0x9

    #@1a
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@1d
    move-result v4

    #@1e
    .line 906
    .local v4, vscroll:F
    const/4 v5, 0x0

    #@1f
    cmpl-float v5, v4, v5

    #@21
    if-eqz v5, :cond_f

    #@23
    .line 907
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getVerticalScrollFactor()F

    #@26
    move-result v5

    #@27
    mul-float/2addr v5, v4

    #@28
    float-to-int v0, v5

    #@29
    .line 908
    .local v0, delta:I
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@2c
    move-result v3

    #@2d
    .line 909
    .local v3, range:I
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@2f
    .line 910
    .local v2, oldScrollY:I
    sub-int v1, v2, v0

    #@31
    .line 911
    .local v1, newScrollY:I
    if-gez v1, :cond_3d

    #@33
    .line 912
    const/4 v1, 0x0

    #@34
    .line 916
    :cond_34
    :goto_34
    if-eq v1, v2, :cond_f

    #@36
    .line 917
    iget v5, p0, Landroid/view/View;->mScrollX:I

    #@38
    invoke-super {p0, v5, v1}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@3b
    .line 918
    const/4 v5, 0x1

    #@3c
    goto :goto_13

    #@3d
    .line 913
    :cond_3d
    if-le v1, v3, :cond_34

    #@3f
    .line 914
    move v1, v3

    #@40
    goto :goto_34

    #@41
    .line 902
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x8
        :pswitch_14
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 995
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 996
    const-class v1, Landroid/widget/ScrollView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 997
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@f
    move-result v1

    #@10
    if-lez v1, :cond_2d

    #@12
    const/4 v0, 0x1

    #@13
    .line 998
    .local v0, scrollable:Z
    :goto_13
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@16
    .line 999
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@18
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    #@1b
    .line 1000
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@1d
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    #@20
    .line 1001
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@22
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    #@25
    .line 1002
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@28
    move-result v1

    #@29
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    #@2c
    .line 1003
    return-void

    #@2d
    .line 997
    .end local v0           #scrollable:Z
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_13
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 977
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 978
    const-class v1, Landroid/widget/ScrollView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 979
    invoke-virtual {p0}, Landroid/widget/ScrollView;->isEnabled()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_2e

    #@12
    .line 980
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@15
    move-result v0

    #@16
    .line 981
    .local v0, scrollRange:I
    if-lez v0, :cond_2e

    #@18
    .line 982
    const/4 v1, 0x1

    #@19
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@1c
    .line 983
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@1e
    if-lez v1, :cond_25

    #@20
    .line 984
    const/16 v1, 0x2000

    #@22
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@25
    .line 986
    :cond_25
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@27
    if-ge v1, v0, :cond_2e

    #@29
    .line 987
    const/16 v1, 0x1000

    #@2b
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@2e
    .line 991
    .end local v0           #scrollRange:I
    :cond_2e
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "ev"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 480
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@6
    move-result v7

    #@7
    .line 481
    .local v7, action:I
    const/4 v1, 0x2

    #@8
    if-ne v7, v1, :cond_f

    #@a
    iget-boolean v1, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 589
    :goto_e
    return v0

    #@f
    .line 488
    :cond_f
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_1d

    #@15
    invoke-virtual {p0, v0}, Landroid/widget/ScrollView;->canScrollVertically(I)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_1d

    #@1b
    move v0, v3

    #@1c
    .line 489
    goto :goto_e

    #@1d
    .line 492
    :cond_1d
    and-int/lit16 v1, v7, 0xff

    #@1f
    packed-switch v1, :pswitch_data_fc

    #@22
    .line 589
    :cond_22
    :goto_22
    :pswitch_22
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@24
    goto :goto_e

    #@25
    .line 503
    :pswitch_25
    iget v8, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@27
    .line 504
    .local v8, activePointerId:I
    if-eq v8, v2, :cond_22

    #@29
    .line 509
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@2c
    move-result v11

    #@2d
    .line 510
    .local v11, pointerIndex:I
    if-ne v11, v2, :cond_4e

    #@2f
    .line 511
    const-string v0, "ScrollView"

    #@31
    new-instance v1, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v2, "Invalid pointerId="

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string v2, " in onInterceptTouchEvent"

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_22

    #@4e
    .line 516
    :cond_4e
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getY(I)F

    #@51
    move-result v1

    #@52
    float-to-int v12, v1

    #@53
    .line 517
    .local v12, y:I
    iget v1, p0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@55
    sub-int v1, v12, v1

    #@57
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@5a
    move-result v13

    #@5b
    .line 518
    .local v13, yDiff:I
    iget v1, p0, Landroid/widget/ScrollView;->mTouchSlop:I

    #@5d
    if-le v13, v1, :cond_22

    #@5f
    .line 519
    iput-boolean v0, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@61
    .line 520
    iput v12, p0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@63
    .line 521
    invoke-direct {p0}, Landroid/widget/ScrollView;->initVelocityTrackerIfNotExists()V

    #@66
    .line 522
    iget-object v1, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@68
    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@6b
    .line 523
    iget-object v1, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@6d
    if-nez v1, :cond_77

    #@6f
    .line 524
    const-string v1, "ScrollView-scroll"

    #@71
    invoke-static {v1}, Landroid/os/StrictMode;->enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;

    #@74
    move-result-object v1

    #@75
    iput-object v1, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@77
    .line 526
    :cond_77
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    #@7a
    move-result-object v10

    #@7b
    .line 527
    .local v10, parent:Landroid/view/ViewParent;
    if-eqz v10, :cond_22

    #@7d
    .line 528
    invoke-interface {v10, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@80
    goto :goto_22

    #@81
    .line 535
    .end local v8           #activePointerId:I
    .end local v10           #parent:Landroid/view/ViewParent;
    .end local v11           #pointerIndex:I
    .end local v12           #y:I
    .end local v13           #yDiff:I
    :pswitch_81
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@84
    move-result v1

    #@85
    float-to-int v12, v1

    #@86
    .line 536
    .restart local v12       #y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@89
    move-result v1

    #@8a
    float-to-int v1, v1

    #@8b
    invoke-direct {p0, v1, v12}, Landroid/widget/ScrollView;->inChild(II)Z

    #@8e
    move-result v1

    #@8f
    if-nez v1, :cond_97

    #@91
    .line 537
    iput-boolean v3, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@93
    .line 538
    invoke-direct {p0}, Landroid/widget/ScrollView;->recycleVelocityTracker()V

    #@96
    goto :goto_22

    #@97
    .line 546
    :cond_97
    iput v12, p0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@99
    .line 547
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@9c
    move-result v1

    #@9d
    iput v1, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@9f
    .line 549
    invoke-direct {p0}, Landroid/widget/ScrollView;->initOrResetVelocityTracker()V

    #@a2
    .line 550
    iget-object v1, p0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a4
    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@a7
    .line 556
    iget-object v1, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@a9
    invoke-virtual {v1}, Landroid/widget/OverScroller;->isFinished()Z

    #@ac
    move-result v1

    #@ad
    if-nez v1, :cond_b0

    #@af
    move v3, v0

    #@b0
    :cond_b0
    iput-boolean v3, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@b2
    .line 557
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@b4
    if-eqz v0, :cond_22

    #@b6
    iget-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@b8
    if-nez v0, :cond_22

    #@ba
    .line 558
    const-string v0, "ScrollView-scroll"

    #@bc
    invoke-static {v0}, Landroid/os/StrictMode;->enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;

    #@bf
    move-result-object v0

    #@c0
    iput-object v0, p0, Landroid/widget/ScrollView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@c2
    goto/16 :goto_22

    #@c4
    .line 566
    .end local v12           #y:I
    :pswitch_c4
    sget-boolean v0, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@c6
    if-eqz v0, :cond_d8

    #@c8
    .line 567
    sput v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@ca
    .line 568
    sput v3, Landroid/widget/ScrollView;->moveCounter:I

    #@cc
    .line 569
    const/4 v9, 0x0

    #@cd
    .local v9, i:I
    :goto_cd
    sget v0, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@cf
    if-ge v9, v0, :cond_d8

    #@d1
    .line 570
    iget-object v0, p0, Landroid/widget/ScrollView;->coorDirection:[I

    #@d3
    aput v3, v0, v9

    #@d5
    .line 569
    add-int/lit8 v9, v9, 0x1

    #@d7
    goto :goto_cd

    #@d8
    .line 573
    .end local v9           #i:I
    :cond_d8
    iput-boolean v3, p0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@da
    .line 574
    iput v2, p0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@dc
    .line 575
    invoke-direct {p0}, Landroid/widget/ScrollView;->recycleVelocityTracker()V

    #@df
    .line 576
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@e1
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@e3
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@e5
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@e8
    move-result v6

    #@e9
    move v4, v3

    #@ea
    move v5, v3

    #@eb
    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@ee
    move-result v0

    #@ef
    if-eqz v0, :cond_22

    #@f1
    .line 577
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@f4
    goto/16 :goto_22

    #@f6
    .line 581
    :pswitch_f6
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@f9
    goto/16 :goto_22

    #@fb
    .line 492
    nop

    #@fc
    :pswitch_data_fc
    .packed-switch 0x0
        :pswitch_81
        :pswitch_c4
        :pswitch_25
        :pswitch_c4
        :pswitch_22
        :pswitch_22
        :pswitch_f6
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1636
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    #@3
    .line 1637
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/ScrollView;->mIsLayoutDirty:Z

    #@6
    .line 1639
    iget-object v0, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@8
    if-eqz v0, :cond_17

    #@a
    iget-object v0, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@c
    invoke-static {v0, p0}, Landroid/widget/ScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 1640
    iget-object v0, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@14
    invoke-direct {p0, v0}, Landroid/widget/ScrollView;->scrollToChild(Landroid/view/View;)V

    #@17
    .line 1642
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@1a
    .line 1645
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@1c
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@1e
    invoke-virtual {p0, v0, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    #@21
    .line 1646
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 340
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@3
    .line 342
    iget-boolean v6, p0, Landroid/widget/ScrollView;->mFillViewport:Z

    #@5
    if-nez v6, :cond_8

    #@7
    .line 367
    :cond_7
    :goto_7
    return-void

    #@8
    .line 346
    :cond_8
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v4

    #@c
    .line 347
    .local v4, heightMode:I
    if-eqz v4, :cond_7

    #@e
    .line 351
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@11
    move-result v6

    #@12
    if-lez v6, :cond_7

    #@14
    .line 352
    const/4 v6, 0x0

    #@15
    invoke-virtual {p0, v6}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    .line 353
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    #@1c
    move-result v3

    #@1d
    .line 354
    .local v3, height:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@20
    move-result v6

    #@21
    if-ge v6, v3, :cond_7

    #@23
    .line 355
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@26
    move-result-object v5

    #@27
    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    #@29
    .line 357
    .local v5, lp:Landroid/widget/FrameLayout$LayoutParams;
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@2b
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@2d
    add-int/2addr v6, v7

    #@2e
    iget v7, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@30
    invoke-static {p1, v6, v7}, Landroid/widget/ScrollView;->getChildMeasureSpec(III)I

    #@33
    move-result v2

    #@34
    .line 359
    .local v2, childWidthMeasureSpec:I
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@36
    sub-int/2addr v3, v6

    #@37
    .line 360
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@39
    sub-int/2addr v3, v6

    #@3a
    .line 361
    const/high16 v6, 0x4000

    #@3c
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3f
    move-result v1

    #@40
    .line 364
    .local v1, childHeightMeasureSpec:I
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    #@43
    goto :goto_7
.end method

.method protected onOverScrolled(IIZZ)V
    .registers 12
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 932
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@3
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_25

    #@9
    .line 933
    iput p1, p0, Landroid/view/View;->mScrollX:I

    #@b
    .line 934
    iput p2, p0, Landroid/view/View;->mScrollY:I

    #@d
    .line 935
    invoke-virtual {p0}, Landroid/widget/ScrollView;->invalidateParentIfNeeded()V

    #@10
    .line 936
    if-eqz p4, :cond_21

    #@12
    .line 937
    iget-object v0, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@14
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@16
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@18
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@1b
    move-result v6

    #@1c
    move v4, v3

    #@1d
    move v5, v3

    #@1e
    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@21
    .line 943
    :cond_21
    :goto_21
    invoke-virtual {p0}, Landroid/widget/ScrollView;->awakenScrollBars()Z

    #@24
    .line 944
    return-void

    #@25
    .line 940
    :cond_25
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@28
    goto :goto_21
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 7
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1582
    const/4 v2, 0x2

    #@2
    if-ne p1, v2, :cond_14

    #@4
    .line 1583
    const/16 p1, 0x82

    #@6
    .line 1588
    :cond_6
    :goto_6
    if-nez p2, :cond_1a

    #@8
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@b
    move-result-object v2

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v2, p0, v3, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 1593
    .local v0, nextFocus:Landroid/view/View;
    :goto_11
    if-nez v0, :cond_23

    #@13
    .line 1601
    :cond_13
    :goto_13
    return v1

    #@14
    .line 1584
    .end local v0           #nextFocus:Landroid/view/View;
    :cond_14
    const/4 v2, 0x1

    #@15
    if-ne p1, v2, :cond_6

    #@17
    .line 1585
    const/16 p1, 0x21

    #@19
    goto :goto_6

    #@1a
    .line 1588
    :cond_1a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    goto :goto_11

    #@23
    .line 1597
    .restart local v0       #nextFocus:Landroid/view/View;
    :cond_23
    invoke-direct {p0, v0}, Landroid/widget/ScrollView;->isOffScreen(Landroid/view/View;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_13

    #@29
    .line 1601
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@2c
    move-result v1

    #@2d
    goto :goto_13
.end method

.method protected onSizeChanged(IIII)V
    .registers 8
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 1650
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    #@3
    .line 1652
    invoke-virtual {p0}, Landroid/widget/ScrollView;->findFocus()Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    .line 1653
    .local v0, currentFocused:Landroid/view/View;
    if-eqz v0, :cond_b

    #@9
    if-ne p0, v0, :cond_c

    #@b
    .line 1665
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1659
    :cond_c
    const/4 v2, 0x0

    #@d
    invoke-direct {p0, v0, v2, p4}, Landroid/widget/ScrollView;->isWithinDeltaOfScreen(Landroid/view/View;II)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_b

    #@13
    .line 1660
    iget-object v2, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@15
    invoke-virtual {v0, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@18
    .line 1661
    iget-object v2, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@1a
    invoke-virtual {p0, v0, v2}, Landroid/widget/ScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@1d
    .line 1662
    iget-object v2, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@1f
    invoke-virtual {p0, v2}, Landroid/widget/ScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@22
    move-result v1

    #@23
    .line 1663
    .local v1, scrollDelta:I
    invoke-direct {p0, v1}, Landroid/widget/ScrollView;->doScrollY(I)V

    #@26
    goto :goto_b
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 33
    .parameter "ev"

    #@0
    .prologue
    .line 718
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->initVelocityTrackerIfNotExists()V

    #@3
    .line 719
    move-object/from16 v0, p0

    #@5
    iget-object v3, v0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@7
    move-object/from16 v0, p1

    #@9
    invoke-virtual {v3, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@c
    .line 721
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v17

    #@10
    .line 723
    .local v17, action:I
    sget-boolean v3, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@12
    if-eqz v3, :cond_38

    #@14
    .line 724
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@17
    move-result v3

    #@18
    float-to-int v0, v3

    #@19
    move/from16 v30, v0

    #@1b
    .line 725
    .local v30, y:I
    sget v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@1d
    sget v4, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@1f
    add-int/lit8 v4, v4, -0x1

    #@21
    and-int/2addr v3, v4

    #@22
    if-nez v3, :cond_27

    #@24
    .line 726
    const/4 v3, 0x0

    #@25
    sput v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@27
    .line 728
    :cond_27
    move-object/from16 v0, p0

    #@29
    iget-object v3, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@2b
    sget v4, Landroid/widget/ScrollView;->toCompareY:I

    #@2d
    sget v6, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@2f
    add-int/lit8 v6, v6, -0x1

    #@31
    and-int/2addr v4, v6

    #@32
    aget v3, v3, v4

    #@34
    move/from16 v0, v30

    #@36
    if-ne v3, v0, :cond_41

    #@38
    .line 738
    .end local v30           #y:I
    :cond_38
    :goto_38
    move/from16 v0, v17

    #@3a
    and-int/lit16 v3, v0, 0xff

    #@3c
    packed-switch v3, :pswitch_data_2fa

    #@3f
    .line 879
    :cond_3f
    :goto_3f
    :pswitch_3f
    const/4 v3, 0x1

    #@40
    :goto_40
    return v3

    #@41
    .line 731
    .restart local v30       #y:I
    :cond_41
    move-object/from16 v0, p0

    #@43
    iget-object v3, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@45
    sget v4, Landroid/widget/ScrollView;->eventMonitor:I

    #@47
    sget v6, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@49
    add-int/lit8 v6, v6, -0x1

    #@4b
    and-int/2addr v4, v6

    #@4c
    aput v30, v3, v4

    #@4e
    .line 732
    sget v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@50
    sput v3, Landroid/widget/ScrollView;->toCompareY:I

    #@52
    .line 733
    sget v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@54
    add-int/lit8 v3, v3, 0x1

    #@56
    sput v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@58
    .line 734
    sget v3, Landroid/widget/ScrollView;->moveCounter:I

    #@5a
    add-int/lit8 v3, v3, 0x1

    #@5c
    sput v3, Landroid/widget/ScrollView;->moveCounter:I

    #@5e
    goto :goto_38

    #@5f
    .line 740
    .end local v30           #y:I
    :pswitch_5f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@62
    move-result v3

    #@63
    if-nez v3, :cond_67

    #@65
    .line 741
    const/4 v3, 0x0

    #@66
    goto :goto_40

    #@67
    .line 743
    :cond_67
    move-object/from16 v0, p0

    #@69
    iget-object v3, v0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@6b
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@6e
    move-result v3

    #@6f
    if-nez v3, :cond_bc

    #@71
    const/4 v3, 0x1

    #@72
    :goto_72
    move-object/from16 v0, p0

    #@74
    iput-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@76
    if-eqz v3, :cond_84

    #@78
    .line 744
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    #@7b
    move-result-object v27

    #@7c
    .line 745
    .local v27, parent:Landroid/view/ViewParent;
    if-eqz v27, :cond_84

    #@7e
    .line 746
    const/4 v3, 0x1

    #@7f
    move-object/from16 v0, v27

    #@81
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@84
    .line 754
    .end local v27           #parent:Landroid/view/ViewParent;
    :cond_84
    move-object/from16 v0, p0

    #@86
    iget-object v3, v0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@88
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@8b
    move-result v3

    #@8c
    if-nez v3, :cond_a7

    #@8e
    .line 755
    move-object/from16 v0, p0

    #@90
    iget-object v3, v0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@92
    invoke-virtual {v3}, Landroid/widget/OverScroller;->abortAnimation()V

    #@95
    .line 756
    move-object/from16 v0, p0

    #@97
    iget-object v3, v0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@99
    if-eqz v3, :cond_a7

    #@9b
    .line 757
    move-object/from16 v0, p0

    #@9d
    iget-object v3, v0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@9f
    invoke-virtual {v3}, Landroid/os/StrictMode$Span;->finish()V

    #@a2
    .line 758
    const/4 v3, 0x0

    #@a3
    move-object/from16 v0, p0

    #@a5
    iput-object v3, v0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@a7
    .line 763
    :cond_a7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@aa
    move-result v3

    #@ab
    float-to-int v3, v3

    #@ac
    move-object/from16 v0, p0

    #@ae
    iput v3, v0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@b0
    .line 764
    const/4 v3, 0x0

    #@b1
    move-object/from16 v0, p1

    #@b3
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@b6
    move-result v3

    #@b7
    move-object/from16 v0, p0

    #@b9
    iput v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@bb
    goto :goto_3f

    #@bc
    .line 743
    :cond_bc
    const/4 v3, 0x0

    #@bd
    goto :goto_72

    #@be
    .line 768
    :pswitch_be
    move-object/from16 v0, p0

    #@c0
    iget v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@c7
    move-result v18

    #@c8
    .line 769
    .local v18, activePointerIndex:I
    const/4 v3, -0x1

    #@c9
    move/from16 v0, v18

    #@cb
    if-ne v0, v3, :cond_f1

    #@cd
    .line 770
    const-string v3, "ScrollView"

    #@cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v6, "Invalid pointerId="

    #@d6
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    move-object/from16 v0, p0

    #@dc
    iget v6, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@de
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v4

    #@e2
    const-string v6, " in onTouchEvent"

    #@e4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v4

    #@ec
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_3f

    #@f1
    .line 774
    :cond_f1
    move-object/from16 v0, p1

    #@f3
    move/from16 v1, v18

    #@f5
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@f8
    move-result v3

    #@f9
    float-to-int v0, v3

    #@fa
    move/from16 v30, v0

    #@fc
    .line 775
    .restart local v30       #y:I
    move-object/from16 v0, p0

    #@fe
    iget v3, v0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@100
    sub-int v5, v3, v30

    #@102
    .line 776
    .local v5, deltaY:I
    move-object/from16 v0, p0

    #@104
    iget-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@106
    if-nez v3, :cond_12a

    #@108
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    #@10b
    move-result v3

    #@10c
    move-object/from16 v0, p0

    #@10e
    iget v4, v0, Landroid/widget/ScrollView;->mTouchSlop:I

    #@110
    if-le v3, v4, :cond_12a

    #@112
    .line 777
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    #@115
    move-result-object v27

    #@116
    .line 778
    .restart local v27       #parent:Landroid/view/ViewParent;
    if-eqz v27, :cond_11e

    #@118
    .line 779
    const/4 v3, 0x1

    #@119
    move-object/from16 v0, v27

    #@11b
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@11e
    .line 781
    :cond_11e
    const/4 v3, 0x1

    #@11f
    move-object/from16 v0, p0

    #@121
    iput-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@123
    .line 782
    if-lez v5, :cond_1c6

    #@125
    .line 783
    move-object/from16 v0, p0

    #@127
    iget v3, v0, Landroid/widget/ScrollView;->mTouchSlop:I

    #@129
    sub-int/2addr v5, v3

    #@12a
    .line 788
    .end local v27           #parent:Landroid/view/ViewParent;
    :cond_12a
    :goto_12a
    move-object/from16 v0, p0

    #@12c
    iget-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@12e
    if-eqz v3, :cond_3f

    #@130
    .line 790
    move/from16 v0, v30

    #@132
    move-object/from16 v1, p0

    #@134
    iput v0, v1, Landroid/widget/ScrollView;->mLastMotionY:I

    #@136
    .line 792
    move-object/from16 v0, p0

    #@138
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@13a
    move/from16 v24, v0

    #@13c
    .line 793
    .local v24, oldX:I
    move-object/from16 v0, p0

    #@13e
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@140
    move/from16 v25, v0

    #@142
    .line 794
    .local v25, oldY:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@145
    move-result v9

    #@146
    .line 795
    .local v9, range:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getOverScrollMode()I

    #@149
    move-result v26

    #@14a
    .line 796
    .local v26, overscrollMode:I
    if-eqz v26, :cond_153

    #@14c
    const/4 v3, 0x1

    #@14d
    move/from16 v0, v26

    #@14f
    if-ne v0, v3, :cond_1cd

    #@151
    if-lez v9, :cond_1cd

    #@153
    :cond_153
    const/16 v19, 0x1

    #@155
    .line 799
    .local v19, canOverscroll:Z
    :goto_155
    const/4 v4, 0x0

    #@156
    const/4 v6, 0x0

    #@157
    move-object/from16 v0, p0

    #@159
    iget v7, v0, Landroid/view/View;->mScrollY:I

    #@15b
    const/4 v8, 0x0

    #@15c
    const/4 v10, 0x0

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget v11, v0, Landroid/widget/ScrollView;->mOverscrollDistance:I

    #@161
    const/4 v12, 0x1

    #@162
    move-object/from16 v3, p0

    #@164
    invoke-virtual/range {v3 .. v12}, Landroid/widget/ScrollView;->overScrollBy(IIIIIIIIZ)Z

    #@167
    move-result v3

    #@168
    if-eqz v3, :cond_171

    #@16a
    .line 802
    move-object/from16 v0, p0

    #@16c
    iget-object v3, v0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@16e
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@171
    .line 804
    :cond_171
    move-object/from16 v0, p0

    #@173
    iget v3, v0, Landroid/view/View;->mScrollX:I

    #@175
    move-object/from16 v0, p0

    #@177
    iget v4, v0, Landroid/view/View;->mScrollY:I

    #@179
    move-object/from16 v0, p0

    #@17b
    move/from16 v1, v24

    #@17d
    move/from16 v2, v25

    #@17f
    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    #@182
    .line 806
    if-eqz v19, :cond_3f

    #@184
    .line 807
    add-int v28, v25, v5

    #@186
    .line 808
    .local v28, pulledToY:I
    if-gez v28, :cond_1d0

    #@188
    .line 809
    move-object/from16 v0, p0

    #@18a
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@18c
    int-to-float v4, v5

    #@18d
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getHeight()I

    #@190
    move-result v6

    #@191
    int-to-float v6, v6

    #@192
    div-float/2addr v4, v6

    #@193
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@196
    .line 810
    move-object/from16 v0, p0

    #@198
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@19a
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@19d
    move-result v3

    #@19e
    if-nez v3, :cond_1a7

    #@1a0
    .line 811
    move-object/from16 v0, p0

    #@1a2
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1a4
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@1a7
    .line 819
    :cond_1a7
    :goto_1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1ab
    if-eqz v3, :cond_3f

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1b1
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1b4
    move-result v3

    #@1b5
    if-eqz v3, :cond_1c1

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1bb
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1be
    move-result v3

    #@1bf
    if-nez v3, :cond_3f

    #@1c1
    .line 821
    :cond_1c1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@1c4
    goto/16 :goto_3f

    #@1c6
    .line 785
    .end local v9           #range:I
    .end local v19           #canOverscroll:Z
    .end local v24           #oldX:I
    .end local v25           #oldY:I
    .end local v26           #overscrollMode:I
    .end local v28           #pulledToY:I
    .restart local v27       #parent:Landroid/view/ViewParent;
    :cond_1c6
    move-object/from16 v0, p0

    #@1c8
    iget v3, v0, Landroid/widget/ScrollView;->mTouchSlop:I

    #@1ca
    add-int/2addr v5, v3

    #@1cb
    goto/16 :goto_12a

    #@1cd
    .line 796
    .end local v27           #parent:Landroid/view/ViewParent;
    .restart local v9       #range:I
    .restart local v24       #oldX:I
    .restart local v25       #oldY:I
    .restart local v26       #overscrollMode:I
    :cond_1cd
    const/16 v19, 0x0

    #@1cf
    goto :goto_155

    #@1d0
    .line 813
    .restart local v19       #canOverscroll:Z
    .restart local v28       #pulledToY:I
    :cond_1d0
    move/from16 v0, v28

    #@1d2
    if-le v0, v9, :cond_1a7

    #@1d4
    .line 814
    move-object/from16 v0, p0

    #@1d6
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1d8
    int-to-float v4, v5

    #@1d9
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getHeight()I

    #@1dc
    move-result v6

    #@1dd
    int-to-float v6, v6

    #@1de
    div-float/2addr v4, v6

    #@1df
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@1e2
    .line 815
    move-object/from16 v0, p0

    #@1e4
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1e6
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1e9
    move-result v3

    #@1ea
    if-nez v3, :cond_1a7

    #@1ec
    .line 816
    move-object/from16 v0, p0

    #@1ee
    iget-object v3, v0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1f0
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@1f3
    goto :goto_1a7

    #@1f4
    .line 827
    .end local v5           #deltaY:I
    .end local v9           #range:I
    .end local v18           #activePointerIndex:I
    .end local v19           #canOverscroll:Z
    .end local v24           #oldX:I
    .end local v25           #oldY:I
    .end local v26           #overscrollMode:I
    .end local v28           #pulledToY:I
    .end local v30           #y:I
    :pswitch_1f4
    move-object/from16 v0, p0

    #@1f6
    iget-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@1f8
    if-eqz v3, :cond_3f

    #@1fa
    .line 828
    move-object/from16 v0, p0

    #@1fc
    iget-object v0, v0, Landroid/widget/ScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1fe
    move-object/from16 v29, v0

    #@200
    .line 829
    .local v29, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v3, 0x3e8

    #@202
    move-object/from16 v0, p0

    #@204
    iget v4, v0, Landroid/widget/ScrollView;->mMaximumVelocity:I

    #@206
    int-to-float v4, v4

    #@207
    move-object/from16 v0, v29

    #@209
    invoke-virtual {v0, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@20c
    .line 830
    move-object/from16 v0, p0

    #@20e
    iget v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@210
    move-object/from16 v0, v29

    #@212
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@215
    move-result v3

    #@216
    float-to-int v0, v3

    #@217
    move/from16 v22, v0

    #@219
    .line 832
    .local v22, initialVelocity:I
    sget-boolean v3, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@21b
    if-eqz v3, :cond_234

    #@21d
    .line 833
    move-object/from16 v0, p0

    #@21f
    iget v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@221
    move-object/from16 v0, v29

    #@223
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@226
    move-result v3

    #@227
    float-to-int v0, v3

    #@228
    move/from16 v23, v0

    #@22a
    .line 834
    .local v23, initialXVelocity:I
    move-object/from16 v0, p0

    #@22c
    move/from16 v1, v22

    #@22e
    move/from16 v2, v23

    #@230
    invoke-direct {v0, v1, v2}, Landroid/widget/ScrollView;->getFlickValue(II)I

    #@233
    move-result v22

    #@234
    .line 837
    .end local v23           #initialXVelocity:I
    :cond_234
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@237
    move-result v3

    #@238
    if-lez v3, :cond_24c

    #@23a
    .line 838
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(I)I

    #@23d
    move-result v3

    #@23e
    move-object/from16 v0, p0

    #@240
    iget v4, v0, Landroid/widget/ScrollView;->mMinimumVelocity:I

    #@242
    if-le v3, v4, :cond_256

    #@244
    .line 839
    move/from16 v0, v22

    #@246
    neg-int v3, v0

    #@247
    move-object/from16 v0, p0

    #@249
    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->fling(I)V

    #@24c
    .line 848
    :cond_24c
    :goto_24c
    const/4 v3, -0x1

    #@24d
    move-object/from16 v0, p0

    #@24f
    iput v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@251
    .line 849
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->endDrag()V

    #@254
    goto/16 :goto_3f

    #@256
    .line 841
    :cond_256
    move-object/from16 v0, p0

    #@258
    iget-object v10, v0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@25a
    move-object/from16 v0, p0

    #@25c
    iget v11, v0, Landroid/view/View;->mScrollX:I

    #@25e
    move-object/from16 v0, p0

    #@260
    iget v12, v0, Landroid/view/View;->mScrollY:I

    #@262
    const/4 v13, 0x0

    #@263
    const/4 v14, 0x0

    #@264
    const/4 v15, 0x0

    #@265
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@268
    move-result v16

    #@269
    invoke-virtual/range {v10 .. v16}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@26c
    move-result v3

    #@26d
    if-eqz v3, :cond_24c

    #@26f
    .line 843
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@272
    goto :goto_24c

    #@273
    .line 853
    .end local v22           #initialVelocity:I
    .end local v29           #velocityTracker:Landroid/view/VelocityTracker;
    :pswitch_273
    sget-boolean v3, Landroid/widget/ScrollView;->mCapptouchFlickNoti:Z

    #@275
    if-eqz v3, :cond_28f

    #@277
    .line 854
    const/4 v3, 0x0

    #@278
    sput v3, Landroid/widget/ScrollView;->eventMonitor:I

    #@27a
    .line 855
    const/4 v3, 0x0

    #@27b
    sput v3, Landroid/widget/ScrollView;->moveCounter:I

    #@27d
    .line 856
    const/16 v20, 0x0

    #@27f
    .local v20, i:I
    :goto_27f
    sget v3, Landroid/widget/ScrollView;->COORDINATE_DIRECTION:I

    #@281
    move/from16 v0, v20

    #@283
    if-ge v0, v3, :cond_28f

    #@285
    .line 857
    move-object/from16 v0, p0

    #@287
    iget-object v3, v0, Landroid/widget/ScrollView;->coorDirection:[I

    #@289
    const/4 v4, 0x0

    #@28a
    aput v4, v3, v20

    #@28c
    .line 856
    add-int/lit8 v20, v20, 0x1

    #@28e
    goto :goto_27f

    #@28f
    .line 860
    .end local v20           #i:I
    :cond_28f
    move-object/from16 v0, p0

    #@291
    iget-boolean v3, v0, Landroid/widget/ScrollView;->mIsBeingDragged:Z

    #@293
    if-eqz v3, :cond_3f

    #@295
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@298
    move-result v3

    #@299
    if-lez v3, :cond_3f

    #@29b
    .line 861
    move-object/from16 v0, p0

    #@29d
    iget-object v10, v0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@29f
    move-object/from16 v0, p0

    #@2a1
    iget v11, v0, Landroid/view/View;->mScrollX:I

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    iget v12, v0, Landroid/view/View;->mScrollY:I

    #@2a7
    const/4 v13, 0x0

    #@2a8
    const/4 v14, 0x0

    #@2a9
    const/4 v15, 0x0

    #@2aa
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@2ad
    move-result v16

    #@2ae
    invoke-virtual/range {v10 .. v16}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@2b1
    move-result v3

    #@2b2
    if-eqz v3, :cond_2b7

    #@2b4
    .line 862
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@2b7
    .line 864
    :cond_2b7
    const/4 v3, -0x1

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    iput v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@2bc
    .line 865
    invoke-direct/range {p0 .. p0}, Landroid/widget/ScrollView;->endDrag()V

    #@2bf
    goto/16 :goto_3f

    #@2c1
    .line 869
    :pswitch_2c1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@2c4
    move-result v21

    #@2c5
    .line 870
    .local v21, index:I
    move-object/from16 v0, p1

    #@2c7
    move/from16 v1, v21

    #@2c9
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@2cc
    move-result v3

    #@2cd
    float-to-int v3, v3

    #@2ce
    move-object/from16 v0, p0

    #@2d0
    iput v3, v0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@2d2
    .line 871
    move-object/from16 v0, p1

    #@2d4
    move/from16 v1, v21

    #@2d6
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@2d9
    move-result v3

    #@2da
    move-object/from16 v0, p0

    #@2dc
    iput v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@2de
    goto/16 :goto_3f

    #@2e0
    .line 875
    .end local v21           #index:I
    :pswitch_2e0
    invoke-direct/range {p0 .. p1}, Landroid/widget/ScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@2e3
    .line 876
    move-object/from16 v0, p0

    #@2e5
    iget v3, v0, Landroid/widget/ScrollView;->mActivePointerId:I

    #@2e7
    move-object/from16 v0, p1

    #@2e9
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@2ec
    move-result v3

    #@2ed
    move-object/from16 v0, p1

    #@2ef
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getY(I)F

    #@2f2
    move-result v3

    #@2f3
    float-to-int v3, v3

    #@2f4
    move-object/from16 v0, p0

    #@2f6
    iput v3, v0, Landroid/widget/ScrollView;->mLastMotionY:I

    #@2f8
    goto/16 :goto_3f

    #@2fa
    .line 738
    :pswitch_data_2fa
    .packed-switch 0x0
        :pswitch_5f
        :pswitch_1f4
        :pswitch_be
        :pswitch_273
        :pswitch_3f
        :pswitch_2c1
        :pswitch_2e0
    .end packed-switch
.end method

.method public pageScroll(I)Z
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1111
    const/16 v5, 0x82

    #@3
    if-ne p1, v5, :cond_4b

    #@5
    const/4 v1, 0x1

    #@6
    .line 1112
    .local v1, down:Z
    :goto_6
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@9
    move-result v2

    #@a
    .line 1114
    .local v2, height:I
    if-eqz v1, :cond_4d

    #@c
    .line 1115
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@e
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@11
    move-result v5

    #@12
    add-int/2addr v5, v2

    #@13
    iput v5, v4, Landroid/graphics/Rect;->top:I

    #@15
    .line 1116
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@18
    move-result v0

    #@19
    .line 1117
    .local v0, count:I
    if-lez v0, :cond_35

    #@1b
    .line 1118
    add-int/lit8 v4, v0, -0x1

    #@1d
    invoke-virtual {p0, v4}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v3

    #@21
    .line 1119
    .local v3, view:Landroid/view/View;
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@23
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@25
    add-int/2addr v4, v2

    #@26
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@29
    move-result v5

    #@2a
    if-le v4, v5, :cond_35

    #@2c
    .line 1120
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2e
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@31
    move-result v5

    #@32
    sub-int/2addr v5, v2

    #@33
    iput v5, v4, Landroid/graphics/Rect;->top:I

    #@35
    .line 1129
    .end local v0           #count:I
    .end local v3           #view:Landroid/view/View;
    :cond_35
    :goto_35
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@37
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@39
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@3b
    add-int/2addr v5, v2

    #@3c
    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    #@3e
    .line 1131
    iget-object v4, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@40
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@42
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@44
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@46
    invoke-direct {p0, p1, v4, v5}, Landroid/widget/ScrollView;->scrollAndFocus(III)Z

    #@49
    move-result v4

    #@4a
    return v4

    #@4b
    .end local v1           #down:Z
    .end local v2           #height:I
    :cond_4b
    move v1, v4

    #@4c
    .line 1111
    goto :goto_6

    #@4d
    .line 1124
    .restart local v1       #down:Z
    .restart local v2       #height:I
    :cond_4d
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@4f
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getScrollY()I

    #@52
    move-result v6

    #@53
    sub-int/2addr v6, v2

    #@54
    iput v6, v5, Landroid/graphics/Rect;->top:I

    #@56
    .line 1125
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@58
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@5a
    if-gez v5, :cond_35

    #@5c
    .line 1126
    iget-object v5, p0, Landroid/widget/ScrollView;->mTempRect:Landroid/graphics/Rect;

    #@5e
    iput v4, v5, Landroid/graphics/Rect;->top:I

    #@60
    goto :goto_35
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 9
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 948
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_9

    #@8
    .line 972
    :goto_8
    return v2

    #@9
    .line 951
    :cond_9
    invoke-virtual {p0}, Landroid/widget/ScrollView;->isEnabled()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_11

    #@f
    move v2, v3

    #@10
    .line 952
    goto :goto_8

    #@11
    .line 954
    :cond_11
    sparse-switch p1, :sswitch_data_52

    #@14
    move v2, v3

    #@15
    .line 972
    goto :goto_8

    #@16
    .line 956
    :sswitch_16
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@19
    move-result v4

    #@1a
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@1c
    sub-int/2addr v4, v5

    #@1d
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@1f
    sub-int v1, v4, v5

    #@21
    .line 957
    .local v1, viewportHeight:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@23
    add-int/2addr v4, v1

    #@24
    invoke-direct {p0}, Landroid/widget/ScrollView;->getScrollRange()I

    #@27
    move-result v5

    #@28
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result v0

    #@2c
    .line 958
    .local v0, targetScrollY:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@2e
    if-eq v0, v4, :cond_34

    #@30
    .line 959
    invoke-virtual {p0, v3, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    #@33
    goto :goto_8

    #@34
    :cond_34
    move v2, v3

    #@35
    .line 962
    goto :goto_8

    #@36
    .line 964
    .end local v0           #targetScrollY:I
    .end local v1           #viewportHeight:I
    :sswitch_36
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@39
    move-result v4

    #@3a
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@3c
    sub-int/2addr v4, v5

    #@3d
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@3f
    sub-int v1, v4, v5

    #@41
    .line 965
    .restart local v1       #viewportHeight:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@43
    sub-int/2addr v4, v1

    #@44
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    #@47
    move-result v0

    #@48
    .line 966
    .restart local v0       #targetScrollY:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@4a
    if-eq v0, v4, :cond_50

    #@4c
    .line 967
    invoke-virtual {p0, v3, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    #@4f
    goto :goto_8

    #@50
    :cond_50
    move v2, v3

    #@51
    .line 970
    goto :goto_8

    #@52
    .line 954
    :sswitch_data_52
    .sparse-switch
        0x1000 -> :sswitch_16
        0x2000 -> :sswitch_36
    .end sparse-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 1559
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mIsLayoutDirty:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1560
    invoke-direct {p0, p2}, Landroid/widget/ScrollView;->scrollToChild(Landroid/view/View;)V

    #@7
    .line 1565
    :goto_7
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@a
    .line 1566
    return-void

    #@b
    .line 1563
    :cond_b
    iput-object p2, p0, Landroid/widget/ScrollView;->mChildToScrollTo:Landroid/view/View;

    #@d
    goto :goto_7
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 7
    .parameter "child"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 1608
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    #@10
    move-result v2

    #@11
    sub-int/2addr v1, v2

    #@12
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    #@15
    .line 1611
    invoke-direct {p0, p2, p3}, Landroid/widget/ScrollView;->scrollToChildRect(Landroid/graphics/Rect;Z)Z

    #@18
    move-result v0

    #@19
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "disallowIntercept"

    #@0
    .prologue
    .line 460
    if-eqz p1, :cond_5

    #@2
    .line 461
    invoke-direct {p0}, Landroid/widget/ScrollView;->recycleVelocityTracker()V

    #@5
    .line 463
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    #@8
    .line 464
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 1616
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/ScrollView;->mIsLayoutDirty:Z

    #@3
    .line 1617
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    #@6
    .line 1618
    return-void
.end method

.method public scrollTo(II)V
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1726
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@3
    move-result v1

    #@4
    if-lez v1, :cond_3a

    #@6
    .line 1727
    const/4 v1, 0x0

    #@7
    invoke-virtual {p0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 1728
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getWidth()I

    #@e
    move-result v1

    #@f
    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    #@11
    sub-int/2addr v1, v2

    #@12
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@14
    sub-int/2addr v1, v2

    #@15
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@18
    move-result v2

    #@19
    invoke-static {p1, v1, v2}, Landroid/widget/ScrollView;->clamp(III)I

    #@1c
    move-result p1

    #@1d
    .line 1729
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@20
    move-result v1

    #@21
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@23
    sub-int/2addr v1, v2

    #@24
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@26
    sub-int/2addr v1, v2

    #@27
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@2a
    move-result v2

    #@2b
    invoke-static {p2, v1, v2}, Landroid/widget/ScrollView;->clamp(III)I

    #@2e
    move-result p2

    #@2f
    .line 1730
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@31
    if-ne p1, v1, :cond_37

    #@33
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@35
    if-eq p2, v1, :cond_3a

    #@37
    .line 1731
    :cond_37
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@3a
    .line 1734
    .end local v0           #child:Landroid/view/View;
    :cond_3a
    return-void
.end method

.method public setFillViewport(Z)V
    .registers 3
    .parameter "fillViewport"

    #@0
    .prologue
    .line 317
    iget-boolean v0, p0, Landroid/widget/ScrollView;->mFillViewport:Z

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 318
    iput-boolean p1, p0, Landroid/widget/ScrollView;->mFillViewport:Z

    #@6
    .line 319
    invoke-virtual {p0}, Landroid/widget/ScrollView;->requestLayout()V

    #@9
    .line 321
    :cond_9
    return-void
.end method

.method public setOverScrollMode(I)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1738
    const/4 v1, 0x2

    #@2
    if-eq p1, v1, :cond_1e

    #@4
    .line 1739
    iget-object v1, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@6
    if-nez v1, :cond_1a

    #@8
    .line 1740
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    .line 1741
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/widget/EdgeEffect;

    #@e
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@11
    iput-object v1, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@13
    .line 1742
    new-instance v1, Landroid/widget/EdgeEffect;

    #@15
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@18
    iput-object v1, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1a
    .line 1748
    .end local v0           #context:Landroid/content/Context;
    :cond_1a
    :goto_1a
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOverScrollMode(I)V

    #@1d
    .line 1749
    return-void

    #@1e
    .line 1745
    :cond_1e
    iput-object v2, p0, Landroid/widget/ScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@20
    .line 1746
    iput-object v2, p0, Landroid/widget/ScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@22
    goto :goto_1a
.end method

.method public setSmoothScrollingEnabled(Z)V
    .registers 2
    .parameter "smoothScrollingEnabled"

    #@0
    .prologue
    .line 335
    iput-boolean p1, p0, Landroid/widget/ScrollView;->mSmoothScrollingEnabled:Z

    #@2
    .line 336
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 193
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public final smoothScrollBy(II)V
    .registers 14
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 1302
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getChildCount()I

    #@4
    move-result v6

    #@5
    if-nez v6, :cond_8

    #@7
    .line 1327
    :goto_7
    return-void

    #@8
    .line 1306
    :cond_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@b
    move-result-wide v6

    #@c
    iget-wide v8, p0, Landroid/widget/ScrollView;->mLastScroll:J

    #@e
    sub-long v1, v6, v8

    #@10
    .line 1307
    .local v1, duration:J
    const-wide/16 v6, 0xfa

    #@12
    cmp-long v6, v1, v6

    #@14
    if-lez v6, :cond_4e

    #@16
    .line 1308
    invoke-virtual {p0}, Landroid/widget/ScrollView;->getHeight()I

    #@19
    move-result v6

    #@1a
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@1c
    sub-int/2addr v6, v7

    #@1d
    iget v7, p0, Landroid/view/View;->mPaddingTop:I

    #@1f
    sub-int v3, v6, v7

    #@21
    .line 1309
    .local v3, height:I
    invoke-virtual {p0, v10}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    #@28
    move-result v0

    #@29
    .line 1310
    .local v0, bottom:I
    sub-int v6, v0, v3

    #@2b
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    #@2e
    move-result v4

    #@2f
    .line 1311
    .local v4, maxY:I
    iget v5, p0, Landroid/view/View;->mScrollY:I

    #@31
    .line 1312
    .local v5, scrollY:I
    add-int v6, v5, p2

    #@33
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    #@36
    move-result v6

    #@37
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    #@3a
    move-result v6

    #@3b
    sub-int p2, v6, v5

    #@3d
    .line 1314
    iget-object v6, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@3f
    iget v7, p0, Landroid/view/View;->mScrollX:I

    #@41
    invoke-virtual {v6, v7, v5, v10, p2}, Landroid/widget/OverScroller;->startScroll(IIII)V

    #@44
    .line 1315
    invoke-virtual {p0}, Landroid/widget/ScrollView;->postInvalidateOnAnimation()V

    #@47
    .line 1326
    .end local v0           #bottom:I
    .end local v3           #height:I
    .end local v4           #maxY:I
    .end local v5           #scrollY:I
    :goto_47
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@4a
    move-result-wide v6

    #@4b
    iput-wide v6, p0, Landroid/widget/ScrollView;->mLastScroll:J

    #@4d
    goto :goto_7

    #@4e
    .line 1317
    :cond_4e
    iget-object v6, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@50
    invoke-virtual {v6}, Landroid/widget/OverScroller;->isFinished()Z

    #@53
    move-result v6

    #@54
    if-nez v6, :cond_67

    #@56
    .line 1318
    iget-object v6, p0, Landroid/widget/ScrollView;->mScroller:Landroid/widget/OverScroller;

    #@58
    invoke-virtual {v6}, Landroid/widget/OverScroller;->abortAnimation()V

    #@5b
    .line 1319
    iget-object v6, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@5d
    if-eqz v6, :cond_67

    #@5f
    .line 1320
    iget-object v6, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@61
    invoke-virtual {v6}, Landroid/os/StrictMode$Span;->finish()V

    #@64
    .line 1321
    const/4 v6, 0x0

    #@65
    iput-object v6, p0, Landroid/widget/ScrollView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@67
    .line 1324
    :cond_67
    invoke-virtual {p0, p1, p2}, Landroid/widget/ScrollView;->scrollBy(II)V

    #@6a
    goto :goto_47
.end method

.method public final smoothScrollTo(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1336
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    sub-int v0, p1, v0

    #@4
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@6
    sub-int v1, p2, v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    #@b
    .line 1337
    return-void
.end method
