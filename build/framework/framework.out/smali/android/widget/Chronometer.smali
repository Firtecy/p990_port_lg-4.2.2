.class public Landroid/widget/Chronometer;
.super Landroid/widget/TextView;
.source "Chronometer.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Chronometer$OnChronometerTickListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Chronometer"

.field private static final TICK_WHAT:I = 0x2


# instance fields
.field private mBase:J

.field private mFormat:Ljava/lang/String;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mFormatterArgs:[Ljava/lang/Object;

.field private mFormatterLocale:Ljava/util/Locale;

.field private mHandler:Landroid/os/Handler;

.field private mLogged:Z

.field private mOnChronometerTickListener:Landroid/widget/Chronometer$OnChronometerTickListener;

.field private mRecycle:Ljava/lang/StringBuilder;

.field private mRunning:Z

.field private mStarted:Z

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 84
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/Chronometer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 92
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Chronometer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 72
    const/4 v1, 0x1

    #@5
    new-array v1, v1, [Ljava/lang/Object;

    #@7
    iput-object v1, p0, Landroid/widget/Chronometer;->mFormatterArgs:[Ljava/lang/Object;

    #@9
    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    const/16 v2, 0x8

    #@d
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@10
    iput-object v1, p0, Landroid/widget/Chronometer;->mRecycle:Ljava/lang/StringBuilder;

    #@12
    .line 267
    new-instance v1, Landroid/widget/Chronometer$1;

    #@14
    invoke-direct {v1, p0}, Landroid/widget/Chronometer$1;-><init>(Landroid/widget/Chronometer;)V

    #@17
    iput-object v1, p0, Landroid/widget/Chronometer;->mHandler:Landroid/os/Handler;

    #@19
    .line 102
    sget-object v1, Lcom/android/internal/R$styleable;->Chronometer:[I

    #@1b
    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1e
    move-result-object v0

    #@1f
    .line 105
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p0, v1}, Landroid/widget/Chronometer;->setFormat(Ljava/lang/String;)V

    #@26
    .line 106
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@29
    .line 108
    invoke-direct {p0}, Landroid/widget/Chronometer;->init()V

    #@2c
    .line 109
    return-void
.end method

.method static synthetic access$000(Landroid/widget/Chronometer;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Landroid/widget/Chronometer;->mRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/widget/Chronometer;J)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/Chronometer;->updateText(J)V

    #@3
    return-void
.end method

.method private init()V
    .registers 3

    #@0
    .prologue
    .line 112
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Landroid/widget/Chronometer;->mBase:J

    #@6
    .line 113
    iget-wide v0, p0, Landroid/widget/Chronometer;->mBase:J

    #@8
    invoke-direct {p0, v0, v1}, Landroid/widget/Chronometer;->updateText(J)V

    #@b
    .line 114
    return-void
.end method

.method private updateRunning()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 254
    iget-boolean v1, p0, Landroid/widget/Chronometer;->mVisible:Z

    #@3
    if-eqz v1, :cond_2a

    #@5
    iget-boolean v1, p0, Landroid/widget/Chronometer;->mStarted:Z

    #@7
    if-eqz v1, :cond_2a

    #@9
    const/4 v0, 0x1

    #@a
    .line 255
    .local v0, running:Z
    :goto_a
    iget-boolean v1, p0, Landroid/widget/Chronometer;->mRunning:Z

    #@c
    if-eq v0, v1, :cond_29

    #@e
    .line 256
    if-eqz v0, :cond_2c

    #@10
    .line 257
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v1

    #@14
    invoke-direct {p0, v1, v2}, Landroid/widget/Chronometer;->updateText(J)V

    #@17
    .line 258
    invoke-virtual {p0}, Landroid/widget/Chronometer;->dispatchChronometerTick()V

    #@1a
    .line 259
    iget-object v1, p0, Landroid/widget/Chronometer;->mHandler:Landroid/os/Handler;

    #@1c
    iget-object v2, p0, Landroid/widget/Chronometer;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@21
    move-result-object v2

    #@22
    const-wide/16 v3, 0x3e8

    #@24
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@27
    .line 263
    :goto_27
    iput-boolean v0, p0, Landroid/widget/Chronometer;->mRunning:Z

    #@29
    .line 265
    :cond_29
    return-void

    #@2a
    .line 254
    .end local v0           #running:Z
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_a

    #@2c
    .line 261
    .restart local v0       #running:Z
    :cond_2c
    iget-object v1, p0, Landroid/widget/Chronometer;->mHandler:Landroid/os/Handler;

    #@2e
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@31
    goto :goto_27
.end method

.method private declared-synchronized updateText(J)V
    .registers 11
    .parameter "now"

    #@0
    .prologue
    .line 228
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v5, p0, Landroid/widget/Chronometer;->mBase:J

    #@3
    sub-long v2, p1, v5

    #@5
    .line 229
    .local v2, seconds:J
    const-wide/16 v5, 0x3e8

    #@7
    div-long/2addr v2, v5

    #@8
    .line 230
    iget-object v5, p0, Landroid/widget/Chronometer;->mRecycle:Ljava/lang/StringBuilder;

    #@a
    invoke-static {v5, v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;J)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    .line 232
    .local v4, text:Ljava/lang/String;
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormat:Ljava/lang/String;

    #@10
    if-eqz v5, :cond_47

    #@12
    .line 233
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@15
    move-result-object v1

    #@16
    .line 234
    .local v1, loc:Ljava/util/Locale;
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatter:Ljava/util/Formatter;

    #@18
    if-eqz v5, :cond_22

    #@1a
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatterLocale:Ljava/util/Locale;

    #@1c
    invoke-virtual {v1, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_2d

    #@22
    .line 235
    :cond_22
    iput-object v1, p0, Landroid/widget/Chronometer;->mFormatterLocale:Ljava/util/Locale;

    #@24
    .line 236
    new-instance v5, Ljava/util/Formatter;

    #@26
    iget-object v6, p0, Landroid/widget/Chronometer;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v5, v6, v1}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    #@2b
    iput-object v5, p0, Landroid/widget/Chronometer;->mFormatter:Ljava/util/Formatter;

    #@2d
    .line 238
    :cond_2d
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@2f
    const/4 v6, 0x0

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    #@33
    .line 239
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatterArgs:[Ljava/lang/Object;

    #@35
    const/4 v6, 0x0

    #@36
    aput-object v4, v5, v6
    :try_end_38
    .catchall {:try_start_1 .. :try_end_38} :catchall_6f

    #@38
    .line 241
    :try_start_38
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatter:Ljava/util/Formatter;

    #@3a
    iget-object v6, p0, Landroid/widget/Chronometer;->mFormat:Ljava/lang/String;

    #@3c
    iget-object v7, p0, Landroid/widget/Chronometer;->mFormatterArgs:[Ljava/lang/Object;

    #@3e
    invoke-virtual {v5, v6, v7}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@41
    .line 242
    iget-object v5, p0, Landroid/widget/Chronometer;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_46
    .catchall {:try_start_38 .. :try_end_46} :catchall_6f
    .catch Ljava/util/IllegalFormatException; {:try_start_38 .. :try_end_46} :catch_4c

    #@46
    move-result-object v4

    #@47
    .line 250
    .end local v1           #loc:Ljava/util/Locale;
    :cond_47
    :goto_47
    :try_start_47
    invoke-virtual {p0, v4}, Landroid/widget/Chronometer;->setText(Ljava/lang/CharSequence;)V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_6f

    #@4a
    .line 251
    monitor-exit p0

    #@4b
    return-void

    #@4c
    .line 243
    .restart local v1       #loc:Ljava/util/Locale;
    :catch_4c
    move-exception v0

    #@4d
    .line 244
    .local v0, ex:Ljava/util/IllegalFormatException;
    :try_start_4d
    iget-boolean v5, p0, Landroid/widget/Chronometer;->mLogged:Z

    #@4f
    if-nez v5, :cond_47

    #@51
    .line 245
    const-string v5, "Chronometer"

    #@53
    new-instance v6, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v7, "Illegal format string: "

    #@5a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v6

    #@5e
    iget-object v7, p0, Landroid/widget/Chronometer;->mFormat:Ljava/lang/String;

    #@60
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v6

    #@68
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 246
    const/4 v5, 0x1

    #@6c
    iput-boolean v5, p0, Landroid/widget/Chronometer;->mLogged:Z
    :try_end_6e
    .catchall {:try_start_4d .. :try_end_6e} :catchall_6f

    #@6e
    goto :goto_47

    #@6f
    .line 228
    .end local v0           #ex:Ljava/util/IllegalFormatException;
    .end local v1           #loc:Ljava/util/Locale;
    .end local v2           #seconds:J
    .end local v4           #text:Ljava/lang/String;
    :catchall_6f
    move-exception v5

    #@70
    monitor-exit p0

    #@71
    throw v5
.end method


# virtual methods
.method dispatchChronometerTick()V
    .registers 2

    #@0
    .prologue
    .line 278
    iget-object v0, p0, Landroid/widget/Chronometer;->mOnChronometerTickListener:Landroid/widget/Chronometer$OnChronometerTickListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 279
    iget-object v0, p0, Landroid/widget/Chronometer;->mOnChronometerTickListener:Landroid/widget/Chronometer$OnChronometerTickListener;

    #@6
    invoke-interface {v0, p0}, Landroid/widget/Chronometer$OnChronometerTickListener;->onChronometerTick(Landroid/widget/Chronometer;)V

    #@9
    .line 281
    :cond_9
    return-void
.end method

.method public getBase()J
    .registers 3

    #@0
    .prologue
    .line 132
    iget-wide v0, p0, Landroid/widget/Chronometer;->mBase:J

    #@2
    return-wide v0
.end method

.method public getFormat()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/widget/Chronometer;->mFormat:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOnChronometerTickListener()Landroid/widget/Chronometer$OnChronometerTickListener;
    .registers 2

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Landroid/widget/Chronometer;->mOnChronometerTickListener:Landroid/widget/Chronometer$OnChronometerTickListener;

    #@2
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 215
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    #@3
    .line 216
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/Chronometer;->mVisible:Z

    #@6
    .line 217
    invoke-direct {p0}, Landroid/widget/Chronometer;->updateRunning()V

    #@9
    .line 218
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 285
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 286
    const-class v0, Landroid/widget/Chronometer;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 287
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 291
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 292
    const-class v0, Landroid/widget/Chronometer;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 293
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 222
    invoke-super {p0, p1}, Landroid/widget/TextView;->onWindowVisibilityChanged(I)V

    #@3
    .line 223
    if-nez p1, :cond_c

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    iput-boolean v0, p0, Landroid/widget/Chronometer;->mVisible:Z

    #@8
    .line 224
    invoke-direct {p0}, Landroid/widget/Chronometer;->updateRunning()V

    #@b
    .line 225
    return-void

    #@c
    .line 223
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_6
.end method

.method public setBase(J)V
    .registers 5
    .parameter "base"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 123
    iput-wide p1, p0, Landroid/widget/Chronometer;->mBase:J

    #@2
    .line 124
    invoke-virtual {p0}, Landroid/widget/Chronometer;->dispatchChronometerTick()V

    #@5
    .line 125
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@8
    move-result-wide v0

    #@9
    invoke-direct {p0, v0, v1}, Landroid/widget/Chronometer;->updateText(J)V

    #@c
    .line 126
    return-void
.end method

.method public setFormat(Ljava/lang/String;)V
    .registers 4
    .parameter "format"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Landroid/widget/Chronometer;->mFormat:Ljava/lang/String;

    #@2
    .line 149
    if-eqz p1, :cond_15

    #@4
    iget-object v0, p0, Landroid/widget/Chronometer;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@6
    if-nez v0, :cond_15

    #@8
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    mul-int/lit8 v1, v1, 0x2

    #@10
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@13
    iput-object v0, p0, Landroid/widget/Chronometer;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@15
    .line 152
    :cond_15
    return-void
.end method

.method public setOnChronometerTickListener(Landroid/widget/Chronometer$OnChronometerTickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 167
    iput-object p1, p0, Landroid/widget/Chronometer;->mOnChronometerTickListener:Landroid/widget/Chronometer$OnChronometerTickListener;

    #@2
    .line 168
    return-void
.end method

.method public setStarted(Z)V
    .registers 2
    .parameter "started"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 209
    iput-boolean p1, p0, Landroid/widget/Chronometer;->mStarted:Z

    #@2
    .line 210
    invoke-direct {p0}, Landroid/widget/Chronometer;->updateRunning()V

    #@5
    .line 211
    return-void
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 187
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/Chronometer;->mStarted:Z

    #@3
    .line 188
    invoke-direct {p0}, Landroid/widget/Chronometer;->updateRunning()V

    #@6
    .line 189
    return-void
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 199
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/Chronometer;->mStarted:Z

    #@3
    .line 200
    invoke-direct {p0}, Landroid/widget/Chronometer;->updateRunning()V

    #@6
    .line 201
    return-void
.end method
