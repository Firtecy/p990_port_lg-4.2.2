.class Landroid/widget/ArrayAdapter$ArrayFilter;
.super Landroid/widget/Filter;
.source "ArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ArrayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArrayFilter"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/ArrayAdapter;


# direct methods
.method private constructor <init>(Landroid/widget/ArrayAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 457
    .local p0, this:Landroid/widget/ArrayAdapter$ArrayFilter;,"Landroid/widget/ArrayAdapter<TT;>.ArrayFilter;"
    iput-object p1, p0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@2
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 457
    .local p0, this:Landroid/widget/ArrayAdapter$ArrayFilter;,"Landroid/widget/ArrayAdapter<TT;>.ArrayFilter;"
    invoke-direct {p0, p1}, Landroid/widget/ArrayAdapter$ArrayFilter;-><init>(Landroid/widget/ArrayAdapter;)V

    #@3
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 19
    .parameter "prefix"

    #@0
    .prologue
    .line 460
    .local p0, this:Landroid/widget/ArrayAdapter$ArrayFilter;,"Landroid/widget/ArrayAdapter<TT;>.ArrayFilter;"
    new-instance v7, Landroid/widget/Filter$FilterResults;

    #@2
    invoke-direct {v7}, Landroid/widget/Filter$FilterResults;-><init>()V

    #@5
    .line 462
    .local v7, results:Landroid/widget/Filter$FilterResults;
    move-object/from16 v0, p0

    #@7
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@9
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$100(Landroid/widget/ArrayAdapter;)Ljava/util/ArrayList;

    #@c
    move-result-object v13

    #@d
    if-nez v13, :cond_2f

    #@f
    .line 463
    move-object/from16 v0, p0

    #@11
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@13
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$200(Landroid/widget/ArrayAdapter;)Ljava/lang/Object;

    #@16
    move-result-object v14

    #@17
    monitor-enter v14

    #@18
    .line 464
    :try_start_18
    move-object/from16 v0, p0

    #@1a
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@1c
    new-instance v15, Ljava/util/ArrayList;

    #@1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@22
    move-object/from16 v16, v0

    #@24
    invoke-static/range {v16 .. v16}, Landroid/widget/ArrayAdapter;->access$300(Landroid/widget/ArrayAdapter;)Ljava/util/List;

    #@27
    move-result-object v16

    #@28
    invoke-direct/range {v15 .. v16}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@2b
    invoke-static {v13, v15}, Landroid/widget/ArrayAdapter;->access$102(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@2e
    .line 465
    monitor-exit v14
    :try_end_2f
    .catchall {:try_start_18 .. :try_end_2f} :catchall_57

    #@2f
    .line 468
    :cond_2f
    if-eqz p1, :cond_37

    #@31
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@34
    move-result v13

    #@35
    if-nez v13, :cond_5d

    #@37
    .line 470
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@3b
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$200(Landroid/widget/ArrayAdapter;)Ljava/lang/Object;

    #@3e
    move-result-object v14

    #@3f
    monitor-enter v14

    #@40
    .line 471
    :try_start_40
    new-instance v4, Ljava/util/ArrayList;

    #@42
    move-object/from16 v0, p0

    #@44
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@46
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$100(Landroid/widget/ArrayAdapter;)Ljava/util/ArrayList;

    #@49
    move-result-object v13

    #@4a
    invoke-direct {v4, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@4d
    .line 472
    .local v4, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    monitor-exit v14
    :try_end_4e
    .catchall {:try_start_40 .. :try_end_4e} :catchall_5a

    #@4e
    .line 473
    iput-object v4, v7, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@50
    .line 474
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v13

    #@54
    iput v13, v7, Landroid/widget/Filter$FilterResults;->count:I

    #@56
    .line 511
    .end local v4           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    :goto_56
    return-object v7

    #@57
    .line 465
    :catchall_57
    move-exception v13

    #@58
    :try_start_58
    monitor-exit v14
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_57

    #@59
    throw v13

    #@5a
    .line 472
    :catchall_5a
    move-exception v13

    #@5b
    :try_start_5b
    monitor-exit v14
    :try_end_5c
    .catchall {:try_start_5b .. :try_end_5c} :catchall_5a

    #@5c
    throw v13

    #@5d
    .line 476
    :cond_5d
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@60
    move-result-object v13

    #@61
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    .line 479
    .local v6, prefixString:Ljava/lang/String;
    move-object/from16 v0, p0

    #@67
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@69
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$200(Landroid/widget/ArrayAdapter;)Ljava/lang/Object;

    #@6c
    move-result-object v14

    #@6d
    monitor-enter v14

    #@6e
    .line 480
    :try_start_6e
    new-instance v10, Ljava/util/ArrayList;

    #@70
    move-object/from16 v0, p0

    #@72
    iget-object v13, v0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@74
    invoke-static {v13}, Landroid/widget/ArrayAdapter;->access$100(Landroid/widget/ArrayAdapter;)Ljava/util/ArrayList;

    #@77
    move-result-object v13

    #@78
    invoke-direct {v10, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@7b
    .line 481
    .local v10, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    monitor-exit v14
    :try_end_7c
    .catchall {:try_start_6e .. :try_end_7c} :catchall_a0

    #@7c
    .line 483
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@7f
    move-result v1

    #@80
    .line 484
    .local v1, count:I
    new-instance v5, Ljava/util/ArrayList;

    #@82
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@85
    .line 486
    .local v5, newValues:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    const/4 v2, 0x0

    #@86
    .local v2, i:I
    :goto_86
    if-ge v2, v1, :cond_bc

    #@88
    .line 487
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v8

    #@8c
    .line 488
    .local v8, value:Ljava/lang/Object;,"TT;"
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8f
    move-result-object v13

    #@90
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@93
    move-result-object v9

    #@94
    .line 491
    .local v9, valueText:Ljava/lang/String;
    invoke-virtual {v9, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@97
    move-result v13

    #@98
    if-eqz v13, :cond_a3

    #@9a
    .line 492
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9d
    .line 486
    :cond_9d
    :goto_9d
    add-int/lit8 v2, v2, 0x1

    #@9f
    goto :goto_86

    #@a0
    .line 481
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v5           #newValues:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    .end local v8           #value:Ljava/lang/Object;,"TT;"
    .end local v9           #valueText:Ljava/lang/String;
    .end local v10           #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    :catchall_a0
    move-exception v13

    #@a1
    :try_start_a1
    monitor-exit v14
    :try_end_a2
    .catchall {:try_start_a1 .. :try_end_a2} :catchall_a0

    #@a2
    throw v13

    #@a3
    .line 494
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v5       #newValues:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    .restart local v8       #value:Ljava/lang/Object;,"TT;"
    .restart local v9       #valueText:Ljava/lang/String;
    .restart local v10       #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    :cond_a3
    const-string v13, " "

    #@a5
    invoke-virtual {v9, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@a8
    move-result-object v12

    #@a9
    .line 495
    .local v12, words:[Ljava/lang/String;
    array-length v11, v12

    #@aa
    .line 498
    .local v11, wordCount:I
    const/4 v3, 0x0

    #@ab
    .local v3, k:I
    :goto_ab
    if-ge v3, v11, :cond_9d

    #@ad
    .line 499
    aget-object v13, v12, v3

    #@af
    invoke-virtual {v13, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b2
    move-result v13

    #@b3
    if-eqz v13, :cond_b9

    #@b5
    .line 500
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b8
    goto :goto_9d

    #@b9
    .line 498
    :cond_b9
    add-int/lit8 v3, v3, 0x1

    #@bb
    goto :goto_ab

    #@bc
    .line 507
    .end local v3           #k:I
    .end local v8           #value:Ljava/lang/Object;,"TT;"
    .end local v9           #valueText:Ljava/lang/String;
    .end local v11           #wordCount:I
    .end local v12           #words:[Ljava/lang/String;
    :cond_bc
    iput-object v5, v7, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@be
    .line 508
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@c1
    move-result v13

    #@c2
    iput v13, v7, Landroid/widget/Filter$FilterResults;->count:I

    #@c4
    goto :goto_56
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .parameter "constraint"
    .parameter "results"

    #@0
    .prologue
    .line 517
    .local p0, this:Landroid/widget/ArrayAdapter$ArrayFilter;,"Landroid/widget/ArrayAdapter<TT;>.ArrayFilter;"
    iget-object v1, p0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@2
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@4
    check-cast v0, Ljava/util/List;

    #@6
    invoke-static {v1, v0}, Landroid/widget/ArrayAdapter;->access$302(Landroid/widget/ArrayAdapter;Ljava/util/List;)Ljava/util/List;

    #@9
    .line 518
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    #@b
    if-lez v0, :cond_13

    #@d
    .line 519
    iget-object v0, p0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@f
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    #@12
    .line 523
    :goto_12
    return-void

    #@13
    .line 521
    :cond_13
    iget-object v0, p0, Landroid/widget/ArrayAdapter$ArrayFilter;->this$0:Landroid/widget/ArrayAdapter;

    #@15
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetInvalidated()V

    #@18
    goto :goto_12
.end method
