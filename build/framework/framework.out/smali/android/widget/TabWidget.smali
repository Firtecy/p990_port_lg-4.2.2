.class public Landroid/widget/TabWidget;
.super Landroid/widget/LinearLayout;
.source "TabWidget.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TabWidget$1;,
        Landroid/widget/TabWidget$OnTabSelectionChanged;,
        Landroid/widget/TabWidget$TabClickListener;
    }
.end annotation


# instance fields
.field private final mBounds:Landroid/graphics/Rect;

.field private mDrawBottomStrips:Z

.field private mImposedTabWidths:[I

.field private mImposedTabsHeight:I

.field private mLeftStrip:Landroid/graphics/drawable/Drawable;

.field private mRightStrip:Landroid/graphics/drawable/Drawable;

.field private mSelectedTab:I

.field private mSelectionChangedListener:Landroid/widget/TabWidget$OnTabSelectionChanged;

.field private mStripMoved:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 71
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 75
    const v0, 0x1010083

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v3, 0x1

    #@2
    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 56
    iput v2, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@7
    .line 61
    iput-boolean v3, p0, Landroid/widget/TabWidget;->mDrawBottomStrips:Z

    #@9
    .line 64
    new-instance v1, Landroid/graphics/Rect;

    #@b
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@e
    iput-object v1, p0, Landroid/widget/TabWidget;->mBounds:Landroid/graphics/Rect;

    #@10
    .line 67
    iput v2, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@12
    .line 81
    sget-object v1, Lcom/android/internal/R$styleable;->TabWidget:[I

    #@14
    const/4 v2, 0x0

    #@15
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@18
    move-result-object v0

    #@19
    .line 84
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x3

    #@1a
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1d
    move-result v1

    #@1e
    invoke-virtual {p0, v1}, Landroid/widget/TabWidget;->setStripEnabled(Z)V

    #@21
    .line 85
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p0, v1}, Landroid/widget/TabWidget;->setLeftStripDrawable(Landroid/graphics/drawable/Drawable;)V

    #@28
    .line 86
    const/4 v1, 0x2

    #@29
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {p0, v1}, Landroid/widget/TabWidget;->setRightStripDrawable(Landroid/graphics/drawable/Drawable;)V

    #@30
    .line 88
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 90
    invoke-direct {p0}, Landroid/widget/TabWidget;->initTabWidget()V

    #@36
    .line 91
    return-void
.end method

.method static synthetic access$100(Landroid/widget/TabWidget;)Landroid/widget/TabWidget$OnTabSelectionChanged;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/TabWidget;->mSelectionChangedListener:Landroid/widget/TabWidget$OnTabSelectionChanged;

    #@2
    return-object v0
.end method

.method private initTabWidget()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 117
    invoke-virtual {p0, v4}, Landroid/widget/TabWidget;->setChildrenDrawingOrderEnabled(Z)V

    #@4
    .line 119
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@6
    .line 120
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v1

    #@a
    .line 124
    .local v1, resources:Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@d
    move-result-object v2

    #@e
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@10
    const/4 v3, 0x4

    #@11
    if-gt v2, v3, :cond_34

    #@13
    .line 126
    iget-object v2, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@15
    if-nez v2, :cond_20

    #@17
    .line 127
    const v2, 0x10805b2

    #@1a
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1d
    move-result-object v2

    #@1e
    iput-object v2, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@20
    .line 130
    :cond_20
    iget-object v2, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@22
    if-nez v2, :cond_2d

    #@24
    .line 131
    const v2, 0x10805b4

    #@27
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v2

    #@2b
    iput-object v2, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@2d
    .line 148
    :cond_2d
    :goto_2d
    invoke-virtual {p0, v4}, Landroid/widget/TabWidget;->setFocusable(Z)V

    #@30
    .line 149
    invoke-virtual {p0, p0}, Landroid/widget/TabWidget;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    #@33
    .line 150
    return-void

    #@34
    .line 136
    :cond_34
    iget-object v2, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@36
    if-nez v2, :cond_41

    #@38
    .line 137
    const v2, 0x10805b1

    #@3b
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v2

    #@3f
    iput-object v2, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@41
    .line 140
    :cond_41
    iget-object v2, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@43
    if-nez v2, :cond_2d

    #@45
    .line 141
    const v2, 0x10805b3

    #@48
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4b
    move-result-object v2

    #@4c
    iput-object v2, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@4e
    goto :goto_2d
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 7
    .parameter "child"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 497
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5
    move-result-object v1

    #@6
    if-nez v1, :cond_16

    #@8
    .line 498
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@a
    const/4 v1, -0x1

    #@b
    const/high16 v2, 0x3f80

    #@d
    invoke-direct {v0, v3, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@10
    .line 501
    .local v0, lp:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    #@13
    .line 502
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@16
    .line 506
    .end local v0           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_16
    invoke-virtual {p1, v4}, Landroid/view/View;->setFocusable(Z)V

    #@19
    .line 507
    invoke-virtual {p1, v4}, Landroid/view/View;->setClickable(Z)V

    #@1c
    .line 509
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@1f
    .line 513
    new-instance v1, Landroid/widget/TabWidget$TabClickListener;

    #@21
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@24
    move-result v2

    #@25
    add-int/lit8 v2, v2, -0x1

    #@27
    const/4 v3, 0x0

    #@28
    invoke-direct {v1, p0, v2, v3}, Landroid/widget/TabWidget$TabClickListener;-><init>(Landroid/widget/TabWidget;ILandroid/widget/TabWidget$1;)V

    #@2b
    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2e
    .line 514
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    #@31
    .line 515
    return-void
.end method

.method public childDrawableStateChanged(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 313
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@2
    const/4 v1, -0x1

    #@3
    if-eq v0, v1, :cond_16

    #@5
    .line 314
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@8
    move-result v0

    #@9
    if-lez v0, :cond_16

    #@b
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    if-ne p1, v0, :cond_16

    #@13
    .line 316
    invoke-virtual {p0}, Landroid/widget/TabWidget;->invalidate()V

    #@16
    .line 319
    :cond_16
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->childDrawableStateChanged(Landroid/view/View;)V

    #@19
    .line 320
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 324
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@4
    .line 327
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@7
    move-result v5

    #@8
    if-nez v5, :cond_b

    #@a
    .line 366
    :cond_a
    :goto_a
    return-void

    #@b
    .line 331
    :cond_b
    iget-boolean v5, p0, Landroid/widget/TabWidget;->mDrawBottomStrips:Z

    #@d
    if-eqz v5, :cond_a

    #@f
    .line 337
    iget v5, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@11
    const/4 v6, -0x1

    #@12
    if-ne v5, v6, :cond_1c

    #@14
    .line 338
    const-string v5, "TabWidget"

    #@16
    const-string v6, "Failed to dispatchDraw, because no tab had been selected."

    #@18
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_a

    #@1c
    .line 343
    :cond_1c
    iget v5, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@1e
    invoke-virtual {p0, v5}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@21
    move-result-object v4

    #@22
    .line 345
    .local v4, selectedChild:Landroid/view/View;
    iget-object v1, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@24
    .line 346
    .local v1, leftStrip:Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@26
    .line 348
    .local v3, rightStrip:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/view/View;->getDrawableState()[I

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@2d
    .line 349
    invoke-virtual {v4}, Landroid/view/View;->getDrawableState()[I

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@34
    .line 351
    iget-boolean v5, p0, Landroid/widget/TabWidget;->mStripMoved:Z

    #@36
    if-eqz v5, :cond_7c

    #@38
    .line 352
    iget-object v0, p0, Landroid/widget/TabWidget;->mBounds:Landroid/graphics/Rect;

    #@3a
    .line 353
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    #@3d
    move-result v5

    #@3e
    iput v5, v0, Landroid/graphics/Rect;->left:I

    #@40
    .line 354
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    #@43
    move-result v5

    #@44
    iput v5, v0, Landroid/graphics/Rect;->right:I

    #@46
    .line 355
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getHeight()I

    #@49
    move-result v2

    #@4a
    .line 356
    .local v2, myHeight:I
    iget v5, v0, Landroid/graphics/Rect;->left:I

    #@4c
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@4f
    move-result v6

    #@50
    sub-int/2addr v5, v6

    #@51
    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    #@54
    move-result v5

    #@55
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@58
    move-result v6

    #@59
    sub-int v6, v2, v6

    #@5b
    iget v7, v0, Landroid/graphics/Rect;->left:I

    #@5d
    invoke-virtual {v1, v5, v6, v7, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@60
    .line 358
    iget v5, v0, Landroid/graphics/Rect;->right:I

    #@62
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@65
    move-result v6

    #@66
    sub-int v6, v2, v6

    #@68
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getWidth()I

    #@6b
    move-result v7

    #@6c
    iget v8, v0, Landroid/graphics/Rect;->right:I

    #@6e
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@71
    move-result v9

    #@72
    add-int/2addr v8, v9

    #@73
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@76
    move-result v7

    #@77
    invoke-virtual {v3, v5, v6, v7, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@7a
    .line 360
    iput-boolean v10, p0, Landroid/widget/TabWidget;->mStripMoved:Z

    #@7c
    .line 363
    .end local v0           #bounds:Landroid/graphics/Rect;
    .end local v2           #myHeight:I
    :cond_7c
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@7f
    .line 364
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@82
    goto :goto_a
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 413
    invoke-virtual {p0, p1}, Landroid/widget/TabWidget;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 415
    iget v1, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@5
    const/4 v2, -0x1

    #@6
    if-eq v1, v2, :cond_1b

    #@8
    .line 416
    iget v1, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@a
    invoke-virtual {p0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 417
    .local v0, tabView:Landroid/view/View;
    if-eqz v0, :cond_1b

    #@10
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_1b

    #@16
    .line 418
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@19
    move-result v1

    #@1a
    .line 421
    .end local v0           #tabView:Landroid/view/View;
    :goto_1a
    return v1

    #@1b
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_1a
.end method

.method public focusCurrentTab(I)V
    .registers 6
    .parameter "index"

    #@0
    .prologue
    .line 465
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@5
    move-result v1

    #@6
    if-lt p1, v1, :cond_26

    #@8
    .line 466
    :cond_8
    const-string v1, "TabWidget"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Failed to focusCurrentTab, because new index is invalid value: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 467
    new-instance v1, Ljava/lang/NullPointerException;

    #@22
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@25
    throw v1

    #@26
    .line 468
    :cond_26
    iget v1, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@28
    if-ne p1, v1, :cond_2b

    #@2a
    .line 482
    :goto_2a
    return-void

    #@2b
    .line 472
    :cond_2b
    invoke-virtual {p0, p1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@2e
    move-result-object v0

    #@2f
    .line 473
    .local v0, targetChildTabView:Landroid/view/View;
    if-eqz v0, :cond_38

    #@31
    .line 475
    invoke-virtual {p0, p1}, Landroid/widget/TabWidget;->setCurrentTab(I)V

    #@34
    .line 477
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    #@37
    goto :goto_2a

    #@38
    .line 479
    :cond_38
    const-string v1, "TabWidget"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "focusCurrentTab ignored, failing getChildTabViewAt("

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, ")"

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_2a
.end method

.method protected getChildDrawingOrder(II)I
    .registers 5
    .parameter "childCount"
    .parameter "i"

    #@0
    .prologue
    .line 101
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_6

    #@5
    .line 111
    .end local p2
    :cond_5
    :goto_5
    return p2

    #@6
    .line 106
    .restart local p2
    :cond_6
    add-int/lit8 v0, p1, -0x1

    #@8
    if-ne p2, v0, :cond_d

    #@a
    .line 107
    iget p2, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@c
    goto :goto_5

    #@d
    .line 108
    :cond_d
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@f
    if-lt p2, v0, :cond_5

    #@11
    .line 109
    add-int/lit8 p2, p2, 0x1

    #@13
    goto :goto_5
.end method

.method public getChildTabViewAt(I)Landroid/view/View;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 220
    invoke-virtual {p0, p1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getTabCount()I
    .registers 2

    #@0
    .prologue
    .line 228
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getChildCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isStripEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 308
    iget-boolean v0, p0, Landroid/widget/TabWidget;->mDrawBottomStrips:Z

    #@2
    return v0
.end method

.method measureChildBeforeLayout(Landroid/view/View;IIIII)V
    .registers 9
    .parameter "child"
    .parameter "childIndex"
    .parameter "widthMeasureSpec"
    .parameter "totalWidth"
    .parameter "heightMeasureSpec"
    .parameter "totalHeight"

    #@0
    .prologue
    const/high16 v1, 0x4000

    #@2
    .line 156
    invoke-virtual {p0}, Landroid/widget/TabWidget;->isMeasureWithLargestChildEnabled()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_1b

    #@8
    iget v0, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@a
    if-ltz v0, :cond_1b

    #@c
    .line 157
    iget-object v0, p0, Landroid/widget/TabWidget;->mImposedTabWidths:[I

    #@e
    aget v0, v0, p2

    #@10
    add-int/2addr v0, p4

    #@11
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@14
    move-result p3

    #@15
    .line 159
    iget v0, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@17
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1a
    move-result p5

    #@1b
    .line 163
    :cond_1b
    invoke-super/range {p0 .. p6}, Landroid/widget/LinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    #@1e
    .line 165
    return-void
.end method

.method measureHorizontal(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/16 v12, 0x8

    #@2
    const/4 v11, 0x0

    #@3
    .line 169
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@6
    move-result v9

    #@7
    if-nez v9, :cond_d

    #@9
    .line 170
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->measureHorizontal(II)V

    #@c
    .line 211
    :goto_c
    return-void

    #@d
    .line 175
    :cond_d
    invoke-static {v11, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@10
    move-result v8

    #@11
    .line 176
    .local v8, unspecifiedWidth:I
    const/4 v9, -0x1

    #@12
    iput v9, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@14
    .line 177
    invoke-super {p0, v8, p2}, Landroid/widget/LinearLayout;->measureHorizontal(II)V

    #@17
    .line 179
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getMeasuredWidth()I

    #@1a
    move-result v9

    #@1b
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1e
    move-result v10

    #@1f
    sub-int v5, v9, v10

    #@21
    .line 180
    .local v5, extraWidth:I
    if-lez v5, :cond_7c

    #@23
    .line 181
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getChildCount()I

    #@26
    move-result v3

    #@27
    .line 183
    .local v3, count:I
    const/4 v1, 0x0

    #@28
    .line 184
    .local v1, childCount:I
    const/4 v6, 0x0

    #@29
    .local v6, i:I
    :goto_29
    if-ge v6, v3, :cond_3b

    #@2b
    .line 185
    invoke-virtual {p0, v6}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    #@2e
    move-result-object v0

    #@2f
    .line 186
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@32
    move-result v9

    #@33
    if-ne v9, v12, :cond_38

    #@35
    .line 184
    :goto_35
    add-int/lit8 v6, v6, 0x1

    #@37
    goto :goto_29

    #@38
    .line 187
    :cond_38
    add-int/lit8 v1, v1, 0x1

    #@3a
    goto :goto_35

    #@3b
    .line 190
    .end local v0           #child:Landroid/view/View;
    :cond_3b
    if-lez v1, :cond_7c

    #@3d
    .line 191
    iget-object v9, p0, Landroid/widget/TabWidget;->mImposedTabWidths:[I

    #@3f
    if-eqz v9, :cond_46

    #@41
    iget-object v9, p0, Landroid/widget/TabWidget;->mImposedTabWidths:[I

    #@43
    array-length v9, v9

    #@44
    if-eq v9, v3, :cond_4a

    #@46
    .line 192
    :cond_46
    new-array v9, v3, [I

    #@48
    iput-object v9, p0, Landroid/widget/TabWidget;->mImposedTabWidths:[I

    #@4a
    .line 194
    :cond_4a
    const/4 v6, 0x0

    #@4b
    :goto_4b
    if-ge v6, v3, :cond_7c

    #@4d
    .line 195
    invoke-virtual {p0, v6}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    #@50
    move-result-object v0

    #@51
    .line 196
    .restart local v0       #child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@54
    move-result v9

    #@55
    if-ne v9, v12, :cond_5a

    #@57
    .line 194
    :goto_57
    add-int/lit8 v6, v6, 0x1

    #@59
    goto :goto_4b

    #@5a
    .line 197
    :cond_5a
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@5d
    move-result v2

    #@5e
    .line 198
    .local v2, childWidth:I
    div-int v4, v5, v1

    #@60
    .line 199
    .local v4, delta:I
    sub-int v9, v2, v4

    #@62
    invoke-static {v11, v9}, Ljava/lang/Math;->max(II)I

    #@65
    move-result v7

    #@66
    .line 200
    .local v7, newWidth:I
    iget-object v9, p0, Landroid/widget/TabWidget;->mImposedTabWidths:[I

    #@68
    aput v7, v9, v6

    #@6a
    .line 202
    sub-int v9, v2, v7

    #@6c
    sub-int/2addr v5, v9

    #@6d
    .line 203
    add-int/lit8 v1, v1, -0x1

    #@6f
    .line 204
    iget v9, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@71
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@74
    move-result v10

    #@75
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@78
    move-result v9

    #@79
    iput v9, p0, Landroid/widget/TabWidget;->mImposedTabsHeight:I

    #@7b
    goto :goto_57

    #@7c
    .line 210
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childCount:I
    .end local v2           #childWidth:I
    .end local v3           #count:I
    .end local v4           #delta:I
    .end local v6           #i:I
    .end local v7           #newWidth:I
    :cond_7c
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->measureHorizontal(II)V

    #@7f
    goto :goto_c
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .registers 7
    .parameter "v"
    .parameter "hasFocus"

    #@0
    .prologue
    .line 533
    if-ne p1, p0, :cond_19

    #@2
    if-eqz p2, :cond_19

    #@4
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@7
    move-result v2

    #@8
    if-lez v2, :cond_19

    #@a
    iget v2, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@c
    const/4 v3, -0x1

    #@d
    if-eq v2, v3, :cond_19

    #@f
    .line 534
    iget v2, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@11
    invoke-virtual {p0, v2}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    #@18
    .line 555
    :cond_18
    :goto_18
    return-void

    #@19
    .line 539
    :cond_19
    if-eqz p2, :cond_18

    #@1b
    .line 540
    const/4 v0, 0x0

    #@1c
    .line 541
    .local v0, i:I
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@1f
    move-result v1

    #@20
    .line 542
    .local v1, numTabs:I
    :goto_20
    if-ge v0, v1, :cond_18

    #@22
    .line 543
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@25
    move-result-object v2

    #@26
    if-ne v2, p1, :cond_3d

    #@28
    .line 544
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->setCurrentTab(I)V

    #@2b
    .line 545
    iget-object v2, p0, Landroid/widget/TabWidget;->mSelectionChangedListener:Landroid/widget/TabWidget$OnTabSelectionChanged;

    #@2d
    const/4 v3, 0x0

    #@2e
    invoke-interface {v2, v0, v3}, Landroid/widget/TabWidget$OnTabSelectionChanged;->onTabSelectionChanged(IZ)V

    #@31
    .line 546
    invoke-virtual {p0}, Landroid/widget/TabWidget;->isShown()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_18

    #@37
    .line 548
    const/16 v2, 0x8

    #@39
    invoke-virtual {p0, v2}, Landroid/widget/TabWidget;->sendAccessibilityEvent(I)V

    #@3c
    goto :goto_18

    #@3d
    .line 552
    :cond_3d
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_20
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 426
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 427
    const-class v0, Landroid/widget/TabWidget;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 428
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@f
    move-result v0

    #@10
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@13
    .line 429
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@15
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    #@18
    .line 430
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 445
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 446
    const-class v0, Landroid/widget/TabWidget;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 447
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 95
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/TabWidget;->mStripMoved:Z

    #@3
    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    #@6
    .line 97
    return-void
.end method

.method public removeAllViews()V
    .registers 2

    #@0
    .prologue
    .line 519
    invoke-super {p0}, Landroid/widget/LinearLayout;->removeAllViews()V

    #@3
    .line 520
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@6
    .line 521
    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 436
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x8

    #@6
    if-ne v0, v1, :cond_12

    #@8
    invoke-virtual {p0}, Landroid/widget/TabWidget;->isFocused()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_12

    #@e
    .line 437
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@11
    .line 441
    :goto_11
    return-void

    #@12
    .line 440
    :cond_12
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    #@15
    goto :goto_11
.end method

.method public setCurrentTab(I)V
    .registers 5
    .parameter "index"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 396
    if-ltz p1, :cond_d

    #@3
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@6
    move-result v0

    #@7
    if-ge p1, v0, :cond_d

    #@9
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@b
    if-ne p1, v0, :cond_e

    #@d
    .line 409
    :cond_d
    :goto_d
    return-void

    #@e
    .line 399
    :cond_e
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@10
    const/4 v1, -0x1

    #@11
    if-eq v0, v1, :cond_1d

    #@13
    .line 400
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@15
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    const/4 v1, 0x0

    #@1a
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    #@1d
    .line 402
    :cond_1d
    iput p1, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@1f
    .line 403
    iget v0, p0, Landroid/widget/TabWidget;->mSelectedTab:I

    #@21
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    #@28
    .line 404
    iput-boolean v2, p0, Landroid/widget/TabWidget;->mStripMoved:Z

    #@2a
    .line 406
    invoke-virtual {p0}, Landroid/widget/TabWidget;->isShown()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_d

    #@30
    .line 407
    const/4 v0, 0x4

    #@31
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->sendAccessibilityEvent(I)V

    #@34
    goto :goto_d
.end method

.method public setDividerDrawable(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 246
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 247
    return-void
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "drawable"

    #@0
    .prologue
    .line 237
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3
    .line 238
    return-void
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 486
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    #@3
    .line 488
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getTabCount()I

    #@6
    move-result v1

    #@7
    .line 489
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_14

    #@a
    .line 490
    invoke-virtual {p0, v2}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 491
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    #@11
    .line 489
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_8

    #@14
    .line 493
    .end local v0           #child:Landroid/view/View;
    :cond_14
    return-void
.end method

.method public setLeftStripDrawable(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 267
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->setLeftStripDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 268
    return-void
.end method

.method public setLeftStripDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "drawable"

    #@0
    .prologue
    .line 255
    iput-object p1, p0, Landroid/widget/TabWidget;->mLeftStrip:Landroid/graphics/drawable/Drawable;

    #@2
    .line 256
    invoke-virtual {p0}, Landroid/widget/TabWidget;->requestLayout()V

    #@5
    .line 257
    invoke-virtual {p0}, Landroid/widget/TabWidget;->invalidate()V

    #@8
    .line 258
    return-void
.end method

.method public setRightStripDrawable(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 288
    invoke-virtual {p0}, Landroid/widget/TabWidget;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/TabWidget;->setRightStripDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 289
    return-void
.end method

.method public setRightStripDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "drawable"

    #@0
    .prologue
    .line 276
    iput-object p1, p0, Landroid/widget/TabWidget;->mRightStrip:Landroid/graphics/drawable/Drawable;

    #@2
    .line 277
    invoke-virtual {p0}, Landroid/widget/TabWidget;->requestLayout()V

    #@5
    .line 278
    invoke-virtual {p0}, Landroid/widget/TabWidget;->invalidate()V

    #@8
    .line 279
    return-void
.end method

.method public setStripEnabled(Z)V
    .registers 2
    .parameter "stripEnabled"

    #@0
    .prologue
    .line 299
    iput-boolean p1, p0, Landroid/widget/TabWidget;->mDrawBottomStrips:Z

    #@2
    .line 300
    invoke-virtual {p0}, Landroid/widget/TabWidget;->invalidate()V

    #@5
    .line 301
    return-void
.end method

.method setTabSelectionListener(Landroid/widget/TabWidget$OnTabSelectionChanged;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 527
    iput-object p1, p0, Landroid/widget/TabWidget;->mSelectionChangedListener:Landroid/widget/TabWidget$OnTabSelectionChanged;

    #@2
    .line 528
    return-void
.end method
