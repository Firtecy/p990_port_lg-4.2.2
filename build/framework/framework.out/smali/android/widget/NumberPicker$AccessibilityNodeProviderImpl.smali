.class Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;
.super Landroid/view/accessibility/AccessibilityNodeProvider;
.source "NumberPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccessibilityNodeProviderImpl"
.end annotation


# static fields
.field private static final UNDEFINED:I = -0x80000000

.field private static final VIRTUAL_VIEW_ID_DECREMENT:I = 0x3

.field private static final VIRTUAL_VIEW_ID_INCREMENT:I = 0x1

.field private static final VIRTUAL_VIEW_ID_INPUT:I = 0x2


# instance fields
.field private mAccessibilityFocusedView:I

.field private final mTempArray:[I

.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Landroid/widget/NumberPicker;


# direct methods
.method constructor <init>(Landroid/widget/NumberPicker;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2174
    iput-object p1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeProvider;-><init>()V

    #@5
    .line 2183
    new-instance v0, Landroid/graphics/Rect;

    #@7
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempRect:Landroid/graphics/Rect;

    #@c
    .line 2185
    const/4 v0, 0x2

    #@d
    new-array v0, v0, [I

    #@f
    iput-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempArray:[I

    #@11
    .line 2187
    const/high16 v0, -0x8000

    #@13
    iput v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@15
    return-void
.end method

.method private createAccessibilityNodeInfoForNumberPicker(IIII)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 14
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v7, 0x1

    #@2
    .line 2510
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@5
    move-result-object v3

    #@6
    .line 2511
    .local v3, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    const-class v5, Landroid/widget/NumberPicker;

    #@8
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@f
    .line 2512
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@11
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$6100(Landroid/widget/NumberPicker;)Landroid/content/Context;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    #@1c
    .line 2513
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1e
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;)V

    #@21
    .line 2515
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->hasVirtualDecrementButton()Z

    #@24
    move-result v5

    #@25
    if-eqz v5, :cond_2d

    #@27
    .line 2516
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@29
    const/4 v6, 0x3

    #@2a
    invoke-virtual {v3, v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    #@2d
    .line 2518
    :cond_2d
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2f
    const/4 v6, 0x2

    #@30
    invoke-virtual {v3, v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    #@33
    .line 2519
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->hasVirtualIncrementButton()Z

    #@36
    move-result v5

    #@37
    if-eqz v5, :cond_3e

    #@39
    .line 2520
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@3b
    invoke-virtual {v3, v5, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    #@3e
    .line 2523
    :cond_3e
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@40
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getParentForAccessibility()Landroid/view/ViewParent;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Landroid/view/View;

    #@46
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    #@49
    .line 2524
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@4b
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@4e
    move-result v5

    #@4f
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    #@52
    .line 2525
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@55
    .line 2527
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@57
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getContext()Landroid/content/Context;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@62
    move-result-object v5

    #@63
    iget v0, v5, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@65
    .line 2530
    .local v0, applicationScale:F
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempRect:Landroid/graphics/Rect;

    #@67
    .line 2531
    .local v1, boundsInParent:Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    #@6a
    .line 2532
    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@6d
    .line 2533
    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    #@70
    .line 2535
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@72
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$6200(Landroid/widget/NumberPicker;)Z

    #@75
    move-result v5

    #@76
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    #@79
    .line 2537
    move-object v2, v1

    #@7a
    .line 2538
    .local v2, boundsInScreen:Landroid/graphics/Rect;
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempArray:[I

    #@7c
    .line 2539
    .local v4, locationOnScreen:[I
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@7e
    invoke-virtual {v5, v4}, Landroid/widget/NumberPicker;->getLocationOnScreen([I)V

    #@81
    .line 2540
    const/4 v5, 0x0

    #@82
    aget v5, v4, v5

    #@84
    aget v6, v4, v7

    #@86
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    #@89
    .line 2541
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->scale(F)V

    #@8c
    .line 2542
    invoke-virtual {v3, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    #@8f
    .line 2544
    iget v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@91
    if-eq v5, v8, :cond_98

    #@93
    .line 2545
    const/16 v5, 0x40

    #@95
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@98
    .line 2547
    :cond_98
    iget v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@9a
    if-ne v5, v8, :cond_a1

    #@9c
    .line 2548
    const/16 v5, 0x80

    #@9e
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@a1
    .line 2550
    :cond_a1
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a3
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@a6
    move-result v5

    #@a7
    if-eqz v5, :cond_df

    #@a9
    .line 2551
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@ab
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@ae
    move-result v5

    #@af
    if-nez v5, :cond_bf

    #@b1
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@b3
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getValue()I

    #@b6
    move-result v5

    #@b7
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@b9
    invoke-virtual {v6}, Landroid/widget/NumberPicker;->getMaxValue()I

    #@bc
    move-result v6

    #@bd
    if-ge v5, v6, :cond_c4

    #@bf
    .line 2552
    :cond_bf
    const/16 v5, 0x1000

    #@c1
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@c4
    .line 2554
    :cond_c4
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@c6
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@c9
    move-result v5

    #@ca
    if-nez v5, :cond_da

    #@cc
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@ce
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getValue()I

    #@d1
    move-result v5

    #@d2
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@d4
    invoke-virtual {v6}, Landroid/widget/NumberPicker;->getMinValue()I

    #@d7
    move-result v6

    #@d8
    if-le v5, v6, :cond_df

    #@da
    .line 2555
    :cond_da
    const/16 v5, 0x2000

    #@dc
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@df
    .line 2559
    :cond_df
    return-object v3
.end method

.method private createAccessibilityNodeInfoForVirtualButton(ILjava/lang/String;IIII)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 13
    .parameter "virtualViewId"
    .parameter "text"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 2476
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@4
    move-result-object v2

    #@5
    .line 2477
    .local v2, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    const-class v4, Landroid/widget/Button;

    #@7
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@e
    .line 2478
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@10
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$5900(Landroid/widget/NumberPicker;)Landroid/content/Context;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    #@1b
    .line 2479
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1d
    invoke-virtual {v2, v4, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    #@20
    .line 2480
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@22
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    #@25
    .line 2481
    invoke-virtual {v2, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    #@28
    .line 2482
    invoke-virtual {v2, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    #@2b
    .line 2483
    invoke-virtual {v2, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    #@2e
    .line 2484
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@30
    invoke-virtual {v4}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@33
    move-result v4

    #@34
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    #@37
    .line 2485
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempRect:Landroid/graphics/Rect;

    #@39
    .line 2486
    .local v0, boundsInParent:Landroid/graphics/Rect;
    invoke-virtual {v0, p3, p4, p5, p6}, Landroid/graphics/Rect;->set(IIII)V

    #@3c
    .line 2487
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@3e
    invoke-static {v4, v0}, Landroid/widget/NumberPicker;->access$6000(Landroid/widget/NumberPicker;Landroid/graphics/Rect;)Z

    #@41
    move-result v4

    #@42
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    #@45
    .line 2488
    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    #@48
    .line 2489
    move-object v1, v0

    #@49
    .line 2490
    .local v1, boundsInScreen:Landroid/graphics/Rect;
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mTempArray:[I

    #@4b
    .line 2491
    .local v3, locationOnScreen:[I
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@4d
    invoke-virtual {v4, v3}, Landroid/widget/NumberPicker;->getLocationOnScreen([I)V

    #@50
    .line 2492
    const/4 v4, 0x0

    #@51
    aget v4, v3, v4

    #@53
    aget v5, v3, v5

    #@55
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    #@58
    .line 2493
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    #@5b
    .line 2495
    iget v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@5d
    if-eq v4, p1, :cond_64

    #@5f
    .line 2496
    const/16 v4, 0x40

    #@61
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@64
    .line 2498
    :cond_64
    iget v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@66
    if-ne v4, p1, :cond_6d

    #@68
    .line 2499
    const/16 v4, 0x80

    #@6a
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@6d
    .line 2501
    :cond_6d
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@6f
    invoke-virtual {v4}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@72
    move-result v4

    #@73
    if-eqz v4, :cond_7a

    #@75
    .line 2502
    const/16 v4, 0x10

    #@77
    invoke-virtual {v2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@7a
    .line 2505
    :cond_7a
    return-object v2
.end method

.method private createAccessibiltyNodeInfoForInputText()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 2463
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@3
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/widget/EditText;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@a
    move-result-object v0

    #@b
    .line 2464
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    #@10
    .line 2465
    iget v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@12
    if-eq v1, v2, :cond_19

    #@14
    .line 2466
    const/16 v1, 0x40

    #@16
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@19
    .line 2468
    :cond_19
    iget v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@1b
    if-ne v1, v2, :cond_22

    #@1d
    .line 2469
    const/16 v1, 0x80

    #@1f
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@22
    .line 2471
    :cond_22
    return-object v0
.end method

.method private findAccessibilityNodeInfosByTextInChild(Ljava/lang/String;ILjava/util/List;)V
    .registers 8
    .parameter "searchedLowerCase"
    .parameter "virtualViewId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, outResult:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    const/4 v3, 0x2

    #@1
    .line 2430
    packed-switch p2, :pswitch_data_94

    #@4
    .line 2460
    :cond_4
    :goto_4
    return-void

    #@5
    .line 2432
    :pswitch_5
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualDecrementButtonText()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 2433
    .local v1, text:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_4

    #@f
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_4

    #@1d
    .line 2435
    const/4 v2, 0x3

    #@1e
    invoke-virtual {p0, v2}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@21
    move-result-object v2

    #@22
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@25
    goto :goto_4

    #@26
    .line 2439
    .end local v1           #text:Ljava/lang/String;
    :pswitch_26
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@28
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@2f
    move-result-object v1

    #@30
    .line 2440
    .local v1, text:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@33
    move-result v2

    #@34
    if-nez v2, :cond_4c

    #@36
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_4c

    #@44
    .line 2442
    invoke-virtual {p0, v3}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@47
    move-result-object v2

    #@48
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4b
    goto :goto_4

    #@4c
    .line 2445
    :cond_4c
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@4e
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@55
    move-result-object v0

    #@56
    .line 2446
    .local v0, contentDesc:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@59
    move-result v2

    #@5a
    if-nez v2, :cond_4

    #@5c
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_4

    #@6a
    .line 2448
    invoke-virtual {p0, v3}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@6d
    move-result-object v2

    #@6e
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@71
    goto :goto_4

    #@72
    .line 2453
    .end local v0           #contentDesc:Ljava/lang/CharSequence;
    .end local v1           #text:Ljava/lang/CharSequence;
    :pswitch_72
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualIncrementButtonText()Ljava/lang/String;

    #@75
    move-result-object v1

    #@76
    .line 2454
    .local v1, text:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@79
    move-result v2

    #@7a
    if-nez v2, :cond_4

    #@7c
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@87
    move-result v2

    #@88
    if-eqz v2, :cond_4

    #@8a
    .line 2456
    const/4 v2, 0x1

    #@8b
    invoke-virtual {p0, v2}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@8e
    move-result-object v2

    #@8f
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@92
    goto/16 :goto_4

    #@94
    .line 2430
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_72
        :pswitch_26
        :pswitch_5
    .end packed-switch
.end method

.method private getVirtualDecrementButtonText()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2571
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$6300(Landroid/widget/NumberPicker;)I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .line 2572
    .local v0, value:I
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$6400(Landroid/widget/NumberPicker;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_16

    #@10
    .line 2573
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@12
    invoke-static {v1, v0}, Landroid/widget/NumberPicker;->access$6500(Landroid/widget/NumberPicker;I)I

    #@15
    move-result v0

    #@16
    .line 2575
    :cond_16
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@18
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$6600(Landroid/widget/NumberPicker;)I

    #@1b
    move-result v1

    #@1c
    if-lt v0, v1, :cond_3e

    #@1e
    .line 2576
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@20
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    if-nez v1, :cond_2d

    #@26
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@28
    invoke-static {v1, v0}, Landroid/widget/NumberPicker;->access$6700(Landroid/widget/NumberPicker;I)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    .line 2579
    :goto_2c
    return-object v1

    #@2d
    .line 2576
    :cond_2d
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2f
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@35
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$6600(Landroid/widget/NumberPicker;)I

    #@38
    move-result v2

    #@39
    sub-int v2, v0, v2

    #@3b
    aget-object v1, v1, v2

    #@3d
    goto :goto_2c

    #@3e
    .line 2579
    :cond_3e
    const/4 v1, 0x0

    #@3f
    goto :goto_2c
.end method

.method private getVirtualIncrementButtonText()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2583
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$6300(Landroid/widget/NumberPicker;)I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, 0x1

    #@8
    .line 2584
    .local v0, value:I
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$6400(Landroid/widget/NumberPicker;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_16

    #@10
    .line 2585
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@12
    invoke-static {v1, v0}, Landroid/widget/NumberPicker;->access$6500(Landroid/widget/NumberPicker;I)I

    #@15
    move-result v0

    #@16
    .line 2587
    :cond_16
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@18
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$1100(Landroid/widget/NumberPicker;)I

    #@1b
    move-result v1

    #@1c
    if-gt v0, v1, :cond_3e

    #@1e
    .line 2588
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@20
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    if-nez v1, :cond_2d

    #@26
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@28
    invoke-static {v1, v0}, Landroid/widget/NumberPicker;->access$6700(Landroid/widget/NumberPicker;I)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    .line 2591
    :goto_2c
    return-object v1

    #@2d
    .line 2588
    :cond_2d
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2f
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$900(Landroid/widget/NumberPicker;)[Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@35
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$6600(Landroid/widget/NumberPicker;)I

    #@38
    move-result v2

    #@39
    sub-int v2, v0, v2

    #@3b
    aget-object v1, v1, v2

    #@3d
    goto :goto_2c

    #@3e
    .line 2591
    :cond_3e
    const/4 v1, 0x0

    #@3f
    goto :goto_2c
.end method

.method private hasVirtualDecrementButton()Z
    .registers 3

    #@0
    .prologue
    .line 2563
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_16

    #@8
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    #@d
    move-result v0

    #@e
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@10
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getMinValue()I

    #@13
    move-result v1

    #@14
    if-le v0, v1, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method private hasVirtualIncrementButton()Z
    .registers 3

    #@0
    .prologue
    .line 2567
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_16

    #@8
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    #@d
    move-result v0

    #@e
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@10
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getMaxValue()I

    #@13
    move-result v1

    #@14
    if-ge v0, v1, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method private sendAccessibilityEventForVirtualButton(IILjava/lang/String;)V
    .registers 7
    .parameter "virtualViewId"
    .parameter "eventType"
    .parameter "text"

    #@0
    .prologue
    .line 2417
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$5700(Landroid/widget/NumberPicker;)Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_46

    #@10
    .line 2418
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@13
    move-result-object v0

    #@14
    .line 2419
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    const-class v1, Landroid/widget/Button;

    #@16
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@1d
    .line 2420
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1f
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$5800(Landroid/widget/NumberPicker;)Landroid/content/Context;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@2a
    .line 2421
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@2d
    move-result-object v1

    #@2e
    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    .line 2422
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@33
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@36
    move-result v1

    #@37
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    #@3a
    .line 2423
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@3c
    invoke-virtual {v0, v1, p1}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    #@3f
    .line 2424
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@41
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@43
    invoke-virtual {v1, v2, v0}, Landroid/widget/NumberPicker;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@46
    .line 2426
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_46
    return-void
.end method

.method private sendAccessibilityEventForVirtualText(I)V
    .registers 5
    .parameter "eventType"

    #@0
    .prologue
    .line 2406
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$5600(Landroid/widget/NumberPicker;)Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_33

    #@10
    .line 2407
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@13
    move-result-object v0

    #@14
    .line 2408
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@16
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@1d
    .line 2409
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1f
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@26
    .line 2410
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@28
    const/4 v2, 0x2

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    #@2c
    .line 2411
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2e
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@30
    invoke-virtual {v1, v2, v0}, Landroid/widget/NumberPicker;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@33
    .line 2413
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_33
    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 10
    .parameter "virtualViewId"

    #@0
    .prologue
    .line 2191
    packed-switch p1, :pswitch_data_c6

    #@3
    .line 2208
    :pswitch_3
    invoke-super {p0, p1}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 2193
    :pswitch_8
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$2900(Landroid/widget/NumberPicker;)I

    #@d
    move-result v0

    #@e
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@10
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$3000(Landroid/widget/NumberPicker;)I

    #@13
    move-result v1

    #@14
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@16
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$3100(Landroid/widget/NumberPicker;)I

    #@19
    move-result v2

    #@1a
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1c
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$3200(Landroid/widget/NumberPicker;)I

    #@1f
    move-result v3

    #@20
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@22
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$3300(Landroid/widget/NumberPicker;)I

    #@25
    move-result v4

    #@26
    sub-int/2addr v3, v4

    #@27
    add-int/2addr v2, v3

    #@28
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2a
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$3400(Landroid/widget/NumberPicker;)I

    #@2d
    move-result v3

    #@2e
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@30
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$3500(Landroid/widget/NumberPicker;)I

    #@33
    move-result v4

    #@34
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@36
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$3600(Landroid/widget/NumberPicker;)I

    #@39
    move-result v5

    #@3a
    sub-int/2addr v4, v5

    #@3b
    add-int/2addr v3, v4

    #@3c
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfoForNumberPicker(IIII)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3f
    move-result-object v0

    #@40
    goto :goto_7

    #@41
    .line 2196
    :pswitch_41
    const/4 v1, 0x3

    #@42
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualDecrementButtonText()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@48
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$3700(Landroid/widget/NumberPicker;)I

    #@4b
    move-result v3

    #@4c
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@4e
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$3800(Landroid/widget/NumberPicker;)I

    #@51
    move-result v4

    #@52
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@54
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$3900(Landroid/widget/NumberPicker;)I

    #@57
    move-result v0

    #@58
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@5a
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$4000(Landroid/widget/NumberPicker;)I

    #@5d
    move-result v5

    #@5e
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@60
    invoke-static {v6}, Landroid/widget/NumberPicker;->access$4100(Landroid/widget/NumberPicker;)I

    #@63
    move-result v6

    #@64
    sub-int/2addr v5, v6

    #@65
    add-int/2addr v5, v0

    #@66
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@68
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$1900(Landroid/widget/NumberPicker;)I

    #@6b
    move-result v0

    #@6c
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@6e
    invoke-static {v6}, Landroid/widget/NumberPicker;->access$4200(Landroid/widget/NumberPicker;)I

    #@71
    move-result v6

    #@72
    add-int/2addr v6, v0

    #@73
    move-object v0, p0

    #@74
    invoke-direct/range {v0 .. v6}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfoForVirtualButton(ILjava/lang/String;IIII)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@77
    move-result-object v0

    #@78
    goto :goto_7

    #@79
    .line 2201
    :pswitch_79
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibiltyNodeInfoForInputText()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@7c
    move-result-object v0

    #@7d
    goto :goto_7

    #@7e
    .line 2203
    :pswitch_7e
    const/4 v1, 0x1

    #@7f
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualIncrementButtonText()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@85
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$4300(Landroid/widget/NumberPicker;)I

    #@88
    move-result v3

    #@89
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@8b
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$1400(Landroid/widget/NumberPicker;)I

    #@8e
    move-result v0

    #@8f
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@91
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$4200(Landroid/widget/NumberPicker;)I

    #@94
    move-result v4

    #@95
    sub-int v4, v0, v4

    #@97
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@99
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$4400(Landroid/widget/NumberPicker;)I

    #@9c
    move-result v0

    #@9d
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@9f
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$4500(Landroid/widget/NumberPicker;)I

    #@a2
    move-result v5

    #@a3
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a5
    invoke-static {v6}, Landroid/widget/NumberPicker;->access$4600(Landroid/widget/NumberPicker;)I

    #@a8
    move-result v6

    #@a9
    sub-int/2addr v5, v6

    #@aa
    add-int/2addr v5, v0

    #@ab
    iget-object v0, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@ad
    invoke-static {v0}, Landroid/widget/NumberPicker;->access$4700(Landroid/widget/NumberPicker;)I

    #@b0
    move-result v0

    #@b1
    iget-object v6, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@b3
    invoke-static {v6}, Landroid/widget/NumberPicker;->access$4800(Landroid/widget/NumberPicker;)I

    #@b6
    move-result v6

    #@b7
    iget-object v7, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@b9
    invoke-static {v7}, Landroid/widget/NumberPicker;->access$4900(Landroid/widget/NumberPicker;)I

    #@bc
    move-result v7

    #@bd
    sub-int/2addr v6, v7

    #@be
    add-int/2addr v6, v0

    #@bf
    move-object v0, p0

    #@c0
    invoke-direct/range {v0 .. v6}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->createAccessibilityNodeInfoForVirtualButton(ILjava/lang/String;IIII)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@c3
    move-result-object v0

    #@c4
    goto/16 :goto_7

    #@c6
    .line 2191
    :pswitch_data_c6
    .packed-switch -0x1
        :pswitch_8
        :pswitch_3
        :pswitch_7e
        :pswitch_79
        :pswitch_41
    .end packed-switch
.end method

.method public findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;
    .registers 6
    .parameter "searched"
    .parameter "virtualViewId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2214
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_b

    #@6
    .line 2215
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 2237
    :goto_a
    return-object v0

    #@b
    .line 2217
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 2218
    .local v1, searchedLowerCase:Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    .line 2219
    .local v0, result:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    packed-switch p2, :pswitch_data_2e

    #@17
    .line 2237
    :pswitch_17
    invoke-super {p0, p1, p2}, Landroid/view/accessibility/AccessibilityNodeProvider;->findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;

    #@1a
    move-result-object v0

    #@1b
    goto :goto_a

    #@1c
    .line 2221
    :pswitch_1c
    const/4 v2, 0x3

    #@1d
    invoke-direct {p0, v1, v2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->findAccessibilityNodeInfosByTextInChild(Ljava/lang/String;ILjava/util/List;)V

    #@20
    .line 2223
    const/4 v2, 0x2

    #@21
    invoke-direct {p0, v1, v2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->findAccessibilityNodeInfosByTextInChild(Ljava/lang/String;ILjava/util/List;)V

    #@24
    .line 2225
    const/4 v2, 0x1

    #@25
    invoke-direct {p0, v1, v2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->findAccessibilityNodeInfosByTextInChild(Ljava/lang/String;ILjava/util/List;)V

    #@28
    goto :goto_a

    #@29
    .line 2232
    :pswitch_29
    invoke-direct {p0, v1, p2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->findAccessibilityNodeInfosByTextInChild(Ljava/lang/String;ILjava/util/List;)V

    #@2c
    goto :goto_a

    #@2d
    .line 2219
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch -0x1
        :pswitch_1c
        :pswitch_17
        :pswitch_29
        :pswitch_29
        :pswitch_29
    .end packed-switch
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .registers 10
    .parameter "virtualViewId"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/high16 v5, 0x1

    #@2
    const v4, 0x8000

    #@5
    const/high16 v3, -0x8000

    #@7
    const/4 v1, 0x1

    #@8
    const/4 v0, 0x0

    #@9
    .line 2242
    packed-switch p1, :pswitch_data_1b4

    #@c
    .line 2382
    :goto_c
    :pswitch_c
    invoke-super {p0, p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeProvider;->performAction(IILandroid/os/Bundle;)Z

    #@f
    move-result v0

    #@10
    :cond_10
    :goto_10
    return v0

    #@11
    .line 2244
    :pswitch_11
    sparse-switch p2, :sswitch_data_1c2

    #@14
    goto :goto_c

    #@15
    .line 2246
    :sswitch_15
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@17
    if-eq v2, p1, :cond_10

    #@19
    .line 2247
    iput p1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@1b
    .line 2248
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1d
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->requestAccessibilityFocus()Z

    #@20
    move v0, v1

    #@21
    .line 2249
    goto :goto_10

    #@22
    .line 2253
    :sswitch_22
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@24
    if-ne v2, p1, :cond_10

    #@26
    .line 2254
    iput v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@28
    .line 2255
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@2a
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->clearAccessibilityFocus()V

    #@2d
    move v0, v1

    #@2e
    .line 2256
    goto :goto_10

    #@2f
    .line 2261
    :sswitch_2f
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@31
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_10

    #@37
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@39
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_4d

    #@3f
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@41
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getValue()I

    #@44
    move-result v2

    #@45
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@47
    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getMaxValue()I

    #@4a
    move-result v3

    #@4b
    if-ge v2, v3, :cond_10

    #@4d
    .line 2263
    :cond_4d
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@4f
    invoke-static {v2, v1}, Landroid/widget/NumberPicker;->access$200(Landroid/widget/NumberPicker;Z)V

    #@52
    move v0, v1

    #@53
    .line 2264
    goto :goto_10

    #@54
    .line 2268
    :sswitch_54
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@56
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_10

    #@5c
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@5e
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getWrapSelectorWheel()Z

    #@61
    move-result v2

    #@62
    if-nez v2, :cond_72

    #@64
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@66
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getValue()I

    #@69
    move-result v2

    #@6a
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@6c
    invoke-virtual {v3}, Landroid/widget/NumberPicker;->getMinValue()I

    #@6f
    move-result v3

    #@70
    if-le v2, v3, :cond_10

    #@72
    .line 2270
    :cond_72
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@74
    invoke-static {v2, v0}, Landroid/widget/NumberPicker;->access$200(Landroid/widget/NumberPicker;Z)V

    #@77
    move v0, v1

    #@78
    .line 2271
    goto :goto_10

    #@79
    .line 2277
    :pswitch_79
    sparse-switch p2, :sswitch_data_1d4

    #@7c
    .line 2316
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@7e
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@81
    move-result-object v1

    #@82
    invoke-virtual {v1, p2, p3}, Landroid/widget/EditText;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@85
    move-result v0

    #@86
    goto :goto_10

    #@87
    .line 2279
    :sswitch_87
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@89
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@8c
    move-result v1

    #@8d
    if-eqz v1, :cond_10

    #@8f
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@91
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    #@98
    move-result v1

    #@99
    if-nez v1, :cond_10

    #@9b
    .line 2280
    iget-object v1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@9d
    invoke-static {v1}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@a0
    move-result-object v1

    #@a1
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    #@a4
    move-result v0

    #@a5
    goto/16 :goto_10

    #@a7
    .line 2284
    :sswitch_a7
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@a9
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@ac
    move-result v2

    #@ad
    if-eqz v2, :cond_10

    #@af
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@b1
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@b4
    move-result-object v2

    #@b5
    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    #@b8
    move-result v2

    #@b9
    if-eqz v2, :cond_10

    #@bb
    .line 2285
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@bd
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@c0
    move-result-object v2

    #@c1
    invoke-virtual {v2}, Landroid/widget/EditText;->clearFocus()V

    #@c4
    move v0, v1

    #@c5
    .line 2286
    goto/16 :goto_10

    #@c7
    .line 2291
    :sswitch_c7
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@c9
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@cc
    move-result v2

    #@cd
    if-eqz v2, :cond_10

    #@cf
    .line 2292
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@d1
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$2700(Landroid/widget/NumberPicker;)V

    #@d4
    move v0, v1

    #@d5
    .line 2293
    goto/16 :goto_10

    #@d7
    .line 2298
    :sswitch_d7
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@d9
    if-eq v2, p1, :cond_10

    #@db
    .line 2299
    iput p1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@dd
    .line 2300
    invoke-virtual {p0, p1, v4}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@e0
    .line 2302
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@e2
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@e5
    move-result-object v2

    #@e6
    invoke-virtual {v2}, Landroid/widget/EditText;->invalidate()V

    #@e9
    move v0, v1

    #@ea
    .line 2303
    goto/16 :goto_10

    #@ec
    .line 2307
    :sswitch_ec
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@ee
    if-ne v2, p1, :cond_10

    #@f0
    .line 2308
    iput v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@f2
    .line 2309
    invoke-virtual {p0, p1, v5}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@f5
    .line 2311
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@f7
    invoke-static {v2}, Landroid/widget/NumberPicker;->access$100(Landroid/widget/NumberPicker;)Landroid/widget/EditText;

    #@fa
    move-result-object v2

    #@fb
    invoke-virtual {v2}, Landroid/widget/EditText;->invalidate()V

    #@fe
    move v0, v1

    #@ff
    .line 2312
    goto/16 :goto_10

    #@101
    .line 2321
    :pswitch_101
    sparse-switch p2, :sswitch_data_1ea

    #@104
    goto/16 :goto_10

    #@106
    .line 2323
    :sswitch_106
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@108
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@10b
    move-result v2

    #@10c
    if-eqz v2, :cond_10

    #@10e
    .line 2324
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@110
    invoke-static {v2, v1}, Landroid/widget/NumberPicker;->access$200(Landroid/widget/NumberPicker;Z)V

    #@113
    .line 2325
    invoke-virtual {p0, p1, v1}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@116
    move v0, v1

    #@117
    .line 2327
    goto/16 :goto_10

    #@119
    .line 2331
    :sswitch_119
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@11b
    if-eq v2, p1, :cond_10

    #@11d
    .line 2332
    iput p1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@11f
    .line 2333
    invoke-virtual {p0, p1, v4}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@122
    .line 2335
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@124
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@126
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$1400(Landroid/widget/NumberPicker;)I

    #@129
    move-result v3

    #@12a
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@12c
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$5000(Landroid/widget/NumberPicker;)I

    #@12f
    move-result v4

    #@130
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@132
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$5100(Landroid/widget/NumberPicker;)I

    #@135
    move-result v5

    #@136
    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/widget/NumberPicker;->invalidate(IIII)V

    #@139
    move v0, v1

    #@13a
    .line 2336
    goto/16 :goto_10

    #@13c
    .line 2340
    :sswitch_13c
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@13e
    if-ne v2, p1, :cond_10

    #@140
    .line 2341
    iput v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@142
    .line 2342
    invoke-virtual {p0, p1, v5}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@145
    .line 2344
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@147
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@149
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$1400(Landroid/widget/NumberPicker;)I

    #@14c
    move-result v3

    #@14d
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@14f
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$5200(Landroid/widget/NumberPicker;)I

    #@152
    move-result v4

    #@153
    iget-object v5, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@155
    invoke-static {v5}, Landroid/widget/NumberPicker;->access$5300(Landroid/widget/NumberPicker;)I

    #@158
    move-result v5

    #@159
    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/widget/NumberPicker;->invalidate(IIII)V

    #@15c
    move v0, v1

    #@15d
    .line 2345
    goto/16 :goto_10

    #@15f
    .line 2351
    :pswitch_15f
    sparse-switch p2, :sswitch_data_1f8

    #@162
    goto/16 :goto_10

    #@164
    .line 2353
    :sswitch_164
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@166
    invoke-virtual {v2}, Landroid/widget/NumberPicker;->isEnabled()Z

    #@169
    move-result v2

    #@16a
    if-eqz v2, :cond_10

    #@16c
    .line 2354
    if-ne p1, v1, :cond_16f

    #@16e
    move v0, v1

    #@16f
    .line 2355
    .local v0, increment:Z
    :cond_16f
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@171
    invoke-static {v2, v0}, Landroid/widget/NumberPicker;->access$200(Landroid/widget/NumberPicker;Z)V

    #@174
    .line 2356
    invoke-virtual {p0, p1, v1}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@177
    move v0, v1

    #@178
    .line 2358
    goto/16 :goto_10

    #@17a
    .line 2362
    .end local v0           #increment:Z
    :sswitch_17a
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@17c
    if-eq v2, p1, :cond_10

    #@17e
    .line 2363
    iput p1, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@180
    .line 2364
    invoke-virtual {p0, p1, v4}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@183
    .line 2366
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@185
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@187
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$5400(Landroid/widget/NumberPicker;)I

    #@18a
    move-result v3

    #@18b
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@18d
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$1900(Landroid/widget/NumberPicker;)I

    #@190
    move-result v4

    #@191
    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/widget/NumberPicker;->invalidate(IIII)V

    #@194
    move v0, v1

    #@195
    .line 2367
    goto/16 :goto_10

    #@197
    .line 2371
    :sswitch_197
    iget v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@199
    if-ne v2, p1, :cond_10

    #@19b
    .line 2372
    iput v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->mAccessibilityFocusedView:I

    #@19d
    .line 2373
    invoke-virtual {p0, p1, v5}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualView(II)V

    #@1a0
    .line 2375
    iget-object v2, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1a2
    iget-object v3, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1a4
    invoke-static {v3}, Landroid/widget/NumberPicker;->access$5500(Landroid/widget/NumberPicker;)I

    #@1a7
    move-result v3

    #@1a8
    iget-object v4, p0, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->this$0:Landroid/widget/NumberPicker;

    #@1aa
    invoke-static {v4}, Landroid/widget/NumberPicker;->access$1900(Landroid/widget/NumberPicker;)I

    #@1ad
    move-result v4

    #@1ae
    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/widget/NumberPicker;->invalidate(IIII)V

    #@1b1
    move v0, v1

    #@1b2
    .line 2376
    goto/16 :goto_10

    #@1b4
    .line 2242
    :pswitch_data_1b4
    .packed-switch -0x1
        :pswitch_11
        :pswitch_c
        :pswitch_101
        :pswitch_79
        :pswitch_15f
    .end packed-switch

    #@1c2
    .line 2244
    :sswitch_data_1c2
    .sparse-switch
        0x40 -> :sswitch_15
        0x80 -> :sswitch_22
        0x1000 -> :sswitch_2f
        0x2000 -> :sswitch_54
    .end sparse-switch

    #@1d4
    .line 2277
    :sswitch_data_1d4
    .sparse-switch
        0x1 -> :sswitch_87
        0x2 -> :sswitch_a7
        0x10 -> :sswitch_c7
        0x40 -> :sswitch_d7
        0x80 -> :sswitch_ec
    .end sparse-switch

    #@1ea
    .line 2321
    :sswitch_data_1ea
    .sparse-switch
        0x10 -> :sswitch_106
        0x40 -> :sswitch_119
        0x80 -> :sswitch_13c
    .end sparse-switch

    #@1f8
    .line 2351
    :sswitch_data_1f8
    .sparse-switch
        0x10 -> :sswitch_164
        0x40 -> :sswitch_17a
        0x80 -> :sswitch_197
    .end sparse-switch
.end method

.method public sendAccessibilityEventForVirtualView(II)V
    .registers 4
    .parameter "virtualViewId"
    .parameter "eventType"

    #@0
    .prologue
    .line 2386
    packed-switch p1, :pswitch_data_24

    #@3
    .line 2403
    :cond_3
    :goto_3
    return-void

    #@4
    .line 2388
    :pswitch_4
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->hasVirtualDecrementButton()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_3

    #@a
    .line 2389
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualDecrementButtonText()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualButton(IILjava/lang/String;)V

    #@11
    goto :goto_3

    #@12
    .line 2394
    :pswitch_12
    invoke-direct {p0, p2}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualText(I)V

    #@15
    goto :goto_3

    #@16
    .line 2397
    :pswitch_16
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->hasVirtualIncrementButton()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_3

    #@1c
    .line 2398
    invoke-direct {p0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->getVirtualIncrementButtonText()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/NumberPicker$AccessibilityNodeProviderImpl;->sendAccessibilityEventForVirtualButton(IILjava/lang/String;)V

    #@23
    goto :goto_3

    #@24
    .line 2386
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_16
        :pswitch_12
        :pswitch_4
    .end packed-switch
.end method
