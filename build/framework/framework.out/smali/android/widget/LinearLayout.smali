.class public Landroid/widget/LinearLayout;
.super Landroid/view/ViewGroup;
.source "LinearLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/LinearLayout$LayoutParams;
    }
.end annotation


# static fields
.field public static final HORIZONTAL:I = 0x0

.field private static final INDEX_BOTTOM:I = 0x2

.field private static final INDEX_CENTER_VERTICAL:I = 0x0

.field private static final INDEX_FILL:I = 0x3

.field private static final INDEX_TOP:I = 0x1

.field public static final SHOW_DIVIDER_BEGINNING:I = 0x1

.field public static final SHOW_DIVIDER_END:I = 0x4

.field public static final SHOW_DIVIDER_MIDDLE:I = 0x2

.field public static final SHOW_DIVIDER_NONE:I = 0x0

.field public static final VERTICAL:I = 0x1

.field private static final VERTICAL_GRAVITY_COUNT:I = 0x4


# instance fields
.field private mBaselineAligned:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mBaselineAlignedChildIndex:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mBaselineChildTop:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerHeight:I

.field private mDividerPadding:I

.field private mDividerWidth:I

.field private mGravity:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = -0x1
                mask = -0x1
                name = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x0
                mask = 0x0
                name = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x30
                mask = 0x30
                name = "TOP"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x50
                mask = 0x50
                name = "BOTTOM"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x3
                mask = 0x3
                name = "LEFT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x5
                mask = 0x5
                name = "RIGHT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800003
                mask = 0x800003
                name = "START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800005
                mask = 0x800005
                name = "END"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x10
                mask = 0x10
                name = "CENTER_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x70
                mask = 0x70
                name = "FILL_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1
                mask = 0x1
                name = "CENTER_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x7
                mask = 0x7
                name = "FILL_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x11
                mask = 0x11
                name = "CENTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x77
                mask = 0x77
                name = "FILL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800000
                mask = 0x800000
                name = "RELATIVE"
            .end subannotation
        }
    .end annotation
.end field

.field private mMaxAscent:[I

.field private mMaxDescent:[I

.field private mOrientation:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mShowDividers:I

.field private mTotalLength:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mUseLargestChild:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mWeightSum:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 168
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@3
    .line 84
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@6
    .line 94
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@9
    .line 102
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/widget/LinearLayout;->mBaselineChildTop:I

    #@c
    .line 108
    const v0, 0x800033

    #@f
    iput v0, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@11
    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 172
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v8, 0x5

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v6, -0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 176
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8
    .line 84
    iput-boolean v4, p0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@a
    .line 94
    iput v6, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@c
    .line 102
    iput v5, p0, Landroid/widget/LinearLayout;->mBaselineChildTop:I

    #@e
    .line 108
    const v3, 0x800033

    #@11
    iput v3, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@13
    .line 178
    sget-object v3, Lcom/android/internal/R$styleable;->LinearLayout:[I

    #@15
    invoke-virtual {p1, p2, v3, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@18
    move-result-object v0

    #@19
    .line 181
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1c
    move-result v2

    #@1d
    .line 182
    .local v2, index:I
    if-ltz v2, :cond_22

    #@1f
    .line 183
    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@22
    .line 186
    :cond_22
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@25
    move-result v2

    #@26
    .line 187
    sget-boolean v3, Landroid/widget/LinearLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@28
    if-eqz v3, :cond_35

    #@2a
    .line 188
    and-int/lit8 v3, v2, 0x3

    #@2c
    if-eq v3, v7, :cond_32

    #@2e
    and-int/lit8 v3, v2, 0x5

    #@30
    if-ne v3, v8, :cond_35

    #@32
    .line 189
    :cond_32
    const/high16 v3, 0x80

    #@34
    or-int/2addr v2, v3

    #@35
    .line 192
    :cond_35
    if-ltz v2, :cond_3a

    #@37
    .line 193
    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    #@3a
    .line 196
    :cond_3a
    const/4 v3, 0x2

    #@3b
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3e
    move-result v1

    #@3f
    .line 197
    .local v1, baselineAligned:Z
    if-nez v1, :cond_44

    #@41
    .line 198
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setBaselineAligned(Z)V

    #@44
    .line 201
    :cond_44
    const/4 v3, 0x4

    #@45
    const/high16 v4, -0x4080

    #@47
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@4a
    move-result v3

    #@4b
    iput v3, p0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@4d
    .line 203
    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@50
    move-result v3

    #@51
    iput v3, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@53
    .line 206
    const/4 v3, 0x6

    #@54
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@57
    move-result v3

    #@58
    iput-boolean v3, p0, Landroid/widget/LinearLayout;->mUseLargestChild:Z

    #@5a
    .line 208
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    #@61
    .line 209
    const/4 v3, 0x7

    #@62
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@65
    move-result v3

    #@66
    iput v3, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@68
    .line 210
    const/16 v3, 0x8

    #@6a
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@6d
    move-result v3

    #@6e
    iput v3, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@70
    .line 212
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@73
    .line 213
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 59
    sget-boolean v0, Landroid/widget/LinearLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method private forceUniformHeight(II)V
    .registers 12
    .parameter "count"
    .parameter "widthMeasureSpec"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1351
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    #@4
    move-result v0

    #@5
    const/high16 v2, 0x4000

    #@7
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@a
    move-result v4

    #@b
    .line 1353
    .local v4, uniformMeasureSpec:I
    const/4 v6, 0x0

    #@c
    .local v6, i:I
    :goto_c
    if-ge v6, p1, :cond_38

    #@e
    .line 1354
    invoke-virtual {p0, v6}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    .line 1355
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@15
    move-result v0

    #@16
    const/16 v2, 0x8

    #@18
    if-eq v0, v2, :cond_35

    #@1a
    .line 1356
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1d
    move-result-object v7

    #@1e
    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    #@20
    .line 1358
    .local v7, lp:Landroid/widget/LinearLayout$LayoutParams;
    iget v0, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@22
    const/4 v2, -0x1

    #@23
    if-ne v0, v2, :cond_35

    #@25
    .line 1361
    iget v8, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@27
    .line 1362
    .local v8, oldWidth:I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    #@2a
    move-result v0

    #@2b
    iput v0, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2d
    move-object v0, p0

    #@2e
    move v2, p2

    #@2f
    move v5, v3

    #@30
    .line 1365
    invoke-virtual/range {v0 .. v5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@33
    .line 1366
    iput v8, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@35
    .line 1353
    .end local v7           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v8           #oldWidth:I
    :cond_35
    add-int/lit8 v6, v6, 0x1

    #@37
    goto :goto_c

    #@38
    .line 1370
    .end local v1           #child:Landroid/view/View;
    :cond_38
    return-void
.end method

.method private forceUniformWidth(II)V
    .registers 12
    .parameter "count"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 933
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@4
    move-result v0

    #@5
    const/high16 v4, 0x4000

    #@7
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@a
    move-result v2

    #@b
    .line 935
    .local v2, uniformMeasureSpec:I
    const/4 v6, 0x0

    #@c
    .local v6, i:I
    :goto_c
    if-ge v6, p1, :cond_38

    #@e
    .line 936
    invoke-virtual {p0, v6}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    .line 937
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@15
    move-result v0

    #@16
    const/16 v4, 0x8

    #@18
    if-eq v0, v4, :cond_35

    #@1a
    .line 938
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1d
    move-result-object v7

    #@1e
    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    #@20
    .line 940
    .local v7, lp:Landroid/widget/LinearLayout$LayoutParams;
    iget v0, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@22
    const/4 v4, -0x1

    #@23
    if-ne v0, v4, :cond_35

    #@25
    .line 943
    iget v8, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@27
    .line 944
    .local v8, oldHeight:I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    #@2a
    move-result v0

    #@2b
    iput v0, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2d
    move-object v0, p0

    #@2e
    move v4, p2

    #@2f
    move v5, v3

    #@30
    .line 947
    invoke-virtual/range {v0 .. v5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@33
    .line 948
    iput v8, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@35
    .line 935
    .end local v7           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v8           #oldHeight:I
    :cond_35
    add-int/lit8 v6, v6, 0x1

    #@37
    goto :goto_c

    #@38
    .line 952
    .end local v1           #child:Landroid/view/View;
    :cond_38
    return-void
.end method

.method private setChildFrame(Landroid/view/View;IIII)V
    .registers 8
    .parameter "child"
    .parameter "left"
    .parameter "top"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1670
    add-int v0, p2, p4

    #@2
    add-int v1, p3, p5

    #@4
    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    #@7
    .line 1671
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1777
    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    #@2
    return v0
.end method

.method drawDividersHorizontal(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    .line 354
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@3
    move-result v1

    #@4
    .line 355
    .local v1, count:I
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isLayoutRtl()Z

    #@7
    move-result v3

    #@8
    .line 356
    .local v3, isLayoutRtl:Z
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v1, :cond_41

    #@b
    .line 357
    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    .line 359
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_32

    #@11
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@14
    move-result v6

    #@15
    const/16 v7, 0x8

    #@17
    if-eq v6, v7, :cond_32

    #@19
    .line 360
    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_32

    #@1f
    .line 361
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@22
    move-result-object v4

    #@23
    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    #@25
    .line 363
    .local v4, lp:Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v3, :cond_35

    #@27
    .line 364
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@2a
    move-result v6

    #@2b
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@2d
    add-int v5, v6, v7

    #@2f
    .line 368
    .local v5, position:I
    :goto_2f
    invoke-virtual {p0, p1, v5}, Landroid/widget/LinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    #@32
    .line 356
    .end local v4           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v5           #position:I
    :cond_32
    add-int/lit8 v2, v2, 0x1

    #@34
    goto :goto_9

    #@35
    .line 366
    .restart local v4       #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_35
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@38
    move-result v6

    #@39
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@3b
    sub-int/2addr v6, v7

    #@3c
    iget v7, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@3e
    sub-int v5, v6, v7

    #@40
    .restart local v5       #position:I
    goto :goto_2f

    #@41
    .line 373
    .end local v0           #child:Landroid/view/View;
    .end local v4           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v5           #position:I
    :cond_41
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@44
    move-result v6

    #@45
    if-eqz v6, :cond_58

    #@47
    .line 374
    add-int/lit8 v6, v1, -0x1

    #@49
    invoke-virtual {p0, v6}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@4c
    move-result-object v0

    #@4d
    .line 376
    .restart local v0       #child:Landroid/view/View;
    if-nez v0, :cond_67

    #@4f
    .line 377
    if-eqz v3, :cond_59

    #@51
    .line 378
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    #@54
    move-result v5

    #@55
    .line 390
    .restart local v5       #position:I
    :goto_55
    invoke-virtual {p0, p1, v5}, Landroid/widget/LinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    #@58
    .line 392
    .end local v0           #child:Landroid/view/View;
    .end local v5           #position:I
    :cond_58
    return-void

    #@59
    .line 380
    .restart local v0       #child:Landroid/view/View;
    :cond_59
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getWidth()I

    #@5c
    move-result v6

    #@5d
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    #@60
    move-result v7

    #@61
    sub-int/2addr v6, v7

    #@62
    iget v7, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@64
    sub-int v5, v6, v7

    #@66
    .restart local v5       #position:I
    goto :goto_55

    #@67
    .line 383
    .end local v5           #position:I
    :cond_67
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@6a
    move-result-object v4

    #@6b
    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    #@6d
    .line 384
    .restart local v4       #lp:Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v3, :cond_7b

    #@6f
    .line 385
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@72
    move-result v6

    #@73
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@75
    sub-int/2addr v6, v7

    #@76
    iget v7, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@78
    sub-int v5, v6, v7

    #@7a
    .restart local v5       #position:I
    goto :goto_55

    #@7b
    .line 387
    .end local v5           #position:I
    :cond_7b
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@7e
    move-result v6

    #@7f
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@81
    add-int v5, v6, v7

    #@83
    .restart local v5       #position:I
    goto :goto_55
.end method

.method drawDividersVertical(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    .line 327
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@3
    move-result v2

    #@4
    .line 328
    .local v2, count:I
    const/4 v3, 0x0

    #@5
    .local v3, i:I
    :goto_5
    if-ge v3, v2, :cond_32

    #@7
    .line 329
    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 331
    .local v1, child:Landroid/view/View;
    if-eqz v1, :cond_2f

    #@d
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@10
    move-result v6

    #@11
    const/16 v7, 0x8

    #@13
    if-eq v6, v7, :cond_2f

    #@15
    .line 332
    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@18
    move-result v6

    #@19
    if-eqz v6, :cond_2f

    #@1b
    .line 333
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1e
    move-result-object v4

    #@1f
    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    #@21
    .line 334
    .local v4, lp:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@24
    move-result v6

    #@25
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@27
    sub-int/2addr v6, v7

    #@28
    iget v7, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@2a
    sub-int v5, v6, v7

    #@2c
    .line 335
    .local v5, top:I
    invoke-virtual {p0, p1, v5}, Landroid/widget/LinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    #@2f
    .line 328
    .end local v4           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v5           #top:I
    :cond_2f
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_5

    #@32
    .line 340
    .end local v1           #child:Landroid/view/View;
    :cond_32
    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@35
    move-result v6

    #@36
    if-eqz v6, :cond_51

    #@38
    .line 341
    add-int/lit8 v6, v2, -0x1

    #@3a
    invoke-virtual {p0, v6}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@3d
    move-result-object v1

    #@3e
    .line 342
    .restart local v1       #child:Landroid/view/View;
    const/4 v0, 0x0

    #@3f
    .line 343
    .local v0, bottom:I
    if-nez v1, :cond_52

    #@41
    .line 344
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getHeight()I

    #@44
    move-result v6

    #@45
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    #@48
    move-result v7

    #@49
    sub-int/2addr v6, v7

    #@4a
    iget v7, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@4c
    sub-int v0, v6, v7

    #@4e
    .line 349
    :goto_4e
    invoke-virtual {p0, p1, v0}, Landroid/widget/LinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    #@51
    .line 351
    .end local v0           #bottom:I
    .end local v1           #child:Landroid/view/View;
    :cond_51
    return-void

    #@52
    .line 346
    .restart local v0       #bottom:I
    .restart local v1       #child:Landroid/view/View;
    :cond_52
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@55
    move-result-object v4

    #@56
    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    #@58
    .line 347
    .restart local v4       #lp:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@5b
    move-result v6

    #@5c
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@5e
    add-int v0, v6, v7

    #@60
    goto :goto_4e
.end method

.method drawHorizontalDivider(Landroid/graphics/Canvas;I)V
    .registers 7
    .parameter "canvas"
    .parameter "top"

    #@0
    .prologue
    .line 395
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@8
    add-int/2addr v1, v2

    #@9
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getWidth()I

    #@c
    move-result v2

    #@d
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    #@10
    move-result v3

    #@11
    sub-int/2addr v2, v3

    #@12
    iget v3, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@14
    sub-int/2addr v2, v3

    #@15
    iget v3, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@17
    add-int/2addr v3, p2

    #@18
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1b
    .line 397
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@1d
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@20
    .line 398
    return-void
.end method

.method drawVerticalDivider(Landroid/graphics/Canvas;I)V
    .registers 8
    .parameter "canvas"
    .parameter "left"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingTop()I

    #@5
    move-result v1

    #@6
    iget v2, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@b
    add-int/2addr v2, p2

    #@c
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getHeight()I

    #@f
    move-result v3

    #@10
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    #@13
    move-result v4

    #@14
    sub-int/2addr v3, v4

    #@15
    iget v4, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@17
    sub-int/2addr v3, v4

    #@18
    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1b
    .line 403
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@1d
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@20
    .line 404
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x2

    #@1
    .line 1760
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@3
    if-nez v0, :cond_b

    #@5
    .line 1761
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@7
    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@a
    .line 1765
    :goto_a
    return-object v0

    #@b
    .line 1762
    :cond_b
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@d
    const/4 v1, 0x1

    #@e
    if-ne v0, v1, :cond_17

    #@10
    .line 1763
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@12
    const/4 v1, -0x1

    #@13
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@16
    goto :goto_a

    #@17
    .line 1765
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_a
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 1747
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1770
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public getBaseline()I
    .registers 9

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 463
    iget v6, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@3
    if-gez v6, :cond_a

    #@5
    .line 464
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    #@8
    move-result v5

    #@9
    .line 511
    :cond_9
    :goto_9
    return v5

    #@a
    .line 467
    :cond_a
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@d
    move-result v6

    #@e
    iget v7, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@10
    if-gt v6, v7, :cond_1b

    #@12
    .line 468
    new-instance v5, Ljava/lang/RuntimeException;

    #@14
    const-string/jumbo v6, "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."

    #@17
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v5

    #@1b
    .line 472
    :cond_1b
    iget v6, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@1d
    invoke-virtual {p0, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v0

    #@21
    .line 473
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@24
    move-result v1

    #@25
    .line 475
    .local v1, childBaseline:I
    if-ne v1, v5, :cond_34

    #@27
    .line 476
    iget v6, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@29
    if-eqz v6, :cond_9

    #@2b
    .line 482
    new-instance v5, Ljava/lang/RuntimeException;

    #@2d
    const-string/jumbo v6, "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline."

    #@30
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@33
    throw v5

    #@34
    .line 492
    :cond_34
    iget v2, p0, Landroid/widget/LinearLayout;->mBaselineChildTop:I

    #@36
    .line 494
    .local v2, childTop:I
    iget v5, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@38
    const/4 v6, 0x1

    #@39
    if-ne v5, v6, :cond_46

    #@3b
    .line 495
    iget v5, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@3d
    and-int/lit8 v4, v5, 0x70

    #@3f
    .line 496
    .local v4, majorGravity:I
    const/16 v5, 0x30

    #@41
    if-eq v4, v5, :cond_46

    #@43
    .line 497
    sparse-switch v4, :sswitch_data_70

    #@46
    .line 510
    .end local v4           #majorGravity:I
    :cond_46
    :goto_46
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@49
    move-result-object v3

    #@4a
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    #@4c
    .line 511
    .local v3, lp:Landroid/widget/LinearLayout$LayoutParams;
    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@4e
    add-int/2addr v5, v2

    #@4f
    add-int/2addr v5, v1

    #@50
    goto :goto_9

    #@51
    .line 499
    .end local v3           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .restart local v4       #majorGravity:I
    :sswitch_51
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@53
    iget v6, p0, Landroid/view/View;->mTop:I

    #@55
    sub-int/2addr v5, v6

    #@56
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@58
    sub-int/2addr v5, v6

    #@59
    iget v6, p0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@5b
    sub-int v2, v5, v6

    #@5d
    .line 500
    goto :goto_46

    #@5e
    .line 503
    :sswitch_5e
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@60
    iget v6, p0, Landroid/view/View;->mTop:I

    #@62
    sub-int/2addr v5, v6

    #@63
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@65
    sub-int/2addr v5, v6

    #@66
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@68
    sub-int/2addr v5, v6

    #@69
    iget v6, p0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@6b
    sub-int/2addr v5, v6

    #@6c
    div-int/lit8 v5, v5, 0x2

    #@6e
    add-int/2addr v2, v5

    #@6f
    goto :goto_46

    #@70
    .line 497
    :sswitch_data_70
    .sparse-switch
        0x10 -> :sswitch_5e
        0x50 -> :sswitch_51
    .end sparse-switch
.end method

.method public getBaselineAlignedChildIndex()I
    .registers 2

    #@0
    .prologue
    .line 520
    iget v0, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@2
    return v0
.end method

.method getChildrenSkipCount(Landroid/view/View;I)I
    .registers 4
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 1381
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 250
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getDividerPadding()I
    .registers 2

    #@0
    .prologue
    .line 301
    iget v0, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@2
    return v0
.end method

.method public getDividerWidth()I
    .registers 2

    #@0
    .prologue
    .line 310
    iget v0, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@2
    return v0
.end method

.method getLocationOffset(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 1423
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method getNextLocationOffset(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 1435
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getOrientation()I
    .registers 2

    #@0
    .prologue
    .line 1693
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@2
    return v0
.end method

.method public getShowDividers()I
    .registers 2

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@2
    return v0
.end method

.method getVirtualChildAt(I)Landroid/view/View;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 548
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method getVirtualChildCount()I
    .registers 2

    #@0
    .prologue
    .line 561
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getWeightSum()F
    .registers 2

    #@0
    .prologue
    .line 572
    iget v0, p0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@2
    return v0
.end method

.method protected hasDividerBeforeChildAt(I)Z
    .registers 7
    .parameter "childIndex"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 609
    if-nez p1, :cond_d

    #@4
    .line 610
    iget v4, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@6
    and-int/lit8 v4, v4, 0x1

    #@8
    if-eqz v4, :cond_b

    #@a
    .line 623
    :cond_a
    :goto_a
    return v2

    #@b
    :cond_b
    move v2, v3

    #@c
    .line 610
    goto :goto_a

    #@d
    .line 611
    :cond_d
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@10
    move-result v4

    #@11
    if-ne p1, v4, :cond_1b

    #@13
    .line 612
    iget v4, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@15
    and-int/lit8 v4, v4, 0x4

    #@17
    if-nez v4, :cond_a

    #@19
    move v2, v3

    #@1a
    goto :goto_a

    #@1b
    .line 613
    :cond_1b
    iget v2, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@1d
    and-int/lit8 v2, v2, 0x2

    #@1f
    if-eqz v2, :cond_38

    #@21
    .line 614
    const/4 v0, 0x0

    #@22
    .line 615
    .local v0, hasVisibleViewBefore:Z
    add-int/lit8 v1, p1, -0x1

    #@24
    .local v1, i:I
    :goto_24
    if-ltz v1, :cond_33

    #@26
    .line 616
    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@2d
    move-result v2

    #@2e
    const/16 v3, 0x8

    #@30
    if-eq v2, v3, :cond_35

    #@32
    .line 617
    const/4 v0, 0x1

    #@33
    :cond_33
    move v2, v0

    #@34
    .line 621
    goto :goto_a

    #@35
    .line 615
    :cond_35
    add-int/lit8 v1, v1, -0x1

    #@37
    goto :goto_24

    #@38
    .end local v0           #hasVisibleViewBefore:Z
    .end local v1           #i:I
    :cond_38
    move v2, v3

    #@39
    .line 623
    goto :goto_a
.end method

.method public isBaselineAligned()Z
    .registers 2

    #@0
    .prologue
    .line 413
    iget-boolean v0, p0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@2
    return v0
.end method

.method public isMeasureWithLargestChildEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 441
    iget-boolean v0, p0, Landroid/widget/LinearLayout;->mUseLargestChild:Z

    #@2
    return v0
.end method

.method layoutHorizontal()V
    .registers 31

    #@0
    .prologue
    .line 1546
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->isLayoutRtl()Z

    #@3
    move-result v20

    #@4
    .line 1547
    .local v20, isLayoutRtl:Z
    move-object/from16 v0, p0

    #@6
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@8
    move/from16 v27, v0

    #@a
    .line 1553
    .local v27, paddingTop:I
    move-object/from16 v0, p0

    #@c
    iget v2, v0, Landroid/view/View;->mBottom:I

    #@e
    move-object/from16 v0, p0

    #@10
    iget v4, v0, Landroid/view/View;->mTop:I

    #@12
    sub-int v18, v2, v4

    #@14
    .line 1554
    .local v18, height:I
    move-object/from16 v0, p0

    #@16
    iget v2, v0, Landroid/view/View;->mPaddingBottom:I

    #@18
    sub-int v10, v18, v2

    #@1a
    .line 1557
    .local v10, childBottom:I
    sub-int v2, v18, v27

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget v4, v0, Landroid/view/View;->mPaddingBottom:I

    #@20
    sub-int v13, v2, v4

    #@22
    .line 1559
    .local v13, childSpace:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@25
    move-result v14

    #@26
    .line 1561
    .local v14, count:I
    move-object/from16 v0, p0

    #@28
    iget v2, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@2a
    const v4, 0x800007

    #@2d
    and-int v23, v2, v4

    #@2f
    .line 1562
    .local v23, majorGravity:I
    move-object/from16 v0, p0

    #@31
    iget v2, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@33
    and-int/lit8 v26, v2, 0x70

    #@35
    .line 1564
    .local v26, minorGravity:I
    move-object/from16 v0, p0

    #@37
    iget-boolean v8, v0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@39
    .line 1566
    .local v8, baselineAligned:Z
    move-object/from16 v0, p0

    #@3b
    iget-object v0, v0, Landroid/widget/LinearLayout;->mMaxAscent:[I

    #@3d
    move-object/from16 v24, v0

    #@3f
    .line 1567
    .local v24, maxAscent:[I
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Landroid/widget/LinearLayout;->mMaxDescent:[I

    #@43
    move-object/from16 v25, v0

    #@45
    .line 1569
    .local v25, maxDescent:[I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getLayoutDirection()I

    #@48
    move-result v21

    #@49
    .line 1570
    .local v21, layoutDirection:I
    move/from16 v0, v23

    #@4b
    move/from16 v1, v21

    #@4d
    invoke-static {v0, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@50
    move-result v2

    #@51
    sparse-switch v2, :sswitch_data_154

    #@54
    .line 1583
    move-object/from16 v0, p0

    #@56
    iget v12, v0, Landroid/view/View;->mPaddingLeft:I

    #@58
    .line 1587
    .local v12, childLeft:I
    :goto_58
    const/16 v28, 0x0

    #@5a
    .line 1588
    .local v28, start:I
    const/16 v16, 0x1

    #@5c
    .line 1590
    .local v16, dir:I
    if-eqz v20, :cond_62

    #@5e
    .line 1591
    add-int/lit8 v28, v14, -0x1

    #@60
    .line 1592
    const/16 v16, -0x1

    #@62
    .line 1595
    :cond_62
    const/16 v19, 0x0

    #@64
    .local v19, i:I
    :goto_64
    move/from16 v0, v19

    #@66
    if-ge v0, v14, :cond_153

    #@68
    .line 1596
    mul-int v2, v16, v19

    #@6a
    add-int v11, v28, v2

    #@6c
    .line 1597
    .local v11, childIndex:I
    move-object/from16 v0, p0

    #@6e
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@71
    move-result-object v3

    #@72
    .line 1599
    .local v3, child:Landroid/view/View;
    if-nez v3, :cond_b0

    #@74
    .line 1600
    move-object/from16 v0, p0

    #@76
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@79
    move-result v2

    #@7a
    add-int/2addr v12, v2

    #@7b
    .line 1595
    :cond_7b
    :goto_7b
    add-int/lit8 v19, v19, 0x1

    #@7d
    goto :goto_64

    #@7e
    .line 1573
    .end local v3           #child:Landroid/view/View;
    .end local v11           #childIndex:I
    .end local v12           #childLeft:I
    .end local v16           #dir:I
    .end local v19           #i:I
    .end local v28           #start:I
    :sswitch_7e
    move-object/from16 v0, p0

    #@80
    iget v2, v0, Landroid/view/View;->mPaddingLeft:I

    #@82
    move-object/from16 v0, p0

    #@84
    iget v4, v0, Landroid/view/View;->mRight:I

    #@86
    add-int/2addr v2, v4

    #@87
    move-object/from16 v0, p0

    #@89
    iget v4, v0, Landroid/view/View;->mLeft:I

    #@8b
    sub-int/2addr v2, v4

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget v4, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@90
    sub-int v12, v2, v4

    #@92
    .line 1574
    .restart local v12       #childLeft:I
    goto :goto_58

    #@93
    .line 1578
    .end local v12           #childLeft:I
    :sswitch_93
    move-object/from16 v0, p0

    #@95
    iget v2, v0, Landroid/view/View;->mPaddingLeft:I

    #@97
    move-object/from16 v0, p0

    #@99
    iget v4, v0, Landroid/view/View;->mRight:I

    #@9b
    move-object/from16 v0, p0

    #@9d
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@9f
    move/from16 v29, v0

    #@a1
    sub-int v4, v4, v29

    #@a3
    move-object/from16 v0, p0

    #@a5
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@a7
    move/from16 v29, v0

    #@a9
    sub-int v4, v4, v29

    #@ab
    div-int/lit8 v4, v4, 0x2

    #@ad
    add-int v12, v2, v4

    #@af
    .line 1579
    .restart local v12       #childLeft:I
    goto :goto_58

    #@b0
    .line 1601
    .restart local v3       #child:Landroid/view/View;
    .restart local v11       #childIndex:I
    .restart local v16       #dir:I
    .restart local v19       #i:I
    .restart local v28       #start:I
    :cond_b0
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@b3
    move-result v2

    #@b4
    const/16 v4, 0x8

    #@b6
    if-eq v2, v4, :cond_7b

    #@b8
    .line 1602
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@bb
    move-result v6

    #@bc
    .line 1603
    .local v6, childWidth:I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@bf
    move-result v7

    #@c0
    .line 1604
    .local v7, childHeight:I
    const/4 v9, -0x1

    #@c1
    .line 1606
    .local v9, childBaseline:I
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@c4
    move-result-object v22

    #@c5
    check-cast v22, Landroid/widget/LinearLayout$LayoutParams;

    #@c7
    .line 1609
    .local v22, lp:Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v8, :cond_d4

    #@c9
    move-object/from16 v0, v22

    #@cb
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@cd
    const/4 v4, -0x1

    #@ce
    if-eq v2, v4, :cond_d4

    #@d0
    .line 1610
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    #@d3
    move-result v9

    #@d4
    .line 1613
    :cond_d4
    move-object/from16 v0, v22

    #@d6
    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@d8
    move/from16 v17, v0

    #@da
    .line 1614
    .local v17, gravity:I
    if-gez v17, :cond_de

    #@dc
    .line 1615
    move/from16 v17, v26

    #@de
    .line 1618
    :cond_de
    and-int/lit8 v2, v17, 0x70

    #@e0
    sparse-switch v2, :sswitch_data_15e

    #@e3
    .line 1650
    move/from16 v5, v27

    #@e5
    .line 1654
    .local v5, childTop:I
    :cond_e5
    :goto_e5
    move-object/from16 v0, p0

    #@e7
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@ea
    move-result v2

    #@eb
    if-eqz v2, :cond_f2

    #@ed
    .line 1655
    move-object/from16 v0, p0

    #@ef
    iget v2, v0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@f1
    add-int/2addr v12, v2

    #@f2
    .line 1658
    :cond_f2
    move-object/from16 v0, v22

    #@f4
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@f6
    add-int/2addr v12, v2

    #@f7
    .line 1659
    move-object/from16 v0, p0

    #@f9
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getLocationOffset(Landroid/view/View;)I

    #@fc
    move-result v2

    #@fd
    add-int v4, v12, v2

    #@ff
    move-object/from16 v2, p0

    #@101
    invoke-direct/range {v2 .. v7}, Landroid/widget/LinearLayout;->setChildFrame(Landroid/view/View;IIII)V

    #@104
    .line 1661
    move-object/from16 v0, v22

    #@106
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@108
    add-int/2addr v2, v6

    #@109
    move-object/from16 v0, p0

    #@10b
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@10e
    move-result v4

    #@10f
    add-int/2addr v2, v4

    #@110
    add-int/2addr v12, v2

    #@111
    .line 1664
    move-object/from16 v0, p0

    #@113
    invoke-virtual {v0, v3, v11}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@116
    move-result v2

    #@117
    add-int v19, v19, v2

    #@119
    goto/16 :goto_7b

    #@11b
    .line 1620
    .end local v5           #childTop:I
    :sswitch_11b
    move-object/from16 v0, v22

    #@11d
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@11f
    add-int v5, v27, v2

    #@121
    .line 1621
    .restart local v5       #childTop:I
    const/4 v2, -0x1

    #@122
    if-eq v9, v2, :cond_e5

    #@124
    .line 1622
    const/4 v2, 0x1

    #@125
    aget v2, v24, v2

    #@127
    sub-int/2addr v2, v9

    #@128
    add-int/2addr v5, v2

    #@129
    goto :goto_e5

    #@12a
    .line 1638
    .end local v5           #childTop:I
    :sswitch_12a
    sub-int v2, v13, v7

    #@12c
    div-int/lit8 v2, v2, 0x2

    #@12e
    add-int v2, v2, v27

    #@130
    move-object/from16 v0, v22

    #@132
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@134
    add-int/2addr v2, v4

    #@135
    move-object/from16 v0, v22

    #@137
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@139
    sub-int v5, v2, v4

    #@13b
    .line 1640
    .restart local v5       #childTop:I
    goto :goto_e5

    #@13c
    .line 1643
    .end local v5           #childTop:I
    :sswitch_13c
    sub-int v2, v10, v7

    #@13e
    move-object/from16 v0, v22

    #@140
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@142
    sub-int v5, v2, v4

    #@144
    .line 1644
    .restart local v5       #childTop:I
    const/4 v2, -0x1

    #@145
    if-eq v9, v2, :cond_e5

    #@147
    .line 1645
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@14a
    move-result v2

    #@14b
    sub-int v15, v2, v9

    #@14d
    .line 1646
    .local v15, descent:I
    const/4 v2, 0x2

    #@14e
    aget v2, v25, v2

    #@150
    sub-int/2addr v2, v15

    #@151
    sub-int/2addr v5, v2

    #@152
    .line 1647
    goto :goto_e5

    #@153
    .line 1667
    .end local v3           #child:Landroid/view/View;
    .end local v5           #childTop:I
    .end local v6           #childWidth:I
    .end local v7           #childHeight:I
    .end local v9           #childBaseline:I
    .end local v11           #childIndex:I
    .end local v15           #descent:I
    .end local v17           #gravity:I
    .end local v22           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_153
    return-void

    #@154
    .line 1570
    :sswitch_data_154
    .sparse-switch
        0x1 -> :sswitch_93
        0x5 -> :sswitch_7e
    .end sparse-switch

    #@15e
    .line 1618
    :sswitch_data_15e
    .sparse-switch
        0x10 -> :sswitch_12a
        0x30 -> :sswitch_11b
        0x50 -> :sswitch_13c
    .end sparse-switch
.end method

.method layoutVertical()V
    .registers 22

    #@0
    .prologue
    .line 1456
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@4
    move/from16 v18, v0

    #@6
    .line 1462
    .local v18, paddingLeft:I
    move-object/from16 v0, p0

    #@8
    iget v1, v0, Landroid/view/View;->mRight:I

    #@a
    move-object/from16 v0, p0

    #@c
    iget v4, v0, Landroid/view/View;->mLeft:I

    #@e
    sub-int v19, v1, v4

    #@10
    .line 1463
    .local v19, width:I
    move-object/from16 v0, p0

    #@12
    iget v1, v0, Landroid/view/View;->mPaddingRight:I

    #@14
    sub-int v8, v19, v1

    #@16
    .line 1466
    .local v8, childRight:I
    sub-int v1, v19, v18

    #@18
    move-object/from16 v0, p0

    #@1a
    iget v4, v0, Landroid/view/View;->mPaddingRight:I

    #@1c
    sub-int v9, v1, v4

    #@1e
    .line 1468
    .local v9, childSpace:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@21
    move-result v11

    #@22
    .line 1470
    .local v11, count:I
    move-object/from16 v0, p0

    #@24
    iget v1, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@26
    and-int/lit8 v16, v1, 0x70

    #@28
    .line 1471
    .local v16, majorGravity:I
    move-object/from16 v0, p0

    #@2a
    iget v1, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@2c
    const v4, 0x800007

    #@2f
    and-int v17, v1, v4

    #@31
    .line 1473
    .local v17, minorGravity:I
    sparse-switch v16, :sswitch_data_f4

    #@34
    .line 1486
    move-object/from16 v0, p0

    #@36
    iget v10, v0, Landroid/view/View;->mPaddingTop:I

    #@38
    .line 1490
    .local v10, childTop:I
    :goto_38
    const/4 v13, 0x0

    #@39
    .local v13, i:I
    :goto_39
    if-ge v13, v11, :cond_f2

    #@3b
    .line 1491
    move-object/from16 v0, p0

    #@3d
    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@40
    move-result-object v2

    #@41
    .line 1492
    .local v2, child:Landroid/view/View;
    if-nez v2, :cond_7f

    #@43
    .line 1493
    move-object/from16 v0, p0

    #@45
    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@48
    move-result v1

    #@49
    add-int/2addr v10, v1

    #@4a
    .line 1490
    :cond_4a
    :goto_4a
    add-int/lit8 v13, v13, 0x1

    #@4c
    goto :goto_39

    #@4d
    .line 1476
    .end local v2           #child:Landroid/view/View;
    .end local v10           #childTop:I
    .end local v13           #i:I
    :sswitch_4d
    move-object/from16 v0, p0

    #@4f
    iget v1, v0, Landroid/view/View;->mPaddingTop:I

    #@51
    move-object/from16 v0, p0

    #@53
    iget v4, v0, Landroid/view/View;->mBottom:I

    #@55
    add-int/2addr v1, v4

    #@56
    move-object/from16 v0, p0

    #@58
    iget v4, v0, Landroid/view/View;->mTop:I

    #@5a
    sub-int/2addr v1, v4

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget v4, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@5f
    sub-int v10, v1, v4

    #@61
    .line 1477
    .restart local v10       #childTop:I
    goto :goto_38

    #@62
    .line 1481
    .end local v10           #childTop:I
    :sswitch_62
    move-object/from16 v0, p0

    #@64
    iget v1, v0, Landroid/view/View;->mPaddingTop:I

    #@66
    move-object/from16 v0, p0

    #@68
    iget v4, v0, Landroid/view/View;->mBottom:I

    #@6a
    move-object/from16 v0, p0

    #@6c
    iget v0, v0, Landroid/view/View;->mTop:I

    #@6e
    move/from16 v20, v0

    #@70
    sub-int v4, v4, v20

    #@72
    move-object/from16 v0, p0

    #@74
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@76
    move/from16 v20, v0

    #@78
    sub-int v4, v4, v20

    #@7a
    div-int/lit8 v4, v4, 0x2

    #@7c
    add-int v10, v1, v4

    #@7e
    .line 1482
    .restart local v10       #childTop:I
    goto :goto_38

    #@7f
    .line 1494
    .restart local v2       #child:Landroid/view/View;
    .restart local v13       #i:I
    :cond_7f
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@82
    move-result v1

    #@83
    const/16 v4, 0x8

    #@85
    if-eq v1, v4, :cond_4a

    #@87
    .line 1495
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    #@8a
    move-result v5

    #@8b
    .line 1496
    .local v5, childWidth:I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    #@8e
    move-result v6

    #@8f
    .line 1498
    .local v6, childHeight:I
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@92
    move-result-object v15

    #@93
    check-cast v15, Landroid/widget/LinearLayout$LayoutParams;

    #@95
    .line 1501
    .local v15, lp:Landroid/widget/LinearLayout$LayoutParams;
    iget v12, v15, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@97
    .line 1502
    .local v12, gravity:I
    if-gez v12, :cond_9b

    #@99
    .line 1503
    move/from16 v12, v17

    #@9b
    .line 1505
    :cond_9b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getLayoutDirection()I

    #@9e
    move-result v14

    #@9f
    .line 1506
    .local v14, layoutDirection:I
    invoke-static {v12, v14}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@a2
    move-result v7

    #@a3
    .line 1507
    .local v7, absoluteGravity:I
    and-int/lit8 v1, v7, 0x7

    #@a5
    sparse-switch v1, :sswitch_data_fe

    #@a8
    .line 1519
    iget v1, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@aa
    add-int v3, v18, v1

    #@ac
    .line 1523
    .local v3, childLeft:I
    :goto_ac
    move-object/from16 v0, p0

    #@ae
    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@b1
    move-result v1

    #@b2
    if-eqz v1, :cond_b9

    #@b4
    .line 1524
    move-object/from16 v0, p0

    #@b6
    iget v1, v0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@b8
    add-int/2addr v10, v1

    #@b9
    .line 1527
    :cond_b9
    iget v1, v15, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@bb
    add-int/2addr v10, v1

    #@bc
    .line 1528
    move-object/from16 v0, p0

    #@be
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getLocationOffset(Landroid/view/View;)I

    #@c1
    move-result v1

    #@c2
    add-int v4, v10, v1

    #@c4
    move-object/from16 v1, p0

    #@c6
    invoke-direct/range {v1 .. v6}, Landroid/widget/LinearLayout;->setChildFrame(Landroid/view/View;IIII)V

    #@c9
    .line 1530
    iget v1, v15, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@cb
    add-int/2addr v1, v6

    #@cc
    move-object/from16 v0, p0

    #@ce
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@d1
    move-result v4

    #@d2
    add-int/2addr v1, v4

    #@d3
    add-int/2addr v10, v1

    #@d4
    .line 1532
    move-object/from16 v0, p0

    #@d6
    invoke-virtual {v0, v2, v13}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@d9
    move-result v1

    #@da
    add-int/2addr v13, v1

    #@db
    goto/16 :goto_4a

    #@dd
    .line 1509
    .end local v3           #childLeft:I
    :sswitch_dd
    sub-int v1, v9, v5

    #@df
    div-int/lit8 v1, v1, 0x2

    #@e1
    add-int v1, v1, v18

    #@e3
    iget v4, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@e5
    add-int/2addr v1, v4

    #@e6
    iget v4, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@e8
    sub-int v3, v1, v4

    #@ea
    .line 1511
    .restart local v3       #childLeft:I
    goto :goto_ac

    #@eb
    .line 1514
    .end local v3           #childLeft:I
    :sswitch_eb
    sub-int v1, v8, v5

    #@ed
    iget v4, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@ef
    sub-int v3, v1, v4

    #@f1
    .line 1515
    .restart local v3       #childLeft:I
    goto :goto_ac

    #@f2
    .line 1535
    .end local v2           #child:Landroid/view/View;
    .end local v3           #childLeft:I
    .end local v5           #childWidth:I
    .end local v6           #childHeight:I
    .end local v7           #absoluteGravity:I
    .end local v12           #gravity:I
    .end local v14           #layoutDirection:I
    .end local v15           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_f2
    return-void

    #@f3
    .line 1473
    nop

    #@f4
    :sswitch_data_f4
    .sparse-switch
        0x10 -> :sswitch_62
        0x50 -> :sswitch_4d
    .end sparse-switch

    #@fe
    .line 1507
    :sswitch_data_fe
    .sparse-switch
        0x1 -> :sswitch_dd
        0x5 -> :sswitch_eb
    .end sparse-switch
.end method

.method measureChildBeforeLayout(Landroid/view/View;IIIII)V
    .registers 13
    .parameter "child"
    .parameter "childIndex"
    .parameter "widthMeasureSpec"
    .parameter "totalWidth"
    .parameter "heightMeasureSpec"
    .parameter "totalHeight"

    #@0
    .prologue
    .line 1411
    move-object v0, p0

    #@1
    move-object v1, p1

    #@2
    move v2, p3

    #@3
    move v3, p4

    #@4
    move v4, p5

    #@5
    move v5, p6

    #@6
    invoke-virtual/range {v0 .. v5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@9
    .line 1413
    return-void
.end method

.method measureHorizontal(II)V
    .registers 49
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 966
    const/4 v3, 0x0

    #@1
    move-object/from16 v0, p0

    #@3
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@5
    .line 967
    const/16 v35, 0x0

    #@7
    .line 968
    .local v35, maxHeight:I
    const/16 v18, 0x0

    #@9
    .line 969
    .local v18, childState:I
    const/4 v11, 0x0

    #@a
    .line 970
    .local v11, alternativeMaxHeight:I
    const/16 v42, 0x0

    #@c
    .line 971
    .local v42, weightedMaxHeight:I
    const/4 v10, 0x1

    #@d
    .line 972
    .local v10, allFillParent:Z
    const/16 v39, 0x0

    #@f
    .line 974
    .local v39, totalWeight:F
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@12
    move-result v20

    #@13
    .line 976
    .local v20, count:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@16
    move-result v43

    #@17
    .line 977
    .local v43, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@1a
    move-result v25

    #@1b
    .line 979
    .local v25, heightMode:I
    const/16 v31, 0x0

    #@1d
    .line 981
    .local v31, matchHeight:Z
    move-object/from16 v0, p0

    #@1f
    iget-object v3, v0, Landroid/widget/LinearLayout;->mMaxAscent:[I

    #@21
    if-eqz v3, :cond_29

    #@23
    move-object/from16 v0, p0

    #@25
    iget-object v3, v0, Landroid/widget/LinearLayout;->mMaxDescent:[I

    #@27
    if-nez v3, :cond_37

    #@29
    .line 982
    :cond_29
    const/4 v3, 0x4

    #@2a
    new-array v3, v3, [I

    #@2c
    move-object/from16 v0, p0

    #@2e
    iput-object v3, v0, Landroid/widget/LinearLayout;->mMaxAscent:[I

    #@30
    .line 983
    const/4 v3, 0x4

    #@31
    new-array v3, v3, [I

    #@33
    move-object/from16 v0, p0

    #@35
    iput-object v3, v0, Landroid/widget/LinearLayout;->mMaxDescent:[I

    #@37
    .line 986
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Landroid/widget/LinearLayout;->mMaxAscent:[I

    #@3b
    move-object/from16 v33, v0

    #@3d
    .line 987
    .local v33, maxAscent:[I
    move-object/from16 v0, p0

    #@3f
    iget-object v0, v0, Landroid/widget/LinearLayout;->mMaxDescent:[I

    #@41
    move-object/from16 v34, v0

    #@43
    .line 989
    .local v34, maxDescent:[I
    const/4 v3, 0x0

    #@44
    const/4 v6, 0x1

    #@45
    const/4 v7, 0x2

    #@46
    const/4 v8, 0x3

    #@47
    const/4 v9, -0x1

    #@48
    aput v9, v33, v8

    #@4a
    aput v9, v33, v7

    #@4c
    aput v9, v33, v6

    #@4e
    aput v9, v33, v3

    #@50
    .line 990
    const/4 v3, 0x0

    #@51
    const/4 v6, 0x1

    #@52
    const/4 v7, 0x2

    #@53
    const/4 v8, 0x3

    #@54
    const/4 v9, -0x1

    #@55
    aput v9, v34, v8

    #@57
    aput v9, v34, v7

    #@59
    aput v9, v34, v6

    #@5b
    aput v9, v34, v3

    #@5d
    .line 992
    move-object/from16 v0, p0

    #@5f
    iget-boolean v13, v0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@61
    .line 993
    .local v13, baselineAligned:Z
    move-object/from16 v0, p0

    #@63
    iget-boolean v0, v0, Landroid/widget/LinearLayout;->mUseLargestChild:Z

    #@65
    move/from16 v40, v0

    #@67
    .line 995
    .local v40, useLargestChild:Z
    const/high16 v3, 0x4000

    #@69
    move/from16 v0, v43

    #@6b
    if-ne v0, v3, :cond_90

    #@6d
    const/16 v27, 0x1

    #@6f
    .line 997
    .local v27, isExactly:Z
    :goto_6f
    const/high16 v28, -0x8000

    #@71
    .line 1000
    .local v28, largestChildWidth:I
    const/4 v5, 0x0

    #@72
    .local v5, i:I
    :goto_72
    move/from16 v0, v20

    #@74
    if-ge v5, v0, :cond_245

    #@76
    .line 1001
    move-object/from16 v0, p0

    #@78
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@7b
    move-result-object v4

    #@7c
    .line 1003
    .local v4, child:Landroid/view/View;
    if-nez v4, :cond_93

    #@7e
    .line 1004
    move-object/from16 v0, p0

    #@80
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@82
    move-object/from16 v0, p0

    #@84
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@87
    move-result v6

    #@88
    add-int/2addr v3, v6

    #@89
    move-object/from16 v0, p0

    #@8b
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@8d
    .line 1000
    :goto_8d
    add-int/lit8 v5, v5, 0x1

    #@8f
    goto :goto_72

    #@90
    .line 995
    .end local v4           #child:Landroid/view/View;
    .end local v5           #i:I
    .end local v27           #isExactly:Z
    .end local v28           #largestChildWidth:I
    :cond_90
    const/16 v27, 0x0

    #@92
    goto :goto_6f

    #@93
    .line 1008
    .restart local v4       #child:Landroid/view/View;
    .restart local v5       #i:I
    .restart local v27       #isExactly:Z
    .restart local v28       #largestChildWidth:I
    :cond_93
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@96
    move-result v3

    #@97
    const/16 v6, 0x8

    #@99
    if-ne v3, v6, :cond_a3

    #@9b
    .line 1009
    move-object/from16 v0, p0

    #@9d
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@a0
    move-result v3

    #@a1
    add-int/2addr v5, v3

    #@a2
    .line 1010
    goto :goto_8d

    #@a3
    .line 1013
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@a8
    move-result v3

    #@a9
    if-eqz v3, :cond_b8

    #@ab
    .line 1014
    move-object/from16 v0, p0

    #@ad
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@af
    move-object/from16 v0, p0

    #@b1
    iget v6, v0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@b3
    add-int/2addr v3, v6

    #@b4
    move-object/from16 v0, p0

    #@b6
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@b8
    .line 1017
    :cond_b8
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@bb
    move-result-object v29

    #@bc
    check-cast v29, Landroid/widget/LinearLayout$LayoutParams;

    #@be
    .line 1020
    .local v29, lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v29

    #@c0
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@c2
    add-float v39, v39, v3

    #@c4
    .line 1022
    const/high16 v3, 0x4000

    #@c6
    move/from16 v0, v43

    #@c8
    if-ne v0, v3, :cond_1a1

    #@ca
    move-object/from16 v0, v29

    #@cc
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@ce
    if-nez v3, :cond_1a1

    #@d0
    move-object/from16 v0, v29

    #@d2
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@d4
    const/4 v6, 0x0

    #@d5
    cmpl-float v3, v3, v6

    #@d7
    if-lez v3, :cond_1a1

    #@d9
    .line 1026
    if-eqz v27, :cond_184

    #@db
    .line 1027
    move-object/from16 v0, p0

    #@dd
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@df
    move-object/from16 v0, v29

    #@e1
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@e3
    move-object/from16 v0, v29

    #@e5
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@e7
    add-int/2addr v6, v7

    #@e8
    add-int/2addr v3, v6

    #@e9
    move-object/from16 v0, p0

    #@eb
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@ed
    .line 1039
    :goto_ed
    if-eqz v13, :cond_fc

    #@ef
    .line 1040
    const/4 v3, 0x0

    #@f0
    const/4 v6, 0x0

    #@f1
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@f4
    move-result v23

    #@f5
    .line 1041
    .local v23, freeSpec:I
    move/from16 v0, v23

    #@f7
    move/from16 v1, v23

    #@f9
    invoke-virtual {v4, v0, v1}, Landroid/view/View;->measure(II)V

    #@fc
    .line 1082
    .end local v23           #freeSpec:I
    :cond_fc
    :goto_fc
    const/16 v32, 0x0

    #@fe
    .line 1083
    .local v32, matchHeightLocally:Z
    const/high16 v3, 0x4000

    #@100
    move/from16 v0, v25

    #@102
    if-eq v0, v3, :cond_10f

    #@104
    move-object/from16 v0, v29

    #@106
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@108
    const/4 v6, -0x1

    #@109
    if-ne v3, v6, :cond_10f

    #@10b
    .line 1087
    const/16 v31, 0x1

    #@10d
    .line 1088
    const/16 v32, 0x1

    #@10f
    .line 1091
    :cond_10f
    move-object/from16 v0, v29

    #@111
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@113
    move-object/from16 v0, v29

    #@115
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@117
    add-int v30, v3, v6

    #@119
    .line 1092
    .local v30, margin:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@11c
    move-result v3

    #@11d
    add-int v16, v3, v30

    #@11f
    .line 1093
    .local v16, childHeight:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredState()I

    #@122
    move-result v3

    #@123
    move/from16 v0, v18

    #@125
    invoke-static {v0, v3}, Landroid/widget/LinearLayout;->combineMeasuredStates(II)I

    #@128
    move-result v18

    #@129
    .line 1095
    if-eqz v13, :cond_156

    #@12b
    .line 1096
    invoke-virtual {v4}, Landroid/view/View;->getBaseline()I

    #@12e
    move-result v14

    #@12f
    .line 1097
    .local v14, childBaseline:I
    const/4 v3, -0x1

    #@130
    if-eq v14, v3, :cond_156

    #@132
    .line 1100
    move-object/from16 v0, v29

    #@134
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@136
    if-gez v3, :cond_22b

    #@138
    move-object/from16 v0, p0

    #@13a
    iget v3, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@13c
    :goto_13c
    and-int/lit8 v24, v3, 0x70

    #@13e
    .line 1102
    .local v24, gravity:I
    shr-int/lit8 v3, v24, 0x4

    #@140
    and-int/lit8 v3, v3, -0x2

    #@142
    shr-int/lit8 v26, v3, 0x1

    #@144
    .line 1105
    .local v26, index:I
    aget v3, v33, v26

    #@146
    invoke-static {v3, v14}, Ljava/lang/Math;->max(II)I

    #@149
    move-result v3

    #@14a
    aput v3, v33, v26

    #@14c
    .line 1106
    aget v3, v34, v26

    #@14e
    sub-int v6, v16, v14

    #@150
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@153
    move-result v3

    #@154
    aput v3, v34, v26

    #@156
    .line 1110
    .end local v14           #childBaseline:I
    .end local v24           #gravity:I
    .end local v26           #index:I
    :cond_156
    move/from16 v0, v35

    #@158
    move/from16 v1, v16

    #@15a
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@15d
    move-result v35

    #@15e
    .line 1112
    if-eqz v10, :cond_231

    #@160
    move-object/from16 v0, v29

    #@162
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@164
    const/4 v6, -0x1

    #@165
    if-ne v3, v6, :cond_231

    #@167
    const/4 v10, 0x1

    #@168
    .line 1113
    :goto_168
    move-object/from16 v0, v29

    #@16a
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@16c
    const/4 v6, 0x0

    #@16d
    cmpl-float v3, v3, v6

    #@16f
    if-lez v3, :cond_238

    #@171
    .line 1118
    if-eqz v32, :cond_234

    #@173
    .end local v30           #margin:I
    :goto_173
    move/from16 v0, v42

    #@175
    move/from16 v1, v30

    #@177
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@17a
    move-result v42

    #@17b
    .line 1125
    :goto_17b
    move-object/from16 v0, p0

    #@17d
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@180
    move-result v3

    #@181
    add-int/2addr v5, v3

    #@182
    goto/16 :goto_8d

    #@184
    .line 1029
    .end local v16           #childHeight:I
    .end local v32           #matchHeightLocally:Z
    :cond_184
    move-object/from16 v0, p0

    #@186
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@188
    move/from16 v38, v0

    #@18a
    .line 1030
    .local v38, totalLength:I
    move-object/from16 v0, v29

    #@18c
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@18e
    add-int v3, v3, v38

    #@190
    move-object/from16 v0, v29

    #@192
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@194
    add-int/2addr v3, v6

    #@195
    move/from16 v0, v38

    #@197
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@19a
    move-result v3

    #@19b
    move-object/from16 v0, p0

    #@19d
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@19f
    goto/16 :goto_ed

    #@1a1
    .line 1044
    .end local v38           #totalLength:I
    :cond_1a1
    const/high16 v36, -0x8000

    #@1a3
    .line 1046
    .local v36, oldWidth:I
    move-object/from16 v0, v29

    #@1a5
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1a7
    if-nez v3, :cond_1b9

    #@1a9
    move-object/from16 v0, v29

    #@1ab
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@1ad
    const/4 v6, 0x0

    #@1ae
    cmpl-float v3, v3, v6

    #@1b0
    if-lez v3, :cond_1b9

    #@1b2
    .line 1051
    const/16 v36, 0x0

    #@1b4
    .line 1052
    const/4 v3, -0x2

    #@1b5
    move-object/from16 v0, v29

    #@1b7
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1b9
    .line 1059
    :cond_1b9
    const/4 v3, 0x0

    #@1ba
    cmpl-float v3, v39, v3

    #@1bc
    if-nez v3, :cond_205

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iget v7, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1c2
    :goto_1c2
    const/4 v9, 0x0

    #@1c3
    move-object/from16 v3, p0

    #@1c5
    move/from16 v6, p1

    #@1c7
    move/from16 v8, p2

    #@1c9
    invoke-virtual/range {v3 .. v9}, Landroid/widget/LinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    #@1cc
    .line 1063
    const/high16 v3, -0x8000

    #@1ce
    move/from16 v0, v36

    #@1d0
    if-eq v0, v3, :cond_1d8

    #@1d2
    .line 1064
    move/from16 v0, v36

    #@1d4
    move-object/from16 v1, v29

    #@1d6
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1d8
    .line 1067
    :cond_1d8
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@1db
    move-result v19

    #@1dc
    .line 1068
    .local v19, childWidth:I
    if-eqz v27, :cond_207

    #@1de
    .line 1069
    move-object/from16 v0, p0

    #@1e0
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1e2
    move-object/from16 v0, v29

    #@1e4
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1e6
    add-int v6, v6, v19

    #@1e8
    move-object/from16 v0, v29

    #@1ea
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1ec
    add-int/2addr v6, v7

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@1f2
    move-result v7

    #@1f3
    add-int/2addr v6, v7

    #@1f4
    add-int/2addr v3, v6

    #@1f5
    move-object/from16 v0, p0

    #@1f7
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1f9
    .line 1077
    :goto_1f9
    if-eqz v40, :cond_fc

    #@1fb
    .line 1078
    move/from16 v0, v19

    #@1fd
    move/from16 v1, v28

    #@1ff
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@202
    move-result v28

    #@203
    goto/16 :goto_fc

    #@205
    .line 1059
    .end local v19           #childWidth:I
    :cond_205
    const/4 v7, 0x0

    #@206
    goto :goto_1c2

    #@207
    .line 1072
    .restart local v19       #childWidth:I
    :cond_207
    move-object/from16 v0, p0

    #@209
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@20b
    move/from16 v38, v0

    #@20d
    .line 1073
    .restart local v38       #totalLength:I
    add-int v3, v38, v19

    #@20f
    move-object/from16 v0, v29

    #@211
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@213
    add-int/2addr v3, v6

    #@214
    move-object/from16 v0, v29

    #@216
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@218
    add-int/2addr v3, v6

    #@219
    move-object/from16 v0, p0

    #@21b
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@21e
    move-result v6

    #@21f
    add-int/2addr v3, v6

    #@220
    move/from16 v0, v38

    #@222
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@225
    move-result v3

    #@226
    move-object/from16 v0, p0

    #@228
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@22a
    goto :goto_1f9

    #@22b
    .line 1100
    .end local v19           #childWidth:I
    .end local v36           #oldWidth:I
    .end local v38           #totalLength:I
    .restart local v14       #childBaseline:I
    .restart local v16       #childHeight:I
    .restart local v30       #margin:I
    .restart local v32       #matchHeightLocally:Z
    :cond_22b
    move-object/from16 v0, v29

    #@22d
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@22f
    goto/16 :goto_13c

    #@231
    .line 1112
    .end local v14           #childBaseline:I
    :cond_231
    const/4 v10, 0x0

    #@232
    goto/16 :goto_168

    #@234
    :cond_234
    move/from16 v30, v16

    #@236
    .line 1118
    goto/16 :goto_173

    #@238
    .line 1121
    :cond_238
    if-eqz v32, :cond_242

    #@23a
    .end local v30           #margin:I
    :goto_23a
    move/from16 v0, v30

    #@23c
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@23f
    move-result v11

    #@240
    goto/16 :goto_17b

    #@242
    .restart local v30       #margin:I
    :cond_242
    move/from16 v30, v16

    #@244
    goto :goto_23a

    #@245
    .line 1128
    .end local v4           #child:Landroid/view/View;
    .end local v16           #childHeight:I
    .end local v29           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v30           #margin:I
    .end local v32           #matchHeightLocally:Z
    :cond_245
    move-object/from16 v0, p0

    #@247
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@249
    if-lez v3, :cond_262

    #@24b
    move-object/from16 v0, p0

    #@24d
    move/from16 v1, v20

    #@24f
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@252
    move-result v3

    #@253
    if-eqz v3, :cond_262

    #@255
    .line 1129
    move-object/from16 v0, p0

    #@257
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@259
    move-object/from16 v0, p0

    #@25b
    iget v6, v0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@25d
    add-int/2addr v3, v6

    #@25e
    move-object/from16 v0, p0

    #@260
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@262
    .line 1134
    :cond_262
    const/4 v3, 0x1

    #@263
    aget v3, v33, v3

    #@265
    const/4 v6, -0x1

    #@266
    if-ne v3, v6, :cond_27a

    #@268
    const/4 v3, 0x0

    #@269
    aget v3, v33, v3

    #@26b
    const/4 v6, -0x1

    #@26c
    if-ne v3, v6, :cond_27a

    #@26e
    const/4 v3, 0x2

    #@26f
    aget v3, v33, v3

    #@271
    const/4 v6, -0x1

    #@272
    if-ne v3, v6, :cond_27a

    #@274
    const/4 v3, 0x3

    #@275
    aget v3, v33, v3

    #@277
    const/4 v6, -0x1

    #@278
    if-eq v3, v6, :cond_2b2

    #@27a
    .line 1138
    :cond_27a
    const/4 v3, 0x3

    #@27b
    aget v3, v33, v3

    #@27d
    const/4 v6, 0x0

    #@27e
    aget v6, v33, v6

    #@280
    const/4 v7, 0x1

    #@281
    aget v7, v33, v7

    #@283
    const/4 v8, 0x2

    #@284
    aget v8, v33, v8

    #@286
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@289
    move-result v7

    #@28a
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@28d
    move-result v6

    #@28e
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@291
    move-result v12

    #@292
    .line 1141
    .local v12, ascent:I
    const/4 v3, 0x3

    #@293
    aget v3, v34, v3

    #@295
    const/4 v6, 0x0

    #@296
    aget v6, v34, v6

    #@298
    const/4 v7, 0x1

    #@299
    aget v7, v34, v7

    #@29b
    const/4 v8, 0x2

    #@29c
    aget v8, v34, v8

    #@29e
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@2a1
    move-result v7

    #@2a2
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@2a5
    move-result v6

    #@2a6
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@2a9
    move-result v22

    #@2aa
    .line 1144
    .local v22, descent:I
    add-int v3, v12, v22

    #@2ac
    move/from16 v0, v35

    #@2ae
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@2b1
    move-result v35

    #@2b2
    .line 1147
    .end local v12           #ascent:I
    .end local v22           #descent:I
    :cond_2b2
    if-eqz v40, :cond_338

    #@2b4
    const/high16 v3, -0x8000

    #@2b6
    move/from16 v0, v43

    #@2b8
    if-eq v0, v3, :cond_2bc

    #@2ba
    if-nez v43, :cond_338

    #@2bc
    .line 1149
    :cond_2bc
    const/4 v3, 0x0

    #@2bd
    move-object/from16 v0, p0

    #@2bf
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@2c1
    .line 1151
    const/4 v5, 0x0

    #@2c2
    :goto_2c2
    move/from16 v0, v20

    #@2c4
    if-ge v5, v0, :cond_338

    #@2c6
    .line 1152
    move-object/from16 v0, p0

    #@2c8
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@2cb
    move-result-object v4

    #@2cc
    .line 1154
    .restart local v4       #child:Landroid/view/View;
    if-nez v4, :cond_2e0

    #@2ce
    .line 1155
    move-object/from16 v0, p0

    #@2d0
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@2d7
    move-result v6

    #@2d8
    add-int/2addr v3, v6

    #@2d9
    move-object/from16 v0, p0

    #@2db
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@2dd
    .line 1151
    :goto_2dd
    add-int/lit8 v5, v5, 0x1

    #@2df
    goto :goto_2c2

    #@2e0
    .line 1159
    :cond_2e0
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@2e3
    move-result v3

    #@2e4
    const/16 v6, 0x8

    #@2e6
    if-ne v3, v6, :cond_2f0

    #@2e8
    .line 1160
    move-object/from16 v0, p0

    #@2ea
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@2ed
    move-result v3

    #@2ee
    add-int/2addr v5, v3

    #@2ef
    .line 1161
    goto :goto_2dd

    #@2f0
    .line 1164
    :cond_2f0
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2f3
    move-result-object v29

    #@2f4
    check-cast v29, Landroid/widget/LinearLayout$LayoutParams;

    #@2f6
    .line 1166
    .restart local v29       #lp:Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v27, :cond_314

    #@2f8
    .line 1167
    move-object/from16 v0, p0

    #@2fa
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@2fc
    move-object/from16 v0, v29

    #@2fe
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@300
    add-int v6, v6, v28

    #@302
    move-object/from16 v0, v29

    #@304
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@306
    add-int/2addr v6, v7

    #@307
    move-object/from16 v0, p0

    #@309
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@30c
    move-result v7

    #@30d
    add-int/2addr v6, v7

    #@30e
    add-int/2addr v3, v6

    #@30f
    move-object/from16 v0, p0

    #@311
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@313
    goto :goto_2dd

    #@314
    .line 1170
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@318
    move/from16 v38, v0

    #@31a
    .line 1171
    .restart local v38       #totalLength:I
    add-int v3, v38, v28

    #@31c
    move-object/from16 v0, v29

    #@31e
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@320
    add-int/2addr v3, v6

    #@321
    move-object/from16 v0, v29

    #@323
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@325
    add-int/2addr v3, v6

    #@326
    move-object/from16 v0, p0

    #@328
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@32b
    move-result v6

    #@32c
    add-int/2addr v3, v6

    #@32d
    move/from16 v0, v38

    #@32f
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@332
    move-result v3

    #@333
    move-object/from16 v0, p0

    #@335
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@337
    goto :goto_2dd

    #@338
    .line 1178
    .end local v4           #child:Landroid/view/View;
    .end local v29           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v38           #totalLength:I
    :cond_338
    move-object/from16 v0, p0

    #@33a
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@33c
    move-object/from16 v0, p0

    #@33e
    iget v6, v0, Landroid/view/View;->mPaddingLeft:I

    #@340
    move-object/from16 v0, p0

    #@342
    iget v7, v0, Landroid/view/View;->mPaddingRight:I

    #@344
    add-int/2addr v6, v7

    #@345
    add-int/2addr v3, v6

    #@346
    move-object/from16 v0, p0

    #@348
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@34a
    .line 1180
    move-object/from16 v0, p0

    #@34c
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@34e
    move/from16 v44, v0

    #@350
    .line 1183
    .local v44, widthSize:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getSuggestedMinimumWidth()I

    #@353
    move-result v3

    #@354
    move/from16 v0, v44

    #@356
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@359
    move-result v44

    #@35a
    .line 1186
    const/4 v3, 0x0

    #@35b
    move/from16 v0, v44

    #@35d
    move/from16 v1, p1

    #@35f
    invoke-static {v0, v1, v3}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    #@362
    move-result v45

    #@363
    .line 1187
    .local v45, widthSizeAndState:I
    const v3, 0xffffff

    #@366
    and-int v44, v45, v3

    #@368
    .line 1191
    move-object/from16 v0, p0

    #@36a
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@36c
    sub-int v21, v44, v3

    #@36e
    .line 1192
    .local v21, delta:I
    if-eqz v21, :cond_59f

    #@370
    const/4 v3, 0x0

    #@371
    cmpl-float v3, v39, v3

    #@373
    if-lez v3, :cond_59f

    #@375
    .line 1193
    move-object/from16 v0, p0

    #@377
    iget v3, v0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@379
    const/4 v6, 0x0

    #@37a
    cmpl-float v3, v3, v6

    #@37c
    if-lez v3, :cond_3bd

    #@37e
    move-object/from16 v0, p0

    #@380
    iget v0, v0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@382
    move/from16 v41, v0

    #@384
    .line 1195
    .local v41, weightSum:F
    :goto_384
    const/4 v3, 0x0

    #@385
    const/4 v6, 0x1

    #@386
    const/4 v7, 0x2

    #@387
    const/4 v8, 0x3

    #@388
    const/4 v9, -0x1

    #@389
    aput v9, v33, v8

    #@38b
    aput v9, v33, v7

    #@38d
    aput v9, v33, v6

    #@38f
    aput v9, v33, v3

    #@391
    .line 1196
    const/4 v3, 0x0

    #@392
    const/4 v6, 0x1

    #@393
    const/4 v7, 0x2

    #@394
    const/4 v8, 0x3

    #@395
    const/4 v9, -0x1

    #@396
    aput v9, v34, v8

    #@398
    aput v9, v34, v7

    #@39a
    aput v9, v34, v6

    #@39c
    aput v9, v34, v3

    #@39e
    .line 1197
    const/16 v35, -0x1

    #@3a0
    .line 1199
    const/4 v3, 0x0

    #@3a1
    move-object/from16 v0, p0

    #@3a3
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@3a5
    .line 1201
    const/4 v5, 0x0

    #@3a6
    :goto_3a6
    move/from16 v0, v20

    #@3a8
    if-ge v5, v0, :cond_4fd

    #@3aa
    .line 1202
    move-object/from16 v0, p0

    #@3ac
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@3af
    move-result-object v4

    #@3b0
    .line 1204
    .restart local v4       #child:Landroid/view/View;
    if-eqz v4, :cond_3ba

    #@3b2
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@3b5
    move-result v3

    #@3b6
    const/16 v6, 0x8

    #@3b8
    if-ne v3, v6, :cond_3c0

    #@3ba
    .line 1201
    :cond_3ba
    :goto_3ba
    add-int/lit8 v5, v5, 0x1

    #@3bc
    goto :goto_3a6

    #@3bd
    .end local v4           #child:Landroid/view/View;
    .end local v41           #weightSum:F
    :cond_3bd
    move/from16 v41, v39

    #@3bf
    .line 1193
    goto :goto_384

    #@3c0
    .line 1208
    .restart local v4       #child:Landroid/view/View;
    .restart local v41       #weightSum:F
    :cond_3c0
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3c3
    move-result-object v29

    #@3c4
    check-cast v29, Landroid/widget/LinearLayout$LayoutParams;

    #@3c6
    .line 1211
    .restart local v29       #lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v29

    #@3c8
    iget v15, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@3ca
    .line 1212
    .local v15, childExtra:F
    const/4 v3, 0x0

    #@3cb
    cmpl-float v3, v15, v3

    #@3cd
    if-lez v3, :cond_429

    #@3cf
    .line 1214
    move/from16 v0, v21

    #@3d1
    int-to-float v3, v0

    #@3d2
    mul-float/2addr v3, v15

    #@3d3
    div-float v3, v3, v41

    #@3d5
    float-to-int v0, v3

    #@3d6
    move/from16 v37, v0

    #@3d8
    .line 1215
    .local v37, share:I
    sub-float v41, v41, v15

    #@3da
    .line 1216
    sub-int v21, v21, v37

    #@3dc
    .line 1218
    move-object/from16 v0, p0

    #@3de
    iget v3, v0, Landroid/view/View;->mPaddingTop:I

    #@3e0
    move-object/from16 v0, p0

    #@3e2
    iget v6, v0, Landroid/view/View;->mPaddingBottom:I

    #@3e4
    add-int/2addr v3, v6

    #@3e5
    move-object/from16 v0, v29

    #@3e7
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@3e9
    add-int/2addr v3, v6

    #@3ea
    move-object/from16 v0, v29

    #@3ec
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@3ee
    add-int/2addr v3, v6

    #@3ef
    move-object/from16 v0, v29

    #@3f1
    iget v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3f3
    move/from16 v0, p2

    #@3f5
    invoke-static {v0, v3, v6}, Landroid/widget/LinearLayout;->getChildMeasureSpec(III)I

    #@3f8
    move-result v17

    #@3f9
    .line 1225
    .local v17, childHeightMeasureSpec:I
    move-object/from16 v0, v29

    #@3fb
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3fd
    if-nez v3, :cond_405

    #@3ff
    const/high16 v3, 0x4000

    #@401
    move/from16 v0, v43

    #@403
    if-eq v0, v3, :cond_4b1

    #@405
    .line 1228
    :cond_405
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@408
    move-result v3

    #@409
    add-int v19, v3, v37

    #@40b
    .line 1229
    .restart local v19       #childWidth:I
    if-gez v19, :cond_40f

    #@40d
    .line 1230
    const/16 v19, 0x0

    #@40f
    .line 1233
    :cond_40f
    const/high16 v3, 0x4000

    #@411
    move/from16 v0, v19

    #@413
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@416
    move-result v3

    #@417
    move/from16 v0, v17

    #@419
    invoke-virtual {v4, v3, v0}, Landroid/view/View;->measure(II)V

    #@41c
    .line 1244
    .end local v19           #childWidth:I
    .end local v37           #share:I
    :goto_41c
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredState()I

    #@41f
    move-result v3

    #@420
    const/high16 v6, -0x100

    #@422
    and-int/2addr v3, v6

    #@423
    move/from16 v0, v18

    #@425
    invoke-static {v0, v3}, Landroid/widget/LinearLayout;->combineMeasuredStates(II)I

    #@428
    move-result v18

    #@429
    .line 1248
    .end local v17           #childHeightMeasureSpec:I
    :cond_429
    if-eqz v27, :cond_4c5

    #@42b
    .line 1249
    move-object/from16 v0, p0

    #@42d
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@42f
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@432
    move-result v6

    #@433
    move-object/from16 v0, v29

    #@435
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@437
    add-int/2addr v6, v7

    #@438
    move-object/from16 v0, v29

    #@43a
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@43c
    add-int/2addr v6, v7

    #@43d
    move-object/from16 v0, p0

    #@43f
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@442
    move-result v7

    #@443
    add-int/2addr v6, v7

    #@444
    add-int/2addr v3, v6

    #@445
    move-object/from16 v0, p0

    #@447
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@449
    .line 1257
    :goto_449
    const/high16 v3, 0x4000

    #@44b
    move/from16 v0, v25

    #@44d
    if-eq v0, v3, :cond_4ee

    #@44f
    move-object/from16 v0, v29

    #@451
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@453
    const/4 v6, -0x1

    #@454
    if-ne v3, v6, :cond_4ee

    #@456
    const/16 v32, 0x1

    #@458
    .line 1260
    .restart local v32       #matchHeightLocally:Z
    :goto_458
    move-object/from16 v0, v29

    #@45a
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@45c
    move-object/from16 v0, v29

    #@45e
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@460
    add-int v30, v3, v6

    #@462
    .line 1261
    .restart local v30       #margin:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@465
    move-result v3

    #@466
    add-int v16, v3, v30

    #@468
    .line 1262
    .restart local v16       #childHeight:I
    move/from16 v0, v35

    #@46a
    move/from16 v1, v16

    #@46c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@46f
    move-result v35

    #@470
    .line 1263
    if-eqz v32, :cond_4f2

    #@472
    .end local v30           #margin:I
    :goto_472
    move/from16 v0, v30

    #@474
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@477
    move-result v11

    #@478
    .line 1266
    if-eqz v10, :cond_4f6

    #@47a
    move-object/from16 v0, v29

    #@47c
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@47e
    const/4 v6, -0x1

    #@47f
    if-ne v3, v6, :cond_4f6

    #@481
    const/4 v10, 0x1

    #@482
    .line 1268
    :goto_482
    if-eqz v13, :cond_3ba

    #@484
    .line 1269
    invoke-virtual {v4}, Landroid/view/View;->getBaseline()I

    #@487
    move-result v14

    #@488
    .line 1270
    .restart local v14       #childBaseline:I
    const/4 v3, -0x1

    #@489
    if-eq v14, v3, :cond_3ba

    #@48b
    .line 1272
    move-object/from16 v0, v29

    #@48d
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@48f
    if-gez v3, :cond_4f8

    #@491
    move-object/from16 v0, p0

    #@493
    iget v3, v0, Landroid/widget/LinearLayout;->mGravity:I

    #@495
    :goto_495
    and-int/lit8 v24, v3, 0x70

    #@497
    .line 1274
    .restart local v24       #gravity:I
    shr-int/lit8 v3, v24, 0x4

    #@499
    and-int/lit8 v3, v3, -0x2

    #@49b
    shr-int/lit8 v26, v3, 0x1

    #@49d
    .line 1277
    .restart local v26       #index:I
    aget v3, v33, v26

    #@49f
    invoke-static {v3, v14}, Ljava/lang/Math;->max(II)I

    #@4a2
    move-result v3

    #@4a3
    aput v3, v33, v26

    #@4a5
    .line 1278
    aget v3, v34, v26

    #@4a7
    sub-int v6, v16, v14

    #@4a9
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@4ac
    move-result v3

    #@4ad
    aput v3, v34, v26

    #@4af
    goto/16 :goto_3ba

    #@4b1
    .line 1238
    .end local v14           #childBaseline:I
    .end local v16           #childHeight:I
    .end local v24           #gravity:I
    .end local v26           #index:I
    .end local v32           #matchHeightLocally:Z
    .restart local v17       #childHeightMeasureSpec:I
    .restart local v37       #share:I
    :cond_4b1
    if-lez v37, :cond_4c2

    #@4b3
    .end local v37           #share:I
    :goto_4b3
    const/high16 v3, 0x4000

    #@4b5
    move/from16 v0, v37

    #@4b7
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@4ba
    move-result v3

    #@4bb
    move/from16 v0, v17

    #@4bd
    invoke-virtual {v4, v3, v0}, Landroid/view/View;->measure(II)V

    #@4c0
    goto/16 :goto_41c

    #@4c2
    .restart local v37       #share:I
    :cond_4c2
    const/16 v37, 0x0

    #@4c4
    goto :goto_4b3

    #@4c5
    .line 1252
    .end local v17           #childHeightMeasureSpec:I
    .end local v37           #share:I
    :cond_4c5
    move-object/from16 v0, p0

    #@4c7
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@4c9
    move/from16 v38, v0

    #@4cb
    .line 1253
    .restart local v38       #totalLength:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@4ce
    move-result v3

    #@4cf
    add-int v3, v3, v38

    #@4d1
    move-object/from16 v0, v29

    #@4d3
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@4d5
    add-int/2addr v3, v6

    #@4d6
    move-object/from16 v0, v29

    #@4d8
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@4da
    add-int/2addr v3, v6

    #@4db
    move-object/from16 v0, p0

    #@4dd
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@4e0
    move-result v6

    #@4e1
    add-int/2addr v3, v6

    #@4e2
    move/from16 v0, v38

    #@4e4
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@4e7
    move-result v3

    #@4e8
    move-object/from16 v0, p0

    #@4ea
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@4ec
    goto/16 :goto_449

    #@4ee
    .line 1257
    .end local v38           #totalLength:I
    :cond_4ee
    const/16 v32, 0x0

    #@4f0
    goto/16 :goto_458

    #@4f2
    .restart local v16       #childHeight:I
    .restart local v30       #margin:I
    .restart local v32       #matchHeightLocally:Z
    :cond_4f2
    move/from16 v30, v16

    #@4f4
    .line 1263
    goto/16 :goto_472

    #@4f6
    .line 1266
    .end local v30           #margin:I
    :cond_4f6
    const/4 v10, 0x0

    #@4f7
    goto :goto_482

    #@4f8
    .line 1272
    .restart local v14       #childBaseline:I
    :cond_4f8
    move-object/from16 v0, v29

    #@4fa
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@4fc
    goto :goto_495

    #@4fd
    .line 1285
    .end local v4           #child:Landroid/view/View;
    .end local v14           #childBaseline:I
    .end local v15           #childExtra:F
    .end local v16           #childHeight:I
    .end local v29           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v32           #matchHeightLocally:Z
    :cond_4fd
    move-object/from16 v0, p0

    #@4ff
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@501
    move-object/from16 v0, p0

    #@503
    iget v6, v0, Landroid/view/View;->mPaddingLeft:I

    #@505
    move-object/from16 v0, p0

    #@507
    iget v7, v0, Landroid/view/View;->mPaddingRight:I

    #@509
    add-int/2addr v6, v7

    #@50a
    add-int/2addr v3, v6

    #@50b
    move-object/from16 v0, p0

    #@50d
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@50f
    .line 1290
    const/4 v3, 0x1

    #@510
    aget v3, v33, v3

    #@512
    const/4 v6, -0x1

    #@513
    if-ne v3, v6, :cond_527

    #@515
    const/4 v3, 0x0

    #@516
    aget v3, v33, v3

    #@518
    const/4 v6, -0x1

    #@519
    if-ne v3, v6, :cond_527

    #@51b
    const/4 v3, 0x2

    #@51c
    aget v3, v33, v3

    #@51e
    const/4 v6, -0x1

    #@51f
    if-ne v3, v6, :cond_527

    #@521
    const/4 v3, 0x3

    #@522
    aget v3, v33, v3

    #@524
    const/4 v6, -0x1

    #@525
    if-eq v3, v6, :cond_55f

    #@527
    .line 1294
    :cond_527
    const/4 v3, 0x3

    #@528
    aget v3, v33, v3

    #@52a
    const/4 v6, 0x0

    #@52b
    aget v6, v33, v6

    #@52d
    const/4 v7, 0x1

    #@52e
    aget v7, v33, v7

    #@530
    const/4 v8, 0x2

    #@531
    aget v8, v33, v8

    #@533
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@536
    move-result v7

    #@537
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@53a
    move-result v6

    #@53b
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@53e
    move-result v12

    #@53f
    .line 1297
    .restart local v12       #ascent:I
    const/4 v3, 0x3

    #@540
    aget v3, v34, v3

    #@542
    const/4 v6, 0x0

    #@543
    aget v6, v34, v6

    #@545
    const/4 v7, 0x1

    #@546
    aget v7, v34, v7

    #@548
    const/4 v8, 0x2

    #@549
    aget v8, v34, v8

    #@54b
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@54e
    move-result v7

    #@54f
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@552
    move-result v6

    #@553
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@556
    move-result v22

    #@557
    .line 1300
    .restart local v22       #descent:I
    add-int v3, v12, v22

    #@559
    move/from16 v0, v35

    #@55b
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@55e
    move-result v35

    #@55f
    .line 1329
    .end local v12           #ascent:I
    .end local v22           #descent:I
    .end local v41           #weightSum:F
    :cond_55f
    if-nez v10, :cond_569

    #@561
    const/high16 v3, 0x4000

    #@563
    move/from16 v0, v25

    #@565
    if-eq v0, v3, :cond_569

    #@567
    .line 1330
    move/from16 v35, v11

    #@569
    .line 1333
    :cond_569
    move-object/from16 v0, p0

    #@56b
    iget v3, v0, Landroid/view/View;->mPaddingTop:I

    #@56d
    move-object/from16 v0, p0

    #@56f
    iget v6, v0, Landroid/view/View;->mPaddingBottom:I

    #@571
    add-int/2addr v3, v6

    #@572
    add-int v35, v35, v3

    #@574
    .line 1336
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getSuggestedMinimumHeight()I

    #@577
    move-result v3

    #@578
    move/from16 v0, v35

    #@57a
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@57d
    move-result v35

    #@57e
    .line 1338
    const/high16 v3, -0x100

    #@580
    and-int v3, v3, v18

    #@582
    or-int v3, v3, v45

    #@584
    shl-int/lit8 v6, v18, 0x10

    #@586
    move/from16 v0, v35

    #@588
    move/from16 v1, p2

    #@58a
    invoke-static {v0, v1, v6}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    #@58d
    move-result v6

    #@58e
    move-object/from16 v0, p0

    #@590
    invoke-virtual {v0, v3, v6}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    #@593
    .line 1342
    if-eqz v31, :cond_59e

    #@595
    .line 1343
    move-object/from16 v0, p0

    #@597
    move/from16 v1, v20

    #@599
    move/from16 v2, p1

    #@59b
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout;->forceUniformHeight(II)V

    #@59e
    .line 1345
    :cond_59e
    return-void

    #@59f
    .line 1303
    :cond_59f
    move/from16 v0, v42

    #@5a1
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@5a4
    move-result v11

    #@5a5
    .line 1307
    if-eqz v40, :cond_55f

    #@5a7
    const/high16 v3, 0x4000

    #@5a9
    move/from16 v0, v43

    #@5ab
    if-eq v0, v3, :cond_55f

    #@5ad
    .line 1308
    const/4 v5, 0x0

    #@5ae
    :goto_5ae
    move/from16 v0, v20

    #@5b0
    if-ge v5, v0, :cond_55f

    #@5b2
    .line 1309
    move-object/from16 v0, p0

    #@5b4
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@5b7
    move-result-object v4

    #@5b8
    .line 1311
    .restart local v4       #child:Landroid/view/View;
    if-eqz v4, :cond_5c2

    #@5ba
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@5bd
    move-result v3

    #@5be
    const/16 v6, 0x8

    #@5c0
    if-ne v3, v6, :cond_5c5

    #@5c2
    .line 1308
    :cond_5c2
    :goto_5c2
    add-int/lit8 v5, v5, 0x1

    #@5c4
    goto :goto_5ae

    #@5c5
    .line 1315
    :cond_5c5
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5c8
    move-result-object v29

    #@5c9
    check-cast v29, Landroid/widget/LinearLayout$LayoutParams;

    #@5cb
    .line 1318
    .restart local v29       #lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v29

    #@5cd
    iget v15, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@5cf
    .line 1319
    .restart local v15       #childExtra:F
    const/4 v3, 0x0

    #@5d0
    cmpl-float v3, v15, v3

    #@5d2
    if-lez v3, :cond_5c2

    #@5d4
    .line 1320
    const/high16 v3, 0x4000

    #@5d6
    move/from16 v0, v28

    #@5d8
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@5db
    move-result v3

    #@5dc
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@5df
    move-result v6

    #@5e0
    const/high16 v7, 0x4000

    #@5e2
    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@5e5
    move-result v6

    #@5e6
    invoke-virtual {v4, v3, v6}, Landroid/view/View;->measure(II)V

    #@5e9
    goto :goto_5c2
.end method

.method measureNullChild(I)I
    .registers 3
    .parameter "childIndex"

    #@0
    .prologue
    .line 1392
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method measureVertical(II)V
    .registers 40
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 638
    const/4 v3, 0x0

    #@1
    move-object/from16 v0, p0

    #@3
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@5
    .line 639
    const/16 v27, 0x0

    #@7
    .line 640
    .local v27, maxWidth:I
    const/4 v15, 0x0

    #@8
    .line 641
    .local v15, childState:I
    const/4 v11, 0x0

    #@9
    .line 642
    .local v11, alternativeMaxWidth:I
    const/16 v35, 0x0

    #@b
    .line 643
    .local v35, weightedMaxWidth:I
    const/4 v10, 0x1

    #@c
    .line 644
    .local v10, allFillParent:Z
    const/16 v32, 0x0

    #@e
    .line 646
    .local v32, totalWeight:F
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getVirtualChildCount()I

    #@11
    move-result v17

    #@12
    .line 648
    .local v17, count:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@15
    move-result v36

    #@16
    .line 649
    .local v36, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@19
    move-result v19

    #@1a
    .line 651
    .local v19, heightMode:I
    const/16 v25, 0x0

    #@1c
    .line 653
    .local v25, matchWidth:Z
    move-object/from16 v0, p0

    #@1e
    iget v12, v0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@20
    .line 654
    .local v12, baselineChildIndex:I
    move-object/from16 v0, p0

    #@22
    iget-boolean v0, v0, Landroid/widget/LinearLayout;->mUseLargestChild:Z

    #@24
    move/from16 v33, v0

    #@26
    .line 656
    .local v33, useLargestChild:Z
    const/high16 v22, -0x8000

    #@28
    .line 659
    .local v22, largestChildHeight:I
    const/4 v5, 0x0

    #@29
    .local v5, i:I
    :goto_29
    move/from16 v0, v17

    #@2b
    if-ge v5, v0, :cond_199

    #@2d
    .line 660
    move-object/from16 v0, p0

    #@2f
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@32
    move-result-object v4

    #@33
    .line 662
    .local v4, child:Landroid/view/View;
    if-nez v4, :cond_47

    #@35
    .line 663
    move-object/from16 v0, p0

    #@37
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@39
    move-object/from16 v0, p0

    #@3b
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@3e
    move-result v6

    #@3f
    add-int/2addr v3, v6

    #@40
    move-object/from16 v0, p0

    #@42
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@44
    .line 659
    :goto_44
    add-int/lit8 v5, v5, 0x1

    #@46
    goto :goto_29

    #@47
    .line 667
    :cond_47
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@4a
    move-result v3

    #@4b
    const/16 v6, 0x8

    #@4d
    if-ne v3, v6, :cond_57

    #@4f
    .line 668
    move-object/from16 v0, p0

    #@51
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@54
    move-result v3

    #@55
    add-int/2addr v5, v3

    #@56
    .line 669
    goto :goto_44

    #@57
    .line 672
    :cond_57
    move-object/from16 v0, p0

    #@59
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@5c
    move-result v3

    #@5d
    if-eqz v3, :cond_6c

    #@5f
    .line 673
    move-object/from16 v0, p0

    #@61
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@63
    move-object/from16 v0, p0

    #@65
    iget v6, v0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@67
    add-int/2addr v3, v6

    #@68
    move-object/from16 v0, p0

    #@6a
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@6c
    .line 676
    :cond_6c
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@6f
    move-result-object v23

    #@70
    check-cast v23, Landroid/widget/LinearLayout$LayoutParams;

    #@72
    .line 678
    .local v23, lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v23

    #@74
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@76
    add-float v32, v32, v3

    #@78
    .line 680
    const/high16 v3, 0x4000

    #@7a
    move/from16 v0, v19

    #@7c
    if-ne v0, v3, :cond_c9

    #@7e
    move-object/from16 v0, v23

    #@80
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@82
    if-nez v3, :cond_c9

    #@84
    move-object/from16 v0, v23

    #@86
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@88
    const/4 v6, 0x0

    #@89
    cmpl-float v3, v3, v6

    #@8b
    if-lez v3, :cond_c9

    #@8d
    .line 684
    move-object/from16 v0, p0

    #@8f
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@91
    move/from16 v31, v0

    #@93
    .line 685
    .local v31, totalLength:I
    move-object/from16 v0, v23

    #@95
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@97
    add-int v3, v3, v31

    #@99
    move-object/from16 v0, v23

    #@9b
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@9d
    add-int/2addr v3, v6

    #@9e
    move/from16 v0, v31

    #@a0
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@a3
    move-result v3

    #@a4
    move-object/from16 v0, p0

    #@a6
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@a8
    .line 724
    :cond_a8
    :goto_a8
    if-ltz v12, :cond_b6

    #@aa
    add-int/lit8 v3, v5, 0x1

    #@ac
    if-ne v12, v3, :cond_b6

    #@ae
    .line 725
    move-object/from16 v0, p0

    #@b0
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@b2
    move-object/from16 v0, p0

    #@b4
    iput v3, v0, Landroid/widget/LinearLayout;->mBaselineChildTop:I

    #@b6
    .line 731
    :cond_b6
    if-ge v5, v12, :cond_133

    #@b8
    move-object/from16 v0, v23

    #@ba
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@bc
    const/4 v6, 0x0

    #@bd
    cmpl-float v3, v3, v6

    #@bf
    if-lez v3, :cond_133

    #@c1
    .line 732
    new-instance v3, Ljava/lang/RuntimeException;

    #@c3
    const-string v6, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."

    #@c5
    invoke-direct {v3, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c8
    throw v3

    #@c9
    .line 687
    .end local v31           #totalLength:I
    :cond_c9
    const/high16 v29, -0x8000

    #@cb
    .line 689
    .local v29, oldHeight:I
    move-object/from16 v0, v23

    #@cd
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@cf
    if-nez v3, :cond_e1

    #@d1
    move-object/from16 v0, v23

    #@d3
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@d5
    const/4 v6, 0x0

    #@d6
    cmpl-float v3, v3, v6

    #@d8
    if-lez v3, :cond_e1

    #@da
    .line 694
    const/16 v29, 0x0

    #@dc
    .line 695
    const/4 v3, -0x2

    #@dd
    move-object/from16 v0, v23

    #@df
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@e1
    .line 702
    :cond_e1
    const/4 v7, 0x0

    #@e2
    const/4 v3, 0x0

    #@e3
    cmpl-float v3, v32, v3

    #@e5
    if-nez v3, :cond_131

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget v9, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@eb
    :goto_eb
    move-object/from16 v3, p0

    #@ed
    move/from16 v6, p1

    #@ef
    move/from16 v8, p2

    #@f1
    invoke-virtual/range {v3 .. v9}, Landroid/widget/LinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    #@f4
    .line 706
    const/high16 v3, -0x8000

    #@f6
    move/from16 v0, v29

    #@f8
    if-eq v0, v3, :cond_100

    #@fa
    .line 707
    move/from16 v0, v29

    #@fc
    move-object/from16 v1, v23

    #@fe
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@100
    .line 710
    :cond_100
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@103
    move-result v14

    #@104
    .line 711
    .local v14, childHeight:I
    move-object/from16 v0, p0

    #@106
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@108
    move/from16 v31, v0

    #@10a
    .line 712
    .restart local v31       #totalLength:I
    add-int v3, v31, v14

    #@10c
    move-object/from16 v0, v23

    #@10e
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@110
    add-int/2addr v3, v6

    #@111
    move-object/from16 v0, v23

    #@113
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@115
    add-int/2addr v3, v6

    #@116
    move-object/from16 v0, p0

    #@118
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@11b
    move-result v6

    #@11c
    add-int/2addr v3, v6

    #@11d
    move/from16 v0, v31

    #@11f
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@122
    move-result v3

    #@123
    move-object/from16 v0, p0

    #@125
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@127
    .line 715
    if-eqz v33, :cond_a8

    #@129
    .line 716
    move/from16 v0, v22

    #@12b
    invoke-static {v14, v0}, Ljava/lang/Math;->max(II)I

    #@12e
    move-result v22

    #@12f
    goto/16 :goto_a8

    #@131
    .line 702
    .end local v14           #childHeight:I
    .end local v31           #totalLength:I
    :cond_131
    const/4 v9, 0x0

    #@132
    goto :goto_eb

    #@133
    .line 738
    .end local v29           #oldHeight:I
    .restart local v31       #totalLength:I
    :cond_133
    const/16 v26, 0x0

    #@135
    .line 739
    .local v26, matchWidthLocally:Z
    const/high16 v3, 0x4000

    #@137
    move/from16 v0, v36

    #@139
    if-eq v0, v3, :cond_146

    #@13b
    move-object/from16 v0, v23

    #@13d
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@13f
    const/4 v6, -0x1

    #@140
    if-ne v3, v6, :cond_146

    #@142
    .line 744
    const/16 v25, 0x1

    #@144
    .line 745
    const/16 v26, 0x1

    #@146
    .line 748
    :cond_146
    move-object/from16 v0, v23

    #@148
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@14a
    move-object/from16 v0, v23

    #@14c
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@14e
    add-int v24, v3, v6

    #@150
    .line 749
    .local v24, margin:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@153
    move-result v3

    #@154
    add-int v28, v3, v24

    #@156
    .line 750
    .local v28, measuredWidth:I
    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->max(II)I

    #@159
    move-result v27

    #@15a
    .line 751
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredState()I

    #@15d
    move-result v3

    #@15e
    invoke-static {v15, v3}, Landroid/widget/LinearLayout;->combineMeasuredStates(II)I

    #@161
    move-result v15

    #@162
    .line 753
    if-eqz v10, :cond_188

    #@164
    move-object/from16 v0, v23

    #@166
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@168
    const/4 v6, -0x1

    #@169
    if-ne v3, v6, :cond_188

    #@16b
    const/4 v10, 0x1

    #@16c
    .line 754
    :goto_16c
    move-object/from16 v0, v23

    #@16e
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@170
    const/4 v6, 0x0

    #@171
    cmpl-float v3, v3, v6

    #@173
    if-lez v3, :cond_18d

    #@175
    .line 759
    if-eqz v26, :cond_18a

    #@177
    .end local v24           #margin:I
    :goto_177
    move/from16 v0, v35

    #@179
    move/from16 v1, v24

    #@17b
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@17e
    move-result v35

    #@17f
    .line 766
    :goto_17f
    move-object/from16 v0, p0

    #@181
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@184
    move-result v3

    #@185
    add-int/2addr v5, v3

    #@186
    goto/16 :goto_44

    #@188
    .line 753
    .restart local v24       #margin:I
    :cond_188
    const/4 v10, 0x0

    #@189
    goto :goto_16c

    #@18a
    :cond_18a
    move/from16 v24, v28

    #@18c
    .line 759
    goto :goto_177

    #@18d
    .line 762
    :cond_18d
    if-eqz v26, :cond_196

    #@18f
    .end local v24           #margin:I
    :goto_18f
    move/from16 v0, v24

    #@191
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@194
    move-result v11

    #@195
    goto :goto_17f

    #@196
    .restart local v24       #margin:I
    :cond_196
    move/from16 v24, v28

    #@198
    goto :goto_18f

    #@199
    .line 769
    .end local v4           #child:Landroid/view/View;
    .end local v23           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v24           #margin:I
    .end local v26           #matchWidthLocally:Z
    .end local v28           #measuredWidth:I
    .end local v31           #totalLength:I
    :cond_199
    move-object/from16 v0, p0

    #@19b
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@19d
    if-lez v3, :cond_1b6

    #@19f
    move-object/from16 v0, p0

    #@1a1
    move/from16 v1, v17

    #@1a3
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->hasDividerBeforeChildAt(I)Z

    #@1a6
    move-result v3

    #@1a7
    if-eqz v3, :cond_1b6

    #@1a9
    .line 770
    move-object/from16 v0, p0

    #@1ab
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iget v6, v0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@1b1
    add-int/2addr v3, v6

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1b6
    .line 773
    :cond_1b6
    if-eqz v33, :cond_21e

    #@1b8
    const/high16 v3, -0x8000

    #@1ba
    move/from16 v0, v19

    #@1bc
    if-eq v0, v3, :cond_1c0

    #@1be
    if-nez v19, :cond_21e

    #@1c0
    .line 775
    :cond_1c0
    const/4 v3, 0x0

    #@1c1
    move-object/from16 v0, p0

    #@1c3
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1c5
    .line 777
    const/4 v5, 0x0

    #@1c6
    :goto_1c6
    move/from16 v0, v17

    #@1c8
    if-ge v5, v0, :cond_21e

    #@1ca
    .line 778
    move-object/from16 v0, p0

    #@1cc
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@1cf
    move-result-object v4

    #@1d0
    .line 780
    .restart local v4       #child:Landroid/view/View;
    if-nez v4, :cond_1e4

    #@1d2
    .line 781
    move-object/from16 v0, p0

    #@1d4
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->measureNullChild(I)I

    #@1db
    move-result v6

    #@1dc
    add-int/2addr v3, v6

    #@1dd
    move-object/from16 v0, p0

    #@1df
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1e1
    .line 777
    :goto_1e1
    add-int/lit8 v5, v5, 0x1

    #@1e3
    goto :goto_1c6

    #@1e4
    .line 785
    :cond_1e4
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@1e7
    move-result v3

    #@1e8
    const/16 v6, 0x8

    #@1ea
    if-ne v3, v6, :cond_1f4

    #@1ec
    .line 786
    move-object/from16 v0, p0

    #@1ee
    invoke-virtual {v0, v4, v5}, Landroid/widget/LinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    #@1f1
    move-result v3

    #@1f2
    add-int/2addr v5, v3

    #@1f3
    .line 787
    goto :goto_1e1

    #@1f4
    .line 790
    :cond_1f4
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1f7
    move-result-object v23

    #@1f8
    check-cast v23, Landroid/widget/LinearLayout$LayoutParams;

    #@1fa
    .line 793
    .restart local v23       #lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    #@1fc
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@1fe
    move/from16 v31, v0

    #@200
    .line 794
    .restart local v31       #totalLength:I
    add-int v3, v31, v22

    #@202
    move-object/from16 v0, v23

    #@204
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@206
    add-int/2addr v3, v6

    #@207
    move-object/from16 v0, v23

    #@209
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@20b
    add-int/2addr v3, v6

    #@20c
    move-object/from16 v0, p0

    #@20e
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@211
    move-result v6

    #@212
    add-int/2addr v3, v6

    #@213
    move/from16 v0, v31

    #@215
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@218
    move-result v3

    #@219
    move-object/from16 v0, p0

    #@21b
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@21d
    goto :goto_1e1

    #@21e
    .line 800
    .end local v4           #child:Landroid/view/View;
    .end local v23           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v31           #totalLength:I
    :cond_21e
    move-object/from16 v0, p0

    #@220
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@222
    move-object/from16 v0, p0

    #@224
    iget v6, v0, Landroid/view/View;->mPaddingTop:I

    #@226
    move-object/from16 v0, p0

    #@228
    iget v7, v0, Landroid/view/View;->mPaddingBottom:I

    #@22a
    add-int/2addr v6, v7

    #@22b
    add-int/2addr v3, v6

    #@22c
    move-object/from16 v0, p0

    #@22e
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@230
    .line 802
    move-object/from16 v0, p0

    #@232
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@234
    move/from16 v20, v0

    #@236
    .line 805
    .local v20, heightSize:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getSuggestedMinimumHeight()I

    #@239
    move-result v3

    #@23a
    move/from16 v0, v20

    #@23c
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@23f
    move-result v20

    #@240
    .line 808
    const/4 v3, 0x0

    #@241
    move/from16 v0, v20

    #@243
    move/from16 v1, p2

    #@245
    invoke-static {v0, v1, v3}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    #@248
    move-result v21

    #@249
    .line 809
    .local v21, heightSizeAndState:I
    const v3, 0xffffff

    #@24c
    and-int v20, v21, v3

    #@24e
    .line 813
    move-object/from16 v0, p0

    #@250
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@252
    sub-int v18, v20, v3

    #@254
    .line 814
    .local v18, delta:I
    if-eqz v18, :cond_3b0

    #@256
    const/4 v3, 0x0

    #@257
    cmpl-float v3, v32, v3

    #@259
    if-lez v3, :cond_3b0

    #@25b
    .line 815
    move-object/from16 v0, p0

    #@25d
    iget v3, v0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@25f
    const/4 v6, 0x0

    #@260
    cmpl-float v3, v3, v6

    #@262
    if-lez v3, :cond_285

    #@264
    move-object/from16 v0, p0

    #@266
    iget v0, v0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@268
    move/from16 v34, v0

    #@26a
    .line 817
    .local v34, weightSum:F
    :goto_26a
    const/4 v3, 0x0

    #@26b
    move-object/from16 v0, p0

    #@26d
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@26f
    .line 819
    const/4 v5, 0x0

    #@270
    :goto_270
    move/from16 v0, v17

    #@272
    if-ge v5, v0, :cond_364

    #@274
    .line 820
    move-object/from16 v0, p0

    #@276
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@279
    move-result-object v4

    #@27a
    .line 822
    .restart local v4       #child:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@27d
    move-result v3

    #@27e
    const/16 v6, 0x8

    #@280
    if-ne v3, v6, :cond_288

    #@282
    .line 819
    :goto_282
    add-int/lit8 v5, v5, 0x1

    #@284
    goto :goto_270

    #@285
    .end local v4           #child:Landroid/view/View;
    .end local v34           #weightSum:F
    :cond_285
    move/from16 v34, v32

    #@287
    .line 815
    goto :goto_26a

    #@288
    .line 826
    .restart local v4       #child:Landroid/view/View;
    .restart local v34       #weightSum:F
    :cond_288
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@28b
    move-result-object v23

    #@28c
    check-cast v23, Landroid/widget/LinearLayout$LayoutParams;

    #@28e
    .line 828
    .restart local v23       #lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v23

    #@290
    iget v13, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@292
    .line 829
    .local v13, childExtra:F
    const/4 v3, 0x0

    #@293
    cmpl-float v3, v13, v3

    #@295
    if-lez v3, :cond_2eb

    #@297
    .line 831
    move/from16 v0, v18

    #@299
    int-to-float v3, v0

    #@29a
    mul-float/2addr v3, v13

    #@29b
    div-float v3, v3, v34

    #@29d
    float-to-int v0, v3

    #@29e
    move/from16 v30, v0

    #@2a0
    .line 832
    .local v30, share:I
    sub-float v34, v34, v13

    #@2a2
    .line 833
    sub-int v18, v18, v30

    #@2a4
    .line 835
    move-object/from16 v0, p0

    #@2a6
    iget v3, v0, Landroid/view/View;->mPaddingLeft:I

    #@2a8
    move-object/from16 v0, p0

    #@2aa
    iget v6, v0, Landroid/view/View;->mPaddingRight:I

    #@2ac
    add-int/2addr v3, v6

    #@2ad
    move-object/from16 v0, v23

    #@2af
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2b1
    add-int/2addr v3, v6

    #@2b2
    move-object/from16 v0, v23

    #@2b4
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@2b6
    add-int/2addr v3, v6

    #@2b7
    move-object/from16 v0, v23

    #@2b9
    iget v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2bb
    move/from16 v0, p1

    #@2bd
    invoke-static {v0, v3, v6}, Landroid/widget/LinearLayout;->getChildMeasureSpec(III)I

    #@2c0
    move-result v16

    #@2c1
    .line 841
    .local v16, childWidthMeasureSpec:I
    move-object/from16 v0, v23

    #@2c3
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2c5
    if-nez v3, :cond_2cd

    #@2c7
    const/high16 v3, 0x4000

    #@2c9
    move/from16 v0, v19

    #@2cb
    if-eq v0, v3, :cond_349

    #@2cd
    .line 844
    :cond_2cd
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@2d0
    move-result v3

    #@2d1
    add-int v14, v3, v30

    #@2d3
    .line 845
    .restart local v14       #childHeight:I
    if-gez v14, :cond_2d6

    #@2d5
    .line 846
    const/4 v14, 0x0

    #@2d6
    .line 849
    :cond_2d6
    const/high16 v3, 0x4000

    #@2d8
    invoke-static {v14, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2db
    move-result v3

    #@2dc
    move/from16 v0, v16

    #@2de
    invoke-virtual {v4, v0, v3}, Landroid/view/View;->measure(II)V

    #@2e1
    .line 860
    .end local v14           #childHeight:I
    .end local v30           #share:I
    :goto_2e1
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredState()I

    #@2e4
    move-result v3

    #@2e5
    and-int/lit16 v3, v3, -0x100

    #@2e7
    invoke-static {v15, v3}, Landroid/widget/LinearLayout;->combineMeasuredStates(II)I

    #@2ea
    move-result v15

    #@2eb
    .line 864
    .end local v16           #childWidthMeasureSpec:I
    :cond_2eb
    move-object/from16 v0, v23

    #@2ed
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2ef
    move-object/from16 v0, v23

    #@2f1
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@2f3
    add-int v24, v3, v6

    #@2f5
    .line 865
    .restart local v24       #margin:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@2f8
    move-result v3

    #@2f9
    add-int v28, v3, v24

    #@2fb
    .line 866
    .restart local v28       #measuredWidth:I
    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->max(II)I

    #@2fe
    move-result v27

    #@2ff
    .line 868
    const/high16 v3, 0x4000

    #@301
    move/from16 v0, v36

    #@303
    if-eq v0, v3, :cond_35c

    #@305
    move-object/from16 v0, v23

    #@307
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@309
    const/4 v6, -0x1

    #@30a
    if-ne v3, v6, :cond_35c

    #@30c
    const/16 v26, 0x1

    #@30e
    .line 871
    .restart local v26       #matchWidthLocally:Z
    :goto_30e
    if-eqz v26, :cond_35f

    #@310
    .end local v24           #margin:I
    :goto_310
    move/from16 v0, v24

    #@312
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@315
    move-result v11

    #@316
    .line 874
    if-eqz v10, :cond_362

    #@318
    move-object/from16 v0, v23

    #@31a
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@31c
    const/4 v6, -0x1

    #@31d
    if-ne v3, v6, :cond_362

    #@31f
    const/4 v10, 0x1

    #@320
    .line 876
    :goto_320
    move-object/from16 v0, p0

    #@322
    iget v0, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@324
    move/from16 v31, v0

    #@326
    .line 877
    .restart local v31       #totalLength:I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@329
    move-result v3

    #@32a
    add-int v3, v3, v31

    #@32c
    move-object/from16 v0, v23

    #@32e
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@330
    add-int/2addr v3, v6

    #@331
    move-object/from16 v0, v23

    #@333
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@335
    add-int/2addr v3, v6

    #@336
    move-object/from16 v0, p0

    #@338
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    #@33b
    move-result v6

    #@33c
    add-int/2addr v3, v6

    #@33d
    move/from16 v0, v31

    #@33f
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@342
    move-result v3

    #@343
    move-object/from16 v0, p0

    #@345
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@347
    goto/16 :goto_282

    #@349
    .line 854
    .end local v26           #matchWidthLocally:Z
    .end local v28           #measuredWidth:I
    .end local v31           #totalLength:I
    .restart local v16       #childWidthMeasureSpec:I
    .restart local v30       #share:I
    :cond_349
    if-lez v30, :cond_359

    #@34b
    .end local v30           #share:I
    :goto_34b
    const/high16 v3, 0x4000

    #@34d
    move/from16 v0, v30

    #@34f
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@352
    move-result v3

    #@353
    move/from16 v0, v16

    #@355
    invoke-virtual {v4, v0, v3}, Landroid/view/View;->measure(II)V

    #@358
    goto :goto_2e1

    #@359
    .restart local v30       #share:I
    :cond_359
    const/16 v30, 0x0

    #@35b
    goto :goto_34b

    #@35c
    .line 868
    .end local v16           #childWidthMeasureSpec:I
    .end local v30           #share:I
    .restart local v24       #margin:I
    .restart local v28       #measuredWidth:I
    :cond_35c
    const/16 v26, 0x0

    #@35e
    goto :goto_30e

    #@35f
    .restart local v26       #matchWidthLocally:Z
    :cond_35f
    move/from16 v24, v28

    #@361
    .line 871
    goto :goto_310

    #@362
    .line 874
    .end local v24           #margin:I
    :cond_362
    const/4 v10, 0x0

    #@363
    goto :goto_320

    #@364
    .line 882
    .end local v4           #child:Landroid/view/View;
    .end local v13           #childExtra:F
    .end local v23           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v26           #matchWidthLocally:Z
    .end local v28           #measuredWidth:I
    :cond_364
    move-object/from16 v0, p0

    #@366
    iget v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@368
    move-object/from16 v0, p0

    #@36a
    iget v6, v0, Landroid/view/View;->mPaddingTop:I

    #@36c
    move-object/from16 v0, p0

    #@36e
    iget v7, v0, Landroid/view/View;->mPaddingBottom:I

    #@370
    add-int/2addr v6, v7

    #@371
    add-int/2addr v3, v6

    #@372
    move-object/from16 v0, p0

    #@374
    iput v3, v0, Landroid/widget/LinearLayout;->mTotalLength:I

    #@376
    .line 914
    .end local v34           #weightSum:F
    :cond_376
    if-nez v10, :cond_380

    #@378
    const/high16 v3, 0x4000

    #@37a
    move/from16 v0, v36

    #@37c
    if-eq v0, v3, :cond_380

    #@37e
    .line 915
    move/from16 v27, v11

    #@380
    .line 918
    :cond_380
    move-object/from16 v0, p0

    #@382
    iget v3, v0, Landroid/view/View;->mPaddingLeft:I

    #@384
    move-object/from16 v0, p0

    #@386
    iget v6, v0, Landroid/view/View;->mPaddingRight:I

    #@388
    add-int/2addr v3, v6

    #@389
    add-int v27, v27, v3

    #@38b
    .line 921
    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getSuggestedMinimumWidth()I

    #@38e
    move-result v3

    #@38f
    move/from16 v0, v27

    #@391
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    #@394
    move-result v27

    #@395
    .line 923
    move/from16 v0, v27

    #@397
    move/from16 v1, p1

    #@399
    invoke-static {v0, v1, v15}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    #@39c
    move-result v3

    #@39d
    move-object/from16 v0, p0

    #@39f
    move/from16 v1, v21

    #@3a1
    invoke-virtual {v0, v3, v1}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    #@3a4
    .line 926
    if-eqz v25, :cond_3af

    #@3a6
    .line 927
    move-object/from16 v0, p0

    #@3a8
    move/from16 v1, v17

    #@3aa
    move/from16 v2, p2

    #@3ac
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout;->forceUniformWidth(II)V

    #@3af
    .line 929
    :cond_3af
    return-void

    #@3b0
    .line 885
    :cond_3b0
    move/from16 v0, v35

    #@3b2
    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    #@3b5
    move-result v11

    #@3b6
    .line 891
    if-eqz v33, :cond_376

    #@3b8
    const/high16 v3, 0x4000

    #@3ba
    move/from16 v0, v19

    #@3bc
    if-eq v0, v3, :cond_376

    #@3be
    .line 892
    const/4 v5, 0x0

    #@3bf
    :goto_3bf
    move/from16 v0, v17

    #@3c1
    if-ge v5, v0, :cond_376

    #@3c3
    .line 893
    move-object/from16 v0, p0

    #@3c5
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    #@3c8
    move-result-object v4

    #@3c9
    .line 895
    .restart local v4       #child:Landroid/view/View;
    if-eqz v4, :cond_3d3

    #@3cb
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@3ce
    move-result v3

    #@3cf
    const/16 v6, 0x8

    #@3d1
    if-ne v3, v6, :cond_3d6

    #@3d3
    .line 892
    :cond_3d3
    :goto_3d3
    add-int/lit8 v5, v5, 0x1

    #@3d5
    goto :goto_3bf

    #@3d6
    .line 899
    :cond_3d6
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3d9
    move-result-object v23

    #@3da
    check-cast v23, Landroid/widget/LinearLayout$LayoutParams;

    #@3dc
    .line 902
    .restart local v23       #lp:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v23

    #@3de
    iget v13, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@3e0
    .line 903
    .restart local v13       #childExtra:F
    const/4 v3, 0x0

    #@3e1
    cmpl-float v3, v13, v3

    #@3e3
    if-lez v3, :cond_3d3

    #@3e5
    .line 904
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@3e8
    move-result v3

    #@3e9
    const/high16 v6, 0x4000

    #@3eb
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3ee
    move-result v3

    #@3ef
    const/high16 v6, 0x4000

    #@3f1
    move/from16 v0, v22

    #@3f3
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3f6
    move-result v6

    #@3f7
    invoke-virtual {v4, v3, v6}, Landroid/view/View;->measure(II)V

    #@3fa
    goto :goto_3d3
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 324
    :goto_4
    return-void

    #@5
    .line 319
    :cond_5
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_e

    #@a
    .line 320
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->drawDividersVertical(Landroid/graphics/Canvas;)V

    #@d
    goto :goto_4

    #@e
    .line 322
    :cond_e
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->drawDividersHorizontal(Landroid/graphics/Canvas;)V

    #@11
    goto :goto_4
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1782
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1783
    const-class v0, Landroid/widget/LinearLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1784
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1788
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1789
    const-class v0, Landroid/widget/LinearLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1790
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1440
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_9

    #@5
    .line 1441
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->layoutVertical()V

    #@8
    .line 1445
    :goto_8
    return-void

    #@9
    .line 1443
    :cond_9
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->layoutHorizontal()V

    #@c
    goto :goto_8
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 594
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_9

    #@5
    .line 595
    invoke-virtual {p0, p1, p2}, Landroid/widget/LinearLayout;->measureVertical(II)V

    #@8
    .line 599
    :goto_8
    return-void

    #@9
    .line 597
    :cond_9
    invoke-virtual {p0, p1, p2}, Landroid/widget/LinearLayout;->measureHorizontal(II)V

    #@c
    goto :goto_8
.end method

.method public setBaselineAligned(Z)V
    .registers 2
    .parameter "baselineAligned"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 427
    iput-boolean p1, p0, Landroid/widget/LinearLayout;->mBaselineAligned:Z

    #@2
    .line 428
    return-void
.end method

.method public setBaselineAlignedChildIndex(I)V
    .registers 5
    .parameter "i"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 531
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_2b

    #@8
    .line 532
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "base aligned child index out of range (0, "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ")"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 535
    :cond_2b
    iput p1, p0, Landroid/widget/LinearLayout;->mBaselineAlignedChildIndex:I

    #@2d
    .line 536
    return-void
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "divider"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 263
    iget-object v2, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@4
    if-ne p1, v2, :cond_7

    #@6
    .line 278
    :goto_6
    return-void

    #@7
    .line 266
    :cond_7
    iput-object p1, p0, Landroid/widget/LinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    #@9
    .line 267
    if-eqz p1, :cond_2c

    #@b
    .line 268
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@e
    move-result v2

    #@f
    iput v2, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@11
    .line 269
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@14
    move-result v2

    #@15
    iput v2, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@17
    .line 270
    iget v2, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@19
    if-gez v2, :cond_1d

    #@1b
    iput v0, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@1d
    .line 271
    :cond_1d
    iget v2, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@1f
    if-gez v2, :cond_23

    #@21
    iput v0, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@23
    .line 276
    :cond_23
    :goto_23
    if-nez p1, :cond_31

    #@25
    :goto_25
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setWillNotDraw(Z)V

    #@28
    .line 277
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@2b
    goto :goto_6

    #@2c
    .line 273
    :cond_2c
    iput v1, p0, Landroid/widget/LinearLayout;->mDividerWidth:I

    #@2e
    .line 274
    iput v1, p0, Landroid/widget/LinearLayout;->mDividerHeight:I

    #@30
    goto :goto_23

    #@31
    :cond_31
    move v0, v1

    #@32
    .line 276
    goto :goto_25
.end method

.method public setDividerPadding(I)V
    .registers 2
    .parameter "padding"

    #@0
    .prologue
    .line 290
    iput p1, p0, Landroid/widget/LinearLayout;->mDividerPadding:I

    #@2
    .line 291
    return-void
.end method

.method public setGravity(I)V
    .registers 4
    .parameter "gravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 1708
    iget v0, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@2
    if-eq v0, p1, :cond_2a

    #@4
    .line 1709
    const v0, 0x800007

    #@7
    and-int/2addr v0, p1

    #@8
    if-nez v0, :cond_e

    #@a
    .line 1710
    const v0, 0x800003

    #@d
    or-int/2addr p1, v0

    #@e
    .line 1713
    :cond_e
    and-int/lit8 v0, p1, 0x70

    #@10
    if-nez v0, :cond_14

    #@12
    .line 1714
    or-int/lit8 p1, p1, 0x30

    #@14
    .line 1716
    :cond_14
    sget-boolean v0, Landroid/widget/LinearLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@16
    if-eqz v0, :cond_25

    #@18
    .line 1717
    and-int/lit8 v0, p1, 0x3

    #@1a
    const/4 v1, 0x3

    #@1b
    if-eq v0, v1, :cond_22

    #@1d
    and-int/lit8 v0, p1, 0x5

    #@1f
    const/4 v1, 0x5

    #@20
    if-ne v0, v1, :cond_25

    #@22
    .line 1718
    :cond_22
    const/high16 v0, 0x80

    #@24
    or-int/2addr p1, v0

    #@25
    .line 1722
    :cond_25
    iput p1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@27
    .line 1723
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@2a
    .line 1725
    :cond_2a
    return-void
.end method

.method public setHorizontalGravity(I)V
    .registers 5
    .parameter "horizontalGravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const v2, 0x800007

    #@3
    .line 1729
    and-int v0, p1, v2

    #@5
    .line 1730
    .local v0, gravity:I
    iget v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@7
    and-int/2addr v1, v2

    #@8
    if-eq v1, v0, :cond_16

    #@a
    .line 1731
    iget v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@c
    const v2, -0x800008

    #@f
    and-int/2addr v1, v2

    #@10
    or-int/2addr v1, v0

    #@11
    iput v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@13
    .line 1732
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@16
    .line 1734
    :cond_16
    return-void
.end method

.method public setMeasureWithLargestChildEnabled(Z)V
    .registers 2
    .parameter "enabled"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 458
    iput-boolean p1, p0, Landroid/widget/LinearLayout;->mUseLargestChild:Z

    #@2
    .line 459
    return-void
.end method

.method public setOrientation(I)V
    .registers 3
    .parameter "orientation"

    #@0
    .prologue
    .line 1681
    iget v0, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1682
    iput p1, p0, Landroid/widget/LinearLayout;->mOrientation:I

    #@6
    .line 1683
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@9
    .line 1685
    :cond_9
    return-void
.end method

.method public setShowDividers(I)V
    .registers 3
    .parameter "showDividers"

    #@0
    .prologue
    .line 223
    iget v0, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@2
    if-eq p1, v0, :cond_7

    #@4
    .line 224
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@7
    .line 226
    :cond_7
    iput p1, p0, Landroid/widget/LinearLayout;->mShowDividers:I

    #@9
    .line 227
    return-void
.end method

.method public setVerticalGravity(I)V
    .registers 4
    .parameter "verticalGravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 1738
    and-int/lit8 v0, p1, 0x70

    #@2
    .line 1739
    .local v0, gravity:I
    iget v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@4
    and-int/lit8 v1, v1, 0x70

    #@6
    if-eq v1, v0, :cond_12

    #@8
    .line 1740
    iget v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@a
    and-int/lit8 v1, v1, -0x71

    #@c
    or-int/2addr v1, v0

    #@d
    iput v1, p0, Landroid/widget/LinearLayout;->mGravity:I

    #@f
    .line 1741
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    #@12
    .line 1743
    :cond_12
    return-void
.end method

.method public setWeightSum(F)V
    .registers 3
    .parameter "weightSum"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 589
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    #@4
    move-result v0

    #@5
    iput v0, p0, Landroid/widget/LinearLayout;->mWeightSum:F

    #@7
    .line 590
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 231
    const/4 v0, 0x0

    #@1
    return v0
.end method
