.class Landroid/widget/RemoteViews$ViewPaddingAction;
.super Landroid/widget/RemoteViews$Action;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewPaddingAction"
.end annotation


# static fields
.field public static final TAG:I = 0xe


# instance fields
.field bottom:I

.field left:I

.field right:I

.field final synthetic this$0:Landroid/widget/RemoteViews;

.field top:I


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViews;IIIII)V
    .registers 8
    .parameter
    .parameter "viewId"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1362
    iput-object p1, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 1363
    iput p2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@8
    .line 1364
    iput p3, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->left:I

    #@a
    .line 1365
    iput p4, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->top:I

    #@c
    .line 1366
    iput p5, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->right:I

    #@e
    .line 1367
    iput p6, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->bottom:I

    #@10
    .line 1368
    return-void
.end method

.method public constructor <init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V
    .registers 4
    .parameter
    .parameter "parcel"

    #@0
    .prologue
    .line 1370
    iput-object p1, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->this$0:Landroid/widget/RemoteViews;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews$Action;-><init>(Landroid/widget/RemoteViews$1;)V

    #@6
    .line 1371
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@c
    .line 1372
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->left:I

    #@12
    .line 1373
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->top:I

    #@18
    .line 1374
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->right:I

    #@1e
    .line 1375
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->bottom:I

    #@24
    .line 1376
    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 10
    .parameter "root"
    .parameter "rootParent"
    .parameter "handler"

    #@0
    .prologue
    .line 1389
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 1390
    .local v0, context:Landroid/content/Context;
    iget v2, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@6
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    .line 1391
    .local v1, target:Landroid/view/View;
    if-nez v1, :cond_d

    #@c
    .line 1393
    :goto_c
    return-void

    #@d
    .line 1392
    :cond_d
    iget v2, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->left:I

    #@f
    iget v3, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->top:I

    #@11
    iget v4, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->right:I

    #@13
    iget v5, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->bottom:I

    #@15
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    #@18
    goto :goto_c
.end method

.method public getActionName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1396
    const-string v0, "ViewPaddingAction"

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 1379
    const/16 v0, 0xe

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 1380
    iget v0, p0, Landroid/widget/RemoteViews$Action;->viewId:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 1381
    iget v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->left:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 1382
    iget v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->top:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1383
    iget v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->right:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1384
    iget v0, p0, Landroid/widget/RemoteViews$ViewPaddingAction;->bottom:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 1385
    return-void
.end method
