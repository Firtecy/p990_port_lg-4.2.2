.class Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;
.super Ljava/lang/Object;
.source "RemoteViewsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViewsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FixedSizeRemoteViewsCache"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FixedSizeRemoteViewsCache"

.field private static final sMaxCountSlackPercent:F = 0.75f

.field private static final sMaxMemoryLimitInBytes:I = 0x200000


# instance fields
.field private mIndexMetaData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexRemoteViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/RemoteViews;",
            ">;"
        }
    .end annotation
.end field

.field private mLastRequestedIndex:I

.field private mLoadIndices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxCount:I

.field private mMaxCountSlack:I

.field private final mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

.field private mPreloadLowerBound:I

.field private mPreloadUpperBound:I

.field private mRequestedIndices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mTemporaryMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;


# direct methods
.method public constructor <init>(I)V
    .registers 5
    .parameter "maxCacheSize"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 597
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 598
    iput p1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCount:I

    #@6
    .line 599
    const/high16 v0, 0x3f40

    #@8
    iget v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCount:I

    #@a
    div-int/lit8 v1, v1, 0x2

    #@c
    int-to-float v1, v1

    #@d
    mul-float/2addr v0, v1

    #@e
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCountSlack:I

    #@14
    .line 600
    const/4 v0, 0x0

    #@15
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@17
    .line 601
    iput v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@19
    .line 602
    new-instance v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@1b
    invoke-direct {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@20
    .line 603
    new-instance v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@22
    invoke-direct {v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;-><init>()V

    #@25
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mTemporaryMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@27
    .line 604
    new-instance v0, Ljava/util/HashMap;

    #@29
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2c
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@2e
    .line 605
    new-instance v0, Ljava/util/HashMap;

    #@30
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@33
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@35
    .line 606
    new-instance v0, Ljava/util/HashSet;

    #@37
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@3a
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@3c
    .line 607
    iput v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLastRequestedIndex:I

    #@3e
    .line 608
    new-instance v0, Ljava/util/HashSet;

    #@40
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@43
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@45
    .line 609
    return-void
.end method

.method static synthetic access$1200(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 549
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 549
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method private getFarthestPositionFrom(ILjava/util/ArrayList;)I
    .registers 11
    .parameter "pos"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 680
    .local p2, visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    #@1
    .line 681
    .local v3, maxDist:I
    const/4 v4, -0x1

    #@2
    .line 682
    .local v4, maxDistIndex:I
    const/4 v6, 0x0

    #@3
    .line 683
    .local v6, maxDistNotVisible:I
    const/4 v5, -0x1

    #@4
    .line 684
    .local v5, maxDistIndexNotVisible:I
    iget-object v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@6
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@9
    move-result-object v7

    #@a
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v7

    #@12
    if-eqz v7, :cond_37

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v7

    #@18
    check-cast v7, Ljava/lang/Integer;

    #@1a
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@1d
    move-result v1

    #@1e
    .line 685
    .local v1, i:I
    sub-int v7, v1, p1

    #@20
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@23
    move-result v0

    #@24
    .line 686
    .local v0, dist:I
    if-le v0, v6, :cond_32

    #@26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2d
    move-result v7

    #@2e
    if-nez v7, :cond_32

    #@30
    .line 689
    move v5, v1

    #@31
    .line 690
    move v6, v0

    #@32
    .line 692
    :cond_32
    if-lt v0, v3, :cond_e

    #@34
    .line 695
    move v4, v1

    #@35
    .line 696
    move v3, v0

    #@36
    goto :goto_e

    #@37
    .line 699
    .end local v0           #dist:I
    .end local v1           #i:I
    :cond_37
    const/4 v7, -0x1

    #@38
    if-le v5, v7, :cond_3b

    #@3a
    .line 702
    .end local v5           #maxDistIndexNotVisible:I
    :goto_3a
    return v5

    #@3b
    .restart local v5       #maxDistIndexNotVisible:I
    :cond_3b
    move v5, v4

    #@3c
    goto :goto_3a
.end method

.method private getRemoteViewsBitmapMemoryUsage()I
    .registers 6

    #@0
    .prologue
    .line 668
    const/4 v2, 0x0

    #@1
    .line 669
    .local v2, mem:I
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@6
    move-result-object v4

    #@7
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_27

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/lang/Integer;

    #@17
    .line 670
    .local v0, i:Ljava/lang/Integer;
    iget-object v4, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@19
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/widget/RemoteViews;

    #@1f
    .line 671
    .local v3, v:Landroid/widget/RemoteViews;
    if-eqz v3, :cond_b

    #@21
    .line 672
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->estimateMemoryUsage()I

    #@24
    move-result v4

    #@25
    add-int/2addr v2, v4

    #@26
    goto :goto_b

    #@27
    .line 675
    .end local v0           #i:Ljava/lang/Integer;
    .end local v3           #v:Landroid/widget/RemoteViews;
    :cond_27
    return v2
.end method


# virtual methods
.method public commitTemporaryMetaData()V
    .registers 5

    #@0
    .prologue
    .line 659
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mTemporaryMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@2
    monitor-enter v1

    #@3
    .line 660
    :try_start_3
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@5
    monitor-enter v2
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_13

    #@6
    .line 661
    :try_start_6
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@8
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mTemporaryMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@a
    invoke-virtual {v0, v3}, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->set(Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;)V

    #@d
    .line 662
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_6 .. :try_end_e} :catchall_10

    #@e
    .line 663
    :try_start_e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_e .. :try_end_f} :catchall_13

    #@f
    .line 664
    return-void

    #@10
    .line 662
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    :try_start_12
    throw v0

    #@13
    .line 663
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public containsMetaDataAt(I)Z
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 774
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public containsRemoteViewAt(I)Z
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 771
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .registers 2

    #@0
    .prologue
    .line 640
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@2
    return-object v0
.end method

.method public getMetaDataAt(I)Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 652
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_19

    #@c
    .line 653
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@18
    .line 655
    :goto_18
    return-object v0

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method

.method public getNextIndexToLoad()[I
    .registers 6

    #@0
    .prologue
    .line 750
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@2
    monitor-enter v2

    #@3
    .line 752
    :try_start_3
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@5
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_31

    #@b
    .line 753
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@d
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/lang/Integer;

    #@17
    .line 754
    .local v0, i:Ljava/lang/Integer;
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@19
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@1c
    .line 755
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@1e
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@21
    .line 756
    const/4 v1, 0x2

    #@22
    new-array v1, v1, [I

    #@24
    const/4 v3, 0x0

    #@25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v4

    #@29
    aput v4, v1, v3

    #@2b
    const/4 v3, 0x1

    #@2c
    const/4 v4, 0x1

    #@2d
    aput v4, v1, v3

    #@2f
    monitor-exit v2

    #@30
    .line 766
    .end local v0           #i:Ljava/lang/Integer;
    :goto_30
    return-object v1

    #@31
    .line 760
    :cond_31
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@33
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    #@36
    move-result v1

    #@37
    if-nez v1, :cond_5d

    #@39
    .line 761
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@3b
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v1

    #@3f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@42
    move-result-object v0

    #@43
    check-cast v0, Ljava/lang/Integer;

    #@45
    .line 762
    .restart local v0       #i:Ljava/lang/Integer;
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@47
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@4a
    .line 763
    const/4 v1, 0x2

    #@4b
    new-array v1, v1, [I

    #@4d
    const/4 v3, 0x0

    #@4e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@51
    move-result v4

    #@52
    aput v4, v1, v3

    #@54
    const/4 v3, 0x1

    #@55
    const/4 v4, 0x0

    #@56
    aput v4, v1, v3

    #@58
    monitor-exit v2

    #@59
    goto :goto_30

    #@5a
    .line 767
    .end local v0           #i:Ljava/lang/Integer;
    :catchall_5a
    move-exception v1

    #@5b
    monitor-exit v2
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_5a

    #@5c
    throw v1

    #@5d
    .line 766
    :cond_5d
    const/4 v1, 0x2

    #@5e
    :try_start_5e
    new-array v1, v1, [I

    #@60
    fill-array-data v1, :array_66

    #@63
    monitor-exit v2
    :try_end_64
    .catchall {:try_start_5e .. :try_end_64} :catchall_5a

    #@64
    goto :goto_30

    #@65
    nop

    #@66
    :array_66
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public getRemoteViewsAt(I)Landroid/widget/RemoteViews;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 646
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_19

    #@c
    .line 647
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/widget/RemoteViews;

    #@18
    .line 649
    :goto_18
    return-object v0

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method

.method public getTemporaryMetaData()Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;
    .registers 2

    #@0
    .prologue
    .line 643
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mTemporaryMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@2
    return-object v0
.end method

.method public insert(ILandroid/widget/RemoteViews;JLjava/util/ArrayList;)V
    .registers 11
    .parameter "position"
    .parameter "v"
    .parameter "itemId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/widget/RemoteViews;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 614
    .local p5, visibleWindow:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@5
    move-result v2

    #@6
    iget v3, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCount:I

    #@8
    if-lt v2, v3, :cond_17

    #@a
    .line 615
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@c
    invoke-direct {p0, p1, p5}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getFarthestPositionFrom(ILjava/util/ArrayList;)I

    #@f
    move-result v3

    #@10
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 619
    :cond_17
    iget v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLastRequestedIndex:I

    #@19
    const/4 v3, -0x1

    #@1a
    if-le v2, v3, :cond_34

    #@1c
    iget v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLastRequestedIndex:I

    #@1e
    .line 620
    .local v1, pruneFromPosition:I
    :goto_1e
    invoke-direct {p0}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getRemoteViewsBitmapMemoryUsage()I

    #@21
    move-result v2

    #@22
    const/high16 v3, 0x20

    #@24
    if-lt v2, v3, :cond_36

    #@26
    .line 626
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@28
    invoke-direct {p0, v1, p5}, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->getFarthestPositionFrom(ILjava/util/ArrayList;)I

    #@2b
    move-result v3

    #@2c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    goto :goto_1e

    #@34
    .end local v1           #pruneFromPosition:I
    :cond_34
    move v1, p1

    #@35
    .line 619
    goto :goto_1e

    #@36
    .line 630
    .restart local v1       #pruneFromPosition:I
    :cond_36
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@38
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_5b

    #@42
    .line 631
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@4e
    .line 632
    .local v0, metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    invoke-virtual {v0, p2, p3, p4}, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;->set(Landroid/widget/RemoteViews;J)V

    #@51
    .line 636
    .end local v0           #metaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;
    :goto_51
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 637
    return-void

    #@5b
    .line 634
    :cond_5b
    iget-object v2, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@5d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60
    move-result-object v3

    #@61
    new-instance v4, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;

    #@63
    invoke-direct {v4, p2, p3, p4}, Landroid/widget/RemoteViewsAdapter$RemoteViewsIndexMetaData;-><init>(Landroid/widget/RemoteViews;J)V

    #@66
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    goto :goto_51
.end method

.method public queuePositionsToBePreloadedFromRequestedPosition(I)Z
    .registers 11
    .parameter "position"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 714
    iget v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@3
    if-gt v7, p1, :cond_1b

    #@5
    iget v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@7
    if-gt p1, v7, :cond_1b

    #@9
    .line 715
    iget v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@b
    iget v8, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@d
    add-int/2addr v7, v8

    #@e
    div-int/lit8 v0, v7, 0x2

    #@10
    .line 716
    .local v0, center:I
    sub-int v7, p1, v0

    #@12
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@15
    move-result v7

    #@16
    iget v8, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCountSlack:I

    #@18
    if-ge v7, v8, :cond_1b

    #@1a
    .line 744
    .end local v0           #center:I
    :goto_1a
    return v6

    #@1b
    .line 721
    :cond_1b
    const/4 v1, 0x0

    #@1c
    .line 722
    .local v1, count:I
    iget-object v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@1e
    monitor-enter v7

    #@1f
    .line 723
    :try_start_1f
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMetaData:Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;

    #@21
    iget v1, v6, Landroid/widget/RemoteViewsAdapter$RemoteViewsMetaData;->count:I

    #@23
    .line 724
    monitor-exit v7
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_5d

    #@24
    .line 725
    iget-object v7, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@26
    monitor-enter v7

    #@27
    .line 726
    :try_start_27
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@29
    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    #@2c
    .line 729
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@2e
    iget-object v8, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@30
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@33
    .line 732
    iget v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mMaxCount:I

    #@35
    div-int/lit8 v4, v6, 0x2

    #@37
    .line 733
    .local v4, halfMaxCount:I
    sub-int v6, p1, v4

    #@39
    iput v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@3b
    .line 734
    add-int v6, p1, v4

    #@3d
    iput v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@3f
    .line 735
    const/4 v6, 0x0

    #@40
    iget v8, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@42
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    #@45
    move-result v2

    #@46
    .line 736
    .local v2, effectiveLowerBound:I
    iget v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@48
    add-int/lit8 v8, v1, -0x1

    #@4a
    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    #@4d
    move-result v3

    #@4e
    .line 737
    .local v3, effectiveUpperBound:I
    move v5, v2

    #@4f
    .local v5, i:I
    :goto_4f
    if-gt v5, v3, :cond_60

    #@51
    .line 738
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@53
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_5a
    .catchall {:try_start_27 .. :try_end_5a} :catchall_6e

    #@5a
    .line 737
    add-int/lit8 v5, v5, 0x1

    #@5c
    goto :goto_4f

    #@5d
    .line 724
    .end local v2           #effectiveLowerBound:I
    .end local v3           #effectiveUpperBound:I
    .end local v4           #halfMaxCount:I
    .end local v5           #i:I
    :catchall_5d
    move-exception v6

    #@5e
    :try_start_5e
    monitor-exit v7
    :try_end_5f
    .catchall {:try_start_5e .. :try_end_5f} :catchall_5d

    #@5f
    throw v6

    #@60
    .line 742
    .restart local v2       #effectiveLowerBound:I
    .restart local v3       #effectiveUpperBound:I
    .restart local v4       #halfMaxCount:I
    .restart local v5       #i:I
    :cond_60
    :try_start_60
    iget-object v6, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@62
    iget-object v8, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@64
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v6, v8}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    #@6b
    .line 743
    monitor-exit v7

    #@6c
    .line 744
    const/4 v6, 0x1

    #@6d
    goto :goto_1a

    #@6e
    .line 743
    .end local v2           #effectiveLowerBound:I
    .end local v3           #effectiveUpperBound:I
    .end local v4           #halfMaxCount:I
    .end local v5           #i:I
    :catchall_6e
    move-exception v6

    #@6f
    monitor-exit v7
    :try_end_70
    .catchall {:try_start_60 .. :try_end_70} :catchall_6e

    #@70
    throw v6
.end method

.method public queueRequestedPositionToLoad(I)V
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 706
    iput p1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLastRequestedIndex:I

    #@2
    .line 707
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@4
    monitor-enter v1

    #@5
    .line 708
    :try_start_5
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e
    .line 709
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@17
    .line 710
    monitor-exit v1

    #@18
    .line 711
    return-void

    #@19
    .line 710
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 782
    const/4 v0, 0x0

    #@2
    iput v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadLowerBound:I

    #@4
    .line 783
    iput v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mPreloadUpperBound:I

    #@6
    .line 784
    iput v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLastRequestedIndex:I

    #@8
    .line 785
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexRemoteViews:Ljava/util/HashMap;

    #@a
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@d
    .line 786
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mIndexMetaData:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@12
    .line 787
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@14
    monitor-enter v1

    #@15
    .line 788
    :try_start_15
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mRequestedIndices:Ljava/util/HashSet;

    #@17
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@1a
    .line 789
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$FixedSizeRemoteViewsCache;->mLoadIndices:Ljava/util/HashSet;

    #@1c
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@1f
    .line 790
    monitor-exit v1

    #@20
    .line 791
    return-void

    #@21
    .line 790
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method
