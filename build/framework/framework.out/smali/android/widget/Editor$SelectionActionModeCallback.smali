.class Landroid/widget/Editor$SelectionActionModeCallback;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionActionModeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method private constructor <init>(Landroid/widget/Editor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3193
    iput-object p1, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3193
    invoke-direct {p0, p1}, Landroid/widget/Editor$SelectionActionModeCallback;-><init>(Landroid/widget/Editor;)V

    #@3
    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "mode"
    .parameter "item"

    #@0
    .prologue
    .line 3297
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@2
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@4
    if-eqz v0, :cond_12

    #@6
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@a
    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 3299
    const/4 v0, 0x1

    #@11
    .line 3301
    :goto_11
    return v0

    #@12
    :cond_12
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@14
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@17
    move-result-object v0

    #@18
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    #@1b
    move-result v1

    #@1c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@1f
    move-result v0

    #@20
    goto :goto_11
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 11
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    const/4 v7, 0x6

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 3198
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@5
    if-eqz v5, :cond_37

    #@7
    .line 3199
    iget-object v4, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@9
    iget-object v4, v4, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@b
    if-eqz v4, :cond_17

    #@d
    .line 3200
    iget-object v4, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@f
    iget-object v4, v4, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@11
    invoke-interface {v4, p1, p2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_18

    #@17
    .line 3283
    :cond_17
    :goto_17
    return v3

    #@18
    .line 3205
    :cond_18
    invoke-interface {p2}, Landroid/view/Menu;->hasVisibleItems()Z

    #@1b
    move-result v4

    #@1c
    if-nez v4, :cond_24

    #@1e
    invoke-virtual {p1}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    #@21
    move-result-object v4

    #@22
    if-eqz v4, :cond_28

    #@24
    .line 3206
    :cond_24
    iget-object v4, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@26
    iput-object p1, v4, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@28
    .line 3209
    :cond_28
    iget-object v4, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@2a
    iget-object v4, v4, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@2c
    invoke-interface {v4, p1, p2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    #@2f
    .line 3210
    iget-object v4, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@31
    iget-object v4, v4, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@33
    invoke-interface {v4, p1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    #@36
    goto :goto_17

    #@37
    .line 3216
    :cond_37
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@39
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@40
    move-result-object v5

    #@41
    sget-object v6, Lcom/android/internal/R$styleable;->SelectionModeDrawables:[I

    #@43
    invoke-virtual {v5, v6}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@46
    move-result-object v2

    #@47
    .line 3219
    .local v2, styledAttributes:Landroid/content/res/TypedArray;
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@49
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@54
    move-result-object v5

    #@55
    const v6, 0x111003e

    #@58
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@5b
    move-result v0

    #@5c
    .line 3222
    .local v0, allowText:Z
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@5e
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@65
    move-result-object v5

    #@66
    const v6, 0x10403eb

    #@69
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {p1, v5}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    #@70
    .line 3224
    const/4 v5, 0x0

    #@71
    invoke-virtual {p1, v5}, Landroid/view/ActionMode;->setSubtitle(Ljava/lang/CharSequence;)V

    #@74
    .line 3225
    invoke-virtual {p1, v4}, Landroid/view/ActionMode;->setTitleOptionalHint(Z)V

    #@77
    .line 3232
    const/4 v5, 0x3

    #@78
    invoke-virtual {v2, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@7b
    move-result v1

    #@7c
    .line 3236
    .local v1, selectAllIconId:I
    const v5, 0x102001f

    #@7f
    const v6, 0x104000d

    #@82
    invoke-interface {p2, v3, v5, v3, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    #@85
    move-result-object v5

    #@86
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    #@89
    move-result-object v5

    #@8a
    const/16 v6, 0x61

    #@8c
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    #@8f
    move-result-object v5

    #@90
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    #@93
    .line 3242
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@95
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@98
    move-result-object v5

    #@99
    invoke-virtual {v5}, Landroid/widget/TextView;->canCut()Z

    #@9c
    move-result v5

    #@9d
    if-eqz v5, :cond_ba

    #@9f
    .line 3243
    const v5, 0x1020020

    #@a2
    const v6, 0x1040003

    #@a5
    invoke-interface {p2, v3, v5, v3, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v2, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@ac
    move-result v6

    #@ad
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    #@b0
    move-result-object v5

    #@b1
    const/16 v6, 0x78

    #@b3
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    #@b6
    move-result-object v5

    #@b7
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    #@ba
    .line 3251
    :cond_ba
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@bc
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5}, Landroid/widget/TextView;->canCopy()Z

    #@c3
    move-result v5

    #@c4
    if-eqz v5, :cond_e1

    #@c6
    .line 3252
    const v5, 0x1020021

    #@c9
    const v6, 0x1040001

    #@cc
    invoke-interface {p2, v3, v5, v3, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    #@cf
    move-result-object v5

    #@d0
    invoke-virtual {v2, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@d3
    move-result v6

    #@d4
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    #@d7
    move-result-object v5

    #@d8
    const/16 v6, 0x63

    #@da
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    #@dd
    move-result-object v5

    #@de
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    #@e1
    .line 3260
    :cond_e1
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@e3
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@e6
    move-result-object v5

    #@e7
    invoke-virtual {v5}, Landroid/widget/TextView;->canPaste()Z

    #@ea
    move-result v5

    #@eb
    if-eqz v5, :cond_109

    #@ed
    .line 3261
    const v5, 0x1020022

    #@f0
    const v6, 0x104000b

    #@f3
    invoke-interface {p2, v3, v5, v3, v6}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    #@f6
    move-result-object v5

    #@f7
    const/4 v6, 0x2

    #@f8
    invoke-virtual {v2, v6, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@fb
    move-result v6

    #@fc
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    #@ff
    move-result-object v5

    #@100
    const/16 v6, 0x76

    #@102
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    #@105
    move-result-object v5

    #@106
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    #@109
    .line 3269
    :cond_109
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@10c
    .line 3271
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@10e
    iget-object v5, v5, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@110
    if-eqz v5, :cond_11c

    #@112
    .line 3272
    iget-object v5, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@114
    iget-object v5, v5, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@116
    invoke-interface {v5, p1, p2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    #@119
    move-result v5

    #@11a
    if-eqz v5, :cond_17

    #@11c
    .line 3278
    :cond_11c
    invoke-interface {p2}, Landroid/view/Menu;->hasVisibleItems()Z

    #@11f
    move-result v5

    #@120
    if-nez v5, :cond_128

    #@122
    invoke-virtual {p1}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    #@125
    move-result-object v5

    #@126
    if-eqz v5, :cond_17

    #@128
    .line 3279
    :cond_128
    iget-object v3, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@12a
    invoke-virtual {v3}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@12d
    move-result-object v3

    #@12e
    invoke-virtual {v3}, Landroid/widget/Editor$SelectionModifierCursorController;->show()V

    #@131
    .line 3280
    iget-object v3, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@133
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@136
    move-result-object v3

    #@137
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHasTransientState(Z)V

    #@13a
    move v3, v4

    #@13b
    .line 3281
    goto/16 :goto_17
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3306
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@3
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 3307
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@9
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@b
    invoke-interface {v0, p1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    #@e
    .line 3315
    :cond_e
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@10
    iget-boolean v0, v0, Landroid/widget/Editor;->mPreserveDetachedSelection:Z

    #@12
    if-nez v0, :cond_37

    #@14
    .line 3316
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@16
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/text/Spannable;

    #@20
    iget-object v1, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@22
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@29
    move-result v1

    #@2a
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@2d
    .line 3318
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@2f
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@32
    move-result-object v0

    #@33
    const/4 v1, 0x0

    #@34
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHasTransientState(Z)V

    #@37
    .line 3321
    :cond_37
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@39
    iget-object v0, v0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@3b
    if-eqz v0, :cond_44

    #@3d
    .line 3322
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@3f
    iget-object v0, v0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@41
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionModifierCursorController;->hide()V

    #@44
    .line 3325
    :cond_44
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@46
    iput-object v2, v0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@48
    .line 3326
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@4a
    iput-object v2, v0, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@4c
    .line 3327
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 4
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    .line 3289
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@2
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 3290
    iget-object v0, p0, Landroid/widget/Editor$SelectionActionModeCallback;->this$0:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@a
    invoke-interface {v0, p1, p2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    #@d
    move-result v0

    #@e
    .line 3292
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x1

    #@10
    goto :goto_e
.end method
