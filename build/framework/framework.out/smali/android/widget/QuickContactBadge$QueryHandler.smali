.class Landroid/widget/QuickContactBadge$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "QuickContactBadge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/QuickContactBadge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/QuickContactBadge;


# direct methods
.method public constructor <init>(Landroid/widget/QuickContactBadge;Landroid/content/ContentResolver;)V
    .registers 3
    .parameter
    .parameter "cr"

    #@0
    .prologue
    .line 262
    iput-object p1, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@2
    .line 263
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    #@5
    .line 264
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .registers 15
    .parameter "token"
    .parameter "cookie"
    .parameter "cursor"

    #@0
    .prologue
    .line 268
    const/4 v5, 0x0

    #@1
    .line 269
    .local v5, lookupUri:Landroid/net/Uri;
    const/4 v2, 0x0

    #@2
    .line 270
    .local v2, createUri:Landroid/net/Uri;
    const/4 v6, 0x0

    #@3
    .line 273
    .local v6, trigger:Z
    packed-switch p1, :pswitch_data_88

    #@6
    .line 303
    .end local p2
    :cond_6
    :goto_6
    if-eqz p3, :cond_b

    #@8
    .line 304
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    #@b
    .line 308
    :cond_b
    iget-object v7, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@d
    invoke-static {v7, v5}, Landroid/widget/QuickContactBadge;->access$002(Landroid/widget/QuickContactBadge;Landroid/net/Uri;)Landroid/net/Uri;

    #@10
    .line 309
    iget-object v7, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@12
    invoke-static {v7}, Landroid/widget/QuickContactBadge;->access$100(Landroid/widget/QuickContactBadge;)V

    #@15
    .line 311
    if-eqz v6, :cond_75

    #@17
    if-eqz v5, :cond_75

    #@19
    .line 313
    iget-object v7, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@1b
    invoke-virtual {v7}, Landroid/widget/QuickContactBadge;->getContext()Landroid/content/Context;

    #@1e
    move-result-object v7

    #@1f
    iget-object v8, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@21
    const/4 v9, 0x3

    #@22
    iget-object v10, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@24
    iget-object v10, v10, Landroid/widget/QuickContactBadge;->mExcludeMimes:[Ljava/lang/String;

    #@26
    invoke-static {v7, v8, v5, v9, v10}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    #@29
    .line 320
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 275
    .restart local p2
    :pswitch_2a
    const/4 v6, 0x1

    #@2b
    .line 276
    :try_start_2b
    const-string/jumbo v7, "tel"

    #@2e
    check-cast p2, Ljava/lang/String;

    #@30
    .end local p2
    const/4 v8, 0x0

    #@31
    invoke-static {v7, p2, v8}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@34
    move-result-object v2

    #@35
    .line 280
    :pswitch_35
    if-eqz p3, :cond_6

    #@37
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_6

    #@3d
    .line 281
    const/4 v7, 0x0

    #@3e
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    #@41
    move-result-wide v0

    #@42
    .line 282
    .local v0, contactId:J
    const/4 v7, 0x1

    #@43
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    .line 283
    .local v4, lookupKey:Ljava/lang/String;
    invoke-static {v0, v1, v4}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    #@4a
    move-result-object v5

    #@4b
    .line 284
    goto :goto_6

    #@4c
    .line 289
    .end local v0           #contactId:J
    .end local v4           #lookupKey:Ljava/lang/String;
    .restart local p2
    :pswitch_4c
    const/4 v6, 0x1

    #@4d
    .line 290
    const-string/jumbo v7, "mailto"

    #@50
    check-cast p2, Ljava/lang/String;

    #@52
    .end local p2
    const/4 v8, 0x0

    #@53
    invoke-static {v7, p2, v8}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@56
    move-result-object v2

    #@57
    .line 294
    :pswitch_57
    if-eqz p3, :cond_6

    #@59
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    #@5c
    move-result v7

    #@5d
    if-eqz v7, :cond_6

    #@5f
    .line 295
    const/4 v7, 0x0

    #@60
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    #@63
    move-result-wide v0

    #@64
    .line 296
    .restart local v0       #contactId:J
    const/4 v7, 0x1

    #@65
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    .line 297
    .restart local v4       #lookupKey:Ljava/lang/String;
    invoke-static {v0, v1, v4}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    :try_end_6c
    .catchall {:try_start_2b .. :try_end_6c} :catchall_6e

    #@6c
    move-result-object v5

    #@6d
    goto :goto_6

    #@6e
    .line 303
    .end local v0           #contactId:J
    .end local v4           #lookupKey:Ljava/lang/String;
    :catchall_6e
    move-exception v7

    #@6f
    if-eqz p3, :cond_74

    #@71
    .line 304
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    #@74
    .line 303
    :cond_74
    throw v7

    #@75
    .line 315
    :cond_75
    if-eqz v2, :cond_29

    #@77
    .line 317
    new-instance v3, Landroid/content/Intent;

    #@79
    const-string v7, "com.android.contacts.action.SHOW_OR_CREATE_CONTACT"

    #@7b
    invoke-direct {v3, v7, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@7e
    .line 318
    .local v3, intent:Landroid/content/Intent;
    iget-object v7, p0, Landroid/widget/QuickContactBadge$QueryHandler;->this$0:Landroid/widget/QuickContactBadge;

    #@80
    invoke-virtual {v7}, Landroid/widget/QuickContactBadge;->getContext()Landroid/content/Context;

    #@83
    move-result-object v7

    #@84
    invoke-virtual {v7, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@87
    goto :goto_29

    #@88
    .line 273
    :pswitch_data_88
    .packed-switch 0x0
        :pswitch_57
        :pswitch_35
        :pswitch_4c
        :pswitch_2a
    .end packed-switch
.end method
