.class Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;
.super Ljava/lang/Object;
.source "RemoteViews.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/RemoteViews$SetOnClickFillInIntent;->apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/RemoteViews$SetOnClickFillInIntent;

.field final synthetic val$handler:Landroid/widget/RemoteViews$OnClickHandler;


# direct methods
.method constructor <init>(Landroid/widget/RemoteViews$SetOnClickFillInIntent;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 350
    iput-object p1, p0, Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;->this$1:Landroid/widget/RemoteViews$SetOnClickFillInIntent;

    #@2
    iput-object p2, p0, Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/high16 v7, 0x3f00

    #@4
    .line 353
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/view/View;

    #@a
    .line 355
    .local v1, parent:Landroid/view/View;
    :goto_a
    if-eqz v1, :cond_1b

    #@c
    instance-of v5, v1, Landroid/widget/AdapterView;

    #@e
    if-nez v5, :cond_1b

    #@10
    instance-of v5, v1, Landroid/appwidget/AppWidgetHostView;

    #@12
    if-nez v5, :cond_1b

    #@14
    .line 356
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@17
    move-result-object v1

    #@18
    .end local v1           #parent:Landroid/view/View;
    check-cast v1, Landroid/view/View;

    #@1a
    .restart local v1       #parent:Landroid/view/View;
    goto :goto_a

    #@1b
    .line 359
    :cond_1b
    instance-of v5, v1, Landroid/appwidget/AppWidgetHostView;

    #@1d
    if-nez v5, :cond_21

    #@1f
    if-nez v1, :cond_29

    #@21
    .line 362
    :cond_21
    const-string v5, "RemoteViews"

    #@23
    const-string v6, "Collection item doesn\'t have AdapterView parent"

    #@25
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 388
    :goto_28
    return-void

    #@29
    .line 367
    :cond_29
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@2c
    move-result-object v5

    #@2d
    instance-of v5, v5, Landroid/app/PendingIntent;

    #@2f
    if-nez v5, :cond_39

    #@31
    .line 368
    const-string v5, "RemoteViews"

    #@33
    const-string v6, "Attempting setOnClickFillInIntent without calling setPendingIntentTemplate on parent."

    #@35
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_28

    #@39
    .line 373
    :cond_39
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Landroid/app/PendingIntent;

    #@3f
    .line 375
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@4a
    move-result-object v5

    #@4b
    iget v0, v5, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@4d
    .line 377
    .local v0, appScale:F
    const/4 v5, 0x2

    #@4e
    new-array v3, v5, [I

    #@50
    .line 378
    .local v3, pos:[I
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    #@53
    .line 380
    new-instance v4, Landroid/graphics/Rect;

    #@55
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@58
    .line 381
    .local v4, rect:Landroid/graphics/Rect;
    aget v5, v3, v6

    #@5a
    int-to-float v5, v5

    #@5b
    mul-float/2addr v5, v0

    #@5c
    add-float/2addr v5, v7

    #@5d
    float-to-int v5, v5

    #@5e
    iput v5, v4, Landroid/graphics/Rect;->left:I

    #@60
    .line 382
    aget v5, v3, v8

    #@62
    int-to-float v5, v5

    #@63
    mul-float/2addr v5, v0

    #@64
    add-float/2addr v5, v7

    #@65
    float-to-int v5, v5

    #@66
    iput v5, v4, Landroid/graphics/Rect;->top:I

    #@68
    .line 383
    aget v5, v3, v6

    #@6a
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@6d
    move-result v6

    #@6e
    add-int/2addr v5, v6

    #@6f
    int-to-float v5, v5

    #@70
    mul-float/2addr v5, v0

    #@71
    add-float/2addr v5, v7

    #@72
    float-to-int v5, v5

    #@73
    iput v5, v4, Landroid/graphics/Rect;->right:I

    #@75
    .line 384
    aget v5, v3, v8

    #@77
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@7a
    move-result v6

    #@7b
    add-int/2addr v5, v6

    #@7c
    int-to-float v5, v5

    #@7d
    mul-float/2addr v5, v0

    #@7e
    add-float/2addr v5, v7

    #@7f
    float-to-int v5, v5

    #@80
    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    #@82
    .line 386
    iget-object v5, p0, Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;->this$1:Landroid/widget/RemoteViews$SetOnClickFillInIntent;

    #@84
    iget-object v5, v5, Landroid/widget/RemoteViews$SetOnClickFillInIntent;->fillInIntent:Landroid/content/Intent;

    #@86
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@89
    .line 387
    iget-object v5, p0, Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@8b
    iget-object v6, p0, Landroid/widget/RemoteViews$SetOnClickFillInIntent$1;->this$1:Landroid/widget/RemoteViews$SetOnClickFillInIntent;

    #@8d
    iget-object v6, v6, Landroid/widget/RemoteViews$SetOnClickFillInIntent;->fillInIntent:Landroid/content/Intent;

    #@8f
    invoke-virtual {v5, p1, v2, v6}, Landroid/widget/RemoteViews$OnClickHandler;->onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    #@92
    goto :goto_28
.end method
