.class public abstract Landroid/widget/AdapterView;
.super Landroid/view/ViewGroup;
.source "AdapterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AdapterView$1;,
        Landroid/widget/AdapterView$SelectionNotifier;,
        Landroid/widget/AdapterView$AdapterDataSetObserver;,
        Landroid/widget/AdapterView$AdapterContextMenuInfo;,
        Landroid/widget/AdapterView$OnItemSelectedListener;,
        Landroid/widget/AdapterView$OnItemLongClickListener;,
        Landroid/widget/AdapterView$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# static fields
.field public static final INVALID_POSITION:I = -0x1

.field public static final INVALID_ROW_ID:J = -0x8000000000000000L

.field public static final ITEM_VIEW_TYPE_HEADER_OR_FOOTER:I = -0x2

.field public static final ITEM_VIEW_TYPE_IGNORE:I = -0x1

.field static final SYNC_FIRST_POSITION:I = 0x1

.field static final SYNC_MAX_DURATION_MILLIS:I = 0x64

.field static final SYNC_SELECTED_POSITION:I


# instance fields
.field mBlockLayoutRequests:Z

.field mDataChanged:Z

.field private mDesiredFocusableInTouchModeState:Z

.field private mDesiredFocusableState:Z

.field private mEmptyView:Landroid/view/View;

.field mFirstPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field mInLayout:Z

.field mItemCount:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field private mLayoutHeight:I

.field mNeedSync:Z

.field mNextSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field mNextSelectedRowId:J

.field mOldItemCount:I

.field mOldSelectedPosition:I

.field mOldSelectedRowId:J

.field mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field mSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field mSelectedRowId:J

.field private mSelectionNotifier:Landroid/widget/AdapterView$SelectionNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<TT;>.SelectionNotifier;"
        }
    .end annotation
.end field

.field mSpecificTop:I

.field mSyncHeight:J

.field mSyncMode:I

.field mSyncPosition:I

.field mSyncRowId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v3, -0x1

    #@1
    const-wide/high16 v1, -0x8000

    #@3
    const/4 v0, 0x0

    #@4
    .line 227
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@7
    .line 66
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@9
    .line 83
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@b
    .line 93
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@d
    .line 125
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@f
    .line 151
    iput v3, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@11
    .line 157
    iput-wide v1, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@13
    .line 162
    iput v3, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@15
    .line 168
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@17
    .line 200
    iput v3, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@19
    .line 205
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@1b
    .line 224
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@1d
    .line 228
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v3, -0x1

    #@1
    const-wide/high16 v1, -0x8000

    #@3
    const/4 v0, 0x0

    #@4
    .line 231
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@7
    .line 66
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@9
    .line 83
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@b
    .line 93
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@d
    .line 125
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@f
    .line 151
    iput v3, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@11
    .line 157
    iput-wide v1, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@13
    .line 162
    iput v3, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@15
    .line 168
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@17
    .line 200
    iput v3, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@19
    .line 205
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@1b
    .line 224
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@1d
    .line 232
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v3, -0x1

    #@1
    const-wide/high16 v1, -0x8000

    #@3
    const/4 v0, 0x0

    #@4
    .line 235
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@7
    .line 66
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@9
    .line 83
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@b
    .line 93
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@d
    .line 125
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@f
    .line 151
    iput v3, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@11
    .line 157
    iput-wide v1, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@13
    .line 162
    iput v3, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@15
    .line 168
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@17
    .line 200
    iput v3, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@19
    .line 205
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@1b
    .line 224
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@1d
    .line 238
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getImportantForAccessibility()I

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_27

    #@23
    .line 239
    const/4 v0, 0x1

    #@24
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->setImportantForAccessibility(I)V

    #@27
    .line 241
    :cond_27
    return-void
.end method

.method static synthetic access$000(Landroid/widget/AdapterView;Landroid/os/Parcelable;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/AdapterView;)Landroid/os/Parcelable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/AdapterView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Landroid/widget/AdapterView;->fireOnSelected()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/AdapterView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Landroid/widget/AdapterView;->performAccessibilityActionsOnSelected()V

    #@3
    return-void
.end method

.method private fireOnSelected()V
    .registers 7

    #@0
    .prologue
    .line 886
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 897
    :goto_4
    return-void

    #@5
    .line 889
    :cond_5
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    #@8
    move-result v3

    #@9
    .line 890
    .local v3, selection:I
    if-ltz v3, :cond_1e

    #@b
    .line 891
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedView()Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    .line 892
    .local v2, v:Landroid/view/View;
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@11
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1, v3}, Landroid/widget/Adapter;->getItemId(I)J

    #@18
    move-result-wide v4

    #@19
    move-object v1, p0

    #@1a
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    #@1d
    goto :goto_4

    #@1e
    .line 895
    .end local v2           #v:Landroid/view/View;
    :cond_1e
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@20
    invoke-interface {v0, p0}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    #@23
    goto :goto_4
.end method

.method private isScrollableForAccessibility()Z
    .registers 6

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v2, 0x0

    #@1
    .line 961
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@4
    move-result-object v0

    #@5
    .line 962
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_1c

    #@7
    .line 963
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@a
    move-result v1

    #@b
    .line 964
    .local v1, itemCount:I
    if-lez v1, :cond_1c

    #@d
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    #@10
    move-result v3

    #@11
    if-gtz v3, :cond_1b

    #@13
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    #@16
    move-result v3

    #@17
    add-int/lit8 v4, v1, -0x1

    #@19
    if-ge v3, v4, :cond_1c

    #@1b
    :cond_1b
    const/4 v2, 0x1

    #@1c
    .line 967
    .end local v1           #itemCount:I
    :cond_1c
    return v2
.end method

.method private performAccessibilityActionsOnSelected()V
    .registers 3

    #@0
    .prologue
    .line 900
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 908
    :cond_c
    :goto_c
    return-void

    #@d
    .line 903
    :cond_d
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    #@10
    move-result v0

    #@11
    .line 904
    .local v0, position:I
    if-ltz v0, :cond_c

    #@13
    .line 906
    const/4 v1, 0x4

    #@14
    invoke-virtual {p0, v1}, Landroid/widget/AdapterView;->sendAccessibilityEvent(I)V

    #@17
    goto :goto_c
.end method

.method private updateEmptyStatus(Z)V
    .registers 8
    .parameter "empty"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/16 v2, 0x8

    #@2
    const/4 v1, 0x0

    #@3
    .line 730
    invoke-virtual {p0}, Landroid/widget/AdapterView;->isInFilterMode()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 731
    const/4 p1, 0x0

    #@a
    .line 734
    :cond_a
    if-eqz p1, :cond_2d

    #@c
    .line 735
    iget-object v0, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@e
    if-eqz v0, :cond_29

    #@10
    .line 736
    iget-object v0, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@15
    .line 737
    invoke-virtual {p0, v2}, Landroid/widget/AdapterView;->setVisibility(I)V

    #@18
    .line 746
    :goto_18
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@1a
    if-eqz v0, :cond_28

    #@1c
    .line 747
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@1e
    iget v3, p0, Landroid/view/View;->mTop:I

    #@20
    iget v4, p0, Landroid/view/View;->mRight:I

    #@22
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@24
    move-object v0, p0

    #@25
    invoke-virtual/range {v0 .. v5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    #@28
    .line 753
    :cond_28
    :goto_28
    return-void

    #@29
    .line 740
    :cond_29
    invoke-virtual {p0, v1}, Landroid/widget/AdapterView;->setVisibility(I)V

    #@2c
    goto :goto_18

    #@2d
    .line 750
    :cond_2d
    iget-object v0, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@2f
    if-eqz v0, :cond_36

    #@31
    iget-object v0, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@33
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@36
    .line 751
    :cond_36
    invoke-virtual {p0, v1}, Landroid/widget/AdapterView;->setVisibility(I)V

    #@39
    goto :goto_28
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 451
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "addView(View) is not supported in AdapterView"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .registers 5
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 464
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "addView(View, int) is not supported in AdapterView"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 492
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 477
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method protected canAnimate()Z
    .registers 2

    #@0
    .prologue
    .line 972
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@8
    if-lez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method checkFocus()V
    .registers 7

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 711
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@5
    move-result-object v0

    #@6
    .line 712
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_e

    #@8
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_3d

    #@e
    :cond_e
    move v1, v4

    #@f
    .line 713
    .local v1, empty:Z
    :goto_f
    if-eqz v1, :cond_17

    #@11
    invoke-virtual {p0}, Landroid/widget/AdapterView;->isInFilterMode()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_3f

    #@17
    :cond_17
    move v2, v4

    #@18
    .line 717
    .local v2, focusable:Z
    :goto_18
    if-eqz v2, :cond_41

    #@1a
    iget-boolean v3, p0, Landroid/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    #@1c
    if-eqz v3, :cond_41

    #@1e
    move v3, v4

    #@1f
    :goto_1f
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    #@22
    .line 718
    if-eqz v2, :cond_43

    #@24
    iget-boolean v3, p0, Landroid/widget/AdapterView;->mDesiredFocusableState:Z

    #@26
    if-eqz v3, :cond_43

    #@28
    move v3, v4

    #@29
    :goto_29
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    #@2c
    .line 719
    iget-object v3, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@2e
    if-eqz v3, :cond_3c

    #@30
    .line 720
    if-eqz v0, :cond_38

    #@32
    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_39

    #@38
    :cond_38
    move v5, v4

    #@39
    :cond_39
    invoke-direct {p0, v5}, Landroid/widget/AdapterView;->updateEmptyStatus(Z)V

    #@3c
    .line 722
    :cond_3c
    return-void

    #@3d
    .end local v1           #empty:Z
    .end local v2           #focusable:Z
    :cond_3d
    move v1, v5

    #@3e
    .line 712
    goto :goto_f

    #@3f
    .restart local v1       #empty:Z
    :cond_3f
    move v2, v5

    #@40
    .line 713
    goto :goto_18

    #@41
    .restart local v2       #focusable:Z
    :cond_41
    move v3, v5

    #@42
    .line 717
    goto :goto_1f

    #@43
    :cond_43
    move v3, v5

    #@44
    .line 718
    goto :goto_29
.end method

.method checkSelectionChanged()V
    .registers 5

    #@0
    .prologue
    .line 1042
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    iget v1, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@4
    if-ne v0, v1, :cond_e

    #@6
    iget-wide v0, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@8
    iget-wide v2, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@a
    cmp-long v0, v0, v2

    #@c
    if-eqz v0, :cond_19

    #@e
    .line 1043
    :cond_e
    invoke-virtual {p0}, Landroid/widget/AdapterView;->selectionChanged()V

    #@11
    .line 1044
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@13
    iput v0, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@15
    .line 1045
    iget-wide v0, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@17
    iput-wide v0, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@19
    .line 1047
    :cond_19
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 912
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 913
    .local v0, selectedView:Landroid/view/View;
    if-eqz v0, :cond_14

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_14

    #@12
    .line 915
    const/4 v1, 0x1

    #@13
    .line 917
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 790
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    #@3
    .line 791
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 782
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    #@3
    .line 783
    return-void
.end method

.method findSyncPosition()I
    .registers 20

    #@0
    .prologue
    .line 1058
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    move-object/from16 v0, p0

    #@2
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@4
    .line 1060
    .local v2, count:I
    if-nez v2, :cond_8

    #@6
    .line 1061
    const/4 v14, -0x1

    #@7
    .line 1133
    :cond_7
    :goto_7
    return v14

    #@8
    .line 1064
    :cond_8
    move-object/from16 v0, p0

    #@a
    iget-wide v8, v0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@c
    .line 1065
    .local v8, idToMatch:J
    move-object/from16 v0, p0

    #@e
    iget v14, v0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@10
    .line 1068
    .local v14, seed:I
    const-wide/high16 v15, -0x8000

    #@12
    cmp-long v15, v8, v15

    #@14
    if-nez v15, :cond_18

    #@16
    .line 1069
    const/4 v14, -0x1

    #@17
    goto :goto_7

    #@18
    .line 1073
    :cond_18
    const/4 v15, 0x0

    #@19
    invoke-static {v15, v14}, Ljava/lang/Math;->max(II)I

    #@1c
    move-result v14

    #@1d
    .line 1074
    add-int/lit8 v15, v2, -0x1

    #@1f
    invoke-static {v15, v14}, Ljava/lang/Math;->min(II)I

    #@22
    move-result v14

    #@23
    .line 1076
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26
    move-result-wide v15

    #@27
    const-wide/16 v17, 0x64

    #@29
    add-long v3, v15, v17

    #@2b
    .line 1081
    .local v3, endTime:J
    move v5, v14

    #@2c
    .line 1084
    .local v5, first:I
    move v10, v14

    #@2d
    .line 1087
    .local v10, last:I
    const/4 v11, 0x0

    #@2e
    .line 1097
    .local v11, next:Z
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@31
    move-result-object v1

    #@32
    .line 1098
    .local v1, adapter:Landroid/widget/Adapter;,"TT;"
    if-nez v1, :cond_40

    #@34
    .line 1099
    const/4 v14, -0x1

    #@35
    goto :goto_7

    #@36
    .line 1117
    .local v6, hitFirst:Z
    .local v7, hitLast:Z
    .local v12, rowId:J
    :cond_36
    if-nez v6, :cond_3c

    #@38
    if-eqz v11, :cond_62

    #@3a
    if-nez v7, :cond_62

    #@3c
    .line 1119
    :cond_3c
    add-int/lit8 v10, v10, 0x1

    #@3e
    .line 1120
    move v14, v10

    #@3f
    .line 1122
    const/4 v11, 0x0

    #@40
    .line 1102
    .end local v6           #hitFirst:Z
    .end local v7           #hitLast:Z
    .end local v12           #rowId:J
    :cond_40
    :goto_40
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@43
    move-result-wide v15

    #@44
    cmp-long v15, v15, v3

    #@46
    if-gtz v15, :cond_5c

    #@48
    .line 1103
    invoke-interface {v1, v14}, Landroid/widget/Adapter;->getItemId(I)J

    #@4b
    move-result-wide v12

    #@4c
    .line 1104
    .restart local v12       #rowId:J
    cmp-long v15, v12, v8

    #@4e
    if-eqz v15, :cond_7

    #@50
    .line 1109
    add-int/lit8 v15, v2, -0x1

    #@52
    if-ne v10, v15, :cond_5e

    #@54
    const/4 v7, 0x1

    #@55
    .line 1110
    .restart local v7       #hitLast:Z
    :goto_55
    if-nez v5, :cond_60

    #@57
    const/4 v6, 0x1

    #@58
    .line 1112
    .restart local v6       #hitFirst:Z
    :goto_58
    if-eqz v7, :cond_36

    #@5a
    if-eqz v6, :cond_36

    #@5c
    .line 1133
    .end local v6           #hitFirst:Z
    .end local v7           #hitLast:Z
    .end local v12           #rowId:J
    :cond_5c
    const/4 v14, -0x1

    #@5d
    goto :goto_7

    #@5e
    .line 1109
    .restart local v12       #rowId:J
    :cond_5e
    const/4 v7, 0x0

    #@5f
    goto :goto_55

    #@60
    .line 1110
    .restart local v7       #hitLast:Z
    :cond_60
    const/4 v6, 0x0

    #@61
    goto :goto_58

    #@62
    .line 1123
    .restart local v6       #hitFirst:Z
    :cond_62
    if-nez v7, :cond_68

    #@64
    if-nez v11, :cond_40

    #@66
    if-nez v6, :cond_40

    #@68
    .line 1125
    :cond_68
    add-int/lit8 v5, v5, -0x1

    #@6a
    .line 1126
    move v14, v5

    #@6b
    .line 1128
    const/4 v11, 0x1

    #@6c
    goto :goto_40
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCount()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 581
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 671
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .registers 2

    #@0
    .prologue
    .line 624
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    return v0
.end method

.method public getItemAtPosition(I)Ljava/lang/Object;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 762
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@3
    move-result-object v0

    #@4
    .line 763
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_8

    #@6
    if-gez p1, :cond_a

    #@8
    :cond_8
    const/4 v1, 0x0

    #@9
    :goto_9
    return-object v1

    #@a
    :cond_a
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    goto :goto_9
.end method

.method public getItemIdAtPosition(I)J
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 767
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@3
    move-result-object v0

    #@4
    .line 768
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_8

    #@6
    if-gez p1, :cond_b

    #@8
    :cond_8
    const-wide/high16 v1, -0x8000

    #@a
    :goto_a
    return-wide v1

    #@b
    :cond_b
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    #@e
    move-result-wide v1

    #@f
    goto :goto_a
.end method

.method public getLastVisiblePosition()I
    .registers 3

    #@0
    .prologue
    .line 634
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getChildCount()I

    #@5
    move-result v1

    #@6
    add-int/2addr v0, v1

    #@7
    add-int/lit8 v0, v0, -0x1

    #@9
    return v0
.end method

.method public final getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2

    #@0
    .prologue
    .line 280
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    return-object v0
.end method

.method public final getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .registers 2

    #@0
    .prologue
    .line 346
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@2
    return-object v0
.end method

.method public final getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;
    .registers 2

    #@0
    .prologue
    .line 392
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    return-object v0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .registers 9
    .parameter "view"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v5, -0x1

    #@1
    .line 594
    move-object v3, p1

    #@2
    .line 597
    .local v3, listItem:Landroid/view/View;
    :goto_2
    :try_start_2
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@5
    move-result-object v4

    #@6
    check-cast v4, Landroid/view/View;

    #@8
    .local v4, v:Landroid/view/View;
    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_b} :catch_10

    #@b
    move-result v6

    #@c
    if-nez v6, :cond_12

    #@e
    .line 598
    move-object v3, v4

    #@f
    goto :goto_2

    #@10
    .line 600
    .end local v4           #v:Landroid/view/View;
    :catch_10
    move-exception v1

    #@11
    .line 614
    :cond_11
    :goto_11
    return v5

    #@12
    .line 606
    .restart local v4       #v:Landroid/view/View;
    :cond_12
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getChildCount()I

    #@15
    move-result v0

    #@16
    .line 607
    .local v0, childCount:I
    const/4 v2, 0x0

    #@17
    .local v2, i:I
    :goto_17
    if-ge v2, v0, :cond_11

    #@19
    .line 608
    invoke-virtual {p0, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v6

    #@21
    if-eqz v6, :cond_27

    #@23
    .line 609
    iget v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@25
    add-int/2addr v5, v2

    #@26
    goto :goto_11

    #@27
    .line 607
    :cond_27
    add-int/lit8 v2, v2, 0x1

    #@29
    goto :goto_17
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .registers 4

    #@0
    .prologue
    .line 565
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@3
    move-result-object v0

    #@4
    .line 566
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    #@7
    move-result v1

    #@8
    .line 567
    .local v1, selection:I
    if-eqz v0, :cond_17

    #@a
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@d
    move-result v2

    #@e
    if-lez v2, :cond_17

    #@10
    if-ltz v1, :cond_17

    #@12
    .line 568
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    .line 570
    :goto_16
    return-object v2

    #@17
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16
.end method

.method public getSelectedItemId()J
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 551
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-wide v0, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@2
    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 542
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@2
    return v0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method handleDataChanged()V
    .registers 11

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const-wide/high16 v8, -0x8000

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, -0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 976
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    .line 977
    .local v0, count:I
    const/4 v1, 0x0

    #@8
    .line 979
    .local v1, found:Z
    if-lez v0, :cond_40

    #@a
    .line 984
    iget-boolean v4, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@c
    if-eqz v4, :cond_20

    #@e
    .line 987
    iput-boolean v5, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@10
    .line 991
    invoke-virtual {p0}, Landroid/widget/AdapterView;->findSyncPosition()I

    #@13
    move-result v2

    #@14
    .line 992
    .local v2, newPos:I
    if-ltz v2, :cond_20

    #@16
    .line 994
    invoke-virtual {p0, v2, v7}, Landroid/widget/AdapterView;->lookForSelectablePosition(IZ)I

    #@19
    move-result v3

    #@1a
    .line 995
    .local v3, selectablePos:I
    if-ne v3, v2, :cond_20

    #@1c
    .line 997
    invoke-virtual {p0, v2}, Landroid/widget/AdapterView;->setNextSelectedPositionInt(I)V

    #@1f
    .line 998
    const/4 v1, 0x1

    #@20
    .line 1002
    .end local v2           #newPos:I
    .end local v3           #selectablePos:I
    :cond_20
    if-nez v1, :cond_40

    #@22
    .line 1004
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    #@25
    move-result v2

    #@26
    .line 1007
    .restart local v2       #newPos:I
    if-lt v2, v0, :cond_2a

    #@28
    .line 1008
    add-int/lit8 v2, v0, -0x1

    #@2a
    .line 1010
    :cond_2a
    if-gez v2, :cond_2d

    #@2c
    .line 1011
    const/4 v2, 0x0

    #@2d
    .line 1015
    :cond_2d
    invoke-virtual {p0, v2, v7}, Landroid/widget/AdapterView;->lookForSelectablePosition(IZ)I

    #@30
    move-result v3

    #@31
    .line 1016
    .restart local v3       #selectablePos:I
    if-gez v3, :cond_37

    #@33
    .line 1018
    invoke-virtual {p0, v2, v5}, Landroid/widget/AdapterView;->lookForSelectablePosition(IZ)I

    #@36
    move-result v3

    #@37
    .line 1020
    :cond_37
    if-ltz v3, :cond_40

    #@39
    .line 1021
    invoke-virtual {p0, v3}, Landroid/widget/AdapterView;->setNextSelectedPositionInt(I)V

    #@3c
    .line 1022
    invoke-virtual {p0}, Landroid/widget/AdapterView;->checkSelectionChanged()V

    #@3f
    .line 1023
    const/4 v1, 0x1

    #@40
    .line 1027
    .end local v2           #newPos:I
    .end local v3           #selectablePos:I
    :cond_40
    if-nez v1, :cond_4f

    #@42
    .line 1029
    iput v6, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@44
    .line 1030
    iput-wide v8, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@46
    .line 1031
    iput v6, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@48
    .line 1032
    iput-wide v8, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@4a
    .line 1033
    iput-boolean v5, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@4c
    .line 1034
    invoke-virtual {p0}, Landroid/widget/AdapterView;->checkSelectionChanged()V

    #@4f
    .line 1038
    :cond_4f
    invoke-virtual {p0}, Landroid/widget/AdapterView;->notifyAccessibilityStateChanged()V

    #@52
    .line 1039
    return-void
.end method

.method isInFilterMode()Z
    .registers 2

    #@0
    .prologue
    .line 681
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method lookForSelectablePosition(IZ)I
    .registers 3
    .parameter "position"
    .parameter "lookDown"

    #@0
    .prologue
    .line 1145
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    return p1
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 846
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    #@3
    .line 847
    iget-object v0, p0, Landroid/widget/AdapterView;->mSelectionNotifier:Landroid/widget/AdapterView$SelectionNotifier;

    #@5
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@8
    .line 848
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 947
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 948
    const-class v1, Landroid/widget/AdapterView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 949
    invoke-direct {p0}, Landroid/widget/AdapterView;->isScrollableForAccessibility()Z

    #@f
    move-result v1

    #@10
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@13
    .line 950
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedView()Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    .line 951
    .local v0, selectedView:Landroid/view/View;
    if-eqz v0, :cond_20

    #@19
    .line 952
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    #@1c
    move-result v1

    #@1d
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    #@20
    .line 954
    :cond_20
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    #@23
    move-result v1

    #@24
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    #@27
    .line 955
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    #@2a
    move-result v1

    #@2b
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@2e
    .line 956
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    #@31
    move-result v1

    #@32
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    #@35
    .line 957
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getCount()I

    #@38
    move-result v1

    #@39
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@3c
    .line 958
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 936
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 937
    const-class v1, Landroid/widget/AdapterView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 938
    invoke-direct {p0}, Landroid/widget/AdapterView;->isScrollableForAccessibility()Z

    #@f
    move-result v1

    #@10
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@13
    .line 939
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getSelectedView()Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    .line 940
    .local v0, selectedView:Landroid/view/View;
    if-eqz v0, :cond_20

    #@19
    .line 941
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    #@1c
    move-result v1

    #@1d
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    #@20
    .line 943
    :cond_20
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 7
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 532
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getHeight()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/widget/AdapterView;->mLayoutHeight:I

    #@6
    .line 533
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5
    .parameter "child"
    .parameter "event"

    #@0
    .prologue
    .line 922
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_15

    #@6
    .line 924
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    #@9
    move-result-object v0

    #@a
    .line 925
    .local v0, record:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@d
    .line 927
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@10
    .line 928
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->appendRecord(Landroid/view/accessibility/AccessibilityRecord;)V

    #@13
    .line 929
    const/4 v1, 0x1

    #@14
    .line 931
    .end local v0           #record:Landroid/view/accessibility/AccessibilityEvent;
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .registers 12
    .parameter "view"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v6, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 293
    iget-object v1, p0, Landroid/widget/AdapterView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@4
    if-eqz v1, :cond_18

    #@6
    .line 294
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->playSoundEffect(I)V

    #@9
    .line 295
    if-eqz p1, :cond_e

    #@b
    .line 296
    invoke-virtual {p1, v6}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@e
    .line 298
    :cond_e
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@10
    move-object v1, p0

    #@11
    move-object v2, p1

    #@12
    move v3, p2

    #@13
    move-wide v4, p3

    #@14
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    #@17
    move v0, v6

    #@18
    .line 302
    :cond_18
    return v0
.end method

.method rememberSyncState()V
    .registers 7

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1178
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getChildCount()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_2e

    #@8
    .line 1179
    iput-boolean v5, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@a
    .line 1180
    iget v2, p0, Landroid/widget/AdapterView;->mLayoutHeight:I

    #@c
    int-to-long v2, v2

    #@d
    iput-wide v2, p0, Landroid/widget/AdapterView;->mSyncHeight:J

    #@f
    .line 1181
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@11
    if-ltz v2, :cond_2f

    #@13
    .line 1183
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@15
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@17
    sub-int/2addr v2, v3

    #@18
    invoke-virtual {p0, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v1

    #@1c
    .line 1184
    .local v1, v:Landroid/view/View;
    iget-wide v2, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@1e
    iput-wide v2, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@20
    .line 1185
    iget v2, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@22
    iput v2, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@24
    .line 1186
    if-eqz v1, :cond_2c

    #@26
    .line 1187
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@29
    move-result v2

    #@2a
    iput v2, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@2c
    .line 1189
    :cond_2c
    iput v4, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@2e
    .line 1206
    .end local v1           #v:Landroid/view/View;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 1192
    :cond_2f
    invoke-virtual {p0, v4}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    #@32
    move-result-object v1

    #@33
    .line 1193
    .restart local v1       #v:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@36
    move-result-object v0

    #@37
    .line 1194
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@39
    if-ltz v2, :cond_5a

    #@3b
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3d
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@40
    move-result v3

    #@41
    if-ge v2, v3, :cond_5a

    #@43
    .line 1195
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@45
    invoke-interface {v0, v2}, Landroid/widget/Adapter;->getItemId(I)J

    #@48
    move-result-wide v2

    #@49
    iput-wide v2, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@4b
    .line 1199
    :goto_4b
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4d
    iput v2, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@4f
    .line 1200
    if-eqz v1, :cond_57

    #@51
    .line 1201
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@54
    move-result v2

    #@55
    iput v2, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@57
    .line 1203
    :cond_57
    iput v5, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@59
    goto :goto_2e

    #@5a
    .line 1197
    :cond_5a
    const-wide/16 v2, -0x1

    #@5c
    iput-wide v2, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@5e
    goto :goto_4b
.end method

.method public removeAllViews()V
    .registers 3

    #@0
    .prologue
    .line 527
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string/jumbo v1, "removeAllViews() is not supported in AdapterView"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 505
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string/jumbo v1, "removeView(View) is not supported in AdapterView"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public removeViewAt(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 517
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string/jumbo v1, "removeViewAt(int) is not supported in AdapterView"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method selectionChanged()V
    .registers 3

    #@0
    .prologue
    .line 867
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    if-nez v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@6
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_29

    #@10
    .line 869
    :cond_10
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@12
    if-nez v0, :cond_18

    #@14
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@16
    if-eqz v0, :cond_2a

    #@18
    .line 874
    :cond_18
    iget-object v0, p0, Landroid/widget/AdapterView;->mSelectionNotifier:Landroid/widget/AdapterView$SelectionNotifier;

    #@1a
    if-nez v0, :cond_24

    #@1c
    .line 875
    new-instance v0, Landroid/widget/AdapterView$SelectionNotifier;

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-direct {v0, p0, v1}, Landroid/widget/AdapterView$SelectionNotifier;-><init>(Landroid/widget/AdapterView;Landroid/widget/AdapterView$1;)V

    #@22
    iput-object v0, p0, Landroid/widget/AdapterView;->mSelectionNotifier:Landroid/widget/AdapterView$SelectionNotifier;

    #@24
    .line 877
    :cond_24
    iget-object v0, p0, Landroid/widget/AdapterView;->mSelectionNotifier:Landroid/widget/AdapterView$SelectionNotifier;

    #@26
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->post(Ljava/lang/Runnable;)Z

    #@29
    .line 883
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 879
    :cond_2a
    invoke-direct {p0}, Landroid/widget/AdapterView;->fireOnSelected()V

    #@2d
    .line 880
    invoke-direct {p0}, Landroid/widget/AdapterView;->performAccessibilityActionsOnSelected()V

    #@30
    goto :goto_29
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .registers 5
    .parameter "emptyView"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v1, 0x1

    #@1
    .line 650
    iput-object p1, p0, Landroid/widget/AdapterView;->mEmptyView:Landroid/view/View;

    #@3
    .line 653
    if-eqz p1, :cond_e

    #@5
    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_e

    #@b
    .line 655
    invoke-virtual {p1, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@e
    .line 658
    :cond_e
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@11
    move-result-object v0

    #@12
    .line 659
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_1a

    #@14
    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1e

    #@1a
    .line 660
    .local v1, empty:Z
    :cond_1a
    :goto_1a
    invoke-direct {p0, v1}, Landroid/widget/AdapterView;->updateEmptyStatus(Z)V

    #@1d
    .line 661
    return-void

    #@1e
    .line 659
    .end local v1           #empty:Z
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1a
.end method

.method public setFocusable(Z)V
    .registers 7
    .parameter "focusable"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 686
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@5
    move-result-object v0

    #@6
    .line 687
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_e

    #@8
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@b
    move-result v4

    #@c
    if-nez v4, :cond_23

    #@e
    :cond_e
    move v1, v3

    #@f
    .line 689
    .local v1, empty:Z
    :goto_f
    iput-boolean p1, p0, Landroid/widget/AdapterView;->mDesiredFocusableState:Z

    #@11
    .line 690
    if-nez p1, :cond_15

    #@13
    .line 691
    iput-boolean v2, p0, Landroid/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    #@15
    .line 694
    :cond_15
    if-eqz p1, :cond_25

    #@17
    if-eqz v1, :cond_1f

    #@19
    invoke-virtual {p0}, Landroid/widget/AdapterView;->isInFilterMode()Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_25

    #@1f
    :cond_1f
    :goto_1f
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    #@22
    .line 695
    return-void

    #@23
    .end local v1           #empty:Z
    :cond_23
    move v1, v2

    #@24
    .line 687
    goto :goto_f

    #@25
    .restart local v1       #empty:Z
    :cond_25
    move v3, v2

    #@26
    .line 694
    goto :goto_1f
.end method

.method public setFocusableInTouchMode(Z)V
    .registers 7
    .parameter "focusable"

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 699
    invoke-virtual {p0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@5
    move-result-object v0

    #@6
    .line 700
    .local v0, adapter:Landroid/widget/Adapter;,"TT;"
    if-eqz v0, :cond_e

    #@8
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@b
    move-result v4

    #@c
    if-nez v4, :cond_23

    #@e
    :cond_e
    move v1, v3

    #@f
    .line 702
    .local v1, empty:Z
    :goto_f
    iput-boolean p1, p0, Landroid/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    #@11
    .line 703
    if-eqz p1, :cond_15

    #@13
    .line 704
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mDesiredFocusableState:Z

    #@15
    .line 707
    :cond_15
    if-eqz p1, :cond_25

    #@17
    if-eqz v1, :cond_1f

    #@19
    invoke-virtual {p0}, Landroid/widget/AdapterView;->isInFilterMode()Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_25

    #@1f
    :cond_1f
    :goto_1f
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    #@22
    .line 708
    return-void

    #@23
    .end local v1           #empty:Z
    :cond_23
    move v1, v2

    #@24
    .line 700
    goto :goto_f

    #@25
    .restart local v1       #empty:Z
    :cond_25
    move v3, v2

    #@26
    .line 707
    goto :goto_1f
.end method

.method setNextSelectedPositionInt(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 1163
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iput p1, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@2
    .line 1164
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->getItemIdAtPosition(I)J

    #@5
    move-result-wide v0

    #@6
    iput-wide v0, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@8
    .line 1166
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@a
    if-eqz v0, :cond_18

    #@c
    iget v0, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@e
    if-nez v0, :cond_18

    #@10
    if-ltz p1, :cond_18

    #@12
    .line 1167
    iput p1, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@14
    .line 1168
    iget-wide v0, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@16
    iput-wide v0, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@18
    .line 1170
    :cond_18
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 4
    .parameter "l"

    #@0
    .prologue
    .line 773
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 272
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iput-object p1, p0, Landroid/widget/AdapterView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    .line 273
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 335
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Landroid/widget/AdapterView;->isLongClickable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 336
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->setLongClickable(Z)V

    #@a
    .line 338
    :cond_a
    iput-object p1, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@c
    .line 339
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 388
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iput-object p1, p0, Landroid/widget/AdapterView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    .line 389
    return-void
.end method

.method setSelectedPositionInt(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 1153
    .local p0, this:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<TT;>;"
    iput p1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    .line 1154
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->getItemIdAtPosition(I)J

    #@5
    move-result-wide v0

    #@6
    iput-wide v0, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@8
    .line 1155
    return-void
.end method

.method public abstract setSelection(I)V
.end method
