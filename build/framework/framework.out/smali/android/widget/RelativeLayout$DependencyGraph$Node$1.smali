.class final Landroid/widget/RelativeLayout$DependencyGraph$Node$1;
.super Ljava/lang/Object;
.source "RelativeLayout.java"

# interfaces
.implements Landroid/util/PoolableManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RelativeLayout$DependencyGraph$Node;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/util/PoolableManager",
        "<",
        "Landroid/widget/RelativeLayout$DependencyGraph$Node;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1758
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public bridge synthetic newInstance()Landroid/util/Poolable;
    .registers 2

    #@0
    .prologue
    .line 1758
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$DependencyGraph$Node$1;->newInstance()Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newInstance()Landroid/widget/RelativeLayout$DependencyGraph$Node;
    .registers 2

    #@0
    .prologue
    .line 1760
    new-instance v0, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@2
    invoke-direct {v0}, Landroid/widget/RelativeLayout$DependencyGraph$Node;-><init>()V

    #@5
    return-object v0
.end method

.method public bridge synthetic onAcquired(Landroid/util/Poolable;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1758
    check-cast p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout$DependencyGraph$Node$1;->onAcquired(Landroid/widget/RelativeLayout$DependencyGraph$Node;)V

    #@5
    return-void
.end method

.method public onAcquired(Landroid/widget/RelativeLayout$DependencyGraph$Node;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 1764
    return-void
.end method

.method public bridge synthetic onReleased(Landroid/util/Poolable;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1758
    check-cast p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout$DependencyGraph$Node$1;->onReleased(Landroid/widget/RelativeLayout$DependencyGraph$Node;)V

    #@5
    return-void
.end method

.method public onReleased(Landroid/widget/RelativeLayout$DependencyGraph$Node;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 1767
    return-void
.end method
