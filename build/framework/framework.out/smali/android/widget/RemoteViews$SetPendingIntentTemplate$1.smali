.class Landroid/widget/RemoteViews$SetPendingIntentTemplate$1;
.super Ljava/lang/Object;
.source "RemoteViews.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/RemoteViews$SetPendingIntentTemplate;->apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/RemoteViews$SetPendingIntentTemplate;

.field final synthetic val$handler:Landroid/widget/RemoteViews$OnClickHandler;


# direct methods
.method constructor <init>(Landroid/widget/RemoteViews$SetPendingIntentTemplate;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 430
    iput-object p1, p0, Landroid/widget/RemoteViews$SetPendingIntentTemplate$1;->this$1:Landroid/widget/RemoteViews$SetPendingIntentTemplate;

    #@2
    iput-object p2, p0, Landroid/widget/RemoteViews$SetPendingIntentTemplate$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 17
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 434
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    instance-of v9, p2, Landroid/view/ViewGroup;

    #@2
    if-eqz v9, :cond_14

    #@4
    move-object v8, p2

    #@5
    .line 435
    check-cast v8, Landroid/view/ViewGroup;

    #@7
    .line 439
    .local v8, vg:Landroid/view/ViewGroup;
    instance-of v9, p1, Landroid/widget/AdapterViewAnimator;

    #@9
    if-eqz v9, :cond_12

    #@b
    .line 440
    const/4 v9, 0x0

    #@c
    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v8

    #@10
    .end local v8           #vg:Landroid/view/ViewGroup;
    check-cast v8, Landroid/view/ViewGroup;

    #@12
    .line 442
    .restart local v8       #vg:Landroid/view/ViewGroup;
    :cond_12
    if-nez v8, :cond_15

    #@14
    .line 470
    .end local v8           #vg:Landroid/view/ViewGroup;
    :cond_14
    :goto_14
    return-void

    #@15
    .line 444
    .restart local v8       #vg:Landroid/view/ViewGroup;
    :cond_15
    const/4 v2, 0x0

    #@16
    .line 445
    .local v2, fillInIntent:Landroid/content/Intent;
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    #@19
    move-result v1

    #@1a
    .line 446
    .local v1, childCount:I
    const/4 v3, 0x0

    #@1b
    .local v3, i:I
    :goto_1b
    if-ge v3, v1, :cond_2f

    #@1d
    .line 447
    invoke-virtual {v8, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v9

    #@21
    const v10, 0x1020252

    #@24
    invoke-virtual {v9, v10}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    #@27
    move-result-object v7

    #@28
    .line 448
    .local v7, tag:Ljava/lang/Object;
    instance-of v9, v7, Landroid/content/Intent;

    #@2a
    if-eqz v9, :cond_92

    #@2c
    move-object v2, v7

    #@2d
    .line 449
    check-cast v2, Landroid/content/Intent;

    #@2f
    .line 453
    .end local v7           #tag:Ljava/lang/Object;
    :cond_2f
    if-eqz v2, :cond_14

    #@31
    .line 455
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {v9}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@3c
    move-result-object v9

    #@3d
    iget v0, v9, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@3f
    .line 457
    .local v0, appScale:F
    const/4 v9, 0x2

    #@40
    new-array v5, v9, [I

    #@42
    .line 458
    .local v5, pos:[I
    invoke-virtual {p2, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    #@45
    .line 460
    new-instance v6, Landroid/graphics/Rect;

    #@47
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@4a
    .line 461
    .local v6, rect:Landroid/graphics/Rect;
    const/4 v9, 0x0

    #@4b
    aget v9, v5, v9

    #@4d
    int-to-float v9, v9

    #@4e
    mul-float/2addr v9, v0

    #@4f
    const/high16 v10, 0x3f00

    #@51
    add-float/2addr v9, v10

    #@52
    float-to-int v9, v9

    #@53
    iput v9, v6, Landroid/graphics/Rect;->left:I

    #@55
    .line 462
    const/4 v9, 0x1

    #@56
    aget v9, v5, v9

    #@58
    int-to-float v9, v9

    #@59
    mul-float/2addr v9, v0

    #@5a
    const/high16 v10, 0x3f00

    #@5c
    add-float/2addr v9, v10

    #@5d
    float-to-int v9, v9

    #@5e
    iput v9, v6, Landroid/graphics/Rect;->top:I

    #@60
    .line 463
    const/4 v9, 0x0

    #@61
    aget v9, v5, v9

    #@63
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    #@66
    move-result v10

    #@67
    add-int/2addr v9, v10

    #@68
    int-to-float v9, v9

    #@69
    mul-float/2addr v9, v0

    #@6a
    const/high16 v10, 0x3f00

    #@6c
    add-float/2addr v9, v10

    #@6d
    float-to-int v9, v9

    #@6e
    iput v9, v6, Landroid/graphics/Rect;->right:I

    #@70
    .line 464
    const/4 v9, 0x1

    #@71
    aget v9, v5, v9

    #@73
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    #@76
    move-result v10

    #@77
    add-int/2addr v9, v10

    #@78
    int-to-float v9, v9

    #@79
    mul-float/2addr v9, v0

    #@7a
    const/high16 v10, 0x3f00

    #@7c
    add-float/2addr v9, v10

    #@7d
    float-to-int v9, v9

    #@7e
    iput v9, v6, Landroid/graphics/Rect;->bottom:I

    #@80
    .line 466
    new-instance v4, Landroid/content/Intent;

    #@82
    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    #@85
    .line 467
    .local v4, intent:Landroid/content/Intent;
    invoke-virtual {v4, v6}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@88
    .line 468
    iget-object v9, p0, Landroid/widget/RemoteViews$SetPendingIntentTemplate$1;->val$handler:Landroid/widget/RemoteViews$OnClickHandler;

    #@8a
    iget-object v10, p0, Landroid/widget/RemoteViews$SetPendingIntentTemplate$1;->this$1:Landroid/widget/RemoteViews$SetPendingIntentTemplate;

    #@8c
    iget-object v10, v10, Landroid/widget/RemoteViews$SetPendingIntentTemplate;->pendingIntentTemplate:Landroid/app/PendingIntent;

    #@8e
    invoke-virtual {v9, p2, v10, v2}, Landroid/widget/RemoteViews$OnClickHandler;->onClickHandler(Landroid/view/View;Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    #@91
    goto :goto_14

    #@92
    .line 446
    .end local v0           #appScale:F
    .end local v4           #intent:Landroid/content/Intent;
    .end local v5           #pos:[I
    .end local v6           #rect:Landroid/graphics/Rect;
    .restart local v7       #tag:Ljava/lang/Object;
    :cond_92
    add-int/lit8 v3, v3, 0x1

    #@94
    goto :goto_1b
.end method
