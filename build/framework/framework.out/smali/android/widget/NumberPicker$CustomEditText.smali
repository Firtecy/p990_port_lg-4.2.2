.class public Landroid/widget/NumberPicker$CustomEditText;
.super Landroid/widget/EditText;
.source "NumberPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomEditText"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 2147
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 2148
    return-void
.end method


# virtual methods
.method public onEditorAction(I)V
    .registers 3
    .parameter "actionCode"

    #@0
    .prologue
    .line 2152
    invoke-super {p0, p1}, Landroid/widget/EditText;->onEditorAction(I)V

    #@3
    .line 2153
    const/4 v0, 0x6

    #@4
    if-ne p1, v0, :cond_9

    #@6
    .line 2154
    invoke-virtual {p0}, Landroid/widget/NumberPicker$CustomEditText;->clearFocus()V

    #@9
    .line 2156
    :cond_9
    return-void
.end method
