.class public Landroid/widget/Gallery;
.super Landroid/widget/AbsSpinner;
.source "Gallery.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Gallery$LayoutParams;,
        Landroid/widget/Gallery$FlingRunnable;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final SCROLL_TO_FLING_UNCERTAINTY_TIMEOUT:I = 0xfa

.field private static final TAG:Ljava/lang/String; = "Gallery"

.field private static final localLOGV:Z


# instance fields
.field private mAnimationDuration:I

.field private mContextMenuInfo:Landroid/widget/AdapterView$AdapterContextMenuInfo;

.field private mDisableSuppressSelectionChangedRunnable:Ljava/lang/Runnable;

.field private mDownTouchPosition:I

.field private mDownTouchView:Landroid/view/View;

.field private mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGravity:I

.field private mIsFirstScroll:Z

.field private mIsRtl:Z

.field private mLeftMost:I

.field private mReceivedInvokeKeyDown:Z

.field private mRightMost:I

.field private mSelectedCenterOffset:I

.field private mSelectedChild:Landroid/view/View;

.field private mShouldCallbackDuringFling:Z

.field private mShouldCallbackOnUnselectedItemClick:Z

.field private mShouldStopFling:Z

.field private mSpacing:I

.field private mSuppressSelectionChanged:Z

.field private mUnselectedAlpha:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 192
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 196
    const v0, 0x1010070

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 197
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 200
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 78
    iput v7, p0, Landroid/widget/Gallery;->mSpacing:I

    #@8
    .line 84
    const/16 v5, 0x190

    #@a
    iput v5, p0, Landroid/widget/Gallery;->mAnimationDuration:I

    #@c
    .line 121
    new-instance v5, Landroid/widget/Gallery$FlingRunnable;

    #@e
    invoke-direct {v5, p0}, Landroid/widget/Gallery$FlingRunnable;-><init>(Landroid/widget/Gallery;)V

    #@11
    iput-object v5, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@13
    .line 127
    new-instance v5, Landroid/widget/Gallery$1;

    #@15
    invoke-direct {v5, p0}, Landroid/widget/Gallery$1;-><init>(Landroid/widget/Gallery;)V

    #@18
    iput-object v5, p0, Landroid/widget/Gallery;->mDisableSuppressSelectionChangedRunnable:Ljava/lang/Runnable;

    #@1a
    .line 152
    iput-boolean v6, p0, Landroid/widget/Gallery;->mShouldCallbackDuringFling:Z

    #@1c
    .line 157
    iput-boolean v6, p0, Landroid/widget/Gallery;->mShouldCallbackOnUnselectedItemClick:Z

    #@1e
    .line 183
    iput-boolean v6, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@20
    .line 202
    new-instance v5, Landroid/view/GestureDetector;

    #@22
    invoke-direct {v5, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@25
    iput-object v5, p0, Landroid/widget/Gallery;->mGestureDetector:Landroid/view/GestureDetector;

    #@27
    .line 203
    iget-object v5, p0, Landroid/widget/Gallery;->mGestureDetector:Landroid/view/GestureDetector;

    #@29
    invoke-virtual {v5, v6}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    #@2c
    .line 205
    sget-object v5, Lcom/android/internal/R$styleable;->Gallery:[I

    #@2e
    invoke-virtual {p1, p2, v5, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@31
    move-result-object v0

    #@32
    .line 208
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@35
    move-result v2

    #@36
    .line 209
    .local v2, index:I
    if-ltz v2, :cond_3b

    #@38
    .line 210
    invoke-virtual {p0, v2}, Landroid/widget/Gallery;->setGravity(I)V

    #@3b
    .line 213
    :cond_3b
    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3e
    move-result v1

    #@3f
    .line 215
    .local v1, animationDuration:I
    if-lez v1, :cond_44

    #@41
    .line 216
    invoke-virtual {p0, v1}, Landroid/widget/Gallery;->setAnimationDuration(I)V

    #@44
    .line 219
    :cond_44
    const/4 v5, 0x2

    #@45
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@48
    move-result v3

    #@49
    .line 221
    .local v3, spacing:I
    invoke-virtual {p0, v3}, Landroid/widget/Gallery;->setSpacing(I)V

    #@4c
    .line 223
    const/4 v5, 0x3

    #@4d
    const/high16 v6, 0x3f00

    #@4f
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@52
    move-result v4

    #@53
    .line 225
    .local v4, unselectedAlpha:F
    invoke-virtual {p0, v4}, Landroid/widget/Gallery;->setUnselectedAlpha(F)V

    #@56
    .line 227
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@59
    .line 231
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5b
    or-int/lit16 v5, v5, 0x400

    #@5d
    iput v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5f
    .line 233
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@61
    or-int/lit16 v5, v5, 0x800

    #@63
    iput v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@65
    .line 234
    return-void
.end method

.method static synthetic access$002(Landroid/widget/Gallery;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-boolean p1, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@2
    return p1
.end method

.method static synthetic access$1000(Landroid/widget/Gallery;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Landroid/widget/Gallery;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@2
    return v0
.end method

.method static synthetic access$1200(Landroid/widget/Gallery;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/Gallery;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/widget/Gallery;->dispatchUnpress()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/widget/Gallery;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/widget/Gallery;->mAnimationDuration:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/widget/Gallery;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/widget/Gallery;->scrollIntoSlots()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/Gallery;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-boolean v0, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Landroid/widget/Gallery;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-boolean p1, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@2
    return p1
.end method

.method static synthetic access$702(Landroid/widget/Gallery;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput p1, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@2
    return p1
.end method

.method static synthetic access$800(Landroid/widget/Gallery;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-boolean v0, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Landroid/widget/Gallery;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@2
    return v0
.end method

.method private calculateTop(Landroid/view/View;Z)I
    .registers 9
    .parameter "child"
    .parameter "duringLayout"

    #@0
    .prologue
    .line 933
    if-eqz p2, :cond_13

    #@2
    invoke-virtual {p0}, Landroid/widget/Gallery;->getMeasuredHeight()I

    #@5
    move-result v3

    #@6
    .line 934
    .local v3, myHeight:I
    :goto_6
    if-eqz p2, :cond_18

    #@8
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@b
    move-result v1

    #@c
    .line 936
    .local v1, childHeight:I
    :goto_c
    const/4 v2, 0x0

    #@d
    .line 938
    .local v2, childTop:I
    iget v4, p0, Landroid/widget/Gallery;->mGravity:I

    #@f
    sparse-switch v4, :sswitch_data_42

    #@12
    .line 951
    :goto_12
    return v2

    #@13
    .line 933
    .end local v1           #childHeight:I
    .end local v2           #childTop:I
    .end local v3           #myHeight:I
    :cond_13
    invoke-virtual {p0}, Landroid/widget/Gallery;->getHeight()I

    #@16
    move-result v3

    #@17
    goto :goto_6

    #@18
    .line 934
    .restart local v3       #myHeight:I
    :cond_18
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@1b
    move-result v1

    #@1c
    goto :goto_c

    #@1d
    .line 940
    .restart local v1       #childHeight:I
    .restart local v2       #childTop:I
    :sswitch_1d
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@1f
    iget v2, v4, Landroid/graphics/Rect;->top:I

    #@21
    .line 941
    goto :goto_12

    #@22
    .line 943
    :sswitch_22
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@24
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@26
    sub-int v4, v3, v4

    #@28
    iget-object v5, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@2a
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@2c
    sub-int/2addr v4, v5

    #@2d
    sub-int v0, v4, v1

    #@2f
    .line 945
    .local v0, availableSpace:I
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@31
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@33
    div-int/lit8 v5, v0, 0x2

    #@35
    add-int v2, v4, v5

    #@37
    .line 946
    goto :goto_12

    #@38
    .line 948
    .end local v0           #availableSpace:I
    :sswitch_38
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@3a
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@3c
    sub-int v4, v3, v4

    #@3e
    sub-int v2, v4, v1

    #@40
    goto :goto_12

    #@41
    .line 938
    nop

    #@42
    :sswitch_data_42
    .sparse-switch
        0x10 -> :sswitch_22
        0x30 -> :sswitch_1d
        0x50 -> :sswitch_38
    .end sparse-switch
.end method

.method private detachOffScreenChildren(Z)V
    .registers 13
    .parameter "toLeft"

    #@0
    .prologue
    .line 482
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@3
    move-result v7

    #@4
    .line 483
    .local v7, numChildren:I
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6
    .line 484
    .local v2, firstPosition:I
    const/4 v8, 0x0

    #@7
    .line 485
    .local v8, start:I
    const/4 v1, 0x0

    #@8
    .line 487
    .local v1, count:I
    if-eqz p1, :cond_42

    #@a
    .line 488
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@c
    .line 489
    .local v3, galleryLeft:I
    const/4 v5, 0x0

    #@d
    .local v5, i:I
    :goto_d
    if-ge v5, v7, :cond_21

    #@f
    .line 490
    iget-boolean v9, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@11
    if-eqz v9, :cond_33

    #@13
    add-int/lit8 v9, v7, -0x1

    #@15
    sub-int v6, v9, v5

    #@17
    .line 491
    .local v6, n:I
    :goto_17
    invoke-virtual {p0, v6}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@1a
    move-result-object v0

    #@1b
    .line 492
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@1e
    move-result v9

    #@1f
    if-lt v9, v3, :cond_35

    #@21
    .line 500
    .end local v0           #child:Landroid/view/View;
    .end local v6           #n:I
    :cond_21
    iget-boolean v9, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@23
    if-nez v9, :cond_26

    #@25
    .line 501
    const/4 v8, 0x0

    #@26
    .line 521
    .end local v3           #galleryLeft:I
    :cond_26
    :goto_26
    invoke-virtual {p0, v8, v1}, Landroid/widget/Gallery;->detachViewsFromParent(II)V

    #@29
    .line 523
    iget-boolean v9, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@2b
    if-eq p1, v9, :cond_32

    #@2d
    .line 524
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2f
    add-int/2addr v9, v1

    #@30
    iput v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@32
    .line 526
    :cond_32
    return-void

    #@33
    .restart local v3       #galleryLeft:I
    :cond_33
    move v6, v5

    #@34
    .line 490
    goto :goto_17

    #@35
    .line 495
    .restart local v0       #child:Landroid/view/View;
    .restart local v6       #n:I
    :cond_35
    move v8, v6

    #@36
    .line 496
    add-int/lit8 v1, v1, 0x1

    #@38
    .line 497
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@3a
    add-int v10, v2, v6

    #@3c
    invoke-virtual {v9, v10, v0}, Landroid/widget/AbsSpinner$RecycleBin;->put(ILandroid/view/View;)V

    #@3f
    .line 489
    add-int/lit8 v5, v5, 0x1

    #@41
    goto :goto_d

    #@42
    .line 504
    .end local v0           #child:Landroid/view/View;
    .end local v3           #galleryLeft:I
    .end local v5           #i:I
    .end local v6           #n:I
    :cond_42
    invoke-virtual {p0}, Landroid/widget/Gallery;->getWidth()I

    #@45
    move-result v9

    #@46
    iget v10, p0, Landroid/view/View;->mPaddingRight:I

    #@48
    sub-int v4, v9, v10

    #@4a
    .line 505
    .local v4, galleryRight:I
    add-int/lit8 v5, v7, -0x1

    #@4c
    .restart local v5       #i:I
    :goto_4c
    if-ltz v5, :cond_60

    #@4e
    .line 506
    iget-boolean v9, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@50
    if-eqz v9, :cond_66

    #@52
    add-int/lit8 v9, v7, -0x1

    #@54
    sub-int v6, v9, v5

    #@56
    .line 507
    .restart local v6       #n:I
    :goto_56
    invoke-virtual {p0, v6}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@59
    move-result-object v0

    #@5a
    .line 508
    .restart local v0       #child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@5d
    move-result v9

    #@5e
    if-gt v9, v4, :cond_68

    #@60
    .line 516
    .end local v0           #child:Landroid/view/View;
    .end local v6           #n:I
    :cond_60
    iget-boolean v9, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@62
    if-eqz v9, :cond_26

    #@64
    .line 517
    const/4 v8, 0x0

    #@65
    goto :goto_26

    #@66
    :cond_66
    move v6, v5

    #@67
    .line 506
    goto :goto_56

    #@68
    .line 511
    .restart local v0       #child:Landroid/view/View;
    .restart local v6       #n:I
    :cond_68
    move v8, v6

    #@69
    .line 512
    add-int/lit8 v1, v1, 0x1

    #@6b
    .line 513
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@6d
    add-int v10, v2, v6

    #@6f
    invoke-virtual {v9, v10, v0}, Landroid/widget/AbsSpinner$RecycleBin;->put(ILandroid/view/View;)V

    #@72
    .line 505
    add-int/lit8 v5, v5, -0x1

    #@74
    goto :goto_4c
.end method

.method private dispatchLongPress(Landroid/view/View;IJ)Z
    .registers 12
    .parameter "view"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 1175
    const/4 v6, 0x0

    #@1
    .line 1177
    .local v6, handled:Z
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 1178
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@7
    iget-object v2, p0, Landroid/widget/Gallery;->mDownTouchView:Landroid/view/View;

    #@9
    iget v3, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@b
    move-object v1, p0

    #@c
    move-wide v4, p3

    #@d
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    #@10
    move-result v6

    #@11
    .line 1182
    :cond_11
    if-nez v6, :cond_1e

    #@13
    .line 1183
    new-instance v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@15
    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    #@18
    iput-object v0, p0, Landroid/widget/Gallery;->mContextMenuInfo:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@1a
    .line 1184
    invoke-super {p0, p0}, Landroid/widget/AbsSpinner;->showContextMenuForChild(Landroid/view/View;)Z

    #@1d
    move-result v6

    #@1e
    .line 1187
    :cond_1e
    if-eqz v6, :cond_24

    #@20
    .line 1188
    const/4 v0, 0x0

    #@21
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->performHapticFeedback(I)Z

    #@24
    .line 1191
    :cond_24
    return v6
.end method

.method private dispatchPress(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1111
    if-eqz p1, :cond_6

    #@3
    .line 1112
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    #@6
    .line 1115
    :cond_6
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->setPressed(Z)V

    #@9
    .line 1116
    return-void
.end method

.method private dispatchUnpress()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1120
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@4
    move-result v1

    #@5
    add-int/lit8 v0, v1, -0x1

    #@7
    .local v0, i:I
    :goto_7
    if-ltz v0, :cond_13

    #@9
    .line 1121
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, v2}, Landroid/view/View;->setPressed(Z)V

    #@10
    .line 1120
    add-int/lit8 v0, v0, -0x1

    #@12
    goto :goto_7

    #@13
    .line 1124
    :cond_13
    invoke-virtual {p0, v2}, Landroid/widget/Gallery;->setPressed(Z)V

    #@16
    .line 1125
    return-void
.end method

.method private fillToGalleryLeft()V
    .registers 2

    #@0
    .prologue
    .line 689
    iget-boolean v0, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 690
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryLeftRtl()V

    #@7
    .line 694
    :goto_7
    return-void

    #@8
    .line 692
    :cond_8
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryLeftLtr()V

    #@b
    goto :goto_7
.end method

.method private fillToGalleryLeftLtr()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 728
    iget v3, p0, Landroid/widget/Gallery;->mSpacing:I

    #@3
    .line 729
    .local v3, itemSpacing:I
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@5
    .line 732
    .local v2, galleryLeft:I
    invoke-virtual {p0, v7}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@8
    move-result-object v4

    #@9
    .line 736
    .local v4, prevIterationView:Landroid/view/View;
    if-eqz v4, :cond_2c

    #@b
    .line 737
    iget v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@d
    add-int/lit8 v0, v5, -0x1

    #@f
    .line 738
    .local v0, curPosition:I
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    #@12
    move-result v5

    #@13
    sub-int v1, v5, v3

    #@15
    .line 746
    .local v1, curRightEdge:I
    :goto_15
    if-le v1, v2, :cond_3a

    #@17
    if-ltz v0, :cond_3a

    #@19
    .line 747
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1b
    sub-int v5, v0, v5

    #@1d
    invoke-direct {p0, v0, v5, v1, v7}, Landroid/widget/Gallery;->makeAndAddView(IIIZ)Landroid/view/View;

    #@20
    move-result-object v4

    #@21
    .line 751
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@23
    .line 754
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    #@26
    move-result v5

    #@27
    sub-int v1, v5, v3

    #@29
    .line 755
    add-int/lit8 v0, v0, -0x1

    #@2b
    goto :goto_15

    #@2c
    .line 741
    .end local v0           #curPosition:I
    .end local v1           #curRightEdge:I
    :cond_2c
    const/4 v0, 0x0

    #@2d
    .line 742
    .restart local v0       #curPosition:I
    iget v5, p0, Landroid/view/View;->mRight:I

    #@2f
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@31
    sub-int/2addr v5, v6

    #@32
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@34
    sub-int v1, v5, v6

    #@36
    .line 743
    .restart local v1       #curRightEdge:I
    const/4 v5, 0x1

    #@37
    iput-boolean v5, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@39
    goto :goto_15

    #@3a
    .line 757
    :cond_3a
    return-void
.end method

.method private fillToGalleryLeftRtl()V
    .registers 10

    #@0
    .prologue
    .line 697
    iget v3, p0, Landroid/widget/Gallery;->mSpacing:I

    #@2
    .line 698
    .local v3, itemSpacing:I
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@4
    .line 699
    .local v2, galleryLeft:I
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@7
    move-result v4

    #@8
    .line 700
    .local v4, numChildren:I
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@a
    .line 703
    .local v5, numItems:I
    add-int/lit8 v7, v4, -0x1

    #@c
    invoke-virtual {p0, v7}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v6

    #@10
    .line 707
    .local v6, prevIterationView:Landroid/view/View;
    if-eqz v6, :cond_34

    #@12
    .line 708
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@14
    add-int v0, v7, v4

    #@16
    .line 709
    .local v0, curPosition:I
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    #@19
    move-result v7

    #@1a
    sub-int v1, v7, v3

    #@1c
    .line 717
    .local v1, curRightEdge:I
    :goto_1c
    if-le v1, v2, :cond_47

    #@1e
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@20
    if-ge v0, v7, :cond_47

    #@22
    .line 718
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@24
    sub-int v7, v0, v7

    #@26
    const/4 v8, 0x0

    #@27
    invoke-direct {p0, v0, v7, v1, v8}, Landroid/widget/Gallery;->makeAndAddView(IIIZ)Landroid/view/View;

    #@2a
    move-result-object v6

    #@2b
    .line 722
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    #@2e
    move-result v7

    #@2f
    sub-int v1, v7, v3

    #@31
    .line 723
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_1c

    #@34
    .line 712
    .end local v0           #curPosition:I
    .end local v1           #curRightEdge:I
    :cond_34
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@36
    add-int/lit8 v0, v7, -0x1

    #@38
    .restart local v0       #curPosition:I
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3a
    .line 713
    iget v7, p0, Landroid/view/View;->mRight:I

    #@3c
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@3e
    sub-int/2addr v7, v8

    #@3f
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@41
    sub-int v1, v7, v8

    #@43
    .line 714
    .restart local v1       #curRightEdge:I
    const/4 v7, 0x1

    #@44
    iput-boolean v7, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@46
    goto :goto_1c

    #@47
    .line 725
    :cond_47
    return-void
.end method

.method private fillToGalleryRight()V
    .registers 2

    #@0
    .prologue
    .line 760
    iget-boolean v0, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 761
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryRightRtl()V

    #@7
    .line 765
    :goto_7
    return-void

    #@8
    .line 763
    :cond_8
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryRightLtr()V

    #@b
    goto :goto_7
.end method

.method private fillToGalleryRightLtr()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 799
    iget v3, p0, Landroid/widget/Gallery;->mSpacing:I

    #@3
    .line 800
    .local v3, itemSpacing:I
    iget v7, p0, Landroid/view/View;->mRight:I

    #@5
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@7
    sub-int/2addr v7, v8

    #@8
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    sub-int v2, v7, v8

    #@c
    .line 801
    .local v2, galleryRight:I
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@f
    move-result v4

    #@10
    .line 802
    .local v4, numChildren:I
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@12
    .line 805
    .local v5, numItems:I
    add-int/lit8 v7, v4, -0x1

    #@14
    invoke-virtual {p0, v7}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@17
    move-result-object v6

    #@18
    .line 809
    .local v6, prevIterationView:Landroid/view/View;
    if-eqz v6, :cond_39

    #@1a
    .line 810
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1c
    add-int v1, v7, v4

    #@1e
    .line 811
    .local v1, curPosition:I
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    #@21
    move-result v7

    #@22
    add-int v0, v7, v3

    #@24
    .line 818
    .local v0, curLeftEdge:I
    :goto_24
    if-ge v0, v2, :cond_44

    #@26
    if-ge v1, v5, :cond_44

    #@28
    .line 819
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2a
    sub-int v7, v1, v7

    #@2c
    invoke-direct {p0, v1, v7, v0, v9}, Landroid/widget/Gallery;->makeAndAddView(IIIZ)Landroid/view/View;

    #@2f
    move-result-object v6

    #@30
    .line 823
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    #@33
    move-result v7

    #@34
    add-int v0, v7, v3

    #@36
    .line 824
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_24

    #@39
    .line 813
    .end local v0           #curLeftEdge:I
    .end local v1           #curPosition:I
    :cond_39
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3b
    add-int/lit8 v1, v7, -0x1

    #@3d
    .restart local v1       #curPosition:I
    iput v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3f
    .line 814
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@41
    .line 815
    .restart local v0       #curLeftEdge:I
    iput-boolean v9, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@43
    goto :goto_24

    #@44
    .line 826
    :cond_44
    return-void
.end method

.method private fillToGalleryRightRtl()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 768
    iget v3, p0, Landroid/widget/Gallery;->mSpacing:I

    #@3
    .line 769
    .local v3, itemSpacing:I
    iget v5, p0, Landroid/view/View;->mRight:I

    #@5
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@7
    sub-int/2addr v5, v6

    #@8
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    sub-int v2, v5, v6

    #@c
    .line 772
    .local v2, galleryRight:I
    const/4 v5, 0x0

    #@d
    invoke-virtual {p0, v5}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v4

    #@11
    .line 776
    .local v4, prevIterationView:Landroid/view/View;
    if-eqz v4, :cond_34

    #@13
    .line 777
    iget v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@15
    add-int/lit8 v1, v5, -0x1

    #@17
    .line 778
    .local v1, curPosition:I
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    #@1a
    move-result v5

    #@1b
    add-int v0, v5, v3

    #@1d
    .line 785
    .local v0, curLeftEdge:I
    :goto_1d
    if-ge v0, v2, :cond_3a

    #@1f
    if-ltz v1, :cond_3a

    #@21
    .line 786
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@23
    sub-int v5, v1, v5

    #@25
    invoke-direct {p0, v1, v5, v0, v7}, Landroid/widget/Gallery;->makeAndAddView(IIIZ)Landroid/view/View;

    #@28
    move-result-object v4

    #@29
    .line 790
    iput v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2b
    .line 793
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    #@2e
    move-result v5

    #@2f
    add-int v0, v5, v3

    #@31
    .line 794
    add-int/lit8 v1, v1, -0x1

    #@33
    goto :goto_1d

    #@34
    .line 780
    .end local v0           #curLeftEdge:I
    .end local v1           #curPosition:I
    :cond_34
    const/4 v1, 0x0

    #@35
    .line 781
    .restart local v1       #curPosition:I
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@37
    .line 782
    .restart local v0       #curLeftEdge:I
    iput-boolean v7, p0, Landroid/widget/Gallery;->mShouldStopFling:Z

    #@39
    goto :goto_1d

    #@3a
    .line 796
    :cond_3a
    return-void
.end method

.method private getCenterOfGallery()I
    .registers 3

    #@0
    .prologue
    .line 465
    invoke-virtual {p0}, Landroid/widget/Gallery;->getWidth()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@6
    sub-int/2addr v0, v1

    #@7
    iget v1, p0, Landroid/view/View;->mPaddingRight:I

    #@9
    sub-int/2addr v0, v1

    #@a
    div-int/lit8 v0, v0, 0x2

    #@c
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@e
    add-int/2addr v0, v1

    #@f
    return v0
.end method

.method private static getCenterOfView(Landroid/view/View;)I
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 472
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    #@7
    move-result v1

    #@8
    div-int/lit8 v1, v1, 0x2

    #@a
    add-int/2addr v0, v1

    #@b
    return v0
.end method

.method private makeAndAddView(IIIZ)Landroid/view/View;
    .registers 10
    .parameter "position"
    .parameter "offset"
    .parameter "x"
    .parameter "fromLeft"

    #@0
    .prologue
    .line 846
    iget-boolean v3, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2
    if-nez v3, :cond_2a

    #@4
    .line 847
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@6
    invoke-virtual {v3, p1}, Landroid/widget/AbsSpinner$RecycleBin;->get(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 848
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_2a

    #@c
    .line 850
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@f
    move-result v2

    #@10
    .line 853
    .local v2, childLeft:I
    iget v3, p0, Landroid/widget/Gallery;->mRightMost:I

    #@12
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@15
    move-result v4

    #@16
    add-int/2addr v4, v2

    #@17
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@1a
    move-result v3

    #@1b
    iput v3, p0, Landroid/widget/Gallery;->mRightMost:I

    #@1d
    .line 855
    iget v3, p0, Landroid/widget/Gallery;->mLeftMost:I

    #@1f
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    #@22
    move-result v3

    #@23
    iput v3, p0, Landroid/widget/Gallery;->mLeftMost:I

    #@25
    .line 858
    invoke-direct {p0, v0, p2, p3, p4}, Landroid/widget/Gallery;->setUpChild(Landroid/view/View;IIZ)V

    #@28
    move-object v1, v0

    #@29
    .line 870
    .end local v0           #child:Landroid/view/View;
    .end local v2           #childLeft:I
    .local v1, child:Landroid/view/View;
    :goto_29
    return-object v1

    #@2a
    .line 865
    .end local v1           #child:Landroid/view/View;
    :cond_2a
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@2c
    const/4 v4, 0x0

    #@2d
    invoke-interface {v3, p1, v4, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@30
    move-result-object v0

    #@31
    .line 868
    .restart local v0       #child:Landroid/view/View;
    invoke-direct {p0, v0, p2, p3, p4}, Landroid/widget/Gallery;->setUpChild(Landroid/view/View;IIZ)V

    #@34
    move-object v1, v0

    #@35
    .line 870
    .end local v0           #child:Landroid/view/View;
    .restart local v1       #child:Landroid/view/View;
    goto :goto_29
.end method

.method private offsetChildrenLeftAndRight(I)V
    .registers 4
    .parameter "offset"

    #@0
    .prologue
    .line 456
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@3
    move-result v1

    #@4
    add-int/lit8 v0, v1, -0x1

    #@6
    .local v0, i:I
    :goto_6
    if-ltz v0, :cond_12

    #@8
    .line 457
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@f
    .line 456
    add-int/lit8 v0, v0, -0x1

    #@11
    goto :goto_6

    #@12
    .line 459
    :cond_12
    return-void
.end method

.method private onFinishedMovement()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 548
    iget-boolean v0, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 549
    iput-boolean v1, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@7
    .line 552
    invoke-super {p0}, Landroid/widget/AbsSpinner;->selectionChanged()V

    #@a
    .line 554
    :cond_a
    iput v1, p0, Landroid/widget/Gallery;->mSelectedCenterOffset:I

    #@c
    .line 555
    invoke-virtual {p0}, Landroid/widget/Gallery;->invalidate()V

    #@f
    .line 556
    return-void
.end method

.method private scrollIntoSlots()V
    .registers 5

    #@0
    .prologue
    .line 534
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_a

    #@6
    iget-object v3, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@8
    if-nez v3, :cond_b

    #@a
    .line 545
    :cond_a
    :goto_a
    return-void

    #@b
    .line 536
    :cond_b
    iget-object v3, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@d
    invoke-static {v3}, Landroid/widget/Gallery;->getCenterOfView(Landroid/view/View;)I

    #@10
    move-result v1

    #@11
    .line 537
    .local v1, selectedCenter:I
    invoke-direct {p0}, Landroid/widget/Gallery;->getCenterOfGallery()I

    #@14
    move-result v2

    #@15
    .line 539
    .local v2, targetCenter:I
    sub-int v0, v2, v1

    #@17
    .line 540
    .local v0, scrollAmount:I
    if-eqz v0, :cond_1f

    #@19
    .line 541
    iget-object v3, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@1b
    invoke-virtual {v3, v0}, Landroid/widget/Gallery$FlingRunnable;->startUsingDistance(I)V

    #@1e
    goto :goto_a

    #@1f
    .line 543
    :cond_1f
    invoke-direct {p0}, Landroid/widget/Gallery;->onFinishedMovement()V

    #@22
    goto :goto_a
.end method

.method private scrollToChild(I)Z
    .registers 6
    .parameter "childPosition"

    #@0
    .prologue
    .line 1281
    invoke-virtual {p0, p1}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 1283
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_17

    #@6
    .line 1284
    invoke-direct {p0}, Landroid/widget/Gallery;->getCenterOfGallery()I

    #@9
    move-result v2

    #@a
    invoke-static {v0}, Landroid/widget/Gallery;->getCenterOfView(Landroid/view/View;)I

    #@d
    move-result v3

    #@e
    sub-int v1, v2, v3

    #@10
    .line 1285
    .local v1, distance:I
    iget-object v2, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@12
    invoke-virtual {v2, v1}, Landroid/widget/Gallery$FlingRunnable;->startUsingDistance(I)V

    #@15
    .line 1286
    const/4 v2, 0x1

    #@16
    .line 1289
    .end local v1           #distance:I
    :goto_16
    return v2

    #@17
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16
.end method

.method private setSelectionToCenterChild()V
    .registers 11

    #@0
    .prologue
    .line 571
    iget-object v7, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@2
    .line 572
    .local v7, selView:Landroid/view/View;
    iget-object v8, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@4
    if-nez v8, :cond_7

    #@6
    .line 609
    :cond_6
    :goto_6
    return-void

    #@7
    .line 574
    :cond_7
    invoke-direct {p0}, Landroid/widget/Gallery;->getCenterOfGallery()I

    #@a
    move-result v3

    #@b
    .line 577
    .local v3, galleryCenter:I
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    #@e
    move-result v8

    #@f
    if-gt v8, v3, :cond_17

    #@11
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    #@14
    move-result v8

    #@15
    if-ge v8, v3, :cond_6

    #@17
    .line 582
    :cond_17
    const v2, 0x7fffffff

    #@1a
    .line 583
    .local v2, closestEdgeDistance:I
    const/4 v6, 0x0

    #@1b
    .line 584
    .local v6, newSelectedChildIndex:I
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@1e
    move-result v8

    #@1f
    add-int/lit8 v4, v8, -0x1

    #@21
    .local v4, i:I
    :goto_21
    if-ltz v4, :cond_34

    #@23
    .line 586
    invoke-virtual {p0, v4}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@26
    move-result-object v0

    #@27
    .line 588
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@2a
    move-result v8

    #@2b
    if-gt v8, v3, :cond_46

    #@2d
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@30
    move-result v8

    #@31
    if-lt v8, v3, :cond_46

    #@33
    .line 590
    move v6, v4

    #@34
    .line 602
    .end local v0           #child:Landroid/view/View;
    :cond_34
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@36
    add-int v5, v8, v6

    #@38
    .line 604
    .local v5, newPos:I
    iget v8, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3a
    if-eq v5, v8, :cond_6

    #@3c
    .line 605
    invoke-virtual {p0, v5}, Landroid/widget/Gallery;->setSelectedPositionInt(I)V

    #@3f
    .line 606
    invoke-virtual {p0, v5}, Landroid/widget/Gallery;->setNextSelectedPositionInt(I)V

    #@42
    .line 607
    invoke-virtual {p0}, Landroid/widget/Gallery;->checkSelectionChanged()V

    #@45
    goto :goto_6

    #@46
    .line 594
    .end local v5           #newPos:I
    .restart local v0       #child:Landroid/view/View;
    :cond_46
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@49
    move-result v8

    #@4a
    sub-int/2addr v8, v3

    #@4b
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    #@4e
    move-result v8

    #@4f
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@52
    move-result v9

    #@53
    sub-int/2addr v9, v3

    #@54
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@57
    move-result v9

    #@58
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@5b
    move-result v1

    #@5c
    .line 596
    .local v1, childClosestEdgeDistance:I
    if-ge v1, v2, :cond_60

    #@5e
    .line 597
    move v2, v1

    #@5f
    .line 598
    move v6, v4

    #@60
    .line 584
    :cond_60
    add-int/lit8 v4, v4, -0x1

    #@62
    goto :goto_21
.end method

.method private setUpChild(Landroid/view/View;IIZ)V
    .registers 16
    .parameter "child"
    .parameter "offset"
    .parameter "x"
    .parameter "fromLeft"

    #@0
    .prologue
    .line 889
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v6

    #@4
    check-cast v6, Landroid/widget/Gallery$LayoutParams;

    #@6
    .line 890
    .local v6, lp:Landroid/widget/Gallery$LayoutParams;
    if-nez v6, :cond_e

    #@8
    .line 891
    invoke-virtual {p0}, Landroid/widget/Gallery;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@b
    move-result-object v6

    #@c
    .end local v6           #lp:Landroid/widget/Gallery$LayoutParams;
    check-cast v6, Landroid/widget/Gallery$LayoutParams;

    #@e
    .line 894
    .restart local v6       #lp:Landroid/widget/Gallery$LayoutParams;
    :cond_e
    iget-boolean v8, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@10
    if-eq p4, v8, :cond_59

    #@12
    const/4 v8, -0x1

    #@13
    :goto_13
    invoke-virtual {p0, p1, v8, v6}, Landroid/widget/Gallery;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    #@16
    .line 896
    if-nez p2, :cond_5b

    #@18
    const/4 v8, 0x1

    #@19
    :goto_19
    invoke-virtual {p1, v8}, Landroid/view/View;->setSelected(Z)V

    #@1c
    .line 899
    iget v8, p0, Landroid/widget/AbsSpinner;->mHeightMeasureSpec:I

    #@1e
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@20
    iget v9, v9, Landroid/graphics/Rect;->top:I

    #@22
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@24
    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    #@26
    add-int/2addr v9, v10

    #@27
    iget v10, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@29
    invoke-static {v8, v9, v10}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@2c
    move-result v1

    #@2d
    .line 901
    .local v1, childHeightSpec:I
    iget v8, p0, Landroid/widget/AbsSpinner;->mWidthMeasureSpec:I

    #@2f
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@31
    iget v9, v9, Landroid/graphics/Rect;->left:I

    #@33
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@35
    iget v10, v10, Landroid/graphics/Rect;->right:I

    #@37
    add-int/2addr v9, v10

    #@38
    iget v10, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3a
    invoke-static {v8, v9, v10}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@3d
    move-result v5

    #@3e
    .line 905
    .local v5, childWidthSpec:I
    invoke-virtual {p1, v5, v1}, Landroid/view/View;->measure(II)V

    #@41
    .line 911
    const/4 v8, 0x1

    #@42
    invoke-direct {p0, p1, v8}, Landroid/widget/Gallery;->calculateTop(Landroid/view/View;Z)I

    #@45
    move-result v4

    #@46
    .line 912
    .local v4, childTop:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@49
    move-result v8

    #@4a
    add-int v0, v4, v8

    #@4c
    .line 914
    .local v0, childBottom:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@4f
    move-result v7

    #@50
    .line 915
    .local v7, width:I
    if-eqz p4, :cond_5d

    #@52
    .line 916
    move v2, p3

    #@53
    .line 917
    .local v2, childLeft:I
    add-int v3, v2, v7

    #@55
    .line 923
    .local v3, childRight:I
    :goto_55
    invoke-virtual {p1, v2, v4, v3, v0}, Landroid/view/View;->layout(IIII)V

    #@58
    .line 924
    return-void

    #@59
    .line 894
    .end local v0           #childBottom:I
    .end local v1           #childHeightSpec:I
    .end local v2           #childLeft:I
    .end local v3           #childRight:I
    .end local v4           #childTop:I
    .end local v5           #childWidthSpec:I
    .end local v7           #width:I
    :cond_59
    const/4 v8, 0x0

    #@5a
    goto :goto_13

    #@5b
    .line 896
    :cond_5b
    const/4 v8, 0x0

    #@5c
    goto :goto_19

    #@5d
    .line 919
    .restart local v0       #childBottom:I
    .restart local v1       #childHeightSpec:I
    .restart local v4       #childTop:I
    .restart local v5       #childWidthSpec:I
    .restart local v7       #width:I
    :cond_5d
    sub-int v2, p3, v7

    #@5f
    .line 920
    .restart local v2       #childLeft:I
    move v3, p3

    #@60
    .restart local v3       #childRight:I
    goto :goto_55
.end method

.method private updateSelectedItemMetadata()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1302
    iget-object v1, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@4
    .line 1304
    .local v1, oldSelectedChild:Landroid/view/View;
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8
    sub-int/2addr v2, v3

    #@9
    invoke-virtual {p0, v2}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@f
    .line 1305
    .local v0, child:Landroid/view/View;
    if-nez v0, :cond_12

    #@11
    .line 1328
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1309
    :cond_12
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    #@15
    .line 1310
    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    #@18
    .line 1312
    invoke-virtual {p0}, Landroid/widget/Gallery;->hasFocus()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_21

    #@1e
    .line 1313
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    #@21
    .line 1318
    :cond_21
    if-eqz v1, :cond_11

    #@23
    if-eq v1, v0, :cond_11

    #@25
    .line 1321
    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    #@28
    .line 1325
    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    #@2b
    goto :goto_11
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 326
    instance-of v0, p1, Landroid/widget/Gallery$LayoutParams;

    #@2
    return v0
.end method

.method protected computeHorizontalScrollExtent()I
    .registers 2

    #@0
    .prologue
    .line 309
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected computeHorizontalScrollOffset()I
    .registers 2

    #@0
    .prologue
    .line 315
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 321
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1197
    invoke-virtual {p1, p0, v0, v0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected dispatchSetPressed(Z)V
    .registers 3
    .parameter "pressed"

    #@0
    .prologue
    .line 1140
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1141
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@6
    invoke-virtual {v0, p1}, Landroid/view/View;->setPressed(Z)V

    #@9
    .line 1143
    :cond_9
    return-void
.end method

.method public dispatchSetSelected(Z)V
    .registers 2
    .parameter "selected"

    #@0
    .prologue
    .line 1134
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 344
    new-instance v0, Landroid/widget/Gallery$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    #@6
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 336
    new-instance v0, Landroid/widget/Gallery$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/Gallery;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/Gallery$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 331
    new-instance v0, Landroid/widget/Gallery$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/Gallery$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .registers 6
    .parameter "childCount"
    .parameter "i"

    #@0
    .prologue
    .line 1346
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4
    sub-int v0, v1, v2

    #@6
    .line 1349
    .local v0, selectedIndex:I
    if-gez v0, :cond_9

    #@8
    .line 1359
    .end local p2
    :cond_8
    :goto_8
    return p2

    #@9
    .line 1351
    .restart local p2
    :cond_9
    add-int/lit8 v1, p1, -0x1

    #@b
    if-ne p2, v1, :cond_f

    #@d
    move p2, v0

    #@e
    .line 1353
    goto :goto_8

    #@f
    .line 1354
    :cond_f
    if-lt p2, v0, :cond_8

    #@11
    .line 1356
    add-int/lit8 p2, p2, 0x1

    #@13
    goto :goto_8
.end method

.method getChildHeight(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 363
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected getChildStaticTransformation(Landroid/view/View;Landroid/view/animation/Transformation;)Z
    .registers 4
    .parameter "child"
    .parameter "t"

    #@0
    .prologue
    .line 300
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->clear()V

    #@3
    .line 301
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@5
    if-ne p1, v0, :cond_e

    #@7
    const/high16 v0, 0x3f80

    #@9
    :goto_9
    invoke-virtual {p2, v0}, Landroid/view/animation/Transformation;->setAlpha(F)V

    #@c
    .line 303
    const/4 v0, 0x1

    #@d
    return v0

    #@e
    .line 301
    :cond_e
    iget v0, p0, Landroid/widget/Gallery;->mUnselectedAlpha:F

    #@10
    goto :goto_9
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    #@0
    .prologue
    .line 1147
    iget-object v0, p0, Landroid/widget/Gallery;->mContextMenuInfo:Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@2
    return-object v0
.end method

.method getLimitedMotionScrollAmount(ZI)I
    .registers 10
    .parameter "motionToLeft"
    .parameter "deltaX"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 418
    iget-boolean v6, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@3
    if-eq p1, v6, :cond_15

    #@5
    iget v6, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    add-int/lit8 v3, v6, -0x1

    #@9
    .line 419
    .local v3, extremeItemPosition:I
    :goto_9
    iget v6, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@b
    sub-int v6, v3, v6

    #@d
    invoke-virtual {p0, v6}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v1

    #@11
    .line 421
    .local v1, extremeChild:Landroid/view/View;
    if-nez v1, :cond_17

    #@13
    move v5, p2

    #@14
    .line 444
    :cond_14
    :goto_14
    return v5

    #@15
    .end local v1           #extremeChild:Landroid/view/View;
    .end local v3           #extremeItemPosition:I
    :cond_15
    move v3, v5

    #@16
    .line 418
    goto :goto_9

    #@17
    .line 425
    .restart local v1       #extremeChild:Landroid/view/View;
    .restart local v3       #extremeItemPosition:I
    :cond_17
    invoke-static {v1}, Landroid/widget/Gallery;->getCenterOfView(Landroid/view/View;)I

    #@1a
    move-result v2

    #@1b
    .line 426
    .local v2, extremeChildCenter:I
    invoke-direct {p0}, Landroid/widget/Gallery;->getCenterOfGallery()I

    #@1e
    move-result v4

    #@1f
    .line 428
    .local v4, galleryCenter:I
    if-eqz p1, :cond_2c

    #@21
    .line 429
    if-le v2, v4, :cond_14

    #@23
    .line 442
    :cond_23
    sub-int v0, v4, v2

    #@25
    .line 444
    .local v0, centerDifference:I
    if-eqz p1, :cond_2f

    #@27
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    #@2a
    move-result v5

    #@2b
    goto :goto_14

    #@2c
    .line 435
    .end local v0           #centerDifference:I
    :cond_2c
    if-lt v2, v4, :cond_23

    #@2e
    goto :goto_14

    #@2f
    .line 444
    .restart local v0       #centerDifference:I
    :cond_2f
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    #@32
    move-result v5

    #@33
    goto :goto_14
.end method

.method layout(IZ)V
    .registers 10
    .parameter "delta"
    .parameter "animate"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 624
    invoke-virtual {p0}, Landroid/widget/Gallery;->isLayoutRtl()Z

    #@4
    move-result v4

    #@5
    iput-boolean v4, p0, Landroid/widget/Gallery;->mIsRtl:Z

    #@7
    .line 626
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@9
    iget v0, v4, Landroid/graphics/Rect;->left:I

    #@b
    .line 627
    .local v0, childrenLeft:I
    iget v4, p0, Landroid/view/View;->mRight:I

    #@d
    iget v5, p0, Landroid/view/View;->mLeft:I

    #@f
    sub-int/2addr v4, v5

    #@10
    iget-object v5, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@12
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@14
    sub-int/2addr v4, v5

    #@15
    iget-object v5, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@17
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@19
    sub-int v1, v4, v5

    #@1b
    .line 629
    .local v1, childrenWidth:I
    iget-boolean v4, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@1d
    if-eqz v4, :cond_22

    #@1f
    .line 630
    invoke-virtual {p0}, Landroid/widget/Gallery;->handleDataChanged()V

    #@22
    .line 634
    :cond_22
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@24
    if-nez v4, :cond_2a

    #@26
    .line 635
    invoke-virtual {p0}, Landroid/widget/Gallery;->resetList()V

    #@29
    .line 686
    :goto_29
    return-void

    #@2a
    .line 640
    :cond_2a
    iget v4, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@2c
    if-ltz v4, :cond_33

    #@2e
    .line 641
    iget v4, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@30
    invoke-virtual {p0, v4}, Landroid/widget/Gallery;->setSelectedPositionInt(I)V

    #@33
    .line 645
    :cond_33
    invoke-virtual {p0}, Landroid/widget/Gallery;->recycleAllViews()V

    #@36
    .line 649
    invoke-virtual {p0}, Landroid/widget/Gallery;->detachAllViewsFromParent()V

    #@39
    .line 655
    iput v6, p0, Landroid/widget/Gallery;->mRightMost:I

    #@3b
    .line 656
    iput v6, p0, Landroid/widget/Gallery;->mLeftMost:I

    #@3d
    .line 664
    iget v4, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3f
    iput v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@41
    .line 665
    iget v4, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@43
    const/4 v5, 0x1

    #@44
    invoke-direct {p0, v4, v6, v6, v5}, Landroid/widget/Gallery;->makeAndAddView(IIIZ)Landroid/view/View;

    #@47
    move-result-object v2

    #@48
    .line 668
    .local v2, sel:Landroid/view/View;
    div-int/lit8 v4, v1, 0x2

    #@4a
    add-int/2addr v4, v0

    #@4b
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    #@4e
    move-result v5

    #@4f
    div-int/lit8 v5, v5, 0x2

    #@51
    sub-int/2addr v4, v5

    #@52
    iget v5, p0, Landroid/widget/Gallery;->mSelectedCenterOffset:I

    #@54
    add-int v3, v4, v5

    #@56
    .line 670
    .local v3, selectedOffset:I
    invoke-virtual {v2, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@59
    .line 672
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryRight()V

    #@5c
    .line 673
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryLeft()V

    #@5f
    .line 676
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@61
    invoke-virtual {v4}, Landroid/widget/AbsSpinner$RecycleBin;->clear()V

    #@64
    .line 678
    invoke-virtual {p0}, Landroid/widget/Gallery;->invalidate()V

    #@67
    .line 679
    invoke-virtual {p0}, Landroid/widget/Gallery;->checkSelectionChanged()V

    #@6a
    .line 681
    iput-boolean v6, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@6c
    .line 682
    iput-boolean v6, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@6e
    .line 683
    iget v4, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@70
    invoke-virtual {p0, v4}, Landroid/widget/Gallery;->setNextSelectedPositionInt(I)V

    #@73
    .line 685
    invoke-direct {p0}, Landroid/widget/Gallery;->updateSelectedItemMetadata()V

    #@76
    goto :goto_29
.end method

.method moveNext()Z
    .registers 3

    #@0
    .prologue
    .line 1272
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    if-lez v0, :cond_18

    #@4
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    if-ge v0, v1, :cond_18

    #@c
    .line 1273
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@e
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@10
    sub-int/2addr v0, v1

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    invoke-direct {p0, v0}, Landroid/widget/Gallery;->scrollToChild(I)Z

    #@16
    .line 1274
    const/4 v0, 0x1

    #@17
    .line 1276
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method movePrevious()Z
    .registers 3

    #@0
    .prologue
    .line 1263
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    if-lez v0, :cond_14

    #@4
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    if-lez v0, :cond_14

    #@8
    .line 1264
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@a
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c
    sub-int/2addr v0, v1

    #@d
    add-int/lit8 v0, v0, -0x1

    #@f
    invoke-direct {p0, v0}, Landroid/widget/Gallery;->scrollToChild(I)Z

    #@12
    .line 1265
    const/4 v0, 0x1

    #@13
    .line 1267
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method onCancel()V
    .registers 1

    #@0
    .prologue
    .line 1086
    invoke-virtual {p0}, Landroid/widget/Gallery;->onUp()V

    #@3
    .line 1087
    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "e"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1053
    iget-object v0, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/Gallery$FlingRunnable;->stop(Z)V

    #@7
    .line 1056
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@a
    move-result v0

    #@b
    float-to-int v0, v0

    #@c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@f
    move-result v1

    #@10
    float-to-int v1, v1

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/widget/Gallery;->pointToPosition(II)I

    #@14
    move-result v0

    #@15
    iput v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@17
    .line 1058
    iget v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@19
    if-ltz v0, :cond_2b

    #@1b
    .line 1059
    iget v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@1d
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1f
    sub-int/2addr v0, v1

    #@20
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/widget/Gallery;->mDownTouchView:Landroid/view/View;

    #@26
    .line 1060
    iget-object v0, p0, Landroid/widget/Gallery;->mDownTouchView:Landroid/view/View;

    #@28
    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    #@2b
    .line 1064
    :cond_2b
    iput-boolean v2, p0, Landroid/widget/Gallery;->mIsFirstScroll:Z

    #@2d
    .line 1067
    return v2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 8
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 994
    iget-boolean v0, p0, Landroid/widget/Gallery;->mShouldCallbackDuringFling:Z

    #@3
    if-nez v0, :cond_10

    #@5
    .line 998
    iget-object v0, p0, Landroid/widget/Gallery;->mDisableSuppressSelectionChangedRunnable:Ljava/lang/Runnable;

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@a
    .line 1001
    iget-boolean v0, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@c
    if-nez v0, :cond_10

    #@e
    iput-boolean v2, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@10
    .line 1005
    :cond_10
    iget-object v0, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@12
    neg-float v1, p3

    #@13
    float-to-int v1, v1

    #@14
    invoke-virtual {v0, v1}, Landroid/widget/Gallery$FlingRunnable;->startUsingVelocity(I)V

    #@17
    .line 1007
    return v2
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 6
    .parameter "gainFocus"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 1365
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsSpinner;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 1372
    if-eqz p1, :cond_14

    #@5
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 1373
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@b
    invoke-virtual {v0, p2}, Landroid/view/View;->requestFocus(I)Z

    #@e
    .line 1374
    iget-object v0, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@10
    const/4 v1, 0x1

    #@11
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    #@14
    .line 1377
    :cond_14
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1381
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1382
    const-class v0, Landroid/widget/Gallery;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1383
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1387
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@4
    .line 1388
    const-class v1, Landroid/widget/Gallery;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@d
    .line 1389
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@f
    if-le v1, v0, :cond_3f

    #@11
    :goto_11
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@14
    .line 1390
    invoke-virtual {p0}, Landroid/widget/Gallery;->isEnabled()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_3e

    #@1a
    .line 1391
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1c
    if-lez v0, :cond_2b

    #@1e
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@20
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@22
    add-int/lit8 v1, v1, -0x1

    #@24
    if-ge v0, v1, :cond_2b

    #@26
    .line 1392
    const/16 v0, 0x1000

    #@28
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@2b
    .line 1394
    :cond_2b
    invoke-virtual {p0}, Landroid/widget/Gallery;->isEnabled()Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_3e

    #@31
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@33
    if-lez v0, :cond_3e

    #@35
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@37
    if-lez v0, :cond_3e

    #@39
    .line 1395
    const/16 v0, 0x2000

    #@3b
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@3e
    .line 1398
    :cond_3e
    return-void

    #@3f
    .line 1389
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_11
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1206
    sparse-switch p1, :sswitch_data_22

    #@4
    .line 1226
    :cond_4
    :goto_4
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSpinner;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 1209
    :sswitch_9
    invoke-virtual {p0}, Landroid/widget/Gallery;->movePrevious()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_4

    #@f
    .line 1210
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->playSoundEffect(I)V

    #@12
    goto :goto_8

    #@13
    .line 1215
    :sswitch_13
    invoke-virtual {p0}, Landroid/widget/Gallery;->moveNext()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_4

    #@19
    .line 1216
    const/4 v1, 0x3

    #@1a
    invoke-virtual {p0, v1}, Landroid/widget/Gallery;->playSoundEffect(I)V

    #@1d
    goto :goto_8

    #@1e
    .line 1222
    :sswitch_1e
    iput-boolean v0, p0, Landroid/widget/Gallery;->mReceivedInvokeKeyDown:Z

    #@20
    goto :goto_4

    #@21
    .line 1206
    nop

    #@22
    :sswitch_data_22
    .sparse-switch
        0x15 -> :sswitch_9
        0x16 -> :sswitch_13
        0x17 -> :sswitch_1e
        0x42 -> :sswitch_1e
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1231
    sparse-switch p1, :sswitch_data_3e

    #@3
    .line 1259
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSpinner;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 1235
    :sswitch_8
    iget-boolean v1, p0, Landroid/widget/Gallery;->mReceivedInvokeKeyDown:Z

    #@a
    if-eqz v1, :cond_39

    #@c
    .line 1236
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@e
    if-lez v1, :cond_39

    #@10
    .line 1238
    iget-object v1, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@12
    invoke-direct {p0, v1}, Landroid/widget/Gallery;->dispatchPress(Landroid/view/View;)V

    #@15
    .line 1239
    new-instance v1, Landroid/widget/Gallery$2;

    #@17
    invoke-direct {v1, p0}, Landroid/widget/Gallery$2;-><init>(Landroid/widget/Gallery;)V

    #@1a
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    #@1d
    move-result v2

    #@1e
    int-to-long v2, v2

    #@1f
    invoke-virtual {p0, v1, v2, v3}, Landroid/widget/Gallery;->postDelayed(Ljava/lang/Runnable;J)Z

    #@22
    .line 1246
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@24
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@26
    sub-int v0, v1, v2

    #@28
    .line 1247
    .local v0, selectedIndex:I
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@2b
    move-result-object v1

    #@2c
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2e
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@30
    iget v4, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@32
    invoke-interface {v3, v4}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    #@35
    move-result-wide v3

    #@36
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/widget/Gallery;->performItemClick(Landroid/view/View;IJ)Z

    #@39
    .line 1253
    .end local v0           #selectedIndex:I
    :cond_39
    const/4 v1, 0x0

    #@3a
    iput-boolean v1, p0, Landroid/widget/Gallery;->mReceivedInvokeKeyDown:Z

    #@3c
    .line 1255
    const/4 v1, 0x1

    #@3d
    goto :goto_7

    #@3e
    .line 1231
    :sswitch_data_3e
    .sparse-switch
        0x17 -> :sswitch_8
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 350
    invoke-super/range {p0 .. p5}, Landroid/widget/AbsSpinner;->onLayout(ZIIII)V

    #@4
    .line 356
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@7
    .line 357
    invoke-virtual {p0, v1, v1}, Landroid/widget/Gallery;->layout(IZ)V

    #@a
    .line 358
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@c
    .line 359
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "e"

    #@0
    .prologue
    .line 1092
    iget v2, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@2
    if-gez v2, :cond_5

    #@4
    .line 1099
    :goto_4
    return-void

    #@5
    .line 1096
    :cond_5
    const/4 v2, 0x0

    #@6
    invoke-virtual {p0, v2}, Landroid/widget/Gallery;->performHapticFeedback(I)Z

    #@9
    .line 1097
    iget v2, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@b
    invoke-virtual {p0, v2}, Landroid/widget/Gallery;->getItemIdAtPosition(I)J

    #@e
    move-result-wide v0

    #@f
    .line 1098
    .local v0, id:J
    iget-object v2, p0, Landroid/widget/Gallery;->mDownTouchView:Landroid/view/View;

    #@11
    iget v3, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@13
    invoke-direct {p0, v2, v3, v0, v1}, Landroid/widget/Gallery;->dispatchLongPress(Landroid/view/View;IJ)Z

    #@16
    goto :goto_4
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 10
    .parameter "e1"
    .parameter "e2"
    .parameter "distanceX"
    .parameter "distanceY"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1024
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@4
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@7
    .line 1028
    iget-boolean v0, p0, Landroid/widget/Gallery;->mShouldCallbackDuringFling:Z

    #@9
    if-nez v0, :cond_25

    #@b
    .line 1029
    iget-boolean v0, p0, Landroid/widget/Gallery;->mIsFirstScroll:Z

    #@d
    if-eqz v0, :cond_1c

    #@f
    .line 1035
    iget-boolean v0, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@11
    if-nez v0, :cond_15

    #@13
    iput-boolean v3, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@15
    .line 1036
    :cond_15
    iget-object v0, p0, Landroid/widget/Gallery;->mDisableSuppressSelectionChangedRunnable:Ljava/lang/Runnable;

    #@17
    const-wide/16 v1, 0xfa

    #@19
    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/Gallery;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1c
    .line 1043
    :cond_1c
    :goto_1c
    float-to-int v0, p3

    #@1d
    mul-int/lit8 v0, v0, -0x1

    #@1f
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->trackMotionScroll(I)V

    #@22
    .line 1045
    iput-boolean v4, p0, Landroid/widget/Gallery;->mIsFirstScroll:Z

    #@24
    .line 1046
    return v3

    #@25
    .line 1039
    :cond_25
    iget-boolean v0, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@27
    if-eqz v0, :cond_1c

    #@29
    iput-boolean v4, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@2b
    goto :goto_1c
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    #@0
    .prologue
    .line 1105
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "e"

    #@0
    .prologue
    .line 974
    iget v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@2
    if-ltz v0, :cond_27

    #@4
    .line 977
    iget v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@6
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-direct {p0, v0}, Landroid/widget/Gallery;->scrollToChild(I)Z

    #@c
    .line 980
    iget-boolean v0, p0, Landroid/widget/Gallery;->mShouldCallbackOnUnselectedItemClick:Z

    #@e
    if-nez v0, :cond_16

    #@10
    iget v0, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@12
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@14
    if-ne v0, v1, :cond_25

    #@16
    .line 981
    :cond_16
    iget-object v0, p0, Landroid/widget/Gallery;->mDownTouchView:Landroid/view/View;

    #@18
    iget v1, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@1a
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@1c
    iget v3, p0, Landroid/widget/Gallery;->mDownTouchPosition:I

    #@1e
    invoke-interface {v2, v3}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    #@21
    move-result-wide v2

    #@22
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/widget/Gallery;->performItemClick(Landroid/view/View;IJ)Z

    #@25
    .line 985
    :cond_25
    const/4 v0, 0x1

    #@26
    .line 988
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_26
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 958
    iget-object v2, p0, Landroid/widget/Gallery;->mGestureDetector:Landroid/view/GestureDetector;

    #@2
    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v1

    #@6
    .line 960
    .local v1, retValue:Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@9
    move-result v0

    #@a
    .line 961
    .local v0, action:I
    const/4 v2, 0x1

    #@b
    if-ne v0, v2, :cond_11

    #@d
    .line 963
    invoke-virtual {p0}, Landroid/widget/Gallery;->onUp()V

    #@10
    .line 968
    :cond_10
    :goto_10
    return v1

    #@11
    .line 964
    :cond_11
    const/4 v2, 0x3

    #@12
    if-ne v0, v2, :cond_10

    #@14
    .line 965
    invoke-virtual {p0}, Landroid/widget/Gallery;->onCancel()V

    #@17
    goto :goto_10
.end method

.method onUp()V
    .registers 2

    #@0
    .prologue
    .line 1075
    iget-object v0, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@2
    invoke-static {v0}, Landroid/widget/Gallery$FlingRunnable;->access$200(Landroid/widget/Gallery$FlingRunnable;)Landroid/widget/Scroller;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 1076
    invoke-direct {p0}, Landroid/widget/Gallery;->scrollIntoSlots()V

    #@f
    .line 1079
    :cond_f
    invoke-direct {p0}, Landroid/widget/Gallery;->dispatchUnpress()V

    #@12
    .line 1080
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 7
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1402
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSpinner;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_9

    #@7
    .line 1403
    const/4 v1, 0x1

    #@8
    .line 1419
    :cond_8
    :goto_8
    return v1

    #@9
    .line 1405
    :cond_9
    sparse-switch p1, :sswitch_data_48

    #@c
    goto :goto_8

    #@d
    .line 1407
    :sswitch_d
    invoke-virtual {p0}, Landroid/widget/Gallery;->isEnabled()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_8

    #@13
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@15
    if-lez v2, :cond_8

    #@17
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@19
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1b
    add-int/lit8 v3, v3, -0x1

    #@1d
    if-ge v2, v3, :cond_8

    #@1f
    .line 1408
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@21
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@23
    sub-int v0, v1, v2

    #@25
    .line 1409
    .local v0, currentChildIndex:I
    add-int/lit8 v1, v0, 0x1

    #@27
    invoke-direct {p0, v1}, Landroid/widget/Gallery;->scrollToChild(I)Z

    #@2a
    move-result v1

    #@2b
    goto :goto_8

    #@2c
    .line 1413
    .end local v0           #currentChildIndex:I
    :sswitch_2c
    invoke-virtual {p0}, Landroid/widget/Gallery;->isEnabled()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_8

    #@32
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@34
    if-lez v2, :cond_8

    #@36
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@38
    if-lez v2, :cond_8

    #@3a
    .line 1414
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3c
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3e
    sub-int v0, v1, v2

    #@40
    .line 1415
    .restart local v0       #currentChildIndex:I
    add-int/lit8 v1, v0, -0x1

    #@42
    invoke-direct {p0, v1}, Landroid/widget/Gallery;->scrollToChild(I)Z

    #@45
    move-result v1

    #@46
    goto :goto_8

    #@47
    .line 1405
    nop

    #@48
    :sswitch_data_48
    .sparse-switch
        0x1000 -> :sswitch_d
        0x2000 -> :sswitch_2c
    .end sparse-switch
.end method

.method selectionChanged()V
    .registers 2

    #@0
    .prologue
    .line 560
    iget-boolean v0, p0, Landroid/widget/Gallery;->mSuppressSelectionChanged:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 561
    invoke-super {p0}, Landroid/widget/AbsSpinner;->selectionChanged()V

    #@7
    .line 563
    :cond_7
    return-void
.end method

.method public setAnimationDuration(I)V
    .registers 2
    .parameter "animationDurationMillis"

    #@0
    .prologue
    .line 272
    iput p1, p0, Landroid/widget/Gallery;->mAnimationDuration:I

    #@2
    .line 273
    return-void
.end method

.method public setCallbackDuringFling(Z)V
    .registers 2
    .parameter "shouldCallback"

    #@0
    .prologue
    .line 246
    iput-boolean p1, p0, Landroid/widget/Gallery;->mShouldCallbackDuringFling:Z

    #@2
    .line 247
    return-void
.end method

.method public setCallbackOnUnselectedItemClick(Z)V
    .registers 2
    .parameter "shouldCallback"

    #@0
    .prologue
    .line 259
    iput-boolean p1, p0, Landroid/widget/Gallery;->mShouldCallbackOnUnselectedItemClick:Z

    #@2
    .line 260
    return-void
.end method

.method public setGravity(I)V
    .registers 3
    .parameter "gravity"

    #@0
    .prologue
    .line 1338
    iget v0, p0, Landroid/widget/Gallery;->mGravity:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1339
    iput p1, p0, Landroid/widget/Gallery;->mGravity:I

    #@6
    .line 1340
    invoke-virtual {p0}, Landroid/widget/Gallery;->requestLayout()V

    #@9
    .line 1342
    :cond_9
    return-void
.end method

.method setSelectedPositionInt(I)V
    .registers 2
    .parameter "position"

    #@0
    .prologue
    .line 1294
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setSelectedPositionInt(I)V

    #@3
    .line 1297
    invoke-direct {p0}, Landroid/widget/Gallery;->updateSelectedItemMetadata()V

    #@6
    .line 1298
    return-void
.end method

.method public setSpacing(I)V
    .registers 2
    .parameter "spacing"

    #@0
    .prologue
    .line 283
    iput p1, p0, Landroid/widget/Gallery;->mSpacing:I

    #@2
    .line 284
    return-void
.end method

.method public setUnselectedAlpha(F)V
    .registers 2
    .parameter "unselectedAlpha"

    #@0
    .prologue
    .line 294
    iput p1, p0, Landroid/widget/Gallery;->mUnselectedAlpha:F

    #@2
    .line 295
    return-void
.end method

.method public showContextMenu()Z
    .registers 6

    #@0
    .prologue
    .line 1165
    invoke-virtual {p0}, Landroid/widget/Gallery;->isPressed()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_1d

    #@6
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8
    if-ltz v2, :cond_1d

    #@a
    .line 1166
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@c
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@e
    sub-int v0, v2, v3

    #@10
    .line 1167
    .local v0, index:I
    invoke-virtual {p0, v0}, Landroid/widget/Gallery;->getChildAt(I)Landroid/view/View;

    #@13
    move-result-object v1

    #@14
    .line 1168
    .local v1, v:Landroid/view/View;
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@16
    iget-wide v3, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@18
    invoke-direct {p0, v1, v2, v3, v4}, Landroid/widget/Gallery;->dispatchLongPress(Landroid/view/View;IJ)Z

    #@1b
    move-result v2

    #@1c
    .line 1171
    .end local v0           #index:I
    .end local v1           #v:Landroid/view/View;
    :goto_1c
    return v2

    #@1d
    :cond_1d
    const/4 v2, 0x0

    #@1e
    goto :goto_1c
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 6
    .parameter "originalView"

    #@0
    .prologue
    .line 1153
    invoke-virtual {p0, p1}, Landroid/widget/Gallery;->getPositionForView(Landroid/view/View;)I

    #@3
    move-result v2

    #@4
    .line 1154
    .local v2, longPressPosition:I
    if-gez v2, :cond_8

    #@6
    .line 1155
    const/4 v3, 0x0

    #@7
    .line 1159
    :goto_7
    return v3

    #@8
    .line 1158
    :cond_8
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@a
    invoke-interface {v3, v2}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    #@d
    move-result-wide v0

    #@e
    .line 1159
    .local v0, longPressId:J
    invoke-direct {p0, p1, v2, v0, v1}, Landroid/widget/Gallery;->dispatchLongPress(Landroid/view/View;IJ)Z

    #@11
    move-result v3

    #@12
    goto :goto_7
.end method

.method trackMotionScroll(I)V
    .registers 10
    .parameter "deltaX"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 374
    invoke-virtual {p0}, Landroid/widget/Gallery;->getChildCount()I

    #@4
    move-result v7

    #@5
    if-nez v7, :cond_8

    #@7
    .line 415
    :goto_7
    return-void

    #@8
    .line 378
    :cond_8
    if-gez p1, :cond_4c

    #@a
    const/4 v5, 0x1

    #@b
    .line 380
    .local v5, toLeft:Z
    :goto_b
    invoke-virtual {p0, v5, p1}, Landroid/widget/Gallery;->getLimitedMotionScrollAmount(ZI)I

    #@e
    move-result v3

    #@f
    .line 381
    .local v3, limitedDeltaX:I
    if-eq v3, p1, :cond_19

    #@11
    .line 383
    iget-object v7, p0, Landroid/widget/Gallery;->mFlingRunnable:Landroid/widget/Gallery$FlingRunnable;

    #@13
    invoke-static {v7, v6}, Landroid/widget/Gallery$FlingRunnable;->access$100(Landroid/widget/Gallery$FlingRunnable;Z)V

    #@16
    .line 384
    invoke-direct {p0}, Landroid/widget/Gallery;->onFinishedMovement()V

    #@19
    .line 387
    :cond_19
    invoke-direct {p0, v3}, Landroid/widget/Gallery;->offsetChildrenLeftAndRight(I)V

    #@1c
    .line 389
    invoke-direct {p0, v5}, Landroid/widget/Gallery;->detachOffScreenChildren(Z)V

    #@1f
    .line 391
    if-eqz v5, :cond_4e

    #@21
    .line 393
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryRight()V

    #@24
    .line 400
    :goto_24
    iget-object v7, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@26
    invoke-virtual {v7}, Landroid/widget/AbsSpinner$RecycleBin;->clear()V

    #@29
    .line 402
    invoke-direct {p0}, Landroid/widget/Gallery;->setSelectionToCenterChild()V

    #@2c
    .line 404
    iget-object v4, p0, Landroid/widget/Gallery;->mSelectedChild:Landroid/view/View;

    #@2e
    .line 405
    .local v4, selChild:Landroid/view/View;
    if-eqz v4, :cond_45

    #@30
    .line 406
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    #@33
    move-result v1

    #@34
    .line 407
    .local v1, childLeft:I
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    #@37
    move-result v7

    #@38
    div-int/lit8 v0, v7, 0x2

    #@3a
    .line 408
    .local v0, childCenter:I
    invoke-virtual {p0}, Landroid/widget/Gallery;->getWidth()I

    #@3d
    move-result v7

    #@3e
    div-int/lit8 v2, v7, 0x2

    #@40
    .line 409
    .local v2, galleryCenter:I
    add-int v7, v1, v0

    #@42
    sub-int/2addr v7, v2

    #@43
    iput v7, p0, Landroid/widget/Gallery;->mSelectedCenterOffset:I

    #@45
    .line 412
    .end local v0           #childCenter:I
    .end local v1           #childLeft:I
    .end local v2           #galleryCenter:I
    :cond_45
    invoke-virtual {p0, v6, v6, v6, v6}, Landroid/widget/Gallery;->onScrollChanged(IIII)V

    #@48
    .line 414
    invoke-virtual {p0}, Landroid/widget/Gallery;->invalidate()V

    #@4b
    goto :goto_7

    #@4c
    .end local v3           #limitedDeltaX:I
    .end local v4           #selChild:Landroid/view/View;
    .end local v5           #toLeft:Z
    :cond_4c
    move v5, v6

    #@4d
    .line 378
    goto :goto_b

    #@4e
    .line 396
    .restart local v3       #limitedDeltaX:I
    .restart local v5       #toLeft:Z
    :cond_4e
    invoke-direct {p0}, Landroid/widget/Gallery;->fillToGalleryLeft()V

    #@51
    goto :goto_24
.end method
