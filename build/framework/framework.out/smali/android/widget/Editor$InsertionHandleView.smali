.class Landroid/widget/Editor$InsertionHandleView;
.super Landroid/widget/Editor$HandleView;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertionHandleView"
.end annotation


# static fields
.field private static final DELAY_BEFORE_HANDLE_FADES_OUT:I = 0xfa0

.field private static final RECENT_CUT_COPY_DURATION:I = 0x3a98


# instance fields
.field private mDownPositionX:F

.field private mDownPositionY:F

.field private mHider:Ljava/lang/Runnable;

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter
    .parameter "drawable"

    #@0
    .prologue
    .line 4255
    iput-object p1, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@2
    .line 4256
    invoke-direct {p0, p1, p2, p2}, Landroid/widget/Editor$HandleView;-><init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 4257
    return-void
.end method

.method private hideAfterDelay()V
    .registers 5

    #@0
    .prologue
    .line 4278
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->mHider:Ljava/lang/Runnable;

    #@2
    if-nez v0, :cond_19

    #@4
    .line 4279
    new-instance v0, Landroid/widget/Editor$InsertionHandleView$1;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/Editor$InsertionHandleView$1;-><init>(Landroid/widget/Editor$InsertionHandleView;)V

    #@9
    iput-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->mHider:Ljava/lang/Runnable;

    #@b
    .line 4287
    :goto_b
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@d
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@10
    move-result-object v0

    #@11
    iget-object v1, p0, Landroid/widget/Editor$InsertionHandleView;->mHider:Ljava/lang/Runnable;

    #@13
    const-wide/16 v2, 0xfa0

    #@15
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@18
    .line 4288
    return-void

    #@19
    .line 4285
    :cond_19
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->removeHiderCallback()V

    #@1c
    goto :goto_b
.end method

.method private removeHiderCallback()V
    .registers 3

    #@0
    .prologue
    .line 4291
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->mHider:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 4292
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@6
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/widget/Editor$InsertionHandleView;->mHider:Ljava/lang/Runnable;

    #@c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@f
    .line 4294
    :cond_f
    return-void
.end method


# virtual methods
.method public getCurrentCursorOffset()I
    .registers 2

    #@0
    .prologue
    .line 4346
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected getHotspotX(Landroid/graphics/drawable/Drawable;Z)I
    .registers 4
    .parameter "drawable"
    .parameter "isRtlRun"

    #@0
    .prologue
    .line 4298
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@3
    move-result v0

    #@4
    div-int/lit8 v0, v0, 0x2

    #@6
    return v0
.end method

.method public onDetached()V
    .registers 1

    #@0
    .prologue
    .line 4367
    invoke-super {p0}, Landroid/widget/Editor$HandleView;->onDetached()V

    #@3
    .line 4368
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->removeHiderCallback()V

    #@6
    .line 4369
    return-void
.end method

.method onHandleMoved()V
    .registers 1

    #@0
    .prologue
    .line 4361
    invoke-super {p0}, Landroid/widget/Editor$HandleView;->onHandleMoved()V

    #@3
    .line 4362
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->removeHiderCallback()V

    #@6
    .line 4363
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "ev"

    #@0
    .prologue
    .line 4303
    invoke-super {p0, p1}, Landroid/widget/Editor$HandleView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v3

    #@4
    .line 4305
    .local v3, result:Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@7
    move-result v6

    #@8
    packed-switch v6, :pswitch_data_6c

    #@b
    .line 4341
    :goto_b
    :pswitch_b
    return v3

    #@c
    .line 4307
    :pswitch_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    #@f
    move-result v6

    #@10
    iput v6, p0, Landroid/widget/Editor$InsertionHandleView;->mDownPositionX:F

    #@12
    .line 4308
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    #@15
    move-result v6

    #@16
    iput v6, p0, Landroid/widget/Editor$InsertionHandleView;->mDownPositionY:F

    #@18
    goto :goto_b

    #@19
    .line 4312
    :pswitch_19
    invoke-virtual {p0}, Landroid/widget/Editor$InsertionHandleView;->offsetHasBeenChanged()Z

    #@1c
    move-result v6

    #@1d
    if-nez v6, :cond_5f

    #@1f
    .line 4313
    iget v6, p0, Landroid/widget/Editor$InsertionHandleView;->mDownPositionX:F

    #@21
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    #@24
    move-result v7

    #@25
    sub-float v0, v6, v7

    #@27
    .line 4314
    .local v0, deltaX:F
    iget v6, p0, Landroid/widget/Editor$InsertionHandleView;->mDownPositionY:F

    #@29
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    #@2c
    move-result v7

    #@2d
    sub-float v1, v6, v7

    #@2f
    .line 4315
    .local v1, deltaY:F
    mul-float v6, v0, v0

    #@31
    mul-float v7, v1, v1

    #@33
    add-float v2, v6, v7

    #@35
    .line 4317
    .local v2, distanceSquared:F
    iget-object v6, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@37
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3e
    move-result-object v6

    #@3f
    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@42
    move-result-object v5

    #@43
    .line 4319
    .local v5, viewConfiguration:Landroid/view/ViewConfiguration;
    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@46
    move-result v4

    #@47
    .line 4321
    .local v4, touchSlop:I
    mul-int v6, v4, v4

    #@49
    int-to-float v6, v6

    #@4a
    cmpg-float v6, v2, v6

    #@4c
    if-gez v6, :cond_5f

    #@4e
    .line 4322
    iget-object v6, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@50
    if-eqz v6, :cond_63

    #@52
    iget-object v6, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@54
    invoke-virtual {v6}, Landroid/widget/Editor$ActionPopupWindow;->isShowing()Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_63

    #@5a
    .line 4324
    iget-object v6, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@5c
    invoke-virtual {v6}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@5f
    .line 4330
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v4           #touchSlop:I
    .end local v5           #viewConfiguration:Landroid/view/ViewConfiguration;
    :cond_5f
    :goto_5f
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->hideAfterDelay()V

    #@62
    goto :goto_b

    #@63
    .line 4326
    .restart local v0       #deltaX:F
    .restart local v1       #deltaY:F
    .restart local v2       #distanceSquared:F
    .restart local v4       #touchSlop:I
    .restart local v5       #viewConfiguration:Landroid/view/ViewConfiguration;
    :cond_63
    invoke-virtual {p0}, Landroid/widget/Editor$InsertionHandleView;->showWithActionPopup()V

    #@66
    goto :goto_5f

    #@67
    .line 4334
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v4           #touchSlop:I
    .end local v5           #viewConfiguration:Landroid/view/ViewConfiguration;
    :pswitch_67
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->hideAfterDelay()V

    #@6a
    goto :goto_b

    #@6b
    .line 4305
    nop

    #@6c
    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_19
        :pswitch_b
        :pswitch_67
    .end packed-switch
.end method

.method public show()V
    .registers 7

    #@0
    .prologue
    .line 4261
    invoke-super {p0}, Landroid/widget/Editor$HandleView;->show()V

    #@3
    .line 4263
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v2

    #@7
    sget-wide v4, Landroid/widget/TextView;->LAST_CUT_OR_COPY_TIME:J

    #@9
    sub-long v0, v2, v4

    #@b
    .line 4265
    .local v0, durationSinceCutOrCopy:J
    const-wide/16 v2, 0x3a98

    #@d
    cmp-long v2, v0, v2

    #@f
    if-gez v2, :cond_15

    #@11
    .line 4266
    const/4 v2, 0x0

    #@12
    invoke-virtual {p0, v2}, Landroid/widget/Editor$InsertionHandleView;->showActionPopupWindow(I)V

    #@15
    .line 4269
    :cond_15
    invoke-direct {p0}, Landroid/widget/Editor$InsertionHandleView;->hideAfterDelay()V

    #@18
    .line 4270
    return-void
.end method

.method public showWithActionPopup()V
    .registers 2

    #@0
    .prologue
    .line 4273
    invoke-virtual {p0}, Landroid/widget/Editor$InsertionHandleView;->show()V

    #@3
    .line 4274
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/Editor$InsertionHandleView;->showActionPopupWindow(I)V

    #@7
    .line 4275
    return-void
.end method

.method public updatePosition(FF)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 4356
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x0

    #@b
    invoke-virtual {p0, v0, v1}, Landroid/widget/Editor$InsertionHandleView;->positionAtCursorOffset(IZ)V

    #@e
    .line 4357
    return-void
.end method

.method public updateSelection(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 4351
    iget-object v0, p0, Landroid/widget/Editor$InsertionHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/text/Spannable;

    #@c
    invoke-static {v0, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@f
    .line 4352
    return-void
.end method
