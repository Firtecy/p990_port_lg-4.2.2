.class public Landroid/widget/Editor$UserDictionaryListener;
.super Landroid/os/Handler;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserDictionaryListener"
.end annotation


# instance fields
.field public mOriginalWord:Ljava/lang/String;

.field public mTextView:Landroid/widget/TextView;

.field public mWordEnd:I

.field public mWordStart:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5005
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    return-void
.end method

.method private onUserDictionaryAdded(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "originalWord"
    .parameter "addedWord"

    #@0
    .prologue
    .line 5039
    iget-object v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mOriginalWord:Ljava/lang/String;

    #@2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_e

    #@8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_f

    #@e
    .line 5060
    :cond_e
    :goto_e
    return-void

    #@f
    .line 5042
    :cond_f
    iget v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordStart:I

    #@11
    if-ltz v3, :cond_e

    #@13
    iget v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordEnd:I

    #@15
    iget-object v4, p0, Landroid/widget/Editor$UserDictionaryListener;->mTextView:Landroid/widget/TextView;

    #@17
    invoke-virtual {v4}, Landroid/widget/TextView;->length()I

    #@1a
    move-result v4

    #@1b
    if-ge v3, v4, :cond_e

    #@1d
    .line 5045
    iget-object v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mOriginalWord:Ljava/lang/String;

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_e

    #@25
    .line 5048
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v3

    #@29
    if-nez v3, :cond_e

    #@2b
    .line 5051
    iget-object v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mTextView:Landroid/widget/TextView;

    #@2d
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@30
    move-result-object v1

    #@31
    check-cast v1, Landroid/text/Editable;

    #@33
    .line 5052
    .local v1, editable:Landroid/text/Editable;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    iget v4, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordStart:I

    #@39
    iget v5, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordEnd:I

    #@3b
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    .line 5053
    .local v0, currentWord:Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_e

    #@45
    .line 5056
    iget-object v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mTextView:Landroid/widget/TextView;

    #@47
    iget v4, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordStart:I

    #@49
    iget v5, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordEnd:I

    #@4b
    invoke-virtual {v3, v4, v5, p2}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    #@4e
    .line 5058
    iget v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordStart:I

    #@50
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@53
    move-result v4

    #@54
    add-int v2, v3, v4

    #@56
    .line 5059
    .local v2, newCursorPosition:I
    iget-object v3, p0, Landroid/widget/Editor$UserDictionaryListener;->mTextView:Landroid/widget/TextView;

    #@58
    invoke-virtual {v3, v2, v2}, Landroid/widget/TextView;->setCursorPosition_internal(II)V

    #@5b
    goto :goto_e
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 5021
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v3, :pswitch_data_2a

    #@5
    .line 5034
    :goto_5
    :pswitch_5
    return-void

    #@6
    .line 5024
    :pswitch_6
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    instance-of v3, v3, Landroid/os/Bundle;

    #@a
    if-nez v3, :cond_14

    #@c
    .line 5025
    const-string v3, "Editor"

    #@e
    const-string v4, "Illegal message. Abort handling onUserDictionaryAdded."

    #@10
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    goto :goto_5

    #@14
    .line 5028
    :cond_14
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16
    check-cast v1, Landroid/os/Bundle;

    #@18
    .line 5029
    .local v1, bundle:Landroid/os/Bundle;
    const-string/jumbo v3, "originalWord"

    #@1b
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 5030
    .local v2, originalWord:Ljava/lang/String;
    const-string/jumbo v3, "word"

    #@22
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 5031
    .local v0, addedWord:Ljava/lang/String;
    invoke-direct {p0, v2, v0}, Landroid/widget/Editor$UserDictionaryListener;->onUserDictionaryAdded(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_5

    #@2a
    .line 5021
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public waitForUserDictionaryAdded(Landroid/widget/TextView;Ljava/lang/String;II)V
    .registers 5
    .parameter "tv"
    .parameter "originalWord"
    .parameter "spanStart"
    .parameter "spanEnd"

    #@0
    .prologue
    .line 5013
    iput-object p1, p0, Landroid/widget/Editor$UserDictionaryListener;->mTextView:Landroid/widget/TextView;

    #@2
    .line 5014
    iput-object p2, p0, Landroid/widget/Editor$UserDictionaryListener;->mOriginalWord:Ljava/lang/String;

    #@4
    .line 5015
    iput p3, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordStart:I

    #@6
    .line 5016
    iput p4, p0, Landroid/widget/Editor$UserDictionaryListener;->mWordEnd:I

    #@8
    .line 5017
    return-void
.end method
