.class public Landroid/widget/ExpandableListView;
.super Landroid/widget/ListView;
.source "ExpandableListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ExpandableListView$1;,
        Landroid/widget/ExpandableListView$SavedState;,
        Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;,
        Landroid/widget/ExpandableListView$OnChildClickListener;,
        Landroid/widget/ExpandableListView$OnGroupClickListener;,
        Landroid/widget/ExpandableListView$OnGroupExpandListener;,
        Landroid/widget/ExpandableListView$OnGroupCollapseListener;
    }
.end annotation


# static fields
.field public static final CHILD_INDICATOR_INHERIT:I = -0x1

.field private static final CHILD_LAST_STATE_SET:[I = null

.field private static final EMPTY_STATE_SET:[I = null

.field private static final GROUP_EMPTY_STATE_SET:[I = null

.field private static final GROUP_EXPANDED_EMPTY_STATE_SET:[I = null

.field private static final GROUP_EXPANDED_STATE_SET:[I = null

.field private static final GROUP_STATE_SETS:[[I = null

.field private static final PACKED_POSITION_INT_MASK_CHILD:J = -0x1L

.field private static final PACKED_POSITION_INT_MASK_GROUP:J = 0x7fffffffL

.field private static final PACKED_POSITION_MASK_CHILD:J = 0xffffffffL

.field private static final PACKED_POSITION_MASK_GROUP:J = 0x7fffffff00000000L

.field private static final PACKED_POSITION_MASK_TYPE:J = -0x8000000000000000L

.field private static final PACKED_POSITION_SHIFT_GROUP:J = 0x20L

.field private static final PACKED_POSITION_SHIFT_TYPE:J = 0x3fL

.field public static final PACKED_POSITION_TYPE_CHILD:I = 0x1

.field public static final PACKED_POSITION_TYPE_GROUP:I = 0x0

.field public static final PACKED_POSITION_TYPE_NULL:I = 0x2

.field public static final PACKED_POSITION_VALUE_NULL:J = 0xffffffffL


# instance fields
.field private mAdapter:Landroid/widget/ExpandableListAdapter;

.field private mChildDivider:Landroid/graphics/drawable/Drawable;

.field private mChildIndicator:Landroid/graphics/drawable/Drawable;

.field private mChildIndicatorLeft:I

.field private mChildIndicatorRight:I

.field private mConnector:Landroid/widget/ExpandableListConnector;

.field private mGroupIndicator:Landroid/graphics/drawable/Drawable;

.field private mIndicatorLeft:I

.field private final mIndicatorRect:Landroid/graphics/Rect;

.field private mIndicatorRight:I

.field private mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

.field private mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

.field private mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

.field private mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 161
    new-array v0, v3, [I

    #@5
    sput-object v0, Landroid/widget/ExpandableListView;->EMPTY_STATE_SET:[I

    #@7
    .line 164
    new-array v0, v4, [I

    #@9
    const v1, 0x10100a8

    #@c
    aput v1, v0, v3

    #@e
    sput-object v0, Landroid/widget/ExpandableListView;->GROUP_EXPANDED_STATE_SET:[I

    #@10
    .line 168
    new-array v0, v4, [I

    #@12
    const v1, 0x10100a9

    #@15
    aput v1, v0, v3

    #@17
    sput-object v0, Landroid/widget/ExpandableListView;->GROUP_EMPTY_STATE_SET:[I

    #@19
    .line 172
    new-array v0, v2, [I

    #@1b
    fill-array-data v0, :array_40

    #@1e
    sput-object v0, Landroid/widget/ExpandableListView;->GROUP_EXPANDED_EMPTY_STATE_SET:[I

    #@20
    .line 176
    const/4 v0, 0x4

    #@21
    new-array v0, v0, [[I

    #@23
    sget-object v1, Landroid/widget/ExpandableListView;->EMPTY_STATE_SET:[I

    #@25
    aput-object v1, v0, v3

    #@27
    sget-object v1, Landroid/widget/ExpandableListView;->GROUP_EXPANDED_STATE_SET:[I

    #@29
    aput-object v1, v0, v4

    #@2b
    sget-object v1, Landroid/widget/ExpandableListView;->GROUP_EMPTY_STATE_SET:[I

    #@2d
    aput-object v1, v0, v2

    #@2f
    const/4 v1, 0x3

    #@30
    sget-object v2, Landroid/widget/ExpandableListView;->GROUP_EXPANDED_EMPTY_STATE_SET:[I

    #@32
    aput-object v2, v0, v1

    #@34
    sput-object v0, Landroid/widget/ExpandableListView;->GROUP_STATE_SETS:[[I

    #@36
    .line 184
    new-array v0, v4, [I

    #@38
    const v1, 0x10100a6

    #@3b
    aput v1, v0, v3

    #@3d
    sput-object v0, Landroid/widget/ExpandableListView;->CHILD_LAST_STATE_SET:[I

    #@3f
    return-void

    #@40
    .line 172
    :array_40
    .array-data 0x4
        0xa8t 0x0t 0x1t 0x1t
        0xa9t 0x0t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 194
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 198
    const v0, 0x101006f

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 202
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 191
    new-instance v1, Landroid/graphics/Rect;

    #@7
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v1, p0, Landroid/widget/ExpandableListView;->mIndicatorRect:Landroid/graphics/Rect;

    #@c
    .line 204
    sget-object v1, Lcom/android/internal/R$styleable;->ExpandableListView:[I

    #@e
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@11
    move-result-object v0

    #@12
    .line 208
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@18
    .line 210
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Landroid/widget/ExpandableListView;->mChildIndicator:Landroid/graphics/drawable/Drawable;

    #@1f
    .line 212
    const/4 v1, 0x2

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@23
    move-result v1

    #@24
    iput v1, p0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@26
    .line 214
    const/4 v1, 0x3

    #@27
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@2a
    move-result v1

    #@2b
    iput v1, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@2d
    .line 216
    iget v1, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@2f
    if-nez v1, :cond_40

    #@31
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@33
    if-eqz v1, :cond_40

    #@35
    .line 217
    iget v1, p0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@37
    iget-object v2, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@39
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@3c
    move-result v2

    #@3d
    add-int/2addr v1, v2

    #@3e
    iput v1, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@40
    .line 219
    :cond_40
    const/4 v1, 0x4

    #@41
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@44
    move-result v1

    #@45
    iput v1, p0, Landroid/widget/ExpandableListView;->mChildIndicatorLeft:I

    #@47
    .line 221
    const/4 v1, 0x5

    #@48
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@4b
    move-result v1

    #@4c
    iput v1, p0, Landroid/widget/ExpandableListView;->mChildIndicatorRight:I

    #@4e
    .line 223
    const/4 v1, 0x6

    #@4f
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@52
    move-result-object v1

    #@53
    iput-object v1, p0, Landroid/widget/ExpandableListView;->mChildDivider:Landroid/graphics/drawable/Drawable;

    #@55
    .line 225
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@58
    .line 226
    return-void
.end method

.method private getAbsoluteFlatPosition(I)I
    .registers 3
    .parameter "flatListPosition"

    #@0
    .prologue
    .line 509
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@3
    move-result v0

    #@4
    add-int/2addr v0, p1

    #@5
    return v0
.end method

.method private getChildOrGroupId(Landroid/widget/ExpandableListPosition;)J
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 1012
    iget v0, p1, Landroid/widget/ExpandableListPosition;->type:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_10

    #@5
    .line 1013
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@7
    iget v1, p1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@9
    iget v2, p1, Landroid/widget/ExpandableListPosition;->childPos:I

    #@b
    invoke-interface {v0, v1, v2}, Landroid/widget/ExpandableListAdapter;->getChildId(II)J

    #@e
    move-result-wide v0

    #@f
    .line 1015
    :goto_f
    return-wide v0

    #@10
    :cond_10
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@12
    iget v1, p1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@14
    invoke-interface {v0, v1}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    #@17
    move-result-wide v0

    #@18
    goto :goto_f
.end method

.method private getFlatPositionForConnector(I)I
    .registers 3
    .parameter "flatListPosition"

    #@0
    .prologue
    .line 498
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@3
    move-result v0

    #@4
    sub-int v0, p1, v0

    #@6
    return v0
.end method

.method private getIndicator(Landroid/widget/ExpandableListConnector$PositionMetadata;)Landroid/graphics/drawable/Drawable;
    .registers 11
    .parameter "pos"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 348
    iget-object v7, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@5
    iget v7, v7, Landroid/widget/ExpandableListPosition;->type:I

    #@7
    if-ne v7, v6, :cond_39

    #@9
    .line 349
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@b
    .line 351
    .local v0, indicator:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_34

    #@d
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@10
    move-result v7

    #@11
    if-eqz v7, :cond_34

    #@13
    .line 355
    iget-object v7, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@15
    if-eqz v7, :cond_21

    #@17
    iget-object v7, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@19
    iget v7, v7, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@1b
    iget-object v8, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@1d
    iget v8, v8, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@1f
    if-ne v7, v8, :cond_35

    #@21
    :cond_21
    move v1, v5

    #@22
    .line 358
    .local v1, isEmpty:Z
    :goto_22
    invoke-virtual {p1}, Landroid/widget/ExpandableListConnector$PositionMetadata;->isExpanded()Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_37

    #@28
    :goto_28
    if-eqz v1, :cond_2b

    #@2a
    move v4, v6

    #@2b
    :cond_2b
    or-int v3, v5, v4

    #@2d
    .line 361
    .local v3, stateSetIndex:I
    sget-object v4, Landroid/widget/ExpandableListView;->GROUP_STATE_SETS:[[I

    #@2f
    aget-object v4, v4, v3

    #@31
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@34
    .line 375
    .end local v1           #isEmpty:Z
    .end local v3           #stateSetIndex:I
    :cond_34
    :goto_34
    return-object v0

    #@35
    :cond_35
    move v1, v4

    #@36
    .line 355
    goto :goto_22

    #@37
    .restart local v1       #isEmpty:Z
    :cond_37
    move v5, v4

    #@38
    .line 358
    goto :goto_28

    #@39
    .line 364
    .end local v0           #indicator:Landroid/graphics/drawable/Drawable;
    .end local v1           #isEmpty:Z
    :cond_39
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mChildIndicator:Landroid/graphics/drawable/Drawable;

    #@3b
    .line 366
    .restart local v0       #indicator:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_34

    #@3d
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_34

    #@43
    .line 368
    iget-object v4, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@45
    iget v4, v4, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@47
    iget-object v5, p1, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@49
    iget v5, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@4b
    if-ne v4, v5, :cond_53

    #@4d
    sget-object v2, Landroid/widget/ExpandableListView;->CHILD_LAST_STATE_SET:[I

    #@4f
    .line 371
    .local v2, stateSet:[I
    :goto_4f
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@52
    goto :goto_34

    #@53
    .line 368
    .end local v2           #stateSet:[I
    :cond_53
    sget-object v2, Landroid/widget/ExpandableListView;->EMPTY_STATE_SET:[I

    #@55
    goto :goto_4f
.end method

.method public static getPackedPositionChild(J)I
    .registers 9
    .parameter "packedPosition"

    #@0
    .prologue
    const-wide v5, 0xffffffffL

    #@5
    const-wide/high16 v3, -0x8000

    #@7
    const/4 v0, -0x1

    #@8
    .line 940
    cmp-long v1, p0, v5

    #@a
    if-nez v1, :cond_d

    #@c
    .line 945
    :cond_c
    :goto_c
    return v0

    #@d
    .line 943
    :cond_d
    and-long v1, p0, v3

    #@f
    cmp-long v1, v1, v3

    #@11
    if-nez v1, :cond_c

    #@13
    .line 945
    and-long v0, p0, v5

    #@15
    long-to-int v0, v0

    #@16
    goto :goto_c
.end method

.method public static getPackedPositionForChild(II)J
    .registers 8
    .parameter "groupPosition"
    .parameter "childPosition"

    #@0
    .prologue
    .line 965
    const-wide/high16 v0, -0x8000

    #@2
    int-to-long v2, p0

    #@3
    const-wide/32 v4, 0x7fffffff

    #@6
    and-long/2addr v2, v4

    #@7
    const/16 v4, 0x20

    #@9
    shl-long/2addr v2, v4

    #@a
    or-long/2addr v0, v2

    #@b
    int-to-long v2, p1

    #@c
    const-wide/16 v4, -0x1

    #@e
    and-long/2addr v2, v4

    #@f
    or-long/2addr v0, v2

    #@10
    return-wide v0
.end method

.method public static getPackedPositionForGroup(I)J
    .registers 5
    .parameter "groupPosition"

    #@0
    .prologue
    .line 980
    int-to-long v0, p0

    #@1
    const-wide/32 v2, 0x7fffffff

    #@4
    and-long/2addr v0, v2

    #@5
    const/16 v2, 0x20

    #@7
    shl-long/2addr v0, v2

    #@8
    return-wide v0
.end method

.method public static getPackedPositionGroup(J)I
    .registers 5
    .parameter "packedPosition"

    #@0
    .prologue
    .line 921
    const-wide v0, 0xffffffffL

    #@5
    cmp-long v0, p0, v0

    #@7
    if-nez v0, :cond_b

    #@9
    const/4 v0, -0x1

    #@a
    .line 923
    :goto_a
    return v0

    #@b
    :cond_b
    const-wide v0, 0x7fffffff00000000L

    #@10
    and-long/2addr v0, p0

    #@11
    const/16 v2, 0x20

    #@13
    shr-long/2addr v0, v2

    #@14
    long-to-int v0, v0

    #@15
    goto :goto_a
.end method

.method public static getPackedPositionType(J)I
    .registers 6
    .parameter "packedPosition"

    #@0
    .prologue
    const-wide/high16 v2, -0x8000

    #@2
    .line 901
    const-wide v0, 0xffffffffL

    #@7
    cmp-long v0, p0, v0

    #@9
    if-nez v0, :cond_d

    #@b
    .line 902
    const/4 v0, 0x2

    #@c
    .line 905
    :goto_c
    return v0

    #@d
    :cond_d
    and-long v0, p0, v2

    #@f
    cmp-long v0, v0, v2

    #@11
    if-nez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    goto :goto_c

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_c
.end method

.method private isHeaderOrFooterPosition(I)Z
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 486
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getFooterViewsCount()I

    #@5
    move-result v2

    #@6
    sub-int v0, v1, v2

    #@8
    .line 487
    .local v0, footerViewsStart:I
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@b
    move-result v1

    #@c
    if-lt p1, v1, :cond_10

    #@e
    if-lt p1, v0, :cond_12

    #@10
    :cond_10
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method


# virtual methods
.method public collapseGroup(I)Z
    .registers 4
    .parameter "groupPos"

    #@0
    .prologue
    .line 645
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@2
    invoke-virtual {v1, p1}, Landroid/widget/ExpandableListConnector;->collapseGroup(I)Z

    #@5
    move-result v0

    #@6
    .line 647
    .local v0, retValue:Z
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 648
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

    #@c
    invoke-interface {v1, p1}, Landroid/widget/ExpandableListView$OnGroupCollapseListener;->onGroupCollapse(I)V

    #@f
    .line 651
    :cond_f
    return v0
.end method

.method createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 14
    .parameter "view"
    .parameter "flatListPosition"
    .parameter "id"

    #@0
    .prologue
    .line 986
    invoke-direct {p0, p2}, Landroid/widget/ExpandableListView;->isHeaderOrFooterPosition(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 988
    new-instance v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@8
    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    #@b
    .line 1000
    :goto_b
    return-object v0

    #@c
    .line 991
    :cond_c
    invoke-direct {p0, p2}, Landroid/widget/ExpandableListView;->getFlatPositionForConnector(I)I

    #@f
    move-result v6

    #@10
    .line 992
    .local v6, adjustedPosition:I
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@12
    invoke-virtual {v0, v6}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@15
    move-result-object v7

    #@16
    .line 993
    .local v7, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v8, v7, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@18
    .line 995
    .local v8, pos:Landroid/widget/ExpandableListPosition;
    invoke-direct {p0, v8}, Landroid/widget/ExpandableListView;->getChildOrGroupId(Landroid/widget/ExpandableListPosition;)J

    #@1b
    move-result-wide p3

    #@1c
    .line 996
    invoke-virtual {v8}, Landroid/widget/ExpandableListPosition;->getPackedPosition()J

    #@1f
    move-result-wide v2

    #@20
    .line 998
    .local v2, packedPosition:J
    invoke-virtual {v7}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@23
    .line 1000
    new-instance v0, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    #@25
    move-object v1, p1

    #@26
    move-wide v4, p3

    #@27
    invoke-direct/range {v0 .. v5}, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;-><init>(Landroid/view/View;JJ)V

    #@2a
    goto :goto_b
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 29
    .parameter "canvas"

    #@0
    .prologue
    .line 232
    invoke-super/range {p0 .. p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 235
    move-object/from16 v0, p0

    #@5
    iget-object v0, v0, Landroid/widget/ExpandableListView;->mChildIndicator:Landroid/graphics/drawable/Drawable;

    #@7
    move-object/from16 v22, v0

    #@9
    if-nez v22, :cond_14

    #@b
    move-object/from16 v0, p0

    #@d
    iget-object v0, v0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@f
    move-object/from16 v22, v0

    #@11
    if-nez v22, :cond_14

    #@13
    .line 335
    :cond_13
    :goto_13
    return-void

    #@14
    .line 239
    :cond_14
    const/16 v18, 0x0

    #@16
    .line 240
    .local v18, saveCount:I
    move-object/from16 v0, p0

    #@18
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1a
    move/from16 v22, v0

    #@1c
    and-int/lit8 v22, v22, 0x22

    #@1e
    const/16 v23, 0x22

    #@20
    move/from16 v0, v22

    #@22
    move/from16 v1, v23

    #@24
    if-ne v0, v1, :cond_bb

    #@26
    const/4 v8, 0x1

    #@27
    .line 241
    .local v8, clipToPadding:Z
    :goto_27
    if-eqz v8, :cond_86

    #@29
    .line 242
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@2c
    move-result v18

    #@2d
    .line 243
    move-object/from16 v0, p0

    #@2f
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@31
    move/from16 v19, v0

    #@33
    .line 244
    .local v19, scrollX:I
    move-object/from16 v0, p0

    #@35
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@37
    move/from16 v20, v0

    #@39
    .line 245
    .local v20, scrollY:I
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@3d
    move/from16 v22, v0

    #@3f
    add-int v22, v22, v19

    #@41
    move-object/from16 v0, p0

    #@43
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@45
    move/from16 v23, v0

    #@47
    add-int v23, v23, v20

    #@49
    move-object/from16 v0, p0

    #@4b
    iget v0, v0, Landroid/view/View;->mRight:I

    #@4d
    move/from16 v24, v0

    #@4f
    add-int v24, v24, v19

    #@51
    move-object/from16 v0, p0

    #@53
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@55
    move/from16 v25, v0

    #@57
    sub-int v24, v24, v25

    #@59
    move-object/from16 v0, p0

    #@5b
    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    #@5d
    move/from16 v25, v0

    #@5f
    sub-int v24, v24, v25

    #@61
    move-object/from16 v0, p0

    #@63
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@65
    move/from16 v25, v0

    #@67
    add-int v25, v25, v20

    #@69
    move-object/from16 v0, p0

    #@6b
    iget v0, v0, Landroid/view/View;->mTop:I

    #@6d
    move/from16 v26, v0

    #@6f
    sub-int v25, v25, v26

    #@71
    move-object/from16 v0, p0

    #@73
    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    #@75
    move/from16 v26, v0

    #@77
    sub-int v25, v25, v26

    #@79
    move-object/from16 v0, p1

    #@7b
    move/from16 v1, v22

    #@7d
    move/from16 v2, v23

    #@7f
    move/from16 v3, v24

    #@81
    move/from16 v4, v25

    #@83
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@86
    .line 250
    .end local v19           #scrollX:I
    .end local v20           #scrollY:I
    :cond_86
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@89
    move-result v9

    #@8a
    .line 252
    .local v9, headerViewsCount:I
    move-object/from16 v0, p0

    #@8c
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@8e
    move/from16 v22, v0

    #@90
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ExpandableListView;->getFooterViewsCount()I

    #@93
    move-result v23

    #@94
    sub-int v22, v22, v23

    #@96
    sub-int v22, v22, v9

    #@98
    add-int/lit8 v14, v22, -0x1

    #@9a
    .line 254
    .local v14, lastChildFlPos:I
    move-object/from16 v0, p0

    #@9c
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@9e
    move/from16 v16, v0

    #@a0
    .line 262
    .local v16, myB:I
    const/4 v15, -0x4

    #@a1
    .line 264
    .local v15, lastItemType:I
    move-object/from16 v0, p0

    #@a3
    iget-object v12, v0, Landroid/widget/ExpandableListView;->mIndicatorRect:Landroid/graphics/Rect;

    #@a5
    .line 269
    .local v12, indicatorRect:Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ExpandableListView;->getChildCount()I

    #@a8
    move-result v6

    #@a9
    .line 270
    .local v6, childCount:I
    const/4 v10, 0x0

    #@aa
    .local v10, i:I
    move-object/from16 v0, p0

    #@ac
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@ae
    move/from16 v22, v0

    #@b0
    sub-int v7, v22, v9

    #@b2
    .local v7, childFlPos:I
    :goto_b2
    if-ge v10, v6, :cond_c0

    #@b4
    .line 273
    if-gez v7, :cond_be

    #@b6
    .line 271
    :cond_b6
    :goto_b6
    add-int/lit8 v10, v10, 0x1

    #@b8
    add-int/lit8 v7, v7, 0x1

    #@ba
    goto :goto_b2

    #@bb
    .line 240
    .end local v6           #childCount:I
    .end local v7           #childFlPos:I
    .end local v8           #clipToPadding:Z
    .end local v9           #headerViewsCount:I
    .end local v10           #i:I
    .end local v12           #indicatorRect:Landroid/graphics/Rect;
    .end local v14           #lastChildFlPos:I
    .end local v15           #lastItemType:I
    .end local v16           #myB:I
    :cond_bb
    const/4 v8, 0x0

    #@bc
    goto/16 :goto_27

    #@be
    .line 276
    .restart local v6       #childCount:I
    .restart local v7       #childFlPos:I
    .restart local v8       #clipToPadding:Z
    .restart local v9       #headerViewsCount:I
    .restart local v10       #i:I
    .restart local v12       #indicatorRect:Landroid/graphics/Rect;
    .restart local v14       #lastChildFlPos:I
    .restart local v15       #lastItemType:I
    .restart local v16       #myB:I
    :cond_be
    if-le v7, v14, :cond_cb

    #@c0
    .line 332
    :cond_c0
    if-eqz v8, :cond_13

    #@c2
    .line 333
    move-object/from16 v0, p1

    #@c4
    move/from16 v1, v18

    #@c6
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@c9
    goto/16 :goto_13

    #@cb
    .line 281
    :cond_cb
    move-object/from16 v0, p0

    #@cd
    invoke-virtual {v0, v10}, Landroid/widget/ExpandableListView;->getChildAt(I)Landroid/view/View;

    #@d0
    move-result-object v13

    #@d1
    .line 282
    .local v13, item:Landroid/view/View;
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    #@d4
    move-result v21

    #@d5
    .line 283
    .local v21, t:I
    invoke-virtual {v13}, Landroid/view/View;->getBottom()I

    #@d8
    move-result v5

    #@d9
    .line 286
    .local v5, b:I
    if-ltz v5, :cond_b6

    #@db
    move/from16 v0, v21

    #@dd
    move/from16 v1, v16

    #@df
    if-gt v0, v1, :cond_b6

    #@e1
    .line 289
    move-object/from16 v0, p0

    #@e3
    iget-object v0, v0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@e5
    move-object/from16 v22, v0

    #@e7
    move-object/from16 v0, v22

    #@e9
    invoke-virtual {v0, v7}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@ec
    move-result-object v17

    #@ed
    .line 293
    .local v17, pos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    move-object/from16 v0, v17

    #@ef
    iget-object v0, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@f1
    move-object/from16 v22, v0

    #@f3
    move-object/from16 v0, v22

    #@f5
    iget v0, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@f7
    move/from16 v22, v0

    #@f9
    move/from16 v0, v22

    #@fb
    if-eq v0, v15, :cond_16b

    #@fd
    .line 294
    move-object/from16 v0, v17

    #@ff
    iget-object v0, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@101
    move-object/from16 v22, v0

    #@103
    move-object/from16 v0, v22

    #@105
    iget v0, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@107
    move/from16 v22, v0

    #@109
    const/16 v23, 0x1

    #@10b
    move/from16 v0, v22

    #@10d
    move/from16 v1, v23

    #@10f
    if-ne v0, v1, :cond_1ac

    #@111
    .line 295
    move-object/from16 v0, p0

    #@113
    iget v0, v0, Landroid/widget/ExpandableListView;->mChildIndicatorLeft:I

    #@115
    move/from16 v22, v0

    #@117
    const/16 v23, -0x1

    #@119
    move/from16 v0, v22

    #@11b
    move/from16 v1, v23

    #@11d
    if-ne v0, v1, :cond_19e

    #@11f
    move-object/from16 v0, p0

    #@121
    iget v0, v0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@123
    move/from16 v22, v0

    #@125
    :goto_125
    move/from16 v0, v22

    #@127
    iput v0, v12, Landroid/graphics/Rect;->left:I

    #@129
    .line 297
    move-object/from16 v0, p0

    #@12b
    iget v0, v0, Landroid/widget/ExpandableListView;->mChildIndicatorRight:I

    #@12d
    move/from16 v22, v0

    #@12f
    const/16 v23, -0x1

    #@131
    move/from16 v0, v22

    #@133
    move/from16 v1, v23

    #@135
    if-ne v0, v1, :cond_1a5

    #@137
    move-object/from16 v0, p0

    #@139
    iget v0, v0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@13b
    move/from16 v22, v0

    #@13d
    :goto_13d
    move/from16 v0, v22

    #@13f
    iput v0, v12, Landroid/graphics/Rect;->right:I

    #@141
    .line 304
    :goto_141
    iget v0, v12, Landroid/graphics/Rect;->left:I

    #@143
    move/from16 v22, v0

    #@145
    move-object/from16 v0, p0

    #@147
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@149
    move/from16 v23, v0

    #@14b
    add-int v22, v22, v23

    #@14d
    move/from16 v0, v22

    #@14f
    iput v0, v12, Landroid/graphics/Rect;->left:I

    #@151
    .line 305
    iget v0, v12, Landroid/graphics/Rect;->right:I

    #@153
    move/from16 v22, v0

    #@155
    move-object/from16 v0, p0

    #@157
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@159
    move/from16 v23, v0

    #@15b
    add-int v22, v22, v23

    #@15d
    move/from16 v0, v22

    #@15f
    iput v0, v12, Landroid/graphics/Rect;->right:I

    #@161
    .line 307
    move-object/from16 v0, v17

    #@163
    iget-object v0, v0, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@165
    move-object/from16 v22, v0

    #@167
    move-object/from16 v0, v22

    #@169
    iget v15, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@16b
    .line 310
    :cond_16b
    iget v0, v12, Landroid/graphics/Rect;->left:I

    #@16d
    move/from16 v22, v0

    #@16f
    iget v0, v12, Landroid/graphics/Rect;->right:I

    #@171
    move/from16 v23, v0

    #@173
    move/from16 v0, v22

    #@175
    move/from16 v1, v23

    #@177
    if-eq v0, v1, :cond_199

    #@179
    .line 312
    move-object/from16 v0, p0

    #@17b
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@17d
    move/from16 v22, v0

    #@17f
    if-eqz v22, :cond_1c1

    #@181
    .line 314
    move/from16 v0, v21

    #@183
    iput v0, v12, Landroid/graphics/Rect;->top:I

    #@185
    .line 315
    iput v5, v12, Landroid/graphics/Rect;->bottom:I

    #@187
    .line 322
    :goto_187
    move-object/from16 v0, p0

    #@189
    move-object/from16 v1, v17

    #@18b
    invoke-direct {v0, v1}, Landroid/widget/ExpandableListView;->getIndicator(Landroid/widget/ExpandableListConnector$PositionMetadata;)Landroid/graphics/drawable/Drawable;

    #@18e
    move-result-object v11

    #@18f
    .line 323
    .local v11, indicator:Landroid/graphics/drawable/Drawable;
    if-eqz v11, :cond_199

    #@191
    .line 325
    invoke-virtual {v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@194
    .line 326
    move-object/from16 v0, p1

    #@196
    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@199
    .line 329
    .end local v11           #indicator:Landroid/graphics/drawable/Drawable;
    :cond_199
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@19c
    goto/16 :goto_b6

    #@19e
    .line 295
    :cond_19e
    move-object/from16 v0, p0

    #@1a0
    iget v0, v0, Landroid/widget/ExpandableListView;->mChildIndicatorLeft:I

    #@1a2
    move/from16 v22, v0

    #@1a4
    goto :goto_125

    #@1a5
    .line 297
    :cond_1a5
    move-object/from16 v0, p0

    #@1a7
    iget v0, v0, Landroid/widget/ExpandableListView;->mChildIndicatorRight:I

    #@1a9
    move/from16 v22, v0

    #@1ab
    goto :goto_13d

    #@1ac
    .line 300
    :cond_1ac
    move-object/from16 v0, p0

    #@1ae
    iget v0, v0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@1b0
    move/from16 v22, v0

    #@1b2
    move/from16 v0, v22

    #@1b4
    iput v0, v12, Landroid/graphics/Rect;->left:I

    #@1b6
    .line 301
    move-object/from16 v0, p0

    #@1b8
    iget v0, v0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@1ba
    move/from16 v22, v0

    #@1bc
    move/from16 v0, v22

    #@1be
    iput v0, v12, Landroid/graphics/Rect;->right:I

    #@1c0
    goto :goto_141

    #@1c1
    .line 317
    :cond_1c1
    move/from16 v0, v21

    #@1c3
    iput v0, v12, Landroid/graphics/Rect;->top:I

    #@1c5
    .line 318
    iput v5, v12, Landroid/graphics/Rect;->bottom:I

    #@1c7
    goto :goto_187
.end method

.method drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .registers 10
    .parameter "canvas"
    .parameter "bounds"
    .parameter "childIndex"

    #@0
    .prologue
    .line 391
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    add-int v2, p3, v4

    #@4
    .line 395
    .local v2, flatListPosition:I
    if-ltz v2, :cond_36

    #@6
    .line 396
    invoke-direct {p0, v2}, Landroid/widget/ExpandableListView;->getFlatPositionForConnector(I)I

    #@9
    move-result v0

    #@a
    .line 397
    .local v0, adjustedPosition:I
    iget-object v4, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@c
    invoke-virtual {v4, v0}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@f
    move-result-object v3

    #@10
    .line 399
    .local v3, pos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v4, v3, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@12
    iget v4, v4, Landroid/widget/ExpandableListPosition;->type:I

    #@14
    const/4 v5, 0x1

    #@15
    if-eq v4, v5, :cond_27

    #@17
    invoke-virtual {v3}, Landroid/widget/ExpandableListConnector$PositionMetadata;->isExpanded()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_33

    #@1d
    iget-object v4, v3, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@1f
    iget v4, v4, Landroid/widget/ExpandableListConnector$GroupMetadata;->lastChildFlPos:I

    #@21
    iget-object v5, v3, Landroid/widget/ExpandableListConnector$PositionMetadata;->groupMetadata:Landroid/widget/ExpandableListConnector$GroupMetadata;

    #@23
    iget v5, v5, Landroid/widget/ExpandableListConnector$GroupMetadata;->flPos:I

    #@25
    if-eq v4, v5, :cond_33

    #@27
    .line 402
    :cond_27
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mChildDivider:Landroid/graphics/drawable/Drawable;

    #@29
    .line 403
    .local v1, divider:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@2c
    .line 404
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@2f
    .line 405
    invoke-virtual {v3}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@32
    .line 413
    .end local v0           #adjustedPosition:I
    .end local v1           #divider:Landroid/graphics/drawable/Drawable;
    .end local v3           #pos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    :goto_32
    return-void

    #@33
    .line 408
    .restart local v0       #adjustedPosition:I
    .restart local v3       #pos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    :cond_33
    invoke-virtual {v3}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@36
    .line 412
    .end local v0           #adjustedPosition:I
    .end local v3           #pos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    :cond_36
    invoke-super {p0, p1, p2, v2}, Landroid/widget/ListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    #@39
    goto :goto_32
.end method

.method public expandGroup(I)Z
    .registers 3
    .parameter "groupPos"

    #@0
    .prologue
    .line 603
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/widget/ExpandableListView;->expandGroup(IZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public expandGroup(IZ)Z
    .registers 10
    .parameter "groupPos"
    .parameter "animate"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 615
    const/4 v5, 0x2

    #@2
    invoke-static {v5, p1, v6, v6}, Landroid/widget/ExpandableListPosition;->obtain(IIII)Landroid/widget/ExpandableListPosition;

    #@5
    move-result-object v0

    #@6
    .line 617
    .local v0, elGroupPos:Landroid/widget/ExpandableListPosition;
    iget-object v5, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@8
    invoke-virtual {v5, v0}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@b
    move-result-object v2

    #@c
    .line 618
    .local v2, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    invoke-virtual {v0}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@f
    .line 619
    iget-object v5, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@11
    invoke-virtual {v5, v2}, Landroid/widget/ExpandableListConnector;->expandGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z

    #@14
    move-result v3

    #@15
    .line 621
    .local v3, retValue:Z
    iget-object v5, p0, Landroid/widget/ExpandableListView;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    #@17
    if-eqz v5, :cond_1e

    #@19
    .line 622
    iget-object v5, p0, Landroid/widget/ExpandableListView;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    #@1b
    invoke-interface {v5, p1}, Landroid/widget/ExpandableListView$OnGroupExpandListener;->onGroupExpand(I)V

    #@1e
    .line 625
    :cond_1e
    if-eqz p2, :cond_34

    #@20
    .line 626
    iget-object v5, v2, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@22
    iget v1, v5, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@24
    .line 628
    .local v1, groupFlatPos:I
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@27
    move-result v5

    #@28
    add-int v4, v1, v5

    #@2a
    .line 629
    .local v4, shiftedGroupPosition:I
    iget-object v5, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@2c
    invoke-interface {v5, p1}, Landroid/widget/ExpandableListAdapter;->getChildrenCount(I)I

    #@2f
    move-result v5

    #@30
    add-int/2addr v5, v4

    #@31
    invoke-virtual {p0, v5, v4}, Landroid/widget/ExpandableListView;->smoothScrollToPosition(II)V

    #@34
    .line 632
    .end local v1           #groupFlatPos:I
    .end local v4           #shiftedGroupPosition:I
    :cond_34
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@37
    .line 634
    return v3
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .registers 2

    #@0
    .prologue
    .line 80
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getAdapter()Landroid/widget/ListAdapter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 438
    invoke-super {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;
    .registers 2

    #@0
    .prologue
    .line 478
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    return-object v0
.end method

.method public getExpandableListPosition(I)J
    .registers 7
    .parameter "flatListPosition"

    #@0
    .prologue
    .line 757
    invoke-direct {p0, p1}, Landroid/widget/ExpandableListView;->isHeaderOrFooterPosition(I)Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_c

    #@6
    .line 758
    const-wide v1, 0xffffffffL

    #@b
    .line 765
    :goto_b
    return-wide v1

    #@c
    .line 761
    :cond_c
    invoke-direct {p0, p1}, Landroid/widget/ExpandableListView;->getFlatPositionForConnector(I)I

    #@f
    move-result v0

    #@10
    .line 762
    .local v0, adjustedPosition:I
    iget-object v4, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@12
    invoke-virtual {v4, v0}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@15
    move-result-object v3

    #@16
    .line 763
    .local v3, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v4, v3, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@18
    invoke-virtual {v4}, Landroid/widget/ExpandableListPosition;->getPackedPosition()J

    #@1b
    move-result-wide v1

    #@1c
    .line 764
    .local v1, packedPos:J
    invoke-virtual {v3}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@1f
    goto :goto_b
.end method

.method public getFlatListPosition(J)I
    .registers 7
    .parameter "packedPosition"

    #@0
    .prologue
    .line 780
    invoke-static {p1, p2}, Landroid/widget/ExpandableListPosition;->obtainPosition(J)Landroid/widget/ExpandableListPosition;

    #@3
    move-result-object v0

    #@4
    .line 782
    .local v0, elPackedPos:Landroid/widget/ExpandableListPosition;
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@6
    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@9
    move-result-object v2

    #@a
    .line 783
    .local v2, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    invoke-virtual {v0}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@d
    .line 784
    iget-object v3, v2, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@f
    iget v1, v3, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@11
    .line 785
    .local v1, flatListPosition:I
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@14
    .line 786
    invoke-direct {p0, v1}, Landroid/widget/ExpandableListView;->getAbsoluteFlatPosition(I)I

    #@17
    move-result v3

    #@18
    return v3
.end method

.method public getSelectedId()J
    .registers 6

    #@0
    .prologue
    .line 812
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getSelectedPosition()J

    #@3
    move-result-wide v1

    #@4
    .line 813
    .local v1, packedPos:J
    const-wide v3, 0xffffffffL

    #@9
    cmp-long v3, v1, v3

    #@b
    if-nez v3, :cond_10

    #@d
    const-wide/16 v3, -0x1

    #@f
    .line 822
    :goto_f
    return-wide v3

    #@10
    .line 815
    :cond_10
    invoke-static {v1, v2}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    #@13
    move-result v0

    #@14
    .line 817
    .local v0, groupPos:I
    invoke-static {v1, v2}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_21

    #@1a
    .line 819
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@1c
    invoke-interface {v3, v0}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    #@1f
    move-result-wide v3

    #@20
    goto :goto_f

    #@21
    .line 822
    :cond_21
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@23
    invoke-static {v1, v2}, Landroid/widget/ExpandableListView;->getPackedPositionChild(J)I

    #@26
    move-result v4

    #@27
    invoke-interface {v3, v0, v4}, Landroid/widget/ExpandableListAdapter;->getChildId(II)J

    #@2a
    move-result-wide v3

    #@2b
    goto :goto_f
.end method

.method public getSelectedPosition()J
    .registers 4

    #@0
    .prologue
    .line 798
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getSelectedItemPosition()I

    #@3
    move-result v0

    #@4
    .line 801
    .local v0, selectedPos:I
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListView;->getExpandableListPosition(I)J

    #@7
    move-result-wide v1

    #@8
    return-wide v1
.end method

.method handleItemClick(Landroid/view/View;IJ)Z
    .registers 17
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 534
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@2
    invoke-virtual {v0, p2}, Landroid/widget/ExpandableListConnector;->getUnflattenedPos(I)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@5
    move-result-object v9

    #@6
    .line 536
    .local v9, posMetadata:Landroid/widget/ExpandableListConnector$PositionMetadata;
    iget-object v0, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@8
    invoke-direct {p0, v0}, Landroid/widget/ExpandableListView;->getChildOrGroupId(Landroid/widget/ExpandableListPosition;)J

    #@b
    move-result-wide p3

    #@c
    .line 539
    iget-object v0, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@e
    iget v0, v0, Landroid/widget/ExpandableListPosition;->type:I

    #@10
    const/4 v1, 0x2

    #@11
    if-ne v0, v1, :cond_7b

    #@13
    .line 543
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    #@15
    if-eqz v0, :cond_2b

    #@17
    .line 544
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    #@19
    iget-object v1, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@1b
    iget v3, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@1d
    move-object v1, p0

    #@1e
    move-object v2, p1

    #@1f
    move-wide v4, p3

    #@20
    invoke-interface/range {v0 .. v5}, Landroid/widget/ExpandableListView$OnGroupClickListener;->onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2b

    #@26
    .line 546
    invoke-virtual {v9}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@29
    .line 547
    const/4 v10, 0x1

    #@2a
    .line 592
    :goto_2a
    return v10

    #@2b
    .line 551
    :cond_2b
    invoke-virtual {v9}, Landroid/widget/ExpandableListConnector$PositionMetadata;->isExpanded()Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_4c

    #@31
    .line 553
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@33
    invoke-virtual {v0, v9}, Landroid/widget/ExpandableListConnector;->collapseGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z

    #@36
    .line 555
    const/4 v0, 0x0

    #@37
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    #@3a
    .line 557
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

    #@3c
    if-eqz v0, :cond_47

    #@3e
    .line 558
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

    #@40
    iget-object v1, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@42
    iget v1, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@44
    invoke-interface {v0, v1}, Landroid/widget/ExpandableListView$OnGroupCollapseListener;->onGroupCollapse(I)V

    #@47
    .line 578
    :cond_47
    :goto_47
    const/4 v10, 0x1

    #@48
    .line 590
    .local v10, returnValue:Z
    :goto_48
    invoke-virtual {v9}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@4b
    goto :goto_2a

    #@4c
    .line 562
    .end local v10           #returnValue:Z
    :cond_4c
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@4e
    invoke-virtual {v0, v9}, Landroid/widget/ExpandableListConnector;->expandGroup(Landroid/widget/ExpandableListConnector$PositionMetadata;)Z

    #@51
    .line 564
    const/4 v0, 0x0

    #@52
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    #@55
    .line 566
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    #@57
    if-eqz v0, :cond_62

    #@59
    .line 567
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    #@5b
    iget-object v1, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@5d
    iget v1, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@5f
    invoke-interface {v0, v1}, Landroid/widget/ExpandableListView$OnGroupExpandListener;->onGroupExpand(I)V

    #@62
    .line 570
    :cond_62
    iget-object v0, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@64
    iget v8, v0, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@66
    .line 571
    .local v8, groupPos:I
    iget-object v0, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@68
    iget v7, v0, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@6a
    .line 573
    .local v7, groupFlatPos:I
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getHeaderViewsCount()I

    #@6d
    move-result v0

    #@6e
    add-int v11, v7, v0

    #@70
    .line 574
    .local v11, shiftedGroupPosition:I
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@72
    invoke-interface {v0, v8}, Landroid/widget/ExpandableListAdapter;->getChildrenCount(I)I

    #@75
    move-result v0

    #@76
    add-int/2addr v0, v11

    #@77
    invoke-virtual {p0, v0, v11}, Landroid/widget/ExpandableListView;->smoothScrollToPosition(II)V

    #@7a
    goto :goto_47

    #@7b
    .line 581
    .end local v7           #groupFlatPos:I
    .end local v8           #groupPos:I
    .end local v11           #shiftedGroupPosition:I
    :cond_7b
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    #@7d
    if-eqz v0, :cond_95

    #@7f
    .line 582
    const/4 v0, 0x0

    #@80
    invoke-virtual {p0, v0}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    #@83
    .line 583
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    #@85
    iget-object v1, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@87
    iget v3, v1, Landroid/widget/ExpandableListPosition;->groupPos:I

    #@89
    iget-object v1, v9, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@8b
    iget v4, v1, Landroid/widget/ExpandableListPosition;->childPos:I

    #@8d
    move-object v1, p0

    #@8e
    move-object v2, p1

    #@8f
    move-wide v5, p3

    #@90
    invoke-interface/range {v0 .. v6}, Landroid/widget/ExpandableListView$OnChildClickListener;->onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z

    #@93
    move-result v10

    #@94
    goto :goto_2a

    #@95
    .line 587
    :cond_95
    const/4 v10, 0x0

    #@96
    .restart local v10       #returnValue:Z
    goto :goto_48
.end method

.method public isGroupExpanded(I)Z
    .registers 3
    .parameter "groupPosition"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListConnector;->isGroupExpanded(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1177
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1178
    const-class v0, Landroid/widget/ExpandableListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1179
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1183
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1184
    const-class v0, Landroid/widget/ExpandableListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1185
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1162
    instance-of v1, p1, Landroid/widget/ExpandableListView$SavedState;

    #@2
    if-nez v1, :cond_8

    #@4
    .line 1163
    invoke-super {p0, p1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@7
    .line 1173
    :cond_7
    :goto_7
    return-void

    #@8
    :cond_8
    move-object v0, p1

    #@9
    .line 1167
    check-cast v0, Landroid/widget/ExpandableListView$SavedState;

    #@b
    .line 1168
    .local v0, ss:Landroid/widget/ExpandableListView$SavedState;
    invoke-virtual {v0}, Landroid/widget/ExpandableListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@e
    move-result-object v1

    #@f
    invoke-super {p0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@12
    .line 1170
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@14
    if-eqz v1, :cond_7

    #@16
    iget-object v1, v0, Landroid/widget/ExpandableListView$SavedState;->expandedGroupMetadataList:Ljava/util/ArrayList;

    #@18
    if-eqz v1, :cond_7

    #@1a
    .line 1171
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@1c
    iget-object v2, v0, Landroid/widget/ExpandableListView$SavedState;->expandedGroupMetadataList:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListConnector;->setExpandedGroupMetadataList(Ljava/util/ArrayList;)V

    #@21
    goto :goto_7
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 1155
    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v0

    #@4
    .line 1156
    .local v0, superState:Landroid/os/Parcelable;
    new-instance v2, Landroid/widget/ExpandableListView$SavedState;

    #@6
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@8
    if-eqz v1, :cond_14

    #@a
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@c
    invoke-virtual {v1}, Landroid/widget/ExpandableListConnector;->getExpandedGroupMetadataList()Ljava/util/ArrayList;

    #@f
    move-result-object v1

    #@10
    :goto_10
    invoke-direct {v2, v0, v1}, Landroid/widget/ExpandableListView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    #@13
    return-object v2

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_10
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .registers 7
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 515
    invoke-direct {p0, p2}, Landroid/widget/ExpandableListView;->isHeaderOrFooterPosition(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 517
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    #@9
    move-result v1

    #@a
    .line 522
    :goto_a
    return v1

    #@b
    .line 521
    :cond_b
    invoke-direct {p0, p2}, Landroid/widget/ExpandableListView;->getFlatPositionForConnector(I)I

    #@e
    move-result v0

    #@f
    .line 522
    .local v0, adjustedPosition:I
    invoke-virtual {p0, p1, v0, p3, p4}, Landroid/widget/ExpandableListView;->handleItemClick(Landroid/view/View;IJ)Z

    #@12
    move-result v1

    #@13
    goto :goto_a
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    check-cast p1, Landroid/widget/ListAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/ExpandableListAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    .line 460
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mAdapter:Landroid/widget/ExpandableListAdapter;

    #@2
    .line 462
    if-eqz p1, :cond_11

    #@4
    .line 464
    new-instance v0, Landroid/widget/ExpandableListConnector;

    #@6
    invoke-direct {v0, p1}, Landroid/widget/ExpandableListConnector;-><init>(Landroid/widget/ExpandableListAdapter;)V

    #@9
    iput-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@b
    .line 470
    :goto_b
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@d
    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@10
    .line 471
    return-void

    #@11
    .line 466
    :cond_11
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@14
    goto :goto_b
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 4
    .parameter "adapter"

    #@0
    .prologue
    .line 423
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "For ExpandableListView, use setAdapter(ExpandableListAdapter) instead of setAdapter(ListAdapter)"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setChildDivider(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "childDivider"

    #@0
    .prologue
    .line 386
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mChildDivider:Landroid/graphics/drawable/Drawable;

    #@2
    .line 387
    return-void
.end method

.method public setChildIndicator(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "childIndicator"

    #@0
    .prologue
    .line 1027
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mChildIndicator:Landroid/graphics/drawable/Drawable;

    #@2
    .line 1028
    return-void
.end method

.method public setChildIndicatorBounds(II)V
    .registers 3
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    .line 1042
    iput p1, p0, Landroid/widget/ExpandableListView;->mChildIndicatorLeft:I

    #@2
    .line 1043
    iput p2, p0, Landroid/widget/ExpandableListView;->mChildIndicatorRight:I

    #@4
    .line 1044
    return-void
.end method

.method public setGroupIndicator(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "groupIndicator"

    #@0
    .prologue
    .line 1055
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@2
    .line 1056
    iget v0, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@4
    if-nez v0, :cond_15

    #@6
    iget-object v0, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_15

    #@a
    .line 1057
    iget v0, p0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@c
    iget-object v1, p0, Landroid/widget/ExpandableListView;->mGroupIndicator:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@11
    move-result v1

    #@12
    add-int/2addr v0, v1

    #@13
    iput v0, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@15
    .line 1059
    :cond_15
    return-void
.end method

.method public setIndicatorBounds(II)V
    .registers 3
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    .line 1073
    iput p1, p0, Landroid/widget/ExpandableListView;->mIndicatorLeft:I

    #@2
    .line 1074
    iput p2, p0, Landroid/widget/ExpandableListView;->mIndicatorRight:I

    #@4
    .line 1075
    return-void
.end method

.method public setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V
    .registers 2
    .parameter "onChildClickListener"

    #@0
    .prologue
    .line 739
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    #@2
    .line 740
    return-void
.end method

.method public setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V
    .registers 2
    .parameter "onGroupClickListener"

    #@0
    .prologue
    .line 712
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    #@2
    .line 713
    return-void
.end method

.method public setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V
    .registers 2
    .parameter "onGroupCollapseListener"

    #@0
    .prologue
    .line 669
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mOnGroupCollapseListener:Landroid/widget/ExpandableListView$OnGroupCollapseListener;

    #@2
    .line 670
    return-void
.end method

.method public setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V
    .registers 2
    .parameter "onGroupExpandListener"

    #@0
    .prologue
    .line 687
    iput-object p1, p0, Landroid/widget/ExpandableListView;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    #@2
    .line 688
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 451
    invoke-super {p0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@3
    .line 452
    return-void
.end method

.method public setSelectedChild(IIZ)Z
    .registers 9
    .parameter "groupPosition"
    .parameter "childPosition"
    .parameter "shouldExpandGroup"

    #@0
    .prologue
    .line 852
    invoke-static {p1, p2}, Landroid/widget/ExpandableListPosition;->obtainChildPosition(II)Landroid/widget/ExpandableListPosition;

    #@3
    move-result-object v1

    #@4
    .line 854
    .local v1, elChildPos:Landroid/widget/ExpandableListPosition;
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@6
    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@9
    move-result-object v2

    #@a
    .line 856
    .local v2, flatChildPos:Landroid/widget/ExpandableListConnector$PositionMetadata;
    if-nez v2, :cond_23

    #@c
    .line 860
    if-nez p3, :cond_10

    #@e
    const/4 v3, 0x0

    #@f
    .line 878
    :goto_f
    return v3

    #@10
    .line 862
    :cond_10
    invoke-virtual {p0, p1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    #@13
    .line 864
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@15
    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@18
    move-result-object v2

    #@19
    .line 867
    if-nez v2, :cond_23

    #@1b
    .line 868
    new-instance v3, Ljava/lang/IllegalStateException;

    #@1d
    const-string v4, "Could not find child"

    #@1f
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v3

    #@23
    .line 872
    :cond_23
    iget-object v3, v2, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@25
    iget v3, v3, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@27
    invoke-direct {p0, v3}, Landroid/widget/ExpandableListView;->getAbsoluteFlatPosition(I)I

    #@2a
    move-result v0

    #@2b
    .line 873
    .local v0, absoluteFlatPosition:I
    invoke-super {p0, v0}, Landroid/widget/ListView;->setSelection(I)V

    #@2e
    .line 875
    invoke-virtual {v1}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@31
    .line 876
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@34
    .line 878
    const/4 v3, 0x1

    #@35
    goto :goto_f
.end method

.method public setSelectedGroup(I)V
    .registers 6
    .parameter "groupPosition"

    #@0
    .prologue
    .line 831
    invoke-static {p1}, Landroid/widget/ExpandableListPosition;->obtainGroupPosition(I)Landroid/widget/ExpandableListPosition;

    #@3
    move-result-object v1

    #@4
    .line 833
    .local v1, elGroupPos:Landroid/widget/ExpandableListPosition;
    iget-object v3, p0, Landroid/widget/ExpandableListView;->mConnector:Landroid/widget/ExpandableListConnector;

    #@6
    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListConnector;->getFlattenedPos(Landroid/widget/ExpandableListPosition;)Landroid/widget/ExpandableListConnector$PositionMetadata;

    #@9
    move-result-object v2

    #@a
    .line 834
    .local v2, pm:Landroid/widget/ExpandableListConnector$PositionMetadata;
    invoke-virtual {v1}, Landroid/widget/ExpandableListPosition;->recycle()V

    #@d
    .line 835
    iget-object v3, v2, Landroid/widget/ExpandableListConnector$PositionMetadata;->position:Landroid/widget/ExpandableListPosition;

    #@f
    iget v3, v3, Landroid/widget/ExpandableListPosition;->flatListPos:I

    #@11
    invoke-direct {p0, v3}, Landroid/widget/ExpandableListView;->getAbsoluteFlatPosition(I)I

    #@14
    move-result v0

    #@15
    .line 836
    .local v0, absoluteFlatPosition:I
    invoke-super {p0, v0}, Landroid/widget/ListView;->setSelection(I)V

    #@18
    .line 837
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector$PositionMetadata;->recycle()V

    #@1b
    .line 838
    return-void
.end method
