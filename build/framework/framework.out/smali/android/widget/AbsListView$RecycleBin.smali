.class Landroid/widget/AbsListView$RecycleBin;
.super Ljava/lang/Object;
.source "AbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecycleBin"
.end annotation


# instance fields
.field private mActiveViews:[Landroid/view/View;

.field private mCurrentScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstActivePosition:I

.field private mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSkippedScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTransientStateViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I

.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 6381
    iput-object p1, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 6395
    const/4 v0, 0x0

    #@6
    new-array v0, v0, [Landroid/view/View;

    #@8
    iput-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@a
    return-void
.end method

.method static synthetic access$3500(Landroid/widget/AbsListView$RecycleBin;)Landroid/widget/AbsListView$RecyclerListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 6381
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@2
    return-object v0
.end method

.method static synthetic access$3502(Landroid/widget/AbsListView$RecycleBin;Landroid/widget/AbsListView$RecyclerListener;)Landroid/widget/AbsListView$RecyclerListener;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 6381
    iput-object p1, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@2
    return-object p1
.end method

.method private pruneScrapViews()V
    .registers 14

    #@0
    .prologue
    .line 6677
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@2
    array-length v3, v10

    #@3
    .line 6678
    .local v3, maxViews:I
    iget v9, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@5
    .line 6679
    .local v9, viewTypeCount:I
    iget-object v5, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@7
    .line 6680
    .local v5, scrapViews:[Ljava/util/ArrayList;,"[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v9, :cond_2d

    #@a
    .line 6681
    aget-object v4, v5, v1

    #@c
    .line 6682
    .local v4, scrapPile:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v6

    #@10
    .line 6683
    .local v6, size:I
    sub-int v0, v6, v3

    #@12
    .line 6684
    .local v0, extras:I
    add-int/lit8 v6, v6, -0x1

    #@14
    .line 6685
    const/4 v2, 0x0

    #@15
    .local v2, j:I
    move v7, v6

    #@16
    .end local v6           #size:I
    .local v7, size:I
    :goto_16
    if-ge v2, v0, :cond_2a

    #@18
    .line 6686
    iget-object v11, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@1a
    add-int/lit8 v6, v7, -0x1

    #@1c
    .end local v7           #size:I
    .restart local v6       #size:I
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1f
    move-result-object v10

    #@20
    check-cast v10, Landroid/view/View;

    #@22
    const/4 v12, 0x0

    #@23
    invoke-static {v11, v10, v12}, Landroid/widget/AbsListView;->access$4100(Landroid/widget/AbsListView;Landroid/view/View;Z)V

    #@26
    .line 6685
    add-int/lit8 v2, v2, 0x1

    #@28
    move v7, v6

    #@29
    .end local v6           #size:I
    .restart local v7       #size:I
    goto :goto_16

    #@2a
    .line 6680
    :cond_2a
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_8

    #@2d
    .line 6690
    .end local v0           #extras:I
    .end local v2           #j:I
    .end local v4           #scrapPile:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7           #size:I
    :cond_2d
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@2f
    if-eqz v10, :cond_52

    #@31
    .line 6691
    const/4 v1, 0x0

    #@32
    :goto_32
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@34
    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    #@37
    move-result v10

    #@38
    if-ge v1, v10, :cond_52

    #@3a
    .line 6692
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3c
    invoke-virtual {v10, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3f
    move-result-object v8

    #@40
    check-cast v8, Landroid/view/View;

    #@42
    .line 6693
    .local v8, v:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->hasTransientState()Z

    #@45
    move-result v10

    #@46
    if-nez v10, :cond_4f

    #@48
    .line 6694
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@4a
    invoke-virtual {v10, v1}, Landroid/util/SparseArray;->removeAt(I)V

    #@4d
    .line 6695
    add-int/lit8 v1, v1, -0x1

    #@4f
    .line 6691
    :cond_4f
    add-int/lit8 v1, v1, 0x1

    #@51
    goto :goto_32

    #@52
    .line 6699
    .end local v8           #v:Landroid/view/View;
    :cond_52
    return-void
.end method


# virtual methods
.method addScrapView(Landroid/view/View;I)V
    .registers 8
    .parameter "scrap"
    .parameter "position"

    #@0
    .prologue
    .line 6565
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/AbsListView$LayoutParams;

    #@6
    .line 6566
    .local v0, lp:Landroid/widget/AbsListView$LayoutParams;
    if-nez v0, :cond_9

    #@8
    .line 6604
    :cond_8
    :goto_8
    return-void

    #@9
    .line 6570
    :cond_9
    iput p2, v0, Landroid/widget/AbsListView$LayoutParams;->scrappedFromPosition:I

    #@b
    .line 6574
    iget v2, v0, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@d
    .line 6575
    .local v2, viewType:I
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    #@10
    move-result v1

    #@11
    .line 6576
    .local v1, scrapHasTransientState:Z
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_19

    #@17
    if-eqz v1, :cond_44

    #@19
    .line 6577
    :cond_19
    const/4 v3, -0x2

    #@1a
    if-ne v2, v3, :cond_1e

    #@1c
    if-eqz v1, :cond_2e

    #@1e
    .line 6578
    :cond_1e
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@20
    if-nez v3, :cond_29

    #@22
    .line 6579
    new-instance v3, Ljava/util/ArrayList;

    #@24
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@27
    iput-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@29
    .line 6581
    :cond_29
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2e
    .line 6583
    :cond_2e
    if-eqz v1, :cond_8

    #@30
    .line 6584
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@32
    if-nez v3, :cond_3b

    #@34
    .line 6585
    new-instance v3, Landroid/util/SparseArray;

    #@36
    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    #@39
    iput-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3b
    .line 6587
    :cond_3b
    invoke-virtual {p1}, Landroid/view/View;->dispatchStartTemporaryDetach()V

    #@3e
    .line 6588
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@40
    invoke-virtual {v3, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@43
    goto :goto_8

    #@44
    .line 6593
    :cond_44
    invoke-virtual {p1}, Landroid/view/View;->dispatchStartTemporaryDetach()V

    #@47
    .line 6594
    iget v3, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@49
    const/4 v4, 0x1

    #@4a
    if-ne v3, v4, :cond_5f

    #@4c
    .line 6595
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@4e
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 6600
    :goto_51
    const/4 v3, 0x0

    #@52
    invoke-virtual {p1, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    #@55
    .line 6601
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@57
    if-eqz v3, :cond_8

    #@59
    .line 6602
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@5b
    invoke-interface {v3, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    #@5e
    goto :goto_8

    #@5f
    .line 6597
    :cond_5f
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@61
    aget-object v3, v3, v2

    #@63
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@66
    goto :goto_51
.end method

.method clear()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 6457
    iget v5, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@3
    const/4 v6, 0x1

    #@4
    if-ne v5, v6, :cond_20

    #@6
    .line 6458
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@8
    .line 6459
    .local v2, scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    .line 6460
    .local v3, scrapCount:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v3, :cond_44

    #@f
    .line 6461
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@11
    add-int/lit8 v5, v3, -0x1

    #@13
    sub-int/2addr v5, v0

    #@14
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@17
    move-result-object v5

    #@18
    check-cast v5, Landroid/view/View;

    #@1a
    invoke-static {v6, v5, v7}, Landroid/widget/AbsListView;->access$3700(Landroid/widget/AbsListView;Landroid/view/View;Z)V

    #@1d
    .line 6460
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_d

    #@20
    .line 6464
    .end local v0           #i:I
    .end local v2           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3           #scrapCount:I
    :cond_20
    iget v4, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@22
    .line 6465
    .local v4, typeCount:I
    const/4 v0, 0x0

    #@23
    .restart local v0       #i:I
    :goto_23
    if-ge v0, v4, :cond_44

    #@25
    .line 6466
    iget-object v5, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@27
    aget-object v2, v5, v0

    #@29
    .line 6467
    .restart local v2       #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v3

    #@2d
    .line 6468
    .restart local v3       #scrapCount:I
    const/4 v1, 0x0

    #@2e
    .local v1, j:I
    :goto_2e
    if-ge v1, v3, :cond_41

    #@30
    .line 6469
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@32
    add-int/lit8 v5, v3, -0x1

    #@34
    sub-int/2addr v5, v1

    #@35
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@38
    move-result-object v5

    #@39
    check-cast v5, Landroid/view/View;

    #@3b
    invoke-static {v6, v5, v7}, Landroid/widget/AbsListView;->access$3800(Landroid/widget/AbsListView;Landroid/view/View;Z)V

    #@3e
    .line 6468
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_2e

    #@41
    .line 6465
    :cond_41
    add-int/lit8 v0, v0, 0x1

    #@43
    goto :goto_23

    #@44
    .line 6473
    .end local v1           #j:I
    .end local v2           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3           #scrapCount:I
    .end local v4           #typeCount:I
    :cond_44
    iget-object v5, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@46
    if-eqz v5, :cond_4d

    #@48
    .line 6474
    iget-object v5, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@4a
    invoke-virtual {v5}, Landroid/util/SparseArray;->clear()V

    #@4d
    .line 6476
    :cond_4d
    return-void
.end method

.method clearTransientStateViews()V
    .registers 2

    #@0
    .prologue
    .line 6539
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 6540
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@9
    .line 6542
    :cond_9
    return-void
.end method

.method fillActiveViews(II)V
    .registers 9
    .parameter "childCount"
    .parameter "firstActivePosition"

    #@0
    .prologue
    .line 6486
    iget-object v4, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@2
    array-length v4, v4

    #@3
    if-ge v4, p1, :cond_9

    #@5
    .line 6487
    new-array v4, p1, [Landroid/view/View;

    #@7
    iput-object v4, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@9
    .line 6489
    :cond_9
    iput p2, p0, Landroid/widget/AbsListView$RecycleBin;->mFirstActivePosition:I

    #@b
    .line 6491
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@d
    .line 6492
    .local v0, activeViews:[Landroid/view/View;
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, p1, :cond_28

    #@10
    .line 6493
    iget-object v4, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@12
    invoke-virtual {v4, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v1

    #@16
    .line 6494
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Landroid/widget/AbsListView$LayoutParams;

    #@1c
    .line 6496
    .local v3, lp:Landroid/widget/AbsListView$LayoutParams;
    if-eqz v3, :cond_25

    #@1e
    iget v4, v3, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@20
    const/4 v5, -0x2

    #@21
    if-eq v4, v5, :cond_25

    #@23
    .line 6499
    aput-object v1, v0, v2

    #@25
    .line 6492
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_e

    #@28
    .line 6502
    .end local v1           #child:Landroid/view/View;
    .end local v3           #lp:Landroid/widget/AbsListView$LayoutParams;
    :cond_28
    return-void
.end method

.method getActiveView(I)Landroid/view/View;
    .registers 7
    .parameter "position"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 6512
    iget v4, p0, Landroid/widget/AbsListView$RecycleBin;->mFirstActivePosition:I

    #@3
    sub-int v1, p1, v4

    #@5
    .line 6513
    .local v1, index:I
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@7
    .line 6514
    .local v0, activeViews:[Landroid/view/View;
    if-ltz v1, :cond_11

    #@9
    array-length v4, v0

    #@a
    if-ge v1, v4, :cond_11

    #@c
    .line 6515
    aget-object v2, v0, v1

    #@e
    .line 6516
    .local v2, match:Landroid/view/View;
    aput-object v3, v0, v1

    #@10
    .line 6519
    .end local v2           #match:Landroid/view/View;
    :goto_10
    return-object v2

    #@11
    :cond_11
    move-object v2, v3

    #@12
    goto :goto_10
.end method

.method getScrapView(I)Landroid/view/View;
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 6548
    iget v1, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_c

    #@5
    .line 6549
    iget-object v1, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@7
    invoke-static {v1, p1}, Landroid/widget/AbsListView;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 6556
    :goto_b
    return-object v1

    #@c
    .line 6551
    :cond_c
    iget-object v1, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@e
    iget-object v1, v1, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@10
    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@13
    move-result v0

    #@14
    .line 6552
    .local v0, whichScrap:I
    if-ltz v0, :cond_24

    #@16
    iget-object v1, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@18
    array-length v1, v1

    #@19
    if-ge v0, v1, :cond_24

    #@1b
    .line 6553
    iget-object v1, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@1d
    aget-object v1, v1, v0

    #@1f
    invoke-static {v1, p1}, Landroid/widget/AbsListView;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    #@22
    move-result-object v1

    #@23
    goto :goto_b

    #@24
    .line 6556
    :cond_24
    const/4 v1, 0x0

    #@25
    goto :goto_b
.end method

.method getTransientStateView(I)Landroid/view/View;
    .registers 5
    .parameter "position"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6523
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 6532
    :cond_5
    :goto_5
    return-object v1

    #@6
    .line 6526
    :cond_6
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@b
    move-result v0

    #@c
    .line 6527
    .local v0, index:I
    if-ltz v0, :cond_5

    #@e
    .line 6530
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@10
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/view/View;

    #@16
    .line 6531
    .local v1, result:Landroid/view/View;
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->removeAt(I)V

    #@1b
    goto :goto_5
.end method

.method public markChildrenDirty()V
    .registers 9

    #@0
    .prologue
    .line 6425
    iget v6, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@2
    const/4 v7, 0x1

    #@3
    if-ne v6, v7, :cond_1a

    #@5
    .line 6426
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@7
    .line 6427
    .local v3, scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v4

    #@b
    .line 6428
    .local v4, scrapCount:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v4, :cond_39

    #@e
    .line 6429
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v6

    #@12
    check-cast v6, Landroid/view/View;

    #@14
    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    #@17
    .line 6428
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_c

    #@1a
    .line 6432
    .end local v1           #i:I
    .end local v3           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4           #scrapCount:I
    :cond_1a
    iget v5, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@1c
    .line 6433
    .local v5, typeCount:I
    const/4 v1, 0x0

    #@1d
    .restart local v1       #i:I
    :goto_1d
    if-ge v1, v5, :cond_39

    #@1f
    .line 6434
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@21
    aget-object v3, v6, v1

    #@23
    .line 6435
    .restart local v3       #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v4

    #@27
    .line 6436
    .restart local v4       #scrapCount:I
    const/4 v2, 0x0

    #@28
    .local v2, j:I
    :goto_28
    if-ge v2, v4, :cond_36

    #@2a
    .line 6437
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v6

    #@2e
    check-cast v6, Landroid/view/View;

    #@30
    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    #@33
    .line 6436
    add-int/lit8 v2, v2, 0x1

    #@35
    goto :goto_28

    #@36
    .line 6433
    :cond_36
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_1d

    #@39
    .line 6441
    .end local v2           #j:I
    .end local v3           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4           #scrapCount:I
    .end local v5           #typeCount:I
    :cond_39
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3b
    if-eqz v6, :cond_54

    #@3d
    .line 6442
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3f
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@42
    move-result v0

    #@43
    .line 6443
    .local v0, count:I
    const/4 v1, 0x0

    #@44
    :goto_44
    if-ge v1, v0, :cond_54

    #@46
    .line 6444
    iget-object v6, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@48
    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@4b
    move-result-object v6

    #@4c
    check-cast v6, Landroid/view/View;

    #@4e
    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    #@51
    .line 6443
    add-int/lit8 v1, v1, 0x1

    #@53
    goto :goto_44

    #@54
    .line 6447
    .end local v0           #count:I
    :cond_54
    return-void
.end method

.method reclaimScrapViews(Ljava/util/List;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6705
    .local p1, views:Ljava/util/List;,"Ljava/util/List<Landroid/view/View;>;"
    iget v4, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@2
    const/4 v5, 0x1

    #@3
    if-ne v4, v5, :cond_b

    #@5
    .line 6706
    iget-object v4, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@7
    invoke-interface {p1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@a
    .line 6715
    :cond_a
    return-void

    #@b
    .line 6708
    :cond_b
    iget v3, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@d
    .line 6709
    .local v3, viewTypeCount:I
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@f
    .line 6710
    .local v2, scrapViews:[Ljava/util/ArrayList;,"[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    if-ge v0, v3, :cond_a

    #@12
    .line 6711
    aget-object v1, v2, v0

    #@14
    .line 6712
    .local v1, scrapPile:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@17
    .line 6710
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_10
.end method

.method removeSkippedScrap()V
    .registers 6

    #@0
    .prologue
    .line 6610
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 6618
    :goto_4
    return-void

    #@5
    .line 6613
    :cond_5
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    .line 6614
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_1f

    #@e
    .line 6615
    iget-object v3, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@10
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/view/View;

    #@18
    const/4 v4, 0x0

    #@19
    invoke-static {v3, v2, v4}, Landroid/widget/AbsListView;->access$3900(Landroid/widget/AbsListView;Landroid/view/View;Z)V

    #@1c
    .line 6614
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_c

    #@1f
    .line 6617
    :cond_1f
    iget-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@24
    goto :goto_4
.end method

.method scrapActiveViews()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 6624
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@5
    .line 6625
    .local v0, activeViews:[Landroid/view/View;
    iget-object v12, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@7
    if-eqz v12, :cond_52

    #@9
    move v2, v10

    #@a
    .line 6626
    .local v2, hasListener:Z
    :goto_a
    iget v12, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@c
    if-le v12, v10, :cond_54

    #@e
    move v5, v10

    #@f
    .line 6628
    .local v5, multipleScraps:Z
    :goto_f
    iget-object v7, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@11
    .line 6629
    .local v7, scrapViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    array-length v1, v0

    #@12
    .line 6630
    .local v1, count:I
    add-int/lit8 v3, v1, -0x1

    #@14
    .local v3, i:I
    :goto_14
    if-ltz v3, :cond_72

    #@16
    .line 6631
    aget-object v8, v0, v3

    #@18
    .line 6632
    .local v8, victim:Landroid/view/View;
    if-eqz v8, :cond_4f

    #@1a
    .line 6633
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1d
    move-result-object v4

    #@1e
    check-cast v4, Landroid/widget/AbsListView$LayoutParams;

    #@20
    .line 6635
    .local v4, lp:Landroid/widget/AbsListView$LayoutParams;
    iget v9, v4, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@22
    .line 6637
    .local v9, whichScrap:I
    aput-object v13, v0, v3

    #@24
    .line 6639
    invoke-virtual {v8}, Landroid/view/View;->hasTransientState()Z

    #@27
    move-result v6

    #@28
    .line 6640
    .local v6, scrapHasTransientState:Z
    invoke-virtual {p0, v9}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@2b
    move-result v10

    #@2c
    if-eqz v10, :cond_30

    #@2e
    if-eqz v6, :cond_56

    #@30
    .line 6642
    :cond_30
    const/4 v10, -0x2

    #@31
    if-ne v9, v10, :cond_35

    #@33
    if-eqz v6, :cond_3a

    #@35
    .line 6644
    :cond_35
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->this$0:Landroid/widget/AbsListView;

    #@37
    invoke-static {v10, v8, v11}, Landroid/widget/AbsListView;->access$4000(Landroid/widget/AbsListView;Landroid/view/View;Z)V

    #@3a
    .line 6646
    :cond_3a
    if-eqz v6, :cond_4f

    #@3c
    .line 6647
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@3e
    if-nez v10, :cond_47

    #@40
    .line 6648
    new-instance v10, Landroid/util/SparseArray;

    #@42
    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    #@45
    iput-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@47
    .line 6650
    :cond_47
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mTransientStateViews:Landroid/util/SparseArray;

    #@49
    iget v12, p0, Landroid/widget/AbsListView$RecycleBin;->mFirstActivePosition:I

    #@4b
    add-int/2addr v12, v3

    #@4c
    invoke-virtual {v10, v12, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@4f
    .line 6630
    .end local v4           #lp:Landroid/widget/AbsListView$LayoutParams;
    .end local v6           #scrapHasTransientState:Z
    .end local v9           #whichScrap:I
    :cond_4f
    :goto_4f
    add-int/lit8 v3, v3, -0x1

    #@51
    goto :goto_14

    #@52
    .end local v1           #count:I
    .end local v2           #hasListener:Z
    .end local v3           #i:I
    .end local v5           #multipleScraps:Z
    .end local v7           #scrapViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v8           #victim:Landroid/view/View;
    :cond_52
    move v2, v11

    #@53
    .line 6625
    goto :goto_a

    #@54
    .restart local v2       #hasListener:Z
    :cond_54
    move v5, v11

    #@55
    .line 6626
    goto :goto_f

    #@56
    .line 6655
    .restart local v1       #count:I
    .restart local v3       #i:I
    .restart local v4       #lp:Landroid/widget/AbsListView$LayoutParams;
    .restart local v5       #multipleScraps:Z
    .restart local v6       #scrapHasTransientState:Z
    .restart local v7       #scrapViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .restart local v8       #victim:Landroid/view/View;
    .restart local v9       #whichScrap:I
    :cond_56
    if-eqz v5, :cond_5c

    #@58
    .line 6656
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@5a
    aget-object v7, v10, v9

    #@5c
    .line 6658
    :cond_5c
    invoke-virtual {v8}, Landroid/view/View;->dispatchStartTemporaryDetach()V

    #@5f
    .line 6659
    iget v10, p0, Landroid/widget/AbsListView$RecycleBin;->mFirstActivePosition:I

    #@61
    add-int/2addr v10, v3

    #@62
    iput v10, v4, Landroid/widget/AbsListView$LayoutParams;->scrappedFromPosition:I

    #@64
    .line 6660
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@67
    .line 6662
    invoke-virtual {v8, v13}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    #@6a
    .line 6663
    if-eqz v2, :cond_4f

    #@6c
    .line 6664
    iget-object v10, p0, Landroid/widget/AbsListView$RecycleBin;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    #@6e
    invoke-interface {v10, v8}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    #@71
    goto :goto_4f

    #@72
    .line 6669
    .end local v4           #lp:Landroid/widget/AbsListView$LayoutParams;
    .end local v6           #scrapHasTransientState:Z
    .end local v8           #victim:Landroid/view/View;
    .end local v9           #whichScrap:I
    :cond_72
    invoke-direct {p0}, Landroid/widget/AbsListView$RecycleBin;->pruneScrapViews()V

    #@75
    .line 6670
    return-void
.end method

.method setCacheColorHint(I)V
    .registers 12
    .parameter "color"

    #@0
    .prologue
    .line 6723
    iget v8, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@2
    const/4 v9, 0x1

    #@3
    if-ne v8, v9, :cond_1a

    #@5
    .line 6724
    iget-object v4, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@7
    .line 6725
    .local v4, scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v5

    #@b
    .line 6726
    .local v5, scrapCount:I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v5, :cond_39

    #@e
    .line 6727
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v8

    #@12
    check-cast v8, Landroid/view/View;

    #@14
    invoke-virtual {v8, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@17
    .line 6726
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_c

    #@1a
    .line 6730
    .end local v2           #i:I
    .end local v4           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v5           #scrapCount:I
    :cond_1a
    iget v6, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@1c
    .line 6731
    .local v6, typeCount:I
    const/4 v2, 0x0

    #@1d
    .restart local v2       #i:I
    :goto_1d
    if-ge v2, v6, :cond_39

    #@1f
    .line 6732
    iget-object v8, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@21
    aget-object v4, v8, v2

    #@23
    .line 6733
    .restart local v4       #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v5

    #@27
    .line 6734
    .restart local v5       #scrapCount:I
    const/4 v3, 0x0

    #@28
    .local v3, j:I
    :goto_28
    if-ge v3, v5, :cond_36

    #@2a
    .line 6735
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v8

    #@2e
    check-cast v8, Landroid/view/View;

    #@30
    invoke-virtual {v8, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@33
    .line 6734
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_28

    #@36
    .line 6731
    :cond_36
    add-int/lit8 v2, v2, 0x1

    #@38
    goto :goto_1d

    #@39
    .line 6740
    .end local v3           #j:I
    .end local v4           #scrap:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v5           #scrapCount:I
    .end local v6           #typeCount:I
    :cond_39
    iget-object v0, p0, Landroid/widget/AbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    #@3b
    .line 6741
    .local v0, activeViews:[Landroid/view/View;
    array-length v1, v0

    #@3c
    .line 6742
    .local v1, count:I
    const/4 v2, 0x0

    #@3d
    :goto_3d
    if-ge v2, v1, :cond_49

    #@3f
    .line 6743
    aget-object v7, v0, v2

    #@41
    .line 6744
    .local v7, victim:Landroid/view/View;
    if-eqz v7, :cond_46

    #@43
    .line 6745
    invoke-virtual {v7, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@46
    .line 6742
    :cond_46
    add-int/lit8 v2, v2, 0x1

    #@48
    goto :goto_3d

    #@49
    .line 6748
    .end local v7           #victim:Landroid/view/View;
    :cond_49
    return-void
.end method

.method public setViewTypeCount(I)V
    .registers 6
    .parameter "viewTypeCount"

    #@0
    .prologue
    .line 6411
    const/4 v2, 0x1

    #@1
    if-ge p1, v2, :cond_b

    #@3
    .line 6412
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "Can\'t have a viewTypeCount < 1"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 6415
    :cond_b
    new-array v1, p1, [Ljava/util/ArrayList;

    #@d
    .line 6416
    .local v1, scrapViews:[Ljava/util/ArrayList;,"[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    if-ge v0, p1, :cond_1a

    #@10
    .line 6417
    new-instance v2, Ljava/util/ArrayList;

    #@12
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@15
    aput-object v2, v1, v0

    #@17
    .line 6416
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_e

    #@1a
    .line 6419
    :cond_1a
    iput p1, p0, Landroid/widget/AbsListView$RecycleBin;->mViewTypeCount:I

    #@1c
    .line 6420
    const/4 v2, 0x0

    #@1d
    aget-object v2, v1, v2

    #@1f
    iput-object v2, p0, Landroid/widget/AbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    #@21
    .line 6421
    iput-object v1, p0, Landroid/widget/AbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    #@23
    .line 6422
    return-void
.end method

.method public shouldRecycleViewType(I)Z
    .registers 3
    .parameter "viewType"

    #@0
    .prologue
    .line 6450
    if-ltz p1, :cond_4

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method
