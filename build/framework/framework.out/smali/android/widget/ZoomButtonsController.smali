.class public Landroid/widget/ZoomButtonsController;
.super Ljava/lang/Object;
.source "ZoomButtonsController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ZoomButtonsController$Container;,
        Landroid/widget/ZoomButtonsController$OnZoomListener;
    }
.end annotation


# static fields
.field private static final MSG_DISMISS_ZOOM_CONTROLS:I = 0x3

.field private static final MSG_POST_CONFIGURATION_CHANGED:I = 0x2

.field private static final MSG_POST_SET_VISIBLE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "ZoomButtonsController"

#the value of this static final field might be set in the static constructor
.field private static final ZOOM_CONTROLS_TIMEOUT:I = 0x0

.field private static final ZOOM_CONTROLS_TOUCH_PADDING:I = 0x14


# instance fields
.field private mAutoDismissControls:Z

.field private mCallback:Landroid/widget/ZoomButtonsController$OnZoomListener;

.field private final mConfigurationChangedFilter:Landroid/content/IntentFilter;

.field private final mConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

.field private final mContainer:Landroid/widget/FrameLayout;

.field private mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private final mContainerRawLocation:[I

.field private final mContext:Landroid/content/Context;

.field private mControls:Landroid/widget/ZoomControls;

.field private final mHandler:Landroid/os/Handler;

.field private mIsVisible:Z

.field private final mOwnerView:Landroid/view/View;

.field private final mOwnerViewRawLocation:[I

.field private mPostedVisibleInitializer:Ljava/lang/Runnable;

.field private mReleaseTouchListenerOnUp:Z

.field private final mTempIntArray:[I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchPaddingScaledSq:I

.field private mTouchTargetView:Landroid/view/View;

.field private final mTouchTargetWindowLocation:[I

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 78
    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    #@3
    move-result-wide v0

    #@4
    long-to-int v0, v0

    #@5
    sput v0, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .registers 4
    .parameter "ownerView"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 203
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 86
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mAutoDismissControls:Z

    #@7
    .line 97
    new-array v0, v1, [I

    #@9
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@b
    .line 104
    new-array v0, v1, [I

    #@d
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@f
    .line 116
    new-array v0, v1, [I

    #@11
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetWindowLocation:[I

    #@13
    .line 131
    new-instance v0, Landroid/graphics/Rect;

    #@15
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mTempRect:Landroid/graphics/Rect;

    #@1a
    .line 132
    new-array v0, v1, [I

    #@1c
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mTempIntArray:[I

    #@1e
    .line 144
    new-instance v0, Landroid/content/IntentFilter;

    #@20
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    #@22
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@25
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mConfigurationChangedFilter:Landroid/content/IntentFilter;

    #@27
    .line 150
    new-instance v0, Landroid/widget/ZoomButtonsController$1;

    #@29
    invoke-direct {v0, p0}, Landroid/widget/ZoomButtonsController$1;-><init>(Landroid/widget/ZoomButtonsController;)V

    #@2c
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    #@2e
    .line 170
    new-instance v0, Landroid/widget/ZoomButtonsController$2;

    #@30
    invoke-direct {v0, p0}, Landroid/widget/ZoomButtonsController$2;-><init>(Landroid/widget/ZoomButtonsController;)V

    #@33
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@35
    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@3b
    .line 205
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@3d
    const-string/jumbo v1, "window"

    #@40
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/view/WindowManager;

    #@46
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mWindowManager:Landroid/view/WindowManager;

    #@48
    .line 206
    iput-object p1, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@4a
    .line 208
    const/high16 v0, 0x41a0

    #@4c
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@4e
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@55
    move-result-object v1

    #@56
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    #@58
    mul-float/2addr v0, v1

    #@59
    float-to-int v0, v0

    #@5a
    iput v0, p0, Landroid/widget/ZoomButtonsController;->mTouchPaddingScaledSq:I

    #@5c
    .line 210
    iget v0, p0, Landroid/widget/ZoomButtonsController;->mTouchPaddingScaledSq:I

    #@5e
    iget v1, p0, Landroid/widget/ZoomButtonsController;->mTouchPaddingScaledSq:I

    #@60
    mul-int/2addr v0, v1

    #@61
    iput v0, p0, Landroid/widget/ZoomButtonsController;->mTouchPaddingScaledSq:I

    #@63
    .line 212
    invoke-direct {p0}, Landroid/widget/ZoomButtonsController;->createContainer()Landroid/widget/FrameLayout;

    #@66
    move-result-object v0

    #@67
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@69
    .line 213
    return-void
.end method

.method static synthetic access$000(Landroid/widget/ZoomButtonsController;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/widget/ZoomButtonsController;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/ZoomButtonsController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 74
    invoke-direct {p0}, Landroid/widget/ZoomButtonsController;->onPostConfigurationChanged()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/ZoomButtonsController;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$400()I
    .registers 1

    #@0
    .prologue
    .line 74
    sget v0, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/widget/ZoomButtonsController;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/ZoomButtonsController;->dismissControlsDelayed(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/ZoomButtonsController;)Landroid/widget/ZoomButtonsController$OnZoomListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mCallback:Landroid/widget/ZoomButtonsController$OnZoomListener;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/ZoomButtonsController;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 74
    invoke-direct {p0}, Landroid/widget/ZoomButtonsController;->refreshPositioningVariables()V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/widget/ZoomButtonsController;Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/ZoomButtonsController;->onContainerKey(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private createContainer()Landroid/widget/FrameLayout;
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x2

    #@1
    .line 243
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    #@3
    invoke-direct {v2, v4, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    #@6
    .line 245
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    const v3, 0x800033

    #@9
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@b
    .line 246
    const v3, 0x20218

    #@e
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@10
    .line 250
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@12
    .line 251
    const/4 v3, -0x1

    #@13
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@15
    .line 252
    const/16 v3, 0x3e8

    #@17
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@19
    .line 253
    const/4 v3, -0x3

    #@1a
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    #@1c
    .line 254
    const v3, 0x10301eb

    #@1f
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@21
    .line 255
    iput-object v2, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@23
    .line 257
    new-instance v0, Landroid/widget/ZoomButtonsController$Container;

    #@25
    iget-object v3, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@27
    invoke-direct {v0, p0, v3}, Landroid/widget/ZoomButtonsController$Container;-><init>(Landroid/widget/ZoomButtonsController;Landroid/content/Context;)V

    #@2a
    .line 258
    .local v0, container:Landroid/widget/FrameLayout;
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@2d
    .line 259
    const/4 v3, 0x1

    #@2e
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setMeasureAllChildren(Z)V

    #@31
    .line 261
    iget-object v3, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@33
    const-string/jumbo v4, "layout_inflater"

    #@36
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/view/LayoutInflater;

    #@3c
    .line 263
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v3, 0x10900ef

    #@3f
    invoke-virtual {v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@42
    .line 265
    const v3, 0x10203c5

    #@45
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    #@48
    move-result-object v3

    #@49
    check-cast v3, Landroid/widget/ZoomControls;

    #@4b
    iput-object v3, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@4d
    .line 266
    iget-object v3, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@4f
    new-instance v4, Landroid/widget/ZoomButtonsController$3;

    #@51
    invoke-direct {v4, p0}, Landroid/widget/ZoomButtonsController$3;-><init>(Landroid/widget/ZoomButtonsController;)V

    #@54
    invoke-virtual {v3, v4}, Landroid/widget/ZoomControls;->setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V

    #@57
    .line 272
    iget-object v3, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@59
    new-instance v4, Landroid/widget/ZoomButtonsController$4;

    #@5b
    invoke-direct {v4, p0}, Landroid/widget/ZoomButtonsController$4;-><init>(Landroid/widget/ZoomButtonsController;)V

    #@5e
    invoke-virtual {v3, v4}, Landroid/widget/ZoomControls;->setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V

    #@61
    .line 279
    return-object v0
.end method

.method private dismissControlsDelayed(I)V
    .registers 6
    .parameter "delay"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 440
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mAutoDismissControls:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 441
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@7
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@a
    .line 442
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@c
    int-to-long v1, p1

    #@d
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@10
    .line 444
    :cond_10
    return-void
.end method

.method private findViewForTouch(II)Landroid/view/View;
    .registers 15
    .parameter "rawX"
    .parameter "rawY"

    #@0
    .prologue
    .line 615
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@2
    const/4 v11, 0x0

    #@3
    aget v10, v10, v11

    #@5
    sub-int v3, p1, v10

    #@7
    .line 616
    .local v3, containerCoordsX:I
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@9
    const/4 v11, 0x1

    #@a
    aget v10, v10, v11

    #@c
    sub-int v4, p2, v10

    #@e
    .line 617
    .local v4, containerCoordsY:I
    iget-object v8, p0, Landroid/widget/ZoomButtonsController;->mTempRect:Landroid/graphics/Rect;

    #@10
    .line 619
    .local v8, frame:Landroid/graphics/Rect;
    const/4 v1, 0x0

    #@11
    .line 620
    .local v1, closestChild:Landroid/view/View;
    const v2, 0x7fffffff

    #@14
    .line 622
    .local v2, closestChildDistanceSq:I
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@16
    invoke-virtual {v10}, Landroid/widget/FrameLayout;->getChildCount()I

    #@19
    move-result v10

    #@1a
    add-int/lit8 v9, v10, -0x1

    #@1c
    .local v9, i:I
    :goto_1c
    if-ltz v9, :cond_80

    #@1e
    .line 623
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@20
    invoke-virtual {v10, v9}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    #@23
    move-result-object v0

    #@24
    .line 624
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@27
    move-result v10

    #@28
    if-eqz v10, :cond_2d

    #@2a
    .line 622
    :cond_2a
    :goto_2a
    add-int/lit8 v9, v9, -0x1

    #@2c
    goto :goto_1c

    #@2d
    .line 628
    :cond_2d
    invoke-virtual {v0, v8}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@30
    .line 629
    invoke-virtual {v8, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    #@33
    move-result v10

    #@34
    if-eqz v10, :cond_37

    #@36
    .line 656
    .end local v0           #child:Landroid/view/View;
    :goto_36
    return-object v0

    #@37
    .line 634
    .restart local v0       #child:Landroid/view/View;
    :cond_37
    iget v10, v8, Landroid/graphics/Rect;->left:I

    #@39
    if-lt v3, v10, :cond_58

    #@3b
    iget v10, v8, Landroid/graphics/Rect;->right:I

    #@3d
    if-gt v3, v10, :cond_58

    #@3f
    .line 635
    const/4 v6, 0x0

    #@40
    .line 641
    .local v6, distanceX:I
    :goto_40
    iget v10, v8, Landroid/graphics/Rect;->top:I

    #@42
    if-lt v4, v10, :cond_6c

    #@44
    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    #@46
    if-gt v4, v10, :cond_6c

    #@48
    .line 642
    const/4 v7, 0x0

    #@49
    .line 647
    .local v7, distanceY:I
    :goto_49
    mul-int v10, v6, v6

    #@4b
    mul-int v11, v7, v7

    #@4d
    add-int v5, v10, v11

    #@4f
    .line 649
    .local v5, distanceSq:I
    iget v10, p0, Landroid/widget/ZoomButtonsController;->mTouchPaddingScaledSq:I

    #@51
    if-ge v5, v10, :cond_2a

    #@53
    if-ge v5, v2, :cond_2a

    #@55
    .line 651
    move-object v1, v0

    #@56
    .line 652
    move v2, v5

    #@57
    goto :goto_2a

    #@58
    .line 637
    .end local v5           #distanceSq:I
    .end local v6           #distanceX:I
    .end local v7           #distanceY:I
    :cond_58
    iget v10, v8, Landroid/graphics/Rect;->left:I

    #@5a
    sub-int/2addr v10, v3

    #@5b
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@5e
    move-result v10

    #@5f
    iget v11, v8, Landroid/graphics/Rect;->right:I

    #@61
    sub-int v11, v3, v11

    #@63
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    #@66
    move-result v11

    #@67
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@6a
    move-result v6

    #@6b
    .restart local v6       #distanceX:I
    goto :goto_40

    #@6c
    .line 644
    :cond_6c
    iget v10, v8, Landroid/graphics/Rect;->top:I

    #@6e
    sub-int/2addr v10, v4

    #@6f
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@72
    move-result v10

    #@73
    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    #@75
    sub-int v11, v4, v11

    #@77
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    #@7a
    move-result v11

    #@7b
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@7e
    move-result v7

    #@7f
    .restart local v7       #distanceY:I
    goto :goto_49

    #@80
    .end local v0           #child:Landroid/view/View;
    .end local v6           #distanceX:I
    .end local v7           #distanceY:I
    :cond_80
    move-object v0, v1

    #@81
    .line 656
    goto :goto_36
.end method

.method private isInterestingKey(I)Z
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 515
    sparse-switch p1, :sswitch_data_8

    #@3
    .line 525
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 523
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 515
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x4 -> :sswitch_5
        0x13 -> :sswitch_5
        0x14 -> :sswitch_5
        0x15 -> :sswitch_5
        0x16 -> :sswitch_5
        0x17 -> :sswitch_5
        0x42 -> :sswitch_5
    .end sparse-switch
.end method

.method private onContainerKey(Landroid/view/KeyEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 476
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v1

    #@6
    .line 477
    .local v1, keyCode:I
    invoke-direct {p0, v1}, Landroid/widget/ZoomButtonsController;->isInterestingKey(I)Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_48

    #@c
    .line 479
    const/4 v5, 0x4

    #@d
    if-ne v1, v5, :cond_41

    #@f
    .line 480
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@12
    move-result v5

    #@13
    if-nez v5, :cond_2b

    #@15
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@18
    move-result v5

    #@19
    if-nez v5, :cond_2b

    #@1b
    .line 482
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@1d
    if-eqz v4, :cond_2a

    #@1f
    .line 483
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@21
    invoke-virtual {v4}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@24
    move-result-object v0

    #@25
    .line 484
    .local v0, ds:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_2a

    #@27
    .line 485
    invoke-virtual {v0, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@2a
    .line 510
    .end local v0           #ds:Landroid/view/KeyEvent$DispatcherState;
    :cond_2a
    :goto_2a
    return v3

    #@2b
    .line 489
    :cond_2b
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@2e
    move-result v5

    #@2f
    if-ne v5, v3, :cond_46

    #@31
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isTracking()Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_46

    #@37
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    #@3a
    move-result v5

    #@3b
    if-nez v5, :cond_46

    #@3d
    .line 491
    invoke-virtual {p0, v4}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    #@40
    goto :goto_2a

    #@41
    .line 496
    :cond_41
    sget v3, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@43
    invoke-direct {p0, v3}, Landroid/widget/ZoomButtonsController;->dismissControlsDelayed(I)V

    #@46
    :cond_46
    move v3, v4

    #@47
    .line 500
    goto :goto_2a

    #@48
    .line 504
    :cond_48
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@4a
    invoke-virtual {v4}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@4d
    move-result-object v2

    #@4e
    .line 505
    .local v2, viewRoot:Landroid/view/ViewRootImpl;
    if-eqz v2, :cond_2a

    #@50
    .line 506
    invoke-virtual {v2, p1}, Landroid/view/ViewRootImpl;->dispatchKey(Landroid/view/KeyEvent;)V

    #@53
    goto :goto_2a
.end method

.method private onPostConfigurationChanged()V
    .registers 2

    #@0
    .prologue
    .line 660
    sget v0, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@2
    invoke-direct {p0, v0}, Landroid/widget/ZoomButtonsController;->dismissControlsDelayed(I)V

    #@5
    .line 661
    invoke-direct {p0}, Landroid/widget/ZoomButtonsController;->refreshPositioningVariables()V

    #@8
    .line 662
    return-void
.end method

.method private refreshPositioningVariables()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 448
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@4
    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@7
    move-result-object v4

    #@8
    if-nez v4, :cond_b

    #@a
    .line 472
    :cond_a
    :goto_a
    return-void

    #@b
    .line 451
    :cond_b
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@d
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    #@10
    move-result v1

    #@11
    .line 452
    .local v1, ownerHeight:I
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@13
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    #@16
    move-result v3

    #@17
    .line 454
    .local v3, ownerWidth:I
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@19
    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getHeight()I

    #@1c
    move-result v4

    #@1d
    sub-int v0, v1, v4

    #@1f
    .line 457
    .local v0, containerOwnerYOffset:I
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@21
    iget-object v5, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@23
    invoke-virtual {v4, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    #@26
    .line 458
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@28
    iget-object v5, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@2a
    aget v5, v5, v6

    #@2c
    aput v5, v4, v6

    #@2e
    .line 459
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@30
    iget-object v5, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@32
    aget v5, v5, v7

    #@34
    add-int/2addr v5, v0

    #@35
    aput v5, v4, v7

    #@37
    .line 461
    iget-object v2, p0, Landroid/widget/ZoomButtonsController;->mTempIntArray:[I

    #@39
    .line 462
    .local v2, ownerViewWindowLoc:[I
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@3b
    invoke-virtual {v4, v2}, Landroid/view/View;->getLocationInWindow([I)V

    #@3e
    .line 465
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@40
    aget v5, v2, v6

    #@42
    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    #@44
    .line 466
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@46
    iput v3, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@48
    .line 467
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@4a
    aget v5, v2, v7

    #@4c
    add-int/2addr v5, v0

    #@4d
    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    #@4f
    .line 468
    iget-boolean v4, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@51
    if-eqz v4, :cond_a

    #@53
    .line 469
    iget-object v4, p0, Landroid/widget/ZoomButtonsController;->mWindowManager:Landroid/view/WindowManager;

    #@55
    iget-object v5, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@57
    iget-object v6, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@59
    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@5c
    goto :goto_a
.end method

.method private setTouchTargetView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 600
    iput-object p1, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetView:Landroid/view/View;

    #@2
    .line 601
    if-eqz p1, :cond_9

    #@4
    .line 602
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetWindowLocation:[I

    #@6
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    #@9
    .line 604
    :cond_9
    return-void
.end method


# virtual methods
.method public getContainer()Landroid/view/ViewGroup;
    .registers 2

    #@0
    .prologue
    .line 427
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method public getZoomControls()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@2
    return-object v0
.end method

.method public isAutoDismissed()Z
    .registers 2

    #@0
    .prologue
    .line 317
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mAutoDismissControls:Z

    #@2
    return v0
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 335
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@2
    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    const/high16 v12, -0x3e60

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v11, 0x0

    #@4
    const/4 v8, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 534
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@9
    move-result v0

    #@a
    .line 536
    .local v0, action:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    #@d
    move-result v9

    #@e
    if-le v9, v8, :cond_11

    #@10
    .line 595
    :cond_10
    :goto_10
    return v4

    #@11
    .line 541
    :cond_11
    iget-boolean v9, p0, Landroid/widget/ZoomButtonsController;->mReleaseTouchListenerOnUp:Z

    #@13
    if-eqz v9, :cond_26

    #@15
    .line 543
    if-eq v0, v8, :cond_1a

    #@17
    const/4 v9, 0x3

    #@18
    if-ne v0, v9, :cond_24

    #@1a
    .line 544
    :cond_1a
    iget-object v9, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@1c
    invoke-virtual {v9, v10}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@1f
    .line 545
    invoke-direct {p0, v10}, Landroid/widget/ZoomButtonsController;->setTouchTargetView(Landroid/view/View;)V

    #@22
    .line 546
    iput-boolean v4, p0, Landroid/widget/ZoomButtonsController;->mReleaseTouchListenerOnUp:Z

    #@24
    :cond_24
    move v4, v8

    #@25
    .line 550
    goto :goto_10

    #@26
    .line 553
    :cond_26
    sget v9, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@28
    invoke-direct {p0, v9}, Landroid/widget/ZoomButtonsController;->dismissControlsDelayed(I)V

    #@2b
    .line 555
    iget-object v5, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetView:Landroid/view/View;

    #@2d
    .line 557
    .local v5, targetView:Landroid/view/View;
    packed-switch v0, :pswitch_data_98

    #@30
    .line 569
    :goto_30
    :pswitch_30
    if-eqz v5, :cond_10

    #@32
    .line 571
    iget-object v9, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@34
    aget v9, v9, v4

    #@36
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetWindowLocation:[I

    #@38
    aget v10, v10, v4

    #@3a
    add-int v6, v9, v10

    #@3c
    .line 572
    .local v6, targetViewRawX:I
    iget-object v9, p0, Landroid/widget/ZoomButtonsController;->mContainerRawLocation:[I

    #@3e
    aget v9, v9, v8

    #@40
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetWindowLocation:[I

    #@42
    aget v10, v10, v8

    #@44
    add-int v7, v9, v10

    #@46
    .line 574
    .local v7, targetViewRawY:I
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@49
    move-result-object v1

    #@4a
    .line 577
    .local v1, containerEvent:Landroid/view/MotionEvent;
    iget-object v9, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@4c
    aget v9, v9, v4

    #@4e
    sub-int/2addr v9, v6

    #@4f
    int-to-float v9, v9

    #@50
    iget-object v10, p0, Landroid/widget/ZoomButtonsController;->mOwnerViewRawLocation:[I

    #@52
    aget v8, v10, v8

    #@54
    sub-int/2addr v8, v7

    #@55
    int-to-float v8, v8

    #@56
    invoke-virtual {v1, v9, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@59
    .line 582
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getX()F

    #@5c
    move-result v2

    #@5d
    .line 583
    .local v2, containerX:F
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getY()F

    #@60
    move-result v3

    #@61
    .line 584
    .local v3, containerY:F
    cmpg-float v8, v2, v11

    #@63
    if-gez v8, :cond_6d

    #@65
    cmpl-float v8, v2, v12

    #@67
    if-lez v8, :cond_6d

    #@69
    .line 585
    neg-float v8, v2

    #@6a
    invoke-virtual {v1, v8, v11}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@6d
    .line 587
    :cond_6d
    cmpg-float v8, v3, v11

    #@6f
    if-gez v8, :cond_79

    #@71
    cmpl-float v8, v3, v12

    #@73
    if-lez v8, :cond_79

    #@75
    .line 588
    neg-float v8, v3

    #@76
    invoke-virtual {v1, v11, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@79
    .line 590
    :cond_79
    invoke-virtual {v5, v1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@7c
    move-result v4

    #@7d
    .line 591
    .local v4, retValue:Z
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@80
    goto :goto_10

    #@81
    .line 559
    .end local v1           #containerEvent:Landroid/view/MotionEvent;
    .end local v2           #containerX:F
    .end local v3           #containerY:F
    .end local v4           #retValue:Z
    .end local v6           #targetViewRawX:I
    .end local v7           #targetViewRawY:I
    :pswitch_81
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    #@84
    move-result v9

    #@85
    float-to-int v9, v9

    #@86
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    #@89
    move-result v10

    #@8a
    float-to-int v10, v10

    #@8b
    invoke-direct {p0, v9, v10}, Landroid/widget/ZoomButtonsController;->findViewForTouch(II)Landroid/view/View;

    #@8e
    move-result-object v5

    #@8f
    .line 560
    invoke-direct {p0, v5}, Landroid/widget/ZoomButtonsController;->setTouchTargetView(Landroid/view/View;)V

    #@92
    goto :goto_30

    #@93
    .line 565
    :pswitch_93
    invoke-direct {p0, v10}, Landroid/widget/ZoomButtonsController;->setTouchTargetView(Landroid/view/View;)V

    #@96
    goto :goto_30

    #@97
    .line 557
    nop

    #@98
    :pswitch_data_98
    .packed-switch 0x0
        :pswitch_81
        :pswitch_93
        :pswitch_30
        :pswitch_93
    .end packed-switch
.end method

.method public setAutoDismissed(Z)V
    .registers 3
    .parameter "autoDismiss"

    #@0
    .prologue
    .line 325
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mAutoDismissControls:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 327
    :goto_4
    return-void

    #@5
    .line 326
    :cond_5
    iput-boolean p1, p0, Landroid/widget/ZoomButtonsController;->mAutoDismissControls:Z

    #@7
    goto :goto_4
.end method

.method public setFocusable(Z)V
    .registers 6
    .parameter "focusable"

    #@0
    .prologue
    .line 299
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@2
    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@4
    .line 300
    .local v0, oldFlags:I
    if-eqz p1, :cond_22

    #@6
    .line 301
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@8
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@a
    and-int/lit8 v2, v2, -0x9

    #@c
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e
    .line 306
    :goto_e
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@10
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@12
    if-eq v1, v0, :cond_21

    #@14
    iget-boolean v1, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@16
    if-eqz v1, :cond_21

    #@18
    .line 307
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mWindowManager:Landroid/view/WindowManager;

    #@1a
    iget-object v2, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@1c
    iget-object v3, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@1e
    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@21
    .line 309
    :cond_21
    return-void

    #@22
    .line 303
    :cond_22
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@24
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@26
    or-int/lit8 v2, v2, 0x8

    #@28
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2a
    goto :goto_e
.end method

.method public setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 288
    iput-object p1, p0, Landroid/widget/ZoomButtonsController;->mCallback:Landroid/widget/ZoomButtonsController$OnZoomListener;

    #@2
    .line 289
    return-void
.end method

.method public setVisible(Z)V
    .registers 6
    .parameter "visible"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    const/4 v3, 0x0

    #@2
    .line 345
    if-eqz p1, :cond_1f

    #@4
    .line 346
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    if-nez v0, :cond_1a

    #@c
    .line 352
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 353
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@19
    .line 415
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 358
    :cond_1a
    sget v0, Landroid/widget/ZoomButtonsController;->ZOOM_CONTROLS_TIMEOUT:I

    #@1c
    invoke-direct {p0, v0}, Landroid/widget/ZoomButtonsController;->dismissControlsDelayed(I)V

    #@1f
    .line 361
    :cond_1f
    iget-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@21
    if-eq v0, p1, :cond_19

    #@23
    .line 364
    iput-boolean p1, p0, Landroid/widget/ZoomButtonsController;->mIsVisible:Z

    #@25
    .line 366
    if-eqz p1, :cond_63

    #@27
    .line 367
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@29
    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@2b
    if-nez v0, :cond_37

    #@2d
    .line 368
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@2f
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@31
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@34
    move-result-object v1

    #@35
    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@37
    .line 371
    :cond_37
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mWindowManager:Landroid/view/WindowManager;

    #@39
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@3b
    iget-object v2, p0, Landroid/widget/ZoomButtonsController;->mContainerLayoutParams:Landroid/view/WindowManager$LayoutParams;

    #@3d
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@40
    .line 373
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mPostedVisibleInitializer:Ljava/lang/Runnable;

    #@42
    if-nez v0, :cond_4b

    #@44
    .line 374
    new-instance v0, Landroid/widget/ZoomButtonsController$5;

    #@46
    invoke-direct {v0, p0}, Landroid/widget/ZoomButtonsController$5;-><init>(Landroid/widget/ZoomButtonsController;)V

    #@49
    iput-object v0, p0, Landroid/widget/ZoomButtonsController;->mPostedVisibleInitializer:Ljava/lang/Runnable;

    #@4b
    .line 385
    :cond_4b
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@4d
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mPostedVisibleInitializer:Ljava/lang/Runnable;

    #@4f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@52
    .line 388
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@54
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    #@56
    iget-object v2, p0, Landroid/widget/ZoomButtonsController;->mConfigurationChangedFilter:Landroid/content/IntentFilter;

    #@58
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5b
    .line 391
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@5d
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@60
    .line 392
    iput-boolean v3, p0, Landroid/widget/ZoomButtonsController;->mReleaseTouchListenerOnUp:Z

    #@62
    goto :goto_19

    #@63
    .line 396
    :cond_63
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mTouchTargetView:Landroid/view/View;

    #@65
    if-eqz v0, :cond_89

    #@67
    .line 399
    const/4 v0, 0x1

    #@68
    iput-boolean v0, p0, Landroid/widget/ZoomButtonsController;->mReleaseTouchListenerOnUp:Z

    #@6a
    .line 405
    :goto_6a
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mContext:Landroid/content/Context;

    #@6c
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    #@6e
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@71
    .line 407
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mWindowManager:Landroid/view/WindowManager;

    #@73
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mContainer:Landroid/widget/FrameLayout;

    #@75
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@78
    .line 408
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mHandler:Landroid/os/Handler;

    #@7a
    iget-object v1, p0, Landroid/widget/ZoomButtonsController;->mPostedVisibleInitializer:Ljava/lang/Runnable;

    #@7c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7f
    .line 410
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mCallback:Landroid/widget/ZoomButtonsController$OnZoomListener;

    #@81
    if-eqz v0, :cond_19

    #@83
    .line 411
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mCallback:Landroid/widget/ZoomButtonsController$OnZoomListener;

    #@85
    invoke-interface {v0, v3}, Landroid/widget/ZoomButtonsController$OnZoomListener;->onVisibilityChanged(Z)V

    #@88
    goto :goto_19

    #@89
    .line 401
    :cond_89
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mOwnerView:Landroid/view/View;

    #@8b
    const/4 v1, 0x0

    #@8c
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@8f
    goto :goto_6a
.end method

.method public setZoomInEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomControls;->setIsZoomInEnabled(Z)V

    #@5
    .line 222
    return-void
.end method

.method public setZoomOutEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomControls;->setIsZoomOutEnabled(Z)V

    #@5
    .line 231
    return-void
.end method

.method public setZoomSpeed(J)V
    .registers 4
    .parameter "speed"

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/widget/ZoomButtonsController;->mControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/ZoomControls;->setZoomSpeed(J)V

    #@5
    .line 240
    return-void
.end method
