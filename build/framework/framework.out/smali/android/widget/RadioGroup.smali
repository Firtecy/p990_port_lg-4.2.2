.class public Landroid/widget/RadioGroup;
.super Landroid/widget/LinearLayout;
.source "RadioGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RadioGroup$1;,
        Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;,
        Landroid/widget/RadioGroup$CheckedStateTracker;,
        Landroid/widget/RadioGroup$OnCheckedChangeListener;,
        Landroid/widget/RadioGroup$LayoutParams;
    }
.end annotation


# instance fields
.field private mCheckedId:I

.field private mChildOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mOnCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private mPassThroughListener:Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;

.field private mProtectFromCheckedChange:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 56
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@6
    .line 60
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@9
    .line 69
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Landroid/widget/RadioGroup;->setOrientation(I)V

    #@d
    .line 70
    invoke-direct {p0}, Landroid/widget/RadioGroup;->init()V

    #@10
    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, -0x1

    #@3
    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@6
    .line 56
    iput v5, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@8
    .line 60
    iput-boolean v6, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@a
    .line 81
    sget-object v3, Lcom/android/internal/R$styleable;->RadioGroup:[I

    #@c
    const v4, 0x101007e

    #@f
    invoke-virtual {p1, p2, v3, v4, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@12
    move-result-object v0

    #@13
    .line 84
    .local v0, attributes:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@16
    move-result v2

    #@17
    .line 85
    .local v2, value:I
    if-eq v2, v5, :cond_1b

    #@19
    .line 86
    iput v2, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@1b
    .line 89
    :cond_1b
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1e
    move-result v1

    #@1f
    .line 90
    .local v1, index:I
    invoke-virtual {p0, v1}, Landroid/widget/RadioGroup;->setOrientation(I)V

    #@22
    .line 92
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@25
    .line 93
    invoke-direct {p0}, Landroid/widget/RadioGroup;->init()V

    #@28
    .line 94
    return-void
.end method

.method static synthetic access$300(Landroid/widget/RadioGroup;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-boolean v0, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/widget/RadioGroup;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/widget/RadioGroup;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/widget/RadioGroup;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/RadioGroup;->setCheckedStateForView(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/RadioGroup;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/RadioGroup;->setCheckedId(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/widget/RadioGroup;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/widget/RadioGroup;->mChildOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@2
    return-object v0
.end method

.method private init()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 97
    new-instance v0, Landroid/widget/RadioGroup$CheckedStateTracker;

    #@3
    invoke-direct {v0, p0, v1}, Landroid/widget/RadioGroup$CheckedStateTracker;-><init>(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup$1;)V

    #@6
    iput-object v0, p0, Landroid/widget/RadioGroup;->mChildOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    #@8
    .line 98
    new-instance v0, Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;

    #@a
    invoke-direct {v0, p0, v1}, Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;-><init>(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup$1;)V

    #@d
    iput-object v0, p0, Landroid/widget/RadioGroup;->mPassThroughListener:Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;

    #@f
    .line 99
    iget-object v0, p0, Landroid/widget/RadioGroup;->mPassThroughListener:Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;

    #@11
    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@14
    .line 100
    return-void
.end method

.method private setCheckedId(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 172
    iput p1, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@2
    .line 173
    iget-object v0, p0, Landroid/widget/RadioGroup;->mOnCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 174
    iget-object v0, p0, Landroid/widget/RadioGroup;->mOnCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    #@8
    iget v1, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@a
    invoke-interface {v0, p0, v1}, Landroid/widget/RadioGroup$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/RadioGroup;I)V

    #@d
    .line 176
    :cond_d
    return-void
.end method

.method private setCheckedStateForView(IZ)V
    .registers 5
    .parameter "viewId"
    .parameter "checked"

    #@0
    .prologue
    .line 179
    invoke-virtual {p0, p1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 180
    .local v0, checkedView:Landroid/view/View;
    if-eqz v0, :cond_f

    #@6
    instance-of v1, v0, Landroid/widget/RadioButton;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 181
    check-cast v0, Landroid/widget/RadioButton;

    #@c
    .end local v0           #checkedView:Landroid/view/View;
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setChecked(Z)V

    #@f
    .line 183
    :cond_f
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 8
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 129
    instance-of v1, p1, Landroid/widget/RadioButton;

    #@3
    if-eqz v1, :cond_24

    #@5
    move-object v0, p1

    #@6
    .line 130
    check-cast v0, Landroid/widget/RadioButton;

    #@8
    .line 131
    .local v0, button:Landroid/widget/RadioButton;
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_24

    #@e
    .line 132
    const/4 v1, 0x1

    #@f
    iput-boolean v1, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@11
    .line 133
    iget v1, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@13
    const/4 v2, -0x1

    #@14
    if-eq v1, v2, :cond_1b

    #@16
    .line 134
    iget v1, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@18
    invoke-direct {p0, v1, v3}, Landroid/widget/RadioGroup;->setCheckedStateForView(IZ)V

    #@1b
    .line 136
    :cond_1b
    iput-boolean v3, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@1d
    .line 137
    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    #@20
    move-result v1

    #@21
    invoke-direct {p0, v1}, Landroid/widget/RadioGroup;->setCheckedId(I)V

    #@24
    .line 141
    .end local v0           #button:Landroid/widget/RadioButton;
    :cond_24
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@27
    .line 142
    return-void
.end method

.method public check(I)V
    .registers 5
    .parameter "id"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 156
    if-eq p1, v2, :cond_8

    #@3
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@5
    if-ne p1, v0, :cond_8

    #@7
    .line 169
    :goto_7
    return-void

    #@8
    .line 160
    :cond_8
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@a
    if-eq v0, v2, :cond_12

    #@c
    .line 161
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@e
    const/4 v1, 0x0

    #@f
    invoke-direct {p0, v0, v1}, Landroid/widget/RadioGroup;->setCheckedStateForView(IZ)V

    #@12
    .line 164
    :cond_12
    if-eq p1, v2, :cond_18

    #@14
    .line 165
    const/4 v0, 0x1

    #@15
    invoke-direct {p0, p1, v0}, Landroid/widget/RadioGroup;->setCheckedStateForView(IZ)V

    #@18
    .line 168
    :cond_18
    invoke-direct {p0, p1}, Landroid/widget/RadioGroup;->setCheckedId(I)V

    #@1b
    goto :goto_7
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 235
    instance-of v0, p1, Landroid/widget/RadioGroup$LayoutParams;

    #@2
    return v0
.end method

.method public clearCheck()V
    .registers 2

    #@0
    .prologue
    .line 209
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/RadioGroup;->check(I)V

    #@4
    .line 210
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/widget/RadioGroup;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 240
    new-instance v0, Landroid/widget/RadioGroup$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/widget/RadioGroup$LayoutParams;-><init>(II)V

    #@6
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0, p1}, Landroid/widget/RadioGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RadioGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0, p1}, Landroid/widget/RadioGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RadioGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RadioGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 227
    new-instance v0, Landroid/widget/RadioGroup$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/RadioGroup;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/RadioGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public getCheckedRadioButtonId()I
    .registers 2

    #@0
    .prologue
    .line 197
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@2
    return v0
.end method

.method protected onFinishInflate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 116
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    #@4
    .line 119
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@6
    const/4 v1, -0x1

    #@7
    if-eq v0, v1, :cond_18

    #@9
    .line 120
    iput-boolean v2, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@b
    .line 121
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@d
    invoke-direct {p0, v0, v2}, Landroid/widget/RadioGroup;->setCheckedStateForView(IZ)V

    #@10
    .line 122
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/widget/RadioGroup;->mProtectFromCheckedChange:Z

    #@13
    .line 123
    iget v0, p0, Landroid/widget/RadioGroup;->mCheckedId:I

    #@15
    invoke-direct {p0, v0}, Landroid/widget/RadioGroup;->setCheckedId(I)V

    #@18
    .line 125
    :cond_18
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 245
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 246
    const-class v0, Landroid/widget/RadioGroup;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 247
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 251
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 252
    const-class v0, Landroid/widget/RadioGroup;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 253
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 219
    iput-object p1, p0, Landroid/widget/RadioGroup;->mOnCheckedChangeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    #@2
    .line 220
    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/widget/RadioGroup;->mPassThroughListener:Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;

    #@2
    invoke-static {v0, p1}, Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;->access$202(Landroid/widget/RadioGroup$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@5
    .line 109
    return-void
.end method
