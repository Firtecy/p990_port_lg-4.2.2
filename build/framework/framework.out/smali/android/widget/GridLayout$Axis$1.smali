.class Landroid/widget/GridLayout$Axis$1;
.super Ljava/lang/Object;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/GridLayout$Axis;->topologicalSort([Landroid/widget/GridLayout$Arc;)[Landroid/widget/GridLayout$Arc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field arcsByVertex:[[Landroid/widget/GridLayout$Arc;

.field cursor:I

.field result:[Landroid/widget/GridLayout$Arc;

.field final synthetic this$1:Landroid/widget/GridLayout$Axis;

.field final synthetic val$arcs:[Landroid/widget/GridLayout$Arc;

.field visited:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1382
    const-class v0, Landroid/widget/GridLayout;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/widget/GridLayout$Axis$1;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method constructor <init>(Landroid/widget/GridLayout$Axis;[Landroid/widget/GridLayout$Arc;)V
    .registers 5
    .parameter
    .parameter

    #@0
    .prologue
    .line 1382
    iput-object p1, p0, Landroid/widget/GridLayout$Axis$1;->this$1:Landroid/widget/GridLayout$Axis;

    #@2
    iput-object p2, p0, Landroid/widget/GridLayout$Axis$1;->val$arcs:[Landroid/widget/GridLayout$Arc;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 1383
    iget-object v0, p0, Landroid/widget/GridLayout$Axis$1;->val$arcs:[Landroid/widget/GridLayout$Arc;

    #@9
    array-length v0, v0

    #@a
    new-array v0, v0, [Landroid/widget/GridLayout$Arc;

    #@c
    iput-object v0, p0, Landroid/widget/GridLayout$Axis$1;->result:[Landroid/widget/GridLayout$Arc;

    #@e
    .line 1384
    iget-object v0, p0, Landroid/widget/GridLayout$Axis$1;->result:[Landroid/widget/GridLayout$Arc;

    #@10
    array-length v0, v0

    #@11
    add-int/lit8 v0, v0, -0x1

    #@13
    iput v0, p0, Landroid/widget/GridLayout$Axis$1;->cursor:I

    #@15
    .line 1385
    iget-object v0, p0, Landroid/widget/GridLayout$Axis$1;->this$1:Landroid/widget/GridLayout$Axis;

    #@17
    iget-object v1, p0, Landroid/widget/GridLayout$Axis$1;->val$arcs:[Landroid/widget/GridLayout$Arc;

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout$Axis;->groupArcsByFirstVertex([Landroid/widget/GridLayout$Arc;)[[Landroid/widget/GridLayout$Arc;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Landroid/widget/GridLayout$Axis$1;->arcsByVertex:[[Landroid/widget/GridLayout$Arc;

    #@1f
    .line 1386
    iget-object v0, p0, Landroid/widget/GridLayout$Axis$1;->this$1:Landroid/widget/GridLayout$Axis;

    #@21
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@24
    move-result v0

    #@25
    add-int/lit8 v0, v0, 0x1

    #@27
    new-array v0, v0, [I

    #@29
    iput-object v0, p0, Landroid/widget/GridLayout$Axis$1;->visited:[I

    #@2b
    return-void
.end method


# virtual methods
.method sort()[Landroid/widget/GridLayout$Arc;
    .registers 5

    #@0
    .prologue
    .line 1411
    const/4 v1, 0x0

    #@1
    .local v1, loc:I
    iget-object v2, p0, Landroid/widget/GridLayout$Axis$1;->arcsByVertex:[[Landroid/widget/GridLayout$Arc;

    #@3
    array-length v0, v2

    #@4
    .local v0, N:I
    :goto_4
    if-ge v1, v0, :cond_c

    #@6
    .line 1412
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout$Axis$1;->walk(I)V

    #@9
    .line 1411
    add-int/lit8 v1, v1, 0x1

    #@b
    goto :goto_4

    #@c
    .line 1414
    :cond_c
    sget-boolean v2, Landroid/widget/GridLayout$Axis$1;->$assertionsDisabled:Z

    #@e
    if-nez v2, :cond_1b

    #@10
    iget v2, p0, Landroid/widget/GridLayout$Axis$1;->cursor:I

    #@12
    const/4 v3, -0x1

    #@13
    if-eq v2, v3, :cond_1b

    #@15
    new-instance v2, Ljava/lang/AssertionError;

    #@17
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@1a
    throw v2

    #@1b
    .line 1415
    :cond_1b
    iget-object v2, p0, Landroid/widget/GridLayout$Axis$1;->result:[Landroid/widget/GridLayout$Arc;

    #@1d
    return-object v2
.end method

.method walk(I)V
    .registers 9
    .parameter "loc"

    #@0
    .prologue
    .line 1389
    iget-object v4, p0, Landroid/widget/GridLayout$Axis$1;->visited:[I

    #@2
    aget v4, v4, p1

    #@4
    packed-switch v4, :pswitch_data_3c

    #@7
    .line 1408
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1391
    :pswitch_8
    iget-object v4, p0, Landroid/widget/GridLayout$Axis$1;->visited:[I

    #@a
    const/4 v5, 0x1

    #@b
    aput v5, v4, p1

    #@d
    .line 1392
    iget-object v4, p0, Landroid/widget/GridLayout$Axis$1;->arcsByVertex:[[Landroid/widget/GridLayout$Arc;

    #@f
    aget-object v1, v4, p1

    #@11
    .local v1, arr$:[Landroid/widget/GridLayout$Arc;
    array-length v3, v1

    #@12
    .local v3, len$:I
    const/4 v2, 0x0

    #@13
    .local v2, i$:I
    :goto_13
    if-ge v2, v3, :cond_2b

    #@15
    aget-object v0, v1, v2

    #@17
    .line 1393
    .local v0, arc:Landroid/widget/GridLayout$Arc;
    iget-object v4, v0, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@19
    iget v4, v4, Landroid/widget/GridLayout$Interval;->max:I

    #@1b
    invoke-virtual {p0, v4}, Landroid/widget/GridLayout$Axis$1;->walk(I)V

    #@1e
    .line 1394
    iget-object v4, p0, Landroid/widget/GridLayout$Axis$1;->result:[Landroid/widget/GridLayout$Arc;

    #@20
    iget v5, p0, Landroid/widget/GridLayout$Axis$1;->cursor:I

    #@22
    add-int/lit8 v6, v5, -0x1

    #@24
    iput v6, p0, Landroid/widget/GridLayout$Axis$1;->cursor:I

    #@26
    aput-object v0, v4, v5

    #@28
    .line 1392
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_13

    #@2b
    .line 1396
    .end local v0           #arc:Landroid/widget/GridLayout$Arc;
    :cond_2b
    iget-object v4, p0, Landroid/widget/GridLayout$Axis$1;->visited:[I

    #@2d
    const/4 v5, 0x2

    #@2e
    aput v5, v4, p1

    #@30
    goto :goto_7

    #@31
    .line 1401
    .end local v1           #arr$:[Landroid/widget/GridLayout$Arc;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :pswitch_31
    sget-boolean v4, Landroid/widget/GridLayout$Axis$1;->$assertionsDisabled:Z

    #@33
    if-nez v4, :cond_7

    #@35
    new-instance v4, Ljava/lang/AssertionError;

    #@37
    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    #@3a
    throw v4

    #@3b
    .line 1389
    nop

    #@3c
    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_31
    .end packed-switch
.end method
