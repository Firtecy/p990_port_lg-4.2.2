.class Landroid/widget/PopupWindow$1;
.super Ljava/lang/Object;
.source "PopupWindow.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/PopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/PopupWindow;


# direct methods
.method constructor <init>(Landroid/widget/PopupWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 137
    iput-object p1, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onScrollChanged()V
    .registers 9

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 139
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@3
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$000(Landroid/widget/PopupWindow;)Ljava/lang/ref/WeakReference;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_55

    #@9
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@b
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$000(Landroid/widget/PopupWindow;)Ljava/lang/ref/WeakReference;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/view/View;

    #@15
    move-object v6, v0

    #@16
    .line 140
    .local v6, anchor:Landroid/view/View;
    :goto_16
    if-eqz v6, :cond_54

    #@18
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@1a
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$100(Landroid/widget/PopupWindow;)Landroid/view/View;

    #@1d
    move-result-object v0

    #@1e
    if-eqz v0, :cond_54

    #@20
    .line 142
    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_54

    #@26
    .line 144
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@28
    invoke-static {v0}, Landroid/widget/PopupWindow;->access$100(Landroid/widget/PopupWindow;)Landroid/view/View;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2f
    move-result-object v7

    #@30
    check-cast v7, Landroid/view/WindowManager$LayoutParams;

    #@32
    .line 147
    .local v7, p:Landroid/view/WindowManager$LayoutParams;
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@34
    iget-object v1, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@36
    iget-object v2, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@38
    invoke-static {v2}, Landroid/widget/PopupWindow;->access$200(Landroid/widget/PopupWindow;)I

    #@3b
    move-result v2

    #@3c
    iget-object v4, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@3e
    invoke-static {v4}, Landroid/widget/PopupWindow;->access$300(Landroid/widget/PopupWindow;)I

    #@41
    move-result v4

    #@42
    invoke-static {v1, v6, v7, v2, v4}, Landroid/widget/PopupWindow;->access$400(Landroid/widget/PopupWindow;Landroid/view/View;Landroid/view/WindowManager$LayoutParams;II)Z

    #@45
    move-result v1

    #@46
    invoke-static {v0, v1}, Landroid/widget/PopupWindow;->access$500(Landroid/widget/PopupWindow;Z)V

    #@49
    .line 148
    iget-object v0, p0, Landroid/widget/PopupWindow$1;->this$0:Landroid/widget/PopupWindow;

    #@4b
    iget v1, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    #@4d
    iget v2, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    #@4f
    const/4 v5, 0x1

    #@50
    move v4, v3

    #@51
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(IIIIZ)V

    #@54
    .line 152
    .end local v7           #p:Landroid/view/WindowManager$LayoutParams;
    :cond_54
    return-void

    #@55
    .line 139
    .end local v6           #anchor:Landroid/view/View;
    :cond_55
    const/4 v6, 0x0

    #@56
    goto :goto_16
.end method
