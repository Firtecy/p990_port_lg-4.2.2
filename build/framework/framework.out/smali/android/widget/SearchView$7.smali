.class Landroid/widget/SearchView$7;
.super Ljava/lang/Object;
.source "SearchView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/SearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/SearchView;


# direct methods
.method constructor <init>(Landroid/widget/SearchView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 904
    iput-object p1, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 10
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 907
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@4
    invoke-static {v3}, Landroid/widget/SearchView;->access$1400(Landroid/widget/SearchView;)Landroid/app/SearchableInfo;

    #@7
    move-result-object v3

    #@8
    if-nez v3, :cond_b

    #@a
    .line 945
    :cond_a
    :goto_a
    return v1

    #@b
    .line 918
    :cond_b
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@d
    invoke-static {v3}, Landroid/widget/SearchView;->access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Landroid/widget/SearchView$SearchAutoComplete;->isPopupShowing()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_2b

    #@17
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@19
    invoke-static {v3}, Landroid/widget/SearchView;->access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Landroid/widget/SearchView$SearchAutoComplete;->getListSelection()I

    #@20
    move-result v3

    #@21
    const/4 v4, -0x1

    #@22
    if-eq v3, v4, :cond_2b

    #@24
    .line 920
    iget-object v1, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@26
    invoke-static {v1, p1, p2, p3}, Landroid/widget/SearchView;->access$1500(Landroid/widget/SearchView;Landroid/view/View;ILandroid/view/KeyEvent;)Z

    #@29
    move-result v1

    #@2a
    goto :goto_a

    #@2b
    .line 925
    :cond_2b
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@2d
    invoke-static {v3}, Landroid/widget/SearchView;->access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;

    #@30
    move-result-object v3

    #@31
    invoke-static {v3}, Landroid/widget/SearchView$SearchAutoComplete;->access$1600(Landroid/widget/SearchView$SearchAutoComplete;)Z

    #@34
    move-result v3

    #@35
    if-nez v3, :cond_a

    #@37
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_a

    #@3d
    .line 926
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@40
    move-result v3

    #@41
    if-ne v3, v2, :cond_60

    #@43
    .line 927
    const/16 v3, 0x42

    #@45
    if-ne p2, v3, :cond_60

    #@47
    .line 928
    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    #@4a
    .line 931
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@4c
    const/4 v4, 0x0

    #@4d
    iget-object v5, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@4f
    invoke-static {v5}, Landroid/widget/SearchView;->access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v3, v1, v4, v5}, Landroid/widget/SearchView;->access$1700(Landroid/widget/SearchView;ILjava/lang/String;Ljava/lang/String;)V

    #@5e
    move v1, v2

    #@5f
    .line 933
    goto :goto_a

    #@60
    .line 936
    :cond_60
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@63
    move-result v3

    #@64
    if-nez v3, :cond_a

    #@66
    .line 937
    iget-object v3, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@68
    invoke-static {v3}, Landroid/widget/SearchView;->access$1400(Landroid/widget/SearchView;)Landroid/app/SearchableInfo;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3, p2}, Landroid/app/SearchableInfo;->findActionKey(I)Landroid/app/SearchableInfo$ActionKeyInfo;

    #@6f
    move-result-object v0

    #@70
    .line 938
    .local v0, actionKey:Landroid/app/SearchableInfo$ActionKeyInfo;
    if-eqz v0, :cond_a

    #@72
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getQueryActionMsg()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    if-eqz v3, :cond_a

    #@78
    .line 939
    iget-object v1, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@7a
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getQueryActionMsg()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    iget-object v4, p0, Landroid/widget/SearchView$7;->this$0:Landroid/widget/SearchView;

    #@80
    invoke-static {v4}, Landroid/widget/SearchView;->access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8b
    move-result-object v4

    #@8c
    invoke-static {v1, p2, v3, v4}, Landroid/widget/SearchView;->access$1700(Landroid/widget/SearchView;ILjava/lang/String;Ljava/lang/String;)V

    #@8f
    move v1, v2

    #@90
    .line 941
    goto/16 :goto_a
.end method
