.class public Landroid/widget/HeaderViewListAdapter;
.super Ljava/lang/Object;
.source "HeaderViewListAdapter.java"

# interfaces
.implements Landroid/widget/WrapperListAdapter;
.implements Landroid/widget/Filterable;


# static fields
.field static final EMPTY_INFO_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAdapter:Landroid/widget/ListAdapter;

.field mAreAllFixedViewsSelectable:Z

.field mFooterViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field mHeaderViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsFilterable:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    sput-object v0, Landroid/widget/HeaderViewListAdapter;->EMPTY_INFO_LIST:Ljava/util/ArrayList;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V
    .registers 5
    .parameter
    .parameter
    .parameter "adapter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 52
    .local p1, headerViewInfos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    .local p2, footerViewInfos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    iput-object p3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@5
    .line 54
    instance-of v0, p3, Landroid/widget/Filterable;

    #@7
    iput-boolean v0, p0, Landroid/widget/HeaderViewListAdapter;->mIsFilterable:Z

    #@9
    .line 56
    if-nez p1, :cond_29

    #@b
    .line 57
    sget-object v0, Landroid/widget/HeaderViewListAdapter;->EMPTY_INFO_LIST:Ljava/util/ArrayList;

    #@d
    iput-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@f
    .line 62
    :goto_f
    if-nez p2, :cond_2c

    #@11
    .line 63
    sget-object v0, Landroid/widget/HeaderViewListAdapter;->EMPTY_INFO_LIST:Ljava/util/ArrayList;

    #@13
    iput-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@15
    .line 68
    :goto_15
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@17
    invoke-direct {p0, v0}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_2f

    #@1d
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@1f
    invoke-direct {p0, v0}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_2f

    #@25
    const/4 v0, 0x1

    #@26
    :goto_26
    iput-boolean v0, p0, Landroid/widget/HeaderViewListAdapter;->mAreAllFixedViewsSelectable:Z

    #@28
    .line 71
    return-void

    #@29
    .line 59
    :cond_29
    iput-object p1, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2b
    goto :goto_f

    #@2c
    .line 65
    :cond_2c
    iput-object p2, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@2e
    goto :goto_15

    #@2f
    .line 68
    :cond_2f
    const/4 v0, 0x0

    #@30
    goto :goto_26
.end method

.method private areAllListInfosSelectable(Ljava/util/ArrayList;)Z
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 86
    .local p1, infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    if-eqz p1, :cond_18

    #@2
    .line 87
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_18

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/widget/ListView$FixedViewInfo;

    #@12
    .line 88
    .local v1, info:Landroid/widget/ListView$FixedViewInfo;
    iget-boolean v2, v1, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    #@14
    if-nez v2, :cond_6

    #@16
    .line 89
    const/4 v2, 0x0

    #@17
    .line 93
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :goto_17
    return v2

    #@18
    :cond_18
    const/4 v2, 0x1

    #@19
    goto :goto_17
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 139
    iget-object v1, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    if-eqz v1, :cond_11

    #@5
    .line 140
    iget-boolean v1, p0, Landroid/widget/HeaderViewListAdapter;->mAreAllFixedViewsSelectable:Z

    #@7
    if-eqz v1, :cond_12

    #@9
    iget-object v1, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@b
    invoke-interface {v1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 142
    :cond_11
    :goto_11
    return v0

    #@12
    .line 140
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public getCount()I
    .registers 3

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 132
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getFootersCount()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@b
    move-result v1

    #@c
    add-int/2addr v0, v1

    #@d
    iget-object v1, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@f
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    #@12
    move-result v1

    #@13
    add-int/2addr v0, v1

    #@14
    .line 134
    :goto_14
    return v0

    #@15
    :cond_15
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getFootersCount()I

    #@18
    move-result v0

    #@19
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@1c
    move-result v1

    #@1d
    add-int/2addr v0, v1

    #@1e
    goto :goto_14
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    #@0
    .prologue
    .line 261
    iget-boolean v0, p0, Landroid/widget/HeaderViewListAdapter;->mIsFilterable:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 262
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    check-cast v0, Landroid/widget/Filterable;

    #@8
    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@b
    move-result-object v0

    #@c
    .line 264
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getFootersCount()I
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getHeadersCount()I
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 7
    .parameter "position"

    #@0
    .prologue
    .line 169
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@3
    move-result v2

    #@4
    .line 170
    .local v2, numHeaders:I
    if-ge p1, v2, :cond_11

    #@6
    .line 171
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@e
    iget-object v3, v3, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    #@10
    .line 185
    :goto_10
    return-object v3

    #@11
    .line 175
    :cond_11
    sub-int v1, p1, v2

    #@13
    .line 176
    .local v1, adjPosition:I
    const/4 v0, 0x0

    #@14
    .line 177
    .local v0, adapterCount:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@16
    if-eqz v3, :cond_27

    #@18
    .line 178
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@1a
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@1d
    move-result v0

    #@1e
    .line 179
    if-ge v1, v0, :cond_27

    #@20
    .line 180
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@22
    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    goto :goto_10

    #@27
    .line 185
    :cond_27
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@29
    sub-int v4, v1, v0

    #@2b
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@31
    iget-object v3, v3, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    #@33
    goto :goto_10
.end method

.method public getItemId(I)J
    .registers 7
    .parameter "position"

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@3
    move-result v2

    #@4
    .line 190
    .local v2, numHeaders:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    if-eqz v3, :cond_1b

    #@8
    if-lt p1, v2, :cond_1b

    #@a
    .line 191
    sub-int v1, p1, v2

    #@c
    .line 192
    .local v1, adjPosition:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@e
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@11
    move-result v0

    #@12
    .line 193
    .local v0, adapterCount:I
    if-ge v1, v0, :cond_1b

    #@14
    .line 194
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@16
    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@19
    move-result-wide v3

    #@1a
    .line 197
    .end local v0           #adapterCount:I
    .end local v1           #adjPosition:I
    :goto_1a
    return-wide v3

    #@1b
    :cond_1b
    const-wide/16 v3, -0x1

    #@1d
    goto :goto_1a
.end method

.method public getItemViewType(I)I
    .registers 6
    .parameter "position"

    #@0
    .prologue
    .line 229
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@3
    move-result v2

    #@4
    .line 230
    .local v2, numHeaders:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    if-eqz v3, :cond_1b

    #@8
    if-lt p1, v2, :cond_1b

    #@a
    .line 231
    sub-int v1, p1, v2

    #@c
    .line 232
    .local v1, adjPosition:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@e
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@11
    move-result v0

    #@12
    .line 233
    .local v0, adapterCount:I
    if-ge v1, v0, :cond_1b

    #@14
    .line 234
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@16
    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@19
    move-result v3

    #@1a
    .line 238
    .end local v0           #adapterCount:I
    .end local v1           #adjPosition:I
    :goto_1a
    return v3

    #@1b
    :cond_1b
    const/4 v3, -0x2

    #@1c
    goto :goto_1a
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 209
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@3
    move-result v2

    #@4
    .line 210
    .local v2, numHeaders:I
    if-ge p1, v2, :cond_11

    #@6
    .line 211
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@e
    iget-object v3, v3, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@10
    .line 225
    :goto_10
    return-object v3

    #@11
    .line 215
    :cond_11
    sub-int v1, p1, v2

    #@13
    .line 216
    .local v1, adjPosition:I
    const/4 v0, 0x0

    #@14
    .line 217
    .local v0, adapterCount:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@16
    if-eqz v3, :cond_27

    #@18
    .line 218
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@1a
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@1d
    move-result v0

    #@1e
    .line 219
    if-ge v1, v0, :cond_27

    #@20
    .line 220
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@22
    invoke-interface {v3, v1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@25
    move-result-object v3

    #@26
    goto :goto_10

    #@27
    .line 225
    :cond_27
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@29
    sub-int v4, v1, v0

    #@2b
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@31
    iget-object v3, v3, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@33
    goto :goto_10
.end method

.method public getViewTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 243
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    #@9
    move-result v0

    #@a
    .line 245
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    goto :goto_a
.end method

.method public getWrappedAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 202
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@9
    move-result v0

    #@a
    .line 204
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isEnabled(I)Z
    .registers 7
    .parameter "position"

    #@0
    .prologue
    .line 148
    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@3
    move-result v2

    #@4
    .line 149
    .local v2, numHeaders:I
    if-ge p1, v2, :cond_11

    #@6
    .line 150
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@e
    iget-boolean v3, v3, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    #@10
    .line 164
    :goto_10
    return v3

    #@11
    .line 154
    :cond_11
    sub-int v1, p1, v2

    #@13
    .line 155
    .local v1, adjPosition:I
    const/4 v0, 0x0

    #@14
    .line 156
    .local v0, adapterCount:I
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@16
    if-eqz v3, :cond_27

    #@18
    .line 157
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@1a
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@1d
    move-result v0

    #@1e
    .line 158
    if-ge v1, v0, :cond_27

    #@20
    .line 159
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@22
    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@25
    move-result v3

    #@26
    goto :goto_10

    #@27
    .line 164
    :cond_27
    iget-object v3, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@29
    sub-int v4, v1, v0

    #@2b
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@31
    iget-boolean v3, v3, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    #@33
    goto :goto_10
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 250
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@9
    .line 252
    :cond_9
    return-void
.end method

.method public removeFooter(Landroid/view/View;)Z
    .registers 7
    .parameter "v"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 114
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v4

    #@9
    if-ge v0, v4, :cond_33

    #@b
    .line 115
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/widget/ListView$FixedViewInfo;

    #@13
    .line 116
    .local v1, info:Landroid/widget/ListView$FixedViewInfo;
    iget-object v4, v1, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@15
    if-ne v4, p1, :cond_30

    #@17
    .line 117
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 119
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@1e
    invoke-direct {p0, v4}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_2d

    #@24
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@26
    invoke-direct {p0, v4}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_2d

    #@2c
    move v2, v3

    #@2d
    :cond_2d
    iput-boolean v2, p0, Landroid/widget/HeaderViewListAdapter;->mAreAllFixedViewsSelectable:Z

    #@2f
    .line 127
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :goto_2f
    return v3

    #@30
    .line 114
    .restart local v1       #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_30
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_3

    #@33
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_33
    move v3, v2

    #@34
    .line 127
    goto :goto_2f
.end method

.method public removeHeader(Landroid/view/View;)Z
    .registers 7
    .parameter "v"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 97
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v4

    #@9
    if-ge v0, v4, :cond_33

    #@b
    .line 98
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/widget/ListView$FixedViewInfo;

    #@13
    .line 99
    .local v1, info:Landroid/widget/ListView$FixedViewInfo;
    iget-object v4, v1, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@15
    if-ne v4, p1, :cond_30

    #@17
    .line 100
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 102
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@1e
    invoke-direct {p0, v4}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_2d

    #@24
    iget-object v4, p0, Landroid/widget/HeaderViewListAdapter;->mFooterViewInfos:Ljava/util/ArrayList;

    #@26
    invoke-direct {p0, v4}, Landroid/widget/HeaderViewListAdapter;->areAllListInfosSelectable(Ljava/util/ArrayList;)Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_2d

    #@2c
    move v2, v3

    #@2d
    :cond_2d
    iput-boolean v2, p0, Landroid/widget/HeaderViewListAdapter;->mAreAllFixedViewsSelectable:Z

    #@2f
    .line 110
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :goto_2f
    return v3

    #@30
    .line 97
    .restart local v1       #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_30
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_3

    #@33
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_33
    move v3, v2

    #@34
    .line 110
    goto :goto_2f
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 256
    iget-object v0, p0, Landroid/widget/HeaderViewListAdapter;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@9
    .line 258
    :cond_9
    return-void
.end method
