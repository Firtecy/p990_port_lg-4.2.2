.class public Landroid/widget/AbsListView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "AbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field forceAdd:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field itemId:J

.field recycledHeaderFooter:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field scrappedFromPosition:I

.field viewType:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x1
                to = "ITEM_VIEW_TYPE_IGNORE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x2
                to = "ITEM_VIEW_TYPE_HEADER_OR_FOOTER"
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .registers 5
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 6339
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@3
    .line 6332
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/widget/AbsListView$LayoutParams;->itemId:J

    #@7
    .line 6340
    return-void
.end method

.method public constructor <init>(III)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "viewType"

    #@0
    .prologue
    .line 6343
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@3
    .line 6332
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/widget/AbsListView$LayoutParams;->itemId:J

    #@7
    .line 6344
    iput p3, p0, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@9
    .line 6345
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    .line 6335
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 6332
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/widget/AbsListView$LayoutParams;->itemId:J

    #@7
    .line 6336
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 6348
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 6332
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/widget/AbsListView$LayoutParams;->itemId:J

    #@7
    .line 6349
    return-void
.end method
