.class Landroid/widget/Spinner$DialogPopup;
.super Ljava/lang/Object;
.source "Spinner.java"

# interfaces
.implements Landroid/widget/Spinner$SpinnerPopup;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Spinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DialogPopup"
.end annotation


# instance fields
.field private mListAdapter:Landroid/widget/ListAdapter;

.field private mPopup:Landroid/app/AlertDialog;

.field private mPrompt:Ljava/lang/CharSequence;

.field final synthetic this$0:Landroid/widget/Spinner;


# direct methods
.method private constructor <init>(Landroid/widget/Spinner;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 835
    iput-object p1, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Spinner;Landroid/widget/Spinner$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 835
    invoke-direct {p0, p1}, Landroid/widget/Spinner$DialogPopup;-><init>(Landroid/widget/Spinner;)V

    #@3
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 841
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->mPopup:Landroid/app/AlertDialog;

    #@2
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@5
    .line 842
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/widget/Spinner$DialogPopup;->mPopup:Landroid/app/AlertDialog;

    #@8
    .line 843
    return-void
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 895
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getHintText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 858
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->mPrompt:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getHorizontalOffset()I
    .registers 2

    #@0
    .prologue
    .line 905
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getVerticalOffset()I
    .registers 2

    #@0
    .prologue
    .line 900
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 846
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->mPopup:Landroid/app/AlertDialog;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->mPopup:Landroid/app/AlertDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 871
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@2
    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    #@5
    .line 872
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@7
    iget-object v0, v0, Landroid/widget/AdapterView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@9
    if-eqz v0, :cond_17

    #@b
    .line 873
    iget-object v0, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@d
    const/4 v1, 0x0

    #@e
    iget-object v2, p0, Landroid/widget/Spinner$DialogPopup;->mListAdapter:Landroid/widget/ListAdapter;

    #@10
    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@13
    move-result-wide v2

    #@14
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/widget/Spinner;->performItemClick(Landroid/view/View;IJ)Z

    #@17
    .line 875
    :cond_17
    invoke-virtual {p0}, Landroid/widget/Spinner$DialogPopup;->dismiss()V

    #@1a
    .line 876
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 2
    .parameter "adapter"

    #@0
    .prologue
    .line 850
    iput-object p1, p0, Landroid/widget/Spinner$DialogPopup;->mListAdapter:Landroid/widget/ListAdapter;

    #@2
    .line 851
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "bg"

    #@0
    .prologue
    .line 880
    const-string v0, "Spinner"

    #@2
    const-string v1, "Cannot set popup background for MODE_DIALOG, ignoring"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 881
    return-void
.end method

.method public setHorizontalOffset(I)V
    .registers 4
    .parameter "px"

    #@0
    .prologue
    .line 890
    const-string v0, "Spinner"

    #@2
    const-string v1, "Cannot set horizontal offset for MODE_DIALOG, ignoring"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 891
    return-void
.end method

.method public setPromptText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "hintText"

    #@0
    .prologue
    .line 854
    iput-object p1, p0, Landroid/widget/Spinner$DialogPopup;->mPrompt:Ljava/lang/CharSequence;

    #@2
    .line 855
    return-void
.end method

.method public setVerticalOffset(I)V
    .registers 4
    .parameter "px"

    #@0
    .prologue
    .line 885
    const-string v0, "Spinner"

    #@2
    const-string v1, "Cannot set vertical offset for MODE_DIALOG, ignoring"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 886
    return-void
.end method

.method public show()V
    .registers 4

    #@0
    .prologue
    .line 862
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    iget-object v1, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@4
    invoke-virtual {v1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@b
    .line 863
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Landroid/widget/Spinner$DialogPopup;->mPrompt:Ljava/lang/CharSequence;

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 864
    iget-object v1, p0, Landroid/widget/Spinner$DialogPopup;->mPrompt:Ljava/lang/CharSequence;

    #@11
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@14
    .line 866
    :cond_14
    iget-object v1, p0, Landroid/widget/Spinner$DialogPopup;->mListAdapter:Landroid/widget/ListAdapter;

    #@16
    iget-object v2, p0, Landroid/widget/Spinner$DialogPopup;->this$0:Landroid/widget/Spinner;

    #@18
    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@1b
    move-result v2

    #@1c
    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Landroid/widget/Spinner$DialogPopup;->mPopup:Landroid/app/AlertDialog;

    #@26
    .line 868
    return-void
.end method
