.class Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;
.super Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;
.source "AccessibilityIterators.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PageTextSegmentIterator"
.end annotation


# static fields
.field private static sPageInstance:Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;


# instance fields
.field private final mTempRect:Landroid/graphics/Rect;

.field private mView:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 116
    invoke-direct {p0}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;-><init>()V

    #@3
    .line 121
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mTempRect:Landroid/graphics/Rect;

    #@a
    return-void
.end method

.method public static getInstance()Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;
    .registers 1

    #@0
    .prologue
    .line 124
    sget-object v0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->sPageInstance:Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 125
    new-instance v0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;

    #@6
    invoke-direct {v0}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;-><init>()V

    #@9
    sput-object v0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->sPageInstance:Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;

    #@b
    .line 127
    :cond_b
    sget-object v0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->sPageInstance:Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;

    #@d
    return-object v0
.end method


# virtual methods
.method public following(I)[I
    .registers 14
    .parameter "offset"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 137
    iget-object v10, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@6
    move-result v8

    #@7
    .line 138
    .local v8, textLegth:I
    if-gtz v8, :cond_a

    #@9
    .line 161
    :cond_9
    :goto_9
    return-object v9

    #@a
    .line 141
    :cond_a
    iget-object v10, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@c
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@f
    move-result v10

    #@10
    if-ge p1, v10, :cond_9

    #@12
    .line 144
    iget-object v10, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@14
    iget-object v11, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mTempRect:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@19
    move-result v10

    #@1a
    if-eqz v10, :cond_9

    #@1c
    .line 148
    const/4 v9, 0x0

    #@1d
    invoke-static {v9, p1}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v7

    #@21
    .line 150
    .local v7, start:I
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@23
    invoke-virtual {v9, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    #@26
    move-result v0

    #@27
    .line 151
    .local v0, currentLine:I
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@29
    invoke-virtual {v9, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@2c
    move-result v1

    #@2d
    .line 152
    .local v1, currentLineTop:I
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mTempRect:Landroid/graphics/Rect;

    #@2f
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    #@32
    move-result v9

    #@33
    iget-object v10, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@35
    invoke-virtual {v10}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@38
    move-result v10

    #@39
    sub-int/2addr v9, v10

    #@3a
    iget-object v10, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@3c
    invoke-virtual {v10}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@3f
    move-result v10

    #@40
    sub-int v6, v9, v10

    #@42
    .line 154
    .local v6, pageHeight:I
    add-int v5, v1, v6

    #@44
    .line 155
    .local v5, nextPageStartY:I
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@46
    iget-object v10, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@48
    invoke-virtual {v10}, Landroid/text/Layout;->getLineCount()I

    #@4b
    move-result v10

    #@4c
    add-int/lit8 v10, v10, -0x1

    #@4e
    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineTop(I)I

    #@51
    move-result v4

    #@52
    .line 156
    .local v4, lastLineTop:I
    if-ge v5, v4, :cond_68

    #@54
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@56
    invoke-virtual {v9, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    #@59
    move-result v9

    #@5a
    add-int/lit8 v2, v9, -0x1

    #@5c
    .line 159
    .local v2, currentPageEndLine:I
    :goto_5c
    const/4 v9, 0x1

    #@5d
    invoke-virtual {p0, v2, v9}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->getLineEdgeIndex(II)I

    #@60
    move-result v9

    #@61
    add-int/lit8 v3, v9, 0x1

    #@63
    .line 161
    .local v3, end:I
    invoke-virtual {p0, v7, v3}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->getRange(II)[I

    #@66
    move-result-object v9

    #@67
    goto :goto_9

    #@68
    .line 156
    .end local v2           #currentPageEndLine:I
    .end local v3           #end:I
    :cond_68
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@6a
    invoke-virtual {v9}, Landroid/text/Layout;->getLineCount()I

    #@6d
    move-result v9

    #@6e
    add-int/lit8 v2, v9, -0x1

    #@70
    goto :goto_5c
.end method

.method public initialize(Landroid/widget/TextView;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 131
    invoke-virtual {p1}, Landroid/widget/TextView;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/text/Spannable;

    #@6
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v1

    #@a
    invoke-super {p0, v0, v1}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->initialize(Landroid/text/Spannable;Landroid/text/Layout;)V

    #@d
    .line 132
    iput-object p1, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@f
    .line 133
    return-void
.end method

.method public preceding(I)[I
    .registers 13
    .parameter "offset"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 166
    iget-object v9, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@3
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@6
    move-result v7

    #@7
    .line 167
    .local v7, textLegth:I
    if-gtz v7, :cond_a

    #@9
    .line 189
    :cond_9
    :goto_9
    return-object v8

    #@a
    .line 170
    :cond_a
    if-lez p1, :cond_9

    #@c
    .line 173
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@e
    iget-object v10, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mTempRect:Landroid/graphics/Rect;

    #@10
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@13
    move-result v9

    #@14
    if-eqz v9, :cond_9

    #@16
    .line 177
    iget-object v8, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@18
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@1b
    move-result v8

    #@1c
    invoke-static {v8, p1}, Ljava/lang/Math;->min(II)I

    #@1f
    move-result v3

    #@20
    .line 179
    .local v3, end:I
    iget-object v8, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@22
    invoke-virtual {v8, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    #@25
    move-result v0

    #@26
    .line 180
    .local v0, currentLine:I
    iget-object v8, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@28
    invoke-virtual {v8, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@2b
    move-result v1

    #@2c
    .line 181
    .local v1, currentLineTop:I
    iget-object v8, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mTempRect:Landroid/graphics/Rect;

    #@2e
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@31
    move-result v8

    #@32
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@34
    invoke-virtual {v9}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@37
    move-result v9

    #@38
    sub-int/2addr v8, v9

    #@39
    iget-object v9, p0, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->mView:Landroid/widget/TextView;

    #@3b
    invoke-virtual {v9}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@3e
    move-result v9

    #@3f
    sub-int v4, v8, v9

    #@41
    .line 183
    .local v4, pageHeight:I
    sub-int v5, v1, v4

    #@43
    .line 184
    .local v5, previousPageEndY:I
    if-lez v5, :cond_57

    #@45
    iget-object v8, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@47
    invoke-virtual {v8, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    #@4a
    move-result v8

    #@4b
    add-int/lit8 v2, v8, 0x1

    #@4d
    .line 187
    .local v2, currentPageStartLine:I
    :goto_4d
    const/4 v8, -0x1

    #@4e
    invoke-virtual {p0, v2, v8}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->getLineEdgeIndex(II)I

    #@51
    move-result v6

    #@52
    .line 189
    .local v6, start:I
    invoke-virtual {p0, v6, v3}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->getRange(II)[I

    #@55
    move-result-object v8

    #@56
    goto :goto_9

    #@57
    .line 184
    .end local v2           #currentPageStartLine:I
    .end local v6           #start:I
    :cond_57
    const/4 v2, 0x0

    #@58
    goto :goto_4d
.end method
