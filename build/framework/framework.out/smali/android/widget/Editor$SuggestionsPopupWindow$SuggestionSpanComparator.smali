.class Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor$SuggestionsPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestionSpanComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/text/style/SuggestionSpan;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/Editor$SuggestionsPopupWindow;


# direct methods
.method private constructor <init>(Landroid/widget/Editor$SuggestionsPopupWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2821
    iput-object p1, p0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;->this$1:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Editor$SuggestionsPopupWindow;Landroid/widget/Editor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2821
    invoke-direct {p0, p1}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;-><init>(Landroid/widget/Editor$SuggestionsPopupWindow;)V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/text/style/SuggestionSpan;Landroid/text/style/SuggestionSpan;)I
    .registers 12
    .parameter "span1"
    .parameter "span2"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 2823
    invoke-virtual {p1}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@6
    move-result v2

    #@7
    .line 2824
    .local v2, flag1:I
    invoke-virtual {p2}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@a
    move-result v3

    #@b
    .line 2825
    .local v3, flag2:I
    if-eq v2, v3, :cond_38

    #@d
    .line 2827
    and-int/lit8 v8, v2, 0x1

    #@f
    if-eqz v8, :cond_26

    #@11
    move v0, v7

    #@12
    .line 2828
    .local v0, easy1:Z
    :goto_12
    and-int/lit8 v8, v3, 0x1

    #@14
    if-eqz v8, :cond_28

    #@16
    move v1, v7

    #@17
    .line 2829
    .local v1, easy2:Z
    :goto_17
    and-int/lit8 v8, v2, 0x2

    #@19
    if-eqz v8, :cond_2a

    #@1b
    move v4, v7

    #@1c
    .line 2830
    .local v4, misspelled1:Z
    :goto_1c
    and-int/lit8 v8, v3, 0x2

    #@1e
    if-eqz v8, :cond_21

    #@20
    move v5, v7

    #@21
    .line 2831
    .local v5, misspelled2:Z
    :cond_21
    if-eqz v0, :cond_2c

    #@23
    if-nez v4, :cond_2c

    #@25
    .line 2837
    .end local v0           #easy1:Z
    .end local v1           #easy2:Z
    .end local v4           #misspelled1:Z
    .end local v5           #misspelled2:Z
    :cond_25
    :goto_25
    return v6

    #@26
    :cond_26
    move v0, v5

    #@27
    .line 2827
    goto :goto_12

    #@28
    .restart local v0       #easy1:Z
    :cond_28
    move v1, v5

    #@29
    .line 2828
    goto :goto_17

    #@2a
    .restart local v1       #easy2:Z
    :cond_2a
    move v4, v5

    #@2b
    .line 2829
    goto :goto_1c

    #@2c
    .line 2832
    .restart local v4       #misspelled1:Z
    .restart local v5       #misspelled2:Z
    :cond_2c
    if-eqz v1, :cond_32

    #@2e
    if-nez v5, :cond_32

    #@30
    move v6, v7

    #@31
    goto :goto_25

    #@32
    .line 2833
    :cond_32
    if-nez v4, :cond_25

    #@34
    .line 2834
    if-eqz v5, :cond_38

    #@36
    move v6, v7

    #@37
    goto :goto_25

    #@38
    .line 2837
    .end local v0           #easy1:Z
    .end local v1           #easy2:Z
    .end local v4           #misspelled1:Z
    .end local v5           #misspelled2:Z
    :cond_38
    iget-object v6, p0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;->this$1:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@3a
    invoke-static {v6}, Landroid/widget/Editor$SuggestionsPopupWindow;->access$2100(Landroid/widget/Editor$SuggestionsPopupWindow;)Ljava/util/HashMap;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    check-cast v6, Ljava/lang/Integer;

    #@44
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@47
    move-result v7

    #@48
    iget-object v6, p0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;->this$1:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@4a
    invoke-static {v6}, Landroid/widget/Editor$SuggestionsPopupWindow;->access$2100(Landroid/widget/Editor$SuggestionsPopupWindow;)Ljava/util/HashMap;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    move-result-object v6

    #@52
    check-cast v6, Ljava/lang/Integer;

    #@54
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@57
    move-result v6

    #@58
    sub-int v6, v7, v6

    #@5a
    goto :goto_25
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2821
    check-cast p1, Landroid/text/style/SuggestionSpan;

    #@2
    .end local p1
    check-cast p2, Landroid/text/style/SuggestionSpan;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;->compare(Landroid/text/style/SuggestionSpan;Landroid/text/style/SuggestionSpan;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
