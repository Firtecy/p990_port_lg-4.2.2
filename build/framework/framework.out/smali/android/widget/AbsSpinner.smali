.class public abstract Landroid/widget/AbsSpinner;
.super Landroid/widget/AdapterView;
.source "AbsSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AbsSpinner$1;,
        Landroid/widget/AbsSpinner$RecycleBin;,
        Landroid/widget/AbsSpinner$SavedState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/SpinnerAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field mAdapter:Landroid/widget/SpinnerAdapter;

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field mHeightMeasureSpec:I

.field final mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

.field mSelectionBottomPadding:I

.field mSelectionLeftPadding:I

.field mSelectionRightPadding:I

.field mSelectionTopPadding:I

.field final mSpinnerPadding:Landroid/graphics/Rect;

.field private mTouchFrame:Landroid/graphics/Rect;

.field mWidthMeasureSpec:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 59
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    #@4
    .line 46
    iput v0, p0, Landroid/widget/AbsSpinner;->mSelectionLeftPadding:I

    #@6
    .line 47
    iput v0, p0, Landroid/widget/AbsSpinner;->mSelectionTopPadding:I

    #@8
    .line 48
    iput v0, p0, Landroid/widget/AbsSpinner;->mSelectionRightPadding:I

    #@a
    .line 49
    iput v0, p0, Landroid/widget/AbsSpinner;->mSelectionBottomPadding:I

    #@c
    .line 50
    new-instance v0, Landroid/graphics/Rect;

    #@e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v0, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@13
    .line 52
    new-instance v0, Landroid/widget/AbsSpinner$RecycleBin;

    #@15
    invoke-direct {v0, p0}, Landroid/widget/AbsSpinner$RecycleBin;-><init>(Landroid/widget/AbsSpinner;)V

    #@18
    iput-object v0, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@1a
    .line 60
    invoke-direct {p0}, Landroid/widget/AbsSpinner;->initAbsSpinner()V

    #@1d
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AbsSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 46
    iput v4, p0, Landroid/widget/AbsSpinner;->mSelectionLeftPadding:I

    #@6
    .line 47
    iput v4, p0, Landroid/widget/AbsSpinner;->mSelectionTopPadding:I

    #@8
    .line 48
    iput v4, p0, Landroid/widget/AbsSpinner;->mSelectionRightPadding:I

    #@a
    .line 49
    iput v4, p0, Landroid/widget/AbsSpinner;->mSelectionBottomPadding:I

    #@c
    .line 50
    new-instance v3, Landroid/graphics/Rect;

    #@e
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v3, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@13
    .line 52
    new-instance v3, Landroid/widget/AbsSpinner$RecycleBin;

    #@15
    invoke-direct {v3, p0}, Landroid/widget/AbsSpinner$RecycleBin;-><init>(Landroid/widget/AbsSpinner;)V

    #@18
    iput-object v3, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@1a
    .line 69
    invoke-direct {p0}, Landroid/widget/AbsSpinner;->initAbsSpinner()V

    #@1d
    .line 71
    sget-object v3, Lcom/android/internal/R$styleable;->AbsSpinner:[I

    #@1f
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@22
    move-result-object v0

    #@23
    .line 74
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@26
    move-result-object v2

    #@27
    .line 75
    .local v2, entries:[Ljava/lang/CharSequence;
    if-eqz v2, :cond_3a

    #@29
    .line 76
    new-instance v1, Landroid/widget/ArrayAdapter;

    #@2b
    const v3, 0x1090008

    #@2e
    invoke-direct {v1, p1, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    #@31
    .line 79
    .local v1, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v3, 0x1090009

    #@34
    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    #@37
    .line 80
    invoke-virtual {p0, v1}, Landroid/widget/AbsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@3a
    .line 83
    .end local v1           #adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    :cond_3a
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3d
    .line 84
    return-void
.end method

.method static synthetic access$100(Landroid/widget/AbsSpinner;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsSpinner;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method private initAbsSpinner()V
    .registers 2

    #@0
    .prologue
    .line 90
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/AbsSpinner;->setFocusable(Z)V

    #@4
    .line 91
    const/4 v0, 0x0

    #@5
    invoke-virtual {p0, v0}, Landroid/widget/AbsSpinner;->setWillNotDraw(Z)V

    #@8
    .line 92
    return-void
.end method


# virtual methods
.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    #@0
    .prologue
    .line 249
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@2
    const/4 v1, -0x1

    #@3
    const/4 v2, -0x2

    #@4
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@7
    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .registers 2

    #@0
    .prologue
    .line 40
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getAdapter()Landroid/widget/SpinnerAdapter;
    .registers 2

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@2
    return-object v0
.end method

.method getChildHeight(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 240
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method getChildWidth(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 333
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .registers 3

    #@0
    .prologue
    .line 306
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    if-lez v0, :cond_12

    #@4
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    if-ltz v0, :cond_12

    #@8
    .line 307
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@a
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c
    sub-int/2addr v0, v1

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/AbsSpinner;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 309
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method abstract layout(IZ)V
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 474
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 475
    const-class v0, Landroid/widget/AbsSpinner;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 476
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 480
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 481
    const-class v0, Landroid/widget/AbsSpinner;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 482
    return-void
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 166
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@5
    move-result v6

    #@6
    .line 170
    .local v6, widthMode:I
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@8
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@a
    iget v10, p0, Landroid/widget/AbsSpinner;->mSelectionLeftPadding:I

    #@c
    if-le v8, v10, :cond_de

    #@e
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@10
    :goto_10
    iput v8, v9, Landroid/graphics/Rect;->left:I

    #@12
    .line 172
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@14
    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    #@16
    iget v10, p0, Landroid/widget/AbsSpinner;->mSelectionTopPadding:I

    #@18
    if-le v8, v10, :cond_e2

    #@1a
    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    #@1c
    :goto_1c
    iput v8, v9, Landroid/graphics/Rect;->top:I

    #@1e
    .line 174
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@20
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@22
    iget v10, p0, Landroid/widget/AbsSpinner;->mSelectionRightPadding:I

    #@24
    if-le v8, v10, :cond_e6

    #@26
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@28
    :goto_28
    iput v8, v9, Landroid/graphics/Rect;->right:I

    #@2a
    .line 176
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@2c
    iget v8, p0, Landroid/view/View;->mPaddingBottom:I

    #@2e
    iget v10, p0, Landroid/widget/AbsSpinner;->mSelectionBottomPadding:I

    #@30
    if-le v8, v10, :cond_ea

    #@32
    iget v8, p0, Landroid/view/View;->mPaddingBottom:I

    #@34
    :goto_34
    iput v8, v9, Landroid/graphics/Rect;->bottom:I

    #@36
    .line 179
    iget-boolean v8, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@38
    if-eqz v8, :cond_3d

    #@3a
    .line 180
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->handleDataChanged()V

    #@3d
    .line 183
    :cond_3d
    const/4 v2, 0x0

    #@3e
    .line 184
    .local v2, preferredHeight:I
    const/4 v3, 0x0

    #@3f
    .line 185
    .local v3, preferredWidth:I
    const/4 v1, 0x1

    #@40
    .line 187
    .local v1, needsMeasuring:Z
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getSelectedItemPosition()I

    #@43
    move-result v4

    #@44
    .line 188
    .local v4, selectedPosition:I
    if-ltz v4, :cond_a6

    #@46
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@48
    if-eqz v8, :cond_a6

    #@4a
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@4c
    invoke-interface {v8}, Landroid/widget/SpinnerAdapter;->getCount()I

    #@4f
    move-result v8

    #@50
    if-ge v4, v8, :cond_a6

    #@52
    .line 190
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@54
    invoke-virtual {v8, v4}, Landroid/widget/AbsSpinner$RecycleBin;->get(I)Landroid/view/View;

    #@57
    move-result-object v5

    #@58
    .line 191
    .local v5, view:Landroid/view/View;
    if-nez v5, :cond_6a

    #@5a
    .line 193
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@5c
    const/4 v9, 0x0

    #@5d
    invoke-interface {v8, v4, v9, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@60
    move-result-object v5

    #@61
    .line 195
    invoke-virtual {v5}, Landroid/view/View;->getImportantForAccessibility()I

    #@64
    move-result v8

    #@65
    if-nez v8, :cond_6a

    #@67
    .line 196
    invoke-virtual {v5, v12}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@6a
    .line 200
    :cond_6a
    if-eqz v5, :cond_71

    #@6c
    .line 202
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@6e
    invoke-virtual {v8, v4, v5}, Landroid/widget/AbsSpinner$RecycleBin;->put(ILandroid/view/View;)V

    #@71
    .line 205
    :cond_71
    if-eqz v5, :cond_a6

    #@73
    .line 206
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@76
    move-result-object v8

    #@77
    if-nez v8, :cond_84

    #@79
    .line 207
    iput-boolean v12, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@7b
    .line 208
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@7e
    move-result-object v8

    #@7f
    invoke-virtual {v5, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@82
    .line 209
    iput-boolean v11, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@84
    .line 211
    :cond_84
    invoke-virtual {p0, v5, p1, p2}, Landroid/widget/AbsSpinner;->measureChild(Landroid/view/View;II)V

    #@87
    .line 213
    invoke-virtual {p0, v5}, Landroid/widget/AbsSpinner;->getChildHeight(Landroid/view/View;)I

    #@8a
    move-result v8

    #@8b
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@8d
    iget v9, v9, Landroid/graphics/Rect;->top:I

    #@8f
    add-int/2addr v8, v9

    #@90
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@92
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@94
    add-int v2, v8, v9

    #@96
    .line 214
    invoke-virtual {p0, v5}, Landroid/widget/AbsSpinner;->getChildWidth(Landroid/view/View;)I

    #@99
    move-result v8

    #@9a
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@9c
    iget v9, v9, Landroid/graphics/Rect;->left:I

    #@9e
    add-int/2addr v8, v9

    #@9f
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@a1
    iget v9, v9, Landroid/graphics/Rect;->right:I

    #@a3
    add-int v3, v8, v9

    #@a5
    .line 216
    const/4 v1, 0x0

    #@a6
    .line 220
    .end local v5           #view:Landroid/view/View;
    :cond_a6
    if-eqz v1, :cond_be

    #@a8
    .line 222
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@aa
    iget v8, v8, Landroid/graphics/Rect;->top:I

    #@ac
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@ae
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@b0
    add-int v2, v8, v9

    #@b2
    .line 223
    if-nez v6, :cond_be

    #@b4
    .line 224
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@b6
    iget v8, v8, Landroid/graphics/Rect;->left:I

    #@b8
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@ba
    iget v9, v9, Landroid/graphics/Rect;->right:I

    #@bc
    add-int v3, v8, v9

    #@be
    .line 228
    :cond_be
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getSuggestedMinimumHeight()I

    #@c1
    move-result v8

    #@c2
    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    #@c5
    move-result v2

    #@c6
    .line 229
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getSuggestedMinimumWidth()I

    #@c9
    move-result v8

    #@ca
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    #@cd
    move-result v3

    #@ce
    .line 231
    invoke-static {v2, p2, v11}, Landroid/widget/AbsSpinner;->resolveSizeAndState(III)I

    #@d1
    move-result v0

    #@d2
    .line 232
    .local v0, heightSize:I
    invoke-static {v3, p1, v11}, Landroid/widget/AbsSpinner;->resolveSizeAndState(III)I

    #@d5
    move-result v7

    #@d6
    .line 234
    .local v7, widthSize:I
    invoke-virtual {p0, v7, v0}, Landroid/widget/AbsSpinner;->setMeasuredDimension(II)V

    #@d9
    .line 235
    iput p2, p0, Landroid/widget/AbsSpinner;->mHeightMeasureSpec:I

    #@db
    .line 236
    iput p1, p0, Landroid/widget/AbsSpinner;->mWidthMeasureSpec:I

    #@dd
    .line 237
    return-void

    #@de
    .line 170
    .end local v0           #heightSize:I
    .end local v1           #needsMeasuring:Z
    .end local v2           #preferredHeight:I
    .end local v3           #preferredWidth:I
    .end local v4           #selectedPosition:I
    .end local v7           #widthSize:I
    :cond_de
    iget v8, p0, Landroid/widget/AbsSpinner;->mSelectionLeftPadding:I

    #@e0
    goto/16 :goto_10

    #@e2
    .line 172
    :cond_e2
    iget v8, p0, Landroid/widget/AbsSpinner;->mSelectionTopPadding:I

    #@e4
    goto/16 :goto_1c

    #@e6
    .line 174
    :cond_e6
    iget v8, p0, Landroid/widget/AbsSpinner;->mSelectionRightPadding:I

    #@e8
    goto/16 :goto_28

    #@ea
    .line 176
    :cond_ea
    iget v8, p0, Landroid/widget/AbsSpinner;->mSelectionBottomPadding:I

    #@ec
    goto/16 :goto_34
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 426
    move-object v0, p1

    #@2
    check-cast v0, Landroid/widget/AbsSpinner$SavedState;

    #@4
    .line 428
    .local v0, ss:Landroid/widget/AbsSpinner$SavedState;
    invoke-virtual {v0}, Landroid/widget/AbsSpinner$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@7
    move-result-object v1

    #@8
    invoke-super {p0, v1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@b
    .line 430
    iget-wide v1, v0, Landroid/widget/AbsSpinner$SavedState;->selectedId:J

    #@d
    const-wide/16 v3, 0x0

    #@f
    cmp-long v1, v1, v3

    #@11
    if-ltz v1, :cond_25

    #@13
    .line 431
    iput-boolean v5, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@15
    .line 432
    iput-boolean v5, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@17
    .line 433
    iget-wide v1, v0, Landroid/widget/AbsSpinner$SavedState;->selectedId:J

    #@19
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@1b
    .line 434
    iget v1, v0, Landroid/widget/AbsSpinner$SavedState;->position:I

    #@1d
    iput v1, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@1f
    .line 435
    const/4 v1, 0x0

    #@20
    iput v1, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@22
    .line 436
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->requestLayout()V

    #@25
    .line 438
    :cond_25
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 7

    #@0
    .prologue
    .line 413
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 414
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Landroid/widget/AbsSpinner$SavedState;

    #@6
    invoke-direct {v0, v1}, Landroid/widget/AbsSpinner$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@9
    .line 415
    .local v0, ss:Landroid/widget/AbsSpinner$SavedState;
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getSelectedItemId()J

    #@c
    move-result-wide v2

    #@d
    iput-wide v2, v0, Landroid/widget/AbsSpinner$SavedState;->selectedId:J

    #@f
    .line 416
    iget-wide v2, v0, Landroid/widget/AbsSpinner$SavedState;->selectedId:J

    #@11
    const-wide/16 v4, 0x0

    #@13
    cmp-long v2, v2, v4

    #@15
    if-ltz v2, :cond_1e

    #@17
    .line 417
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getSelectedItemPosition()I

    #@1a
    move-result v2

    #@1b
    iput v2, v0, Landroid/widget/AbsSpinner$SavedState;->position:I

    #@1d
    .line 421
    :goto_1d
    return-object v0

    #@1e
    .line 419
    :cond_1e
    const/4 v2, -0x1

    #@1f
    iput v2, v0, Landroid/widget/AbsSpinner$SavedState;->position:I

    #@21
    goto :goto_1d
.end method

.method public pointToPosition(II)I
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 345
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mTouchFrame:Landroid/graphics/Rect;

    #@2
    .line 346
    .local v2, frame:Landroid/graphics/Rect;
    if-nez v2, :cond_d

    #@4
    .line 347
    new-instance v4, Landroid/graphics/Rect;

    #@6
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v4, p0, Landroid/widget/AbsSpinner;->mTouchFrame:Landroid/graphics/Rect;

    #@b
    .line 348
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mTouchFrame:Landroid/graphics/Rect;

    #@d
    .line 351
    :cond_d
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getChildCount()I

    #@10
    move-result v1

    #@11
    .line 352
    .local v1, count:I
    add-int/lit8 v3, v1, -0x1

    #@13
    .local v3, i:I
    :goto_13
    if-ltz v3, :cond_2f

    #@15
    .line 353
    invoke-virtual {p0, v3}, Landroid/widget/AbsSpinner;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    .line 354
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@1c
    move-result v4

    #@1d
    if-nez v4, :cond_2c

    #@1f
    .line 355
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@22
    .line 356
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_2c

    #@28
    .line 357
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2a
    add-int/2addr v4, v3

    #@2b
    .line 361
    .end local v0           #child:Landroid/view/View;
    :goto_2b
    return v4

    #@2c
    .line 352
    .restart local v0       #child:Landroid/view/View;
    :cond_2c
    add-int/lit8 v3, v3, -0x1

    #@2e
    goto :goto_13

    #@2f
    .line 361
    .end local v0           #child:Landroid/view/View;
    :cond_2f
    const/4 v4, -0x1

    #@30
    goto :goto_2b
.end method

.method recycleAllViews()V
    .registers 7

    #@0
    .prologue
    .line 255
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 256
    .local v0, childCount:I
    iget-object v4, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@6
    .line 257
    .local v4, recycleBin:Landroid/widget/AbsSpinner$RecycleBin;
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8
    .line 260
    .local v3, position:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_17

    #@b
    .line 261
    invoke-virtual {p0, v1}, Landroid/widget/AbsSpinner;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v5

    #@f
    .line 262
    .local v5, v:Landroid/view/View;
    add-int v2, v3, v1

    #@11
    .line 263
    .local v2, index:I
    invoke-virtual {v4, v2, v5}, Landroid/widget/AbsSpinner$RecycleBin;->put(ILandroid/view/View;)V

    #@14
    .line 260
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_9

    #@17
    .line 265
    .end local v2           #index:I
    .end local v5           #v:Landroid/view/View;
    :cond_17
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 321
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 322
    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    #@7
    .line 324
    :cond_7
    return-void
.end method

.method resetList()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 144
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@4
    .line 145
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@6
    .line 147
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->removeAllViewsInLayout()V

    #@9
    .line 148
    iput v2, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@b
    .line 149
    const-wide/high16 v0, -0x8000

    #@d
    iput-wide v0, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@f
    .line 151
    invoke-virtual {p0, v2}, Landroid/widget/AbsSpinner;->setSelectedPositionInt(I)V

    #@12
    .line 152
    invoke-virtual {p0, v2}, Landroid/widget/AbsSpinner;->setNextSelectedPositionInt(I)V

    #@15
    .line 153
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->invalidate()V

    #@18
    .line 154
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    check-cast p1, Landroid/widget/SpinnerAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/AbsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .registers 5
    .parameter "adapter"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 102
    iget-object v1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 103
    iget-object v1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@7
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@9
    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@c
    .line 104
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->resetList()V

    #@f
    .line 107
    :cond_f
    iput-object p1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@11
    .line 109
    iput v0, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@13
    .line 110
    const-wide/high16 v1, -0x8000

    #@15
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@17
    .line 112
    iget-object v1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@19
    if-eqz v1, :cond_4e

    #@1b
    .line 113
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1d
    iput v1, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@1f
    .line 114
    iget-object v1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@21
    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    #@24
    move-result v1

    #@25
    iput v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@27
    .line 115
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->checkFocus()V

    #@2a
    .line 117
    new-instance v1, Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@2c
    invoke-direct {v1, p0}, Landroid/widget/AdapterView$AdapterDataSetObserver;-><init>(Landroid/widget/AdapterView;)V

    #@2f
    iput-object v1, p0, Landroid/widget/AbsSpinner;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@31
    .line 118
    iget-object v1, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@33
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mDataSetObserver:Landroid/database/DataSetObserver;

    #@35
    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@38
    .line 120
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3a
    if-lez v1, :cond_3d

    #@3c
    const/4 v0, 0x0

    #@3d
    .line 122
    .local v0, position:I
    :cond_3d
    invoke-virtual {p0, v0}, Landroid/widget/AbsSpinner;->setSelectedPositionInt(I)V

    #@40
    .line 123
    invoke-virtual {p0, v0}, Landroid/widget/AbsSpinner;->setNextSelectedPositionInt(I)V

    #@43
    .line 125
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@45
    if-nez v1, :cond_4a

    #@47
    .line 127
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->checkSelectionChanged()V

    #@4a
    .line 137
    .end local v0           #position:I
    :cond_4a
    :goto_4a
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->requestLayout()V

    #@4d
    .line 138
    return-void

    #@4e
    .line 131
    :cond_4e
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->checkFocus()V

    #@51
    .line 132
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->resetList()V

    #@54
    .line 134
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->checkSelectionChanged()V

    #@57
    goto :goto_4a
.end method

.method public setSelection(I)V
    .registers 2
    .parameter "position"

    #@0
    .prologue
    .line 279
    invoke-virtual {p0, p1}, Landroid/widget/AbsSpinner;->setNextSelectedPositionInt(I)V

    #@3
    .line 280
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->requestLayout()V

    #@6
    .line 281
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->invalidate()V

    #@9
    .line 282
    return-void
.end method

.method public setSelection(IZ)V
    .registers 6
    .parameter "position"
    .parameter "animate"

    #@0
    .prologue
    .line 272
    if-eqz p2, :cond_16

    #@2
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4
    if-gt v1, p1, :cond_16

    #@6
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8
    invoke-virtual {p0}, Landroid/widget/AbsSpinner;->getChildCount()I

    #@b
    move-result v2

    #@c
    add-int/2addr v1, v2

    #@d
    add-int/lit8 v1, v1, -0x1

    #@f
    if-gt p1, v1, :cond_16

    #@11
    const/4 v0, 0x1

    #@12
    .line 274
    .local v0, shouldAnimate:Z
    :goto_12
    invoke-virtual {p0, p1, v0}, Landroid/widget/AbsSpinner;->setSelectionInt(IZ)V

    #@15
    .line 275
    return-void

    #@16
    .line 272
    .end local v0           #shouldAnimate:Z
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_12
.end method

.method setSelectionInt(IZ)V
    .registers 5
    .parameter "position"
    .parameter "animate"

    #@0
    .prologue
    .line 293
    iget v1, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@2
    if-eq p1, v1, :cond_14

    #@4
    .line 294
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@7
    .line 295
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@9
    sub-int v0, p1, v1

    #@b
    .line 296
    .local v0, delta:I
    invoke-virtual {p0, p1}, Landroid/widget/AbsSpinner;->setNextSelectedPositionInt(I)V

    #@e
    .line 297
    invoke-virtual {p0, v0, p2}, Landroid/widget/AbsSpinner;->layout(IZ)V

    #@11
    .line 298
    const/4 v1, 0x0

    #@12
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@14
    .line 300
    .end local v0           #delta:I
    :cond_14
    return-void
.end method
