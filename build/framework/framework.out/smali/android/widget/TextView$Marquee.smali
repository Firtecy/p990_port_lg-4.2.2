.class final Landroid/widget/TextView$Marquee;
.super Landroid/os/Handler;
.source "TextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Marquee"
.end annotation


# static fields
.field private static final MARQUEE_DELAY:I = 0x4b0

.field private static final MARQUEE_DELTA_MAX:F = 0.07f

.field private static final MARQUEE_PIXELS_PER_SECOND:I = 0x1e

.field private static final MARQUEE_RESOLUTION:I = 0x21

.field private static final MARQUEE_RESTART_DELAY:I = 0x4b0

.field private static final MARQUEE_RUNNING:B = 0x2t

.field private static final MARQUEE_STARTING:B = 0x1t

.field private static final MARQUEE_STOPPED:B = 0x0t

.field private static final MESSAGE_RESTART:I = 0x3

.field private static final MESSAGE_START:I = 0x1

.field private static final MESSAGE_TICK:I = 0x2


# instance fields
.field private mFadeStop:F

.field private mGhostOffset:F

.field private mGhostStart:F

.field private mMaxFadeScroll:F

.field private mMaxScroll:F

.field private mRepeatLimit:I

.field private mScroll:F

.field private final mScrollUnit:F

.field private mStatus:B

.field private final mView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 9293
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 9282
    const/4 v1, 0x0

    #@4
    iput-byte v1, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@6
    .line 9294
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@11
    move-result-object v1

    #@12
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    #@14
    .line 9295
    .local v0, density:F
    const/high16 v1, 0x41f0

    #@16
    mul-float/2addr v1, v0

    #@17
    const/high16 v2, 0x4204

    #@19
    div-float/2addr v1, v2

    #@1a
    iput v1, p0, Landroid/widget/TextView$Marquee;->mScrollUnit:F

    #@1c
    .line 9296
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@1e
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@21
    iput-object v1, p0, Landroid/widget/TextView$Marquee;->mView:Ljava/lang/ref/WeakReference;

    #@23
    .line 9297
    return-void
.end method

.method private resetScroll()V
    .registers 3

    #@0
    .prologue
    .line 9352
    const/4 v1, 0x0

    #@1
    iput v1, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@3
    .line 9353
    iget-object v1, p0, Landroid/widget/TextView$Marquee;->mView:Ljava/lang/ref/WeakReference;

    #@5
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/widget/TextView;

    #@b
    .line 9354
    .local v0, textView:Landroid/widget/TextView;
    if-eqz v0, :cond_10

    #@d
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    #@10
    .line 9355
    :cond_10
    return-void
.end method


# virtual methods
.method getGhostOffset()F
    .registers 2

    #@0
    .prologue
    .line 9383
    iget v0, p0, Landroid/widget/TextView$Marquee;->mGhostOffset:F

    #@2
    return v0
.end method

.method getMaxFadeScroll()F
    .registers 2

    #@0
    .prologue
    .line 9391
    iget v0, p0, Landroid/widget/TextView$Marquee;->mMaxFadeScroll:F

    #@2
    return v0
.end method

.method getScroll()F
    .registers 2

    #@0
    .prologue
    .line 9387
    iget v0, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@2
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 9301
    iget v0, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v0, :pswitch_data_26

    #@6
    .line 9318
    :cond_6
    :goto_6
    return-void

    #@7
    .line 9303
    :pswitch_7
    iput-byte v1, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@9
    .line 9304
    invoke-virtual {p0}, Landroid/widget/TextView$Marquee;->tick()V

    #@c
    goto :goto_6

    #@d
    .line 9307
    :pswitch_d
    invoke-virtual {p0}, Landroid/widget/TextView$Marquee;->tick()V

    #@10
    goto :goto_6

    #@11
    .line 9310
    :pswitch_11
    iget-byte v0, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@13
    if-ne v0, v1, :cond_6

    #@15
    .line 9311
    iget v0, p0, Landroid/widget/TextView$Marquee;->mRepeatLimit:I

    #@17
    if-ltz v0, :cond_1f

    #@19
    .line 9312
    iget v0, p0, Landroid/widget/TextView$Marquee;->mRepeatLimit:I

    #@1b
    add-int/lit8 v0, v0, -0x1

    #@1d
    iput v0, p0, Landroid/widget/TextView$Marquee;->mRepeatLimit:I

    #@1f
    .line 9314
    :cond_1f
    iget v0, p0, Landroid/widget/TextView$Marquee;->mRepeatLimit:I

    #@21
    invoke-virtual {p0, v0}, Landroid/widget/TextView$Marquee;->start(I)V

    #@24
    goto :goto_6

    #@25
    .line 9301
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_7
        :pswitch_d
        :pswitch_11
    .end packed-switch
.end method

.method isRunning()Z
    .registers 3

    #@0
    .prologue
    .line 9403
    iget-byte v0, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method isStopped()Z
    .registers 2

    #@0
    .prologue
    .line 9407
    iget-byte v0, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method shouldDrawGhost()Z
    .registers 3

    #@0
    .prologue
    .line 9399
    iget-byte v0, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_f

    #@5
    iget v0, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@7
    iget v1, p0, Landroid/widget/TextView$Marquee;->mGhostStart:F

    #@9
    cmpl-float v0, v0, v1

    #@b
    if-lez v0, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method shouldDrawLeftFade()Z
    .registers 3

    #@0
    .prologue
    .line 9395
    iget v0, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@2
    iget v1, p0, Landroid/widget/TextView$Marquee;->mFadeStop:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gtz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method start(I)V
    .registers 9
    .parameter "repeatLimit"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 9358
    if-nez p1, :cond_7

    #@3
    .line 9359
    invoke-virtual {p0}, Landroid/widget/TextView$Marquee;->stop()V

    #@6
    .line 9380
    :cond_6
    :goto_6
    return-void

    #@7
    .line 9362
    :cond_7
    iput p1, p0, Landroid/widget/TextView$Marquee;->mRepeatLimit:I

    #@9
    .line 9363
    iget-object v4, p0, Landroid/widget/TextView$Marquee;->mView:Ljava/lang/ref/WeakReference;

    #@b
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/widget/TextView;

    #@11
    .line 9364
    .local v2, textView:Landroid/widget/TextView;
    if-eqz v2, :cond_6

    #@13
    invoke-static {v2}, Landroid/widget/TextView;->access$600(Landroid/widget/TextView;)Landroid/text/Layout;

    #@16
    move-result-object v4

    #@17
    if-eqz v4, :cond_6

    #@19
    .line 9365
    iput-byte v6, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@1b
    .line 9366
    const/4 v4, 0x0

    #@1c
    iput v4, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@1e
    .line 9367
    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    #@21
    move-result v4

    #@22
    invoke-virtual {v2}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@25
    move-result v5

    #@26
    sub-int/2addr v4, v5

    #@27
    invoke-virtual {v2}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@2a
    move-result v5

    #@2b
    sub-int v3, v4, v5

    #@2d
    .line 9369
    .local v3, textWidth:I
    invoke-static {v2}, Landroid/widget/TextView;->access$600(Landroid/widget/TextView;)Landroid/text/Layout;

    #@30
    move-result-object v4

    #@31
    const/4 v5, 0x0

    #@32
    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineWidth(I)F

    #@35
    move-result v1

    #@36
    .line 9370
    .local v1, lineWidth:F
    int-to-float v4, v3

    #@37
    const/high16 v5, 0x4040

    #@39
    div-float v0, v4, v5

    #@3b
    .line 9371
    .local v0, gap:F
    int-to-float v4, v3

    #@3c
    sub-float v4, v1, v4

    #@3e
    add-float/2addr v4, v0

    #@3f
    iput v4, p0, Landroid/widget/TextView$Marquee;->mGhostStart:F

    #@41
    .line 9372
    iget v4, p0, Landroid/widget/TextView$Marquee;->mGhostStart:F

    #@43
    int-to-float v5, v3

    #@44
    add-float/2addr v4, v5

    #@45
    iput v4, p0, Landroid/widget/TextView$Marquee;->mMaxScroll:F

    #@47
    .line 9373
    add-float v4, v1, v0

    #@49
    iput v4, p0, Landroid/widget/TextView$Marquee;->mGhostOffset:F

    #@4b
    .line 9374
    int-to-float v4, v3

    #@4c
    const/high16 v5, 0x40c0

    #@4e
    div-float/2addr v4, v5

    #@4f
    add-float/2addr v4, v1

    #@50
    iput v4, p0, Landroid/widget/TextView$Marquee;->mFadeStop:F

    #@52
    .line 9375
    iget v4, p0, Landroid/widget/TextView$Marquee;->mGhostStart:F

    #@54
    add-float/2addr v4, v1

    #@55
    add-float/2addr v4, v1

    #@56
    iput v4, p0, Landroid/widget/TextView$Marquee;->mMaxFadeScroll:F

    #@58
    .line 9377
    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    #@5b
    .line 9378
    const-wide/16 v4, 0x4b0

    #@5d
    invoke-virtual {p0, v6, v4, v5}, Landroid/widget/TextView$Marquee;->sendEmptyMessageDelayed(IJ)Z

    #@60
    goto :goto_6
.end method

.method stop()V
    .registers 2

    #@0
    .prologue
    .line 9344
    const/4 v0, 0x0

    #@1
    iput-byte v0, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@3
    .line 9345
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/TextView$Marquee;->removeMessages(I)V

    #@7
    .line 9346
    const/4 v0, 0x3

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/TextView$Marquee;->removeMessages(I)V

    #@b
    .line 9347
    const/4 v0, 0x2

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/TextView$Marquee;->removeMessages(I)V

    #@f
    .line 9348
    invoke-direct {p0}, Landroid/widget/TextView$Marquee;->resetScroll()V

    #@12
    .line 9349
    return-void
.end method

.method tick()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 9321
    iget-byte v1, p0, Landroid/widget/TextView$Marquee;->mStatus:B

    #@3
    if-eq v1, v3, :cond_6

    #@5
    .line 9341
    :cond_5
    :goto_5
    return-void

    #@6
    .line 9325
    :cond_6
    invoke-virtual {p0, v3}, Landroid/widget/TextView$Marquee;->removeMessages(I)V

    #@9
    .line 9327
    iget-object v1, p0, Landroid/widget/TextView$Marquee;->mView:Ljava/lang/ref/WeakReference;

    #@b
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/widget/TextView;

    #@11
    .line 9330
    .local v0, textView:Landroid/widget/TextView;
    if-eqz v0, :cond_5

    #@13
    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_25

    #@19
    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_25

    #@1f
    invoke-virtual {v0}, Landroid/widget/TextView;->isMarqueeAlwaysEnable()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_5

    #@25
    .line 9331
    :cond_25
    iget v1, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@27
    iget v2, p0, Landroid/widget/TextView$Marquee;->mScrollUnit:F

    #@29
    add-float/2addr v1, v2

    #@2a
    iput v1, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@2c
    .line 9332
    iget v1, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@2e
    iget v2, p0, Landroid/widget/TextView$Marquee;->mMaxScroll:F

    #@30
    cmpl-float v1, v1, v2

    #@32
    if-lez v1, :cond_42

    #@34
    .line 9333
    iget v1, p0, Landroid/widget/TextView$Marquee;->mMaxScroll:F

    #@36
    iput v1, p0, Landroid/widget/TextView$Marquee;->mScroll:F

    #@38
    .line 9334
    const/4 v1, 0x3

    #@39
    const-wide/16 v2, 0x4b0

    #@3b
    invoke-virtual {p0, v1, v2, v3}, Landroid/widget/TextView$Marquee;->sendEmptyMessageDelayed(IJ)Z

    #@3e
    .line 9338
    :goto_3e
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    #@41
    goto :goto_5

    #@42
    .line 9336
    :cond_42
    const-wide/16 v1, 0x21

    #@44
    invoke-virtual {p0, v3, v1, v2}, Landroid/widget/TextView$Marquee;->sendEmptyMessageDelayed(IJ)Z

    #@47
    goto :goto_3e
.end method
