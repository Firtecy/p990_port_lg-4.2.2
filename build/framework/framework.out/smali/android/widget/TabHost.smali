.class public Landroid/widget/TabHost;
.super Landroid/widget/FrameLayout;
.source "TabHost.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TabHost$IntentContentStrategy;,
        Landroid/widget/TabHost$FactoryContentStrategy;,
        Landroid/widget/TabHost$ViewIdContentStrategy;,
        Landroid/widget/TabHost$ViewIndicatorStrategy;,
        Landroid/widget/TabHost$LabelAndIconIndicatorStrategy;,
        Landroid/widget/TabHost$LabelIndicatorStrategy;,
        Landroid/widget/TabHost$ContentStrategy;,
        Landroid/widget/TabHost$IndicatorStrategy;,
        Landroid/widget/TabHost$TabSpec;,
        Landroid/widget/TabHost$TabContentFactory;,
        Landroid/widget/TabHost$OnTabChangeListener;
    }
.end annotation


# static fields
.field private static final TABWIDGET_LOCATION_BOTTOM:I = 0x3

.field private static final TABWIDGET_LOCATION_LEFT:I = 0x0

.field private static final TABWIDGET_LOCATION_RIGHT:I = 0x2

.field private static final TABWIDGET_LOCATION_TOP:I = 0x1


# instance fields
.field protected mCurrentTab:I

.field private mCurrentView:Landroid/view/View;

.field protected mLocalActivityManager:Landroid/app/LocalActivityManager;

.field private mOnTabChangeListener:Landroid/widget/TabHost$OnTabChangeListener;

.field private mTabContent:Landroid/widget/FrameLayout;

.field private mTabKeyListener:Landroid/view/View$OnKeyListener;

.field private mTabLayoutId:I

.field private mTabSpecs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TabHost$TabSpec;",
            ">;"
        }
    .end annotation
.end field

.field private mTabWidget:Landroid/widget/TabWidget;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@4
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@c
    .line 62
    const/4 v0, -0x1

    #@d
    iput v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@f
    .line 63
    iput-object v2, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@11
    .line 68
    iput-object v2, p0, Landroid/widget/TabHost;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    #@13
    .line 76
    invoke-direct {p0}, Landroid/widget/TabHost;->initTabHost()V

    #@16
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 57
    new-instance v1, Ljava/util/ArrayList;

    #@7
    const/4 v2, 0x2

    #@8
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@b
    iput-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@d
    .line 62
    const/4 v1, -0x1

    #@e
    iput v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@10
    .line 63
    iput-object v4, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@12
    .line 68
    iput-object v4, p0, Landroid/widget/TabHost;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    #@14
    .line 82
    sget-object v1, Lcom/android/internal/R$styleable;->TabWidget:[I

    #@16
    const v2, 0x1010083

    #@19
    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1c
    move-result-object v0

    #@1d
    .line 86
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x4

    #@1e
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@21
    move-result v1

    #@22
    iput v1, p0, Landroid/widget/TabHost;->mTabLayoutId:I

    #@24
    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@27
    .line 89
    iget v1, p0, Landroid/widget/TabHost;->mTabLayoutId:I

    #@29
    if-nez v1, :cond_30

    #@2b
    .line 92
    const v1, 0x10900d5

    #@2e
    iput v1, p0, Landroid/widget/TabHost;->mTabLayoutId:I

    #@30
    .line 95
    :cond_30
    invoke-direct {p0}, Landroid/widget/TabHost;->initTabHost()V

    #@33
    .line 96
    return-void
.end method

.method static synthetic access$100(Landroid/widget/TabHost;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/TabHost;)Landroid/widget/TabWidget;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/TabHost;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/widget/TabHost;->mTabLayoutId:I

    #@2
    return v0
.end method

.method private getTabWidgetLocation()I
    .registers 4

    #@0
    .prologue
    .line 306
    const/4 v0, 0x1

    #@1
    .line 308
    .local v0, location:I
    iget-object v1, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@3
    invoke-virtual {v1}, Landroid/widget/TabWidget;->getOrientation()I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_2e

    #@a
    .line 315
    iget-object v1, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@c
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    #@f
    move-result v1

    #@10
    iget-object v2, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@12
    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTop()I

    #@15
    move-result v2

    #@16
    if-ge v1, v2, :cond_2c

    #@18
    const/4 v0, 0x3

    #@19
    .line 319
    :goto_19
    return v0

    #@1a
    .line 310
    :pswitch_1a
    iget-object v1, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@1c
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLeft()I

    #@1f
    move-result v1

    #@20
    iget-object v2, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@22
    invoke-virtual {v2}, Landroid/widget/TabWidget;->getLeft()I

    #@25
    move-result v2

    #@26
    if-ge v1, v2, :cond_2a

    #@28
    const/4 v0, 0x2

    #@29
    .line 312
    :goto_29
    goto :goto_19

    #@2a
    .line 310
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29

    #@2c
    .line 315
    :cond_2c
    const/4 v0, 0x1

    #@2d
    goto :goto_19

    #@2e
    .line 308
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_1a
    .end packed-switch
.end method

.method private initTabHost()V
    .registers 2

    #@0
    .prologue
    .line 99
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/TabHost;->setFocusableInTouchMode(Z)V

    #@4
    .line 100
    const/high16 v0, 0x4

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/TabHost;->setDescendantFocusability(I)V

    #@9
    .line 102
    const/4 v0, -0x1

    #@a
    iput v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@c
    .line 103
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@f
    .line 104
    return-void
.end method

.method private invokeOnTabChangeListener()V
    .registers 3

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Landroid/widget/TabHost;->mOnTabChangeListener:Landroid/widget/TabHost$OnTabChangeListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 446
    iget-object v0, p0, Landroid/widget/TabHost;->mOnTabChangeListener:Landroid/widget/TabHost$OnTabChangeListener;

    #@6
    invoke-virtual {p0}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v0, v1}, Landroid/widget/TabHost$OnTabChangeListener;->onTabChanged(Ljava/lang/String;)V

    #@d
    .line 448
    :cond_d
    return-void
.end method


# virtual methods
.method public addTab(Landroid/widget/TabHost$TabSpec;)V
    .registers 6
    .parameter "tabSpec"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 220
    invoke-static {p1}, Landroid/widget/TabHost$TabSpec;->access$200(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$IndicatorStrategy;

    #@4
    move-result-object v1

    #@5
    if-nez v1, :cond_10

    #@7
    .line 221
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v2, "you must specify a way to create the tab indicator."

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 224
    :cond_10
    invoke-static {p1}, Landroid/widget/TabHost$TabSpec;->access$300(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$ContentStrategy;

    #@13
    move-result-object v1

    #@14
    if-nez v1, :cond_1f

    #@16
    .line 225
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@18
    const-string/jumbo v2, "you must specify a way to create the tab content"

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 227
    :cond_1f
    invoke-static {p1}, Landroid/widget/TabHost$TabSpec;->access$200(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$IndicatorStrategy;

    #@22
    move-result-object v1

    #@23
    invoke-interface {v1}, Landroid/widget/TabHost$IndicatorStrategy;->createIndicatorView()Landroid/view/View;

    #@26
    move-result-object v0

    #@27
    .line 228
    .local v0, tabIndicator:Landroid/view/View;
    iget-object v1, p0, Landroid/widget/TabHost;->mTabKeyListener:Landroid/view/View$OnKeyListener;

    #@29
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@2c
    .line 232
    invoke-static {p1}, Landroid/widget/TabHost$TabSpec;->access$200(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$IndicatorStrategy;

    #@2f
    move-result-object v1

    #@30
    instance-of v1, v1, Landroid/widget/TabHost$ViewIndicatorStrategy;

    #@32
    if-eqz v1, :cond_39

    #@34
    .line 233
    iget-object v1, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@36
    invoke-virtual {v1, v3}, Landroid/widget/TabWidget;->setStripEnabled(Z)V

    #@39
    .line 236
    :cond_39
    iget-object v1, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@3b
    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->addView(Landroid/view/View;)V

    #@3e
    .line 237
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@40
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@43
    .line 239
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@45
    const/4 v2, -0x1

    #@46
    if-ne v1, v2, :cond_4b

    #@48
    .line 240
    invoke-virtual {p0, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    #@4b
    .line 242
    :cond_4b
    return-void
.end method

.method public clearAllTabs()V
    .registers 2

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@2
    invoke-virtual {v0}, Landroid/widget/TabWidget;->removeAllViews()V

    #@5
    .line 250
    invoke-direct {p0}, Landroid/widget/TabHost;->initTabHost()V

    #@8
    .line 251
    iget-object v0, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@a
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@d
    .line 252
    iget-object v0, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@f
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@12
    .line 253
    invoke-virtual {p0}, Landroid/widget/TabHost;->requestLayout()V

    #@15
    .line 254
    invoke-virtual {p0}, Landroid/widget/TabHost;->invalidate()V

    #@18
    .line 255
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 324
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v1

    #@4
    .line 329
    .local v1, handled:Z
    if-nez v1, :cond_52

    #@6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_52

    #@c
    iget-object v4, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@e
    if-eqz v4, :cond_52

    #@10
    iget-object v4, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@12
    invoke-virtual {v4}, Landroid/view/View;->isRootNamespace()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_52

    #@18
    iget-object v4, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@1a
    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_52

    #@20
    .line 334
    const/16 v2, 0x13

    #@22
    .line 335
    .local v2, keyCodeShouldChangeFocus:I
    const/16 v0, 0x21

    #@24
    .line 336
    .local v0, directionShouldChangeFocus:I
    const/4 v3, 0x2

    #@25
    .line 338
    .local v3, soundEffect:I
    invoke-direct {p0}, Landroid/widget/TabHost;->getTabWidgetLocation()I

    #@28
    move-result v4

    #@29
    packed-switch v4, :pswitch_data_66

    #@2c
    .line 356
    :pswitch_2c
    const/16 v2, 0x13

    #@2e
    .line 357
    const/16 v0, 0x21

    #@30
    .line 358
    const/4 v3, 0x2

    #@31
    .line 361
    :goto_31
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@34
    move-result v4

    #@35
    if-ne v4, v2, :cond_52

    #@37
    iget-object v4, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@39
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v0}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    #@40
    move-result-object v4

    #@41
    if-nez v4, :cond_52

    #@43
    .line 363
    iget-object v4, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@45
    iget v5, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@47
    invoke-virtual {v4, v5}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    #@4e
    .line 364
    invoke-virtual {p0, v3}, Landroid/widget/TabHost;->playSoundEffect(I)V

    #@51
    .line 365
    const/4 v1, 0x1

    #@52
    .line 368
    .end local v0           #directionShouldChangeFocus:I
    .end local v1           #handled:Z
    .end local v2           #keyCodeShouldChangeFocus:I
    .end local v3           #soundEffect:I
    :cond_52
    return v1

    #@53
    .line 340
    .restart local v0       #directionShouldChangeFocus:I
    .restart local v1       #handled:Z
    .restart local v2       #keyCodeShouldChangeFocus:I
    .restart local v3       #soundEffect:I
    :pswitch_53
    const/16 v2, 0x15

    #@55
    .line 341
    const/16 v0, 0x11

    #@57
    .line 342
    const/4 v3, 0x1

    #@58
    .line 343
    goto :goto_31

    #@59
    .line 345
    :pswitch_59
    const/16 v2, 0x16

    #@5b
    .line 346
    const/16 v0, 0x42

    #@5d
    .line 347
    const/4 v3, 0x3

    #@5e
    .line 348
    goto :goto_31

    #@5f
    .line 350
    :pswitch_5f
    const/16 v2, 0x14

    #@61
    .line 351
    const/16 v0, 0x82

    #@63
    .line 352
    const/4 v3, 0x4

    #@64
    .line 353
    goto :goto_31

    #@65
    .line 338
    nop

    #@66
    :pswitch_data_66
    .packed-switch 0x0
        :pswitch_53
        :pswitch_2c
        :pswitch_59
        :pswitch_5f
    .end packed-switch
.end method

.method public dispatchWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasFocus"

    #@0
    .prologue
    .line 374
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 375
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@6
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchWindowFocusChanged(Z)V

    #@9
    .line 377
    :cond_9
    return-void
.end method

.method public getCurrentTab()I
    .registers 2

    #@0
    .prologue
    .line 262
    iget v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@2
    return v0
.end method

.method public getCurrentTabTag()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 266
    iget v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@2
    if-ltz v0, :cond_1d

    #@4
    iget v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@6
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@8
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@b
    move-result v1

    #@c
    if-ge v0, v1, :cond_1d

    #@e
    .line 267
    iget-object v0, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@10
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@12
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/widget/TabHost$TabSpec;

    #@18
    invoke-virtual {v0}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 269
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method public getCurrentTabView()Landroid/view/View;
    .registers 3

    #@0
    .prologue
    .line 273
    iget v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@2
    if-ltz v0, :cond_17

    #@4
    iget v0, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@6
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@8
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@b
    move-result v1

    #@c
    if-ge v0, v1, :cond_17

    #@e
    .line 274
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@10
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 276
    :goto_16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public getCurrentView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 280
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getTabContentView()Landroid/widget/FrameLayout;
    .registers 2

    #@0
    .prologue
    .line 297
    iget-object v0, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method public getTabWidget()Landroid/widget/TabWidget;
    .registers 2

    #@0
    .prologue
    .line 258
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@2
    return-object v0
.end method

.method public newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    .line 111
    new-instance v0, Landroid/widget/TabHost$TabSpec;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/widget/TabHost$TabSpec;-><init>(Landroid/widget/TabHost;Ljava/lang/String;Landroid/widget/TabHost$1;)V

    #@6
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 189
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    #@3
    .line 190
    invoke-virtual {p0}, Landroid/widget/TabHost;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@6
    move-result-object v0

    #@7
    .line 191
    .local v0, treeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@a
    .line 192
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 196
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    #@3
    .line 197
    invoke-virtual {p0}, Landroid/widget/TabHost;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@6
    move-result-object v0

    #@7
    .line 198
    .local v0, treeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@a
    .line 199
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 381
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 382
    const-class v0, Landroid/widget/TabHost;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 383
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 387
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 388
    const-class v0, Landroid/widget/TabHost;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 389
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .registers 4
    .parameter "isInTouchMode"

    #@0
    .prologue
    .line 205
    if-nez p1, :cond_21

    #@2
    .line 208
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@4
    if-eqz v0, :cond_21

    #@6
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@8
    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_16

    #@e
    iget-object v0, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@10
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_21

    #@16
    .line 209
    :cond_16
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@18
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@1a
    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    #@21
    .line 212
    :cond_21
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .registers 2
    .parameter "eventType"

    #@0
    .prologue
    .line 172
    return-void
.end method

.method public setCurrentTab(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 392
    if-ltz p1, :cond_b

    #@3
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@5
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@8
    move-result v1

    #@9
    if-lt p1, v1, :cond_c

    #@b
    .line 432
    :cond_b
    :goto_b
    return-void

    #@c
    .line 396
    :cond_c
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@e
    if-eq p1, v1, :cond_b

    #@10
    .line 401
    iget v1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@12
    if-eq v1, v4, :cond_25

    #@14
    .line 402
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@16
    iget v2, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@18
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/widget/TabHost$TabSpec;

    #@1e
    invoke-static {v1}, Landroid/widget/TabHost$TabSpec;->access$300(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$ContentStrategy;

    #@21
    move-result-object v1

    #@22
    invoke-interface {v1}, Landroid/widget/TabHost$ContentStrategy;->tabClosed()V

    #@25
    .line 405
    :cond_25
    iput p1, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@27
    .line 406
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@29
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Landroid/widget/TabHost$TabSpec;

    #@2f
    .line 410
    .local v0, spec:Landroid/widget/TabHost$TabSpec;
    iget-object v1, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@31
    iget v2, p0, Landroid/widget/TabHost;->mCurrentTab:I

    #@33
    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->focusCurrentTab(I)V

    #@36
    .line 413
    invoke-static {v0}, Landroid/widget/TabHost$TabSpec;->access$300(Landroid/widget/TabHost$TabSpec;)Landroid/widget/TabHost$ContentStrategy;

    #@39
    move-result-object v1

    #@3a
    invoke-interface {v1}, Landroid/widget/TabHost$ContentStrategy;->getContentView()Landroid/view/View;

    #@3d
    move-result-object v1

    #@3e
    iput-object v1, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@40
    .line 415
    iget-object v1, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@42
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@45
    move-result-object v1

    #@46
    if-nez v1, :cond_54

    #@48
    .line 416
    iget-object v1, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@4a
    iget-object v2, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@4c
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    #@4e
    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@51
    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@54
    .line 424
    :cond_54
    iget-object v1, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@56
    invoke-virtual {v1}, Landroid/widget/TabWidget;->hasFocus()Z

    #@59
    move-result v1

    #@5a
    if-nez v1, :cond_61

    #@5c
    .line 427
    iget-object v1, p0, Landroid/widget/TabHost;->mCurrentView:Landroid/view/View;

    #@5e
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    #@61
    .line 431
    :cond_61
    invoke-direct {p0}, Landroid/widget/TabHost;->invokeOnTabChangeListener()V

    #@64
    goto :goto_b
.end method

.method public setCurrentTabByTag(Ljava/lang/String;)V
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    .line 285
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@3
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1e

    #@9
    .line 286
    iget-object v1, p0, Landroid/widget/TabHost;->mTabSpecs:Ljava/util/List;

    #@b
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/widget/TabHost$TabSpec;

    #@11
    invoke-virtual {v1}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_1f

    #@1b
    .line 287
    invoke-virtual {p0, v0}, Landroid/widget/TabHost;->setCurrentTab(I)V

    #@1e
    .line 291
    :cond_1e
    return-void

    #@1f
    .line 285
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_1
.end method

.method public setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 441
    iput-object p1, p0, Landroid/widget/TabHost;->mOnTabChangeListener:Landroid/widget/TabHost$OnTabChangeListener;

    #@2
    .line 442
    return-void
.end method

.method public setup()V
    .registers 3

    #@0
    .prologue
    .line 126
    const v0, 0x1020013

    #@3
    invoke-virtual {p0, v0}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/TabWidget;

    #@9
    iput-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@b
    .line 127
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@d
    if-nez v0, :cond_17

    #@f
    .line 128
    new-instance v0, Ljava/lang/RuntimeException;

    #@11
    const-string v1, "Your TabHost must have a TabWidget whose id attribute is \'android.R.id.tabs\'"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 134
    :cond_17
    new-instance v0, Landroid/widget/TabHost$1;

    #@19
    invoke-direct {v0, p0}, Landroid/widget/TabHost$1;-><init>(Landroid/widget/TabHost;)V

    #@1c
    iput-object v0, p0, Landroid/widget/TabHost;->mTabKeyListener:Landroid/view/View$OnKeyListener;

    #@1e
    .line 152
    iget-object v0, p0, Landroid/widget/TabHost;->mTabWidget:Landroid/widget/TabWidget;

    #@20
    new-instance v1, Landroid/widget/TabHost$2;

    #@22
    invoke-direct {v1, p0}, Landroid/widget/TabHost$2;-><init>(Landroid/widget/TabHost;)V

    #@25
    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->setTabSelectionListener(Landroid/widget/TabWidget$OnTabSelectionChanged;)V

    #@28
    .line 161
    const v0, 0x1020011

    #@2b
    invoke-virtual {p0, v0}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Landroid/widget/FrameLayout;

    #@31
    iput-object v0, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@33
    .line 162
    iget-object v0, p0, Landroid/widget/TabHost;->mTabContent:Landroid/widget/FrameLayout;

    #@35
    if-nez v0, :cond_3f

    #@37
    .line 163
    new-instance v0, Ljava/lang/RuntimeException;

    #@39
    const-string v1, "Your TabHost must have a FrameLayout whose id attribute is \'android.R.id.tabcontent\'"

    #@3b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v0

    #@3f
    .line 167
    :cond_3f
    return-void
.end method

.method public setup(Landroid/app/LocalActivityManager;)V
    .registers 2
    .parameter "activityGroup"

    #@0
    .prologue
    .line 182
    invoke-virtual {p0}, Landroid/widget/TabHost;->setup()V

    #@3
    .line 183
    iput-object p1, p0, Landroid/widget/TabHost;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    #@5
    .line 184
    return-void
.end method
