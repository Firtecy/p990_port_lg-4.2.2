.class public Landroid/widget/AppSecurityPermissions$PermissionItemView;
.super Landroid/widget/LinearLayout;
.source "AppSecurityPermissions.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AppSecurityPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PermissionItemView"
.end annotation


# instance fields
.field mDialog:Landroid/app/AlertDialog;

.field mGroup:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

.field mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 157
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->setClickable(Z)V

    #@7
    .line 158
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 13
    .parameter "v"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 196
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mGroup:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@4
    if-eqz v6, :cond_51

    #@6
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@8
    if-eqz v6, :cond_51

    #@a
    .line 197
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@c
    if-eqz v6, :cond_13

    #@e
    .line 198
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@10
    invoke-virtual {v6}, Landroid/app/AlertDialog;->dismiss()V

    #@13
    .line 200
    :cond_13
    invoke-virtual {p0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->getContext()Landroid/content/Context;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1a
    move-result-object v4

    #@1b
    .line 201
    .local v4, pm:Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@1d
    invoke-virtual {p0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->getContext()Landroid/content/Context;

    #@20
    move-result-object v6

    #@21
    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@24
    .line 202
    .local v2, builder:Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mGroup:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@26
    iget-object v6, v6, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    #@28
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@2b
    .line 203
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@2d
    iget v6, v6, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@2f
    if-eqz v6, :cond_52

    #@31
    .line 204
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@33
    invoke-virtual {v6, v4}, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@3a
    .line 220
    :goto_3a
    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@3d
    .line 221
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mGroup:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@3f
    invoke-virtual {v6, v4}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->loadGroupIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    #@46
    .line 222
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@49
    move-result-object v6

    #@4a
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@4c
    .line 223
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@4e
    invoke-virtual {v6, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    #@51
    .line 225
    .end local v2           #builder:Landroid/app/AlertDialog$Builder;
    .end local v4           #pm:Landroid/content/pm/PackageManager;
    :cond_51
    return-void

    #@52
    .line 208
    .restart local v2       #builder:Landroid/app/AlertDialog$Builder;
    .restart local v4       #pm:Landroid/content/pm/PackageManager;
    :cond_52
    :try_start_52
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@54
    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@56
    const/4 v7, 0x0

    #@57
    invoke-virtual {v4, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@5a
    move-result-object v0

    #@5b
    .line 209
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    :try_end_5e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_52 .. :try_end_5e} :catch_8c

    #@5e
    move-result-object v1

    #@5f
    .line 213
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    .local v1, appName:Ljava/lang/CharSequence;
    :goto_5f
    new-instance v5, Ljava/lang/StringBuilder;

    #@61
    const/16 v6, 0x80

    #@63
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@66
    .line 214
    .local v5, sbuilder:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->getContext()Landroid/content/Context;

    #@69
    move-result-object v6

    #@6a
    const v7, 0x104045c

    #@6d
    new-array v8, v9, [Ljava/lang/Object;

    #@6f
    aput-object v1, v8, v10

    #@71
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    .line 216
    const-string v6, "\n\n"

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 217
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@7f
    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    .line 218
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@8b
    goto :goto_3a

    #@8c
    .line 210
    .end local v1           #appName:Ljava/lang/CharSequence;
    .end local v5           #sbuilder:Ljava/lang/StringBuilder;
    :catch_8c
    move-exception v3

    #@8d
    .line 211
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@8f
    iget-object v1, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@91
    .restart local v1       #appName:Ljava/lang/CharSequence;
    goto :goto_5f
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 229
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    #@3
    .line 230
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 231
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mDialog:Landroid/app/AlertDialog;

    #@9
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@c
    .line 233
    :cond_c
    return-void
.end method

.method public setPermission(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)V
    .registers 15
    .parameter "grp"
    .parameter "perm"
    .parameter "first"
    .parameter "newPermPrefix"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 162
    iput-object p1, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mGroup:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@3
    .line 163
    iput-object p2, p0, Landroid/widget/AppSecurityPermissions$PermissionItemView;->mPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@5
    .line 165
    const v8, 0x1020277

    #@8
    invoke-virtual {p0, v8}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v5

    #@c
    check-cast v5, Landroid/widget/ImageView;

    #@e
    .line 166
    .local v5, permGrpIcon:Landroid/widget/ImageView;
    const v8, 0x1020278

    #@11
    invoke-virtual {p0, v8}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v6

    #@15
    check-cast v6, Landroid/widget/TextView;

    #@17
    .line 168
    .local v6, permNameView:Landroid/widget/TextView;
    invoke-virtual {p0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->getContext()Landroid/content/Context;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1e
    move-result-object v7

    #@1f
    .line 169
    .local v7, pm:Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    #@20
    .line 170
    .local v1, icon:Landroid/graphics/drawable/Drawable;
    if-eqz p3, :cond_26

    #@22
    .line 171
    invoke-virtual {p1, v7}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->loadGroupIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v1

    #@26
    .line 173
    :cond_26
    iget-object v2, p2, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    #@28
    .line 174
    .local v2, label:Ljava/lang/CharSequence;
    iget-boolean v8, p2, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mNew:Z

    #@2a
    if-eqz v8, :cond_4f

    #@2c
    if-eqz p4, :cond_4f

    #@2e
    .line 176
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@30
    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@33
    .line 177
    .local v0, builder:Landroid/text/SpannableStringBuilder;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@36
    move-result-object v4

    #@37
    .line 178
    .local v4, parcel:Landroid/os/Parcel;
    invoke-static {p4, v4, v9}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@3a
    .line 179
    invoke-virtual {v4, v9}, Landroid/os/Parcel;->setDataPosition(I)V

    #@3d
    .line 180
    sget-object v8, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@3f
    invoke-interface {v8, v4}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@42
    move-result-object v3

    #@43
    check-cast v3, Ljava/lang/CharSequence;

    #@45
    .line 181
    .local v3, newStr:Ljava/lang/CharSequence;
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 182
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@4b
    .line 183
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@4e
    .line 184
    move-object v2, v0

    #@4f
    .line 187
    .end local v0           #builder:Landroid/text/SpannableStringBuilder;
    .end local v3           #newStr:Ljava/lang/CharSequence;
    .end local v4           #parcel:Landroid/os/Parcel;
    :cond_4f
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@52
    .line 188
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@55
    .line 189
    invoke-virtual {p0, p0}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@58
    .line 192
    return-void
.end method
