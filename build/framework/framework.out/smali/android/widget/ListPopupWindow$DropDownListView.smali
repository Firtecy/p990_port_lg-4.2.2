.class Landroid/widget/ListPopupWindow$DropDownListView;
.super Landroid/widget/ListView;
.source "ListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DropDownListView"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ListPopupWindow.DropDownListView"


# instance fields
.field private mHijackFocus:Z

.field private mListSelectionHidden:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 5
    .parameter "context"
    .parameter "hijackFocus"

    #@0
    .prologue
    .line 1181
    const/4 v0, 0x0

    #@1
    const v1, 0x101006d

    #@4
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@7
    .line 1182
    iput-boolean p2, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    #@9
    .line 1184
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v0}, Landroid/widget/ListPopupWindow$DropDownListView;->setCacheColorHint(I)V

    #@d
    .line 1185
    return-void
.end method

.method static synthetic access$502(Landroid/widget/ListPopupWindow$DropDownListView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1138
    iput-boolean p1, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mListSelectionHidden:Z

    #@2
    return p1
.end method


# virtual methods
.method public hasFocus()Z
    .registers 2

    #@0
    .prologue
    .line 1238
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    #@2
    if-nez v0, :cond_a

    #@4
    invoke-super {p0}, Landroid/widget/ListView;->hasFocus()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hasWindowFocus()Z
    .registers 2

    #@0
    .prologue
    .line 1218
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    #@2
    if-nez v0, :cond_a

    #@4
    invoke-super {p0}, Landroid/widget/ListView;->hasWindowFocus()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isFocused()Z
    .registers 2

    #@0
    .prologue
    .line 1228
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    #@2
    if-nez v0, :cond_a

    #@4
    invoke-super {p0}, Landroid/widget/ListView;->isFocused()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isInTouchMode()Z
    .registers 2

    #@0
    .prologue
    .line 1208
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-boolean v0, p0, Landroid/widget/ListPopupWindow$DropDownListView;->mListSelectionHidden:Z

    #@6
    if-nez v0, :cond_e

    #@8
    :cond_8
    invoke-super {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method obtainView(I[Z)Landroid/view/View;
    .registers 6
    .parameter "position"
    .parameter "isScrap"

    #@0
    .prologue
    .line 1196
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 1198
    .local v0, view:Landroid/view/View;
    instance-of v1, v0, Landroid/widget/TextView;

    #@6
    if-eqz v1, :cond_f

    #@8
    move-object v1, v0

    #@9
    .line 1199
    check-cast v1, Landroid/widget/TextView;

    #@b
    const/4 v2, 0x1

    #@c
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    #@f
    .line 1202
    :cond_f
    return-object v0
.end method
