.class Landroid/widget/OverScroller$SplineOverScroller$FrameRate;
.super Ljava/lang/Object;
.source "OverScroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/OverScroller$SplineOverScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FrameRate"
.end annotation


# instance fields
.field public extraTime:I

.field public frameCount:I

.field public frameTotalCount:I

.field final synthetic this$0:Landroid/widget/OverScroller$SplineOverScroller;

.field public timePassed:J

.field public timeTotalPassed:J


# direct methods
.method public constructor <init>(Landroid/widget/OverScroller$SplineOverScroller;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 701
    iput-object p1, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->this$0:Landroid/widget/OverScroller$SplineOverScroller;

    #@5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 702
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@a
    .line 703
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@c
    .line 704
    iput-wide v1, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@e
    .line 705
    iput-wide v1, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@10
    .line 706
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->extraTime:I

    #@12
    .line 707
    return-void
.end method


# virtual methods
.method public getTimeDiff()F
    .registers 5

    #@0
    .prologue
    .line 720
    iget-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@2
    iget-wide v2, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@4
    add-long/2addr v0, v2

    #@5
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@7
    .line 721
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@9
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@b
    add-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@e
    .line 722
    iget-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@10
    long-to-float v0, v0

    #@11
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    int-to-float v1, v1

    #@16
    div-float/2addr v0, v1

    #@17
    return v0
.end method

.method public reset()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 710
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@3
    .line 711
    const-wide/16 v0, 0x0

    #@5
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@7
    .line 712
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->extraTime:I

    #@9
    .line 713
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@b
    const/16 v1, 0x190

    #@d
    if-le v0, v1, :cond_1e

    #@f
    .line 714
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@11
    div-int/lit8 v0, v0, 0x2

    #@13
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@15
    .line 715
    iget-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@17
    const-wide/16 v2, 0x2

    #@19
    div-long/2addr v0, v2

    #@1a
    long-to-int v0, v0

    #@1b
    int-to-long v0, v0

    #@1c
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timeTotalPassed:J

    #@1e
    .line 717
    :cond_1e
    return-void
.end method
