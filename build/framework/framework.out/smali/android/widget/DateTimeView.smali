.class public Landroid/widget/DateTimeView;
.super Landroid/widget/TextView;
.source "DateTimeView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final SHOW_MONTH_DAY_YEAR:I = 0x1

.field private static final SHOW_TIME:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DateTimeView"

.field private static final TWELVE_HOURS_IN_MINUTES:J = 0x2d0L

.field private static final TWENTY_FOUR_HOURS_IN_MILLIS:J = 0x5265c00L


# instance fields
.field private mAttachedToWindow:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContentObserver:Landroid/database/ContentObserver;

.field mLastDisplay:I

.field mLastFormat:Ljava/text/DateFormat;

.field mTime:Ljava/util/Date;

.field mTimeMillis:J

.field private mUpdateTimeMillis:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 72
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@3
    .line 65
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/DateTimeView;->mLastDisplay:I

    #@6
    .line 238
    new-instance v0, Landroid/widget/DateTimeView$1;

    #@8
    invoke-direct {v0, p0}, Landroid/widget/DateTimeView$1;-><init>(Landroid/widget/DateTimeView;)V

    #@b
    iput-object v0, p0, Landroid/widget/DateTimeView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@d
    .line 256
    new-instance v0, Landroid/widget/DateTimeView$2;

    #@f
    new-instance v1, Landroid/os/Handler;

    #@11
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@14
    invoke-direct {v0, p0, v1}, Landroid/widget/DateTimeView$2;-><init>(Landroid/widget/DateTimeView;Landroid/os/Handler;)V

    #@17
    iput-object v0, p0, Landroid/widget/DateTimeView;->mContentObserver:Landroid/database/ContentObserver;

    #@19
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 65
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/DateTimeView;->mLastDisplay:I

    #@6
    .line 238
    new-instance v0, Landroid/widget/DateTimeView$1;

    #@8
    invoke-direct {v0, p0}, Landroid/widget/DateTimeView$1;-><init>(Landroid/widget/DateTimeView;)V

    #@b
    iput-object v0, p0, Landroid/widget/DateTimeView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@d
    .line 256
    new-instance v0, Landroid/widget/DateTimeView$2;

    #@f
    new-instance v1, Landroid/os/Handler;

    #@11
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@14
    invoke-direct {v0, p0, v1}, Landroid/widget/DateTimeView$2;-><init>(Landroid/widget/DateTimeView;Landroid/os/Handler;)V

    #@17
    iput-object v0, p0, Landroid/widget/DateTimeView;->mContentObserver:Landroid/database/ContentObserver;

    #@19
    .line 77
    return-void
.end method

.method static synthetic access$000(Landroid/widget/DateTimeView;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-wide v0, p0, Landroid/widget/DateTimeView;->mUpdateTimeMillis:J

    #@2
    return-wide v0
.end method

.method private getDateFormat()Ljava/text/DateFormat;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 204
    invoke-virtual {p0}, Landroid/widget/DateTimeView;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v2

    #@9
    const-string v3, "date_format"

    #@b
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 206
    .local v1, format:Ljava/lang/String;
    if-eqz v1, :cond_19

    #@11
    const-string v2, ""

    #@13
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_1e

    #@19
    .line 207
    :cond_19
    invoke-static {v4}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    #@1c
    move-result-object v2

    #@1d
    .line 213
    :goto_1d
    return-object v2

    #@1e
    .line 210
    :cond_1e
    :try_start_1e
    new-instance v2, Ljava/text/SimpleDateFormat;

    #@20
    invoke-direct {v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_23
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1e .. :try_end_23} :catch_24

    #@23
    goto :goto_1d

    #@24
    .line 211
    :catch_24
    move-exception v0

    #@25
    .line 213
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-static {v4}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    #@28
    move-result-object v2

    #@29
    goto :goto_1d
.end method

.method private getTimeFormat()Ljava/text/DateFormat;
    .registers 5

    #@0
    .prologue
    .line 193
    invoke-virtual {p0}, Landroid/widget/DateTimeView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 194
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_17

    #@a
    .line 195
    const v2, 0x104004d

    #@d
    .line 199
    .local v2, res:I
    :goto_d
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 200
    .local v1, format:Ljava/lang/String;
    new-instance v3, Ljava/text/SimpleDateFormat;

    #@13
    invoke-direct {v3, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@16
    return-object v3

    #@17
    .line 197
    .end local v1           #format:Ljava/lang/String;
    .end local v2           #res:I
    :cond_17
    const v2, 0x104004c

    #@1a
    .restart local v2       #res:I
    goto :goto_d
.end method

.method private registerReceivers()V
    .registers 7

    #@0
    .prologue
    .line 219
    invoke-virtual {p0}, Landroid/widget/DateTimeView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 221
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/content/IntentFilter;

    #@6
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@9
    .line 222
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.TIME_TICK"

    #@b
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@e
    .line 223
    const-string v3, "android.intent.action.TIME_SET"

    #@10
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@13
    .line 224
    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    #@15
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18
    .line 225
    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    #@1a
    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 226
    iget-object v3, p0, Landroid/widget/DateTimeView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@1f
    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@22
    .line 228
    const-string v3, "date_format"

    #@24
    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@27
    move-result-object v2

    #@28
    .line 229
    .local v2, uri:Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v3

    #@2c
    const/4 v4, 0x1

    #@2d
    iget-object v5, p0, Landroid/widget/DateTimeView;->mContentObserver:Landroid/database/ContentObserver;

    #@2f
    invoke-virtual {v3, v2, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@32
    .line 230
    return-void
.end method

.method private unregisterReceivers()V
    .registers 4

    #@0
    .prologue
    .line 233
    invoke-virtual {p0}, Landroid/widget/DateTimeView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 234
    .local v0, context:Landroid/content/Context;
    iget-object v1, p0, Landroid/widget/DateTimeView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@9
    .line 235
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Landroid/widget/DateTimeView;->mContentObserver:Landroid/database/ContentObserver;

    #@f
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@12
    .line 236
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 81
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    #@3
    .line 82
    invoke-direct {p0}, Landroid/widget/DateTimeView;->registerReceivers()V

    #@6
    .line 83
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/widget/DateTimeView;->mAttachedToWindow:Z

    #@9
    .line 84
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 88
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    #@3
    .line 89
    invoke-direct {p0}, Landroid/widget/DateTimeView;->unregisterReceivers()V

    #@6
    .line 90
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/widget/DateTimeView;->mAttachedToWindow:Z

    #@9
    .line 91
    return-void
.end method

.method public setTime(J)V
    .registers 11
    .parameter "time"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 95
    new-instance v7, Landroid/text/format/Time;

    #@3
    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    #@6
    .line 96
    .local v7, t:Landroid/text/format/Time;
    invoke-virtual {v7, p1, p2}, Landroid/text/format/Time;->set(J)V

    #@9
    .line 97
    iput v6, v7, Landroid/text/format/Time;->second:I

    #@b
    .line 98
    invoke-virtual {v7, v6}, Landroid/text/format/Time;->toMillis(Z)J

    #@e
    move-result-wide v0

    #@f
    iput-wide v0, p0, Landroid/widget/DateTimeView;->mTimeMillis:J

    #@11
    .line 99
    new-instance v0, Ljava/util/Date;

    #@13
    iget v1, v7, Landroid/text/format/Time;->year:I

    #@15
    add-int/lit16 v1, v1, -0x76c

    #@17
    iget v2, v7, Landroid/text/format/Time;->month:I

    #@19
    iget v3, v7, Landroid/text/format/Time;->monthDay:I

    #@1b
    iget v4, v7, Landroid/text/format/Time;->hour:I

    #@1d
    iget v5, v7, Landroid/text/format/Time;->minute:I

    #@1f
    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    #@22
    iput-object v0, p0, Landroid/widget/DateTimeView;->mTime:Ljava/util/Date;

    #@24
    .line 100
    invoke-virtual {p0}, Landroid/widget/DateTimeView;->update()V

    #@27
    .line 101
    return-void
.end method

.method update()V
    .registers 26

    #@0
    .prologue
    .line 104
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/DateTimeView;->mTime:Ljava/util/Date;

    #@4
    move-object/from16 v22, v0

    #@6
    if-nez v22, :cond_9

    #@8
    .line 189
    :goto_8
    return-void

    #@9
    .line 108
    :cond_9
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@c
    move-result-wide v13

    #@d
    .line 111
    .local v13, start:J
    move-object/from16 v0, p0

    #@f
    iget-object v0, v0, Landroid/widget/DateTimeView;->mTime:Ljava/util/Date;

    #@11
    move-object/from16 v17, v0

    #@13
    .line 113
    .local v17, time:Ljava/util/Date;
    new-instance v15, Landroid/text/format/Time;

    #@15
    invoke-direct {v15}, Landroid/text/format/Time;-><init>()V

    #@18
    .line 114
    .local v15, t:Landroid/text/format/Time;
    move-object/from16 v0, p0

    #@1a
    iget-wide v0, v0, Landroid/widget/DateTimeView;->mTimeMillis:J

    #@1c
    move-wide/from16 v22, v0

    #@1e
    move-wide/from16 v0, v22

    #@20
    invoke-virtual {v15, v0, v1}, Landroid/text/format/Time;->set(J)V

    #@23
    .line 115
    const/16 v22, 0x0

    #@25
    move/from16 v0, v22

    #@27
    iput v0, v15, Landroid/text/format/Time;->second:I

    #@29
    .line 117
    iget v0, v15, Landroid/text/format/Time;->hour:I

    #@2b
    move/from16 v22, v0

    #@2d
    add-int/lit8 v22, v22, -0xc

    #@2f
    move/from16 v0, v22

    #@31
    iput v0, v15, Landroid/text/format/Time;->hour:I

    #@33
    .line 118
    const/16 v22, 0x0

    #@35
    move/from16 v0, v22

    #@37
    invoke-virtual {v15, v0}, Landroid/text/format/Time;->toMillis(Z)J

    #@3a
    move-result-wide v20

    #@3b
    .line 119
    .local v20, twelveHoursBefore:J
    iget v0, v15, Landroid/text/format/Time;->hour:I

    #@3d
    move/from16 v22, v0

    #@3f
    add-int/lit8 v22, v22, 0xc

    #@41
    move/from16 v0, v22

    #@43
    iput v0, v15, Landroid/text/format/Time;->hour:I

    #@45
    .line 120
    const/16 v22, 0x0

    #@47
    move/from16 v0, v22

    #@49
    invoke-virtual {v15, v0}, Landroid/text/format/Time;->toMillis(Z)J

    #@4c
    move-result-wide v18

    #@4d
    .line 121
    .local v18, twelveHoursAfter:J
    const/16 v22, 0x0

    #@4f
    move/from16 v0, v22

    #@51
    iput v0, v15, Landroid/text/format/Time;->hour:I

    #@53
    .line 122
    const/16 v22, 0x0

    #@55
    move/from16 v0, v22

    #@57
    iput v0, v15, Landroid/text/format/Time;->minute:I

    #@59
    .line 123
    const/16 v22, 0x0

    #@5b
    move/from16 v0, v22

    #@5d
    invoke-virtual {v15, v0}, Landroid/text/format/Time;->toMillis(Z)J

    #@60
    move-result-wide v9

    #@61
    .line 124
    .local v9, midnightBefore:J
    iget v0, v15, Landroid/text/format/Time;->monthDay:I

    #@63
    move/from16 v22, v0

    #@65
    add-int/lit8 v22, v22, 0x1

    #@67
    move/from16 v0, v22

    #@69
    iput v0, v15, Landroid/text/format/Time;->monthDay:I

    #@6b
    .line 125
    const/16 v22, 0x0

    #@6d
    move/from16 v0, v22

    #@6f
    invoke-virtual {v15, v0}, Landroid/text/format/Time;->toMillis(Z)J

    #@72
    move-result-wide v7

    #@73
    .line 127
    .local v7, midnightAfter:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@76
    move-result-wide v11

    #@77
    .line 128
    .local v11, nowMillis:J
    invoke-virtual {v15, v11, v12}, Landroid/text/format/Time;->set(J)V

    #@7a
    .line 129
    const/16 v22, 0x0

    #@7c
    move/from16 v0, v22

    #@7e
    iput v0, v15, Landroid/text/format/Time;->second:I

    #@80
    .line 130
    const/16 v22, 0x0

    #@82
    move/from16 v0, v22

    #@84
    invoke-virtual {v15, v0}, Landroid/text/format/Time;->normalize(Z)J

    #@87
    move-result-wide v11

    #@88
    .line 134
    cmp-long v22, v11, v9

    #@8a
    if-ltz v22, :cond_90

    #@8c
    cmp-long v22, v11, v7

    #@8e
    if-ltz v22, :cond_98

    #@90
    :cond_90
    cmp-long v22, v11, v20

    #@92
    if-ltz v22, :cond_d4

    #@94
    cmp-long v22, v11, v18

    #@96
    if-gez v22, :cond_d4

    #@98
    .line 136
    :cond_98
    const/4 v3, 0x0

    #@99
    .line 146
    .local v3, display:I
    :goto_99
    move-object/from16 v0, p0

    #@9b
    iget v0, v0, Landroid/widget/DateTimeView;->mLastDisplay:I

    #@9d
    move/from16 v22, v0

    #@9f
    move/from16 v0, v22

    #@a1
    if-ne v3, v0, :cond_d6

    #@a3
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Landroid/widget/DateTimeView;->mLastFormat:Ljava/text/DateFormat;

    #@a7
    move-object/from16 v22, v0

    #@a9
    if-eqz v22, :cond_d6

    #@ab
    .line 148
    move-object/from16 v0, p0

    #@ad
    iget-object v6, v0, Landroid/widget/DateTimeView;->mLastFormat:Ljava/text/DateFormat;

    #@af
    .line 164
    .local v6, format:Ljava/text/DateFormat;
    :goto_af
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Landroid/widget/DateTimeView;->mTime:Ljava/util/Date;

    #@b3
    move-object/from16 v22, v0

    #@b5
    move-object/from16 v0, v22

    #@b7
    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@ba
    move-result-object v16

    #@bb
    .line 165
    .local v16, text:Ljava/lang/String;
    move-object/from16 v0, p0

    #@bd
    move-object/from16 v1, v16

    #@bf
    invoke-virtual {v0, v1}, Landroid/widget/DateTimeView;->setText(Ljava/lang/CharSequence;)V

    #@c2
    .line 168
    if-nez v3, :cond_106

    #@c4
    .line 170
    cmp-long v22, v18, v7

    #@c6
    if-lez v22, :cond_103

    #@c8
    .end local v18           #twelveHoursAfter:J
    :goto_c8
    move-wide/from16 v0, v18

    #@ca
    move-object/from16 v2, p0

    #@cc
    iput-wide v0, v2, Landroid/widget/DateTimeView;->mUpdateTimeMillis:J

    #@ce
    .line 188
    .end local v20           #twelveHoursBefore:J
    :goto_ce
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@d1
    move-result-wide v4

    #@d2
    .line 189
    .local v4, finish:J
    goto/16 :goto_8

    #@d4
    .line 140
    .end local v3           #display:I
    .end local v4           #finish:J
    .end local v6           #format:Ljava/text/DateFormat;
    .end local v16           #text:Ljava/lang/String;
    .restart local v18       #twelveHoursAfter:J
    .restart local v20       #twelveHoursBefore:J
    :cond_d4
    const/4 v3, 0x1

    #@d5
    .restart local v3       #display:I
    goto :goto_99

    #@d6
    .line 150
    :cond_d6
    packed-switch v3, :pswitch_data_128

    #@d9
    .line 158
    new-instance v22, Ljava/lang/RuntimeException;

    #@db
    new-instance v23, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string/jumbo v24, "unknown display value: "

    #@e3
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v23

    #@e7
    move-object/from16 v0, v23

    #@e9
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v23

    #@ed
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v23

    #@f1
    invoke-direct/range {v22 .. v23}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f4
    throw v22

    #@f5
    .line 152
    :pswitch_f5
    invoke-direct/range {p0 .. p0}, Landroid/widget/DateTimeView;->getTimeFormat()Ljava/text/DateFormat;

    #@f8
    move-result-object v6

    #@f9
    .line 160
    .restart local v6       #format:Ljava/text/DateFormat;
    :goto_f9
    move-object/from16 v0, p0

    #@fb
    iput-object v6, v0, Landroid/widget/DateTimeView;->mLastFormat:Ljava/text/DateFormat;

    #@fd
    goto :goto_af

    #@fe
    .line 155
    .end local v6           #format:Ljava/text/DateFormat;
    :pswitch_fe
    invoke-direct/range {p0 .. p0}, Landroid/widget/DateTimeView;->getDateFormat()Ljava/text/DateFormat;

    #@101
    move-result-object v6

    #@102
    .line 156
    .restart local v6       #format:Ljava/text/DateFormat;
    goto :goto_f9

    #@103
    .restart local v16       #text:Ljava/lang/String;
    :cond_103
    move-wide/from16 v18, v7

    #@105
    .line 170
    goto :goto_c8

    #@106
    .line 173
    :cond_106
    move-object/from16 v0, p0

    #@108
    iget-wide v0, v0, Landroid/widget/DateTimeView;->mTimeMillis:J

    #@10a
    move-wide/from16 v22, v0

    #@10c
    cmp-long v22, v22, v11

    #@10e
    if-gez v22, :cond_119

    #@110
    .line 175
    const-wide/16 v22, 0x0

    #@112
    move-wide/from16 v0, v22

    #@114
    move-object/from16 v2, p0

    #@116
    iput-wide v0, v2, Landroid/widget/DateTimeView;->mUpdateTimeMillis:J

    #@118
    goto :goto_ce

    #@119
    .line 179
    :cond_119
    cmp-long v22, v20, v9

    #@11b
    if-gez v22, :cond_124

    #@11d
    .end local v20           #twelveHoursBefore:J
    :goto_11d
    move-wide/from16 v0, v20

    #@11f
    move-object/from16 v2, p0

    #@121
    iput-wide v0, v2, Landroid/widget/DateTimeView;->mUpdateTimeMillis:J

    #@123
    goto :goto_ce

    #@124
    .restart local v20       #twelveHoursBefore:J
    :cond_124
    move-wide/from16 v20, v9

    #@126
    goto :goto_11d

    #@127
    .line 150
    nop

    #@128
    :pswitch_data_128
    .packed-switch 0x0
        :pswitch_f5
        :pswitch_fe
    .end packed-switch
.end method
