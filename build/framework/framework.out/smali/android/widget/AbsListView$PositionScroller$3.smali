.class Landroid/widget/AbsListView$PositionScroller$3;
.super Ljava/lang/Object;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/AbsListView$PositionScroller;->startWithOffset(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/AbsListView$PositionScroller;

.field final synthetic val$duration:I

.field final synthetic val$position:I

.field final synthetic val$postOffset:I


# direct methods
.method constructor <init>(Landroid/widget/AbsListView$PositionScroller;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 4549
    iput-object p1, p0, Landroid/widget/AbsListView$PositionScroller$3;->this$1:Landroid/widget/AbsListView$PositionScroller;

    #@2
    iput p2, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$position:I

    #@4
    iput p3, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$postOffset:I

    #@6
    iput p4, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$duration:I

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 4551
    iget-object v0, p0, Landroid/widget/AbsListView$PositionScroller$3;->this$1:Landroid/widget/AbsListView$PositionScroller;

    #@2
    iget v1, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$position:I

    #@4
    iget v2, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$postOffset:I

    #@6
    iget v3, p0, Landroid/widget/AbsListView$PositionScroller$3;->val$duration:I

    #@8
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView$PositionScroller;->startWithOffset(III)V

    #@b
    .line 4552
    return-void
.end method
