.class public Landroid/widget/TimePicker;
.super Landroid/widget/FrameLayout;
.source "TimePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TimePicker$SavedState;,
        Landroid/widget/TimePicker$OnTimeChangedListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_ENABLED_STATE:Z = true

.field private static final HOURS_IN_HALF_DAY:I = 0xc

.field private static final NO_OP_CHANGE_LISTENER:Landroid/widget/TimePicker$OnTimeChangedListener;


# instance fields
.field private final mAmPmButton:Landroid/widget/Button;

.field private final mAmPmSpinner:Landroid/widget/NumberPicker;

.field private final mAmPmSpinnerInput:Landroid/widget/EditText;

.field private final mAmPmStrings:[Ljava/lang/String;

.field private mCurrentLocale:Ljava/util/Locale;

.field private final mDivider:Landroid/widget/TextView;

.field private final mHourSpinner:Landroid/widget/NumberPicker;

.field private final mHourSpinnerInput:Landroid/widget/EditText;

.field private mIs24HourView:Z

.field private mIsAm:Z

.field private mIsEnabled:Z

.field private final mMinuteSpinner:Landroid/widget/NumberPicker;

.field private final mMinuteSpinnerInput:Landroid/widget/EditText;

.field private mOnTimeChangedListener:Landroid/widget/TimePicker$OnTimeChangedListener;

.field private mTempCalendar:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    new-instance v0, Landroid/widget/TimePicker$1;

    #@2
    invoke-direct {v0}, Landroid/widget/TimePicker$1;-><init>()V

    #@5
    sput-object v0, Landroid/widget/TimePicker;->NO_OP_CHANGE_LISTENER:Landroid/widget/TimePicker$OnTimeChangedListener;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 122
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 126
    const v0, 0x10103df

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 16
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v11, 0x5

    #@1
    const/4 v10, 0x0

    #@2
    const v9, 0x1020349

    #@5
    const/4 v8, 0x1

    #@6
    const/4 v7, 0x0

    #@7
    .line 130
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@a
    .line 99
    iput-boolean v8, p0, Landroid/widget/TimePicker;->mIsEnabled:Z

    #@c
    .line 133
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@f
    move-result-object v4

    #@10
    invoke-direct {p0, v4}, Landroid/widget/TimePicker;->setCurrentLocale(Ljava/util/Locale;)V

    #@13
    .line 136
    sget-object v4, Lcom/android/internal/R$styleable;->TimePicker:[I

    #@15
    invoke-virtual {p1, p2, v4, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@18
    move-result-object v1

    #@19
    .line 138
    .local v1, attributesArray:Landroid/content/res/TypedArray;
    const v4, 0x10900df

    #@1c
    invoke-virtual {v1, v7, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1f
    move-result v3

    #@20
    .line 140
    .local v3, layoutResourceId:I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@23
    .line 142
    const-string/jumbo v4, "layout_inflater"

    #@26
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@29
    move-result-object v2

    #@2a
    check-cast v2, Landroid/view/LayoutInflater;

    #@2c
    .line 144
    .local v2, inflater:Landroid/view/LayoutInflater;
    invoke-virtual {v2, v3, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@2f
    .line 147
    const v4, 0x10203a3

    #@32
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->findViewById(I)Landroid/view/View;

    #@35
    move-result-object v4

    #@36
    check-cast v4, Landroid/widget/NumberPicker;

    #@38
    iput-object v4, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@3a
    .line 148
    iget-object v4, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@3c
    new-instance v5, Landroid/widget/TimePicker$2;

    #@3e
    invoke-direct {v5, p0}, Landroid/widget/TimePicker$2;-><init>(Landroid/widget/TimePicker;)V

    #@41
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@44
    .line 161
    iget-object v4, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@46
    invoke-virtual {v4, v9}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@49
    move-result-object v4

    #@4a
    check-cast v4, Landroid/widget/EditText;

    #@4c
    iput-object v4, p0, Landroid/widget/TimePicker;->mHourSpinnerInput:Landroid/widget/EditText;

    #@4e
    .line 162
    iget-object v4, p0, Landroid/widget/TimePicker;->mHourSpinnerInput:Landroid/widget/EditText;

    #@50
    invoke-virtual {v4, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    #@53
    .line 165
    const v4, 0x10203a7

    #@56
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->findViewById(I)Landroid/view/View;

    #@59
    move-result-object v4

    #@5a
    check-cast v4, Landroid/widget/TextView;

    #@5c
    iput-object v4, p0, Landroid/widget/TimePicker;->mDivider:Landroid/widget/TextView;

    #@5e
    .line 166
    iget-object v4, p0, Landroid/widget/TimePicker;->mDivider:Landroid/widget/TextView;

    #@60
    if-eqz v4, :cond_6a

    #@62
    .line 167
    iget-object v4, p0, Landroid/widget/TimePicker;->mDivider:Landroid/widget/TextView;

    #@64
    const v5, 0x1040087

    #@67
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    #@6a
    .line 171
    :cond_6a
    const v4, 0x10203a4

    #@6d
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->findViewById(I)Landroid/view/View;

    #@70
    move-result-object v4

    #@71
    check-cast v4, Landroid/widget/NumberPicker;

    #@73
    iput-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@75
    .line 172
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@77
    invoke-virtual {v4, v7}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@7a
    .line 173
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@7c
    const/16 v5, 0x3b

    #@7e
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@81
    .line 174
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@83
    const-wide/16 v5, 0x64

    #@85
    invoke-virtual {v4, v5, v6}, Landroid/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    #@88
    .line 175
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@8a
    invoke-static {}, Landroid/widget/NumberPicker;->getTwoDigitFormatter()Landroid/widget/NumberPicker$Formatter;

    #@8d
    move-result-object v5

    #@8e
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    #@91
    .line 176
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@93
    new-instance v5, Landroid/widget/TimePicker$3;

    #@95
    invoke-direct {v5, p0}, Landroid/widget/TimePicker$3;-><init>(Landroid/widget/TimePicker;)V

    #@98
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@9b
    .line 199
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@9d
    invoke-virtual {v4, v9}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@a0
    move-result-object v4

    #@a1
    check-cast v4, Landroid/widget/EditText;

    #@a3
    iput-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@a5
    .line 200
    iget-object v4, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@a7
    invoke-virtual {v4, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    #@aa
    .line 203
    new-instance v4, Ljava/text/DateFormatSymbols;

    #@ac
    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    #@af
    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    iput-object v4, p0, Landroid/widget/TimePicker;->mAmPmStrings:[Ljava/lang/String;

    #@b5
    .line 206
    const v4, 0x10203a5

    #@b8
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->findViewById(I)Landroid/view/View;

    #@bb
    move-result-object v0

    #@bc
    .line 207
    .local v0, amPmView:Landroid/view/View;
    instance-of v4, v0, Landroid/widget/Button;

    #@be
    if-eqz v4, :cond_114

    #@c0
    .line 208
    iput-object v10, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@c2
    .line 209
    iput-object v10, p0, Landroid/widget/TimePicker;->mAmPmSpinnerInput:Landroid/widget/EditText;

    #@c4
    .line 210
    check-cast v0, Landroid/widget/Button;

    #@c6
    .end local v0           #amPmView:Landroid/view/View;
    iput-object v0, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@c8
    .line 211
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@ca
    new-instance v5, Landroid/widget/TimePicker$4;

    #@cc
    invoke-direct {v5, p0}, Landroid/widget/TimePicker$4;-><init>(Landroid/widget/TimePicker;)V

    #@cf
    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d2
    .line 239
    :goto_d2
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateHourControl()V

    #@d5
    .line 241
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateMinuteControl()V

    #@d8
    .line 243
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateAmPmControl()V

    #@db
    .line 245
    sget-object v4, Landroid/widget/TimePicker;->NO_OP_CHANGE_LISTENER:Landroid/widget/TimePicker$OnTimeChangedListener;

    #@dd
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    #@e0
    .line 248
    iget-object v4, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@e2
    const/16 v5, 0xb

    #@e4
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    #@e7
    move-result v4

    #@e8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@eb
    move-result-object v4

    #@ec
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@ef
    .line 249
    iget-object v4, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@f1
    const/16 v5, 0xc

    #@f3
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    #@f6
    move-result v4

    #@f7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fa
    move-result-object v4

    #@fb
    invoke-virtual {p0, v4}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    #@fe
    .line 251
    invoke-virtual {p0}, Landroid/widget/TimePicker;->isEnabled()Z

    #@101
    move-result v4

    #@102
    if-nez v4, :cond_107

    #@104
    .line 252
    invoke-virtual {p0, v7}, Landroid/widget/TimePicker;->setEnabled(Z)V

    #@107
    .line 256
    :cond_107
    invoke-direct {p0}, Landroid/widget/TimePicker;->setContentDescriptions()V

    #@10a
    .line 259
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getImportantForAccessibility()I

    #@10d
    move-result v4

    #@10e
    if-nez v4, :cond_113

    #@110
    .line 260
    invoke-virtual {p0, v8}, Landroid/widget/TimePicker;->setImportantForAccessibility(I)V

    #@113
    .line 262
    :cond_113
    return-void

    #@114
    .line 220
    .restart local v0       #amPmView:Landroid/view/View;
    :cond_114
    iput-object v10, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@116
    .line 221
    check-cast v0, Landroid/widget/NumberPicker;

    #@118
    .end local v0           #amPmView:Landroid/view/View;
    iput-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@11a
    .line 222
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@11c
    invoke-virtual {v4, v7}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@11f
    .line 223
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@121
    invoke-virtual {v4, v8}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@124
    .line 224
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@126
    iget-object v5, p0, Landroid/widget/TimePicker;->mAmPmStrings:[Ljava/lang/String;

    #@128
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    #@12b
    .line 225
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@12d
    new-instance v5, Landroid/widget/TimePicker$5;

    #@12f
    invoke-direct {v5, p0}, Landroid/widget/TimePicker$5;-><init>(Landroid/widget/TimePicker;)V

    #@132
    invoke-virtual {v4, v5}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    #@135
    .line 234
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@137
    invoke-virtual {v4, v9}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    #@13a
    move-result-object v4

    #@13b
    check-cast v4, Landroid/widget/EditText;

    #@13d
    iput-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinnerInput:Landroid/widget/EditText;

    #@13f
    .line 235
    iget-object v4, p0, Landroid/widget/TimePicker;->mAmPmSpinnerInput:Landroid/widget/EditText;

    #@141
    const/4 v5, 0x6

    #@142
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setImeOptions(I)V

    #@145
    goto :goto_d2
.end method

.method static synthetic access$000(Landroid/widget/TimePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateInputState()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/TimePicker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-boolean v0, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/widget/TimePicker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    iput-boolean p1, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/widget/TimePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateAmPmControl()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/TimePicker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/widget/TimePicker;->onTimeChanged()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@2
    return-object v0
.end method

.method private onTimeChanged()V
    .registers 4

    #@0
    .prologue
    .line 551
    const/4 v0, 0x4

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/TimePicker;->sendAccessibilityEvent(I)V

    #@4
    .line 552
    iget-object v0, p0, Landroid/widget/TimePicker;->mOnTimeChangedListener:Landroid/widget/TimePicker$OnTimeChangedListener;

    #@6
    if-eqz v0, :cond_1d

    #@8
    .line 553
    iget-object v0, p0, Landroid/widget/TimePicker;->mOnTimeChangedListener:Landroid/widget/TimePicker$OnTimeChangedListener;

    #@a
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@11
    move-result v1

    #@12
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@19
    move-result v2

    #@1a
    invoke-interface {v0, p0, v1, v2}, Landroid/widget/TimePicker$OnTimeChangedListener;->onTimeChanged(Landroid/widget/TimePicker;II)V

    #@1d
    .line 555
    :cond_1d
    return-void
.end method

.method private setContentDescriptions()V
    .registers 5

    #@0
    .prologue
    const v3, 0x102034a

    #@3
    const v2, 0x1020348

    #@6
    .line 559
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@8
    const v1, 0x10404e7

    #@b
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@e
    .line 561
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@10
    const v1, 0x10404e8

    #@13
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@16
    .line 564
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@18
    const v1, 0x10404e9

    #@1b
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@1e
    .line 566
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@20
    const v1, 0x10404ea

    #@23
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@26
    .line 569
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@28
    if-eqz v0, :cond_3a

    #@2a
    .line 570
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@2c
    const v1, 0x10404eb

    #@2f
    invoke-direct {p0, v0, v2, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@32
    .line 572
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@34
    const v1, 0x10404ec

    #@37
    invoke-direct {p0, v0, v3, v1}, Landroid/widget/TimePicker;->trySetContentDescription(Landroid/view/View;II)V

    #@3a
    .line 575
    :cond_3a
    return-void
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/widget/TimePicker;->mCurrentLocale:Ljava/util/Locale;

    #@2
    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 305
    :goto_8
    return-void

    #@9
    .line 303
    :cond_9
    iput-object p1, p0, Landroid/widget/TimePicker;->mCurrentLocale:Ljava/util/Locale;

    #@b
    .line 304
    invoke-static {p1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@11
    goto :goto_8
.end method

.method private trySetContentDescription(Landroid/view/View;II)V
    .registers 6
    .parameter "root"
    .parameter "viewId"
    .parameter "contDescResId"

    #@0
    .prologue
    .line 578
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 579
    .local v0, target:Landroid/view/View;
    if-eqz v0, :cond_f

    #@6
    .line 580
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    #@f
    .line 582
    :cond_f
    return-void
.end method

.method private updateAmPmControl()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v1, 0x0

    #@3
    .line 518
    invoke-virtual {p0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_1d

    #@9
    .line 519
    iget-object v1, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@b
    if-eqz v1, :cond_17

    #@d
    .line 520
    iget-object v1, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@f
    invoke-virtual {v1, v3}, Landroid/widget/NumberPicker;->setVisibility(I)V

    #@12
    .line 534
    :goto_12
    const/4 v1, 0x4

    #@13
    invoke-virtual {p0, v1}, Landroid/widget/TimePicker;->sendAccessibilityEvent(I)V

    #@16
    .line 535
    return-void

    #@17
    .line 522
    :cond_17
    iget-object v1, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@19
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    #@1c
    goto :goto_12

    #@1d
    .line 525
    :cond_1d
    iget-boolean v2, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@1f
    if-eqz v2, :cond_31

    #@21
    move v0, v1

    #@22
    .line 526
    .local v0, index:I
    :goto_22
    iget-object v2, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@24
    if-eqz v2, :cond_33

    #@26
    .line 527
    iget-object v2, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@28
    invoke-virtual {v2, v0}, Landroid/widget/NumberPicker;->setValue(I)V

    #@2b
    .line 528
    iget-object v2, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@2d
    invoke-virtual {v2, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    #@30
    goto :goto_12

    #@31
    .line 525
    .end local v0           #index:I
    :cond_31
    const/4 v0, 0x1

    #@32
    goto :goto_22

    #@33
    .line 530
    .restart local v0       #index:I
    :cond_33
    iget-object v2, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@35
    iget-object v3, p0, Landroid/widget/TimePicker;->mAmPmStrings:[Ljava/lang/String;

    #@37
    aget-object v3, v3, v0

    #@39
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@3c
    .line 531
    iget-object v2, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@3e
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    #@41
    goto :goto_12
.end method

.method private updateHourControl()V
    .registers 3

    #@0
    .prologue
    .line 506
    invoke-virtual {p0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1d

    #@6
    .line 507
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@c
    .line 508
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@e
    const/16 v1, 0x17

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@13
    .line 509
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@15
    invoke-static {}, Landroid/widget/NumberPicker;->getTwoDigitFormatter()Landroid/widget/NumberPicker$Formatter;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    #@1c
    .line 515
    :goto_1c
    return-void

    #@1d
    .line 511
    :cond_1d
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@1f
    const/4 v1, 0x1

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMinValue(I)V

    #@23
    .line 512
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@25
    const/16 v1, 0xc

    #@27
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    #@2a
    .line 513
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@2c
    const/4 v1, 0x0

    #@2d
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    #@30
    goto :goto_1c
.end method

.method private updateInputState()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 590
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@4
    move-result-object v0

    #@5
    .line 591
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1b

    #@7
    .line 592
    iget-object v1, p0, Landroid/widget/TimePicker;->mHourSpinnerInput:Landroid/widget/EditText;

    #@9
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1c

    #@f
    .line 593
    iget-object v1, p0, Landroid/widget/TimePicker;->mHourSpinnerInput:Landroid/widget/EditText;

    #@11
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@14
    .line 594
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getWindowToken()Landroid/os/IBinder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1b
    .line 603
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 595
    :cond_1c
    iget-object v1, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_31

    #@24
    .line 596
    iget-object v1, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@26
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@29
    .line 597
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getWindowToken()Landroid/os/IBinder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@30
    goto :goto_1b

    #@31
    .line 598
    :cond_31
    iget-object v1, p0, Landroid/widget/TimePicker;->mAmPmSpinnerInput:Landroid/widget/EditText;

    #@33
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_1b

    #@39
    .line 599
    iget-object v1, p0, Landroid/widget/TimePicker;->mAmPmSpinnerInput:Landroid/widget/EditText;

    #@3b
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    #@3e
    .line 600
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getWindowToken()Landroid/os/IBinder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@45
    goto :goto_1b
.end method

.method private updateMinuteControl()V
    .registers 3

    #@0
    .prologue
    .line 538
    invoke-virtual {p0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 539
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@8
    if-eqz v0, :cond_10

    #@a
    .line 540
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@c
    const/4 v1, 0x6

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    #@10
    .line 547
    :cond_10
    :goto_10
    return-void

    #@11
    .line 543
    :cond_11
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@13
    if-eqz v0, :cond_10

    #@15
    .line 544
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinnerInput:Landroid/widget/EditText;

    #@17
    const/4 v1, 0x5

    #@18
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    #@1b
    goto :goto_10
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 472
    invoke-virtual {p0, p1}, Landroid/widget/TimePicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 473
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public getBaseline()I
    .registers 2

    #@0
    .prologue
    .line 467
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@2
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getBaseline()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCurrentHour()Ljava/lang/Integer;
    .registers 3

    #@0
    .prologue
    .line 382
    iget-object v1, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@2
    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    #@5
    move-result v0

    #@6
    .line 383
    .local v0, currentHour:I
    invoke-virtual {p0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 384
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v1

    #@10
    .line 388
    :goto_10
    return-object v1

    #@11
    .line 385
    :cond_11
    iget-boolean v1, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@13
    if-eqz v1, :cond_1c

    #@15
    .line 386
    rem-int/lit8 v1, v0, 0xc

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    goto :goto_10

    #@1c
    .line 388
    :cond_1c
    rem-int/lit8 v1, v0, 0xc

    #@1e
    add-int/lit8 v1, v1, 0xc

    #@20
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v1

    #@24
    goto :goto_10
.end method

.method public getCurrentMinute()Ljava/lang/Integer;
    .registers 2

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@2
    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    #@5
    move-result v0

    #@6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public is24HourView()Z
    .registers 2

    #@0
    .prologue
    .line 444
    iget-boolean v0, p0, Landroid/widget/TimePicker;->mIs24HourView:Z

    #@2
    return v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 285
    iget-boolean v0, p0, Landroid/widget/TimePicker;->mIsEnabled:Z

    #@2
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 290
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 291
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5
    invoke-direct {p0, v0}, Landroid/widget/TimePicker;->setCurrentLocale(Ljava/util/Locale;)V

    #@8
    .line 292
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 495
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 496
    const-class v0, Landroid/widget/TimePicker;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 497
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 501
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 502
    const-class v0, Landroid/widget/TimePicker;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 503
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 478
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 480
    const/4 v0, 0x1

    #@4
    .line 481
    .local v0, flags:I
    iget-boolean v2, p0, Landroid/widget/TimePicker;->mIs24HourView:Z

    #@6
    if-eqz v2, :cond_3c

    #@8
    .line 482
    or-int/lit16 v0, v0, 0x80

    #@a
    .line 486
    :goto_a
    iget-object v2, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@c
    const/16 v3, 0xb

    #@e
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v4

    #@16
    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@19
    .line 487
    iget-object v2, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@1b
    const/16 v3, 0xc

    #@1d
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@24
    move-result v4

    #@25
    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    #@28
    .line 488
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2a
    iget-object v3, p0, Landroid/widget/TimePicker;->mTempCalendar:Ljava/util/Calendar;

    #@2c
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2f
    move-result-wide v3

    #@30
    invoke-static {v2, v3, v4, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 490
    .local v1, selectedDateUtterance:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@37
    move-result-object v2

    #@38
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3b
    .line 491
    return-void

    #@3c
    .line 484
    .end local v1           #selectedDateUtterance:Ljava/lang/String;
    :cond_3c
    or-int/lit8 v0, v0, 0x40

    #@3e
    goto :goto_a
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 363
    move-object v0, p1

    #@1
    check-cast v0, Landroid/widget/TimePicker$SavedState;

    #@3
    .line 364
    .local v0, ss:Landroid/widget/TimePicker$SavedState;
    invoke-virtual {v0}, Landroid/widget/TimePicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 365
    invoke-virtual {v0}, Landroid/widget/TimePicker$SavedState;->getHour()I

    #@d
    move-result v1

    #@e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {p0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@15
    .line 366
    invoke-virtual {v0}, Landroid/widget/TimePicker$SavedState;->getMinute()I

    #@18
    move-result v1

    #@19
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    #@20
    .line 367
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 6

    #@0
    .prologue
    .line 357
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v0

    #@4
    .line 358
    .local v0, superState:Landroid/os/Parcelable;
    new-instance v1, Landroid/widget/TimePicker$SavedState;

    #@6
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v2

    #@e
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v3

    #@16
    const/4 v4, 0x0

    #@17
    invoke-direct {v1, v0, v2, v3, v4}, Landroid/widget/TimePicker$SavedState;-><init>(Landroid/os/Parcelable;IILandroid/widget/TimePicker$1;)V

    #@1a
    return-object v1
.end method

.method public setCurrentHour(Ljava/lang/Integer;)V
    .registers 4
    .parameter "currentHour"

    #@0
    .prologue
    const/16 v1, 0xc

    #@2
    .line 397
    if-eqz p1, :cond_a

    #@4
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@7
    move-result-object v0

    #@8
    if-ne p1, v0, :cond_b

    #@a
    .line 417
    :cond_a
    :goto_a
    return-void

    #@b
    .line 400
    :cond_b
    invoke-virtual {p0}, Landroid/widget/TimePicker;->is24HourView()Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_2d

    #@11
    .line 402
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@14
    move-result v0

    #@15
    if-lt v0, v1, :cond_3a

    #@17
    .line 403
    const/4 v0, 0x0

    #@18
    iput-boolean v0, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@1a
    .line 404
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@1d
    move-result v0

    #@1e
    if-le v0, v1, :cond_2a

    #@20
    .line 405
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@23
    move-result v0

    #@24
    add-int/lit8 v0, v0, -0xc

    #@26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object p1

    #@2a
    .line 413
    :cond_2a
    :goto_2a
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateAmPmControl()V

    #@2d
    .line 415
    :cond_2d
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@2f
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@32
    move-result v1

    #@33
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    #@36
    .line 416
    invoke-direct {p0}, Landroid/widget/TimePicker;->onTimeChanged()V

    #@39
    goto :goto_a

    #@3a
    .line 408
    :cond_3a
    const/4 v0, 0x1

    #@3b
    iput-boolean v0, p0, Landroid/widget/TimePicker;->mIsAm:Z

    #@3d
    .line 409
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@40
    move-result v0

    #@41
    if-nez v0, :cond_2a

    #@43
    .line 410
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object p1

    #@47
    goto :goto_2a
.end method

.method public setCurrentMinute(Ljava/lang/Integer;)V
    .registers 4
    .parameter "currentMinute"

    #@0
    .prologue
    .line 458
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    if-ne p1, v0, :cond_7

    #@6
    .line 463
    :goto_6
    return-void

    #@7
    .line 461
    :cond_7
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@c
    move-result v1

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    #@10
    .line 462
    invoke-direct {p0}, Landroid/widget/TimePicker;->onTimeChanged()V

    #@13
    goto :goto_6
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 266
    iget-boolean v0, p0, Landroid/widget/TimePicker;->mIsEnabled:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 281
    :goto_4
    return-void

    #@5
    .line 269
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    #@8
    .line 270
    iget-object v0, p0, Landroid/widget/TimePicker;->mMinuteSpinner:Landroid/widget/NumberPicker;

    #@a
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@d
    .line 271
    iget-object v0, p0, Landroid/widget/TimePicker;->mDivider:Landroid/widget/TextView;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 272
    iget-object v0, p0, Landroid/widget/TimePicker;->mDivider:Landroid/widget/TextView;

    #@13
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@16
    .line 274
    :cond_16
    iget-object v0, p0, Landroid/widget/TimePicker;->mHourSpinner:Landroid/widget/NumberPicker;

    #@18
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@1b
    .line 275
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@1d
    if-eqz v0, :cond_27

    #@1f
    .line 276
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmSpinner:Landroid/widget/NumberPicker;

    #@21
    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setEnabled(Z)V

    #@24
    .line 280
    :goto_24
    iput-boolean p1, p0, Landroid/widget/TimePicker;->mIsEnabled:Z

    #@26
    goto :goto_4

    #@27
    .line 278
    :cond_27
    iget-object v0, p0, Landroid/widget/TimePicker;->mAmPmButton:Landroid/widget/Button;

    #@29
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    #@2c
    goto :goto_24
.end method

.method public setIs24HourView(Ljava/lang/Boolean;)V
    .registers 5
    .parameter "is24HourView"

    #@0
    .prologue
    .line 425
    iget-boolean v1, p0, Landroid/widget/TimePicker;->mIs24HourView:Z

    #@2
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@5
    move-result v2

    #@6
    if-ne v1, v2, :cond_9

    #@8
    .line 438
    :goto_8
    return-void

    #@9
    .line 428
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@c
    move-result v1

    #@d
    iput-boolean v1, p0, Landroid/widget/TimePicker;->mIs24HourView:Z

    #@f
    .line 430
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@16
    move-result v0

    #@17
    .line 431
    .local v0, currentHour:I
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateHourControl()V

    #@1a
    .line 433
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@21
    .line 435
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateMinuteControl()V

    #@24
    .line 437
    invoke-direct {p0}, Landroid/widget/TimePicker;->updateAmPmControl()V

    #@27
    goto :goto_8
.end method

.method public setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V
    .registers 2
    .parameter "onTimeChangedListener"

    #@0
    .prologue
    .line 375
    iput-object p1, p0, Landroid/widget/TimePicker;->mOnTimeChangedListener:Landroid/widget/TimePicker$OnTimeChangedListener;

    #@2
    .line 376
    return-void
.end method
