.class Landroid/widget/SpellChecker$1;
.super Ljava/lang/Object;
.source "SpellChecker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/SpellChecker;->scheduleNewSpellCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/SpellChecker;


# direct methods
.method constructor <init>(Landroid/widget/SpellChecker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 434
    iput-object p1, p0, Landroid/widget/SpellChecker$1;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 437
    iget-object v3, p0, Landroid/widget/SpellChecker$1;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-static {v3}, Landroid/widget/SpellChecker;->access$100(Landroid/widget/SpellChecker;)[Landroid/widget/SpellChecker$SpellParser;

    #@5
    move-result-object v3

    #@6
    array-length v1, v3

    #@7
    .line 438
    .local v1, length:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_1b

    #@a
    .line 439
    iget-object v3, p0, Landroid/widget/SpellChecker$1;->this$0:Landroid/widget/SpellChecker;

    #@c
    invoke-static {v3}, Landroid/widget/SpellChecker;->access$100(Landroid/widget/SpellChecker;)[Landroid/widget/SpellChecker$SpellParser;

    #@f
    move-result-object v3

    #@10
    aget-object v2, v3, v0

    #@12
    .line 440
    .local v2, spellParser:Landroid/widget/SpellChecker$SpellParser;
    invoke-virtual {v2}, Landroid/widget/SpellChecker$SpellParser;->isFinished()Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_1c

    #@18
    .line 441
    invoke-virtual {v2}, Landroid/widget/SpellChecker$SpellParser;->parse()V

    #@1b
    .line 445
    .end local v2           #spellParser:Landroid/widget/SpellChecker$SpellParser;
    :cond_1b
    return-void

    #@1c
    .line 438
    .restart local v2       #spellParser:Landroid/widget/SpellChecker$SpellParser;
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_8
.end method
