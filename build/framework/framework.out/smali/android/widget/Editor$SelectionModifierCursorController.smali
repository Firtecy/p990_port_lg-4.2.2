.class Landroid/widget/Editor$SelectionModifierCursorController;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Landroid/widget/Editor$CursorController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SelectionModifierCursorController"
.end annotation


# static fields
.field private static final DELAY_BEFORE_REPLACE_ACTION:I = 0xc8


# instance fields
.field private mDownPositionX:F

.field private mDownPositionY:F

.field private mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

.field private mGestureStayedInTapRegion:Z

.field private mMaxTouchOffset:I

.field private mMinTouchOffset:I

.field private mPreviousTapUpTime:J

.field private mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method constructor <init>(Landroid/widget/Editor;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 4611
    iput-object p1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4607
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mPreviousTapUpTime:J

    #@9
    .line 4612
    invoke-virtual {p0}, Landroid/widget/Editor$SelectionModifierCursorController;->resetTouchOffsets()V

    #@c
    .line 4613
    return-void
.end method

.method private initDrawables()V
    .registers 4

    #@0
    .prologue
    .line 4633
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$3600(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_27

    #@8
    .line 4634
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@a
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@c
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@1a
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1d
    move-result-object v2

    #@1e
    iget v2, v2, Landroid/widget/TextView;->mTextSelectHandleLeftRes:I

    #@20
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$3602(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    #@27
    .line 4637
    :cond_27
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v0}, Landroid/widget/Editor;->access$3700(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@2c
    move-result-object v0

    #@2d
    if-nez v0, :cond_4e

    #@2f
    .line 4638
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@31
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@33
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v1

    #@3f
    iget-object v2, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@41
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@44
    move-result-object v2

    #@45
    iget v2, v2, Landroid/widget/TextView;->mTextSelectHandleRightRes:I

    #@47
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/widget/Editor;->access$3702(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    #@4e
    .line 4641
    :cond_4e
    return-void
.end method

.method private initHandles()V
    .registers 5

    #@0
    .prologue
    .line 4645
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@2
    if-nez v0, :cond_19

    #@4
    .line 4646
    new-instance v0, Landroid/widget/Editor$SelectionStartHandleView;

    #@6
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@8
    iget-object v2, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@a
    invoke-static {v2}, Landroid/widget/Editor;->access$3600(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@10
    invoke-static {v3}, Landroid/widget/Editor;->access$3700(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@13
    move-result-object v3

    #@14
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/Editor$SelectionStartHandleView;-><init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@17
    iput-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@19
    .line 4648
    :cond_19
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@1b
    if-nez v0, :cond_32

    #@1d
    .line 4649
    new-instance v0, Landroid/widget/Editor$SelectionEndHandleView;

    #@1f
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@21
    iget-object v2, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@23
    invoke-static {v2}, Landroid/widget/Editor;->access$3700(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v2

    #@27
    iget-object v3, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v3}, Landroid/widget/Editor;->access$3600(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;

    #@2c
    move-result-object v3

    #@2d
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/Editor$SelectionEndHandleView;-><init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@30
    iput-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@32
    .line 4652
    :cond_32
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@34
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionStartHandleView;->show()V

    #@37
    .line 4653
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@39
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionEndHandleView;->show()V

    #@3c
    .line 4657
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@3e
    const/16 v1, 0xc8

    #@40
    invoke-virtual {v0, v1}, Landroid/widget/Editor$SelectionStartHandleView;->showActionPopupWindow(I)V

    #@43
    .line 4658
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@45
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@47
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionStartHandleView;->getActionPopupWindow()Landroid/widget/Editor$ActionPopupWindow;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v0, v1}, Landroid/widget/Editor$SelectionEndHandleView;->setActionPopupWindow(Landroid/widget/Editor$ActionPopupWindow;)V

    #@4e
    .line 4660
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@50
    invoke-static {v0}, Landroid/widget/Editor;->access$3500(Landroid/widget/Editor;)V

    #@53
    .line 4661
    return-void
.end method

.method private updateMinAndMaxOffsets(Landroid/view/MotionEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 4758
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v2

    #@4
    .line 4759
    .local v2, pointerCount:I
    const/4 v0, 0x0

    #@5
    .local v0, index:I
    :goto_5
    if-ge v0, v2, :cond_28

    #@7
    .line 4760
    iget-object v3, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@9
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@10
    move-result v4

    #@11
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@14
    move-result v5

    #@15
    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@18
    move-result v1

    #@19
    .line 4761
    .local v1, offset:I
    iget v3, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMinTouchOffset:I

    #@1b
    if-ge v1, v3, :cond_1f

    #@1d
    iput v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMinTouchOffset:I

    #@1f
    .line 4762
    :cond_1f
    iget v3, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMaxTouchOffset:I

    #@21
    if-le v1, v3, :cond_25

    #@23
    iput v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMaxTouchOffset:I

    #@25
    .line 4759
    :cond_25
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_5

    #@28
    .line 4764
    .end local v1           #offset:I
    :cond_28
    return-void
.end method


# virtual methods
.method public crossHandles(Z)V
    .registers 3
    .parameter "crossing"

    #@0
    .prologue
    .line 4617
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/Editor$SelectionStartHandleView;->crossHandle(Z)V

    #@9
    .line 4618
    :cond_9
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@b
    if-eqz v0, :cond_12

    #@d
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@f
    invoke-virtual {v0, p1}, Landroid/widget/Editor$SelectionEndHandleView;->crossHandle(Z)V

    #@12
    .line 4619
    :cond_12
    return-void
.end method

.method public getEndHandle()Landroid/widget/Editor$SelectionEndHandleView;
    .registers 2

    #@0
    .prologue
    .line 4673
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@2
    return-object v0
.end method

.method public getMaxTouchOffset()I
    .registers 2

    #@0
    .prologue
    .line 4771
    iget v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMaxTouchOffset:I

    #@2
    return v0
.end method

.method public getMinTouchOffset()I
    .registers 2

    #@0
    .prologue
    .line 4767
    iget v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMinTouchOffset:I

    #@2
    return v0
.end method

.method public getStartHandle()Landroid/widget/Editor$SelectionStartHandleView;
    .registers 2

    #@0
    .prologue
    .line 4665
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 4666
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@6
    .line 4668
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    .line 4677
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionStartHandleView;->hide()V

    #@9
    .line 4678
    :cond_9
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@b
    if-eqz v0, :cond_12

    #@d
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@f
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionEndHandleView;->hide()V

    #@12
    .line 4679
    :cond_12
    return-void
.end method

.method public isSelectionStartDragged()Z
    .registers 2

    #@0
    .prologue
    .line 4782
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionStartHandleView;->isDragging()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onDetached()V
    .registers 4

    #@0
    .prologue
    .line 4793
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@9
    move-result-object v0

    #@a
    .line 4794
    .local v0, observer:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@d
    .line 4796
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@f
    if-eqz v1, :cond_2e

    #@11
    .line 4797
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@13
    if-eqz v1, :cond_1a

    #@15
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@17
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionStartHandleView;->hide()V

    #@1a
    .line 4798
    :cond_1a
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@1c
    if-eqz v1, :cond_23

    #@1e
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@20
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionEndHandleView;->hide()V

    #@23
    .line 4799
    :cond_23
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@25
    iget-boolean v1, v1, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@27
    if-eqz v1, :cond_2e

    #@29
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@2b
    const/4 v2, 0x0

    #@2c
    iput-boolean v2, v1, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@2e
    .line 4802
    :cond_2e
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@30
    if-eqz v1, :cond_37

    #@32
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mStartHandle:Landroid/widget/Editor$SelectionStartHandleView;

    #@34
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionStartHandleView;->onDetached()V

    #@37
    .line 4803
    :cond_37
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@39
    if-eqz v1, :cond_40

    #@3b
    iget-object v1, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mEndHandle:Landroid/widget/Editor$SelectionEndHandleView;

    #@3d
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionEndHandleView;->onDetached()V

    #@40
    .line 4804
    :cond_40
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .registers 17
    .parameter "event"

    #@0
    .prologue
    .line 4684
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v11

    #@4
    packed-switch v11, :pswitch_data_ee

    #@7
    .line 4752
    :cond_7
    :goto_7
    :pswitch_7
    return-void

    #@8
    .line 4686
    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@b
    move-result v9

    #@c
    .line 4687
    .local v9, x:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@f
    move-result v10

    #@10
    .line 4690
    .local v10, y:F
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@12
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@15
    move-result-object v11

    #@16
    invoke-virtual {v11, v9, v10}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@19
    move-result v11

    #@1a
    iput v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMaxTouchOffset:I

    #@1c
    iput v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMinTouchOffset:I

    #@1e
    .line 4693
    iget-boolean v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mGestureStayedInTapRegion:Z

    #@20
    if-eqz v11, :cond_88

    #@22
    .line 4694
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@25
    move-result-wide v11

    #@26
    iget-wide v13, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mPreviousTapUpTime:J

    #@28
    sub-long v5, v11, v13

    #@2a
    .line 4695
    .local v5, duration:J
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@2d
    move-result v11

    #@2e
    int-to-long v11, v11

    #@2f
    cmp-long v11, v5, v11

    #@31
    if-gtz v11, :cond_88

    #@33
    .line 4696
    iget v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionX:F

    #@35
    sub-float v0, v9, v11

    #@37
    .line 4697
    .local v0, deltaX:F
    iget v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionY:F

    #@39
    sub-float v1, v10, v11

    #@3b
    .line 4698
    .local v1, deltaY:F
    mul-float v11, v0, v0

    #@3d
    mul-float v12, v1, v1

    #@3f
    add-float v2, v11, v12

    #@41
    .line 4700
    .local v2, distanceSquared:F
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@43
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@46
    move-result-object v11

    #@47
    invoke-virtual {v11}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v11

    #@4b
    invoke-static {v11}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@4e
    move-result-object v8

    #@4f
    .line 4702
    .local v8, viewConfiguration:Landroid/view/ViewConfiguration;
    invoke-virtual {v8}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@52
    move-result v3

    #@53
    .line 4703
    .local v3, doubleTapSlop:I
    mul-int v11, v3, v3

    #@55
    int-to-float v11, v11

    #@56
    cmpg-float v11, v2, v11

    #@58
    if-gez v11, :cond_91

    #@5a
    const/4 v7, 0x1

    #@5b
    .line 4705
    .local v7, stayedInArea:Z
    :goto_5b
    if-eqz v7, :cond_88

    #@5d
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@5f
    invoke-static {v11, v9, v10}, Landroid/widget/Editor;->access$3800(Landroid/widget/Editor;FF)Z

    #@62
    move-result v11

    #@63
    if-eqz v11, :cond_88

    #@65
    .line 4706
    sget-boolean v11, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@67
    if-eqz v11, :cond_7e

    #@69
    .line 4707
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@6b
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6e
    move-result-object v11

    #@6f
    iget-boolean v11, v11, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@71
    if-eqz v11, :cond_7

    #@73
    .line 4708
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@75
    const/4 v12, 0x1

    #@76
    invoke-virtual {v11, v12}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@79
    .line 4709
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@7b
    invoke-static {v11}, Landroid/widget/Editor;->access$3900(Landroid/widget/Editor;)Z

    #@7e
    .line 4711
    :cond_7e
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@80
    invoke-virtual {v11}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@83
    .line 4712
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@85
    const/4 v12, 0x1

    #@86
    iput-boolean v12, v11, Landroid/widget/Editor;->mDiscardNextActionUp:Z

    #@88
    .line 4717
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v3           #doubleTapSlop:I
    .end local v5           #duration:J
    .end local v7           #stayedInArea:Z
    .end local v8           #viewConfiguration:Landroid/view/ViewConfiguration;
    :cond_88
    iput v9, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionX:F

    #@8a
    .line 4718
    iput v10, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionY:F

    #@8c
    .line 4719
    const/4 v11, 0x1

    #@8d
    iput-boolean v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mGestureStayedInTapRegion:Z

    #@8f
    goto/16 :goto_7

    #@91
    .line 4703
    .restart local v0       #deltaX:F
    .restart local v1       #deltaY:F
    .restart local v2       #distanceSquared:F
    .restart local v3       #doubleTapSlop:I
    .restart local v5       #duration:J
    .restart local v8       #viewConfiguration:Landroid/view/ViewConfiguration;
    :cond_91
    const/4 v7, 0x0

    #@92
    goto :goto_5b

    #@93
    .line 4726
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v3           #doubleTapSlop:I
    .end local v5           #duration:J
    .end local v8           #viewConfiguration:Landroid/view/ViewConfiguration;
    .end local v9           #x:F
    .end local v10           #y:F
    :pswitch_93
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@95
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@98
    move-result-object v11

    #@99
    invoke-virtual {v11}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@9c
    move-result-object v11

    #@9d
    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a0
    move-result-object v11

    #@a1
    const-string v12, "android.hardware.touchscreen.multitouch.distinct"

    #@a3
    invoke-virtual {v11, v12}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@a6
    move-result v11

    #@a7
    if-eqz v11, :cond_7

    #@a9
    .line 4728
    invoke-direct/range {p0 .. p1}, Landroid/widget/Editor$SelectionModifierCursorController;->updateMinAndMaxOffsets(Landroid/view/MotionEvent;)V

    #@ac
    goto/16 :goto_7

    #@ae
    .line 4733
    :pswitch_ae
    iget-boolean v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mGestureStayedInTapRegion:Z

    #@b0
    if-eqz v11, :cond_7

    #@b2
    .line 4734
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@b5
    move-result v11

    #@b6
    iget v12, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionX:F

    #@b8
    sub-float v0, v11, v12

    #@ba
    .line 4735
    .restart local v0       #deltaX:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@bd
    move-result v11

    #@be
    iget v12, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mDownPositionY:F

    #@c0
    sub-float v1, v11, v12

    #@c2
    .line 4736
    .restart local v1       #deltaY:F
    mul-float v11, v0, v0

    #@c4
    mul-float v12, v1, v1

    #@c6
    add-float v2, v11, v12

    #@c8
    .line 4738
    .restart local v2       #distanceSquared:F
    iget-object v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@ca
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@cd
    move-result-object v11

    #@ce
    invoke-virtual {v11}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@d1
    move-result-object v11

    #@d2
    invoke-static {v11}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@d5
    move-result-object v8

    #@d6
    .line 4740
    .restart local v8       #viewConfiguration:Landroid/view/ViewConfiguration;
    invoke-virtual {v8}, Landroid/view/ViewConfiguration;->getScaledDoubleTapTouchSlop()I

    #@d9
    move-result v4

    #@da
    .line 4742
    .local v4, doubleTapTouchSlop:I
    mul-int v11, v4, v4

    #@dc
    int-to-float v11, v11

    #@dd
    cmpl-float v11, v2, v11

    #@df
    if-lez v11, :cond_7

    #@e1
    .line 4743
    const/4 v11, 0x0

    #@e2
    iput-boolean v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mGestureStayedInTapRegion:Z

    #@e4
    goto/16 :goto_7

    #@e6
    .line 4749
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v4           #doubleTapTouchSlop:I
    .end local v8           #viewConfiguration:Landroid/view/ViewConfiguration;
    :pswitch_e6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e9
    move-result-wide v11

    #@ea
    iput-wide v11, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mPreviousTapUpTime:J

    #@ec
    goto/16 :goto_7

    #@ee
    .line 4684
    :pswitch_data_ee
    .packed-switch 0x0
        :pswitch_8
        :pswitch_e6
        :pswitch_ae
        :pswitch_7
        :pswitch_7
        :pswitch_93
        :pswitch_93
    .end packed-switch
.end method

.method public onTouchModeChanged(Z)V
    .registers 2
    .parameter "isInTouchMode"

    #@0
    .prologue
    .line 4786
    if-nez p1, :cond_5

    #@2
    .line 4787
    invoke-virtual {p0}, Landroid/widget/Editor$SelectionModifierCursorController;->hide()V

    #@5
    .line 4789
    :cond_5
    return-void
.end method

.method public resetTouchOffsets()V
    .registers 2

    #@0
    .prologue
    .line 4775
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMaxTouchOffset:I

    #@3
    iput v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->mMinTouchOffset:I

    #@5
    .line 4776
    return-void
.end method

.method public show()V
    .registers 2

    #@0
    .prologue
    .line 4622
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->isInBatchEditMode()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_21

    #@c
    .line 4623
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@e
    if-eqz v0, :cond_20

    #@10
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@12
    iget-boolean v0, v0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@14
    if-nez v0, :cond_21

    #@16
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@18
    iget-object v0, v0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@1a
    invoke-virtual {v0}, Landroid/widget/BubblePopupHelper;->isShowingBubblePopup()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_21

    #@20
    .line 4630
    :cond_20
    :goto_20
    return-void

    #@21
    .line 4627
    :cond_21
    invoke-direct {p0}, Landroid/widget/Editor$SelectionModifierCursorController;->initDrawables()V

    #@24
    .line 4628
    invoke-direct {p0}, Landroid/widget/Editor$SelectionModifierCursorController;->initHandles()V

    #@27
    .line 4629
    iget-object v0, p0, Landroid/widget/Editor$SelectionModifierCursorController;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v0}, Landroid/widget/Editor;->access$3500(Landroid/widget/Editor;)V

    #@2c
    goto :goto_20
.end method
