.class public Landroid/widget/VideoView;
.super Landroid/view/SurfaceView;
.source "VideoView.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;


# static fields
.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x4

.field private static final STATE_PLAYBACK_COMPLETED:I = 0x5

.field private static final STATE_PLAYING:I = 0x3

.field private static final STATE_PREPARED:I = 0x2

.field private static final STATE_PREPARING:I = 0x1


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCanPause:Z

.field private mCanSeekBack:Z

.field private mCanSeekForward:Z

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mCurrentBufferPercentage:I

.field private mCurrentState:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaController:Landroid/widget/MediaController;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSeekWhenPrepared:I

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSurfaceHeight:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceWidth:I

.field private mTargetState:I

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 94
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    #@5
    .line 53
    const-string v0, "VideoView"

    #@7
    iput-object v0, p0, Landroid/widget/VideoView;->TAG:Ljava/lang/String;

    #@9
    .line 72
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@b
    .line 73
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@d
    .line 76
    iput-object v2, p0, Landroid/widget/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@f
    .line 77
    iput-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@11
    .line 278
    new-instance v0, Landroid/widget/VideoView$1;

    #@13
    invoke-direct {v0, p0}, Landroid/widget/VideoView$1;-><init>(Landroid/widget/VideoView;)V

    #@16
    iput-object v0, p0, Landroid/widget/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@18
    .line 290
    new-instance v0, Landroid/widget/VideoView$2;

    #@1a
    invoke-direct {v0, p0}, Landroid/widget/VideoView$2;-><init>(Landroid/widget/VideoView;)V

    #@1d
    iput-object v0, p0, Landroid/widget/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@1f
    .line 352
    new-instance v0, Landroid/widget/VideoView$3;

    #@21
    invoke-direct {v0, p0}, Landroid/widget/VideoView$3;-><init>(Landroid/widget/VideoView;)V

    #@24
    iput-object v0, p0, Landroid/widget/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@26
    .line 366
    new-instance v0, Landroid/widget/VideoView$4;

    #@28
    invoke-direct {v0, p0}, Landroid/widget/VideoView$4;-><init>(Landroid/widget/VideoView;)V

    #@2b
    iput-object v0, p0, Landroid/widget/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2d
    .line 418
    new-instance v0, Landroid/widget/VideoView$5;

    #@2f
    invoke-direct {v0, p0}, Landroid/widget/VideoView$5;-><init>(Landroid/widget/VideoView;)V

    #@32
    iput-object v0, p0, Landroid/widget/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@34
    .line 470
    new-instance v0, Landroid/widget/VideoView$6;

    #@36
    invoke-direct {v0, p0}, Landroid/widget/VideoView$6;-><init>(Landroid/widget/VideoView;)V

    #@39
    iput-object v0, p0, Landroid/widget/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    #@3b
    .line 95
    invoke-direct {p0}, Landroid/widget/VideoView;->initVideoView()V

    #@3e
    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 99
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 100
    invoke-direct {p0}, Landroid/widget/VideoView;->initVideoView()V

    #@7
    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 53
    const-string v0, "VideoView"

    #@7
    iput-object v0, p0, Landroid/widget/VideoView;->TAG:Ljava/lang/String;

    #@9
    .line 72
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@b
    .line 73
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@d
    .line 76
    iput-object v2, p0, Landroid/widget/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@f
    .line 77
    iput-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@11
    .line 278
    new-instance v0, Landroid/widget/VideoView$1;

    #@13
    invoke-direct {v0, p0}, Landroid/widget/VideoView$1;-><init>(Landroid/widget/VideoView;)V

    #@16
    iput-object v0, p0, Landroid/widget/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@18
    .line 290
    new-instance v0, Landroid/widget/VideoView$2;

    #@1a
    invoke-direct {v0, p0}, Landroid/widget/VideoView$2;-><init>(Landroid/widget/VideoView;)V

    #@1d
    iput-object v0, p0, Landroid/widget/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@1f
    .line 352
    new-instance v0, Landroid/widget/VideoView$3;

    #@21
    invoke-direct {v0, p0}, Landroid/widget/VideoView$3;-><init>(Landroid/widget/VideoView;)V

    #@24
    iput-object v0, p0, Landroid/widget/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@26
    .line 366
    new-instance v0, Landroid/widget/VideoView$4;

    #@28
    invoke-direct {v0, p0}, Landroid/widget/VideoView$4;-><init>(Landroid/widget/VideoView;)V

    #@2b
    iput-object v0, p0, Landroid/widget/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2d
    .line 418
    new-instance v0, Landroid/widget/VideoView$5;

    #@2f
    invoke-direct {v0, p0}, Landroid/widget/VideoView$5;-><init>(Landroid/widget/VideoView;)V

    #@32
    iput-object v0, p0, Landroid/widget/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@34
    .line 470
    new-instance v0, Landroid/widget/VideoView$6;

    #@36
    invoke-direct {v0, p0}, Landroid/widget/VideoView$6;-><init>(Landroid/widget/VideoView;)V

    #@39
    iput-object v0, p0, Landroid/widget/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    #@3b
    .line 105
    invoke-direct {p0}, Landroid/widget/VideoView;->initVideoView()V

    #@3e
    .line 106
    return-void
.end method

.method static synthetic access$000(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@2
    return v0
.end method

.method static synthetic access$1000(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mSurfaceWidth:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mSurfaceWidth:I

    #@2
    return p1
.end method

.method static synthetic access$102(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@2
    return p1
.end method

.method static synthetic access$1100(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mSurfaceHeight:I

    #@2
    return v0
.end method

.method static synthetic access$1102(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mSurfaceHeight:I

    #@2
    return p1
.end method

.method static synthetic access$1200(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mTargetState:I

    #@2
    return v0
.end method

.method static synthetic access$1202(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@2
    return p1
.end method

.method static synthetic access$1300(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Landroid/widget/VideoView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/widget/VideoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/widget/VideoView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1802(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mCurrentBufferPercentage:I

    #@2
    return p1
.end method

.method static synthetic access$1902(Landroid/widget/VideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-object p1, p0, Landroid/widget/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    return-object p1
.end method

.method static synthetic access$2000(Landroid/widget/VideoView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/VideoView;->openVideo()V

    #@3
    return-void
.end method

.method static synthetic access$202(Landroid/widget/VideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput p1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@2
    return p1
.end method

.method static synthetic access$2100(Landroid/widget/VideoView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/VideoView;->release(Z)V

    #@3
    return-void
.end method

.method static synthetic access$302(Landroid/widget/VideoView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-boolean p1, p0, Landroid/widget/VideoView;->mCanPause:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Landroid/widget/VideoView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-boolean p1, p0, Landroid/widget/VideoView;->mCanSeekBack:Z

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/widget/VideoView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-boolean p1, p0, Landroid/widget/VideoView;->mCanSeekForward:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/VideoView;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/VideoView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/widget/VideoView;->mSeekWhenPrepared:I

    #@2
    return v0
.end method

.method private attachMediaController()V
    .registers 4

    #@0
    .prologue
    .line 269
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v1, :cond_2a

    #@4
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@6
    if-eqz v1, :cond_2a

    #@8
    .line 270
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@a
    invoke-virtual {v1, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    #@d
    .line 271
    invoke-virtual {p0}, Landroid/widget/VideoView;->getParent()Landroid/view/ViewParent;

    #@10
    move-result-object v1

    #@11
    instance-of v1, v1, Landroid/view/View;

    #@13
    if-eqz v1, :cond_2b

    #@15
    invoke-virtual {p0}, Landroid/widget/VideoView;->getParent()Landroid/view/ViewParent;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/view/View;

    #@1b
    move-object v0, v1

    #@1c
    .line 273
    .local v0, anchorView:Landroid/view/View;
    :goto_1c
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@1e
    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    #@21
    .line 274
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@23
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Landroid/widget/MediaController;->setEnabled(Z)V

    #@2a
    .line 276
    .end local v0           #anchorView:Landroid/view/View;
    :cond_2a
    return-void

    #@2b
    :cond_2b
    move-object v0, p0

    #@2c
    .line 271
    goto :goto_1c
.end method

.method private initVideoView()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 172
    iput v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@4
    .line 173
    iput v2, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@6
    .line 174
    invoke-virtual {p0}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/widget/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    #@c
    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    #@f
    .line 175
    invoke-virtual {p0}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    #@12
    move-result-object v0

    #@13
    const/4 v1, 0x3

    #@14
    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    #@17
    .line 176
    invoke-virtual {p0, v3}, Landroid/widget/VideoView;->setFocusable(Z)V

    #@1a
    .line 177
    invoke-virtual {p0, v3}, Landroid/widget/VideoView;->setFocusableInTouchMode(Z)V

    #@1d
    .line 178
    invoke-virtual {p0}, Landroid/widget/VideoView;->requestFocus()Z

    #@20
    .line 179
    iput v2, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@22
    .line 180
    iput v2, p0, Landroid/widget/VideoView;->mTargetState:I

    #@24
    .line 181
    return-void
.end method

.method private isInPlaybackState()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 645
    iget-object v1, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3
    if-eqz v1, :cond_13

    #@5
    iget v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@7
    const/4 v2, -0x1

    #@8
    if-eq v1, v2, :cond_13

    #@a
    iget v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@c
    if-eqz v1, :cond_13

    #@e
    iget v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@10
    if-eq v1, v0, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method private openVideo()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, -0x1

    #@3
    .line 214
    iget-object v2, p0, Landroid/widget/VideoView;->mUri:Landroid/net/Uri;

    #@5
    if-eqz v2, :cond_b

    #@7
    iget-object v2, p0, Landroid/widget/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@9
    if-nez v2, :cond_c

    #@b
    .line 258
    :cond_b
    :goto_b
    return-void

    #@c
    .line 220
    :cond_c
    new-instance v1, Landroid/content/Intent;

    #@e
    const-string v2, "com.android.music.musicservicecommand"

    #@10
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    .line 221
    .local v1, i:Landroid/content/Intent;
    const-string v2, "command"

    #@15
    const-string/jumbo v3, "pause"

    #@18
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b
    .line 222
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@20
    .line 226
    invoke-direct {p0, v7}, Landroid/widget/VideoView;->release(Z)V

    #@23
    .line 228
    :try_start_23
    new-instance v2, Landroid/media/MediaPlayer;

    #@25
    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    #@28
    iput-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2a
    .line 229
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2c
    iget-object v3, p0, Landroid/widget/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2e
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    #@31
    .line 230
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@33
    iget-object v3, p0, Landroid/widget/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@35
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    #@38
    .line 231
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3a
    iget-object v3, p0, Landroid/widget/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@3c
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@3f
    .line 232
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@41
    iget-object v3, p0, Landroid/widget/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@43
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@46
    .line 233
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@48
    iget-object v3, p0, Landroid/widget/VideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    #@4a
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    #@4d
    .line 234
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@4f
    iget-object v3, p0, Landroid/widget/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@51
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    #@54
    .line 235
    const/4 v2, 0x0

    #@55
    iput v2, p0, Landroid/widget/VideoView;->mCurrentBufferPercentage:I

    #@57
    .line 236
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@59
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@5b
    iget-object v4, p0, Landroid/widget/VideoView;->mUri:Landroid/net/Uri;

    #@5d
    iget-object v5, p0, Landroid/widget/VideoView;->mHeaders:Ljava/util/Map;

    #@5f
    invoke-virtual {v2, v3, v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    #@62
    .line 237
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@64
    iget-object v3, p0, Landroid/widget/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@66
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    #@69
    .line 238
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@6b
    const/4 v3, 0x3

    #@6c
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@6f
    .line 239
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@71
    const/4 v3, 0x1

    #@72
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    #@75
    .line 240
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@77
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V

    #@7a
    .line 243
    const/4 v2, 0x1

    #@7b
    iput v2, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@7d
    .line 244
    invoke-direct {p0}, Landroid/widget/VideoView;->attachMediaController()V
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_80} :catch_81
    .catch Ljava/lang/IllegalArgumentException; {:try_start_23 .. :try_end_80} :catch_a9

    #@80
    goto :goto_b

    #@81
    .line 245
    :catch_81
    move-exception v0

    #@82
    .line 246
    .local v0, ex:Ljava/io/IOException;
    iget-object v2, p0, Landroid/widget/VideoView;->TAG:Ljava/lang/String;

    #@84
    new-instance v3, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v4, "Unable to open content: "

    #@8b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    iget-object v4, p0, Landroid/widget/VideoView;->mUri:Landroid/net/Uri;

    #@91
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v3

    #@99
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9c
    .line 247
    iput v6, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@9e
    .line 248
    iput v6, p0, Landroid/widget/VideoView;->mTargetState:I

    #@a0
    .line 249
    iget-object v2, p0, Landroid/widget/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@a2
    iget-object v3, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@a4
    invoke-interface {v2, v3, v8, v7}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    #@a7
    goto/16 :goto_b

    #@a9
    .line 251
    .end local v0           #ex:Ljava/io/IOException;
    :catch_a9
    move-exception v0

    #@aa
    .line 252
    .local v0, ex:Ljava/lang/IllegalArgumentException;
    iget-object v2, p0, Landroid/widget/VideoView;->TAG:Ljava/lang/String;

    #@ac
    new-instance v3, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v4, "Unable to open content: "

    #@b3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v3

    #@b7
    iget-object v4, p0, Landroid/widget/VideoView;->mUri:Landroid/net/Uri;

    #@b9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v3

    #@bd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v3

    #@c1
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c4
    .line 253
    iput v6, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@c6
    .line 254
    iput v6, p0, Landroid/widget/VideoView;->mTargetState:I

    #@c8
    .line 255
    iget-object v2, p0, Landroid/widget/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@ca
    iget-object v3, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@cc
    invoke-interface {v2, v3, v8, v7}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    #@cf
    goto/16 :goto_b
.end method

.method private release(Z)V
    .registers 4
    .parameter "cleartargetstate"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 506
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3
    if-eqz v0, :cond_18

    #@5
    .line 507
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@a
    .line 508
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@c
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@f
    .line 509
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@12
    .line 510
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@14
    .line 511
    if-eqz p1, :cond_18

    #@16
    .line 512
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@18
    .line 515
    :cond_18
    return-void
.end method

.method private toggleMediaControlsVisiblity()V
    .registers 2

    #@0
    .prologue
    .line 576
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@2
    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 577
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@a
    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    #@d
    .line 581
    :goto_d
    return-void

    #@e
    .line 579
    :cond_e
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@10
    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    #@13
    goto :goto_d
.end method


# virtual methods
.method public canPause()Z
    .registers 2

    #@0
    .prologue
    .line 652
    iget-boolean v0, p0, Landroid/widget/VideoView;->mCanPause:Z

    #@2
    return v0
.end method

.method public canSeekBackward()Z
    .registers 2

    #@0
    .prologue
    .line 656
    iget-boolean v0, p0, Landroid/widget/VideoView;->mCanSeekBack:Z

    #@2
    return v0
.end method

.method public canSeekForward()Z
    .registers 2

    #@0
    .prologue
    .line 660
    iget-boolean v0, p0, Landroid/widget/VideoView;->mCanSeekForward:Z

    #@2
    return v0
.end method

.method public getBufferPercentage()I
    .registers 2

    #@0
    .prologue
    .line 638
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 639
    iget v0, p0, Landroid/widget/VideoView;->mCurrentBufferPercentage:I

    #@6
    .line 641
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getCurrentPosition()I
    .registers 2

    #@0
    .prologue
    .line 618
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 619
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@b
    move-result v0

    #@c
    .line 621
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getDuration()I
    .registers 2

    #@0
    .prologue
    .line 610
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 611
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    #@b
    move-result v0

    #@c
    .line 614
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, -0x1

    #@e
    goto :goto_c
.end method

.method public isPlaying()Z
    .registers 2

    #@0
    .prologue
    .line 634
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 133
    const-class v0, Landroid/widget/VideoView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 134
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 139
    const-class v0, Landroid/widget/VideoView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 140
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 536
    const/4 v2, 0x4

    #@2
    if-eq p1, v2, :cond_40

    #@4
    const/16 v2, 0x18

    #@6
    if-eq p1, v2, :cond_40

    #@8
    const/16 v2, 0x19

    #@a
    if-eq p1, v2, :cond_40

    #@c
    const/16 v2, 0xa4

    #@e
    if-eq p1, v2, :cond_40

    #@10
    const/16 v2, 0x52

    #@12
    if-eq p1, v2, :cond_40

    #@14
    const/4 v2, 0x5

    #@15
    if-eq p1, v2, :cond_40

    #@17
    const/4 v2, 0x6

    #@18
    if-eq p1, v2, :cond_40

    #@1a
    move v0, v1

    #@1b
    .line 543
    .local v0, isKeyCodeSupported:Z
    :goto_1b
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_7c

    #@21
    if-eqz v0, :cond_7c

    #@23
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@25
    if-eqz v2, :cond_7c

    #@27
    .line 544
    const/16 v2, 0x4f

    #@29
    if-eq p1, v2, :cond_2f

    #@2b
    const/16 v2, 0x55

    #@2d
    if-ne p1, v2, :cond_4b

    #@2f
    .line 546
    :cond_2f
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@31
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_42

    #@37
    .line 547
    invoke-virtual {p0}, Landroid/widget/VideoView;->pause()V

    #@3a
    .line 548
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@3c
    invoke-virtual {v2}, Landroid/widget/MediaController;->show()V

    #@3f
    .line 572
    :cond_3f
    :goto_3f
    return v1

    #@40
    .line 536
    .end local v0           #isKeyCodeSupported:Z
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_1b

    #@42
    .line 550
    .restart local v0       #isKeyCodeSupported:Z
    :cond_42
    invoke-virtual {p0}, Landroid/widget/VideoView;->start()V

    #@45
    .line 551
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@47
    invoke-virtual {v2}, Landroid/widget/MediaController;->hide()V

    #@4a
    goto :goto_3f

    #@4b
    .line 554
    :cond_4b
    const/16 v2, 0x7e

    #@4d
    if-ne p1, v2, :cond_60

    #@4f
    .line 555
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@51
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@54
    move-result v2

    #@55
    if-nez v2, :cond_3f

    #@57
    .line 556
    invoke-virtual {p0}, Landroid/widget/VideoView;->start()V

    #@5a
    .line 557
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@5c
    invoke-virtual {v2}, Landroid/widget/MediaController;->hide()V

    #@5f
    goto :goto_3f

    #@60
    .line 560
    :cond_60
    const/16 v2, 0x56

    #@62
    if-eq p1, v2, :cond_68

    #@64
    const/16 v2, 0x7f

    #@66
    if-ne p1, v2, :cond_79

    #@68
    .line 562
    :cond_68
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@6a
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@6d
    move-result v2

    #@6e
    if-eqz v2, :cond_3f

    #@70
    .line 563
    invoke-virtual {p0}, Landroid/widget/VideoView;->pause()V

    #@73
    .line 564
    iget-object v2, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@75
    invoke-virtual {v2}, Landroid/widget/MediaController;->show()V

    #@78
    goto :goto_3f

    #@79
    .line 568
    :cond_79
    invoke-direct {p0}, Landroid/widget/VideoView;->toggleMediaControlsVisiblity()V

    #@7c
    .line 572
    :cond_7c
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@7f
    move-result v1

    #@80
    goto :goto_3f
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 111
    iget v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@2
    invoke-static {v2, p1}, Landroid/widget/VideoView;->getDefaultSize(II)I

    #@5
    move-result v1

    #@6
    .line 112
    .local v1, width:I
    iget v2, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@8
    invoke-static {v2, p2}, Landroid/widget/VideoView;->getDefaultSize(II)I

    #@b
    move-result v0

    #@c
    .line 113
    .local v0, height:I
    iget v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@e
    if-lez v2, :cond_23

    #@10
    iget v2, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@12
    if-lez v2, :cond_23

    #@14
    .line 114
    iget v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@16
    mul-int/2addr v2, v0

    #@17
    iget v3, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@19
    mul-int/2addr v3, v1

    #@1a
    if-le v2, v3, :cond_27

    #@1c
    .line 116
    iget v2, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@1e
    mul-int/2addr v2, v1

    #@1f
    iget v3, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@21
    div-int v0, v2, v3

    #@23
    .line 127
    :cond_23
    :goto_23
    invoke-virtual {p0, v1, v0}, Landroid/widget/VideoView;->setMeasuredDimension(II)V

    #@26
    .line 128
    return-void

    #@27
    .line 117
    :cond_27
    iget v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@29
    mul-int/2addr v2, v0

    #@2a
    iget v3, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@2c
    mul-int/2addr v3, v1

    #@2d
    if-ge v2, v3, :cond_23

    #@2f
    .line 119
    iget v2, p0, Landroid/widget/VideoView;->mVideoWidth:I

    #@31
    mul-int/2addr v2, v0

    #@32
    iget v3, p0, Landroid/widget/VideoView;->mVideoHeight:I

    #@34
    div-int v1, v2, v3

    #@36
    goto :goto_23
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 519
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 520
    invoke-direct {p0}, Landroid/widget/VideoView;->toggleMediaControlsVisiblity()V

    #@d
    .line 522
    :cond_d
    const/4 v0, 0x0

    #@e
    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 527
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 528
    invoke-direct {p0}, Landroid/widget/VideoView;->toggleMediaControlsVisiblity()V

    #@d
    .line 530
    :cond_d
    const/4 v0, 0x0

    #@e
    return v0
.end method

.method public pause()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 592
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_16

    #@7
    .line 593
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@9
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 594
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@11
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    #@14
    .line 595
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@16
    .line 598
    :cond_16
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@18
    .line 599
    return-void
.end method

.method public resolveAdjustedSize(II)I
    .registers 6
    .parameter "desiredSize"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 143
    move v0, p1

    #@1
    .line 144
    .local v0, result:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v1

    #@5
    .line 145
    .local v1, specMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 147
    .local v2, specSize:I
    sparse-switch v1, :sswitch_data_16

    #@c
    .line 168
    :goto_c
    return v0

    #@d
    .line 152
    :sswitch_d
    move v0, p1

    #@e
    .line 153
    goto :goto_c

    #@f
    .line 160
    :sswitch_f
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v0

    #@13
    .line 161
    goto :goto_c

    #@14
    .line 165
    :sswitch_14
    move v0, v2

    #@15
    goto :goto_c

    #@16
    .line 147
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_f
        0x0 -> :sswitch_d
        0x40000000 -> :sswitch_14
    .end sparse-switch
.end method

.method public resume()V
    .registers 1

    #@0
    .prologue
    .line 606
    invoke-direct {p0}, Landroid/widget/VideoView;->openVideo()V

    #@3
    .line 607
    return-void
.end method

.method public seekTo(I)V
    .registers 3
    .parameter "msec"

    #@0
    .prologue
    .line 625
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 626
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    #@b
    .line 627
    const/4 v0, 0x0

    #@c
    iput v0, p0, Landroid/widget/VideoView;->mSeekWhenPrepared:I

    #@e
    .line 631
    :goto_e
    return-void

    #@f
    .line 629
    :cond_f
    iput p1, p0, Landroid/widget/VideoView;->mSeekWhenPrepared:I

    #@11
    goto :goto_e
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .registers 3
    .parameter "controller"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 262
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@6
    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    #@9
    .line 264
    :cond_9
    iput-object p1, p0, Landroid/widget/VideoView;->mMediaController:Landroid/widget/MediaController;

    #@b
    .line 265
    invoke-direct {p0}, Landroid/widget/VideoView;->attachMediaController()V

    #@e
    .line 266
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 444
    iput-object p1, p0, Landroid/widget/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@2
    .line 445
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 457
    iput-object p1, p0, Landroid/widget/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2
    .line 458
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 467
    iput-object p1, p0, Landroid/widget/VideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    #@2
    .line 468
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 433
    iput-object p1, p0, Landroid/widget/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2
    .line 434
    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 184
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    #@7
    .line 185
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .registers 3
    .parameter "uri"

    #@0
    .prologue
    .line 188
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V

    #@4
    .line 189
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V
    .registers 4
    .parameter "uri"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 195
    .local p2, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Landroid/widget/VideoView;->mUri:Landroid/net/Uri;

    #@2
    .line 196
    iput-object p2, p0, Landroid/widget/VideoView;->mHeaders:Ljava/util/Map;

    #@4
    .line 197
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/widget/VideoView;->mSeekWhenPrepared:I

    #@7
    .line 198
    invoke-direct {p0}, Landroid/widget/VideoView;->openVideo()V

    #@a
    .line 199
    invoke-virtual {p0}, Landroid/widget/VideoView;->requestLayout()V

    #@d
    .line 200
    invoke-virtual {p0}, Landroid/widget/VideoView;->invalidate()V

    #@10
    .line 201
    return-void
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 584
    invoke-direct {p0}, Landroid/widget/VideoView;->isInPlaybackState()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 585
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@9
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    #@c
    .line 586
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@e
    .line 588
    :cond_e
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@10
    .line 589
    return-void
.end method

.method public stopPlayback()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 204
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3
    if-eqz v0, :cond_16

    #@5
    .line 205
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    #@a
    .line 206
    iget-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@c
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@f
    .line 207
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/widget/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@12
    .line 208
    iput v1, p0, Landroid/widget/VideoView;->mCurrentState:I

    #@14
    .line 209
    iput v1, p0, Landroid/widget/VideoView;->mTargetState:I

    #@16
    .line 211
    :cond_16
    return-void
.end method

.method public suspend()V
    .registers 2

    #@0
    .prologue
    .line 602
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/widget/VideoView;->release(Z)V

    #@4
    .line 603
    return-void
.end method
