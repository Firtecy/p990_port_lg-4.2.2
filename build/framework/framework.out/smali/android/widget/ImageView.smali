.class public Landroid/widget/ImageView;
.super Landroid/view/View;
.source "ImageView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ImageView$ScaleType;
    }
.end annotation


# static fields
.field private static final sS2FArray:[Landroid/graphics/Matrix$ScaleToFit;

.field private static final sScaleTypeArray:[Landroid/widget/ImageView$ScaleType;


# instance fields
.field private mAdjustViewBounds:Z

.field private mAlpha:I

.field private mBaseline:I

.field private mBaselineAlignBottom:Z

.field private mColorFilter:Landroid/graphics/ColorFilter;

.field private mColorMod:Z

.field private mCropToPadding:Z

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mDrawableHeight:I

.field private mDrawableWidth:I

.field private mHaveFrame:Z

.field private mLevel:I

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMaxHeight:I

.field private mMaxWidth:I

.field private mMergeState:Z

.field private mResource:I

.field private mScaleType:Landroid/widget/ImageView$ScaleType;

.field private mState:[I

.field private mTempDst:Landroid/graphics/RectF;

.field private mTempSrc:Landroid/graphics/RectF;

.field private mUri:Landroid/net/Uri;

.field private mViewAlphaScale:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 93
    const/16 v0, 0x8

    #@7
    new-array v0, v0, [Landroid/widget/ImageView$ScaleType;

    #@9
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    #@b
    aput-object v1, v0, v3

    #@d
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    #@f
    aput-object v1, v0, v4

    #@11
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    #@13
    aput-object v1, v0, v5

    #@15
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    #@17
    aput-object v1, v0, v6

    #@19
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    #@1b
    aput-object v1, v0, v7

    #@1d
    const/4 v1, 0x5

    #@1e
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    #@2a
    aput-object v2, v0, v1

    #@2c
    sput-object v0, Landroid/widget/ImageView;->sScaleTypeArray:[Landroid/widget/ImageView$ScaleType;

    #@2e
    .line 708
    new-array v0, v7, [Landroid/graphics/Matrix$ScaleToFit;

    #@30
    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    #@32
    aput-object v1, v0, v3

    #@34
    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->START:Landroid/graphics/Matrix$ScaleToFit;

    #@36
    aput-object v1, v0, v4

    #@38
    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    #@3a
    aput-object v1, v0, v5

    #@3c
    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->END:Landroid/graphics/Matrix$ScaleToFit;

    #@3e
    aput-object v1, v0, v6

    #@40
    sput-object v0, Landroid/widget/ImageView;->sS2FArray:[Landroid/graphics/Matrix$ScaleToFit;

    #@42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x0

    #@5
    .line 105
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@8
    .line 62
    iput v1, p0, Landroid/widget/ImageView;->mResource:I

    #@a
    .line 65
    iput-boolean v1, p0, Landroid/widget/ImageView;->mHaveFrame:Z

    #@c
    .line 66
    iput-boolean v1, p0, Landroid/widget/ImageView;->mAdjustViewBounds:Z

    #@e
    .line 67
    iput v0, p0, Landroid/widget/ImageView;->mMaxWidth:I

    #@10
    .line 68
    iput v0, p0, Landroid/widget/ImageView;->mMaxHeight:I

    #@12
    .line 72
    const/16 v0, 0xff

    #@14
    iput v0, p0, Landroid/widget/ImageView;->mAlpha:I

    #@16
    .line 73
    const/16 v0, 0x100

    #@18
    iput v0, p0, Landroid/widget/ImageView;->mViewAlphaScale:I

    #@1a
    .line 74
    iput-boolean v1, p0, Landroid/widget/ImageView;->mColorMod:Z

    #@1c
    .line 76
    iput-object v2, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 77
    iput-object v2, p0, Landroid/widget/ImageView;->mState:[I

    #@20
    .line 78
    iput-boolean v1, p0, Landroid/widget/ImageView;->mMergeState:Z

    #@22
    .line 79
    iput v1, p0, Landroid/widget/ImageView;->mLevel:I

    #@24
    .line 82
    iput-object v2, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@26
    .line 85
    new-instance v0, Landroid/graphics/RectF;

    #@28
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@2b
    iput-object v0, p0, Landroid/widget/ImageView;->mTempSrc:Landroid/graphics/RectF;

    #@2d
    .line 86
    new-instance v0, Landroid/graphics/RectF;

    #@2f
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@32
    iput-object v0, p0, Landroid/widget/ImageView;->mTempDst:Landroid/graphics/RectF;

    #@34
    .line 90
    const/4 v0, -0x1

    #@35
    iput v0, p0, Landroid/widget/ImageView;->mBaseline:I

    #@37
    .line 91
    iput-boolean v1, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@39
    .line 106
    invoke-direct {p0}, Landroid/widget/ImageView;->initImageView()V

    #@3c
    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 110
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 15
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/16 v9, 0xff

    #@3
    const/4 v8, -0x1

    #@4
    const v7, 0x7fffffff

    #@7
    const/4 v6, 0x0

    #@8
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@b
    .line 62
    iput v6, p0, Landroid/widget/ImageView;->mResource:I

    #@d
    .line 65
    iput-boolean v6, p0, Landroid/widget/ImageView;->mHaveFrame:Z

    #@f
    .line 66
    iput-boolean v6, p0, Landroid/widget/ImageView;->mAdjustViewBounds:Z

    #@11
    .line 67
    iput v7, p0, Landroid/widget/ImageView;->mMaxWidth:I

    #@13
    .line 68
    iput v7, p0, Landroid/widget/ImageView;->mMaxHeight:I

    #@15
    .line 72
    iput v9, p0, Landroid/widget/ImageView;->mAlpha:I

    #@17
    .line 73
    const/16 v5, 0x100

    #@19
    iput v5, p0, Landroid/widget/ImageView;->mViewAlphaScale:I

    #@1b
    .line 74
    iput-boolean v6, p0, Landroid/widget/ImageView;->mColorMod:Z

    #@1d
    .line 76
    iput-object v10, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1f
    .line 77
    iput-object v10, p0, Landroid/widget/ImageView;->mState:[I

    #@21
    .line 78
    iput-boolean v6, p0, Landroid/widget/ImageView;->mMergeState:Z

    #@23
    .line 79
    iput v6, p0, Landroid/widget/ImageView;->mLevel:I

    #@25
    .line 82
    iput-object v10, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@27
    .line 85
    new-instance v5, Landroid/graphics/RectF;

    #@29
    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    #@2c
    iput-object v5, p0, Landroid/widget/ImageView;->mTempSrc:Landroid/graphics/RectF;

    #@2e
    .line 86
    new-instance v5, Landroid/graphics/RectF;

    #@30
    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    #@33
    iput-object v5, p0, Landroid/widget/ImageView;->mTempDst:Landroid/graphics/RectF;

    #@35
    .line 90
    iput v8, p0, Landroid/widget/ImageView;->mBaseline:I

    #@37
    .line 91
    iput-boolean v6, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@39
    .line 115
    invoke-direct {p0}, Landroid/widget/ImageView;->initImageView()V

    #@3c
    .line 117
    sget-object v5, Lcom/android/internal/R$styleable;->ImageView:[I

    #@3e
    invoke-virtual {p1, p2, v5, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@41
    move-result-object v0

    #@42
    .line 120
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@45
    move-result-object v2

    #@46
    .line 121
    .local v2, d:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_4b

    #@48
    .line 122
    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@4b
    .line 125
    :cond_4b
    const/4 v5, 0x6

    #@4c
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4f
    move-result v5

    #@50
    iput-boolean v5, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@52
    .line 128
    const/16 v5, 0x8

    #@54
    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@57
    move-result v5

    #@58
    iput v5, p0, Landroid/widget/ImageView;->mBaseline:I

    #@5a
    .line 131
    const/4 v5, 0x2

    #@5b
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@5e
    move-result v5

    #@5f
    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    #@62
    .line 135
    const/4 v5, 0x3

    #@63
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@66
    move-result v5

    #@67
    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setMaxWidth(I)V

    #@6a
    .line 138
    const/4 v5, 0x4

    #@6b
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@6e
    move-result v5

    #@6f
    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setMaxHeight(I)V

    #@72
    .line 141
    const/4 v5, 0x1

    #@73
    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@76
    move-result v3

    #@77
    .line 142
    .local v3, index:I
    if-ltz v3, :cond_80

    #@79
    .line 143
    sget-object v5, Landroid/widget/ImageView;->sScaleTypeArray:[Landroid/widget/ImageView$ScaleType;

    #@7b
    aget-object v5, v5, v3

    #@7d
    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@80
    .line 146
    :cond_80
    const/4 v5, 0x5

    #@81
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@84
    move-result v4

    #@85
    .line 147
    .local v4, tint:I
    if-eqz v4, :cond_8a

    #@87
    .line 148
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    #@8a
    .line 151
    :cond_8a
    const/16 v5, 0x9

    #@8c
    invoke-virtual {v0, v5, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8f
    move-result v1

    #@90
    .line 152
    .local v1, alpha:I
    if-eq v1, v9, :cond_95

    #@92
    .line 153
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    #@95
    .line 156
    :cond_95
    const/4 v5, 0x7

    #@96
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@99
    move-result v5

    #@9a
    iput-boolean v5, p0, Landroid/widget/ImageView;->mCropToPadding:Z

    #@9c
    .line 159
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@9f
    .line 162
    return-void
.end method

.method private applyColorMod()V
    .registers 4

    #@0
    .prologue
    .line 1151
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_23

    #@4
    iget-boolean v0, p0, Landroid/widget/ImageView;->mColorMod:Z

    #@6
    if-eqz v0, :cond_23

    #@8
    .line 1152
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@10
    .line 1153
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    iget-object v1, p0, Landroid/widget/ImageView;->mColorFilter:Landroid/graphics/ColorFilter;

    #@14
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@17
    .line 1154
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@19
    iget v1, p0, Landroid/widget/ImageView;->mAlpha:I

    #@1b
    iget v2, p0, Landroid/widget/ImageView;->mViewAlphaScale:I

    #@1d
    mul-int/2addr v1, v2

    #@1e
    shr-int/lit8 v1, v1, 0x8

    #@20
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@23
    .line 1156
    :cond_23
    return-void
.end method

.method private configureBounds()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v12, 0x0

    #@2
    const/4 v8, 0x0

    #@3
    const/high16 v11, 0x3f00

    #@5
    .line 861
    iget-object v9, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@7
    if-eqz v9, :cond_d

    #@9
    iget-boolean v9, p0, Landroid/widget/ImageView;->mHaveFrame:Z

    #@b
    if-nez v9, :cond_e

    #@d
    .line 943
    :cond_d
    :goto_d
    return-void

    #@e
    .line 865
    :cond_e
    iget v1, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@10
    .line 866
    .local v1, dwidth:I
    iget v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@12
    .line 868
    .local v0, dheight:I
    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    #@15
    move-result v9

    #@16
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    #@18
    sub-int/2addr v9, v10

    #@19
    iget v10, p0, Landroid/view/View;->mPaddingRight:I

    #@1b
    sub-int v7, v9, v10

    #@1d
    .line 869
    .local v7, vwidth:I
    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    #@20
    move-result v9

    #@21
    iget v10, p0, Landroid/view/View;->mPaddingTop:I

    #@23
    sub-int/2addr v9, v10

    #@24
    iget v10, p0, Landroid/view/View;->mPaddingBottom:I

    #@26
    sub-int v6, v9, v10

    #@28
    .line 871
    .local v6, vheight:I
    if-ltz v1, :cond_2c

    #@2a
    if-ne v7, v1, :cond_43

    #@2c
    :cond_2c
    if-ltz v0, :cond_30

    #@2e
    if-ne v6, v0, :cond_43

    #@30
    :cond_30
    const/4 v4, 0x1

    #@31
    .line 874
    .local v4, fits:Z
    :goto_31
    if-lez v1, :cond_3b

    #@33
    if-lez v0, :cond_3b

    #@35
    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    #@37
    iget-object v10, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@39
    if-ne v9, v10, :cond_45

    #@3b
    .line 878
    :cond_3b
    iget-object v9, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3d
    invoke-virtual {v9, v8, v8, v7, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@40
    .line 879
    iput-object v13, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@42
    goto :goto_d

    #@43
    .end local v4           #fits:Z
    :cond_43
    move v4, v8

    #@44
    .line 871
    goto :goto_31

    #@45
    .line 883
    .restart local v4       #fits:Z
    :cond_45
    iget-object v9, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@47
    invoke-virtual {v9, v8, v8, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@4a
    .line 885
    sget-object v8, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    #@4c
    iget-object v9, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@4e
    if-ne v8, v9, :cond_60

    #@50
    .line 887
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@52
    invoke-virtual {v8}, Landroid/graphics/Matrix;->isIdentity()Z

    #@55
    move-result v8

    #@56
    if-eqz v8, :cond_5b

    #@58
    .line 888
    iput-object v13, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@5a
    goto :goto_d

    #@5b
    .line 890
    :cond_5b
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@5d
    iput-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@5f
    goto :goto_d

    #@60
    .line 892
    :cond_60
    if-eqz v4, :cond_65

    #@62
    .line 894
    iput-object v13, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@64
    goto :goto_d

    #@65
    .line 895
    :cond_65
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@67
    iget-object v9, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@69
    if-ne v8, v9, :cond_83

    #@6b
    .line 897
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@6d
    iput-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@6f
    .line 898
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@71
    sub-int v9, v7, v1

    #@73
    int-to-float v9, v9

    #@74
    mul-float/2addr v9, v11

    #@75
    add-float/2addr v9, v11

    #@76
    float-to-int v9, v9

    #@77
    int-to-float v9, v9

    #@78
    sub-int v10, v6, v0

    #@7a
    int-to-float v10, v10

    #@7b
    mul-float/2addr v10, v11

    #@7c
    add-float/2addr v10, v11

    #@7d
    float-to-int v10, v10

    #@7e
    int-to-float v10, v10

    #@7f
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@82
    goto :goto_d

    #@83
    .line 900
    :cond_83
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    #@85
    iget-object v9, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@87
    if-ne v8, v9, :cond_be

    #@89
    .line 901
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@8b
    iput-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@8d
    .line 904
    const/4 v2, 0x0

    #@8e
    .local v2, dx:F
    const/4 v3, 0x0

    #@8f
    .line 906
    .local v3, dy:F
    mul-int v8, v1, v6

    #@91
    mul-int v9, v7, v0

    #@93
    if-le v8, v9, :cond_b3

    #@95
    .line 907
    int-to-float v8, v6

    #@96
    int-to-float v9, v0

    #@97
    div-float v5, v8, v9

    #@99
    .line 908
    .local v5, scale:F
    int-to-float v8, v7

    #@9a
    int-to-float v9, v1

    #@9b
    mul-float/2addr v9, v5

    #@9c
    sub-float/2addr v8, v9

    #@9d
    mul-float v2, v8, v11

    #@9f
    .line 914
    :goto_9f
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@a1
    invoke-virtual {v8, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    #@a4
    .line 915
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@a6
    add-float v9, v2, v11

    #@a8
    float-to-int v9, v9

    #@a9
    int-to-float v9, v9

    #@aa
    add-float v10, v3, v11

    #@ac
    float-to-int v10, v10

    #@ad
    int-to-float v10, v10

    #@ae
    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@b1
    goto/16 :goto_d

    #@b3
    .line 910
    .end local v5           #scale:F
    :cond_b3
    int-to-float v8, v7

    #@b4
    int-to-float v9, v1

    #@b5
    div-float v5, v8, v9

    #@b7
    .line 911
    .restart local v5       #scale:F
    int-to-float v8, v6

    #@b8
    int-to-float v9, v0

    #@b9
    mul-float/2addr v9, v5

    #@ba
    sub-float/2addr v8, v9

    #@bb
    mul-float v3, v8, v11

    #@bd
    goto :goto_9f

    #@be
    .line 916
    .end local v2           #dx:F
    .end local v3           #dy:F
    .end local v5           #scale:F
    :cond_be
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    #@c0
    iget-object v9, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@c2
    if-ne v8, v9, :cond_f5

    #@c4
    .line 917
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@c6
    iput-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@c8
    .line 922
    if-gt v1, v7, :cond_ea

    #@ca
    if-gt v0, v6, :cond_ea

    #@cc
    .line 923
    const/high16 v5, 0x3f80

    #@ce
    .line 929
    .restart local v5       #scale:F
    :goto_ce
    int-to-float v8, v7

    #@cf
    int-to-float v9, v1

    #@d0
    mul-float/2addr v9, v5

    #@d1
    sub-float/2addr v8, v9

    #@d2
    mul-float/2addr v8, v11

    #@d3
    add-float/2addr v8, v11

    #@d4
    float-to-int v8, v8

    #@d5
    int-to-float v2, v8

    #@d6
    .line 930
    .restart local v2       #dx:F
    int-to-float v8, v6

    #@d7
    int-to-float v9, v0

    #@d8
    mul-float/2addr v9, v5

    #@d9
    sub-float/2addr v8, v9

    #@da
    mul-float/2addr v8, v11

    #@db
    add-float/2addr v8, v11

    #@dc
    float-to-int v8, v8

    #@dd
    int-to-float v3, v8

    #@de
    .line 932
    .restart local v3       #dy:F
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@e0
    invoke-virtual {v8, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    #@e3
    .line 933
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@e5
    invoke-virtual {v8, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@e8
    goto/16 :goto_d

    #@ea
    .line 925
    .end local v2           #dx:F
    .end local v3           #dy:F
    .end local v5           #scale:F
    :cond_ea
    int-to-float v8, v7

    #@eb
    int-to-float v9, v1

    #@ec
    div-float/2addr v8, v9

    #@ed
    int-to-float v9, v6

    #@ee
    int-to-float v10, v0

    #@ef
    div-float/2addr v9, v10

    #@f0
    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    #@f3
    move-result v5

    #@f4
    .restart local v5       #scale:F
    goto :goto_ce

    #@f5
    .line 936
    .end local v5           #scale:F
    :cond_f5
    iget-object v8, p0, Landroid/widget/ImageView;->mTempSrc:Landroid/graphics/RectF;

    #@f7
    int-to-float v9, v1

    #@f8
    int-to-float v10, v0

    #@f9
    invoke-virtual {v8, v12, v12, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    #@fc
    .line 937
    iget-object v8, p0, Landroid/widget/ImageView;->mTempDst:Landroid/graphics/RectF;

    #@fe
    int-to-float v9, v7

    #@ff
    int-to-float v10, v6

    #@100
    invoke-virtual {v8, v12, v12, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    #@103
    .line 939
    iget-object v8, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@105
    iput-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@107
    .line 940
    iget-object v8, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@109
    iget-object v9, p0, Landroid/widget/ImageView;->mTempSrc:Landroid/graphics/RectF;

    #@10b
    iget-object v10, p0, Landroid/widget/ImageView;->mTempDst:Landroid/graphics/RectF;

    #@10d
    iget-object v11, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@10f
    invoke-static {v11}, Landroid/widget/ImageView;->scaleTypeToScaleToFit(Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Matrix$ScaleToFit;

    #@112
    move-result-object v11

    #@113
    invoke-virtual {v8, v9, v10, v11}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    #@116
    goto/16 :goto_d
.end method

.method private initImageView()V
    .registers 2

    #@0
    .prologue
    .line 165
    new-instance v0, Landroid/graphics/Matrix;

    #@2
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@5
    iput-object v0, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@7
    .line 166
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    #@9
    iput-object v0, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@b
    .line 167
    return-void
.end method

.method private resizeFromDrawable()V
    .registers 5

    #@0
    .prologue
    .line 694
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 695
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_23

    #@4
    .line 696
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@7
    move-result v2

    #@8
    .line 697
    .local v2, w:I
    if-gez v2, :cond_c

    #@a
    iget v2, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@c
    .line 698
    :cond_c
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@f
    move-result v1

    #@10
    .line 699
    .local v1, h:I
    if-gez v1, :cond_14

    #@12
    iget v1, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@14
    .line 700
    :cond_14
    iget v3, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@16
    if-ne v2, v3, :cond_1c

    #@18
    iget v3, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@1a
    if-eq v1, v3, :cond_23

    #@1c
    .line 701
    :cond_1c
    iput v2, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@1e
    .line 702
    iput v1, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@20
    .line 703
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@23
    .line 706
    .end local v1           #h:I
    .end local v2           #w:I
    :cond_23
    return-void
.end method

.method private resolveAdjustedSize(III)I
    .registers 8
    .parameter "desiredSize"
    .parameter "maxSize"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 828
    move v0, p1

    #@1
    .line 829
    .local v0, result:I
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v1

    #@5
    .line 830
    .local v1, specMode:I
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 831
    .local v2, specSize:I
    sparse-switch v1, :sswitch_data_1e

    #@c
    .line 849
    :goto_c
    return v0

    #@d
    .line 836
    :sswitch_d
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    #@10
    move-result v0

    #@11
    .line 837
    goto :goto_c

    #@12
    .line 842
    :sswitch_12
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v3

    #@16
    invoke-static {v3, p2}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v0

    #@1a
    .line 843
    goto :goto_c

    #@1b
    .line 846
    :sswitch_1b
    move v0, v2

    #@1c
    goto :goto_c

    #@1d
    .line 831
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        -0x80000000 -> :sswitch_12
        0x0 -> :sswitch_d
        0x40000000 -> :sswitch_1b
    .end sparse-switch
.end method

.method private resolveUri()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 603
    iget-object v5, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    if-eqz v5, :cond_6

    #@5
    .line 657
    :cond_5
    :goto_5
    return-void

    #@6
    .line 607
    :cond_6
    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v3

    #@a
    .line 608
    .local v3, rsrc:Landroid/content/res/Resources;
    if-eqz v3, :cond_5

    #@c
    .line 612
    const/4 v0, 0x0

    #@d
    .line 614
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iget v5, p0, Landroid/widget/ImageView;->mResource:I

    #@f
    if-eqz v5, :cond_39

    #@11
    .line 616
    :try_start_11
    iget v5, p0, Landroid/widget/ImageView;->mResource:I

    #@13
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_16} :catch_1b

    #@16
    move-result-object v0

    #@17
    .line 656
    :cond_17
    :goto_17
    invoke-direct {p0, v0}, Landroid/widget/ImageView;->updateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1a
    goto :goto_5

    #@1b
    .line 617
    :catch_1b
    move-exception v1

    #@1c
    .line 618
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "ImageView"

    #@1e
    new-instance v6, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v7, "Unable to find resource: "

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    iget v7, p0, Landroid/widget/ImageView;->mResource:I

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    .line 620
    iput-object v8, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@38
    goto :goto_17

    #@39
    .line 622
    .end local v1           #e:Ljava/lang/Exception;
    :cond_39
    iget-object v5, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@3b
    if-eqz v5, :cond_5

    #@3d
    .line 623
    iget-object v5, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@3f
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    .line 624
    .local v4, scheme:Ljava/lang/String;
    const-string v5, "android.resource"

    #@45
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v5

    #@49
    if-eqz v5, :cond_9b

    #@4b
    .line 627
    :try_start_4b
    iget-object v5, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4d
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@50
    move-result-object v5

    #@51
    iget-object v6, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@53
    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;

    #@56
    move-result-object v2

    #@57
    .line 629
    .local v2, r:Landroid/content/ContentResolver$OpenResourceIdResult;
    iget-object v5, v2, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    #@59
    iget v6, v2, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    #@5b
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_5e} :catch_7f

    #@5e
    move-result-object v0

    #@5f
    .line 646
    .end local v2           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :goto_5f
    if-nez v0, :cond_17

    #@61
    .line 647
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@63
    new-instance v6, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string/jumbo v7, "resolveUri failed on bad bitmap uri: "

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    iget-object v7, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@71
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@7c
    .line 650
    iput-object v8, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@7e
    goto :goto_17

    #@7f
    .line 630
    :catch_7f
    move-exception v1

    #@80
    .line 631
    .restart local v1       #e:Ljava/lang/Exception;
    const-string v5, "ImageView"

    #@82
    new-instance v6, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v7, "Unable to open content: "

    #@89
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v6

    #@8d
    iget-object v7, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@8f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v6

    #@93
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v6

    #@97
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9a
    goto :goto_5f

    #@9b
    .line 633
    .end local v1           #e:Ljava/lang/Exception;
    :cond_9b
    const-string v5, "content"

    #@9d
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v5

    #@a1
    if-nez v5, :cond_ab

    #@a3
    const-string v5, "file"

    #@a5
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v5

    #@a9
    if-eqz v5, :cond_d9

    #@ab
    .line 636
    :cond_ab
    :try_start_ab
    iget-object v5, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@ad
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b0
    move-result-object v5

    #@b1
    iget-object v6, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@b3
    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@b6
    move-result-object v5

    #@b7
    const/4 v6, 0x0

    #@b8
    invoke-static {v5, v6}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_bb
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_bb} :catch_bd

    #@bb
    move-result-object v0

    #@bc
    goto :goto_5f

    #@bd
    .line 639
    :catch_bd
    move-exception v1

    #@be
    .line 640
    .restart local v1       #e:Ljava/lang/Exception;
    const-string v5, "ImageView"

    #@c0
    new-instance v6, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v7, "Unable to open content: "

    #@c7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v6

    #@cb
    iget-object v7, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@cd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v6

    #@d1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v6

    #@d5
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d8
    goto :goto_5f

    #@d9
    .line 643
    .end local v1           #e:Ljava/lang/Exception;
    :cond_d9
    iget-object v5, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@db
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@de
    move-result-object v5

    #@df
    invoke-static {v5}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@e2
    move-result-object v0

    #@e3
    goto/16 :goto_5f
.end method

.method private static scaleTypeToScaleToFit(Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Matrix$ScaleToFit;
    .registers 3
    .parameter "st"

    #@0
    .prologue
    .line 717
    sget-object v0, Landroid/widget/ImageView;->sS2FArray:[Landroid/graphics/Matrix$ScaleToFit;

    #@2
    iget v1, p0, Landroid/widget/ImageView$ScaleType;->nativeInt:I

    #@4
    add-int/lit8 v1, v1, -0x1

    #@6
    aget-object v0, v0, v1

    #@8
    return-object v0
.end method

.method private updateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 672
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 673
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@a
    .line 674
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 676
    :cond_f
    iput-object p1, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    .line 677
    if-eqz p1, :cond_42

    #@13
    .line 678
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@16
    .line 679
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_23

    #@1c
    .line 680
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawableState()[I

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@23
    .line 682
    :cond_23
    iget v0, p0, Landroid/widget/ImageView;->mLevel:I

    #@25
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@28
    .line 683
    invoke-virtual {p0}, Landroid/widget/ImageView;->getLayoutDirection()I

    #@2b
    move-result v0

    #@2c
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@2f
    .line 684
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@32
    move-result v0

    #@33
    iput v0, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@35
    .line 685
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@38
    move-result v0

    #@39
    iput v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@3b
    .line 686
    invoke-direct {p0}, Landroid/widget/ImageView;->applyColorMod()V

    #@3e
    .line 687
    invoke-direct {p0}, Landroid/widget/ImageView;->configureBounds()V

    #@41
    .line 691
    :goto_41
    return-void

    #@42
    .line 689
    :cond_42
    const/4 v0, -0x1

    #@43
    iput v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@45
    iput v0, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@47
    goto :goto_41
.end method


# virtual methods
.method public final clearColorFilter()V
    .registers 2

    #@0
    .prologue
    .line 1075
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@4
    .line 1076
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 947
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    #@3
    .line 948
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 949
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_14

    #@7
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 950
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawableState()[I

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@14
    .line 952
    :cond_14
    return-void
.end method

.method public getAdjustViewBounds()Z
    .registers 2

    #@0
    .prologue
    .line 222
    iget-boolean v0, p0, Landroid/widget/ImageView;->mAdjustViewBounds:Z

    #@2
    return v0
.end method

.method public getBaseline()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    .line 1000
    iget-boolean v0, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1001
    invoke-virtual {p0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    #@7
    move-result v0

    #@8
    .line 1003
    :goto_8
    return v0

    #@9
    :cond_9
    iget v0, p0, Landroid/widget/ImageView;->mBaseline:I

    #@b
    goto :goto_8
.end method

.method public getBaselineAlignBottom()Z
    .registers 2

    #@0
    .prologue
    .line 1046
    iget-boolean v0, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@2
    return v0
.end method

.method public getColorFilter()Landroid/graphics/ColorFilter;
    .registers 2

    #@0
    .prologue
    .line 1086
    iget-object v0, p0, Landroid/widget/ImageView;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    return-object v0
.end method

.method public getCropToPadding()Z
    .registers 2

    #@0
    .prologue
    .line 582
    iget-boolean v0, p0, Landroid/widget/ImageView;->mCropToPadding:Z

    #@2
    return v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 323
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getImageAlpha()I
    .registers 2

    #@0
    .prologue
    .line 1113
    iget v0, p0, Landroid/widget/ImageView;->mAlpha:I

    #@2
    return v0
.end method

.method public getImageMatrix()Landroid/graphics/Matrix;
    .registers 2

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method public getMaxHeight()I
    .registers 2

    #@0
    .prologue
    .line 291
    iget v0, p0, Landroid/widget/ImageView;->mMaxHeight:I

    #@2
    return v0
.end method

.method public getMaxWidth()I
    .registers 2

    #@0
    .prologue
    .line 253
    iget v0, p0, Landroid/widget/ImageView;->mMaxWidth:I

    #@2
    return v0
.end method

.method public getScaleType()Landroid/widget/ImageView$ScaleType;
    .registers 2

    #@0
    .prologue
    .line 544
    iget-object v0, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@2
    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .registers 2

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "dr"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_8

    #@4
    .line 190
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@7
    .line 194
    :goto_7
    return-void

    #@8
    .line 192
    :cond_8
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    goto :goto_7
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 176
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@3
    .line 177
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 178
    :cond_c
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1169
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@4
    .line 1170
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 1171
    iget-object v2, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {p0}, Landroid/widget/ImageView;->getVisibility()I

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_15

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@14
    .line 1173
    :cond_14
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 1171
    goto :goto_11
.end method

.method public onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 661
    iget-object v0, p0, Landroid/widget/ImageView;->mState:[I

    #@2
    if-nez v0, :cond_9

    #@4
    .line 662
    invoke-super {p0, p1}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@7
    move-result-object v0

    #@8
    .line 666
    :goto_8
    return-object v0

    #@9
    .line 663
    :cond_9
    iget-boolean v0, p0, Landroid/widget/ImageView;->mMergeState:Z

    #@b
    if-nez v0, :cond_10

    #@d
    .line 664
    iget-object v0, p0, Landroid/widget/ImageView;->mState:[I

    #@f
    goto :goto_8

    #@10
    .line 666
    :cond_10
    iget-object v0, p0, Landroid/widget/ImageView;->mState:[I

    #@12
    array-length v0, v0

    #@13
    add-int/2addr v0, p1

    #@14
    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Landroid/widget/ImageView;->mState:[I

    #@1a
    invoke-static {v0, v1}, Landroid/widget/ImageView;->mergeDrawableStates([I[I)[I

    #@1d
    move-result-object v0

    #@1e
    goto :goto_8
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1177
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@4
    .line 1178
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 1179
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@d
    .line 1181
    :cond_d
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    .line 956
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 958
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-nez v3, :cond_8

    #@7
    .line 988
    :cond_7
    :goto_7
    return-void

    #@8
    .line 962
    :cond_8
    iget v3, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@a
    if-eqz v3, :cond_7

    #@c
    iget v3, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@e
    if-eqz v3, :cond_7

    #@10
    .line 966
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@12
    if-nez v3, :cond_22

    #@14
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@16
    if-nez v3, :cond_22

    #@18
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@1a
    if-nez v3, :cond_22

    #@1c
    .line 967
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@21
    goto :goto_7

    #@22
    .line 969
    :cond_22
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    #@25
    move-result v0

    #@26
    .line 970
    .local v0, saveCount:I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@29
    .line 972
    iget-boolean v3, p0, Landroid/widget/ImageView;->mCropToPadding:Z

    #@2b
    if-eqz v3, :cond_4c

    #@2d
    .line 973
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@2f
    .line 974
    .local v1, scrollX:I
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@31
    .line 975
    .local v2, scrollY:I
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@33
    add-int/2addr v3, v1

    #@34
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@36
    add-int/2addr v4, v2

    #@37
    iget v5, p0, Landroid/view/View;->mRight:I

    #@39
    add-int/2addr v5, v1

    #@3a
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@3c
    sub-int/2addr v5, v6

    #@3d
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@3f
    sub-int/2addr v5, v6

    #@40
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@42
    add-int/2addr v6, v2

    #@43
    iget v7, p0, Landroid/view/View;->mTop:I

    #@45
    sub-int/2addr v6, v7

    #@46
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@48
    sub-int/2addr v6, v7

    #@49
    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@4c
    .line 980
    .end local v1           #scrollX:I
    .end local v2           #scrollY:I
    :cond_4c
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@4e
    int-to-float v3, v3

    #@4f
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@51
    int-to-float v4, v4

    #@52
    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@55
    .line 982
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@57
    if-eqz v3, :cond_5e

    #@59
    .line 983
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    #@5b
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    #@5e
    .line 985
    :cond_5e
    iget-object v3, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@60
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@63
    .line 986
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@66
    goto :goto_7
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1185
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1186
    const-class v0, Landroid/widget/ImageView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1187
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1191
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1192
    const-class v0, Landroid/widget/ImageView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1193
    return-void
.end method

.method protected onMeasure(II)V
    .registers 28
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 722
    invoke-direct/range {p0 .. p0}, Landroid/widget/ImageView;->resolveUri()V

    #@3
    .line 727
    const/4 v5, 0x0

    #@4
    .line 730
    .local v5, desiredAspect:F
    const/16 v17, 0x0

    #@6
    .line 733
    .local v17, resizeWidth:Z
    const/16 v16, 0x0

    #@8
    .line 735
    .local v16, resizeHeight:Z
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v20

    #@c
    .line 736
    .local v20, widthSpecMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@f
    move-result v9

    #@10
    .line 738
    .local v9, heightSpecMode:I
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@14
    move-object/from16 v21, v0

    #@16
    if-nez v21, :cond_da

    #@18
    .line 740
    const/16 v21, -0x1

    #@1a
    move/from16 v0, v21

    #@1c
    move-object/from16 v1, p0

    #@1e
    iput v0, v1, Landroid/widget/ImageView;->mDrawableWidth:I

    #@20
    .line 741
    const/16 v21, -0x1

    #@22
    move/from16 v0, v21

    #@24
    move-object/from16 v1, p0

    #@26
    iput v0, v1, Landroid/widget/ImageView;->mDrawableHeight:I

    #@28
    .line 742
    const/4 v7, 0x0

    #@29
    .local v7, h:I
    move/from16 v18, v7

    #@2b
    .line 759
    .local v18, w:I
    :cond_2b
    :goto_2b
    move-object/from16 v0, p0

    #@2d
    iget v13, v0, Landroid/view/View;->mPaddingLeft:I

    #@2f
    .line 760
    .local v13, pleft:I
    move-object/from16 v0, p0

    #@31
    iget v14, v0, Landroid/view/View;->mPaddingRight:I

    #@33
    .line 761
    .local v14, pright:I
    move-object/from16 v0, p0

    #@35
    iget v15, v0, Landroid/view/View;->mPaddingTop:I

    #@37
    .line 762
    .local v15, ptop:I
    move-object/from16 v0, p0

    #@39
    iget v12, v0, Landroid/view/View;->mPaddingBottom:I

    #@3b
    .line 767
    .local v12, pbottom:I
    if-nez v17, :cond_3f

    #@3d
    if-eqz v16, :cond_117

    #@3f
    .line 774
    :cond_3f
    add-int v21, v18, v13

    #@41
    add-int v21, v21, v14

    #@43
    move-object/from16 v0, p0

    #@45
    iget v0, v0, Landroid/widget/ImageView;->mMaxWidth:I

    #@47
    move/from16 v22, v0

    #@49
    move-object/from16 v0, p0

    #@4b
    move/from16 v1, v21

    #@4d
    move/from16 v2, v22

    #@4f
    move/from16 v3, p1

    #@51
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageView;->resolveAdjustedSize(III)I

    #@54
    move-result v19

    #@55
    .line 777
    .local v19, widthSize:I
    add-int v21, v7, v15

    #@57
    add-int v21, v21, v12

    #@59
    move-object/from16 v0, p0

    #@5b
    iget v0, v0, Landroid/widget/ImageView;->mMaxHeight:I

    #@5d
    move/from16 v22, v0

    #@5f
    move-object/from16 v0, p0

    #@61
    move/from16 v1, v21

    #@63
    move/from16 v2, v22

    #@65
    move/from16 v3, p2

    #@67
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageView;->resolveAdjustedSize(III)I

    #@6a
    move-result v8

    #@6b
    .line 779
    .local v8, heightSize:I
    const/16 v21, 0x0

    #@6d
    cmpl-float v21, v5, v21

    #@6f
    if-eqz v21, :cond_d2

    #@71
    .line 781
    sub-int v21, v19, v13

    #@73
    sub-int v21, v21, v14

    #@75
    move/from16 v0, v21

    #@77
    int-to-float v0, v0

    #@78
    move/from16 v21, v0

    #@7a
    sub-int v22, v8, v15

    #@7c
    sub-int v22, v22, v12

    #@7e
    move/from16 v0, v22

    #@80
    int-to-float v0, v0

    #@81
    move/from16 v22, v0

    #@83
    div-float v4, v21, v22

    #@85
    .line 784
    .local v4, actualAspect:F
    sub-float v21, v4, v5

    #@87
    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    #@8a
    move-result v21

    #@8b
    move/from16 v0, v21

    #@8d
    float-to-double v0, v0

    #@8e
    move-wide/from16 v21, v0

    #@90
    const-wide v23, 0x3e7ad7f29abcaf48L

    #@95
    cmpl-double v21, v21, v23

    #@97
    if-lez v21, :cond_d2

    #@99
    .line 786
    const/4 v6, 0x0

    #@9a
    .line 789
    .local v6, done:Z
    if-eqz v17, :cond_b7

    #@9c
    .line 790
    sub-int v21, v8, v15

    #@9e
    sub-int v21, v21, v12

    #@a0
    move/from16 v0, v21

    #@a2
    int-to-float v0, v0

    #@a3
    move/from16 v21, v0

    #@a5
    mul-float v21, v21, v5

    #@a7
    move/from16 v0, v21

    #@a9
    float-to-int v0, v0

    #@aa
    move/from16 v21, v0

    #@ac
    add-int v21, v21, v13

    #@ae
    add-int v11, v21, v14

    #@b0
    .line 792
    .local v11, newWidth:I
    move/from16 v0, v19

    #@b2
    if-gt v11, v0, :cond_b7

    #@b4
    .line 793
    move/from16 v19, v11

    #@b6
    .line 794
    const/4 v6, 0x1

    #@b7
    .line 799
    .end local v11           #newWidth:I
    :cond_b7
    if-nez v6, :cond_d2

    #@b9
    if-eqz v16, :cond_d2

    #@bb
    .line 800
    sub-int v21, v19, v13

    #@bd
    sub-int v21, v21, v14

    #@bf
    move/from16 v0, v21

    #@c1
    int-to-float v0, v0

    #@c2
    move/from16 v21, v0

    #@c4
    div-float v21, v21, v5

    #@c6
    move/from16 v0, v21

    #@c8
    float-to-int v0, v0

    #@c9
    move/from16 v21, v0

    #@cb
    add-int v21, v21, v15

    #@cd
    add-int v10, v21, v12

    #@cf
    .line 802
    .local v10, newHeight:I
    if-gt v10, v8, :cond_d2

    #@d1
    .line 803
    move v8, v10

    #@d2
    .line 823
    .end local v4           #actualAspect:F
    .end local v6           #done:Z
    .end local v10           #newHeight:I
    :cond_d2
    :goto_d2
    move-object/from16 v0, p0

    #@d4
    move/from16 v1, v19

    #@d6
    invoke-virtual {v0, v1, v8}, Landroid/widget/ImageView;->setMeasuredDimension(II)V

    #@d9
    .line 824
    return-void

    #@da
    .line 744
    .end local v7           #h:I
    .end local v8           #heightSize:I
    .end local v12           #pbottom:I
    .end local v13           #pleft:I
    .end local v14           #pright:I
    .end local v15           #ptop:I
    .end local v18           #w:I
    .end local v19           #widthSize:I
    :cond_da
    move-object/from16 v0, p0

    #@dc
    iget v0, v0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@de
    move/from16 v18, v0

    #@e0
    .line 745
    .restart local v18       #w:I
    move-object/from16 v0, p0

    #@e2
    iget v7, v0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@e4
    .line 746
    .restart local v7       #h:I
    if-gtz v18, :cond_e8

    #@e6
    const/16 v18, 0x1

    #@e8
    .line 747
    :cond_e8
    if-gtz v7, :cond_eb

    #@ea
    const/4 v7, 0x1

    #@eb
    .line 751
    :cond_eb
    move-object/from16 v0, p0

    #@ed
    iget-boolean v0, v0, Landroid/widget/ImageView;->mAdjustViewBounds:Z

    #@ef
    move/from16 v21, v0

    #@f1
    if-eqz v21, :cond_2b

    #@f3
    .line 752
    const/high16 v21, 0x4000

    #@f5
    move/from16 v0, v20

    #@f7
    move/from16 v1, v21

    #@f9
    if-eq v0, v1, :cond_111

    #@fb
    const/16 v17, 0x1

    #@fd
    .line 753
    :goto_fd
    const/high16 v21, 0x4000

    #@ff
    move/from16 v0, v21

    #@101
    if-eq v9, v0, :cond_114

    #@103
    const/16 v16, 0x1

    #@105
    .line 755
    :goto_105
    move/from16 v0, v18

    #@107
    int-to-float v0, v0

    #@108
    move/from16 v21, v0

    #@10a
    int-to-float v0, v7

    #@10b
    move/from16 v22, v0

    #@10d
    div-float v5, v21, v22

    #@10f
    goto/16 :goto_2b

    #@111
    .line 752
    :cond_111
    const/16 v17, 0x0

    #@113
    goto :goto_fd

    #@114
    .line 753
    :cond_114
    const/16 v16, 0x0

    #@116
    goto :goto_105

    #@117
    .line 813
    .restart local v12       #pbottom:I
    .restart local v13       #pleft:I
    .restart local v14       #pright:I
    .restart local v15       #ptop:I
    :cond_117
    add-int v21, v13, v14

    #@119
    add-int v18, v18, v21

    #@11b
    .line 814
    add-int v21, v15, v12

    #@11d
    add-int v7, v7, v21

    #@11f
    .line 816
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ImageView;->getSuggestedMinimumWidth()I

    #@122
    move-result v21

    #@123
    move/from16 v0, v18

    #@125
    move/from16 v1, v21

    #@127
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@12a
    move-result v18

    #@12b
    .line 817
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ImageView;->getSuggestedMinimumHeight()I

    #@12e
    move-result v21

    #@12f
    move/from16 v0, v21

    #@131
    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    #@134
    move-result v7

    #@135
    .line 819
    const/16 v21, 0x0

    #@137
    move/from16 v0, v18

    #@139
    move/from16 v1, p1

    #@13b
    move/from16 v2, v21

    #@13d
    invoke-static {v0, v1, v2}, Landroid/widget/ImageView;->resolveSizeAndState(III)I

    #@140
    move-result v19

    #@141
    .line 820
    .restart local v19       #widthSize:I
    const/16 v21, 0x0

    #@143
    move/from16 v0, p2

    #@145
    move/from16 v1, v21

    #@147
    invoke-static {v7, v0, v1}, Landroid/widget/ImageView;->resolveSizeAndState(III)I

    #@14a
    move-result v8

    #@14b
    .restart local v8       #heightSize:I
    goto :goto_d2
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 203
    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 204
    invoke-virtual {p0}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    #@6
    move-result-object v0

    #@7
    .line 205
    .local v0, contentDescription:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_14

    #@d
    .line 206
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@14
    .line 208
    :cond_14
    return-void
.end method

.method public setAdjustViewBounds(Z)V
    .registers 3
    .parameter "adjustViewBounds"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 237
    iput-boolean p1, p0, Landroid/widget/ImageView;->mAdjustViewBounds:Z

    #@2
    .line 238
    if-eqz p1, :cond_9

    #@4
    .line 239
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@9
    .line 241
    :cond_9
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1138
    and-int/lit16 p1, p1, 0xff

    #@2
    .line 1139
    iget v0, p0, Landroid/widget/ImageView;->mAlpha:I

    #@4
    if-eq v0, p1, :cond_11

    #@6
    .line 1140
    iput p1, p0, Landroid/widget/ImageView;->mAlpha:I

    #@8
    .line 1141
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/widget/ImageView;->mColorMod:Z

    #@b
    .line 1142
    invoke-direct {p0}, Landroid/widget/ImageView;->applyColorMod()V

    #@e
    .line 1143
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@11
    .line 1145
    :cond_11
    return-void
.end method

.method public setBaseline(I)V
    .registers 3
    .parameter "baseline"

    #@0
    .prologue
    .line 1018
    iget v0, p0, Landroid/widget/ImageView;->mBaseline:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1019
    iput p1, p0, Landroid/widget/ImageView;->mBaseline:I

    #@6
    .line 1020
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@9
    .line 1022
    :cond_9
    return-void
.end method

.method public setBaselineAlignBottom(Z)V
    .registers 3
    .parameter "aligned"

    #@0
    .prologue
    .line 1034
    iget-boolean v0, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1035
    iput-boolean p1, p0, Landroid/widget/ImageView;->mBaselineAlignBottom:Z

    #@6
    .line 1036
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@9
    .line 1038
    :cond_9
    return-void
.end method

.method public final setColorFilter(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 1071
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    #@5
    .line 1072
    return-void
.end method

.method public final setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 4
    .parameter "color"
    .parameter "mode"

    #@0
    .prologue
    .line 1059
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    #@5
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@8
    .line 1060
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 1097
    iget-object v0, p0, Landroid/widget/ImageView;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    if-eq v0, p1, :cond_f

    #@4
    .line 1098
    iput-object p1, p0, Landroid/widget/ImageView;->mColorFilter:Landroid/graphics/ColorFilter;

    #@6
    .line 1099
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/widget/ImageView;->mColorMod:Z

    #@9
    .line 1100
    invoke-direct {p0}, Landroid/widget/ImageView;->applyColorMod()V

    #@c
    .line 1101
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@f
    .line 1103
    :cond_f
    return-void
.end method

.method public setCropToPadding(Z)V
    .registers 3
    .parameter "cropToPadding"

    #@0
    .prologue
    .line 595
    iget-boolean v0, p0, Landroid/widget/ImageView;->mCropToPadding:Z

    #@2
    if-eq v0, p1, :cond_c

    #@4
    .line 596
    iput-boolean p1, p0, Landroid/widget/ImageView;->mCropToPadding:Z

    #@6
    .line 597
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@9
    .line 598
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@c
    .line 600
    :cond_c
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 7
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 854
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setFrame(IIII)Z

    #@3
    move-result v0

    #@4
    .line 855
    .local v0, changed:Z
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/widget/ImageView;->mHaveFrame:Z

    #@7
    .line 856
    invoke-direct {p0}, Landroid/widget/ImageView;->configureBounds()V

    #@a
    .line 857
    return v0
.end method

.method public setImageAlpha(I)V
    .registers 2
    .parameter "alpha"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 1125
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setAlpha(I)V

    #@3
    .line 1126
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bm"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 421
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    #@2
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@b
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@e
    .line 422
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "drawable"

    #@0
    .prologue
    .line 396
    iget-object v2, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq v2, p1, :cond_1f

    #@4
    .line 397
    const/4 v2, 0x0

    #@5
    iput v2, p0, Landroid/widget/ImageView;->mResource:I

    #@7
    .line 398
    const/4 v2, 0x0

    #@8
    iput-object v2, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@a
    .line 400
    iget v1, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@c
    .line 401
    .local v1, oldWidth:I
    iget v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@e
    .line 403
    .local v0, oldHeight:I
    invoke-direct {p0, p1}, Landroid/widget/ImageView;->updateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 405
    iget v2, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@13
    if-ne v1, v2, :cond_19

    #@15
    iget v2, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@17
    if-eq v0, v2, :cond_1c

    #@19
    .line 406
    :cond_19
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@1c
    .line 408
    :cond_1c
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@1f
    .line 410
    .end local v0           #oldHeight:I
    .end local v1           #oldWidth:I
    :cond_1f
    return-void
.end method

.method public setImageLevel(I)V
    .registers 3
    .parameter "level"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 447
    iput p1, p0, Landroid/widget/ImageView;->mLevel:I

    #@2
    .line 448
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 449
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@b
    .line 450
    invoke-direct {p0}, Landroid/widget/ImageView;->resizeFromDrawable()V

    #@e
    .line 452
    :cond_e
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .registers 3
    .parameter "matrix"

    #@0
    .prologue
    .line 559
    if-eqz p1, :cond_9

    #@2
    invoke-virtual {p1}, Landroid/graphics/Matrix;->isIdentity()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 560
    const/4 p1, 0x0

    #@9
    .line 564
    :cond_9
    if-nez p1, :cond_13

    #@b
    iget-object v0, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@d
    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_1d

    #@13
    :cond_13
    if-eqz p1, :cond_28

    #@15
    iget-object v0, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@17
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_28

    #@1d
    .line 566
    :cond_1d
    iget-object v0, p0, Landroid/widget/ImageView;->mMatrix:Landroid/graphics/Matrix;

    #@1f
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@22
    .line 567
    invoke-direct {p0}, Landroid/widget/ImageView;->configureBounds()V

    #@25
    .line 568
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@28
    .line 570
    :cond_28
    return-void
.end method

.method public setImageResource(I)V
    .registers 6
    .parameter "resId"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 341
    iget-object v2, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@3
    if-nez v2, :cond_9

    #@5
    iget v2, p0, Landroid/widget/ImageView;->mResource:I

    #@7
    if-eq v2, p1, :cond_25

    #@9
    .line 342
    :cond_9
    invoke-direct {p0, v3}, Landroid/widget/ImageView;->updateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@c
    .line 343
    iput p1, p0, Landroid/widget/ImageView;->mResource:I

    #@e
    .line 344
    iput-object v3, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@10
    .line 346
    iget v1, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@12
    .line 347
    .local v1, oldWidth:I
    iget v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@14
    .line 349
    .local v0, oldHeight:I
    invoke-direct {p0}, Landroid/widget/ImageView;->resolveUri()V

    #@17
    .line 351
    iget v2, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@19
    if-ne v1, v2, :cond_1f

    #@1b
    iget v2, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@1d
    if-eq v0, v2, :cond_22

    #@1f
    .line 352
    :cond_1f
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@22
    .line 354
    :cond_22
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@25
    .line 356
    .end local v0           #oldHeight:I
    .end local v1           #oldWidth:I
    :cond_25
    return-void
.end method

.method public setImageState([IZ)V
    .registers 4
    .parameter "state"
    .parameter "merge"

    #@0
    .prologue
    .line 425
    iput-object p1, p0, Landroid/widget/ImageView;->mState:[I

    #@2
    .line 426
    iput-boolean p2, p0, Landroid/widget/ImageView;->mMergeState:Z

    #@4
    .line 427
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 428
    invoke-virtual {p0}, Landroid/widget/ImageView;->refreshDrawableState()V

    #@b
    .line 429
    invoke-direct {p0}, Landroid/widget/ImageView;->resizeFromDrawable()V

    #@e
    .line 431
    :cond_e
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .registers 5
    .parameter "uri"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 371
    iget v2, p0, Landroid/widget/ImageView;->mResource:I

    #@2
    if-nez v2, :cond_16

    #@4
    iget-object v2, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@6
    if-eq v2, p1, :cond_34

    #@8
    if-eqz p1, :cond_16

    #@a
    iget-object v2, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@c
    if-eqz v2, :cond_16

    #@e
    iget-object v2, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@10
    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_34

    #@16
    .line 374
    :cond_16
    const/4 v2, 0x0

    #@17
    invoke-direct {p0, v2}, Landroid/widget/ImageView;->updateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1a
    .line 375
    const/4 v2, 0x0

    #@1b
    iput v2, p0, Landroid/widget/ImageView;->mResource:I

    #@1d
    .line 376
    iput-object p1, p0, Landroid/widget/ImageView;->mUri:Landroid/net/Uri;

    #@1f
    .line 378
    iget v1, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@21
    .line 379
    .local v1, oldWidth:I
    iget v0, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@23
    .line 381
    .local v0, oldHeight:I
    invoke-direct {p0}, Landroid/widget/ImageView;->resolveUri()V

    #@26
    .line 383
    iget v2, p0, Landroid/widget/ImageView;->mDrawableWidth:I

    #@28
    if-ne v1, v2, :cond_2e

    #@2a
    iget v2, p0, Landroid/widget/ImageView;->mDrawableHeight:I

    #@2c
    if-eq v0, v2, :cond_31

    #@2e
    .line 384
    :cond_2e
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@31
    .line 386
    :cond_31
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@34
    .line 388
    .end local v0           #oldHeight:I
    .end local v1           #oldWidth:I
    :cond_34
    return-void
.end method

.method public setMaxHeight(I)V
    .registers 2
    .parameter "maxHeight"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 316
    iput p1, p0, Landroid/widget/ImageView;->mMaxHeight:I

    #@2
    .line 317
    return-void
.end method

.method public setMaxWidth(I)V
    .registers 2
    .parameter "maxWidth"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 278
    iput p1, p0, Landroid/widget/ImageView;->mMaxWidth:I

    #@2
    .line 279
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .registers 4
    .parameter "scaleType"

    #@0
    .prologue
    .line 522
    if-nez p1, :cond_8

    #@2
    .line 523
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 526
    :cond_8
    iget-object v0, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@a
    if-eq v0, p1, :cond_1e

    #@c
    .line 527
    iput-object p1, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@e
    .line 529
    iget-object v0, p0, Landroid/widget/ImageView;->mScaleType:Landroid/widget/ImageView$ScaleType;

    #@10
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@12
    if-ne v0, v1, :cond_1f

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setWillNotCacheDrawing(Z)V

    #@18
    .line 531
    invoke-virtual {p0}, Landroid/widget/ImageView;->requestLayout()V

    #@1b
    .line 532
    invoke-virtual {p0}, Landroid/widget/ImageView;->invalidate()V

    #@1e
    .line 534
    :cond_1e
    return-void

    #@1f
    .line 529
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_15
.end method

.method public setSelected(Z)V
    .registers 2
    .parameter "selected"

    #@0
    .prologue
    .line 435
    invoke-super {p0, p1}, Landroid/view/View;->setSelected(Z)V

    #@3
    .line 436
    invoke-direct {p0}, Landroid/widget/ImageView;->resizeFromDrawable()V

    #@6
    .line 437
    return-void
.end method

.method public setVisibility(I)V
    .registers 5
    .parameter "visibility"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1161
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    #@4
    .line 1162
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 1163
    iget-object v2, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    if-nez p1, :cond_11

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@10
    .line 1165
    :cond_10
    return-void

    #@11
    :cond_11
    move v0, v1

    #@12
    .line 1163
    goto :goto_d
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "dr"

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Landroid/widget/ImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq v0, p1, :cond_a

    #@4
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
