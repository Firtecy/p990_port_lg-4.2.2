.class public Landroid/widget/SearchView$SearchAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "SearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/SearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchAutoComplete"
.end annotation


# instance fields
.field private mSearchView:Landroid/widget/SearchView;

.field private mThreshold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1658
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    #@3
    .line 1659
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getThreshold()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/widget/SearchView$SearchAutoComplete;->mThreshold:I

    #@9
    .line 1660
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 1663
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 1664
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getThreshold()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/widget/SearchView$SearchAutoComplete;->mThreshold:I

    #@9
    .line 1665
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 1668
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 1669
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getThreshold()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/widget/SearchView$SearchAutoComplete;->mThreshold:I

    #@9
    .line 1670
    return-void
.end method

.method static synthetic access$1600(Landroid/widget/SearchView$SearchAutoComplete;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1652
    invoke-direct {p0}, Landroid/widget/SearchView$SearchAutoComplete;->isEmpty()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 1686
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method


# virtual methods
.method public enoughToFilter()Z
    .registers 2

    #@0
    .prologue
    .line 1739
    iget v0, p0, Landroid/widget/SearchView$SearchAutoComplete;->mThreshold:I

    #@2
    if-lez v0, :cond_a

    #@4
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 1729
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 1730
    iget-object v0, p0, Landroid/widget/SearchView$SearchAutoComplete;->mSearchView:Landroid/widget/SearchView;

    #@5
    invoke-virtual {v0}, Landroid/widget/SearchView;->onTextFocusChanged()V

    #@8
    .line 1731
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1744
    const/4 v2, 0x4

    #@3
    if-ne p1, v2, :cond_48

    #@5
    .line 1745
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@7
    if-eqz v2, :cond_c

    #@9
    invoke-static {v3}, Landroid/widget/BubblePopupHelper;->setShowingAnyBubblePopup(Z)V

    #@c
    .line 1749
    :cond_c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_22

    #@12
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_22

    #@18
    .line 1750
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@1b
    move-result-object v0

    #@1c
    .line 1751
    .local v0, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_21

    #@1e
    .line 1752
    invoke-virtual {v0, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@21
    .line 1767
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_21
    :goto_21
    return v1

    #@22
    .line 1755
    :cond_22
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@25
    move-result v2

    #@26
    if-ne v2, v1, :cond_48

    #@28
    .line 1756
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@2b
    move-result-object v0

    #@2c
    .line 1757
    .restart local v0       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_31

    #@2e
    .line 1758
    invoke-virtual {v0, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@31
    .line 1760
    :cond_31
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@34
    move-result v2

    #@35
    if-eqz v2, :cond_48

    #@37
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@3a
    move-result v2

    #@3b
    if-nez v2, :cond_48

    #@3d
    .line 1761
    iget-object v2, p0, Landroid/widget/SearchView$SearchAutoComplete;->mSearchView:Landroid/widget/SearchView;

    #@3f
    invoke-virtual {v2}, Landroid/widget/SearchView;->clearFocus()V

    #@42
    .line 1762
    iget-object v2, p0, Landroid/widget/SearchView$SearchAutoComplete;->mSearchView:Landroid/widget/SearchView;

    #@44
    invoke-static {v2, v3}, Landroid/widget/SearchView;->access$2100(Landroid/widget/SearchView;Z)V

    #@47
    goto :goto_21

    #@48
    .line 1767
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_48
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@4b
    move-result v1

    #@4c
    goto :goto_21
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 1713
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onWindowFocusChanged(Z)V

    #@3
    .line 1715
    if-eqz p1, :cond_31

    #@5
    iget-object v1, p0, Landroid/widget/SearchView$SearchAutoComplete;->mSearchView:Landroid/widget/SearchView;

    #@7
    invoke-virtual {v1}, Landroid/widget/SearchView;->hasFocus()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_31

    #@d
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getVisibility()I

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_31

    #@13
    .line 1716
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getContext()Landroid/content/Context;

    #@16
    move-result-object v1

    #@17
    const-string v2, "input_method"

    #@19
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@1f
    .line 1718
    .local v0, inputManager:Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x0

    #@20
    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@23
    .line 1721
    invoke-virtual {p0}, Landroid/widget/SearchView$SearchAutoComplete;->getContext()Landroid/content/Context;

    #@26
    move-result-object v1

    #@27
    invoke-static {v1}, Landroid/widget/SearchView;->isLandscapeMode(Landroid/content/Context;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_31

    #@2d
    .line 1722
    const/4 v1, 0x1

    #@2e
    invoke-virtual {p0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->ensureImeVisible(Z)V

    #@31
    .line 1725
    .end local v0           #inputManager:Landroid/view/inputmethod/InputMethodManager;
    :cond_31
    return-void
.end method

.method public performCompletion()V
    .registers 1

    #@0
    .prologue
    .line 1705
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 1695
    return-void
.end method

.method setSearchView(Landroid/widget/SearchView;)V
    .registers 2
    .parameter "searchView"

    #@0
    .prologue
    .line 1673
    iput-object p1, p0, Landroid/widget/SearchView$SearchAutoComplete;->mSearchView:Landroid/widget/SearchView;

    #@2
    .line 1674
    return-void
.end method

.method public setThreshold(I)V
    .registers 2
    .parameter "threshold"

    #@0
    .prologue
    .line 1678
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    #@3
    .line 1679
    iput p1, p0, Landroid/widget/SearchView$SearchAutoComplete;->mThreshold:I

    #@5
    .line 1680
    return-void
.end method
