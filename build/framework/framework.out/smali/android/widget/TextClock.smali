.class public Landroid/widget/TextClock;
.super Landroid/widget/TextView;
.source "TextClock.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field public static final DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

.field public static final DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;


# instance fields
.field private mAttached:Z

.field private mFormat:Ljava/lang/CharSequence;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mFormat12:Ljava/lang/CharSequence;

.field private mFormat24:Ljava/lang/CharSequence;

.field private final mFormatChangeObserver:Landroid/database/ContentObserver;

.field private mHasSeconds:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private final mTicker:Ljava/lang/Runnable;

.field private mTime:Ljava/util/Calendar;

.field private mTimeZone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 94
    const-string v0, "h:mm aa"

    #@2
    sput-object v0, Landroid/widget/TextClock;->DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

    #@4
    .line 106
    const-string/jumbo v0, "k:mm"

    #@7
    sput-object v0, Landroid/widget/TextClock;->DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 167
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@3
    .line 108
    sget-object v0, Landroid/widget/TextClock;->DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

    #@5
    iput-object v0, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@7
    .line 109
    sget-object v0, Landroid/widget/TextClock;->DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;

    #@9
    iput-object v0, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@b
    .line 121
    new-instance v0, Landroid/widget/TextClock$1;

    #@d
    new-instance v1, Landroid/os/Handler;

    #@f
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@12
    invoke-direct {v0, p0, v1}, Landroid/widget/TextClock$1;-><init>(Landroid/widget/TextClock;Landroid/os/Handler;)V

    #@15
    iput-object v0, p0, Landroid/widget/TextClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@17
    .line 135
    new-instance v0, Landroid/widget/TextClock$2;

    #@19
    invoke-direct {v0, p0}, Landroid/widget/TextClock$2;-><init>(Landroid/widget/TextClock;)V

    #@1c
    iput-object v0, p0, Landroid/widget/TextClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1e
    .line 146
    new-instance v0, Landroid/widget/TextClock$3;

    #@20
    invoke-direct {v0, p0}, Landroid/widget/TextClock$3;-><init>(Landroid/widget/TextClock;)V

    #@23
    iput-object v0, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@25
    .line 168
    invoke-direct {p0}, Landroid/widget/TextClock;->init()V

    #@28
    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 184
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/TextClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 200
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 108
    sget-object v2, Landroid/widget/TextClock;->DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

    #@6
    iput-object v2, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@8
    .line 109
    sget-object v2, Landroid/widget/TextClock;->DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;

    #@a
    iput-object v2, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@c
    .line 121
    new-instance v2, Landroid/widget/TextClock$1;

    #@e
    new-instance v3, Landroid/os/Handler;

    #@10
    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    #@13
    invoke-direct {v2, p0, v3}, Landroid/widget/TextClock$1;-><init>(Landroid/widget/TextClock;Landroid/os/Handler;)V

    #@16
    iput-object v2, p0, Landroid/widget/TextClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@18
    .line 135
    new-instance v2, Landroid/widget/TextClock$2;

    #@1a
    invoke-direct {v2, p0}, Landroid/widget/TextClock$2;-><init>(Landroid/widget/TextClock;)V

    #@1d
    iput-object v2, p0, Landroid/widget/TextClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1f
    .line 146
    new-instance v2, Landroid/widget/TextClock$3;

    #@21
    invoke-direct {v2, p0}, Landroid/widget/TextClock$3;-><init>(Landroid/widget/TextClock;)V

    #@24
    iput-object v2, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@26
    .line 202
    sget-object v2, Lcom/android/internal/R$styleable;->TextClock:[I

    #@28
    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@2b
    move-result-object v0

    #@2c
    .line 206
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@30
    move-result-object v1

    #@31
    .line 207
    .local v1, format:Ljava/lang/CharSequence;
    if-nez v1, :cond_35

    #@33
    sget-object v1, Landroid/widget/TextClock;->DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

    #@35
    .end local v1           #format:Ljava/lang/CharSequence;
    :cond_35
    iput-object v1, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@37
    .line 209
    const/4 v2, 0x1

    #@38
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@3b
    move-result-object v1

    #@3c
    .line 210
    .restart local v1       #format:Ljava/lang/CharSequence;
    if-nez v1, :cond_40

    #@3e
    sget-object v1, Landroid/widget/TextClock;->DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;

    #@40
    .end local v1           #format:Ljava/lang/CharSequence;
    :cond_40
    iput-object v1, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@42
    .line 212
    const/4 v2, 0x2

    #@43
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    iput-object v2, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_2d .. :try_end_49} :catchall_50

    #@49
    .line 214
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@4c
    .line 217
    invoke-direct {p0}, Landroid/widget/TextClock;->init()V

    #@4f
    .line 218
    return-void

    #@50
    .line 214
    :catchall_50
    move-exception v2

    #@51
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@54
    throw v2
.end method

.method private static abc(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 417
    if-nez p0, :cond_7

    #@2
    if-nez p1, :cond_5

    #@4
    .end local p2
    :goto_4
    return-object p2

    #@5
    .restart local p2
    :cond_5
    move-object p2, p1

    #@6
    goto :goto_4

    #@7
    :cond_7
    move-object p2, p0

    #@8
    goto :goto_4
.end method

.method static synthetic access$000(Landroid/widget/TextClock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Landroid/widget/TextClock;->chooseFormat()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/TextClock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Landroid/widget/TextClock;->onTimeChanged()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/TextClock;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/TextClock;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/widget/TextClock;->createTime(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/widget/TextClock;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method private chooseFormat()V
    .registers 2

    #@0
    .prologue
    .line 385
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/widget/TextClock;->chooseFormat(Z)V

    #@4
    .line 386
    return-void
.end method

.method private chooseFormat(Z)V
    .registers 7
    .parameter "handleTicker"

    #@0
    .prologue
    .line 396
    invoke-virtual {p0}, Landroid/widget/TextClock;->is24HourModeEnabled()Z

    #@3
    move-result v0

    #@4
    .line 398
    .local v0, format24Requested:Z
    if-eqz v0, :cond_32

    #@6
    .line 399
    iget-object v2, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@8
    iget-object v3, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@a
    sget-object v4, Landroid/widget/TextClock;->DEFAULT_FORMAT_24_HOUR:Ljava/lang/CharSequence;

    #@c
    invoke-static {v2, v3, v4}, Landroid/widget/TextClock;->abc(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@f
    move-result-object v2

    #@10
    iput-object v2, p0, Landroid/widget/TextClock;->mFormat:Ljava/lang/CharSequence;

    #@12
    .line 404
    :goto_12
    iget-boolean v1, p0, Landroid/widget/TextClock;->mHasSeconds:Z

    #@14
    .line 405
    .local v1, hadSeconds:Z
    iget-object v2, p0, Landroid/widget/TextClock;->mFormat:Ljava/lang/CharSequence;

    #@16
    invoke-static {v2}, Landroid/text/format/DateFormat;->hasSeconds(Ljava/lang/CharSequence;)Z

    #@19
    move-result v2

    #@1a
    iput-boolean v2, p0, Landroid/widget/TextClock;->mHasSeconds:Z

    #@1c
    .line 407
    if-eqz p1, :cond_31

    #@1e
    iget-boolean v2, p0, Landroid/widget/TextClock;->mAttached:Z

    #@20
    if-eqz v2, :cond_31

    #@22
    iget-boolean v2, p0, Landroid/widget/TextClock;->mHasSeconds:Z

    #@24
    if-eq v1, v2, :cond_31

    #@26
    .line 408
    if-eqz v1, :cond_3f

    #@28
    invoke-virtual {p0}, Landroid/widget/TextClock;->getHandler()Landroid/os/Handler;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@2e
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@31
    .line 411
    :cond_31
    :goto_31
    return-void

    #@32
    .line 401
    .end local v1           #hadSeconds:Z
    :cond_32
    iget-object v2, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@34
    iget-object v3, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@36
    sget-object v4, Landroid/widget/TextClock;->DEFAULT_FORMAT_12_HOUR:Ljava/lang/CharSequence;

    #@38
    invoke-static {v2, v3, v4}, Landroid/widget/TextClock;->abc(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@3b
    move-result-object v2

    #@3c
    iput-object v2, p0, Landroid/widget/TextClock;->mFormat:Ljava/lang/CharSequence;

    #@3e
    goto :goto_12

    #@3f
    .line 409
    .restart local v1       #hadSeconds:Z
    :cond_3f
    iget-object v2, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@41
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    #@44
    goto :goto_31
.end method

.method private createTime(Ljava/lang/String;)V
    .registers 3
    .parameter "timeZone"

    #@0
    .prologue
    .line 227
    if-eqz p1, :cond_d

    #@2
    .line 228
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/widget/TextClock;->mTime:Ljava/util/Calendar;

    #@c
    .line 232
    :goto_c
    return-void

    #@d
    .line 230
    :cond_d
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/widget/TextClock;->mTime:Ljava/util/Calendar;

    #@13
    goto :goto_c
.end method

.method private init()V
    .registers 2

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0}, Landroid/widget/TextClock;->createTime(Ljava/lang/String;)V

    #@5
    .line 223
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0}, Landroid/widget/TextClock;->chooseFormat(Z)V

    #@9
    .line 224
    return-void
.end method

.method private onTimeChanged()V
    .registers 4

    #@0
    .prologue
    .line 479
    iget-object v0, p0, Landroid/widget/TextClock;->mTime:Ljava/util/Calendar;

    #@2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v1

    #@6
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@9
    .line 480
    iget-object v0, p0, Landroid/widget/TextClock;->mFormat:Ljava/lang/CharSequence;

    #@b
    iget-object v1, p0, Landroid/widget/TextClock;->mTime:Ljava/util/Calendar;

    #@d
    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p0, v0}, Landroid/widget/TextClock;->setText(Ljava/lang/CharSequence;)V

    #@14
    .line 481
    return-void
.end method

.method private registerObserver()V
    .registers 5

    #@0
    .prologue
    .line 465
    invoke-virtual {p0}, Landroid/widget/TextClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    .line 466
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@a
    const/4 v2, 0x1

    #@b
    iget-object v3, p0, Landroid/widget/TextClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@10
    .line 467
    return-void
.end method

.method private registerReceiver()V
    .registers 6

    #@0
    .prologue
    .line 455
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 457
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 458
    const-string v1, "android.intent.action.TIME_SET"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 459
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 461
    invoke-virtual {p0}, Landroid/widget/TextClock;->getContext()Landroid/content/Context;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/widget/TextClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    const/4 v3, 0x0

    #@1b
    invoke-virtual {p0}, Landroid/widget/TextClock;->getHandler()Landroid/os/Handler;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@22
    .line 462
    return-void
.end method

.method private unregisterObserver()V
    .registers 3

    #@0
    .prologue
    .line 474
    invoke-virtual {p0}, Landroid/widget/TextClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    .line 475
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v1, p0, Landroid/widget/TextClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@a
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@d
    .line 476
    return-void
.end method

.method private unregisterReceiver()V
    .registers 3

    #@0
    .prologue
    .line 470
    invoke-virtual {p0}, Landroid/widget/TextClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/widget/TextClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@9
    .line 471
    return-void
.end method


# virtual methods
.method public getFormat12Hour()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getFormat24Hour()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 352
    iget-object v0, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public is24HourModeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 338
    invoke-virtual {p0}, Landroid/widget/TextClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 422
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    #@3
    .line 424
    iget-boolean v0, p0, Landroid/widget/TextClock;->mAttached:Z

    #@5
    if-nez v0, :cond_1e

    #@7
    .line 425
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/widget/TextClock;->mAttached:Z

    #@a
    .line 427
    invoke-direct {p0}, Landroid/widget/TextClock;->registerReceiver()V

    #@d
    .line 428
    invoke-direct {p0}, Landroid/widget/TextClock;->registerObserver()V

    #@10
    .line 430
    iget-object v0, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;

    #@12
    invoke-direct {p0, v0}, Landroid/widget/TextClock;->createTime(Ljava/lang/String;)V

    #@15
    .line 432
    iget-boolean v0, p0, Landroid/widget/TextClock;->mHasSeconds:Z

    #@17
    if-eqz v0, :cond_1f

    #@19
    .line 433
    iget-object v0, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@1b
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@1e
    .line 438
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 435
    :cond_1f
    invoke-direct {p0}, Landroid/widget/TextClock;->onTimeChanged()V

    #@22
    goto :goto_1e
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 442
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    #@3
    .line 444
    iget-boolean v0, p0, Landroid/widget/TextClock;->mAttached:Z

    #@5
    if-eqz v0, :cond_19

    #@7
    .line 445
    invoke-direct {p0}, Landroid/widget/TextClock;->unregisterReceiver()V

    #@a
    .line 446
    invoke-direct {p0}, Landroid/widget/TextClock;->unregisterObserver()V

    #@d
    .line 448
    invoke-virtual {p0}, Landroid/widget/TextClock;->getHandler()Landroid/os/Handler;

    #@10
    move-result-object v0

    #@11
    iget-object v1, p0, Landroid/widget/TextClock;->mTicker:Ljava/lang/Runnable;

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@16
    .line 450
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Landroid/widget/TextClock;->mAttached:Z

    #@19
    .line 452
    :cond_19
    return-void
.end method

.method public setFormat12Hour(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "format"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 270
    iput-object p1, p0, Landroid/widget/TextClock;->mFormat12:Ljava/lang/CharSequence;

    #@2
    .line 272
    invoke-direct {p0}, Landroid/widget/TextClock;->chooseFormat()V

    #@5
    .line 273
    invoke-direct {p0}, Landroid/widget/TextClock;->onTimeChanged()V

    #@8
    .line 274
    return-void
.end method

.method public setFormat24Hour(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "format"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 312
    iput-object p1, p0, Landroid/widget/TextClock;->mFormat24:Ljava/lang/CharSequence;

    #@2
    .line 314
    invoke-direct {p0}, Landroid/widget/TextClock;->chooseFormat()V

    #@5
    .line 315
    invoke-direct {p0}, Landroid/widget/TextClock;->onTimeChanged()V

    #@8
    .line 316
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .registers 2
    .parameter "timeZone"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 372
    iput-object p1, p0, Landroid/widget/TextClock;->mTimeZone:Ljava/lang/String;

    #@2
    .line 374
    invoke-direct {p0, p1}, Landroid/widget/TextClock;->createTime(Ljava/lang/String;)V

    #@5
    .line 375
    invoke-direct {p0}, Landroid/widget/TextClock;->onTimeChanged()V

    #@8
    .line 376
    return-void
.end method
