.class public Landroid/widget/LinearLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "LinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/LinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public gravity:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x1
                to = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x30
                to = "TOP"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x50
                to = "BOTTOM"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "LEFT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "RIGHT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x800003
                to = "START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x800005
                to = "END"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x10
                to = "CENTER_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x70
                to = "FILL_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "CENTER_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7
                to = "FILL_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x11
                to = "CENTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x77
                to = "FILL"
            .end subannotation
        }
    .end annotation
.end field

.field public weight:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1854
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@3
    .line 1813
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@6
    .line 1855
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@9
    .line 1856
    return-void
.end method

.method public constructor <init>(IIF)V
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "weight"

    #@0
    .prologue
    .line 1869
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@3
    .line 1813
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@6
    .line 1870
    iput p3, p0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@8
    .line 1871
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const/4 v2, -0x1

    #@2
    .line 1835
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 1813
    iput v2, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@7
    .line 1836
    sget-object v1, Lcom/android/internal/R$styleable;->LinearLayout_Layout:[I

    #@9
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 1839
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@11
    move-result v1

    #@12
    iput v1, p0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@14
    .line 1840
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@18
    move-result v1

    #@19
    iput v1, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@1b
    .line 1841
    invoke-static {}, Landroid/widget/LinearLayout;->access$000()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_35

    #@21
    .line 1842
    iget v1, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@23
    and-int/lit8 v1, v1, 0x3

    #@25
    if-eq v1, v3, :cond_2e

    #@27
    iget v1, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@29
    and-int/lit8 v1, v1, 0x5

    #@2b
    const/4 v2, 0x5

    #@2c
    if-ne v1, v2, :cond_35

    #@2e
    .line 1843
    :cond_2e
    iget v1, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@30
    const/high16 v2, 0x80

    #@32
    or-int/2addr v1, v2

    #@33
    iput v1, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@35
    .line 1847
    :cond_35
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@38
    .line 1848
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1877
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 1813
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@6
    .line 1878
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 1884
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 1813
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@6
    .line 1885
    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "output"

    #@0
    .prologue
    .line 1889
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "LinearLayout.LayoutParams={width="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@11
    invoke-static {v1}, Landroid/widget/LinearLayout$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", height="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@21
    invoke-static {v1}, Landroid/widget/LinearLayout$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " weight="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string/jumbo v1, "}"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    return-object v0
.end method
