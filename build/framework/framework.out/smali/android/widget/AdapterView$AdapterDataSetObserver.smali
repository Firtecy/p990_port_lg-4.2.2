.class Landroid/widget/AdapterView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "AdapterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field private mInstanceState:Landroid/os/Parcelable;

.field final synthetic this$0:Landroid/widget/AdapterView;


# direct methods
.method constructor <init>(Landroid/widget/AdapterView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 793
    .local p0, this:Landroid/widget/AdapterView$AdapterDataSetObserver;,"Landroid/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    iput-object p1, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@2
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    #@5
    .line 795
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@8
    return-void
.end method


# virtual methods
.method public clearSavedState()V
    .registers 2

    #@0
    .prologue
    .line 840
    .local p0, this:Landroid/widget/AdapterView$AdapterDataSetObserver;,"Landroid/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@3
    .line 841
    return-void
.end method

.method public onChanged()V
    .registers 3

    #@0
    .prologue
    .line 799
    .local p0, this:Landroid/widget/AdapterView$AdapterDataSetObserver;,"Landroid/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@2
    const/4 v1, 0x1

    #@3
    iput-boolean v1, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@5
    .line 800
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@7
    iget-object v1, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@9
    iget v1, v1, Landroid/widget/AdapterView;->mItemCount:I

    #@b
    iput v1, v0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@d
    .line 801
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@f
    iget-object v1, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@11
    invoke-virtual {v1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    #@18
    move-result v1

    #@19
    iput v1, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@1b
    .line 805
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@1d
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@20
    move-result-object v0

    #@21
    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_4c

    #@27
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@29
    if-eqz v0, :cond_4c

    #@2b
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@2d
    iget v0, v0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@2f
    if-nez v0, :cond_4c

    #@31
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@33
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@35
    if-lez v0, :cond_4c

    #@37
    .line 807
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@39
    iget-object v1, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@3b
    invoke-static {v0, v1}, Landroid/widget/AdapterView;->access$000(Landroid/widget/AdapterView;Landroid/os/Parcelable;)V

    #@3e
    .line 808
    const/4 v0, 0x0

    #@3f
    iput-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@41
    .line 812
    :goto_41
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@43
    invoke-virtual {v0}, Landroid/widget/AdapterView;->checkFocus()V

    #@46
    .line 813
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@48
    invoke-virtual {v0}, Landroid/widget/AdapterView;->requestLayout()V

    #@4b
    .line 814
    return-void

    #@4c
    .line 810
    :cond_4c
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@4e
    invoke-virtual {v0}, Landroid/widget/AdapterView;->rememberSyncState()V

    #@51
    goto :goto_41
.end method

.method public onInvalidated()V
    .registers 7

    #@0
    .prologue
    .local p0, this:Landroid/widget/AdapterView$AdapterDataSetObserver;,"Landroid/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    const-wide/high16 v4, -0x8000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, -0x1

    #@4
    .line 818
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@6
    const/4 v1, 0x1

    #@7
    iput-boolean v1, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@9
    .line 820
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@b
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@e
    move-result-object v0

    #@f
    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1d

    #@15
    .line 823
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@17
    invoke-static {v0}, Landroid/widget/AdapterView;->access$100(Landroid/widget/AdapterView;)Landroid/os/Parcelable;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    #@1d
    .line 827
    :cond_1d
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@1f
    iget-object v1, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@21
    iget v1, v1, Landroid/widget/AdapterView;->mItemCount:I

    #@23
    iput v1, v0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@25
    .line 828
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@27
    iput v3, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@29
    .line 829
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@2b
    iput v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2d
    .line 830
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@2f
    iput-wide v4, v0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@31
    .line 831
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@33
    iput v2, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@35
    .line 832
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@37
    iput-wide v4, v0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@39
    .line 833
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@3b
    iput-boolean v3, v0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@3d
    .line 835
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@3f
    invoke-virtual {v0}, Landroid/widget/AdapterView;->checkFocus()V

    #@42
    .line 836
    iget-object v0, p0, Landroid/widget/AdapterView$AdapterDataSetObserver;->this$0:Landroid/widget/AdapterView;

    #@44
    invoke-virtual {v0}, Landroid/widget/AdapterView;->requestLayout()V

    #@47
    .line 837
    return-void
.end method
