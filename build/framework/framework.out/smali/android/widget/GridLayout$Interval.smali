.class final Landroid/widget/GridLayout$Interval;
.super Ljava/lang/Object;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/GridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Interval"
.end annotation


# instance fields
.field public final max:I

.field public final min:I


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 2266
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2267
    iput p1, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@5
    .line 2268
    iput p2, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@7
    .line 2269
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "that"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2291
    if-ne p0, p1, :cond_5

    #@4
    .line 2308
    :cond_4
    :goto_4
    return v1

    #@5
    .line 2294
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    .line 2295
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 2298
    check-cast v0, Landroid/widget/GridLayout$Interval;

    #@16
    .line 2300
    .local v0, interval:Landroid/widget/GridLayout$Interval;
    iget v3, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@18
    iget v4, v0, Landroid/widget/GridLayout$Interval;->max:I

    #@1a
    if-eq v3, v4, :cond_1e

    #@1c
    move v1, v2

    #@1d
    .line 2301
    goto :goto_4

    #@1e
    .line 2304
    :cond_1e
    iget v3, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@20
    iget v4, v0, Landroid/widget/GridLayout$Interval;->min:I

    #@22
    if-eq v3, v4, :cond_4

    #@24
    move v1, v2

    #@25
    .line 2305
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 2313
    iget v0, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@2
    .line 2314
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@4
    iget v2, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@6
    add-int v0, v1, v2

    #@8
    .line 2315
    return v0
.end method

.method inverse()Landroid/widget/GridLayout$Interval;
    .registers 4

    #@0
    .prologue
    .line 2276
    new-instance v0, Landroid/widget/GridLayout$Interval;

    #@2
    iget v1, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@4
    iget v2, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@6
    invoke-direct {v0, v1, v2}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@9
    return-object v0
.end method

.method size()I
    .registers 3

    #@0
    .prologue
    .line 2272
    iget v0, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@2
    iget v1, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2320
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/widget/GridLayout$Interval;->max:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "]"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
