.class public Landroid/widget/GridLayout;
.super Landroid/view/ViewGroup;
.source "GridLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/GridLayout$Alignment;,
        Landroid/widget/GridLayout$Spec;,
        Landroid/widget/GridLayout$Interval;,
        Landroid/widget/GridLayout$Bounds;,
        Landroid/widget/GridLayout$PackedMap;,
        Landroid/widget/GridLayout$Assoc;,
        Landroid/widget/GridLayout$MutableInt;,
        Landroid/widget/GridLayout$Arc;,
        Landroid/widget/GridLayout$LayoutParams;,
        Landroid/widget/GridLayout$Axis;
    }
.end annotation


# static fields
.field private static final ALIGNMENT_MODE:I = 0x6

.field public static final ALIGN_BOUNDS:I = 0x0

.field public static final ALIGN_MARGINS:I = 0x1

.field public static final BASELINE:Landroid/widget/GridLayout$Alignment; = null

.field public static final BOTTOM:Landroid/widget/GridLayout$Alignment; = null

.field private static final CAN_STRETCH:I = 0x2

.field public static final CENTER:Landroid/widget/GridLayout$Alignment; = null

.field private static final COLUMN_COUNT:I = 0x3

.field private static final COLUMN_ORDER_PRESERVED:I = 0x4

.field private static final DEFAULT_ALIGNMENT_MODE:I = 0x1

.field static final DEFAULT_CONTAINER_MARGIN:I = 0x0

.field private static final DEFAULT_COUNT:I = -0x80000000

.field private static final DEFAULT_ORDER_PRESERVED:Z = true

.field private static final DEFAULT_ORIENTATION:I = 0x0

.field private static final DEFAULT_USE_DEFAULT_MARGINS:Z = false

.field public static final END:Landroid/widget/GridLayout$Alignment; = null

.field public static final FILL:Landroid/widget/GridLayout$Alignment; = null

.field public static final HORIZONTAL:I = 0x0

.field private static final INFLEXIBLE:I = 0x0

.field private static final LEADING:Landroid/widget/GridLayout$Alignment; = null

.field public static final LEFT:Landroid/widget/GridLayout$Alignment; = null

.field static final MAX_SIZE:I = 0x186a0

.field private static final ORIENTATION:I = 0x0

.field public static final RIGHT:Landroid/widget/GridLayout$Alignment; = null

.field private static final ROW_COUNT:I = 0x1

.field private static final ROW_ORDER_PRESERVED:I = 0x2

.field public static final START:Landroid/widget/GridLayout$Alignment; = null

.field static final TAG:Ljava/lang/String; = null

.field public static final TOP:Landroid/widget/GridLayout$Alignment; = null

.field private static final TRAILING:Landroid/widget/GridLayout$Alignment; = null

.field public static final UNDEFINED:I = -0x80000000

.field static final UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment; = null

.field static final UNINITIALIZED_HASH:I = 0x0

.field private static final USE_DEFAULT_MARGINS:I = 0x5

.field public static final VERTICAL:I = 0x1


# instance fields
.field alignmentMode:I

.field defaultGap:I

.field final horizontalAxis:Landroid/widget/GridLayout$Axis;

.field lastLayoutParamsHashCode:I

.field orientation:I

.field useDefaultMargins:Z

.field final verticalAxis:Landroid/widget/GridLayout$Axis;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 211
    const-class v0, Landroid/widget/GridLayout;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/GridLayout;->TAG:Ljava/lang/String;

    #@8
    .line 2530
    new-instance v0, Landroid/widget/GridLayout$1;

    #@a
    invoke-direct {v0}, Landroid/widget/GridLayout$1;-><init>()V

    #@d
    sput-object v0, Landroid/widget/GridLayout;->UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment;

    #@f
    .line 2546
    new-instance v0, Landroid/widget/GridLayout$2;

    #@11
    invoke-direct {v0}, Landroid/widget/GridLayout$2;-><init>()V

    #@14
    sput-object v0, Landroid/widget/GridLayout;->LEADING:Landroid/widget/GridLayout$Alignment;

    #@16
    .line 2562
    new-instance v0, Landroid/widget/GridLayout$3;

    #@18
    invoke-direct {v0}, Landroid/widget/GridLayout$3;-><init>()V

    #@1b
    sput-object v0, Landroid/widget/GridLayout;->TRAILING:Landroid/widget/GridLayout$Alignment;

    #@1d
    .line 2578
    sget-object v0, Landroid/widget/GridLayout;->LEADING:Landroid/widget/GridLayout$Alignment;

    #@1f
    sput-object v0, Landroid/widget/GridLayout;->TOP:Landroid/widget/GridLayout$Alignment;

    #@21
    .line 2584
    sget-object v0, Landroid/widget/GridLayout;->TRAILING:Landroid/widget/GridLayout$Alignment;

    #@23
    sput-object v0, Landroid/widget/GridLayout;->BOTTOM:Landroid/widget/GridLayout$Alignment;

    #@25
    .line 2590
    sget-object v0, Landroid/widget/GridLayout;->LEADING:Landroid/widget/GridLayout$Alignment;

    #@27
    sput-object v0, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    #@29
    .line 2596
    sget-object v0, Landroid/widget/GridLayout;->TRAILING:Landroid/widget/GridLayout$Alignment;

    #@2b
    sput-object v0, Landroid/widget/GridLayout;->END:Landroid/widget/GridLayout$Alignment;

    #@2d
    .line 2616
    sget-object v0, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    #@2f
    sget-object v1, Landroid/widget/GridLayout;->END:Landroid/widget/GridLayout$Alignment;

    #@31
    invoke-static {v0, v1}, Landroid/widget/GridLayout;->createSwitchingAlignment(Landroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Alignment;

    #@34
    move-result-object v0

    #@35
    sput-object v0, Landroid/widget/GridLayout;->LEFT:Landroid/widget/GridLayout$Alignment;

    #@37
    .line 2622
    sget-object v0, Landroid/widget/GridLayout;->END:Landroid/widget/GridLayout$Alignment;

    #@39
    sget-object v1, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    #@3b
    invoke-static {v0, v1}, Landroid/widget/GridLayout;->createSwitchingAlignment(Landroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Alignment;

    #@3e
    move-result-object v0

    #@3f
    sput-object v0, Landroid/widget/GridLayout;->RIGHT:Landroid/widget/GridLayout$Alignment;

    #@41
    .line 2629
    new-instance v0, Landroid/widget/GridLayout$5;

    #@43
    invoke-direct {v0}, Landroid/widget/GridLayout$5;-><init>()V

    #@46
    sput-object v0, Landroid/widget/GridLayout;->CENTER:Landroid/widget/GridLayout$Alignment;

    #@48
    .line 2648
    new-instance v0, Landroid/widget/GridLayout$6;

    #@4a
    invoke-direct {v0}, Landroid/widget/GridLayout$6;-><init>()V

    #@4d
    sput-object v0, Landroid/widget/GridLayout;->BASELINE:Landroid/widget/GridLayout$Alignment;

    #@4f
    .line 2708
    new-instance v0, Landroid/widget/GridLayout$7;

    #@51
    invoke-direct {v0}, Landroid/widget/GridLayout$7;-><init>()V

    #@54
    sput-object v0, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    #@56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 279
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 270
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 250
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 236
    new-instance v1, Landroid/widget/GridLayout$Axis;

    #@8
    invoke-direct {v1, p0, v3, v4}, Landroid/widget/GridLayout$Axis;-><init>(Landroid/widget/GridLayout;ZLandroid/widget/GridLayout$1;)V

    #@b
    iput-object v1, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@d
    .line 237
    new-instance v1, Landroid/widget/GridLayout$Axis;

    #@f
    invoke-direct {v1, p0, v2, v4}, Landroid/widget/GridLayout$Axis;-><init>(Landroid/widget/GridLayout;ZLandroid/widget/GridLayout$1;)V

    #@12
    iput-object v1, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@14
    .line 238
    iput v2, p0, Landroid/widget/GridLayout;->orientation:I

    #@16
    .line 239
    iput-boolean v2, p0, Landroid/widget/GridLayout;->useDefaultMargins:Z

    #@18
    .line 240
    iput v3, p0, Landroid/widget/GridLayout;->alignmentMode:I

    #@1a
    .line 242
    iput v2, p0, Landroid/widget/GridLayout;->lastLayoutParamsHashCode:I

    #@1c
    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v1

    #@20
    const v2, 0x105004a

    #@23
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    #@26
    move-result v1

    #@27
    iput v1, p0, Landroid/widget/GridLayout;->defaultGap:I

    #@29
    .line 252
    sget-object v1, Lcom/android/internal/R$styleable;->GridLayout:[I

    #@2b
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@2e
    move-result-object v0

    #@2f
    .line 254
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    #@30
    const/high16 v2, -0x8000

    #@32
    :try_start_32
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@35
    move-result v1

    #@36
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setRowCount(I)V

    #@39
    .line 255
    const/4 v1, 0x3

    #@3a
    const/high16 v2, -0x8000

    #@3c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3f
    move-result v1

    #@40
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setColumnCount(I)V

    #@43
    .line 256
    const/4 v1, 0x0

    #@44
    const/4 v2, 0x0

    #@45
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@48
    move-result v1

    #@49
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setOrientation(I)V

    #@4c
    .line 257
    const/4 v1, 0x5

    #@4d
    const/4 v2, 0x0

    #@4e
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@51
    move-result v1

    #@52
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setUseDefaultMargins(Z)V

    #@55
    .line 258
    const/4 v1, 0x6

    #@56
    const/4 v2, 0x1

    #@57
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@5a
    move-result v1

    #@5b
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setAlignmentMode(I)V

    #@5e
    .line 259
    const/4 v1, 0x2

    #@5f
    const/4 v2, 0x1

    #@60
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@63
    move-result v1

    #@64
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setRowOrderPreserved(Z)V

    #@67
    .line 260
    const/4 v1, 0x4

    #@68
    const/4 v2, 0x1

    #@69
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6c
    move-result v1

    #@6d
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->setColumnOrderPreserved(Z)V
    :try_end_70
    .catchall {:try_start_32 .. :try_end_70} :catchall_74

    #@70
    .line 262
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@73
    .line 264
    return-void

    #@74
    .line 262
    :catchall_74
    move-exception v1

    #@75
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@78
    throw v1
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 151
    invoke-static {p0}, Landroid/widget/GridLayout;->handleInvalidParams(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static append([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;[TT;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, a:[Ljava/lang/Object;,"[TT;"
    .local p1, b:[Ljava/lang/Object;,"[TT;"
    const/4 v4, 0x0

    #@1
    .line 571
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    #@8
    move-result-object v1

    #@9
    array-length v2, p0

    #@a
    array-length v3, p1

    #@b
    add-int/2addr v2, v3

    #@c
    invoke-static {v1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, [Ljava/lang/Object;

    #@12
    move-object v0, v1

    #@13
    check-cast v0, [Ljava/lang/Object;

    #@15
    .line 572
    .local v0, result:[Ljava/lang/Object;,"[TT;"
    array-length v1, p0

    #@16
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 573
    array-length v1, p0

    #@1a
    array-length v2, p1

    #@1b
    invoke-static {p1, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1e
    .line 574
    return-object v0
.end method

.method static canStretch(I)Z
    .registers 2
    .parameter "flexibility"

    #@0
    .prologue
    .line 2726
    and-int/lit8 v0, p0, 0x2

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private checkLayoutParams(Landroid/widget/GridLayout$LayoutParams;Z)V
    .registers 11
    .parameter "lp"
    .parameter "horizontal"

    #@0
    .prologue
    const/high16 v7, -0x8000

    #@2
    .line 777
    if-eqz p2, :cond_7d

    #@4
    const-string v2, "column"

    #@6
    .line 778
    .local v2, groupName:Ljava/lang/String;
    :goto_6
    if-eqz p2, :cond_81

    #@8
    iget-object v4, p1, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@a
    .line 779
    .local v4, spec:Landroid/widget/GridLayout$Spec;
    :goto_a
    iget-object v3, v4, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@c
    .line 780
    .local v3, span:Landroid/widget/GridLayout$Interval;
    iget v5, v3, Landroid/widget/GridLayout$Interval;->min:I

    #@e
    if-eq v5, v7, :cond_2a

    #@10
    iget v5, v3, Landroid/widget/GridLayout$Interval;->min:I

    #@12
    if-gez v5, :cond_2a

    #@14
    .line 781
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, " indices must be positive"

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v5}, Landroid/widget/GridLayout;->handleInvalidParams(Ljava/lang/String;)V

    #@2a
    .line 783
    :cond_2a
    if-eqz p2, :cond_84

    #@2c
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2e
    .line 784
    .local v0, axis:Landroid/widget/GridLayout$Axis;
    :goto_2e
    iget v1, v0, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@30
    .line 785
    .local v1, count:I
    if-eq v1, v7, :cond_7c

    #@32
    .line 786
    iget v5, v3, Landroid/widget/GridLayout$Interval;->max:I

    #@34
    if-le v5, v1, :cond_56

    #@36
    .line 787
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    const-string v6, " indices (start + span) mustn\'t exceed the "

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    const-string v6, " count"

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v5}, Landroid/widget/GridLayout;->handleInvalidParams(Ljava/lang/String;)V

    #@56
    .line 790
    :cond_56
    invoke-virtual {v3}, Landroid/widget/GridLayout$Interval;->size()I

    #@59
    move-result v5

    #@5a
    if-le v5, v1, :cond_7c

    #@5c
    .line 791
    new-instance v5, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    const-string v6, " span mustn\'t exceed the "

    #@67
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    const-string v6, " count"

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-static {v5}, Landroid/widget/GridLayout;->handleInvalidParams(Ljava/lang/String;)V

    #@7c
    .line 794
    :cond_7c
    return-void

    #@7d
    .line 777
    .end local v0           #axis:Landroid/widget/GridLayout$Axis;
    .end local v1           #count:I
    .end local v2           #groupName:Ljava/lang/String;
    .end local v3           #span:Landroid/widget/GridLayout$Interval;
    .end local v4           #spec:Landroid/widget/GridLayout$Spec;
    :cond_7d
    const-string/jumbo v2, "row"

    #@80
    goto :goto_6

    #@81
    .line 778
    .restart local v2       #groupName:Ljava/lang/String;
    :cond_81
    iget-object v4, p1, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@83
    goto :goto_a

    #@84
    .line 783
    .restart local v3       #span:Landroid/widget/GridLayout$Interval;
    .restart local v4       #spec:Landroid/widget/GridLayout$Spec;
    :cond_84
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@86
    goto :goto_2e
.end method

.method private static clip(Landroid/widget/GridLayout$Interval;ZI)I
    .registers 6
    .parameter "minorRange"
    .parameter "minorWasDefined"
    .parameter "count"

    #@0
    .prologue
    .line 673
    invoke-virtual {p0}, Landroid/widget/GridLayout$Interval;->size()I

    #@3
    move-result v1

    #@4
    .line 674
    .local v1, size:I
    if-nez p2, :cond_7

    #@6
    .line 678
    .end local v1           #size:I
    :goto_6
    return v1

    #@7
    .line 677
    .restart local v1       #size:I
    :cond_7
    if-eqz p1, :cond_16

    #@9
    iget v2, p0, Landroid/widget/GridLayout$Interval;->min:I

    #@b
    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    #@e
    move-result v0

    #@f
    .line 678
    .local v0, min:I
    :goto_f
    sub-int v2, p2, v0

    #@11
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@14
    move-result v1

    #@15
    goto :goto_6

    #@16
    .line 677
    .end local v0           #min:I
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_f
.end method

.method private computeLayoutParamsHashCode()I
    .registers 8

    #@0
    .prologue
    .line 922
    const/4 v4, 0x1

    #@1
    .line 923
    .local v4, result:I
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getChildCount()I

    #@5
    move-result v0

    #@6
    .local v0, N:I
    :goto_6
    if-ge v2, v0, :cond_26

    #@8
    .line 924
    invoke-virtual {p0, v2}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    .line 925
    .local v1, c:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@f
    move-result v5

    #@10
    const/16 v6, 0x8

    #@12
    if-ne v5, v6, :cond_17

    #@14
    .line 923
    :goto_14
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_6

    #@17
    .line 926
    :cond_17
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Landroid/widget/GridLayout$LayoutParams;

    #@1d
    .line 927
    .local v3, lp:Landroid/widget/GridLayout$LayoutParams;
    mul-int/lit8 v5, v4, 0x1f

    #@1f
    invoke-virtual {v3}, Landroid/widget/GridLayout$LayoutParams;->hashCode()I

    #@22
    move-result v6

    #@23
    add-int v4, v5, v6

    #@25
    goto :goto_14

    #@26
    .line 929
    .end local v1           #c:Landroid/view/View;
    .end local v3           #lp:Landroid/widget/GridLayout$LayoutParams;
    :cond_26
    return v4
.end method

.method private consistencyCheck()V
    .registers 3

    #@0
    .prologue
    .line 933
    iget v0, p0, Landroid/widget/GridLayout;->lastLayoutParamsHashCode:I

    #@2
    if-nez v0, :cond_e

    #@4
    .line 934
    invoke-direct {p0}, Landroid/widget/GridLayout;->validateLayoutParams()V

    #@7
    .line 935
    invoke-direct {p0}, Landroid/widget/GridLayout;->computeLayoutParamsHashCode()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/widget/GridLayout;->lastLayoutParamsHashCode:I

    #@d
    .line 942
    :cond_d
    :goto_d
    return-void

    #@e
    .line 936
    :cond_e
    iget v0, p0, Landroid/widget/GridLayout;->lastLayoutParamsHashCode:I

    #@10
    invoke-direct {p0}, Landroid/widget/GridLayout;->computeLayoutParamsHashCode()I

    #@13
    move-result v1

    #@14
    if-eq v0, v1, :cond_d

    #@16
    .line 937
    sget-object v0, Landroid/widget/GridLayout;->TAG:Ljava/lang/String;

    #@18
    const-string v1, "The fields of some layout parameters were modified in between layout operations. Check the javadoc for GridLayout.LayoutParams#rowSpec."

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 939
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@20
    .line 940
    invoke-direct {p0}, Landroid/widget/GridLayout;->consistencyCheck()V

    #@23
    goto :goto_d
.end method

.method private static createSwitchingAlignment(Landroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Alignment;
    .registers 3
    .parameter "ltr"
    .parameter "rtl"

    #@0
    .prologue
    .line 2599
    new-instance v0, Landroid/widget/GridLayout$4;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/widget/GridLayout$4;-><init>(Landroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$Alignment;)V

    #@5
    return-object v0
.end method

.method private drawLine(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .registers 16
    .parameter "graphics"
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"
    .parameter "paint"

    #@0
    .prologue
    .line 827
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingLeft()I

    #@3
    move-result v6

    #@4
    .line 828
    .local v6, dx:I
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingTop()I

    #@7
    move-result v7

    #@8
    .line 829
    .local v7, dy:I
    invoke-virtual {p0}, Landroid/widget/GridLayout;->isLayoutRtl()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_26

    #@e
    .line 830
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getWidth()I

    #@11
    move-result v8

    #@12
    .line 831
    .local v8, width:I
    sub-int v0, v8, v6

    #@14
    sub-int/2addr v0, p2

    #@15
    int-to-float v1, v0

    #@16
    add-int v0, v7, p3

    #@18
    int-to-float v2, v0

    #@19
    sub-int v0, v8, v6

    #@1b
    sub-int/2addr v0, p4

    #@1c
    int-to-float v3, v0

    #@1d
    add-int v0, v7, p5

    #@1f
    int-to-float v4, v0

    #@20
    move-object v0, p1

    #@21
    move-object v5, p6

    #@22
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@25
    .line 835
    .end local v8           #width:I
    :goto_25
    return-void

    #@26
    .line 833
    :cond_26
    add-int v0, v6, p2

    #@28
    int-to-float v1, v0

    #@29
    add-int v0, v7, p3

    #@2b
    int-to-float v2, v0

    #@2c
    add-int v0, v6, p4

    #@2e
    int-to-float v3, v0

    #@2f
    add-int v0, v7, p5

    #@31
    int-to-float v4, v0

    #@32
    move-object v0, p1

    #@33
    move-object v5, p6

    #@34
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@37
    goto :goto_25
.end method

.method private static fits([IIII)Z
    .registers 7
    .parameter "a"
    .parameter "value"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 650
    array-length v2, p0

    #@2
    if-le p3, v2, :cond_5

    #@4
    .line 658
    :cond_4
    :goto_4
    return v1

    #@5
    .line 653
    :cond_5
    move v0, p2

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, p3, :cond_f

    #@8
    .line 654
    aget v2, p0, v0

    #@a
    if-gt v2, p1, :cond_4

    #@c
    .line 653
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_6

    #@f
    .line 658
    :cond_f
    const/4 v1, 0x1

    #@10
    goto :goto_4
.end method

.method static getAlignment(IZ)Landroid/widget/GridLayout$Alignment;
    .registers 6
    .parameter "gravity"
    .parameter "horizontal"

    #@0
    .prologue
    .line 578
    if-eqz p1, :cond_10

    #@2
    const/4 v1, 0x7

    #@3
    .line 579
    .local v1, mask:I
    :goto_3
    if-eqz p1, :cond_13

    #@5
    const/4 v2, 0x0

    #@6
    .line 580
    .local v2, shift:I
    :goto_6
    and-int v3, p0, v1

    #@8
    shr-int v0, v3, v2

    #@a
    .line 581
    .local v0, flags:I
    sparse-switch v0, :sswitch_data_32

    #@d
    .line 595
    sget-object v3, Landroid/widget/GridLayout;->UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment;

    #@f
    :goto_f
    return-object v3

    #@10
    .line 578
    .end local v0           #flags:I
    .end local v1           #mask:I
    .end local v2           #shift:I
    :cond_10
    const/16 v1, 0x70

    #@12
    goto :goto_3

    #@13
    .line 579
    .restart local v1       #mask:I
    :cond_13
    const/4 v2, 0x4

    #@14
    goto :goto_6

    #@15
    .line 583
    .restart local v0       #flags:I
    .restart local v2       #shift:I
    :sswitch_15
    if-eqz p1, :cond_1a

    #@17
    sget-object v3, Landroid/widget/GridLayout;->LEFT:Landroid/widget/GridLayout$Alignment;

    #@19
    goto :goto_f

    #@1a
    :cond_1a
    sget-object v3, Landroid/widget/GridLayout;->TOP:Landroid/widget/GridLayout$Alignment;

    #@1c
    goto :goto_f

    #@1d
    .line 585
    :sswitch_1d
    if-eqz p1, :cond_22

    #@1f
    sget-object v3, Landroid/widget/GridLayout;->RIGHT:Landroid/widget/GridLayout$Alignment;

    #@21
    goto :goto_f

    #@22
    :cond_22
    sget-object v3, Landroid/widget/GridLayout;->BOTTOM:Landroid/widget/GridLayout$Alignment;

    #@24
    goto :goto_f

    #@25
    .line 587
    :sswitch_25
    sget-object v3, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    #@27
    goto :goto_f

    #@28
    .line 589
    :sswitch_28
    sget-object v3, Landroid/widget/GridLayout;->CENTER:Landroid/widget/GridLayout$Alignment;

    #@2a
    goto :goto_f

    #@2b
    .line 591
    :sswitch_2b
    sget-object v3, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    #@2d
    goto :goto_f

    #@2e
    .line 593
    :sswitch_2e
    sget-object v3, Landroid/widget/GridLayout;->END:Landroid/widget/GridLayout$Alignment;

    #@30
    goto :goto_f

    #@31
    .line 581
    nop

    #@32
    :sswitch_data_32
    .sparse-switch
        0x1 -> :sswitch_28
        0x3 -> :sswitch_15
        0x5 -> :sswitch_1d
        0x7 -> :sswitch_25
        0x800003 -> :sswitch_2b
        0x800005 -> :sswitch_2e
    .end sparse-switch
.end method

.method private getDefaultMargin(Landroid/view/View;Landroid/widget/GridLayout$LayoutParams;ZZ)I
    .registers 13
    .parameter "c"
    .parameter "p"
    .parameter "horizontal"
    .parameter "leading"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 612
    iget-boolean v6, p0, Landroid/widget/GridLayout;->useDefaultMargins:Z

    #@4
    if-nez v6, :cond_7

    #@6
    .line 621
    :goto_6
    return v5

    #@7
    .line 615
    :cond_7
    if-eqz p3, :cond_27

    #@9
    iget-object v4, p2, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 616
    .local v4, spec:Landroid/widget/GridLayout$Spec;
    :goto_b
    if-eqz p3, :cond_2a

    #@d
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@f
    .line 617
    .local v0, axis:Landroid/widget/GridLayout$Axis;
    :goto_f
    iget-object v3, v4, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@11
    .line 618
    .local v3, span:Landroid/widget/GridLayout$Interval;
    if-eqz p3, :cond_2f

    #@13
    invoke-virtual {p0}, Landroid/widget/GridLayout;->isLayoutRtl()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_2f

    #@19
    if-nez p4, :cond_2d

    #@1b
    move v2, v1

    #@1c
    .line 619
    .local v2, leading1:Z
    :goto_1c
    if-eqz v2, :cond_33

    #@1e
    iget v6, v3, Landroid/widget/GridLayout$Interval;->min:I

    #@20
    if-nez v6, :cond_31

    #@22
    .line 621
    .local v1, isAtEdge:Z
    :cond_22
    :goto_22
    invoke-direct {p0, p1, v1, p3, p4}, Landroid/widget/GridLayout;->getDefaultMargin(Landroid/view/View;ZZZ)I

    #@25
    move-result v5

    #@26
    goto :goto_6

    #@27
    .line 615
    .end local v0           #axis:Landroid/widget/GridLayout$Axis;
    .end local v1           #isAtEdge:Z
    .end local v2           #leading1:Z
    .end local v3           #span:Landroid/widget/GridLayout$Interval;
    .end local v4           #spec:Landroid/widget/GridLayout$Spec;
    :cond_27
    iget-object v4, p2, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@29
    goto :goto_b

    #@2a
    .line 616
    .restart local v4       #spec:Landroid/widget/GridLayout$Spec;
    :cond_2a
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@2c
    goto :goto_f

    #@2d
    .restart local v0       #axis:Landroid/widget/GridLayout$Axis;
    .restart local v3       #span:Landroid/widget/GridLayout$Interval;
    :cond_2d
    move v2, v5

    #@2e
    .line 618
    goto :goto_1c

    #@2f
    :cond_2f
    move v2, p4

    #@30
    goto :goto_1c

    #@31
    .restart local v2       #leading1:Z
    :cond_31
    move v1, v5

    #@32
    .line 619
    goto :goto_22

    #@33
    :cond_33
    iget v6, v3, Landroid/widget/GridLayout$Interval;->max:I

    #@35
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@38
    move-result v7

    #@39
    if-eq v6, v7, :cond_22

    #@3b
    move v1, v5

    #@3c
    goto :goto_22
.end method

.method private getDefaultMargin(Landroid/view/View;ZZ)I
    .registers 6
    .parameter "c"
    .parameter "horizontal"
    .parameter "leading"

    #@0
    .prologue
    .line 601
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    const-class v1, Landroid/widget/Space;

    #@6
    if-ne v0, v1, :cond_a

    #@8
    .line 602
    const/4 v0, 0x0

    #@9
    .line 604
    :goto_9
    return v0

    #@a
    :cond_a
    iget v0, p0, Landroid/widget/GridLayout;->defaultGap:I

    #@c
    div-int/lit8 v0, v0, 0x2

    #@e
    goto :goto_9
.end method

.method private getDefaultMargin(Landroid/view/View;ZZZ)I
    .registers 6
    .parameter "c"
    .parameter "isAtEdge"
    .parameter "horizontal"
    .parameter "leading"

    #@0
    .prologue
    .line 608
    if-eqz p2, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/GridLayout;->getDefaultMargin(Landroid/view/View;ZZ)I

    #@7
    move-result v0

    #@8
    goto :goto_3
.end method

.method private getMargin(Landroid/view/View;ZZ)I
    .registers 11
    .parameter "view"
    .parameter "horizontal"
    .parameter "leading"

    #@0
    .prologue
    .line 633
    iget v5, p0, Landroid/widget/GridLayout;->alignmentMode:I

    #@2
    const/4 v6, 0x1

    #@3
    if-ne v5, v6, :cond_a

    #@5
    .line 634
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@8
    move-result v5

    #@9
    .line 641
    :goto_9
    return v5

    #@a
    .line 636
    :cond_a
    if-eqz p2, :cond_25

    #@c
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@e
    .line 637
    .local v0, axis:Landroid/widget/GridLayout$Axis;
    :goto_e
    if-eqz p3, :cond_28

    #@10
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getLeadingMargins()[I

    #@13
    move-result-object v3

    #@14
    .line 638
    .local v3, margins:[I
    :goto_14
    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@17
    move-result-object v2

    #@18
    .line 639
    .local v2, lp:Landroid/widget/GridLayout$LayoutParams;
    if-eqz p2, :cond_2d

    #@1a
    iget-object v4, v2, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@1c
    .line 640
    .local v4, spec:Landroid/widget/GridLayout$Spec;
    :goto_1c
    if-eqz p3, :cond_30

    #@1e
    iget-object v5, v4, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@20
    iget v1, v5, Landroid/widget/GridLayout$Interval;->min:I

    #@22
    .line 641
    .local v1, index:I
    :goto_22
    aget v5, v3, v1

    #@24
    goto :goto_9

    #@25
    .line 636
    .end local v0           #axis:Landroid/widget/GridLayout$Axis;
    .end local v1           #index:I
    .end local v2           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v3           #margins:[I
    .end local v4           #spec:Landroid/widget/GridLayout$Spec;
    :cond_25
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@27
    goto :goto_e

    #@28
    .line 637
    .restart local v0       #axis:Landroid/widget/GridLayout$Axis;
    :cond_28
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getTrailingMargins()[I

    #@2b
    move-result-object v3

    #@2c
    goto :goto_14

    #@2d
    .line 639
    .restart local v2       #lp:Landroid/widget/GridLayout$LayoutParams;
    .restart local v3       #margins:[I
    :cond_2d
    iget-object v4, v2, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@2f
    goto :goto_1c

    #@30
    .line 640
    .restart local v4       #spec:Landroid/widget/GridLayout$Spec;
    :cond_30
    iget-object v5, v4, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@32
    iget v1, v5, Landroid/widget/GridLayout$Interval;->max:I

    #@34
    goto :goto_22
.end method

.method private getMeasurement(Landroid/view/View;Z)I
    .registers 7
    .parameter "c"
    .parameter "horizontal"

    #@0
    .prologue
    .line 1016
    if-eqz p2, :cond_1a

    #@2
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@5
    move-result v1

    #@6
    .line 1017
    .local v1, result:I
    :goto_6
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getLayoutMode()I

    #@9
    move-result v2

    #@a
    const/4 v3, 0x1

    #@b
    if-ne v2, v3, :cond_19

    #@d
    .line 1018
    invoke-virtual {p1}, Landroid/view/View;->getOpticalInsets()Landroid/graphics/Insets;

    #@10
    move-result-object v0

    #@11
    .line 1019
    .local v0, insets:Landroid/graphics/Insets;
    if-eqz p2, :cond_1f

    #@13
    iget v2, v0, Landroid/graphics/Insets;->left:I

    #@15
    iget v3, v0, Landroid/graphics/Insets;->right:I

    #@17
    add-int/2addr v2, v3

    #@18
    :goto_18
    sub-int/2addr v1, v2

    #@19
    .line 1021
    .end local v0           #insets:Landroid/graphics/Insets;
    .end local v1           #result:I
    :cond_19
    return v1

    #@1a
    .line 1016
    :cond_1a
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@1d
    move-result v1

    #@1e
    goto :goto_6

    #@1f
    .line 1019
    .restart local v0       #insets:Landroid/graphics/Insets;
    .restart local v1       #result:I
    :cond_1f
    iget v2, v0, Landroid/graphics/Insets;->top:I

    #@21
    iget v3, v0, Landroid/graphics/Insets;->bottom:I

    #@23
    add-int/2addr v2, v3

    #@24
    goto :goto_18
.end method

.method private getTotalMargin(Landroid/view/View;Z)I
    .registers 5
    .parameter "child"
    .parameter "horizontal"

    #@0
    .prologue
    .line 646
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@4
    move-result v0

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {p0, p1, p2, v1}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@9
    move-result v1

    #@a
    add-int/2addr v0, v1

    #@b
    return v0
.end method

.method private static handleInvalidParams(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 773
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const-string v2, ". "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method private invalidateStructure()V
    .registers 2

    #@0
    .prologue
    .line 740
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/widget/GridLayout;->lastLayoutParamsHashCode:I

    #@3
    .line 741
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@5
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->invalidateStructure()V

    #@8
    .line 742
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@a
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->invalidateStructure()V

    #@d
    .line 744
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateValues()V

    #@10
    .line 745
    return-void
.end method

.method private invalidateValues()V
    .registers 2

    #@0
    .prologue
    .line 750
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 751
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@a
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->invalidateValues()V

    #@d
    .line 752
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@f
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->invalidateValues()V

    #@12
    .line 754
    :cond_12
    return-void
.end method

.method static max2([II)I
    .registers 6
    .parameter "a"
    .parameter "valueIfEmpty"

    #@0
    .prologue
    .line 562
    move v2, p1

    #@1
    .line 563
    .local v2, result:I
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    array-length v0, p0

    #@3
    .local v0, N:I
    :goto_3
    if-ge v1, v0, :cond_e

    #@5
    .line 564
    aget v3, p0, v1

    #@7
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@a
    move-result v2

    #@b
    .line 563
    add-int/lit8 v1, v1, 0x1

    #@d
    goto :goto_3

    #@e
    .line 566
    :cond_e
    return v2
.end method

.method private measureChildWithMargins2(Landroid/view/View;IIII)V
    .registers 10
    .parameter "child"
    .parameter "parentWidthSpec"
    .parameter "parentHeightSpec"
    .parameter "childWidth"
    .parameter "childHeight"

    #@0
    .prologue
    .line 948
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@2
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@4
    add-int/2addr v2, v3

    #@5
    const/4 v3, 0x1

    #@6
    invoke-direct {p0, p1, v3}, Landroid/widget/GridLayout;->getTotalMargin(Landroid/view/View;Z)I

    #@9
    move-result v3

    #@a
    add-int/2addr v2, v3

    #@b
    invoke-static {p2, v2, p4}, Landroid/widget/GridLayout;->getChildMeasureSpec(III)I

    #@e
    move-result v1

    #@f
    .line 950
    .local v1, childWidthSpec:I
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@11
    iget v3, p0, Landroid/view/View;->mPaddingBottom:I

    #@13
    add-int/2addr v2, v3

    #@14
    const/4 v3, 0x0

    #@15
    invoke-direct {p0, p1, v3}, Landroid/widget/GridLayout;->getTotalMargin(Landroid/view/View;Z)I

    #@18
    move-result v3

    #@19
    add-int/2addr v2, v3

    #@1a
    invoke-static {p3, v2, p5}, Landroid/widget/GridLayout;->getChildMeasureSpec(III)I

    #@1d
    move-result v0

    #@1e
    .line 952
    .local v0, childHeightSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@21
    .line 953
    return-void
.end method

.method private measureChildrenWithMargins(IIZ)V
    .registers 26
    .parameter "widthSpec"
    .parameter "heightSpec"
    .parameter "firstPass"

    #@0
    .prologue
    .line 956
    const/16 v17, 0x0

    #@2
    .local v17, i:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getChildCount()I

    #@5
    move-result v13

    #@6
    .local v13, N:I
    :goto_6
    move/from16 v0, v17

    #@8
    if-ge v0, v13, :cond_a7

    #@a
    .line 957
    move-object/from16 v0, p0

    #@c
    move/from16 v1, v17

    #@e
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v3

    #@12
    .line 958
    .local v3, c:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@15
    move-result v2

    #@16
    const/16 v4, 0x8

    #@18
    if-ne v2, v4, :cond_1d

    #@1a
    .line 956
    :cond_1a
    :goto_1a
    add-int/lit8 v17, v17, 0x1

    #@1c
    goto :goto_6

    #@1d
    .line 959
    :cond_1d
    move-object/from16 v0, p0

    #@1f
    invoke-virtual {v0, v3}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@22
    move-result-object v19

    #@23
    .line 960
    .local v19, lp:Landroid/widget/GridLayout$LayoutParams;
    if-eqz p3, :cond_37

    #@25
    .line 961
    move-object/from16 v0, v19

    #@27
    iget v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@29
    move-object/from16 v0, v19

    #@2b
    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2d
    move-object/from16 v2, p0

    #@2f
    move/from16 v4, p1

    #@31
    move/from16 v5, p2

    #@33
    invoke-direct/range {v2 .. v7}, Landroid/widget/GridLayout;->measureChildWithMargins2(Landroid/view/View;IIII)V

    #@36
    goto :goto_1a

    #@37
    .line 963
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget v2, v0, Landroid/widget/GridLayout;->orientation:I

    #@3b
    if-nez v2, :cond_87

    #@3d
    const/16 v16, 0x1

    #@3f
    .line 964
    .local v16, horizontal:Z
    :goto_3f
    if-eqz v16, :cond_8a

    #@41
    move-object/from16 v0, v19

    #@43
    iget-object v0, v0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@45
    move-object/from16 v21, v0

    #@47
    .line 965
    .local v21, spec:Landroid/widget/GridLayout$Spec;
    :goto_47
    move-object/from16 v0, v21

    #@49
    iget-object v2, v0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@4b
    sget-object v4, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    #@4d
    if-ne v2, v4, :cond_1a

    #@4f
    .line 966
    move-object/from16 v0, v21

    #@51
    iget-object v0, v0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@53
    move-object/from16 v20, v0

    #@55
    .line 967
    .local v20, span:Landroid/widget/GridLayout$Interval;
    if-eqz v16, :cond_91

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v14, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@5b
    .line 968
    .local v14, axis:Landroid/widget/GridLayout$Axis;
    :goto_5b
    invoke-virtual {v14}, Landroid/widget/GridLayout$Axis;->getLocations()[I

    #@5e
    move-result-object v18

    #@5f
    .line 969
    .local v18, locations:[I
    move-object/from16 v0, v20

    #@61
    iget v2, v0, Landroid/widget/GridLayout$Interval;->max:I

    #@63
    aget v2, v18, v2

    #@65
    move-object/from16 v0, v20

    #@67
    iget v4, v0, Landroid/widget/GridLayout$Interval;->min:I

    #@69
    aget v4, v18, v4

    #@6b
    sub-int v15, v2, v4

    #@6d
    .line 970
    .local v15, cellSize:I
    move-object/from16 v0, p0

    #@6f
    move/from16 v1, v16

    #@71
    invoke-direct {v0, v3, v1}, Landroid/widget/GridLayout;->getTotalMargin(Landroid/view/View;Z)I

    #@74
    move-result v2

    #@75
    sub-int v6, v15, v2

    #@77
    .line 971
    .local v6, viewSize:I
    if-eqz v16, :cond_96

    #@79
    .line 972
    move-object/from16 v0, v19

    #@7b
    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@7d
    move-object/from16 v2, p0

    #@7f
    move/from16 v4, p1

    #@81
    move/from16 v5, p2

    #@83
    invoke-direct/range {v2 .. v7}, Landroid/widget/GridLayout;->measureChildWithMargins2(Landroid/view/View;IIII)V

    #@86
    goto :goto_1a

    #@87
    .line 963
    .end local v6           #viewSize:I
    .end local v14           #axis:Landroid/widget/GridLayout$Axis;
    .end local v15           #cellSize:I
    .end local v16           #horizontal:Z
    .end local v18           #locations:[I
    .end local v20           #span:Landroid/widget/GridLayout$Interval;
    .end local v21           #spec:Landroid/widget/GridLayout$Spec;
    :cond_87
    const/16 v16, 0x0

    #@89
    goto :goto_3f

    #@8a
    .line 964
    .restart local v16       #horizontal:Z
    :cond_8a
    move-object/from16 v0, v19

    #@8c
    iget-object v0, v0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@8e
    move-object/from16 v21, v0

    #@90
    goto :goto_47

    #@91
    .line 967
    .restart local v20       #span:Landroid/widget/GridLayout$Interval;
    .restart local v21       #spec:Landroid/widget/GridLayout$Spec;
    :cond_91
    move-object/from16 v0, p0

    #@93
    iget-object v14, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@95
    goto :goto_5b

    #@96
    .line 974
    .restart local v6       #viewSize:I
    .restart local v14       #axis:Landroid/widget/GridLayout$Axis;
    .restart local v15       #cellSize:I
    .restart local v18       #locations:[I
    :cond_96
    move-object/from16 v0, v19

    #@98
    iget v11, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@9a
    move-object/from16 v7, p0

    #@9c
    move-object v8, v3

    #@9d
    move/from16 v9, p1

    #@9f
    move/from16 v10, p2

    #@a1
    move v12, v6

    #@a2
    invoke-direct/range {v7 .. v12}, Landroid/widget/GridLayout;->measureChildWithMargins2(Landroid/view/View;IIII)V

    #@a5
    goto/16 :goto_1a

    #@a7
    .line 979
    .end local v3           #c:Landroid/view/View;
    .end local v6           #viewSize:I
    .end local v14           #axis:Landroid/widget/GridLayout$Axis;
    .end local v15           #cellSize:I
    .end local v16           #horizontal:Z
    .end local v18           #locations:[I
    .end local v19           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v20           #span:Landroid/widget/GridLayout$Interval;
    .end local v21           #spec:Landroid/widget/GridLayout$Spec;
    :cond_a7
    return-void
.end method

.method private static procrusteanFill([IIII)V
    .registers 7
    .parameter "a"
    .parameter "start"
    .parameter "end"
    .parameter "value"

    #@0
    .prologue
    .line 662
    array-length v0, p0

    #@1
    .line 663
    .local v0, length:I
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    #@4
    move-result v1

    #@5
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    #@8
    move-result v2

    #@9
    invoke-static {p0, v1, v2, p3}, Ljava/util/Arrays;->fill([IIII)V

    #@c
    .line 664
    return-void
.end method

.method private static setCellGroup(Landroid/widget/GridLayout$LayoutParams;IIII)V
    .registers 7
    .parameter "lp"
    .parameter "row"
    .parameter "rowSpan"
    .parameter "col"
    .parameter "colSpan"

    #@0
    .prologue
    .line 667
    new-instance v0, Landroid/widget/GridLayout$Interval;

    #@2
    add-int v1, p1, p2

    #@4
    invoke-direct {v0, p1, v1}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/GridLayout$LayoutParams;->setRowSpecSpan(Landroid/widget/GridLayout$Interval;)V

    #@a
    .line 668
    new-instance v0, Landroid/widget/GridLayout$Interval;

    #@c
    add-int v1, p3, p4

    #@e
    invoke-direct {v0, p3, v1}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@11
    invoke-virtual {p0, v0}, Landroid/widget/GridLayout$LayoutParams;->setColumnSpecSpan(Landroid/widget/GridLayout$Interval;)V

    #@14
    .line 669
    return-void
.end method

.method public static spec(I)Landroid/widget/GridLayout$Spec;
    .registers 2
    .parameter "start"

    #@0
    .prologue
    .line 2466
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static spec(II)Landroid/widget/GridLayout$Spec;
    .registers 3
    .parameter "start"
    .parameter "size"

    #@0
    .prologue
    .line 2454
    sget-object v0, Landroid/widget/GridLayout;->UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment;

    #@2
    invoke-static {p0, p1, v0}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;
    .registers 9
    .parameter "start"
    .parameter "size"
    .parameter "alignment"

    #@0
    .prologue
    .line 2427
    new-instance v0, Landroid/widget/GridLayout$Spec;

    #@2
    const/high16 v1, -0x8000

    #@4
    if-eq p0, v1, :cond_f

    #@6
    const/4 v1, 0x1

    #@7
    :goto_7
    const/4 v5, 0x0

    #@8
    move v2, p0

    #@9
    move v3, p1

    #@a
    move-object v4, p2

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/widget/GridLayout$Spec;-><init>(ZIILandroid/widget/GridLayout$Alignment;Landroid/widget/GridLayout$1;)V

    #@e
    return-object v0

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_7
.end method

.method public static spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;
    .registers 3
    .parameter "start"
    .parameter "alignment"

    #@0
    .prologue
    .line 2441
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0, p1}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private validateLayoutParams()V
    .registers 22

    #@0
    .prologue
    .line 683
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/widget/GridLayout;->orientation:I

    #@4
    move/from16 v19, v0

    #@6
    if-nez v19, :cond_73

    #@8
    const/4 v5, 0x1

    #@9
    .line 684
    .local v5, horizontal:Z
    :goto_9
    if-eqz v5, :cond_75

    #@b
    move-object/from16 v0, p0

    #@d
    iget-object v3, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@f
    .line 685
    .local v3, axis:Landroid/widget/GridLayout$Axis;
    :goto_f
    iget v0, v3, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@11
    move/from16 v19, v0

    #@13
    const/high16 v20, -0x8000

    #@15
    move/from16 v0, v19

    #@17
    move/from16 v1, v20

    #@19
    if-eq v0, v1, :cond_7a

    #@1b
    iget v4, v3, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@1d
    .line 687
    .local v4, count:I
    :goto_1d
    const/4 v8, 0x0

    #@1e
    .line 688
    .local v8, major:I
    const/4 v14, 0x0

    #@1f
    .line 689
    .local v14, minor:I
    new-array v13, v4, [I

    #@21
    .line 691
    .local v13, maxSizes:[I
    const/4 v6, 0x0

    #@22
    .local v6, i:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getChildCount()I

    #@25
    move-result v2

    #@26
    .local v2, N:I
    :goto_26
    if-ge v6, v2, :cond_af

    #@28
    .line 692
    move-object/from16 v0, p0

    #@2a
    invoke-virtual {v0, v6}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@2d
    move-result-object v19

    #@2e
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@31
    move-result-object v7

    #@32
    check-cast v7, Landroid/widget/GridLayout$LayoutParams;

    #@34
    .line 694
    .local v7, lp:Landroid/widget/GridLayout$LayoutParams;
    if-eqz v5, :cond_7c

    #@36
    iget-object v11, v7, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@38
    .line 695
    .local v11, majorSpec:Landroid/widget/GridLayout$Spec;
    :goto_38
    iget-object v9, v11, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@3a
    .line 696
    .local v9, majorRange:Landroid/widget/GridLayout$Interval;
    iget-boolean v12, v11, Landroid/widget/GridLayout$Spec;->startDefined:Z

    #@3c
    .line 697
    .local v12, majorWasDefined:Z
    invoke-virtual {v9}, Landroid/widget/GridLayout$Interval;->size()I

    #@3f
    move-result v10

    #@40
    .line 698
    .local v10, majorSpan:I
    if-eqz v12, :cond_44

    #@42
    .line 699
    iget v8, v9, Landroid/widget/GridLayout$Interval;->min:I

    #@44
    .line 702
    :cond_44
    if-eqz v5, :cond_7f

    #@46
    iget-object v0, v7, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@48
    move-object/from16 v17, v0

    #@4a
    .line 703
    .local v17, minorSpec:Landroid/widget/GridLayout$Spec;
    :goto_4a
    move-object/from16 v0, v17

    #@4c
    iget-object v15, v0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@4e
    .line 704
    .local v15, minorRange:Landroid/widget/GridLayout$Interval;
    move-object/from16 v0, v17

    #@50
    iget-boolean v0, v0, Landroid/widget/GridLayout$Spec;->startDefined:Z

    #@52
    move/from16 v18, v0

    #@54
    .line 705
    .local v18, minorWasDefined:Z
    move/from16 v0, v18

    #@56
    invoke-static {v15, v0, v4}, Landroid/widget/GridLayout;->clip(Landroid/widget/GridLayout$Interval;ZI)I

    #@59
    move-result v16

    #@5a
    .line 706
    .local v16, minorSpan:I
    if-eqz v18, :cond_5e

    #@5c
    .line 707
    iget v14, v15, Landroid/widget/GridLayout$Interval;->min:I

    #@5e
    .line 710
    :cond_5e
    if-eqz v4, :cond_9c

    #@60
    .line 712
    if-eqz v12, :cond_64

    #@62
    if-nez v18, :cond_91

    #@64
    .line 713
    :cond_64
    :goto_64
    add-int v19, v14, v16

    #@66
    move/from16 v0, v19

    #@68
    invoke-static {v13, v8, v14, v0}, Landroid/widget/GridLayout;->fits([IIII)Z

    #@6b
    move-result v19

    #@6c
    if-nez v19, :cond_91

    #@6e
    .line 714
    if-eqz v18, :cond_84

    #@70
    .line 715
    add-int/lit8 v8, v8, 0x1

    #@72
    goto :goto_64

    #@73
    .line 683
    .end local v2           #N:I
    .end local v3           #axis:Landroid/widget/GridLayout$Axis;
    .end local v4           #count:I
    .end local v5           #horizontal:Z
    .end local v6           #i:I
    .end local v7           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v8           #major:I
    .end local v9           #majorRange:Landroid/widget/GridLayout$Interval;
    .end local v10           #majorSpan:I
    .end local v11           #majorSpec:Landroid/widget/GridLayout$Spec;
    .end local v12           #majorWasDefined:Z
    .end local v13           #maxSizes:[I
    .end local v14           #minor:I
    .end local v15           #minorRange:Landroid/widget/GridLayout$Interval;
    .end local v16           #minorSpan:I
    .end local v17           #minorSpec:Landroid/widget/GridLayout$Spec;
    .end local v18           #minorWasDefined:Z
    :cond_73
    const/4 v5, 0x0

    #@74
    goto :goto_9

    #@75
    .line 684
    .restart local v5       #horizontal:Z
    :cond_75
    move-object/from16 v0, p0

    #@77
    iget-object v3, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@79
    goto :goto_f

    #@7a
    .line 685
    .restart local v3       #axis:Landroid/widget/GridLayout$Axis;
    :cond_7a
    const/4 v4, 0x0

    #@7b
    goto :goto_1d

    #@7c
    .line 694
    .restart local v2       #N:I
    .restart local v4       #count:I
    .restart local v6       #i:I
    .restart local v7       #lp:Landroid/widget/GridLayout$LayoutParams;
    .restart local v8       #major:I
    .restart local v13       #maxSizes:[I
    .restart local v14       #minor:I
    :cond_7c
    iget-object v11, v7, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@7e
    goto :goto_38

    #@7f
    .line 702
    .restart local v9       #majorRange:Landroid/widget/GridLayout$Interval;
    .restart local v10       #majorSpan:I
    .restart local v11       #majorSpec:Landroid/widget/GridLayout$Spec;
    .restart local v12       #majorWasDefined:Z
    :cond_7f
    iget-object v0, v7, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@81
    move-object/from16 v17, v0

    #@83
    goto :goto_4a

    #@84
    .line 717
    .restart local v15       #minorRange:Landroid/widget/GridLayout$Interval;
    .restart local v16       #minorSpan:I
    .restart local v17       #minorSpec:Landroid/widget/GridLayout$Spec;
    .restart local v18       #minorWasDefined:Z
    :cond_84
    add-int v19, v14, v16

    #@86
    move/from16 v0, v19

    #@88
    if-gt v0, v4, :cond_8d

    #@8a
    .line 718
    add-int/lit8 v14, v14, 0x1

    #@8c
    goto :goto_64

    #@8d
    .line 720
    :cond_8d
    const/4 v14, 0x0

    #@8e
    .line 721
    add-int/lit8 v8, v8, 0x1

    #@90
    goto :goto_64

    #@91
    .line 726
    :cond_91
    add-int v19, v14, v16

    #@93
    add-int v20, v8, v10

    #@95
    move/from16 v0, v19

    #@97
    move/from16 v1, v20

    #@99
    invoke-static {v13, v14, v0, v1}, Landroid/widget/GridLayout;->procrusteanFill([IIII)V

    #@9c
    .line 729
    :cond_9c
    if-eqz v5, :cond_a9

    #@9e
    .line 730
    move/from16 v0, v16

    #@a0
    invoke-static {v7, v8, v10, v14, v0}, Landroid/widget/GridLayout;->setCellGroup(Landroid/widget/GridLayout$LayoutParams;IIII)V

    #@a3
    .line 735
    :goto_a3
    add-int v14, v14, v16

    #@a5
    .line 691
    add-int/lit8 v6, v6, 0x1

    #@a7
    goto/16 :goto_26

    #@a9
    .line 732
    :cond_a9
    move/from16 v0, v16

    #@ab
    invoke-static {v7, v14, v0, v8, v10}, Landroid/widget/GridLayout;->setCellGroup(Landroid/widget/GridLayout$LayoutParams;IIII)V

    #@ae
    goto :goto_a3

    #@af
    .line 737
    .end local v7           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v9           #majorRange:Landroid/widget/GridLayout$Interval;
    .end local v10           #majorSpan:I
    .end local v11           #majorSpec:Landroid/widget/GridLayout$Spec;
    .end local v12           #majorWasDefined:Z
    .end local v15           #minorRange:Landroid/widget/GridLayout$Interval;
    .end local v16           #minorSpan:I
    .end local v17           #minorSpec:Landroid/widget/GridLayout$Spec;
    .end local v18           #minorWasDefined:Z
    :cond_af
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 6
    .parameter "p"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 798
    instance-of v3, p1, Landroid/widget/GridLayout$LayoutParams;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 806
    :goto_6
    return v1

    #@7
    :cond_7
    move-object v0, p1

    #@8
    .line 801
    check-cast v0, Landroid/widget/GridLayout$LayoutParams;

    #@a
    .line 803
    .local v0, lp:Landroid/widget/GridLayout$LayoutParams;
    invoke-direct {p0, v0, v2}, Landroid/widget/GridLayout;->checkLayoutParams(Landroid/widget/GridLayout$LayoutParams;Z)V

    #@d
    .line 804
    invoke-direct {p0, v0, v1}, Landroid/widget/GridLayout;->checkLayoutParams(Landroid/widget/GridLayout$LayoutParams;Z)V

    #@10
    move v1, v2

    #@11
    .line 806
    goto :goto_6
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 150
    invoke-virtual {p0}, Landroid/widget/GridLayout;->generateDefaultLayoutParams()Landroid/widget/GridLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/GridLayout$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 811
    new-instance v0, Landroid/widget/GridLayout$LayoutParams;

    #@2
    invoke-direct {v0}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 150
    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/GridLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 150
    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/GridLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/GridLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 816
    new-instance v0, Landroid/widget/GridLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/GridLayout$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 821
    new-instance v0, Landroid/widget/GridLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method final getAlignment(Landroid/widget/GridLayout$Alignment;Z)Landroid/widget/GridLayout$Alignment;
    .registers 4
    .parameter "alignment"
    .parameter "horizontal"

    #@0
    .prologue
    .line 1038
    sget-object v0, Landroid/widget/GridLayout;->UNDEFINED_ALIGNMENT:Landroid/widget/GridLayout$Alignment;

    #@2
    if-eq p1, v0, :cond_5

    #@4
    .end local p1
    :goto_4
    return-object p1

    #@5
    .restart local p1
    :cond_5
    if-eqz p2, :cond_a

    #@7
    sget-object p1, Landroid/widget/GridLayout;->START:Landroid/widget/GridLayout$Alignment;

    #@9
    goto :goto_4

    #@a
    :cond_a
    sget-object p1, Landroid/widget/GridLayout;->BASELINE:Landroid/widget/GridLayout$Alignment;

    #@c
    goto :goto_4
.end method

.method public getAlignmentMode()I
    .registers 2

    #@0
    .prologue
    .line 464
    iget v0, p0, Landroid/widget/GridLayout;->alignmentMode:I

    #@2
    return v0
.end method

.method public getColumnCount()I
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method final getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 769
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/widget/GridLayout$LayoutParams;

    #@6
    return-object v0
.end method

.method getMargin1(Landroid/view/View;ZZ)I
    .registers 7
    .parameter "view"
    .parameter "horizontal"
    .parameter "leading"

    #@0
    .prologue
    .line 625
    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 626
    .local v0, lp:Landroid/widget/GridLayout$LayoutParams;
    if-eqz p2, :cond_16

    #@6
    if-eqz p3, :cond_13

    #@8
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@a
    .line 629
    .local v1, margin:I
    :goto_a
    const/high16 v2, -0x8000

    #@c
    if-ne v1, v2, :cond_12

    #@e
    invoke-direct {p0, p1, v0, p2, p3}, Landroid/widget/GridLayout;->getDefaultMargin(Landroid/view/View;Landroid/widget/GridLayout$LayoutParams;ZZ)I

    #@11
    move-result v1

    #@12
    .end local v1           #margin:I
    :cond_12
    return v1

    #@13
    .line 626
    :cond_13
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@15
    goto :goto_a

    #@16
    :cond_16
    if-eqz p3, :cond_1b

    #@18
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1a
    goto :goto_a

    #@1b
    :cond_1b
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@1d
    goto :goto_a
.end method

.method final getMeasurementIncludingMargin(Landroid/view/View;Z)I
    .registers 5
    .parameter "c"
    .parameter "horizontal"

    #@0
    .prologue
    .line 1025
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x8

    #@6
    if-ne v0, v1, :cond_a

    #@8
    .line 1026
    const/4 v0, 0x0

    #@9
    .line 1028
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;->getMeasurement(Landroid/view/View;Z)I

    #@d
    move-result v0

    #@e
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;->getTotalMargin(Landroid/view/View;Z)I

    #@11
    move-result v1

    #@12
    add-int/2addr v0, v1

    #@13
    goto :goto_9
.end method

.method public getOrientation()I
    .registers 2

    #@0
    .prologue
    .line 293
    iget v0, p0, Landroid/widget/GridLayout;->orientation:I

    #@2
    return v0
.end method

.method public getRowCount()I
    .registers 2

    #@0
    .prologue
    .line 353
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getUseDefaultMargins()Z
    .registers 2

    #@0
    .prologue
    .line 417
    iget-boolean v0, p0, Landroid/widget/GridLayout;->useDefaultMargins:Z

    #@2
    return v0
.end method

.method public isColumnOrderPreserved()Z
    .registers 2

    #@0
    .prologue
    .line 534
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->isOrderPreserved()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isRowOrderPreserved()Z
    .registers 2

    #@0
    .prologue
    .line 498
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis;->isOrderPreserved()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected onChildVisibilityChanged(Landroid/view/View;II)V
    .registers 5
    .parameter "child"
    .parameter "oldVisibility"
    .parameter "newVisibility"

    #@0
    .prologue
    const/16 v0, 0x8

    #@2
    .line 915
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->onChildVisibilityChanged(Landroid/view/View;II)V

    #@5
    .line 916
    if-eq p2, v0, :cond_9

    #@7
    if-ne p3, v0, :cond_c

    #@9
    .line 917
    :cond_9
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@c
    .line 919
    :cond_c
    return-void
.end method

.method protected onDebugDraw(Landroid/graphics/Canvas;)V
    .registers 24
    .parameter "canvas"

    #@0
    .prologue
    .line 861
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getHeight()I

    #@3
    move-result v2

    #@4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingTop()I

    #@7
    move-result v3

    #@8
    sub-int/2addr v2, v3

    #@9
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingBottom()I

    #@c
    move-result v3

    #@d
    sub-int v16, v2, v3

    #@f
    .line 862
    .local v16, height:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getWidth()I

    #@12
    move-result v2

    #@13
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingLeft()I

    #@16
    move-result v3

    #@17
    sub-int/2addr v2, v3

    #@18
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingRight()I

    #@1b
    move-result v3

    #@1c
    sub-int v19, v2, v3

    #@1e
    .line 864
    .local v19, width:I
    new-instance v8, Landroid/graphics/Paint;

    #@20
    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    #@23
    .line 865
    .local v8, paint:Landroid/graphics/Paint;
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@25
    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@28
    .line 866
    const/16 v2, 0x32

    #@2a
    const/16 v3, 0xff

    #@2c
    const/16 v5, 0xff

    #@2e
    const/16 v6, 0xff

    #@30
    invoke-static {v2, v3, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    #@33
    move-result v2

    #@34
    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@37
    .line 868
    move-object/from16 v0, p0

    #@39
    iget-object v2, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@3b
    iget-object v0, v2, Landroid/widget/GridLayout$Axis;->locations:[I

    #@3d
    move-object/from16 v20, v0

    #@3f
    .line 869
    .local v20, xs:[I
    if-eqz v20, :cond_5e

    #@41
    .line 870
    const/16 v17, 0x0

    #@43
    .local v17, i:I
    move-object/from16 v0, v20

    #@45
    array-length v0, v0

    #@46
    move/from16 v18, v0

    #@48
    .local v18, length:I
    :goto_48
    move/from16 v0, v17

    #@4a
    move/from16 v1, v18

    #@4c
    if-ge v0, v1, :cond_5e

    #@4e
    .line 871
    aget v4, v20, v17

    #@50
    .line 872
    .local v4, x:I
    const/4 v5, 0x0

    #@51
    add-int/lit8 v7, v16, -0x1

    #@53
    move-object/from16 v2, p0

    #@55
    move-object/from16 v3, p1

    #@57
    move v6, v4

    #@58
    invoke-direct/range {v2 .. v8}, Landroid/widget/GridLayout;->drawLine(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    #@5b
    .line 870
    add-int/lit8 v17, v17, 0x1

    #@5d
    goto :goto_48

    #@5e
    .line 876
    .end local v4           #x:I
    .end local v17           #i:I
    .end local v18           #length:I
    :cond_5e
    move-object/from16 v0, p0

    #@60
    iget-object v2, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@62
    iget-object v0, v2, Landroid/widget/GridLayout$Axis;->locations:[I

    #@64
    move-object/from16 v21, v0

    #@66
    .line 877
    .local v21, ys:[I
    if-eqz v21, :cond_86

    #@68
    .line 878
    const/16 v17, 0x0

    #@6a
    .restart local v17       #i:I
    move-object/from16 v0, v21

    #@6c
    array-length v0, v0

    #@6d
    move/from16 v18, v0

    #@6f
    .restart local v18       #length:I
    :goto_6f
    move/from16 v0, v17

    #@71
    move/from16 v1, v18

    #@73
    if-ge v0, v1, :cond_86

    #@75
    .line 879
    aget v12, v21, v17

    #@77
    .line 880
    .local v12, y:I
    const/4 v11, 0x0

    #@78
    add-int/lit8 v13, v19, -0x1

    #@7a
    move-object/from16 v9, p0

    #@7c
    move-object/from16 v10, p1

    #@7e
    move v14, v12

    #@7f
    move-object v15, v8

    #@80
    invoke-direct/range {v9 .. v15}, Landroid/widget/GridLayout;->drawLine(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    #@83
    .line 878
    add-int/lit8 v17, v17, 0x1

    #@85
    goto :goto_6f

    #@86
    .line 884
    .end local v12           #y:I
    .end local v17           #i:I
    .end local v18           #length:I
    :cond_86
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDebugDraw(Landroid/graphics/Canvas;)V

    #@89
    .line 885
    return-void
.end method

.method protected onDebugDrawMargins(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 843
    new-instance v3, Landroid/widget/GridLayout$LayoutParams;

    #@4
    invoke-direct {v3}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    #@7
    .line 844
    .local v3, lp:Landroid/widget/GridLayout$LayoutParams;
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getChildCount()I

    #@b
    move-result v4

    #@c
    if-ge v1, v4, :cond_44

    #@e
    .line 845
    invoke-virtual {p0, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v0

    #@12
    .line 846
    .local v0, c:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getLayoutMode()I

    #@15
    move-result v4

    #@16
    if-ne v4, v9, :cond_41

    #@18
    invoke-virtual {v0}, Landroid/view/View;->getOpticalInsets()Landroid/graphics/Insets;

    #@1b
    move-result-object v2

    #@1c
    .line 847
    .local v2, insets:Landroid/graphics/Insets;
    :goto_1c
    invoke-virtual {p0, v0, v9, v9}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@1f
    move-result v4

    #@20
    iget v5, v2, Landroid/graphics/Insets;->left:I

    #@22
    sub-int/2addr v4, v5

    #@23
    invoke-virtual {p0, v0, v10, v9}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@26
    move-result v5

    #@27
    iget v6, v2, Landroid/graphics/Insets;->top:I

    #@29
    sub-int/2addr v5, v6

    #@2a
    invoke-virtual {p0, v0, v9, v10}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@2d
    move-result v6

    #@2e
    iget v7, v2, Landroid/graphics/Insets;->right:I

    #@30
    sub-int/2addr v6, v7

    #@31
    invoke-virtual {p0, v0, v10, v10}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@34
    move-result v7

    #@35
    iget v8, v2, Landroid/graphics/Insets;->bottom:I

    #@37
    sub-int/2addr v7, v8

    #@38
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@3b
    .line 852
    invoke-virtual {v3, v0, p1}, Landroid/widget/GridLayout$LayoutParams;->onDebugDraw(Landroid/view/View;Landroid/graphics/Canvas;)V

    #@3e
    .line 844
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_8

    #@41
    .line 846
    .end local v2           #insets:Landroid/graphics/Insets;
    :cond_41
    sget-object v2, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@43
    goto :goto_1c

    #@44
    .line 854
    .end local v0           #c:Landroid/view/View;
    :cond_44
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1144
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1145
    const-class v0, Landroid/widget/GridLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1146
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1150
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1151
    const-class v0, Landroid/widget/GridLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1152
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 59
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1058
    invoke-direct/range {p0 .. p0}, Landroid/widget/GridLayout;->consistencyCheck()V

    #@3
    .line 1060
    sub-int v44, p4, p2

    #@5
    .line 1061
    .local v44, targetWidth:I
    sub-int v43, p5, p3

    #@7
    .line 1063
    .local v43, targetHeight:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingLeft()I

    #@a
    move-result v35

    #@b
    .line 1064
    .local v35, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingTop()I

    #@e
    move-result v37

    #@f
    .line 1065
    .local v37, paddingTop:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingRight()I

    #@12
    move-result v36

    #@13
    .line 1066
    .local v36, paddingRight:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getPaddingBottom()I

    #@16
    move-result v34

    #@17
    .line 1068
    .local v34, paddingBottom:I
    move-object/from16 v0, p0

    #@19
    iget-object v3, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@1b
    sub-int v6, v44, v35

    #@1d
    sub-int v6, v6, v36

    #@1f
    invoke-virtual {v3, v6}, Landroid/widget/GridLayout$Axis;->layout(I)V

    #@22
    .line 1069
    move-object/from16 v0, p0

    #@24
    iget-object v3, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@26
    sub-int v6, v43, v37

    #@28
    sub-int v6, v6, v34

    #@2a
    invoke-virtual {v3, v6}, Landroid/widget/GridLayout$Axis;->layout(I)V

    #@2d
    .line 1071
    move-object/from16 v0, p0

    #@2f
    iget-object v3, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@31
    invoke-virtual {v3}, Landroid/widget/GridLayout$Axis;->getLocations()[I

    #@34
    move-result-object v26

    #@35
    .line 1072
    .local v26, hLocations:[I
    move-object/from16 v0, p0

    #@37
    iget-object v3, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@39
    invoke-virtual {v3}, Landroid/widget/GridLayout$Axis;->getLocations()[I

    #@3c
    move-result-object v47

    #@3d
    .line 1074
    .local v47, vLocations:[I
    const/16 v28, 0x0

    #@3f
    .local v28, i:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getChildCount()I

    #@42
    move-result v12

    #@43
    .local v12, N:I
    :goto_43
    move/from16 v0, v28

    #@45
    if-ge v0, v12, :cond_1bb

    #@47
    .line 1075
    move-object/from16 v0, p0

    #@49
    move/from16 v1, v28

    #@4b
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@4e
    move-result-object v4

    #@4f
    .line 1076
    .local v4, c:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@52
    move-result v3

    #@53
    const/16 v6, 0x8

    #@55
    if-ne v3, v6, :cond_5a

    #@57
    .line 1074
    :goto_57
    add-int/lit8 v28, v28, 0x1

    #@59
    goto :goto_43

    #@5a
    .line 1077
    :cond_5a
    move-object/from16 v0, p0

    #@5c
    invoke-virtual {v0, v4}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@5f
    move-result-object v31

    #@60
    .line 1078
    .local v31, lp:Landroid/widget/GridLayout$LayoutParams;
    move-object/from16 v0, v31

    #@62
    iget-object v0, v0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@64
    move-object/from16 v20, v0

    #@66
    .line 1079
    .local v20, columnSpec:Landroid/widget/GridLayout$Spec;
    move-object/from16 v0, v31

    #@68
    iget-object v0, v0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@6a
    move-object/from16 v40, v0

    #@6c
    .line 1081
    .local v40, rowSpec:Landroid/widget/GridLayout$Spec;
    move-object/from16 v0, v20

    #@6e
    iget-object v0, v0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@70
    move-object/from16 v19, v0

    #@72
    .line 1082
    .local v19, colSpan:Landroid/widget/GridLayout$Interval;
    move-object/from16 v0, v40

    #@74
    iget-object v0, v0, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@76
    move-object/from16 v39, v0

    #@78
    .line 1084
    .local v39, rowSpan:Landroid/widget/GridLayout$Interval;
    move-object/from16 v0, v19

    #@7a
    iget v3, v0, Landroid/widget/GridLayout$Interval;->min:I

    #@7c
    aget v49, v26, v3

    #@7e
    .line 1085
    .local v49, x1:I
    move-object/from16 v0, v39

    #@80
    iget v3, v0, Landroid/widget/GridLayout$Interval;->min:I

    #@82
    aget v51, v47, v3

    #@84
    .line 1087
    .local v51, y1:I
    move-object/from16 v0, v19

    #@86
    iget v3, v0, Landroid/widget/GridLayout$Interval;->max:I

    #@88
    aget v50, v26, v3

    #@8a
    .line 1088
    .local v50, x2:I
    move-object/from16 v0, v39

    #@8c
    iget v3, v0, Landroid/widget/GridLayout$Interval;->max:I

    #@8e
    aget v52, v47, v3

    #@90
    .line 1090
    .local v52, y2:I
    sub-int v18, v50, v49

    #@92
    .line 1091
    .local v18, cellWidth:I
    sub-int v17, v52, v51

    #@94
    .line 1093
    .local v17, cellHeight:I
    const/4 v3, 0x1

    #@95
    move-object/from16 v0, p0

    #@97
    invoke-direct {v0, v4, v3}, Landroid/widget/GridLayout;->getMeasurement(Landroid/view/View;Z)I

    #@9a
    move-result v33

    #@9b
    .line 1094
    .local v33, pWidth:I
    const/4 v3, 0x0

    #@9c
    move-object/from16 v0, p0

    #@9e
    invoke-direct {v0, v4, v3}, Landroid/widget/GridLayout;->getMeasurement(Landroid/view/View;Z)I

    #@a1
    move-result v32

    #@a2
    .line 1096
    .local v32, pHeight:I
    move-object/from16 v0, v20

    #@a4
    iget-object v3, v0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@a6
    const/4 v6, 0x1

    #@a7
    move-object/from16 v0, p0

    #@a9
    invoke-virtual {v0, v3, v6}, Landroid/widget/GridLayout;->getAlignment(Landroid/widget/GridLayout$Alignment;Z)Landroid/widget/GridLayout$Alignment;

    #@ac
    move-result-object v5

    #@ad
    .line 1097
    .local v5, hAlign:Landroid/widget/GridLayout$Alignment;
    move-object/from16 v0, v40

    #@af
    iget-object v3, v0, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@b1
    const/4 v6, 0x0

    #@b2
    move-object/from16 v0, p0

    #@b4
    invoke-virtual {v0, v3, v6}, Landroid/widget/GridLayout;->getAlignment(Landroid/widget/GridLayout$Alignment;Z)Landroid/widget/GridLayout$Alignment;

    #@b7
    move-result-object v9

    #@b8
    .line 1099
    .local v9, vAlign:Landroid/widget/GridLayout$Alignment;
    move-object/from16 v0, p0

    #@ba
    iget-object v3, v0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@bc
    invoke-virtual {v3}, Landroid/widget/GridLayout$Axis;->getGroupBounds()Landroid/widget/GridLayout$PackedMap;

    #@bf
    move-result-object v3

    #@c0
    move/from16 v0, v28

    #@c2
    invoke-virtual {v3, v0}, Landroid/widget/GridLayout$PackedMap;->getValue(I)Ljava/lang/Object;

    #@c5
    move-result-object v2

    #@c6
    check-cast v2, Landroid/widget/GridLayout$Bounds;

    #@c8
    .line 1100
    .local v2, boundsX:Landroid/widget/GridLayout$Bounds;
    move-object/from16 v0, p0

    #@ca
    iget-object v3, v0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@cc
    invoke-virtual {v3}, Landroid/widget/GridLayout$Axis;->getGroupBounds()Landroid/widget/GridLayout$PackedMap;

    #@cf
    move-result-object v3

    #@d0
    move/from16 v0, v28

    #@d2
    invoke-virtual {v3, v0}, Landroid/widget/GridLayout$PackedMap;->getValue(I)Ljava/lang/Object;

    #@d5
    move-result-object v16

    #@d6
    check-cast v16, Landroid/widget/GridLayout$Bounds;

    #@d8
    .line 1103
    .local v16, boundsY:Landroid/widget/GridLayout$Bounds;
    const/4 v3, 0x1

    #@d9
    invoke-virtual {v2, v3}, Landroid/widget/GridLayout$Bounds;->size(Z)I

    #@dc
    move-result v3

    #@dd
    sub-int v3, v18, v3

    #@df
    invoke-virtual {v5, v4, v3}, Landroid/widget/GridLayout$Alignment;->getGravityOffset(Landroid/view/View;I)I

    #@e2
    move-result v24

    #@e3
    .line 1104
    .local v24, gravityOffsetX:I
    const/4 v3, 0x1

    #@e4
    move-object/from16 v0, v16

    #@e6
    invoke-virtual {v0, v3}, Landroid/widget/GridLayout$Bounds;->size(Z)I

    #@e9
    move-result v3

    #@ea
    sub-int v3, v17, v3

    #@ec
    invoke-virtual {v9, v4, v3}, Landroid/widget/GridLayout$Alignment;->getGravityOffset(Landroid/view/View;I)I

    #@ef
    move-result v25

    #@f0
    .line 1106
    .local v25, gravityOffsetY:I
    const/4 v3, 0x1

    #@f1
    const/4 v6, 0x1

    #@f2
    move-object/from16 v0, p0

    #@f4
    invoke-direct {v0, v4, v3, v6}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@f7
    move-result v30

    #@f8
    .line 1107
    .local v30, leftMargin:I
    const/4 v3, 0x0

    #@f9
    const/4 v6, 0x1

    #@fa
    move-object/from16 v0, p0

    #@fc
    invoke-direct {v0, v4, v3, v6}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@ff
    move-result v45

    #@100
    .line 1108
    .local v45, topMargin:I
    const/4 v3, 0x1

    #@101
    const/4 v6, 0x0

    #@102
    move-object/from16 v0, p0

    #@104
    invoke-direct {v0, v4, v3, v6}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@107
    move-result v38

    #@108
    .line 1109
    .local v38, rightMargin:I
    const/4 v3, 0x0

    #@109
    const/4 v6, 0x0

    #@10a
    move-object/from16 v0, p0

    #@10c
    invoke-direct {v0, v4, v3, v6}, Landroid/widget/GridLayout;->getMargin(Landroid/view/View;ZZ)I

    #@10f
    move-result v15

    #@110
    .line 1111
    .local v15, bottomMargin:I
    add-int v41, v30, v38

    #@112
    .line 1112
    .local v41, sumMarginsX:I
    add-int v42, v45, v15

    #@114
    .line 1115
    .local v42, sumMarginsY:I
    add-int v6, v33, v41

    #@116
    const/4 v7, 0x1

    #@117
    move-object/from16 v3, p0

    #@119
    invoke-virtual/range {v2 .. v7}, Landroid/widget/GridLayout$Bounds;->getOffset(Landroid/widget/GridLayout;Landroid/view/View;Landroid/widget/GridLayout$Alignment;IZ)I

    #@11c
    move-result v13

    #@11d
    .line 1116
    .local v13, alignmentOffsetX:I
    add-int v10, v32, v42

    #@11f
    const/4 v11, 0x0

    #@120
    move-object/from16 v6, v16

    #@122
    move-object/from16 v7, p0

    #@124
    move-object v8, v4

    #@125
    invoke-virtual/range {v6 .. v11}, Landroid/widget/GridLayout$Bounds;->getOffset(Landroid/widget/GridLayout;Landroid/view/View;Landroid/widget/GridLayout$Alignment;IZ)I

    #@128
    move-result v14

    #@129
    .line 1118
    .local v14, alignmentOffsetY:I
    sub-int v3, v18, v41

    #@12b
    move/from16 v0, v33

    #@12d
    invoke-virtual {v5, v4, v0, v3}, Landroid/widget/GridLayout$Alignment;->getSizeInCell(Landroid/view/View;II)I

    #@130
    move-result v48

    #@131
    .line 1119
    .local v48, width:I
    sub-int v3, v17, v42

    #@133
    move/from16 v0, v32

    #@135
    invoke-virtual {v9, v4, v0, v3}, Landroid/widget/GridLayout$Alignment;->getSizeInCell(Landroid/view/View;II)I

    #@138
    move-result v27

    #@139
    .line 1121
    .local v27, height:I
    add-int v3, v49, v24

    #@13b
    add-int v23, v3, v13

    #@13d
    .line 1123
    .local v23, dx:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->isLayoutRtl()Z

    #@140
    move-result v3

    #@141
    if-nez v3, :cond_1af

    #@143
    add-int v3, v35, v30

    #@145
    add-int v21, v3, v23

    #@147
    .line 1125
    .local v21, cx:I
    :goto_147
    add-int v3, v37, v51

    #@149
    add-int v3, v3, v25

    #@14b
    add-int/2addr v3, v14

    #@14c
    add-int v22, v3, v45

    #@14e
    .line 1127
    .local v22, cy:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridLayout;->getLayoutMode()I

    #@151
    move-result v3

    #@152
    const/4 v6, 0x1

    #@153
    if-ne v3, v6, :cond_1b8

    #@155
    const/16 v46, 0x1

    #@157
    .line 1128
    .local v46, useLayoutBounds:Z
    :goto_157
    if-eqz v46, :cond_17f

    #@159
    .line 1129
    invoke-virtual {v4}, Landroid/view/View;->getOpticalInsets()Landroid/graphics/Insets;

    #@15c
    move-result-object v29

    #@15d
    .line 1130
    .local v29, insets:Landroid/graphics/Insets;
    move-object/from16 v0, v29

    #@15f
    iget v3, v0, Landroid/graphics/Insets;->left:I

    #@161
    sub-int v21, v21, v3

    #@163
    .line 1131
    move-object/from16 v0, v29

    #@165
    iget v3, v0, Landroid/graphics/Insets;->top:I

    #@167
    sub-int v22, v22, v3

    #@169
    .line 1132
    move-object/from16 v0, v29

    #@16b
    iget v3, v0, Landroid/graphics/Insets;->left:I

    #@16d
    move-object/from16 v0, v29

    #@16f
    iget v6, v0, Landroid/graphics/Insets;->right:I

    #@171
    add-int/2addr v3, v6

    #@172
    add-int v48, v48, v3

    #@174
    .line 1133
    move-object/from16 v0, v29

    #@176
    iget v3, v0, Landroid/graphics/Insets;->top:I

    #@178
    move-object/from16 v0, v29

    #@17a
    iget v6, v0, Landroid/graphics/Insets;->bottom:I

    #@17c
    add-int/2addr v3, v6

    #@17d
    add-int v27, v27, v3

    #@17f
    .line 1135
    .end local v29           #insets:Landroid/graphics/Insets;
    :cond_17f
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@182
    move-result v3

    #@183
    move/from16 v0, v48

    #@185
    if-ne v0, v3, :cond_18f

    #@187
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@18a
    move-result v3

    #@18b
    move/from16 v0, v27

    #@18d
    if-eq v0, v3, :cond_1a2

    #@18f
    .line 1136
    :cond_18f
    const/high16 v3, 0x4000

    #@191
    move/from16 v0, v48

    #@193
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@196
    move-result v3

    #@197
    const/high16 v6, 0x4000

    #@199
    move/from16 v0, v27

    #@19b
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@19e
    move-result v6

    #@19f
    invoke-virtual {v4, v3, v6}, Landroid/view/View;->measure(II)V

    #@1a2
    .line 1138
    :cond_1a2
    add-int v3, v21, v48

    #@1a4
    add-int v6, v22, v27

    #@1a6
    move/from16 v0, v21

    #@1a8
    move/from16 v1, v22

    #@1aa
    invoke-virtual {v4, v0, v1, v3, v6}, Landroid/view/View;->layout(IIII)V

    #@1ad
    goto/16 :goto_57

    #@1af
    .line 1123
    .end local v21           #cx:I
    .end local v22           #cy:I
    .end local v46           #useLayoutBounds:Z
    :cond_1af
    sub-int v3, v44, v48

    #@1b1
    sub-int v3, v3, v36

    #@1b3
    sub-int v3, v3, v38

    #@1b5
    sub-int v21, v3, v23

    #@1b7
    goto :goto_147

    #@1b8
    .line 1127
    .restart local v21       #cx:I
    .restart local v22       #cy:I
    :cond_1b8
    const/16 v46, 0x0

    #@1ba
    goto :goto_157

    #@1bb
    .line 1140
    .end local v2           #boundsX:Landroid/widget/GridLayout$Bounds;
    .end local v4           #c:Landroid/view/View;
    .end local v5           #hAlign:Landroid/widget/GridLayout$Alignment;
    .end local v9           #vAlign:Landroid/widget/GridLayout$Alignment;
    .end local v13           #alignmentOffsetX:I
    .end local v14           #alignmentOffsetY:I
    .end local v15           #bottomMargin:I
    .end local v16           #boundsY:Landroid/widget/GridLayout$Bounds;
    .end local v17           #cellHeight:I
    .end local v18           #cellWidth:I
    .end local v19           #colSpan:Landroid/widget/GridLayout$Interval;
    .end local v20           #columnSpec:Landroid/widget/GridLayout$Spec;
    .end local v21           #cx:I
    .end local v22           #cy:I
    .end local v23           #dx:I
    .end local v24           #gravityOffsetX:I
    .end local v25           #gravityOffsetY:I
    .end local v27           #height:I
    .end local v30           #leftMargin:I
    .end local v31           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v32           #pHeight:I
    .end local v33           #pWidth:I
    .end local v38           #rightMargin:I
    .end local v39           #rowSpan:Landroid/widget/GridLayout$Interval;
    .end local v40           #rowSpec:Landroid/widget/GridLayout$Spec;
    .end local v41           #sumMarginsX:I
    .end local v42           #sumMarginsY:I
    .end local v45           #topMargin:I
    .end local v48           #width:I
    .end local v49           #x1:I
    .end local v50           #x2:I
    .end local v51           #y1:I
    .end local v52           #y2:I
    :cond_1bb
    return-void
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthSpec"
    .parameter "heightSpec"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 983
    invoke-direct {p0}, Landroid/widget/GridLayout;->consistencyCheck()V

    #@4
    .line 987
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateValues()V

    #@7
    .line 989
    const/4 v6, 0x1

    #@8
    invoke-direct {p0, p1, p2, v6}, Landroid/widget/GridLayout;->measureChildrenWithMargins(IIZ)V

    #@b
    .line 994
    iget v6, p0, Landroid/widget/GridLayout;->orientation:I

    #@d
    if-nez v6, :cond_52

    #@f
    .line 995
    iget-object v6, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@11
    invoke-virtual {v6, p1}, Landroid/widget/GridLayout$Axis;->getMeasure(I)I

    #@14
    move-result v5

    #@15
    .line 996
    .local v5, width:I
    invoke-direct {p0, p1, p2, v8}, Landroid/widget/GridLayout;->measureChildrenWithMargins(IIZ)V

    #@18
    .line 997
    iget-object v6, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@1a
    invoke-virtual {v6, p2}, Landroid/widget/GridLayout$Axis;->getMeasure(I)I

    #@1d
    move-result v1

    #@1e
    .line 1004
    .local v1, height:I
    :goto_1e
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingLeft()I

    #@21
    move-result v6

    #@22
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingRight()I

    #@25
    move-result v7

    #@26
    add-int v0, v6, v7

    #@28
    .line 1005
    .local v0, hPadding:I
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingTop()I

    #@2b
    move-result v6

    #@2c
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getPaddingBottom()I

    #@2f
    move-result v7

    #@30
    add-int v4, v6, v7

    #@32
    .line 1007
    .local v4, vPadding:I
    add-int v6, v0, v5

    #@34
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getSuggestedMinimumWidth()I

    #@37
    move-result v7

    #@38
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@3b
    move-result v3

    #@3c
    .line 1008
    .local v3, measuredWidth:I
    add-int v6, v4, v1

    #@3e
    invoke-virtual {p0}, Landroid/widget/GridLayout;->getSuggestedMinimumHeight()I

    #@41
    move-result v7

    #@42
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@45
    move-result v2

    #@46
    .line 1010
    .local v2, measuredHeight:I
    invoke-static {v3, p1, v8}, Landroid/widget/GridLayout;->resolveSizeAndState(III)I

    #@49
    move-result v6

    #@4a
    invoke-static {v2, p2, v8}, Landroid/widget/GridLayout;->resolveSizeAndState(III)I

    #@4d
    move-result v7

    #@4e
    invoke-virtual {p0, v6, v7}, Landroid/widget/GridLayout;->setMeasuredDimension(II)V

    #@51
    .line 1013
    return-void

    #@52
    .line 999
    .end local v0           #hPadding:I
    .end local v1           #height:I
    .end local v2           #measuredHeight:I
    .end local v3           #measuredWidth:I
    .end local v4           #vPadding:I
    .end local v5           #width:I
    :cond_52
    iget-object v6, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@54
    invoke-virtual {v6, p2}, Landroid/widget/GridLayout$Axis;->getMeasure(I)I

    #@57
    move-result v1

    #@58
    .line 1000
    .restart local v1       #height:I
    invoke-direct {p0, p1, p2, v8}, Landroid/widget/GridLayout;->measureChildrenWithMargins(IIZ)V

    #@5b
    .line 1001
    iget-object v6, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@5d
    invoke-virtual {v6, p1}, Landroid/widget/GridLayout$Axis;->getMeasure(I)I

    #@60
    move-result v5

    #@61
    .restart local v5       #width:I
    goto :goto_1e
.end method

.method protected onSetLayoutParams(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "child"
    .parameter "layoutParams"

    #@0
    .prologue
    .line 759
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onSetLayoutParams(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 761
    invoke-virtual {p0, p2}, Landroid/widget/GridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_f

    #@9
    .line 762
    const-string/jumbo v0, "supplied LayoutParams are of the wrong type"

    #@c
    invoke-static {v0}, Landroid/widget/GridLayout;->handleInvalidParams(Ljava/lang/String;)V

    #@f
    .line 765
    :cond_f
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@12
    .line 766
    return-void
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 894
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onViewAdded(Landroid/view/View;)V

    #@3
    .line 895
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@6
    .line 896
    return-void
.end method

.method protected onViewRemoved(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 903
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    #@3
    .line 904
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@6
    .line 905
    return-void
.end method

.method public requestLayout()V
    .registers 1

    #@0
    .prologue
    .line 1033
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@3
    .line 1034
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateValues()V

    #@6
    .line 1035
    return-void
.end method

.method public setAlignmentMode(I)V
    .registers 2
    .parameter "alignmentMode"

    #@0
    .prologue
    .line 483
    iput p1, p0, Landroid/widget/GridLayout;->alignmentMode:I

    #@2
    .line 484
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@5
    .line 485
    return-void
.end method

.method public setColumnCount(I)V
    .registers 3
    .parameter "columnCount"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Axis;->setCount(I)V

    #@5
    .line 402
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@8
    .line 403
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@b
    .line 404
    return-void
.end method

.method public setColumnOrderPreserved(Z)V
    .registers 3
    .parameter "columnOrderPreserved"

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Landroid/widget/GridLayout;->horizontalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Axis;->setOrderPreserved(Z)V

    #@5
    .line 555
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@8
    .line 556
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@b
    .line 557
    return-void
.end method

.method public setOrientation(I)V
    .registers 3
    .parameter "orientation"

    #@0
    .prologue
    .line 333
    iget v0, p0, Landroid/widget/GridLayout;->orientation:I

    #@2
    if-eq v0, p1, :cond_c

    #@4
    .line 334
    iput p1, p0, Landroid/widget/GridLayout;->orientation:I

    #@6
    .line 335
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@9
    .line 336
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@c
    .line 338
    :cond_c
    return-void
.end method

.method public setRowCount(I)V
    .registers 3
    .parameter "rowCount"

    #@0
    .prologue
    .line 368
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Axis;->setCount(I)V

    #@5
    .line 369
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@8
    .line 370
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@b
    .line 371
    return-void
.end method

.method public setRowOrderPreserved(Z)V
    .registers 3
    .parameter "rowOrderPreserved"

    #@0
    .prologue
    .line 518
    iget-object v0, p0, Landroid/widget/GridLayout;->verticalAxis:Landroid/widget/GridLayout$Axis;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Axis;->setOrderPreserved(Z)V

    #@5
    .line 519
    invoke-direct {p0}, Landroid/widget/GridLayout;->invalidateStructure()V

    #@8
    .line 520
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@b
    .line 521
    return-void
.end method

.method public setUseDefaultMargins(Z)V
    .registers 2
    .parameter "useDefaultMargins"

    #@0
    .prologue
    .line 447
    iput-boolean p1, p0, Landroid/widget/GridLayout;->useDefaultMargins:Z

    #@2
    .line 448
    invoke-virtual {p0}, Landroid/widget/GridLayout;->requestLayout()V

    #@5
    .line 449
    return-void
.end method
