.class public Landroid/widget/RelativeLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "RelativeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RelativeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public alignWithParent:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mBottom:I

.field private mEnd:I

.field private mInitialRules:[I

.field private mLeft:I

.field private mRight:I

.field private mRules:[I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        indexMapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "above"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "alignBaseline"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x8
                to = "alignBottom"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "alignLeft"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xc
                to = "alignParentBottom"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x9
                to = "alignParentLeft"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xb
                to = "alignParentRight"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xa
                to = "alignParentTop"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7
                to = "alignRight"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x6
                to = "alignTop"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "below"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xe
                to = "centerHorizontal"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xd
                to = "center"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0xf
                to = "centerVertical"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "leftOf"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "rightOf"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x12
                to = "alignStart"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x13
                to = "alignEnd"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x14
                to = "alignParentStart"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x15
                to = "alignParentEnd"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x10
                to = "startOf"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x11
                to = "endOf"
            .end subannotation
        }
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x1
                to = "true"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "false/NO_ID"
            .end subannotation
        }
        resolveId = true
    .end annotation
.end field

.field private mRulesChanged:Z

.field private mStart:I

.field private mTop:I


# direct methods
.method public constructor <init>(II)V
    .registers 6
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    const/16 v2, 0x16

    #@2
    const/high16 v1, -0x8000

    #@4
    .line 1322
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@7
    .line 1154
    new-array v0, v2, [I

    #@9
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@b
    .line 1183
    new-array v0, v2, [I

    #@d
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@f
    .line 1187
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@11
    .line 1188
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@13
    .line 1190
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@16
    .line 1323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 16
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v12, 0x10

    #@2
    const/high16 v10, -0x8000

    #@4
    const/16 v11, 0x16

    #@6
    const/4 v8, -0x1

    #@7
    const/4 v9, 0x0

    #@8
    .line 1200
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@b
    .line 1154
    new-array v7, v11, [I

    #@d
    iput-object v7, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@f
    .line 1183
    new-array v7, v11, [I

    #@11
    iput-object v7, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@13
    .line 1187
    iput v10, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@15
    .line 1188
    iput v10, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@17
    .line 1190
    iput-boolean v9, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@19
    .line 1202
    sget-object v7, Lcom/android/internal/R$styleable;->RelativeLayout_Layout:[I

    #@1b
    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@1e
    move-result-object v1

    #@1f
    .line 1205
    .local v1, a:Landroid/content/res/TypedArray;
    iget-object v6, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@21
    .line 1206
    .local v6, rules:[I
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@23
    .line 1208
    .local v4, initialRules:[I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@26
    move-result v0

    #@27
    .line 1209
    .local v0, N:I
    const/4 v3, 0x0

    #@28
    .local v3, i:I
    :goto_28
    if-ge v3, v0, :cond_196

    #@2a
    .line 1210
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@2d
    move-result v2

    #@2e
    .line 1211
    .local v2, attr:I
    packed-switch v2, :pswitch_data_1a4

    #@31
    .line 1209
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_28

    #@34
    .line 1213
    :pswitch_34
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@37
    move-result v7

    #@38
    iput-boolean v7, p0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    #@3a
    goto :goto_31

    #@3b
    .line 1216
    :pswitch_3b
    invoke-static {}, Landroid/widget/RelativeLayout;->access$700()Z

    #@3e
    move-result v7

    #@3f
    if-eqz v7, :cond_48

    #@41
    .line 1217
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@44
    move-result v7

    #@45
    aput v7, v6, v12

    #@47
    goto :goto_31

    #@48
    .line 1220
    :cond_48
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4b
    move-result v7

    #@4c
    aput v7, v6, v9

    #@4e
    goto :goto_31

    #@4f
    .line 1224
    :pswitch_4f
    invoke-static {}, Landroid/widget/RelativeLayout;->access$800()Z

    #@52
    move-result v7

    #@53
    if-eqz v7, :cond_5e

    #@55
    .line 1225
    const/16 v7, 0x11

    #@57
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5a
    move-result v10

    #@5b
    aput v10, v6, v7

    #@5d
    goto :goto_31

    #@5e
    .line 1228
    :cond_5e
    const/4 v7, 0x1

    #@5f
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@62
    move-result v10

    #@63
    aput v10, v6, v7

    #@65
    goto :goto_31

    #@66
    .line 1232
    :pswitch_66
    const/4 v7, 0x2

    #@67
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@6a
    move-result v10

    #@6b
    aput v10, v6, v7

    #@6d
    goto :goto_31

    #@6e
    .line 1235
    :pswitch_6e
    const/4 v7, 0x3

    #@6f
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@72
    move-result v10

    #@73
    aput v10, v6, v7

    #@75
    goto :goto_31

    #@76
    .line 1238
    :pswitch_76
    const/4 v7, 0x4

    #@77
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@7a
    move-result v10

    #@7b
    aput v10, v6, v7

    #@7d
    goto :goto_31

    #@7e
    .line 1241
    :pswitch_7e
    invoke-static {}, Landroid/widget/RelativeLayout;->access$900()Z

    #@81
    move-result v7

    #@82
    if-eqz v7, :cond_8d

    #@84
    .line 1242
    const/16 v7, 0x12

    #@86
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@89
    move-result v10

    #@8a
    aput v10, v6, v7

    #@8c
    goto :goto_31

    #@8d
    .line 1245
    :cond_8d
    const/4 v7, 0x5

    #@8e
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@91
    move-result v10

    #@92
    aput v10, v6, v7

    #@94
    goto :goto_31

    #@95
    .line 1249
    :pswitch_95
    const/4 v7, 0x6

    #@96
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@99
    move-result v10

    #@9a
    aput v10, v6, v7

    #@9c
    goto :goto_31

    #@9d
    .line 1252
    :pswitch_9d
    invoke-static {}, Landroid/widget/RelativeLayout;->access$1000()Z

    #@a0
    move-result v7

    #@a1
    if-eqz v7, :cond_ac

    #@a3
    .line 1253
    const/16 v7, 0x13

    #@a5
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@a8
    move-result v10

    #@a9
    aput v10, v6, v7

    #@ab
    goto :goto_31

    #@ac
    .line 1256
    :cond_ac
    const/4 v7, 0x7

    #@ad
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@b0
    move-result v10

    #@b1
    aput v10, v6, v7

    #@b3
    goto/16 :goto_31

    #@b5
    .line 1260
    :pswitch_b5
    const/16 v7, 0x8

    #@b7
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@ba
    move-result v10

    #@bb
    aput v10, v6, v7

    #@bd
    goto/16 :goto_31

    #@bf
    .line 1263
    :pswitch_bf
    invoke-static {}, Landroid/widget/RelativeLayout;->access$1100()Z

    #@c2
    move-result v7

    #@c3
    if-eqz v7, :cond_d4

    #@c5
    .line 1264
    const/16 v10, 0x14

    #@c7
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ca
    move-result v7

    #@cb
    if-eqz v7, :cond_d2

    #@cd
    move v7, v8

    #@ce
    :goto_ce
    aput v7, v6, v10

    #@d0
    goto/16 :goto_31

    #@d2
    :cond_d2
    move v7, v9

    #@d3
    goto :goto_ce

    #@d4
    .line 1267
    :cond_d4
    const/16 v10, 0x9

    #@d6
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@d9
    move-result v7

    #@da
    if-eqz v7, :cond_e1

    #@dc
    move v7, v8

    #@dd
    :goto_dd
    aput v7, v6, v10

    #@df
    goto/16 :goto_31

    #@e1
    :cond_e1
    move v7, v9

    #@e2
    goto :goto_dd

    #@e3
    .line 1271
    :pswitch_e3
    const/16 v10, 0xa

    #@e5
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@e8
    move-result v7

    #@e9
    if-eqz v7, :cond_f0

    #@eb
    move v7, v8

    #@ec
    :goto_ec
    aput v7, v6, v10

    #@ee
    goto/16 :goto_31

    #@f0
    :cond_f0
    move v7, v9

    #@f1
    goto :goto_ec

    #@f2
    .line 1274
    :pswitch_f2
    invoke-static {}, Landroid/widget/RelativeLayout;->access$1200()Z

    #@f5
    move-result v7

    #@f6
    if-eqz v7, :cond_107

    #@f8
    .line 1275
    const/16 v10, 0x15

    #@fa
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@fd
    move-result v7

    #@fe
    if-eqz v7, :cond_105

    #@100
    move v7, v8

    #@101
    :goto_101
    aput v7, v6, v10

    #@103
    goto/16 :goto_31

    #@105
    :cond_105
    move v7, v9

    #@106
    goto :goto_101

    #@107
    .line 1278
    :cond_107
    const/16 v10, 0xb

    #@109
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@10c
    move-result v7

    #@10d
    if-eqz v7, :cond_114

    #@10f
    move v7, v8

    #@110
    :goto_110
    aput v7, v6, v10

    #@112
    goto/16 :goto_31

    #@114
    :cond_114
    move v7, v9

    #@115
    goto :goto_110

    #@116
    .line 1282
    :pswitch_116
    const/16 v10, 0xc

    #@118
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@11b
    move-result v7

    #@11c
    if-eqz v7, :cond_123

    #@11e
    move v7, v8

    #@11f
    :goto_11f
    aput v7, v6, v10

    #@121
    goto/16 :goto_31

    #@123
    :cond_123
    move v7, v9

    #@124
    goto :goto_11f

    #@125
    .line 1285
    :pswitch_125
    const/16 v10, 0xd

    #@127
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@12a
    move-result v7

    #@12b
    if-eqz v7, :cond_132

    #@12d
    move v7, v8

    #@12e
    :goto_12e
    aput v7, v6, v10

    #@130
    goto/16 :goto_31

    #@132
    :cond_132
    move v7, v9

    #@133
    goto :goto_12e

    #@134
    .line 1288
    :pswitch_134
    const/16 v10, 0xe

    #@136
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@139
    move-result v7

    #@13a
    if-eqz v7, :cond_141

    #@13c
    move v7, v8

    #@13d
    :goto_13d
    aput v7, v6, v10

    #@13f
    goto/16 :goto_31

    #@141
    :cond_141
    move v7, v9

    #@142
    goto :goto_13d

    #@143
    .line 1291
    :pswitch_143
    const/16 v10, 0xf

    #@145
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@148
    move-result v7

    #@149
    if-eqz v7, :cond_150

    #@14b
    move v7, v8

    #@14c
    :goto_14c
    aput v7, v6, v10

    #@14e
    goto/16 :goto_31

    #@150
    :cond_150
    move v7, v9

    #@151
    goto :goto_14c

    #@152
    .line 1294
    :pswitch_152
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@155
    move-result v7

    #@156
    aput v7, v6, v12

    #@158
    goto/16 :goto_31

    #@15a
    .line 1297
    :pswitch_15a
    const/16 v7, 0x11

    #@15c
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@15f
    move-result v10

    #@160
    aput v10, v6, v7

    #@162
    goto/16 :goto_31

    #@164
    .line 1300
    :pswitch_164
    const/16 v7, 0x12

    #@166
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@169
    move-result v10

    #@16a
    aput v10, v6, v7

    #@16c
    goto/16 :goto_31

    #@16e
    .line 1303
    :pswitch_16e
    const/16 v7, 0x13

    #@170
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@173
    move-result v10

    #@174
    aput v10, v6, v7

    #@176
    goto/16 :goto_31

    #@178
    .line 1306
    :pswitch_178
    const/16 v10, 0x14

    #@17a
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@17d
    move-result v7

    #@17e
    if-eqz v7, :cond_185

    #@180
    move v7, v8

    #@181
    :goto_181
    aput v7, v6, v10

    #@183
    goto/16 :goto_31

    #@185
    :cond_185
    move v7, v9

    #@186
    goto :goto_181

    #@187
    .line 1309
    :pswitch_187
    const/16 v10, 0x15

    #@189
    invoke-virtual {v1, v2, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@18c
    move-result v7

    #@18d
    if-eqz v7, :cond_194

    #@18f
    move v7, v8

    #@190
    :goto_190
    aput v7, v6, v10

    #@192
    goto/16 :goto_31

    #@194
    :cond_194
    move v7, v9

    #@195
    goto :goto_190

    #@196
    .line 1314
    .end local v2           #attr:I
    :cond_196
    const/4 v5, 0x0

    #@197
    .local v5, n:I
    :goto_197
    if-ge v5, v11, :cond_1a0

    #@199
    .line 1315
    aget v7, v6, v5

    #@19b
    aput v7, v4, v5

    #@19d
    .line 1314
    add-int/lit8 v5, v5, 0x1

    #@19f
    goto :goto_197

    #@1a0
    .line 1318
    :cond_1a0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@1a3
    .line 1319
    return-void

    #@1a4
    .line 1211
    :pswitch_data_1a4
    .packed-switch 0x0
        :pswitch_3b
        :pswitch_4f
        :pswitch_66
        :pswitch_6e
        :pswitch_76
        :pswitch_7e
        :pswitch_95
        :pswitch_9d
        :pswitch_b5
        :pswitch_bf
        :pswitch_e3
        :pswitch_f2
        :pswitch_116
        :pswitch_125
        :pswitch_134
        :pswitch_143
        :pswitch_34
        :pswitch_152
        :pswitch_15a
        :pswitch_164
        :pswitch_16e
        :pswitch_178
        :pswitch_187
    .end packed-switch
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    const/16 v2, 0x16

    #@2
    const/high16 v1, -0x8000

    #@4
    .line 1329
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@7
    .line 1154
    new-array v0, v2, [I

    #@9
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@b
    .line 1183
    new-array v0, v2, [I

    #@d
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@f
    .line 1187
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@11
    .line 1188
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@13
    .line 1190
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@16
    .line 1330
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    const/16 v2, 0x16

    #@2
    const/high16 v1, -0x8000

    #@4
    .line 1336
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@7
    .line 1154
    new-array v0, v2, [I

    #@9
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@b
    .line 1183
    new-array v0, v2, [I

    #@d
    iput-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@f
    .line 1187
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@11
    .line 1188
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@13
    .line 1190
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@16
    .line 1337
    return-void
.end method

.method static synthetic access$100(Landroid/widget/RelativeLayout$LayoutParams;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iput p1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@2
    return p1
.end method

.method static synthetic access$112(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@5
    return v0
.end method

.method static synthetic access$1500(Landroid/widget/RelativeLayout$LayoutParams;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1153
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/RelativeLayout$LayoutParams;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mBottom:I

    #@2
    return v0
.end method

.method static synthetic access$202(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iput p1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mBottom:I

    #@2
    return p1
.end method

.method static synthetic access$212(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mBottom:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mBottom:I

    #@5
    return v0
.end method

.method static synthetic access$300(Landroid/widget/RelativeLayout$LayoutParams;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iput p1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@2
    return p1
.end method

.method static synthetic access$312(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@5
    return v0
.end method

.method static synthetic access$400(Landroid/widget/RelativeLayout$LayoutParams;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mTop:I

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iput p1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mTop:I

    #@2
    return p1
.end method

.method static synthetic access$412(Landroid/widget/RelativeLayout$LayoutParams;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1153
    iget v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mTop:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mTop:I

    #@5
    return v0
.end method

.method private hasRelativeRules()Z
    .registers 3

    #@0
    .prologue
    .line 1435
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@2
    const/16 v1, 0x10

    #@4
    aget v0, v0, v1

    #@6
    if-nez v0, :cond_30

    #@8
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@a
    const/16 v1, 0x11

    #@c
    aget v0, v0, v1

    #@e
    if-nez v0, :cond_30

    #@10
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@12
    const/16 v1, 0x12

    #@14
    aget v0, v0, v1

    #@16
    if-nez v0, :cond_30

    #@18
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@1a
    const/16 v1, 0x13

    #@1c
    aget v0, v0, v1

    #@1e
    if-nez v0, :cond_30

    #@20
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@22
    const/16 v1, 0x14

    #@24
    aget v0, v0, v1

    #@26
    if-nez v0, :cond_30

    #@28
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@2a
    const/16 v1, 0x15

    #@2c
    aget v0, v0, v1

    #@2e
    if-eqz v0, :cond_32

    #@30
    :cond_30
    const/4 v0, 0x1

    #@31
    :goto_31
    return v0

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_31
.end method

.method private resolveRules(I)V
    .registers 13
    .parameter "layoutDirection"

    #@0
    .prologue
    const/16 v7, 0x9

    #@2
    const/4 v5, 0x7

    #@3
    const/4 v6, 0x5

    #@4
    const/4 v2, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 1441
    if-ne p1, v2, :cond_19

    #@8
    move v0, v2

    #@9
    .line 1443
    .local v0, isLayoutRtl:Z
    :goto_9
    const/4 v1, 0x0

    #@a
    .local v1, n:I
    :goto_a
    const/16 v4, 0x16

    #@c
    if-ge v1, v4, :cond_1b

    #@e
    .line 1444
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@10
    iget-object v8, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@12
    aget v8, v8, v1

    #@14
    aput v8, v4, v1

    #@16
    .line 1443
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_a

    #@19
    .end local v0           #isLayoutRtl:Z
    .end local v1           #n:I
    :cond_19
    move v0, v3

    #@1a
    .line 1441
    goto :goto_9

    #@1b
    .line 1447
    .restart local v0       #isLayoutRtl:Z
    .restart local v1       #n:I
    :cond_1b
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@1d
    const/16 v8, 0x12

    #@1f
    aget v4, v4, v8

    #@21
    if-eqz v4, :cond_30

    #@23
    .line 1448
    iget-object v8, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@25
    if-eqz v0, :cond_9b

    #@27
    move v4, v5

    #@28
    :goto_28
    iget-object v9, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@2a
    const/16 v10, 0x12

    #@2c
    aget v9, v9, v10

    #@2e
    aput v9, v8, v4

    #@30
    .line 1450
    :cond_30
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@32
    const/16 v8, 0x13

    #@34
    aget v4, v4, v8

    #@36
    if-eqz v4, :cond_44

    #@38
    .line 1451
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@3a
    if-eqz v0, :cond_9d

    #@3c
    :goto_3c
    iget-object v5, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@3e
    const/16 v8, 0x13

    #@40
    aget v5, v5, v8

    #@42
    aput v5, v4, v6

    #@44
    .line 1453
    :cond_44
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@46
    const/16 v5, 0x10

    #@48
    aget v4, v4, v5

    #@4a
    if-eqz v4, :cond_59

    #@4c
    .line 1454
    iget-object v5, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@4e
    if-eqz v0, :cond_9f

    #@50
    move v4, v2

    #@51
    :goto_51
    iget-object v6, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@53
    const/16 v8, 0x10

    #@55
    aget v6, v6, v8

    #@57
    aput v6, v5, v4

    #@59
    .line 1456
    :cond_59
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@5b
    const/16 v5, 0x11

    #@5d
    aget v4, v4, v5

    #@5f
    if-eqz v4, :cond_6e

    #@61
    .line 1457
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@63
    if-eqz v0, :cond_66

    #@65
    move v2, v3

    #@66
    :cond_66
    iget-object v5, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@68
    const/16 v6, 0x11

    #@6a
    aget v5, v5, v6

    #@6c
    aput v5, v4, v2

    #@6e
    .line 1459
    :cond_6e
    iget-object v2, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@70
    const/16 v4, 0x14

    #@72
    aget v2, v2, v4

    #@74
    if-eqz v2, :cond_84

    #@76
    .line 1460
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@78
    if-eqz v0, :cond_a1

    #@7a
    const/16 v2, 0xb

    #@7c
    :goto_7c
    iget-object v5, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@7e
    const/16 v6, 0x14

    #@80
    aget v5, v5, v6

    #@82
    aput v5, v4, v2

    #@84
    .line 1462
    :cond_84
    iget-object v2, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@86
    const/16 v4, 0x15

    #@88
    aget v2, v2, v4

    #@8a
    if-eqz v2, :cond_98

    #@8c
    .line 1463
    iget-object v2, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@8e
    if-eqz v0, :cond_a3

    #@90
    :goto_90
    iget-object v4, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@92
    const/16 v5, 0x15

    #@94
    aget v4, v4, v5

    #@96
    aput v4, v2, v7

    #@98
    .line 1465
    :cond_98
    iput-boolean v3, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@9a
    .line 1466
    return-void

    #@9b
    :cond_9b
    move v4, v6

    #@9c
    .line 1448
    goto :goto_28

    #@9d
    :cond_9d
    move v6, v5

    #@9e
    .line 1451
    goto :goto_3c

    #@9f
    :cond_9f
    move v4, v3

    #@a0
    .line 1454
    goto :goto_51

    #@a1
    :cond_a1
    move v2, v7

    #@a2
    .line 1460
    goto :goto_7c

    #@a3
    .line 1463
    :cond_a3
    const/16 v7, 0xb

    #@a5
    goto :goto_90
.end method


# virtual methods
.method public addRule(I)V
    .registers 4
    .parameter "verb"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1358
    invoke-static {}, Landroid/widget/RelativeLayout;->access$1300()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 1359
    packed-switch p1, :pswitch_data_1c

    #@a
    .line 1370
    :cond_a
    :goto_a
    :pswitch_a
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@c
    aput v1, v0, p1

    #@e
    .line 1371
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@10
    aput v1, v0, p1

    #@12
    .line 1372
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@15
    .line 1373
    return-void

    #@16
    .line 1361
    :pswitch_16
    const/16 p1, 0x14

    #@18
    .line 1362
    goto :goto_a

    #@19
    .line 1364
    :pswitch_19
    const/16 p1, 0x15

    #@1b
    .line 1365
    goto :goto_a

    #@1c
    .line 1359
    :pswitch_data_1c
    .packed-switch 0x9
        :pswitch_16
        :pswitch_a
        :pswitch_19
    .end packed-switch
.end method

.method public addRule(II)V
    .registers 4
    .parameter "verb"
    .parameter "anchor"

    #@0
    .prologue
    .line 1390
    invoke-static {}, Landroid/widget/RelativeLayout;->access$1400()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 1391
    packed-switch p1, :pswitch_data_28

    #@9
    .line 1414
    :cond_9
    :goto_9
    :pswitch_9
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@b
    aput p2, v0, p1

    #@d
    .line 1415
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@f
    aput p2, v0, p1

    #@11
    .line 1416
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@14
    .line 1417
    return-void

    #@15
    .line 1393
    :pswitch_15
    const/16 p1, 0x10

    #@17
    .line 1394
    goto :goto_9

    #@18
    .line 1396
    :pswitch_18
    const/16 p1, 0x11

    #@1a
    .line 1397
    goto :goto_9

    #@1b
    .line 1399
    :pswitch_1b
    const/16 p1, 0x12

    #@1d
    .line 1400
    goto :goto_9

    #@1e
    .line 1402
    :pswitch_1e
    const/16 p1, 0x13

    #@20
    .line 1403
    goto :goto_9

    #@21
    .line 1405
    :pswitch_21
    const/16 p1, 0x14

    #@23
    .line 1406
    goto :goto_9

    #@24
    .line 1408
    :pswitch_24
    const/16 p1, 0x15

    #@26
    .line 1409
    goto :goto_9

    #@27
    .line 1391
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_15
        :pswitch_18
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_1b
        :pswitch_9
        :pswitch_1e
        :pswitch_9
        :pswitch_21
        :pswitch_9
        :pswitch_24
    .end packed-switch
.end method

.method public debug(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "output"

    #@0
    .prologue
    .line 1341
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "ViewGroup.LayoutParams={ width="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@11
    invoke-static {v1}, Landroid/widget/RelativeLayout$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", height="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@21
    invoke-static {v1}, Landroid/widget/RelativeLayout$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " }"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method

.method public getRules()[I
    .registers 2

    #@0
    .prologue
    .line 1502
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@2
    return-object v0
.end method

.method public getRules(I)[I
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 1483
    invoke-direct {p0}, Landroid/widget/RelativeLayout$LayoutParams;->hasRelativeRules()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1c

    #@6
    iget-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@8
    if-nez v0, :cond_10

    #@a
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$LayoutParams;->getLayoutDirection()I

    #@d
    move-result v0

    #@e
    if-eq p1, v0, :cond_1c

    #@10
    .line 1485
    :cond_10
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->resolveRules(I)V

    #@13
    .line 1486
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$LayoutParams;->getLayoutDirection()I

    #@16
    move-result v0

    #@17
    if-eq p1, v0, :cond_1c

    #@19
    .line 1487
    invoke-virtual {p0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->setLayoutDirection(I)V

    #@1c
    .line 1490
    :cond_1c
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@1e
    return-object v0
.end method

.method public removeRule(I)V
    .registers 4
    .parameter "verb"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1429
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRules:[I

    #@3
    aput v1, v0, p1

    #@5
    .line 1430
    iget-object v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mInitialRules:[I

    #@7
    aput v1, v0, p1

    #@9
    .line 1431
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRulesChanged:Z

    #@c
    .line 1432
    return-void
.end method

.method public resolveLayoutDirection(I)V
    .registers 5
    .parameter "layoutDirection"

    #@0
    .prologue
    const/high16 v2, -0x8000

    #@2
    .line 1507
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$LayoutParams;->isLayoutRtl()Z

    #@5
    move-result v0

    #@6
    .line 1508
    .local v0, isLayoutRtl:Z
    if-eqz v0, :cond_2b

    #@8
    .line 1509
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@a
    if-eq v1, v2, :cond_10

    #@c
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@e
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@10
    .line 1510
    :cond_10
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@12
    if-eq v1, v2, :cond_18

    #@14
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@16
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@18
    .line 1516
    :cond_18
    :goto_18
    invoke-direct {p0}, Landroid/widget/RelativeLayout$LayoutParams;->hasRelativeRules()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_27

    #@1e
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$LayoutParams;->getLayoutDirection()I

    #@21
    move-result v1

    #@22
    if-eq p1, v1, :cond_27

    #@24
    .line 1517
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout$LayoutParams;->resolveRules(I)V

    #@27
    .line 1520
    :cond_27
    invoke-super {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->resolveLayoutDirection(I)V

    #@2a
    .line 1521
    return-void

    #@2b
    .line 1512
    :cond_2b
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@2d
    if-eq v1, v2, :cond_33

    #@2f
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mStart:I

    #@31
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mLeft:I

    #@33
    .line 1513
    :cond_33
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@35
    if-eq v1, v2, :cond_18

    #@37
    iget v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mEnd:I

    #@39
    iput v1, p0, Landroid/widget/RelativeLayout$LayoutParams;->mRight:I

    #@3b
    goto :goto_18
.end method
