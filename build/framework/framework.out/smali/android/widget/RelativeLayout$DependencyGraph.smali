.class Landroid/widget/RelativeLayout$DependencyGraph;
.super Ljava/lang/Object;
.source "RelativeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RelativeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DependencyGraph"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RelativeLayout$DependencyGraph$Node;
    }
.end annotation


# instance fields
.field private mKeyNodes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/RelativeLayout$DependencyGraph$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/RelativeLayout$DependencyGraph$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mRoots:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Landroid/widget/RelativeLayout$DependencyGraph$Node;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1524
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1528
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mNodes:Ljava/util/ArrayList;

    #@a
    .line 1534
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mKeyNodes:Landroid/util/SparseArray;

    #@11
    .line 1540
    new-instance v0, Ljava/util/ArrayDeque;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    #@16
    iput-object v0, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mRoots:Ljava/util/ArrayDeque;

    #@18
    .line 1734
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/RelativeLayout$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1524
    invoke-direct {p0}, Landroid/widget/RelativeLayout$DependencyGraph;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/widget/RelativeLayout$DependencyGraph;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1524
    iget-object v0, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mKeyNodes:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method private static appendViewId(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V
    .registers 5
    .parameter "resources"
    .parameter "node"
    .parameter "buffer"

    #@0
    .prologue
    .line 1695
    iget-object v0, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@2
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    #@5
    move-result v0

    #@6
    const/4 v1, -0x1

    #@7
    if-eq v0, v1, :cond_17

    #@9
    .line 1696
    iget-object v0, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@b
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    #@e
    move-result v0

    #@f
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 1700
    :goto_16
    return-void

    #@17
    .line 1698
    :cond_17
    const-string v0, "NO_ID"

    #@19
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    goto :goto_16
.end method

.method private findRoots([I)Ljava/util/ArrayDeque;
    .registers 15
    .parameter "rulesFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/ArrayDeque",
            "<",
            "Landroid/widget/RelativeLayout$DependencyGraph$Node;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1622
    iget-object v4, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mKeyNodes:Landroid/util/SparseArray;

    #@2
    .line 1623
    .local v4, keyNodes:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    iget-object v7, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mNodes:Ljava/util/ArrayList;

    #@4
    .line 1624
    .local v7, nodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    .line 1628
    .local v0, count:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_1e

    #@b
    .line 1629
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v6

    #@f
    check-cast v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@11
    .line 1630
    .local v6, node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    iget-object v12, v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@13
    invoke-virtual {v12}, Ljava/util/HashMap;->clear()V

    #@16
    .line 1631
    iget-object v12, v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependencies:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v12}, Landroid/util/SparseArray;->clear()V

    #@1b
    .line 1628
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_9

    #@1e
    .line 1635
    .end local v6           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_1e
    const/4 v2, 0x0

    #@1f
    :goto_1f
    if-ge v2, v0, :cond_58

    #@21
    .line 1636
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v6

    #@25
    check-cast v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@27
    .line 1638
    .restart local v6       #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    iget-object v12, v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@29
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2c
    move-result-object v5

    #@2d
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    #@2f
    .line 1639
    .local v5, layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Landroid/widget/RelativeLayout$LayoutParams;->access$1500(Landroid/widget/RelativeLayout$LayoutParams;)[I

    #@32
    move-result-object v10

    #@33
    .line 1640
    .local v10, rules:[I
    array-length v11, p1

    #@34
    .line 1644
    .local v11, rulesCount:I
    const/4 v3, 0x0

    #@35
    .local v3, j:I
    :goto_35
    if-ge v3, v11, :cond_55

    #@37
    .line 1645
    aget v12, p1, v3

    #@39
    aget v9, v10, v12

    #@3b
    .line 1646
    .local v9, rule:I
    if-lez v9, :cond_47

    #@3d
    .line 1648
    invoke-virtual {v4, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v1

    #@41
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@43
    .line 1650
    .local v1, dependency:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    if-eqz v1, :cond_47

    #@45
    if-ne v1, v6, :cond_4a

    #@47
    .line 1644
    .end local v1           #dependency:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_47
    :goto_47
    add-int/lit8 v3, v3, 0x1

    #@49
    goto :goto_35

    #@4a
    .line 1654
    .restart local v1       #dependency:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_4a
    iget-object v12, v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@4c
    invoke-virtual {v12, v6, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    .line 1656
    iget-object v12, v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependencies:Landroid/util/SparseArray;

    #@51
    invoke-virtual {v12, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@54
    goto :goto_47

    #@55
    .line 1635
    .end local v1           #dependency:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    .end local v9           #rule:I
    :cond_55
    add-int/lit8 v2, v2, 0x1

    #@57
    goto :goto_1f

    #@58
    .line 1661
    .end local v3           #j:I
    .end local v5           #layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    .end local v10           #rules:[I
    .end local v11           #rulesCount:I
    :cond_58
    iget-object v8, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mRoots:Ljava/util/ArrayDeque;

    #@5a
    .line 1662
    .local v8, roots:Ljava/util/ArrayDeque;,"Ljava/util/ArrayDeque<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    invoke-virtual {v8}, Ljava/util/ArrayDeque;->clear()V

    #@5d
    .line 1665
    const/4 v2, 0x0

    #@5e
    :goto_5e
    if-ge v2, v0, :cond_74

    #@60
    .line 1666
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@63
    move-result-object v6

    #@64
    check-cast v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@66
    .line 1667
    .restart local v6       #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    iget-object v12, v6, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependencies:Landroid/util/SparseArray;

    #@68
    invoke-virtual {v12}, Landroid/util/SparseArray;->size()I

    #@6b
    move-result v12

    #@6c
    if-nez v12, :cond_71

    #@6e
    invoke-virtual {v8, v6}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    #@71
    .line 1665
    :cond_71
    add-int/lit8 v2, v2, 0x1

    #@73
    goto :goto_5e

    #@74
    .line 1670
    .end local v6           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_74
    return-object v8
.end method

.method private static printNode(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;)V
    .registers 6
    .parameter "resources"
    .parameter "node"

    #@0
    .prologue
    .line 1703
    iget-object v3, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_e

    #@8
    .line 1704
    iget-object v3, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@a
    invoke-static {p0, v3}, Landroid/widget/RelativeLayout$DependencyGraph;->printViewId(Landroid/content/res/Resources;Landroid/view/View;)V

    #@d
    .line 1712
    :cond_d
    return-void

    #@e
    .line 1706
    :cond_e
    iget-object v3, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@10
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@13
    move-result-object v3

    #@14
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v2

    #@18
    .local v2, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_d

    #@1e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@24
    .line 1707
    .local v1, dependent:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    .line 1708
    .local v0, buffer:Ljava/lang/StringBuilder;
    invoke-static {p0, p1, v0}, Landroid/widget/RelativeLayout$DependencyGraph;->appendViewId(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V

    #@2c
    .line 1709
    invoke-static {p0, v1, v0}, Landroid/widget/RelativeLayout$DependencyGraph;->printdependents(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V

    #@2f
    goto :goto_18
.end method

.method static printViewId(Landroid/content/res/Resources;Landroid/view/View;)V
    .registers 4
    .parameter "resources"
    .parameter "view"

    #@0
    .prologue
    .line 1687
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-eq v0, v1, :cond_15

    #@7
    .line 1688
    const-string v0, "RelativeLayout"

    #@9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 1692
    :goto_14
    return-void

    #@15
    .line 1690
    :cond_15
    const-string v0, "RelativeLayout"

    #@17
    const-string v1, "NO_ID"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_14
.end method

.method private static printdependents(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter "resources"
    .parameter "node"
    .parameter "buffer"

    #@0
    .prologue
    .line 1715
    const-string v3, " -> "

    #@2
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 1716
    invoke-static {p0, p1, p2}, Landroid/widget/RelativeLayout$DependencyGraph;->appendViewId(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V

    #@8
    .line 1718
    iget-object v3, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@a
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_1a

    #@10
    .line 1719
    const-string v3, "RelativeLayout"

    #@12
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1726
    :cond_19
    return-void

    #@1a
    .line 1721
    :cond_1a
    iget-object v3, p1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@1c
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@1f
    move-result-object v3

    #@20
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v1

    #@24
    .local v1, i$:Ljava/util/Iterator;
    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_19

    #@2a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    check-cast v0, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@30
    .line 1722
    .local v0, dependent:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@35
    .line 1723
    .local v2, subBuffer:Ljava/lang/StringBuilder;
    invoke-static {p0, v0, v2}, Landroid/widget/RelativeLayout$DependencyGraph;->printdependents(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;Ljava/lang/StringBuilder;)V

    #@38
    goto :goto_24
.end method


# virtual methods
.method add(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 1564
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@3
    move-result v0

    #@4
    .line 1565
    .local v0, id:I
    invoke-static {p1}, Landroid/widget/RelativeLayout$DependencyGraph$Node;->acquire(Landroid/view/View;)Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@7
    move-result-object v1

    #@8
    .line 1567
    .local v1, node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    const/4 v2, -0x1

    #@9
    if-eq v0, v2, :cond_10

    #@b
    .line 1568
    iget-object v2, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mKeyNodes:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@10
    .line 1571
    :cond_10
    iget-object v2, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mNodes:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 1572
    return-void
.end method

.method clear()V
    .registers 5

    #@0
    .prologue
    .line 1546
    iget-object v2, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mNodes:Ljava/util/ArrayList;

    #@2
    .line 1547
    .local v2, nodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 1549
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 1550
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@f
    invoke-virtual {v3}, Landroid/widget/RelativeLayout$DependencyGraph$Node;->release()V

    #@12
    .line 1549
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_7

    #@15
    .line 1552
    :cond_15
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@18
    .line 1554
    iget-object v3, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mKeyNodes:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    #@1d
    .line 1555
    iget-object v3, p0, Landroid/widget/RelativeLayout$DependencyGraph;->mRoots:Ljava/util/ArrayDeque;

    #@1f
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->clear()V

    #@22
    .line 1556
    return-void
.end method

.method varargs getSortedViews([Landroid/view/View;[I)V
    .registers 15
    .parameter "sorted"
    .parameter "rules"

    #@0
    .prologue
    .line 1585
    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout$DependencyGraph;->findRoots([I)Ljava/util/ArrayDeque;

    #@3
    move-result-object v8

    #@4
    .line 1586
    .local v8, roots:Ljava/util/ArrayDeque;,"Ljava/util/ArrayDeque<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    const/4 v4, 0x0

    #@5
    .line 1589
    .local v4, index:I
    :goto_5
    invoke-virtual {v8}, Ljava/util/ArrayDeque;->pollLast()Ljava/lang/Object;

    #@8
    move-result-object v7

    #@9
    check-cast v7, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@b
    .local v7, node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    if-eqz v7, :cond_3e

    #@d
    .line 1590
    iget-object v9, v7, Landroid/widget/RelativeLayout$DependencyGraph$Node;->view:Landroid/view/View;

    #@f
    .line 1591
    .local v9, view:Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getId()I

    #@12
    move-result v6

    #@13
    .line 1593
    .local v6, key:I
    add-int/lit8 v5, v4, 0x1

    #@15
    .end local v4           #index:I
    .local v5, index:I
    aput-object v9, p1, v4

    #@17
    .line 1595
    iget-object v2, v7, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependents:Ljava/util/HashMap;

    #@19
    .line 1596
    .local v2, dependents:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/widget/RelativeLayout$DependencyGraph$Node;Landroid/widget/RelativeLayout$DependencyGraph;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@1c
    move-result-object v10

    #@1d
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v3

    #@21
    .local v3, i$:Ljava/util/Iterator;
    :cond_21
    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v10

    #@25
    if-eqz v10, :cond_3c

    #@27
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v1

    #@2b
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@2d
    .line 1597
    .local v1, dependent:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    iget-object v0, v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;->dependencies:Landroid/util/SparseArray;

    #@2f
    .line 1599
    .local v0, dependencies:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    #@32
    .line 1600
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@35
    move-result v10

    #@36
    if-nez v10, :cond_21

    #@38
    .line 1601
    invoke-virtual {v8, v1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    #@3b
    goto :goto_21

    #@3c
    .end local v0           #dependencies:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    .end local v1           #dependent:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_3c
    move v4, v5

    #@3d
    .line 1604
    .end local v5           #index:I
    .restart local v4       #index:I
    goto :goto_5

    #@3e
    .line 1606
    .end local v2           #dependents:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/widget/RelativeLayout$DependencyGraph$Node;Landroid/widget/RelativeLayout$DependencyGraph;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #key:I
    .end local v9           #view:Landroid/view/View;
    :cond_3e
    array-length v10, p1

    #@3f
    if-ge v4, v10, :cond_49

    #@41
    .line 1607
    new-instance v10, Ljava/lang/IllegalStateException;

    #@43
    const-string v11, "Circular dependencies cannot exist in RelativeLayout"

    #@45
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@48
    throw v10

    #@49
    .line 1610
    :cond_49
    return-void
.end method

.method varargs log(Landroid/content/res/Resources;[I)V
    .registers 7
    .parameter "resources"
    .parameter "rules"

    #@0
    .prologue
    .line 1680
    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout$DependencyGraph;->findRoots([I)Ljava/util/ArrayDeque;

    #@3
    move-result-object v2

    #@4
    .line 1681
    .local v2, roots:Ljava/util/ArrayDeque;,"Ljava/util/ArrayDeque<Landroid/widget/RelativeLayout$DependencyGraph$Node;>;"
    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v0

    #@8
    .local v0, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_18

    #@e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/widget/RelativeLayout$DependencyGraph$Node;

    #@14
    .line 1682
    .local v1, node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    invoke-static {p1, v1}, Landroid/widget/RelativeLayout$DependencyGraph;->printNode(Landroid/content/res/Resources;Landroid/widget/RelativeLayout$DependencyGraph$Node;)V

    #@17
    goto :goto_8

    #@18
    .line 1684
    .end local v1           #node:Landroid/widget/RelativeLayout$DependencyGraph$Node;
    :cond_18
    return-void
.end method
