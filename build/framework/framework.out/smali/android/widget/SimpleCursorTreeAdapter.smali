.class public abstract Landroid/widget/SimpleCursorTreeAdapter;
.super Landroid/widget/ResourceCursorTreeAdapter;
.source "SimpleCursorTreeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;
    }
.end annotation


# instance fields
.field private mChildFrom:[I

.field private mChildFromNames:[Ljava/lang/String;

.field private mChildTo:[I

.field private mGroupFrom:[I

.field private mGroupFromNames:[Ljava/lang/String;

.field private mGroupTo:[I

.field private mViewBinder:Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;II[Ljava/lang/String;[III[Ljava/lang/String;[I)V
    .registers 20
    .parameter "context"
    .parameter "cursor"
    .parameter "collapsedGroupLayout"
    .parameter "expandedGroupLayout"
    .parameter "groupFrom"
    .parameter "groupTo"
    .parameter "childLayout"
    .parameter "lastChildLayout"
    .parameter "childFrom"
    .parameter "childTo"

    #@0
    .prologue
    .line 105
    move-object v2, p0

    #@1
    move-object v3, p1

    #@2
    move-object v4, p2

    #@3
    move v5, p3

    #@4
    move v6, p4

    #@5
    move/from16 v7, p7

    #@7
    move/from16 v8, p8

    #@9
    invoke-direct/range {v2 .. v8}, Landroid/widget/ResourceCursorTreeAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;IIII)V

    #@c
    .line 107
    move-object/from16 v0, p9

    #@e
    move-object/from16 v1, p10

    #@10
    invoke-direct {p0, p5, p6, v0, v1}, Landroid/widget/SimpleCursorTreeAdapter;->init([Ljava/lang/String;[I[Ljava/lang/String;[I)V

    #@13
    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;II[Ljava/lang/String;[II[Ljava/lang/String;[I)V
    .registers 16
    .parameter "context"
    .parameter "cursor"
    .parameter "collapsedGroupLayout"
    .parameter "expandedGroupLayout"
    .parameter "groupFrom"
    .parameter "groupTo"
    .parameter "childLayout"
    .parameter "childFrom"
    .parameter "childTo"

    #@0
    .prologue
    .line 142
    move-object v0, p0

    #@1
    move-object v1, p1

    #@2
    move-object v2, p2

    #@3
    move v3, p3

    #@4
    move v4, p4

    #@5
    move v5, p7

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/widget/ResourceCursorTreeAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;III)V

    #@9
    .line 143
    invoke-direct {p0, p5, p6, p8, p9}, Landroid/widget/SimpleCursorTreeAdapter;->init([Ljava/lang/String;[I[Ljava/lang/String;[I)V

    #@c
    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I[Ljava/lang/String;[II[Ljava/lang/String;[I)V
    .registers 9
    .parameter "context"
    .parameter "cursor"
    .parameter "groupLayout"
    .parameter "groupFrom"
    .parameter "groupTo"
    .parameter "childLayout"
    .parameter "childFrom"
    .parameter "childTo"

    #@0
    .prologue
    .line 175
    invoke-direct {p0, p1, p2, p3, p6}, Landroid/widget/ResourceCursorTreeAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;II)V

    #@3
    .line 176
    invoke-direct {p0, p4, p5, p7, p8}, Landroid/widget/SimpleCursorTreeAdapter;->init([Ljava/lang/String;[I[Ljava/lang/String;[I)V

    #@6
    .line 177
    return-void
.end method

.method private bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;[I[I)V
    .registers 13
    .parameter "view"
    .parameter "context"
    .parameter "cursor"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mViewBinder:Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;

    #@2
    .line 215
    .local v0, binder:Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    array-length v5, p5

    #@4
    if-ge v2, v5, :cond_41

    #@6
    .line 216
    aget v5, p5, v2

    #@8
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@b
    move-result-object v4

    #@c
    .line 217
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_2c

    #@e
    .line 218
    const/4 v1, 0x0

    #@f
    .line 219
    .local v1, bound:Z
    if-eqz v0, :cond_17

    #@11
    .line 220
    aget v5, p4, v2

    #@13
    invoke-interface {v0, v4, p3, v5}, Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;->setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z

    #@16
    move-result v1

    #@17
    .line 223
    :cond_17
    if-nez v1, :cond_2c

    #@19
    .line 224
    aget v5, p4, v2

    #@1b
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    .line 225
    .local v3, text:Ljava/lang/String;
    if-nez v3, :cond_23

    #@21
    .line 226
    const-string v3, ""

    #@23
    .line 228
    :cond_23
    instance-of v5, v4, Landroid/widget/TextView;

    #@25
    if-eqz v5, :cond_2f

    #@27
    .line 229
    check-cast v4, Landroid/widget/TextView;

    #@29
    .end local v4           #v:Landroid/view/View;
    invoke-virtual {p0, v4, v3}, Landroid/widget/SimpleCursorTreeAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;)V

    #@2c
    .line 215
    .end local v1           #bound:Z
    .end local v3           #text:Ljava/lang/String;
    :cond_2c
    :goto_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_3

    #@2f
    .line 230
    .restart local v1       #bound:Z
    .restart local v3       #text:Ljava/lang/String;
    .restart local v4       #v:Landroid/view/View;
    :cond_2f
    instance-of v5, v4, Landroid/widget/ImageView;

    #@31
    if-eqz v5, :cond_39

    #@33
    .line 231
    check-cast v4, Landroid/widget/ImageView;

    #@35
    .end local v4           #v:Landroid/view/View;
    invoke-virtual {p0, v4, v3}, Landroid/widget/SimpleCursorTreeAdapter;->setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    #@38
    goto :goto_2c

    #@39
    .line 233
    .restart local v4       #v:Landroid/view/View;
    :cond_39
    new-instance v5, Ljava/lang/IllegalStateException;

    #@3b
    const-string v6, "SimpleCursorTreeAdapter can bind values only to TextView and ImageView!"

    #@3d
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@40
    throw v5

    #@41
    .line 239
    .end local v1           #bound:Z
    .end local v3           #text:Ljava/lang/String;
    .end local v4           #v:Landroid/view/View;
    :cond_41
    return-void
.end method

.method private init([Ljava/lang/String;[I[Ljava/lang/String;[I)V
    .registers 5
    .parameter "groupFromNames"
    .parameter "groupTo"
    .parameter "childFromNames"
    .parameter "childTo"

    #@0
    .prologue
    .line 182
    iput-object p1, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFromNames:[Ljava/lang/String;

    #@2
    .line 183
    iput-object p2, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupTo:[I

    #@4
    .line 185
    iput-object p3, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFromNames:[Ljava/lang/String;

    #@6
    .line 186
    iput-object p4, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildTo:[I

    #@8
    .line 187
    return-void
.end method

.method private initFromColumns(Landroid/database/Cursor;[Ljava/lang/String;[I)V
    .registers 6
    .parameter "cursor"
    .parameter "fromColumnNames"
    .parameter "fromColumns"

    #@0
    .prologue
    .line 242
    array-length v1, p2

    #@1
    add-int/lit8 v0, v1, -0x1

    #@3
    .local v0, i:I
    :goto_3
    if-ltz v0, :cond_10

    #@5
    .line 243
    aget-object v1, p2, v0

    #@7
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a
    move-result v1

    #@b
    aput v1, p3, v0

    #@d
    .line 242
    add-int/lit8 v0, v0, -0x1

    #@f
    goto :goto_3

    #@10
    .line 245
    :cond_10
    return-void
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .registers 11
    .parameter "view"
    .parameter "context"
    .parameter "cursor"
    .parameter "isLastChild"

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFrom:[I

    #@2
    if-nez v0, :cond_12

    #@4
    .line 250
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFromNames:[Ljava/lang/String;

    #@6
    array-length v0, v0

    #@7
    new-array v0, v0, [I

    #@9
    iput-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFrom:[I

    #@b
    .line 251
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFromNames:[Ljava/lang/String;

    #@d
    iget-object v1, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFrom:[I

    #@f
    invoke-direct {p0, p3, v0, v1}, Landroid/widget/SimpleCursorTreeAdapter;->initFromColumns(Landroid/database/Cursor;[Ljava/lang/String;[I)V

    #@12
    .line 254
    :cond_12
    iget-object v4, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildFrom:[I

    #@14
    iget-object v5, p0, Landroid/widget/SimpleCursorTreeAdapter;->mChildTo:[I

    #@16
    move-object v0, p0

    #@17
    move-object v1, p1

    #@18
    move-object v2, p2

    #@19
    move-object v3, p3

    #@1a
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorTreeAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;[I[I)V

    #@1d
    .line 255
    return-void
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .registers 11
    .parameter "view"
    .parameter "context"
    .parameter "cursor"
    .parameter "isExpanded"

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFrom:[I

    #@2
    if-nez v0, :cond_12

    #@4
    .line 260
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFromNames:[Ljava/lang/String;

    #@6
    array-length v0, v0

    #@7
    new-array v0, v0, [I

    #@9
    iput-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFrom:[I

    #@b
    .line 261
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFromNames:[Ljava/lang/String;

    #@d
    iget-object v1, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFrom:[I

    #@f
    invoke-direct {p0, p3, v0, v1}, Landroid/widget/SimpleCursorTreeAdapter;->initFromColumns(Landroid/database/Cursor;[Ljava/lang/String;[I)V

    #@12
    .line 264
    :cond_12
    iget-object v4, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupFrom:[I

    #@14
    iget-object v5, p0, Landroid/widget/SimpleCursorTreeAdapter;->mGroupTo:[I

    #@16
    move-object v0, p0

    #@17
    move-object v1, p1

    #@18
    move-object v2, p2

    #@19
    move-object v3, p3

    #@1a
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorTreeAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;[I[I)V

    #@1d
    .line 265
    return-void
.end method

.method public getViewBinder()Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;
    .registers 2

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Landroid/widget/SimpleCursorTreeAdapter;->mViewBinder:Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;

    #@2
    return-object v0
.end method

.method public setViewBinder(Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;)V
    .registers 2
    .parameter "viewBinder"

    #@0
    .prologue
    .line 209
    iput-object p1, p0, Landroid/widget/SimpleCursorTreeAdapter;->mViewBinder:Landroid/widget/SimpleCursorTreeAdapter$ViewBinder;

    #@2
    .line 210
    return-void
.end method

.method protected setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .registers 5
    .parameter "v"
    .parameter "value"

    #@0
    .prologue
    .line 277
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 281
    :goto_7
    return-void

    #@8
    .line 278
    :catch_8
    move-exception v0

    #@9
    .line 279
    .local v0, nfe:Ljava/lang/NumberFormatException;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    #@10
    goto :goto_7
.end method

.method public setViewText(Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 3
    .parameter "v"
    .parameter "text"

    #@0
    .prologue
    .line 295
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@3
    .line 296
    return-void
.end method
