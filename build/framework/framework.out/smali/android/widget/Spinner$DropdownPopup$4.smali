.class Landroid/widget/Spinner$DropdownPopup$4;
.super Ljava/lang/Object;
.source "Spinner.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/Spinner$DropdownPopup;->show()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/Spinner$DropdownPopup;

.field final synthetic val$layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field final synthetic val$scrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# direct methods
.method constructor <init>(Landroid/widget/Spinner$DropdownPopup;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1011
    iput-object p1, p0, Landroid/widget/Spinner$DropdownPopup$4;->this$1:Landroid/widget/Spinner$DropdownPopup;

    #@2
    iput-object p2, p0, Landroid/widget/Spinner$DropdownPopup$4;->val$layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@4
    iput-object p3, p0, Landroid/widget/Spinner$DropdownPopup$4;->val$scrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public onDismiss()V
    .registers 3

    #@0
    .prologue
    .line 1013
    iget-object v1, p0, Landroid/widget/Spinner$DropdownPopup$4;->this$1:Landroid/widget/Spinner$DropdownPopup;

    #@2
    iget-object v1, v1, Landroid/widget/Spinner$DropdownPopup;->this$0:Landroid/widget/Spinner;

    #@4
    invoke-virtual {v1}, Landroid/widget/Spinner;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@7
    move-result-object v0

    #@8
    .line 1014
    .local v0, vto:Landroid/view/ViewTreeObserver;
    if-eqz v0, :cond_14

    #@a
    .line 1015
    iget-object v1, p0, Landroid/widget/Spinner$DropdownPopup$4;->val$layoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@c
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@f
    .line 1016
    iget-object v1, p0, Landroid/widget/Spinner$DropdownPopup$4;->val$scrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@11
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@14
    .line 1018
    :cond_14
    return-void
.end method
