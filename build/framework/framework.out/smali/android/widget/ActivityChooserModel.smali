.class public Landroid/widget/ActivityChooserModel;
.super Landroid/database/DataSetObservable;
.source "ActivityChooserModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ActivityChooserModel$1;,
        Landroid/widget/ActivityChooserModel$DataModelPackageMonitor;,
        Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;,
        Landroid/widget/ActivityChooserModel$DefaultSorter;,
        Landroid/widget/ActivityChooserModel$ActivityResolveInfo;,
        Landroid/widget/ActivityChooserModel$HistoricalRecord;,
        Landroid/widget/ActivityChooserModel$OnChooseActivityListener;,
        Landroid/widget/ActivityChooserModel$ActivitySorter;,
        Landroid/widget/ActivityChooserModel$ActivityChooserModelClient;
    }
.end annotation


# static fields
.field private static final ATTRIBUTE_ACTIVITY:Ljava/lang/String; = "activity"

.field private static final ATTRIBUTE_TIME:Ljava/lang/String; = "time"

.field private static final ATTRIBUTE_WEIGHT:Ljava/lang/String; = "weight"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_ACTIVITY_INFLATION:I = 0x5

.field private static final DEFAULT_HISTORICAL_RECORD_WEIGHT:F = 1.0f

.field public static final DEFAULT_HISTORY_FILE_NAME:Ljava/lang/String; = "activity_choser_model_history.xml"

.field public static final DEFAULT_HISTORY_MAX_LENGTH:I = 0x32

.field private static final HISTORY_FILE_EXTENSION:Ljava/lang/String; = ".xml"

.field private static final INVALID_INDEX:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = null

.field private static final TAG_HISTORICAL_RECORD:Ljava/lang/String; = "historical-record"

.field private static final TAG_HISTORICAL_RECORDS:Ljava/lang/String; = "historical-records"

.field private static final sDataModelRegistry:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/ActivityChooserModel;",
            ">;"
        }
    .end annotation
.end field

.field private static final sRegistryLock:Ljava/lang/Object;


# instance fields
.field private final mActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ActivityChooserModel$ActivityResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityChoserModelPolicy:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

.field private mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

.field private mCanReadHistoricalData:Z

.field private final mContext:Landroid/content/Context;

.field private final mHistoricalRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ActivityChooserModel$HistoricalRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mHistoricalRecordsChanged:Z

.field private final mHistoryFileName:Ljava/lang/String;

.field private mHistoryMaxSize:I

.field private final mInstanceLock:Ljava/lang/Object;

.field private mIntent:Landroid/content/Intent;

.field private final mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mReadShareHistoryCalled:Z

.field private mReloadActivities:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 161
    const-class v0, Landroid/widget/ActivityChooserModel;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/ActivityChooserModel;->LOG_TAG:Ljava/lang/String;

    #@8
    .line 222
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@d
    sput-object v0, Landroid/widget/ActivityChooserModel;->sRegistryLock:Ljava/lang/Object;

    #@f
    .line 227
    new-instance v0, Ljava/util/HashMap;

    #@11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@14
    sput-object v0, Landroid/widget/ActivityChooserModel;->sDataModelRegistry:Ljava/util/Map;

    #@16
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "historyFileName"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 355
    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    #@6
    .line 233
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@d
    .line 238
    new-instance v0, Ljava/util/ArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@14
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    #@16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@1b
    .line 248
    new-instance v0, Landroid/widget/ActivityChooserModel$DataModelPackageMonitor;

    #@1d
    invoke-direct {v0, p0, v3}, Landroid/widget/ActivityChooserModel$DataModelPackageMonitor;-><init>(Landroid/widget/ActivityChooserModel;Landroid/widget/ActivityChooserModel$1;)V

    #@20
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@22
    .line 268
    new-instance v0, Landroid/widget/ActivityChooserModel$DefaultSorter;

    #@24
    invoke-direct {v0, p0, v3}, Landroid/widget/ActivityChooserModel$DefaultSorter;-><init>(Landroid/widget/ActivityChooserModel;Landroid/widget/ActivityChooserModel$1;)V

    #@27
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

    #@29
    .line 273
    const/16 v0, 0x32

    #@2b
    iput v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryMaxSize:I

    #@2d
    .line 283
    iput-boolean v2, p0, Landroid/widget/ActivityChooserModel;->mCanReadHistoricalData:Z

    #@2f
    .line 294
    iput-boolean v1, p0, Landroid/widget/ActivityChooserModel;->mReadShareHistoryCalled:Z

    #@31
    .line 302
    iput-boolean v2, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@33
    .line 307
    iput-boolean v1, p0, Landroid/widget/ActivityChooserModel;->mReloadActivities:Z

    #@35
    .line 356
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mContext:Landroid/content/Context;

    #@3b
    .line 357
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_66

    #@41
    const-string v0, ".xml"

    #@43
    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@46
    move-result v0

    #@47
    if-nez v0, :cond_66

    #@49
    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    const-string v1, ".xml"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@5e
    .line 363
    :goto_5e
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@60
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@65
    .line 364
    return-void

    #@66
    .line 361
    :cond_66
    iput-object p2, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@68
    goto :goto_5e
.end method

.method static synthetic access$300(Landroid/widget/ActivityChooserModel;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 94
    sget-object v0, Landroid/widget/ActivityChooserModel;->LOG_TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/ActivityChooserModel;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Landroid/widget/ActivityChooserModel;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    iput-boolean p1, p0, Landroid/widget/ActivityChooserModel;->mCanReadHistoricalData:Z

    #@2
    return p1
.end method

.method static synthetic access$702(Landroid/widget/ActivityChooserModel;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 94
    iput-boolean p1, p0, Landroid/widget/ActivityChooserModel;->mReloadActivities:Z

    #@2
    return p1
.end method

.method private addHisoricalRecord(Landroid/widget/ActivityChooserModel$HistoricalRecord;)Z
    .registers 4
    .parameter "historicalRecord"

    #@0
    .prologue
    .line 743
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@2
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    .line 744
    .local v0, added:Z
    if-eqz v0, :cond_17

    #@8
    .line 745
    const/4 v1, 0x1

    #@9
    iput-boolean v1, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@b
    .line 746
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->pruneExcessiveHistoricalRecordsIfNeeded()V

    #@e
    .line 747
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->persistHistoricalDataIfNeeded()V

    #@11
    .line 748
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->sortActivitiesIfNeeded()Z

    #@14
    .line 749
    invoke-virtual {p0}, Landroid/widget/ActivityChooserModel;->notifyChanged()V

    #@17
    .line 751
    :cond_17
    return v0
.end method

.method private ensureConsistentState()V
    .registers 3

    #@0
    .prologue
    .line 670
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->loadActivitiesIfNeeded()Z

    #@3
    move-result v0

    #@4
    .line 671
    .local v0, stateChanged:Z
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->readHistoricalDataIfNeeded()Z

    #@7
    move-result v1

    #@8
    or-int/2addr v0, v1

    #@9
    .line 672
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->pruneExcessiveHistoricalRecordsIfNeeded()V

    #@c
    .line 673
    if-eqz v0, :cond_14

    #@e
    .line 674
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->sortActivitiesIfNeeded()Z

    #@11
    .line 675
    invoke-virtual {p0}, Landroid/widget/ActivityChooserModel;->notifyChanged()V

    #@14
    .line 677
    :cond_14
    return-void
.end method

.method public static get(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/ActivityChooserModel;
    .registers 5
    .parameter "context"
    .parameter "historyFileName"

    #@0
    .prologue
    .line 339
    sget-object v2, Landroid/widget/ActivityChooserModel;->sRegistryLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 340
    :try_start_3
    sget-object v1, Landroid/widget/ActivityChooserModel;->sDataModelRegistry:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/widget/ActivityChooserModel;

    #@b
    .line 341
    .local v0, dataModel:Landroid/widget/ActivityChooserModel;
    if-nez v0, :cond_17

    #@d
    .line 342
    new-instance v0, Landroid/widget/ActivityChooserModel;

    #@f
    .end local v0           #dataModel:Landroid/widget/ActivityChooserModel;
    invoke-direct {v0, p0, p1}, Landroid/widget/ActivityChooserModel;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@12
    .line 343
    .restart local v0       #dataModel:Landroid/widget/ActivityChooserModel;
    sget-object v1, Landroid/widget/ActivityChooserModel;->sDataModelRegistry:Ljava/util/Map;

    #@14
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 345
    :cond_17
    monitor-exit v2

    #@18
    return-object v0

    #@19
    .line 346
    .end local v0           #dataModel:Landroid/widget/ActivityChooserModel;
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method private loadActivitiesIfNeeded()Z
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 703
    iget-boolean v5, p0, Landroid/widget/ActivityChooserModel;->mReloadActivities:Z

    #@3
    if-eqz v5, :cond_37

    #@5
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@7
    if-eqz v5, :cond_37

    #@9
    .line 704
    iput-boolean v4, p0, Landroid/widget/ActivityChooserModel;->mReloadActivities:Z

    #@b
    .line 705
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@d
    invoke-interface {v5}, Ljava/util/List;->clear()V

    #@10
    .line 706
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@15
    move-result-object v5

    #@16
    iget-object v6, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@18
    invoke-virtual {v5, v6, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@1b
    move-result-object v3

    #@1c
    .line 708
    .local v3, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@1f
    move-result v2

    #@20
    .line 709
    .local v2, resolveInfoCount:I
    const/4 v0, 0x0

    #@21
    .local v0, i:I
    :goto_21
    if-ge v0, v2, :cond_36

    #@23
    .line 710
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/content/pm/ResolveInfo;

    #@29
    .line 711
    .local v1, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v4, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@2b
    new-instance v5, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@2d
    invoke-direct {v5, p0, v1}, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;-><init>(Landroid/widget/ActivityChooserModel;Landroid/content/pm/ResolveInfo;)V

    #@30
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@33
    .line 709
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_21

    #@36
    .line 713
    .end local v1           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_36
    const/4 v4, 0x1

    #@37
    .line 715
    .end local v0           #i:I
    .end local v2           #resolveInfoCount:I
    .end local v3           #resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_37
    return v4
.end method

.method private persistHistoricalDataIfNeeded()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 576
    iget-boolean v0, p0, Landroid/widget/ActivityChooserModel;->mReadShareHistoryCalled:Z

    #@3
    if-nez v0, :cond_d

    #@5
    .line 577
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "No preceding call to #readHistoricalData"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 579
    :cond_d
    iget-boolean v0, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@f
    if-nez v0, :cond_12

    #@11
    .line 587
    :cond_11
    :goto_11
    return-void

    #@12
    .line 582
    :cond_12
    iput-boolean v5, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@14
    .line 583
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_11

    #@1c
    .line 584
    new-instance v0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-direct {v0, p0, v1}, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;-><init>(Landroid/widget/ActivityChooserModel;Landroid/widget/ActivityChooserModel$1;)V

    #@22
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@24
    const/4 v2, 0x2

    #@25
    new-array v2, v2, [Ljava/lang/Object;

    #@27
    new-instance v3, Ljava/util/ArrayList;

    #@29
    iget-object v4, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@2b
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@2e
    aput-object v3, v2, v5

    #@30
    const/4 v3, 0x1

    #@31
    iget-object v4, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@33
    aput-object v4, v2, v3

    #@35
    invoke-virtual {v0, v1, v2}, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@38
    goto :goto_11
.end method

.method private pruneExcessiveHistoricalRecordsIfNeeded()V
    .registers 5

    #@0
    .prologue
    .line 758
    iget-object v2, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@5
    move-result v2

    #@6
    iget v3, p0, Landroid/widget/ActivityChooserModel;->mHistoryMaxSize:I

    #@8
    sub-int v1, v2, v3

    #@a
    .line 759
    .local v1, pruneCount:I
    if-gtz v1, :cond_d

    #@c
    .line 769
    :cond_c
    return-void

    #@d
    .line 762
    :cond_d
    const/4 v2, 0x1

    #@e
    iput-boolean v2, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@10
    .line 763
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v1, :cond_c

    #@13
    .line 764
    iget-object v2, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@15
    const/4 v3, 0x0

    #@16
    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@1c
    .line 763
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_11
.end method

.method private readHistoricalDataIfNeeded()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 726
    iget-boolean v2, p0, Landroid/widget/ActivityChooserModel;->mCanReadHistoricalData:Z

    #@4
    if-eqz v2, :cond_1a

    #@6
    iget-boolean v2, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecordsChanged:Z

    #@8
    if-eqz v2, :cond_1a

    #@a
    iget-object v2, p0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@c
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_1a

    #@12
    .line 728
    iput-boolean v1, p0, Landroid/widget/ActivityChooserModel;->mCanReadHistoricalData:Z

    #@14
    .line 729
    iput-boolean v0, p0, Landroid/widget/ActivityChooserModel;->mReadShareHistoryCalled:Z

    #@16
    .line 730
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->readHistoricalDataImpl()V

    #@19
    .line 733
    :goto_19
    return v0

    #@1a
    :cond_1a
    move v0, v1

    #@1b
    goto :goto_19
.end method

.method private readHistoricalDataImpl()V
    .registers 18

    #@0
    .prologue
    .line 976
    const/4 v2, 0x0

    #@1
    .line 978
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v14, v0, Landroid/widget/ActivityChooserModel;->mContext:Landroid/content/Context;

    #@5
    move-object/from16 v0, p0

    #@7
    iget-object v15, v0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@9
    invoke-virtual {v14, v15}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_c} :catch_21

    #@c
    move-result-object v2

    #@d
    .line 986
    :try_start_d
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@10
    move-result-object v7

    #@11
    .line 987
    .local v7, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v14, 0x0

    #@12
    invoke-interface {v7, v2, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@15
    .line 989
    const/4 v11, 0x0

    #@16
    .line 990
    .local v11, type:I
    :goto_16
    const/4 v14, 0x1

    #@17
    if-eq v11, v14, :cond_23

    #@19
    const/4 v14, 0x2

    #@1a
    if-eq v11, v14, :cond_23

    #@1c
    .line 991
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1f
    move-result v11

    #@20
    goto :goto_16

    #@21
    .line 979
    .end local v7           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v11           #type:I
    :catch_21
    move-exception v3

    #@22
    .line 1044
    :cond_22
    :goto_22
    return-void

    #@23
    .line 994
    .restart local v7       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #type:I
    :cond_23
    const-string v14, "historical-records"

    #@25
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@28
    move-result-object v15

    #@29
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v14

    #@2d
    if-nez v14, :cond_5e

    #@2f
    .line 995
    new-instance v14, Lorg/xmlpull/v1/XmlPullParserException;

    #@31
    const-string v15, "Share records file does not start with historical-records tag."

    #@33
    invoke-direct {v14, v15}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@36
    throw v14
    :try_end_37
    .catchall {:try_start_d .. :try_end_37} :catchall_da
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_d .. :try_end_37} :catch_37
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_37} :catch_8c

    #@37
    .line 1031
    .end local v7           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v11           #type:I
    :catch_37
    move-exception v13

    #@38
    .line 1032
    .local v13, xppe:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_38
    sget-object v14, Landroid/widget/ActivityChooserModel;->LOG_TAG:Ljava/lang/String;

    #@3a
    new-instance v15, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v16, "Error reading historical recrod file: "

    #@41
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v15

    #@45
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@49
    move-object/from16 v16, v0

    #@4b
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v15

    #@4f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v15

    #@53
    invoke-static {v14, v15, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_56
    .catchall {:try_start_38 .. :try_end_56} :catchall_da

    #@56
    .line 1036
    if-eqz v2, :cond_22

    #@58
    .line 1038
    :try_start_58
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_5b} :catch_5c

    #@5b
    goto :goto_22

    #@5c
    .line 1039
    .end local v13           #xppe:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_5c
    move-exception v14

    #@5d
    goto :goto_22

    #@5e
    .line 999
    .restart local v7       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #type:I
    :cond_5e
    :try_start_5e
    move-object/from16 v0, p0

    #@60
    iget-object v4, v0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@62
    .line 1000
    .local v4, historicalRecords:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$HistoricalRecord;>;"
    invoke-interface {v4}, Ljava/util/List;->clear()V

    #@65
    .line 1003
    :cond_65
    :goto_65
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_68
    .catchall {:try_start_5e .. :try_end_68} :catchall_da
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5e .. :try_end_68} :catch_37
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_68} :catch_8c

    #@68
    move-result v11

    #@69
    .line 1004
    const/4 v14, 0x1

    #@6a
    if-ne v11, v14, :cond_72

    #@6c
    .line 1036
    if-eqz v2, :cond_22

    #@6e
    .line 1038
    :try_start_6e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_5c

    #@71
    goto :goto_22

    #@72
    .line 1007
    :cond_72
    const/4 v14, 0x3

    #@73
    if-eq v11, v14, :cond_65

    #@75
    const/4 v14, 0x4

    #@76
    if-eq v11, v14, :cond_65

    #@78
    .line 1010
    :try_start_78
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    .line 1011
    .local v6, nodeName:Ljava/lang/String;
    const-string v14, "historical-record"

    #@7e
    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v14

    #@82
    if-nez v14, :cond_b2

    #@84
    .line 1012
    new-instance v14, Lorg/xmlpull/v1/XmlPullParserException;

    #@86
    const-string v15, "Share records file not well-formed."

    #@88
    invoke-direct {v14, v15}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@8b
    throw v14
    :try_end_8c
    .catchall {:try_start_78 .. :try_end_8c} :catchall_da
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_78 .. :try_end_8c} :catch_37
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_8c} :catch_8c

    #@8c
    .line 1033
    .end local v4           #historicalRecords:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$HistoricalRecord;>;"
    .end local v6           #nodeName:Ljava/lang/String;
    .end local v7           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v11           #type:I
    :catch_8c
    move-exception v5

    #@8d
    .line 1034
    .local v5, ioe:Ljava/io/IOException;
    :try_start_8d
    sget-object v14, Landroid/widget/ActivityChooserModel;->LOG_TAG:Ljava/lang/String;

    #@8f
    new-instance v15, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v16, "Error reading historical recrod file: "

    #@96
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v15

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Landroid/widget/ActivityChooserModel;->mHistoryFileName:Ljava/lang/String;

    #@9e
    move-object/from16 v16, v0

    #@a0
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v15

    #@a4
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v15

    #@a8
    invoke-static {v14, v15, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ab
    .catchall {:try_start_8d .. :try_end_ab} :catchall_da

    #@ab
    .line 1036
    if-eqz v2, :cond_22

    #@ad
    .line 1038
    :try_start_ad
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b0
    .catch Ljava/io/IOException; {:try_start_ad .. :try_end_b0} :catch_5c

    #@b0
    goto/16 :goto_22

    #@b2
    .line 1015
    .end local v5           #ioe:Ljava/io/IOException;
    .restart local v4       #historicalRecords:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$HistoricalRecord;>;"
    .restart local v6       #nodeName:Ljava/lang/String;
    .restart local v7       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #type:I
    :cond_b2
    const/4 v14, 0x0

    #@b3
    :try_start_b3
    const-string v15, "activity"

    #@b5
    invoke-interface {v7, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b8
    move-result-object v1

    #@b9
    .line 1016
    .local v1, activity:Ljava/lang/String;
    const/4 v14, 0x0

    #@ba
    const-string/jumbo v15, "time"

    #@bd
    invoke-interface {v7, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c0
    move-result-object v14

    #@c1
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@c4
    move-result-wide v9

    #@c5
    .line 1018
    .local v9, time:J
    const/4 v14, 0x0

    #@c6
    const-string/jumbo v15, "weight"

    #@c9
    invoke-interface {v7, v14, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@cc
    move-result-object v14

    #@cd
    invoke-static {v14}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@d0
    move-result v12

    #@d1
    .line 1020
    .local v12, weight:F
    new-instance v8, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@d3
    invoke-direct {v8, v1, v9, v10, v12}, Landroid/widget/ActivityChooserModel$HistoricalRecord;-><init>(Ljava/lang/String;JF)V

    #@d6
    .line 1021
    .local v8, readRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_d9
    .catchall {:try_start_b3 .. :try_end_d9} :catchall_da
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b3 .. :try_end_d9} :catch_37
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_d9} :catch_8c

    #@d9
    goto :goto_65

    #@da
    .line 1036
    .end local v1           #activity:Ljava/lang/String;
    .end local v4           #historicalRecords:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$HistoricalRecord;>;"
    .end local v6           #nodeName:Ljava/lang/String;
    .end local v7           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v8           #readRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    .end local v9           #time:J
    .end local v11           #type:I
    .end local v12           #weight:F
    :catchall_da
    move-exception v14

    #@db
    if-eqz v2, :cond_e0

    #@dd
    .line 1038
    :try_start_dd
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_e0} :catch_e1

    #@e0
    .line 1036
    :cond_e0
    :goto_e0
    throw v14

    #@e1
    .line 1039
    :catch_e1
    move-exception v15

    #@e2
    goto :goto_e0
.end method

.method private sortActivitiesIfNeeded()Z
    .registers 5

    #@0
    .prologue
    .line 687
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

    #@2
    if-eqz v0, :cond_29

    #@4
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@6
    if-eqz v0, :cond_29

    #@8
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@a
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_29

    #@10
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@12
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_29

    #@18
    .line 689
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

    #@1a
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@1c
    iget-object v2, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@1e
    iget-object v3, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@20
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@23
    move-result-object v3

    #@24
    invoke-interface {v0, v1, v2, v3}, Landroid/widget/ActivityChooserModel$ActivitySorter;->sort(Landroid/content/Intent;Ljava/util/List;Ljava/util/List;)V

    #@27
    .line 691
    const/4 v0, 0x1

    #@28
    .line 693
    :goto_28
    return v0

    #@29
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_28
.end method


# virtual methods
.method public chooseActivity(I)Landroid/content/Intent;
    .registers 12
    .parameter "index"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 467
    iget-object v7, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@3
    monitor-enter v7

    #@4
    .line 468
    :try_start_4
    iget-object v8, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@6
    if-nez v8, :cond_b

    #@8
    .line 469
    monitor-exit v7

    #@9
    move-object v0, v6

    #@a
    .line 497
    :goto_a
    return-object v0

    #@b
    .line 472
    :cond_b
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@e
    .line 474
    iget-object v8, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@10
    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@16
    .line 476
    .local v2, chosenActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    new-instance v3, Landroid/content/ComponentName;

    #@18
    iget-object v8, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@1a
    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1c
    iget-object v8, v8, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1e
    iget-object v9, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@20
    iget-object v9, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@22
    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@24
    invoke-direct {v3, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 480
    .local v3, chosenName:Landroid/content/ComponentName;
    new-instance v0, Landroid/content/Intent;

    #@29
    iget-object v8, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@2b
    invoke-direct {v0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@2e
    .line 481
    .local v0, choiceIntent:Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@31
    .line 483
    iget-object v8, p0, Landroid/widget/ActivityChooserModel;->mActivityChoserModelPolicy:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@33
    if-eqz v8, :cond_45

    #@35
    .line 485
    new-instance v1, Landroid/content/Intent;

    #@37
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@3a
    .line 486
    .local v1, choiceIntentCopy:Landroid/content/Intent;
    iget-object v8, p0, Landroid/widget/ActivityChooserModel;->mActivityChoserModelPolicy:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@3c
    invoke-interface {v8, p0, v1}, Landroid/widget/ActivityChooserModel$OnChooseActivityListener;->onChooseActivity(Landroid/widget/ActivityChooserModel;Landroid/content/Intent;)Z

    #@3f
    move-result v4

    #@40
    .line 488
    .local v4, handled:Z
    if-eqz v4, :cond_45

    #@42
    .line 489
    monitor-exit v7

    #@43
    move-object v0, v6

    #@44
    goto :goto_a

    #@45
    .line 493
    .end local v1           #choiceIntentCopy:Landroid/content/Intent;
    .end local v4           #handled:Z
    :cond_45
    new-instance v5, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4a
    move-result-wide v8

    #@4b
    const/high16 v6, 0x3f80

    #@4d
    invoke-direct {v5, v3, v8, v9, v6}, Landroid/widget/ActivityChooserModel$HistoricalRecord;-><init>(Landroid/content/ComponentName;JF)V

    #@50
    .line 495
    .local v5, historicalRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    invoke-direct {p0, v5}, Landroid/widget/ActivityChooserModel;->addHisoricalRecord(Landroid/widget/ActivityChooserModel$HistoricalRecord;)Z

    #@53
    .line 497
    monitor-exit v7

    #@54
    goto :goto_a

    #@55
    .line 498
    .end local v0           #choiceIntent:Landroid/content/Intent;
    .end local v2           #chosenActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    .end local v3           #chosenName:Landroid/content/ComponentName;
    .end local v5           #historicalRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    :catchall_55
    move-exception v6

    #@56
    monitor-exit v7
    :try_end_57
    .catchall {:try_start_4 .. :try_end_57} :catchall_55

    #@57
    throw v6
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 659
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@3
    .line 660
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    #@8
    .line 661
    return-void
.end method

.method public getActivity(I)Landroid/content/pm/ResolveInfo;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 420
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 421
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 422
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@8
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@e
    iget-object v0, v0, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 423
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getActivityCount()I
    .registers 3

    #@0
    .prologue
    .line 405
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 406
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 407
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    monitor-exit v1

    #@d
    return v0

    #@e
    .line 408
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public getActivityIndex(Landroid/content/pm/ResolveInfo;)I
    .registers 8
    .parameter "activity"

    #@0
    .prologue
    .line 434
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 435
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 436
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@8
    .line 437
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$ActivityResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v1

    #@c
    .line 438
    .local v1, activityCount:I
    const/4 v3, 0x0

    #@d
    .local v3, i:I
    :goto_d
    if-ge v3, v1, :cond_1e

    #@f
    .line 439
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@15
    .line 440
    .local v2, currentActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    iget-object v4, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@17
    if-ne v4, p1, :cond_1b

    #@19
    .line 441
    monitor-exit v5

    #@1a
    .line 444
    .end local v2           #currentActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    .end local v3           #i:I
    :goto_1a
    return v3

    #@1b
    .line 438
    .restart local v2       #currentActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    .restart local v3       #i:I
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    #@1d
    goto :goto_d

    #@1e
    .line 444
    .end local v2           #currentActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    :cond_1e
    const/4 v3, -0x1

    #@1f
    monitor-exit v5

    #@20
    goto :goto_1a

    #@21
    .line 445
    .end local v0           #activities:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$ActivityResolveInfo;>;"
    .end local v1           #activityCount:I
    .end local v3           #i:I
    :catchall_21
    move-exception v4

    #@22
    monitor-exit v5
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v4
.end method

.method public getDefaultActivity()Landroid/content/pm/ResolveInfo;
    .registers 4

    #@0
    .prologue
    .line 522
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 523
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 524
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_1b

    #@e
    .line 525
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@17
    iget-object v0, v0, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@19
    monitor-exit v1

    #@1a
    .line 528
    :goto_1a
    return-object v0

    #@1b
    .line 527
    :cond_1b
    monitor-exit v1

    #@1c
    .line 528
    const/4 v0, 0x0

    #@1d
    goto :goto_1a

    #@1e
    .line 527
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public getHistoryMaxSize()I
    .registers 3

    #@0
    .prologue
    .line 640
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 641
    :try_start_3
    iget v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryMaxSize:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 642
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getHistorySize()I
    .registers 3

    #@0
    .prologue
    .line 651
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 652
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 653
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mHistoricalRecords:Ljava/util/List;

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    monitor-exit v1

    #@d
    return v0

    #@e
    .line 654
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 3

    #@0
    .prologue
    .line 392
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 393
    :try_start_3
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 394
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setActivitySorter(Landroid/widget/ActivityChooserModel$ActivitySorter;)V
    .registers 4
    .parameter "activitySorter"

    #@0
    .prologue
    .line 597
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 598
    :try_start_3
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

    #@5
    if-ne v0, p1, :cond_9

    #@7
    .line 599
    monitor-exit v1

    #@8
    .line 606
    :goto_8
    return-void

    #@9
    .line 601
    :cond_9
    iput-object p1, p0, Landroid/widget/ActivityChooserModel;->mActivitySorter:Landroid/widget/ActivityChooserModel$ActivitySorter;

    #@b
    .line 602
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->sortActivitiesIfNeeded()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_14

    #@11
    .line 603
    invoke-virtual {p0}, Landroid/widget/ActivityChooserModel;->notifyChanged()V

    #@14
    .line 605
    :cond_14
    monitor-exit v1

    #@15
    goto :goto_8

    #@16
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public setDefaultActivity(I)V
    .registers 11
    .parameter "index"

    #@0
    .prologue
    .line 542
    iget-object v6, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 543
    :try_start_3
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@6
    .line 545
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@8
    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@e
    .line 546
    .local v2, newDefaultActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    iget-object v5, p0, Landroid/widget/ActivityChooserModel;->mActivities:Ljava/util/List;

    #@10
    const/4 v7, 0x0

    #@11
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;

    #@17
    .line 549
    .local v3, oldDefaultActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    if-eqz v3, :cond_41

    #@19
    .line 551
    iget v5, v3, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->weight:F

    #@1b
    iget v7, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->weight:F

    #@1d
    sub-float/2addr v5, v7

    #@1e
    const/high16 v7, 0x40a0

    #@20
    add-float v4, v5, v7

    #@22
    .line 557
    .local v4, weight:F
    :goto_22
    new-instance v0, Landroid/content/ComponentName;

    #@24
    iget-object v5, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@26
    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@28
    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@2a
    iget-object v7, v2, Landroid/widget/ActivityChooserModel$ActivityResolveInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    #@2c
    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2e
    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@30
    invoke-direct {v0, v5, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 560
    .local v0, defaultName:Landroid/content/ComponentName;
    new-instance v1, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@38
    move-result-wide v7

    #@39
    invoke-direct {v1, v0, v7, v8, v4}, Landroid/widget/ActivityChooserModel$HistoricalRecord;-><init>(Landroid/content/ComponentName;JF)V

    #@3c
    .line 562
    .local v1, historicalRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    invoke-direct {p0, v1}, Landroid/widget/ActivityChooserModel;->addHisoricalRecord(Landroid/widget/ActivityChooserModel$HistoricalRecord;)Z

    #@3f
    .line 563
    monitor-exit v6

    #@40
    .line 564
    return-void

    #@41
    .line 554
    .end local v0           #defaultName:Landroid/content/ComponentName;
    .end local v1           #historicalRecord:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    .end local v4           #weight:F
    :cond_41
    const/high16 v4, 0x3f80

    #@43
    .restart local v4       #weight:F
    goto :goto_22

    #@44
    .line 563
    .end local v2           #newDefaultActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    .end local v3           #oldDefaultActivity:Landroid/widget/ActivityChooserModel$ActivityResolveInfo;
    .end local v4           #weight:F
    :catchall_44
    move-exception v5

    #@45
    monitor-exit v6
    :try_end_46
    .catchall {:try_start_3 .. :try_end_46} :catchall_44

    #@46
    throw v5
.end method

.method public setHistoryMaxSize(I)V
    .registers 4
    .parameter "historyMaxSize"

    #@0
    .prologue
    .line 622
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 623
    :try_start_3
    iget v0, p0, Landroid/widget/ActivityChooserModel;->mHistoryMaxSize:I

    #@5
    if-ne v0, p1, :cond_9

    #@7
    .line 624
    monitor-exit v1

    #@8
    .line 632
    :goto_8
    return-void

    #@9
    .line 626
    :cond_9
    iput p1, p0, Landroid/widget/ActivityChooserModel;->mHistoryMaxSize:I

    #@b
    .line 627
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->pruneExcessiveHistoricalRecordsIfNeeded()V

    #@e
    .line 628
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->sortActivitiesIfNeeded()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 629
    invoke-virtual {p0}, Landroid/widget/ActivityChooserModel;->notifyChanged()V

    #@17
    .line 631
    :cond_17
    monitor-exit v1

    #@18
    goto :goto_8

    #@19
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 376
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 377
    :try_start_3
    iget-object v0, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@5
    if-ne v0, p1, :cond_9

    #@7
    .line 378
    monitor-exit v1

    #@8
    .line 384
    :goto_8
    return-void

    #@9
    .line 380
    :cond_9
    iput-object p1, p0, Landroid/widget/ActivityChooserModel;->mIntent:Landroid/content/Intent;

    #@b
    .line 381
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/widget/ActivityChooserModel;->mReloadActivities:Z

    #@e
    .line 382
    invoke-direct {p0}, Landroid/widget/ActivityChooserModel;->ensureConsistentState()V

    #@11
    .line 383
    monitor-exit v1

    #@12
    goto :goto_8

    #@13
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public setOnChooseActivityListener(Landroid/widget/ActivityChooserModel$OnChooseActivityListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 507
    iget-object v1, p0, Landroid/widget/ActivityChooserModel;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 508
    :try_start_3
    iput-object p1, p0, Landroid/widget/ActivityChooserModel;->mActivityChoserModelPolicy:Landroid/widget/ActivityChooserModel$OnChooseActivityListener;

    #@5
    .line 509
    monitor-exit v1

    #@6
    .line 510
    return-void

    #@7
    .line 509
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method
