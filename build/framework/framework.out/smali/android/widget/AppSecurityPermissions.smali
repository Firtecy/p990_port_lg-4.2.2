.class public Landroid/widget/AppSecurityPermissions;
.super Ljava/lang/Object;
.source "AppSecurityPermissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;,
        Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;,
        Landroid/widget/AppSecurityPermissions$PermissionItemView;,
        Landroid/widget/AppSecurityPermissions$MyPermissionInfo;,
        Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AppSecurityPermissions"

.field public static final WHICH_ALL:I = 0xffff

.field public static final WHICH_DEVICE:I = 0x2

.field public static final WHICH_NEW:I = 0x4

.field public static final WHICH_PERSONAL:I = 0x1

.field private static final localLOGV:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDangerousIcon:Landroid/graphics/drawable/Drawable;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mInstalledPackageInfo:Landroid/content/pm/PackageInfo;

.field private mNewPermPrefix:Ljava/lang/CharSequence;

.field private mNormalIcon:Landroid/graphics/drawable/Drawable;

.field private final mPermComparator:Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

.field private final mPermGroupComparator:Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

.field private final mPermGroups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPermGroupsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPermsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPm:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageInfo;)V
    .registers 12
    .parameter "context"
    .parameter "info"

    #@0
    .prologue
    .line 273
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    new-instance v6, Ljava/util/HashMap;

    #@5
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@a
    .line 75
    new-instance v6, Ljava/util/ArrayList;

    #@c
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@11
    .line 274
    iput-object p1, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@13
    .line 275
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v6

    #@19
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@1b
    .line 276
    invoke-direct {p0}, Landroid/widget/AppSecurityPermissions;->loadResources()V

    #@1e
    .line 277
    new-instance v6, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@20
    invoke-direct {v6}, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;-><init>()V

    #@23
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermComparator:Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@25
    .line 278
    new-instance v6, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@27
    invoke-direct {v6}, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;-><init>()V

    #@2a
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupComparator:Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@2c
    .line 279
    new-instance v6, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@33
    .line 280
    new-instance v3, Ljava/util/HashSet;

    #@35
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    #@38
    .line 281
    .local v3, permSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    if-nez p2, :cond_3b

    #@3a
    .line 311
    :goto_3a
    return-void

    #@3b
    .line 286
    :cond_3b
    const/4 v2, 0x0

    #@3c
    .line 288
    .local v2, installedPkgInfo:Landroid/content/pm/PackageInfo;
    iget-object v6, p2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@3e
    if-eqz v6, :cond_4d

    #@40
    .line 290
    :try_start_40
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@42
    iget-object v7, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@44
    const/16 v8, 0x1000

    #@46
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_49
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_40 .. :try_end_49} :catch_94

    #@49
    move-result-object v2

    #@4a
    .line 294
    :goto_4a
    invoke-direct {p0, p2, v3, v2}, Landroid/widget/AppSecurityPermissions;->extractPerms(Landroid/content/pm/PackageInfo;Ljava/util/Set;Landroid/content/pm/PackageInfo;)V

    #@4d
    .line 297
    :cond_4d
    iget-object v6, p2, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    #@4f
    if-eqz v6, :cond_5c

    #@51
    .line 300
    :try_start_51
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@53
    iget-object v7, p2, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    #@55
    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getUidForSharedUser(Ljava/lang/String;)I

    #@58
    move-result v4

    #@59
    .line 301
    .local v4, sharedUid:I
    invoke-direct {p0, v4, v3}, Landroid/widget/AppSecurityPermissions;->getAllUsedPermissions(ILjava/util/Set;)V
    :try_end_5c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_51 .. :try_end_5c} :catch_72

    #@5c
    .line 307
    .end local v4           #sharedUid:I
    :cond_5c
    :goto_5c
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5f
    move-result-object v1

    #@60
    .local v1, i$:Ljava/util/Iterator;
    :goto_60
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@63
    move-result v6

    #@64
    if-eqz v6, :cond_8e

    #@66
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@69
    move-result-object v5

    #@6a
    check-cast v5, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@6c
    .line 308
    .local v5, tmpInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@6e
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@71
    goto :goto_60

    #@72
    .line 302
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v5           #tmpInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    :catch_72
    move-exception v0

    #@73
    .line 303
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "AppSecurityPermissions"

    #@75
    new-instance v7, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v8, "Could\'nt retrieve shared user id for:"

    #@7c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    iget-object v8, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@82
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v7

    #@8a
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_5c

    #@8e
    .line 310
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_8e
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@90
    invoke-direct {p0, v6}, Landroid/widget/AppSecurityPermissions;->setPermissions(Ljava/util/List;)V

    #@93
    goto :goto_3a

    #@94
    .line 292
    .end local v1           #i$:Ljava/util/Iterator;
    :catch_94
    move-exception v6

    #@95
    goto :goto_4a
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 11
    .parameter "context"
    .parameter "packageName"

    #@0
    .prologue
    .line 248
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    new-instance v5, Ljava/util/HashMap;

    #@5
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@a
    .line 75
    new-instance v5, Ljava/util/ArrayList;

    #@c
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@11
    .line 249
    iput-object p1, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@13
    .line 250
    iget-object v5, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v5

    #@19
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@1b
    .line 251
    invoke-direct {p0}, Landroid/widget/AppSecurityPermissions;->loadResources()V

    #@1e
    .line 252
    new-instance v5, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@20
    invoke-direct {v5}, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;-><init>()V

    #@23
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermComparator:Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@25
    .line 253
    new-instance v5, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@27
    invoke-direct {v5}, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;-><init>()V

    #@2a
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupComparator:Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@2c
    .line 254
    new-instance v5, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@33
    .line 255
    new-instance v2, Ljava/util/HashSet;

    #@35
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@38
    .line 258
    .local v2, permSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    :try_start_38
    iget-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@3a
    const/16 v6, 0x1000

    #@3c
    invoke-virtual {v5, p2, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_3f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_38 .. :try_end_3f} :catch_68

    #@3f
    move-result-object v3

    #@40
    .line 264
    .local v3, pkgInfo:Landroid/content/pm/PackageInfo;
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@42
    if-eqz v5, :cond_52

    #@44
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@46
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@48
    const/4 v6, -0x1

    #@49
    if-eq v5, v6, :cond_52

    #@4b
    .line 265
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4d
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    #@4f
    invoke-direct {p0, v5, v2}, Landroid/widget/AppSecurityPermissions;->getAllUsedPermissions(ILjava/util/Set;)V

    #@52
    .line 267
    :cond_52
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v1

    #@56
    .local v1, i$:Ljava/util/Iterator;
    :goto_56
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v5

    #@5a
    if-eqz v5, :cond_82

    #@5c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v4

    #@60
    check-cast v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@62
    .line 268
    .local v4, tmpInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    iget-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@64
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@67
    goto :goto_56

    #@68
    .line 259
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #pkgInfo:Landroid/content/pm/PackageInfo;
    .end local v4           #tmpInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    :catch_68
    move-exception v0

    #@69
    .line 260
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "AppSecurityPermissions"

    #@6b
    new-instance v6, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v7, "Couldn\'t retrieve permissions for package:"

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 271
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_81
    return-void

    #@82
    .line 270
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_82
    iget-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@84
    invoke-direct {p0, v5}, Landroid/widget/AppSecurityPermissions;->setPermissions(Ljava/util/List;)V

    #@87
    goto :goto_81
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 7
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PermissionInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 236
    .local p2, permList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    new-instance v2, Ljava/util/HashMap;

    #@5
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@a
    .line 75
    new-instance v2, Ljava/util/ArrayList;

    #@c
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@11
    .line 237
    iput-object p1, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@13
    .line 238
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@15
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@18
    move-result-object v2

    #@19
    iput-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@1b
    .line 239
    invoke-direct {p0}, Landroid/widget/AppSecurityPermissions;->loadResources()V

    #@1e
    .line 240
    new-instance v2, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@20
    invoke-direct {v2}, Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;-><init>()V

    #@23
    iput-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermComparator:Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@25
    .line 241
    new-instance v2, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@27
    invoke-direct {v2}, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;-><init>()V

    #@2a
    iput-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupComparator:Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@2c
    .line 242
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v0

    #@30
    .local v0, i$:Ljava/util/Iterator;
    :goto_30
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_47

    #@36
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/content/pm/PermissionInfo;

    #@3c
    .line 243
    .local v1, pi:Landroid/content/pm/PermissionInfo;
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@3e
    new-instance v3, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@40
    invoke-direct {v3, v1}, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;-><init>(Landroid/content/pm/PermissionInfo;)V

    #@43
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@46
    goto :goto_30

    #@47
    .line 245
    .end local v1           #pi:Landroid/content/pm/PermissionInfo;
    :cond_47
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermsList:Ljava/util/List;

    #@49
    invoke-direct {p0, v2}, Landroid/widget/AppSecurityPermissions;->setPermissions(Ljava/util/List;)V

    #@4c
    .line 246
    return-void
.end method

.method private addPermToList(Ljava/util/List;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;)V
    .registers 5
    .parameter
    .parameter "pInfo"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 603
    .local p1, permList:Ljava/util/List;,"Ljava/util/List<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    iget-object v1, p2, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 604
    iget-object v1, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@6
    invoke-virtual {p2, v1}, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p2, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    #@c
    .line 606
    :cond_c
    iget-object v1, p0, Landroid/widget/AppSecurityPermissions;->mPermComparator:Landroid/widget/AppSecurityPermissions$PermissionInfoComparator;

    #@e
    invoke-static {p1, p2, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    #@11
    move-result v0

    #@12
    .line 608
    .local v0, idx:I
    if-gez v0, :cond_1a

    #@14
    .line 609
    neg-int v1, v0

    #@15
    add-int/lit8 v0, v1, -0x1

    #@17
    .line 610
    invoke-interface {p1, v0, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@1a
    .line 612
    :cond_1a
    return-void
.end method

.method private displayPermissions(Ljava/util/List;Landroid/widget/LinearLayout;I)V
    .registers 14
    .parameter
    .parameter "permListView"
    .parameter "which"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 494
    .local p1, groups:Ljava/util/List;,"Ljava/util/List<Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;>;"
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    #@3
    .line 496
    const/high16 v8, 0x4100

    #@5
    iget-object v9, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v9

    #@b
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@e
    move-result-object v9

    #@f
    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    #@11
    mul-float/2addr v8, v9

    #@12
    float-to-int v6, v8

    #@13
    .line 498
    .local v6, spacing:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@17
    move-result v8

    #@18
    if-ge v1, v8, :cond_6f

    #@1a
    .line 499
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@20
    .line 500
    .local v0, grp:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    invoke-direct {p0, v0, p3}, Landroid/widget/AppSecurityPermissions;->getPermissionList(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;I)Ljava/util/List;

    #@23
    move-result-object v5

    #@24
    .line 501
    .local v5, perms:Ljava/util/List;,"Ljava/util/List<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    const/4 v2, 0x0

    #@25
    .local v2, j:I
    :goto_25
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@28
    move-result v8

    #@29
    if-ge v2, v8, :cond_6c

    #@2b
    .line 502
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@31
    .line 503
    .local v4, perm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    if-nez v2, :cond_67

    #@33
    const/4 v8, 0x1

    #@34
    move v9, v8

    #@35
    :goto_35
    const/4 v8, 0x4

    #@36
    if-eq p3, v8, :cond_6a

    #@38
    iget-object v8, p0, Landroid/widget/AppSecurityPermissions;->mNewPermPrefix:Ljava/lang/CharSequence;

    #@3a
    :goto_3a
    invoke-direct {p0, v0, v4, v9, v8}, Landroid/widget/AppSecurityPermissions;->getPermissionItemView(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)Landroid/widget/AppSecurityPermissions$PermissionItemView;

    #@3d
    move-result-object v7

    #@3e
    .line 505
    .local v7, view:Landroid/view/View;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    #@40
    const/4 v8, -0x1

    #@41
    const/4 v9, -0x2

    #@42
    invoke-direct {v3, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@45
    .line 508
    .local v3, lp:Landroid/widget/LinearLayout$LayoutParams;
    if-nez v2, :cond_49

    #@47
    .line 509
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@49
    .line 511
    :cond_49
    iget-object v8, v0, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@4e
    move-result v8

    #@4f
    add-int/lit8 v8, v8, -0x1

    #@51
    if-ne v2, v8, :cond_55

    #@53
    .line 512
    iput v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@55
    .line 514
    :cond_55
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    #@58
    move-result v8

    #@59
    if-nez v8, :cond_61

    #@5b
    .line 515
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@5d
    mul-int/lit8 v8, v8, 0x2

    #@5f
    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@61
    .line 517
    :cond_61
    invoke-virtual {p2, v7, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@64
    .line 501
    add-int/lit8 v2, v2, 0x1

    #@66
    goto :goto_25

    #@67
    .line 503
    .end local v3           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v7           #view:Landroid/view/View;
    :cond_67
    const/4 v8, 0x0

    #@68
    move v9, v8

    #@69
    goto :goto_35

    #@6a
    :cond_6a
    const/4 v8, 0x0

    #@6b
    goto :goto_3a

    #@6c
    .line 498
    .end local v4           #perm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    :cond_6c
    add-int/lit8 v1, v1, 0x1

    #@6e
    goto :goto_14

    #@6f
    .line 520
    .end local v0           #grp:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v2           #j:I
    .end local v5           #perms:Ljava/util/List;,"Ljava/util/List<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    :cond_6f
    return-void
.end method

.method private extractPerms(Landroid/content/pm/PackageInfo;Ljava/util/Set;Landroid/content/pm/PackageInfo;)V
    .registers 25
    .parameter "info"
    .parameter
    .parameter "installedPkgInfo"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageInfo;",
            "Ljava/util/Set",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;",
            "Landroid/content/pm/PackageInfo;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 365
    .local p2, permSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@4
    move-object/from16 v16, v0

    #@6
    .line 366
    .local v16, strList:[Ljava/lang/String;
    move-object/from16 v0, p1

    #@8
    iget-object v6, v0, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    #@a
    .line 367
    .local v6, flagsList:[I
    if-eqz v16, :cond_13

    #@c
    move-object/from16 v0, v16

    #@e
    array-length v0, v0

    #@f
    move/from16 v18, v0

    #@11
    if-nez v18, :cond_14

    #@13
    .line 443
    :cond_13
    return-void

    #@14
    .line 370
    :cond_14
    move-object/from16 v0, p3

    #@16
    move-object/from16 v1, p0

    #@18
    iput-object v0, v1, Landroid/widget/AppSecurityPermissions;->mInstalledPackageInfo:Landroid/content/pm/PackageInfo;

    #@1a
    .line 371
    const/4 v10, 0x0

    #@1b
    .local v10, i:I
    :goto_1b
    move-object/from16 v0, v16

    #@1d
    array-length v0, v0

    #@1e
    move/from16 v18, v0

    #@20
    move/from16 v0, v18

    #@22
    if-ge v10, v0, :cond_13

    #@24
    .line 372
    aget-object v15, v16, v10

    #@26
    .line 375
    .local v15, permName:Ljava/lang/String;
    if-eqz p3, :cond_37

    #@28
    move-object/from16 v0, p1

    #@2a
    move-object/from16 v1, p3

    #@2c
    if-ne v0, v1, :cond_37

    #@2e
    .line 376
    aget v18, v6, v10

    #@30
    and-int/lit8 v18, v18, 0x2

    #@32
    if-nez v18, :cond_37

    #@34
    .line 371
    :cond_34
    :goto_34
    add-int/lit8 v10, v10, 0x1

    #@36
    goto :goto_1b

    #@37
    .line 381
    :cond_37
    :try_start_37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@3b
    move-object/from16 v18, v0

    #@3d
    const/16 v19, 0x0

    #@3f
    move-object/from16 v0, v18

    #@41
    move/from16 v1, v19

    #@43
    invoke-virtual {v0, v15, v1}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    #@46
    move-result-object v17

    #@47
    .line 382
    .local v17, tmpPermInfo:Landroid/content/pm/PermissionInfo;
    if-eqz v17, :cond_34

    #@49
    .line 385
    const/4 v5, -0x1

    #@4a
    .line 386
    .local v5, existingIndex:I
    if-eqz p3, :cond_75

    #@4c
    move-object/from16 v0, p3

    #@4e
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@50
    move-object/from16 v18, v0

    #@52
    if-eqz v18, :cond_75

    #@54
    .line 388
    const/4 v11, 0x0

    #@55
    .local v11, j:I
    :goto_55
    move-object/from16 v0, p3

    #@57
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@59
    move-object/from16 v18, v0

    #@5b
    move-object/from16 v0, v18

    #@5d
    array-length v0, v0

    #@5e
    move/from16 v18, v0

    #@60
    move/from16 v0, v18

    #@62
    if-ge v11, v0, :cond_75

    #@64
    .line 389
    move-object/from16 v0, p3

    #@66
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@68
    move-object/from16 v18, v0

    #@6a
    aget-object v18, v18, v11

    #@6c
    move-object/from16 v0, v18

    #@6e
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v18

    #@72
    if-eqz v18, :cond_115

    #@74
    .line 390
    move v5, v11

    #@75
    .line 395
    .end local v11           #j:I
    :cond_75
    if-ltz v5, :cond_119

    #@77
    move-object/from16 v0, p3

    #@79
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    #@7b
    move-object/from16 v18, v0

    #@7d
    aget v4, v18, v5

    #@7f
    .line 397
    .local v4, existingFlags:I
    :goto_7f
    aget v18, v6, v10

    #@81
    move-object/from16 v0, p0

    #@83
    move-object/from16 v1, v17

    #@85
    move/from16 v2, v18

    #@87
    invoke-direct {v0, v1, v2, v4}, Landroid/widget/AppSecurityPermissions;->isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z

    #@8a
    move-result v18

    #@8b
    if-eqz v18, :cond_34

    #@8d
    .line 402
    move-object/from16 v0, v17

    #@8f
    iget-object v14, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@91
    .line 403
    .local v14, origGroupName:Ljava/lang/String;
    move-object v8, v14

    #@92
    .line 404
    .local v8, groupName:Ljava/lang/String;
    if-nez v8, :cond_9c

    #@94
    .line 405
    move-object/from16 v0, v17

    #@96
    iget-object v8, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@98
    .line 406
    move-object/from16 v0, v17

    #@9a
    iput-object v8, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@9c
    .line 408
    :cond_9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@a0
    move-object/from16 v18, v0

    #@a2
    move-object/from16 v0, v18

    #@a4
    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a7
    move-result-object v7

    #@a8
    check-cast v7, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@aa
    .line 409
    .local v7, group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    if-nez v7, :cond_d9

    #@ac
    .line 410
    const/4 v9, 0x0

    #@ad
    .line 411
    .local v9, grp:Landroid/content/pm/PermissionGroupInfo;
    if-eqz v14, :cond_bf

    #@af
    .line 412
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@b3
    move-object/from16 v18, v0

    #@b5
    const/16 v19, 0x0

    #@b7
    move-object/from16 v0, v18

    #@b9
    move/from16 v1, v19

    #@bb
    invoke-virtual {v0, v14, v1}, Landroid/content/pm/PackageManager;->getPermissionGroupInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;

    #@be
    move-result-object v9

    #@bf
    .line 414
    :cond_bf
    if-eqz v9, :cond_11c

    #@c1
    .line 415
    new-instance v7, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@c3
    .end local v7           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    invoke-direct {v7, v9}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;-><init>(Landroid/content/pm/PermissionGroupInfo;)V

    #@c6
    .line 428
    .restart local v7       #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    :goto_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@ca
    move-object/from16 v18, v0

    #@cc
    move-object/from16 v0, v17

    #@ce
    iget-object v0, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@d0
    move-object/from16 v19, v0

    #@d2
    move-object/from16 v0, v18

    #@d4
    move-object/from16 v1, v19

    #@d6
    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d9
    .line 430
    .end local v9           #grp:Landroid/content/pm/PermissionGroupInfo;
    :cond_d9
    if-eqz p3, :cond_14c

    #@db
    and-int/lit8 v18, v4, 0x2

    #@dd
    if-nez v18, :cond_14c

    #@df
    const/4 v13, 0x1

    #@e0
    .line 432
    .local v13, newPerm:Z
    :goto_e0
    new-instance v12, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@e2
    move-object/from16 v0, v17

    #@e4
    invoke-direct {v12, v0}, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;-><init>(Landroid/content/pm/PermissionInfo;)V

    #@e7
    .line 433
    .local v12, myPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    aget v18, v6, v10

    #@e9
    move/from16 v0, v18

    #@eb
    iput v0, v12, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mNewReqFlags:I

    #@ed
    .line 434
    iput v4, v12, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mExistingReqFlags:I

    #@ef
    .line 437
    iput-boolean v13, v12, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mNew:Z

    #@f1
    .line 438
    move-object/from16 v0, p2

    #@f3
    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_f6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_37 .. :try_end_f6} :catch_f8

    #@f6
    goto/16 :goto_34

    #@f8
    .line 439
    .end local v4           #existingFlags:I
    .end local v5           #existingIndex:I
    .end local v7           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v8           #groupName:Ljava/lang/String;
    .end local v12           #myPerm:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    .end local v13           #newPerm:Z
    .end local v14           #origGroupName:Ljava/lang/String;
    .end local v17           #tmpPermInfo:Landroid/content/pm/PermissionInfo;
    :catch_f8
    move-exception v3

    #@f9
    .line 440
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v18, "AppSecurityPermissions"

    #@fb
    new-instance v19, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v20, "Ignoring unknown permission:"

    #@102
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v19

    #@106
    move-object/from16 v0, v19

    #@108
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v19

    #@10c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v19

    #@110
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    goto/16 :goto_34

    #@115
    .line 388
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #existingIndex:I
    .restart local v11       #j:I
    .restart local v17       #tmpPermInfo:Landroid/content/pm/PermissionInfo;
    :cond_115
    add-int/lit8 v11, v11, 0x1

    #@117
    goto/16 :goto_55

    #@119
    .line 395
    .end local v11           #j:I
    :cond_119
    const/4 v4, 0x0

    #@11a
    goto/16 :goto_7f

    #@11c
    .line 421
    .restart local v4       #existingFlags:I
    .restart local v7       #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    .restart local v8       #groupName:Ljava/lang/String;
    .restart local v9       #grp:Landroid/content/pm/PermissionGroupInfo;
    .restart local v14       #origGroupName:Ljava/lang/String;
    :cond_11c
    :try_start_11c
    move-object/from16 v0, v17

    #@11e
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@120
    move-object/from16 v18, v0

    #@122
    move-object/from16 v0, v18

    #@124
    move-object/from16 v1, v17

    #@126
    iput-object v0, v1, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@128
    .line 422
    move-object/from16 v0, p0

    #@12a
    iget-object v0, v0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@12c
    move-object/from16 v18, v0

    #@12e
    move-object/from16 v0, v17

    #@130
    iget-object v0, v0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@132
    move-object/from16 v19, v0

    #@134
    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@137
    move-result-object v7

    #@138
    .end local v7           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    check-cast v7, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@13a
    .line 423
    .restart local v7       #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    if-nez v7, :cond_143

    #@13c
    .line 424
    new-instance v7, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@13e
    .end local v7           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    move-object/from16 v0, v17

    #@140
    invoke-direct {v7, v0}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;-><init>(Landroid/content/pm/PermissionInfo;)V

    #@143
    .line 426
    .restart local v7       #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    :cond_143
    new-instance v7, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@145
    .end local v7           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    move-object/from16 v0, v17

    #@147
    invoke-direct {v7, v0}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;-><init>(Landroid/content/pm/PermissionInfo;)V
    :try_end_14a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11c .. :try_end_14a} :catch_f8

    #@14a
    .restart local v7       #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    goto/16 :goto_c6

    #@14c
    .line 430
    .end local v9           #grp:Landroid/content/pm/PermissionGroupInfo;
    :cond_14c
    const/4 v13, 0x0

    #@14d
    goto :goto_e0
.end method

.method private getAllUsedPermissions(ILjava/util/Set;)V
    .registers 9
    .parameter "sharedUid"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 340
    .local p2, permSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    iget-object v5, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@2
    invoke-virtual {v5, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 341
    .local v4, sharedPkgList:[Ljava/lang/String;
    if-eqz v4, :cond_b

    #@8
    array-length v5, v4

    #@9
    if-nez v5, :cond_c

    #@b
    .line 347
    :cond_b
    return-void

    #@c
    .line 344
    :cond_c
    move-object v0, v4

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@e
    .local v2, len$:I
    const/4 v1, 0x0

    #@f
    .local v1, i$:I
    :goto_f
    if-ge v1, v2, :cond_b

    #@11
    aget-object v3, v0, v1

    #@13
    .line 345
    .local v3, sharedPkg:Ljava/lang/String;
    invoke-direct {p0, v3, p2}, Landroid/widget/AppSecurityPermissions;->getPermissionsForPackage(Ljava/lang/String;Ljava/util/Set;)V

    #@16
    .line 344
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_f
.end method

.method public static getPermissionItemView(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/view/View;
    .registers 10
    .parameter "context"
    .parameter "grpName"
    .parameter "description"
    .parameter "dangerous"

    #@0
    .prologue
    .line 327
    const-string/jumbo v0, "layout_inflater"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/view/LayoutInflater;

    #@9
    .line 329
    .local v1, inflater:Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v2

    #@d
    if-eqz p3, :cond_1f

    #@f
    const v0, 0x10802b5

    #@12
    :goto_12
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@15
    move-result-object v5

    #@16
    .local v5, icon:Landroid/graphics/drawable/Drawable;
    move-object v0, p0

    #@17
    move-object v2, p1

    #@18
    move-object v3, p2

    #@19
    move v4, p3

    #@1a
    .line 331
    invoke-static/range {v0 .. v5}, Landroid/widget/AppSecurityPermissions;->getPermissionItemViewOld(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/graphics/drawable/Drawable;)Landroid/view/View;

    #@1d
    move-result-object v0

    #@1e
    return-object v0

    #@1f
    .line 329
    .end local v5           #icon:Landroid/graphics/drawable/Drawable;
    :cond_1f
    const v0, 0x1080365

    #@22
    goto :goto_12
.end method

.method private static getPermissionItemView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)Landroid/widget/AppSecurityPermissions$PermissionItemView;
    .registers 9
    .parameter "context"
    .parameter "inflater"
    .parameter "grp"
    .parameter "perm"
    .parameter "first"
    .parameter "newPermPrefix"

    #@0
    .prologue
    .line 530
    iget v1, p3, Landroid/content/pm/PermissionInfo;->flags:I

    #@2
    and-int/lit8 v1, v1, 0x1

    #@4
    if-eqz v1, :cond_14

    #@6
    const v1, 0x109002b

    #@9
    :goto_9
    const/4 v2, 0x0

    #@a
    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/widget/AppSecurityPermissions$PermissionItemView;

    #@10
    .line 534
    .local v0, permView:Landroid/widget/AppSecurityPermissions$PermissionItemView;
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/widget/AppSecurityPermissions$PermissionItemView;->setPermission(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)V

    #@13
    .line 535
    return-object v0

    #@14
    .line 530
    .end local v0           #permView:Landroid/widget/AppSecurityPermissions$PermissionItemView;
    :cond_14
    const v1, 0x109002a

    #@17
    goto :goto_9
.end method

.method private getPermissionItemView(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)Landroid/widget/AppSecurityPermissions$PermissionItemView;
    .registers 11
    .parameter "grp"
    .parameter "perm"
    .parameter "first"
    .parameter "newPermPrefix"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Landroid/widget/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    #@4
    move-object v2, p1

    #@5
    move-object v3, p2

    #@6
    move v4, p3

    #@7
    move-object v5, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/widget/AppSecurityPermissions;->getPermissionItemView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;ZLjava/lang/CharSequence;)Landroid/widget/AppSecurityPermissions$PermissionItemView;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private static getPermissionItemViewOld(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/graphics/drawable/Drawable;)Landroid/view/View;
    .registers 12
    .parameter "context"
    .parameter "inflater"
    .parameter "grpName"
    .parameter "permList"
    .parameter "dangerous"
    .parameter "icon"

    #@0
    .prologue
    .line 540
    const v4, 0x109002c

    #@3
    const/4 v5, 0x0

    #@4
    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@7
    move-result-object v3

    #@8
    .line 542
    .local v3, permView:Landroid/view/View;
    const v4, 0x102027b

    #@b
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/widget/TextView;

    #@11
    .line 543
    .local v2, permGrpView:Landroid/widget/TextView;
    const v4, 0x102027c

    #@14
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/widget/TextView;

    #@1a
    .line 545
    .local v1, permDescView:Landroid/widget/TextView;
    const v4, 0x1020277

    #@1d
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/widget/ImageView;

    #@23
    .line 546
    .local v0, imgView:Landroid/widget/ImageView;
    invoke-virtual {v0, p5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@26
    .line 547
    if-eqz p2, :cond_2f

    #@28
    .line 548
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2b
    .line 549
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2e
    .line 554
    :goto_2e
    return-object v3

    #@2f
    .line 551
    :cond_2f
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@32
    .line 552
    const/16 v4, 0x8

    #@34
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    #@37
    goto :goto_2e
.end method

.method private getPermissionList(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;I)Ljava/util/List;
    .registers 4
    .parameter "grp"
    .parameter "which"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 450
    const/4 v0, 0x4

    #@1
    if-ne p2, v0, :cond_6

    #@3
    .line 451
    iget-object v0, p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mNewPermissions:Ljava/util/ArrayList;

    #@5
    .line 457
    :goto_5
    return-object v0

    #@6
    .line 452
    :cond_6
    const/4 v0, 0x1

    #@7
    if-ne p2, v0, :cond_c

    #@9
    .line 453
    iget-object v0, p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mPersonalPermissions:Ljava/util/ArrayList;

    #@b
    goto :goto_5

    #@c
    .line 454
    :cond_c
    const/4 v0, 0x2

    #@d
    if-ne p2, v0, :cond_12

    #@f
    .line 455
    iget-object v0, p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mDevicePermissions:Ljava/util/ArrayList;

    #@11
    goto :goto_5

    #@12
    .line 457
    :cond_12
    iget-object v0, p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    #@14
    goto :goto_5
.end method

.method private getPermissionsForPackage(Ljava/lang/String;Ljava/util/Set;)V
    .registers 8
    .parameter "packageName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 353
    .local p2, permSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    :try_start_0
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@2
    const/16 v3, 0x1000

    #@4
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_7} :catch_12

    #@7
    move-result-object v1

    #@8
    .line 358
    .local v1, pkgInfo:Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_11

    #@a
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@c
    if-eqz v2, :cond_11

    #@e
    .line 359
    invoke-direct {p0, v1, p2, v1}, Landroid/widget/AppSecurityPermissions;->extractPerms(Landroid/content/pm/PackageInfo;Ljava/util/Set;Landroid/content/pm/PackageInfo;)V

    #@11
    .line 361
    .end local v1           #pkgInfo:Landroid/content/pm/PackageInfo;
    :cond_11
    :goto_11
    return-void

    #@12
    .line 354
    :catch_12
    move-exception v0

    #@13
    .line 355
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "AppSecurityPermissions"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Couldn\'t retrieve permissions for package:"

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_11
.end method

.method private isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z
    .registers 7
    .parameter "pInfo"
    .parameter "newReqFlags"
    .parameter "existingReqFlags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 559
    iget v2, p1, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@3
    and-int/lit8 v0, v2, 0xf

    #@5
    .line 561
    .local v0, base:I
    if-eq v0, v1, :cond_9

    #@7
    if-nez v0, :cond_a

    #@9
    .line 574
    :cond_9
    :goto_9
    return v1

    #@a
    .line 568
    :cond_a
    and-int/lit8 v2, p3, 0x2

    #@c
    if-eqz v2, :cond_14

    #@e
    iget v2, p1, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@10
    and-int/lit8 v2, v2, 0x20

    #@12
    if-nez v2, :cond_9

    #@14
    .line 574
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_9
.end method

.method private loadResources()V
    .registers 3

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@2
    const v1, 0x104045b

    #@5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/widget/AppSecurityPermissions;->mNewPermPrefix:Ljava/lang/CharSequence;

    #@b
    .line 316
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v0

    #@11
    const v1, 0x1080365

    #@14
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/widget/AppSecurityPermissions;->mNormalIcon:Landroid/graphics/drawable/Drawable;

    #@1a
    .line 317
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v0

    #@20
    const v1, 0x10802b5

    #@23
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v0

    #@27
    iput-object v0, p0, Landroid/widget/AppSecurityPermissions;->mDangerousIcon:Landroid/graphics/drawable/Drawable;

    #@29
    .line 318
    return-void
.end method

.method private setPermissions(Ljava/util/List;)V
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/AppSecurityPermissions$MyPermissionInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 615
    .local p1, permList:Ljava/util/List;,"Ljava/util/List<Landroid/widget/AppSecurityPermissions$MyPermissionInfo;>;"
    if-eqz p1, :cond_50

    #@2
    .line 617
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v3

    #@6
    .local v3, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_50

    #@c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v4

    #@10
    check-cast v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;

    #@12
    .line 619
    .local v4, pInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    iget v6, v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mNewReqFlags:I

    #@14
    iget v7, v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mExistingReqFlags:I

    #@16
    invoke-direct {p0, v4, v6, v7}, Landroid/widget/AppSecurityPermissions;->isDisplayablePermission(Landroid/content/pm/PermissionInfo;II)Z

    #@19
    move-result v6

    #@1a
    if-eqz v6, :cond_6

    #@1c
    .line 623
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@1e
    iget-object v7, v4, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@20
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@26
    .line 624
    .local v2, group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    if-eqz v2, :cond_6

    #@28
    .line 625
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@2a
    invoke-virtual {v4, v6}, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@2d
    move-result-object v6

    #@2e
    iput-object v6, v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mLabel:Ljava/lang/CharSequence;

    #@30
    .line 626
    iget-object v6, v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mAllPermissions:Ljava/util/ArrayList;

    #@32
    invoke-direct {p0, v6, v4}, Landroid/widget/AppSecurityPermissions;->addPermToList(Ljava/util/List;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;)V

    #@35
    .line 627
    iget-boolean v6, v4, Landroid/widget/AppSecurityPermissions$MyPermissionInfo;->mNew:Z

    #@37
    if-eqz v6, :cond_3e

    #@39
    .line 628
    iget-object v6, v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mNewPermissions:Ljava/util/ArrayList;

    #@3b
    invoke-direct {p0, v6, v4}, Landroid/widget/AppSecurityPermissions;->addPermToList(Ljava/util/List;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;)V

    #@3e
    .line 630
    :cond_3e
    iget v6, v2, Landroid/content/pm/PermissionGroupInfo;->flags:I

    #@40
    and-int/lit8 v6, v6, 0x1

    #@42
    if-eqz v6, :cond_4a

    #@44
    .line 631
    iget-object v6, v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mPersonalPermissions:Ljava/util/ArrayList;

    #@46
    invoke-direct {p0, v6, v4}, Landroid/widget/AppSecurityPermissions;->addPermToList(Ljava/util/List;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;)V

    #@49
    goto :goto_6

    #@4a
    .line 633
    :cond_4a
    iget-object v6, v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mDevicePermissions:Ljava/util/ArrayList;

    #@4c
    invoke-direct {p0, v6, v4}, Landroid/widget/AppSecurityPermissions;->addPermToList(Ljava/util/List;Landroid/widget/AppSecurityPermissions$MyPermissionInfo;)V

    #@4f
    goto :goto_6

    #@50
    .line 639
    .end local v2           #group:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #pInfo:Landroid/widget/AppSecurityPermissions$MyPermissionInfo;
    :cond_50
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroups:Ljava/util/Map;

    #@52
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@55
    move-result-object v6

    #@56
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@59
    move-result-object v3

    #@5a
    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_5a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@5d
    move-result v6

    #@5e
    if-eqz v6, :cond_98

    #@60
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@63
    move-result-object v5

    #@64
    check-cast v5, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@66
    .line 640
    .local v5, pgrp:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    iget v6, v5, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@68
    if-nez v6, :cond_6e

    #@6a
    iget-object v6, v5, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@6c
    if-eqz v6, :cond_7c

    #@6e
    .line 641
    :cond_6e
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@70
    invoke-virtual {v5, v6}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@73
    move-result-object v6

    #@74
    iput-object v6, v5, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    #@76
    .line 651
    :goto_76
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@78
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@7b
    goto :goto_5a

    #@7c
    .line 645
    :cond_7c
    :try_start_7c
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@7e
    iget-object v7, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@80
    const/4 v8, 0x0

    #@81
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@84
    move-result-object v0

    #@85
    .line 646
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@87
    invoke-virtual {v0, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@8a
    move-result-object v6

    #@8b
    iput-object v6, v5, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;
    :try_end_8d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7c .. :try_end_8d} :catch_8e

    #@8d
    goto :goto_76

    #@8e
    .line 647
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    :catch_8e
    move-exception v1

    #@8f
    .line 648
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPm:Landroid/content/pm/PackageManager;

    #@91
    invoke-virtual {v5, v6}, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@94
    move-result-object v6

    #@95
    iput-object v6, v5, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    #@97
    goto :goto_76

    #@98
    .line 653
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5           #pgrp:Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;
    :cond_98
    iget-object v6, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@9a
    iget-object v7, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupComparator:Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;

    #@9c
    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@9f
    .line 661
    return-void
.end method


# virtual methods
.method public getInstalledPackageInfo()Landroid/content/pm/PackageInfo;
    .registers 2

    #@0
    .prologue
    .line 336
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions;->mInstalledPackageInfo:Landroid/content/pm/PackageInfo;

    #@2
    return-object v0
.end method

.method public getPermissionCount()I
    .registers 2

    #@0
    .prologue
    .line 446
    const v0, 0xffff

    #@3
    invoke-virtual {p0, v0}, Landroid/widget/AppSecurityPermissions;->getPermissionCount(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getPermissionCount(I)I
    .registers 5
    .parameter "which"

    #@0
    .prologue
    .line 462
    const/4 v0, 0x0

    #@1
    .line 463
    .local v0, N:I
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@4
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7
    move-result v2

    #@8
    if-ge v1, v2, :cond_1e

    #@a
    .line 464
    iget-object v2, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@c
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@12
    invoke-direct {p0, v2, p1}, Landroid/widget/AppSecurityPermissions;->getPermissionList(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;I)Ljava/util/List;

    #@15
    move-result-object v2

    #@16
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@19
    move-result v2

    #@1a
    add-int/2addr v0, v2

    #@1b
    .line 463
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_2

    #@1e
    .line 466
    :cond_1e
    return v0
.end method

.method public getPermissionsView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 470
    const v0, 0xffff

    #@3
    invoke-virtual {p0, v0}, Landroid/widget/AppSecurityPermissions;->getPermissionsView(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getPermissionsView(I)Landroid/view/View;
    .registers 8
    .parameter "which"

    #@0
    .prologue
    .line 474
    iget-object v3, p0, Landroid/widget/AppSecurityPermissions;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v4, "layout_inflater"

    #@5
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    check-cast v3, Landroid/view/LayoutInflater;

    #@b
    iput-object v3, p0, Landroid/widget/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    #@d
    .line 476
    iget-object v3, p0, Landroid/widget/AppSecurityPermissions;->mInflater:Landroid/view/LayoutInflater;

    #@f
    const v4, 0x109002d

    #@12
    const/4 v5, 0x0

    #@13
    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Landroid/widget/LinearLayout;

    #@19
    .line 477
    .local v2, permsView:Landroid/widget/LinearLayout;
    const v3, 0x102027e

    #@1c
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/widget/LinearLayout;

    #@22
    .line 478
    .local v0, displayList:Landroid/widget/LinearLayout;
    const v3, 0x102027d

    #@25
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v1

    #@29
    .line 480
    .local v1, noPermsView:Landroid/view/View;
    iget-object v3, p0, Landroid/widget/AppSecurityPermissions;->mPermGroupsList:Ljava/util/List;

    #@2b
    invoke-direct {p0, v3, v0, p1}, Landroid/widget/AppSecurityPermissions;->displayPermissions(Ljava/util/List;Landroid/widget/LinearLayout;I)V

    #@2e
    .line 481
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    #@31
    move-result v3

    #@32
    if-gtz v3, :cond_38

    #@34
    .line 482
    const/4 v3, 0x0

    #@35
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    #@38
    .line 485
    :cond_38
    return-object v2
.end method
