.class Landroid/widget/TextView$Drawables;
.super Ljava/lang/Object;
.source "TextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Drawables"
.end annotation


# static fields
.field static final DRAWABLE_LEFT:I = 0x1

.field static final DRAWABLE_NONE:I = -0x1

.field static final DRAWABLE_RIGHT:I


# instance fields
.field final mCompoundRect:Landroid/graphics/Rect;

.field mDrawableBottom:Landroid/graphics/drawable/Drawable;

.field mDrawableEnd:Landroid/graphics/drawable/Drawable;

.field mDrawableError:Landroid/graphics/drawable/Drawable;

.field mDrawableHeightEnd:I

.field mDrawableHeightError:I

.field mDrawableHeightLeft:I

.field mDrawableHeightRight:I

.field mDrawableHeightStart:I

.field mDrawableHeightTemp:I

.field mDrawableLeft:Landroid/graphics/drawable/Drawable;

.field mDrawablePadding:I

.field mDrawableRight:Landroid/graphics/drawable/Drawable;

.field mDrawableSaved:I

.field mDrawableSizeBottom:I

.field mDrawableSizeEnd:I

.field mDrawableSizeError:I

.field mDrawableSizeLeft:I

.field mDrawableSizeRight:I

.field mDrawableSizeStart:I

.field mDrawableSizeTemp:I

.field mDrawableSizeTop:I

.field mDrawableStart:Landroid/graphics/drawable/Drawable;

.field mDrawableTemp:Landroid/graphics/drawable/Drawable;

.field mDrawableTop:Landroid/graphics/drawable/Drawable;

.field mDrawableWidthBottom:I

.field mDrawableWidthTop:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 326
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 331
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mCompoundRect:Landroid/graphics/Rect;

    #@a
    .line 344
    const/4 v0, -0x1

    #@b
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSaved:I

    #@d
    return-void
.end method

.method private applyErrorDrawableIfNeeded(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 440
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSaved:I

    #@2
    packed-switch v0, :pswitch_data_5e

    #@5
    .line 455
    :goto_5
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@7
    if-eqz v0, :cond_27

    #@9
    .line 456
    packed-switch p1, :pswitch_data_66

    #@c
    .line 470
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSaved:I

    #@f
    .line 472
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@11
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@13
    .line 473
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@15
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@17
    .line 474
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@19
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@1b
    .line 476
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@1d
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@1f
    .line 477
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeError:I

    #@21
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@23
    .line 478
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightError:I

    #@25
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@27
    .line 482
    :cond_27
    :goto_27
    return-void

    #@28
    .line 442
    :pswitch_28
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@2a
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@2c
    .line 443
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@2e
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@30
    .line 444
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@32
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@34
    goto :goto_5

    #@35
    .line 447
    :pswitch_35
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@37
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@39
    .line 448
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@3b
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@3d
    .line 449
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@3f
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@41
    goto :goto_5

    #@42
    .line 458
    :pswitch_42
    const/4 v0, 0x1

    #@43
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSaved:I

    #@45
    .line 460
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@47
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@49
    .line 461
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@4b
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@4d
    .line 462
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@4f
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@51
    .line 464
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@53
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@55
    .line 465
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeError:I

    #@57
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@59
    .line 466
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightError:I

    #@5b
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@5d
    goto :goto_27

    #@5e
    .line 440
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_35
        :pswitch_28
    .end packed-switch

    #@66
    .line 456
    :pswitch_data_66
    .packed-switch 0x1
        :pswitch_42
    .end packed-switch
.end method

.method private updateDrawablesLayoutDirection(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 404
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 405
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@9
    .line 407
    :cond_9
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 408
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@12
    .line 410
    :cond_12
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 411
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@18
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@1b
    .line 413
    :cond_1b
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 414
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@21
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@24
    .line 416
    :cond_24
    return-void
.end method


# virtual methods
.method public resolveWithLayoutDirection(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 347
    packed-switch p1, :pswitch_data_86

    #@3
    .line 385
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 386
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@9
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@b
    .line 388
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@d
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@f
    .line 389
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@11
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@13
    .line 391
    :cond_13
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@15
    if-eqz v0, :cond_23

    #@17
    .line 392
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@19
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@1b
    .line 394
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@1d
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@1f
    .line 395
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@21
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@23
    .line 399
    :cond_23
    :goto_23
    invoke-direct {p0, p1}, Landroid/widget/TextView$Drawables;->applyErrorDrawableIfNeeded(I)V

    #@26
    .line 400
    invoke-direct {p0, p1}, Landroid/widget/TextView$Drawables;->updateDrawablesLayoutDirection(I)V

    #@29
    .line 401
    return-void

    #@2a
    .line 351
    :pswitch_2a
    invoke-static {}, Landroid/widget/TextView;->access$000()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_64

    #@30
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@32
    if-nez v0, :cond_64

    #@34
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@36
    if-nez v0, :cond_64

    #@38
    .line 353
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@3a
    if-nez v0, :cond_40

    #@3c
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@3e
    if-eqz v0, :cond_64

    #@40
    .line 354
    :cond_40
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@42
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@44
    .line 355
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@46
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@48
    .line 356
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@4a
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@4c
    .line 358
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@4e
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@50
    .line 359
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@52
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@54
    .line 360
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@56
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@58
    .line 362
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableTemp:Landroid/graphics/drawable/Drawable;

    #@5a
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@5c
    .line 363
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeTemp:I

    #@5e
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@60
    .line 364
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightTemp:I

    #@62
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@64
    .line 369
    :cond_64
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@66
    if-eqz v0, :cond_74

    #@68
    .line 370
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@6a
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@6c
    .line 372
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@6e
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@70
    .line 373
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@72
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@74
    .line 375
    :cond_74
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@76
    if-eqz v0, :cond_23

    #@78
    .line 376
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@7a
    iput-object v0, p0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@7c
    .line 378
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@7e
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@80
    .line 379
    iget v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@82
    iput v0, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@84
    goto :goto_23

    #@85
    .line 347
    nop

    #@86
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_2a
    .end packed-switch
.end method

.method public setErrorDrawable(Landroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V
    .registers 7
    .parameter "dr"
    .parameter "tv"

    #@0
    .prologue
    .line 419
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq v2, p1, :cond_e

    #@4
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v2, :cond_e

    #@8
    .line 420
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@e
    .line 422
    :cond_e
    iput-object p1, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@10
    .line 424
    iget-object v0, p0, Landroid/widget/TextView$Drawables;->mCompoundRect:Landroid/graphics/Rect;

    #@12
    .line 425
    .local v0, compoundRect:Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/widget/TextView;->getDrawableState()[I

    #@15
    move-result-object v1

    #@16
    .line 427
    .local v1, state:[I
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@18
    if-eqz v2, :cond_36

    #@1a
    .line 428
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@1c
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@1f
    .line 429
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@21
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@24
    .line 430
    iget-object v2, p0, Landroid/widget/TextView$Drawables;->mDrawableError:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {v2, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@29
    .line 431
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@2c
    move-result v2

    #@2d
    iput v2, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeError:I

    #@2f
    .line 432
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@32
    move-result v2

    #@33
    iput v2, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightError:I

    #@35
    .line 436
    :goto_35
    return-void

    #@36
    .line 434
    :cond_36
    const/4 v2, 0x0

    #@37
    iput v2, p0, Landroid/widget/TextView$Drawables;->mDrawableHeightError:I

    #@39
    iput v2, p0, Landroid/widget/TextView$Drawables;->mDrawableSizeError:I

    #@3b
    goto :goto_35
.end method
