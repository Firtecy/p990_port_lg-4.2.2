.class public Landroid/widget/ZoomControls;
.super Landroid/widget/LinearLayout;
.source "ZoomControls.java"


# instance fields
.field private final mZoomIn:Landroid/widget/ZoomButton;

.field private final mZoomOut:Landroid/widget/ZoomButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ZoomControls;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 47
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v1}, Landroid/widget/ZoomControls;->setFocusable(Z)V

    #@7
    .line 49
    const-string/jumbo v1, "layout_inflater"

    #@a
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/view/LayoutInflater;

    #@10
    .line 51
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x10900f0

    #@13
    const/4 v2, 0x1

    #@14
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@17
    .line 54
    const v1, 0x10203c7

    #@1a
    invoke-virtual {p0, v1}, Landroid/widget/ZoomControls;->findViewById(I)Landroid/view/View;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/widget/ZoomButton;

    #@20
    iput-object v1, p0, Landroid/widget/ZoomControls;->mZoomIn:Landroid/widget/ZoomButton;

    #@22
    .line 55
    const v1, 0x10203c6

    #@25
    invoke-virtual {p0, v1}, Landroid/widget/ZoomControls;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Landroid/widget/ZoomButton;

    #@2b
    iput-object v1, p0, Landroid/widget/ZoomControls;->mZoomOut:Landroid/widget/ZoomButton;

    #@2d
    .line 56
    return-void
.end method

.method private fade(IFF)V
    .registers 7
    .parameter "visibility"
    .parameter "startAlpha"
    .parameter "endAlpha"

    #@0
    .prologue
    .line 93
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    #@2
    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    #@5
    .line 94
    .local v0, anim:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    #@a
    .line 95
    invoke-virtual {p0, v0}, Landroid/widget/ZoomControls;->startAnimation(Landroid/view/animation/Animation;)V

    #@d
    .line 96
    invoke-virtual {p0, p1}, Landroid/widget/ZoomControls;->setVisibility(I)V

    #@10
    .line 97
    return-void
.end method


# virtual methods
.method public hasFocus()Z
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomIn:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0}, Landroid/widget/ZoomButton;->hasFocus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomOut:Landroid/widget/ZoomButton;

    #@a
    invoke-virtual {v0}, Landroid/widget/ZoomButton;->hasFocus()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public hide()V
    .registers 4

    #@0
    .prologue
    .line 89
    const/16 v0, 0x8

    #@2
    const/high16 v1, 0x3f80

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/ZoomControls;->fade(IFF)V

    #@8
    .line 90
    return-void
.end method

.method public isLayoutRtl()Z
    .registers 2

    #@0
    .prologue
    .line 130
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 115
    const-class v0, Landroid/widget/ZoomControls;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 116
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 121
    const-class v0, Landroid/widget/ZoomControls;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 122
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 81
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setIsZoomInEnabled(Z)V
    .registers 3
    .parameter "isEnabled"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomIn:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomButton;->setEnabled(Z)V

    #@5
    .line 101
    return-void
.end method

.method public setIsZoomOutEnabled(Z)V
    .registers 3
    .parameter "isEnabled"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomOut:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomButton;->setEnabled(Z)V

    #@5
    .line 105
    return-void
.end method

.method public setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomIn:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@5
    .line 60
    return-void
.end method

.method public setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomOut:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@5
    .line 64
    return-void
.end method

.method public setZoomSpeed(J)V
    .registers 4
    .parameter "speed"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomIn:Landroid/widget/ZoomButton;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/ZoomButton;->setZoomSpeed(J)V

    #@5
    .line 72
    iget-object v0, p0, Landroid/widget/ZoomControls;->mZoomOut:Landroid/widget/ZoomButton;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/widget/ZoomButton;->setZoomSpeed(J)V

    #@a
    .line 73
    return-void
.end method

.method public show()V
    .registers 4

    #@0
    .prologue
    .line 85
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    const/high16 v2, 0x3f80

    #@4
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/ZoomControls;->fade(IFF)V

    #@7
    .line 86
    return-void
.end method
