.class Landroid/widget/SimpleAdapter$SimpleFilter;
.super Landroid/widget/Filter;
.source "SimpleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/SimpleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleFilter"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/SimpleAdapter;


# direct methods
.method private constructor <init>(Landroid/widget/SimpleAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@2
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/SimpleAdapter;Landroid/widget/SimpleAdapter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 323
    invoke-direct {p0, p1}, Landroid/widget/SimpleAdapter$SimpleFilter;-><init>(Landroid/widget/SimpleAdapter;)V

    #@3
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 21
    .parameter "prefix"

    #@0
    .prologue
    .line 327
    new-instance v10, Landroid/widget/Filter$FilterResults;

    #@2
    invoke-direct {v10}, Landroid/widget/Filter$FilterResults;-><init>()V

    #@5
    .line 329
    .local v10, results:Landroid/widget/Filter$FilterResults;
    move-object/from16 v0, p0

    #@7
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@9
    move-object/from16 v16, v0

    #@b
    invoke-static/range {v16 .. v16}, Landroid/widget/SimpleAdapter;->access$100(Landroid/widget/SimpleAdapter;)Ljava/util/ArrayList;

    #@e
    move-result-object v16

    #@f
    if-nez v16, :cond_29

    #@11
    .line 330
    move-object/from16 v0, p0

    #@13
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@15
    move-object/from16 v16, v0

    #@17
    new-instance v17, Ljava/util/ArrayList;

    #@19
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@1d
    move-object/from16 v18, v0

    #@1f
    invoke-static/range {v18 .. v18}, Landroid/widget/SimpleAdapter;->access$200(Landroid/widget/SimpleAdapter;)Ljava/util/List;

    #@22
    move-result-object v18

    #@23
    invoke-direct/range {v17 .. v18}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@26
    invoke-static/range {v16 .. v17}, Landroid/widget/SimpleAdapter;->access$102(Landroid/widget/SimpleAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@29
    .line 333
    :cond_29
    if-eqz p1, :cond_31

    #@2b
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@2e
    move-result v16

    #@2f
    if-nez v16, :cond_46

    #@31
    .line 334
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@35
    move-object/from16 v16, v0

    #@37
    invoke-static/range {v16 .. v16}, Landroid/widget/SimpleAdapter;->access$100(Landroid/widget/SimpleAdapter;)Ljava/util/ArrayList;

    #@3a
    move-result-object v7

    #@3b
    .line 335
    .local v7, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;*>;>;"
    iput-object v7, v10, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@3d
    .line 336
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@40
    move-result v16

    #@41
    move/from16 v0, v16

    #@43
    iput v0, v10, Landroid/widget/Filter$FilterResults;->count:I

    #@45
    .line 373
    .end local v7           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;*>;>;"
    :goto_45
    return-object v10

    #@46
    .line 338
    :cond_46
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@49
    move-result-object v16

    #@4a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@4d
    move-result-object v9

    #@4e
    .line 340
    .local v9, prefixString:Ljava/lang/String;
    move-object/from16 v0, p0

    #@50
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@52
    move-object/from16 v16, v0

    #@54
    invoke-static/range {v16 .. v16}, Landroid/widget/SimpleAdapter;->access$100(Landroid/widget/SimpleAdapter;)Ljava/util/ArrayList;

    #@57
    move-result-object v12

    #@58
    .line 341
    .local v12, unfilteredValues:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;*>;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@5b
    move-result v1

    #@5c
    .line 343
    .local v1, count:I
    new-instance v8, Ljava/util/ArrayList;

    #@5e
    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@61
    .line 345
    .local v8, newValues:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/Map<Ljava/lang/String;*>;>;"
    const/4 v3, 0x0

    #@62
    .local v3, i:I
    :goto_62
    if-ge v3, v1, :cond_b6

    #@64
    .line 346
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@67
    move-result-object v2

    #@68
    check-cast v2, Ljava/util/Map;

    #@6a
    .line 347
    .local v2, h:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v2, :cond_b3

    #@6c
    .line 349
    move-object/from16 v0, p0

    #@6e
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@70
    move-object/from16 v16, v0

    #@72
    invoke-static/range {v16 .. v16}, Landroid/widget/SimpleAdapter;->access$300(Landroid/widget/SimpleAdapter;)[I

    #@75
    move-result-object v16

    #@76
    move-object/from16 v0, v16

    #@78
    array-length v6, v0

    #@79
    .line 351
    .local v6, len:I
    const/4 v4, 0x0

    #@7a
    .local v4, j:I
    :goto_7a
    if-ge v4, v6, :cond_b3

    #@7c
    .line 352
    move-object/from16 v0, p0

    #@7e
    iget-object v0, v0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@80
    move-object/from16 v16, v0

    #@82
    invoke-static/range {v16 .. v16}, Landroid/widget/SimpleAdapter;->access$400(Landroid/widget/SimpleAdapter;)[Ljava/lang/String;

    #@85
    move-result-object v16

    #@86
    aget-object v16, v16, v4

    #@88
    move-object/from16 v0, v16

    #@8a
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8d
    move-result-object v11

    #@8e
    check-cast v11, Ljava/lang/String;

    #@90
    .line 354
    .local v11, str:Ljava/lang/String;
    const-string v16, " "

    #@92
    move-object/from16 v0, v16

    #@94
    invoke-virtual {v11, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@97
    move-result-object v15

    #@98
    .line 355
    .local v15, words:[Ljava/lang/String;
    array-length v14, v15

    #@99
    .line 357
    .local v14, wordCount:I
    const/4 v5, 0x0

    #@9a
    .local v5, k:I
    :goto_9a
    if-ge v5, v14, :cond_ad

    #@9c
    .line 358
    aget-object v13, v15, v5

    #@9e
    .line 360
    .local v13, word:Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@a1
    move-result-object v16

    #@a2
    move-object/from16 v0, v16

    #@a4
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@a7
    move-result v16

    #@a8
    if-eqz v16, :cond_b0

    #@aa
    .line 361
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ad
    .line 351
    .end local v13           #word:Ljava/lang/String;
    :cond_ad
    add-int/lit8 v4, v4, 0x1

    #@af
    goto :goto_7a

    #@b0
    .line 357
    .restart local v13       #word:Ljava/lang/String;
    :cond_b0
    add-int/lit8 v5, v5, 0x1

    #@b2
    goto :goto_9a

    #@b3
    .line 345
    .end local v4           #j:I
    .end local v5           #k:I
    .end local v6           #len:I
    .end local v11           #str:Ljava/lang/String;
    .end local v13           #word:Ljava/lang/String;
    .end local v14           #wordCount:I
    .end local v15           #words:[Ljava/lang/String;
    :cond_b3
    add-int/lit8 v3, v3, 0x1

    #@b5
    goto :goto_62

    #@b6
    .line 369
    .end local v2           #h:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    :cond_b6
    iput-object v8, v10, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@b8
    .line 370
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@bb
    move-result v16

    #@bc
    move/from16 v0, v16

    #@be
    iput v0, v10, Landroid/widget/Filter$FilterResults;->count:I

    #@c0
    goto :goto_45
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .parameter "constraint"
    .parameter "results"

    #@0
    .prologue
    .line 379
    iget-object v1, p0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@2
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    #@4
    check-cast v0, Ljava/util/List;

    #@6
    invoke-static {v1, v0}, Landroid/widget/SimpleAdapter;->access$202(Landroid/widget/SimpleAdapter;Ljava/util/List;)Ljava/util/List;

    #@9
    .line 380
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    #@b
    if-lez v0, :cond_13

    #@d
    .line 381
    iget-object v0, p0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@f
    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    #@12
    .line 385
    :goto_12
    return-void

    #@13
    .line 383
    :cond_13
    iget-object v0, p0, Landroid/widget/SimpleAdapter$SimpleFilter;->this$0:Landroid/widget/SimpleAdapter;

    #@15
    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->notifyDataSetInvalidated()V

    #@18
    goto :goto_12
.end method
