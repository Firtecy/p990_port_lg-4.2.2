.class public Landroid/widget/EdgeEffect;
.super Ljava/lang/Object;
.source "EdgeEffect.java"


# static fields
.field private static final EPSILON:F = 0.001f

.field private static final HELD_EDGE_SCALE_Y:F = 0.5f

.field private static final MAX_ALPHA:F = 1.0f

.field private static final MAX_GLOW_HEIGHT:F = 4.0f

.field private static final MIN_VELOCITY:I = 0x64

.field private static final MIN_WIDTH:I = 0x12c

.field private static final PULL_DECAY_TIME:I = 0x3e8

.field private static final PULL_DISTANCE_ALPHA_GLOW_FACTOR:F = 1.1f

.field private static final PULL_DISTANCE_EDGE_FACTOR:I = 0x7

.field private static final PULL_DISTANCE_GLOW_FACTOR:I = 0x7

.field private static final PULL_EDGE_BEGIN:F = 0.6f

.field private static final PULL_GLOW_BEGIN:F = 1.0f

.field private static final PULL_TIME:I = 0xa7

.field private static final RECEDE_TIME:I = 0x3e8

.field private static final STATE_ABSORB:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PULL:I = 0x1

.field private static final STATE_PULL_DECAY:I = 0x4

.field private static final STATE_RECEDE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "EdgeEffect"

.field private static final VELOCITY_EDGE_FACTOR:I = 0x8

.field private static final VELOCITY_GLOW_FACTOR:I = 0x10


# instance fields
.field private final mBounds:Landroid/graphics/Rect;

.field private mDuration:F

.field private final mEdge:Landroid/graphics/drawable/Drawable;

.field private mEdgeAlpha:F

.field private mEdgeAlphaFinish:F

.field private mEdgeAlphaStart:F

.field private final mEdgeHeight:I

.field private mEdgeScaleY:F

.field private mEdgeScaleYFinish:F

.field private mEdgeScaleYStart:F

.field private final mGlow:Landroid/graphics/drawable/Drawable;

.field private mGlowAlpha:F

.field private mGlowAlphaFinish:F

.field private mGlowAlphaStart:F

.field private final mGlowHeight:I

.field private mGlowScaleY:F

.field private mGlowScaleYFinish:F

.field private mGlowScaleYStart:F

.field private final mGlowWidth:I

.field private mHeight:I

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private final mMaxEffectHeight:I

.field private final mMinWidth:I

.field private mPullDistance:F

.field private mStartTime:J

.field private mState:I

.field private mWidth:I

.field private mX:I

.field private mY:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/high16 v4, 0x4080

    #@2
    const/high16 v3, 0x3f00

    #@4
    .line 135
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 120
    const/4 v1, 0x0

    #@8
    iput v1, p0, Landroid/widget/EdgeEffect;->mState:I

    #@a
    .line 124
    new-instance v1, Landroid/graphics/Rect;

    #@c
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v1, p0, Landroid/widget/EdgeEffect;->mBounds:Landroid/graphics/Rect;

    #@11
    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v0

    #@15
    .line 137
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x108043f

    #@18
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 138
    const v1, 0x1080440

    #@21
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@27
    .line 140
    iget-object v1, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@29
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Landroid/widget/EdgeEffect;->mEdgeHeight:I

    #@2f
    .line 141
    iget-object v1, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@31
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@34
    move-result v1

    #@35
    iput v1, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@37
    .line 142
    iget-object v1, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@39
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@3c
    move-result v1

    #@3d
    iput v1, p0, Landroid/widget/EdgeEffect;->mGlowWidth:I

    #@3f
    .line 144
    iget v1, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@41
    int-to-float v1, v1

    #@42
    mul-float/2addr v1, v4

    #@43
    iget v2, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@45
    int-to-float v2, v2

    #@46
    mul-float/2addr v1, v2

    #@47
    iget v2, p0, Landroid/widget/EdgeEffect;->mGlowWidth:I

    #@49
    int-to-float v2, v2

    #@4a
    div-float/2addr v1, v2

    #@4b
    const v2, 0x3f19999a

    #@4e
    mul-float/2addr v1, v2

    #@4f
    iget v2, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@51
    int-to-float v2, v2

    #@52
    mul-float/2addr v2, v4

    #@53
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    #@56
    move-result v1

    #@57
    add-float/2addr v1, v3

    #@58
    float-to-int v1, v1

    #@59
    iput v1, p0, Landroid/widget/EdgeEffect;->mMaxEffectHeight:I

    #@5b
    .line 148
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@5e
    move-result-object v1

    #@5f
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    #@61
    const/high16 v2, 0x4396

    #@63
    mul-float/2addr v1, v2

    #@64
    add-float/2addr v1, v3

    #@65
    float-to-int v1, v1

    #@66
    iput v1, p0, Landroid/widget/EdgeEffect;->mMinWidth:I

    #@68
    .line 149
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    #@6a
    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@6d
    iput-object v1, p0, Landroid/widget/EdgeEffect;->mInterpolator:Landroid/view/animation/Interpolator;

    #@6f
    .line 150
    return-void
.end method

.method private update()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/high16 v10, 0x447a

    #@3
    const/high16 v9, 0x3f80

    #@5
    const/4 v8, 0x0

    #@6
    .line 380
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@9
    move-result-wide v3

    #@a
    .line 381
    .local v3, time:J
    iget-wide v5, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@c
    sub-long v5, v3, v5

    #@e
    long-to-float v5, v5

    #@f
    iget v6, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@11
    div-float/2addr v5, v6

    #@12
    invoke-static {v5, v9}, Ljava/lang/Math;->min(FF)F

    #@15
    move-result v2

    #@16
    .line 383
    .local v2, t:F
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mInterpolator:Landroid/view/animation/Interpolator;

    #@18
    invoke-interface {v5, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@1b
    move-result v1

    #@1c
    .line 385
    .local v1, interp:F
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@1e
    iget v6, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@20
    iget v7, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@22
    sub-float/2addr v6, v7

    #@23
    mul-float/2addr v6, v1

    #@24
    add-float/2addr v5, v6

    #@25
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@27
    .line 386
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@29
    iget v6, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@2b
    iget v7, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@2d
    sub-float/2addr v6, v7

    #@2e
    mul-float/2addr v6, v1

    #@2f
    add-float/2addr v5, v6

    #@30
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@32
    .line 387
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@34
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@36
    iget v7, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@38
    sub-float/2addr v6, v7

    #@39
    mul-float/2addr v6, v1

    #@3a
    add-float/2addr v5, v6

    #@3b
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@3d
    .line 388
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@3f
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@41
    iget v7, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@43
    sub-float/2addr v6, v7

    #@44
    mul-float/2addr v6, v1

    #@45
    add-float/2addr v5, v6

    #@46
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@48
    .line 390
    const v5, 0x3f7fbe77

    #@4b
    cmpl-float v5, v2, v5

    #@4d
    if-ltz v5, :cond_54

    #@4f
    .line 391
    iget v5, p0, Landroid/widget/EdgeEffect;->mState:I

    #@51
    packed-switch v5, :pswitch_data_c0

    #@54
    .line 440
    :cond_54
    :goto_54
    return-void

    #@55
    .line 393
    :pswitch_55
    iput v11, p0, Landroid/widget/EdgeEffect;->mState:I

    #@57
    .line 394
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@5a
    move-result-wide v5

    #@5b
    iput-wide v5, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@5d
    .line 395
    iput v10, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@5f
    .line 397
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@61
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@63
    .line 398
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@65
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@67
    .line 399
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@69
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@6b
    .line 400
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@6d
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@6f
    .line 403
    iput v8, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@71
    .line 404
    iput v8, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@73
    .line 405
    iput v8, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@75
    .line 406
    iput v8, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@77
    goto :goto_54

    #@78
    .line 409
    :pswitch_78
    const/4 v5, 0x4

    #@79
    iput v5, p0, Landroid/widget/EdgeEffect;->mState:I

    #@7b
    .line 410
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@7e
    move-result-wide v5

    #@7f
    iput-wide v5, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@81
    .line 411
    iput v10, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@83
    .line 413
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@85
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@87
    .line 414
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@89
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@8b
    .line 415
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@8d
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@8f
    .line 416
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@91
    iput v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@93
    .line 419
    iput v8, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@95
    .line 420
    iput v8, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@97
    .line 421
    iput v8, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@99
    .line 422
    iput v8, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@9b
    goto :goto_54

    #@9c
    .line 427
    :pswitch_9c
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@9e
    cmpl-float v5, v5, v8

    #@a0
    if-eqz v5, :cond_b8

    #@a2
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@a4
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@a6
    mul-float/2addr v5, v6

    #@a7
    div-float v0, v9, v5

    #@a9
    .line 430
    .local v0, factor:F
    :goto_a9
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@ab
    iget v6, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@ad
    iget v7, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@af
    sub-float/2addr v6, v7

    #@b0
    mul-float/2addr v6, v1

    #@b1
    mul-float/2addr v6, v0

    #@b2
    add-float/2addr v5, v6

    #@b3
    iput v5, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@b5
    .line 433
    iput v11, p0, Landroid/widget/EdgeEffect;->mState:I

    #@b7
    goto :goto_54

    #@b8
    .line 427
    .end local v0           #factor:F
    :cond_b8
    const v0, 0x7f7fffff

    #@bb
    goto :goto_a9

    #@bc
    .line 436
    :pswitch_bc
    const/4 v5, 0x0

    #@bd
    iput v5, p0, Landroid/widget/EdgeEffect;->mState:I

    #@bf
    goto :goto_54

    #@c0
    .line 391
    :pswitch_data_c0
    .packed-switch 0x1
        :pswitch_78
        :pswitch_55
        :pswitch_bc
        :pswitch_9c
    .end packed-switch
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)Z
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    const/high16 v10, 0x437f

    #@2
    const/high16 v9, 0x3f80

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v4, 0x0

    #@6
    .line 329
    invoke-direct {p0}, Landroid/widget/EdgeEffect;->update()V

    #@9
    .line 331
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@b
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@d
    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    #@10
    move-result v6

    #@11
    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    #@14
    move-result v6

    #@15
    mul-float/2addr v6, v10

    #@16
    float-to-int v6, v6

    #@17
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@1a
    .line 333
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@1c
    int-to-float v5, v5

    #@1d
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@1f
    mul-float/2addr v5, v6

    #@20
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@22
    int-to-float v6, v6

    #@23
    mul-float/2addr v5, v6

    #@24
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowWidth:I

    #@26
    int-to-float v6, v6

    #@27
    div-float/2addr v5, v6

    #@28
    const v6, 0x3f19999a

    #@2b
    mul-float/2addr v5, v6

    #@2c
    iget v6, p0, Landroid/widget/EdgeEffect;->mGlowHeight:I

    #@2e
    int-to-float v6, v6

    #@2f
    const/high16 v7, 0x4080

    #@31
    mul-float/2addr v6, v7

    #@32
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    #@35
    move-result v5

    #@36
    float-to-int v2, v5

    #@37
    .line 336
    .local v2, glowBottom:I
    iget v5, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@39
    iget v6, p0, Landroid/widget/EdgeEffect;->mMinWidth:I

    #@3b
    if-ge v5, v6, :cond_94

    #@3d
    .line 338
    iget v5, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@3f
    iget v6, p0, Landroid/widget/EdgeEffect;->mMinWidth:I

    #@41
    sub-int/2addr v5, v6

    #@42
    div-int/lit8 v3, v5, 0x2

    #@44
    .line 339
    .local v3, glowLeft:I
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@46
    iget v6, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@48
    sub-int/2addr v6, v3

    #@49
    invoke-virtual {v5, v3, v4, v6, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@4c
    .line 345
    .end local v3           #glowLeft:I
    :goto_4c
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@4e
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@51
    .line 347
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@53
    iget v6, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@55
    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    #@58
    move-result v6

    #@59
    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    #@5c
    move-result v6

    #@5d
    mul-float/2addr v6, v10

    #@5e
    float-to-int v6, v6

    #@5f
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@62
    .line 349
    iget v5, p0, Landroid/widget/EdgeEffect;->mEdgeHeight:I

    #@64
    int-to-float v5, v5

    #@65
    iget v6, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@67
    mul-float/2addr v5, v6

    #@68
    float-to-int v0, v5

    #@69
    .line 350
    .local v0, edgeBottom:I
    iget v5, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@6b
    iget v6, p0, Landroid/widget/EdgeEffect;->mMinWidth:I

    #@6d
    if-ge v5, v6, :cond_9c

    #@6f
    .line 352
    iget v5, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@71
    iget v6, p0, Landroid/widget/EdgeEffect;->mMinWidth:I

    #@73
    sub-int/2addr v5, v6

    #@74
    div-int/lit8 v1, v5, 0x2

    #@76
    .line 353
    .local v1, edgeLeft:I
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@78
    iget v6, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@7a
    sub-int/2addr v6, v1

    #@7b
    invoke-virtual {v5, v1, v4, v6, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@7e
    .line 358
    .end local v1           #edgeLeft:I
    :goto_7e
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@80
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@83
    .line 360
    iget v5, p0, Landroid/widget/EdgeEffect;->mState:I

    #@85
    const/4 v6, 0x3

    #@86
    if-ne v5, v6, :cond_8e

    #@88
    if-nez v2, :cond_8e

    #@8a
    if-nez v0, :cond_8e

    #@8c
    .line 361
    iput v4, p0, Landroid/widget/EdgeEffect;->mState:I

    #@8e
    .line 364
    :cond_8e
    iget v5, p0, Landroid/widget/EdgeEffect;->mState:I

    #@90
    if-eqz v5, :cond_93

    #@92
    const/4 v4, 0x1

    #@93
    :cond_93
    return v4

    #@94
    .line 342
    .end local v0           #edgeBottom:I
    :cond_94
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    #@96
    iget v6, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@98
    invoke-virtual {v5, v4, v4, v6, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@9b
    goto :goto_4c

    #@9c
    .line 356
    .restart local v0       #edgeBottom:I
    :cond_9c
    iget-object v5, p0, Landroid/widget/EdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    #@9e
    iget v6, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@a0
    invoke-virtual {v5, v4, v4, v6, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@a3
    goto :goto_7e
.end method

.method public finish()V
    .registers 2

    #@0
    .prologue
    .line 191
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@3
    .line 192
    return-void
.end method

.method public getBounds(Z)Landroid/graphics/Rect;
    .registers 6
    .parameter "reverse"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 373
    iget-object v1, p0, Landroid/widget/EdgeEffect;->mBounds:Landroid/graphics/Rect;

    #@3
    iget v2, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@5
    iget v3, p0, Landroid/widget/EdgeEffect;->mMaxEffectHeight:I

    #@7
    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 374
    iget-object v1, p0, Landroid/widget/EdgeEffect;->mBounds:Landroid/graphics/Rect;

    #@c
    iget v2, p0, Landroid/widget/EdgeEffect;->mX:I

    #@e
    iget v3, p0, Landroid/widget/EdgeEffect;->mY:I

    #@10
    if-eqz p1, :cond_14

    #@12
    iget v0, p0, Landroid/widget/EdgeEffect;->mMaxEffectHeight:I

    #@14
    :cond_14
    sub-int v0, v3, v0

    #@16
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    #@19
    .line 376
    iget-object v0, p0, Landroid/widget/EdgeEffect;->mBounds:Landroid/graphics/Rect;

    #@1b
    return-object v0
.end method

.method public isFinished()Z
    .registers 2

    #@0
    .prologue
    .line 183
    iget v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onAbsorb(I)V
    .registers 8
    .parameter "velocity"

    #@0
    .prologue
    const/high16 v5, 0x3f80

    #@2
    const/high16 v4, 0x3f00

    #@4
    const/4 v3, 0x0

    #@5
    .line 285
    const/4 v0, 0x2

    #@6
    iput v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@8
    .line 286
    const/16 v0, 0x64

    #@a
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@d
    move-result v1

    #@e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@11
    move-result p1

    #@12
    .line 288
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@15
    move-result-wide v0

    #@16
    iput-wide v0, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@18
    .line 289
    const v0, 0x3dcccccd

    #@1b
    int-to-float v1, p1

    #@1c
    const v2, 0x3cf5c28f

    #@1f
    mul-float/2addr v1, v2

    #@20
    add-float/2addr v0, v1

    #@21
    iput v0, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@23
    .line 293
    iput v3, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@25
    .line 294
    iput v3, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@27
    iput v3, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@29
    .line 297
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@2b
    .line 298
    iput v3, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@2d
    .line 302
    const/4 v0, 0x0

    #@2e
    mul-int/lit8 v1, p1, 0x8

    #@30
    const/4 v2, 0x1

    #@31
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@34
    move-result v1

    #@35
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@38
    move-result v0

    #@39
    int-to-float v0, v0

    #@3a
    iput v0, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@3c
    .line 304
    mul-int/lit8 v0, p1, 0x8

    #@3e
    int-to-float v0, v0

    #@3f
    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    #@42
    move-result v0

    #@43
    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    #@46
    move-result v0

    #@47
    iput v0, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@49
    .line 311
    const v0, 0x3ccccccd

    #@4c
    div-int/lit8 v1, p1, 0x64

    #@4e
    mul-int/2addr v1, p1

    #@4f
    int-to-float v1, v1

    #@50
    const v2, 0x391d4952

    #@53
    mul-float/2addr v1, v2

    #@54
    add-float/2addr v0, v1

    #@55
    const/high16 v1, 0x3fe0

    #@57
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@5a
    move-result v0

    #@5b
    iput v0, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@5d
    .line 313
    iget v0, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@5f
    mul-int/lit8 v1, p1, 0x10

    #@61
    int-to-float v1, v1

    #@62
    const v2, 0x3727c5ac

    #@65
    mul-float/2addr v1, v2

    #@66
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    #@69
    move-result v1

    #@6a
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@6d
    move-result v0

    #@6e
    iput v0, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@70
    .line 315
    return-void
.end method

.method public onPull(F)V
    .registers 12
    .parameter "deltaDistance"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/high16 v9, 0x40e0

    #@3
    const/high16 v8, 0x3f80

    #@5
    const/4 v7, 0x0

    #@6
    .line 205
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@9
    move-result-wide v2

    #@a
    .line 206
    .local v2, now:J
    iget v4, p0, Landroid/widget/EdgeEffect;->mState:I

    #@c
    const/4 v5, 0x4

    #@d
    if-ne v4, v5, :cond_1b

    #@f
    iget-wide v4, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@11
    sub-long v4, v2, v4

    #@13
    long-to-float v4, v4

    #@14
    iget v5, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@16
    cmpg-float v4, v4, v5

    #@18
    if-gez v4, :cond_1b

    #@1a
    .line 244
    :goto_1a
    return-void

    #@1b
    .line 209
    :cond_1b
    iget v4, p0, Landroid/widget/EdgeEffect;->mState:I

    #@1d
    if-eq v4, v6, :cond_21

    #@1f
    .line 210
    iput v8, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@21
    .line 212
    :cond_21
    iput v6, p0, Landroid/widget/EdgeEffect;->mState:I

    #@23
    .line 214
    iput-wide v2, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@25
    .line 215
    const/high16 v4, 0x4327

    #@27
    iput v4, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@29
    .line 217
    iget v4, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@2b
    add-float/2addr v4, p1

    #@2c
    iput v4, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@2e
    .line 218
    iget v4, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@30
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@33
    move-result v0

    #@34
    .line 220
    .local v0, distance:F
    const v4, 0x3f19999a

    #@37
    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    #@3a
    move-result v5

    #@3b
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@3e
    move-result v4

    #@3f
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@41
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@43
    .line 221
    const/high16 v4, 0x3f00

    #@45
    mul-float v5, v0, v9

    #@47
    invoke-static {v5, v8}, Ljava/lang/Math;->min(FF)F

    #@4a
    move-result v5

    #@4b
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@4e
    move-result v4

    #@4f
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@51
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@53
    .line 224
    iget v4, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@55
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@58
    move-result v5

    #@59
    const v6, 0x3f8ccccd

    #@5c
    mul-float/2addr v5, v6

    #@5d
    add-float/2addr v4, v5

    #@5e
    invoke-static {v8, v4}, Ljava/lang/Math;->min(FF)F

    #@61
    move-result v4

    #@62
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@64
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@66
    .line 228
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@69
    move-result v1

    #@6a
    .line 229
    .local v1, glowChange:F
    cmpl-float v4, p1, v7

    #@6c
    if-lez v4, :cond_75

    #@6e
    iget v4, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@70
    cmpg-float v4, v4, v7

    #@72
    if-gez v4, :cond_75

    #@74
    .line 230
    neg-float v1, v1

    #@75
    .line 232
    :cond_75
    iget v4, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@77
    cmpl-float v4, v4, v7

    #@79
    if-nez v4, :cond_7d

    #@7b
    .line 233
    iput v7, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@7d
    .line 237
    :cond_7d
    const/high16 v4, 0x4080

    #@7f
    iget v5, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@81
    mul-float v6, v1, v9

    #@83
    add-float/2addr v5, v6

    #@84
    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    #@87
    move-result v5

    #@88
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    #@8b
    move-result v4

    #@8c
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@8e
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@90
    .line 240
    iget v4, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@92
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@94
    .line 241
    iget v4, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@96
    iput v4, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@98
    .line 242
    iget v4, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@9a
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@9c
    .line 243
    iget v4, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@9e
    iput v4, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@a0
    goto/16 :goto_1a
.end method

.method public onRelease()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 253
    iput v2, p0, Landroid/widget/EdgeEffect;->mPullDistance:F

    #@3
    .line 255
    iget v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@5
    const/4 v1, 0x1

    #@6
    if-eq v0, v1, :cond_e

    #@8
    iget v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@a
    const/4 v1, 0x4

    #@b
    if-eq v0, v1, :cond_e

    #@d
    .line 272
    :goto_d
    return-void

    #@e
    .line 259
    :cond_e
    const/4 v0, 0x3

    #@f
    iput v0, p0, Landroid/widget/EdgeEffect;->mState:I

    #@11
    .line 260
    iget v0, p0, Landroid/widget/EdgeEffect;->mEdgeAlpha:F

    #@13
    iput v0, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaStart:F

    #@15
    .line 261
    iget v0, p0, Landroid/widget/EdgeEffect;->mEdgeScaleY:F

    #@17
    iput v0, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYStart:F

    #@19
    .line 262
    iget v0, p0, Landroid/widget/EdgeEffect;->mGlowAlpha:F

    #@1b
    iput v0, p0, Landroid/widget/EdgeEffect;->mGlowAlphaStart:F

    #@1d
    .line 263
    iget v0, p0, Landroid/widget/EdgeEffect;->mGlowScaleY:F

    #@1f
    iput v0, p0, Landroid/widget/EdgeEffect;->mGlowScaleYStart:F

    #@21
    .line 265
    iput v2, p0, Landroid/widget/EdgeEffect;->mEdgeAlphaFinish:F

    #@23
    .line 266
    iput v2, p0, Landroid/widget/EdgeEffect;->mEdgeScaleYFinish:F

    #@25
    .line 267
    iput v2, p0, Landroid/widget/EdgeEffect;->mGlowAlphaFinish:F

    #@27
    .line 268
    iput v2, p0, Landroid/widget/EdgeEffect;->mGlowScaleYFinish:F

    #@29
    .line 270
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@2c
    move-result-wide v0

    #@2d
    iput-wide v0, p0, Landroid/widget/EdgeEffect;->mStartTime:J

    #@2f
    .line 271
    const/high16 v0, 0x447a

    #@31
    iput v0, p0, Landroid/widget/EdgeEffect;->mDuration:F

    #@33
    goto :goto_d
.end method

.method setPosition(II)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 171
    iput p1, p0, Landroid/widget/EdgeEffect;->mX:I

    #@2
    .line 172
    iput p2, p0, Landroid/widget/EdgeEffect;->mY:I

    #@4
    .line 173
    return-void
.end method

.method public setSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 159
    iput p1, p0, Landroid/widget/EdgeEffect;->mWidth:I

    #@2
    .line 160
    iput p2, p0, Landroid/widget/EdgeEffect;->mHeight:I

    #@4
    .line 161
    return-void
.end method
