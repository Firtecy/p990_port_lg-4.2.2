.class Landroid/widget/Editor$PositionListener;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositionListener"
.end annotation


# instance fields
.field private final MAXIMUM_NUMBER_OF_LISTENERS:I

.field private mCanMove:[Z

.field private mNumberOfListeners:I

.field private mPositionHasChanged:Z

.field private mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

.field private mPositionX:I

.field private mPositionY:I

.field private mScrollHasChanged:Z

.field final mTempCoords:[I

.field final mTempSize:[I

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method private constructor <init>(Landroid/widget/Editor;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x6

    #@2
    .line 2411
    iput-object p1, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 2414
    iput v1, p0, Landroid/widget/Editor$PositionListener;->MAXIMUM_NUMBER_OF_LISTENERS:I

    #@9
    .line 2415
    new-array v0, v1, [Landroid/widget/Editor$TextViewPositionListener;

    #@b
    iput-object v0, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@d
    .line 2417
    new-array v0, v1, [Z

    #@f
    iput-object v0, p0, Landroid/widget/Editor$PositionListener;->mCanMove:[Z

    #@11
    .line 2418
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@14
    .line 2423
    new-array v0, v2, [I

    #@16
    iput-object v0, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@18
    .line 2425
    new-array v0, v2, [I

    #@1a
    iput-object v0, p0, Landroid/widget/Editor$PositionListener;->mTempSize:[I

    #@1c
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2411
    invoke-direct {p0, p1}, Landroid/widget/Editor$PositionListener;-><init>(Landroid/widget/Editor;)V

    #@3
    return-void
.end method

.method private updatePosition()V
    .registers 12

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 2496
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v6

    #@8
    iget-object v9, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@a
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->getLocationInWindow([I)V

    #@d
    .line 2498
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@f
    if-eqz v6, :cond_4f

    #@11
    .line 2499
    new-instance v2, Landroid/graphics/Rect;

    #@13
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@16
    .line 2500
    .local v2, rect:Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    #@18
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@1b
    .line 2501
    .local v4, totalRect:Landroid/graphics/Rect;
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@1d
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->isWindowSplit(Landroid/graphics/Rect;)Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_4f

    #@27
    .line 2502
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@34
    move-result-object v6

    #@35
    const v9, 0x105000c

    #@38
    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@3b
    move-result v3

    #@3c
    .line 2503
    .local v3, statusBarHeight:I
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@3e
    aget v9, v6, v7

    #@40
    iget v10, v2, Landroid/graphics/Rect;->left:I

    #@42
    add-int/2addr v9, v10

    #@43
    aput v9, v6, v7

    #@45
    .line 2504
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@47
    aget v9, v6, v8

    #@49
    iget v10, v2, Landroid/graphics/Rect;->top:I

    #@4b
    sub-int/2addr v10, v3

    #@4c
    add-int/2addr v9, v10

    #@4d
    aput v9, v6, v8

    #@4f
    .line 2508
    .end local v2           #rect:Landroid/graphics/Rect;
    .end local v3           #statusBarHeight:I
    .end local v4           #totalRect:Landroid/graphics/Rect;
    :cond_4f
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@51
    aget v6, v6, v7

    #@53
    iget v9, p0, Landroid/widget/Editor$PositionListener;->mPositionX:I

    #@55
    if-ne v6, v9, :cond_5f

    #@57
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@59
    aget v6, v6, v8

    #@5b
    iget v9, p0, Landroid/widget/Editor$PositionListener;->mPositionY:I

    #@5d
    if-eq v6, v9, :cond_b8

    #@5f
    :cond_5f
    move v6, v8

    #@60
    :goto_60
    iput-boolean v6, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@62
    .line 2510
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@64
    if-eqz v6, :cond_ab

    #@66
    iget-boolean v6, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@68
    if-nez v6, :cond_ab

    #@6a
    .line 2511
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@6c
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    #@73
    move-result v5

    #@74
    .line 2512
    .local v5, width:I
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@76
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@7d
    move-result v1

    #@7e
    .line 2513
    .local v1, height:I
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempSize:[I

    #@80
    aget v6, v6, v7

    #@82
    if-ne v6, v5, :cond_8a

    #@84
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempSize:[I

    #@86
    aget v6, v6, v8

    #@88
    if-eq v6, v1, :cond_ab

    #@8a
    .line 2514
    :cond_8a
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempSize:[I

    #@8c
    aput v5, v6, v7

    #@8e
    .line 2515
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempSize:[I

    #@90
    aput v1, v6, v8

    #@92
    .line 2517
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@94
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@97
    move-result-object v6

    #@98
    invoke-virtual {v6}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@9b
    move-result-object v6

    #@9c
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@9f
    move-result-object v0

    #@a0
    .line 2518
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@a2
    if-lt v6, v5, :cond_ba

    #@a4
    iget v6, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@a6
    if-lt v6, v1, :cond_ba

    #@a8
    move v6, v8

    #@a9
    :goto_a9
    iput-boolean v6, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@ab
    .line 2522
    .end local v0           #displayMetrics:Landroid/util/DisplayMetrics;
    .end local v1           #height:I
    .end local v5           #width:I
    :cond_ab
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@ad
    aget v6, v6, v7

    #@af
    iput v6, p0, Landroid/widget/Editor$PositionListener;->mPositionX:I

    #@b1
    .line 2523
    iget-object v6, p0, Landroid/widget/Editor$PositionListener;->mTempCoords:[I

    #@b3
    aget v6, v6, v8

    #@b5
    iput v6, p0, Landroid/widget/Editor$PositionListener;->mPositionY:I

    #@b7
    .line 2524
    return-void

    #@b8
    :cond_b8
    move v6, v7

    #@b9
    .line 2508
    goto :goto_60

    #@ba
    .restart local v0       #displayMetrics:Landroid/util/DisplayMetrics;
    .restart local v1       #height:I
    .restart local v5       #width:I
    :cond_ba
    move v6, v7

    #@bb
    .line 2518
    goto :goto_a9
.end method


# virtual methods
.method public addSubscriber(Landroid/widget/Editor$TextViewPositionListener;Z)V
    .registers 8
    .parameter "positionListener"
    .parameter "canMove"

    #@0
    .prologue
    .line 2428
    iget v4, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@2
    if-nez v4, :cond_14

    #@4
    .line 2429
    invoke-direct {p0}, Landroid/widget/Editor$PositionListener;->updatePosition()V

    #@7
    .line 2430
    iget-object v4, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@9
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@10
    move-result-object v3

    #@11
    .line 2431
    .local v3, vto:Landroid/view/ViewTreeObserver;
    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@14
    .line 2434
    .end local v3           #vto:Landroid/view/ViewTreeObserver;
    :cond_14
    const/4 v0, -0x1

    #@15
    .line 2435
    .local v0, emptySlotIndex:I
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    const/4 v4, 0x6

    #@17
    if-ge v1, v4, :cond_28

    #@19
    .line 2436
    iget-object v4, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@1b
    aget-object v2, v4, v1

    #@1d
    .line 2437
    .local v2, listener:Landroid/widget/Editor$TextViewPositionListener;
    if-ne v2, p1, :cond_20

    #@1f
    .line 2447
    .end local v2           #listener:Landroid/widget/Editor$TextViewPositionListener;
    :goto_1f
    return-void

    #@20
    .line 2439
    .restart local v2       #listener:Landroid/widget/Editor$TextViewPositionListener;
    :cond_20
    if-gez v0, :cond_25

    #@22
    if-nez v2, :cond_25

    #@24
    .line 2440
    move v0, v1

    #@25
    .line 2435
    :cond_25
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_16

    #@28
    .line 2444
    .end local v2           #listener:Landroid/widget/Editor$TextViewPositionListener;
    :cond_28
    iget-object v4, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@2a
    aput-object p1, v4, v0

    #@2c
    .line 2445
    iget-object v4, p0, Landroid/widget/Editor$PositionListener;->mCanMove:[Z

    #@2e
    aput-boolean p2, v4, v0

    #@30
    .line 2446
    iget v4, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@32
    add-int/lit8 v4, v4, 0x1

    #@34
    iput v4, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@36
    goto :goto_1f
.end method

.method public getPositionX()I
    .registers 2

    #@0
    .prologue
    .line 2465
    iget v0, p0, Landroid/widget/Editor$PositionListener;->mPositionX:I

    #@2
    return v0
.end method

.method public getPositionY()I
    .registers 2

    #@0
    .prologue
    .line 2469
    iget v0, p0, Landroid/widget/Editor$PositionListener;->mPositionY:I

    #@2
    return v0
.end method

.method public onPreDraw()Z
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2474
    invoke-direct {p0}, Landroid/widget/Editor$PositionListener;->updatePosition()V

    #@4
    .line 2477
    iput-boolean v6, p0, Landroid/widget/Editor$PositionListener;->mScrollHasChanged:Z

    #@6
    .line 2480
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    const/4 v2, 0x6

    #@8
    if-ge v0, v2, :cond_2c

    #@a
    .line 2481
    iget-boolean v2, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@c
    if-nez v2, :cond_18

    #@e
    iget-boolean v2, p0, Landroid/widget/Editor$PositionListener;->mScrollHasChanged:Z

    #@10
    if-nez v2, :cond_18

    #@12
    iget-object v2, p0, Landroid/widget/Editor$PositionListener;->mCanMove:[Z

    #@14
    aget-boolean v2, v2, v0

    #@16
    if-eqz v2, :cond_29

    #@18
    .line 2482
    :cond_18
    iget-object v2, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@1a
    aget-object v1, v2, v0

    #@1c
    .line 2483
    .local v1, positionListener:Landroid/widget/Editor$TextViewPositionListener;
    if-eqz v1, :cond_29

    #@1e
    .line 2484
    iget v2, p0, Landroid/widget/Editor$PositionListener;->mPositionX:I

    #@20
    iget v3, p0, Landroid/widget/Editor$PositionListener;->mPositionY:I

    #@22
    iget-boolean v4, p0, Landroid/widget/Editor$PositionListener;->mPositionHasChanged:Z

    #@24
    iget-boolean v5, p0, Landroid/widget/Editor$PositionListener;->mScrollHasChanged:Z

    #@26
    invoke-interface {v1, v2, v3, v4, v5}, Landroid/widget/Editor$TextViewPositionListener;->updatePosition(IIZZ)V

    #@29
    .line 2480
    .end local v1           #positionListener:Landroid/widget/Editor$TextViewPositionListener;
    :cond_29
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_7

    #@2c
    .line 2490
    :cond_2c
    const/4 v2, 0x0

    #@2d
    iput-boolean v2, p0, Landroid/widget/Editor$PositionListener;->mScrollHasChanged:Z

    #@2f
    .line 2492
    return v6
.end method

.method public onScrollChanged()V
    .registers 2

    #@0
    .prologue
    .line 2527
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/Editor$PositionListener;->mScrollHasChanged:Z

    #@3
    .line 2528
    return-void
.end method

.method public removeSubscriber(Landroid/widget/Editor$TextViewPositionListener;)V
    .registers 6
    .parameter "positionListener"

    #@0
    .prologue
    .line 2450
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/4 v2, 0x6

    #@2
    if-ge v0, v2, :cond_15

    #@4
    .line 2451
    iget-object v2, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@6
    aget-object v2, v2, v0

    #@8
    if-ne v2, p1, :cond_27

    #@a
    .line 2452
    iget-object v2, p0, Landroid/widget/Editor$PositionListener;->mPositionListeners:[Landroid/widget/Editor$TextViewPositionListener;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object v3, v2, v0

    #@f
    .line 2453
    iget v2, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@11
    add-int/lit8 v2, v2, -0x1

    #@13
    iput v2, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@15
    .line 2458
    :cond_15
    iget v2, p0, Landroid/widget/Editor$PositionListener;->mNumberOfListeners:I

    #@17
    if-nez v2, :cond_26

    #@19
    .line 2459
    iget-object v2, p0, Landroid/widget/Editor$PositionListener;->this$0:Landroid/widget/Editor;

    #@1b
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@22
    move-result-object v1

    #@23
    .line 2460
    .local v1, vto:Landroid/view/ViewTreeObserver;
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@26
    .line 2462
    .end local v1           #vto:Landroid/view/ViewTreeObserver;
    :cond_26
    return-void

    #@27
    .line 2450
    :cond_27
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_1
.end method
