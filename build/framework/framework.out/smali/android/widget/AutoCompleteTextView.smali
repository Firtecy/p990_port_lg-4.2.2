.class public Landroid/widget/AutoCompleteTextView;
.super Landroid/widget/EditText;
.source "AutoCompleteTextView.java"

# interfaces
.implements Landroid/widget/Filter$FilterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;,
        Landroid/widget/AutoCompleteTextView$PassThroughClickListener;,
        Landroid/widget/AutoCompleteTextView$OnDismissListener;,
        Landroid/widget/AutoCompleteTextView$Validator;,
        Landroid/widget/AutoCompleteTextView$DropDownItemClickListener;,
        Landroid/widget/AutoCompleteTextView$MyWatcher;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field static final EXPAND_MAX:I = 0x3

.field static final TAG:Ljava/lang/String; = "AutoCompleteTextView"


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mBlockCompletion:Z

.field private mDropDownAnchorId:I

.field private mDropDownDismissedOnCompletion:Z

.field private mFilter:Landroid/widget/Filter;

.field private mHintResource:I

.field private mHintText:Ljava/lang/CharSequence;

.field private mHintView:Landroid/widget/TextView;

.field private mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mLastKeyCode:I

.field private mObserver:Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

.field private mOpenBefore:Z

.field private mPassThroughClickListener:Landroid/widget/AutoCompleteTextView$PassThroughClickListener;

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private mPopupCanBeUpdated:Z

.field private mThreshold:I

.field private mValidator:Landroid/widget/AutoCompleteTextView$Validator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 129
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 133
    const v0, 0x101006b

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v8, -0x2

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v4, 0x1

    #@5
    .line 137
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8
    .line 111
    iput-boolean v4, p0, Landroid/widget/AutoCompleteTextView;->mDropDownDismissedOnCompletion:Z

    #@a
    .line 113
    iput v6, p0, Landroid/widget/AutoCompleteTextView;->mLastKeyCode:I

    #@c
    .line 116
    iput-object v5, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@e
    .line 123
    iput-boolean v4, p0, Landroid/widget/AutoCompleteTextView;->mPopupCanBeUpdated:Z

    #@10
    .line 139
    new-instance v2, Landroid/widget/ListPopupWindow;

    #@12
    const v3, 0x101006b

    #@15
    invoke-direct {v2, p1, p2, v3}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@18
    iput-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@1a
    .line 141
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@1c
    const/16 v3, 0x10

    #@1e
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setSoftInputMode(I)V

    #@21
    .line 142
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@23
    invoke-virtual {v2, v4}, Landroid/widget/ListPopupWindow;->setPromptPosition(I)V

    #@26
    .line 144
    sget-object v2, Lcom/android/internal/R$styleable;->AutoCompleteTextView:[I

    #@28
    invoke-virtual {p1, p2, v2, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@2b
    move-result-object v0

    #@2c
    .line 148
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x2

    #@2d
    const/4 v3, 0x2

    #@2e
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@31
    move-result v2

    #@32
    iput v2, p0, Landroid/widget/AutoCompleteTextView;->mThreshold:I

    #@34
    .line 151
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@36
    const/4 v3, 0x3

    #@37
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setListSelector(Landroid/graphics/drawable/Drawable;)V

    #@3e
    .line 152
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@40
    const/16 v3, 0x9

    #@42
    invoke-virtual {v0, v3, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@45
    move-result v3

    #@46
    float-to-int v3, v3

    #@47
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    #@4a
    .line 154
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@4c
    const/16 v3, 0x8

    #@4e
    invoke-virtual {v0, v3, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@51
    move-result v3

    #@52
    float-to-int v3, v3

    #@53
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    #@56
    .line 161
    const/4 v2, 0x6

    #@57
    const/4 v3, -0x1

    #@58
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5b
    move-result v2

    #@5c
    iput v2, p0, Landroid/widget/AutoCompleteTextView;->mDropDownAnchorId:I

    #@5e
    .line 166
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@60
    const/4 v3, 0x5

    #@61
    invoke-virtual {v0, v3, v8}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@64
    move-result v3

    #@65
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    #@68
    .line 169
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@6a
    const/4 v3, 0x7

    #@6b
    invoke-virtual {v0, v3, v8}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@6e
    move-result v3

    #@6f
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    #@72
    .line 173
    const v2, 0x10900cb

    #@75
    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@78
    move-result v2

    #@79
    iput v2, p0, Landroid/widget/AutoCompleteTextView;->mHintResource:I

    #@7b
    .line 176
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@7d
    new-instance v3, Landroid/widget/AutoCompleteTextView$DropDownItemClickListener;

    #@7f
    invoke-direct {v3, p0, v5}, Landroid/widget/AutoCompleteTextView$DropDownItemClickListener;-><init>(Landroid/widget/AutoCompleteTextView;Landroid/widget/AutoCompleteTextView$1;)V

    #@82
    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@85
    .line 177
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {p0, v2}, Landroid/widget/AutoCompleteTextView;->setCompletionHint(Ljava/lang/CharSequence;)V

    #@8c
    .line 181
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getInputType()I

    #@8f
    move-result v1

    #@90
    .line 182
    .local v1, inputType:I
    and-int/lit8 v2, v1, 0xf

    #@92
    if-ne v2, v4, :cond_9a

    #@94
    .line 184
    const/high16 v2, 0x1

    #@96
    or-int/2addr v1, v2

    #@97
    .line 185
    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setRawInputType(I)V

    #@9a
    .line 188
    :cond_9a
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@9d
    .line 190
    invoke-virtual {p0, v4}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    #@a0
    .line 192
    new-instance v2, Landroid/widget/AutoCompleteTextView$MyWatcher;

    #@a2
    invoke-direct {v2, p0, v5}, Landroid/widget/AutoCompleteTextView$MyWatcher;-><init>(Landroid/widget/AutoCompleteTextView;Landroid/widget/AutoCompleteTextView$1;)V

    #@a5
    invoke-virtual {p0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@a8
    .line 194
    new-instance v2, Landroid/widget/AutoCompleteTextView$PassThroughClickListener;

    #@aa
    invoke-direct {v2, p0, v5}, Landroid/widget/AutoCompleteTextView$PassThroughClickListener;-><init>(Landroid/widget/AutoCompleteTextView;Landroid/widget/AutoCompleteTextView$1;)V

    #@ad
    iput-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPassThroughClickListener:Landroid/widget/AutoCompleteTextView$PassThroughClickListener;

    #@af
    .line 195
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPassThroughClickListener:Landroid/widget/AutoCompleteTextView$PassThroughClickListener;

    #@b1
    invoke-super {p0, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@b4
    .line 196
    return-void
.end method

.method static synthetic access$500(Landroid/widget/AutoCompleteTextView;Landroid/view/View;IJ)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/AutoCompleteTextView;->performCompletion(Landroid/view/View;IJ)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/AutoCompleteTextView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 91
    invoke-direct {p0}, Landroid/widget/AutoCompleteTextView;->onClickImpl()V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/widget/AutoCompleteTextView;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/AutoCompleteTextView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 91
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;->updateDropDownForFilter(I)V

    #@3
    return-void
.end method

.method private buildImeCompletions()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 1116
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    .line 1117
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-eqz v0, :cond_44

    #@5
    .line 1118
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@8
    move-result-object v6

    #@9
    .line 1119
    .local v6, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v6, :cond_44

    #@b
    .line 1120
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@e
    move-result v10

    #@f
    const/16 v11, 0x14

    #@11
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@14
    move-result v2

    #@15
    .line 1121
    .local v2, count:I
    new-array v1, v2, [Landroid/view/inputmethod/CompletionInfo;

    #@17
    .line 1122
    .local v1, completions:[Landroid/view/inputmethod/CompletionInfo;
    const/4 v8, 0x0

    #@18
    .line 1124
    .local v8, realCount:I
    const/4 v3, 0x0

    #@19
    .local v3, i:I
    :goto_19
    if-ge v3, v2, :cond_39

    #@1b
    .line 1125
    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@1e
    move-result v10

    #@1f
    if-eqz v10, :cond_36

    #@21
    .line 1126
    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@24
    move-result-object v7

    #@25
    .line 1127
    .local v7, item:Ljava/lang/Object;
    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@28
    move-result-wide v4

    #@29
    .line 1128
    .local v4, id:J
    new-instance v10, Landroid/view/inputmethod/CompletionInfo;

    #@2b
    invoke-virtual {p0, v7}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    #@2e
    move-result-object v11

    #@2f
    invoke-direct {v10, v4, v5, v8, v11}, Landroid/view/inputmethod/CompletionInfo;-><init>(JILjava/lang/CharSequence;)V

    #@32
    aput-object v10, v1, v8

    #@34
    .line 1130
    add-int/lit8 v8, v8, 0x1

    #@36
    .line 1124
    .end local v4           #id:J
    .end local v7           #item:Ljava/lang/Object;
    :cond_36
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_19

    #@39
    .line 1134
    :cond_39
    if-eq v8, v2, :cond_41

    #@3b
    .line 1135
    new-array v9, v8, [Landroid/view/inputmethod/CompletionInfo;

    #@3d
    .line 1136
    .local v9, tmp:[Landroid/view/inputmethod/CompletionInfo;
    invoke-static {v1, v12, v9, v12, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@40
    .line 1137
    move-object v1, v9

    #@41
    .line 1140
    .end local v9           #tmp:[Landroid/view/inputmethod/CompletionInfo;
    :cond_41
    invoke-virtual {v6, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->displayCompletions(Landroid/view/View;[Landroid/view/inputmethod/CompletionInfo;)V

    #@44
    .line 1143
    .end local v1           #completions:[Landroid/view/inputmethod/CompletionInfo;
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v6           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v8           #realCount:I
    :cond_44
    return-void
.end method

.method private onClickImpl()V
    .registers 2

    #@0
    .prologue
    .line 209
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 210
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->ensureImeVisible(Z)V

    #@a
    .line 212
    :cond_a
    return-void
.end method

.method private performCompletion(Landroid/view/View;IJ)V
    .registers 14
    .parameter "selectedView"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 878
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_5b

    #@6
    .line 880
    if-gez p2, :cond_19

    #@8
    .line 881
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@a
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getSelectedItem()Ljava/lang/Object;

    #@d
    move-result-object v8

    #@e
    .line 885
    .local v8, selectedItem:Ljava/lang/Object;
    :goto_e
    if-nez v8, :cond_20

    #@10
    .line 886
    const-string v0, "AutoCompleteTextView"

    #@12
    const-string/jumbo v1, "performCompletion: no selected item"

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 913
    .end local v8           #selectedItem:Ljava/lang/Object;
    :cond_18
    :goto_18
    return-void

    #@19
    .line 883
    :cond_19
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@1b
    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@1e
    move-result-object v8

    #@1f
    .restart local v8       #selectedItem:Ljava/lang/Object;
    goto :goto_e

    #@20
    .line 890
    :cond_20
    const/4 v0, 0x1

    #@21
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@23
    .line 891
    invoke-virtual {p0, v8}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    #@2a
    .line 892
    const/4 v0, 0x0

    #@2b
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@2d
    .line 894
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2f
    if-eqz v0, :cond_5b

    #@31
    .line 895
    iget-object v7, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@33
    .line 898
    .local v7, list:Landroid/widget/ListPopupWindow;
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@36
    move-result-object v6

    #@37
    .line 900
    .local v6, imm:Landroid/view/inputmethod/InputMethodManager;
    if-nez p1, :cond_41

    #@39
    if-eqz v6, :cond_43

    #@3b
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_43

    #@41
    :cond_41
    if-gez p2, :cond_4f

    #@43
    .line 901
    :cond_43
    invoke-virtual {v7}, Landroid/widget/ListPopupWindow;->getSelectedView()Landroid/view/View;

    #@46
    move-result-object p1

    #@47
    .line 902
    invoke-virtual {v7}, Landroid/widget/ListPopupWindow;->getSelectedItemPosition()I

    #@4a
    move-result p2

    #@4b
    .line 903
    invoke-virtual {v7}, Landroid/widget/ListPopupWindow;->getSelectedItemId()J

    #@4e
    move-result-wide p3

    #@4f
    .line 906
    :cond_4f
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@51
    invoke-virtual {v7}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    #@54
    move-result-object v1

    #@55
    move-object v2, p1

    #@56
    move v3, p2

    #@57
    move-wide v4, p3

    #@58
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    #@5b
    .line 910
    .end local v6           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v7           #list:Landroid/widget/ListPopupWindow;
    .end local v8           #selectedItem:Ljava/lang/Object;
    :cond_5b
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mDropDownDismissedOnCompletion:Z

    #@5d
    if-eqz v0, :cond_18

    #@5f
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@61
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@64
    move-result v0

    #@65
    if-nez v0, :cond_18

    #@67
    .line 911
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@6a
    goto :goto_18
.end method

.method private updateDropDownForFilter(I)V
    .registers 6
    .parameter "count"

    #@0
    .prologue
    .line 962
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getWindowVisibility()I

    #@3
    move-result v2

    #@4
    const/16 v3, 0x8

    #@6
    if-ne v2, v3, :cond_9

    #@8
    .line 984
    :cond_8
    :goto_8
    return-void

    #@9
    .line 971
    :cond_9
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@b
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@e
    move-result v0

    #@f
    .line 972
    .local v0, dropDownAlwaysVisible:Z
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    #@12
    move-result v1

    #@13
    .line 973
    .local v1, enoughToFilter:Z
    if-gtz p1, :cond_17

    #@15
    if-eqz v0, :cond_2d

    #@17
    :cond_17
    if-eqz v1, :cond_2d

    #@19
    .line 974
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->hasFocus()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_8

    #@1f
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->hasWindowFocus()Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_8

    #@25
    iget-boolean v2, p0, Landroid/widget/AutoCompleteTextView;->mPopupCanBeUpdated:Z

    #@27
    if-eqz v2, :cond_8

    #@29
    .line 975
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    #@2c
    goto :goto_8

    #@2d
    .line 977
    :cond_2d
    if-nez v0, :cond_8

    #@2f
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_8

    #@35
    .line 978
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@38
    .line 982
    const/4 v2, 0x1

    #@39
    iput-boolean v2, p0, Landroid/widget/AutoCompleteTextView;->mPopupCanBeUpdated:Z

    #@3b
    goto :goto_8
.end method


# virtual methods
.method public clearListSelection()V
    .registers 2

    #@0
    .prologue
    .line 820
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->clearListSelection()V

    #@5
    .line 821
    return-void
.end method

.method protected convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "selectedItem"

    #@0
    .prologue
    .line 812
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public dismissDropDown()V
    .registers 3

    #@0
    .prologue
    .line 1033
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 1034
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_a

    #@6
    .line 1035
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->displayCompletions(Landroid/view/View;[Landroid/view/inputmethod/CompletionInfo;)V

    #@a
    .line 1037
    :cond_a
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@c
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@f
    .line 1038
    const/4 v1, 0x0

    #@10
    iput-boolean v1, p0, Landroid/widget/AutoCompleteTextView;->mPopupCanBeUpdated:Z

    #@12
    .line 1039
    return-void
.end method

.method doAfterTextChanged()V
    .registers 3

    #@0
    .prologue
    .line 764
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 792
    :cond_4
    :goto_4
    return-void

    #@5
    .line 771
    :cond_5
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mOpenBefore:Z

    #@7
    if-eqz v0, :cond_f

    #@9
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_4

    #@f
    .line 777
    :cond_f
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_26

    #@15
    .line 778
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@17
    if-eqz v0, :cond_4

    #@19
    .line 779
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mPopupCanBeUpdated:Z

    #@1c
    .line 780
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@1f
    move-result-object v0

    #@20
    iget v1, p0, Landroid/widget/AutoCompleteTextView;->mLastKeyCode:I

    #@22
    invoke-virtual {p0, v0, v1}, Landroid/widget/AutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    #@25
    goto :goto_4

    #@26
    .line 785
    :cond_26
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@28
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@2b
    move-result v0

    #@2c
    if-nez v0, :cond_31

    #@2e
    .line 786
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@31
    .line 788
    :cond_31
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@33
    if-eqz v0, :cond_4

    #@35
    .line 789
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@37
    const/4 v1, 0x0

    #@38
    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    #@3b
    goto :goto_4
.end method

.method doBeforeTextChanged()V
    .registers 2

    #@0
    .prologue
    .line 755
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 761
    :goto_4
    return-void

    #@5
    .line 759
    :cond_5
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@8
    move-result v0

    #@9
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mOpenBefore:Z

    #@b
    goto :goto_4
.end method

.method public enoughToFilter()Z
    .registers 3

    #@0
    .prologue
    .line 735
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@7
    move-result v0

    #@8
    iget v1, p0, Landroid/widget/AutoCompleteTextView;->mThreshold:I

    #@a
    if-lt v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public ensureImeVisible(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 1068
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    if-eqz p1, :cond_1e

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    #@8
    .line 1070
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@a
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_1a

    #@10
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@12
    if-eqz v0, :cond_1d

    #@14
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    .line 1071
    :cond_1a
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    #@1d
    .line 1073
    :cond_1d
    return-void

    #@1e
    .line 1068
    :cond_1e
    const/4 v0, 0x2

    #@1f
    goto :goto_5
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 604
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method public getCompletionHint()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 253
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mHintText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getDropDownAnchor()I
    .registers 2

    #@0
    .prologue
    .line 318
    iget v0, p0, Landroid/widget/AutoCompleteTextView;->mDropDownAnchorId:I

    #@2
    return v0
.end method

.method public getDropDownAnimationStyle()I
    .registers 2

    #@0
    .prologue
    .line 429
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnimationStyle()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDropDownHeight()I
    .registers 2

    #@0
    .prologue
    .line 293
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownHorizontalOffset()I
    .registers 2

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getHorizontalOffset()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownVerticalOffset()I
    .registers 2

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getVerticalOffset()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownWidth()I
    .registers 2

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected getFilter()Landroid/widget/Filter;
    .registers 2

    #@0
    .prologue
    .line 1191
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@2
    return-object v0
.end method

.method public getItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 545
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    return-object v0
.end method

.method public getItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 558
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    return-object v0
.end method

.method public getListSelection()I
    .registers 2

    #@0
    .prologue
    .line 843
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getSelectedItemPosition()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    return-object v0
.end method

.method public getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;
    .registers 2

    #@0
    .prologue
    .line 578
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    return-object v0
.end method

.method public getThreshold()I
    .registers 2

    #@0
    .prologue
    .line 490
    iget v0, p0, Landroid/widget/AutoCompleteTextView;->mThreshold:I

    #@2
    return v0
.end method

.method public getValidator()Landroid/widget/AutoCompleteTextView$Validator;
    .registers 2

    #@0
    .prologue
    .line 1165
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@2
    return-object v0
.end method

.method public isDropDownAlwaysVisible()Z
    .registers 2

    #@0
    .prologue
    .line 438
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDropDownDismissedOnCompletion()Z
    .registers 2

    #@0
    .prologue
    .line 464
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mDropDownDismissedOnCompletion:Z

    #@2
    return v0
.end method

.method public isInputMethodNotNeeded()Z
    .registers 3

    #@0
    .prologue
    .line 1079
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getInputMethodMode()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isPerformingCompletion()Z
    .registers 2

    #@0
    .prologue
    .line 920
    iget-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@2
    return v0
.end method

.method public isPopupShowing()Z
    .registers 2

    #@0
    .prologue
    .line 800
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 1020
    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    #@3
    .line 1021
    return-void
.end method

.method public onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .registers 4
    .parameter "completion"

    #@0
    .prologue
    .line 872
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 873
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@8
    invoke-virtual {p1}, Landroid/view/inputmethod/CompletionInfo;->getPosition()I

    #@b
    move-result v1

    #@c
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->performItemClick(I)Z

    #@f
    .line 875
    :cond_f
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 1025
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@3
    .line 1026
    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    #@6
    .line 1027
    return-void
.end method

.method protected onDisplayHint(I)V
    .registers 3
    .parameter "hint"

    #@0
    .prologue
    .line 996
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDisplayHint(I)V

    #@3
    .line 997
    packed-switch p1, :pswitch_data_14

    #@6
    .line 1004
    :cond_6
    :goto_6
    return-void

    #@7
    .line 999
    :pswitch_7
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@9
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_6

    #@f
    .line 1000
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@12
    goto :goto_6

    #@13
    .line 997
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x4
        :pswitch_7
    .end packed-switch
.end method

.method public onFilterComplete(I)V
    .registers 2
    .parameter "count"

    #@0
    .prologue
    .line 957
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;->updateDropDownForFilter(I)V

    #@3
    .line 958
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 1008
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 1010
    if-nez p1, :cond_8

    #@5
    .line 1011
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->performValidation()V

    #@8
    .line 1013
    :cond_8
    if-nez p1, :cond_15

    #@a
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@c
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_15

    #@12
    .line 1014
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@15
    .line 1016
    :cond_15
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 698
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@3
    invoke-virtual {v1, p1, p2}, Landroid/widget/ListPopupWindow;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 723
    :cond_9
    :goto_9
    return v0

    #@a
    .line 702
    :cond_a
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_13

    #@10
    .line 703
    packed-switch p1, :pswitch_data_42

    #@13
    .line 711
    :cond_13
    :goto_13
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_23

    #@19
    const/16 v1, 0x3d

    #@1b
    if-ne p1, v1, :cond_23

    #@1d
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_9

    #@23
    .line 715
    :cond_23
    iput p1, p0, Landroid/widget/AutoCompleteTextView;->mLastKeyCode:I

    #@25
    .line 716
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@28
    move-result v0

    #@29
    .line 717
    .local v0, handled:Z
    const/4 v1, 0x0

    #@2a
    iput v1, p0, Landroid/widget/AutoCompleteTextView;->mLastKeyCode:I

    #@2c
    .line 719
    if-eqz v0, :cond_9

    #@2e
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_9

    #@34
    .line 720
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->clearListSelection()V

    #@37
    goto :goto_9

    #@38
    .line 705
    .end local v0           #handled:Z
    :pswitch_38
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_13

    #@3e
    .line 706
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->performValidation()V

    #@41
    goto :goto_13

    #@42
    .line 703
    :pswitch_data_42
    .packed-switch 0x14
        :pswitch_38
    .end packed-switch
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 646
    const/4 v2, 0x4

    #@2
    if-ne p1, v2, :cond_47

    #@4
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_47

    #@a
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@c
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_47

    #@12
    .line 650
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_28

    #@18
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_28

    #@1e
    .line 651
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@21
    move-result-object v0

    #@22
    .line 652
    .local v0, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_27

    #@24
    .line 653
    invoke-virtual {v0, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@27
    .line 667
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_27
    :goto_27
    return v1

    #@28
    .line 656
    :cond_28
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@2b
    move-result v2

    #@2c
    if-ne v2, v1, :cond_47

    #@2e
    .line 657
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@31
    move-result-object v0

    #@32
    .line 658
    .restart local v0       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_37

    #@34
    .line 659
    invoke-virtual {v0, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@37
    .line 661
    :cond_37
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_47

    #@3d
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@40
    move-result v2

    #@41
    if-nez v2, :cond_47

    #@43
    .line 662
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@46
    goto :goto_27

    #@47
    .line 667
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_47
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@4a
    move-result v1

    #@4b
    goto :goto_27
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 672
    iget-object v2, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@3
    invoke-virtual {v2, p1, p2}, Landroid/widget/ListPopupWindow;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@6
    move-result v0

    #@7
    .line 673
    .local v0, consumed:Z
    if-eqz v0, :cond_c

    #@9
    .line 674
    sparse-switch p1, :sswitch_data_30

    #@c
    .line 688
    :cond_c
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_2a

    #@12
    const/16 v2, 0x3d

    #@14
    if-ne p1, v2, :cond_2a

    #@16
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_2a

    #@1c
    .line 689
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->performCompletion()V

    #@1f
    .line 693
    :cond_1f
    :goto_1f
    return v1

    #@20
    .line 681
    :sswitch_20
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_1f

    #@26
    .line 682
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->performCompletion()V

    #@29
    goto :goto_1f

    #@2a
    .line 693
    :cond_2a
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@2d
    move-result v1

    #@2e
    goto :goto_1f

    #@2f
    .line 674
    nop

    #@30
    :sswitch_data_30
    .sparse-switch
        0x17 -> :sswitch_20
        0x3d -> :sswitch_20
        0x42 -> :sswitch_20
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 988
    invoke-super {p0, p1}, Landroid/widget/EditText;->onWindowFocusChanged(Z)V

    #@3
    .line 989
    if-nez p1, :cond_10

    #@5
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@7
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isDropDownAlwaysVisible()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_10

    #@d
    .line 990
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    #@10
    .line 992
    :cond_10
    return-void
.end method

.method public performCompletion()V
    .registers 5

    #@0
    .prologue
    .line 867
    const/4 v0, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const-wide/16 v2, -0x1

    #@4
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/widget/AutoCompleteTextView;->performCompletion(Landroid/view/View;IJ)V

    #@7
    .line 868
    return-void
.end method

.method protected performFiltering(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "text"
    .parameter "keyCode"

    #@0
    .prologue
    .line 858
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@2
    invoke-virtual {v0, p1, p0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    #@5
    .line 859
    return-void
.end method

.method public performValidation()V
    .registers 3

    #@0
    .prologue
    .line 1176
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1183
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1178
    :cond_5
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@8
    move-result-object v0

    #@9
    .line 1180
    .local v0, text:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_4

    #@f
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@11
    invoke-interface {v1, v0}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_4

    #@17
    .line 1181
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@19
    invoke-interface {v1, v0}, Landroid/widget/AutoCompleteTextView$Validator;->fixText(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    #@20
    goto :goto_4
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 947
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->clearComposingText()V

    #@3
    .line 949
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    #@6
    .line 951
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@9
    move-result-object v0

    #@a
    .line 952
    .local v0, spannable:Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@d
    move-result v1

    #@e
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@11
    .line 953
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/widget/ListAdapter;",
            ":",
            "Landroid/widget/Filterable;",
            ">(TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, adapter:Landroid/widget/ListAdapter;,"TT;"
    const/4 v2, 0x0

    #@1
    .line 627
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mObserver:Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

    #@3
    if-nez v0, :cond_29

    #@5
    .line 628
    new-instance v0, Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

    #@7
    invoke-direct {v0, p0, v2}, Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;-><init>(Landroid/widget/AutoCompleteTextView;Landroid/widget/AutoCompleteTextView$1;)V

    #@a
    iput-object v0, p0, Landroid/widget/AutoCompleteTextView;->mObserver:Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

    #@c
    .line 632
    :cond_c
    :goto_c
    iput-object p1, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@e
    .line 633
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@10
    if-eqz v0, :cond_35

    #@12
    .line 635
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@14
    check-cast v0, Landroid/widget/Filterable;

    #@16
    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@1c
    .line 636
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mObserver:Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

    #@1e
    invoke-interface {p1, v0}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@21
    .line 641
    :goto_21
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@23
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@25
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@28
    .line 642
    return-void

    #@29
    .line 629
    :cond_29
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@2b
    if-eqz v0, :cond_c

    #@2d
    .line 630
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mAdapter:Landroid/widget/ListAdapter;

    #@2f
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mObserver:Landroid/widget/AutoCompleteTextView$PopupDataSetObserver;

    #@31
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@34
    goto :goto_c

    #@35
    .line 638
    :cond_35
    iput-object v2, p0, Landroid/widget/AutoCompleteTextView;->mFilter:Landroid/widget/Filter;

    #@37
    goto :goto_21
.end method

.method public setCompletionHint(Ljava/lang/CharSequence;)V
    .registers 6
    .parameter "hint"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 226
    iput-object p1, p0, Landroid/widget/AutoCompleteTextView;->mHintText:Ljava/lang/CharSequence;

    #@3
    .line 227
    if-eqz p1, :cond_33

    #@5
    .line 228
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mHintView:Landroid/widget/TextView;

    #@7
    if-nez v1, :cond_2d

    #@9
    .line 229
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@10
    move-result-object v1

    #@11
    iget v2, p0, Landroid/widget/AutoCompleteTextView;->mHintResource:I

    #@13
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@16
    move-result-object v1

    #@17
    const v2, 0x1020014

    #@1a
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/widget/TextView;

    #@20
    .line 231
    .local v0, hintView:Landroid/widget/TextView;
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mHintText:Ljava/lang/CharSequence;

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@25
    .line 232
    iput-object v0, p0, Landroid/widget/AutoCompleteTextView;->mHintView:Landroid/widget/TextView;

    #@27
    .line 233
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@29
    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setPromptView(Landroid/view/View;)V

    #@2c
    .line 241
    .end local v0           #hintView:Landroid/widget/TextView;
    :goto_2c
    return-void

    #@2d
    .line 235
    :cond_2d
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mHintView:Landroid/widget/TextView;

    #@2f
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@32
    goto :goto_2c

    #@33
    .line 238
    :cond_33
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@35
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow;->setPromptView(Landroid/view/View;)V

    #@38
    .line 239
    iput-object v3, p0, Landroid/widget/AutoCompleteTextView;->mHintView:Landroid/widget/TextView;

    #@3a
    goto :goto_2c
.end method

.method public setDropDownAlwaysVisible(Z)V
    .registers 3
    .parameter "dropDownAlwaysVisible"

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setDropDownAlwaysVisible(Z)V

    #@5
    .line 456
    return-void
.end method

.method public setDropDownAnchor(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 331
    iput p1, p0, Landroid/widget/AutoCompleteTextView;->mDropDownAnchorId:I

    #@2
    .line 332
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@8
    .line 333
    return-void
.end method

.method public setDropDownAnimationStyle(I)V
    .registers 3
    .parameter "animationStyle"

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAnimationStyle(I)V

    #@5
    .line 418
    return-void
.end method

.method public setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 355
    return-void
.end method

.method public setDropDownBackgroundResource(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 365
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@d
    .line 366
    return-void
.end method

.method public setDropDownDismissedOnCompletion(Z)V
    .registers 2
    .parameter "dropDownDismissedOnCompletion"

    #@0
    .prologue
    .line 476
    iput-boolean p1, p0, Landroid/widget/AutoCompleteTextView;->mDropDownDismissedOnCompletion:Z

    #@2
    .line 477
    return-void
.end method

.method public setDropDownHeight(I)V
    .registers 3
    .parameter "height"

    #@0
    .prologue
    .line 307
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    #@5
    .line 308
    return-void
.end method

.method public setDropDownHorizontalOffset(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    #@5
    .line 393
    return-void
.end method

.method public setDropDownVerticalOffset(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 374
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    #@5
    .line 375
    return-void
.end method

.method public setDropDownWidth(I)V
    .registers 3
    .parameter "width"

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    #@5
    .line 280
    return-void
.end method

.method public setForceIgnoreOutsideTouch(Z)V
    .registers 3
    .parameter "forceIgnoreOutsideTouch"

    #@0
    .prologue
    .line 1112
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setForceIgnoreOutsideTouch(Z)V

    #@5
    .line 1113
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 7
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1043
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->setFrame(IIII)Z

    #@3
    move-result v0

    #@4
    .line 1045
    .local v0, result:Z
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_d

    #@a
    .line 1046
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    #@d
    .line 1049
    :cond_d
    return v0
.end method

.method public setListSelection(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 829
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setSelection(I)V

    #@5
    .line 830
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPassThroughClickListener:Landroid/widget/AutoCompleteTextView$PassThroughClickListener;

    #@2
    invoke-static {v0, p1}, Landroid/widget/AutoCompleteTextView$PassThroughClickListener;->access$302(Landroid/widget/AutoCompleteTextView$PassThroughClickListener;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    #@5
    .line 201
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/AutoCompleteTextView$OnDismissListener;)V
    .registers 4
    .parameter "dismissListener"

    #@0
    .prologue
    .line 587
    const/4 v0, 0x0

    #@1
    .line 588
    .local v0, wrappedListener:Landroid/widget/PopupWindow$OnDismissListener;
    if-eqz p1, :cond_8

    #@3
    .line 589
    new-instance v0, Landroid/widget/AutoCompleteTextView$1;

    #@5
    .end local v0           #wrappedListener:Landroid/widget/PopupWindow$OnDismissListener;
    invoke-direct {v0, p0, p1}, Landroid/widget/AutoCompleteTextView$1;-><init>(Landroid/widget/AutoCompleteTextView;Landroid/widget/AutoCompleteTextView$OnDismissListener;)V

    #@8
    .line 595
    .restart local v0       #wrappedListener:Landroid/widget/PopupWindow$OnDismissListener;
    :cond_8
    iget-object v1, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@a
    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    #@d
    .line 596
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 522
    iput-object p1, p0, Landroid/widget/AutoCompleteTextView;->mItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@2
    .line 523
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 532
    iput-object p1, p0, Landroid/widget/AutoCompleteTextView;->mItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@2
    .line 533
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Z)V
    .registers 4
    .parameter "text"
    .parameter "filter"

    #@0
    .prologue
    .line 930
    if-eqz p2, :cond_6

    #@2
    .line 931
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    #@5
    .line 937
    :goto_5
    return-void

    #@6
    .line 933
    :cond_6
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@9
    .line 934
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    #@c
    .line 935
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/widget/AutoCompleteTextView;->mBlockCompletion:Z

    #@f
    goto :goto_5
.end method

.method public setThreshold(I)V
    .registers 2
    .parameter "threshold"

    #@0
    .prologue
    .line 508
    if-gtz p1, :cond_3

    #@2
    .line 509
    const/4 p1, 0x1

    #@3
    .line 512
    :cond_3
    iput p1, p0, Landroid/widget/AutoCompleteTextView;->mThreshold:I

    #@5
    .line 513
    return-void
.end method

.method public setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V
    .registers 2
    .parameter "validator"

    #@0
    .prologue
    .line 1154
    iput-object p1, p0, Landroid/widget/AutoCompleteTextView;->mValidator:Landroid/widget/AutoCompleteTextView$Validator;

    #@2
    .line 1155
    return-void
.end method

.method public showDropDown()V
    .registers 4

    #@0
    .prologue
    .line 1086
    invoke-direct {p0}, Landroid/widget/AutoCompleteTextView;->buildImeCompletions()V

    #@3
    .line 1088
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@5
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    if-nez v0, :cond_1f

    #@b
    .line 1089
    iget v0, p0, Landroid/widget/AutoCompleteTextView;->mDropDownAnchorId:I

    #@d
    const/4 v1, -0x1

    #@e
    if-eq v0, v1, :cond_41

    #@10
    .line 1090
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@12
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getRootView()Landroid/view/View;

    #@15
    move-result-object v1

    #@16
    iget v2, p0, Landroid/widget/AutoCompleteTextView;->mDropDownAnchorId:I

    #@18
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@1f
    .line 1095
    :cond_1f
    :goto_1f
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_31

    #@25
    .line 1097
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@27
    const/4 v1, 0x1

    #@28
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    #@2b
    .line 1098
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2d
    const/4 v1, 0x3

    #@2e
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setListItemExpandMax(I)V

    #@31
    .line 1100
    :cond_31
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@33
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    #@36
    .line 1101
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@38
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    #@3b
    move-result-object v0

    #@3c
    const/4 v1, 0x0

    #@3d
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    #@40
    .line 1102
    return-void

    #@41
    .line 1092
    :cond_41
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@43
    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@46
    goto :goto_1f
.end method

.method public showDropDownAfterLayout()V
    .registers 2

    #@0
    .prologue
    .line 1058
    iget-object v0, p0, Landroid/widget/AutoCompleteTextView;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->postShow()V

    #@5
    .line 1059
    return-void
.end method
