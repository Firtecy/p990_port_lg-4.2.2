.class public Landroid/widget/ToggleButton;
.super Landroid/widget/CompoundButton;
.source "ToggleButton.java"


# static fields
.field private static final NO_ALPHA:I = 0xff


# instance fields
.field private mDisabledAlpha:F

.field private mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

.field private mTextOff:Ljava/lang/CharSequence;

.field private mTextOn:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 68
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 64
    const v0, 0x101004b

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 53
    sget-object v1, Lcom/android/internal/R$styleable;->ToggleButton:[I

    #@6
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 56
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Landroid/widget/ToggleButton;->mTextOn:Ljava/lang/CharSequence;

    #@11
    .line 57
    const/4 v1, 0x2

    #@12
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/widget/ToggleButton;->mTextOff:Ljava/lang/CharSequence;

    #@18
    .line 58
    const/high16 v1, 0x3f00

    #@1a
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@1d
    move-result v1

    #@1e
    iput v1, p0, Landroid/widget/ToggleButton;->mDisabledAlpha:F

    #@20
    .line 59
    invoke-direct {p0}, Landroid/widget/ToggleButton;->syncTextState()V

    #@23
    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@26
    .line 61
    return-void
.end method

.method private syncTextState()V
    .registers 3

    #@0
    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/widget/ToggleButton;->isChecked()Z

    #@3
    move-result v0

    #@4
    .line 80
    .local v0, checked:Z
    if-eqz v0, :cond_10

    #@6
    iget-object v1, p0, Landroid/widget/ToggleButton;->mTextOn:Ljava/lang/CharSequence;

    #@8
    if-eqz v1, :cond_10

    #@a
    .line 81
    iget-object v1, p0, Landroid/widget/ToggleButton;->mTextOn:Ljava/lang/CharSequence;

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/ToggleButton;->setText(Ljava/lang/CharSequence;)V

    #@f
    .line 85
    :cond_f
    :goto_f
    return-void

    #@10
    .line 82
    :cond_10
    if-nez v0, :cond_f

    #@12
    iget-object v1, p0, Landroid/widget/ToggleButton;->mTextOff:Ljava/lang/CharSequence;

    #@14
    if-eqz v1, :cond_f

    #@16
    .line 83
    iget-object v1, p0, Landroid/widget/ToggleButton;->mTextOff:Ljava/lang/CharSequence;

    #@18
    invoke-virtual {p0, v1}, Landroid/widget/ToggleButton;->setText(Ljava/lang/CharSequence;)V

    #@1b
    goto :goto_f
.end method

.method private updateReferenceToIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "backgroundDrawable"

    #@0
    .prologue
    .line 138
    instance-of v1, p1, Landroid/graphics/drawable/LayerDrawable;

    #@2
    if-eqz v1, :cond_11

    #@4
    move-object v0, p1

    #@5
    .line 139
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    #@7
    .line 140
    .local v0, layerDrawable:Landroid/graphics/drawable/LayerDrawable;
    const v1, 0x1020017

    #@a
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/widget/ToggleButton;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    #@10
    .line 145
    .end local v0           #layerDrawable:Landroid/graphics/drawable/LayerDrawable;
    :goto_10
    return-void

    #@11
    .line 143
    :cond_11
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Landroid/widget/ToggleButton;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    #@14
    goto :goto_10
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 149
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    #@3
    .line 151
    iget-object v0, p0, Landroid/widget/ToggleButton;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_14

    #@7
    .line 152
    iget-object v1, p0, Landroid/widget/ToggleButton;->mIndicatorDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {p0}, Landroid/widget/ToggleButton;->isEnabled()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_15

    #@f
    const/16 v0, 0xff

    #@11
    :goto_11
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@14
    .line 154
    :cond_14
    return-void

    #@15
    .line 152
    :cond_15
    const/high16 v0, 0x437f

    #@17
    iget v2, p0, Landroid/widget/ToggleButton;->mDisabledAlpha:F

    #@19
    mul-float/2addr v0, v2

    #@1a
    float-to-int v0, v0

    #@1b
    goto :goto_11
.end method

.method public getTextOff()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/widget/ToggleButton;->mTextOff:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTextOn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/ToggleButton;->mTextOn:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method protected onFinishInflate()V
    .registers 2

    #@0
    .prologue
    .line 125
    invoke-super {p0}, Landroid/widget/CompoundButton;->onFinishInflate()V

    #@3
    .line 127
    invoke-virtual {p0}, Landroid/widget/ToggleButton;->getBackground()Landroid/graphics/drawable/Drawable;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/ToggleButton;->updateReferenceToIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V

    #@a
    .line 128
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 159
    const-class v0, Landroid/widget/ToggleButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 160
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 165
    const-class v0, Landroid/widget/ToggleButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 166
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "d"

    #@0
    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3
    .line 134
    invoke-direct {p0, p1}, Landroid/widget/ToggleButton;->updateReferenceToIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V

    #@6
    .line 135
    return-void
.end method

.method public setChecked(Z)V
    .registers 2
    .parameter "checked"

    #@0
    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@3
    .line 75
    invoke-direct {p0}, Landroid/widget/ToggleButton;->syncTextState()V

    #@6
    .line 76
    return-void
.end method

.method public setTextOff(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "textOff"

    #@0
    .prologue
    .line 120
    iput-object p1, p0, Landroid/widget/ToggleButton;->mTextOff:Ljava/lang/CharSequence;

    #@2
    .line 121
    return-void
.end method

.method public setTextOn(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "textOn"

    #@0
    .prologue
    .line 102
    iput-object p1, p0, Landroid/widget/ToggleButton;->mTextOn:Ljava/lang/CharSequence;

    #@2
    .line 103
    return-void
.end method
