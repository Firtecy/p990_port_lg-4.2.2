.class Landroid/widget/TextView$3;
.super Ljava/lang/Object;
.source "TextView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/TextView;->updateTextServicesLocaleAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8192
    iput-object p1, p0, Landroid/widget/TextView$3;->this$0:Landroid/widget/TextView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 8195
    iget-object v0, p0, Landroid/widget/TextView$3;->this$0:Landroid/widget/TextView;

    #@2
    invoke-static {v0}, Landroid/widget/TextView;->access$300(Landroid/widget/TextView;)Ljava/util/concurrent/locks/ReentrantLock;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_1a

    #@c
    .line 8197
    :try_start_c
    iget-object v0, p0, Landroid/widget/TextView$3;->this$0:Landroid/widget/TextView;

    #@e
    invoke-static {v0}, Landroid/widget/TextView;->access$400(Landroid/widget/TextView;)V
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_1b

    #@11
    .line 8199
    iget-object v0, p0, Landroid/widget/TextView$3;->this$0:Landroid/widget/TextView;

    #@13
    invoke-static {v0}, Landroid/widget/TextView;->access$300(Landroid/widget/TextView;)Ljava/util/concurrent/locks/ReentrantLock;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@1a
    .line 8202
    :cond_1a
    return-void

    #@1b
    .line 8199
    :catchall_1b
    move-exception v0

    #@1c
    iget-object v1, p0, Landroid/widget/TextView$3;->this$0:Landroid/widget/TextView;

    #@1e
    invoke-static {v1}, Landroid/widget/TextView;->access$300(Landroid/widget/TextView;)Ljava/util/concurrent/locks/ReentrantLock;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@25
    throw v0
.end method
