.class Landroid/widget/Editor$SelectionStartHandleView;
.super Landroid/widget/Editor$HandleView;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionStartHandleView"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter
    .parameter "drawableLtr"
    .parameter "drawableRtl"

    #@0
    .prologue
    .line 4374
    iput-object p1, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@2
    .line 4375
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Editor$HandleView;-><init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 4376
    return-void
.end method


# virtual methods
.method public getActionPopupWindow()Landroid/widget/Editor$ActionPopupWindow;
    .registers 2

    #@0
    .prologue
    .line 4446
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@2
    return-object v0
.end method

.method public getCurrentCursorOffset()I
    .registers 2

    #@0
    .prologue
    .line 4397
    iget-object v0, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected getHotspotX(Landroid/graphics/drawable/Drawable;Z)I
    .registers 4
    .parameter "drawable"
    .parameter "isRtlRun"

    #@0
    .prologue
    .line 4380
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    iget-boolean v0, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 4381
    if-eqz p2, :cond_13

    #@a
    .line 4382
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@d
    move-result v0

    #@e
    mul-int/lit8 v0, v0, 0x3

    #@10
    div-int/lit8 v0, v0, 0x4

    #@12
    .line 4391
    :goto_12
    return v0

    #@13
    .line 4384
    :cond_13
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@16
    move-result v0

    #@17
    div-int/lit8 v0, v0, 0x4

    #@19
    goto :goto_12

    #@1a
    .line 4388
    :cond_1a
    if-eqz p2, :cond_23

    #@1c
    .line 4389
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1f
    move-result v0

    #@20
    div-int/lit8 v0, v0, 0x4

    #@22
    goto :goto_12

    #@23
    .line 4391
    :cond_23
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@26
    move-result v0

    #@27
    mul-int/lit8 v0, v0, 0x3

    #@29
    div-int/lit8 v0, v0, 0x4

    #@2b
    goto :goto_12
.end method

.method public updatePosition(FF)V
    .registers 10
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 4409
    iget-object v4, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4, p1, p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@b
    move-result v1

    #@c
    .line 4412
    .local v1, offset:I
    iget-object v4, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@e
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionEnd()I

    #@15
    move-result v2

    #@16
    .line 4413
    .local v2, selectionEnd:I
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@18
    if-nez v4, :cond_26

    #@1a
    .line 4414
    if-lt v1, v2, :cond_22

    #@1c
    add-int/lit8 v4, v2, -0x1

    #@1e
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    #@21
    move-result v1

    #@22
    .line 4442
    :cond_22
    :goto_22
    invoke-virtual {p0, v1, v5}, Landroid/widget/Editor$SelectionStartHandleView;->positionAtCursorOffset(IZ)V

    #@25
    .line 4443
    :cond_25
    return-void

    #@26
    .line 4416
    :cond_26
    iget-object v4, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@28
    invoke-virtual {v4}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@2b
    move-result-object v0

    #@2c
    .line 4417
    .local v0, controller:Landroid/widget/Editor$SelectionModifierCursorController;
    if-eqz v0, :cond_25

    #@2e
    .line 4419
    iget-object v4, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@30
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionStart()I

    #@37
    move-result v3

    #@38
    .line 4420
    .local v3, selectionStart:I
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@3a
    if-ne v3, v4, :cond_3e

    #@3c
    if-eq v2, v1, :cond_44

    #@3e
    :cond_3e
    if-ne v3, v2, :cond_74

    #@40
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@42
    if-ne v4, v1, :cond_74

    #@44
    .line 4422
    :cond_44
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@46
    if-eqz v4, :cond_65

    #@48
    .line 4423
    add-int/lit8 v1, v2, 0x1

    #@4a
    .line 4424
    iget-object v4, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@4c
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@53
    move-result-object v4

    #@54
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@57
    move-result v4

    #@58
    if-lt v1, v4, :cond_5d

    #@5a
    add-int/lit8 v1, v2, -0x1

    #@5c
    goto :goto_22

    #@5d
    .line 4425
    :cond_5d
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@5f
    if-nez v4, :cond_22

    #@61
    invoke-virtual {v0, v6}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@64
    goto :goto_22

    #@65
    .line 4427
    :cond_65
    add-int/lit8 v1, v2, -0x1

    #@67
    .line 4428
    if-gtz v1, :cond_6c

    #@69
    add-int/lit8 v1, v2, 0x1

    #@6b
    goto :goto_22

    #@6c
    .line 4429
    :cond_6c
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@6e
    if-eqz v4, :cond_22

    #@70
    invoke-virtual {v0, v5}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@73
    goto :goto_22

    #@74
    .line 4431
    :cond_74
    if-le v3, v2, :cond_82

    #@76
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@78
    if-gt v4, v1, :cond_82

    #@7a
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@7c
    if-nez v4, :cond_82

    #@7e
    .line 4432
    invoke-virtual {v0, v6}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@81
    goto :goto_22

    #@82
    .line 4433
    :cond_82
    if-ge v3, v2, :cond_90

    #@84
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@86
    if-lt v4, v1, :cond_90

    #@88
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@8a
    if-eqz v4, :cond_90

    #@8c
    .line 4434
    invoke-virtual {v0, v5}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@8f
    goto :goto_22

    #@90
    .line 4435
    :cond_90
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@92
    if-ne v3, v4, :cond_a4

    #@94
    if-le v2, v1, :cond_a4

    #@96
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@98
    if-eqz v4, :cond_a4

    #@9a
    if-le v3, v2, :cond_a4

    #@9c
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@9e
    if-le v4, v1, :cond_a4

    #@a0
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@a2
    if-nez v4, :cond_b8

    #@a4
    :cond_a4
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@a6
    if-ne v3, v4, :cond_22

    #@a8
    if-ge v2, v1, :cond_22

    #@aa
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@ac
    if-nez v4, :cond_22

    #@ae
    if-ge v3, v2, :cond_22

    #@b0
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@b2
    if-ge v4, v1, :cond_22

    #@b4
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@b6
    if-nez v4, :cond_22

    #@b8
    .line 4439
    :cond_b8
    iget v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@ba
    goto/16 :goto_22
.end method

.method public updateSelection(I)V
    .registers 4
    .parameter "offset"

    #@0
    .prologue
    .line 4402
    iget-object v0, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/text/Spannable;

    #@c
    iget-object v1, p0, Landroid/widget/Editor$SelectionStartHandleView;->this$0:Landroid/widget/Editor;

    #@e
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@15
    move-result v1

    #@16
    invoke-static {v0, p1, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@19
    .line 4404
    invoke-virtual {p0}, Landroid/widget/Editor$SelectionStartHandleView;->updateDrawable()V

    #@1c
    .line 4405
    return-void
.end method
