.class public Landroid/widget/ActivityChooserView;
.super Landroid/view/ViewGroup;
.source "ActivityChooserView.java"

# interfaces
.implements Landroid/widget/ActivityChooserModel$ActivityChooserModelClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;,
        Landroid/widget/ActivityChooserView$Callbacks;
    }
.end annotation


# instance fields
.field private final mActivityChooserContent:Landroid/widget/LinearLayout;

.field private final mActivityChooserContentBackground:Landroid/graphics/drawable/Drawable;

.field private final mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

.field private final mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

.field private mDefaultActionButtonContentDescription:I

.field private final mDefaultActivityButton:Landroid/widget/FrameLayout;

.field private final mDefaultActivityButtonImage:Landroid/widget/ImageView;

.field private final mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

.field private final mExpandActivityOverflowButtonImage:Landroid/widget/ImageView;

.field private mInitialActivityCount:I

.field private mIsAttachedToWindow:Z

.field private mIsSelectingDefaultActivity:Z

.field private final mListPopupMaxWidth:I

.field private mListPopupWindow:Landroid/widget/ListPopupWindow;

.field private final mModelDataSetOberver:Landroid/database/DataSetObserver;

.field private mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private final mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field mProvider:Landroid/view/ActionProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 182
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ActivityChooserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 192
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ActivityChooserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const v8, 0x1020261

    #@4
    const/4 v7, 0x4

    #@5
    const/4 v6, 0x1

    #@6
    const/4 v5, 0x0

    #@7
    .line 203
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@a
    .line 116
    new-instance v4, Landroid/widget/ActivityChooserView$1;

    #@c
    invoke-direct {v4, p0}, Landroid/widget/ActivityChooserView$1;-><init>(Landroid/widget/ActivityChooserView;)V

    #@f
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mModelDataSetOberver:Landroid/database/DataSetObserver;

    #@11
    .line 130
    new-instance v4, Landroid/widget/ActivityChooserView$2;

    #@13
    invoke-direct {v4, p0}, Landroid/widget/ActivityChooserView$2;-><init>(Landroid/widget/ActivityChooserView;)V

    #@16
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@18
    .line 164
    iput v7, p0, Landroid/widget/ActivityChooserView;->mInitialActivityCount:I

    #@1a
    .line 205
    sget-object v4, Lcom/android/internal/R$styleable;->ActivityChooserView:[I

    #@1c
    invoke-virtual {p1, p2, v4, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1f
    move-result-object v0

    #@20
    .line 208
    .local v0, attributesArray:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@23
    move-result v4

    #@24
    iput v4, p0, Landroid/widget/ActivityChooserView;->mInitialActivityCount:I

    #@26
    .line 212
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@29
    move-result-object v1

    #@2a
    .line 215
    .local v1, expandActivityOverflowButtonDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2d
    .line 217
    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2f
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@32
    move-result-object v2

    #@33
    .line 218
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v4, 0x109001f

    #@36
    invoke-virtual {v2, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@39
    .line 220
    new-instance v4, Landroid/widget/ActivityChooserView$Callbacks;

    #@3b
    invoke-direct {v4, p0, v9}, Landroid/widget/ActivityChooserView$Callbacks;-><init>(Landroid/widget/ActivityChooserView;Landroid/widget/ActivityChooserView$1;)V

    #@3e
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@40
    .line 222
    const v4, 0x102025f

    #@43
    invoke-virtual {p0, v4}, Landroid/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    #@46
    move-result-object v4

    #@47
    check-cast v4, Landroid/widget/LinearLayout;

    #@49
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@4b
    .line 223
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@4d
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    #@50
    move-result-object v4

    #@51
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContentBackground:Landroid/graphics/drawable/Drawable;

    #@53
    .line 225
    const v4, 0x1020262

    #@56
    invoke-virtual {p0, v4}, Landroid/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    #@59
    move-result-object v4

    #@5a
    check-cast v4, Landroid/widget/FrameLayout;

    #@5c
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@5e
    .line 226
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@60
    iget-object v5, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@62
    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@65
    .line 227
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@67
    iget-object v5, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@69
    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@6c
    .line 228
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@6e
    invoke-virtual {v4, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    #@71
    move-result-object v4

    #@72
    check-cast v4, Landroid/widget/ImageView;

    #@74
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButtonImage:Landroid/widget/ImageView;

    #@76
    .line 230
    const v4, 0x1020260

    #@79
    invoke-virtual {p0, v4}, Landroid/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    #@7c
    move-result-object v4

    #@7d
    check-cast v4, Landroid/widget/FrameLayout;

    #@7f
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@81
    .line 231
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@83
    iget-object v5, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@85
    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@88
    .line 232
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@8a
    invoke-virtual {v4, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    #@8d
    move-result-object v4

    #@8e
    check-cast v4, Landroid/widget/ImageView;

    #@90
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButtonImage:Landroid/widget/ImageView;

    #@92
    .line 234
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButtonImage:Landroid/widget/ImageView;

    #@94
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@97
    .line 236
    new-instance v4, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@99
    invoke-direct {v4, p0, v9}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;-><init>(Landroid/widget/ActivityChooserView;Landroid/widget/ActivityChooserView$1;)V

    #@9c
    iput-object v4, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@9e
    .line 237
    iget-object v4, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@a0
    new-instance v5, Landroid/widget/ActivityChooserView$3;

    #@a2
    invoke-direct {v5, p0}, Landroid/widget/ActivityChooserView$3;-><init>(Landroid/widget/ActivityChooserView;)V

    #@a5
    invoke-virtual {v4, v5}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@a8
    .line 245
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@ab
    move-result-object v3

    #@ac
    .line 246
    .local v3, resources:Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@af
    move-result-object v4

    #@b0
    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    #@b2
    div-int/lit8 v4, v4, 0x2

    #@b4
    const v5, 0x1050007

    #@b7
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@ba
    move-result v5

    #@bb
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@be
    move-result v4

    #@bf
    iput v4, p0, Landroid/widget/ActivityChooserView;->mListPopupMaxWidth:I

    #@c1
    .line 248
    return-void
.end method

.method static synthetic access$000(Landroid/widget/ActivityChooserView;)Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/widget/ActivityChooserView;)Landroid/widget/ListPopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/widget/ActivityChooserView;->getListPopupWindow()Landroid/widget/ListPopupWindow;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/widget/ActivityChooserView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/widget/ActivityChooserView;->mInitialActivityCount:I

    #@2
    return v0
.end method

.method static synthetic access$1200(Landroid/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/widget/ActivityChooserView;)Landroid/database/DataSetObserver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mModelDataSetOberver:Landroid/database/DataSetObserver;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Landroid/widget/ActivityChooserView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/widget/ActivityChooserView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/ActivityChooserView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/widget/ActivityChooserView;->updateAppearance()V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/widget/ActivityChooserView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/widget/ActivityChooserView;->showPopupUnchecked(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/ActivityChooserView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView;->mIsSelectingDefaultActivity:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Landroid/widget/ActivityChooserView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    iput-boolean p1, p0, Landroid/widget/ActivityChooserView;->mIsSelectingDefaultActivity:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Landroid/widget/ActivityChooserView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/ActivityChooserView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private getListPopupWindow()Landroid/widget/ListPopupWindow;
    .registers 3

    #@0
    .prologue
    .line 476
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@2
    if-nez v0, :cond_2f

    #@4
    .line 477
    new-instance v0, Landroid/widget/ListPopupWindow;

    #@6
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v1

    #@a
    invoke-direct {v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    #@d
    iput-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@f
    .line 478
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@11
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@13
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@16
    .line 479
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@18
    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@1b
    .line 480
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@1d
    const/4 v1, 0x1

    #@1e
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    #@21
    .line 481
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@23
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@25
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@28
    .line 482
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@2a
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mCallbacks:Landroid/widget/ActivityChooserView$Callbacks;

    #@2c
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    #@2f
    .line 484
    :cond_2f
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mListPopupWindow:Landroid/widget/ListPopupWindow;

    #@31
    return-object v0
.end method

.method private showPopupUnchecked(I)V
    .registers 11
    .parameter "maxActivityCount"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 319
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@4
    invoke-virtual {v7}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDataModel()Landroid/widget/ActivityChooserModel;

    #@7
    move-result-object v7

    #@8
    if-nez v7, :cond_12

    #@a
    .line 320
    new-instance v5, Ljava/lang/IllegalStateException;

    #@c
    const-string v6, "No data model. Did you call #setDataModel?"

    #@e
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v5

    #@12
    .line 323
    :cond_12
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@15
    move-result-object v7

    #@16
    iget-object v8, p0, Landroid/widget/ActivityChooserView;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@18
    invoke-virtual {v7, v8}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@1b
    .line 325
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@1d
    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getVisibility()I

    #@20
    move-result v7

    #@21
    if-nez v7, :cond_83

    #@23
    move v2, v5

    #@24
    .line 328
    .local v2, defaultActivityButtonShown:Z
    :goto_24
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@26
    invoke-virtual {v7}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getActivityCount()I

    #@29
    move-result v0

    #@2a
    .line 329
    .local v0, activityCount:I
    if-eqz v2, :cond_85

    #@2c
    move v3, v5

    #@2d
    .line 330
    .local v3, maxActivityCountOffset:I
    :goto_2d
    const v7, 0x7fffffff

    #@30
    if-eq p1, v7, :cond_87

    #@32
    add-int v7, p1, v3

    #@34
    if-le v0, v7, :cond_87

    #@36
    .line 332
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@38
    invoke-virtual {v7, v5}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setShowFooterView(Z)V

    #@3b
    .line 333
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@3d
    add-int/lit8 v8, p1, -0x1

    #@3f
    invoke-virtual {v7, v8}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setMaxActivityCount(I)V

    #@42
    .line 339
    :goto_42
    invoke-direct {p0}, Landroid/widget/ActivityChooserView;->getListPopupWindow()Landroid/widget/ListPopupWindow;

    #@45
    move-result-object v4

    #@46
    .line 340
    .local v4, popupWindow:Landroid/widget/ListPopupWindow;
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@49
    move-result v7

    #@4a
    if-nez v7, :cond_82

    #@4c
    .line 341
    iget-boolean v7, p0, Landroid/widget/ActivityChooserView;->mIsSelectingDefaultActivity:Z

    #@4e
    if-nez v7, :cond_52

    #@50
    if-nez v2, :cond_92

    #@52
    .line 342
    :cond_52
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@54
    invoke-virtual {v6, v5, v2}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setShowDefaultActivity(ZZ)V

    #@57
    .line 346
    :goto_57
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@59
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->measureContentWidth()I

    #@5c
    move-result v6

    #@5d
    iget v7, p0, Landroid/widget/ActivityChooserView;->mListPopupMaxWidth:I

    #@5f
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    #@62
    move-result v1

    #@63
    .line 347
    .local v1, contentWidth:I
    invoke-virtual {v4, v1}, Landroid/widget/ListPopupWindow;->setContentWidth(I)V

    #@66
    .line 348
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->show()V

    #@69
    .line 349
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mProvider:Landroid/view/ActionProvider;

    #@6b
    if-eqz v6, :cond_72

    #@6d
    .line 350
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mProvider:Landroid/view/ActionProvider;

    #@6f
    invoke-virtual {v6, v5}, Landroid/view/ActionProvider;->subUiVisibilityChanged(Z)V

    #@72
    .line 352
    :cond_72
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    #@75
    move-result-object v5

    #@76
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@78
    const v7, 0x10404fa

    #@7b
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@82
    .line 355
    .end local v1           #contentWidth:I
    :cond_82
    return-void

    #@83
    .end local v0           #activityCount:I
    .end local v2           #defaultActivityButtonShown:Z
    .end local v3           #maxActivityCountOffset:I
    .end local v4           #popupWindow:Landroid/widget/ListPopupWindow;
    :cond_83
    move v2, v6

    #@84
    .line 325
    goto :goto_24

    #@85
    .restart local v0       #activityCount:I
    .restart local v2       #defaultActivityButtonShown:Z
    :cond_85
    move v3, v6

    #@86
    .line 329
    goto :goto_2d

    #@87
    .line 335
    .restart local v3       #maxActivityCountOffset:I
    :cond_87
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@89
    invoke-virtual {v7, v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setShowFooterView(Z)V

    #@8c
    .line 336
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@8e
    invoke-virtual {v7, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setMaxActivityCount(I)V

    #@91
    goto :goto_42

    #@92
    .line 344
    .restart local v4       #popupWindow:Landroid/widget/ListPopupWindow;
    :cond_92
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@94
    invoke-virtual {v7, v6, v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setShowDefaultActivity(ZZ)V

    #@97
    goto :goto_57
.end method

.method private updateAppearance()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 492
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@4
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getCount()I

    #@7
    move-result v6

    #@8
    if-lez v6, :cond_64

    #@a
    .line 493
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@c
    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    #@f
    .line 498
    :goto_f
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@11
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getActivityCount()I

    #@14
    move-result v1

    #@15
    .line 499
    .local v1, activityCount:I
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@17
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getHistorySize()I

    #@1a
    move-result v3

    #@1b
    .line 500
    .local v3, historySize:I
    if-eq v1, v8, :cond_21

    #@1d
    if-le v1, v8, :cond_6a

    #@1f
    if-lez v3, :cond_6a

    #@21
    .line 501
    :cond_21
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@23
    invoke-virtual {v6, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@26
    .line 502
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@28
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDefaultActivity()Landroid/content/pm/ResolveInfo;

    #@2b
    move-result-object v0

    #@2c
    .line 503
    .local v0, activity:Landroid/content/pm/ResolveInfo;
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@31
    move-result-object v5

    #@32
    .line 504
    .local v5, packageManager:Landroid/content/pm/PackageManager;
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButtonImage:Landroid/widget/ImageView;

    #@34
    invoke-virtual {v0, v5}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3b
    .line 505
    iget v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActionButtonContentDescription:I

    #@3d
    if-eqz v6, :cond_54

    #@3f
    .line 506
    invoke-virtual {v0, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@42
    move-result-object v4

    #@43
    .line 507
    .local v4, label:Ljava/lang/CharSequence;
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@45
    iget v7, p0, Landroid/widget/ActivityChooserView;->mDefaultActionButtonContentDescription:I

    #@47
    new-array v8, v8, [Ljava/lang/Object;

    #@49
    aput-object v4, v8, v9

    #@4b
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    .line 509
    .local v2, contentDescription:Ljava/lang/String;
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@51
    invoke-virtual {v6, v2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    #@54
    .line 515
    .end local v0           #activity:Landroid/content/pm/ResolveInfo;
    .end local v2           #contentDescription:Ljava/lang/String;
    .end local v4           #label:Ljava/lang/CharSequence;
    .end local v5           #packageManager:Landroid/content/pm/PackageManager;
    :cond_54
    :goto_54
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@56
    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getVisibility()I

    #@59
    move-result v6

    #@5a
    if-nez v6, :cond_72

    #@5c
    .line 516
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@5e
    iget-object v7, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContentBackground:Landroid/graphics/drawable/Drawable;

    #@60
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@63
    .line 520
    :goto_63
    return-void

    #@64
    .line 495
    .end local v1           #activityCount:I
    .end local v3           #historySize:I
    :cond_64
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButton:Landroid/widget/FrameLayout;

    #@66
    invoke-virtual {v6, v9}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    #@69
    goto :goto_f

    #@6a
    .line 512
    .restart local v1       #activityCount:I
    .restart local v3       #historySize:I
    :cond_6a
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@6c
    const/16 v7, 0x8

    #@6e
    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@71
    goto :goto_54

    #@72
    .line 518
    :cond_72
    iget-object v6, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@74
    const/4 v7, 0x0

    #@75
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@78
    goto :goto_63
.end method


# virtual methods
.method public dismissPopup()Z
    .registers 3

    #@0
    .prologue
    .line 363
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->isShowingPopup()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1c

    #@6
    .line 364
    invoke-direct {p0}, Landroid/widget/ActivityChooserView;->getListPopupWindow()Landroid/widget/ListPopupWindow;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@d
    .line 365
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@10
    move-result-object v0

    #@11
    .line 366
    .local v0, viewTreeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_1c

    #@17
    .line 367
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@19
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@1c
    .line 370
    .end local v0           #viewTreeObserver:Landroid/view/ViewTreeObserver;
    :cond_1c
    const/4 v1, 0x1

    #@1d
    return v1
.end method

.method public getDataModel()Landroid/widget/ActivityChooserModel;
    .registers 2

    #@0
    .prologue
    .line 432
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@2
    invoke-virtual {v0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDataModel()Landroid/widget/ActivityChooserModel;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public isShowingPopup()Z
    .registers 2

    #@0
    .prologue
    .line 379
    invoke-direct {p0}, Landroid/widget/ActivityChooserView;->getListPopupWindow()Landroid/widget/ListPopupWindow;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 384
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    #@3
    .line 385
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@5
    invoke-virtual {v1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDataModel()Landroid/widget/ActivityChooserModel;

    #@8
    move-result-object v0

    #@9
    .line 386
    .local v0, dataModel:Landroid/widget/ActivityChooserModel;
    if-eqz v0, :cond_10

    #@b
    .line 387
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mModelDataSetOberver:Landroid/database/DataSetObserver;

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/ActivityChooserModel;->registerObserver(Ljava/lang/Object;)V

    #@10
    .line 389
    :cond_10
    const/4 v1, 0x1

    #@11
    iput-boolean v1, p0, Landroid/widget/ActivityChooserView;->mIsAttachedToWindow:Z

    #@13
    .line 390
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 4

    #@0
    .prologue
    .line 394
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    #@3
    .line 395
    iget-object v2, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@5
    invoke-virtual {v2}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDataModel()Landroid/widget/ActivityChooserModel;

    #@8
    move-result-object v0

    #@9
    .line 396
    .local v0, dataModel:Landroid/widget/ActivityChooserModel;
    if-eqz v0, :cond_10

    #@b
    .line 397
    iget-object v2, p0, Landroid/widget/ActivityChooserView;->mModelDataSetOberver:Landroid/database/DataSetObserver;

    #@d
    invoke-virtual {v0, v2}, Landroid/widget/ActivityChooserModel;->unregisterObserver(Ljava/lang/Object;)V

    #@10
    .line 399
    :cond_10
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@13
    move-result-object v1

    #@14
    .line 400
    .local v1, viewTreeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1f

    #@1a
    .line 401
    iget-object v2, p0, Landroid/widget/ActivityChooserView;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    #@1c
    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@1f
    .line 403
    :cond_1f
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->isShowingPopup()Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_28

    #@25
    .line 404
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->dismissPopup()Z

    #@28
    .line 406
    :cond_28
    const/4 v2, 0x0

    #@29
    iput-boolean v2, p0, Landroid/widget/ActivityChooserView;->mIsAttachedToWindow:Z

    #@2b
    .line 407
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 10
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 425
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@3
    sub-int v1, p4, p2

    #@5
    sub-int v2, p5, p3

    #@7
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/widget/LinearLayout;->layout(IIII)V

    #@a
    .line 426
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->isShowingPopup()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_13

    #@10
    .line 427
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->dismissPopup()Z

    #@13
    .line 429
    :cond_13
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 411
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mActivityChooserContent:Landroid/widget/LinearLayout;

    #@2
    .line 415
    .local v0, child:Landroid/view/View;
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mDefaultActivityButton:Landroid/widget/FrameLayout;

    #@4
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_14

    #@a
    .line 416
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@d
    move-result v1

    #@e
    const/high16 v2, 0x4000

    #@10
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result p2

    #@14
    .line 419
    :cond_14
    invoke-virtual {p0, v0, p1, p2}, Landroid/widget/ActivityChooserView;->measureChild(Landroid/view/View;II)V

    #@17
    .line 420
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {p0, v1, v2}, Landroid/widget/ActivityChooserView;->setMeasuredDimension(II)V

    #@22
    .line 421
    return-void
.end method

.method public setActivityChooserModel(Landroid/widget/ActivityChooserModel;)V
    .registers 3
    .parameter "dataModel"

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mAdapter:Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->setDataModel(Landroid/widget/ActivityChooserModel;)V

    #@5
    .line 255
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->isShowingPopup()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 256
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->dismissPopup()Z

    #@e
    .line 257
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->showPopup()Z

    #@11
    .line 259
    :cond_11
    return-void
.end method

.method public setDefaultActionButtonContentDescription(I)V
    .registers 2
    .parameter "resourceId"

    #@0
    .prologue
    .line 467
    iput p1, p0, Landroid/widget/ActivityChooserView;->mDefaultActionButtonContentDescription:I

    #@2
    .line 468
    return-void
.end method

.method public setExpandActivityOverflowButtonContentDescription(I)V
    .registers 4
    .parameter "resourceId"

    #@0
    .prologue
    .line 287
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 288
    .local v0, contentDescription:Ljava/lang/CharSequence;
    iget-object v1, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButtonImage:Landroid/widget/ImageView;

    #@8
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@b
    .line 289
    return-void
.end method

.method public setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    #@0
    .prologue
    .line 273
    iget-object v0, p0, Landroid/widget/ActivityChooserView;->mExpandActivityOverflowButtonImage:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 274
    return-void
.end method

.method public setInitialActivityCount(I)V
    .registers 2
    .parameter "itemCount"

    #@0
    .prologue
    .line 453
    iput p1, p0, Landroid/widget/ActivityChooserView;->mInitialActivityCount:I

    #@2
    .line 454
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 441
    iput-object p1, p0, Landroid/widget/ActivityChooserView;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    #@2
    .line 442
    return-void
.end method

.method public setProvider(Landroid/view/ActionProvider;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 296
    iput-object p1, p0, Landroid/widget/ActivityChooserView;->mProvider:Landroid/view/ActionProvider;

    #@2
    .line 297
    return-void
.end method

.method public showPopup()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 305
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView;->isShowingPopup()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_b

    #@7
    iget-boolean v1, p0, Landroid/widget/ActivityChooserView;->mIsAttachedToWindow:Z

    #@9
    if-nez v1, :cond_c

    #@b
    .line 310
    :cond_b
    :goto_b
    return v0

    #@c
    .line 308
    :cond_c
    iput-boolean v0, p0, Landroid/widget/ActivityChooserView;->mIsSelectingDefaultActivity:Z

    #@e
    .line 309
    iget v0, p0, Landroid/widget/ActivityChooserView;->mInitialActivityCount:I

    #@10
    invoke-direct {p0, v0}, Landroid/widget/ActivityChooserView;->showPopupUnchecked(I)V

    #@13
    .line 310
    const/4 v0, 0x1

    #@14
    goto :goto_b
.end method
