.class public Landroid/widget/RatingBar;
.super Landroid/widget/AbsSeekBar;
.source "RatingBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RatingBar$OnRatingBarChangeListener;
    }
.end annotation


# instance fields
.field private mNumStars:I

.field private mOnRatingBarChangeListener:Landroid/widget/RatingBar$OnRatingBarChangeListener;

.field private mProgressOnStartTracking:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 120
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 116
    const v0, 0x101007c

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    const/high16 v8, -0x4080

    #@4
    const/4 v4, 0x0

    #@5
    .line 86
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8
    .line 79
    const/4 v6, 0x5

    #@9
    iput v6, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@b
    .line 88
    sget-object v6, Lcom/android/internal/R$styleable;->RatingBar:[I

    #@d
    invoke-virtual {p1, p2, v6, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@10
    move-result-object v0

    #@11
    .line 90
    .local v0, a:Landroid/content/res/TypedArray;
    iget v6, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@13
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@16
    move-result v1

    #@17
    .line 91
    .local v1, numStars:I
    const/4 v6, 0x3

    #@18
    iget-boolean v7, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@1a
    if-nez v7, :cond_1d

    #@1c
    move v4, v5

    #@1d
    :cond_1d
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@20
    move-result v4

    #@21
    invoke-virtual {p0, v4}, Landroid/widget/RatingBar;->setIsIndicator(Z)V

    #@24
    .line 92
    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@27
    move-result v2

    #@28
    .line 93
    .local v2, rating:F
    const/4 v4, 0x2

    #@29
    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@2c
    move-result v3

    #@2d
    .line 94
    .local v3, stepSize:F
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@30
    .line 96
    if-lez v1, :cond_39

    #@32
    iget v4, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@34
    if-eq v1, v4, :cond_39

    #@36
    .line 97
    invoke-virtual {p0, v1}, Landroid/widget/RatingBar;->setNumStars(I)V

    #@39
    .line 100
    :cond_39
    cmpl-float v4, v3, v9

    #@3b
    if-ltz v4, :cond_4d

    #@3d
    .line 101
    invoke-virtual {p0, v3}, Landroid/widget/RatingBar;->setStepSize(F)V

    #@40
    .line 106
    :goto_40
    cmpl-float v4, v2, v9

    #@42
    if-ltz v4, :cond_47

    #@44
    .line 107
    invoke-virtual {p0, v2}, Landroid/widget/RatingBar;->setRating(F)V

    #@47
    .line 112
    :cond_47
    const v4, 0x3f8ccccd

    #@4a
    iput v4, p0, Landroid/widget/AbsSeekBar;->mTouchProgressOffset:F

    #@4c
    .line 113
    return-void

    #@4d
    .line 103
    :cond_4d
    const/high16 v4, 0x3f00

    #@4f
    invoke-virtual {p0, v4}, Landroid/widget/RatingBar;->setStepSize(F)V

    #@52
    goto :goto_40
.end method

.method private getProgressPerStar()F
    .registers 3

    #@0
    .prologue
    const/high16 v0, 0x3f80

    #@2
    .line 236
    iget v1, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@4
    if-lez v1, :cond_10

    #@6
    .line 237
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getMax()I

    #@9
    move-result v1

    #@a
    int-to-float v1, v1

    #@b
    mul-float/2addr v0, v1

    #@c
    iget v1, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@e
    int-to-float v1, v1

    #@f
    div-float/2addr v0, v1

    #@10
    .line 239
    :cond_10
    return v0
.end method

.method private updateSecondaryProgress(I)V
    .registers 9
    .parameter "progress"

    #@0
    .prologue
    .line 270
    invoke-direct {p0}, Landroid/widget/RatingBar;->getProgressPerStar()F

    #@3
    move-result v1

    #@4
    .line 271
    .local v1, ratio:F
    const/4 v3, 0x0

    #@5
    cmpl-float v3, v1, v3

    #@7
    if-lez v3, :cond_17

    #@9
    .line 272
    int-to-float v3, p1

    #@a
    div-float v0, v3, v1

    #@c
    .line 273
    .local v0, progressInStars:F
    float-to-double v3, v0

    #@d
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@10
    move-result-wide v3

    #@11
    float-to-double v5, v1

    #@12
    mul-double/2addr v3, v5

    #@13
    double-to-int v2, v3

    #@14
    .line 274
    .local v2, secondaryProgress:I
    invoke-virtual {p0, v2}, Landroid/widget/RatingBar;->setSecondaryProgress(I)V

    #@17
    .line 276
    .end local v0           #progressInStars:F
    .end local v2           #secondaryProgress:I
    :cond_17
    return-void
.end method


# virtual methods
.method dispatchRatingChange(Z)V
    .registers 4
    .parameter "fromUser"

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Landroid/widget/RatingBar;->mOnRatingBarChangeListener:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 315
    iget-object v0, p0, Landroid/widget/RatingBar;->mOnRatingBarChangeListener:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    #@6
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getRating()F

    #@9
    move-result v1

    #@a
    invoke-interface {v0, p0, v1, p1}, Landroid/widget/RatingBar$OnRatingBarChangeListener;->onRatingChanged(Landroid/widget/RatingBar;FZ)V

    #@d
    .line 318
    :cond_d
    return-void
.end method

.method getDrawableShape()Landroid/graphics/drawable/shapes/Shape;
    .registers 2

    #@0
    .prologue
    .line 246
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    #@2
    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    #@5
    return-object v0
.end method

.method public getNumStars()I
    .registers 2

    #@0
    .prologue
    .line 185
    iget v0, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@2
    return v0
.end method

.method public getOnRatingBarChangeListener()Landroid/widget/RatingBar$OnRatingBarChangeListener;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/widget/RatingBar;->mOnRatingBarChangeListener:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    #@2
    return-object v0
.end method

.method public getRating()F
    .registers 3

    #@0
    .prologue
    .line 203
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getProgress()I

    #@3
    move-result v0

    #@4
    int-to-float v0, v0

    #@5
    invoke-direct {p0}, Landroid/widget/RatingBar;->getProgressPerStar()F

    #@8
    move-result v1

    #@9
    div-float/2addr v0, v1

    #@a
    return v0
.end method

.method public getStepSize()F
    .registers 3

    #@0
    .prologue
    .line 229
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getNumStars()I

    #@3
    move-result v0

    #@4
    int-to-float v0, v0

    #@5
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getMax()I

    #@8
    move-result v1

    #@9
    int-to-float v1, v1

    #@a
    div-float/2addr v0, v1

    #@b
    return v0
.end method

.method public isIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 159
    iget-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 332
    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 333
    const-class v0, Landroid/widget/RatingBar;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 334
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 338
    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 339
    const-class v0, Landroid/widget/RatingBar;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 340
    return-void
.end method

.method onKeyChange()V
    .registers 2

    #@0
    .prologue
    .line 309
    invoke-super {p0}, Landroid/widget/AbsSeekBar;->onKeyChange()V

    #@3
    .line 310
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/RatingBar;->dispatchRatingChange(Z)V

    #@7
    .line 311
    return-void
.end method

.method protected declared-synchronized onMeasure(II)V
    .registers 6
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 280
    monitor-enter p0

    #@1
    :try_start_1
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSeekBar;->onMeasure(II)V

    #@4
    .line 282
    iget-object v1, p0, Landroid/widget/ProgressBar;->mSampleTile:Landroid/graphics/Bitmap;

    #@6
    if-eqz v1, :cond_1e

    #@8
    .line 285
    iget-object v1, p0, Landroid/widget/ProgressBar;->mSampleTile:Landroid/graphics/Bitmap;

    #@a
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@d
    move-result v1

    #@e
    iget v2, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@10
    mul-int v0, v1, v2

    #@12
    .line 286
    .local v0, width:I
    const/4 v1, 0x0

    #@13
    invoke-static {v0, p1, v1}, Landroid/widget/RatingBar;->resolveSizeAndState(III)I

    #@16
    move-result v1

    #@17
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getMeasuredHeight()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {p0, v1, v2}, Landroid/widget/RatingBar;->setMeasuredDimension(II)V
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_20

    #@1e
    .line 289
    .end local v0           #width:I
    :cond_1e
    monitor-exit p0

    #@1f
    return-void

    #@20
    .line 280
    :catchall_20
    move-exception v1

    #@21
    monitor-exit p0

    #@22
    throw v1
.end method

.method onProgressRefresh(FZ)V
    .registers 4
    .parameter "scale"
    .parameter "fromUser"

    #@0
    .prologue
    .line 251
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSeekBar;->onProgressRefresh(FZ)V

    #@3
    .line 254
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getProgress()I

    #@6
    move-result v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/RatingBar;->updateSecondaryProgress(I)V

    #@a
    .line 256
    if-nez p2, :cond_10

    #@c
    .line 258
    const/4 v0, 0x0

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/RatingBar;->dispatchRatingChange(Z)V

    #@10
    .line 260
    :cond_10
    return-void
.end method

.method onStartTrackingTouch()V
    .registers 2

    #@0
    .prologue
    .line 293
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getProgress()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/widget/RatingBar;->mProgressOnStartTracking:I

    #@6
    .line 295
    invoke-super {p0}, Landroid/widget/AbsSeekBar;->onStartTrackingTouch()V

    #@9
    .line 296
    return-void
.end method

.method onStopTrackingTouch()V
    .registers 3

    #@0
    .prologue
    .line 300
    invoke-super {p0}, Landroid/widget/AbsSeekBar;->onStopTrackingTouch()V

    #@3
    .line 302
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getProgress()I

    #@6
    move-result v0

    #@7
    iget v1, p0, Landroid/widget/RatingBar;->mProgressOnStartTracking:I

    #@9
    if-eq v0, v1, :cond_f

    #@b
    .line 303
    const/4 v0, 0x1

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/RatingBar;->dispatchRatingChange(Z)V

    #@f
    .line 305
    :cond_f
    return-void
.end method

.method public setIsIndicator(Z)V
    .registers 5
    .parameter "isIndicator"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 149
    if-nez p1, :cond_d

    #@4
    move v0, v1

    #@5
    :goto_5
    iput-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@7
    .line 150
    if-nez p1, :cond_f

    #@9
    :goto_9
    invoke-virtual {p0, v1}, Landroid/widget/RatingBar;->setFocusable(Z)V

    #@c
    .line 151
    return-void

    #@d
    :cond_d
    move v0, v2

    #@e
    .line 149
    goto :goto_5

    #@f
    :cond_f
    move v1, v2

    #@10
    .line 150
    goto :goto_9
.end method

.method public declared-synchronized setMax(I)V
    .registers 3
    .parameter "max"

    #@0
    .prologue
    .line 323
    monitor-enter p0

    #@1
    if-gtz p1, :cond_5

    #@3
    .line 328
    :goto_3
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 327
    :cond_5
    :try_start_5
    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->setMax(I)V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_9

    #@8
    goto :goto_3

    #@9
    .line 323
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public setNumStars(I)V
    .registers 2
    .parameter "numStars"

    #@0
    .prologue
    .line 170
    if-gtz p1, :cond_3

    #@2
    .line 178
    :goto_2
    return-void

    #@3
    .line 174
    :cond_3
    iput p1, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@5
    .line 177
    invoke-virtual {p0}, Landroid/widget/RatingBar;->requestLayout()V

    #@8
    goto :goto_2
.end method

.method public setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 129
    iput-object p1, p0, Landroid/widget/RatingBar;->mOnRatingBarChangeListener:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    #@2
    .line 130
    return-void
.end method

.method public setRating(F)V
    .registers 3
    .parameter "rating"

    #@0
    .prologue
    .line 194
    invoke-direct {p0}, Landroid/widget/RatingBar;->getProgressPerStar()F

    #@3
    move-result v0

    #@4
    mul-float/2addr v0, p1

    #@5
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@8
    move-result v0

    #@9
    invoke-virtual {p0, v0}, Landroid/widget/RatingBar;->setProgress(I)V

    #@c
    .line 195
    return-void
.end method

.method public setStepSize(F)V
    .registers 6
    .parameter "stepSize"

    #@0
    .prologue
    .line 213
    const/4 v2, 0x0

    #@1
    cmpg-float v2, p1, v2

    #@3
    if-gtz v2, :cond_6

    #@5
    .line 221
    :goto_5
    return-void

    #@6
    .line 217
    :cond_6
    iget v2, p0, Landroid/widget/RatingBar;->mNumStars:I

    #@8
    int-to-float v2, v2

    #@9
    div-float v0, v2, p1

    #@b
    .line 218
    .local v0, newMax:F
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getMax()I

    #@e
    move-result v2

    #@f
    int-to-float v2, v2

    #@10
    div-float v2, v0, v2

    #@12
    invoke-virtual {p0}, Landroid/widget/RatingBar;->getProgress()I

    #@15
    move-result v3

    #@16
    int-to-float v3, v3

    #@17
    mul-float/2addr v2, v3

    #@18
    float-to-int v1, v2

    #@19
    .line 219
    .local v1, newProgress:I
    float-to-int v2, v0

    #@1a
    invoke-virtual {p0, v2}, Landroid/widget/RatingBar;->setMax(I)V

    #@1d
    .line 220
    invoke-virtual {p0, v1}, Landroid/widget/RatingBar;->setProgress(I)V

    #@20
    goto :goto_5
.end method
