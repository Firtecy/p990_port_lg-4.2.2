.class Landroid/widget/FastScroller$1;
.super Ljava/lang/Object;
.source "FastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/FastScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/FastScroller;


# direct methods
.method constructor <init>(Landroid/widget/FastScroller;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 132
    iput-object p1, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 134
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@2
    iget-object v2, v2, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@4
    iget-boolean v2, v2, Landroid/widget/AbsListView;->mIsAttached:Z

    #@6
    if-eqz v2, :cond_3a

    #@8
    .line 135
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@a
    invoke-virtual {v2}, Landroid/widget/FastScroller;->beginDrag()V

    #@d
    .line 137
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@f
    iget-object v2, v2, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@11
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getHeight()I

    #@14
    move-result v1

    #@15
    .line 139
    .local v1, viewHeight:I
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@17
    iget v2, v2, Landroid/widget/FastScroller;->mInitialTouchY:F

    #@19
    float-to-int v2, v2

    #@1a
    iget-object v3, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@1c
    iget v3, v3, Landroid/widget/FastScroller;->mThumbH:I

    #@1e
    sub-int/2addr v2, v3

    #@1f
    add-int/lit8 v0, v2, 0xa

    #@21
    .line 140
    .local v0, newThumbY:I
    if-gez v0, :cond_40

    #@23
    .line 141
    const/4 v0, 0x0

    #@24
    .line 145
    :cond_24
    :goto_24
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@26
    iput v0, v2, Landroid/widget/FastScroller;->mThumbY:I

    #@28
    .line 146
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@2a
    iget-object v3, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@2c
    iget v3, v3, Landroid/widget/FastScroller;->mThumbY:I

    #@2e
    int-to-float v3, v3

    #@2f
    iget-object v4, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@31
    iget v4, v4, Landroid/widget/FastScroller;->mThumbH:I

    #@33
    sub-int v4, v1, v4

    #@35
    int-to-float v4, v4

    #@36
    div-float/2addr v3, v4

    #@37
    invoke-virtual {v2, v3}, Landroid/widget/FastScroller;->scrollTo(F)V

    #@3a
    .line 149
    .end local v0           #newThumbY:I
    .end local v1           #viewHeight:I
    :cond_3a
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@3c
    const/4 v3, 0x0

    #@3d
    iput-boolean v3, v2, Landroid/widget/FastScroller;->mPendingDrag:Z

    #@3f
    .line 150
    return-void

    #@40
    .line 142
    .restart local v0       #newThumbY:I
    .restart local v1       #viewHeight:I
    :cond_40
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@42
    iget v2, v2, Landroid/widget/FastScroller;->mThumbH:I

    #@44
    add-int/2addr v2, v0

    #@45
    if-le v2, v1, :cond_24

    #@47
    .line 143
    iget-object v2, p0, Landroid/widget/FastScroller$1;->this$0:Landroid/widget/FastScroller;

    #@49
    iget v2, v2, Landroid/widget/FastScroller;->mThumbH:I

    #@4b
    sub-int v0, v1, v2

    #@4d
    goto :goto_24
.end method
