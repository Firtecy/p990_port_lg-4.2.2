.class public Landroid/widget/RadioGroup$LayoutParams;
.super Landroid/widget/LinearLayout$LayoutParams;
.source "RadioGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RadioGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 277
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@3
    .line 278
    return-void
.end method

.method public constructor <init>(IIF)V
    .registers 4
    .parameter "w"
    .parameter "h"
    .parameter "initWeight"

    #@0
    .prologue
    .line 284
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@3
    .line 285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    .line 270
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 271
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 2
    .parameter "p"

    #@0
    .prologue
    .line 291
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 292
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 298
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 299
    return-void
.end method


# virtual methods
.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .registers 6
    .parameter "a"
    .parameter "widthAttr"
    .parameter "heightAttr"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 315
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_20

    #@7
    .line 316
    const-string/jumbo v0, "layout_width"

    #@a
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@10
    .line 321
    :goto_10
    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_23

    #@16
    .line 322
    const-string/jumbo v0, "layout_height"

    #@19
    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1f
    .line 326
    :goto_1f
    return-void

    #@20
    .line 318
    :cond_20
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@22
    goto :goto_10

    #@23
    .line 324
    :cond_23
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@25
    goto :goto_1f
.end method
