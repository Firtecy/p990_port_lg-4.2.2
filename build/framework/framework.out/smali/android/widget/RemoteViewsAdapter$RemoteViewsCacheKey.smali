.class Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
.super Ljava/lang/Object;
.source "RemoteViewsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViewsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RemoteViewsCacheKey"
.end annotation


# instance fields
.field final filter:Landroid/content/Intent$FilterComparison;

.field final userId:I

.field final widgetId:I


# direct methods
.method constructor <init>(Landroid/content/Intent$FilterComparison;II)V
    .registers 4
    .parameter "filter"
    .parameter "widgetId"
    .parameter "userId"

    #@0
    .prologue
    .line 799
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 800
    iput-object p1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->filter:Landroid/content/Intent$FilterComparison;

    #@5
    .line 801
    iput p2, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->widgetId:I

    #@7
    .line 802
    iput p3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->userId:I

    #@9
    .line 803
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 807
    instance-of v2, p1, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 811
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 810
    check-cast v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;

    #@9
    .line 811
    .local v0, other:Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;
    iget-object v2, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->filter:Landroid/content/Intent$FilterComparison;

    #@b
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->filter:Landroid/content/Intent$FilterComparison;

    #@d
    invoke-virtual {v2, v3}, Landroid/content/Intent$FilterComparison;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_5

    #@13
    iget v2, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->widgetId:I

    #@15
    iget v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->widgetId:I

    #@17
    if-ne v2, v3, :cond_5

    #@19
    iget v2, v0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->userId:I

    #@1b
    iget v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->userId:I

    #@1d
    if-ne v2, v3, :cond_5

    #@1f
    const/4 v1, 0x1

    #@20
    goto :goto_5
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 817
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->filter:Landroid/content/Intent$FilterComparison;

    #@2
    if-nez v0, :cond_10

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    iget v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->widgetId:I

    #@7
    shl-int/lit8 v1, v1, 0x2

    #@9
    xor-int/2addr v0, v1

    #@a
    iget v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->userId:I

    #@c
    shl-int/lit8 v1, v1, 0xa

    #@e
    xor-int/2addr v0, v1

    #@f
    return v0

    #@10
    :cond_10
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsCacheKey;->filter:Landroid/content/Intent$FilterComparison;

    #@12
    invoke-virtual {v0}, Landroid/content/Intent$FilterComparison;->hashCode()I

    #@15
    move-result v0

    #@16
    goto :goto_5
.end method
