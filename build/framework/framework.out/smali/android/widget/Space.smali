.class public final Landroid/widget/Space;
.super Landroid/view/View;
.source "Space.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 52
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/Space;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Space;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 35
    invoke-virtual {p0}, Landroid/widget/Space;->getVisibility()I

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_d

    #@9
    .line 36
    const/4 v0, 0x4

    #@a
    invoke-virtual {p0, v0}, Landroid/widget/Space;->setVisibility(I)V

    #@d
    .line 38
    :cond_d
    return-void
.end method

.method private static getDefaultSize2(II)I
    .registers 5
    .parameter "size"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 70
    move v0, p0

    #@1
    .line 71
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v1

    #@5
    .line 72
    .local v1, specMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 74
    .local v2, specSize:I
    sparse-switch v1, :sswitch_data_16

    #@c
    .line 85
    :goto_c
    return v0

    #@d
    .line 76
    :sswitch_d
    move v0, p0

    #@e
    .line 77
    goto :goto_c

    #@f
    .line 79
    :sswitch_f
    invoke-static {p0, v2}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v0

    #@13
    .line 80
    goto :goto_c

    #@14
    .line 82
    :sswitch_14
    move v0, v2

    #@15
    goto :goto_c

    #@16
    .line 74
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_f
        0x0 -> :sswitch_d
        0x40000000 -> :sswitch_14
    .end sparse-switch
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 62
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 90
    invoke-virtual {p0}, Landroid/widget/Space;->getSuggestedMinimumWidth()I

    #@3
    move-result v0

    #@4
    invoke-static {v0, p1}, Landroid/widget/Space;->getDefaultSize2(II)I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0}, Landroid/widget/Space;->getSuggestedMinimumHeight()I

    #@b
    move-result v1

    #@c
    invoke-static {v1, p2}, Landroid/widget/Space;->getDefaultSize2(II)I

    #@f
    move-result v1

    #@10
    invoke-virtual {p0, v0, v1}, Landroid/widget/Space;->setMeasuredDimension(II)V

    #@13
    .line 93
    return-void
.end method
