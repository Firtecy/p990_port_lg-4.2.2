.class public Landroid/widget/BubblePopupHelper;
.super Ljava/lang/Object;
.source "BubblePopupHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BubblePopupHelper"

.field private static final TARGET_OPERATOR:Ljava/lang/String;

.field private static sHelper:Landroid/widget/BubblePopupHelper;


# instance fields
.field mIsShowingBubblePopup:Z

.field mView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 22
    const-string/jumbo v0, "ro.build.target_operator"

    #@3
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/widget/BubblePopupHelper;->TARGET_OPERATOR:Ljava/lang/String;

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/widget/TextView;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    iput-object p1, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@5
    .line 29
    return-void
.end method

.method private checkParentVisibility(Landroid/view/View;)Z
    .registers 8
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 113
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@5
    move-result v4

    #@6
    const v5, 0x10202a8

    #@9
    if-ne v4, v5, :cond_14

    #@b
    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_12

    #@11
    .line 122
    :cond_11
    :goto_11
    return v2

    #@12
    :cond_12
    move v2, v3

    #@13
    .line 114
    goto :goto_11

    #@14
    .line 117
    :cond_14
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@17
    move-result-object v0

    #@18
    .line 118
    .local v0, parent:Landroid/view/ViewParent;
    const/4 v1, 0x0

    #@19
    .line 119
    .local v1, target:Landroid/view/View;
    instance-of v4, v0, Landroid/view/View;

    #@1b
    if-eqz v4, :cond_20

    #@1d
    move-object v1, v0

    #@1e
    .line 120
    check-cast v1, Landroid/view/View;

    #@20
    .line 122
    :cond_20
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    #@23
    move-result v4

    #@24
    if-nez v4, :cond_2e

    #@26
    if-eqz v1, :cond_11

    #@28
    invoke-direct {p0, v1}, Landroid/widget/BubblePopupHelper;->checkParentVisibility(Landroid/view/View;)Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_11

    #@2e
    :cond_2e
    move v2, v3

    #@2f
    goto :goto_11
.end method

.method private isFloatingWindow()Z
    .registers 6

    #@0
    .prologue
    .line 126
    const/4 v0, 0x0

    #@1
    .line 127
    .local v0, isFloatingWindow:Z
    iget-object v3, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v3}, Landroid/widget/TextView;->getRootView()Landroid/view/View;

    #@6
    move-result-object v2

    #@7
    .line 128
    .local v2, rootView:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@a
    move-result-object v1

    #@b
    .line 129
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v3, v1, Landroid/view/WindowManager$LayoutParams;

    #@d
    if-eqz v3, :cond_28

    #@f
    .line 130
    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    #@11
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@13
    const/16 v4, 0x7d2

    #@15
    if-ne v3, v4, :cond_29

    #@17
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, "FrameView"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_29

    #@27
    const/4 v0, 0x1

    #@28
    .line 134
    :cond_28
    :goto_28
    return v0

    #@29
    .line 130
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_28
.end method

.method public static isShowingAnyBubblePopup()Z
    .registers 2

    #@0
    .prologue
    .line 44
    sget-object v0, Landroid/widget/BubblePopupHelper;->sHelper:Landroid/widget/BubblePopupHelper;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 45
    sget-object v0, Landroid/widget/BubblePopupHelper;->sHelper:Landroid/widget/BubblePopupHelper;

    #@6
    iget-boolean v0, v0, Landroid/widget/BubblePopupHelper;->mIsShowingBubblePopup:Z

    #@8
    .line 48
    :goto_8
    return v0

    #@9
    .line 47
    :cond_9
    const-string v0, "BubblePopupHelper"

    #@b
    const-string v1, "BubblePopupHelper has no instance. we don\'t ready to call isShowingBubblePopup and just return false"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 48
    const/4 v0, 0x0

    #@11
    goto :goto_8
.end method

.method public static setShowingAnyBubblePopup(Z)V
    .registers 4
    .parameter "showing"

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/widget/BubblePopupHelper;->sHelper:Landroid/widget/BubblePopupHelper;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 37
    sget-object v0, Landroid/widget/BubblePopupHelper;->sHelper:Landroid/widget/BubblePopupHelper;

    #@6
    invoke-virtual {v0, p0}, Landroid/widget/BubblePopupHelper;->setShowingBubblePopup(Z)V

    #@9
    .line 41
    :goto_9
    return-void

    #@a
    .line 39
    :cond_a
    const-string v0, "BubblePopupHelper"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "BubblePopupHelper has no instance. Hence we cannot call setShowingBubblePopup : "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_9
.end method


# virtual methods
.method public checkIsPossibleHideBubblePopup()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 63
    invoke-virtual {p0}, Landroid/widget/BubblePopupHelper;->isShowingBubblePopup()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 65
    :cond_7
    :goto_7
    return v0

    #@8
    .line 64
    :cond_8
    iget-object v1, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@a
    instance-of v1, v1, Landroid/inputmethodservice/ExtractEditText;

    #@c
    if-eqz v1, :cond_7

    #@e
    invoke-virtual {p0}, Landroid/widget/BubblePopupHelper;->isFullscreenMode()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_7

    #@14
    const/4 v0, 0x0

    #@15
    goto :goto_7
.end method

.method public checkIsPossibleShowBubblePopup()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 69
    iget-object v1, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@3
    instance-of v1, v1, Landroid/inputmethodservice/ExtractEditText;

    #@5
    if-eqz v1, :cond_e

    #@7
    invoke-virtual {p0}, Landroid/widget/BubblePopupHelper;->isFullscreenMode()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_e

    #@d
    .line 71
    :cond_d
    :goto_d
    return v0

    #@e
    .line 70
    :cond_e
    iget-object v1, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@10
    instance-of v1, v1, Landroid/inputmethodservice/ExtractEditText;

    #@12
    if-nez v1, :cond_1a

    #@14
    invoke-virtual {p0}, Landroid/widget/BubblePopupHelper;->isFullscreenMode()Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_d

    #@1a
    .line 71
    :cond_1a
    invoke-virtual {p0}, Landroid/widget/BubblePopupHelper;->isShowingBubblePopup()Z

    #@1d
    move-result v0

    #@1e
    goto :goto_d
.end method

.method public isFullscreenMode()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 75
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@3
    instance-of v6, v6, Landroid/inputmethodservice/ExtractEditText;

    #@5
    if-eqz v6, :cond_e

    #@7
    .line 77
    iget-object v5, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@9
    invoke-direct {p0, v5}, Landroid/widget/BubblePopupHelper;->checkParentVisibility(Landroid/view/View;)Z

    #@c
    move-result v5

    #@d
    .line 108
    :cond_d
    :goto_d
    return v5

    #@e
    .line 79
    :cond_e
    invoke-direct {p0}, Landroid/widget/BubblePopupHelper;->isFloatingWindow()Z

    #@11
    move-result v6

    #@12
    if-nez v6, :cond_d

    #@14
    .line 82
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@16
    if-eqz v6, :cond_25

    #@18
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@1a
    new-instance v7, Landroid/graphics/Rect;

    #@1c
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@1f
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->isWindowSplit(Landroid/graphics/Rect;)Z

    #@22
    move-result v6

    #@23
    if-nez v6, :cond_d

    #@25
    .line 85
    :cond_25
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@27
    invoke-virtual {v6}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@2e
    move-result-object v0

    #@2f
    .line 86
    .local v0, config:Landroid/content/res/Configuration;
    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    #@31
    const/4 v7, 0x2

    #@32
    if-ne v6, v7, :cond_d

    #@34
    .line 87
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@36
    invoke-virtual {v6}, Landroid/widget/TextView;->getImeOptions()I

    #@39
    move-result v6

    #@3a
    const/high16 v7, 0x200

    #@3c
    and-int/2addr v6, v7

    #@3d
    if-nez v6, :cond_d

    #@3f
    .line 88
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@41
    invoke-virtual {v6}, Landroid/widget/TextView;->getImeOptions()I

    #@44
    move-result v6

    #@45
    const/high16 v7, 0x1000

    #@47
    and-int/2addr v6, v7

    #@48
    if-nez v6, :cond_d

    #@4a
    .line 91
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@4c
    invoke-virtual {v6}, Landroid/widget/TextView;->isTextSelectable()Z

    #@4f
    move-result v6

    #@50
    if-eqz v6, :cond_5a

    #@52
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@54
    invoke-virtual {v6}, Landroid/widget/TextView;->isTextEditable()Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_d

    #@5a
    .line 93
    :cond_5a
    sget-object v6, Landroid/widget/BubblePopupHelper;->TARGET_OPERATOR:Ljava/lang/String;

    #@5c
    const-string v7, "DCM"

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v6

    #@62
    if-eqz v6, :cond_70

    #@64
    .line 94
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@67
    move-result-object v1

    #@68
    .line 95
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_70

    #@6a
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    #@6d
    move-result v6

    #@6e
    if-eqz v6, :cond_d

    #@70
    .line 99
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_70
    const/4 v2, 0x0

    #@71
    .line 100
    .local v2, mContext:Landroid/content/Context;
    iget-object v6, p0, Landroid/widget/BubblePopupHelper;->mView:Landroid/widget/TextView;

    #@73
    invoke-virtual {v6}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@76
    move-result-object v2

    #@77
    .line 101
    if-eqz v2, :cond_8d

    #@79
    instance-of v6, v2, Landroid/app/Activity;

    #@7b
    if-eqz v6, :cond_8d

    #@7d
    move-object v3, v2

    #@7e
    .line 103
    check-cast v3, Landroid/app/Activity;

    #@80
    .line 104
    .local v3, mHost:Landroid/app/Activity;
    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@87
    move-result-object v6

    #@88
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@8a
    .line 105
    .local v4, windowSoftInputMode:I
    const/4 v6, 0x3

    #@8b
    if-eq v4, v6, :cond_d

    #@8d
    .line 108
    .end local v3           #mHost:Landroid/app/Activity;
    .end local v4           #windowSoftInputMode:I
    :cond_8d
    const/4 v5, 0x1

    #@8e
    goto/16 :goto_d
.end method

.method public isShowingBubblePopup()Z
    .registers 4

    #@0
    .prologue
    .line 57
    const-string v0, "BubblePopupHelper"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isShowingBubblePopup : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-boolean v2, p0, Landroid/widget/BubblePopupHelper;->mIsShowingBubblePopup:Z

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 58
    iget-boolean v0, p0, Landroid/widget/BubblePopupHelper;->mIsShowingBubblePopup:Z

    #@1c
    return v0
.end method

.method public setShowingBubblePopup(Z)V
    .registers 2
    .parameter "showing"

    #@0
    .prologue
    .line 53
    iput-boolean p1, p0, Landroid/widget/BubblePopupHelper;->mIsShowingBubblePopup:Z

    #@2
    .line 54
    return-void
.end method

.method public setTargetHelper()V
    .registers 1

    #@0
    .prologue
    .line 32
    sput-object p0, Landroid/widget/BubblePopupHelper;->sHelper:Landroid/widget/BubblePopupHelper;

    #@2
    .line 33
    return-void
.end method
