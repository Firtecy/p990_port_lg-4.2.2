.class public Landroid/widget/RemoteViews;
.super Ljava/lang/Object;
.source "RemoteViews.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/view/LayoutInflater$Filter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/RemoteViews$2;,
        Landroid/widget/RemoteViews$MemoryUsageCounter;,
        Landroid/widget/RemoteViews$ViewPaddingAction;,
        Landroid/widget/RemoteViews$TextViewSizeAction;,
        Landroid/widget/RemoteViews$TextViewDrawableAction;,
        Landroid/widget/RemoteViews$ViewGroupAction;,
        Landroid/widget/RemoteViews$ReflectionAction;,
        Landroid/widget/RemoteViews$BitmapReflectionAction;,
        Landroid/widget/RemoteViews$BitmapCache;,
        Landroid/widget/RemoteViews$ReflectionActionWithoutParams;,
        Landroid/widget/RemoteViews$SetDrawableParameters;,
        Landroid/widget/RemoteViews$SetOnClickPendingIntent;,
        Landroid/widget/RemoteViews$SetRemoteViewsAdapterIntent;,
        Landroid/widget/RemoteViews$SetPendingIntentTemplate;,
        Landroid/widget/RemoteViews$SetOnClickFillInIntent;,
        Landroid/widget/RemoteViews$SetEmptyView;,
        Landroid/widget/RemoteViews$Action;,
        Landroid/widget/RemoteViews$OnClickHandler;,
        Landroid/widget/RemoteViews$ActionException;,
        Landroid/widget/RemoteViews$RemoteView;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/widget/RemoteViews;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_ON_CLICK_HANDLER:Landroid/widget/RemoteViews$OnClickHandler; = null

.field static final EXTRA_REMOTEADAPTER_APPWIDGET_ID:Ljava/lang/String; = "remoteAdapterAppWidgetId"

.field private static final LOG_TAG:Ljava/lang/String; = "RemoteViews"

.field private static final MODE_HAS_LANDSCAPE_AND_PORTRAIT:I = 0x1

.field private static final MODE_NORMAL:I


# instance fields
.field private mActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/RemoteViews$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

.field private mIsRoot:Z

.field private mIsWidgetCollectionChild:Z

.field private mLandscape:Landroid/widget/RemoteViews;

.field private final mLayoutId:I

.field private mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

.field private final mPackage:Ljava/lang/String;

.field private mPortrait:Landroid/widget/RemoteViews;

.field private mUser:Landroid/os/UserHandle;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 137
    new-instance v0, Landroid/widget/RemoteViews$OnClickHandler;

    #@2
    invoke-direct {v0}, Landroid/widget/RemoteViews$OnClickHandler;-><init>()V

    #@5
    sput-object v0, Landroid/widget/RemoteViews;->DEFAULT_ON_CLICK_HANDLER:Landroid/widget/RemoteViews$OnClickHandler;

    #@7
    .line 2384
    new-instance v0, Landroid/widget/RemoteViews$1;

    #@9
    invoke-direct {v0}, Landroid/widget/RemoteViews$1;-><init>()V

    #@c
    sput-object v0, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "parcel"

    #@0
    .prologue
    .line 1507
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V

    #@4
    .line 1508
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V
    .registers 11
    .parameter "parcel"
    .parameter "bitmapCache"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 1510
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 78
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@9
    move-result-object v6

    #@a
    iput-object v6, p0, Landroid/widget/RemoteViews;->mUser:Landroid/os/UserHandle;

    #@c
    .line 111
    iput-boolean v4, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@e
    .line 125
    iput-object v7, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@10
    .line 126
    iput-object v7, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@12
    .line 135
    iput-boolean v5, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@14
    .line 1511
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    .line 1514
    .local v2, mode:I
    if-nez p2, :cond_6d

    #@1a
    .line 1515
    new-instance v6, Landroid/widget/RemoteViews$BitmapCache;

    #@1c
    invoke-direct {v6, p1}, Landroid/widget/RemoteViews$BitmapCache;-><init>(Landroid/os/Parcel;)V

    #@1f
    iput-object v6, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@21
    .line 1521
    :goto_21
    if-nez v2, :cond_10a

    #@23
    .line 1522
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    iput-object v6, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@29
    .line 1523
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v6

    #@2d
    iput v6, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@2f
    .line 1524
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v6

    #@33
    if-ne v6, v4, :cond_74

    #@35
    :goto_35
    iput-boolean v4, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@37
    .line 1526
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v0

    #@3b
    .line 1527
    .local v0, count:I
    if-lez v0, :cond_12c

    #@3d
    .line 1528
    new-instance v4, Ljava/util/ArrayList;

    #@3f
    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@42
    iput-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@44
    .line 1529
    const/4 v1, 0x0

    #@45
    .local v1, i:I
    :goto_45
    if-ge v1, v0, :cond_12c

    #@47
    .line 1530
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v3

    #@4b
    .line 1531
    .local v3, tag:I
    packed-switch v3, :pswitch_data_138

    #@4e
    .line 1572
    :pswitch_4e
    new-instance v4, Landroid/widget/RemoteViews$ActionException;

    #@50
    new-instance v5, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v6, "Tag "

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    const-string v6, " not found"

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-direct {v4, v5}, Landroid/widget/RemoteViews$ActionException;-><init>(Ljava/lang/String;)V

    #@6c
    throw v4

    #@6d
    .line 1517
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #tag:I
    :cond_6d
    invoke-direct {p0, p2}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@70
    .line 1518
    invoke-virtual {p0}, Landroid/widget/RemoteViews;->setNotRoot()V

    #@73
    goto :goto_21

    #@74
    :cond_74
    move v4, v5

    #@75
    .line 1524
    goto :goto_35

    #@76
    .line 1533
    .restart local v0       #count:I
    .restart local v1       #i:I
    .restart local v3       #tag:I
    :pswitch_76
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@78
    new-instance v5, Landroid/widget/RemoteViews$SetOnClickPendingIntent;

    #@7a
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetOnClickPendingIntent;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@7d
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@80
    .line 1529
    :goto_80
    add-int/lit8 v1, v1, 0x1

    #@82
    goto :goto_45

    #@83
    .line 1536
    :pswitch_83
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@85
    new-instance v5, Landroid/widget/RemoteViews$SetDrawableParameters;

    #@87
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetDrawableParameters;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@8a
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8d
    goto :goto_80

    #@8e
    .line 1539
    :pswitch_8e
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@90
    new-instance v5, Landroid/widget/RemoteViews$ReflectionAction;

    #@92
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@95
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@98
    goto :goto_80

    #@99
    .line 1542
    :pswitch_99
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@9b
    new-instance v5, Landroid/widget/RemoteViews$ViewGroupAction;

    #@9d
    iget-object v6, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@9f
    invoke-direct {v5, p0, p1, v6}, Landroid/widget/RemoteViews$ViewGroupAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V

    #@a2
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a5
    goto :goto_80

    #@a6
    .line 1545
    :pswitch_a6
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@a8
    new-instance v5, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;

    #@aa
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@ad
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b0
    goto :goto_80

    #@b1
    .line 1548
    :pswitch_b1
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@b3
    new-instance v5, Landroid/widget/RemoteViews$SetEmptyView;

    #@b5
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetEmptyView;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@b8
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bb
    goto :goto_80

    #@bc
    .line 1551
    :pswitch_bc
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@be
    new-instance v5, Landroid/widget/RemoteViews$SetPendingIntentTemplate;

    #@c0
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetPendingIntentTemplate;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@c3
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c6
    goto :goto_80

    #@c7
    .line 1554
    :pswitch_c7
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@c9
    new-instance v5, Landroid/widget/RemoteViews$SetOnClickFillInIntent;

    #@cb
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetOnClickFillInIntent;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@ce
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d1
    goto :goto_80

    #@d2
    .line 1557
    :pswitch_d2
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@d4
    new-instance v5, Landroid/widget/RemoteViews$SetRemoteViewsAdapterIntent;

    #@d6
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$SetRemoteViewsAdapterIntent;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@d9
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@dc
    goto :goto_80

    #@dd
    .line 1560
    :pswitch_dd
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@df
    new-instance v5, Landroid/widget/RemoteViews$TextViewDrawableAction;

    #@e1
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$TextViewDrawableAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@e4
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e7
    goto :goto_80

    #@e8
    .line 1563
    :pswitch_e8
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@ea
    new-instance v5, Landroid/widget/RemoteViews$TextViewSizeAction;

    #@ec
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$TextViewSizeAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@ef
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f2
    goto :goto_80

    #@f3
    .line 1566
    :pswitch_f3
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@f5
    new-instance v5, Landroid/widget/RemoteViews$ViewPaddingAction;

    #@f7
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$ViewPaddingAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@fa
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@fd
    goto :goto_80

    #@fe
    .line 1569
    :pswitch_fe
    iget-object v4, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@100
    new-instance v5, Landroid/widget/RemoteViews$BitmapReflectionAction;

    #@102
    invoke-direct {v5, p0, p1}, Landroid/widget/RemoteViews$BitmapReflectionAction;-><init>(Landroid/widget/RemoteViews;Landroid/os/Parcel;)V

    #@105
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@108
    goto/16 :goto_80

    #@10a
    .line 1578
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #tag:I
    :cond_10a
    new-instance v4, Landroid/widget/RemoteViews;

    #@10c
    iget-object v5, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@10e
    invoke-direct {v4, p1, v5}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V

    #@111
    iput-object v4, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@113
    .line 1579
    new-instance v4, Landroid/widget/RemoteViews;

    #@115
    iget-object v5, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@117
    invoke-direct {v4, p1, v5}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V

    #@11a
    iput-object v4, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@11c
    .line 1580
    iget-object v4, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@11e
    invoke-virtual {v4}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@121
    move-result-object v4

    #@122
    iput-object v4, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@124
    .line 1581
    iget-object v4, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@126
    invoke-virtual {v4}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@129
    move-result v4

    #@12a
    iput v4, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@12c
    .line 1585
    :cond_12c
    new-instance v4, Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@12e
    invoke-direct {v4, p0, v7}, Landroid/widget/RemoteViews$MemoryUsageCounter;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$1;)V

    #@131
    iput-object v4, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@133
    .line 1586
    invoke-direct {p0}, Landroid/widget/RemoteViews;->recalculateMemoryUsage()V

    #@136
    .line 1587
    return-void

    #@137
    .line 1531
    nop

    #@138
    :pswitch_data_138
    .packed-switch 0x1
        :pswitch_76
        :pswitch_8e
        :pswitch_83
        :pswitch_99
        :pswitch_a6
        :pswitch_b1
        :pswitch_4e
        :pswitch_bc
        :pswitch_c7
        :pswitch_d2
        :pswitch_dd
        :pswitch_fe
        :pswitch_e8
        :pswitch_f3
    .end packed-switch
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;Landroid/widget/RemoteViews$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;Landroid/widget/RemoteViews$BitmapCache;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V
    .registers 6
    .parameter "landscape"
    .parameter "portrait"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1478
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 78
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/RemoteViews;->mUser:Landroid/os/UserHandle;

    #@a
    .line 111
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@d
    .line 125
    iput-object v2, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@f
    .line 126
    iput-object v2, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@11
    .line 135
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@14
    .line 1479
    if-eqz p1, :cond_18

    #@16
    if-nez p2, :cond_20

    #@18
    .line 1480
    :cond_18
    new-instance v0, Ljava/lang/RuntimeException;

    #@1a
    const-string v1, "Both RemoteViews must be non-null"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 1482
    :cond_20
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_36

    #@2e
    .line 1483
    new-instance v0, Ljava/lang/RuntimeException;

    #@30
    const-string v1, "Both RemoteViews must share the same package"

    #@32
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 1485
    :cond_36
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@3c
    .line 1486
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@3f
    move-result v0

    #@40
    iput v0, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@42
    .line 1488
    iput-object p1, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@44
    .line 1489
    iput-object p2, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@46
    .line 1492
    new-instance v0, Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@48
    invoke-direct {v0, p0, v2}, Landroid/widget/RemoteViews$MemoryUsageCounter;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$1;)V

    #@4b
    iput-object v0, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@4d
    .line 1494
    new-instance v0, Landroid/widget/RemoteViews$BitmapCache;

    #@4f
    invoke-direct {v0}, Landroid/widget/RemoteViews$BitmapCache;-><init>()V

    #@52
    iput-object v0, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@54
    .line 1495
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->configureRemoteViewsAsChild(Landroid/widget/RemoteViews;)V

    #@57
    .line 1496
    invoke-direct {p0, p2}, Landroid/widget/RemoteViews;->configureRemoteViewsAsChild(Landroid/widget/RemoteViews;)V

    #@5a
    .line 1498
    invoke-direct {p0}, Landroid/widget/RemoteViews;->recalculateMemoryUsage()V

    #@5d
    .line 1499
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 5
    .parameter "packageName"
    .parameter "layoutId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1452
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 78
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/RemoteViews;->mUser:Landroid/os/UserHandle;

    #@a
    .line 111
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@d
    .line 125
    iput-object v1, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@f
    .line 126
    iput-object v1, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@11
    .line 135
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@14
    .line 1453
    iput-object p1, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@16
    .line 1454
    iput p2, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@18
    .line 1455
    new-instance v0, Landroid/widget/RemoteViews$BitmapCache;

    #@1a
    invoke-direct {v0}, Landroid/widget/RemoteViews$BitmapCache;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@1f
    .line 1458
    new-instance v0, Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@21
    invoke-direct {v0, p0, v1}, Landroid/widget/RemoteViews$MemoryUsageCounter;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$1;)V

    #@24
    iput-object v0, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@26
    .line 1459
    invoke-direct {p0}, Landroid/widget/RemoteViews;->recalculateMemoryUsage()V

    #@29
    .line 1460
    return-void
.end method

.method static synthetic access$100(Landroid/widget/RemoteViews;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-boolean v0, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews$BitmapCache;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->configureRemoteViewsAsChild(Landroid/widget/RemoteViews;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$BitmapCache;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@3
    return-void
.end method

.method private addAction(Landroid/widget/RemoteViews$Action;)V
    .registers 4
    .parameter "a"

    #@0
    .prologue
    .line 1679
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1680
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "RemoteViews specifying separate landscape and portrait layouts cannot be modified. Instead, fully configure the landscape and portrait layouts individually before constructing the combined layout."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1684
    :cond_e
    iget-object v0, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@10
    if-nez v0, :cond_19

    #@12
    .line 1685
    new-instance v0, Ljava/util/ArrayList;

    #@14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@17
    iput-object v0, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@19
    .line 1687
    :cond_19
    iget-object v0, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 1690
    iget-object v0, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@20
    invoke-virtual {p1, v0}, Landroid/widget/RemoteViews$Action;->updateMemoryUsageEstimate(Landroid/widget/RemoteViews$MemoryUsageCounter;)V

    #@23
    .line 1691
    return-void
.end method

.method private configureRemoteViewsAsChild(Landroid/widget/RemoteViews;)V
    .registers 4
    .parameter "rv"

    #@0
    .prologue
    .line 1176
    iget-object v0, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@2
    iget-object v1, p1, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/RemoteViews$BitmapCache;->assimilate(Landroid/widget/RemoteViews$BitmapCache;)V

    #@7
    .line 1177
    iget-object v0, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@9
    invoke-direct {p1, v0}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@c
    .line 1178
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->setNotRoot()V

    #@f
    .line 1179
    return-void
.end method

.method private getRemoteViewsToApply(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 2227
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_15

    #@6
    .line 2228
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@d
    move-result-object v1

    #@e
    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    #@10
    .line 2229
    .local v0, orientation:I
    const/4 v1, 0x2

    #@11
    if-ne v0, v1, :cond_16

    #@13
    .line 2230
    iget-object p0, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@15
    .line 2235
    .end local v0           #orientation:I
    .end local p0
    :cond_15
    :goto_15
    return-object p0

    #@16
    .line 2232
    .restart local v0       #orientation:I
    .restart local p0
    :cond_16
    iget-object p0, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@18
    goto :goto_15
.end method

.method private hasLandscapeAndPortraitLayouts()Z
    .registers 2

    #@0
    .prologue
    .line 1468
    iget-object v0, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private performApply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 8
    .parameter "v"
    .parameter "parent"
    .parameter "handler"

    #@0
    .prologue
    .line 2305
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_1f

    #@4
    .line 2306
    if-nez p3, :cond_8

    #@6
    sget-object p3, Landroid/widget/RemoteViews;->DEFAULT_ON_CLICK_HANDLER:Landroid/widget/RemoteViews$OnClickHandler;

    #@8
    .line 2307
    :cond_8
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v1

    #@e
    .line 2308
    .local v1, count:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v1, :cond_1f

    #@11
    .line 2309
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/RemoteViews$Action;

    #@19
    .line 2310
    .local v0, a:Landroid/widget/RemoteViews$Action;
    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/RemoteViews$Action;->apply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@1c
    .line 2308
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_f

    #@1f
    .line 2313
    .end local v0           #a:Landroid/widget/RemoteViews$Action;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_1f
    return-void
.end method

.method private prepareContext(Landroid/content/Context;)Landroid/content/Context;
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 2317
    iget-object v2, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@2
    .line 2319
    .local v2, packageName:Ljava/lang/String;
    if-eqz v2, :cond_2d

    #@4
    .line 2321
    const/4 v3, 0x4

    #@5
    :try_start_5
    iget-object v4, p0, Landroid/widget/RemoteViews;->mUser:Landroid/os/UserHandle;

    #@7
    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_a} :catch_c

    #@a
    move-result-object v0

    #@b
    .line 2331
    .local v0, c:Landroid/content/Context;
    :goto_b
    return-object v0

    #@c
    .line 2323
    .end local v0           #c:Landroid/content/Context;
    :catch_c
    move-exception v1

    #@d
    .line 2324
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "RemoteViews"

    #@f
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, "Package name "

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    const-string v5, " not found"

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 2325
    move-object v0, p1

    #@2c
    .line 2326
    .restart local v0       #c:Landroid/content/Context;
    goto :goto_b

    #@2d
    .line 2328
    .end local v0           #c:Landroid/content/Context;
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2d
    move-object v0, p1

    #@2e
    .restart local v0       #c:Landroid/content/Context;
    goto :goto_b
.end method

.method private recalculateMemoryUsage()V
    .registers 5

    #@0
    .prologue
    .line 1627
    iget-object v2, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@2
    invoke-virtual {v2}, Landroid/widget/RemoteViews$MemoryUsageCounter;->clear()V

    #@5
    .line 1629
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_34

    #@b
    .line 1631
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@d
    if-eqz v2, :cond_28

    #@f
    .line 1632
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v0

    #@15
    .line 1633
    .local v0, count:I
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    if-ge v1, v0, :cond_28

    #@18
    .line 1634
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/widget/RemoteViews$Action;

    #@20
    iget-object v3, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@22
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews$Action;->updateMemoryUsageEstimate(Landroid/widget/RemoteViews$MemoryUsageCounter;)V

    #@25
    .line 1633
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_16

    #@28
    .line 1637
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_28
    iget-boolean v2, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@2a
    if-eqz v2, :cond_33

    #@2c
    .line 1638
    iget-object v2, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@2e
    iget-object v3, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@30
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews$BitmapCache;->addBitmapMemory(Landroid/widget/RemoteViews$MemoryUsageCounter;)V

    #@33
    .line 1645
    :cond_33
    :goto_33
    return-void

    #@34
    .line 1641
    :cond_34
    iget-object v2, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@36
    iget-object v3, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@38
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->estimateMemoryUsage()I

    #@3b
    move-result v3

    #@3c
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews$MemoryUsageCounter;->increment(I)V

    #@3f
    .line 1642
    iget-object v2, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@41
    iget-object v3, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@43
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->estimateMemoryUsage()I

    #@46
    move-result v3

    #@47
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews$MemoryUsageCounter;->increment(I)V

    #@4a
    .line 1643
    iget-object v2, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@4c
    iget-object v3, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@4e
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews$BitmapCache;->addBitmapMemory(Landroid/widget/RemoteViews$MemoryUsageCounter;)V

    #@51
    goto :goto_33
.end method

.method private setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V
    .registers 5
    .parameter "bitmapCache"

    #@0
    .prologue
    .line 1651
    iput-object p1, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@2
    .line 1652
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_23

    #@8
    .line 1653
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@a
    if-eqz v2, :cond_2d

    #@c
    .line 1654
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v0

    #@12
    .line 1655
    .local v0, count:I
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v0, :cond_2d

    #@15
    .line 1656
    iget-object v2, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/widget/RemoteViews$Action;

    #@1d
    invoke-virtual {v2, p1}, Landroid/widget/RemoteViews$Action;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@20
    .line 1655
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_13

    #@23
    .line 1660
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_23
    iget-object v2, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@25
    invoke-direct {v2, p1}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@28
    .line 1661
    iget-object v2, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@2a
    invoke-direct {v2, p1}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@2d
    .line 1663
    :cond_2d
    return-void
.end method


# virtual methods
.method public addView(ILandroid/widget/RemoteViews;)V
    .registers 4
    .parameter "viewId"
    .parameter "nestedView"

    #@0
    .prologue
    .line 1704
    new-instance v0, Landroid/widget/RemoteViews$ViewGroupAction;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$ViewGroupAction;-><init>(Landroid/widget/RemoteViews;ILandroid/widget/RemoteViews;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1705
    return-void
.end method

.method public apply(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 4
    .parameter "context"
    .parameter "parent"

    #@0
    .prologue
    .line 2250
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;
    .registers 10
    .parameter "context"
    .parameter "parent"
    .parameter "handler"

    #@0
    .prologue
    .line 2255
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->getRemoteViewsToApply(Landroid/content/Context;)Landroid/widget/RemoteViews;

    #@3
    move-result-object v3

    #@4
    .line 2259
    .local v3, rvToApply:Landroid/widget/RemoteViews;
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->prepareContext(Landroid/content/Context;)Landroid/content/Context;

    #@7
    move-result-object v0

    #@8
    .line 2261
    .local v0, c:Landroid/content/Context;
    const-string/jumbo v4, "layout_inflater"

    #@b
    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/view/LayoutInflater;

    #@11
    .line 2264
    .local v1, inflater:Landroid/view/LayoutInflater;
    invoke-virtual {v1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@14
    move-result-object v1

    #@15
    .line 2265
    invoke-virtual {v1, p0}, Landroid/view/LayoutInflater;->setFilter(Landroid/view/LayoutInflater$Filter;)V

    #@18
    .line 2267
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@1b
    move-result v4

    #@1c
    const/4 v5, 0x0

    #@1d
    invoke-virtual {v1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@20
    move-result-object v2

    #@21
    .line 2269
    .local v2, result:Landroid/view/View;
    invoke-direct {v3, v2, p2, p3}, Landroid/widget/RemoteViews;->performApply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@24
    .line 2271
    return-object v2
.end method

.method public clone()Landroid/widget/RemoteViews;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1591
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1592
    .local v0, p:Landroid/os/Parcel;
    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@8
    .line 1593
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@b
    .line 1594
    new-instance v1, Landroid/widget/RemoteViews;

    #@d
    invoke-direct {v1, v0}, Landroid/widget/RemoteViews;-><init>(Landroid/os/Parcel;)V

    #@10
    return-object v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 2344
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public estimateMemoryUsage()I
    .registers 2

    #@0
    .prologue
    .line 1670
    iget-object v0, p0, Landroid/widget/RemoteViews;->mMemoryUsageCounter:Landroid/widget/RemoteViews$MemoryUsageCounter;

    #@2
    invoke-virtual {v0}, Landroid/widget/RemoteViews$MemoryUsageCounter;->getMemoryUsage()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLayoutId()I
    .registers 2

    #@0
    .prologue
    .line 1609
    iget v0, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@2
    return v0
.end method

.method public getPackage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1598
    iget-object v0, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public mergeRemoteViews(Landroid/widget/RemoteViews;)V
    .registers 12
    .parameter "newRv"

    #@0
    .prologue
    .line 240
    if-nez p1, :cond_3

    #@2
    .line 278
    :cond_2
    :goto_2
    return-void

    #@3
    .line 244
    :cond_3
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@6
    move-result-object v1

    #@7
    .line 246
    .local v1, copy:Landroid/widget/RemoteViews;
    new-instance v5, Ljava/util/HashMap;

    #@9
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@c
    .line 247
    .local v5, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/widget/RemoteViews$Action;>;"
    iget-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@e
    if-nez v8, :cond_17

    #@10
    .line 248
    new-instance v8, Ljava/util/ArrayList;

    #@12
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@17
    .line 251
    :cond_17
    iget-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v2

    #@1d
    .line 252
    .local v2, count:I
    const/4 v3, 0x0

    #@1e
    .local v3, i:I
    :goto_1e
    if-ge v3, v2, :cond_32

    #@20
    .line 253
    iget-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/widget/RemoteViews$Action;

    #@28
    .line 254
    .local v0, a:Landroid/widget/RemoteViews$Action;
    invoke-virtual {v0}, Landroid/widget/RemoteViews$Action;->getUniqueKey()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v5, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 252
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_1e

    #@32
    .line 257
    .end local v0           #a:Landroid/widget/RemoteViews$Action;
    :cond_32
    iget-object v7, v1, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@34
    .line 258
    .local v7, newActions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/RemoteViews$Action;>;"
    if-eqz v7, :cond_2

    #@36
    .line 259
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@39
    move-result v2

    #@3a
    .line 260
    const/4 v3, 0x0

    #@3b
    :goto_3b
    if-ge v3, v2, :cond_78

    #@3d
    .line 261
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v0

    #@41
    check-cast v0, Landroid/widget/RemoteViews$Action;

    #@43
    .line 262
    .restart local v0       #a:Landroid/widget/RemoteViews$Action;
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@46
    move-result-object v8

    #@47
    check-cast v8, Landroid/widget/RemoteViews$Action;

    #@49
    invoke-virtual {v8}, Landroid/widget/RemoteViews$Action;->getUniqueKey()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 263
    .local v4, key:Ljava/lang/String;
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@50
    move-result-object v8

    #@51
    check-cast v8, Landroid/widget/RemoteViews$Action;

    #@53
    invoke-virtual {v8}, Landroid/widget/RemoteViews$Action;->mergeBehavior()I

    #@56
    move-result v6

    #@57
    .line 264
    .local v6, mergeBehavior:I
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5a
    move-result v8

    #@5b
    if-eqz v8, :cond_6b

    #@5d
    if-nez v6, :cond_6b

    #@5f
    .line 265
    iget-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@61
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@68
    .line 266
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    .line 270
    :cond_6b
    if-eqz v6, :cond_70

    #@6d
    const/4 v8, 0x1

    #@6e
    if-ne v6, v8, :cond_75

    #@70
    .line 271
    :cond_70
    iget-object v8, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    .line 260
    :cond_75
    add-int/lit8 v3, v3, 0x1

    #@77
    goto :goto_3b

    #@78
    .line 276
    .end local v0           #a:Landroid/widget/RemoteViews$Action;
    .end local v4           #key:Ljava/lang/String;
    .end local v6           #mergeBehavior:I
    :cond_78
    new-instance v8, Landroid/widget/RemoteViews$BitmapCache;

    #@7a
    invoke-direct {v8}, Landroid/widget/RemoteViews$BitmapCache;-><init>()V

    #@7d
    iput-object v8, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@7f
    .line 277
    iget-object v8, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@81
    invoke-direct {p0, v8}, Landroid/widget/RemoteViews;->setBitmapCache(Landroid/widget/RemoteViews$BitmapCache;)V

    #@84
    goto/16 :goto_2
.end method

.method public onLoadClass(Ljava/lang/Class;)Z
    .registers 3
    .parameter "clazz"

    #@0
    .prologue
    .line 2340
    const-class v0, Landroid/widget/RemoteViews$RemoteView;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public reapply(Landroid/content/Context;Landroid/view/View;)V
    .registers 4
    .parameter "context"
    .parameter "v"

    #@0
    .prologue
    .line 2283
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/RemoteViews;->reapply(Landroid/content/Context;Landroid/view/View;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@4
    .line 2284
    return-void
.end method

.method public reapply(Landroid/content/Context;Landroid/view/View;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 7
    .parameter "context"
    .parameter "v"
    .parameter "handler"

    #@0
    .prologue
    .line 2288
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->getRemoteViewsToApply(Landroid/content/Context;)Landroid/widget/RemoteViews;

    #@3
    move-result-object v0

    #@4
    .line 2293
    .local v0, rvToApply:Landroid/widget/RemoteViews;
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1c

    #@a
    .line 2294
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    #@d
    move-result v1

    #@e
    invoke-virtual {v0}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@11
    move-result v2

    #@12
    if-eq v1, v2, :cond_1c

    #@14
    .line 2295
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    const-string v2, "Attempting to re-apply RemoteViews to a view that that does not share the same root layout id."

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 2300
    :cond_1c
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews;->prepareContext(Landroid/content/Context;)Landroid/content/Context;

    #@1f
    .line 2301
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Landroid/view/ViewGroup;

    #@25
    invoke-direct {v0, p2, v1, p3}, Landroid/widget/RemoteViews;->performApply(Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@28
    .line 2302
    return-void
.end method

.method public removeAllViews(I)V
    .registers 4
    .parameter "viewId"

    #@0
    .prologue
    .line 1714
    new-instance v0, Landroid/widget/RemoteViews$ViewGroupAction;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/widget/RemoteViews$ViewGroupAction;-><init>(Landroid/widget/RemoteViews;ILandroid/widget/RemoteViews;)V

    #@6
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@9
    .line 1715
    return-void
.end method

.method public setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2181
    new-instance v0, Landroid/widget/RemoteViews$BitmapReflectionAction;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/widget/RemoteViews$BitmapReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;Landroid/graphics/Bitmap;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 2182
    return-void
.end method

.method public setBoolean(ILjava/lang/String;Z)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2055
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x1

    #@3
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2056
    return-void
.end method

.method public setBundle(ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2192
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/16 v4, 0xd

    #@4
    move-object v1, p0

    #@5
    move v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v5, p3

    #@8
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@b
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@e
    .line 2193
    return-void
.end method

.method public setByte(ILjava/lang/String;B)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2066
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2067
    return-void
.end method

.method public setChar(ILjava/lang/String;C)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2132
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/16 v4, 0x8

    #@4
    invoke-static {p3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@7
    move-result-object v5

    #@8
    move-object v1, p0

    #@9
    move v2, p1

    #@a
    move-object v3, p2

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@e
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@11
    .line 2133
    return-void
.end method

.method public setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2154
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/16 v4, 0xa

    #@4
    move-object v1, p0

    #@5
    move v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v5, p3

    #@8
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@b
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@e
    .line 2155
    return-void
.end method

.method public setChronometer(IJLjava/lang/String;Z)V
    .registers 7
    .parameter "viewId"
    .parameter "base"
    .parameter "format"
    .parameter "started"

    #@0
    .prologue
    .line 1860
    const-string/jumbo v0, "setBase"

    #@3
    invoke-virtual {p0, p1, v0, p2, p3}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    #@6
    .line 1861
    const-string/jumbo v0, "setFormat"

    #@9
    invoke-virtual {p0, p1, v0, p4}, Landroid/widget/RemoteViews;->setString(ILjava/lang/String;Ljava/lang/String;)V

    #@c
    .line 1862
    const-string/jumbo v0, "setStarted"

    #@f
    invoke-virtual {p0, p1, v0, p5}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    #@12
    .line 1863
    return-void
.end method

.method public setContentDescription(ILjava/lang/CharSequence;)V
    .registers 4
    .parameter "viewId"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 2213
    const-string/jumbo v0, "setContentDescription"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V

    #@6
    .line 2214
    return-void
.end method

.method public setDisplayedChild(II)V
    .registers 4
    .parameter "viewId"
    .parameter "childIndex"

    #@0
    .prologue
    .line 1742
    const-string/jumbo v0, "setDisplayedChild"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 1743
    return-void
.end method

.method public setDouble(ILjava/lang/String;D)V
    .registers 11
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2121
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x7

    #@3
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2122
    return-void
.end method

.method public setDrawableParameters(IZIILandroid/graphics/PorterDuff$Mode;I)V
    .registers 15
    .parameter "viewId"
    .parameter "targetBackground"
    .parameter "alpha"
    .parameter "colorFilter"
    .parameter "mode"
    .parameter "level"

    #@0
    .prologue
    .line 1971
    new-instance v0, Landroid/widget/RemoteViews$SetDrawableParameters;

    #@2
    move-object v1, p0

    #@3
    move v2, p1

    #@4
    move v3, p2

    #@5
    move v4, p3

    #@6
    move v5, p4

    #@7
    move-object v6, p5

    #@8
    move v7, p6

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/widget/RemoteViews$SetDrawableParameters;-><init>(Landroid/widget/RemoteViews;IZIILandroid/graphics/PorterDuff$Mode;I)V

    #@c
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@f
    .line 1973
    return-void
.end method

.method public setEmptyView(II)V
    .registers 4
    .parameter "viewId"
    .parameter "emptyViewId"

    #@0
    .prologue
    .line 1842
    new-instance v0, Landroid/widget/RemoteViews$SetEmptyView;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$SetEmptyView;-><init>(Landroid/widget/RemoteViews;II)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1843
    return-void
.end method

.method public setFloat(ILjava/lang/String;F)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2110
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x6

    #@3
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2111
    return-void
.end method

.method public setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    .registers 4
    .parameter "viewId"
    .parameter "bitmap"

    #@0
    .prologue
    .line 1832
    const-string/jumbo v0, "setImageBitmap"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    #@6
    .line 1833
    return-void
.end method

.method public setImageViewResource(II)V
    .registers 4
    .parameter "viewId"
    .parameter "srcId"

    #@0
    .prologue
    .line 1812
    const-string/jumbo v0, "setImageResource"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 1813
    return-void
.end method

.method public setImageViewUri(ILandroid/net/Uri;)V
    .registers 4
    .parameter "viewId"
    .parameter "uri"

    #@0
    .prologue
    .line 1822
    const-string/jumbo v0, "setImageURI"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setUri(ILjava/lang/String;Landroid/net/Uri;)V

    #@6
    .line 1823
    return-void
.end method

.method public setInt(ILjava/lang/String;I)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2088
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x4

    #@3
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2089
    return-void
.end method

.method public setIntent(ILjava/lang/String;Landroid/content/Intent;)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2203
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/16 v4, 0xe

    #@4
    move-object v1, p0

    #@5
    move v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v5, p3

    #@8
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@b
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@e
    .line 2204
    return-void
.end method

.method setIsWidgetCollectionChild(Z)V
    .registers 2
    .parameter "isWidgetCollectionChild"

    #@0
    .prologue
    .line 1620
    iput-boolean p1, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@2
    .line 1621
    return-void
.end method

.method public setLabelFor(II)V
    .registers 4
    .parameter "viewId"
    .parameter "labeledId"

    #@0
    .prologue
    .line 2223
    const-string/jumbo v0, "setLabelFor"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 2224
    return-void
.end method

.method public setLong(ILjava/lang/String;J)V
    .registers 11
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2099
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x5

    #@3
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2100
    return-void
.end method

.method setNotRoot()V
    .registers 2

    #@0
    .prologue
    .line 1182
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@3
    .line 1183
    return-void
.end method

.method public setOnClickFillInIntent(ILandroid/content/Intent;)V
    .registers 4
    .parameter "viewId"
    .parameter "fillInIntent"

    #@0
    .prologue
    .line 1940
    new-instance v0, Landroid/widget/RemoteViews$SetOnClickFillInIntent;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$SetOnClickFillInIntent;-><init>(Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1941
    return-void
.end method

.method public setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    .registers 4
    .parameter "viewId"
    .parameter "pendingIntent"

    #@0
    .prologue
    .line 1901
    new-instance v0, Landroid/widget/RemoteViews$SetOnClickPendingIntent;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$SetOnClickPendingIntent;-><init>(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1902
    return-void
.end method

.method public setPendingIntentTemplate(ILandroid/app/PendingIntent;)V
    .registers 4
    .parameter "viewId"
    .parameter "pendingIntentTemplate"

    #@0
    .prologue
    .line 1917
    new-instance v0, Landroid/widget/RemoteViews$SetPendingIntentTemplate;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$SetPendingIntentTemplate;-><init>(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1918
    return-void
.end method

.method public setProgressBar(IIIZ)V
    .registers 6
    .parameter "viewId"
    .parameter "max"
    .parameter "progress"
    .parameter "indeterminate"

    #@0
    .prologue
    .line 1880
    const-string/jumbo v0, "setIndeterminate"

    #@3
    invoke-virtual {p0, p1, v0, p4}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    #@6
    .line 1881
    if-nez p4, :cond_14

    #@8
    .line 1882
    const-string/jumbo v0, "setMax"

    #@b
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@e
    .line 1883
    const-string/jumbo v0, "setProgress"

    #@11
    invoke-virtual {p0, p1, v0, p3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@14
    .line 1885
    :cond_14
    return-void
.end method

.method public setRelativeScrollPosition(II)V
    .registers 4
    .parameter "viewId"
    .parameter "offset"

    #@0
    .prologue
    .line 2031
    const-string/jumbo v0, "smoothScrollByOffset"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 2032
    return-void
.end method

.method public setRemoteAdapter(IILandroid/content/Intent;)V
    .registers 4
    .parameter "appWidgetId"
    .parameter "viewId"
    .parameter "intent"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1999
    invoke-virtual {p0, p2, p3}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    #@3
    .line 2000
    return-void
.end method

.method public setRemoteAdapter(ILandroid/content/Intent;)V
    .registers 4
    .parameter "viewId"
    .parameter "intent"

    #@0
    .prologue
    .line 2011
    new-instance v0, Landroid/widget/RemoteViews$SetRemoteViewsAdapterIntent;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/widget/RemoteViews$SetRemoteViewsAdapterIntent;-><init>(Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 2012
    return-void
.end method

.method public setScrollPosition(II)V
    .registers 4
    .parameter "viewId"
    .parameter "position"

    #@0
    .prologue
    .line 2021
    const-string/jumbo v0, "smoothScrollToPosition"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 2022
    return-void
.end method

.method public setShort(ILjava/lang/String;S)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2077
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/4 v4, 0x3

    #@3
    invoke-static {p3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@6
    move-result-object v5

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move-object v3, p2

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@d
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@10
    .line 2078
    return-void
.end method

.method public setString(ILjava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2143
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@2
    const/16 v4, 0x9

    #@4
    move-object v1, p0

    #@5
    move v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v5, p3

    #@8
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@b
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@e
    .line 2144
    return-void
.end method

.method public setTextColor(II)V
    .registers 4
    .parameter "viewId"
    .parameter "color"

    #@0
    .prologue
    .line 1983
    const-string/jumbo v0, "setTextColor"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 1984
    return-void
.end method

.method public setTextViewCompoundDrawables(IIIII)V
    .registers 14
    .parameter "viewId"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1787
    new-instance v0, Landroid/widget/RemoteViews$TextViewDrawableAction;

    #@2
    const/4 v3, 0x0

    #@3
    move-object v1, p0

    #@4
    move v2, p1

    #@5
    move v4, p2

    #@6
    move v5, p3

    #@7
    move v6, p4

    #@8
    move v7, p5

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/widget/RemoteViews$TextViewDrawableAction;-><init>(Landroid/widget/RemoteViews;IZIIII)V

    #@c
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@f
    .line 1788
    return-void
.end method

.method public setTextViewCompoundDrawablesRelative(IIIII)V
    .registers 14
    .parameter "viewId"
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    .line 1802
    new-instance v0, Landroid/widget/RemoteViews$TextViewDrawableAction;

    #@2
    const/4 v3, 0x1

    #@3
    move-object v1, p0

    #@4
    move v2, p1

    #@5
    move v4, p2

    #@6
    move v5, p3

    #@7
    move v6, p4

    #@8
    move v7, p5

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/widget/RemoteViews$TextViewDrawableAction;-><init>(Landroid/widget/RemoteViews;IZIIII)V

    #@c
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@f
    .line 1803
    return-void
.end method

.method public setTextViewText(ILjava/lang/CharSequence;)V
    .registers 4
    .parameter "viewId"
    .parameter "text"

    #@0
    .prologue
    .line 1762
    const-string/jumbo v0, "setText"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setCharSequence(ILjava/lang/String;Ljava/lang/CharSequence;)V

    #@6
    .line 1763
    return-void
.end method

.method public setTextViewTextSize(IIF)V
    .registers 5
    .parameter "viewId"
    .parameter "units"
    .parameter "size"

    #@0
    .prologue
    .line 1773
    new-instance v0, Landroid/widget/RemoteViews$TextViewSizeAction;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/widget/RemoteViews$TextViewSizeAction;-><init>(Landroid/widget/RemoteViews;IIF)V

    #@5
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@8
    .line 1774
    return-void
.end method

.method public setUri(ILjava/lang/String;Landroid/net/Uri;)V
    .registers 10
    .parameter "viewId"
    .parameter "methodName"
    .parameter "value"

    #@0
    .prologue
    .line 2166
    invoke-virtual {p3}, Landroid/net/Uri;->getCanonicalUri()Landroid/net/Uri;

    #@3
    move-result-object p3

    #@4
    .line 2167
    new-instance v0, Landroid/widget/RemoteViews$ReflectionAction;

    #@6
    const/16 v4, 0xb

    #@8
    move-object v1, p0

    #@9
    move v2, p1

    #@a
    move-object v3, p2

    #@b
    move-object v5, p3

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/widget/RemoteViews$ReflectionAction;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;ILjava/lang/Object;)V

    #@f
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@12
    .line 2168
    return-void
.end method

.method public setUser(Landroid/os/UserHandle;)V
    .registers 2
    .parameter "user"

    #@0
    .prologue
    .line 1464
    iput-object p1, p0, Landroid/widget/RemoteViews;->mUser:Landroid/os/UserHandle;

    #@2
    .line 1465
    return-void
.end method

.method public setViewPadding(IIIII)V
    .registers 13
    .parameter "viewId"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2044
    new-instance v0, Landroid/widget/RemoteViews$ViewPaddingAction;

    #@2
    move-object v1, p0

    #@3
    move v2, p1

    #@4
    move v3, p2

    #@5
    move v4, p3

    #@6
    move v5, p4

    #@7
    move v6, p5

    #@8
    invoke-direct/range {v0 .. v6}, Landroid/widget/RemoteViews$ViewPaddingAction;-><init>(Landroid/widget/RemoteViews;IIIII)V

    #@b
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@e
    .line 2045
    return-void
.end method

.method public setViewVisibility(II)V
    .registers 4
    .parameter "viewId"
    .parameter "visibility"

    #@0
    .prologue
    .line 1752
    const-string/jumbo v0, "setVisibility"

    #@3
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@6
    .line 1753
    return-void
.end method

.method public showNext(I)V
    .registers 4
    .parameter "viewId"

    #@0
    .prologue
    .line 1723
    new-instance v0, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;

    #@2
    const-string/jumbo v1, "showNext"

    #@5
    invoke-direct {v0, p0, p1, v1}, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    #@8
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@b
    .line 1724
    return-void
.end method

.method public showPrevious(I)V
    .registers 4
    .parameter "viewId"

    #@0
    .prologue
    .line 1732
    new-instance v0, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;

    #@2
    const-string/jumbo v1, "showPrevious"

    #@5
    invoke-direct {v0, p0, p1, v1}, Landroid/widget/RemoteViews$ReflectionActionWithoutParams;-><init>(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    #@8
    invoke-direct {p0, v0}, Landroid/widget/RemoteViews;->addAction(Landroid/widget/RemoteViews$Action;)V

    #@b
    .line 1733
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2348
    invoke-direct {p0}, Landroid/widget/RemoteViews;->hasLandscapeAndPortraitLayouts()Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_47

    #@8
    .line 2349
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 2352
    iget-boolean v5, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@d
    if-eqz v5, :cond_14

    #@f
    .line 2353
    iget-object v5, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@11
    invoke-virtual {v5, p1, p2}, Landroid/widget/RemoteViews$BitmapCache;->writeBitmapsToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 2355
    :cond_14
    iget-object v5, p0, Landroid/widget/RemoteViews;->mPackage:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 2356
    iget v5, p0, Landroid/widget/RemoteViews;->mLayoutId:I

    #@1b
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 2357
    iget-boolean v5, p0, Landroid/widget/RemoteViews;->mIsWidgetCollectionChild:Z

    #@20
    if-eqz v5, :cond_43

    #@22
    :goto_22
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 2359
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@27
    if-eqz v3, :cond_45

    #@29
    .line 2360
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v1

    #@2f
    .line 2364
    .local v1, count:I
    :goto_2f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 2365
    const/4 v2, 0x0

    #@33
    .local v2, i:I
    :goto_33
    if-ge v2, v1, :cond_5d

    #@35
    .line 2366
    iget-object v3, p0, Landroid/widget/RemoteViews;->mActions:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v0

    #@3b
    check-cast v0, Landroid/widget/RemoteViews$Action;

    #@3d
    .line 2367
    .local v0, a:Landroid/widget/RemoteViews$Action;
    invoke-virtual {v0, p1, v4}, Landroid/widget/RemoteViews$Action;->writeToParcel(Landroid/os/Parcel;I)V

    #@40
    .line 2365
    add-int/lit8 v2, v2, 0x1

    #@42
    goto :goto_33

    #@43
    .end local v0           #a:Landroid/widget/RemoteViews$Action;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_43
    move v3, v4

    #@44
    .line 2357
    goto :goto_22

    #@45
    .line 2362
    :cond_45
    const/4 v1, 0x0

    #@46
    .restart local v1       #count:I
    goto :goto_2f

    #@47
    .line 2370
    .end local v1           #count:I
    :cond_47
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    .line 2373
    iget-boolean v3, p0, Landroid/widget/RemoteViews;->mIsRoot:Z

    #@4c
    if-eqz v3, :cond_53

    #@4e
    .line 2374
    iget-object v3, p0, Landroid/widget/RemoteViews;->mBitmapCache:Landroid/widget/RemoteViews$BitmapCache;

    #@50
    invoke-virtual {v3, p1, p2}, Landroid/widget/RemoteViews$BitmapCache;->writeBitmapsToParcel(Landroid/os/Parcel;I)V

    #@53
    .line 2376
    :cond_53
    iget-object v3, p0, Landroid/widget/RemoteViews;->mLandscape:Landroid/widget/RemoteViews;

    #@55
    invoke-virtual {v3, p1, p2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@58
    .line 2377
    iget-object v3, p0, Landroid/widget/RemoteViews;->mPortrait:Landroid/widget/RemoteViews;

    #@5a
    invoke-virtual {v3, p1, p2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@5d
    .line 2379
    :cond_5d
    return-void
.end method
