.class Landroid/widget/Editor$ActionPopupWindow;
.super Landroid/widget/Editor$PinnedPopupWindow;
.source "Editor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionPopupWindow"
.end annotation


# instance fields
.field private POPUP_TEXT_LAYOUT:I

.field private isTextLinkInstalled:Z

.field private mCliptrayCueShower:Ljava/lang/Runnable;

.field private mCliptrayTextView:Landroid/widget/TextView;

.field private mCopyTextView:Landroid/widget/TextView;

.field private mCutTextView:Landroid/widget/TextView;

.field private mPasteTextView:Landroid/widget/TextView;

.field private mReplaceTextView:Landroid/widget/TextView;

.field private mSelectAllTextView:Landroid/widget/TextView;

.field private mTextLinkView:Landroid/widget/TextView;

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 3348
    iput-object p1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct {p0, p1}, Landroid/widget/Editor$PinnedPopupWindow;-><init>(Landroid/widget/Editor;)V

    #@5
    .line 3331
    const v0, 0x10900d7

    #@8
    iput v0, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@a
    .line 3343
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/widget/Editor$ActionPopupWindow;->isTextLinkInstalled:Z

    #@d
    .line 3349
    invoke-static {p1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v0

    #@15
    const v1, 0x105000c

    #@18
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mStatusBarHeight:I

    #@1e
    .line 3350
    return-void
.end method

.method private getPositionYAboveHandle(II)I
    .registers 10
    .parameter "offset"
    .parameter "parentPosition"

    #@0
    .prologue
    .line 3694
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v1

    #@a
    .line 3695
    .local v1, layout:Landroid/text/Layout;
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@d
    move-result v2

    #@e
    .line 3696
    .local v2, line:I
    invoke-virtual {p0, v2}, Landroid/widget/Editor$ActionPopupWindow;->getVerticalLocalPosition(I)I

    #@11
    move-result v5

    #@12
    .line 3697
    .local v5, positionY:I
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@14
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@1b
    move-result v6

    #@1c
    add-int/2addr v5, v6

    #@1d
    .line 3698
    add-int/2addr v5, p2

    #@1e
    .line 3701
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@21
    move-result v4

    #@22
    .line 3702
    .local v4, lineTop:I
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineBottom(I)I

    #@25
    move-result v3

    #@26
    .line 3703
    .local v3, lineBottom:I
    iget-object v6, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@28
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@2b
    move-result v6

    #@2c
    add-int v0, v5, v6

    #@2e
    .line 3704
    .local v0, contentBottom:I
    if-lt v5, v4, :cond_32

    #@30
    if-le v5, v3, :cond_36

    #@32
    :cond_32
    if-lt v0, v4, :cond_3e

    #@34
    if-gt v0, v3, :cond_3e

    #@36
    .line 3707
    :cond_36
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@38
    invoke-static {v6}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@3b
    move-result v6

    #@3c
    if-eqz v6, :cond_3f

    #@3e
    .line 3714
    .end local v5           #positionY:I
    :cond_3e
    :goto_3e
    return v5

    #@3f
    .line 3710
    .restart local v5       #positionY:I
    :cond_3f
    const/4 v5, -0x1

    #@40
    goto :goto_3e
.end method

.method private getPositionYBelowHandle(II)I
    .registers 8
    .parameter "offset"
    .parameter "parentPosition"

    #@0
    .prologue
    .line 3720
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v3, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@d
    move-result v1

    #@e
    .line 3721
    .local v1, line:I
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@10
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@1b
    move-result v2

    #@1c
    .line 3722
    .local v2, positionY:I
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1e
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@25
    move-result v3

    #@26
    add-int/2addr v2, v3

    #@27
    .line 3723
    add-int/2addr v2, p2

    #@28
    .line 3724
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2a
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@35
    move-result-object v3

    #@36
    iget-object v4, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@38
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3b
    move-result-object v4

    #@3c
    iget v4, v4, Landroid/widget/TextView;->mTextSelectHandleRes:I

    #@3e
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@41
    move-result-object v0

    #@42
    .line 3725
    .local v0, handle:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_49

    #@44
    .line 3726
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@47
    move-result v3

    #@48
    add-int/2addr v2, v3

    #@49
    .line 3727
    :cond_49
    return v2
.end method

.method private hideCliptrayOpenCueAfterDelay()V
    .registers 6

    #@0
    .prologue
    .line 3765
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@2
    if-eqz v1, :cond_65

    #@4
    .line 3766
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@6
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/widget/TextView;->canPaste()Z

    #@d
    move-result v0

    #@e
    .line 3767
    .local v0, canPaste:Z
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@10
    iget-object v1, v1, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@12
    if-eqz v1, :cond_65

    #@14
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@16
    iget-object v1, v1, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@18
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@1b
    move-result v1

    #@1c
    const/16 v2, 0x8

    #@1e
    if-ne v1, v2, :cond_65

    #@20
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@22
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Landroid/widget/TextView;->isFocused()Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_65

    #@2c
    .line 3769
    if-eqz v0, :cond_46

    #@2e
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@30
    iget-object v1, v1, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@32
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isCliptraycueShowing()Z

    #@35
    move-result v1

    #@36
    if-nez v1, :cond_46

    #@38
    .line 3770
    const-string v1, "Cliptray_editer"

    #@3a
    const-string v2, "hideCliptrayOpenCueAfterDelay : showCliptraycue"

    #@3c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 3771
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@41
    iget-object v1, v1, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@43
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->showCliptraycue()V

    #@46
    .line 3773
    :cond_46
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayCueShower:Ljava/lang/Runnable;

    #@48
    if-nez v1, :cond_66

    #@4a
    .line 3775
    new-instance v1, Landroid/widget/Editor$ActionPopupWindow$1;

    #@4c
    invoke-direct {v1, p0}, Landroid/widget/Editor$ActionPopupWindow$1;-><init>(Landroid/widget/Editor$ActionPopupWindow;)V

    #@4f
    iput-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayCueShower:Ljava/lang/Runnable;

    #@51
    .line 3789
    :goto_51
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@53
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@56
    move-result-object v1

    #@57
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayCueShower:Ljava/lang/Runnable;

    #@59
    const-wide/16 v3, 0x7d0

    #@5b
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@5e
    .line 3790
    const-string v1, "Cliptray_editer"

    #@60
    const-string v2, "Editor::ActionPopupWindow, hideCliptrayOpenCueAfterDelay(),postDelayed(mCliptrayCueShower,2000)"

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 3793
    .end local v0           #canPaste:Z
    :cond_65
    return-void

    #@66
    .line 3787
    .restart local v0       #canPaste:Z
    :cond_66
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@68
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6b
    move-result-object v1

    #@6c
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayCueShower:Ljava/lang/Runnable;

    #@6e
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@71
    goto :goto_51
.end method


# virtual methods
.method protected clipVertically(I)I
    .registers 10
    .parameter "positionY"

    #@0
    .prologue
    .line 3657
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-nez v6, :cond_45

    #@4
    if-gez p1, :cond_45

    #@6
    .line 3658
    invoke-virtual {p0}, Landroid/widget/Editor$ActionPopupWindow;->getTextOffset()I

    #@9
    move-result v3

    #@a
    .line 3659
    .local v3, offset:I
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@13
    move-result-object v1

    #@14
    .line 3660
    .local v1, layout:Landroid/text/Layout;
    invoke-virtual {v1, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    #@17
    move-result v2

    #@18
    .line 3661
    .local v2, line:I
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineBottom(I)I

    #@1b
    move-result v6

    #@1c
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@1f
    move-result v7

    #@20
    sub-int/2addr v6, v7

    #@21
    add-int/2addr p1, v6

    #@22
    .line 3662
    iget-object v6, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@24
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@27
    move-result v6

    #@28
    add-int/2addr p1, v6

    #@29
    .line 3665
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2b
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@32
    move-result-object v6

    #@33
    iget-object v7, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@35
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@38
    move-result-object v7

    #@39
    iget v7, v7, Landroid/widget/TextView;->mTextSelectHandleRes:I

    #@3b
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v0

    #@3f
    .line 3667
    .local v0, handle:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@42
    move-result v6

    #@43
    add-int/2addr p1, v6

    #@44
    .line 3689
    .end local v0           #handle:Landroid/graphics/drawable/Drawable;
    .end local v1           #layout:Landroid/text/Layout;
    .end local v2           #line:I
    .end local v3           #offset:I
    :cond_44
    :goto_44
    return p1

    #@45
    .line 3668
    :cond_45
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@47
    if-eqz v6, :cond_44

    #@49
    .line 3669
    move v4, p1

    #@4a
    .line 3670
    .local v4, parentPosition:I
    const/4 v5, 0x0

    #@4b
    .line 3671
    .local v5, positionY_temp:I
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@4d
    iget-object v7, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@4f
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Landroid/widget/TextView;->getSelectionStart()I

    #@56
    move-result v7

    #@57
    invoke-static {v6, v7}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@5a
    move-result v6

    #@5b
    if-eqz v6, :cond_8f

    #@5d
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5f
    iget-object v7, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@61
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v7}, Landroid/widget/TextView;->getSelectionEnd()I

    #@68
    move-result v7

    #@69
    invoke-static {v6, v7}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@6c
    move-result v6

    #@6d
    if-eqz v6, :cond_8f

    #@6f
    .line 3672
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@71
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@78
    move-result v6

    #@79
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYAboveHandle(II)I

    #@7c
    move-result v5

    #@7d
    .line 3673
    if-gez v5, :cond_8d

    #@7f
    .line 3674
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@81
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    #@88
    move-result v6

    #@89
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYBelowHandle(II)I

    #@8c
    move-result v5

    #@8d
    .line 3686
    :cond_8d
    :goto_8d
    move p1, v5

    #@8e
    goto :goto_44

    #@8f
    .line 3676
    :cond_8f
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@91
    iget-object v7, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@93
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v7}, Landroid/widget/TextView;->getSelectionStart()I

    #@9a
    move-result v7

    #@9b
    invoke-static {v6, v7}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@9e
    move-result v6

    #@9f
    if-eqz v6, :cond_c0

    #@a1
    .line 3677
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@a3
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@aa
    move-result v6

    #@ab
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYAboveHandle(II)I

    #@ae
    move-result v5

    #@af
    .line 3678
    if-gez v5, :cond_8d

    #@b1
    .line 3679
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@b3
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@b6
    move-result-object v6

    #@b7
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@ba
    move-result v6

    #@bb
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYBelowHandle(II)I

    #@be
    move-result v5

    #@bf
    goto :goto_8d

    #@c0
    .line 3681
    :cond_c0
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c2
    iget-object v7, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c4
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c7
    move-result-object v7

    #@c8
    invoke-virtual {v7}, Landroid/widget/TextView;->getSelectionEnd()I

    #@cb
    move-result v7

    #@cc
    invoke-static {v6, v7}, Landroid/widget/Editor;->access$1400(Landroid/widget/Editor;I)Z

    #@cf
    move-result v6

    #@d0
    if-eqz v6, :cond_e1

    #@d2
    .line 3682
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@d4
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@d7
    move-result-object v6

    #@d8
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    #@db
    move-result v6

    #@dc
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYBelowHandle(II)I

    #@df
    move-result v5

    #@e0
    goto :goto_8d

    #@e1
    .line 3684
    :cond_e1
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@e3
    invoke-static {v6}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@e6
    move-result-object v6

    #@e7
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@ea
    move-result v6

    #@eb
    invoke-direct {p0, v6, v4}, Landroid/widget/Editor$ActionPopupWindow;->getPositionYBelowHandle(II)I

    #@ee
    move-result v5

    #@ef
    goto :goto_8d
.end method

.method protected createPopupWindow()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3355
    new-instance v0, Landroid/widget/PopupWindow;

    #@3
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    const/4 v2, 0x0

    #@e
    const v3, 0x10102c8

    #@11
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@14
    iput-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@16
    .line 3358
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@18
    invoke-static {v0}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_64

    #@1e
    .line 3359
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@20
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    #@23
    .line 3363
    :goto_23
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@25
    if-eqz v0, :cond_2f

    #@27
    .line 3365
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@29
    const v1, 0x1030002

    #@2c
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    #@2f
    .line 3367
    :cond_2f
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@31
    if-eqz v0, :cond_63

    #@33
    .line 3368
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@35
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@37
    if-eqz v0, :cond_63

    #@39
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@3b
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@3d
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_63

    #@43
    .line 3369
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@45
    invoke-static {v0}, Landroid/widget/Editor;->access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@48
    move-result-object v0

    #@49
    if-eqz v0, :cond_6b

    #@4b
    .line 3370
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@4d
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@4f
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@51
    invoke-static {v1}, Landroid/widget/Editor;->access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@54
    move-result-object v1

    #@55
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@58
    .line 3371
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5a
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@5c
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5e
    iget v1, v1, Landroid/widget/Editor;->mClipDataType:I

    #@60
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@63
    .line 3378
    :cond_63
    :goto_63
    return-void

    #@64
    .line 3361
    :cond_64
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@66
    const/4 v1, 0x1

    #@67
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    #@6a
    goto :goto_23

    #@6b
    .line 3373
    :cond_6b
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@6d
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6f
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@71
    invoke-static {v1}, Landroid/widget/Editor;->access$2600(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@74
    move-result-object v1

    #@75
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@78
    .line 3374
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@7a
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@7c
    invoke-interface {v0, v4}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@7f
    goto :goto_63
.end method

.method protected getTextOffset()I
    .registers 3

    #@0
    .prologue
    .line 3647
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    #@9
    move-result v0

    #@a
    iget-object v1, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@13
    move-result v1

    #@14
    add-int/2addr v0, v1

    #@15
    div-int/lit8 v0, v0, 0x2

    #@17
    return v0
.end method

.method protected getVerticalLocalPosition(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 3652
    iget-object v0, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getLineTop(I)I

    #@d
    move-result v0

    #@e
    iget-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@10
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@13
    move-result v1

    #@14
    sub-int/2addr v0, v1

    #@15
    return v0
.end method

.method protected initContentView()V
    .registers 15

    #@0
    .prologue
    const v13, 0x10805d0

    #@3
    const/4 v12, -0x2

    #@4
    const/4 v11, 0x0

    #@5
    .line 3382
    const/4 v5, 0x0

    #@6
    .line 3384
    .local v5, parentContentView:Landroid/view/ViewGroup;
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@8
    if-nez v9, :cond_24

    #@a
    .line 3385
    new-instance v2, Landroid/widget/LinearLayout;

    #@c
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@e
    invoke-static {v9}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@11
    move-result-object v9

    #@12
    invoke-virtual {v9}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@15
    move-result-object v9

    #@16
    invoke-direct {v2, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@19
    .line 3386
    .local v2, linearLayout:Landroid/widget/LinearLayout;
    const/4 v9, 0x0

    #@1a
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@1d
    .line 3387
    iput-object v2, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@1f
    .line 3388
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@21
    invoke-virtual {v9, v13}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    #@24
    .line 3392
    .end local v2           #linearLayout:Landroid/widget/LinearLayout;
    :cond_24
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@26
    invoke-static {v9}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@29
    move-result-object v9

    #@2a
    invoke-virtual {v9}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@2d
    move-result-object v9

    #@2e
    const-string/jumbo v10, "layout_inflater"

    #@31
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Landroid/view/LayoutInflater;

    #@37
    .line 3395
    .local v1, inflater:Landroid/view/LayoutInflater;
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    #@39
    invoke-direct {v8, v12, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@3c
    .line 3398
    .local v8, wrapContent:Landroid/view/ViewGroup$LayoutParams;
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3e
    if-eqz v9, :cond_cc

    #@40
    .line 3400
    const v9, 0x2030004

    #@43
    iput v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@45
    .line 3401
    const v9, 0x2030003

    #@48
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@4b
    move-result-object v5

    #@4c
    .end local v5           #parentContentView:Landroid/view/ViewGroup;
    check-cast v5, Landroid/view/ViewGroup;

    #@4e
    .line 3402
    .restart local v5       #parentContentView:Landroid/view/ViewGroup;
    invoke-virtual {v5, v13}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    #@51
    .line 3404
    const v9, 0x20d0041

    #@54
    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@57
    move-result-object v9

    #@58
    check-cast v9, Landroid/view/ViewGroup;

    #@5a
    iput-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@5c
    .line 3405
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@5e
    if-nez v9, :cond_63

    #@60
    .line 3406
    iput-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@62
    .line 3483
    :cond_62
    :goto_62
    return-void

    #@63
    .line 3411
    :cond_63
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@65
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@68
    move-result-object v9

    #@69
    check-cast v9, Landroid/widget/TextView;

    #@6b
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@6d
    .line 3412
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@6f
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@72
    .line 3413
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@74
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@76
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@79
    .line 3414
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@7b
    const v10, 0x104000d

    #@7e
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@81
    .line 3415
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@83
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@86
    .line 3417
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@88
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@8b
    move-result-object v9

    #@8c
    check-cast v9, Landroid/widget/TextView;

    #@8e
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@90
    .line 3418
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@92
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@95
    .line 3419
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@97
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@99
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@9c
    .line 3420
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@9e
    const v10, 0x1040003

    #@a1
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@a4
    .line 3421
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@a6
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@a9
    .line 3423
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@ab
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@ae
    move-result-object v9

    #@af
    check-cast v9, Landroid/widget/TextView;

    #@b1
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@b3
    .line 3424
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@b5
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@b8
    .line 3425
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@ba
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@bc
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@bf
    .line 3426
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@c1
    const v10, 0x1040001

    #@c4
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@c7
    .line 3427
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@c9
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@cc
    .line 3432
    :cond_cc
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@ce
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@d1
    move-result-object v9

    #@d2
    check-cast v9, Landroid/widget/TextView;

    #@d4
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@d6
    .line 3433
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@d8
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@db
    .line 3434
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@dd
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@df
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@e2
    .line 3435
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@e4
    const v10, 0x104000b

    #@e7
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@ea
    .line 3436
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@ec
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@ef
    .line 3438
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@f1
    if-nez v9, :cond_116

    #@f3
    .line 3439
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@f5
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@f8
    move-result-object v9

    #@f9
    check-cast v9, Landroid/widget/TextView;

    #@fb
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@fd
    .line 3440
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@ff
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@102
    .line 3441
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@104
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@106
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@109
    .line 3442
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@10b
    const v10, 0x10403e9

    #@10e
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@111
    .line 3443
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@113
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@116
    .line 3446
    :cond_116
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@118
    if-eqz v9, :cond_13d

    #@11a
    .line 3447
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@11c
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@11f
    move-result-object v9

    #@120
    check-cast v9, Landroid/widget/TextView;

    #@122
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@124
    .line 3448
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@126
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@129
    .line 3449
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@12b
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@12d
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@130
    .line 3450
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@132
    const v10, 0x20902a0

    #@135
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@138
    .line 3451
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@13a
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@13d
    .line 3454
    :cond_13d
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@13f
    if-eqz v9, :cond_62

    #@141
    .line 3457
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@143
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@146
    move-result-object v9

    #@147
    check-cast v9, Landroid/widget/TextView;

    #@149
    iput-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@14b
    .line 3458
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@14d
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@150
    .line 3459
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@152
    iget-object v10, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@154
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@157
    .line 3460
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@159
    const v10, 0x20902e1

    #@15c
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    #@15f
    .line 3461
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@161
    invoke-virtual {v9, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@164
    .line 3464
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@166
    iget-object v9, v9, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@168
    if-eqz v9, :cond_19f

    #@16a
    .line 3465
    iget-object v9, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@16c
    iget-object v9, v9, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@16e
    invoke-virtual {v9}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    #@171
    move-result-object v3

    #@172
    .line 3466
    .local v3, m:Landroid/view/Menu;
    const/4 v0, 0x0

    #@173
    .local v0, i:I
    :goto_173
    invoke-interface {v3}, Landroid/view/Menu;->size()I

    #@176
    move-result v9

    #@177
    if-ge v0, v9, :cond_19f

    #@179
    .line 3467
    invoke-interface {v3, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    #@17c
    move-result-object v4

    #@17d
    .line 3468
    .local v4, mi:Landroid/view/MenuItem;
    invoke-interface {v4}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    #@180
    move-result-object v6

    #@181
    .line 3469
    .local v6, text:Ljava/lang/CharSequence;
    iget v9, p0, Landroid/widget/Editor$ActionPopupWindow;->POPUP_TEXT_LAYOUT:I

    #@183
    invoke-virtual {v1, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@186
    move-result-object v7

    #@187
    check-cast v7, Landroid/widget/TextView;

    #@189
    .line 3470
    .local v7, tv:Landroid/widget/TextView;
    if-eqz v7, :cond_19c

    #@18b
    .line 3471
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@18e
    .line 3472
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    #@191
    .line 3473
    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@194
    .line 3474
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@197
    .line 3475
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@199
    invoke-virtual {v9, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@19c
    .line 3466
    :cond_19c
    add-int/lit8 v0, v0, 0x1

    #@19e
    goto :goto_173

    #@19f
    .line 3480
    .end local v0           #i:I
    .end local v3           #m:Landroid/view/Menu;
    .end local v4           #mi:Landroid/view/MenuItem;
    .end local v6           #text:Ljava/lang/CharSequence;
    .end local v7           #tv:Landroid/widget/TextView;
    :cond_19f
    iput-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@1a1
    .line 3481
    iget-object v9, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@1a3
    const/4 v10, 0x1

    #@1a4
    invoke-virtual {v9, v10, v11}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    #@1a7
    goto/16 :goto_62
.end method

.method protected measureContent()V
    .registers 8

    #@0
    .prologue
    const/high16 v6, -0x8000

    #@2
    .line 3733
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4
    if-nez v5, :cond_a

    #@6
    .line 3734
    invoke-super {p0}, Landroid/widget/Editor$PinnedPopupWindow;->measureContent()V

    #@9
    .line 3763
    :goto_9
    return-void

    #@a
    .line 3738
    :cond_a
    iget-object v5, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1b
    move-result-object v0

    #@1c
    .line 3739
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1e
    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@21
    move-result v1

    #@22
    .line 3741
    .local v1, horizontalMeasure:I
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@24
    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@27
    move-result v3

    #@28
    .line 3743
    .local v3, verticalMeasure:I
    const/4 v4, 0x0

    #@29
    .line 3745
    .local v4, width:I
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@2b
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2e
    move-result-object v5

    #@2f
    const/4 v6, -0x2

    #@30
    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@32
    .line 3746
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@34
    invoke-virtual {v5, v1, v3}, Landroid/view/ViewGroup;->measure(II)V

    #@37
    .line 3747
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@39
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    #@3c
    move-result v5

    #@3d
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@40
    move-result v4

    #@41
    .line 3751
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@43
    const/high16 v6, 0x4000

    #@45
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@48
    move-result v6

    #@49
    invoke-virtual {v5, v6, v3}, Landroid/view/ViewGroup;->measure(II)V

    #@4c
    .line 3755
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@4e
    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@51
    move-result-object v2

    #@52
    .line 3756
    .local v2, popupBackground:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_81

    #@54
    .line 3757
    iget-object v5, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@56
    invoke-static {v5}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@59
    move-result-object v5

    #@5a
    if-nez v5, :cond_66

    #@5c
    iget-object v5, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5e
    new-instance v6, Landroid/graphics/Rect;

    #@60
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@63
    invoke-static {v5, v6}, Landroid/widget/Editor;->access$2202(Landroid/widget/Editor;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@66
    .line 3758
    :cond_66
    iget-object v5, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@68
    invoke-static {v5}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@6f
    .line 3759
    iget-object v5, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@71
    invoke-static {v5}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@74
    move-result-object v5

    #@75
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@77
    iget-object v6, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@79
    invoke-static {v6}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@7c
    move-result-object v6

    #@7d
    iget v6, v6, Landroid/graphics/Rect;->right:I

    #@7f
    add-int/2addr v5, v6

    #@80
    add-int/2addr v4, v5

    #@81
    .line 3761
    :cond_81
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@83
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@86
    move-result-object v5

    #@87
    iput v4, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@89
    .line 3762
    iget-object v5, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@8b
    invoke-virtual {v5, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@8e
    goto/16 :goto_9
.end method

.method public onClick(Landroid/view/View;)V
    .registers 9
    .parameter "view"

    #@0
    .prologue
    const v6, 0x1020022

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 3570
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@7
    if-ne p1, v2, :cond_112

    #@9
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@b
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Landroid/widget/TextView;->canPaste()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_112

    #@15
    .line 3571
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@17
    if-eqz v2, :cond_1e

    #@19
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1b
    invoke-virtual {v2, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@1e
    .line 3572
    :cond_1e
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@20
    if-eqz v2, :cond_107

    #@22
    .line 3573
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@24
    iget-object v2, v2, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@26
    if-eqz v2, :cond_f5

    #@28
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@2a
    invoke-static {v2}, Landroid/widget/Editor;->access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@2d
    move-result-object v2

    #@2e
    if-eqz v2, :cond_f5

    #@30
    .line 3574
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@33
    move-result-object v2

    #@34
    if-eqz v2, :cond_40

    #@36
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@39
    move-result-object v2

    #@3a
    invoke-interface {v2, v4}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@3d
    move-result v2

    #@3e
    if-nez v2, :cond_4e

    #@40
    .line 3575
    :cond_40
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@42
    iget-object v2, v2, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@44
    invoke-interface {v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->cliptrayPaste()V

    #@47
    .line 3576
    const-string v2, "Editor"

    #@49
    const-string v3, "Editor::onClick,  mClipManager.cliptrayPaste()"

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 3585
    :cond_4e
    :goto_4e
    invoke-virtual {p0}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@51
    .line 3593
    :cond_51
    :goto_51
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@53
    if-eqz v2, :cond_b1

    #@55
    .line 3594
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@57
    if-ne p1, v2, :cond_151

    #@59
    .line 3595
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5b
    invoke-static {v2, v4}, Landroid/widget/Editor;->access$3002(Landroid/widget/Editor;Z)Z

    #@5e
    .line 3596
    const-string v2, "Cliptray_editer"

    #@60
    new-instance v3, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v4, "Editor::ActionPopupWindow ,  mCopyTextView onClick isHideCliptrayOpenCueAfterDelay: "

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    iget-object v4, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@6d
    invoke-static {v4}, Landroid/widget/Editor;->access$3000(Landroid/widget/Editor;)Z

    #@70
    move-result v4

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 3597
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@7e
    invoke-virtual {v2, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@81
    .line 3598
    invoke-direct {p0}, Landroid/widget/Editor$ActionPopupWindow;->hideCliptrayOpenCueAfterDelay()V

    #@84
    .line 3599
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@86
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@89
    move-result-object v2

    #@8a
    const v3, 0x1020021

    #@8d
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@90
    .line 3600
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@92
    invoke-static {v2}, Landroid/widget/Editor;->access$3100(Landroid/widget/Editor;)V

    #@95
    .line 3601
    invoke-virtual {p0}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@98
    .line 3602
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@9a
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@a1
    move-result-object v2

    #@a2
    check-cast v2, Landroid/text/Spannable;

    #@a4
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@a6
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@a9
    move-result-object v3

    #@aa
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionEnd()I

    #@ad
    move-result v3

    #@ae
    invoke-static {v2, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@b1
    .line 3630
    :cond_b1
    :goto_b1
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@b3
    if-eqz v2, :cond_f4

    #@b5
    .line 3631
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@b7
    if-ne p1, v2, :cond_f4

    #@b9
    .line 3632
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@bb
    iget-object v2, v2, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@bd
    if-eqz v2, :cond_f4

    #@bf
    .line 3633
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@c1
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c4
    move-result-object v2

    #@c5
    invoke-virtual {v2}, Landroid/widget/TextView;->isTextSelectable()Z

    #@c8
    move-result v2

    #@c9
    if-nez v2, :cond_ed

    #@cb
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@cd
    iget-boolean v2, v2, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@cf
    if-eqz v2, :cond_ed

    #@d1
    .line 3634
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@d4
    move-result-object v0

    #@d5
    .line 3635
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_ed

    #@d7
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@d9
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@dc
    move-result-object v2

    #@dd
    invoke-virtual {v2}, Landroid/widget/TextView;->isTextEditable()Z

    #@e0
    move-result v2

    #@e1
    if-eqz v2, :cond_ed

    #@e3
    .line 3636
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@e5
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@e8
    move-result-object v2

    #@e9
    const/4 v3, 0x0

    #@ea
    invoke-virtual {v0, v2, v5, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    #@ed
    .line 3639
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_ed
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@ef
    iget-object v2, v2, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@f1
    invoke-interface {v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->showCliptray()V

    #@f4
    .line 3643
    :cond_f4
    return-void

    #@f5
    .line 3579
    :cond_f5
    const-string v2, "Editor"

    #@f7
    const-string v3, "Editor::onClick, mTextView.onTextContextMenuItem"

    #@f9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 3580
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@fe
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@101
    move-result-object v2

    #@102
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@105
    goto/16 :goto_4e

    #@107
    .line 3583
    :cond_107
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@109
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@110
    goto/16 :goto_4e

    #@112
    .line 3586
    :cond_112
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@114
    if-ne p1, v2, :cond_51

    #@116
    .line 3587
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@118
    if-eqz v2, :cond_11f

    #@11a
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@11c
    invoke-virtual {v2, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@11f
    .line 3588
    :cond_11f
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@121
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@124
    move-result-object v2

    #@125
    invoke-virtual {v2}, Landroid/widget/TextView;->getSelectionStart()I

    #@128
    move-result v2

    #@129
    iget-object v3, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@12b
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@12e
    move-result-object v3

    #@12f
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionEnd()I

    #@132
    move-result v3

    #@133
    add-int/2addr v2, v3

    #@134
    div-int/lit8 v1, v2, 0x2

    #@136
    .line 3589
    .local v1, middle:I
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@138
    invoke-virtual {v2}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@13b
    .line 3590
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@13d
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@140
    move-result-object v2

    #@141
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@144
    move-result-object v2

    #@145
    check-cast v2, Landroid/text/Spannable;

    #@147
    invoke-static {v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@14a
    .line 3591
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@14c
    invoke-virtual {v2}, Landroid/widget/Editor;->showSuggestions()V

    #@14f
    goto/16 :goto_51

    #@151
    .line 3603
    .end local v1           #middle:I
    :cond_151
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@153
    if-ne p1, v2, :cond_191

    #@155
    .line 3604
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@157
    invoke-static {v2, v4}, Landroid/widget/Editor;->access$3002(Landroid/widget/Editor;Z)Z

    #@15a
    .line 3605
    const-string v2, "Cliptray_editer"

    #@15c
    new-instance v3, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v4, "Editor::ActionPopupWindow , mCutTextView onClick isHideCliptrayOpenCueAfterDelay: "

    #@163
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v3

    #@167
    iget-object v4, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@169
    invoke-static {v4}, Landroid/widget/Editor;->access$3000(Landroid/widget/Editor;)Z

    #@16c
    move-result v4

    #@16d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@170
    move-result-object v3

    #@171
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v3

    #@175
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@178
    .line 3606
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@17a
    invoke-virtual {v2, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@17d
    .line 3607
    invoke-direct {p0}, Landroid/widget/Editor$ActionPopupWindow;->hideCliptrayOpenCueAfterDelay()V

    #@180
    .line 3608
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@182
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@185
    move-result-object v2

    #@186
    const v3, 0x1020020

    #@189
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@18c
    .line 3609
    invoke-virtual {p0}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@18f
    goto/16 :goto_b1

    #@191
    .line 3610
    :cond_191
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@193
    if-ne p1, v2, :cond_1b3

    #@195
    .line 3611
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@197
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@19a
    move-result-object v2

    #@19b
    const v3, 0x102001f

    #@19e
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@1a1
    .line 3613
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@1a3
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    #@1a6
    .line 3614
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@1a8
    const v3, -0x777778

    #@1ab
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    #@1ae
    .line 3616
    invoke-super {p0}, Landroid/widget/Editor$PinnedPopupWindow;->show()V

    #@1b1
    goto/16 :goto_b1

    #@1b3
    .line 3617
    :cond_1b3
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1b5
    iget-object v2, v2, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@1b7
    if-eqz v2, :cond_1e2

    #@1b9
    if-eqz p1, :cond_1e2

    #@1bb
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@1be
    move-result-object v2

    #@1bf
    if-eqz v2, :cond_1e2

    #@1c1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@1c4
    move-result-object v2

    #@1c5
    instance-of v2, v2, Landroid/view/MenuItem;

    #@1c7
    if-eqz v2, :cond_1e2

    #@1c9
    .line 3618
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1cb
    iget-object v2, v2, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@1cd
    if-eqz v2, :cond_b1

    #@1cf
    .line 3619
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1d1
    iget-object v3, v2, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@1d3
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1d5
    iget-object v4, v2, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@1d7
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@1da
    move-result-object v2

    #@1db
    check-cast v2, Landroid/view/MenuItem;

    #@1dd
    invoke-interface {v3, v4, v2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    #@1e0
    goto/16 :goto_b1

    #@1e2
    .line 3622
    :cond_1e2
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@1e4
    if-ne p1, v2, :cond_b1

    #@1e6
    .line 3623
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1e8
    invoke-virtual {v2, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@1eb
    .line 3624
    iget-object v2, p0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1ed
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1f0
    move-result-object v2

    #@1f1
    const v3, 0x20d003e

    #@1f4
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@1f7
    .line 3625
    invoke-virtual {p0}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@1fa
    goto/16 :goto_b1
.end method

.method public show()V
    .registers 18

    #@0
    .prologue
    .line 3487
    move-object/from16 v0, p0

    #@2
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v15

    #@8
    invoke-virtual {v15}, Landroid/widget/TextView;->canPaste()Z

    #@b
    move-result v5

    #@c
    .line 3488
    .local v5, canPaste:Z
    move-object/from16 v0, p0

    #@e
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@10
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@13
    move-result-object v15

    #@14
    invoke-virtual {v15}, Landroid/widget/TextView;->isSuggestionsEnabled()Z

    #@17
    move-result v15

    #@18
    if-eqz v15, :cond_1e8

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1e
    invoke-static {v15}, Landroid/widget/Editor;->access$2700(Landroid/widget/Editor;)Z

    #@21
    move-result v15

    #@22
    if-eqz v15, :cond_1e8

    #@24
    const/4 v7, 0x1

    #@25
    .line 3490
    .local v7, canSuggest:Z
    :goto_25
    move-object/from16 v0, p0

    #@27
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2c
    move-result-object v15

    #@2d
    invoke-virtual {v15}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@30
    move-result-object v8

    #@31
    .line 3491
    .local v8, cnxt:Landroid/content/Context;
    move-object/from16 v0, p0

    #@33
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@35
    invoke-static {v15}, Landroid/widget/Editor;->access$2800(Landroid/widget/Editor;)Z

    #@38
    move-result v11

    #@39
    .line 3492
    .local v11, isPassword:Z
    move-object/from16 v0, p0

    #@3b
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@3d
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@40
    move-result-object v15

    #@41
    invoke-virtual {v15}, Landroid/widget/TextView;->canCut()Z

    #@44
    move-result v4

    #@45
    .line 3493
    .local v4, canCut:Z
    move-object/from16 v0, p0

    #@47
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@49
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@4c
    move-result-object v15

    #@4d
    invoke-virtual {v15}, Landroid/widget/TextView;->canCopy()Z

    #@50
    move-result v3

    #@51
    .line 3494
    .local v3, canCopy:Z
    if-nez v4, :cond_65

    #@53
    if-nez v3, :cond_65

    #@55
    if-eqz v11, :cond_1eb

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@5b
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5e
    move-result-object v15

    #@5f
    invoke-virtual {v15}, Landroid/widget/TextView;->hasSelection()Z

    #@62
    move-result v15

    #@63
    if-eqz v15, :cond_1eb

    #@65
    :cond_65
    const/4 v6, 0x1

    #@66
    .line 3496
    .local v6, canSelectText:Z
    :goto_66
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@69
    move-result-object v15

    #@6a
    if-eqz v15, :cond_1ee

    #@6c
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6f
    move-result-object v15

    #@70
    const/16 v16, 0x0

    #@72
    invoke-interface/range {v15 .. v16}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@75
    move-result v15

    #@76
    if-eqz v15, :cond_1ee

    #@78
    const/4 v10, 0x1

    #@79
    .line 3499
    .local v10, isMDMEnable:Z
    :goto_79
    if-nez v11, :cond_1f1

    #@7b
    move-object/from16 v0, p0

    #@7d
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@7f
    invoke-static {v15}, Landroid/widget/Editor;->access$2900(Landroid/widget/Editor;)Z

    #@82
    move-result v15

    #@83
    if-nez v15, :cond_1f1

    #@85
    if-nez v10, :cond_1f1

    #@87
    if-eqz v5, :cond_1f1

    #@89
    const/4 v2, 0x1

    #@8a
    .line 3500
    .local v2, canClipTray:Z
    :goto_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@8e
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@91
    move-result-object v15

    #@92
    invoke-virtual {v15}, Landroid/widget/TextView;->getSelectionStart()I

    #@95
    move-result v15

    #@96
    if-nez v15, :cond_1f4

    #@98
    move-object/from16 v0, p0

    #@9a
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@9c
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9f
    move-result-object v15

    #@a0
    invoke-virtual {v15}, Landroid/widget/TextView;->getSelectionEnd()I

    #@a3
    move-result v15

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@a8
    move-object/from16 v16, v0

    #@aa
    invoke-static/range {v16 .. v16}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@ad
    move-result-object v16

    #@ae
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@b1
    move-result-object v16

    #@b2
    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    #@b5
    move-result v16

    #@b6
    move/from16 v0, v16

    #@b8
    if-ne v15, v0, :cond_1f4

    #@ba
    const/4 v9, 0x1

    #@bb
    .line 3503
    .local v9, isEntireTextSelected:Z
    :goto_bb
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@bd
    if-eqz v15, :cond_171

    #@bf
    .line 3505
    move-object/from16 v0, p0

    #@c1
    iget-object v15, v0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@c3
    const v16, 0x20d0040

    #@c6
    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@c9
    move-result-object v13

    #@ca
    check-cast v13, Landroid/widget/HorizontalScrollView;

    #@cc
    .line 3506
    .local v13, scrollView:Landroid/widget/HorizontalScrollView;
    if-eqz v13, :cond_da

    #@ce
    move-object/from16 v0, p0

    #@d0
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@d2
    invoke-static {v15}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@d5
    const/16 v15, 0x11

    #@d7
    invoke-virtual {v13, v15}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    #@da
    .line 3508
    :cond_da
    move-object/from16 v0, p0

    #@dc
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@de
    move-object/from16 v16, v0

    #@e0
    if-eqz v6, :cond_1f7

    #@e2
    const/4 v15, 0x0

    #@e3
    :goto_e3
    move-object/from16 v0, v16

    #@e5
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@e8
    .line 3509
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mCutTextView:Landroid/widget/TextView;

    #@ec
    move-object/from16 v16, v0

    #@ee
    if-eqz v4, :cond_1fb

    #@f0
    const/4 v15, 0x0

    #@f1
    :goto_f1
    move-object/from16 v0, v16

    #@f3
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@f6
    .line 3510
    move-object/from16 v0, p0

    #@f8
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mCopyTextView:Landroid/widget/TextView;

    #@fa
    move-object/from16 v16, v0

    #@fc
    if-eqz v3, :cond_1ff

    #@fe
    const/4 v15, 0x0

    #@ff
    :goto_ff
    move-object/from16 v0, v16

    #@101
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@104
    .line 3512
    if-eqz v6, :cond_124

    #@106
    .line 3513
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@10a
    move-object/from16 v16, v0

    #@10c
    if-nez v9, :cond_203

    #@10e
    const/4 v15, 0x1

    #@10f
    :goto_10f
    move-object/from16 v0, v16

    #@111
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setEnabled(Z)V

    #@114
    .line 3514
    move-object/from16 v0, p0

    #@116
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mSelectAllTextView:Landroid/widget/TextView;

    #@118
    move-object/from16 v16, v0

    #@11a
    if-eqz v9, :cond_206

    #@11c
    const v15, -0x777778

    #@11f
    :goto_11f
    move-object/from16 v0, v16

    #@121
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setTextColor(I)V

    #@124
    .line 3518
    :cond_124
    new-instance v14, Landroid/content/Intent;

    #@126
    const-string v15, "com.lge.smarttext.action.SEND"

    #@128
    invoke-direct {v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@12b
    .line 3519
    .local v14, smartTextIntent:Landroid/content/Intent;
    const-string/jumbo v15, "text/plain"

    #@12e
    invoke-virtual {v14, v15}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@131
    .line 3520
    if-eqz v8, :cond_14a

    #@133
    .line 3521
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@136
    move-result-object v12

    #@137
    .line 3522
    .local v12, pm:Landroid/content/pm/PackageManager;
    const/high16 v15, 0x1

    #@139
    invoke-virtual {v12, v14, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@13c
    move-result-object v1

    #@13d
    .line 3523
    .local v1, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_145

    #@13f
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@142
    move-result v15

    #@143
    if-gtz v15, :cond_20a

    #@145
    .line 3524
    :cond_145
    const/4 v15, 0x0

    #@146
    move-object/from16 v0, p0

    #@148
    iput-boolean v15, v0, Landroid/widget/Editor$ActionPopupWindow;->isTextLinkInstalled:Z

    #@14a
    .line 3529
    .end local v1           #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v12           #pm:Landroid/content/pm/PackageManager;
    :cond_14a
    :goto_14a
    move-object/from16 v0, p0

    #@14c
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@14e
    invoke-static {v15}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@151
    move-result v15

    #@152
    if-nez v15, :cond_211

    #@154
    move-object/from16 v0, p0

    #@156
    iget-boolean v15, v0, Landroid/widget/Editor$ActionPopupWindow;->isTextLinkInstalled:Z

    #@158
    if-eqz v15, :cond_211

    #@15a
    if-nez v4, :cond_15e

    #@15c
    if-eqz v3, :cond_211

    #@15e
    :cond_15e
    move-object/from16 v0, p0

    #@160
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@162
    invoke-static {v15}, Landroid/widget/Editor;->access$2900(Landroid/widget/Editor;)Z

    #@165
    move-result v15

    #@166
    if-nez v15, :cond_211

    #@168
    .line 3530
    move-object/from16 v0, p0

    #@16a
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@16c
    const/16 v16, 0x0

    #@16e
    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setVisibility(I)V

    #@171
    .line 3537
    .end local v13           #scrollView:Landroid/widget/HorizontalScrollView;
    .end local v14           #smartTextIntent:Landroid/content/Intent;
    :cond_171
    :goto_171
    move-object/from16 v0, p0

    #@173
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@175
    move-object/from16 v16, v0

    #@177
    if-eqz v5, :cond_222

    #@179
    const/4 v15, 0x0

    #@17a
    :goto_17a
    move-object/from16 v0, v16

    #@17c
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@17f
    .line 3539
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@181
    if-eqz v15, :cond_1d1

    #@183
    .line 3542
    move-object/from16 v0, p0

    #@185
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@187
    if-eqz v15, :cond_197

    #@189
    .line 3543
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mCliptrayTextView:Landroid/widget/TextView;

    #@18d
    move-object/from16 v16, v0

    #@18f
    if-eqz v2, :cond_226

    #@191
    const/4 v15, 0x0

    #@192
    :goto_192
    move-object/from16 v0, v16

    #@194
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@197
    .line 3545
    :cond_197
    move-object/from16 v0, p0

    #@199
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@19b
    iget-object v15, v15, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@19d
    if-eqz v15, :cond_1d1

    #@19f
    .line 3547
    move-object/from16 v0, p0

    #@1a1
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1a3
    invoke-static {v15}, Landroid/widget/Editor;->access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@1a6
    move-result-object v15

    #@1a7
    if-eqz v15, :cond_22a

    #@1a9
    .line 3548
    move-object/from16 v0, p0

    #@1ab
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1ad
    iget-object v15, v15, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1b3
    move-object/from16 v16, v0

    #@1b5
    invoke-static/range {v16 .. v16}, Landroid/widget/Editor;->access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@1b8
    move-result-object v16

    #@1b9
    invoke-interface/range {v15 .. v16}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@1bc
    .line 3549
    move-object/from16 v0, p0

    #@1be
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1c0
    iget-object v15, v15, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@1c6
    move-object/from16 v16, v0

    #@1c8
    move-object/from16 v0, v16

    #@1ca
    iget v0, v0, Landroid/widget/Editor;->mClipDataType:I

    #@1cc
    move/from16 v16, v0

    #@1ce
    invoke-interface/range {v15 .. v16}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@1d1
    .line 3558
    :cond_1d1
    :goto_1d1
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@1d3
    if-nez v15, :cond_24c

    #@1d5
    .line 3559
    move-object/from16 v0, p0

    #@1d7
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->mReplaceTextView:Landroid/widget/TextView;

    #@1d9
    move-object/from16 v16, v0

    #@1db
    if-eqz v7, :cond_249

    #@1dd
    const/4 v15, 0x0

    #@1de
    :goto_1de
    move-object/from16 v0, v16

    #@1e0
    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setVisibility(I)V

    #@1e3
    .line 3560
    if-nez v5, :cond_250

    #@1e5
    if-nez v7, :cond_250

    #@1e7
    .line 3566
    :cond_1e7
    :goto_1e7
    return-void

    #@1e8
    .line 3488
    .end local v2           #canClipTray:Z
    .end local v3           #canCopy:Z
    .end local v4           #canCut:Z
    .end local v6           #canSelectText:Z
    .end local v7           #canSuggest:Z
    .end local v8           #cnxt:Landroid/content/Context;
    .end local v9           #isEntireTextSelected:Z
    .end local v10           #isMDMEnable:Z
    .end local v11           #isPassword:Z
    :cond_1e8
    const/4 v7, 0x0

    #@1e9
    goto/16 :goto_25

    #@1eb
    .line 3494
    .restart local v3       #canCopy:Z
    .restart local v4       #canCut:Z
    .restart local v7       #canSuggest:Z
    .restart local v8       #cnxt:Landroid/content/Context;
    .restart local v11       #isPassword:Z
    :cond_1eb
    const/4 v6, 0x0

    #@1ec
    goto/16 :goto_66

    #@1ee
    .line 3496
    .restart local v6       #canSelectText:Z
    :cond_1ee
    const/4 v10, 0x0

    #@1ef
    goto/16 :goto_79

    #@1f1
    .line 3499
    .restart local v10       #isMDMEnable:Z
    :cond_1f1
    const/4 v2, 0x0

    #@1f2
    goto/16 :goto_8a

    #@1f4
    .line 3500
    .restart local v2       #canClipTray:Z
    :cond_1f4
    const/4 v9, 0x0

    #@1f5
    goto/16 :goto_bb

    #@1f7
    .line 3508
    .restart local v9       #isEntireTextSelected:Z
    .restart local v13       #scrollView:Landroid/widget/HorizontalScrollView;
    :cond_1f7
    const/16 v15, 0x8

    #@1f9
    goto/16 :goto_e3

    #@1fb
    .line 3509
    :cond_1fb
    const/16 v15, 0x8

    #@1fd
    goto/16 :goto_f1

    #@1ff
    .line 3510
    :cond_1ff
    const/16 v15, 0x8

    #@201
    goto/16 :goto_ff

    #@203
    .line 3513
    :cond_203
    const/4 v15, 0x0

    #@204
    goto/16 :goto_10f

    #@206
    .line 3514
    :cond_206
    const/high16 v15, -0x100

    #@208
    goto/16 :goto_11f

    #@20a
    .line 3526
    .restart local v1       #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v12       #pm:Landroid/content/pm/PackageManager;
    .restart local v14       #smartTextIntent:Landroid/content/Intent;
    :cond_20a
    const/4 v15, 0x1

    #@20b
    move-object/from16 v0, p0

    #@20d
    iput-boolean v15, v0, Landroid/widget/Editor$ActionPopupWindow;->isTextLinkInstalled:Z

    #@20f
    goto/16 :goto_14a

    #@211
    .line 3531
    .end local v1           #activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v12           #pm:Landroid/content/pm/PackageManager;
    :cond_211
    move-object/from16 v0, p0

    #@213
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@215
    if-eqz v15, :cond_171

    #@217
    .line 3532
    move-object/from16 v0, p0

    #@219
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->mTextLinkView:Landroid/widget/TextView;

    #@21b
    const/16 v16, 0x8

    #@21d
    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setVisibility(I)V

    #@220
    goto/16 :goto_171

    #@222
    .line 3537
    .end local v13           #scrollView:Landroid/widget/HorizontalScrollView;
    .end local v14           #smartTextIntent:Landroid/content/Intent;
    :cond_222
    const/16 v15, 0x8

    #@224
    goto/16 :goto_17a

    #@226
    .line 3543
    :cond_226
    const/16 v15, 0x8

    #@228
    goto/16 :goto_192

    #@22a
    .line 3551
    :cond_22a
    move-object/from16 v0, p0

    #@22c
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@22e
    iget-object v15, v15, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@230
    move-object/from16 v0, p0

    #@232
    iget-object v0, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@234
    move-object/from16 v16, v0

    #@236
    invoke-static/range {v16 .. v16}, Landroid/widget/Editor;->access$2600(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@239
    move-result-object v16

    #@23a
    invoke-interface/range {v15 .. v16}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@23d
    .line 3552
    move-object/from16 v0, p0

    #@23f
    iget-object v15, v0, Landroid/widget/Editor$ActionPopupWindow;->this$0:Landroid/widget/Editor;

    #@241
    iget-object v15, v15, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@243
    const/16 v16, 0x0

    #@245
    invoke-interface/range {v15 .. v16}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@248
    goto :goto_1d1

    #@249
    .line 3559
    :cond_249
    const/16 v15, 0x8

    #@24b
    goto :goto_1de

    #@24c
    .line 3562
    :cond_24c
    if-nez v5, :cond_250

    #@24e
    if-eqz v6, :cond_1e7

    #@250
    .line 3565
    :cond_250
    invoke-super/range {p0 .. p0}, Landroid/widget/Editor$PinnedPopupWindow;->show()V

    #@253
    goto :goto_1e7
.end method
