.class public abstract Landroid/widget/AbsSeekBar;
.super Landroid/widget/ProgressBar;
.source "AbsSeekBar.java"


# static fields
.field private static final NO_ALPHA:I = 0xff


# instance fields
.field private mDisabledAlpha:F

.field private mIsDragging:Z

.field mIsUserSeekable:Z

.field private mKeyProgressIncrement:I

.field private mScaledTouchSlop:I

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mThumbOffset:I

.field private mTouchDownX:F

.field mTouchProgressOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 61
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    #@4
    .line 45
    iput-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@6
    .line 51
    iput v0, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@8
    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 45
    iput-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@6
    .line 51
    iput v0, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@8
    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 45
    iput-boolean v5, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@7
    .line 51
    iput v5, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@9
    .line 71
    sget-object v3, Lcom/android/internal/R$styleable;->SeekBar:[I

    #@b
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@e
    move-result-object v0

    #@f
    .line 73
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@12
    move-result-object v1

    #@13
    .line 74
    .local v1, thumb:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v1}, Landroid/widget/AbsSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    #@16
    .line 76
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getThumbOffset()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@1d
    move-result v2

    #@1e
    .line 78
    .local v2, thumbOffset:I
    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->setThumbOffset(I)V

    #@21
    .line 79
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@24
    .line 81
    sget-object v3, Lcom/android/internal/R$styleable;->Theme:[I

    #@26
    invoke-virtual {p1, p2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@29
    move-result-object v0

    #@2a
    .line 83
    const/4 v3, 0x3

    #@2b
    const/high16 v4, 0x3f00

    #@2d
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@30
    move-result v3

    #@31
    iput v3, p0, Landroid/widget/AbsSeekBar;->mDisabledAlpha:F

    #@33
    .line 84
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@36
    .line 86
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@3d
    move-result v3

    #@3e
    iput v3, p0, Landroid/widget/AbsSeekBar;->mScaledTouchSlop:I

    #@40
    .line 87
    return-void
.end method

.method private attemptClaimDrag()V
    .registers 3

    #@0
    .prologue
    .line 459
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 460
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    const/4 v1, 0x1

    #@7
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@a
    .line 462
    :cond_a
    return-void
.end method

.method private setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V
    .registers 15
    .parameter "w"
    .parameter "thumb"
    .parameter "scale"
    .parameter "gap"

    #@0
    .prologue
    .line 287
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@2
    sub-int v8, p1, v8

    #@4
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@6
    sub-int v0, v8, v9

    #@8
    .line 288
    .local v0, available:I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@b
    move-result v6

    #@c
    .line 289
    .local v6, thumbWidth:I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@f
    move-result v4

    #@10
    .line 290
    .local v4, thumbHeight:I
    sub-int/2addr v0, v6

    #@11
    .line 293
    iget v8, p0, Landroid/widget/AbsSeekBar;->mThumbOffset:I

    #@13
    mul-int/lit8 v8, v8, 0x2

    #@15
    add-int/2addr v0, v8

    #@16
    .line 295
    int-to-float v8, v0

    #@17
    mul-float/2addr v8, p3

    #@18
    float-to-int v5, v8

    #@19
    .line 298
    .local v5, thumbPos:I
    const/high16 v8, -0x8000

    #@1b
    if-ne p4, v8, :cond_33

    #@1d
    .line 299
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@20
    move-result-object v3

    #@21
    .line 300
    .local v3, oldBounds:Landroid/graphics/Rect;
    iget v7, v3, Landroid/graphics/Rect;->top:I

    #@23
    .line 301
    .local v7, topBound:I
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    #@25
    .line 308
    .end local v3           #oldBounds:Landroid/graphics/Rect;
    .local v1, bottomBound:I
    :goto_25
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isLayoutRtl()Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_37

    #@2b
    sub-int v2, v0, v5

    #@2d
    .line 309
    .local v2, left:I
    :goto_2d
    add-int v8, v2, v6

    #@2f
    invoke-virtual {p2, v2, v7, v8, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@32
    .line 310
    return-void

    #@33
    .line 303
    .end local v1           #bottomBound:I
    .end local v2           #left:I
    .end local v7           #topBound:I
    :cond_33
    move v7, p4

    #@34
    .line 304
    .restart local v7       #topBound:I
    add-int v1, p4, v4

    #@36
    .restart local v1       #bottomBound:I
    goto :goto_25

    #@37
    :cond_37
    move v2, v5

    #@38
    .line 308
    goto :goto_2d
.end method

.method private trackTouchEvent(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter "event"

    #@0
    .prologue
    .line 424
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getWidth()I

    #@3
    move-result v4

    #@4
    .line 425
    .local v4, width:I
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@6
    sub-int v6, v4, v6

    #@8
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    sub-int v0, v6, v7

    #@c
    .line 426
    .local v0, available:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@f
    move-result v6

    #@10
    float-to-int v5, v6

    #@11
    .line 428
    .local v5, x:I
    const/4 v2, 0x0

    #@12
    .line 429
    .local v2, progress:F
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isLayoutRtl()Z

    #@15
    move-result v6

    #@16
    if-eqz v6, :cond_3f

    #@18
    .line 430
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@1a
    sub-int v6, v4, v6

    #@1c
    if-le v5, v6, :cond_2c

    #@1e
    .line 431
    const/4 v3, 0x0

    #@1f
    .line 448
    .local v3, scale:F
    :goto_1f
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@22
    move-result v1

    #@23
    .line 449
    .local v1, max:I
    int-to-float v6, v1

    #@24
    mul-float/2addr v6, v3

    #@25
    add-float/2addr v2, v6

    #@26
    .line 451
    float-to-int v6, v2

    #@27
    const/4 v7, 0x1

    #@28
    invoke-virtual {p0, v6, v7}, Landroid/widget/AbsSeekBar;->setProgress(IZ)V

    #@2b
    .line 452
    return-void

    #@2c
    .line 432
    .end local v1           #max:I
    .end local v3           #scale:F
    :cond_2c
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@2e
    if-ge v5, v6, :cond_33

    #@30
    .line 433
    const/high16 v3, 0x3f80

    #@32
    .restart local v3       #scale:F
    goto :goto_1f

    #@33
    .line 435
    .end local v3           #scale:F
    :cond_33
    sub-int v6, v0, v5

    #@35
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    #@37
    add-int/2addr v6, v7

    #@38
    int-to-float v6, v6

    #@39
    int-to-float v7, v0

    #@3a
    div-float v3, v6, v7

    #@3c
    .line 436
    .restart local v3       #scale:F
    iget v2, p0, Landroid/widget/AbsSeekBar;->mTouchProgressOffset:F

    #@3e
    goto :goto_1f

    #@3f
    .line 439
    .end local v3           #scale:F
    :cond_3f
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@41
    if-ge v5, v6, :cond_45

    #@43
    .line 440
    const/4 v3, 0x0

    #@44
    .restart local v3       #scale:F
    goto :goto_1f

    #@45
    .line 441
    .end local v3           #scale:F
    :cond_45
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@47
    sub-int v6, v4, v6

    #@49
    if-le v5, v6, :cond_4e

    #@4b
    .line 442
    const/high16 v3, 0x3f80

    #@4d
    .restart local v3       #scale:F
    goto :goto_1f

    #@4e
    .line 444
    .end local v3           #scale:F
    :cond_4e
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@50
    sub-int v6, v5, v6

    #@52
    int-to-float v6, v6

    #@53
    int-to-float v7, v0

    #@54
    div-float v3, v6, v7

    #@56
    .line 445
    .restart local v3       #scale:F
    iget v2, p0, Landroid/widget/AbsSeekBar;->mTouchProgressOffset:F

    #@58
    goto :goto_1f
.end method

.method private updateThumbPos(II)V
    .registers 15
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 249
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    #@4
    move-result-object v0

    #@5
    .line 250
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@7
    .line 251
    .local v5, thumb:Landroid/graphics/drawable/Drawable;
    if-nez v5, :cond_45

    #@9
    move v6, v8

    #@a
    .line 254
    .local v6, thumbHeight:I
    :goto_a
    iget v9, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@c
    iget v10, p0, Landroid/view/View;->mPaddingTop:I

    #@e
    sub-int v10, p2, v10

    #@10
    iget v11, p0, Landroid/view/View;->mPaddingBottom:I

    #@12
    sub-int/2addr v10, v11

    #@13
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@16
    move-result v7

    #@17
    .line 256
    .local v7, trackHeight:I
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@1a
    move-result v3

    #@1b
    .line 257
    .local v3, max:I
    if-lez v3, :cond_4a

    #@1d
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgress()I

    #@20
    move-result v9

    #@21
    int-to-float v9, v9

    #@22
    int-to-float v10, v3

    #@23
    div-float v4, v9, v10

    #@25
    .line 259
    .local v4, scale:F
    :goto_25
    if-le v6, v7, :cond_4c

    #@27
    .line 260
    if-eqz v5, :cond_2c

    #@29
    .line 261
    invoke-direct {p0, p1, v5, v4, v8}, Landroid/widget/AbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    #@2c
    .line 263
    :cond_2c
    sub-int v9, v6, v7

    #@2e
    div-int/lit8 v2, v9, 0x2

    #@30
    .line 264
    .local v2, gapForCenteringTrack:I
    if-eqz v0, :cond_44

    #@32
    .line 266
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@34
    sub-int v9, p1, v9

    #@36
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    #@38
    sub-int/2addr v9, v10

    #@39
    iget v10, p0, Landroid/view/View;->mPaddingBottom:I

    #@3b
    sub-int v10, p2, v10

    #@3d
    sub-int/2addr v10, v2

    #@3e
    iget v11, p0, Landroid/view/View;->mPaddingTop:I

    #@40
    sub-int/2addr v10, v11

    #@41
    invoke-virtual {v0, v8, v2, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@44
    .line 281
    .end local v2           #gapForCenteringTrack:I
    :cond_44
    :goto_44
    return-void

    #@45
    .line 251
    .end local v3           #max:I
    .end local v4           #scale:F
    .end local v6           #thumbHeight:I
    .end local v7           #trackHeight:I
    :cond_45
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@48
    move-result v6

    #@49
    goto :goto_a

    #@4a
    .line 257
    .restart local v3       #max:I
    .restart local v6       #thumbHeight:I
    .restart local v7       #trackHeight:I
    :cond_4a
    const/4 v4, 0x0

    #@4b
    goto :goto_25

    #@4c
    .line 271
    .restart local v4       #scale:F
    :cond_4c
    if-eqz v0, :cond_5f

    #@4e
    .line 273
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@50
    sub-int v9, p1, v9

    #@52
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    #@54
    sub-int/2addr v9, v10

    #@55
    iget v10, p0, Landroid/view/View;->mPaddingBottom:I

    #@57
    sub-int v10, p2, v10

    #@59
    iget v11, p0, Landroid/view/View;->mPaddingTop:I

    #@5b
    sub-int/2addr v10, v11

    #@5c
    invoke-virtual {v0, v8, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@5f
    .line 276
    :cond_5f
    sub-int v8, v7, v6

    #@61
    div-int/lit8 v1, v8, 0x2

    #@63
    .line 277
    .local v1, gap:I
    if-eqz v5, :cond_44

    #@65
    .line 278
    invoke-direct {p0, p1, v5, v4, v1}, Landroid/widget/AbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    #@68
    goto :goto_44
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 5

    #@0
    .prologue
    .line 213
    invoke-super {p0}, Landroid/widget/ProgressBar;->drawableStateChanged()V

    #@3
    .line 215
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    #@6
    move-result-object v0

    #@7
    .line 216
    .local v0, progressDrawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_14

    #@9
    .line 217
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isEnabled()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_2a

    #@f
    const/16 v2, 0xff

    #@11
    :goto_11
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@14
    .line 220
    :cond_14
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v2, :cond_29

    #@18
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@1a
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_29

    #@20
    .line 221
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getDrawableState()[I

    #@23
    move-result-object v1

    #@24
    .line 222
    .local v1, state:[I
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@29
    .line 224
    .end local v1           #state:[I
    :cond_29
    return-void

    #@2a
    .line 217
    :cond_2a
    const/high16 v2, 0x437f

    #@2c
    iget v3, p0, Landroid/widget/AbsSeekBar;->mDisabledAlpha:F

    #@2e
    mul-float/2addr v2, v3

    #@2f
    float-to-int v2, v2

    #@30
    goto :goto_11
.end method

.method public getKeyProgressIncrement()I
    .registers 2

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@2
    return v0
.end method

.method public getThumb()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getThumbOffset()I
    .registers 2

    #@0
    .prologue
    .line 153
    iget v0, p0, Landroid/widget/AbsSeekBar;->mThumbOffset:I

    #@2
    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 207
    invoke-super {p0}, Landroid/widget/ProgressBar;->jumpDrawablesToCurrentState()V

    #@3
    .line 208
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 209
    :cond_c
    return-void
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 326
    monitor-enter p0

    #@1
    :try_start_1
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    #@4
    .line 327
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_1f

    #@8
    .line 328
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@b
    .line 331
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@d
    iget v1, p0, Landroid/widget/AbsSeekBar;->mThumbOffset:I

    #@f
    sub-int/2addr v0, v1

    #@10
    int-to-float v0, v0

    #@11
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@13
    int-to-float v1, v1

    #@14
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@17
    .line 332
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@1c
    .line 333
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_21

    #@1f
    .line 335
    :cond_1f
    monitor-exit p0

    #@20
    return-void

    #@21
    .line 326
    :catchall_21
    move-exception v0

    #@22
    monitor-exit p0

    #@23
    throw v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 509
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 510
    const-class v0, Landroid/widget/AbsSeekBar;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 511
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 515
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 516
    const-class v1, Landroid/widget/AbsSeekBar;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 518
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isEnabled()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_28

    #@12
    .line 519
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgress()I

    #@15
    move-result v0

    #@16
    .line 520
    .local v0, progress:I
    if-lez v0, :cond_1d

    #@18
    .line 521
    const/16 v1, 0x2000

    #@1a
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@1d
    .line 523
    :cond_1d
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@20
    move-result v1

    #@21
    if-ge v0, v1, :cond_28

    #@23
    .line 524
    const/16 v1, 0x1000

    #@25
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@28
    .line 527
    .end local v0           #progress:I
    :cond_28
    return-void
.end method

.method onKeyChange()V
    .registers 1

    #@0
    .prologue
    .line 483
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 487
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isEnabled()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_e

    #@7
    .line 488
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgress()I

    #@a
    move-result v0

    #@b
    .line 489
    .local v0, progress:I
    packed-switch p1, :pswitch_data_30

    #@e
    .line 504
    .end local v0           #progress:I
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/ProgressBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@11
    move-result v1

    #@12
    :goto_12
    return v1

    #@13
    .line 491
    .restart local v0       #progress:I
    :pswitch_13
    if-lez v0, :cond_e

    #@15
    .line 492
    iget v2, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@17
    sub-int v2, v0, v2

    #@19
    invoke-virtual {p0, v2, v1}, Landroid/widget/AbsSeekBar;->setProgress(IZ)V

    #@1c
    .line 493
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onKeyChange()V

    #@1f
    goto :goto_12

    #@20
    .line 497
    :pswitch_20
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@23
    move-result v2

    #@24
    if-ge v0, v2, :cond_e

    #@26
    .line 498
    iget v2, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@28
    add-int/2addr v2, v0

    #@29
    invoke-virtual {p0, v2, v1}, Landroid/widget/AbsSeekBar;->setProgress(IZ)V

    #@2c
    .line 499
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onKeyChange()V

    #@2f
    goto :goto_12

    #@30
    .line 489
    :pswitch_data_30
    .packed-switch 0x15
        :pswitch_13
        :pswitch_20
    .end packed-switch
.end method

.method protected declared-synchronized onMeasure(II)V
    .registers 10
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 339
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    .line 341
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@8
    if-nez v4, :cond_4d

    #@a
    .line 342
    .local v3, thumbHeight:I
    :goto_a
    const/4 v2, 0x0

    #@b
    .line 343
    .local v2, dw:I
    const/4 v1, 0x0

    #@c
    .line 344
    .local v1, dh:I
    if-eqz v0, :cond_32

    #@e
    .line 345
    iget v4, p0, Landroid/widget/ProgressBar;->mMinWidth:I

    #@10
    iget v5, p0, Landroid/widget/ProgressBar;->mMaxWidth:I

    #@12
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@15
    move-result v6

    #@16
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v5

    #@1a
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@1d
    move-result v2

    #@1e
    .line 346
    iget v4, p0, Landroid/widget/ProgressBar;->mMinHeight:I

    #@20
    iget v5, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@22
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@25
    move-result v6

    #@26
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@29
    move-result v5

    #@2a
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@2d
    move-result v1

    #@2e
    .line 347
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    #@31
    move-result v1

    #@32
    .line 349
    :cond_32
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@34
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@36
    add-int/2addr v4, v5

    #@37
    add-int/2addr v2, v4

    #@38
    .line 350
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@3a
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@3c
    add-int/2addr v4, v5

    #@3d
    add-int/2addr v1, v4

    #@3e
    .line 352
    const/4 v4, 0x0

    #@3f
    invoke-static {v2, p1, v4}, Landroid/widget/AbsSeekBar;->resolveSizeAndState(III)I

    #@42
    move-result v4

    #@43
    const/4 v5, 0x0

    #@44
    invoke-static {v1, p2, v5}, Landroid/widget/AbsSeekBar;->resolveSizeAndState(III)I

    #@47
    move-result v5

    #@48
    invoke-virtual {p0, v4, v5}, Landroid/widget/AbsSeekBar;->setMeasuredDimension(II)V
    :try_end_4b
    .catchall {:try_start_2 .. :try_end_4b} :catchall_54

    #@4b
    .line 354
    monitor-exit p0

    #@4c
    return-void

    #@4d
    .line 341
    .end local v1           #dh:I
    .end local v2           #dw:I
    .end local v3           #thumbHeight:I
    :cond_4d
    :try_start_4d
    iget-object v4, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@4f
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
    :try_end_52
    .catchall {:try_start_4d .. :try_end_52} :catchall_54

    #@52
    move-result v3

    #@53
    goto :goto_a

    #@54
    .line 339
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    :catchall_54
    move-exception v4

    #@55
    monitor-exit p0

    #@56
    throw v4
.end method

.method onProgressRefresh(FZ)V
    .registers 6
    .parameter "scale"
    .parameter "fromUser"

    #@0
    .prologue
    .line 228
    invoke-super {p0, p1, p2}, Landroid/widget/ProgressBar;->onProgressRefresh(FZ)V

    #@3
    .line 229
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@5
    .line 230
    .local v0, thumb:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_13

    #@7
    .line 231
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getWidth()I

    #@a
    move-result v1

    #@b
    const/high16 v2, -0x8000

    #@d
    invoke-direct {p0, v1, v0, p1, v2}, Landroid/widget/AbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    #@10
    .line 237
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@13
    .line 239
    :cond_13
    return-void
.end method

.method public onResolveDrawables(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 317
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onResolveDrawables(I)V

    #@3
    .line 319
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 320
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@c
    .line 322
    :cond_c
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 7
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 562
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onRtlPropertiesChanged(I)V

    #@3
    .line 564
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@6
    move-result v0

    #@7
    .line 565
    .local v0, max:I
    if-lez v0, :cond_22

    #@9
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgress()I

    #@c
    move-result v3

    #@d
    int-to-float v3, v3

    #@e
    int-to-float v4, v0

    #@f
    div-float v1, v3, v4

    #@11
    .line 567
    .local v1, scale:F
    :goto_11
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@13
    .line 568
    .local v2, thumb:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_21

    #@15
    .line 569
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getWidth()I

    #@18
    move-result v3

    #@19
    const/high16 v4, -0x8000

    #@1b
    invoke-direct {p0, v3, v2, v1, v4}, Landroid/widget/AbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    #@1e
    .line 575
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@21
    .line 577
    :cond_21
    return-void

    #@22
    .line 565
    .end local v1           #scale:F
    .end local v2           #thumb:Landroid/graphics/drawable/Drawable;
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_11
.end method

.method protected onSizeChanged(IIII)V
    .registers 5
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 244
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ProgressBar;->onSizeChanged(IIII)V

    #@3
    .line 245
    invoke-direct {p0, p1, p2}, Landroid/widget/AbsSeekBar;->updateThumbPos(II)V

    #@6
    .line 246
    return-void
.end method

.method onStartTrackingTouch()V
    .registers 2

    #@0
    .prologue
    .line 468
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsDragging:Z

    #@3
    .line 469
    return-void
.end method

.method onStopTrackingTouch()V
    .registers 2

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/AbsSeekBar;->mIsDragging:Z

    #@3
    .line 477
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 358
    iget-boolean v3, p0, Landroid/widget/AbsSeekBar;->mIsUserSeekable:Z

    #@4
    if-eqz v3, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isEnabled()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_e

    #@c
    :cond_c
    move v1, v2

    #@d
    .line 420
    :cond_d
    :goto_d
    return v1

    #@e
    .line 362
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@11
    move-result v3

    #@12
    packed-switch v3, :pswitch_data_9c

    #@15
    goto :goto_d

    #@16
    .line 364
    :pswitch_16
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isInScrollingContainer()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_23

    #@1c
    .line 365
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@1f
    move-result v2

    #@20
    iput v2, p0, Landroid/widget/AbsSeekBar;->mTouchDownX:F

    #@22
    goto :goto_d

    #@23
    .line 367
    :cond_23
    invoke-virtual {p0, v1}, Landroid/widget/AbsSeekBar;->setPressed(Z)V

    #@26
    .line 368
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@28
    if-eqz v2, :cond_33

    #@2a
    .line 369
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@2c
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->invalidate(Landroid/graphics/Rect;)V

    #@33
    .line 371
    :cond_33
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStartTrackingTouch()V

    #@36
    .line 372
    invoke-direct {p0, p1}, Landroid/widget/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    #@39
    .line 373
    invoke-direct {p0}, Landroid/widget/AbsSeekBar;->attemptClaimDrag()V

    #@3c
    goto :goto_d

    #@3d
    .line 378
    :pswitch_3d
    iget-boolean v2, p0, Landroid/widget/AbsSeekBar;->mIsDragging:Z

    #@3f
    if-eqz v2, :cond_45

    #@41
    .line 379
    invoke-direct {p0, p1}, Landroid/widget/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    #@44
    goto :goto_d

    #@45
    .line 381
    :cond_45
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@48
    move-result v0

    #@49
    .line 382
    .local v0, x:F
    iget v2, p0, Landroid/widget/AbsSeekBar;->mTouchDownX:F

    #@4b
    sub-float v2, v0, v2

    #@4d
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    #@50
    move-result v2

    #@51
    iget v3, p0, Landroid/widget/AbsSeekBar;->mScaledTouchSlop:I

    #@53
    int-to-float v3, v3

    #@54
    cmpl-float v2, v2, v3

    #@56
    if-lez v2, :cond_d

    #@58
    .line 383
    invoke-virtual {p0, v1}, Landroid/widget/AbsSeekBar;->setPressed(Z)V

    #@5b
    .line 384
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@5d
    if-eqz v2, :cond_68

    #@5f
    .line 385
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@61
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->invalidate(Landroid/graphics/Rect;)V

    #@68
    .line 387
    :cond_68
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStartTrackingTouch()V

    #@6b
    .line 388
    invoke-direct {p0, p1}, Landroid/widget/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    #@6e
    .line 389
    invoke-direct {p0}, Landroid/widget/AbsSeekBar;->attemptClaimDrag()V

    #@71
    goto :goto_d

    #@72
    .line 395
    .end local v0           #x:F
    :pswitch_72
    iget-boolean v3, p0, Landroid/widget/AbsSeekBar;->mIsDragging:Z

    #@74
    if-eqz v3, :cond_83

    #@76
    .line 396
    invoke-direct {p0, p1}, Landroid/widget/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    #@79
    .line 397
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStopTrackingTouch()V

    #@7c
    .line 398
    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->setPressed(Z)V

    #@7f
    .line 409
    :goto_7f
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@82
    goto :goto_d

    #@83
    .line 402
    :cond_83
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStartTrackingTouch()V

    #@86
    .line 403
    invoke-direct {p0, p1}, Landroid/widget/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    #@89
    .line 404
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStopTrackingTouch()V

    #@8c
    goto :goto_7f

    #@8d
    .line 413
    :pswitch_8d
    iget-boolean v3, p0, Landroid/widget/AbsSeekBar;->mIsDragging:Z

    #@8f
    if-eqz v3, :cond_97

    #@91
    .line 414
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onStopTrackingTouch()V

    #@94
    .line 415
    invoke-virtual {p0, v2}, Landroid/widget/AbsSeekBar;->setPressed(Z)V

    #@97
    .line 417
    :cond_97
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@9a
    goto/16 :goto_d

    #@9c
    .line 362
    :pswitch_data_9c
    .packed-switch 0x0
        :pswitch_16
        :pswitch_72
        :pswitch_3d
        :pswitch_8d
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 9
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 531
    invoke-super {p0, p1, p2}, Landroid/widget/ProgressBar;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_9

    #@8
    .line 557
    :goto_8
    return v2

    #@9
    .line 534
    :cond_9
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->isEnabled()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_11

    #@f
    move v2, v3

    #@10
    .line 535
    goto :goto_8

    #@11
    .line 537
    :cond_11
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getProgress()I

    #@14
    move-result v1

    #@15
    .line 538
    .local v1, progress:I
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@18
    move-result v4

    #@19
    int-to-float v4, v4

    #@1a
    const/high16 v5, 0x40a0

    #@1c
    div-float/2addr v4, v5

    #@1d
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@20
    move-result v4

    #@21
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    #@24
    move-result v0

    #@25
    .line 539
    .local v0, increment:I
    sparse-switch p1, :sswitch_data_48

    #@28
    move v2, v3

    #@29
    .line 557
    goto :goto_8

    #@2a
    .line 541
    :sswitch_2a
    if-gtz v1, :cond_2e

    #@2c
    move v2, v3

    #@2d
    .line 542
    goto :goto_8

    #@2e
    .line 544
    :cond_2e
    sub-int v3, v1, v0

    #@30
    invoke-virtual {p0, v3, v2}, Landroid/widget/AbsSeekBar;->setProgress(IZ)V

    #@33
    .line 545
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onKeyChange()V

    #@36
    goto :goto_8

    #@37
    .line 549
    :sswitch_37
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@3a
    move-result v4

    #@3b
    if-lt v1, v4, :cond_3f

    #@3d
    move v2, v3

    #@3e
    .line 550
    goto :goto_8

    #@3f
    .line 552
    :cond_3f
    add-int v3, v1, v0

    #@41
    invoke-virtual {p0, v3, v2}, Landroid/widget/AbsSeekBar;->setProgress(IZ)V

    #@44
    .line 553
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->onKeyChange()V

    #@47
    goto :goto_8

    #@48
    .line 539
    :sswitch_data_48
    .sparse-switch
        0x1000 -> :sswitch_37
        0x2000 -> :sswitch_2a
    .end sparse-switch
.end method

.method public setKeyProgressIncrement(I)V
    .registers 2
    .parameter "increment"

    #@0
    .prologue
    .line 174
    if-gez p1, :cond_3

    #@2
    neg-int p1, p1

    #@3
    .end local p1
    :cond_3
    iput p1, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@5
    .line 175
    return-void
.end method

.method public declared-synchronized setMax(I)V
    .registers 5
    .parameter "max"

    #@0
    .prologue
    .line 191
    monitor-enter p0

    #@1
    :try_start_1
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    #@4
    .line 193
    iget v0, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@6
    if-eqz v0, :cond_13

    #@8
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@b
    move-result v0

    #@c
    iget v1, p0, Landroid/widget/AbsSeekBar;->mKeyProgressIncrement:I

    #@e
    div-int/2addr v0, v1

    #@f
    const/16 v1, 0x14

    #@11
    if-le v0, v1, :cond_27

    #@13
    .line 196
    :cond_13
    const/4 v0, 0x1

    #@14
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getMax()I

    #@17
    move-result v1

    #@18
    int-to-float v1, v1

    #@19
    const/high16 v2, 0x41a0

    #@1b
    div-float/2addr v1, v2

    #@1c
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@1f
    move-result v1

    #@20
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@23
    move-result v0

    #@24
    invoke-virtual {p0, v0}, Landroid/widget/AbsSeekBar;->setKeyProgressIncrement(I)V
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    #@27
    .line 198
    :cond_27
    monitor-exit p0

    #@28
    return-void

    #@29
    .line 191
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0

    #@2b
    throw v0
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "thumb"

    #@0
    .prologue
    .line 102
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v2, :cond_68

    #@4
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@6
    if-eq p1, v2, :cond_68

    #@8
    .line 103
    iget-object v2, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@e
    .line 104
    const/4 v0, 0x1

    #@f
    .line 108
    .local v0, needUpdate:Z
    :goto_f
    if-eqz p1, :cond_46

    #@11
    .line 109
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@14
    .line 110
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->canResolveLayoutDirection()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_21

    #@1a
    .line 111
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getLayoutDirection()I

    #@1d
    move-result v2

    #@1e
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@21
    .line 117
    :cond_21
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@24
    move-result v2

    #@25
    div-int/lit8 v2, v2, 0x2

    #@27
    iput v2, p0, Landroid/widget/AbsSeekBar;->mThumbOffset:I

    #@29
    .line 120
    if-eqz v0, :cond_46

    #@2b
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@2e
    move-result v2

    #@2f
    iget-object v3, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@31
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@34
    move-result v3

    #@35
    if-ne v2, v3, :cond_43

    #@37
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@3a
    move-result v2

    #@3b
    iget-object v3, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@3d
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@40
    move-result v3

    #@41
    if-eq v2, v3, :cond_46

    #@43
    .line 123
    :cond_43
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->requestLayout()V

    #@46
    .line 126
    :cond_46
    iput-object p1, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@48
    .line 127
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@4b
    .line 128
    if-eqz v0, :cond_67

    #@4d
    .line 129
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getWidth()I

    #@50
    move-result v2

    #@51
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getHeight()I

    #@54
    move-result v3

    #@55
    invoke-direct {p0, v2, v3}, Landroid/widget/AbsSeekBar;->updateThumbPos(II)V

    #@58
    .line 130
    if-eqz p1, :cond_67

    #@5a
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@5d
    move-result v2

    #@5e
    if-eqz v2, :cond_67

    #@60
    .line 133
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->getDrawableState()[I

    #@63
    move-result-object v1

    #@64
    .line 134
    .local v1, state:[I
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@67
    .line 137
    .end local v1           #state:[I
    :cond_67
    return-void

    #@68
    .line 106
    .end local v0           #needUpdate:Z
    :cond_68
    const/4 v0, 0x0

    #@69
    .restart local v0       #needUpdate:Z
    goto :goto_f
.end method

.method public setThumbOffset(I)V
    .registers 2
    .parameter "thumbOffset"

    #@0
    .prologue
    .line 163
    iput p1, p0, Landroid/widget/AbsSeekBar;->mThumbOffset:I

    #@2
    .line 164
    invoke-virtual {p0}, Landroid/widget/AbsSeekBar;->invalidate()V

    #@5
    .line 165
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/widget/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq p1, v0, :cond_a

    #@4
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
