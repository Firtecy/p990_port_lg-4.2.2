.class Landroid/widget/Scroller$FrameRate;
.super Ljava/lang/Object;
.source "Scroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Scroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FrameRate"
.end annotation


# instance fields
.field public extraTime:I

.field public frameCount:I

.field public frameTotalCount:I

.field final synthetic this$0:Landroid/widget/Scroller;

.field public timePassed:J

.field public timeTotalPassed:J


# direct methods
.method public constructor <init>(Landroid/widget/Scroller;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 333
    iput-object p1, p0, Landroid/widget/Scroller$FrameRate;->this$0:Landroid/widget/Scroller;

    #@5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 334
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@a
    .line 335
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@c
    .line 336
    iput-wide v1, p0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@e
    .line 337
    iput-wide v1, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@10
    .line 338
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@12
    .line 339
    return-void
.end method


# virtual methods
.method public allReset()V
    .registers 4

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 352
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@5
    .line 353
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@7
    .line 354
    iput-wide v1, p0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@9
    .line 355
    iput-wide v1, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@b
    .line 356
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@d
    .line 357
    return-void
.end method

.method public getTimeDiff()F
    .registers 5

    #@0
    .prologue
    .line 360
    iget-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@2
    iget-wide v2, p0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@4
    add-long/2addr v0, v2

    #@5
    iput-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@7
    .line 361
    iget v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@9
    iget v1, p0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@b
    add-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@e
    .line 362
    iget-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@10
    long-to-float v0, v0

    #@11
    iget v1, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    int-to-float v1, v1

    #@16
    div-float/2addr v0, v1

    #@17
    return v0
.end method

.method public reset()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 342
    iput v2, p0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@3
    .line 343
    const-wide/16 v0, 0x0

    #@5
    iput-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@7
    .line 344
    iput v2, p0, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@9
    .line 345
    iget v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@b
    const/16 v1, 0x64

    #@d
    if-le v0, v1, :cond_1e

    #@f
    .line 346
    iget v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@11
    div-int/lit8 v0, v0, 0x2

    #@13
    iput v0, p0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@15
    .line 347
    iget-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@17
    const-wide/16 v2, 0x2

    #@19
    div-long/2addr v0, v2

    #@1a
    long-to-int v0, v0

    #@1b
    int-to-long v0, v0

    #@1c
    iput-wide v0, p0, Landroid/widget/Scroller$FrameRate;->timeTotalPassed:J

    #@1e
    .line 349
    :cond_1e
    return-void
.end method
