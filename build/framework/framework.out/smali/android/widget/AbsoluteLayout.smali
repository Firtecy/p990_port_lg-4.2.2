.class public Landroid/widget/AbsoluteLayout;
.super Landroid/view/ViewGroup;
.source "AbsoluteLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AbsoluteLayout$LayoutParams;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@3
    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 53
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 136
    instance-of v0, p1, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@2
    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x2

    #@2
    .line 103
    new-instance v0, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@4
    invoke-direct {v0, v1, v1, v2, v2}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    #@7
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 130
    new-instance v0, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 141
    new-instance v0, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .registers 14
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getChildCount()I

    #@3
    move-result v3

    #@4
    .line 111
    .local v3, count:I
    const/4 v4, 0x0

    #@5
    .local v4, i:I
    :goto_5
    if-ge v4, v3, :cond_35

    #@7
    .line 112
    invoke-virtual {p0, v4}, Landroid/widget/AbsoluteLayout;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 113
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@e
    move-result v6

    #@f
    const/16 v7, 0x8

    #@11
    if-eq v6, v7, :cond_32

    #@13
    .line 115
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@16
    move-result-object v5

    #@17
    check-cast v5, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@19
    .line 118
    .local v5, lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@1b
    iget v7, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->x:I

    #@1d
    add-int v1, v6, v7

    #@1f
    .line 119
    .local v1, childLeft:I
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@21
    iget v7, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    #@23
    add-int v2, v6, v7

    #@25
    .line 120
    .local v2, childTop:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@28
    move-result v6

    #@29
    add-int/2addr v6, v1

    #@2a
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@2d
    move-result v7

    #@2e
    add-int/2addr v7, v2

    #@2f
    invoke-virtual {v0, v1, v2, v6, v7}, Landroid/view/View;->layout(IIII)V

    #@32
    .line 111
    .end local v1           #childLeft:I
    .end local v2           #childTop:I
    .end local v5           #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_32
    add-int/lit8 v4, v4, 0x1

    #@34
    goto :goto_5

    #@35
    .line 126
    .end local v0           #child:Landroid/view/View;
    :cond_35
    return-void
.end method

.method protected onMeasure(II)V
    .registers 14
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 57
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getChildCount()I

    #@4
    move-result v3

    #@5
    .line 59
    .local v3, count:I
    const/4 v6, 0x0

    #@6
    .line 60
    .local v6, maxHeight:I
    const/4 v7, 0x0

    #@7
    .line 63
    .local v7, maxWidth:I
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->measureChildren(II)V

    #@a
    .line 66
    const/4 v4, 0x0

    #@b
    .local v4, i:I
    :goto_b
    if-ge v4, v3, :cond_3a

    #@d
    .line 67
    invoke-virtual {p0, v4}, Landroid/widget/AbsoluteLayout;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 68
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@14
    move-result v8

    #@15
    const/16 v9, 0x8

    #@17
    if-eq v8, v9, :cond_37

    #@19
    .line 72
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@1f
    .line 75
    .local v5, lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    iget v8, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->x:I

    #@21
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@24
    move-result v9

    #@25
    add-int v2, v8, v9

    #@27
    .line 76
    .local v2, childRight:I
    iget v8, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    #@29
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@2c
    move-result v9

    #@2d
    add-int v1, v8, v9

    #@2f
    .line 78
    .local v1, childBottom:I
    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    #@32
    move-result v7

    #@33
    .line 79
    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    #@36
    move-result v6

    #@37
    .line 66
    .end local v1           #childBottom:I
    .end local v2           #childRight:I
    .end local v5           #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_37
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_b

    #@3a
    .line 84
    .end local v0           #child:Landroid/view/View;
    :cond_3a
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@3c
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@3e
    add-int/2addr v8, v9

    #@3f
    add-int/2addr v7, v8

    #@40
    .line 85
    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    #@42
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@44
    add-int/2addr v8, v9

    #@45
    add-int/2addr v6, v8

    #@46
    .line 88
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getSuggestedMinimumHeight()I

    #@49
    move-result v8

    #@4a
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    #@4d
    move-result v6

    #@4e
    .line 89
    invoke-virtual {p0}, Landroid/widget/AbsoluteLayout;->getSuggestedMinimumWidth()I

    #@51
    move-result v8

    #@52
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@55
    move-result v7

    #@56
    .line 91
    invoke-static {v7, p1, v10}, Landroid/widget/AbsoluteLayout;->resolveSizeAndState(III)I

    #@59
    move-result v8

    #@5a
    invoke-static {v6, p2, v10}, Landroid/widget/AbsoluteLayout;->resolveSizeAndState(III)I

    #@5d
    move-result v9

    #@5e
    invoke-virtual {p0, v8, v9}, Landroid/widget/AbsoluteLayout;->setMeasuredDimension(II)V

    #@61
    .line 93
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 146
    const/4 v0, 0x0

    #@1
    return v0
.end method
