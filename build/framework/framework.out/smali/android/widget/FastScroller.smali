.class Landroid/widget/FastScroller;
.super Ljava/lang/Object;
.source "FastScroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/FastScroller$ScrollFade;
    }
.end annotation


# static fields
.field private static final ATTRS:[I = null

.field private static final DEFAULT_STATES:[I = null

.field private static final FADE_TIMEOUT:I = 0x5dc

.field private static MIN_PAGES:I = 0x0

.field private static final OVERLAY_AT_THUMB:I = 0x1

.field private static final OVERLAY_FLOATING:I = 0x0

.field private static final OVERLAY_POSITION:I = 0x5

.field private static final PENDING_DRAG_DELAY:I = 0xb4

.field private static final PRESSED_STATES:[I = null

.field private static final PREVIEW_BACKGROUND_LEFT:I = 0x3

.field private static final PREVIEW_BACKGROUND_RIGHT:I = 0x4

.field private static final STATE_DRAGGING:I = 0x3

.field private static final STATE_ENTER:I = 0x1

.field private static final STATE_EXIT:I = 0x4

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_VISIBLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FastScroller"

.field private static final TEXT_COLOR:I = 0x0

.field private static final THUMB_DRAWABLE:I = 0x1

.field private static final TRACK_DRAWABLE:I = 0x2


# instance fields
.field private mAlwaysShow:Z

.field private mChangedBounds:Z

.field private final mDeferStartDrag:Ljava/lang/Runnable;

.field private mDrawOverlay:Z

.field private mHandler:Landroid/os/Handler;

.field mInitialTouchY:F

.field private mItemCount:I

.field mList:Landroid/widget/AbsListView;

.field mListAdapter:Landroid/widget/BaseAdapter;

.field private mListOffset:I

.field private mLongList:Z

.field private mMatchDragPosition:Z

.field private mOverlayDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverlayDrawableLeft:Landroid/graphics/drawable/Drawable;

.field private mOverlayDrawableRight:Landroid/graphics/drawable/Drawable;

.field private mOverlayPos:Landroid/graphics/RectF;

.field private mOverlayPosition:I

.field private mOverlaySize:I

.field private mPaint:Landroid/graphics/Paint;

.field mPendingDrag:Z

.field private mPosition:I

.field private mScaledTouchSlop:I

.field mScrollCompleted:Z

.field private mScrollFade:Landroid/widget/FastScroller$ScrollFade;

.field private mSectionIndexer:Landroid/widget/SectionIndexer;

.field private mSectionText:Ljava/lang/String;

.field private mSections:[Ljava/lang/Object;

.field private mState:I

.field private mThumbDrawable:Landroid/graphics/drawable/Drawable;

.field mThumbH:I

.field mThumbW:I

.field mThumbY:I

.field private final mTmpRect:Landroid/graphics/Rect;

.field private mTrackDrawable:Landroid/graphics/drawable/Drawable;

.field private mVisibleItem:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 42
    const/4 v0, 0x4

    #@2
    sput v0, Landroid/widget/FastScroller;->MIN_PAGES:I

    #@4
    .line 54
    const/4 v0, 0x1

    #@5
    new-array v0, v0, [I

    #@7
    const v1, 0x10100a7

    #@a
    aput v1, v0, v2

    #@c
    sput-object v0, Landroid/widget/FastScroller;->PRESSED_STATES:[I

    #@e
    .line 58
    new-array v0, v2, [I

    #@10
    sput-object v0, Landroid/widget/FastScroller;->DEFAULT_STATES:[I

    #@12
    .line 60
    const/4 v0, 0x6

    #@13
    new-array v0, v0, [I

    #@15
    fill-array-data v0, :array_1c

    #@18
    sput-object v0, Landroid/widget/FastScroller;->ATTRS:[I

    #@1a
    return-void

    #@1b
    nop

    #@1c
    :array_1c
    .array-data 0x4
        0x59t 0x3t 0x1t 0x1t
        0x36t 0x3t 0x1t 0x1t
        0x39t 0x3t 0x1t 0x1t
        0x37t 0x3t 0x1t 0x1t
        0x38t 0x3t 0x1t 0x1t
        0x3at 0x3t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;)V
    .registers 4
    .parameter "context"
    .parameter "listView"

    #@0
    .prologue
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 98
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/FastScroller;->mItemCount:I

    #@6
    .line 108
    new-instance v0, Landroid/os/Handler;

    #@8
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@d
    .line 130
    new-instance v0, Landroid/graphics/Rect;

    #@f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12
    iput-object v0, p0, Landroid/widget/FastScroller;->mTmpRect:Landroid/graphics/Rect;

    #@14
    .line 132
    new-instance v0, Landroid/widget/FastScroller$1;

    #@16
    invoke-direct {v0, p0}, Landroid/widget/FastScroller$1;-><init>(Landroid/widget/FastScroller;)V

    #@19
    iput-object v0, p0, Landroid/widget/FastScroller;->mDeferStartDrag:Ljava/lang/Runnable;

    #@1b
    .line 154
    iput-object p2, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1d
    .line 155
    invoke-direct {p0, p1}, Landroid/widget/FastScroller;->init(Landroid/content/Context;)V

    #@20
    .line 156
    return-void
.end method

.method private cancelFling()V
    .registers 10

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 674
    const/4 v4, 0x3

    #@4
    const/4 v7, 0x0

    #@5
    move-wide v2, v0

    #@6
    move v6, v5

    #@7
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@a
    move-result-object v8

    #@b
    .line 675
    .local v8, cancelFling:Landroid/view/MotionEvent;
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@d
    invoke-virtual {v0, v8}, Landroid/widget/AbsListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@10
    .line 676
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    #@13
    .line 677
    return-void
.end method

.method private getThumbPositionForListPosition(III)I
    .registers 20
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    #@0
    .prologue
    .line 631
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@4
    if-eqz v13, :cond_c

    #@6
    move-object/from16 v0, p0

    #@8
    iget-object v13, v0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@a
    if-nez v13, :cond_f

    #@c
    .line 632
    :cond_c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FastScroller;->getSectionsFromIndexer()V

    #@f
    .line 634
    :cond_f
    move-object/from16 v0, p0

    #@11
    iget-object v13, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@13
    if-eqz v13, :cond_1b

    #@15
    move-object/from16 v0, p0

    #@17
    iget-boolean v13, v0, Landroid/widget/FastScroller;->mMatchDragPosition:Z

    #@19
    if-nez v13, :cond_2f

    #@1b
    .line 635
    :cond_1b
    move-object/from16 v0, p0

    #@1d
    iget-object v13, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1f
    invoke-virtual {v13}, Landroid/widget/AbsListView;->getHeight()I

    #@22
    move-result v13

    #@23
    move-object/from16 v0, p0

    #@25
    iget v14, v0, Landroid/widget/FastScroller;->mThumbH:I

    #@27
    sub-int/2addr v13, v14

    #@28
    mul-int v13, v13, p1

    #@2a
    sub-int v14, p3, p2

    #@2c
    div-int v8, v13, v14

    #@2e
    .line 669
    :cond_2e
    :goto_2e
    return v8

    #@2f
    .line 639
    :cond_2f
    move-object/from16 v0, p0

    #@31
    iget v13, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@33
    sub-int p1, p1, v13

    #@35
    .line 640
    if-gez p1, :cond_39

    #@37
    .line 641
    const/4 v8, 0x0

    #@38
    goto :goto_2e

    #@39
    .line 643
    :cond_39
    move-object/from16 v0, p0

    #@3b
    iget v13, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@3d
    sub-int p3, p3, v13

    #@3f
    .line 645
    move-object/from16 v0, p0

    #@41
    iget-object v13, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@43
    invoke-virtual {v13}, Landroid/widget/AbsListView;->getHeight()I

    #@46
    move-result v13

    #@47
    move-object/from16 v0, p0

    #@49
    iget v14, v0, Landroid/widget/FastScroller;->mThumbH:I

    #@4b
    sub-int v12, v13, v14

    #@4d
    .line 647
    .local v12, trackHeight:I
    move-object/from16 v0, p0

    #@4f
    iget-object v13, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@51
    move/from16 v0, p1

    #@53
    invoke-interface {v13, v0}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    #@56
    move-result v9

    #@57
    .line 648
    .local v9, section:I
    move-object/from16 v0, p0

    #@59
    iget-object v13, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@5b
    invoke-interface {v13, v9}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@5e
    move-result v11

    #@5f
    .line 649
    .local v11, sectionPos:I
    move-object/from16 v0, p0

    #@61
    iget-object v13, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@63
    add-int/lit8 v14, v9, 0x1

    #@65
    invoke-interface {v13, v14}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@68
    move-result v5

    #@69
    .line 650
    .local v5, nextSectionPos:I
    move-object/from16 v0, p0

    #@6b
    iget-object v13, v0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@6d
    array-length v10, v13

    #@6e
    .line 651
    .local v10, sectionCount:I
    sub-int v7, v5, v11

    #@70
    .line 653
    .local v7, positionsInSection:I
    move-object/from16 v0, p0

    #@72
    iget-object v13, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@74
    const/4 v14, 0x0

    #@75
    invoke-virtual {v13, v14}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@78
    move-result-object v1

    #@79
    .line 654
    .local v1, child:Landroid/view/View;
    if-nez v1, :cond_c2

    #@7b
    const/4 v2, 0x0

    #@7c
    .line 656
    .local v2, incrementalPos:F
    :goto_7c
    int-to-float v13, v11

    #@7d
    sub-float v13, v2, v13

    #@7f
    int-to-float v14, v7

    #@80
    div-float v6, v13, v14

    #@82
    .line 657
    .local v6, posWithinSection:F
    int-to-float v13, v9

    #@83
    add-float/2addr v13, v6

    #@84
    int-to-float v14, v10

    #@85
    div-float/2addr v13, v14

    #@86
    int-to-float v14, v12

    #@87
    mul-float/2addr v13, v14

    #@88
    float-to-int v8, v13

    #@89
    .line 662
    .local v8, result:I
    if-lez p1, :cond_2e

    #@8b
    add-int v13, p1, p2

    #@8d
    move/from16 v0, p3

    #@8f
    if-ne v13, v0, :cond_2e

    #@91
    .line 663
    move-object/from16 v0, p0

    #@93
    iget-object v13, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@95
    add-int/lit8 v14, p2, -0x1

    #@97
    invoke-virtual {v13, v14}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@9a
    move-result-object v3

    #@9b
    .line 664
    .local v3, lastChild:Landroid/view/View;
    move-object/from16 v0, p0

    #@9d
    iget-object v13, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@9f
    invoke-virtual {v13}, Landroid/widget/AbsListView;->getHeight()I

    #@a2
    move-result v13

    #@a3
    move-object/from16 v0, p0

    #@a5
    iget-object v14, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@a7
    invoke-virtual {v14}, Landroid/widget/AbsListView;->getPaddingBottom()I

    #@aa
    move-result v14

    #@ab
    sub-int/2addr v13, v14

    #@ac
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@af
    move-result v14

    #@b0
    sub-int/2addr v13, v14

    #@b1
    int-to-float v13, v13

    #@b2
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    #@b5
    move-result v14

    #@b6
    int-to-float v14, v14

    #@b7
    div-float v4, v13, v14

    #@b9
    .line 666
    .local v4, lastItemVisible:F
    int-to-float v13, v8

    #@ba
    sub-int v14, v12, v8

    #@bc
    int-to-float v14, v14

    #@bd
    mul-float/2addr v14, v4

    #@be
    add-float/2addr v13, v14

    #@bf
    float-to-int v8, v13

    #@c0
    goto/16 :goto_2e

    #@c2
    .line 654
    .end local v2           #incrementalPos:F
    .end local v3           #lastChild:Landroid/view/View;
    .end local v4           #lastItemVisible:F
    .end local v6           #posWithinSection:F
    .end local v8           #result:I
    :cond_c2
    move/from16 v0, p1

    #@c4
    int-to-float v13, v0

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget-object v14, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@c9
    invoke-virtual {v14}, Landroid/widget/AbsListView;->getPaddingTop()I

    #@cc
    move-result v14

    #@cd
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@d0
    move-result v15

    #@d1
    sub-int/2addr v14, v15

    #@d2
    int-to-float v14, v14

    #@d3
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    #@d6
    move-result v15

    #@d7
    int-to-float v15, v15

    #@d8
    div-float/2addr v14, v15

    #@d9
    add-float v2, v13, v14

    #@db
    goto :goto_7c
.end method

.method private init(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 261
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@5
    move-result-object v5

    #@6
    sget-object v6, Landroid/widget/FastScroller;->ATTRS:[I

    #@8
    invoke-virtual {v5, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 262
    .local v0, ta:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v5

    #@10
    invoke-direct {p0, p1, v5}, Landroid/widget/FastScroller;->useThumbDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    #@13
    .line 263
    const/4 v5, 0x2

    #@14
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v5

    #@18
    iput-object v5, p0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1a
    .line 265
    const/4 v5, 0x3

    #@1b
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1e
    move-result-object v5

    #@1f
    iput-object v5, p0, Landroid/widget/FastScroller;->mOverlayDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@21
    .line 266
    const/4 v5, 0x4

    #@22
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v5

    #@26
    iput-object v5, p0, Landroid/widget/FastScroller;->mOverlayDrawableRight:Landroid/graphics/drawable/Drawable;

    #@28
    .line 267
    const/4 v5, 0x5

    #@29
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2c
    move-result v5

    #@2d
    iput v5, p0, Landroid/widget/FastScroller;->mOverlayPosition:I

    #@2f
    .line 269
    iput-boolean v3, p0, Landroid/widget/FastScroller;->mScrollCompleted:Z

    #@31
    .line 271
    invoke-virtual {p0}, Landroid/widget/FastScroller;->getSectionsFromIndexer()V

    #@34
    .line 273
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@37
    move-result-object v5

    #@38
    const v6, 0x1050015

    #@3b
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@3e
    move-result v5

    #@3f
    iput v5, p0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@41
    .line 275
    new-instance v5, Landroid/graphics/RectF;

    #@43
    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    #@46
    iput-object v5, p0, Landroid/widget/FastScroller;->mOverlayPos:Landroid/graphics/RectF;

    #@48
    .line 276
    new-instance v5, Landroid/widget/FastScroller$ScrollFade;

    #@4a
    invoke-direct {v5, p0}, Landroid/widget/FastScroller$ScrollFade;-><init>(Landroid/widget/FastScroller;)V

    #@4d
    iput-object v5, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@4f
    .line 277
    new-instance v5, Landroid/graphics/Paint;

    #@51
    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    #@54
    iput-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@56
    .line 278
    iget-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@58
    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@5b
    .line 279
    iget-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@5d
    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@5f
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    #@62
    .line 280
    iget-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@64
    iget v6, p0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@66
    div-int/lit8 v6, v6, 0x2

    #@68
    int-to-float v6, v6

    #@69
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    #@6c
    .line 282
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@6f
    move-result-object v1

    #@70
    .line 283
    .local v1, textColor:Landroid/content/res/ColorStateList;
    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    #@73
    move-result v2

    #@74
    .line 284
    .local v2, textColorNormal:I
    iget-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@76
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@79
    .line 285
    iget-object v5, p0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@7b
    sget-object v6, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    #@7d
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@80
    .line 288
    iget-object v5, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@82
    invoke-virtual {v5}, Landroid/widget/AbsListView;->getWidth()I

    #@85
    move-result v5

    #@86
    if-lez v5, :cond_9f

    #@88
    iget-object v5, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@8a
    invoke-virtual {v5}, Landroid/widget/AbsListView;->getHeight()I

    #@8d
    move-result v5

    #@8e
    if-lez v5, :cond_9f

    #@90
    .line 289
    iget-object v5, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@92
    invoke-virtual {v5}, Landroid/widget/AbsListView;->getWidth()I

    #@95
    move-result v5

    #@96
    iget-object v6, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@98
    invoke-virtual {v6}, Landroid/widget/AbsListView;->getHeight()I

    #@9b
    move-result v6

    #@9c
    invoke-virtual {p0, v5, v6, v4, v4}, Landroid/widget/FastScroller;->onSizeChanged(IIII)V

    #@9f
    .line 292
    :cond_9f
    iput v4, p0, Landroid/widget/FastScroller;->mState:I

    #@a1
    .line 293
    invoke-direct {p0}, Landroid/widget/FastScroller;->refreshDrawableState()V

    #@a4
    .line 295
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@a7
    .line 297
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@ae
    move-result v5

    #@af
    iput v5, p0, Landroid/widget/FastScroller;->mScaledTouchSlop:I

    #@b1
    .line 299
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@b4
    move-result-object v5

    #@b5
    iget v5, v5, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@b7
    const/16 v6, 0xb

    #@b9
    if-lt v5, v6, :cond_c7

    #@bb
    :goto_bb
    iput-boolean v3, p0, Landroid/widget/FastScroller;->mMatchDragPosition:Z

    #@bd
    .line 302
    iget-object v3, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@bf
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getVerticalScrollbarPosition()I

    #@c2
    move-result v3

    #@c3
    invoke-virtual {p0, v3}, Landroid/widget/FastScroller;->setScrollbarPosition(I)V

    #@c6
    .line 303
    return-void

    #@c7
    :cond_c7
    move v3, v4

    #@c8
    .line 299
    goto :goto_bb
.end method

.method private refreshDrawableState()V
    .registers 4

    #@0
    .prologue
    .line 173
    iget v1, p0, Landroid/widget/FastScroller;->mState:I

    #@2
    const/4 v2, 0x3

    #@3
    if-ne v1, v2, :cond_2a

    #@5
    sget-object v0, Landroid/widget/FastScroller;->PRESSED_STATES:[I

    #@7
    .line 175
    .local v0, state:[I
    :goto_7
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v1, :cond_18

    #@b
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_18

    #@13
    .line 176
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@18
    .line 178
    :cond_18
    iget-object v1, p0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1a
    if-eqz v1, :cond_29

    #@1c
    iget-object v1, p0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 179
    iget-object v1, p0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@29
    .line 181
    :cond_29
    return-void

    #@2a
    .line 173
    .end local v0           #state:[I
    :cond_2a
    sget-object v0, Landroid/widget/FastScroller;->DEFAULT_STATES:[I

    #@2c
    goto :goto_7
.end method

.method private resetThumbPos()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 232
    iget-object v1, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@3
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getWidth()I

    #@6
    move-result v0

    #@7
    .line 234
    .local v0, viewWidth:I
    iget v1, p0, Landroid/widget/FastScroller;->mPosition:I

    #@9
    packed-switch v1, :pswitch_data_2a

    #@c
    .line 242
    :goto_c
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@e
    const/16 v2, 0xd0

    #@10
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@13
    .line 243
    return-void

    #@14
    .line 236
    :pswitch_14
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    iget v2, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@18
    sub-int v2, v0, v2

    #@1a
    iget v3, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@1c
    invoke-virtual {v1, v2, v4, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1f
    goto :goto_c

    #@20
    .line 239
    :pswitch_20
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@22
    iget v2, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@24
    iget v3, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@26
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@29
    goto :goto_c

    #@2a
    .line 234
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_20
        :pswitch_14
    .end packed-switch
.end method

.method private useThumbDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "context"
    .parameter "drawable"

    #@0
    .prologue
    .line 246
    iput-object p2, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 247
    instance-of v0, p2, Landroid/graphics/drawable/NinePatchDrawable;

    #@4
    if-eqz v0, :cond_24

    #@6
    .line 248
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v0

    #@a
    const v1, 0x1050016

    #@d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@13
    .line 250
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v0

    #@17
    const v1, 0x1050017

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@20
    .line 256
    :goto_20
    const/4 v0, 0x1

    #@21
    iput-boolean v0, p0, Landroid/widget/FastScroller;->mChangedBounds:Z

    #@23
    .line 257
    return-void

    #@24
    .line 253
    :cond_24
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@2a
    .line 254
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@30
    goto :goto_20
.end method


# virtual methods
.method beginDrag()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 690
    const/4 v0, 0x3

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/FastScroller;->setState(I)V

    #@5
    .line 691
    iget-object v0, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@7
    if-nez v0, :cond_10

    #@9
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 692
    invoke-virtual {p0}, Landroid/widget/FastScroller;->getSectionsFromIndexer()V

    #@10
    .line 694
    :cond_10
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@12
    if-eqz v0, :cond_1e

    #@14
    .line 695
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->requestDisallowInterceptTouchEvent(Z)V

    #@19
    .line 696
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@1e
    .line 699
    :cond_1e
    invoke-direct {p0}, Landroid/widget/FastScroller;->cancelFling()V

    #@21
    .line 700
    return-void
.end method

.method cancelPendingDrag()V
    .registers 3

    #@0
    .prologue
    .line 680
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@2
    iget-object v1, p0, Landroid/widget/FastScroller;->mDeferStartDrag:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@7
    .line 681
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/widget/FastScroller;->mPendingDrag:Z

    #@a
    .line 682
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 29
    .parameter "canvas"

    #@0
    .prologue
    .line 315
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/widget/FastScroller;->mState:I

    #@4
    move/from16 v22, v0

    #@6
    if-nez v22, :cond_9

    #@8
    .line 407
    :cond_8
    :goto_8
    return-void

    #@9
    .line 320
    :cond_9
    move-object/from16 v0, p0

    #@b
    iget v0, v0, Landroid/widget/FastScroller;->mThumbY:I

    #@d
    move/from16 v21, v0

    #@f
    .line 321
    .local v21, y:I
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@13
    move-object/from16 v22, v0

    #@15
    invoke-virtual/range {v22 .. v22}, Landroid/widget/AbsListView;->getWidth()I

    #@18
    move-result v20

    #@19
    .line 322
    .local v20, viewWidth:I
    move-object/from16 v0, p0

    #@1b
    iget-object v13, v0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@1d
    .line 324
    .local v13, scrollFade:Landroid/widget/FastScroller$ScrollFade;
    const/4 v5, -0x1

    #@1e
    .line 325
    .local v5, alpha:I
    move-object/from16 v0, p0

    #@20
    iget v0, v0, Landroid/widget/FastScroller;->mState:I

    #@22
    move/from16 v22, v0

    #@24
    const/16 v23, 0x4

    #@26
    move/from16 v0, v22

    #@28
    move/from16 v1, v23

    #@2a
    if-ne v0, v1, :cond_74

    #@2c
    .line 326
    invoke-virtual {v13}, Landroid/widget/FastScroller$ScrollFade;->getAlpha()I

    #@2f
    move-result v5

    #@30
    .line 327
    const/16 v22, 0x68

    #@32
    move/from16 v0, v22

    #@34
    if-ge v5, v0, :cond_41

    #@36
    .line 328
    move-object/from16 v0, p0

    #@38
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@3a
    move-object/from16 v22, v0

    #@3c
    mul-int/lit8 v23, v5, 0x2

    #@3e
    invoke-virtual/range {v22 .. v23}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@41
    .line 330
    :cond_41
    const/4 v9, 0x0

    #@42
    .line 331
    .local v9, left:I
    move-object/from16 v0, p0

    #@44
    iget v0, v0, Landroid/widget/FastScroller;->mPosition:I

    #@46
    move/from16 v22, v0

    #@48
    packed-switch v22, :pswitch_data_364

    #@4b
    .line 339
    :goto_4b
    move-object/from16 v0, p0

    #@4d
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@4f
    move-object/from16 v22, v0

    #@51
    const/16 v23, 0x0

    #@53
    move-object/from16 v0, p0

    #@55
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@57
    move/from16 v24, v0

    #@59
    add-int v24, v24, v9

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget v0, v0, Landroid/widget/FastScroller;->mThumbH:I

    #@5f
    move/from16 v25, v0

    #@61
    move-object/from16 v0, v22

    #@63
    move/from16 v1, v23

    #@65
    move/from16 v2, v24

    #@67
    move/from16 v3, v25

    #@69
    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@6c
    .line 340
    const/16 v22, 0x1

    #@6e
    move/from16 v0, v22

    #@70
    move-object/from16 v1, p0

    #@72
    iput-boolean v0, v1, Landroid/widget/FastScroller;->mChangedBounds:Z

    #@74
    .line 343
    .end local v9           #left:I
    :cond_74
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@78
    move-object/from16 v22, v0

    #@7a
    if-eqz v22, :cond_d8

    #@7c
    .line 344
    move-object/from16 v0, p0

    #@7e
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@80
    move-object/from16 v22, v0

    #@82
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@85
    move-result-object v14

    #@86
    .line 345
    .local v14, thumbBounds:Landroid/graphics/Rect;
    iget v9, v14, Landroid/graphics/Rect;->left:I

    #@88
    .line 346
    .restart local v9       #left:I
    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    #@8a
    move/from16 v22, v0

    #@8c
    iget v0, v14, Landroid/graphics/Rect;->top:I

    #@8e
    move/from16 v23, v0

    #@90
    sub-int v22, v22, v23

    #@92
    div-int/lit8 v8, v22, 0x2

    #@94
    .line 347
    .local v8, halfThumbHeight:I
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@98
    move-object/from16 v22, v0

    #@9a
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@9d
    move-result v18

    #@9e
    .line 348
    .local v18, trackWidth:I
    move-object/from16 v0, p0

    #@a0
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@a2
    move/from16 v22, v0

    #@a4
    div-int/lit8 v22, v22, 0x2

    #@a6
    add-int v22, v22, v9

    #@a8
    div-int/lit8 v23, v18, 0x2

    #@aa
    sub-int v17, v22, v23

    #@ac
    .line 349
    .local v17, trackLeft:I
    move-object/from16 v0, p0

    #@ae
    iget-object v0, v0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@b0
    move-object/from16 v22, v0

    #@b2
    add-int v23, v17, v18

    #@b4
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@b8
    move-object/from16 v24, v0

    #@ba
    invoke-virtual/range {v24 .. v24}, Landroid/widget/AbsListView;->getHeight()I

    #@bd
    move-result v24

    #@be
    sub-int v24, v24, v8

    #@c0
    move-object/from16 v0, v22

    #@c2
    move/from16 v1, v17

    #@c4
    move/from16 v2, v23

    #@c6
    move/from16 v3, v24

    #@c8
    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@cb
    .line 351
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@cf
    move-object/from16 v22, v0

    #@d1
    move-object/from16 v0, v22

    #@d3
    move-object/from16 v1, p1

    #@d5
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@d8
    .line 354
    .end local v8           #halfThumbHeight:I
    .end local v9           #left:I
    .end local v14           #thumbBounds:Landroid/graphics/Rect;
    .end local v17           #trackLeft:I
    .end local v18           #trackWidth:I
    :cond_d8
    const/16 v22, 0x0

    #@da
    move/from16 v0, v21

    #@dc
    int-to-float v0, v0

    #@dd
    move/from16 v23, v0

    #@df
    move-object/from16 v0, p1

    #@e1
    move/from16 v1, v22

    #@e3
    move/from16 v2, v23

    #@e5
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@e8
    .line 355
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@ec
    move-object/from16 v22, v0

    #@ee
    move-object/from16 v0, v22

    #@f0
    move-object/from16 v1, p1

    #@f2
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@f5
    .line 356
    const/16 v22, 0x0

    #@f7
    move/from16 v0, v21

    #@f9
    neg-int v0, v0

    #@fa
    move/from16 v23, v0

    #@fc
    move/from16 v0, v23

    #@fe
    int-to-float v0, v0

    #@ff
    move/from16 v23, v0

    #@101
    move-object/from16 v0, p1

    #@103
    move/from16 v1, v22

    #@105
    move/from16 v2, v23

    #@107
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@10a
    .line 359
    move-object/from16 v0, p0

    #@10c
    iget v0, v0, Landroid/widget/FastScroller;->mState:I

    #@10e
    move/from16 v22, v0

    #@110
    const/16 v23, 0x3

    #@112
    move/from16 v0, v22

    #@114
    move/from16 v1, v23

    #@116
    if-ne v0, v1, :cond_2f2

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-boolean v0, v0, Landroid/widget/FastScroller;->mDrawOverlay:Z

    #@11c
    move/from16 v22, v0

    #@11e
    if-eqz v22, :cond_2f2

    #@120
    .line 360
    move-object/from16 v0, p0

    #@122
    iget v0, v0, Landroid/widget/FastScroller;->mOverlayPosition:I

    #@124
    move/from16 v22, v0

    #@126
    const/16 v23, 0x1

    #@128
    move/from16 v0, v22

    #@12a
    move/from16 v1, v23

    #@12c
    if-ne v0, v1, :cond_1ff

    #@12e
    .line 361
    const/4 v9, 0x0

    #@12f
    .line 362
    .restart local v9       #left:I
    move-object/from16 v0, p0

    #@131
    iget v0, v0, Landroid/widget/FastScroller;->mPosition:I

    #@133
    move/from16 v22, v0

    #@135
    packed-switch v22, :pswitch_data_36c

    #@138
    .line 365
    const/16 v22, 0x0

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@13e
    move-object/from16 v23, v0

    #@140
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@143
    move-result-object v23

    #@144
    move-object/from16 v0, v23

    #@146
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@148
    move/from16 v23, v0

    #@14a
    move-object/from16 v0, p0

    #@14c
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@14e
    move/from16 v24, v0

    #@150
    sub-int v23, v23, v24

    #@152
    move-object/from16 v0, p0

    #@154
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@156
    move/from16 v24, v0

    #@158
    sub-int v23, v23, v24

    #@15a
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    #@15d
    move-result v9

    #@15e
    .line 374
    :goto_15e
    const/16 v22, 0x0

    #@160
    move-object/from16 v0, p0

    #@162
    iget v0, v0, Landroid/widget/FastScroller;->mThumbH:I

    #@164
    move/from16 v23, v0

    #@166
    move-object/from16 v0, p0

    #@168
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@16a
    move/from16 v24, v0

    #@16c
    sub-int v23, v23, v24

    #@16e
    div-int/lit8 v23, v23, 0x2

    #@170
    add-int v23, v23, v21

    #@172
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@176
    move-object/from16 v24, v0

    #@178
    invoke-virtual/range {v24 .. v24}, Landroid/widget/AbsListView;->getHeight()I

    #@17b
    move-result v24

    #@17c
    move-object/from16 v0, p0

    #@17e
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@180
    move/from16 v25, v0

    #@182
    sub-int v24, v24, v25

    #@184
    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(II)I

    #@187
    move-result v23

    #@188
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    #@18b
    move-result v16

    #@18c
    .line 377
    .local v16, top:I
    move-object/from16 v0, p0

    #@18e
    iget-object v11, v0, Landroid/widget/FastScroller;->mOverlayPos:Landroid/graphics/RectF;

    #@190
    .line 378
    .local v11, pos:Landroid/graphics/RectF;
    int-to-float v0, v9

    #@191
    move/from16 v22, v0

    #@193
    move/from16 v0, v22

    #@195
    iput v0, v11, Landroid/graphics/RectF;->left:F

    #@197
    .line 379
    iget v0, v11, Landroid/graphics/RectF;->left:F

    #@199
    move/from16 v22, v0

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@19f
    move/from16 v23, v0

    #@1a1
    move/from16 v0, v23

    #@1a3
    int-to-float v0, v0

    #@1a4
    move/from16 v23, v0

    #@1a6
    add-float v22, v22, v23

    #@1a8
    move/from16 v0, v22

    #@1aa
    iput v0, v11, Landroid/graphics/RectF;->right:F

    #@1ac
    .line 380
    move/from16 v0, v16

    #@1ae
    int-to-float v0, v0

    #@1af
    move/from16 v22, v0

    #@1b1
    move/from16 v0, v22

    #@1b3
    iput v0, v11, Landroid/graphics/RectF;->top:F

    #@1b5
    .line 381
    iget v0, v11, Landroid/graphics/RectF;->top:F

    #@1b7
    move/from16 v22, v0

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@1bd
    move/from16 v23, v0

    #@1bf
    move/from16 v0, v23

    #@1c1
    int-to-float v0, v0

    #@1c2
    move/from16 v23, v0

    #@1c4
    add-float v22, v22, v23

    #@1c6
    move/from16 v0, v22

    #@1c8
    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    #@1ca
    .line 382
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@1ce
    move-object/from16 v22, v0

    #@1d0
    if-eqz v22, :cond_1ff

    #@1d2
    .line 383
    move-object/from16 v0, p0

    #@1d4
    iget-object v0, v0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@1d6
    move-object/from16 v22, v0

    #@1d8
    iget v0, v11, Landroid/graphics/RectF;->left:F

    #@1da
    move/from16 v23, v0

    #@1dc
    move/from16 v0, v23

    #@1de
    float-to-int v0, v0

    #@1df
    move/from16 v23, v0

    #@1e1
    iget v0, v11, Landroid/graphics/RectF;->top:F

    #@1e3
    move/from16 v24, v0

    #@1e5
    move/from16 v0, v24

    #@1e7
    float-to-int v0, v0

    #@1e8
    move/from16 v24, v0

    #@1ea
    iget v0, v11, Landroid/graphics/RectF;->right:F

    #@1ec
    move/from16 v25, v0

    #@1ee
    move/from16 v0, v25

    #@1f0
    float-to-int v0, v0

    #@1f1
    move/from16 v25, v0

    #@1f3
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    #@1f5
    move/from16 v26, v0

    #@1f7
    move/from16 v0, v26

    #@1f9
    float-to-int v0, v0

    #@1fa
    move/from16 v26, v0

    #@1fc
    invoke-virtual/range {v22 .. v26}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1ff
    .line 387
    .end local v9           #left:I
    .end local v11           #pos:Landroid/graphics/RectF;
    .end local v16           #top:I
    :cond_1ff
    move-object/from16 v0, p0

    #@201
    iget-object v0, v0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@203
    move-object/from16 v22, v0

    #@205
    move-object/from16 v0, v22

    #@207
    move-object/from16 v1, p1

    #@209
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@20c
    .line 388
    move-object/from16 v0, p0

    #@20e
    iget-object v10, v0, Landroid/widget/FastScroller;->mPaint:Landroid/graphics/Paint;

    #@210
    .line 389
    .local v10, paint:Landroid/graphics/Paint;
    invoke-virtual {v10}, Landroid/graphics/Paint;->descent()F

    #@213
    move-result v6

    #@214
    .line 390
    .local v6, descent:F
    move-object/from16 v0, p0

    #@216
    iget-object v12, v0, Landroid/widget/FastScroller;->mOverlayPos:Landroid/graphics/RectF;

    #@218
    .line 391
    .local v12, rectF:Landroid/graphics/RectF;
    move-object/from16 v0, p0

    #@21a
    iget-object v15, v0, Landroid/widget/FastScroller;->mTmpRect:Landroid/graphics/Rect;

    #@21c
    .line 392
    .local v15, tmpRect:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@21e
    iget-object v0, v0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@220
    move-object/from16 v22, v0

    #@222
    move-object/from16 v0, v22

    #@224
    invoke-virtual {v0, v15}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@227
    .line 393
    iget v0, v15, Landroid/graphics/Rect;->right:I

    #@229
    move/from16 v22, v0

    #@22b
    iget v0, v15, Landroid/graphics/Rect;->left:I

    #@22d
    move/from16 v23, v0

    #@22f
    sub-int v22, v22, v23

    #@231
    div-int/lit8 v7, v22, 0x2

    #@233
    .line 394
    .local v7, hOff:I
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    #@235
    move/from16 v22, v0

    #@237
    iget v0, v15, Landroid/graphics/Rect;->top:I

    #@239
    move/from16 v23, v0

    #@23b
    sub-int v22, v22, v23

    #@23d
    div-int/lit8 v19, v22, 0x2

    #@23f
    .line 395
    .local v19, vOff:I
    move-object/from16 v0, p0

    #@241
    iget-object v0, v0, Landroid/widget/FastScroller;->mSectionText:Ljava/lang/String;

    #@243
    move-object/from16 v22, v0

    #@245
    iget v0, v12, Landroid/graphics/RectF;->left:F

    #@247
    move/from16 v23, v0

    #@249
    iget v0, v12, Landroid/graphics/RectF;->right:F

    #@24b
    move/from16 v24, v0

    #@24d
    add-float v23, v23, v24

    #@24f
    move/from16 v0, v23

    #@251
    float-to-int v0, v0

    #@252
    move/from16 v23, v0

    #@254
    div-int/lit8 v23, v23, 0x2

    #@256
    sub-int v23, v23, v7

    #@258
    move/from16 v0, v23

    #@25a
    int-to-float v0, v0

    #@25b
    move/from16 v23, v0

    #@25d
    iget v0, v12, Landroid/graphics/RectF;->bottom:F

    #@25f
    move/from16 v24, v0

    #@261
    iget v0, v12, Landroid/graphics/RectF;->top:F

    #@263
    move/from16 v25, v0

    #@265
    add-float v24, v24, v25

    #@267
    move/from16 v0, v24

    #@269
    float-to-int v0, v0

    #@26a
    move/from16 v24, v0

    #@26c
    div-int/lit8 v24, v24, 0x2

    #@26e
    move-object/from16 v0, p0

    #@270
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@272
    move/from16 v25, v0

    #@274
    div-int/lit8 v25, v25, 0x4

    #@276
    add-int v24, v24, v25

    #@278
    move/from16 v0, v24

    #@27a
    int-to-float v0, v0

    #@27b
    move/from16 v24, v0

    #@27d
    sub-float v24, v24, v6

    #@27f
    move/from16 v0, v19

    #@281
    int-to-float v0, v0

    #@282
    move/from16 v25, v0

    #@284
    sub-float v24, v24, v25

    #@286
    move-object/from16 v0, p1

    #@288
    move-object/from16 v1, v22

    #@28a
    move/from16 v2, v23

    #@28c
    move/from16 v3, v24

    #@28e
    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@291
    goto/16 :goto_8

    #@293
    .line 333
    .end local v6           #descent:F
    .end local v7           #hOff:I
    .end local v10           #paint:Landroid/graphics/Paint;
    .end local v12           #rectF:Landroid/graphics/RectF;
    .end local v15           #tmpRect:Landroid/graphics/Rect;
    .end local v19           #vOff:I
    .restart local v9       #left:I
    :pswitch_293
    move-object/from16 v0, p0

    #@295
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@297
    move/from16 v22, v0

    #@299
    mul-int v22, v22, v5

    #@29b
    move/from16 v0, v22

    #@29d
    div-int/lit16 v0, v0, 0xd0

    #@29f
    move/from16 v22, v0

    #@2a1
    sub-int v9, v20, v22

    #@2a3
    .line 334
    goto/16 :goto_4b

    #@2a5
    .line 336
    :pswitch_2a5
    move-object/from16 v0, p0

    #@2a7
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@2a9
    move/from16 v22, v0

    #@2ab
    move/from16 v0, v22

    #@2ad
    neg-int v0, v0

    #@2ae
    move/from16 v22, v0

    #@2b0
    move-object/from16 v0, p0

    #@2b2
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@2b4
    move/from16 v23, v0

    #@2b6
    mul-int v23, v23, v5

    #@2b8
    move/from16 v0, v23

    #@2ba
    div-int/lit16 v0, v0, 0xd0

    #@2bc
    move/from16 v23, v0

    #@2be
    add-int v9, v22, v23

    #@2c0
    goto/16 :goto_4b

    #@2c2
    .line 369
    :pswitch_2c2
    move-object/from16 v0, p0

    #@2c4
    iget-object v0, v0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@2c6
    move-object/from16 v22, v0

    #@2c8
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@2cb
    move-result-object v22

    #@2cc
    move-object/from16 v0, v22

    #@2ce
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@2d0
    move/from16 v22, v0

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@2d6
    move/from16 v23, v0

    #@2d8
    add-int v22, v22, v23

    #@2da
    move-object/from16 v0, p0

    #@2dc
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@2de
    move-object/from16 v23, v0

    #@2e0
    invoke-virtual/range {v23 .. v23}, Landroid/widget/AbsListView;->getWidth()I

    #@2e3
    move-result v23

    #@2e4
    move-object/from16 v0, p0

    #@2e6
    iget v0, v0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@2e8
    move/from16 v24, v0

    #@2ea
    sub-int v23, v23, v24

    #@2ec
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    #@2ef
    move-result v9

    #@2f0
    goto/16 :goto_15e

    #@2f2
    .line 398
    .end local v9           #left:I
    :cond_2f2
    move-object/from16 v0, p0

    #@2f4
    iget v0, v0, Landroid/widget/FastScroller;->mState:I

    #@2f6
    move/from16 v22, v0

    #@2f8
    const/16 v23, 0x4

    #@2fa
    move/from16 v0, v22

    #@2fc
    move/from16 v1, v23

    #@2fe
    if-ne v0, v1, :cond_8

    #@300
    .line 399
    if-nez v5, :cond_30d

    #@302
    .line 400
    const/16 v22, 0x0

    #@304
    move-object/from16 v0, p0

    #@306
    move/from16 v1, v22

    #@308
    invoke-virtual {v0, v1}, Landroid/widget/FastScroller;->setState(I)V

    #@30b
    goto/16 :goto_8

    #@30d
    .line 401
    :cond_30d
    move-object/from16 v0, p0

    #@30f
    iget-object v0, v0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@311
    move-object/from16 v22, v0

    #@313
    if-eqz v22, :cond_33e

    #@315
    .line 402
    move-object/from16 v0, p0

    #@317
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@319
    move-object/from16 v22, v0

    #@31b
    move-object/from16 v0, p0

    #@31d
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@31f
    move/from16 v23, v0

    #@321
    sub-int v23, v20, v23

    #@323
    const/16 v24, 0x0

    #@325
    move-object/from16 v0, p0

    #@327
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@329
    move-object/from16 v25, v0

    #@32b
    invoke-virtual/range {v25 .. v25}, Landroid/widget/AbsListView;->getHeight()I

    #@32e
    move-result v25

    #@32f
    move-object/from16 v0, v22

    #@331
    move/from16 v1, v23

    #@333
    move/from16 v2, v24

    #@335
    move/from16 v3, v20

    #@337
    move/from16 v4, v25

    #@339
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/AbsListView;->invalidate(IIII)V

    #@33c
    goto/16 :goto_8

    #@33e
    .line 404
    :cond_33e
    move-object/from16 v0, p0

    #@340
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@342
    move-object/from16 v22, v0

    #@344
    move-object/from16 v0, p0

    #@346
    iget v0, v0, Landroid/widget/FastScroller;->mThumbW:I

    #@348
    move/from16 v23, v0

    #@34a
    sub-int v23, v20, v23

    #@34c
    move-object/from16 v0, p0

    #@34e
    iget v0, v0, Landroid/widget/FastScroller;->mThumbH:I

    #@350
    move/from16 v24, v0

    #@352
    add-int v24, v24, v21

    #@354
    move-object/from16 v0, v22

    #@356
    move/from16 v1, v23

    #@358
    move/from16 v2, v21

    #@35a
    move/from16 v3, v20

    #@35c
    move/from16 v4, v24

    #@35e
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/AbsListView;->invalidate(IIII)V

    #@361
    goto/16 :goto_8

    #@363
    .line 331
    nop

    #@364
    :pswitch_data_364
    .packed-switch 0x1
        :pswitch_2a5
        :pswitch_293
    .end packed-switch

    #@36c
    .line 362
    :pswitch_data_36c
    .packed-switch 0x1
        :pswitch_2c2
    .end packed-switch
.end method

.method getSectionIndexer()Landroid/widget/SectionIndexer;
    .registers 2

    #@0
    .prologue
    .line 478
    iget-object v0, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@2
    return-object v0
.end method

.method getSections()[Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 482
    iget-object v0, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@2
    if-nez v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 483
    invoke-virtual {p0}, Landroid/widget/FastScroller;->getSectionsFromIndexer()V

    #@b
    .line 485
    :cond_b
    iget-object v0, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@d
    return-object v0
.end method

.method getSectionsFromIndexer()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 489
    iget-object v2, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@4
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@7
    move-result-object v0

    #@8
    .line 490
    .local v0, adapter:Landroid/widget/Adapter;
    const/4 v2, 0x0

    #@9
    iput-object v2, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@b
    .line 491
    instance-of v2, v0, Landroid/widget/HeaderViewListAdapter;

    #@d
    if-eqz v2, :cond_1e

    #@f
    move-object v2, v0

    #@10
    .line 492
    check-cast v2, Landroid/widget/HeaderViewListAdapter;

    #@12
    invoke-virtual {v2}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    #@15
    move-result v2

    #@16
    iput v2, p0, Landroid/widget/FastScroller;->mListOffset:I

    #@18
    .line 493
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    #@1a
    .end local v0           #adapter:Landroid/widget/Adapter;
    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    #@1d
    move-result-object v0

    #@1e
    .line 495
    .restart local v0       #adapter:Landroid/widget/Adapter;
    :cond_1e
    instance-of v2, v0, Landroid/widget/ExpandableListConnector;

    #@20
    if-eqz v2, :cond_3e

    #@22
    move-object v2, v0

    #@23
    .line 496
    check-cast v2, Landroid/widget/ExpandableListConnector;

    #@25
    invoke-virtual {v2}, Landroid/widget/ExpandableListConnector;->getAdapter()Landroid/widget/ExpandableListAdapter;

    #@28
    move-result-object v1

    #@29
    .line 497
    .local v1, expAdapter:Landroid/widget/ExpandableListAdapter;
    instance-of v2, v1, Landroid/widget/SectionIndexer;

    #@2b
    if-eqz v2, :cond_3d

    #@2d
    .line 498
    check-cast v1, Landroid/widget/SectionIndexer;

    #@2f
    .end local v1           #expAdapter:Landroid/widget/ExpandableListAdapter;
    iput-object v1, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@31
    .line 499
    check-cast v0, Landroid/widget/BaseAdapter;

    #@33
    .end local v0           #adapter:Landroid/widget/Adapter;
    iput-object v0, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@35
    .line 500
    iget-object v2, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@37
    invoke-interface {v2}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    #@3a
    move-result-object v2

    #@3b
    iput-object v2, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@3d
    .line 515
    :cond_3d
    :goto_3d
    return-void

    #@3e
    .line 503
    .restart local v0       #adapter:Landroid/widget/Adapter;
    :cond_3e
    instance-of v2, v0, Landroid/widget/SectionIndexer;

    #@40
    if-eqz v2, :cond_60

    #@42
    move-object v2, v0

    #@43
    .line 504
    check-cast v2, Landroid/widget/BaseAdapter;

    #@45
    iput-object v2, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@47
    .line 505
    check-cast v0, Landroid/widget/SectionIndexer;

    #@49
    .end local v0           #adapter:Landroid/widget/Adapter;
    iput-object v0, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@4b
    .line 506
    iget-object v2, p0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@4d
    invoke-interface {v2}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    #@50
    move-result-object v2

    #@51
    iput-object v2, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@53
    .line 507
    iget-object v2, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@55
    if-nez v2, :cond_3d

    #@57
    .line 508
    new-array v2, v3, [Ljava/lang/String;

    #@59
    const-string v3, " "

    #@5b
    aput-object v3, v2, v4

    #@5d
    iput-object v2, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@5f
    goto :goto_3d

    #@60
    .line 511
    .restart local v0       #adapter:Landroid/widget/Adapter;
    :cond_60
    check-cast v0, Landroid/widget/BaseAdapter;

    #@62
    .end local v0           #adapter:Landroid/widget/Adapter;
    iput-object v0, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@64
    .line 512
    new-array v2, v3, [Ljava/lang/String;

    #@66
    const-string v3, " "

    #@68
    aput-object v3, v2, v4

    #@6a
    iput-object v2, p0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@6c
    goto :goto_3d
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@2
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 201
    iget v0, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@2
    return v0
.end method

.method public isAlwaysShowEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 169
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@2
    return v0
.end method

.method isPointInside(FF)Z
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 819
    const/4 v0, 0x0

    #@3
    .line 820
    .local v0, inTrack:Z
    iget v3, p0, Landroid/widget/FastScroller;->mPosition:I

    #@5
    packed-switch v3, :pswitch_data_3e

    #@8
    .line 823
    iget-object v3, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@a
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getWidth()I

    #@d
    move-result v3

    #@e
    iget v4, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@10
    sub-int/2addr v3, v4

    #@11
    int-to-float v3, v3

    #@12
    cmpl-float v3, p1, v3

    #@14
    if-lez v3, :cond_2f

    #@16
    move v0, v1

    #@17
    .line 831
    :goto_17
    if-eqz v0, :cond_3c

    #@19
    iget-object v3, p0, Landroid/widget/FastScroller;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1b
    if-nez v3, :cond_2e

    #@1d
    iget v3, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@1f
    int-to-float v3, v3

    #@20
    cmpl-float v3, p2, v3

    #@22
    if-ltz v3, :cond_3c

    #@24
    iget v3, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@26
    iget v4, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@28
    add-int/2addr v3, v4

    #@29
    int-to-float v3, v3

    #@2a
    cmpg-float v3, p2, v3

    #@2c
    if-gtz v3, :cond_3c

    #@2e
    :cond_2e
    :goto_2e
    return v1

    #@2f
    :cond_2f
    move v0, v2

    #@30
    .line 823
    goto :goto_17

    #@31
    .line 826
    :pswitch_31
    iget v3, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@33
    int-to-float v3, v3

    #@34
    cmpg-float v3, p1, v3

    #@36
    if-gez v3, :cond_3a

    #@38
    move v0, v1

    #@39
    :goto_39
    goto :goto_17

    #@3a
    :cond_3a
    move v0, v2

    #@3b
    goto :goto_39

    #@3c
    :cond_3c
    move v1, v2

    #@3d
    .line 831
    goto :goto_2e

    #@3e
    .line 820
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_31
    .end packed-switch
.end method

.method isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 310
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 703
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_36

    #@7
    .line 719
    :cond_7
    :goto_7
    :pswitch_7
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 705
    :pswitch_9
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@b
    if-lez v0, :cond_7

    #@d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@10
    move-result v0

    #@11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@14
    move-result v1

    #@15
    invoke-virtual {p0, v0, v1}, Landroid/widget/FastScroller;->isPointInside(FF)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_7

    #@1b
    .line 706
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1d
    invoke-virtual {v0}, Landroid/widget/AbsListView;->isInScrollingContainer()Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_28

    #@23
    .line 707
    invoke-virtual {p0}, Landroid/widget/FastScroller;->beginDrag()V

    #@26
    .line 708
    const/4 v0, 0x1

    #@27
    goto :goto_8

    #@28
    .line 710
    :cond_28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Landroid/widget/FastScroller;->mInitialTouchY:F

    #@2e
    .line 711
    invoke-virtual {p0}, Landroid/widget/FastScroller;->startPendingDrag()V

    #@31
    goto :goto_7

    #@32
    .line 716
    :pswitch_32
    invoke-virtual {p0}, Landroid/widget/FastScroller;->cancelPendingDrag()V

    #@35
    goto :goto_7

    #@36
    .line 703
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_9
        :pswitch_32
        :pswitch_7
        :pswitch_32
    .end packed-switch
.end method

.method onItemCountChanged(II)V
    .registers 4
    .parameter "oldCount"
    .parameter "newCount"

    #@0
    .prologue
    .line 435
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 436
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/FastScroller;->mLongList:Z

    #@7
    .line 438
    :cond_7
    return-void
.end method

.method onScroll(Landroid/widget/AbsListView;III)V
    .registers 10
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 443
    iget v0, p0, Landroid/widget/FastScroller;->mItemCount:I

    #@5
    if-eq v0, p4, :cond_15

    #@7
    if-lez p3, :cond_15

    #@9
    .line 444
    iput p4, p0, Landroid/widget/FastScroller;->mItemCount:I

    #@b
    .line 445
    iget v0, p0, Landroid/widget/FastScroller;->mItemCount:I

    #@d
    div-int/2addr v0, p3

    #@e
    sget v3, Landroid/widget/FastScroller;->MIN_PAGES:I

    #@10
    if-lt v0, v3, :cond_27

    #@12
    move v0, v1

    #@13
    :goto_13
    iput-boolean v0, p0, Landroid/widget/FastScroller;->mLongList:Z

    #@15
    .line 447
    :cond_15
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@17
    if-eqz v0, :cond_1b

    #@19
    .line 448
    iput-boolean v1, p0, Landroid/widget/FastScroller;->mLongList:Z

    #@1b
    .line 450
    :cond_1b
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mLongList:Z

    #@1d
    if-nez v0, :cond_29

    #@1f
    .line 451
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@21
    if-eqz v0, :cond_26

    #@23
    .line 452
    invoke-virtual {p0, v2}, Landroid/widget/FastScroller;->setState(I)V

    #@26
    .line 475
    :cond_26
    :goto_26
    return-void

    #@27
    :cond_27
    move v0, v2

    #@28
    .line 445
    goto :goto_13

    #@29
    .line 456
    :cond_29
    sub-int v0, p4, p3

    #@2b
    if-lez v0, :cond_40

    #@2d
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@2f
    if-eq v0, v4, :cond_40

    #@31
    .line 457
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/FastScroller;->getThumbPositionForListPosition(III)I

    #@34
    move-result v0

    #@35
    iput v0, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@37
    .line 459
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mChangedBounds:Z

    #@39
    if-eqz v0, :cond_40

    #@3b
    .line 460
    invoke-direct {p0}, Landroid/widget/FastScroller;->resetThumbPos()V

    #@3e
    .line 461
    iput-boolean v2, p0, Landroid/widget/FastScroller;->mChangedBounds:Z

    #@40
    .line 464
    :cond_40
    iput-boolean v1, p0, Landroid/widget/FastScroller;->mScrollCompleted:Z

    #@42
    .line 465
    iget v0, p0, Landroid/widget/FastScroller;->mVisibleItem:I

    #@44
    if-eq p2, v0, :cond_26

    #@46
    .line 468
    iput p2, p0, Landroid/widget/FastScroller;->mVisibleItem:I

    #@48
    .line 469
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@4a
    if-eq v0, v4, :cond_26

    #@4c
    .line 470
    const/4 v0, 0x2

    #@4d
    invoke-virtual {p0, v0}, Landroid/widget/FastScroller;->setState(I)V

    #@50
    .line 471
    iget-boolean v0, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@52
    if-nez v0, :cond_26

    #@54
    .line 472
    iget-object v0, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@56
    iget-object v1, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@58
    const-wide/16 v2, 0x5dc

    #@5a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@5d
    goto :goto_26
.end method

.method public onSectionsChanged()V
    .registers 2

    #@0
    .prologue
    .line 518
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@3
    .line 519
    return-void
.end method

.method onSizeChanged(IIII)V
    .registers 11
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 410
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    if-eqz v1, :cond_15

    #@5
    .line 411
    iget v1, p0, Landroid/widget/FastScroller;->mPosition:I

    #@7
    packed-switch v1, :pswitch_data_5a

    #@a
    .line 414
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    iget v2, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@e
    sub-int v2, p1, v2

    #@10
    iget v3, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@12
    invoke-virtual {v1, v2, v4, p1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@15
    .line 421
    :cond_15
    :goto_15
    iget v1, p0, Landroid/widget/FastScroller;->mOverlayPosition:I

    #@17
    if-nez v1, :cond_4e

    #@19
    .line 422
    iget-object v0, p0, Landroid/widget/FastScroller;->mOverlayPos:Landroid/graphics/RectF;

    #@1b
    .line 423
    .local v0, pos:Landroid/graphics/RectF;
    iget v1, p0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@1d
    sub-int v1, p1, v1

    #@1f
    div-int/lit8 v1, v1, 0x2

    #@21
    int-to-float v1, v1

    #@22
    iput v1, v0, Landroid/graphics/RectF;->left:F

    #@24
    .line 424
    iget v1, v0, Landroid/graphics/RectF;->left:F

    #@26
    iget v2, p0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@28
    int-to-float v2, v2

    #@29
    add-float/2addr v1, v2

    #@2a
    iput v1, v0, Landroid/graphics/RectF;->right:F

    #@2c
    .line 425
    div-int/lit8 v1, p2, 0xa

    #@2e
    int-to-float v1, v1

    #@2f
    iput v1, v0, Landroid/graphics/RectF;->top:F

    #@31
    .line 426
    iget v1, v0, Landroid/graphics/RectF;->top:F

    #@33
    iget v2, p0, Landroid/widget/FastScroller;->mOverlaySize:I

    #@35
    int-to-float v2, v2

    #@36
    add-float/2addr v1, v2

    #@37
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    #@39
    .line 427
    iget-object v1, p0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@3b
    if-eqz v1, :cond_4e

    #@3d
    .line 428
    iget-object v1, p0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@3f
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@41
    float-to-int v2, v2

    #@42
    iget v3, v0, Landroid/graphics/RectF;->top:F

    #@44
    float-to-int v3, v3

    #@45
    iget v4, v0, Landroid/graphics/RectF;->right:F

    #@47
    float-to-int v4, v4

    #@48
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    #@4a
    float-to-int v5, v5

    #@4b
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@4e
    .line 432
    .end local v0           #pos:Landroid/graphics/RectF;
    :cond_4e
    return-void

    #@4f
    .line 417
    :pswitch_4f
    iget-object v1, p0, Landroid/widget/FastScroller;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@51
    iget v2, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@53
    iget v3, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@55
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@58
    goto :goto_15

    #@59
    .line 411
    nop

    #@5a
    :pswitch_data_5a
    .packed-switch 0x1
        :pswitch_4f
    .end packed-switch
.end method

.method onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter "me"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x3

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 723
    iget v7, p0, Landroid/widget/FastScroller;->mState:I

    #@6
    if-nez v7, :cond_9

    #@8
    .line 815
    :cond_8
    :goto_8
    return v5

    #@9
    .line 727
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@c
    move-result v0

    #@d
    .line 729
    .local v0, action:I
    if-nez v0, :cond_34

    #@f
    .line 730
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@12
    move-result v7

    #@13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@16
    move-result v8

    #@17
    invoke-virtual {p0, v7, v8}, Landroid/widget/FastScroller;->isPointInside(FF)Z

    #@1a
    move-result v7

    #@1b
    if-eqz v7, :cond_8

    #@1d
    .line 731
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1f
    invoke-virtual {v7}, Landroid/widget/AbsListView;->isInScrollingContainer()Z

    #@22
    move-result v7

    #@23
    if-nez v7, :cond_2a

    #@25
    .line 732
    invoke-virtual {p0}, Landroid/widget/FastScroller;->beginDrag()V

    #@28
    move v5, v6

    #@29
    .line 733
    goto :goto_8

    #@2a
    .line 735
    :cond_2a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2d
    move-result v6

    #@2e
    iput v6, p0, Landroid/widget/FastScroller;->mInitialTouchY:F

    #@30
    .line 736
    invoke-virtual {p0}, Landroid/widget/FastScroller;->startPendingDrag()V

    #@33
    goto :goto_8

    #@34
    .line 738
    :cond_34
    if-ne v0, v6, :cond_9a

    #@36
    .line 739
    iget-boolean v7, p0, Landroid/widget/FastScroller;->mPendingDrag:Z

    #@38
    if-eqz v7, :cond_61

    #@3a
    .line 741
    invoke-virtual {p0}, Landroid/widget/FastScroller;->beginDrag()V

    #@3d
    .line 743
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@3f
    invoke-virtual {v7}, Landroid/widget/AbsListView;->getHeight()I

    #@42
    move-result v3

    #@43
    .line 745
    .local v3, viewHeight:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@46
    move-result v7

    #@47
    float-to-int v7, v7

    #@48
    iget v8, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@4a
    sub-int/2addr v7, v8

    #@4b
    add-int/lit8 v2, v7, 0xa

    #@4d
    .line 746
    .local v2, newThumbY:I
    if-gez v2, :cond_90

    #@4f
    .line 747
    const/4 v2, 0x0

    #@50
    .line 751
    :cond_50
    :goto_50
    iput v2, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@52
    .line 752
    iget v7, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@54
    int-to-float v7, v7

    #@55
    iget v8, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@57
    sub-int v8, v3, v8

    #@59
    int-to-float v8, v8

    #@5a
    div-float/2addr v7, v8

    #@5b
    invoke-virtual {p0, v7}, Landroid/widget/FastScroller;->scrollTo(F)V

    #@5e
    .line 754
    invoke-virtual {p0}, Landroid/widget/FastScroller;->cancelPendingDrag()V

    #@61
    .line 757
    .end local v2           #newThumbY:I
    .end local v3           #viewHeight:I
    :cond_61
    iget v7, p0, Landroid/widget/FastScroller;->mState:I

    #@63
    if-ne v7, v9, :cond_8

    #@65
    .line 758
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@67
    if-eqz v7, :cond_73

    #@69
    .line 762
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@6b
    invoke-virtual {v7, v5}, Landroid/widget/AbsListView;->requestDisallowInterceptTouchEvent(Z)V

    #@6e
    .line 763
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@70
    invoke-virtual {v7, v5}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@73
    .line 765
    :cond_73
    invoke-virtual {p0, v10}, Landroid/widget/FastScroller;->setState(I)V

    #@76
    .line 766
    iget-object v1, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@78
    .line 767
    .local v1, handler:Landroid/os/Handler;
    iget-object v5, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@7a
    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7d
    .line 768
    iget-boolean v5, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@7f
    if-nez v5, :cond_88

    #@81
    .line 769
    iget-object v5, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@83
    const-wide/16 v7, 0x3e8

    #@85
    invoke-virtual {v1, v5, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@88
    .line 772
    :cond_88
    iget-object v5, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@8a
    invoke-virtual {v5}, Landroid/widget/AbsListView;->invalidate()V

    #@8d
    move v5, v6

    #@8e
    .line 773
    goto/16 :goto_8

    #@90
    .line 748
    .end local v1           #handler:Landroid/os/Handler;
    .restart local v2       #newThumbY:I
    .restart local v3       #viewHeight:I
    :cond_90
    iget v7, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@92
    add-int/2addr v7, v2

    #@93
    if-le v7, v3, :cond_50

    #@95
    .line 749
    iget v7, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@97
    sub-int v2, v3, v7

    #@99
    goto :goto_50

    #@9a
    .line 775
    .end local v2           #newThumbY:I
    .end local v3           #viewHeight:I
    :cond_9a
    if-ne v0, v10, :cond_117

    #@9c
    .line 776
    iget-boolean v7, p0, Landroid/widget/FastScroller;->mPendingDrag:Z

    #@9e
    if-eqz v7, :cond_d5

    #@a0
    .line 777
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@a3
    move-result v4

    #@a4
    .line 778
    .local v4, y:F
    iget v7, p0, Landroid/widget/FastScroller;->mInitialTouchY:F

    #@a6
    sub-float v7, v4, v7

    #@a8
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    #@ab
    move-result v7

    #@ac
    iget v8, p0, Landroid/widget/FastScroller;->mScaledTouchSlop:I

    #@ae
    int-to-float v8, v8

    #@af
    cmpl-float v7, v7, v8

    #@b1
    if-lez v7, :cond_d5

    #@b3
    .line 779
    invoke-virtual {p0, v9}, Landroid/widget/FastScroller;->setState(I)V

    #@b6
    .line 780
    iget-object v7, p0, Landroid/widget/FastScroller;->mListAdapter:Landroid/widget/BaseAdapter;

    #@b8
    if-nez v7, :cond_c1

    #@ba
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@bc
    if-eqz v7, :cond_c1

    #@be
    .line 781
    invoke-virtual {p0}, Landroid/widget/FastScroller;->getSectionsFromIndexer()V

    #@c1
    .line 783
    :cond_c1
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@c3
    if-eqz v7, :cond_cf

    #@c5
    .line 784
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@c7
    invoke-virtual {v7, v6}, Landroid/widget/AbsListView;->requestDisallowInterceptTouchEvent(Z)V

    #@ca
    .line 785
    iget-object v7, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@cc
    invoke-virtual {v7, v6}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@cf
    .line 788
    :cond_cf
    invoke-direct {p0}, Landroid/widget/FastScroller;->cancelFling()V

    #@d2
    .line 789
    invoke-virtual {p0}, Landroid/widget/FastScroller;->cancelPendingDrag()V

    #@d5
    .line 793
    .end local v4           #y:F
    :cond_d5
    iget v7, p0, Landroid/widget/FastScroller;->mState:I

    #@d7
    if-ne v7, v9, :cond_8

    #@d9
    .line 794
    iget-object v5, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@db
    invoke-virtual {v5}, Landroid/widget/AbsListView;->getHeight()I

    #@de
    move-result v3

    #@df
    .line 796
    .restart local v3       #viewHeight:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@e2
    move-result v5

    #@e3
    float-to-int v5, v5

    #@e4
    iget v7, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@e6
    sub-int/2addr v5, v7

    #@e7
    add-int/lit8 v2, v5, 0xa

    #@e9
    .line 797
    .restart local v2       #newThumbY:I
    if-gez v2, :cond_f8

    #@eb
    .line 798
    const/4 v2, 0x0

    #@ec
    .line 802
    :cond_ec
    :goto_ec
    iget v5, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@ee
    sub-int/2addr v5, v2

    #@ef
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    #@f2
    move-result v5

    #@f3
    if-ge v5, v10, :cond_102

    #@f5
    move v5, v6

    #@f6
    .line 803
    goto/16 :goto_8

    #@f8
    .line 799
    :cond_f8
    iget v5, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@fa
    add-int/2addr v5, v2

    #@fb
    if-le v5, v3, :cond_ec

    #@fd
    .line 800
    iget v5, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@ff
    sub-int v2, v3, v5

    #@101
    goto :goto_ec

    #@102
    .line 805
    :cond_102
    iput v2, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@104
    .line 807
    iget-boolean v5, p0, Landroid/widget/FastScroller;->mScrollCompleted:Z

    #@106
    if-eqz v5, :cond_114

    #@108
    .line 808
    iget v5, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@10a
    int-to-float v5, v5

    #@10b
    iget v7, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@10d
    sub-int v7, v3, v7

    #@10f
    int-to-float v7, v7

    #@110
    div-float/2addr v5, v7

    #@111
    invoke-virtual {p0, v5}, Landroid/widget/FastScroller;->scrollTo(F)V

    #@114
    :cond_114
    move v5, v6

    #@115
    .line 810
    goto/16 :goto_8

    #@117
    .line 812
    .end local v2           #newThumbY:I
    .end local v3           #viewHeight:I
    :cond_117
    if-ne v0, v9, :cond_8

    #@119
    .line 813
    invoke-virtual {p0}, Landroid/widget/FastScroller;->cancelPendingDrag()V

    #@11c
    goto/16 :goto_8
.end method

.method scrollTo(F)V
    .registers 24
    .parameter "position"

    #@0
    .prologue
    .line 522
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@4
    move-object/from16 v19, v0

    #@6
    invoke-virtual/range {v19 .. v19}, Landroid/widget/AbsListView;->getCount()I

    #@9
    move-result v2

    #@a
    .line 523
    .local v2, count:I
    const/16 v19, 0x0

    #@c
    move/from16 v0, v19

    #@e
    move-object/from16 v1, p0

    #@10
    iput-boolean v0, v1, Landroid/widget/FastScroller;->mScrollCompleted:Z

    #@12
    .line 524
    const/high16 v19, 0x3f80

    #@14
    int-to-float v0, v2

    #@15
    move/from16 v20, v0

    #@17
    div-float v19, v19, v20

    #@19
    const/high16 v20, 0x4100

    #@1b
    div-float v7, v19, v20

    #@1d
    .line 525
    .local v7, fThreshold:F
    move-object/from16 v0, p0

    #@1f
    iget-object v0, v0, Landroid/widget/FastScroller;->mSections:[Ljava/lang/Object;

    #@21
    move-object/from16 v17, v0

    #@23
    .line 527
    .local v17, sections:[Ljava/lang/Object;
    if-eqz v17, :cond_177

    #@25
    move-object/from16 v0, v17

    #@27
    array-length v0, v0

    #@28
    move/from16 v19, v0

    #@2a
    const/16 v20, 0x1

    #@2c
    move/from16 v0, v19

    #@2e
    move/from16 v1, v20

    #@30
    if-le v0, v1, :cond_177

    #@32
    .line 528
    move-object/from16 v0, v17

    #@34
    array-length v9, v0

    #@35
    .line 529
    .local v9, nSections:I
    int-to-float v0, v9

    #@36
    move/from16 v19, v0

    #@38
    mul-float v19, v19, p1

    #@3a
    move/from16 v0, v19

    #@3c
    float-to-int v15, v0

    #@3d
    .line 530
    .local v15, section:I
    if-lt v15, v9, :cond_41

    #@3f
    .line 531
    add-int/lit8 v15, v9, -0x1

    #@41
    .line 533
    :cond_41
    move v3, v15

    #@42
    .line 534
    .local v3, exactSection:I
    move/from16 v16, v15

    #@44
    .line 535
    .local v16, sectionIndex:I
    move-object/from16 v0, p0

    #@46
    iget-object v0, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@48
    move-object/from16 v19, v0

    #@4a
    move-object/from16 v0, v19

    #@4c
    invoke-interface {v0, v15}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@4f
    move-result v8

    #@50
    .line 542
    .local v8, index:I
    move v10, v2

    #@51
    .line 543
    .local v10, nextIndex:I
    move v13, v8

    #@52
    .line 544
    .local v13, prevIndex:I
    move v14, v15

    #@53
    .line 545
    .local v14, prevSection:I
    add-int/lit8 v12, v15, 0x1

    #@55
    .line 547
    .local v12, nextSection:I
    add-int/lit8 v19, v9, -0x1

    #@57
    move/from16 v0, v19

    #@59
    if-ge v15, v0, :cond_67

    #@5b
    .line 548
    move-object/from16 v0, p0

    #@5d
    iget-object v0, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@5f
    move-object/from16 v19, v0

    #@61
    add-int/lit8 v20, v15, 0x1

    #@63
    invoke-interface/range {v19 .. v20}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@66
    move-result v10

    #@67
    .line 552
    :cond_67
    if-ne v10, v8, :cond_7e

    #@69
    .line 554
    :cond_69
    if-lez v15, :cond_7e

    #@6b
    .line 555
    add-int/lit8 v15, v15, -0x1

    #@6d
    .line 556
    move-object/from16 v0, p0

    #@6f
    iget-object v0, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@71
    move-object/from16 v19, v0

    #@73
    move-object/from16 v0, v19

    #@75
    invoke-interface {v0, v15}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@78
    move-result v13

    #@79
    .line 557
    if-eq v13, v8, :cond_97

    #@7b
    .line 558
    move v14, v15

    #@7c
    .line 559
    move/from16 v16, v15

    #@7e
    .line 574
    :cond_7e
    :goto_7e
    add-int/lit8 v11, v12, 0x1

    #@80
    .line 576
    .local v11, nextNextSection:I
    :goto_80
    if-ge v11, v9, :cond_9c

    #@82
    move-object/from16 v0, p0

    #@84
    iget-object v0, v0, Landroid/widget/FastScroller;->mSectionIndexer:Landroid/widget/SectionIndexer;

    #@86
    move-object/from16 v19, v0

    #@88
    move-object/from16 v0, v19

    #@8a
    invoke-interface {v0, v11}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    #@8d
    move-result v19

    #@8e
    move/from16 v0, v19

    #@90
    if-ne v0, v10, :cond_9c

    #@92
    .line 577
    add-int/lit8 v11, v11, 0x1

    #@94
    .line 578
    add-int/lit8 v12, v12, 0x1

    #@96
    goto :goto_80

    #@97
    .line 561
    .end local v11           #nextNextSection:I
    :cond_97
    if-nez v15, :cond_69

    #@99
    .line 564
    const/16 v16, 0x0

    #@9b
    .line 565
    goto :goto_7e

    #@9c
    .line 583
    .restart local v11       #nextNextSection:I
    :cond_9c
    int-to-float v0, v14

    #@9d
    move/from16 v19, v0

    #@9f
    int-to-float v0, v9

    #@a0
    move/from16 v20, v0

    #@a2
    div-float v6, v19, v20

    #@a4
    .line 584
    .local v6, fPrev:F
    int-to-float v0, v12

    #@a5
    move/from16 v19, v0

    #@a7
    int-to-float v0, v9

    #@a8
    move/from16 v20, v0

    #@aa
    div-float v5, v19, v20

    #@ac
    .line 585
    .local v5, fNext:F
    if-ne v14, v3, :cond_128

    #@ae
    sub-float v19, p1, v6

    #@b0
    cmpg-float v19, v19, v7

    #@b2
    if-gez v19, :cond_128

    #@b4
    .line 586
    move v8, v13

    #@b5
    .line 592
    :goto_b5
    add-int/lit8 v19, v2, -0x1

    #@b7
    move/from16 v0, v19

    #@b9
    if-le v8, v0, :cond_bd

    #@bb
    add-int/lit8 v8, v2, -0x1

    #@bd
    .line 594
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@c1
    move-object/from16 v19, v0

    #@c3
    move-object/from16 v0, v19

    #@c5
    instance-of v0, v0, Landroid/widget/ExpandableListView;

    #@c7
    move/from16 v19, v0

    #@c9
    if-eqz v19, :cond_140

    #@cb
    .line 595
    move-object/from16 v0, p0

    #@cd
    iget-object v4, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@cf
    check-cast v4, Landroid/widget/ExpandableListView;

    #@d1
    .line 596
    .local v4, expList:Landroid/widget/ExpandableListView;
    move-object/from16 v0, p0

    #@d3
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@d5
    move/from16 v19, v0

    #@d7
    add-int v19, v19, v8

    #@d9
    invoke-static/range {v19 .. v19}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    #@dc
    move-result-wide v19

    #@dd
    move-wide/from16 v0, v19

    #@df
    invoke-virtual {v4, v0, v1}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    #@e2
    move-result v19

    #@e3
    const/16 v20, 0x0

    #@e5
    move/from16 v0, v19

    #@e7
    move/from16 v1, v20

    #@e9
    invoke-virtual {v4, v0, v1}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    #@ec
    .line 620
    .end local v3           #exactSection:I
    .end local v4           #expList:Landroid/widget/ExpandableListView;
    .end local v5           #fNext:F
    .end local v6           #fPrev:F
    .end local v9           #nSections:I
    .end local v10           #nextIndex:I
    .end local v11           #nextNextSection:I
    .end local v12           #nextSection:I
    .end local v13           #prevIndex:I
    .end local v14           #prevSection:I
    .end local v15           #section:I
    :goto_ec
    if-ltz v16, :cond_1f4

    #@ee
    .line 621
    aget-object v19, v17, v16

    #@f0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f3
    move-result-object v18

    #@f4
    move-object/from16 v0, v18

    #@f6
    move-object/from16 v1, p0

    #@f8
    iput-object v0, v1, Landroid/widget/FastScroller;->mSectionText:Ljava/lang/String;

    #@fa
    .line 622
    .local v18, text:Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@fd
    move-result v19

    #@fe
    const/16 v20, 0x1

    #@100
    move/from16 v0, v19

    #@102
    move/from16 v1, v20

    #@104
    if-ne v0, v1, :cond_114

    #@106
    const/16 v19, 0x0

    #@108
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->charAt(I)C

    #@10b
    move-result v19

    #@10c
    const/16 v20, 0x20

    #@10e
    move/from16 v0, v19

    #@110
    move/from16 v1, v20

    #@112
    if-eq v0, v1, :cond_1f0

    #@114
    :cond_114
    move-object/from16 v0, v17

    #@116
    array-length v0, v0

    #@117
    move/from16 v19, v0

    #@119
    move/from16 v0, v16

    #@11b
    move/from16 v1, v19

    #@11d
    if-ge v0, v1, :cond_1f0

    #@11f
    const/16 v19, 0x1

    #@121
    :goto_121
    move/from16 v0, v19

    #@123
    move-object/from16 v1, p0

    #@125
    iput-boolean v0, v1, Landroid/widget/FastScroller;->mDrawOverlay:Z

    #@127
    .line 627
    .end local v18           #text:Ljava/lang/String;
    :goto_127
    return-void

    #@128
    .line 588
    .restart local v3       #exactSection:I
    .restart local v5       #fNext:F
    .restart local v6       #fPrev:F
    .restart local v9       #nSections:I
    .restart local v10       #nextIndex:I
    .restart local v11       #nextNextSection:I
    .restart local v12       #nextSection:I
    .restart local v13       #prevIndex:I
    .restart local v14       #prevSection:I
    .restart local v15       #section:I
    :cond_128
    sub-int v19, v10, v13

    #@12a
    move/from16 v0, v19

    #@12c
    int-to-float v0, v0

    #@12d
    move/from16 v19, v0

    #@12f
    sub-float v20, p1, v6

    #@131
    mul-float v19, v19, v20

    #@133
    sub-float v20, v5, v6

    #@135
    div-float v19, v19, v20

    #@137
    move/from16 v0, v19

    #@139
    float-to-int v0, v0

    #@13a
    move/from16 v19, v0

    #@13c
    add-int v8, v13, v19

    #@13e
    goto/16 :goto_b5

    #@140
    .line 598
    :cond_140
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@144
    move-object/from16 v19, v0

    #@146
    move-object/from16 v0, v19

    #@148
    instance-of v0, v0, Landroid/widget/ListView;

    #@14a
    move/from16 v19, v0

    #@14c
    if-eqz v19, :cond_164

    #@14e
    .line 599
    move-object/from16 v0, p0

    #@150
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@152
    move-object/from16 v19, v0

    #@154
    check-cast v19, Landroid/widget/ListView;

    #@156
    move-object/from16 v0, p0

    #@158
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@15a
    move/from16 v20, v0

    #@15c
    add-int v20, v20, v8

    #@15e
    const/16 v21, 0x0

    #@160
    invoke-virtual/range {v19 .. v21}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@163
    goto :goto_ec

    #@164
    .line 601
    :cond_164
    move-object/from16 v0, p0

    #@166
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@168
    move-object/from16 v19, v0

    #@16a
    move-object/from16 v0, p0

    #@16c
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@16e
    move/from16 v20, v0

    #@170
    add-int v20, v20, v8

    #@172
    invoke-virtual/range {v19 .. v20}, Landroid/widget/AbsListView;->setSelection(I)V

    #@175
    goto/16 :goto_ec

    #@177
    .line 604
    .end local v3           #exactSection:I
    .end local v5           #fNext:F
    .end local v6           #fPrev:F
    .end local v8           #index:I
    .end local v9           #nSections:I
    .end local v10           #nextIndex:I
    .end local v11           #nextNextSection:I
    .end local v12           #nextSection:I
    .end local v13           #prevIndex:I
    .end local v14           #prevSection:I
    .end local v15           #section:I
    .end local v16           #sectionIndex:I
    :cond_177
    int-to-float v0, v2

    #@178
    move/from16 v19, v0

    #@17a
    mul-float v19, v19, p1

    #@17c
    move/from16 v0, v19

    #@17e
    float-to-int v8, v0

    #@17f
    .line 606
    .restart local v8       #index:I
    add-int/lit8 v19, v2, -0x1

    #@181
    move/from16 v0, v19

    #@183
    if-le v8, v0, :cond_187

    #@185
    add-int/lit8 v8, v2, -0x1

    #@187
    .line 608
    :cond_187
    move-object/from16 v0, p0

    #@189
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@18b
    move-object/from16 v19, v0

    #@18d
    move-object/from16 v0, v19

    #@18f
    instance-of v0, v0, Landroid/widget/ExpandableListView;

    #@191
    move/from16 v19, v0

    #@193
    if-eqz v19, :cond_1ba

    #@195
    .line 609
    move-object/from16 v0, p0

    #@197
    iget-object v4, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@199
    check-cast v4, Landroid/widget/ExpandableListView;

    #@19b
    .line 610
    .restart local v4       #expList:Landroid/widget/ExpandableListView;
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@19f
    move/from16 v19, v0

    #@1a1
    add-int v19, v19, v8

    #@1a3
    invoke-static/range {v19 .. v19}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    #@1a6
    move-result-wide v19

    #@1a7
    move-wide/from16 v0, v19

    #@1a9
    invoke-virtual {v4, v0, v1}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    #@1ac
    move-result v19

    #@1ad
    const/16 v20, 0x0

    #@1af
    move/from16 v0, v19

    #@1b1
    move/from16 v1, v20

    #@1b3
    invoke-virtual {v4, v0, v1}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    #@1b6
    .line 617
    .end local v4           #expList:Landroid/widget/ExpandableListView;
    :goto_1b6
    const/16 v16, -0x1

    #@1b8
    .restart local v16       #sectionIndex:I
    goto/16 :goto_ec

    #@1ba
    .line 612
    .end local v16           #sectionIndex:I
    :cond_1ba
    move-object/from16 v0, p0

    #@1bc
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1be
    move-object/from16 v19, v0

    #@1c0
    move-object/from16 v0, v19

    #@1c2
    instance-of v0, v0, Landroid/widget/ListView;

    #@1c4
    move/from16 v19, v0

    #@1c6
    if-eqz v19, :cond_1de

    #@1c8
    .line 613
    move-object/from16 v0, p0

    #@1ca
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1cc
    move-object/from16 v19, v0

    #@1ce
    check-cast v19, Landroid/widget/ListView;

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@1d4
    move/from16 v20, v0

    #@1d6
    add-int v20, v20, v8

    #@1d8
    const/16 v21, 0x0

    #@1da
    invoke-virtual/range {v19 .. v21}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@1dd
    goto :goto_1b6

    #@1de
    .line 615
    :cond_1de
    move-object/from16 v0, p0

    #@1e0
    iget-object v0, v0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@1e2
    move-object/from16 v19, v0

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iget v0, v0, Landroid/widget/FastScroller;->mListOffset:I

    #@1e8
    move/from16 v20, v0

    #@1ea
    add-int v20, v20, v8

    #@1ec
    invoke-virtual/range {v19 .. v20}, Landroid/widget/AbsListView;->setSelection(I)V

    #@1ef
    goto :goto_1b6

    #@1f0
    .line 622
    .restart local v16       #sectionIndex:I
    .restart local v18       #text:Ljava/lang/String;
    :cond_1f0
    const/16 v19, 0x0

    #@1f2
    goto/16 :goto_121

    #@1f4
    .line 625
    .end local v18           #text:Ljava/lang/String;
    :cond_1f4
    const/16 v19, 0x0

    #@1f6
    move/from16 v0, v19

    #@1f8
    move-object/from16 v1, p0

    #@1fa
    iput-boolean v0, v1, Landroid/widget/FastScroller;->mDrawOverlay:Z

    #@1fc
    goto/16 :goto_127
.end method

.method public setAlwaysShow(Z)V
    .registers 6
    .parameter "alwaysShow"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 159
    iput-boolean p1, p0, Landroid/widget/FastScroller;->mAlwaysShow:Z

    #@3
    .line 160
    if-eqz p1, :cond_10

    #@5
    .line 161
    iget-object v0, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@7
    iget-object v1, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@c
    .line 162
    invoke-virtual {p0, v2}, Landroid/widget/FastScroller;->setState(I)V

    #@f
    .line 166
    :cond_f
    :goto_f
    return-void

    #@10
    .line 163
    :cond_10
    iget v0, p0, Landroid/widget/FastScroller;->mState:I

    #@12
    if-ne v0, v2, :cond_f

    #@14
    .line 164
    iget-object v0, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@16
    iget-object v1, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@18
    const-wide/16 v2, 0x5dc

    #@1a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1d
    goto :goto_f
.end method

.method public setScrollbarPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 184
    if-nez p1, :cond_b

    #@2
    .line 185
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@4
    invoke-virtual {v0}, Landroid/widget/AbsListView;->isLayoutRtl()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_15

    #@a
    const/4 p1, 0x1

    #@b
    .line 188
    :cond_b
    :goto_b
    iput p1, p0, Landroid/widget/FastScroller;->mPosition:I

    #@d
    .line 189
    packed-switch p1, :pswitch_data_1c

    #@10
    .line 192
    iget-object v0, p0, Landroid/widget/FastScroller;->mOverlayDrawableRight:Landroid/graphics/drawable/Drawable;

    #@12
    iput-object v0, p0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@14
    .line 198
    :goto_14
    return-void

    #@15
    .line 185
    :cond_15
    const/4 p1, 0x2

    #@16
    goto :goto_b

    #@17
    .line 195
    :pswitch_17
    iget-object v0, p0, Landroid/widget/FastScroller;->mOverlayDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@19
    iput-object v0, p0, Landroid/widget/FastScroller;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    #@1b
    goto :goto_14

    #@1c
    .line 189
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_17
    .end packed-switch
.end method

.method public setState(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 205
    packed-switch p1, :pswitch_data_3e

    #@3
    .line 223
    :goto_3
    :pswitch_3
    iput p1, p0, Landroid/widget/FastScroller;->mState:I

    #@5
    .line 224
    invoke-direct {p0}, Landroid/widget/FastScroller;->refreshDrawableState()V

    #@8
    .line 225
    return-void

    #@9
    .line 207
    :pswitch_9
    iget-object v1, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@b
    iget-object v2, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@d
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@10
    .line 208
    iget-object v1, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@12
    invoke-virtual {v1}, Landroid/widget/AbsListView;->invalidate()V

    #@15
    goto :goto_3

    #@16
    .line 211
    :pswitch_16
    iget v1, p0, Landroid/widget/FastScroller;->mState:I

    #@18
    const/4 v2, 0x2

    #@19
    if-eq v1, v2, :cond_1e

    #@1b
    .line 212
    invoke-direct {p0}, Landroid/widget/FastScroller;->resetThumbPos()V

    #@1e
    .line 216
    :cond_1e
    :pswitch_1e
    iget-object v1, p0, Landroid/widget/FastScroller;->mHandler:Landroid/os/Handler;

    #@20
    iget-object v2, p0, Landroid/widget/FastScroller;->mScrollFade:Landroid/widget/FastScroller$ScrollFade;

    #@22
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@25
    goto :goto_3

    #@26
    .line 219
    :pswitch_26
    iget-object v1, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@28
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getWidth()I

    #@2b
    move-result v0

    #@2c
    .line 220
    .local v0, viewWidth:I
    iget-object v1, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@2e
    iget v2, p0, Landroid/widget/FastScroller;->mThumbW:I

    #@30
    sub-int v2, v0, v2

    #@32
    iget v3, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@34
    iget v4, p0, Landroid/widget/FastScroller;->mThumbY:I

    #@36
    iget v5, p0, Landroid/widget/FastScroller;->mThumbH:I

    #@38
    add-int/2addr v4, v5

    #@39
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/widget/AbsListView;->invalidate(IIII)V

    #@3c
    goto :goto_3

    #@3d
    .line 205
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_9
        :pswitch_3
        :pswitch_16
        :pswitch_1e
        :pswitch_26
    .end packed-switch
.end method

.method startPendingDrag()V
    .registers 5

    #@0
    .prologue
    .line 685
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/FastScroller;->mPendingDrag:Z

    #@3
    .line 686
    iget-object v0, p0, Landroid/widget/FastScroller;->mList:Landroid/widget/AbsListView;

    #@5
    iget-object v1, p0, Landroid/widget/FastScroller;->mDeferStartDrag:Ljava/lang/Runnable;

    #@7
    const-wide/16 v2, 0xb4

    #@9
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@c
    .line 687
    return-void
.end method

.method stop()V
    .registers 2

    #@0
    .prologue
    .line 306
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/FastScroller;->setState(I)V

    #@4
    .line 307
    return-void
.end method
