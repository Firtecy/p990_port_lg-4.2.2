.class public Landroid/widget/FrameLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "FrameLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/FrameLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public gravity:I


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 624
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@3
    .line 602
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@6
    .line 625
    return-void
.end method

.method public constructor <init>(III)V
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "gravity"

    #@0
    .prologue
    .line 640
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@3
    .line 602
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@6
    .line 641
    iput p3, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@8
    .line 642
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 608
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 602
    iput v2, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@6
    .line 610
    sget-object v1, Lcom/android/internal/R$styleable;->FrameLayout_Layout:[I

    #@8
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 611
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@13
    .line 612
    invoke-static {}, Landroid/widget/FrameLayout;->access$000()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_2e

    #@19
    .line 613
    iget v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@1b
    and-int/lit8 v1, v1, 0x3

    #@1d
    const/4 v2, 0x3

    #@1e
    if-eq v1, v2, :cond_27

    #@20
    iget v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@22
    and-int/lit8 v1, v1, 0x5

    #@24
    const/4 v2, 0x5

    #@25
    if-ne v1, v2, :cond_2e

    #@27
    .line 614
    :cond_27
    iget v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@29
    const/high16 v2, 0x80

    #@2b
    or-int/2addr v1, v2

    #@2c
    iput v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@2e
    .line 617
    :cond_2e
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@31
    .line 618
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 648
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 602
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@6
    .line 649
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 655
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 602
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@6
    .line 656
    return-void
.end method
