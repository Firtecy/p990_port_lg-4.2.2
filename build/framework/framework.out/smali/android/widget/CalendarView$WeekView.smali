.class Landroid/widget/CalendarView$WeekView;
.super Landroid/view/View;
.source "CalendarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/CalendarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeekView"
.end annotation


# instance fields
.field private mDayNumbers:[Ljava/lang/String;

.field private final mDrawPaint:Landroid/graphics/Paint;

.field private mFirstDay:Ljava/util/Calendar;

.field private mFocusDay:[Z

.field private mHasFocusedDay:Z

.field private mHasSelectedDay:Z

.field private mHasUnfocusedDay:Z

.field private mHeight:I

.field private mLastWeekDayMonth:I

.field private final mMonthNumDrawPaint:Landroid/graphics/Paint;

.field private mMonthOfFirstWeekDay:I

.field private mNumCells:I

.field private mSelectedDay:I

.field private mSelectedLeft:I

.field private mSelectedRight:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mWeek:I

.field private mWidth:I

.field final synthetic this$0:Landroid/widget/CalendarView;


# direct methods
.method public constructor <init>(Landroid/widget/CalendarView;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1544
    iput-object p1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@3
    .line 1545
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@6
    .line 1492
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@d
    .line 1494
    new-instance v0, Landroid/graphics/Paint;

    #@f
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@12
    iput-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@14
    .line 1496
    new-instance v0, Landroid/graphics/Paint;

    #@16
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@19
    iput-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@1b
    .line 1514
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mMonthOfFirstWeekDay:I

    #@1d
    .line 1517
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mLastWeekDayMonth:I

    #@1f
    .line 1521
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mWeek:I

    #@21
    .line 1530
    const/4 v0, 0x0

    #@22
    iput-boolean v0, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@24
    .line 1533
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mSelectedDay:I

    #@26
    .line 1539
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@28
    .line 1542
    iput v1, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@2a
    .line 1548
    invoke-direct {p0}, Landroid/widget/CalendarView$WeekView;->initilaizePaints()V

    #@2d
    .line 1549
    return-void
.end method

.method static synthetic access$100(Landroid/widget/CalendarView$WeekView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1490
    iget-boolean v0, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/CalendarView$WeekView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1490
    iget-boolean v0, p0, Landroid/widget/CalendarView$WeekView;->mHasFocusedDay:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/CalendarView$WeekView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1490
    iget-boolean v0, p0, Landroid/widget/CalendarView$WeekView;->mHasUnfocusedDay:Z

    #@2
    return v0
.end method

.method private drawBackground(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1711
    iget-boolean v2, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1738
    :goto_5
    return-void

    #@6
    .line 1714
    :cond_6
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@8
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@a
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2700(Landroid/widget/CalendarView;)I

    #@d
    move-result v3

    #@e
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    #@11
    .line 1716
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@13
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@15
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2800(Landroid/widget/CalendarView;)I

    #@18
    move-result v3

    #@19
    iput v3, v2, Landroid/graphics/Rect;->top:I

    #@1b
    .line 1717
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@1d
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@1f
    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    #@21
    .line 1719
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeekView;->isLayoutRtl()Z

    #@24
    move-result v0

    #@25
    .line 1721
    .local v0, isLayoutRtl:Z
    if-eqz v0, :cond_60

    #@27
    .line 1722
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@29
    iput v1, v2, Landroid/graphics/Rect;->left:I

    #@2b
    .line 1723
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@2d
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@2f
    add-int/lit8 v2, v2, -0x2

    #@31
    iput v2, v1, Landroid/graphics/Rect;->right:I

    #@33
    .line 1728
    :goto_33
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@35
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@37
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@3a
    .line 1730
    if-eqz v0, :cond_7d

    #@3c
    .line 1731
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@3e
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@40
    add-int/lit8 v2, v2, 0x3

    #@42
    iput v2, v1, Landroid/graphics/Rect;->left:I

    #@44
    .line 1732
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@46
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@48
    invoke-static {v1}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@4b
    move-result v1

    #@4c
    if-eqz v1, :cond_7a

    #@4e
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@50
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@52
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@54
    div-int/2addr v3, v4

    #@55
    sub-int/2addr v1, v3

    #@56
    :goto_56
    iput v1, v2, Landroid/graphics/Rect;->right:I

    #@58
    .line 1737
    :goto_58
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@5a
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@5c
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@5f
    goto :goto_5

    #@60
    .line 1725
    :cond_60
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@62
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@64
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@67
    move-result v3

    #@68
    if-eqz v3, :cond_6f

    #@6a
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@6c
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@6e
    div-int/2addr v1, v3

    #@6f
    :cond_6f
    iput v1, v2, Landroid/graphics/Rect;->left:I

    #@71
    .line 1726
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@73
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@75
    add-int/lit8 v2, v2, -0x2

    #@77
    iput v2, v1, Landroid/graphics/Rect;->right:I

    #@79
    goto :goto_33

    #@7a
    .line 1732
    :cond_7a
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@7c
    goto :goto_56

    #@7d
    .line 1734
    :cond_7d
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@7f
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@81
    add-int/lit8 v2, v2, 0x3

    #@83
    iput v2, v1, Landroid/graphics/Rect;->left:I

    #@85
    .line 1735
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->mTempRect:Landroid/graphics/Rect;

    #@87
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@89
    iput v2, v1, Landroid/graphics/Rect;->right:I

    #@8b
    goto :goto_58
.end method

.method private drawSelectedDateVerticalBars(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    #@0
    .prologue
    .line 1818
    iget-boolean v0, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1829
    :goto_4
    return-void

    #@5
    .line 1821
    :cond_5
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@7
    invoke-static {v0}, Landroid/widget/CalendarView;->access$3400(Landroid/widget/CalendarView;)Landroid/graphics/drawable/Drawable;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@d
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@f
    invoke-static {v2}, Landroid/widget/CalendarView;->access$3300(Landroid/widget/CalendarView;)I

    #@12
    move-result v2

    #@13
    div-int/lit8 v2, v2, 0x2

    #@15
    sub-int/2addr v1, v2

    #@16
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@18
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2800(Landroid/widget/CalendarView;)I

    #@1b
    move-result v2

    #@1c
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@1e
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@20
    invoke-static {v4}, Landroid/widget/CalendarView;->access$3300(Landroid/widget/CalendarView;)I

    #@23
    move-result v4

    #@24
    div-int/lit8 v4, v4, 0x2

    #@26
    add-int/2addr v3, v4

    #@27
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@29
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2c
    .line 1824
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@2e
    invoke-static {v0}, Landroid/widget/CalendarView;->access$3400(Landroid/widget/CalendarView;)Landroid/graphics/drawable/Drawable;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@35
    .line 1825
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@37
    invoke-static {v0}, Landroid/widget/CalendarView;->access$3400(Landroid/widget/CalendarView;)Landroid/graphics/drawable/Drawable;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@3d
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@3f
    invoke-static {v2}, Landroid/widget/CalendarView;->access$3300(Landroid/widget/CalendarView;)I

    #@42
    move-result v2

    #@43
    div-int/lit8 v2, v2, 0x2

    #@45
    sub-int/2addr v1, v2

    #@46
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@48
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2800(Landroid/widget/CalendarView;)I

    #@4b
    move-result v2

    #@4c
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@4e
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@50
    invoke-static {v4}, Landroid/widget/CalendarView;->access$3300(Landroid/widget/CalendarView;)I

    #@53
    move-result v4

    #@54
    div-int/lit8 v4, v4, 0x2

    #@56
    add-int/2addr v3, v4

    #@57
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@59
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@5c
    .line 1828
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@5e
    invoke-static {v0}, Landroid/widget/CalendarView;->access$3400(Landroid/widget/CalendarView;)Landroid/graphics/drawable/Drawable;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@65
    goto :goto_4
.end method

.method private drawWeekNumbersAndDates(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 1746
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@3
    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    #@6
    move-result v3

    #@7
    .line 1747
    .local v3, textHeight:F
    iget v6, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@9
    int-to-float v6, v6

    #@a
    add-float/2addr v6, v3

    #@b
    const/high16 v7, 0x4000

    #@d
    div-float/2addr v6, v7

    #@e
    float-to-int v6, v6

    #@f
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@11
    invoke-static {v7}, Landroid/widget/CalendarView;->access$2800(Landroid/widget/CalendarView;)I

    #@14
    move-result v7

    #@15
    sub-int v5, v6, v7

    #@17
    .line 1748
    .local v5, y:I
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@19
    .line 1749
    .local v2, nDays:I
    mul-int/lit8 v0, v2, 0x2

    #@1b
    .line 1751
    .local v0, divisor:I
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@1d
    sget-object v7, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@1f
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    #@22
    .line 1752
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@24
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@26
    invoke-static {v7}, Landroid/widget/CalendarView;->access$2600(Landroid/widget/CalendarView;)I

    #@29
    move-result v7

    #@2a
    int-to-float v7, v7

    #@2b
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    #@2e
    .line 1754
    const/4 v1, 0x0

    #@2f
    .line 1756
    .local v1, i:I
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeekView;->isLayoutRtl()Z

    #@32
    move-result v6

    #@33
    if-eqz v6, :cond_91

    #@35
    .line 1757
    :goto_35
    add-int/lit8 v6, v2, -0x1

    #@37
    if-ge v1, v6, :cond_6b

    #@39
    .line 1758
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@3b
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mFocusDay:[Z

    #@3d
    aget-boolean v6, v6, v1

    #@3f
    if-eqz v6, :cond_64

    #@41
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@43
    invoke-static {v6}, Landroid/widget/CalendarView;->access$2900(Landroid/widget/CalendarView;)I

    #@46
    move-result v6

    #@47
    :goto_47
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    #@4a
    .line 1760
    mul-int/lit8 v6, v1, 0x2

    #@4c
    add-int/lit8 v6, v6, 0x1

    #@4e
    iget v7, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@50
    mul-int/2addr v6, v7

    #@51
    div-int v4, v6, v0

    #@53
    .line 1761
    .local v4, x:I
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@55
    add-int/lit8 v7, v2, -0x1

    #@57
    sub-int/2addr v7, v1

    #@58
    aget-object v6, v6, v7

    #@5a
    int-to-float v7, v4

    #@5b
    int-to-float v8, v5

    #@5c
    iget-object v9, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@5e
    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@61
    .line 1757
    add-int/lit8 v1, v1, 0x1

    #@63
    goto :goto_35

    #@64
    .line 1758
    .end local v4           #x:I
    :cond_64
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@66
    invoke-static {v6}, Landroid/widget/CalendarView;->access$3000(Landroid/widget/CalendarView;)I

    #@69
    move-result v6

    #@6a
    goto :goto_47

    #@6b
    .line 1763
    :cond_6b
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@6d
    invoke-static {v6}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_90

    #@73
    .line 1764
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@75
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@77
    invoke-static {v7}, Landroid/widget/CalendarView;->access$3100(Landroid/widget/CalendarView;)I

    #@7a
    move-result v7

    #@7b
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    #@7e
    .line 1765
    iget v6, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@80
    iget v7, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@82
    div-int/2addr v7, v0

    #@83
    sub-int v4, v6, v7

    #@85
    .line 1766
    .restart local v4       #x:I
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@87
    aget-object v6, v6, v10

    #@89
    int-to-float v7, v4

    #@8a
    int-to-float v8, v5

    #@8b
    iget-object v9, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@8d
    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@90
    .line 1782
    .end local v4           #x:I
    :cond_90
    return-void

    #@91
    .line 1769
    :cond_91
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@93
    invoke-static {v6}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@96
    move-result v6

    #@97
    if-eqz v6, :cond_b5

    #@99
    .line 1770
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@9b
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@9d
    invoke-static {v7}, Landroid/widget/CalendarView;->access$3100(Landroid/widget/CalendarView;)I

    #@a0
    move-result v7

    #@a1
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    #@a4
    .line 1771
    iget v6, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@a6
    div-int v4, v6, v0

    #@a8
    .line 1772
    .restart local v4       #x:I
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@aa
    aget-object v6, v6, v10

    #@ac
    int-to-float v7, v4

    #@ad
    int-to-float v8, v5

    #@ae
    iget-object v9, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@b0
    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@b3
    .line 1773
    add-int/lit8 v1, v1, 0x1

    #@b5
    .line 1775
    .end local v4           #x:I
    :cond_b5
    :goto_b5
    if-ge v1, v2, :cond_90

    #@b7
    .line 1776
    iget-object v7, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@b9
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mFocusDay:[Z

    #@bb
    aget-boolean v6, v6, v1

    #@bd
    if-eqz v6, :cond_df

    #@bf
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@c1
    invoke-static {v6}, Landroid/widget/CalendarView;->access$2900(Landroid/widget/CalendarView;)I

    #@c4
    move-result v6

    #@c5
    :goto_c5
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    #@c8
    .line 1778
    mul-int/lit8 v6, v1, 0x2

    #@ca
    add-int/lit8 v6, v6, 0x1

    #@cc
    iget v7, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@ce
    mul-int/2addr v6, v7

    #@cf
    div-int v4, v6, v0

    #@d1
    .line 1779
    .restart local v4       #x:I
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@d3
    aget-object v6, v6, v1

    #@d5
    int-to-float v7, v4

    #@d6
    int-to-float v8, v5

    #@d7
    iget-object v9, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@d9
    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@dc
    .line 1775
    add-int/lit8 v1, v1, 0x1

    #@de
    goto :goto_b5

    #@df
    .line 1776
    .end local v4           #x:I
    :cond_df
    iget-object v6, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@e1
    invoke-static {v6}, Landroid/widget/CalendarView;->access$3000(Landroid/widget/CalendarView;)I

    #@e4
    move-result v6

    #@e5
    goto :goto_c5
.end method

.method private drawWeekSeparators(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "canvas"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1791
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@3
    invoke-static {v0}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    #@a
    move-result v6

    #@b
    .line 1792
    .local v6, firstFullyVisiblePosition:I
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@d
    invoke-static {v0}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@10
    move-result-object v0

    #@11
    const/4 v4, 0x0

    #@12
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@19
    move-result v0

    #@1a
    if-gez v0, :cond_1e

    #@1c
    .line 1793
    add-int/lit8 v6, v6, 0x1

    #@1e
    .line 1795
    :cond_1e
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mWeek:I

    #@20
    if-ne v6, v0, :cond_23

    #@22
    .line 1810
    :goto_22
    return-void

    #@23
    .line 1798
    :cond_23
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@25
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@27
    invoke-static {v4}, Landroid/widget/CalendarView;->access$3200(Landroid/widget/CalendarView;)I

    #@2a
    move-result v4

    #@2b
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@2e
    .line 1799
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@30
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@32
    invoke-static {v4}, Landroid/widget/CalendarView;->access$2800(Landroid/widget/CalendarView;)I

    #@35
    move-result v4

    #@36
    int-to-float v4, v4

    #@37
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@3a
    .line 1802
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeekView;->isLayoutRtl()Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_5e

    #@40
    .line 1803
    const/4 v1, 0x0

    #@41
    .line 1804
    .local v1, startX:F
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@43
    invoke-static {v0}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_5a

    #@49
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@4b
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@4d
    iget v5, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@4f
    div-int/2addr v4, v5

    #@50
    sub-int/2addr v0, v4

    #@51
    int-to-float v3, v0

    #@52
    .line 1809
    .local v3, stopX:F
    :goto_52
    iget-object v5, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@54
    move-object v0, p1

    #@55
    move v4, v2

    #@56
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@59
    goto :goto_22

    #@5a
    .line 1804
    .end local v3           #stopX:F
    :cond_5a
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@5c
    int-to-float v3, v0

    #@5d
    goto :goto_52

    #@5e
    .line 1806
    .end local v1           #startX:F
    :cond_5e
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@60
    invoke-static {v0}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_70

    #@66
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@68
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@6a
    div-int/2addr v0, v4

    #@6b
    int-to-float v1, v0

    #@6c
    .line 1807
    .restart local v1       #startX:F
    :goto_6c
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@6e
    int-to-float v3, v0

    #@6f
    .restart local v3       #stopX:F
    goto :goto_52

    #@70
    .end local v1           #startX:F
    .end local v3           #stopX:F
    :cond_70
    move v1, v2

    #@71
    .line 1806
    goto :goto_6c
.end method

.method private initilaizePaints()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1620
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    #@7
    .line 1621
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@9
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@c
    .line 1622
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mDrawPaint:Landroid/graphics/Paint;

    #@e
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@10
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@13
    .line 1624
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@15
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    #@18
    .line 1625
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@1a
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@1d
    .line 1626
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@1f
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@21
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@24
    .line 1627
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@26
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@28
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    #@2b
    .line 1628
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthNumDrawPaint:Landroid/graphics/Paint;

    #@2d
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@2f
    invoke-static {v1}, Landroid/widget/CalendarView;->access$2600(Landroid/widget/CalendarView;)I

    #@32
    move-result v1

    #@33
    int-to-float v1, v1

    #@34
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    #@37
    .line 1629
    return-void
.end method

.method private updateSelectionPositions()V
    .registers 6

    #@0
    .prologue
    .line 1841
    iget-boolean v2, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@2
    if-eqz v2, :cond_3f

    #@4
    .line 1842
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeekView;->isLayoutRtl()Z

    #@7
    move-result v0

    #@8
    .line 1843
    .local v0, isLayoutRtl:Z
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedDay:I

    #@a
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@c
    invoke-static {v3}, Landroid/widget/CalendarView;->access$1900(Landroid/widget/CalendarView;)I

    #@f
    move-result v3

    #@10
    sub-int v1, v2, v3

    #@12
    .line 1844
    .local v1, selectedPosition:I
    if-gez v1, :cond_16

    #@14
    .line 1845
    add-int/lit8 v1, v1, 0x7

    #@16
    .line 1847
    :cond_16
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@18
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_22

    #@1e
    if-nez v0, :cond_22

    #@20
    .line 1848
    add-int/lit8 v1, v1, 0x1

    #@22
    .line 1850
    :cond_22
    if-eqz v0, :cond_40

    #@24
    .line 1851
    iget-object v2, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@26
    invoke-static {v2}, Landroid/widget/CalendarView;->access$2500(Landroid/widget/CalendarView;)I

    #@29
    move-result v2

    #@2a
    add-int/lit8 v2, v2, -0x1

    #@2c
    sub-int/2addr v2, v1

    #@2d
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@2f
    mul-int/2addr v2, v3

    #@30
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@32
    div-int/2addr v2, v3

    #@33
    iput v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@35
    .line 1856
    :goto_35
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@37
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@39
    iget v4, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@3b
    div-int/2addr v3, v4

    #@3c
    add-int/2addr v2, v3

    #@3d
    iput v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedRight:I

    #@3f
    .line 1858
    .end local v0           #isLayoutRtl:Z
    .end local v1           #selectedPosition:I
    :cond_3f
    return-void

    #@40
    .line 1854
    .restart local v0       #isLayoutRtl:Z
    .restart local v1       #selectedPosition:I
    :cond_40
    iget v2, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@42
    mul-int/2addr v2, v1

    #@43
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@45
    div-int/2addr v2, v3

    #@46
    iput v2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedLeft:I

    #@48
    goto :goto_35
.end method


# virtual methods
.method public getDayFromLocation(FLjava/util/Calendar;)Z
    .registers 11
    .parameter "x"
    .parameter "outCalendar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1666
    invoke-virtual {p0}, Landroid/widget/CalendarView$WeekView;->isLayoutRtl()Z

    #@4
    move-result v2

    #@5
    .line 1671
    .local v2, isLayoutRtl:Z
    if-eqz v2, :cond_2a

    #@7
    .line 1672
    const/4 v3, 0x0

    #@8
    .line 1673
    .local v3, start:I
    iget-object v5, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@a
    invoke-static {v5}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_27

    #@10
    iget v5, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@12
    iget v6, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@14
    iget v7, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@16
    div-int/2addr v6, v7

    #@17
    sub-int v1, v5, v6

    #@19
    .line 1679
    .local v1, end:I
    :goto_19
    int-to-float v5, v3

    #@1a
    cmpg-float v5, p1, v5

    #@1c
    if-ltz v5, :cond_23

    #@1e
    int-to-float v5, v1

    #@1f
    cmpl-float v5, p1, v5

    #@21
    if-lez v5, :cond_3d

    #@23
    .line 1680
    :cond_23
    invoke-virtual {p2}, Ljava/util/Calendar;->clear()V

    #@26
    .line 1694
    :goto_26
    return v4

    #@27
    .line 1673
    .end local v1           #end:I
    :cond_27
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@29
    goto :goto_19

    #@2a
    .line 1675
    .end local v3           #start:I
    :cond_2a
    iget-object v5, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@2c
    invoke-static {v5}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@2f
    move-result v5

    #@30
    if-eqz v5, :cond_3b

    #@32
    iget v5, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@34
    iget v6, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@36
    div-int v3, v5, v6

    #@38
    .line 1676
    .restart local v3       #start:I
    :goto_38
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@3a
    .restart local v1       #end:I
    goto :goto_19

    #@3b
    .end local v1           #end:I
    .end local v3           #start:I
    :cond_3b
    move v3, v4

    #@3c
    .line 1675
    goto :goto_38

    #@3d
    .line 1685
    .restart local v1       #end:I
    .restart local v3       #start:I
    :cond_3d
    int-to-float v4, v3

    #@3e
    sub-float v4, p1, v4

    #@40
    iget-object v5, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@42
    invoke-static {v5}, Landroid/widget/CalendarView;->access$2500(Landroid/widget/CalendarView;)I

    #@45
    move-result v5

    #@46
    int-to-float v5, v5

    #@47
    mul-float/2addr v4, v5

    #@48
    sub-int v5, v1, v3

    #@4a
    int-to-float v5, v5

    #@4b
    div-float/2addr v4, v5

    #@4c
    float-to-int v0, v4

    #@4d
    .line 1687
    .local v0, dayPosition:I
    if-eqz v2, :cond_59

    #@4f
    .line 1688
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@51
    invoke-static {v4}, Landroid/widget/CalendarView;->access$2500(Landroid/widget/CalendarView;)I

    #@54
    move-result v4

    #@55
    add-int/lit8 v4, v4, -0x1

    #@57
    sub-int v0, v4, v0

    #@59
    .line 1691
    :cond_59
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->mFirstDay:Ljava/util/Calendar;

    #@5b
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    #@5e
    move-result-wide v4

    #@5f
    invoke-virtual {p2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@62
    .line 1692
    const/4 v4, 0x5

    #@63
    invoke-virtual {p2, v4, v0}, Ljava/util/Calendar;->add(II)V

    #@66
    .line 1694
    const/4 v4, 0x1

    #@67
    goto :goto_26
.end method

.method public getFirstDay()Ljava/util/Calendar;
    .registers 2

    #@0
    .prologue
    .line 1655
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->mFirstDay:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method public getMonthOfFirstWeekDay()I
    .registers 2

    #@0
    .prologue
    .line 1637
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mMonthOfFirstWeekDay:I

    #@2
    return v0
.end method

.method public getMonthOfLastWeekDay()I
    .registers 2

    #@0
    .prologue
    .line 1646
    iget v0, p0, Landroid/widget/CalendarView$WeekView;->mLastWeekDayMonth:I

    #@2
    return v0
.end method

.method public init(III)V
    .registers 15
    .parameter "weekNumber"
    .parameter "selectedWeekDay"
    .parameter "focusedMonth"

    #@0
    .prologue
    .line 1563
    iput p2, p0, Landroid/widget/CalendarView$WeekView;->mSelectedDay:I

    #@2
    .line 1564
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mSelectedDay:I

    #@4
    const/4 v4, -0x1

    #@5
    if-eq v3, v4, :cond_11d

    #@7
    const/4 v3, 0x1

    #@8
    :goto_8
    iput-boolean v3, p0, Landroid/widget/CalendarView$WeekView;->mHasSelectedDay:Z

    #@a
    .line 1565
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@c
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_120

    #@12
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@14
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2500(Landroid/widget/CalendarView;)I

    #@17
    move-result v3

    #@18
    add-int/lit8 v3, v3, 0x1

    #@1a
    :goto_1a
    iput v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@1c
    .line 1566
    iput p1, p0, Landroid/widget/CalendarView$WeekView;->mWeek:I

    #@1e
    .line 1567
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@20
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@23
    move-result-object v3

    #@24
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@26
    invoke-static {v4}, Landroid/widget/CalendarView;->access$1800(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2d
    move-result-wide v4

    #@2e
    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@31
    .line 1569
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@33
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@36
    move-result-object v3

    #@37
    const/4 v4, 0x3

    #@38
    iget v5, p0, Landroid/widget/CalendarView$WeekView;->mWeek:I

    #@3a
    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    #@3d
    .line 1570
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@3f
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@42
    move-result-object v3

    #@43
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@45
    invoke-static {v4}, Landroid/widget/CalendarView;->access$1900(Landroid/widget/CalendarView;)I

    #@48
    move-result v4

    #@49
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    #@4c
    .line 1573
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@4e
    new-array v3, v3, [Ljava/lang/String;

    #@50
    iput-object v3, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@52
    .line 1574
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@54
    new-array v3, v3, [Z

    #@56
    iput-object v3, p0, Landroid/widget/CalendarView$WeekView;->mFocusDay:[Z

    #@58
    .line 1577
    const/4 v1, 0x0

    #@59
    .line 1578
    .local v1, i:I
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@5b
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2400(Landroid/widget/CalendarView;)Z

    #@5e
    move-result v3

    #@5f
    if-eqz v3, :cond_87

    #@61
    .line 1579
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@63
    const/4 v4, 0x0

    #@64
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@67
    move-result-object v5

    #@68
    const-string v6, "%d"

    #@6a
    const/4 v7, 0x1

    #@6b
    new-array v7, v7, [Ljava/lang/Object;

    #@6d
    const/4 v8, 0x0

    #@6e
    iget-object v9, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@70
    invoke-static {v9}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@73
    move-result-object v9

    #@74
    const/4 v10, 0x3

    #@75
    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    #@78
    move-result v9

    #@79
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7c
    move-result-object v9

    #@7d
    aput-object v9, v7, v8

    #@7f
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    aput-object v5, v3, v4

    #@85
    .line 1581
    add-int/lit8 v1, v1, 0x1

    #@87
    .line 1585
    :cond_87
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@89
    invoke-static {v3}, Landroid/widget/CalendarView;->access$1900(Landroid/widget/CalendarView;)I

    #@8c
    move-result v3

    #@8d
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@8f
    invoke-static {v4}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@92
    move-result-object v4

    #@93
    const/4 v5, 0x7

    #@94
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    #@97
    move-result v4

    #@98
    sub-int v0, v3, v4

    #@9a
    .line 1586
    .local v0, diff:I
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@9c
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@9f
    move-result-object v3

    #@a0
    const/4 v4, 0x5

    #@a1
    invoke-virtual {v3, v4, v0}, Ljava/util/Calendar;->add(II)V

    #@a4
    .line 1588
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@a6
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@a9
    move-result-object v3

    #@aa
    invoke-virtual {v3}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    #@ad
    move-result-object v3

    #@ae
    check-cast v3, Ljava/util/Calendar;

    #@b0
    iput-object v3, p0, Landroid/widget/CalendarView$WeekView;->mFirstDay:Ljava/util/Calendar;

    #@b2
    .line 1589
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@b4
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@b7
    move-result-object v3

    #@b8
    const/4 v4, 0x2

    #@b9
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    #@bc
    move-result v3

    #@bd
    iput v3, p0, Landroid/widget/CalendarView$WeekView;->mMonthOfFirstWeekDay:I

    #@bf
    .line 1591
    const/4 v3, 0x1

    #@c0
    iput-boolean v3, p0, Landroid/widget/CalendarView$WeekView;->mHasUnfocusedDay:Z

    #@c2
    .line 1592
    :goto_c2
    iget v3, p0, Landroid/widget/CalendarView$WeekView;->mNumCells:I

    #@c4
    if-ge v1, v3, :cond_150

    #@c6
    .line 1593
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@c8
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@cb
    move-result-object v3

    #@cc
    const/4 v4, 0x2

    #@cd
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    #@d0
    move-result v3

    #@d1
    if-ne v3, p3, :cond_128

    #@d3
    const/4 v2, 0x1

    #@d4
    .line 1594
    .local v2, isFocusedDay:Z
    :goto_d4
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->mFocusDay:[Z

    #@d6
    aput-boolean v2, v3, v1

    #@d8
    .line 1595
    iget-boolean v3, p0, Landroid/widget/CalendarView$WeekView;->mHasFocusedDay:Z

    #@da
    or-int/2addr v3, v2

    #@db
    iput-boolean v3, p0, Landroid/widget/CalendarView$WeekView;->mHasFocusedDay:Z

    #@dd
    .line 1596
    iget-boolean v4, p0, Landroid/widget/CalendarView$WeekView;->mHasUnfocusedDay:Z

    #@df
    if-nez v2, :cond_12a

    #@e1
    const/4 v3, 0x1

    #@e2
    :goto_e2
    and-int/2addr v3, v4

    #@e3
    iput-boolean v3, p0, Landroid/widget/CalendarView$WeekView;->mHasUnfocusedDay:Z

    #@e5
    .line 1598
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@e7
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@ea
    move-result-object v3

    #@eb
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@ed
    invoke-static {v4}, Landroid/widget/CalendarView;->access$1800(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@f0
    move-result-object v4

    #@f1
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@f4
    move-result v3

    #@f5
    if-nez v3, :cond_109

    #@f7
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@f9
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@fc
    move-result-object v3

    #@fd
    iget-object v4, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@ff
    invoke-static {v4}, Landroid/widget/CalendarView;->access$1700(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@102
    move-result-object v4

    #@103
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@106
    move-result v3

    #@107
    if-eqz v3, :cond_12c

    #@109
    .line 1599
    :cond_109
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@10b
    const-string v4, ""

    #@10d
    aput-object v4, v3, v1

    #@10f
    .line 1604
    :goto_10f
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@111
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@114
    move-result-object v3

    #@115
    const/4 v4, 0x5

    #@116
    const/4 v5, 0x1

    #@117
    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    #@11a
    .line 1592
    add-int/lit8 v1, v1, 0x1

    #@11c
    goto :goto_c2

    #@11d
    .line 1564
    .end local v0           #diff:I
    .end local v1           #i:I
    .end local v2           #isFocusedDay:Z
    :cond_11d
    const/4 v3, 0x0

    #@11e
    goto/16 :goto_8

    #@120
    .line 1565
    :cond_120
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@122
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2500(Landroid/widget/CalendarView;)I

    #@125
    move-result v3

    #@126
    goto/16 :goto_1a

    #@128
    .line 1593
    .restart local v0       #diff:I
    .restart local v1       #i:I
    :cond_128
    const/4 v2, 0x0

    #@129
    goto :goto_d4

    #@12a
    .line 1596
    .restart local v2       #isFocusedDay:Z
    :cond_12a
    const/4 v3, 0x0

    #@12b
    goto :goto_e2

    #@12c
    .line 1601
    :cond_12c
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->mDayNumbers:[Ljava/lang/String;

    #@12e
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@131
    move-result-object v4

    #@132
    const-string v5, "%d"

    #@134
    const/4 v6, 0x1

    #@135
    new-array v6, v6, [Ljava/lang/Object;

    #@137
    const/4 v7, 0x0

    #@138
    iget-object v8, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@13a
    invoke-static {v8}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@13d
    move-result-object v8

    #@13e
    const/4 v9, 0x5

    #@13f
    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    #@142
    move-result v8

    #@143
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@146
    move-result-object v8

    #@147
    aput-object v8, v6, v7

    #@149
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14c
    move-result-object v4

    #@14d
    aput-object v4, v3, v1

    #@14f
    goto :goto_10f

    #@150
    .line 1608
    .end local v2           #isFocusedDay:Z
    :cond_150
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@152
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@155
    move-result-object v3

    #@156
    const/4 v4, 0x5

    #@157
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    #@15a
    move-result v3

    #@15b
    const/4 v4, 0x1

    #@15c
    if-ne v3, v4, :cond_169

    #@15e
    .line 1609
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@160
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@163
    move-result-object v3

    #@164
    const/4 v4, 0x5

    #@165
    const/4 v5, -0x1

    #@166
    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    #@169
    .line 1611
    :cond_169
    iget-object v3, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@16b
    invoke-static {v3}, Landroid/widget/CalendarView;->access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;

    #@16e
    move-result-object v3

    #@16f
    const/4 v4, 0x2

    #@170
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    #@173
    move-result v3

    #@174
    iput v3, p0, Landroid/widget/CalendarView$WeekView;->mLastWeekDayMonth:I

    #@176
    .line 1613
    invoke-direct {p0}, Landroid/widget/CalendarView$WeekView;->updateSelectionPositions()V

    #@179
    .line 1614
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 1699
    invoke-direct {p0, p1}, Landroid/widget/CalendarView$WeekView;->drawBackground(Landroid/graphics/Canvas;)V

    #@3
    .line 1700
    invoke-direct {p0, p1}, Landroid/widget/CalendarView$WeekView;->drawWeekNumbersAndDates(Landroid/graphics/Canvas;)V

    #@6
    .line 1701
    invoke-direct {p0, p1}, Landroid/widget/CalendarView$WeekView;->drawWeekSeparators(Landroid/graphics/Canvas;)V

    #@9
    .line 1702
    invoke-direct {p0, p1}, Landroid/widget/CalendarView$WeekView;->drawSelectedDateVerticalBars(Landroid/graphics/Canvas;)V

    #@c
    .line 1703
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 1862
    iget-object v0, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@2
    invoke-static {v0}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    #@9
    move-result v0

    #@a
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@c
    invoke-static {v1}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    #@13
    move-result v1

    #@14
    sub-int/2addr v0, v1

    #@15
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@17
    invoke-static {v1}, Landroid/widget/CalendarView;->access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingBottom()I

    #@1e
    move-result v1

    #@1f
    sub-int/2addr v0, v1

    #@20
    iget-object v1, p0, Landroid/widget/CalendarView$WeekView;->this$0:Landroid/widget/CalendarView;

    #@22
    invoke-static {v1}, Landroid/widget/CalendarView;->access$3500(Landroid/widget/CalendarView;)I

    #@25
    move-result v1

    #@26
    div-int/2addr v0, v1

    #@27
    iput v0, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@29
    .line 1864
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@2c
    move-result v0

    #@2d
    iget v1, p0, Landroid/widget/CalendarView$WeekView;->mHeight:I

    #@2f
    invoke-virtual {p0, v0, v1}, Landroid/widget/CalendarView$WeekView;->setMeasuredDimension(II)V

    #@32
    .line 1865
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 5
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 1833
    iput p1, p0, Landroid/widget/CalendarView$WeekView;->mWidth:I

    #@2
    .line 1834
    invoke-direct {p0}, Landroid/widget/CalendarView$WeekView;->updateSelectionPositions()V

    #@5
    .line 1835
    return-void
.end method
