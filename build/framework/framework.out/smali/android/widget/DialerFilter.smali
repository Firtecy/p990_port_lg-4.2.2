.class public Landroid/widget/DialerFilter;
.super Landroid/widget/RelativeLayout;
.source "DialerFilter.java"


# static fields
.field public static final DIGITS_AND_LETTERS:I = 0x1

.field public static final DIGITS_AND_LETTERS_NO_DIGITS:I = 0x2

.field public static final DIGITS_AND_LETTERS_NO_LETTERS:I = 0x3

.field public static final DIGITS_ONLY:I = 0x4

.field public static final LETTERS_ONLY:I = 0x5


# instance fields
.field mDigits:Landroid/widget/EditText;

.field mHint:Landroid/widget/EditText;

.field mIcon:Landroid/widget/ImageView;

.field mInputFilters:[Landroid/text/InputFilter;

.field private mIsQwerty:Z

.field mLetters:Landroid/widget/EditText;

.field mMode:I

.field mPrimary:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 46
    return-void
.end method

.method private makeDigitsPrimary()V
    .registers 3

    #@0
    .prologue
    .line 263
    iget-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@2
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@4
    if-ne v0, v1, :cond_a

    #@6
    .line 264
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/DialerFilter;->swapPrimaryAndHint(Z)V

    #@a
    .line 266
    :cond_a
    return-void
.end method

.method private makeLettersPrimary()V
    .registers 3

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@2
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@4
    if-ne v0, v1, :cond_a

    #@6
    .line 258
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Landroid/widget/DialerFilter;->swapPrimaryAndHint(Z)V

    #@a
    .line 260
    :cond_a
    return-void
.end method

.method private swapPrimaryAndHint(Z)V
    .registers 8
    .parameter "makeLettersPrimary"

    #@0
    .prologue
    .line 269
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@5
    move-result-object v3

    #@6
    .line 270
    .local v3, lettersText:Landroid/text/Editable;
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@8
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@b
    move-result-object v1

    #@c
    .line 271
    .local v1, digitsText:Landroid/text/Editable;
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@e
    invoke-virtual {v4}, Landroid/widget/EditText;->getKeyListener()Landroid/text/method/KeyListener;

    #@11
    move-result-object v2

    #@12
    .line 272
    .local v2, lettersInput:Landroid/text/method/KeyListener;
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@14
    invoke-virtual {v4}, Landroid/widget/EditText;->getKeyListener()Landroid/text/method/KeyListener;

    #@17
    move-result-object v0

    #@18
    .line 274
    .local v0, digitsInput:Landroid/text/method/KeyListener;
    if-eqz p1, :cond_5f

    #@1a
    .line 275
    iget-object v4, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@1c
    iput-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@1e
    .line 276
    iget-object v4, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@20
    iput-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@22
    .line 282
    :goto_22
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@24
    invoke-virtual {v4, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@27
    .line 283
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@29
    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@2c
    .line 284
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2e
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@31
    move-result-object v3

    #@32
    .line 285
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@35
    move-result v4

    #@36
    invoke-static {v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@39
    .line 287
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@3b
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@3e
    .line 288
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@40
    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@43
    .line 289
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@45
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@48
    move-result-object v1

    #@49
    .line 290
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@4c
    move-result v4

    #@4d
    invoke-static {v1, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@50
    .line 293
    iget-object v4, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@52
    iget-object v5, p0, Landroid/widget/DialerFilter;->mInputFilters:[Landroid/text/InputFilter;

    #@54
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@57
    .line 294
    iget-object v4, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@59
    iget-object v5, p0, Landroid/widget/DialerFilter;->mInputFilters:[Landroid/text/InputFilter;

    #@5b
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@5e
    .line 295
    return-void

    #@5f
    .line 278
    :cond_5f
    iget-object v4, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@61
    iput-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@63
    .line 279
    iget-object v4, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@65
    iput-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@67
    goto :goto_22
.end method


# virtual methods
.method public append(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 323
    iget v0, p0, Landroid/widget/DialerFilter;->mMode:I

    #@2
    packed-switch v0, :pswitch_data_2e

    #@5
    .line 339
    :goto_5
    return-void

    #@6
    .line 325
    :pswitch_6
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@8
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@f
    .line 326
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@11
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@14
    move-result-object v0

    #@15
    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@18
    goto :goto_5

    #@19
    .line 331
    :pswitch_19
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@1b
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@1e
    move-result-object v0

    #@1f
    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@22
    goto :goto_5

    #@23
    .line 336
    :pswitch_23
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@25
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@28
    move-result-object v0

    #@29
    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@2c
    goto :goto_5

    #@2d
    .line 323
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_23
        :pswitch_19
        :pswitch_19
        :pswitch_23
    .end packed-switch
.end method

.method public clearText()V
    .registers 3

    #@0
    .prologue
    .line 347
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@5
    move-result-object v0

    #@6
    .line 348
    .local v0, text:Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    #@9
    .line 350
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@b
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@e
    move-result-object v0

    #@f
    .line 351
    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    #@12
    .line 354
    iget-boolean v1, p0, Landroid/widget/DialerFilter;->mIsQwerty:Z

    #@14
    if-eqz v1, :cond_1b

    #@16
    .line 355
    const/4 v1, 0x1

    #@17
    invoke-virtual {p0, v1}, Landroid/widget/DialerFilter;->setMode(I)V

    #@1a
    .line 359
    :goto_1a
    return-void

    #@1b
    .line 357
    :cond_1b
    const/4 v1, 0x4

    #@1c
    invoke-virtual {p0, v1}, Landroid/widget/DialerFilter;->setMode(I)V

    #@1f
    goto :goto_1a
.end method

.method public getDigits()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 307
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_f

    #@8
    .line 308
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@a
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    .line 310
    :goto_e
    return-object v0

    #@f
    :cond_f
    const-string v0, ""

    #@11
    goto :goto_e
.end method

.method public getFilterText()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 315
    iget v0, p0, Landroid/widget/DialerFilter;->mMode:I

    #@2
    const/4 v1, 0x4

    #@3
    if-eq v0, v1, :cond_a

    #@5
    .line 316
    invoke-virtual {p0}, Landroid/widget/DialerFilter;->getLetters()Ljava/lang/CharSequence;

    #@8
    move-result-object v0

    #@9
    .line 318
    :goto_9
    return-object v0

    #@a
    :cond_a
    invoke-virtual {p0}, Landroid/widget/DialerFilter;->getDigits()Ljava/lang/CharSequence;

    #@d
    move-result-object v0

    #@e
    goto :goto_9
.end method

.method public getLetters()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2
    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_f

    #@8
    .line 300
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@a
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    .line 302
    :goto_e
    return-object v0

    #@f
    :cond_f
    const-string v0, ""

    #@11
    goto :goto_e
.end method

.method public getMode()I
    .registers 2

    #@0
    .prologue
    .line 210
    iget v0, p0, Landroid/widget/DialerFilter;->mMode:I

    #@2
    return v0
.end method

.method public isQwertyKeyboard()Z
    .registers 2

    #@0
    .prologue
    .line 103
    iget-boolean v0, p0, Landroid/widget/DialerFilter;->mIsQwerty:Z

    #@2
    return v0
.end method

.method protected onFinishInflate()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 50
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    #@6
    .line 53
    new-array v0, v2, [Landroid/text/InputFilter;

    #@8
    new-instance v1, Landroid/text/InputFilter$AllCaps;

    #@a
    invoke-direct {v1}, Landroid/text/InputFilter$AllCaps;-><init>()V

    #@d
    aput-object v1, v0, v3

    #@f
    iput-object v0, p0, Landroid/widget/DialerFilter;->mInputFilters:[Landroid/text/InputFilter;

    #@11
    .line 55
    const v0, 0x1020005

    #@14
    invoke-virtual {p0, v0}, Landroid/widget/DialerFilter;->findViewById(I)Landroid/view/View;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/widget/EditText;

    #@1a
    iput-object v0, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@1c
    .line 56
    iget-object v0, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@1e
    if-nez v0, :cond_28

    #@20
    .line 57
    new-instance v0, Ljava/lang/IllegalStateException;

    #@22
    const-string v1, "DialerFilter must have a child EditText named hint"

    #@24
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 59
    :cond_28
    iget-object v0, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@2a
    iget-object v1, p0, Landroid/widget/DialerFilter;->mInputFilters:[Landroid/text/InputFilter;

    #@2c
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@2f
    .line 61
    iget-object v0, p0, Landroid/widget/DialerFilter;->mHint:Landroid/widget/EditText;

    #@31
    iput-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@33
    .line 62
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@35
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@3c
    .line 63
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@3e
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@41
    .line 64
    iget-object v0, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@43
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    #@46
    .line 67
    const v0, 0x102000c

    #@49
    invoke-virtual {p0, v0}, Landroid/widget/DialerFilter;->findViewById(I)Landroid/view/View;

    #@4c
    move-result-object v0

    #@4d
    check-cast v0, Landroid/widget/EditText;

    #@4f
    iput-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@51
    .line 68
    iget-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@53
    if-nez v0, :cond_5d

    #@55
    .line 69
    new-instance v0, Ljava/lang/IllegalStateException;

    #@57
    const-string v1, "DialerFilter must have a child EditText named primary"

    #@59
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v0

    #@5d
    .line 71
    :cond_5d
    iget-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@5f
    iget-object v1, p0, Landroid/widget/DialerFilter;->mInputFilters:[Landroid/text/InputFilter;

    #@61
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@64
    .line 73
    iget-object v0, p0, Landroid/widget/DialerFilter;->mPrimary:Landroid/widget/EditText;

    #@66
    iput-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@68
    .line 74
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@6a
    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    #@71
    .line 75
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@73
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@76
    .line 76
    iget-object v0, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@78
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    #@7b
    .line 79
    const v0, 0x1020006

    #@7e
    invoke-virtual {p0, v0}, Landroid/widget/DialerFilter;->findViewById(I)Landroid/view/View;

    #@81
    move-result-object v0

    #@82
    check-cast v0, Landroid/widget/ImageView;

    #@84
    iput-object v0, p0, Landroid/widget/DialerFilter;->mIcon:Landroid/widget/ImageView;

    #@86
    .line 82
    invoke-virtual {p0, v2}, Landroid/widget/DialerFilter;->setFocusable(Z)V

    #@89
    .line 85
    iput-boolean v2, p0, Landroid/widget/DialerFilter;->mIsQwerty:Z

    #@8b
    .line 86
    invoke-virtual {p0, v2}, Landroid/widget/DialerFilter;->setMode(I)V

    #@8e
    .line 87
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 6
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 94
    invoke-super {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 96
    iget-object v0, p0, Landroid/widget/DialerFilter;->mIcon:Landroid/widget/ImageView;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 97
    iget-object v1, p0, Landroid/widget/DialerFilter;->mIcon:Landroid/widget/ImageView;

    #@9
    if-eqz p1, :cond_10

    #@b
    const/4 v0, 0x0

    #@c
    :goto_c
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    #@f
    .line 99
    :cond_f
    return-void

    #@10
    .line 97
    :cond_10
    const/16 v0, 0x8

    #@12
    goto :goto_c
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 108
    const/4 v1, 0x0

    #@2
    .line 110
    .local v1, handled:Z
    sparse-switch p1, :sswitch_data_c0

    #@5
    .line 154
    iget v4, p0, Landroid/widget/DialerFilter;->mMode:I

    #@7
    packed-switch v4, :pswitch_data_de

    #@a
    .line 195
    :cond_a
    :goto_a
    :sswitch_a
    if-nez v1, :cond_10

    #@c
    .line 196
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@f
    move-result v3

    #@10
    .line 198
    :cond_10
    return v3

    #@11
    .line 120
    :sswitch_11
    iget v4, p0, Landroid/widget/DialerFilter;->mMode:I

    #@13
    packed-switch v4, :pswitch_data_ec

    #@16
    goto :goto_a

    #@17
    .line 122
    :pswitch_17
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@19
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@1c
    move-result v1

    #@1d
    .line 123
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@1f
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@22
    move-result v4

    #@23
    and-int/2addr v1, v4

    #@24
    .line 124
    goto :goto_a

    #@25
    .line 127
    :pswitch_25
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@27
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@2a
    move-result v1

    #@2b
    .line 128
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2d
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@30
    move-result-object v4

    #@31
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    #@34
    move-result v4

    #@35
    iget-object v5, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@37
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@3a
    move-result-object v5

    #@3b
    invoke-interface {v5}, Landroid/text/Editable;->length()I

    #@3e
    move-result v5

    #@3f
    if-ne v4, v5, :cond_a

    #@41
    .line 129
    invoke-virtual {p0, v3}, Landroid/widget/DialerFilter;->setMode(I)V

    #@44
    goto :goto_a

    #@45
    .line 134
    :pswitch_45
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@47
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@4a
    move-result-object v4

    #@4b
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    #@4e
    move-result v4

    #@4f
    iget-object v5, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@51
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@54
    move-result-object v5

    #@55
    invoke-interface {v5}, Landroid/text/Editable;->length()I

    #@58
    move-result v5

    #@59
    if-ne v4, v5, :cond_63

    #@5b
    .line 135
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@5d
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@60
    .line 136
    invoke-virtual {p0, v3}, Landroid/widget/DialerFilter;->setMode(I)V

    #@63
    .line 138
    :cond_63
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@65
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@68
    move-result v1

    #@69
    .line 139
    goto :goto_a

    #@6a
    .line 142
    :pswitch_6a
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@6c
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@6f
    move-result v1

    #@70
    .line 143
    goto :goto_a

    #@71
    .line 146
    :pswitch_71
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@73
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@76
    move-result v1

    #@77
    goto :goto_a

    #@78
    .line 156
    :pswitch_78
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@7a
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@7d
    move-result v1

    #@7e
    .line 160
    invoke-static {p1}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    #@81
    move-result v4

    #@82
    if-eqz v4, :cond_8b

    #@84
    .line 161
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@86
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@89
    .line 162
    const/4 v1, 0x1

    #@8a
    .line 163
    goto :goto_a

    #@8b
    .line 171
    :cond_8b
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isPrintingKey()Z

    #@8e
    move-result v2

    #@8f
    .line 172
    .local v2, isPrint:Z
    if-nez v2, :cond_99

    #@91
    const/16 v4, 0x3e

    #@93
    if-eq p1, v4, :cond_99

    #@95
    const/16 v4, 0x3d

    #@97
    if-ne p1, v4, :cond_a

    #@99
    .line 174
    :cond_99
    sget-object v4, Landroid/text/method/DialerKeyListener;->CHARACTERS:[C

    #@9b
    invoke-virtual {p2, v4}, Landroid/view/KeyEvent;->getMatch([C)C

    #@9e
    move-result v0

    #@9f
    .line 175
    .local v0, c:C
    if-eqz v0, :cond_aa

    #@a1
    .line 176
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@a3
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@a6
    move-result v4

    #@a7
    and-int/2addr v1, v4

    #@a8
    goto/16 :goto_a

    #@aa
    .line 178
    :cond_aa
    const/4 v4, 0x2

    #@ab
    invoke-virtual {p0, v4}, Landroid/widget/DialerFilter;->setMode(I)V

    #@ae
    goto/16 :goto_a

    #@b0
    .line 185
    .end local v0           #c:C
    .end local v2           #isPrint:Z
    :pswitch_b0
    iget-object v4, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@b2
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@b5
    move-result v1

    #@b6
    .line 186
    goto/16 :goto_a

    #@b8
    .line 190
    :pswitch_b8
    iget-object v4, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@ba
    invoke-virtual {v4, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@bd
    move-result v1

    #@be
    goto/16 :goto_a

    #@c0
    .line 110
    :sswitch_data_c0
    .sparse-switch
        0x13 -> :sswitch_a
        0x14 -> :sswitch_a
        0x15 -> :sswitch_a
        0x16 -> :sswitch_a
        0x17 -> :sswitch_a
        0x42 -> :sswitch_a
        0x43 -> :sswitch_11
    .end sparse-switch

    #@de
    .line 154
    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_78
        :pswitch_b8
        :pswitch_b0
        :pswitch_b0
        :pswitch_b8
    .end packed-switch

    #@ec
    .line 120
    :pswitch_data_ec
    .packed-switch 0x1
        :pswitch_17
        :pswitch_25
        :pswitch_45
        :pswitch_6a
        :pswitch_71
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 204
    iget-object v2, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2
    invoke-virtual {v2, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    .line 205
    .local v0, a:Z
    iget-object v2, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@8
    invoke-virtual {v2, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@b
    move-result v1

    #@c
    .line 206
    .local v1, b:Z
    if-nez v0, :cond_10

    #@e
    if-eqz v1, :cond_12

    #@10
    :cond_10
    const/4 v2, 0x1

    #@11
    :goto_11
    return v2

    #@12
    :cond_12
    const/4 v2, 0x0

    #@13
    goto :goto_11
.end method

.method protected onModeChange(II)V
    .registers 3
    .parameter "oldMode"
    .parameter "newMode"

    #@0
    .prologue
    .line 396
    return-void
.end method

.method public removeFilterWatcher(Landroid/text/TextWatcher;)V
    .registers 5
    .parameter "watcher"

    #@0
    .prologue
    .line 383
    iget v1, p0, Landroid/widget/DialerFilter;->mMode:I

    #@2
    const/4 v2, 0x4

    #@3
    if-eq v1, v2, :cond_f

    #@5
    .line 384
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@7
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@a
    move-result-object v0

    #@b
    .line 388
    .local v0, text:Landroid/text/Spannable;
    :goto_b
    invoke-interface {v0, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@e
    .line 389
    return-void

    #@f
    .line 386
    .end local v0           #text:Landroid/text/Spannable;
    :cond_f
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@11
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@14
    move-result-object v0

    #@15
    .restart local v0       #text:Landroid/text/Spannable;
    goto :goto_b
.end method

.method public setDigitsWatcher(Landroid/text/TextWatcher;)V
    .registers 7
    .parameter "watcher"

    #@0
    .prologue
    .line 368
    iget-object v2, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@2
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@5
    move-result-object v1

    #@6
    .local v1, text:Ljava/lang/CharSequence;
    move-object v0, v1

    #@7
    .line 369
    check-cast v0, Landroid/text/Spannable;

    #@9
    .line 370
    .local v0, span:Landroid/text/Spannable;
    const/4 v2, 0x0

    #@a
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v3

    #@e
    const/16 v4, 0x12

    #@10
    invoke-interface {v0, p1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@13
    .line 371
    return-void
.end method

.method public setFilterWatcher(Landroid/text/TextWatcher;)V
    .registers 4
    .parameter "watcher"

    #@0
    .prologue
    .line 374
    iget v0, p0, Landroid/widget/DialerFilter;->mMode:I

    #@2
    const/4 v1, 0x4

    #@3
    if-eq v0, v1, :cond_9

    #@5
    .line 375
    invoke-virtual {p0, p1}, Landroid/widget/DialerFilter;->setLettersWatcher(Landroid/text/TextWatcher;)V

    #@8
    .line 379
    :goto_8
    return-void

    #@9
    .line 377
    :cond_9
    invoke-virtual {p0, p1}, Landroid/widget/DialerFilter;->setDigitsWatcher(Landroid/text/TextWatcher;)V

    #@c
    goto :goto_8
.end method

.method public setLettersWatcher(Landroid/text/TextWatcher;)V
    .registers 7
    .parameter "watcher"

    #@0
    .prologue
    .line 362
    iget-object v2, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@2
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@5
    move-result-object v1

    #@6
    .local v1, text:Ljava/lang/CharSequence;
    move-object v0, v1

    #@7
    .line 363
    check-cast v0, Landroid/text/Spannable;

    #@9
    .line 364
    .local v0, span:Landroid/text/Spannable;
    const/4 v2, 0x0

    #@a
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v3

    #@e
    const/16 v4, 0x12

    #@10
    invoke-interface {v0, p1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@13
    .line 365
    return-void
.end method

.method public setMode(I)V
    .registers 7
    .parameter "newMode"

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    const/4 v3, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    .line 219
    packed-switch p1, :pswitch_data_56

    #@7
    .line 251
    :goto_7
    iget v0, p0, Landroid/widget/DialerFilter;->mMode:I

    #@9
    .line 252
    .local v0, oldMode:I
    iput p1, p0, Landroid/widget/DialerFilter;->mMode:I

    #@b
    .line 253
    invoke-virtual {p0, v0, p1}, Landroid/widget/DialerFilter;->onModeChange(II)V

    #@e
    .line 254
    return-void

    #@f
    .line 221
    .end local v0           #oldMode:I
    :pswitch_f
    invoke-direct {p0}, Landroid/widget/DialerFilter;->makeDigitsPrimary()V

    #@12
    .line 222
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@14
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@17
    .line 223
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@19
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@1c
    goto :goto_7

    #@1d
    .line 227
    :pswitch_1d
    invoke-direct {p0}, Landroid/widget/DialerFilter;->makeDigitsPrimary()V

    #@20
    .line 228
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@22
    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    #@25
    .line 229
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@27
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@2a
    goto :goto_7

    #@2b
    .line 233
    :pswitch_2b
    invoke-direct {p0}, Landroid/widget/DialerFilter;->makeLettersPrimary()V

    #@2e
    .line 234
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@30
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@33
    .line 235
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@35
    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    #@38
    goto :goto_7

    #@39
    .line 239
    :pswitch_39
    invoke-direct {p0}, Landroid/widget/DialerFilter;->makeDigitsPrimary()V

    #@3c
    .line 240
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@3e
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setVisibility(I)V

    #@41
    .line 241
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@43
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@46
    goto :goto_7

    #@47
    .line 245
    :pswitch_47
    invoke-direct {p0}, Landroid/widget/DialerFilter;->makeLettersPrimary()V

    #@4a
    .line 246
    iget-object v1, p0, Landroid/widget/DialerFilter;->mLetters:Landroid/widget/EditText;

    #@4c
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    #@4f
    .line 247
    iget-object v1, p0, Landroid/widget/DialerFilter;->mDigits:Landroid/widget/EditText;

    #@51
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setVisibility(I)V

    #@54
    goto :goto_7

    #@55
    .line 219
    nop

    #@56
    :pswitch_data_56
    .packed-switch 0x1
        :pswitch_f
        :pswitch_47
        :pswitch_39
        :pswitch_1d
        :pswitch_2b
    .end packed-switch
.end method
