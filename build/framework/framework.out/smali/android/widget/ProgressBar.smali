.class public Landroid/widget/ProgressBar;
.super Landroid/view/View;
.source "ProgressBar.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ProgressBar$1;,
        Landroid/widget/ProgressBar$AccessibilityEventSender;,
        Landroid/widget/ProgressBar$SavedState;,
        Landroid/widget/ProgressBar$RefreshData;,
        Landroid/widget/ProgressBar$RefreshProgressRunnable;
    }
.end annotation


# static fields
.field private static final MAX_LEVEL:I = 0x2710

.field private static final TIMEOUT_SEND_ACCESSIBILITY_EVENT:I = 0xc8


# instance fields
.field private mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

.field private mAnimation:Landroid/view/animation/AlphaAnimation;

.field private mAttached:Z

.field private mBehavior:I

.field private mCurrentDrawable:Landroid/graphics/drawable/Drawable;

.field private mDuration:I

.field private mHasAnimation:Z

.field private mInDrawing:Z

.field private mIndeterminate:Z

.field private mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mMax:I

.field mMaxHeight:I

.field mMaxWidth:I

.field mMinHeight:I

.field mMinWidth:I

.field private mNoInvalidate:Z

.field private mOnlyIndeterminate:Z

.field private mProgress:I

.field private mProgressDrawable:Landroid/graphics/drawable/Drawable;

.field private final mRefreshData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ProgressBar$RefreshData;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshIsPosted:Z

.field private mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

.field mSampleTile:Landroid/graphics/Bitmap;

.field private mSecondaryProgress:I

.field private mShouldStartAnimationDrawable:Z

.field private mTransformation:Landroid/view/animation/Transformation;

.field private mUiThreadId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 238
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 239
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 242
    const v0, 0x1010077

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 246
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@4
    .line 247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"
    .parameter "styleRes"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 253
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 229
    new-instance v5, Ljava/util/ArrayList;

    #@7
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v5, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@c
    .line 254
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v5

    #@14
    iput-wide v5, p0, Landroid/widget/ProgressBar;->mUiThreadId:J

    #@16
    .line 255
    invoke-direct {p0}, Landroid/widget/ProgressBar;->initProgressBar()V

    #@19
    .line 257
    sget-object v5, Lcom/android/internal/R$styleable;->ProgressBar:[I

    #@1b
    invoke-virtual {p1, p2, v5, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1e
    move-result-object v0

    #@1f
    .line 260
    .local v0, a:Landroid/content/res/TypedArray;
    iput-boolean v4, p0, Landroid/widget/ProgressBar;->mNoInvalidate:Z

    #@21
    .line 262
    const/16 v5, 0x8

    #@23
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v1

    #@27
    .line 263
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_30

    #@29
    .line 264
    invoke-direct {p0, v1, v3}, Landroid/widget/ProgressBar;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    #@2c
    move-result-object v1

    #@2d
    .line 267
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    #@30
    .line 271
    :cond_30
    const/16 v5, 0x9

    #@32
    iget v6, p0, Landroid/widget/ProgressBar;->mDuration:I

    #@34
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@37
    move-result v5

    #@38
    iput v5, p0, Landroid/widget/ProgressBar;->mDuration:I

    #@3a
    .line 273
    const/16 v5, 0xb

    #@3c
    iget v6, p0, Landroid/widget/ProgressBar;->mMinWidth:I

    #@3e
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@41
    move-result v5

    #@42
    iput v5, p0, Landroid/widget/ProgressBar;->mMinWidth:I

    #@44
    .line 274
    iget v5, p0, Landroid/widget/ProgressBar;->mMaxWidth:I

    #@46
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@49
    move-result v5

    #@4a
    iput v5, p0, Landroid/widget/ProgressBar;->mMaxWidth:I

    #@4c
    .line 275
    const/16 v5, 0xc

    #@4e
    iget v6, p0, Landroid/widget/ProgressBar;->mMinHeight:I

    #@50
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@53
    move-result v5

    #@54
    iput v5, p0, Landroid/widget/ProgressBar;->mMinHeight:I

    #@56
    .line 276
    iget v5, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@58
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@5b
    move-result v5

    #@5c
    iput v5, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@5e
    .line 278
    const/16 v5, 0xa

    #@60
    iget v6, p0, Landroid/widget/ProgressBar;->mBehavior:I

    #@62
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@65
    move-result v5

    #@66
    iput v5, p0, Landroid/widget/ProgressBar;->mBehavior:I

    #@68
    .line 280
    const/16 v5, 0xd

    #@6a
    const v6, 0x10a000b

    #@6d
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@70
    move-result v2

    #@71
    .line 283
    .local v2, resID:I
    if-lez v2, :cond_76

    #@73
    .line 284
    invoke-virtual {p0, p1, v2}, Landroid/widget/ProgressBar;->setInterpolator(Landroid/content/Context;I)V

    #@76
    .line 287
    :cond_76
    const/4 v5, 0x2

    #@77
    iget v6, p0, Landroid/widget/ProgressBar;->mMax:I

    #@79
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@7c
    move-result v5

    #@7d
    invoke-virtual {p0, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    #@80
    .line 289
    const/4 v5, 0x3

    #@81
    iget v6, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@83
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@86
    move-result v5

    #@87
    invoke-virtual {p0, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@8a
    .line 291
    const/4 v5, 0x4

    #@8b
    iget v6, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@8d
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@90
    move-result v5

    #@91
    invoke-virtual {p0, v5}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    #@94
    .line 294
    const/4 v5, 0x7

    #@95
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@98
    move-result-object v1

    #@99
    .line 295
    if-eqz v1, :cond_a2

    #@9b
    .line 296
    invoke-direct {p0, v1}, Landroid/widget/ProgressBar;->tileifyIndeterminate(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    #@9e
    move-result-object v1

    #@9f
    .line 297
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@a2
    .line 300
    :cond_a2
    const/4 v5, 0x6

    #@a3
    iget-boolean v6, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@a5
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a8
    move-result v5

    #@a9
    iput-boolean v5, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@ab
    .line 303
    iput-boolean v3, p0, Landroid/widget/ProgressBar;->mNoInvalidate:Z

    #@ad
    .line 305
    iget-boolean v5, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@af
    if-nez v5, :cond_ba

    #@b1
    const/4 v5, 0x5

    #@b2
    iget-boolean v6, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@b4
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@b7
    move-result v5

    #@b8
    if-eqz v5, :cond_bb

    #@ba
    :cond_ba
    move v3, v4

    #@bb
    :cond_bb
    invoke-virtual {p0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    #@be
    .line 308
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@c1
    .line 309
    return-void
.end method

.method static synthetic access$000(Landroid/widget/ProgressBar;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/widget/ProgressBar;IIZZ)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 195
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ProgressBar;->doRefreshProgress(IIZZ)V

    #@3
    return-void
.end method

.method static synthetic access$202(Landroid/widget/ProgressBar;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 195
    iput-boolean p1, p0, Landroid/widget/ProgressBar;->mRefreshIsPosted:Z

    #@2
    return p1
.end method

.method private declared-synchronized doRefreshProgress(IIZZ)V
    .registers 12
    .parameter "id"
    .parameter "progress"
    .parameter "fromUser"
    .parameter "callBackToApp"

    #@0
    .prologue
    .line 667
    monitor-enter p0

    #@1
    :try_start_1
    iget v5, p0, Landroid/widget/ProgressBar;->mMax:I

    #@3
    if-lez v5, :cond_41

    #@5
    int-to-float v5, p2

    #@6
    iget v6, p0, Landroid/widget/ProgressBar;->mMax:I

    #@8
    int-to-float v6, v6

    #@9
    div-float v4, v5, v6

    #@b
    .line 668
    .local v4, scale:F
    :goto_b
    iget-object v1, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    .line 669
    .local v1, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_45

    #@f
    .line 670
    const/4 v3, 0x0

    #@10
    .line 672
    .local v3, progressDrawable:Landroid/graphics/drawable/Drawable;
    instance-of v5, v1, Landroid/graphics/drawable/LayerDrawable;

    #@12
    if-eqz v5, :cond_2b

    #@14
    .line 673
    move-object v0, v1

    #@15
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    #@17
    move-object v5, v0

    #@18
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v3

    #@1c
    .line 674
    if-eqz v3, :cond_2b

    #@1e
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->canResolveLayoutDirection()Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_2b

    #@24
    .line 675
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getLayoutDirection()I

    #@27
    move-result v5

    #@28
    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@2b
    .line 679
    :cond_2b
    const v5, 0x461c4000

    #@2e
    mul-float/2addr v5, v4

    #@2f
    float-to-int v2, v5

    #@30
    .line 680
    .local v2, level:I
    if-eqz v3, :cond_43

    #@32
    .end local v3           #progressDrawable:Landroid/graphics/drawable/Drawable;
    :goto_32
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@35
    .line 685
    .end local v2           #level:I
    :goto_35
    if-eqz p4, :cond_3f

    #@37
    const v5, 0x102000d

    #@3a
    if-ne p1, v5, :cond_3f

    #@3c
    .line 686
    invoke-virtual {p0, v4, p3}, Landroid/widget/ProgressBar;->onProgressRefresh(FZ)V
    :try_end_3f
    .catchall {:try_start_1 .. :try_end_3f} :catchall_49

    #@3f
    .line 688
    :cond_3f
    monitor-exit p0

    #@40
    return-void

    #@41
    .line 667
    .end local v1           #d:Landroid/graphics/drawable/Drawable;
    .end local v4           #scale:F
    :cond_41
    const/4 v4, 0x0

    #@42
    goto :goto_b

    #@43
    .restart local v1       #d:Landroid/graphics/drawable/Drawable;
    .restart local v2       #level:I
    .restart local v3       #progressDrawable:Landroid/graphics/drawable/Drawable;
    .restart local v4       #scale:F
    :cond_43
    move-object v3, v1

    #@44
    .line 680
    goto :goto_32

    #@45
    .line 682
    .end local v2           #level:I
    .end local v3           #progressDrawable:Landroid/graphics/drawable/Drawable;
    :cond_45
    :try_start_45
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->invalidate()V
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_49

    #@48
    goto :goto_35

    #@49
    .line 667
    .end local v1           #d:Landroid/graphics/drawable/Drawable;
    .end local v4           #scale:F
    :catchall_49
    move-exception v5

    #@4a
    monitor-exit p0

    #@4b
    throw v5
.end method

.method private initProgressBar()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x30

    #@2
    const/16 v2, 0x18

    #@4
    const/4 v1, 0x0

    #@5
    .line 405
    const/16 v0, 0x64

    #@7
    iput v0, p0, Landroid/widget/ProgressBar;->mMax:I

    #@9
    .line 406
    iput v1, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@b
    .line 407
    iput v1, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@d
    .line 408
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@f
    .line 409
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@11
    .line 410
    const/16 v0, 0xfa0

    #@13
    iput v0, p0, Landroid/widget/ProgressBar;->mDuration:I

    #@15
    .line 411
    const/4 v0, 0x1

    #@16
    iput v0, p0, Landroid/widget/ProgressBar;->mBehavior:I

    #@18
    .line 412
    iput v2, p0, Landroid/widget/ProgressBar;->mMinWidth:I

    #@1a
    .line 413
    iput v3, p0, Landroid/widget/ProgressBar;->mMaxWidth:I

    #@1c
    .line 414
    iput v2, p0, Landroid/widget/ProgressBar;->mMinHeight:I

    #@1e
    .line 415
    iput v3, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@20
    .line 416
    return-void
.end method

.method private declared-synchronized refreshProgress(IIZ)V
    .registers 9
    .parameter "id"
    .parameter "progress"
    .parameter "fromUser"

    #@0
    .prologue
    .line 697
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v1, p0, Landroid/widget/ProgressBar;->mUiThreadId:J

    #@3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    #@a
    move-result-wide v3

    #@b
    cmp-long v1, v1, v3

    #@d
    if-nez v1, :cond_15

    #@f
    .line 698
    const/4 v1, 0x1

    #@10
    invoke-direct {p0, p1, p2, p3, v1}, Landroid/widget/ProgressBar;->doRefreshProgress(IIZZ)V
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_3b

    #@13
    .line 711
    :cond_13
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 700
    :cond_15
    :try_start_15
    iget-object v1, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@17
    if-nez v1, :cond_21

    #@19
    .line 701
    new-instance v1, Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-direct {v1, p0, v2}, Landroid/widget/ProgressBar$RefreshProgressRunnable;-><init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar$1;)V

    #@1f
    iput-object v1, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@21
    .line 704
    :cond_21
    invoke-static {p1, p2, p3}, Landroid/widget/ProgressBar$RefreshData;->obtain(IIZ)Landroid/widget/ProgressBar$RefreshData;

    #@24
    move-result-object v0

    #@25
    .line 705
    .local v0, rd:Landroid/widget/ProgressBar$RefreshData;
    iget-object v1, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 706
    iget-boolean v1, p0, Landroid/widget/ProgressBar;->mAttached:Z

    #@2c
    if-eqz v1, :cond_13

    #@2e
    iget-boolean v1, p0, Landroid/widget/ProgressBar;->mRefreshIsPosted:Z

    #@30
    if-nez v1, :cond_13

    #@32
    .line 707
    iget-object v1, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@34
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->post(Ljava/lang/Runnable;)Z

    #@37
    .line 708
    const/4 v1, 0x1

    #@38
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mRefreshIsPosted:Z
    :try_end_3a
    .catchall {:try_start_15 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_13

    #@3b
    .line 697
    .end local v0           #rd:Landroid/widget/ProgressBar$RefreshData;
    :catchall_3b
    move-exception v1

    #@3c
    monitor-exit p0

    #@3d
    throw v1
.end method

.method private scheduleAccessibilityEventSender()V
    .registers 4

    #@0
    .prologue
    .line 1253
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 1254
    new-instance v0, Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/widget/ProgressBar$AccessibilityEventSender;-><init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar$1;)V

    #@a
    iput-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@c
    .line 1258
    :goto_c
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@e
    const-wide/16 v1, 0xc8

    #@10
    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/ProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    #@13
    .line 1259
    return-void

    #@14
    .line 1256
    :cond_14
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@16
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@19
    goto :goto_c
.end method

.method private tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
    .registers 20
    .parameter "drawable"
    .parameter "clip"

    #@0
    .prologue
    .line 317
    move-object/from16 v0, p1

    #@2
    instance-of v14, v0, Landroid/graphics/drawable/LayerDrawable;

    #@4
    if-eqz v14, :cond_45

    #@6
    move-object/from16 v3, p1

    #@8
    .line 318
    check-cast v3, Landroid/graphics/drawable/LayerDrawable;

    #@a
    .line 319
    .local v3, background:Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    #@d
    move-result v2

    #@e
    .line 320
    .local v2, N:I
    new-array v11, v2, [Landroid/graphics/drawable/Drawable;

    #@10
    .line 322
    .local v11, outDrawables:[Landroid/graphics/drawable/Drawable;
    const/4 v5, 0x0

    #@11
    .local v5, i:I
    :goto_11
    if-ge v5, v2, :cond_33

    #@13
    .line 323
    invoke-virtual {v3, v5}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    #@16
    move-result v6

    #@17
    .line 324
    .local v6, id:I
    invoke-virtual {v3, v5}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1a
    move-result-object v15

    #@1b
    const v14, 0x102000d

    #@1e
    if-eq v6, v14, :cond_25

    #@20
    const v14, 0x102000f

    #@23
    if-ne v6, v14, :cond_31

    #@25
    :cond_25
    const/4 v14, 0x1

    #@26
    :goto_26
    move-object/from16 v0, p0

    #@28
    invoke-direct {v0, v15, v14}, Landroid/widget/ProgressBar;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    #@2b
    move-result-object v14

    #@2c
    aput-object v14, v11, v5

    #@2e
    .line 322
    add-int/lit8 v5, v5, 0x1

    #@30
    goto :goto_11

    #@31
    .line 324
    :cond_31
    const/4 v14, 0x0

    #@32
    goto :goto_26

    #@33
    .line 328
    .end local v6           #id:I
    :cond_33
    new-instance v8, Landroid/graphics/drawable/LayerDrawable;

    #@35
    invoke-direct {v8, v11}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    #@38
    .line 330
    .local v8, newBg:Landroid/graphics/drawable/LayerDrawable;
    const/4 v5, 0x0

    #@39
    :goto_39
    if-ge v5, v2, :cond_72

    #@3b
    .line 331
    invoke-virtual {v3, v5}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    #@3e
    move-result v14

    #@3f
    invoke-virtual {v8, v5, v14}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    #@42
    .line 330
    add-int/lit8 v5, v5, 0x1

    #@44
    goto :goto_39

    #@45
    .line 336
    .end local v2           #N:I
    .end local v3           #background:Landroid/graphics/drawable/LayerDrawable;
    .end local v5           #i:I
    .end local v8           #newBg:Landroid/graphics/drawable/LayerDrawable;
    .end local v11           #outDrawables:[Landroid/graphics/drawable/Drawable;
    :cond_45
    move-object/from16 v0, p1

    #@47
    instance-of v14, v0, Landroid/graphics/drawable/StateListDrawable;

    #@49
    if-eqz v14, :cond_73

    #@4b
    move-object/from16 v7, p1

    #@4d
    .line 337
    check-cast v7, Landroid/graphics/drawable/StateListDrawable;

    #@4f
    .line 338
    .local v7, in:Landroid/graphics/drawable/StateListDrawable;
    new-instance v10, Landroid/graphics/drawable/StateListDrawable;

    #@51
    invoke-direct {v10}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    #@54
    .line 339
    .local v10, out:Landroid/graphics/drawable/StateListDrawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    #@57
    move-result v9

    #@58
    .line 340
    .local v9, numStates:I
    const/4 v5, 0x0

    #@59
    .restart local v5       #i:I
    :goto_59
    if-ge v5, v9, :cond_71

    #@5b
    .line 341
    invoke-virtual {v7, v5}, Landroid/graphics/drawable/StateListDrawable;->getStateSet(I)[I

    #@5e
    move-result-object v14

    #@5f
    invoke-virtual {v7, v5}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@62
    move-result-object v15

    #@63
    move-object/from16 v0, p0

    #@65
    move/from16 v1, p2

    #@67
    invoke-direct {v0, v15, v1}, Landroid/widget/ProgressBar;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    #@6a
    move-result-object v15

    #@6b
    invoke-virtual {v10, v14, v15}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    #@6e
    .line 340
    add-int/lit8 v5, v5, 0x1

    #@70
    goto :goto_59

    #@71
    :cond_71
    move-object v8, v10

    #@72
    .line 361
    .end local v5           #i:I
    .end local v7           #in:Landroid/graphics/drawable/StateListDrawable;
    .end local v9           #numStates:I
    .end local v10           #out:Landroid/graphics/drawable/StateListDrawable;
    .end local p1
    :cond_72
    :goto_72
    return-object v8

    #@73
    .line 345
    .restart local p1
    :cond_73
    move-object/from16 v0, p1

    #@75
    instance-of v14, v0, Landroid/graphics/drawable/BitmapDrawable;

    #@77
    if-eqz v14, :cond_b1

    #@79
    .line 346
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    #@7b
    .end local p1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    #@7e
    move-result-object v13

    #@7f
    .line 347
    .local v13, tileBitmap:Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    #@81
    iget-object v14, v0, Landroid/widget/ProgressBar;->mSampleTile:Landroid/graphics/Bitmap;

    #@83
    if-nez v14, :cond_89

    #@85
    .line 348
    move-object/from16 v0, p0

    #@87
    iput-object v13, v0, Landroid/widget/ProgressBar;->mSampleTile:Landroid/graphics/Bitmap;

    #@89
    .line 351
    :cond_89
    new-instance v12, Landroid/graphics/drawable/ShapeDrawable;

    #@8b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getDrawableShape()Landroid/graphics/drawable/shapes/Shape;

    #@8e
    move-result-object v14

    #@8f
    invoke-direct {v12, v14}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    #@92
    .line 353
    .local v12, shapeDrawable:Landroid/graphics/drawable/ShapeDrawable;
    new-instance v4, Landroid/graphics/BitmapShader;

    #@94
    sget-object v14, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    #@96
    sget-object v15, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@98
    invoke-direct {v4, v13, v14, v15}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    #@9b
    .line 355
    .local v4, bitmapShader:Landroid/graphics/BitmapShader;
    invoke-virtual {v12}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    #@9e
    move-result-object v14

    #@9f
    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@a2
    .line 357
    if-eqz p2, :cond_af

    #@a4
    new-instance v14, Landroid/graphics/drawable/ClipDrawable;

    #@a6
    const/4 v15, 0x3

    #@a7
    const/16 v16, 0x1

    #@a9
    move/from16 v0, v16

    #@ab
    invoke-direct {v14, v12, v15, v0}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    #@ae
    move-object v12, v14

    #@af
    .end local v12           #shapeDrawable:Landroid/graphics/drawable/ShapeDrawable;
    :cond_af
    move-object v8, v12

    #@b0
    goto :goto_72

    #@b1
    .end local v4           #bitmapShader:Landroid/graphics/BitmapShader;
    .end local v13           #tileBitmap:Landroid/graphics/Bitmap;
    .restart local p1
    :cond_b1
    move-object/from16 v8, p1

    #@b3
    .line 361
    goto :goto_72
.end method

.method private tileifyIndeterminate(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 10
    .parameter "drawable"

    #@0
    .prologue
    const/16 v7, 0x2710

    #@2
    .line 375
    instance-of v5, p1, Landroid/graphics/drawable/AnimationDrawable;

    #@4
    if-eqz v5, :cond_36

    #@6
    move-object v1, p1

    #@7
    .line 376
    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    #@9
    .line 377
    .local v1, background:Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    #@c
    move-result v0

    #@d
    .line 378
    .local v0, N:I
    new-instance v4, Landroid/graphics/drawable/AnimationDrawable;

    #@f
    invoke-direct {v4}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    #@12
    .line 379
    .local v4, newBg:Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->isOneShot()Z

    #@15
    move-result v5

    #@16
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    #@19
    .line 381
    const/4 v3, 0x0

    #@1a
    .local v3, i:I
    :goto_1a
    if-ge v3, v0, :cond_32

    #@1c
    .line 382
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v5

    #@20
    const/4 v6, 0x1

    #@21
    invoke-direct {p0, v5, v6}, Landroid/widget/ProgressBar;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    #@24
    move-result-object v2

    #@25
    .line 383
    .local v2, frame:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2, v7}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@28
    .line 384
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    #@2b
    move-result v5

    #@2c
    invoke-virtual {v4, v2, v5}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    #@2f
    .line 381
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_1a

    #@32
    .line 386
    .end local v2           #frame:Landroid/graphics/drawable/Drawable;
    :cond_32
    invoke-virtual {v4, v7}, Landroid/graphics/drawable/AnimationDrawable;->setLevel(I)Z

    #@35
    .line 387
    move-object p1, v4

    #@36
    .line 389
    .end local v0           #N:I
    .end local v1           #background:Landroid/graphics/drawable/AnimationDrawable;
    .end local v3           #i:I
    .end local v4           #newBg:Landroid/graphics/drawable/AnimationDrawable;
    :cond_36
    return-object p1
.end method

.method private updateDrawableBounds(II)V
    .registers 18
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 1012
    iget v12, p0, Landroid/view/View;->mPaddingRight:I

    #@2
    iget v13, p0, Landroid/view/View;->mPaddingLeft:I

    #@4
    add-int/2addr v12, v13

    #@5
    sub-int p1, p1, v12

    #@7
    .line 1013
    iget v12, p0, Landroid/view/View;->mPaddingTop:I

    #@9
    iget v13, p0, Landroid/view/View;->mPaddingBottom:I

    #@b
    add-int/2addr v12, v13

    #@c
    sub-int p2, p2, v12

    #@e
    .line 1015
    move/from16 v8, p1

    #@10
    .line 1016
    .local v8, right:I
    move/from16 v1, p2

    #@12
    .line 1017
    .local v1, bottom:I
    const/4 v10, 0x0

    #@13
    .line 1018
    .local v10, top:I
    const/4 v7, 0x0

    #@14
    .line 1020
    .local v7, left:I
    iget-object v12, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v12, :cond_5d

    #@18
    .line 1022
    iget-boolean v12, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@1a
    if-eqz v12, :cond_4d

    #@1c
    iget-object v12, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    instance-of v12, v12, Landroid/graphics/drawable/AnimationDrawable;

    #@20
    if-nez v12, :cond_4d

    #@22
    .line 1025
    iget-object v12, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@24
    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@27
    move-result v6

    #@28
    .line 1026
    .local v6, intrinsicWidth:I
    iget-object v12, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@2a
    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2d
    move-result v5

    #@2e
    .line 1027
    .local v5, intrinsicHeight:I
    int-to-float v12, v6

    #@2f
    int-to-float v13, v5

    #@30
    div-float v4, v12, v13

    #@32
    .line 1028
    .local v4, intrinsicAspect:F
    move/from16 v0, p1

    #@34
    int-to-float v12, v0

    #@35
    move/from16 v0, p2

    #@37
    int-to-float v13, v0

    #@38
    div-float v2, v12, v13

    #@3a
    .line 1029
    .local v2, boundAspect:F
    cmpl-float v12, v4, v2

    #@3c
    if-eqz v12, :cond_4d

    #@3e
    .line 1030
    cmpl-float v12, v2, v4

    #@40
    if-lez v12, :cond_69

    #@42
    .line 1032
    move/from16 v0, p2

    #@44
    int-to-float v12, v0

    #@45
    mul-float/2addr v12, v4

    #@46
    float-to-int v11, v12

    #@47
    .line 1033
    .local v11, width:I
    sub-int v12, p1, v11

    #@49
    div-int/lit8 v7, v12, 0x2

    #@4b
    .line 1034
    add-int v8, v7, v11

    #@4d
    .line 1043
    .end local v2           #boundAspect:F
    .end local v4           #intrinsicAspect:F
    .end local v5           #intrinsicHeight:I
    .end local v6           #intrinsicWidth:I
    .end local v11           #width:I
    :cond_4d
    :goto_4d
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->isLayoutRtl()Z

    #@50
    move-result v12

    #@51
    if-eqz v12, :cond_58

    #@53
    .line 1044
    move v9, v7

    #@54
    .line 1045
    .local v9, tempLeft:I
    sub-int v7, p1, v8

    #@56
    .line 1046
    sub-int v8, p1, v9

    #@58
    .line 1048
    .end local v9           #tempLeft:I
    :cond_58
    iget-object v12, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@5a
    invoke-virtual {v12, v7, v10, v8, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@5d
    .line 1051
    :cond_5d
    iget-object v12, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@5f
    if-eqz v12, :cond_68

    #@61
    .line 1052
    iget-object v12, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@63
    const/4 v13, 0x0

    #@64
    const/4 v14, 0x0

    #@65
    invoke-virtual {v12, v13, v14, v8, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@68
    .line 1054
    :cond_68
    return-void

    #@69
    .line 1037
    .restart local v2       #boundAspect:F
    .restart local v4       #intrinsicAspect:F
    .restart local v5       #intrinsicHeight:I
    .restart local v6       #intrinsicWidth:I
    :cond_69
    move/from16 v0, p1

    #@6b
    int-to-float v12, v0

    #@6c
    const/high16 v13, 0x3f80

    #@6e
    div-float/2addr v13, v4

    #@6f
    mul-float/2addr v12, v13

    #@70
    float-to-int v3, v12

    #@71
    .line 1038
    .local v3, height:I
    sub-int v12, p2, v3

    #@73
    div-int/lit8 v10, v12, 0x2

    #@75
    .line 1039
    add-int v1, v10, v3

    #@77
    goto :goto_4d
.end method

.method private updateDrawableState()V
    .registers 3

    #@0
    .prologue
    .line 1117
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getDrawableState()[I

    #@3
    move-result-object v0

    #@4
    .line 1119
    .local v0, state:[I
    iget-object v1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v1, :cond_15

    #@8
    iget-object v1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_15

    #@10
    .line 1120
    iget-object v1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@15
    .line 1123
    :cond_15
    iget-object v1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@17
    if-eqz v1, :cond_26

    #@19
    iget-object v1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@1b
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_26

    #@21
    .line 1124
    iget-object v1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@23
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@26
    .line 1126
    :cond_26
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 1112
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    #@3
    .line 1113
    invoke-direct {p0}, Landroid/widget/ProgressBar;->updateDrawableState()V

    #@6
    .line 1114
    return-void
.end method

.method getCurrentDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 552
    iget-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method getDrawableShape()Landroid/graphics/drawable/shapes/Shape;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 365
    const/16 v1, 0x8

    #@3
    new-array v0, v1, [F

    #@5
    fill-array-data v0, :array_e

    #@8
    .line 366
    .local v0, roundedCorners:[F
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    #@a
    invoke-direct {v1, v0, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    #@d
    return-object v1

    #@e
    .line 365
    :array_e
    .array-data 0x4
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
    .end array-data
.end method

.method public getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 464
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getInterpolator()Landroid/view/animation/Interpolator;
    .registers 2

    #@0
    .prologue
    .line 954
    iget-object v0, p0, Landroid/widget/ProgressBar;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    return-object v0
.end method

.method public declared-synchronized getMax()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "progress"
    .end annotation

    #@0
    .prologue
    .line 826
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/widget/ProgressBar;->mMax:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getProgress()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "progress"
    .end annotation

    #@0
    .prologue
    .line 795
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    #@3
    if-eqz v0, :cond_8

    #@5
    const/4 v0, 0x0

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget v0, p0, Landroid/widget/ProgressBar;->mProgress:I
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_b

    #@a
    goto :goto_6

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public getProgressDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public declared-synchronized getSecondaryProgress()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "progress"
    .end annotation

    #@0
    .prologue
    .line 812
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    #@3
    if-eqz v0, :cond_8

    #@5
    const/4 v0, 0x0

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget v0, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_b

    #@a
    goto :goto_6

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public final declared-synchronized incrementProgressBy(I)V
    .registers 3
    .parameter "diff"

    #@0
    .prologue
    .line 862
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@3
    add-int/2addr v0, p1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setProgress(I)V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 863
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 862
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public final declared-synchronized incrementSecondaryProgressBy(I)V
    .registers 3
    .parameter "diff"

    #@0
    .prologue
    .line 873
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@3
    add-int/2addr v0, p1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 874
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 873
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 9
    .parameter "dr"

    #@0
    .prologue
    .line 990
    iget-boolean v3, p0, Landroid/widget/ProgressBar;->mInDrawing:Z

    #@2
    if-nez v3, :cond_29

    #@4
    .line 991
    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_2a

    #@a
    .line 992
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@d
    move-result-object v0

    #@e
    .line 993
    .local v0, dirty:Landroid/graphics/Rect;
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@10
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@12
    add-int v1, v3, v4

    #@14
    .line 994
    .local v1, scrollX:I
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@16
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@18
    add-int v2, v3, v4

    #@1a
    .line 996
    .local v2, scrollY:I
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@1c
    add-int/2addr v3, v1

    #@1d
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@1f
    add-int/2addr v4, v2

    #@20
    iget v5, v0, Landroid/graphics/Rect;->right:I

    #@22
    add-int/2addr v5, v1

    #@23
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    #@25
    add-int/2addr v6, v2

    #@26
    invoke-virtual {p0, v3, v4, v5, v6}, Landroid/widget/ProgressBar;->invalidate(IIII)V

    #@29
    .line 1002
    .end local v0           #dirty:Landroid/graphics/Rect;
    .end local v1           #scrollX:I
    .end local v2           #scrollY:I
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 999
    :cond_2a
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@2d
    goto :goto_29
.end method

.method public declared-synchronized isIndeterminate()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "progress"
    .end annotation

    #@0
    .prologue
    .line 425
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 563
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@3
    .line 564
    iget-object v0, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 565
    :cond_c
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@e
    if-eqz v0, :cond_15

    #@10
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@15
    .line 566
    :cond_15
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 1190
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@4
    .line 1191
    iget-boolean v3, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@6
    if-eqz v3, :cond_b

    #@8
    .line 1192
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->startAnimation()V

    #@b
    .line 1194
    :cond_b
    iget-object v3, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@d
    if-eqz v3, :cond_3a

    #@f
    .line 1195
    monitor-enter p0

    #@10
    .line 1196
    :try_start_10
    iget-object v3, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v0

    #@16
    .line 1197
    .local v0, count:I
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    :goto_17
    if-ge v1, v0, :cond_31

    #@19
    .line 1198
    iget-object v3, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Landroid/widget/ProgressBar$RefreshData;

    #@21
    .line 1199
    .local v2, rd:Landroid/widget/ProgressBar$RefreshData;
    iget v3, v2, Landroid/widget/ProgressBar$RefreshData;->id:I

    #@23
    iget v4, v2, Landroid/widget/ProgressBar$RefreshData;->progress:I

    #@25
    iget-boolean v5, v2, Landroid/widget/ProgressBar$RefreshData;->fromUser:Z

    #@27
    const/4 v6, 0x1

    #@28
    invoke-direct {p0, v3, v4, v5, v6}, Landroid/widget/ProgressBar;->doRefreshProgress(IIZZ)V

    #@2b
    .line 1200
    invoke-virtual {v2}, Landroid/widget/ProgressBar$RefreshData;->recycle()V

    #@2e
    .line 1197
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_17

    #@31
    .line 1202
    .end local v2           #rd:Landroid/widget/ProgressBar$RefreshData;
    :cond_31
    iget-object v3, p0, Landroid/widget/ProgressBar;->mRefreshData:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@36
    .line 1204
    const/4 v3, 0x0

    #@37
    iput-boolean v3, p0, Landroid/widget/ProgressBar;->mRefreshIsPosted:Z

    #@39
    .line 1206
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_10 .. :try_end_3a} :catchall_3d

    #@3a
    .line 1208
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_3a
    iput-boolean v7, p0, Landroid/widget/ProgressBar;->mAttached:Z

    #@3c
    .line 1209
    return-void

    #@3d
    .line 1206
    :catchall_3d
    move-exception v3

    #@3e
    :try_start_3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v3
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 1213
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1214
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->stopAnimation()V

    #@7
    .line 1216
    :cond_7
    iget-object v0, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 1217
    iget-object v0, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@10
    .line 1219
    :cond_10
    iget-object v0, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@12
    if-eqz v0, :cond_1d

    #@14
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mRefreshIsPosted:Z

    #@16
    if-eqz v0, :cond_1d

    #@18
    .line 1220
    iget-object v0, p0, Landroid/widget/ProgressBar;->mRefreshProgressRunnable:Landroid/widget/ProgressBar$RefreshProgressRunnable;

    #@1a
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1d
    .line 1222
    :cond_1d
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@1f
    if-eqz v0, :cond_26

    #@21
    .line 1223
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAccessibilityEventSender:Landroid/widget/ProgressBar$AccessibilityEventSender;

    #@23
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@26
    .line 1227
    :cond_26
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@29
    .line 1228
    const/4 v0, 0x0

    #@2a
    iput-boolean v0, p0, Landroid/widget/ProgressBar;->mAttached:Z

    #@2c
    .line 1229
    return-void
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter "canvas"

    #@0
    .prologue
    .line 1058
    monitor-enter p0

    #@1
    :try_start_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@4
    .line 1060
    iget-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    .line 1061
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_62

    #@8
    .line 1064
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@b
    .line 1065
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->isLayoutRtl()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_64

    #@11
    .line 1066
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getWidth()I

    #@14
    move-result v4

    #@15
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@17
    sub-int/2addr v4, v5

    #@18
    int-to-float v4, v4

    #@19
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@1b
    int-to-float v5, v5

    #@1c
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@1f
    .line 1067
    const/high16 v4, -0x4080

    #@21
    const/high16 v5, 0x3f80

    #@23
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    #@26
    .line 1071
    :goto_26
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getDrawingTime()J

    #@29
    move-result-wide v2

    #@2a
    .line 1072
    .local v2, time:J
    iget-boolean v4, p0, Landroid/widget/ProgressBar;->mHasAnimation:Z

    #@2c
    if-eqz v4, :cond_4c

    #@2e
    .line 1073
    iget-object v4, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@30
    iget-object v5, p0, Landroid/widget/ProgressBar;->mTransformation:Landroid/view/animation/Transformation;

    #@32
    invoke-virtual {v4, v2, v3, v5}, Landroid/view/animation/AlphaAnimation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@35
    .line 1074
    iget-object v4, p0, Landroid/widget/ProgressBar;->mTransformation:Landroid/view/animation/Transformation;

    #@37
    invoke-virtual {v4}, Landroid/view/animation/Transformation;->getAlpha()F
    :try_end_3a
    .catchall {:try_start_1 .. :try_end_3a} :catchall_6e

    #@3a
    move-result v1

    #@3b
    .line 1076
    .local v1, scale:F
    const/4 v4, 0x1

    #@3c
    :try_start_3c
    iput-boolean v4, p0, Landroid/widget/ProgressBar;->mInDrawing:Z

    #@3e
    .line 1077
    const v4, 0x461c4000

    #@41
    mul-float/2addr v4, v1

    #@42
    float-to-int v4, v4

    #@43
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z
    :try_end_46
    .catchall {:try_start_3c .. :try_end_46} :catchall_71

    #@46
    .line 1079
    const/4 v4, 0x0

    #@47
    :try_start_47
    iput-boolean v4, p0, Landroid/widget/ProgressBar;->mInDrawing:Z

    #@49
    .line 1081
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidateOnAnimation()V

    #@4c
    .line 1083
    .end local v1           #scale:F
    :cond_4c
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@4f
    .line 1084
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@52
    .line 1085
    iget-boolean v4, p0, Landroid/widget/ProgressBar;->mShouldStartAnimationDrawable:Z

    #@54
    if-eqz v4, :cond_62

    #@56
    instance-of v4, v0, Landroid/graphics/drawable/Animatable;

    #@58
    if-eqz v4, :cond_62

    #@5a
    .line 1086
    check-cast v0, Landroid/graphics/drawable/Animatable;

    #@5c
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    #@5f
    .line 1087
    const/4 v4, 0x0

    #@60
    iput-boolean v4, p0, Landroid/widget/ProgressBar;->mShouldStartAnimationDrawable:Z
    :try_end_62
    .catchall {:try_start_47 .. :try_end_62} :catchall_6e

    #@62
    .line 1090
    .end local v2           #time:J
    :cond_62
    monitor-exit p0

    #@63
    return-void

    #@64
    .line 1069
    .restart local v0       #d:Landroid/graphics/drawable/Drawable;
    :cond_64
    :try_start_64
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@66
    int-to-float v4, v4

    #@67
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@69
    int-to-float v5, v5

    #@6a
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V
    :try_end_6d
    .catchall {:try_start_64 .. :try_end_6d} :catchall_6e

    #@6d
    goto :goto_26

    #@6e
    .line 1058
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    :catchall_6e
    move-exception v4

    #@6f
    monitor-exit p0

    #@70
    throw v4

    #@71
    .line 1079
    .restart local v0       #d:Landroid/graphics/drawable/Drawable;
    .restart local v1       #scale:F
    .restart local v2       #time:J
    :catchall_71
    move-exception v4

    #@72
    const/4 v5, 0x0

    #@73
    :try_start_73
    iput-boolean v5, p0, Landroid/widget/ProgressBar;->mInDrawing:Z

    #@75
    throw v4
    :try_end_76
    .catchall {:try_start_73 .. :try_end_76} :catchall_6e
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1233
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1234
    const-class v0, Landroid/widget/ProgressBar;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1235
    iget v0, p0, Landroid/widget/ProgressBar;->mMax:I

    #@e
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@11
    .line 1236
    iget v0, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@13
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    #@16
    .line 1237
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1241
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1242
    const-class v0, Landroid/widget/ProgressBar;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1243
    return-void
.end method

.method protected declared-synchronized onMeasure(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 1094
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    .line 1096
    .local v0, d:Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    #@4
    .line 1097
    .local v2, dw:I
    const/4 v1, 0x0

    #@5
    .line 1098
    .local v1, dh:I
    if-eqz v0, :cond_27

    #@7
    .line 1099
    iget v3, p0, Landroid/widget/ProgressBar;->mMinWidth:I

    #@9
    iget v4, p0, Landroid/widget/ProgressBar;->mMaxWidth:I

    #@b
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@e
    move-result v5

    #@f
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v4

    #@13
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@16
    move-result v2

    #@17
    .line 1100
    iget v3, p0, Landroid/widget/ProgressBar;->mMinHeight:I

    #@19
    iget v4, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@1b
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@1e
    move-result v5

    #@1f
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@22
    move-result v4

    #@23
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@26
    move-result v1

    #@27
    .line 1102
    :cond_27
    invoke-direct {p0}, Landroid/widget/ProgressBar;->updateDrawableState()V

    #@2a
    .line 1103
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@2c
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@2e
    add-int/2addr v3, v4

    #@2f
    add-int/2addr v2, v3

    #@30
    .line 1104
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@32
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@34
    add-int/2addr v3, v4

    #@35
    add-int/2addr v1, v3

    #@36
    .line 1106
    const/4 v3, 0x0

    #@37
    invoke-static {v2, p1, v3}, Landroid/widget/ProgressBar;->resolveSizeAndState(III)I

    #@3a
    move-result v3

    #@3b
    const/4 v4, 0x0

    #@3c
    invoke-static {v1, p2, v4}, Landroid/widget/ProgressBar;->resolveSizeAndState(III)I

    #@3f
    move-result v4

    #@40
    invoke-virtual {p0, v3, v4}, Landroid/widget/ProgressBar;->setMeasuredDimension(II)V
    :try_end_43
    .catchall {:try_start_1 .. :try_end_43} :catchall_45

    #@43
    .line 1108
    monitor-exit p0

    #@44
    return-void

    #@45
    .line 1094
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    .end local v1           #dh:I
    .end local v2           #dw:I
    :catchall_45
    move-exception v3

    #@46
    monitor-exit p0

    #@47
    throw v3
.end method

.method onProgressRefresh(FZ)V
    .registers 4
    .parameter "scale"
    .parameter "fromUser"

    #@0
    .prologue
    .line 691
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 692
    invoke-direct {p0}, Landroid/widget/ProgressBar;->scheduleAccessibilityEventSender()V

    #@f
    .line 694
    :cond_f
    return-void
.end method

.method public onResolveDrawables(I)V
    .registers 4
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 573
    iget-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 574
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_7

    #@4
    .line 575
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@7
    .line 577
    :cond_7
    iget-object v1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 578
    iget-object v1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@10
    .line 580
    :cond_10
    iget-object v1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    if-eqz v1, :cond_19

    #@14
    .line 581
    iget-object v1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@19
    .line 583
    :cond_19
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1181
    move-object v0, p1

    #@1
    check-cast v0, Landroid/widget/ProgressBar$SavedState;

    #@3
    .line 1182
    .local v0, ss:Landroid/widget/ProgressBar$SavedState;
    invoke-virtual {v0}, Landroid/widget/ProgressBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 1184
    iget v1, v0, Landroid/widget/ProgressBar$SavedState;->progress:I

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@f
    .line 1185
    iget v1, v0, Landroid/widget/ProgressBar$SavedState;->secondaryProgress:I

    #@11
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    #@14
    .line 1186
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 1170
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 1171
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Landroid/widget/ProgressBar$SavedState;

    #@6
    invoke-direct {v0, v1}, Landroid/widget/ProgressBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@9
    .line 1173
    .local v0, ss:Landroid/widget/ProgressBar$SavedState;
    iget v2, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@b
    iput v2, v0, Landroid/widget/ProgressBar$SavedState;->progress:I

    #@d
    .line 1174
    iget v2, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@f
    iput v2, v0, Landroid/widget/ProgressBar$SavedState;->secondaryProgress:I

    #@11
    .line 1176
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .registers 5
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 1006
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;->updateDrawableBounds(II)V

    #@3
    .line 1007
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 976
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 978
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 980
    const/16 v0, 0x8

    #@9
    if-eq p2, v0, :cond_e

    #@b
    const/4 v0, 0x4

    #@c
    if-ne p2, v0, :cond_12

    #@e
    .line 981
    :cond_e
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->stopAnimation()V

    #@11
    .line 986
    :cond_11
    :goto_11
    return-void

    #@12
    .line 983
    :cond_12
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->startAnimation()V

    #@15
    goto :goto_11
.end method

.method public postInvalidate()V
    .registers 2

    #@0
    .prologue
    .line 587
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mNoInvalidate:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 588
    invoke-super {p0}, Landroid/view/View;->postInvalidate()V

    #@7
    .line 590
    :cond_7
    return-void
.end method

.method public declared-synchronized setIndeterminate(Z)V
    .registers 3
    .parameter "indeterminate"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 440
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mOnlyIndeterminate:Z

    #@3
    if-eqz v0, :cond_9

    #@5
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@7
    if-nez v0, :cond_18

    #@9
    :cond_9
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@b
    if-eq p1, v0, :cond_18

    #@d
    .line 441
    iput-boolean p1, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@f
    .line 443
    if-eqz p1, :cond_1a

    #@11
    .line 445
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    iput-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    .line 446
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->startAnimation()V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_22

    #@18
    .line 452
    :cond_18
    :goto_18
    monitor-exit p0

    #@19
    return-void

    #@1a
    .line 448
    :cond_1a
    :try_start_1a
    iget-object v0, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@1c
    iput-object v0, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 449
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->stopAnimation()V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_22

    #@21
    goto :goto_18

    #@22
    .line 440
    :catchall_22
    move-exception v0

    #@23
    monitor-exit p0

    #@24
    throw v0
.end method

.method public setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 477
    if-eqz p1, :cond_5

    #@2
    .line 478
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@5
    .line 480
    :cond_5
    iput-object p1, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@7
    .line 481
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v0, :cond_1a

    #@b
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->canResolveLayoutDirection()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 482
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getLayoutDirection()I

    #@16
    move-result v1

    #@17
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@1a
    .line 484
    :cond_1a
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@1c
    if-eqz v0, :cond_23

    #@1e
    .line 485
    iput-object p1, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@20
    .line 486
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidate()V

    #@23
    .line 488
    :cond_23
    return-void
.end method

.method public setInterpolator(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resID"

    #@0
    .prologue
    .line 935
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@7
    .line 936
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .registers 2
    .parameter "interpolator"

    #@0
    .prologue
    .line 945
    iput-object p1, p0, Landroid/widget/ProgressBar;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    .line 946
    return-void
.end method

.method public declared-synchronized setMax(I)V
    .registers 5
    .parameter "max"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 840
    monitor-enter p0

    #@1
    if-gez p1, :cond_4

    #@3
    .line 841
    const/4 p1, 0x0

    #@4
    .line 843
    :cond_4
    :try_start_4
    iget v0, p0, Landroid/widget/ProgressBar;->mMax:I

    #@6
    if-eq p1, v0, :cond_1c

    #@8
    .line 844
    iput p1, p0, Landroid/widget/ProgressBar;->mMax:I

    #@a
    .line 845
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidate()V

    #@d
    .line 847
    iget v0, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@f
    if-le v0, p1, :cond_13

    #@11
    .line 848
    iput p1, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@13
    .line 850
    :cond_13
    const v0, 0x102000d

    #@16
    iget v1, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@18
    const/4 v2, 0x0

    #@19
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/ProgressBar;->refreshProgress(IIZ)V
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_1e

    #@1c
    .line 852
    :cond_1c
    monitor-exit p0

    #@1d
    return-void

    #@1e
    .line 840
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0

    #@20
    throw v0
.end method

.method public declared-synchronized setProgress(I)V
    .registers 3
    .parameter "progress"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 726
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    invoke-virtual {p0, p1, v0}, Landroid/widget/ProgressBar;->setProgress(IZ)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    #@5
    .line 727
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 726
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method declared-synchronized setProgress(IZ)V
    .registers 5
    .parameter "progress"
    .parameter "fromUser"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 731
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1f

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 747
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 735
    :cond_7
    if-gez p1, :cond_a

    #@9
    .line 736
    const/4 p1, 0x0

    #@a
    .line 739
    :cond_a
    :try_start_a
    iget v0, p0, Landroid/widget/ProgressBar;->mMax:I

    #@c
    if-le p1, v0, :cond_10

    #@e
    .line 740
    iget p1, p0, Landroid/widget/ProgressBar;->mMax:I

    #@10
    .line 743
    :cond_10
    iget v0, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@12
    if-eq p1, v0, :cond_5

    #@14
    .line 744
    iput p1, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@16
    .line 745
    const v0, 0x102000d

    #@19
    iget v1, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@1b
    invoke-direct {p0, v0, v1, p2}, Landroid/widget/ProgressBar;->refreshProgress(IIZ)V
    :try_end_1e
    .catchall {:try_start_a .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_5

    #@1f
    .line 731
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0

    #@21
    throw v0
.end method

.method public setProgressDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 7
    .parameter "d"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 514
    iget-object v2, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    if-eqz v2, :cond_5b

    #@5
    iget-object v2, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@7
    if-eq p1, v2, :cond_5b

    #@9
    .line 515
    iget-object v2, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@b
    const/4 v3, 0x0

    #@c
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@f
    .line 516
    const/4 v1, 0x1

    #@10
    .line 521
    .local v1, needUpdate:Z
    :goto_10
    if-eqz p1, :cond_2f

    #@12
    .line 522
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@15
    .line 523
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->canResolveLayoutDirection()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_22

    #@1b
    .line 524
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getLayoutDirection()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@22
    .line 528
    :cond_22
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@25
    move-result v0

    #@26
    .line 529
    .local v0, drawableHeight:I
    iget v2, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@28
    if-ge v2, v0, :cond_2f

    #@2a
    .line 530
    iput v0, p0, Landroid/widget/ProgressBar;->mMaxHeight:I

    #@2c
    .line 531
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->requestLayout()V

    #@2f
    .line 534
    .end local v0           #drawableHeight:I
    :cond_2f
    iput-object p1, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@31
    .line 535
    iget-boolean v2, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@33
    if-nez v2, :cond_3a

    #@35
    .line 536
    iput-object p1, p0, Landroid/widget/ProgressBar;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    #@37
    .line 537
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidate()V

    #@3a
    .line 540
    :cond_3a
    if-eqz v1, :cond_5a

    #@3c
    .line 541
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getWidth()I

    #@3f
    move-result v2

    #@40
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getHeight()I

    #@43
    move-result v3

    #@44
    invoke-direct {p0, v2, v3}, Landroid/widget/ProgressBar;->updateDrawableBounds(II)V

    #@47
    .line 542
    invoke-direct {p0}, Landroid/widget/ProgressBar;->updateDrawableState()V

    #@4a
    .line 543
    const v2, 0x102000d

    #@4d
    iget v3, p0, Landroid/widget/ProgressBar;->mProgress:I

    #@4f
    invoke-direct {p0, v2, v3, v4, v4}, Landroid/widget/ProgressBar;->doRefreshProgress(IIZZ)V

    #@52
    .line 544
    const v2, 0x102000f

    #@55
    iget v3, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@57
    invoke-direct {p0, v2, v3, v4, v4}, Landroid/widget/ProgressBar;->doRefreshProgress(IIZZ)V

    #@5a
    .line 546
    :cond_5a
    return-void

    #@5b
    .line 518
    .end local v1           #needUpdate:Z
    :cond_5b
    const/4 v1, 0x0

    #@5c
    .restart local v1       #needUpdate:Z
    goto :goto_10
.end method

.method public declared-synchronized setSecondaryProgress(I)V
    .registers 5
    .parameter "secondaryProgress"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 763
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_20

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 779
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 767
    :cond_7
    if-gez p1, :cond_a

    #@9
    .line 768
    const/4 p1, 0x0

    #@a
    .line 771
    :cond_a
    :try_start_a
    iget v0, p0, Landroid/widget/ProgressBar;->mMax:I

    #@c
    if-le p1, v0, :cond_10

    #@e
    .line 772
    iget p1, p0, Landroid/widget/ProgressBar;->mMax:I

    #@10
    .line 775
    :cond_10
    iget v0, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@12
    if-eq p1, v0, :cond_5

    #@14
    .line 776
    iput p1, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@16
    .line 777
    const v0, 0x102000f

    #@19
    iget v1, p0, Landroid/widget/ProgressBar;->mSecondaryProgress:I

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/ProgressBar;->refreshProgress(IIZ)V
    :try_end_1f
    .catchall {:try_start_a .. :try_end_1f} :catchall_20

    #@1f
    goto :goto_5

    #@20
    .line 763
    :catchall_20
    move-exception v0

    #@21
    monitor-exit p0

    #@22
    throw v0
.end method

.method public setVisibility(I)V
    .registers 3
    .parameter "v"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 960
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getVisibility()I

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_17

    #@6
    .line 961
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    #@9
    .line 963
    iget-boolean v0, p0, Landroid/widget/ProgressBar;->mIndeterminate:Z

    #@b
    if-eqz v0, :cond_17

    #@d
    .line 965
    const/16 v0, 0x8

    #@f
    if-eq p1, v0, :cond_14

    #@11
    const/4 v0, 0x4

    #@12
    if-ne p1, v0, :cond_18

    #@14
    .line 966
    :cond_14
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->stopAnimation()V

    #@17
    .line 972
    :cond_17
    :goto_17
    return-void

    #@18
    .line 968
    :cond_18
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->startAnimation()V

    #@1b
    goto :goto_17
.end method

.method startAnimation()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 880
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getVisibility()I

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_8

    #@7
    .line 913
    :goto_7
    return-void

    #@8
    .line 884
    :cond_8
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    instance-of v0, v0, Landroid/graphics/drawable/Animatable;

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 885
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mShouldStartAnimationDrawable:Z

    #@10
    .line 886
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/widget/ProgressBar;->mHasAnimation:Z

    #@13
    .line 912
    :goto_13
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidate()V

    #@16
    goto :goto_7

    #@17
    .line 888
    :cond_17
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mHasAnimation:Z

    #@19
    .line 890
    iget-object v0, p0, Landroid/widget/ProgressBar;->mInterpolator:Landroid/view/animation/Interpolator;

    #@1b
    if-nez v0, :cond_24

    #@1d
    .line 891
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    #@1f
    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@22
    iput-object v0, p0, Landroid/widget/ProgressBar;->mInterpolator:Landroid/view/animation/Interpolator;

    #@24
    .line 894
    :cond_24
    iget-object v0, p0, Landroid/widget/ProgressBar;->mTransformation:Landroid/view/animation/Transformation;

    #@26
    if-nez v0, :cond_61

    #@28
    .line 895
    new-instance v0, Landroid/view/animation/Transformation;

    #@2a
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@2d
    iput-object v0, p0, Landroid/widget/ProgressBar;->mTransformation:Landroid/view/animation/Transformation;

    #@2f
    .line 900
    :goto_2f
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@31
    if-nez v0, :cond_67

    #@33
    .line 901
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    #@35
    const/4 v1, 0x0

    #@36
    const/high16 v2, 0x3f80

    #@38
    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    #@3b
    iput-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@3d
    .line 906
    :goto_3d
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@3f
    iget v1, p0, Landroid/widget/ProgressBar;->mBehavior:I

    #@41
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    #@44
    .line 907
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@46
    const/4 v1, -0x1

    #@47
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    #@4a
    .line 908
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@4c
    iget v1, p0, Landroid/widget/ProgressBar;->mDuration:I

    #@4e
    int-to-long v1, v1

    #@4f
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    #@52
    .line 909
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@54
    iget-object v1, p0, Landroid/widget/ProgressBar;->mInterpolator:Landroid/view/animation/Interpolator;

    #@56
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@59
    .line 910
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@5b
    const-wide/16 v1, -0x1

    #@5d
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setStartTime(J)V

    #@60
    goto :goto_13

    #@61
    .line 897
    :cond_61
    iget-object v0, p0, Landroid/widget/ProgressBar;->mTransformation:Landroid/view/animation/Transformation;

    #@63
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->clear()V

    #@66
    goto :goto_2f

    #@67
    .line 903
    :cond_67
    iget-object v0, p0, Landroid/widget/ProgressBar;->mAnimation:Landroid/view/animation/AlphaAnimation;

    #@69
    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->reset()V

    #@6c
    goto :goto_3d
.end method

.method stopAnimation()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 919
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mHasAnimation:Z

    #@3
    .line 920
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    instance-of v0, v0, Landroid/graphics/drawable/Animatable;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 921
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@b
    check-cast v0, Landroid/graphics/drawable/Animatable;

    #@d
    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    #@10
    .line 922
    iput-boolean v1, p0, Landroid/widget/ProgressBar;->mShouldStartAnimationDrawable:Z

    #@12
    .line 924
    :cond_12
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->postInvalidate()V

    #@15
    .line 925
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 557
    iget-object v0, p0, Landroid/widget/ProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq p1, v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/ProgressBar;->mIndeterminateDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eq p1, v0, :cond_e

    #@8
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method
