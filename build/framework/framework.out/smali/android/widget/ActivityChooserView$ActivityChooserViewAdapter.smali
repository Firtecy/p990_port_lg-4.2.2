.class Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "ActivityChooserView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ActivityChooserView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityChooserViewAdapter"
.end annotation


# static fields
.field private static final ITEM_VIEW_TYPE_ACTIVITY:I = 0x0

.field private static final ITEM_VIEW_TYPE_COUNT:I = 0x3

.field private static final ITEM_VIEW_TYPE_FOOTER:I = 0x1

.field public static final MAX_ACTIVITY_COUNT_DEFAULT:I = 0x4

.field public static final MAX_ACTIVITY_COUNT_UNLIMITED:I = 0x7fffffff


# instance fields
.field private mDataModel:Landroid/widget/ActivityChooserModel;

.field private mHighlightDefaultActivity:Z

.field private mMaxActivityCount:I

.field private mShowDefaultActivity:Z

.field private mShowFooterView:Z

.field final synthetic this$0:Landroid/widget/ActivityChooserView;


# direct methods
.method private constructor <init>(Landroid/widget/ActivityChooserView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 610
    iput-object p1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@2
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@5
    .line 624
    const/4 v0, 0x4

    #@6
    iput v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@8
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ActivityChooserView;Landroid/widget/ActivityChooserView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 610
    invoke-direct {p0, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;-><init>(Landroid/widget/ActivityChooserView;)V

    #@3
    return-void
.end method


# virtual methods
.method public getActivityCount()I
    .registers 2

    #@0
    .prologue
    .line 771
    iget-object v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@2
    invoke-virtual {v0}, Landroid/widget/ActivityChooserModel;->getActivityCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCount()I
    .registers 4

    #@0
    .prologue
    .line 659
    const/4 v1, 0x0

    #@1
    .line 660
    .local v1, count:I
    iget-object v2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@3
    invoke-virtual {v2}, Landroid/widget/ActivityChooserModel;->getActivityCount()I

    #@6
    move-result v0

    #@7
    .line 661
    .local v0, activityCount:I
    iget-boolean v2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@9
    if-nez v2, :cond_15

    #@b
    iget-object v2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@d
    invoke-virtual {v2}, Landroid/widget/ActivityChooserModel;->getDefaultActivity()Landroid/content/pm/ResolveInfo;

    #@10
    move-result-object v2

    #@11
    if-eqz v2, :cond_15

    #@13
    .line 662
    add-int/lit8 v0, v0, -0x1

    #@15
    .line 664
    :cond_15
    iget v2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@17
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    #@1a
    move-result v1

    #@1b
    .line 665
    iget-boolean v2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowFooterView:Z

    #@1d
    if-eqz v2, :cond_21

    #@1f
    .line 666
    add-int/lit8 v1, v1, 0x1

    #@21
    .line 668
    :cond_21
    return v1
.end method

.method public getDataModel()Landroid/widget/ActivityChooserModel;
    .registers 2

    #@0
    .prologue
    .line 783
    iget-object v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@2
    return-object v0
.end method

.method public getDefaultActivity()Landroid/content/pm/ResolveInfo;
    .registers 2

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@2
    invoke-virtual {v0}, Landroid/widget/ActivityChooserModel;->getDefaultActivity()Landroid/content/pm/ResolveInfo;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHistorySize()I
    .registers 2

    #@0
    .prologue
    .line 775
    iget-object v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@2
    invoke-virtual {v0}, Landroid/widget/ActivityChooserModel;->getHistorySize()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 672
    invoke-virtual {p0, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getItemViewType(I)I

    #@3
    move-result v0

    #@4
    .line 673
    .local v0, itemViewType:I
    packed-switch v0, :pswitch_data_24

    #@7
    .line 682
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@c
    throw v1

    #@d
    .line 675
    :pswitch_d
    const/4 v1, 0x0

    #@e
    .line 680
    :goto_e
    return-object v1

    #@f
    .line 677
    :pswitch_f
    iget-boolean v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@11
    if-nez v1, :cond_1d

    #@13
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@15
    invoke-virtual {v1}, Landroid/widget/ActivityChooserModel;->getDefaultActivity()Landroid/content/pm/ResolveInfo;

    #@18
    move-result-object v1

    #@19
    if-eqz v1, :cond_1d

    #@1b
    .line 678
    add-int/lit8 p1, p1, 0x1

    #@1d
    .line 680
    :cond_1d
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@1f
    invoke-virtual {v1, p1}, Landroid/widget/ActivityChooserModel;->getActivity(I)Landroid/content/pm/ResolveInfo;

    #@22
    move-result-object v1

    #@23
    goto :goto_e

    #@24
    .line 673
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_f
        :pswitch_d
    .end packed-switch
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 687
    int-to-long v0, p1

    #@1
    return-wide v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 646
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowFooterView:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getCount()I

    #@7
    move-result v0

    #@8
    add-int/lit8 v0, v0, -0x1

    #@a
    if-ne p1, v0, :cond_e

    #@c
    .line 647
    const/4 v0, 0x1

    #@d
    .line 649
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getMaxActivityCount()I
    .registers 2

    #@0
    .prologue
    .line 779
    iget v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@2
    return v0
.end method

.method public getShowDefaultActivity()Z
    .registers 2

    #@0
    .prologue
    .line 797
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@2
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 16
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const v11, 0x1090020

    #@3
    const v10, 0x1020016

    #@6
    const/4 v9, 0x1

    #@7
    const/4 v8, 0x0

    #@8
    .line 691
    invoke-virtual {p0, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getItemViewType(I)I

    #@b
    move-result v3

    #@c
    .line 692
    .local v3, itemViewType:I
    packed-switch v3, :pswitch_data_a0

    #@f
    .line 724
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@11
    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@14
    throw v6

    #@15
    .line 694
    :pswitch_15
    if-eqz p2, :cond_1d

    #@17
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    #@1a
    move-result v6

    #@1b
    if-eq v6, v9, :cond_44

    #@1d
    .line 695
    :cond_1d
    iget-object v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@1f
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    #@22
    move-result-object v6

    #@23
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, v11, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@2a
    move-result-object p2

    #@2b
    .line 697
    invoke-virtual {p2, v9}, Landroid/view/View;->setId(I)V

    #@2e
    .line 698
    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@31
    move-result-object v5

    #@32
    check-cast v5, Landroid/widget/TextView;

    #@34
    .line 699
    .local v5, titleView:Landroid/widget/TextView;
    iget-object v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@36
    invoke-static {v6}, Landroid/widget/ActivityChooserView;->access$1400(Landroid/widget/ActivityChooserView;)Landroid/content/Context;

    #@39
    move-result-object v6

    #@3a
    const v7, 0x104052d

    #@3d
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@44
    .end local v5           #titleView:Landroid/widget/TextView;
    :cond_44
    move-object v1, p2

    #@45
    .line 722
    .end local p2
    .local v1, convertView:Landroid/view/View;
    :goto_45
    return-object v1

    #@46
    .line 704
    .end local v1           #convertView:Landroid/view/View;
    .restart local p2
    :pswitch_46
    if-eqz p2, :cond_51

    #@48
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    #@4b
    move-result v6

    #@4c
    const v7, 0x1020263

    #@4f
    if-eq v6, v7, :cond_5f

    #@51
    .line 705
    :cond_51
    iget-object v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@53
    invoke-virtual {v6}, Landroid/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    #@56
    move-result-object v6

    #@57
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6, v11, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@5e
    move-result-object p2

    #@5f
    .line 708
    :cond_5f
    iget-object v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@61
    invoke-static {v6}, Landroid/widget/ActivityChooserView;->access$1500(Landroid/widget/ActivityChooserView;)Landroid/content/Context;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@68
    move-result-object v4

    #@69
    .line 710
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    const v6, 0x1020006

    #@6c
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6f
    move-result-object v2

    #@70
    check-cast v2, Landroid/widget/ImageView;

    #@72
    .line 711
    .local v2, iconView:Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getItem(I)Ljava/lang/Object;

    #@75
    move-result-object v0

    #@76
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@78
    .line 712
    .local v0, activity:Landroid/content/pm/ResolveInfo;
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@7f
    .line 714
    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@82
    move-result-object v5

    #@83
    check-cast v5, Landroid/widget/TextView;

    #@85
    .line 715
    .restart local v5       #titleView:Landroid/widget/TextView;
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8c
    .line 717
    iget-boolean v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@8e
    if-eqz v6, :cond_9b

    #@90
    if-nez p1, :cond_9b

    #@92
    iget-boolean v6, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mHighlightDefaultActivity:Z

    #@94
    if-eqz v6, :cond_9b

    #@96
    .line 718
    invoke-virtual {p2, v9}, Landroid/view/View;->setActivated(Z)V

    #@99
    :goto_99
    move-object v1, p2

    #@9a
    .line 722
    .end local p2
    .restart local v1       #convertView:Landroid/view/View;
    goto :goto_45

    #@9b
    .line 720
    .end local v1           #convertView:Landroid/view/View;
    .restart local p2
    :cond_9b
    invoke-virtual {p2, v8}, Landroid/view/View;->setActivated(Z)V

    #@9e
    goto :goto_99

    #@9f
    .line 692
    nop

    #@a0
    :pswitch_data_a0
    .packed-switch 0x0
        :pswitch_46
        :pswitch_15
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 655
    const/4 v0, 0x3

    #@1
    return v0
.end method

.method public measureContentWidth()I
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 731
    iget v5, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@3
    .line 732
    .local v5, oldMaxActivityCount:I
    const v7, 0x7fffffff

    #@6
    iput v7, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@8
    .line 734
    const/4 v0, 0x0

    #@9
    .line 735
    .local v0, contentWidth:I
    const/4 v4, 0x0

    #@a
    .line 737
    .local v4, itemView:Landroid/view/View;
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@d
    move-result v6

    #@e
    .line 738
    .local v6, widthMeasureSpec:I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@11
    move-result v2

    #@12
    .line 739
    .local v2, heightMeasureSpec:I
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getCount()I

    #@15
    move-result v1

    #@16
    .line 741
    .local v1, count:I
    const/4 v3, 0x0

    #@17
    .local v3, i:I
    :goto_17
    if-ge v3, v1, :cond_2c

    #@19
    .line 742
    const/4 v7, 0x0

    #@1a
    invoke-virtual {p0, v3, v4, v7}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@1d
    move-result-object v4

    #@1e
    .line 743
    invoke-virtual {v4, v6, v2}, Landroid/view/View;->measure(II)V

    #@21
    .line 744
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@24
    move-result v7

    #@25
    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    #@28
    move-result v0

    #@29
    .line 741
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_17

    #@2c
    .line 747
    :cond_2c
    iput v5, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@2e
    .line 749
    return v0
.end method

.method public setDataModel(Landroid/widget/ActivityChooserModel;)V
    .registers 4
    .parameter "dataModel"

    #@0
    .prologue
    .line 633
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@2
    invoke-static {v1}, Landroid/widget/ActivityChooserView;->access$000(Landroid/widget/ActivityChooserView;)Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->getDataModel()Landroid/widget/ActivityChooserModel;

    #@9
    move-result-object v0

    #@a
    .line 634
    .local v0, oldDataModel:Landroid/widget/ActivityChooserModel;
    if-eqz v0, :cond_1d

    #@c
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@e
    invoke-virtual {v1}, Landroid/widget/ActivityChooserView;->isShown()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1d

    #@14
    .line 635
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@16
    invoke-static {v1}, Landroid/widget/ActivityChooserView;->access$1300(Landroid/widget/ActivityChooserView;)Landroid/database/DataSetObserver;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Landroid/widget/ActivityChooserModel;->unregisterObserver(Ljava/lang/Object;)V

    #@1d
    .line 637
    :cond_1d
    iput-object p1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mDataModel:Landroid/widget/ActivityChooserModel;

    #@1f
    .line 638
    if-eqz p1, :cond_32

    #@21
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@23
    invoke-virtual {v1}, Landroid/widget/ActivityChooserView;->isShown()Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_32

    #@29
    .line 639
    iget-object v1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->this$0:Landroid/widget/ActivityChooserView;

    #@2b
    invoke-static {v1}, Landroid/widget/ActivityChooserView;->access$1300(Landroid/widget/ActivityChooserView;)Landroid/database/DataSetObserver;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {p1, v1}, Landroid/widget/ActivityChooserModel;->registerObserver(Ljava/lang/Object;)V

    #@32
    .line 641
    :cond_32
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->notifyDataSetChanged()V

    #@35
    .line 642
    return-void
.end method

.method public setMaxActivityCount(I)V
    .registers 3
    .parameter "maxActivityCount"

    #@0
    .prologue
    .line 753
    iget v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 754
    iput p1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mMaxActivityCount:I

    #@6
    .line 755
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->notifyDataSetChanged()V

    #@9
    .line 757
    :cond_9
    return-void
.end method

.method public setShowDefaultActivity(ZZ)V
    .registers 4
    .parameter "showDefaultActivity"
    .parameter "highlightDefaultActivity"

    #@0
    .prologue
    .line 788
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@2
    if-ne v0, p1, :cond_8

    #@4
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mHighlightDefaultActivity:Z

    #@6
    if-eq v0, p2, :cond_f

    #@8
    .line 790
    :cond_8
    iput-boolean p1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowDefaultActivity:Z

    #@a
    .line 791
    iput-boolean p2, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mHighlightDefaultActivity:Z

    #@c
    .line 792
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->notifyDataSetChanged()V

    #@f
    .line 794
    :cond_f
    return-void
.end method

.method public setShowFooterView(Z)V
    .registers 3
    .parameter "showFooterView"

    #@0
    .prologue
    .line 764
    iget-boolean v0, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowFooterView:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 765
    iput-boolean p1, p0, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->mShowFooterView:Z

    #@6
    .line 766
    invoke-virtual {p0}, Landroid/widget/ActivityChooserView$ActivityChooserViewAdapter;->notifyDataSetChanged()V

    #@9
    .line 768
    :cond_9
    return-void
.end method
