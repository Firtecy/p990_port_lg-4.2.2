.class public abstract Landroid/widget/BaseExpandableListAdapter;
.super Ljava/lang/Object;
.source "BaseExpandableListAdapter.java"

# interfaces
.implements Landroid/widget/ExpandableListAdapter;
.implements Landroid/widget/HeterogeneousExpandableList;


# instance fields
.field private final mDataSetObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    new-instance v0, Landroid/database/DataSetObservable;

    #@5
    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/BaseExpandableListAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@a
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 60
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getChildType(II)I
    .registers 4
    .parameter "groupPosition"
    .parameter "childPosition"

    #@0
    .prologue
    .line 112
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getChildTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 120
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getCombinedChildId(JJ)J
    .registers 10
    .parameter "groupId"
    .parameter "childId"

    #@0
    .prologue
    .line 81
    const-wide/high16 v0, -0x8000

    #@2
    const-wide/32 v2, 0x7fffffff

    #@5
    and-long/2addr v2, p1

    #@6
    const/16 v4, 0x20

    #@8
    shl-long/2addr v2, v4

    #@9
    or-long/2addr v0, v2

    #@a
    const-wide/16 v2, -0x1

    #@c
    and-long/2addr v2, p3

    #@d
    or-long/2addr v0, v2

    #@e
    return-wide v0
.end method

.method public getCombinedGroupId(J)J
    .registers 6
    .parameter "groupId"

    #@0
    .prologue
    .line 96
    const-wide/32 v0, 0x7fffffff

    #@3
    and-long/2addr v0, p1

    #@4
    const/16 v2, 0x20

    #@6
    shl-long/2addr v0, v2

    #@7
    return-wide v0
.end method

.method public getGroupType(I)I
    .registers 3
    .parameter "groupPosition"

    #@0
    .prologue
    .line 128
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getGroupTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 136
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 103
    invoke-virtual {p0}, Landroid/widget/BaseExpandableListAdapter;->getGroupCount()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public notifyDataSetChanged()V
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/widget/BaseExpandableListAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    #@5
    .line 57
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .registers 2

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/widget/BaseExpandableListAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    #@5
    .line 50
    return-void
.end method

.method public onGroupCollapsed(I)V
    .registers 2
    .parameter "groupPosition"

    #@0
    .prologue
    .line 64
    return-void
.end method

.method public onGroupExpanded(I)V
    .registers 2
    .parameter "groupPosition"

    #@0
    .prologue
    .line 67
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/widget/BaseExpandableListAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    #@5
    .line 39
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/widget/BaseExpandableListAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    #@2
    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    #@5
    .line 43
    return-void
.end method
