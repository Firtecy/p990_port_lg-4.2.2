.class Landroid/widget/Editor$SuggestionsPopupWindow;
.super Landroid/widget/Editor$PinnedPopupWindow;
.source "Editor.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestionsPopupWindow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;,
        Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;,
        Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;,
        Landroid/widget/Editor$SuggestionsPopupWindow$CustomPopupWindow;
    }
.end annotation


# static fields
.field private static final ADD_TO_DICTIONARY:I = -0x1

.field private static final DELETE_TEXT:I = -0x2

.field private static final MAX_NUMBER_SUGGESTIONS:I = 0x5


# instance fields
.field private mCursorWasVisibleBeforeSuggestions:Z

.field private mIsShowingUp:Z

.field private mNumberOfSuggestions:I

.field private final mSpansLengths:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/text/style/SuggestionSpan;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

.field private final mSuggestionSpanComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/text/style/SuggestionSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionsAdapter:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 2730
    iput-object p1, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct {p0, p1}, Landroid/widget/Editor$PinnedPopupWindow;-><init>(Landroid/widget/Editor;)V

    #@5
    .line 2704
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mIsShowingUp:Z

    #@8
    .line 2731
    iget-boolean v0, p1, Landroid/widget/Editor;->mCursorVisible:Z

    #@a
    iput-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mCursorWasVisibleBeforeSuggestions:Z

    #@c
    .line 2732
    new-instance v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-direct {v0, p0, v1}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionSpanComparator;-><init>(Landroid/widget/Editor$SuggestionsPopupWindow;Landroid/widget/Editor$1;)V

    #@12
    iput-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionSpanComparator:Ljava/util/Comparator;

    #@14
    .line 2733
    new-instance v0, Ljava/util/HashMap;

    #@16
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@19
    iput-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSpansLengths:Ljava/util/HashMap;

    #@1b
    .line 2734
    return-void
.end method

.method static synthetic access$1500(Landroid/widget/Editor$SuggestionsPopupWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2697
    iget-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mCursorWasVisibleBeforeSuggestions:Z

    #@2
    return v0
.end method

.method static synthetic access$1900(Landroid/widget/Editor$SuggestionsPopupWindow;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2697
    iget v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@2
    return v0
.end method

.method static synthetic access$2000(Landroid/widget/Editor$SuggestionsPopupWindow;)[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2697
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Landroid/widget/Editor$SuggestionsPopupWindow;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2697
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSpansLengths:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method private getSuggestionSpans()[Landroid/text/style/SuggestionSpan;
    .registers 12

    #@0
    .prologue
    .line 2846
    iget-object v9, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v9}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v9

    #@6
    invoke-virtual {v9}, Landroid/widget/TextView;->getSelectionStart()I

    #@9
    move-result v4

    #@a
    .line 2847
    .local v4, pos:I
    iget-object v9, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@c
    invoke-static {v9}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@f
    move-result-object v9

    #@10
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@13
    move-result-object v5

    #@14
    check-cast v5, Landroid/text/Spannable;

    #@16
    .line 2848
    .local v5, spannable:Landroid/text/Spannable;
    const-class v9, Landroid/text/style/SuggestionSpan;

    #@18
    invoke-interface {v5, v4, v4, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@1b
    move-result-object v8

    #@1c
    check-cast v8, [Landroid/text/style/SuggestionSpan;

    #@1e
    .line 2850
    .local v8, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    iget-object v9, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSpansLengths:Ljava/util/HashMap;

    #@20
    invoke-virtual {v9}, Ljava/util/HashMap;->clear()V

    #@23
    .line 2851
    move-object v0, v8

    #@24
    .local v0, arr$:[Landroid/text/style/SuggestionSpan;
    array-length v3, v0

    #@25
    .local v3, len$:I
    const/4 v2, 0x0

    #@26
    .local v2, i$:I
    :goto_26
    if-ge v2, v3, :cond_40

    #@28
    aget-object v7, v0, v2

    #@2a
    .line 2852
    .local v7, suggestionSpan:Landroid/text/style/SuggestionSpan;
    invoke-interface {v5, v7}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@2d
    move-result v6

    #@2e
    .line 2853
    .local v6, start:I
    invoke-interface {v5, v7}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@31
    move-result v1

    #@32
    .line 2854
    .local v1, end:I
    iget-object v9, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSpansLengths:Ljava/util/HashMap;

    #@34
    sub-int v10, v1, v6

    #@36
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v10

    #@3a
    invoke-virtual {v9, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 2851
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_26

    #@40
    .line 2859
    .end local v1           #end:I
    .end local v6           #start:I
    .end local v7           #suggestionSpan:Landroid/text/style/SuggestionSpan;
    :cond_40
    iget-object v9, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionSpanComparator:Ljava/util/Comparator;

    #@42
    invoke-static {v8, v9}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    #@45
    .line 2860
    return-object v8
.end method

.method private highlightTextDifferences(Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;II)V
    .registers 13
    .parameter "suggestionInfo"
    .parameter "unionStart"
    .parameter "unionEnd"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 3048
    iget-object v4, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@3
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6
    move-result-object v4

    #@7
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Landroid/text/Spannable;

    #@d
    .line 3049
    .local v2, text:Landroid/text/Spannable;
    iget-object v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@f
    invoke-interface {v2, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@12
    move-result v1

    #@13
    .line 3050
    .local v1, spanStart:I
    iget-object v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@15
    invoke-interface {v2, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@18
    move-result v0

    #@19
    .line 3053
    .local v0, spanEnd:I
    sub-int v4, v1, p2

    #@1b
    iput v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionStart:I

    #@1d
    .line 3054
    iget v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionStart:I

    #@1f
    iget-object v5, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@21
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    #@24
    move-result v5

    #@25
    add-int/2addr v4, v5

    #@26
    iput v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionEnd:I

    #@28
    .line 3057
    iget-object v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@2a
    iget-object v5, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->highlightSpan:Landroid/text/style/TextAppearanceSpan;

    #@2c
    iget-object v6, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@2e
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    #@31
    move-result v6

    #@32
    const/16 v7, 0x21

    #@34
    invoke-virtual {v4, v5, v8, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@37
    .line 3061
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    .line 3062
    .local v3, textAsString:Ljava/lang/String;
    iget-object v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@3d
    invoke-virtual {v3, p2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v4, v8, v5}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@44
    .line 3063
    iget-object v4, p1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@46
    invoke-virtual {v3, v0, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@4d
    .line 3064
    return-void
.end method

.method private updateSuggestions()Z
    .registers 36

    #@0
    .prologue
    .line 2929
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    move-object/from16 v30, v0

    #@6
    invoke-static/range {v30 .. v30}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v30

    #@a
    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@d
    move-result-object v21

    #@e
    check-cast v21, Landroid/text/Spannable;

    #@10
    .line 2930
    .local v21, spannable:Landroid/text/Spannable;
    invoke-direct/range {p0 .. p0}, Landroid/widget/Editor$SuggestionsPopupWindow;->getSuggestionSpans()[Landroid/text/style/SuggestionSpan;

    #@13
    move-result-object v27

    #@14
    .line 2932
    .local v27, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    move-object/from16 v0, v27

    #@16
    array-length v10, v0

    #@17
    .line 2934
    .local v10, nbSpans:I
    if-nez v10, :cond_1c

    #@19
    const/16 v30, 0x0

    #@1b
    .line 3043
    :goto_1b
    return v30

    #@1c
    .line 2936
    :cond_1c
    const/16 v30, 0x0

    #@1e
    move/from16 v0, v30

    #@20
    move-object/from16 v1, p0

    #@22
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@24
    .line 2937
    move-object/from16 v0, p0

    #@26
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@28
    move-object/from16 v30, v0

    #@2a
    invoke-static/range {v30 .. v30}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2d
    move-result-object v30

    #@2e
    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@31
    move-result-object v30

    #@32
    invoke-interface/range {v30 .. v30}, Ljava/lang/CharSequence;->length()I

    #@35
    move-result v20

    #@36
    .line 2938
    .local v20, spanUnionStart:I
    const/16 v19, 0x0

    #@38
    .line 2940
    .local v19, spanUnionEnd:I
    const/4 v8, 0x0

    #@39
    .line 2941
    .local v8, misspelledSpan:Landroid/text/style/SuggestionSpan;
    const/16 v29, 0x0

    #@3b
    .line 2943
    .local v29, underlineColor:I
    const/16 v17, 0x0

    #@3d
    .local v17, spanIndex:I
    :goto_3d
    move/from16 v0, v17

    #@3f
    if-ge v0, v10, :cond_130

    #@41
    .line 2944
    aget-object v26, v27, v17

    #@43
    .line 2945
    .local v26, suggestionSpan:Landroid/text/style/SuggestionSpan;
    move-object/from16 v0, v21

    #@45
    move-object/from16 v1, v26

    #@47
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@4a
    move-result v18

    #@4b
    .line 2946
    .local v18, spanStart:I
    move-object/from16 v0, v21

    #@4d
    move-object/from16 v1, v26

    #@4f
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@52
    move-result v16

    #@53
    .line 2947
    .local v16, spanEnd:I
    move/from16 v0, v18

    #@55
    move/from16 v1, v20

    #@57
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@5a
    move-result v20

    #@5b
    .line 2948
    move/from16 v0, v16

    #@5d
    move/from16 v1, v19

    #@5f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@62
    move-result v19

    #@63
    .line 2950
    invoke-virtual/range {v26 .. v26}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@66
    move-result v30

    #@67
    and-int/lit8 v30, v30, 0x2

    #@69
    if-eqz v30, :cond_6d

    #@6b
    .line 2951
    move-object/from16 v8, v26

    #@6d
    .line 2955
    :cond_6d
    if-nez v17, :cond_73

    #@6f
    invoke-virtual/range {v26 .. v26}, Landroid/text/style/SuggestionSpan;->getUnderlineColor()I

    #@72
    move-result v29

    #@73
    .line 2957
    :cond_73
    invoke-virtual/range {v26 .. v26}, Landroid/text/style/SuggestionSpan;->getSuggestions()[Ljava/lang/String;

    #@76
    move-result-object v28

    #@77
    .line 2958
    .local v28, suggestions:[Ljava/lang/String;
    move-object/from16 v0, v28

    #@79
    array-length v11, v0

    #@7a
    .line 2959
    .local v11, nbSuggestions:I
    const/16 v23, 0x0

    #@7c
    .local v23, suggestionIndex:I
    :goto_7c
    move/from16 v0, v23

    #@7e
    if-ge v0, v11, :cond_124

    #@80
    .line 2960
    aget-object v22, v28, v23

    #@82
    .line 2962
    .local v22, suggestion:Ljava/lang/String;
    const/16 v25, 0x0

    #@84
    .line 2963
    .local v25, suggestionIsDuplicate:Z
    const/4 v6, 0x0

    #@85
    .local v6, i:I
    :goto_85
    move-object/from16 v0, p0

    #@87
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@89
    move/from16 v30, v0

    #@8b
    move/from16 v0, v30

    #@8d
    if-ge v6, v0, :cond_cd

    #@8f
    .line 2964
    move-object/from16 v0, p0

    #@91
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@93
    move-object/from16 v30, v0

    #@95
    aget-object v30, v30, v6

    #@97
    move-object/from16 v0, v30

    #@99
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@9b
    move-object/from16 v30, v0

    #@9d
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v30

    #@a1
    move-object/from16 v0, v30

    #@a3
    move-object/from16 v1, v22

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v30

    #@a9
    if-eqz v30, :cond_128

    #@ab
    .line 2965
    move-object/from16 v0, p0

    #@ad
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@af
    move-object/from16 v30, v0

    #@b1
    aget-object v30, v30, v6

    #@b3
    move-object/from16 v0, v30

    #@b5
    iget-object v15, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@b7
    .line 2966
    .local v15, otherSuggestionSpan:Landroid/text/style/SuggestionSpan;
    move-object/from16 v0, v21

    #@b9
    invoke-interface {v0, v15}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@bc
    move-result v14

    #@bd
    .line 2967
    .local v14, otherSpanStart:I
    move-object/from16 v0, v21

    #@bf
    invoke-interface {v0, v15}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@c2
    move-result v13

    #@c3
    .line 2968
    .local v13, otherSpanEnd:I
    move/from16 v0, v18

    #@c5
    if-ne v0, v14, :cond_128

    #@c7
    move/from16 v0, v16

    #@c9
    if-ne v0, v13, :cond_128

    #@cb
    .line 2969
    const/16 v25, 0x1

    #@cd
    .line 2975
    .end local v13           #otherSpanEnd:I
    .end local v14           #otherSpanStart:I
    .end local v15           #otherSuggestionSpan:Landroid/text/style/SuggestionSpan;
    :cond_cd
    if-nez v25, :cond_12c

    #@cf
    .line 2976
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@d3
    move-object/from16 v30, v0

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@d9
    move/from16 v31, v0

    #@db
    aget-object v24, v30, v31

    #@dd
    .line 2977
    .local v24, suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    move-object/from16 v0, v26

    #@df
    move-object/from16 v1, v24

    #@e1
    iput-object v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@e3
    .line 2978
    move/from16 v0, v23

    #@e5
    move-object/from16 v1, v24

    #@e7
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@e9
    .line 2979
    move-object/from16 v0, v24

    #@eb
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@ed
    move-object/from16 v30, v0

    #@ef
    const/16 v31, 0x0

    #@f1
    move-object/from16 v0, v24

    #@f3
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@f5
    move-object/from16 v32, v0

    #@f7
    invoke-virtual/range {v32 .. v32}, Landroid/text/SpannableStringBuilder;->length()I

    #@fa
    move-result v32

    #@fb
    move-object/from16 v0, v30

    #@fd
    move/from16 v1, v31

    #@ff
    move/from16 v2, v32

    #@101
    move-object/from16 v3, v22

    #@103
    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@106
    .line 2981
    move-object/from16 v0, p0

    #@108
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@10a
    move/from16 v30, v0

    #@10c
    add-int/lit8 v30, v30, 0x1

    #@10e
    move/from16 v0, v30

    #@110
    move-object/from16 v1, p0

    #@112
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@114
    .line 2983
    move-object/from16 v0, p0

    #@116
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@118
    move/from16 v30, v0

    #@11a
    const/16 v31, 0x5

    #@11c
    move/from16 v0, v30

    #@11e
    move/from16 v1, v31

    #@120
    if-ne v0, v1, :cond_12c

    #@122
    .line 2985
    move/from16 v17, v10

    #@124
    .line 2943
    .end local v6           #i:I
    .end local v22           #suggestion:Ljava/lang/String;
    .end local v24           #suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    .end local v25           #suggestionIsDuplicate:Z
    :cond_124
    add-int/lit8 v17, v17, 0x1

    #@126
    goto/16 :goto_3d

    #@128
    .line 2963
    .restart local v6       #i:I
    .restart local v22       #suggestion:Ljava/lang/String;
    .restart local v25       #suggestionIsDuplicate:Z
    :cond_128
    add-int/lit8 v6, v6, 0x1

    #@12a
    goto/16 :goto_85

    #@12c
    .line 2959
    :cond_12c
    add-int/lit8 v23, v23, 0x1

    #@12e
    goto/16 :goto_7c

    #@130
    .line 2992
    .end local v6           #i:I
    .end local v11           #nbSuggestions:I
    .end local v16           #spanEnd:I
    .end local v18           #spanStart:I
    .end local v22           #suggestion:Ljava/lang/String;
    .end local v23           #suggestionIndex:I
    .end local v25           #suggestionIsDuplicate:Z
    .end local v26           #suggestionSpan:Landroid/text/style/SuggestionSpan;
    .end local v28           #suggestions:[Ljava/lang/String;
    :cond_130
    const/4 v6, 0x0

    #@131
    .restart local v6       #i:I
    :goto_131
    move-object/from16 v0, p0

    #@133
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@135
    move/from16 v30, v0

    #@137
    move/from16 v0, v30

    #@139
    if-ge v6, v0, :cond_151

    #@13b
    .line 2993
    move-object/from16 v0, p0

    #@13d
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@13f
    move-object/from16 v30, v0

    #@141
    aget-object v30, v30, v6

    #@143
    move-object/from16 v0, p0

    #@145
    move-object/from16 v1, v30

    #@147
    move/from16 v2, v20

    #@149
    move/from16 v3, v19

    #@14b
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/Editor$SuggestionsPopupWindow;->highlightTextDifferences(Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;II)V

    #@14e
    .line 2992
    add-int/lit8 v6, v6, 0x1

    #@150
    goto :goto_131

    #@151
    .line 2997
    :cond_151
    if-eqz v8, :cond_1ca

    #@153
    .line 2998
    move-object/from16 v0, v21

    #@155
    invoke-interface {v0, v8}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@158
    move-result v9

    #@159
    .line 2999
    .local v9, misspelledStart:I
    move-object/from16 v0, v21

    #@15b
    invoke-interface {v0, v8}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@15e
    move-result v7

    #@15f
    .line 3000
    .local v7, misspelledEnd:I
    if-ltz v9, :cond_1ca

    #@161
    if-le v7, v9, :cond_1ca

    #@163
    .line 3001
    move-object/from16 v0, p0

    #@165
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@167
    move-object/from16 v30, v0

    #@169
    move-object/from16 v0, p0

    #@16b
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@16d
    move/from16 v31, v0

    #@16f
    aget-object v24, v30, v31

    #@171
    .line 3002
    .restart local v24       #suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    move-object/from16 v0, v24

    #@173
    iput-object v8, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@175
    .line 3003
    const/16 v30, -0x1

    #@177
    move/from16 v0, v30

    #@179
    move-object/from16 v1, v24

    #@17b
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@17d
    .line 3004
    move-object/from16 v0, v24

    #@17f
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@181
    move-object/from16 v30, v0

    #@183
    const/16 v31, 0x0

    #@185
    move-object/from16 v0, v24

    #@187
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@189
    move-object/from16 v32, v0

    #@18b
    invoke-virtual/range {v32 .. v32}, Landroid/text/SpannableStringBuilder;->length()I

    #@18e
    move-result v32

    #@18f
    move-object/from16 v0, p0

    #@191
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@193
    move-object/from16 v33, v0

    #@195
    invoke-static/range {v33 .. v33}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@198
    move-result-object v33

    #@199
    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@19c
    move-result-object v33

    #@19d
    const v34, 0x2090129

    #@1a0
    invoke-virtual/range {v33 .. v34}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v33

    #@1a4
    invoke-virtual/range {v30 .. v33}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@1a7
    .line 3009
    move-object/from16 v0, v24

    #@1a9
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@1ab
    move-object/from16 v30, v0

    #@1ad
    move-object/from16 v0, v24

    #@1af
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->highlightSpan:Landroid/text/style/TextAppearanceSpan;

    #@1b1
    move-object/from16 v31, v0

    #@1b3
    const/16 v32, 0x0

    #@1b5
    const/16 v33, 0x0

    #@1b7
    const/16 v34, 0x21

    #@1b9
    invoke-virtual/range {v30 .. v34}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@1bc
    .line 3012
    move-object/from16 v0, p0

    #@1be
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@1c0
    move/from16 v30, v0

    #@1c2
    add-int/lit8 v30, v30, 0x1

    #@1c4
    move/from16 v0, v30

    #@1c6
    move-object/from16 v1, p0

    #@1c8
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@1ca
    .line 3017
    .end local v7           #misspelledEnd:I
    .end local v9           #misspelledStart:I
    .end local v24           #suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    :cond_1ca
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@1ce
    move-object/from16 v30, v0

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@1d4
    move/from16 v31, v0

    #@1d6
    aget-object v24, v30, v31

    #@1d8
    .line 3018
    .restart local v24       #suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    const/16 v30, 0x0

    #@1da
    move-object/from16 v0, v30

    #@1dc
    move-object/from16 v1, v24

    #@1de
    iput-object v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@1e0
    .line 3019
    const/16 v30, -0x2

    #@1e2
    move/from16 v0, v30

    #@1e4
    move-object/from16 v1, v24

    #@1e6
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@1e8
    .line 3020
    move-object/from16 v0, v24

    #@1ea
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@1ec
    move-object/from16 v30, v0

    #@1ee
    const/16 v31, 0x0

    #@1f0
    move-object/from16 v0, v24

    #@1f2
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@1f4
    move-object/from16 v32, v0

    #@1f6
    invoke-virtual/range {v32 .. v32}, Landroid/text/SpannableStringBuilder;->length()I

    #@1f9
    move-result v32

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@1fe
    move-object/from16 v33, v0

    #@200
    invoke-static/range {v33 .. v33}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@203
    move-result-object v33

    #@204
    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@207
    move-result-object v33

    #@208
    const v34, 0x10403ea

    #@20b
    invoke-virtual/range {v33 .. v34}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@20e
    move-result-object v33

    #@20f
    invoke-virtual/range {v30 .. v33}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@212
    .line 3025
    move-object/from16 v0, v24

    #@214
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@216
    move-object/from16 v30, v0

    #@218
    move-object/from16 v0, v24

    #@21a
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->highlightSpan:Landroid/text/style/TextAppearanceSpan;

    #@21c
    move-object/from16 v31, v0

    #@21e
    const/16 v32, 0x0

    #@220
    const/16 v33, 0x0

    #@222
    const/16 v34, 0x21

    #@224
    invoke-virtual/range {v30 .. v34}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@227
    .line 3027
    move-object/from16 v0, p0

    #@229
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@22b
    move/from16 v30, v0

    #@22d
    add-int/lit8 v30, v30, 0x1

    #@22f
    move/from16 v0, v30

    #@231
    move-object/from16 v1, p0

    #@233
    iput v0, v1, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@235
    .line 3029
    move-object/from16 v0, p0

    #@237
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@239
    move-object/from16 v30, v0

    #@23b
    move-object/from16 v0, v30

    #@23d
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@23f
    move-object/from16 v30, v0

    #@241
    if-nez v30, :cond_254

    #@243
    move-object/from16 v0, p0

    #@245
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@247
    move-object/from16 v30, v0

    #@249
    new-instance v31, Landroid/text/style/SuggestionRangeSpan;

    #@24b
    invoke-direct/range {v31 .. v31}, Landroid/text/style/SuggestionRangeSpan;-><init>()V

    #@24e
    move-object/from16 v0, v31

    #@250
    move-object/from16 v1, v30

    #@252
    iput-object v0, v1, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@254
    .line 3030
    :cond_254
    if-nez v29, :cond_29d

    #@256
    .line 3032
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@25a
    move-object/from16 v30, v0

    #@25c
    move-object/from16 v0, v30

    #@25e
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@260
    move-object/from16 v30, v0

    #@262
    move-object/from16 v0, p0

    #@264
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@266
    move-object/from16 v31, v0

    #@268
    invoke-static/range {v31 .. v31}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@26b
    move-result-object v31

    #@26c
    move-object/from16 v0, v31

    #@26e
    iget v0, v0, Landroid/widget/TextView;->mHighlightColor:I

    #@270
    move/from16 v31, v0

    #@272
    invoke-virtual/range {v30 .. v31}, Landroid/text/style/SuggestionRangeSpan;->setBackgroundColor(I)V

    #@275
    .line 3039
    :goto_275
    move-object/from16 v0, p0

    #@277
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@279
    move-object/from16 v30, v0

    #@27b
    move-object/from16 v0, v30

    #@27d
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@27f
    move-object/from16 v30, v0

    #@281
    const/16 v31, 0x21

    #@283
    move-object/from16 v0, v21

    #@285
    move-object/from16 v1, v30

    #@287
    move/from16 v2, v20

    #@289
    move/from16 v3, v19

    #@28b
    move/from16 v4, v31

    #@28d
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@290
    .line 3042
    move-object/from16 v0, p0

    #@292
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionsAdapter:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

    #@294
    move-object/from16 v30, v0

    #@296
    invoke-virtual/range {v30 .. v30}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;->notifyDataSetChanged()V

    #@299
    .line 3043
    const/16 v30, 0x1

    #@29b
    goto/16 :goto_1b

    #@29d
    .line 3034
    :cond_29d
    const v5, 0x3ecccccd

    #@2a0
    .line 3035
    .local v5, BACKGROUND_TRANSPARENCY:F
    invoke-static/range {v29 .. v29}, Landroid/graphics/Color;->alpha(I)I

    #@2a3
    move-result v30

    #@2a4
    move/from16 v0, v30

    #@2a6
    int-to-float v0, v0

    #@2a7
    move/from16 v30, v0

    #@2a9
    const v31, 0x3ecccccd

    #@2ac
    mul-float v30, v30, v31

    #@2ae
    move/from16 v0, v30

    #@2b0
    float-to-int v12, v0

    #@2b1
    .line 3036
    .local v12, newAlpha:I
    move-object/from16 v0, p0

    #@2b3
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2b5
    move-object/from16 v30, v0

    #@2b7
    move-object/from16 v0, v30

    #@2b9
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@2bb
    move-object/from16 v30, v0

    #@2bd
    const v31, 0xffffff

    #@2c0
    and-int v31, v31, v29

    #@2c2
    shl-int/lit8 v32, v12, 0x18

    #@2c4
    add-int v31, v31, v32

    #@2c6
    invoke-virtual/range {v30 .. v31}, Landroid/text/style/SuggestionRangeSpan;->setBackgroundColor(I)V

    #@2c9
    goto :goto_275
.end method


# virtual methods
.method protected clipVertically(I)I
    .registers 5
    .parameter "positionY"

    #@0
    .prologue
    .line 2918
    iget-object v2, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@2
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@5
    move-result v1

    #@6
    .line 2919
    .local v1, height:I
    iget-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@8
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@13
    move-result-object v0

    #@14
    .line 2920
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@16
    sub-int/2addr v2, v1

    #@17
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    #@1a
    move-result v2

    #@1b
    return v2
.end method

.method protected createPopupWindow()V
    .registers 4

    #@0
    .prologue
    .line 2738
    new-instance v0, Landroid/widget/Editor$SuggestionsPopupWindow$CustomPopupWindow;

    #@2
    iget-object v1, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    const v2, 0x1010373

    #@f
    invoke-direct {v0, p0, v1, v2}, Landroid/widget/Editor$SuggestionsPopupWindow$CustomPopupWindow;-><init>(Landroid/widget/Editor$SuggestionsPopupWindow;Landroid/content/Context;I)V

    #@12
    iput-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@14
    .line 2740
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@16
    const/4 v1, 0x2

    #@17
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@1a
    .line 2741
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@1c
    const/4 v1, 0x1

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    #@20
    .line 2742
    iget-object v0, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@22
    const/4 v1, 0x0

    #@23
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    #@26
    .line 2743
    return-void
.end method

.method protected getTextOffset()I
    .registers 2

    #@0
    .prologue
    .line 2908
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected getVerticalLocalPosition(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 2913
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getLineBottom(I)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public hide()V
    .registers 1

    #@0
    .prologue
    .line 2925
    invoke-super {p0}, Landroid/widget/Editor$PinnedPopupWindow;->hide()V

    #@3
    .line 2926
    return-void
.end method

.method protected initContentView()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2747
    new-instance v1, Landroid/widget/ListView;

    #@3
    iget-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@5
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@c
    move-result-object v2

    #@d
    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    #@10
    .line 2748
    .local v1, listView:Landroid/widget/ListView;
    new-instance v2, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

    #@12
    invoke-direct {v2, p0, v4}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;-><init>(Landroid/widget/Editor$SuggestionsPopupWindow;Landroid/widget/Editor$1;)V

    #@15
    iput-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionsAdapter:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

    #@17
    .line 2749
    iget-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionsAdapter:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

    #@19
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@1c
    .line 2750
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@1f
    .line 2751
    iput-object v1, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@21
    .line 2754
    const/4 v2, 0x7

    #@22
    new-array v2, v2, [Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@24
    iput-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@26
    .line 2755
    const/4 v0, 0x0

    #@27
    .local v0, i:I
    :goto_27
    iget-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@29
    array-length v2, v2

    #@2a
    if-ge v0, v2, :cond_38

    #@2c
    .line 2756
    iget-object v2, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@2e
    new-instance v3, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@30
    invoke-direct {v3, p0, v4}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;-><init>(Landroid/widget/Editor$SuggestionsPopupWindow;Landroid/widget/Editor$1;)V

    #@33
    aput-object v3, v2, v0

    #@35
    .line 2755
    add-int/lit8 v0, v0, 0x1

    #@37
    goto :goto_27

    #@38
    .line 2758
    :cond_38
    return-void
.end method

.method public isShowingUp()Z
    .registers 2

    #@0
    .prologue
    .line 2761
    iget-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mIsShowingUp:Z

    #@2
    return v0
.end method

.method protected measureContent()V
    .registers 10

    #@0
    .prologue
    const/high16 v8, -0x8000

    #@2
    .line 2877
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v7}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v7

    #@8
    invoke-virtual {v7}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v7

    #@c
    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@f
    move-result-object v0

    #@10
    .line 2878
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget v7, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@12
    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@15
    move-result v1

    #@16
    .line 2880
    .local v1, horizontalMeasure:I
    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@18
    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1b
    move-result v4

    #@1c
    .line 2883
    .local v4, verticalMeasure:I
    const/4 v6, 0x0

    #@1d
    .line 2884
    .local v6, width:I
    const/4 v5, 0x0

    #@1e
    .line 2885
    .local v5, view:Landroid/view/View;
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    iget v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mNumberOfSuggestions:I

    #@21
    if-ge v2, v7, :cond_40

    #@23
    .line 2886
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionsAdapter:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;

    #@25
    iget-object v8, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@27
    invoke-virtual {v7, v2, v5, v8}, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@2a
    move-result-object v5

    #@2b
    .line 2887
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2e
    move-result-object v7

    #@2f
    const/4 v8, -0x2

    #@30
    iput v8, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@32
    .line 2888
    invoke-virtual {v5, v1, v4}, Landroid/view/View;->measure(II)V

    #@35
    .line 2889
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    #@38
    move-result v7

    #@39
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@3c
    move-result v6

    #@3d
    .line 2885
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_1f

    #@40
    .line 2893
    :cond_40
    iget-object v7, p0, Landroid/widget/Editor$PinnedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@42
    const/high16 v8, 0x4000

    #@44
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@47
    move-result v8

    #@48
    invoke-virtual {v7, v8, v4}, Landroid/view/ViewGroup;->measure(II)V

    #@4b
    .line 2897
    iget-object v7, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@4d
    invoke-virtual {v7}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    #@50
    move-result-object v3

    #@51
    .line 2898
    .local v3, popupBackground:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_80

    #@53
    .line 2899
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@55
    invoke-static {v7}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@58
    move-result-object v7

    #@59
    if-nez v7, :cond_65

    #@5b
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@5d
    new-instance v8, Landroid/graphics/Rect;

    #@5f
    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    #@62
    invoke-static {v7, v8}, Landroid/widget/Editor;->access$2202(Landroid/widget/Editor;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@65
    .line 2900
    :cond_65
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@67
    invoke-static {v7}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@6a
    move-result-object v7

    #@6b
    invoke-virtual {v3, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@6e
    .line 2901
    iget-object v7, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@70
    invoke-static {v7}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@73
    move-result-object v7

    #@74
    iget v7, v7, Landroid/graphics/Rect;->left:I

    #@76
    iget-object v8, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@78
    invoke-static {v8}, Landroid/widget/Editor;->access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;

    #@7b
    move-result-object v8

    #@7c
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@7e
    add-int/2addr v7, v8

    #@7f
    add-int/2addr v6, v7

    #@80
    .line 2903
    :cond_80
    iget-object v7, p0, Landroid/widget/Editor$PinnedPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@82
    invoke-virtual {v7, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@85
    .line 2904
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 36
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3068
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@4
    move-object/from16 v25, v0

    #@6
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v25

    #@a
    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Landroid/text/Editable;

    #@10
    .line 3069
    .local v2, editable:Landroid/text/Editable;
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->mSuggestionInfos:[Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;

    #@14
    move-object/from16 v25, v0

    #@16
    aget-object v16, v25, p3

    #@18
    .line 3071
    .local v16, suggestionInfo:Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;
    move-object/from16 v0, v16

    #@1a
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@1c
    move/from16 v25, v0

    #@1e
    const/16 v26, -0x2

    #@20
    move/from16 v0, v25

    #@22
    move/from16 v1, v26

    #@24
    if-ne v0, v1, :cond_85

    #@26
    .line 3072
    move-object/from16 v0, p0

    #@28
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2a
    move-object/from16 v25, v0

    #@2c
    move-object/from16 v0, v25

    #@2e
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@30
    move-object/from16 v25, v0

    #@32
    move-object/from16 v0, v25

    #@34
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@37
    move-result v13

    #@38
    .line 3073
    .local v13, spanUnionStart:I
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@3c
    move-object/from16 v25, v0

    #@3e
    move-object/from16 v0, v25

    #@40
    iget-object v0, v0, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@42
    move-object/from16 v25, v0

    #@44
    move-object/from16 v0, v25

    #@46
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@49
    move-result v12

    #@4a
    .line 3074
    .local v12, spanUnionEnd:I
    if-ltz v13, :cond_81

    #@4c
    if-le v12, v13, :cond_81

    #@4e
    .line 3076
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@51
    move-result v25

    #@52
    move/from16 v0, v25

    #@54
    if-ge v12, v0, :cond_72

    #@56
    invoke-interface {v2, v12}, Landroid/text/Editable;->charAt(I)C

    #@59
    move-result v25

    #@5a
    invoke-static/range {v25 .. v25}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@5d
    move-result v25

    #@5e
    if-eqz v25, :cond_72

    #@60
    if-eqz v13, :cond_70

    #@62
    add-int/lit8 v25, v13, -0x1

    #@64
    move/from16 v0, v25

    #@66
    invoke-interface {v2, v0}, Landroid/text/Editable;->charAt(I)C

    #@69
    move-result v25

    #@6a
    invoke-static/range {v25 .. v25}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@6d
    move-result v25

    #@6e
    if-eqz v25, :cond_72

    #@70
    .line 3080
    :cond_70
    add-int/lit8 v12, v12, 0x1

    #@72
    .line 3082
    :cond_72
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@76
    move-object/from16 v25, v0

    #@78
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7b
    move-result-object v25

    #@7c
    move-object/from16 v0, v25

    #@7e
    invoke-virtual {v0, v13, v12}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@81
    .line 3084
    :cond_81
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor$SuggestionsPopupWindow;->hide()V

    #@84
    .line 3184
    .end local v12           #spanUnionEnd:I
    .end local v13           #spanUnionStart:I
    :goto_84
    return-void

    #@85
    .line 3088
    :cond_85
    move-object/from16 v0, v16

    #@87
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@89
    move-object/from16 v25, v0

    #@8b
    move-object/from16 v0, v25

    #@8d
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@90
    move-result v11

    #@91
    .line 3089
    .local v11, spanStart:I
    move-object/from16 v0, v16

    #@93
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@95
    move-object/from16 v25, v0

    #@97
    move-object/from16 v0, v25

    #@99
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@9c
    move-result v10

    #@9d
    .line 3090
    .local v10, spanEnd:I
    if-ltz v11, :cond_a1

    #@9f
    if-gt v10, v11, :cond_a5

    #@a1
    .line 3092
    :cond_a1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor$SuggestionsPopupWindow;->hide()V

    #@a4
    goto :goto_84

    #@a5
    .line 3096
    :cond_a5
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a8
    move-result-object v25

    #@a9
    move-object/from16 v0, v25

    #@ab
    invoke-virtual {v0, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ae
    move-result-object v9

    #@af
    .line 3098
    .local v9, originalText:Ljava/lang/String;
    move-object/from16 v0, v16

    #@b1
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@b3
    move/from16 v25, v0

    #@b5
    const/16 v26, -0x1

    #@b7
    move/from16 v0, v25

    #@b9
    move/from16 v1, v26

    #@bb
    if-ne v0, v1, :cond_160

    #@bd
    .line 3099
    new-instance v5, Landroid/content/Intent;

    #@bf
    const-string v25, "com.android.settings.USER_DICTIONARY_INSERT"

    #@c1
    move-object/from16 v0, v25

    #@c3
    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c6
    .line 3100
    .local v5, intent:Landroid/content/Intent;
    const-string/jumbo v25, "word"

    #@c9
    move-object/from16 v0, v25

    #@cb
    invoke-virtual {v5, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ce
    .line 3101
    const-string/jumbo v25, "locale"

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@d5
    move-object/from16 v26, v0

    #@d7
    invoke-static/range {v26 .. v26}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@da
    move-result-object v26

    #@db
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getTextServicesLocale()Ljava/util/Locale;

    #@de
    move-result-object v26

    #@df
    invoke-virtual/range {v26 .. v26}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@e2
    move-result-object v26

    #@e3
    move-object/from16 v0, v25

    #@e5
    move-object/from16 v1, v26

    #@e7
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ea
    .line 3104
    move-object/from16 v0, p0

    #@ec
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@ee
    move-object/from16 v25, v0

    #@f0
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$2300(Landroid/widget/Editor;)Landroid/widget/Editor$UserDictionaryListener;

    #@f3
    move-result-object v25

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@f8
    move-object/from16 v26, v0

    #@fa
    invoke-static/range {v26 .. v26}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@fd
    move-result-object v26

    #@fe
    move-object/from16 v0, v25

    #@100
    move-object/from16 v1, v26

    #@102
    invoke-virtual {v0, v1, v9, v11, v10}, Landroid/widget/Editor$UserDictionaryListener;->waitForUserDictionaryAdded(Landroid/widget/TextView;Ljava/lang/String;II)V

    #@105
    .line 3106
    const-string/jumbo v25, "listener"

    #@108
    new-instance v26, Landroid/os/Messenger;

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@10e
    move-object/from16 v27, v0

    #@110
    invoke-static/range {v27 .. v27}, Landroid/widget/Editor;->access$2300(Landroid/widget/Editor;)Landroid/widget/Editor$UserDictionaryListener;

    #@113
    move-result-object v27

    #@114
    invoke-direct/range {v26 .. v27}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@117
    move-object/from16 v0, v25

    #@119
    move-object/from16 v1, v26

    #@11b
    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@11e
    .line 3107
    invoke-virtual {v5}, Landroid/content/Intent;->getFlags()I

    #@121
    move-result v25

    #@122
    const/high16 v26, 0x1000

    #@124
    or-int v25, v25, v26

    #@126
    move/from16 v0, v25

    #@128
    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@12b
    .line 3108
    move-object/from16 v0, p0

    #@12d
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@12f
    move-object/from16 v25, v0

    #@131
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@134
    move-result-object v25

    #@135
    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@138
    move-result-object v25

    #@139
    move-object/from16 v0, v25

    #@13b
    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@13e
    .line 3111
    move-object/from16 v0, v16

    #@140
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@142
    move-object/from16 v25, v0

    #@144
    move-object/from16 v0, v25

    #@146
    invoke-interface {v2, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@149
    .line 3112
    invoke-static {v2, v10}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@14c
    .line 3113
    move-object/from16 v0, p0

    #@14e
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@150
    move-object/from16 v25, v0

    #@152
    const/16 v26, 0x0

    #@154
    move-object/from16 v0, v25

    #@156
    move/from16 v1, v26

    #@158
    invoke-static {v0, v11, v10, v1}, Landroid/widget/Editor;->access$2400(Landroid/widget/Editor;IIZ)V

    #@15b
    .line 3183
    .end local v5           #intent:Landroid/content/Intent;
    :goto_15b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor$SuggestionsPopupWindow;->hide()V

    #@15e
    goto/16 :goto_84

    #@160
    .line 3116
    :cond_160
    const-class v25, Landroid/text/style/SuggestionSpan;

    #@162
    move-object/from16 v0, v25

    #@164
    invoke-interface {v2, v11, v10, v0}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@167
    move-result-object v19

    #@168
    check-cast v19, [Landroid/text/style/SuggestionSpan;

    #@16a
    .line 3118
    .local v19, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    move-object/from16 v0, v19

    #@16c
    array-length v6, v0

    #@16d
    .line 3119
    .local v6, length:I
    new-array v0, v6, [I

    #@16f
    move-object/from16 v22, v0

    #@171
    .line 3120
    .local v22, suggestionSpansStarts:[I
    new-array v0, v6, [I

    #@173
    move-object/from16 v20, v0

    #@175
    .line 3121
    .local v20, suggestionSpansEnds:[I
    new-array v0, v6, [I

    #@177
    move-object/from16 v21, v0

    #@179
    .line 3122
    .local v21, suggestionSpansFlags:[I
    const/4 v3, 0x0

    #@17a
    .local v3, i:I
    :goto_17a
    if-ge v3, v6, :cond_1a8

    #@17c
    .line 3123
    aget-object v17, v19, v3

    #@17e
    .line 3124
    .local v17, suggestionSpan:Landroid/text/style/SuggestionSpan;
    move-object/from16 v0, v17

    #@180
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@183
    move-result v25

    #@184
    aput v25, v22, v3

    #@186
    .line 3125
    move-object/from16 v0, v17

    #@188
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@18b
    move-result v25

    #@18c
    aput v25, v20, v3

    #@18e
    .line 3126
    move-object/from16 v0, v17

    #@190
    invoke-interface {v2, v0}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    #@193
    move-result v25

    #@194
    aput v25, v21, v3

    #@196
    .line 3129
    invoke-virtual/range {v17 .. v17}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@199
    move-result v18

    #@19a
    .line 3130
    .local v18, suggestionSpanFlags:I
    and-int/lit8 v25, v18, 0x2

    #@19c
    if-lez v25, :cond_1a5

    #@19e
    .line 3131
    and-int/lit8 v18, v18, -0x3

    #@1a0
    .line 3132
    and-int/lit8 v18, v18, -0x2

    #@1a2
    .line 3133
    invoke-virtual/range {v17 .. v18}, Landroid/text/style/SuggestionSpan;->setFlags(I)V

    #@1a5
    .line 3122
    :cond_1a5
    add-int/lit8 v3, v3, 0x1

    #@1a7
    goto :goto_17a

    #@1a8
    .line 3137
    .end local v17           #suggestionSpan:Landroid/text/style/SuggestionSpan;
    .end local v18           #suggestionSpanFlags:I
    :cond_1a8
    move-object/from16 v0, v16

    #@1aa
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionStart:I

    #@1ac
    move/from16 v23, v0

    #@1ae
    .line 3138
    .local v23, suggestionStart:I
    move-object/from16 v0, v16

    #@1b0
    iget v15, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionEnd:I

    #@1b2
    .line 3139
    .local v15, suggestionEnd:I
    move-object/from16 v0, v16

    #@1b4
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->text:Landroid/text/SpannableStringBuilder;

    #@1b6
    move-object/from16 v25, v0

    #@1b8
    move-object/from16 v0, v25

    #@1ba
    move/from16 v1, v23

    #@1bc
    invoke-virtual {v0, v1, v15}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    #@1bf
    move-result-object v25

    #@1c0
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1c3
    move-result-object v14

    #@1c4
    .line 3141
    .local v14, suggestion:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1c6
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@1c8
    move-object/from16 v25, v0

    #@1ca
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1cd
    move-result-object v25

    #@1ce
    move-object/from16 v0, v25

    #@1d0
    invoke-virtual {v0, v11, v10, v14}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    #@1d3
    .line 3144
    move-object/from16 v0, v16

    #@1d5
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@1d7
    move-object/from16 v25, v0

    #@1d9
    invoke-virtual/range {v25 .. v25}, Landroid/text/style/SuggestionSpan;->getNotificationTargetClassName()Ljava/lang/String;

    #@1dc
    move-result-object v25

    #@1dd
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1e0
    move-result v25

    #@1e1
    if-nez v25, :cond_1fc

    #@1e3
    .line 3146
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@1e6
    move-result-object v4

    #@1e7
    .line 3147
    .local v4, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v4, :cond_1fc

    #@1e9
    .line 3148
    move-object/from16 v0, v16

    #@1eb
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@1ed
    move-object/from16 v25, v0

    #@1ef
    move-object/from16 v0, v16

    #@1f1
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@1f3
    move/from16 v26, v0

    #@1f5
    move-object/from16 v0, v25

    #@1f7
    move/from16 v1, v26

    #@1f9
    invoke-virtual {v4, v0, v9, v1}, Landroid/view/inputmethod/InputMethodManager;->notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)V

    #@1fc
    .line 3154
    .end local v4           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_1fc
    move-object/from16 v0, v16

    #@1fe
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionSpan:Landroid/text/style/SuggestionSpan;

    #@200
    move-object/from16 v25, v0

    #@202
    invoke-virtual/range {v25 .. v25}, Landroid/text/style/SuggestionSpan;->getSuggestions()[Ljava/lang/String;

    #@205
    move-result-object v24

    #@206
    .line 3155
    .local v24, suggestions:[Ljava/lang/String;
    move-object/from16 v0, v16

    #@208
    iget v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow$SuggestionInfo;->suggestionIndex:I

    #@20a
    move/from16 v25, v0

    #@20c
    aput-object v9, v24, v25

    #@20e
    .line 3158
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    #@211
    move-result v25

    #@212
    sub-int v26, v10, v11

    #@214
    sub-int v7, v25, v26

    #@216
    .line 3159
    .local v7, lengthDifference:I
    const/4 v3, 0x0

    #@217
    :goto_217
    if-ge v3, v6, :cond_292

    #@219
    .line 3161
    move-object/from16 v0, p0

    #@21b
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@21d
    move-object/from16 v25, v0

    #@21f
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@222
    move-result-object v25

    #@223
    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->length()I

    #@226
    move-result v25

    #@227
    aget v26, v20, v3

    #@229
    add-int v26, v26, v7

    #@22b
    move/from16 v0, v25

    #@22d
    move/from16 v1, v26

    #@22f
    if-ge v0, v1, :cond_26c

    #@231
    .line 3163
    const-string v25, "TextView"

    #@233
    new-instance v26, Ljava/lang/StringBuilder;

    #@235
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@238
    const-string v27, "[hyk] onItemClick() mTextView.length() = "

    #@23a
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23d
    move-result-object v26

    #@23e
    move-object/from16 v0, p0

    #@240
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@242
    move-object/from16 v27, v0

    #@244
    invoke-static/range {v27 .. v27}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@247
    move-result-object v27

    #@248
    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->length()I

    #@24b
    move-result v27

    #@24c
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24f
    move-result-object v26

    #@250
    const-string/jumbo v27, "suggestionSpansEnds[i] + lengthDifference = "

    #@253
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@256
    move-result-object v26

    #@257
    aget v27, v20, v3

    #@259
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v26

    #@25d
    move-object/from16 v0, v26

    #@25f
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@262
    move-result-object v26

    #@263
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@266
    move-result-object v26

    #@267
    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26a
    goto/16 :goto_84

    #@26c
    .line 3171
    :cond_26c
    aget v25, v22, v3

    #@26e
    move/from16 v0, v25

    #@270
    if-gt v0, v11, :cond_28f

    #@272
    aget v25, v20, v3

    #@274
    move/from16 v0, v25

    #@276
    if-lt v0, v10, :cond_28f

    #@278
    .line 3173
    move-object/from16 v0, p0

    #@27a
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@27c
    move-object/from16 v25, v0

    #@27e
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@281
    move-result-object v25

    #@282
    aget-object v26, v19, v3

    #@284
    aget v27, v22, v3

    #@286
    aget v28, v20, v3

    #@288
    add-int v28, v28, v7

    #@28a
    aget v29, v21, v3

    #@28c
    invoke-virtual/range {v25 .. v29}, Landroid/widget/TextView;->setSpan_internal(Ljava/lang/Object;III)V

    #@28f
    .line 3159
    :cond_28f
    add-int/lit8 v3, v3, 0x1

    #@291
    goto :goto_217

    #@292
    .line 3179
    :cond_292
    add-int v8, v10, v7

    #@294
    .line 3180
    .local v8, newCursorPosition:I
    move-object/from16 v0, p0

    #@296
    iget-object v0, v0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@298
    move-object/from16 v25, v0

    #@29a
    invoke-static/range {v25 .. v25}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@29d
    move-result-object v25

    #@29e
    move-object/from16 v0, v25

    #@2a0
    invoke-virtual {v0, v8, v8}, Landroid/widget/TextView;->setCursorPosition_internal(II)V

    #@2a3
    goto/16 :goto_15b
.end method

.method public onParentLostFocus()V
    .registers 2

    #@0
    .prologue
    .line 2765
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mIsShowingUp:Z

    #@3
    .line 2766
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 2865
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    instance-of v0, v0, Landroid/text/Editable;

    #@c
    if-nez v0, :cond_f

    #@e
    .line 2873
    :cond_e
    :goto_e
    return-void

    #@f
    .line 2867
    :cond_f
    invoke-direct {p0}, Landroid/widget/Editor$SuggestionsPopupWindow;->updateSuggestions()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_e

    #@15
    .line 2868
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@17
    iget-boolean v0, v0, Landroid/widget/Editor;->mCursorVisible:Z

    #@19
    iput-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mCursorWasVisibleBeforeSuggestions:Z

    #@1b
    .line 2869
    iget-object v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->this$0:Landroid/widget/Editor;

    #@1d
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@20
    move-result-object v0

    #@21
    const/4 v1, 0x0

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCursorVisible(Z)V

    #@25
    .line 2870
    const/4 v0, 0x1

    #@26
    iput-boolean v0, p0, Landroid/widget/Editor$SuggestionsPopupWindow;->mIsShowingUp:Z

    #@28
    .line 2871
    invoke-super {p0}, Landroid/widget/Editor$PinnedPopupWindow;->show()V

    #@2b
    goto :goto_e
.end method
