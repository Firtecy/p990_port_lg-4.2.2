.class Landroid/widget/AbsListView$ListItemAccessibilityDelegate;
.super Landroid/view/View$AccessibilityDelegate;
.source "AbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListItemAccessibilityDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2225
    iput-object p1, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 8
    .parameter "host"
    .parameter "info"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 2228
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@4
    .line 2230
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@6
    invoke-virtual {v3, p1}, Landroid/widget/AbsListView;->getPositionForView(Landroid/view/View;)I

    #@9
    move-result v2

    #@a
    .line 2231
    .local v2, position:I
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@c
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/widget/ListAdapter;

    #@12
    .line 2233
    .local v0, adapter:Landroid/widget/ListAdapter;
    const/4 v3, -0x1

    #@13
    if-eq v2, v3, :cond_17

    #@15
    if-nez v0, :cond_18

    #@17
    .line 2262
    :cond_17
    :goto_17
    return-void

    #@18
    .line 2238
    :cond_18
    :try_start_18
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@1a
    invoke-virtual {v3}, Landroid/widget/AbsListView;->isEnabled()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_17

    #@20
    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->isEnabled(I)Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_23} :catch_57

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_17

    #@26
    .line 2245
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@28
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@2b
    move-result v3

    #@2c
    if-ne v2, v3, :cond_59

    #@2e
    .line 2246
    invoke-virtual {p2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    #@31
    .line 2247
    const/16 v3, 0x8

    #@33
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@36
    .line 2252
    :goto_36
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@38
    invoke-virtual {v3}, Landroid/widget/AbsListView;->isClickable()Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_46

    #@3e
    .line 2253
    const/16 v3, 0x10

    #@40
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@43
    .line 2254
    invoke-virtual {p2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    #@46
    .line 2257
    :cond_46
    iget-object v3, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@48
    invoke-virtual {v3}, Landroid/widget/AbsListView;->isLongClickable()Z

    #@4b
    move-result v3

    #@4c
    if-eqz v3, :cond_17

    #@4e
    .line 2258
    const/16 v3, 0x20

    #@50
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@53
    .line 2259
    invoke-virtual {p2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    #@56
    goto :goto_17

    #@57
    .line 2241
    :catch_57
    move-exception v1

    #@58
    .line 2242
    .local v1, e:Ljava/lang/Exception;
    goto :goto_17

    #@59
    .line 2249
    .end local v1           #e:Ljava/lang/Exception;
    :cond_59
    const/4 v3, 0x4

    #@5a
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@5d
    goto :goto_36
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 12
    .parameter "host"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 2266
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    #@6
    move-result v6

    #@7
    if-eqz v6, :cond_a

    #@9
    .line 2310
    :goto_9
    return v4

    #@a
    .line 2270
    :cond_a
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@c
    invoke-virtual {v6, p1}, Landroid/widget/AbsListView;->getPositionForView(Landroid/view/View;)I

    #@f
    move-result v3

    #@10
    .line 2271
    .local v3, position:I
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@12
    invoke-virtual {v6}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/widget/ListAdapter;

    #@18
    .line 2273
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-eq v3, v7, :cond_1c

    #@1a
    if-nez v0, :cond_1e

    #@1c
    :cond_1c
    move v4, v5

    #@1d
    .line 2275
    goto :goto_9

    #@1e
    .line 2278
    :cond_1e
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@20
    invoke-virtual {v6}, Landroid/widget/AbsListView;->isEnabled()Z

    #@23
    move-result v6

    #@24
    if-eqz v6, :cond_2c

    #@26
    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@29
    move-result v6

    #@2a
    if-nez v6, :cond_2e

    #@2c
    :cond_2c
    move v4, v5

    #@2d
    .line 2280
    goto :goto_9

    #@2e
    .line 2283
    :cond_2e
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@30
    invoke-virtual {v6, v3}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    #@33
    move-result-wide v1

    #@34
    .line 2285
    .local v1, id:J
    sparse-switch p2, :sswitch_data_7c

    #@37
    move v4, v5

    #@38
    .line 2310
    goto :goto_9

    #@39
    .line 2287
    :sswitch_39
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@3b
    invoke-virtual {v6}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@3e
    move-result v6

    #@3f
    if-ne v6, v3, :cond_47

    #@41
    .line 2288
    iget-object v5, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@43
    invoke-virtual {v5, v7}, Landroid/widget/AbsListView;->setSelection(I)V

    #@46
    goto :goto_9

    #@47
    :cond_47
    move v4, v5

    #@48
    .line 2291
    goto :goto_9

    #@49
    .line 2293
    :sswitch_49
    iget-object v6, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@4b
    invoke-virtual {v6}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@4e
    move-result v6

    #@4f
    if-eq v6, v3, :cond_57

    #@51
    .line 2294
    iget-object v5, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@53
    invoke-virtual {v5, v3}, Landroid/widget/AbsListView;->setSelection(I)V

    #@56
    goto :goto_9

    #@57
    :cond_57
    move v4, v5

    #@58
    .line 2297
    goto :goto_9

    #@59
    .line 2299
    :sswitch_59
    iget-object v4, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@5b
    invoke-virtual {v4}, Landroid/widget/AbsListView;->isClickable()Z

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_68

    #@61
    .line 2300
    iget-object v4, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@63
    invoke-virtual {v4, p1, v3, v1, v2}, Landroid/widget/AbsListView;->performItemClick(Landroid/view/View;IJ)Z

    #@66
    move-result v4

    #@67
    goto :goto_9

    #@68
    :cond_68
    move v4, v5

    #@69
    .line 2302
    goto :goto_9

    #@6a
    .line 2304
    :sswitch_6a
    iget-object v4, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@6c
    invoke-virtual {v4}, Landroid/widget/AbsListView;->isLongClickable()Z

    #@6f
    move-result v4

    #@70
    if-eqz v4, :cond_79

    #@72
    .line 2305
    iget-object v4, p0, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;->this$0:Landroid/widget/AbsListView;

    #@74
    invoke-virtual {v4, p1, v3, v1, v2}, Landroid/widget/AbsListView;->performLongPress(Landroid/view/View;IJ)Z

    #@77
    move-result v4

    #@78
    goto :goto_9

    #@79
    :cond_79
    move v4, v5

    #@7a
    .line 2307
    goto :goto_9

    #@7b
    .line 2285
    nop

    #@7c
    :sswitch_data_7c
    .sparse-switch
        0x4 -> :sswitch_49
        0x8 -> :sswitch_39
        0x10 -> :sswitch_59
        0x20 -> :sswitch_6a
    .end sparse-switch
.end method
