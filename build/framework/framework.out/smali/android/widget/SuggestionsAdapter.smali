.class Landroid/widget/SuggestionsAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "SuggestionsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SuggestionsAdapter$ChildViewCache;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DELETE_KEY_POST_DELAY:J = 0x1f4L

.field static final INVALID_INDEX:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = "SuggestionsAdapter"

.field private static final QUERY_LIMIT:I = 0x32

.field static final REFINE_ALL:I = 0x2

.field static final REFINE_BY_ENTRY:I = 0x1

.field static final REFINE_NONE:I


# instance fields
.field private mClosed:Z

.field private mFlagsCol:I

.field private mIconName1Col:I

.field private mIconName2Col:I

.field private mOutsideDrawablesCache:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field

.field private mProviderContext:Landroid/content/Context;

.field private mQueryRefinement:I

.field private mSearchManager:Landroid/app/SearchManager;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSearchable:Landroid/app/SearchableInfo;

.field private mStringInputedConstraint:Ljava/lang/String;

.field private mText1Col:I

.field private mText2Col:I

.field private mText2UrlCol:I

.field private mUrlColor:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V
    .registers 10
    .parameter "context"
    .parameter "searchView"
    .parameter "searchable"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/SearchView;",
            "Landroid/app/SearchableInfo;",
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p4, outsideDrawablesCache:Ljava/util/WeakHashMap;,"Ljava/util/WeakHashMap<Ljava/lang/String;Landroid/graphics/drawable/Drawable$ConstantState;>;"
    const/4 v4, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    .line 113
    const v1, 0x10900c3

    #@5
    const/4 v2, 0x0

    #@6
    invoke-direct {p0, p1, v1, v2, v4}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V

    #@9
    .line 80
    const/4 v1, 0x0

    #@a
    iput-boolean v1, p0, Landroid/widget/SuggestionsAdapter;->mClosed:Z

    #@c
    .line 81
    iput v4, p0, Landroid/widget/SuggestionsAdapter;->mQueryRefinement:I

    #@e
    .line 94
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mText1Col:I

    #@10
    .line 95
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mText2Col:I

    #@12
    .line 96
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mText2UrlCol:I

    #@14
    .line 97
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mIconName1Col:I

    #@16
    .line 98
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mIconName2Col:I

    #@18
    .line 99
    iput v3, p0, Landroid/widget/SuggestionsAdapter;->mFlagsCol:I

    #@1a
    .line 117
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@1c
    const-string/jumbo v2, "search"

    #@1f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Landroid/app/SearchManager;

    #@25
    iput-object v1, p0, Landroid/widget/SuggestionsAdapter;->mSearchManager:Landroid/app/SearchManager;

    #@27
    .line 118
    iput-object p2, p0, Landroid/widget/SuggestionsAdapter;->mSearchView:Landroid/widget/SearchView;

    #@29
    .line 119
    iput-object p3, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@2b
    .line 121
    iget-object v1, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@2d
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v1, v2}, Landroid/app/SearchableInfo;->getActivityContext(Landroid/content/Context;)Landroid/content/Context;

    #@32
    move-result-object v0

    #@33
    .line 122
    .local v0, activityContext:Landroid/content/Context;
    iget-object v1, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@35
    iget-object v2, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@37
    invoke-virtual {v1, v2, v0}, Landroid/app/SearchableInfo;->getProviderContext(Landroid/content/Context;Landroid/content/Context;)Landroid/content/Context;

    #@3a
    move-result-object v1

    #@3b
    iput-object v1, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@3d
    .line 124
    iput-object p4, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@3f
    .line 139
    invoke-virtual {p0}, Landroid/widget/SuggestionsAdapter;->getFilter()Landroid/widget/Filter;

    #@42
    move-result-object v1

    #@43
    new-instance v2, Landroid/widget/SuggestionsAdapter$1;

    #@45
    invoke-direct {v2, p0}, Landroid/widget/SuggestionsAdapter$1;-><init>(Landroid/widget/SuggestionsAdapter;)V

    #@48
    invoke-virtual {v1, v2}, Landroid/widget/Filter;->setDelayer(Landroid/widget/Filter$Delayer;)V

    #@4b
    .line 151
    return-void
.end method

.method private checkIconCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "resourceUri"

    #@0
    .prologue
    .line 638
    iget-object v1, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    #@8
    .line 639
    .local v0, cached:Landroid/graphics/drawable/Drawable$ConstantState;
    if-nez v0, :cond_c

    #@a
    .line 640
    const/4 v1, 0x0

    #@b
    .line 643
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method private formatUrl(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 10
    .parameter "url"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 387
    iget-object v0, p0, Landroid/widget/SuggestionsAdapter;->mUrlColor:Landroid/content/res/ColorStateList;

    #@4
    if-nez v0, :cond_26

    #@6
    .line 389
    new-instance v6, Landroid/util/TypedValue;

    #@8
    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    #@b
    .line 390
    .local v6, colorValue:Landroid/util/TypedValue;
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@10
    move-result-object v0

    #@11
    const v3, 0x1010267

    #@14
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v3, v6, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@18
    .line 391
    iget-object v0, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v0

    #@1e
    iget v3, v6, Landroid/util/TypedValue;->resourceId:I

    #@20
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/widget/SuggestionsAdapter;->mUrlColor:Landroid/content/res/ColorStateList;

    #@26
    .line 394
    .end local v6           #colorValue:Landroid/util/TypedValue;
    :cond_26
    new-instance v7, Landroid/text/SpannableString;

    #@28
    invoke-direct {v7, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@2b
    .line 395
    .local v7, text:Landroid/text/SpannableString;
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    #@2d
    iget-object v4, p0, Landroid/widget/SuggestionsAdapter;->mUrlColor:Landroid/content/res/ColorStateList;

    #@2f
    move v3, v2

    #@30
    move-object v5, v1

    #@31
    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    #@34
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@37
    move-result v1

    #@38
    const/16 v3, 0x21

    #@3a
    invoke-virtual {v7, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@3d
    .line 398
    return-object v7
.end method

.method private getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    .registers 12
    .parameter "component"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 702
    iget-object v7, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v5

    #@7
    .line 705
    .local v5, pm:Landroid/content/pm/PackageManager;
    const/16 v7, 0x80

    #@9
    :try_start_9
    invoke-virtual {v5, p1, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_c} :catch_15

    #@c
    move-result-object v0

    #@d
    .line 710
    .local v0, activityInfo:Landroid/content/pm/ActivityInfo;
    invoke-virtual {v0}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@10
    move-result v3

    #@11
    .line 711
    .local v3, iconId:I
    if-nez v3, :cond_21

    #@13
    move-object v1, v6

    #@14
    .line 719
    .end local v0           #activityInfo:Landroid/content/pm/ActivityInfo;
    .end local v3           #iconId:I
    :cond_14
    :goto_14
    return-object v1

    #@15
    .line 706
    :catch_15
    move-exception v2

    #@16
    .line 707
    .local v2, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "SuggestionsAdapter"

    #@18
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    #@1b
    move-result-object v8

    #@1c
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    move-object v1, v6

    #@20
    .line 708
    goto :goto_14

    #@21
    .line 712
    .end local v2           #ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #activityInfo:Landroid/content/pm/ActivityInfo;
    .restart local v3       #iconId:I
    :cond_21
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    .line 713
    .local v4, pkg:Ljava/lang/String;
    iget-object v7, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@27
    invoke-virtual {v5, v4, v3, v7}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v1

    #@2b
    .line 714
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_14

    #@2d
    .line 715
    const-string v7, "SuggestionsAdapter"

    #@2f
    new-instance v8, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v9, "Invalid icon resource "

    #@36
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    const-string v9, " for "

    #@40
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v8

    #@44
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v8

    #@4c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v8

    #@50
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    move-object v1, v6

    #@54
    .line 717
    goto :goto_14
.end method

.method private getActivityIconWithCache(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "component"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 680
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 682
    .local v1, componentIconKey:Ljava/lang/String;
    iget-object v5, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@7
    invoke-virtual {v5, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_23

    #@d
    .line 683
    iget-object v5, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@f
    invoke-virtual {v5, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    #@15
    .line 684
    .local v0, cached:Landroid/graphics/drawable/Drawable$ConstantState;
    if-nez v0, :cond_18

    #@17
    .line 691
    .end local v0           #cached:Landroid/graphics/drawable/Drawable$ConstantState;
    :goto_17
    return-object v4

    #@18
    .line 684
    .restart local v0       #cached:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_18
    iget-object v4, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v4

    #@22
    goto :goto_17

    #@23
    .line 687
    .end local v0           #cached:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_23
    invoke-direct {p0, p1}, Landroid/widget/SuggestionsAdapter;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v2

    #@27
    .line 689
    .local v2, drawable:Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_31

    #@29
    move-object v3, v4

    #@2a
    .line 690
    .local v3, toCache:Landroid/graphics/drawable/Drawable$ConstantState;
    :goto_2a
    iget-object v4, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@2c
    invoke-virtual {v4, v1, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-object v4, v2

    #@30
    .line 691
    goto :goto_17

    #@31
    .line 689
    .end local v3           #toCache:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_31
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@34
    move-result-object v3

    #@35
    goto :goto_2a
.end method

.method public static getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "cursor"
    .parameter "columnName"

    #@0
    .prologue
    .line 731
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 732
    .local v0, col:I
    invoke-static {p0, v0}, Landroid/widget/SuggestionsAdapter;->getStringOrNull(Landroid/database/Cursor;I)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method private getDefaultIcon1(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "cursor"

    #@0
    .prologue
    .line 661
    iget-object v1, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@5
    move-result-object v1

    #@6
    invoke-direct {p0, v1}, Landroid/widget/SuggestionsAdapter;->getActivityIconWithCache(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v0

    #@a
    .line 662
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_d

    #@c
    .line 667
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :goto_c
    return-object v0

    #@d
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    :cond_d
    iget-object v1, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v0

    #@17
    goto :goto_c
.end method

.method private getDrawable(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .registers 12
    .parameter "uri"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 605
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@4
    move-result-object v3

    #@5
    .line 606
    .local v3, scheme:Ljava/lang/String;
    const-string v5, "android.resource"

    #@7
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_63

    #@d
    .line 608
    iget-object v5, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@f
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p1}, Landroid/content/ContentResolver;->getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_16} :catch_3a

    #@16
    move-result-object v2

    #@17
    .line 611
    .local v2, r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :try_start_17
    iget-object v5, v2, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    #@19
    iget v7, v2, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    #@1b
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1e
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_17 .. :try_end_1e} :catch_20
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_1e} :catch_3a

    #@1e
    move-result-object v5

    #@1f
    .line 633
    .end local v2           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    .end local v3           #scheme:Ljava/lang/String;
    :goto_1f
    return-object v5

    #@20
    .line 612
    .restart local v2       #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    .restart local v3       #scheme:Ljava/lang/String;
    :catch_20
    move-exception v0

    #@21
    .line 613
    .local v0, ex:Landroid/content/res/Resources$NotFoundException;
    :try_start_21
    new-instance v5, Ljava/io/FileNotFoundException;

    #@23
    new-instance v7, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v8, "Resource does not exist: "

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v7

    #@32
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    invoke-direct {v5, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@39
    throw v5
    :try_end_3a
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_3a} :catch_3a

    #@3a
    .line 631
    .end local v0           #ex:Landroid/content/res/Resources$NotFoundException;
    .end local v2           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    .end local v3           #scheme:Ljava/lang/String;
    :catch_3a
    move-exception v1

    #@3b
    .line 632
    .local v1, fnfe:Ljava/io/FileNotFoundException;
    const-string v5, "SuggestionsAdapter"

    #@3d
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v8, "Icon not found: "

    #@44
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    const-string v8, ", "

    #@4e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v7

    #@52
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v7

    #@5e
    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    move-object v5, v6

    #@62
    .line 633
    goto :goto_1f

    #@63
    .line 617
    .end local v1           #fnfe:Ljava/io/FileNotFoundException;
    .restart local v3       #scheme:Ljava/lang/String;
    :cond_63
    :try_start_63
    iget-object v5, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@65
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@6c
    move-result-object v4

    #@6d
    .line 618
    .local v4, stream:Ljava/io/InputStream;
    if-nez v4, :cond_88

    #@6f
    .line 619
    new-instance v5, Ljava/io/FileNotFoundException;

    #@71
    new-instance v7, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v8, "Failed to open "

    #@78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v7

    #@84
    invoke-direct {v5, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@87
    throw v5
    :try_end_88
    .catch Ljava/io/FileNotFoundException; {:try_start_63 .. :try_end_88} :catch_3a

    #@88
    .line 622
    :cond_88
    const/4 v5, 0x0

    #@89
    :try_start_89
    invoke-static {v4, v5}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_8c
    .catchall {:try_start_89 .. :try_end_8c} :catchall_ac

    #@8c
    move-result-object v5

    #@8d
    .line 625
    :try_start_8d
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_90} :catch_91
    .catch Ljava/io/FileNotFoundException; {:try_start_8d .. :try_end_90} :catch_3a

    #@90
    goto :goto_1f

    #@91
    .line 626
    :catch_91
    move-exception v0

    #@92
    .line 627
    .local v0, ex:Ljava/io/IOException;
    :try_start_92
    const-string v7, "SuggestionsAdapter"

    #@94
    new-instance v8, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v9, "Error closing icon stream for "

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_aa
    .catch Ljava/io/FileNotFoundException; {:try_start_92 .. :try_end_aa} :catch_3a

    #@aa
    goto/16 :goto_1f

    #@ac
    .line 624
    .end local v0           #ex:Ljava/io/IOException;
    :catchall_ac
    move-exception v5

    #@ad
    .line 625
    :try_start_ad
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_b0
    .catch Ljava/io/IOException; {:try_start_ad .. :try_end_b0} :catch_b1
    .catch Ljava/io/FileNotFoundException; {:try_start_ad .. :try_end_b0} :catch_3a

    #@b0
    .line 624
    :goto_b0
    :try_start_b0
    throw v5

    #@b1
    .line 626
    :catch_b1
    move-exception v0

    #@b2
    .line 627
    .restart local v0       #ex:Ljava/io/IOException;
    const-string v7, "SuggestionsAdapter"

    #@b4
    new-instance v8, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v9, "Error closing icon stream for "

    #@bb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v8

    #@c3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v8

    #@c7
    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ca
    .catch Ljava/io/FileNotFoundException; {:try_start_b0 .. :try_end_ca} :catch_3a

    #@ca
    goto :goto_b0
.end method

.method private getDrawableFromResourceValue(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 11
    .parameter "drawableId"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 562
    if-eqz p1, :cond_11

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v6

    #@7
    if-eqz v6, :cond_11

    #@9
    const-string v6, "0"

    #@b
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_13

    #@11
    :cond_11
    move-object v0, v5

    #@12
    .line 594
    :cond_12
    :goto_12
    return-object v0

    #@13
    .line 567
    :cond_13
    :try_start_13
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@16
    move-result v3

    #@17
    .line 569
    .local v3, resourceId:I
    new-instance v6, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v7, "android.resource://"

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    iget-object v7, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@24
    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    const-string v7, "/"

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    .line 572
    .local v1, drawableUri:Ljava/lang/String;
    invoke-direct {p0, v1}, Landroid/widget/SuggestionsAdapter;->checkIconCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@3d
    move-result-object v0

    #@3e
    .line 573
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_12

    #@40
    .line 577
    iget-object v6, p0, Landroid/widget/SuggestionsAdapter;->mProviderContext:Landroid/content/Context;

    #@42
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@49
    move-result-object v0

    #@4a
    .line 579
    invoke-direct {p0, v1, v0}, Landroid/widget/SuggestionsAdapter;->storeInIconCache(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    :try_end_4d
    .catch Ljava/lang/NumberFormatException; {:try_start_13 .. :try_end_4d} :catch_4e
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_13 .. :try_end_4d} :catch_61

    #@4d
    goto :goto_12

    #@4e
    .line 581
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    .end local v1           #drawableUri:Ljava/lang/String;
    .end local v3           #resourceId:I
    :catch_4e
    move-exception v2

    #@4f
    .line 583
    .local v2, nfe:Ljava/lang/NumberFormatException;
    invoke-direct {p0, p1}, Landroid/widget/SuggestionsAdapter;->checkIconCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@52
    move-result-object v0

    #@53
    .line 584
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_12

    #@55
    .line 587
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@58
    move-result-object v4

    #@59
    .line 588
    .local v4, uri:Landroid/net/Uri;
    invoke-direct {p0, v4}, Landroid/widget/SuggestionsAdapter;->getDrawable(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    #@5c
    move-result-object v0

    #@5d
    .line 589
    invoke-direct {p0, p1, v0}, Landroid/widget/SuggestionsAdapter;->storeInIconCache(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    #@60
    goto :goto_12

    #@61
    .line 591
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    .end local v2           #nfe:Ljava/lang/NumberFormatException;
    .end local v4           #uri:Landroid/net/Uri;
    :catch_61
    move-exception v2

    #@62
    .line 593
    .local v2, nfe:Landroid/content/res/Resources$NotFoundException;
    const-string v6, "SuggestionsAdapter"

    #@64
    new-instance v7, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v8, "Icon resource not found: "

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    move-object v0, v5

    #@7b
    .line 594
    goto :goto_12
.end method

.method private getIcon1(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;
    .registers 6
    .parameter "cursor"

    #@0
    .prologue
    .line 440
    iget v2, p0, Landroid/widget/SuggestionsAdapter;->mIconName1Col:I

    #@2
    const/4 v3, -0x1

    #@3
    if-ne v2, v3, :cond_7

    #@5
    .line 441
    const/4 v0, 0x0

    #@6
    .line 448
    :cond_6
    :goto_6
    return-object v0

    #@7
    .line 443
    :cond_7
    iget v2, p0, Landroid/widget/SuggestionsAdapter;->mIconName1Col:I

    #@9
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 444
    .local v1, value:Ljava/lang/String;
    invoke-direct {p0, v1}, Landroid/widget/SuggestionsAdapter;->getDrawableFromResourceValue(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v0

    #@11
    .line 445
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_6

    #@13
    .line 448
    invoke-direct {p0, p1}, Landroid/widget/SuggestionsAdapter;->getDefaultIcon1(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v0

    #@17
    goto :goto_6
.end method

.method private getIcon2(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "cursor"

    #@0
    .prologue
    .line 452
    iget v1, p0, Landroid/widget/SuggestionsAdapter;->mIconName2Col:I

    #@2
    const/4 v2, -0x1

    #@3
    if-ne v1, v2, :cond_7

    #@5
    .line 453
    const/4 v1, 0x0

    #@6
    .line 456
    :goto_6
    return-object v1

    #@7
    .line 455
    :cond_7
    iget v1, p0, Landroid/widget/SuggestionsAdapter;->mIconName2Col:I

    #@9
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 456
    .local v0, value:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/widget/SuggestionsAdapter;->getDrawableFromResourceValue(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v1

    #@11
    goto :goto_6
.end method

.method private static getStringOrNull(Landroid/database/Cursor;I)Ljava/lang/String;
    .registers 6
    .parameter "cursor"
    .parameter "col"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 736
    const/4 v2, -0x1

    #@2
    if-ne p1, v2, :cond_5

    #@4
    .line 745
    :goto_4
    return-object v1

    #@5
    .line 740
    :cond_5
    :try_start_5
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_8} :catch_a

    #@8
    move-result-object v1

    #@9
    goto :goto_4

    #@a
    .line 741
    :catch_a
    move-exception v0

    #@b
    .line 742
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "SuggestionsAdapter"

    #@d
    const-string/jumbo v3, "unexpected error retrieving valid column from cursor, did the remote process die?"

    #@10
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    goto :goto_4
.end method

.method private setViewDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V
    .registers 6
    .parameter "v"
    .parameter "drawable"
    .parameter "nullVisibility"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 466
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@4
    .line 468
    if-nez p2, :cond_a

    #@6
    .line 469
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@9
    .line 481
    :goto_9
    return-void

    #@a
    .line 471
    :cond_a
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@d
    .line 478
    invoke-virtual {p2, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@10
    .line 479
    const/4 v0, 0x1

    #@11
    invoke-virtual {p2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@14
    goto :goto_9
.end method

.method private setViewText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .registers 15
    .parameter "v"
    .parameter "text"

    #@0
    .prologue
    const/high16 v9, 0x207

    #@2
    const/16 v11, 0x8

    #@4
    const/4 v8, 0x2

    #@5
    const/4 v10, 0x0

    #@6
    .line 405
    if-nez p2, :cond_88

    #@8
    const-string v2, ""

    #@a
    .line 406
    .local v2, OriginalQueryedResult:Ljava/lang/String;
    :goto_a
    new-instance v1, Ljava/lang/StringBuffer;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@f
    .line 407
    .local v1, ColorCodedStringForTextView:Ljava/lang/StringBuffer;
    iget-object v6, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    #@18
    move-result v6

    #@19
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    if-nez v6, :cond_8e

    #@1f
    const-string v0, ""

    #@21
    .line 409
    .local v0, ColorCode:Ljava/lang/String;
    :goto_21
    iget-object v6, p0, Landroid/widget/SuggestionsAdapter;->mStringInputedConstraint:Ljava/lang/String;

    #@23
    invoke-static {v6, v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    #@26
    move-result-object v5

    #@27
    .line 410
    .local v5, p:Ljava/util/regex/Pattern;
    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@2a
    move-result-object v4

    #@2b
    .line 412
    .local v4, m:Ljava/util/regex/Matcher;
    const/4 v3, 0x0

    #@2c
    .line 413
    .local v3, i:I
    :cond_2c
    :goto_2c
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_ea

    #@32
    .line 414
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    if-eqz v6, :cond_2c

    #@38
    .line 416
    const/16 v6, 0x1e

    #@3a
    if-ge v3, v6, :cond_b5

    #@3c
    .line 417
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@4c
    move-result v8

    #@4d
    iget-object v9, p0, Landroid/widget/SuggestionsAdapter;->mStringInputedConstraint:Ljava/lang/String;

    #@4f
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@52
    move-result v9

    #@53
    sub-int/2addr v8, v9

    #@54
    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, "<font color=\""

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    const-string v7, "\"\\>"

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@73
    move-result-object v7

    #@74
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    const-string v7, "</font>"

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v4, v1, v6}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    #@85
    .line 418
    add-int/lit8 v3, v3, 0x1

    #@87
    goto :goto_2c

    #@88
    .line 405
    .end local v0           #ColorCode:Ljava/lang/String;
    .end local v1           #ColorCodedStringForTextView:Ljava/lang/StringBuffer;
    .end local v2           #OriginalQueryedResult:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #m:Ljava/util/regex/Matcher;
    .end local v5           #p:Ljava/util/regex/Pattern;
    :cond_88
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    goto/16 :goto_a

    #@8e
    .line 407
    .restart local v1       #ColorCodedStringForTextView:Ljava/lang/StringBuffer;
    .restart local v2       #OriginalQueryedResult:Ljava/lang/String;
    :cond_8e
    new-instance v6, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v7, "#"

    #@95
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v6

    #@99
    iget-object v7, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@9b
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9e
    move-result-object v7

    #@9f
    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    #@a2
    move-result v7

    #@a3
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    invoke-virtual {v7, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@aa
    move-result-object v7

    #@ab
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v6

    #@af
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v0

    #@b3
    goto/16 :goto_21

    #@b5
    .line 421
    .restart local v0       #ColorCode:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #m:Ljava/util/regex/Matcher;
    .restart local v5       #p:Ljava/util/regex/Pattern;
    :cond_b5
    new-instance v6, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@c5
    move-result v8

    #@c6
    iget-object v9, p0, Landroid/widget/SuggestionsAdapter;->mStringInputedConstraint:Ljava/lang/String;

    #@c8
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@cb
    move-result v9

    #@cc
    sub-int/2addr v8, v9

    #@cd
    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v6

    #@d5
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@d8
    move-result-object v7

    #@d9
    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v6

    #@e1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v6

    #@e5
    invoke-virtual {v4, v1, v6}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    #@e8
    goto/16 :goto_2c

    #@ea
    .line 426
    :cond_ea
    invoke-virtual {v4, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    #@ed
    .line 428
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@f0
    move-result-object v6

    #@f1
    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    #@f4
    move-result-object v6

    #@f5
    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f8
    .line 432
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@fb
    move-result v6

    #@fc
    if-eqz v6, :cond_102

    #@fe
    .line 433
    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    #@101
    .line 437
    :goto_101
    return-void

    #@102
    .line 435
    :cond_102
    invoke-virtual {p1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    #@105
    goto :goto_101
.end method

.method private storeInIconCache(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "resourceUri"
    .parameter "drawable"

    #@0
    .prologue
    .line 647
    if-eqz p2, :cond_b

    #@2
    .line 648
    iget-object v0, p0, Landroid/widget/SuggestionsAdapter;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@4
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 650
    :cond_b
    return-void
.end method

.method private updateSpinnerState(Landroid/database/Cursor;)V
    .registers 4
    .parameter "cursor"

    #@0
    .prologue
    .line 249
    if-eqz p1, :cond_11

    #@2
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    #@5
    move-result-object v0

    #@6
    .line 257
    .local v0, extras:Landroid/os/Bundle;
    :goto_6
    if-eqz v0, :cond_10

    #@8
    const-string v1, "in_progress"

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_10

    #@10
    .line 264
    :cond_10
    return-void

    #@11
    .line 249
    .end local v0           #extras:Landroid/os/Bundle;
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_6
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 15
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    #@0
    .prologue
    const/16 v10, 0x8

    #@2
    const/4 v9, 0x2

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v7, 0x1

    #@5
    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    check-cast v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;

    #@b
    .line 329
    .local v3, views:Landroid/widget/SuggestionsAdapter$ChildViewCache;
    const/4 v0, 0x0

    #@c
    .line 330
    .local v0, flags:I
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mFlagsCol:I

    #@e
    const/4 v5, -0x1

    #@f
    if-eq v4, v5, :cond_17

    #@11
    .line 331
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mFlagsCol:I

    #@13
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    #@16
    move-result v0

    #@17
    .line 333
    :cond_17
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@19
    if-eqz v4, :cond_26

    #@1b
    .line 334
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mText1Col:I

    #@1d
    invoke-static {p3, v4}, Landroid/widget/SuggestionsAdapter;->getStringOrNull(Landroid/database/Cursor;I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    .line 335
    .local v1, text1:Ljava/lang/String;
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@23
    invoke-direct {p0, v4, v1}, Landroid/widget/SuggestionsAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    #@26
    .line 337
    .end local v1           #text1:Ljava/lang/String;
    :cond_26
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText2:Landroid/widget/TextView;

    #@28
    if-eqz v4, :cond_4f

    #@2a
    .line 339
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mText2UrlCol:I

    #@2c
    invoke-static {p3, v4}, Landroid/widget/SuggestionsAdapter;->getStringOrNull(Landroid/database/Cursor;I)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    .line 340
    .local v2, text2:Ljava/lang/CharSequence;
    if-eqz v2, :cond_8c

    #@32
    .line 341
    invoke-direct {p0, v2}, Landroid/widget/SuggestionsAdapter;->formatUrl(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@35
    move-result-object v2

    #@36
    .line 348
    :goto_36
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@39
    move-result v4

    #@3a
    if-eqz v4, :cond_93

    #@3c
    .line 349
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@3e
    if-eqz v4, :cond_4a

    #@40
    .line 350
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@42
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    #@45
    .line 351
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@47
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    #@4a
    .line 359
    :cond_4a
    :goto_4a
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText2:Landroid/widget/TextView;

    #@4c
    invoke-direct {p0, v4, v2}, Landroid/widget/SuggestionsAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    #@4f
    .line 362
    .end local v2           #text2:Ljava/lang/CharSequence;
    :cond_4f
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIcon1:Landroid/widget/ImageView;

    #@51
    if-eqz v4, :cond_5d

    #@53
    .line 363
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIcon1:Landroid/widget/ImageView;

    #@55
    invoke-direct {p0, p3}, Landroid/widget/SuggestionsAdapter;->getIcon1(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;

    #@58
    move-result-object v5

    #@59
    const/4 v6, 0x4

    #@5a
    invoke-direct {p0, v4, v5, v6}, Landroid/widget/SuggestionsAdapter;->setViewDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V

    #@5d
    .line 365
    :cond_5d
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIcon2:Landroid/widget/ImageView;

    #@5f
    if-eqz v4, :cond_6a

    #@61
    .line 366
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIcon2:Landroid/widget/ImageView;

    #@63
    invoke-direct {p0, p3}, Landroid/widget/SuggestionsAdapter;->getIcon2(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;

    #@66
    move-result-object v5

    #@67
    invoke-direct {p0, v4, v5, v10}, Landroid/widget/SuggestionsAdapter;->setViewDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V

    #@6a
    .line 368
    :cond_6a
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mQueryRefinement:I

    #@6c
    if-eq v4, v9, :cond_76

    #@6e
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mQueryRefinement:I

    #@70
    if-ne v4, v7, :cond_a2

    #@72
    and-int/lit8 v4, v0, 0x1

    #@74
    if-eqz v4, :cond_a2

    #@76
    .line 371
    :cond_76
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIconRefine:Landroid/widget/ImageView;

    #@78
    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    #@7b
    .line 372
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIconRefine:Landroid/widget/ImageView;

    #@7d
    iget-object v5, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@7f
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    #@86
    .line 373
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIconRefine:Landroid/widget/ImageView;

    #@88
    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@8b
    .line 377
    :goto_8b
    return-void

    #@8c
    .line 343
    .restart local v2       #text2:Ljava/lang/CharSequence;
    :cond_8c
    iget v4, p0, Landroid/widget/SuggestionsAdapter;->mText2Col:I

    #@8e
    invoke-static {p3, v4}, Landroid/widget/SuggestionsAdapter;->getStringOrNull(Landroid/database/Cursor;I)Ljava/lang/String;

    #@91
    move-result-object v2

    #@92
    goto :goto_36

    #@93
    .line 354
    :cond_93
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@95
    if-eqz v4, :cond_4a

    #@97
    .line 355
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@99
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    #@9c
    .line 356
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@9e
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    #@a1
    goto :goto_4a

    #@a2
    .line 375
    .end local v2           #text2:Ljava/lang/CharSequence;
    :cond_a2
    iget-object v4, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mIconRefine:Landroid/widget/ImageView;

    #@a4
    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    #@a7
    goto :goto_8b
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 273
    iget-boolean v1, p0, Landroid/widget/SuggestionsAdapter;->mClosed:Z

    #@2
    if-eqz v1, :cond_11

    #@4
    .line 274
    const-string v1, "SuggestionsAdapter"

    #@6
    const-string v2, "Tried to change cursor after adapter was closed."

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 275
    if-eqz p1, :cond_10

    #@d
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    #@10
    .line 293
    :cond_10
    :goto_10
    return-void

    #@11
    .line 280
    :cond_11
    :try_start_11
    invoke-super {p0, p1}, Landroid/widget/ResourceCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    #@14
    .line 282
    if-eqz p1, :cond_10

    #@16
    .line 283
    const-string/jumbo v1, "suggest_text_1"

    #@19
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mText1Col:I

    #@1f
    .line 284
    const-string/jumbo v1, "suggest_text_2"

    #@22
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@25
    move-result v1

    #@26
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mText2Col:I

    #@28
    .line 285
    const-string/jumbo v1, "suggest_text_2_url"

    #@2b
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@2e
    move-result v1

    #@2f
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mText2UrlCol:I

    #@31
    .line 286
    const-string/jumbo v1, "suggest_icon_1"

    #@34
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@37
    move-result v1

    #@38
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mIconName1Col:I

    #@3a
    .line 287
    const-string/jumbo v1, "suggest_icon_2"

    #@3d
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@40
    move-result v1

    #@41
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mIconName2Col:I

    #@43
    .line 288
    const-string/jumbo v1, "suggest_flags"

    #@46
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v1

    #@4a
    iput v1, p0, Landroid/widget/SuggestionsAdapter;->mFlagsCol:I
    :try_end_4c
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_4c} :catch_4d

    #@4c
    goto :goto_10

    #@4d
    .line 290
    :catch_4d
    move-exception v0

    #@4e
    .line 291
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "SuggestionsAdapter"

    #@50
    const-string v2, "error changing cursor and caching columns"

    #@52
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@55
    goto :goto_10
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 226
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/SuggestionsAdapter;->changeCursor(Landroid/database/Cursor;)V

    #@4
    .line 227
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/SuggestionsAdapter;->mClosed:Z

    #@7
    .line 228
    return-void
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 7
    .parameter "cursor"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 493
    if-nez p1, :cond_5

    #@3
    move-object v1, v3

    #@4
    .line 516
    :cond_4
    :goto_4
    return-object v1

    #@5
    .line 497
    :cond_5
    const-string/jumbo v4, "suggest_intent_query"

    #@8
    invoke-static {p1, v4}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 498
    .local v1, query:Ljava/lang/String;
    if-nez v1, :cond_4

    #@e
    .line 502
    iget-object v4, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@10
    invoke-virtual {v4}, Landroid/app/SearchableInfo;->shouldRewriteQueryFromData()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_21

    #@16
    .line 503
    const-string/jumbo v4, "suggest_intent_data"

    #@19
    invoke-static {p1, v4}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 504
    .local v0, data:Ljava/lang/String;
    if-eqz v0, :cond_21

    #@1f
    move-object v1, v0

    #@20
    .line 505
    goto :goto_4

    #@21
    .line 509
    .end local v0           #data:Ljava/lang/String;
    :cond_21
    iget-object v4, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@23
    invoke-virtual {v4}, Landroid/app/SearchableInfo;->shouldRewriteQueryFromText()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_34

    #@29
    .line 510
    const-string/jumbo v4, "suggest_text_1"

    #@2c
    invoke-static {p1, v4}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    .line 511
    .local v2, text1:Ljava/lang/String;
    if-eqz v2, :cond_34

    #@32
    move-object v1, v2

    #@33
    .line 512
    goto :goto_4

    #@34
    .end local v2           #text1:Ljava/lang/String;
    :cond_34
    move-object v1, v3

    #@35
    .line 516
    goto :goto_4
.end method

.method public getQueryRefinement()I
    .registers 2

    #@0
    .prologue
    .line 171
    iget v0, p0, Landroid/widget/SuggestionsAdapter;->mQueryRefinement:I

    #@2
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 528
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-object v2

    #@4
    .line 538
    :cond_4
    :goto_4
    return-object v2

    #@5
    .line 529
    :catch_5
    move-exception v0

    #@6
    .line 530
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v4, "SuggestionsAdapter"

    #@8
    const-string v5, "Search suggestions cursor threw exception."

    #@a
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d
    .line 532
    iget-object v4, p0, Landroid/widget/CursorAdapter;->mContext:Landroid/content/Context;

    #@f
    iget-object v5, p0, Landroid/widget/CursorAdapter;->mCursor:Landroid/database/Cursor;

    #@11
    invoke-virtual {p0, v4, v5, p3}, Landroid/widget/SuggestionsAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    .line 533
    .local v2, v:Landroid/view/View;
    if-eqz v2, :cond_4

    #@17
    .line 534
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;

    #@1d
    .line 535
    .local v3, views:Landroid/widget/SuggestionsAdapter$ChildViewCache;
    iget-object v1, v3, Landroid/widget/SuggestionsAdapter$ChildViewCache;->mText1:Landroid/widget/TextView;

    #@1f
    .line 536
    .local v1, tv:Landroid/widget/TextView;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@26
    goto :goto_4
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 180
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    #@0
    .prologue
    .line 300
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 301
    .local v0, v:Landroid/view/View;
    new-instance v1, Landroid/widget/SuggestionsAdapter$ChildViewCache;

    #@6
    invoke-direct {v1, v0}, Landroid/widget/SuggestionsAdapter$ChildViewCache;-><init>(Landroid/view/View;)V

    #@9
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@c
    .line 302
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .registers 2

    #@0
    .prologue
    .line 233
    invoke-super {p0}, Landroid/widget/ResourceCursorAdapter;->notifyDataSetChanged()V

    #@3
    .line 237
    invoke-virtual {p0}, Landroid/widget/SuggestionsAdapter;->getCursor()Landroid/database/Cursor;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/SuggestionsAdapter;->updateSpinnerState(Landroid/database/Cursor;)V

    #@a
    .line 238
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .registers 2

    #@0
    .prologue
    .line 243
    invoke-super {p0}, Landroid/widget/ResourceCursorAdapter;->notifyDataSetInvalidated()V

    #@3
    .line 245
    invoke-virtual {p0}, Landroid/widget/SuggestionsAdapter;->getCursor()Landroid/database/Cursor;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/SuggestionsAdapter;->updateSpinnerState(Landroid/database/Cursor;)V

    #@a
    .line 246
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 380
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 381
    .local v0, tag:Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/CharSequence;

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 382
    iget-object v1, p0, Landroid/widget/SuggestionsAdapter;->mSearchView:Landroid/widget/SearchView;

    #@a
    check-cast v0, Ljava/lang/CharSequence;

    #@c
    .end local v0           #tag:Ljava/lang/Object;
    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->onQueryRefine(Ljava/lang/CharSequence;)V

    #@f
    .line 384
    :cond_f
    return-void
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .registers 9
    .parameter "constraint"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 191
    if-nez p1, :cond_1e

    #@3
    const-string v2, ""

    #@5
    .line 194
    .local v2, query:Ljava/lang/String;
    :goto_5
    if-nez p1, :cond_23

    #@7
    const-string v3, ""

    #@9
    :goto_9
    iput-object v3, p0, Landroid/widget/SuggestionsAdapter;->mStringInputedConstraint:Ljava/lang/String;

    #@b
    .line 201
    const/4 v0, 0x0

    #@c
    .line 202
    .local v0, cursor:Landroid/database/Cursor;
    iget-object v3, p0, Landroid/widget/SuggestionsAdapter;->mSearchView:Landroid/widget/SearchView;

    #@e
    invoke-virtual {v3}, Landroid/widget/SearchView;->getVisibility()I

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_1c

    #@14
    iget-object v3, p0, Landroid/widget/SuggestionsAdapter;->mSearchView:Landroid/widget/SearchView;

    #@16
    invoke-virtual {v3}, Landroid/widget/SearchView;->getWindowVisibility()I

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_28

    #@1c
    :cond_1c
    move-object v3, v4

    #@1d
    .line 221
    :goto_1d
    return-object v3

    #@1e
    .line 191
    .end local v0           #cursor:Landroid/database/Cursor;
    .end local v2           #query:Ljava/lang/String;
    :cond_1e
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    goto :goto_5

    #@23
    .line 194
    .restart local v2       #query:Ljava/lang/String;
    :cond_23
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    goto :goto_9

    #@28
    .line 208
    .restart local v0       #cursor:Landroid/database/Cursor;
    :cond_28
    :try_start_28
    iget-object v3, p0, Landroid/widget/SuggestionsAdapter;->mSearchManager:Landroid/app/SearchManager;

    #@2a
    iget-object v5, p0, Landroid/widget/SuggestionsAdapter;->mSearchable:Landroid/app/SearchableInfo;

    #@2c
    const/16 v6, 0x32

    #@2e
    invoke-virtual {v3, v5, v2, v6}, Landroid/app/SearchManager;->getSuggestions(Landroid/app/SearchableInfo;Ljava/lang/String;I)Landroid/database/Cursor;

    #@31
    move-result-object v0

    #@32
    .line 211
    if-eqz v0, :cond_41

    #@34
    .line 212
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_37
    .catch Ljava/lang/RuntimeException; {:try_start_28 .. :try_end_37} :catch_39

    #@37
    move-object v3, v0

    #@38
    .line 213
    goto :goto_1d

    #@39
    .line 215
    :catch_39
    move-exception v1

    #@3a
    .line 216
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v3, "SuggestionsAdapter"

    #@3c
    const-string v5, "Search suggestions query threw an exception."

    #@3e
    invoke-static {v3, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    .end local v1           #e:Ljava/lang/RuntimeException;
    :cond_41
    move-object v3, v4

    #@42
    .line 221
    goto :goto_1d
.end method

.method public setQueryRefinement(I)V
    .registers 2
    .parameter "refineWhat"

    #@0
    .prologue
    .line 163
    iput p1, p0, Landroid/widget/SuggestionsAdapter;->mQueryRefinement:I

    #@2
    .line 164
    return-void
.end method
