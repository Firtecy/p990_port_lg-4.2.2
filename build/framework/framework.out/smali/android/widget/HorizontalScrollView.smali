.class public Landroid/widget/HorizontalScrollView;
.super Landroid/widget/FrameLayout;
.source "HorizontalScrollView.java"


# static fields
.field private static final ANIMATED_SCROLL_GAP:I = 0xfa

.field private static final INVALID_POINTER:I = -0x1

.field private static final MAX_SCROLL_FACTOR:F = 0.5f

.field private static final TAG:Ljava/lang/String; = "HorizontalScrollView"


# instance fields
.field private mActivePointerId:I

.field private mChildToScrollTo:Landroid/view/View;

.field private mEdgeGlowLeft:Landroid/widget/EdgeEffect;

.field private mEdgeGlowRight:Landroid/widget/EdgeEffect;

.field private mFillViewport:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mIsBeingDragged:Z

.field private mIsLayoutDirty:Z

.field private mLastMotionX:I

.field private mLastScroll:J

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOverflingDistance:I

.field private mOverscrollDistance:I

.field private mScroller:Landroid/widget/OverScroller;

.field private mSmoothScrollingEnabled:Z

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 137
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 141
    const v0, 0x1010353

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 145
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 70
    new-instance v1, Landroid/graphics/Rect;

    #@7
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v1, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    .line 84
    iput-boolean v3, p0, Landroid/widget/HorizontalScrollView;->mIsLayoutDirty:Z

    #@e
    .line 91
    const/4 v1, 0x0

    #@f
    iput-object v1, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@11
    .line 98
    iput-boolean v2, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@13
    .line 115
    iput-boolean v3, p0, Landroid/widget/HorizontalScrollView;->mSmoothScrollingEnabled:Z

    #@15
    .line 128
    const/4 v1, -0x1

    #@16
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@18
    .line 146
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->initScrollView()V

    #@1b
    .line 148
    sget-object v1, Landroid/R$styleable;->HorizontalScrollView:[I

    #@1d
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@20
    move-result-object v0

    #@21
    .line 151
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@24
    move-result v1

    #@25
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->setFillViewport(Z)V

    #@28
    .line 153
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2b
    .line 154
    return-void
.end method

.method private canScroll()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 248
    invoke-virtual {p0, v2}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    .line 249
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_18

    #@7
    .line 250
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@a
    move-result v1

    #@b
    .line 251
    .local v1, childWidth:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@e
    move-result v3

    #@f
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@11
    add-int/2addr v4, v1

    #@12
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@14
    add-int/2addr v4, v5

    #@15
    if-ge v3, v4, :cond_18

    #@17
    const/4 v2, 0x1

    #@18
    .line 253
    .end local v1           #childWidth:I
    :cond_18
    return v2
.end method

.method private static clamp(III)I
    .registers 4
    .parameter "n"
    .parameter "my"
    .parameter "child"

    #@0
    .prologue
    .line 1595
    if-ge p1, p2, :cond_4

    #@2
    if-gez p0, :cond_6

    #@4
    .line 1596
    :cond_4
    const/4 p0, 0x0

    #@5
    .line 1601
    .end local p0
    :cond_5
    :goto_5
    return p0

    #@6
    .line 1598
    .restart local p0
    :cond_6
    add-int v0, p1, p0

    #@8
    if-le v0, p2, :cond_5

    #@a
    .line 1599
    sub-int p0, p2, p1

    #@c
    goto :goto_5
.end method

.method private doScrollX(I)V
    .registers 4
    .parameter "delta"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1125
    if-eqz p1, :cond_a

    #@3
    .line 1126
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mSmoothScrollingEnabled:Z

    #@5
    if-eqz v0, :cond_b

    #@7
    .line 1127
    invoke-virtual {p0, p1, v1}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    #@a
    .line 1132
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1129
    :cond_b
    invoke-virtual {p0, p1, v1}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    #@e
    goto :goto_a
.end method

.method private findFocusableViewInBounds(ZII)Landroid/view/View;
    .registers 15
    .parameter "leftFocus"
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    .line 871
    const/4 v10, 0x2

    #@1
    invoke-virtual {p0, v10}, Landroid/widget/HorizontalScrollView;->getFocusables(I)Ljava/util/ArrayList;

    #@4
    move-result-object v2

    #@5
    .line 872
    .local v2, focusables:Ljava/util/List;,"Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    #@6
    .line 881
    .local v1, focusCandidate:Landroid/view/View;
    const/4 v3, 0x0

    #@7
    .line 883
    .local v3, foundFullyContainedFocusable:Z
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 884
    .local v0, count:I
    const/4 v4, 0x0

    #@c
    .local v4, i:I
    :goto_c
    if-ge v4, v0, :cond_52

    #@e
    .line 885
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v5

    #@12
    check-cast v5, Landroid/view/View;

    #@14
    .line 886
    .local v5, view:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    #@17
    move-result v8

    #@18
    .line 887
    .local v8, viewLeft:I
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    #@1b
    move-result v9

    #@1c
    .line 889
    .local v9, viewRight:I
    if-ge p2, v9, :cond_29

    #@1e
    if-ge v8, p3, :cond_29

    #@20
    .line 895
    if-ge p2, v8, :cond_2c

    #@22
    if-ge v9, p3, :cond_2c

    #@24
    const/4 v7, 0x1

    #@25
    .line 898
    .local v7, viewIsFullyContained:Z
    :goto_25
    if-nez v1, :cond_2e

    #@27
    .line 900
    move-object v1, v5

    #@28
    .line 901
    move v3, v7

    #@29
    .line 884
    .end local v7           #viewIsFullyContained:Z
    :cond_29
    :goto_29
    add-int/lit8 v4, v4, 0x1

    #@2b
    goto :goto_c

    #@2c
    .line 895
    :cond_2c
    const/4 v7, 0x0

    #@2d
    goto :goto_25

    #@2e
    .line 903
    .restart local v7       #viewIsFullyContained:Z
    :cond_2e
    if-eqz p1, :cond_36

    #@30
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    #@33
    move-result v10

    #@34
    if-lt v8, v10, :cond_3e

    #@36
    :cond_36
    if-nez p1, :cond_47

    #@38
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    #@3b
    move-result v10

    #@3c
    if-le v9, v10, :cond_47

    #@3e
    :cond_3e
    const/4 v6, 0x1

    #@3f
    .line 907
    .local v6, viewIsCloserToBoundary:Z
    :goto_3f
    if-eqz v3, :cond_49

    #@41
    .line 908
    if-eqz v7, :cond_29

    #@43
    if-eqz v6, :cond_29

    #@45
    .line 914
    move-object v1, v5

    #@46
    goto :goto_29

    #@47
    .line 903
    .end local v6           #viewIsCloserToBoundary:Z
    :cond_47
    const/4 v6, 0x0

    #@48
    goto :goto_3f

    #@49
    .line 917
    .restart local v6       #viewIsCloserToBoundary:Z
    :cond_49
    if-eqz v7, :cond_4e

    #@4b
    .line 919
    move-object v1, v5

    #@4c
    .line 920
    const/4 v3, 0x1

    #@4d
    goto :goto_29

    #@4e
    .line 921
    :cond_4e
    if-eqz v6, :cond_29

    #@50
    .line 926
    move-object v1, v5

    #@51
    goto :goto_29

    #@52
    .line 933
    .end local v5           #view:Landroid/view/View;
    .end local v6           #viewIsCloserToBoundary:Z
    .end local v7           #viewIsFullyContained:Z
    .end local v8           #viewLeft:I
    .end local v9           #viewRight:I
    :cond_52
    return-object v1
.end method

.method private findFocusableViewInMyBounds(ZILandroid/view/View;)Landroid/view/View;
    .registers 8
    .parameter "leftFocus"
    .parameter "left"
    .parameter "preferredFocusable"

    #@0
    .prologue
    .line 840
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    #@3
    move-result v3

    #@4
    div-int/lit8 v0, v3, 0x2

    #@6
    .line 841
    .local v0, fadingEdgeLength:I
    add-int v1, p2, v0

    #@8
    .line 842
    .local v1, leftWithoutFadingEdge:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@b
    move-result v3

    #@c
    add-int/2addr v3, p2

    #@d
    sub-int v2, v3, v0

    #@f
    .line 844
    .local v2, rightWithoutFadingEdge:I
    if-eqz p3, :cond_1e

    #@11
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    #@14
    move-result v3

    #@15
    if-ge v3, v2, :cond_1e

    #@17
    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    #@1a
    move-result v3

    #@1b
    if-le v3, v1, :cond_1e

    #@1d
    .line 850
    .end local p3
    :goto_1d
    return-object p3

    #@1e
    .restart local p3
    :cond_1e
    invoke-direct {p0, p1, v1, v2}, Landroid/widget/HorizontalScrollView;->findFocusableViewInBounds(ZII)Landroid/view/View;

    #@21
    move-result-object p3

    #@22
    goto :goto_1d
.end method

.method private getScrollRange()I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 808
    const/4 v1, 0x0

    #@2
    .line 809
    .local v1, scrollRange:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_1f

    #@8
    .line 810
    invoke-virtual {p0, v5}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 811
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@13
    move-result v3

    #@14
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@16
    sub-int/2addr v3, v4

    #@17
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@19
    sub-int/2addr v3, v4

    #@1a
    sub-int/2addr v2, v3

    #@1b
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    #@1e
    move-result v1

    #@1f
    .line 814
    .end local v0           #child:Landroid/view/View;
    :cond_1f
    return v1
.end method

.method private inChild(II)Z
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 385
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@4
    move-result v3

    #@5
    if-lez v3, :cond_28

    #@7
    .line 386
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@9
    .line 387
    .local v1, scrollX:I
    invoke-virtual {p0, v2}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 388
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@10
    move-result v3

    #@11
    if-lt p2, v3, :cond_28

    #@13
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@16
    move-result v3

    #@17
    if-ge p2, v3, :cond_28

    #@19
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    #@1c
    move-result v3

    #@1d
    sub-int/2addr v3, v1

    #@1e
    if-lt p1, v3, :cond_28

    #@20
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    #@23
    move-result v3

    #@24
    sub-int/2addr v3, v1

    #@25
    if-ge p1, v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 393
    .end local v0           #child:Landroid/view/View;
    .end local v1           #scrollX:I
    :cond_28
    return v2
.end method

.method private initOrResetVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 398
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 402
    :goto_a
    return-void

    #@b
    .line 400
    :cond_b
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@d
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    #@10
    goto :goto_a
.end method

.method private initScrollView()V
    .registers 4

    #@0
    .prologue
    .line 196
    new-instance v1, Landroid/widget/OverScroller;

    #@2
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-direct {v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v1, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@b
    .line 197
    const/4 v1, 0x1

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->setFocusable(Z)V

    #@f
    .line 198
    const/high16 v1, 0x4

    #@11
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->setDescendantFocusability(I)V

    #@14
    .line 199
    const/4 v1, 0x0

    #@15
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->setWillNotDraw(Z)V

    #@18
    .line 200
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1a
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@1d
    move-result-object v0

    #@1e
    .line 201
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@21
    move-result v1

    #@22
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mTouchSlop:I

    #@24
    .line 202
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@27
    move-result v1

    #@28
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mMinimumVelocity:I

    #@2a
    .line 203
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mMaximumVelocity:I

    #@30
    .line 204
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    #@33
    move-result v1

    #@34
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mOverscrollDistance:I

    #@36
    .line 205
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    #@39
    move-result v1

    #@3a
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mOverflingDistance:I

    #@3c
    .line 206
    return-void
.end method

.method private initVelocityTrackerIfNotExists()V
    .registers 2

    #@0
    .prologue
    .line 405
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 406
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 408
    :cond_a
    return-void
.end method

.method private isOffScreen(Landroid/view/View;)Z
    .registers 4
    .parameter "descendant"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1104
    invoke-direct {p0, p1, v0}, Landroid/widget/HorizontalScrollView;->isWithinDeltaOfScreen(Landroid/view/View;I)Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    const/4 v0, 0x1

    #@8
    :cond_8
    return v0
.end method

.method private static isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z
    .registers 5
    .parameter "child"
    .parameter "parent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1485
    if-ne p0, p1, :cond_4

    #@3
    .line 1490
    :cond_3
    :goto_3
    return v1

    #@4
    .line 1489
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@7
    move-result-object v0

    #@8
    .line 1490
    .local v0, theParent:Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@a
    if-eqz v2, :cond_14

    #@c
    check-cast v0, Landroid/view/View;

    #@e
    .end local v0           #theParent:Landroid/view/ViewParent;
    invoke-static {v0, p1}, Landroid/widget/HorizontalScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_3

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_3
.end method

.method private isWithinDeltaOfScreen(Landroid/view/View;I)Z
    .registers 6
    .parameter "descendant"
    .parameter "delta"

    #@0
    .prologue
    .line 1112
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@5
    .line 1113
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {p0, p1, v0}, Landroid/widget/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@a
    .line 1115
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@e
    add-int/2addr v0, p2

    #@f
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@12
    move-result v1

    #@13
    if-lt v0, v1, :cond_27

    #@15
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@17
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@19
    sub-int/2addr v0, p2

    #@1a
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@1d
    move-result v1

    #@1e
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@21
    move-result v2

    #@22
    add-int/2addr v1, v2

    #@23
    if-gt v0, v1, :cond_27

    #@25
    const/4 v0, 0x1

    #@26
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_26
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    .line 675
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v3

    #@4
    const v4, 0xff00

    #@7
    and-int/2addr v3, v4

    #@8
    shr-int/lit8 v2, v3, 0x8

    #@a
    .line 677
    .local v2, pointerIndex:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@d
    move-result v1

    #@e
    .line 678
    .local v1, pointerId:I
    iget v3, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@10
    if-ne v1, v3, :cond_2b

    #@12
    .line 682
    if-nez v2, :cond_2c

    #@14
    const/4 v0, 0x1

    #@15
    .line 683
    .local v0, newPointerIndex:I
    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@18
    move-result v3

    #@19
    float-to-int v3, v3

    #@1a
    iput v3, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@1c
    .line 684
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1f
    move-result v3

    #@20
    iput v3, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@22
    .line 685
    iget-object v3, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@24
    if-eqz v3, :cond_2b

    #@26
    .line 686
    iget-object v3, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@28
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@2b
    .line 689
    .end local v0           #newPointerIndex:I
    :cond_2b
    return-void

    #@2c
    .line 682
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_15
.end method

.method private recycleVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 411
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 412
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@9
    .line 413
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    .line 415
    :cond_c
    return-void
.end method

.method private scrollAndFocus(III)Z
    .registers 12
    .parameter "direction"
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    .line 1016
    const/4 v4, 0x1

    #@1
    .line 1018
    .local v4, handled:Z
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@4
    move-result v6

    #@5
    .line 1019
    .local v6, width:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@8
    move-result v0

    #@9
    .line 1020
    .local v0, containerLeft:I
    add-int v1, v0, v6

    #@b
    .line 1021
    .local v1, containerRight:I
    const/16 v7, 0x11

    #@d
    if-ne p1, v7, :cond_26

    #@f
    const/4 v3, 0x1

    #@10
    .line 1023
    .local v3, goLeft:Z
    :goto_10
    invoke-direct {p0, v3, p2, p3}, Landroid/widget/HorizontalScrollView;->findFocusableViewInBounds(ZII)Landroid/view/View;

    #@13
    move-result-object v5

    #@14
    .line 1024
    .local v5, newFocused:Landroid/view/View;
    if-nez v5, :cond_17

    #@16
    .line 1025
    move-object v5, p0

    #@17
    .line 1028
    :cond_17
    if-lt p2, v0, :cond_28

    #@19
    if-gt p3, v1, :cond_28

    #@1b
    .line 1029
    const/4 v4, 0x0

    #@1c
    .line 1035
    :goto_1c
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->findFocus()Landroid/view/View;

    #@1f
    move-result-object v7

    #@20
    if-eq v5, v7, :cond_25

    #@22
    invoke-virtual {v5, p1}, Landroid/view/View;->requestFocus(I)Z

    #@25
    .line 1037
    :cond_25
    return v4

    #@26
    .line 1021
    .end local v3           #goLeft:Z
    .end local v5           #newFocused:Landroid/view/View;
    :cond_26
    const/4 v3, 0x0

    #@27
    goto :goto_10

    #@28
    .line 1031
    .restart local v3       #goLeft:Z
    .restart local v5       #newFocused:Landroid/view/View;
    :cond_28
    if-eqz v3, :cond_30

    #@2a
    sub-int v2, p2, v0

    #@2c
    .line 1032
    .local v2, delta:I
    :goto_2c
    invoke-direct {p0, v2}, Landroid/widget/HorizontalScrollView;->doScrollX(I)V

    #@2f
    goto :goto_1c

    #@30
    .line 1031
    .end local v2           #delta:I
    :cond_30
    sub-int v2, p3, v1

    #@32
    goto :goto_2c
.end method

.method private scrollToChild(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 1287
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@5
    .line 1290
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {p0, p1, v1}, Landroid/widget/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@a
    .line 1292
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@f
    move-result v0

    #@10
    .line 1294
    .local v0, scrollDelta:I
    if-eqz v0, :cond_16

    #@12
    .line 1295
    const/4 v1, 0x0

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    #@16
    .line 1297
    :cond_16
    return-void
.end method

.method private scrollToChildRect(Landroid/graphics/Rect;Z)Z
    .registers 6
    .parameter "rect"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1308
    invoke-virtual {p0, p1}, Landroid/widget/HorizontalScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@4
    move-result v0

    #@5
    .line 1309
    .local v0, delta:I
    if-eqz v0, :cond_10

    #@7
    const/4 v1, 0x1

    #@8
    .line 1310
    .local v1, scroll:Z
    :goto_8
    if-eqz v1, :cond_f

    #@a
    .line 1311
    if-eqz p2, :cond_12

    #@c
    .line 1312
    invoke-virtual {p0, v0, v2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    #@f
    .line 1317
    :cond_f
    :goto_f
    return v1

    #@10
    .end local v1           #scroll:Z
    :cond_10
    move v1, v2

    #@11
    .line 1309
    goto :goto_8

    #@12
    .line 1314
    .restart local v1       #scroll:Z
    :cond_12
    invoke-virtual {p0, v0, v2}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    #@15
    goto :goto_f
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 210
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 211
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "HorizontalScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 214
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@11
    .line 215
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .registers 5
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 219
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 220
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "HorizontalScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 223
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    #@11
    .line 224
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 237
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 238
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "HorizontalScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 241
    :cond_e
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 242
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 228
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "HorizontalScrollView can host only one direct child"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 232
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 233
    return-void
.end method

.method public arrowScroll(I)Z
    .registers 13
    .parameter "direction"

    #@0
    .prologue
    const/16 v10, 0x42

    #@2
    const/4 v7, 0x0

    #@3
    .line 1049
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->findFocus()Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    .line 1050
    .local v0, currentFocused:Landroid/view/View;
    if-ne v0, p0, :cond_a

    #@9
    const/4 v0, 0x0

    #@a
    .line 1052
    :cond_a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@d
    move-result-object v8

    #@e
    invoke-virtual {v8, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@11
    move-result-object v4

    #@12
    .line 1054
    .local v4, nextFocused:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getMaxScrollAmount()I

    #@15
    move-result v3

    #@16
    .line 1056
    .local v3, maxJump:I
    if-eqz v4, :cond_53

    #@18
    invoke-direct {p0, v4, v3}, Landroid/widget/HorizontalScrollView;->isWithinDeltaOfScreen(Landroid/view/View;I)Z

    #@1b
    move-result v8

    #@1c
    if-eqz v8, :cond_53

    #@1e
    .line 1057
    iget-object v7, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@20
    invoke-virtual {v4, v7}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@23
    .line 1058
    iget-object v7, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@25
    invoke-virtual {p0, v4, v7}, Landroid/widget/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@28
    .line 1059
    iget-object v7, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2a
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@2d
    move-result v6

    #@2e
    .line 1060
    .local v6, scrollDelta:I
    invoke-direct {p0, v6}, Landroid/widget/HorizontalScrollView;->doScrollX(I)V

    #@31
    .line 1061
    invoke-virtual {v4, p1}, Landroid/view/View;->requestFocus(I)Z

    #@34
    .line 1084
    :goto_34
    if-eqz v0, :cond_51

    #@36
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    #@39
    move-result v7

    #@3a
    if-eqz v7, :cond_51

    #@3c
    invoke-direct {p0, v0}, Landroid/widget/HorizontalScrollView;->isOffScreen(Landroid/view/View;)Z

    #@3f
    move-result v7

    #@40
    if-eqz v7, :cond_51

    #@42
    .line 1091
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getDescendantFocusability()I

    #@45
    move-result v2

    #@46
    .line 1092
    .local v2, descendantFocusability:I
    const/high16 v7, 0x2

    #@48
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->setDescendantFocusability(I)V

    #@4b
    .line 1093
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->requestFocus()Z

    #@4e
    .line 1094
    invoke-virtual {p0, v2}, Landroid/widget/HorizontalScrollView;->setDescendantFocusability(I)V

    #@51
    .line 1096
    .end local v2           #descendantFocusability:I
    :cond_51
    const/4 v7, 0x1

    #@52
    :cond_52
    return v7

    #@53
    .line 1064
    .end local v6           #scrollDelta:I
    :cond_53
    move v6, v3

    #@54
    .line 1066
    .restart local v6       #scrollDelta:I
    const/16 v8, 0x11

    #@56
    if-ne p1, v8, :cond_6b

    #@58
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@5b
    move-result v8

    #@5c
    if-ge v8, v6, :cond_6b

    #@5e
    .line 1067
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@61
    move-result v6

    #@62
    .line 1078
    :cond_62
    :goto_62
    if-eqz v6, :cond_52

    #@64
    .line 1081
    if-ne p1, v10, :cond_8c

    #@66
    move v7, v6

    #@67
    :goto_67
    invoke-direct {p0, v7}, Landroid/widget/HorizontalScrollView;->doScrollX(I)V

    #@6a
    goto :goto_34

    #@6b
    .line 1068
    :cond_6b
    if-ne p1, v10, :cond_62

    #@6d
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@70
    move-result v8

    #@71
    if-lez v8, :cond_62

    #@73
    .line 1070
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@76
    move-result-object v8

    #@77
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    #@7a
    move-result v1

    #@7b
    .line 1072
    .local v1, daRight:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@7e
    move-result v8

    #@7f
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@82
    move-result v9

    #@83
    add-int v5, v8, v9

    #@85
    .line 1074
    .local v5, screenRight:I
    sub-int v8, v1, v5

    #@87
    if-ge v8, v3, :cond_62

    #@89
    .line 1075
    sub-int v6, v1, v5

    #@8b
    goto :goto_62

    #@8c
    .line 1081
    .end local v1           #daRight:I
    .end local v5           #screenRight:I
    :cond_8c
    neg-int v7, v6

    #@8d
    goto :goto_67
.end method

.method protected computeHorizontalScrollOffset()I
    .registers 3

    #@0
    .prologue
    .line 1200
    const/4 v0, 0x0

    #@1
    invoke-super {p0}, Landroid/widget/FrameLayout;->computeHorizontalScrollOffset()I

    #@4
    move-result v1

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1180
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@4
    move-result v1

    #@5
    .line 1181
    .local v1, count:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@8
    move-result v5

    #@9
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@b
    sub-int/2addr v5, v6

    #@c
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@e
    sub-int v0, v5, v6

    #@10
    .line 1182
    .local v0, contentWidth:I
    if-nez v1, :cond_13

    #@12
    .line 1195
    .end local v0           #contentWidth:I
    :goto_12
    return v0

    #@13
    .line 1186
    .restart local v0       #contentWidth:I
    :cond_13
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    #@1a
    move-result v3

    #@1b
    .line 1187
    .local v3, scrollRange:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@1d
    .line 1188
    .local v4, scrollX:I
    sub-int v5, v3, v0

    #@1f
    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    #@22
    move-result v2

    #@23
    .line 1189
    .local v2, overscrollRight:I
    if-gez v4, :cond_28

    #@25
    .line 1190
    sub-int/2addr v3, v4

    #@26
    :cond_26
    :goto_26
    move v0, v3

    #@27
    .line 1195
    goto :goto_12

    #@28
    .line 1191
    :cond_28
    if-le v4, v2, :cond_26

    #@2a
    .line 1192
    sub-int v5, v4, v2

    #@2c
    add-int/2addr v3, v5

    #@2d
    goto :goto_26
.end method

.method public computeScroll()V
    .registers 15

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1234
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@4
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_5a

    #@a
    .line 1251
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@c
    .line 1252
    .local v3, oldX:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@e
    .line 1253
    .local v4, oldY:I
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@10
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    #@13
    move-result v12

    #@14
    .line 1254
    .local v12, x:I
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@16
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    #@19
    move-result v13

    #@1a
    .line 1256
    .local v13, y:I
    if-ne v3, v12, :cond_1e

    #@1c
    if-eq v4, v13, :cond_51

    #@1e
    .line 1257
    :cond_1e
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@21
    move-result v5

    #@22
    .line 1258
    .local v5, range:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getOverScrollMode()I

    #@25
    move-result v11

    #@26
    .line 1259
    .local v11, overscrollMode:I
    if-eqz v11, :cond_2c

    #@28
    if-ne v11, v10, :cond_5b

    #@2a
    if-lez v5, :cond_5b

    #@2c
    .line 1262
    .local v10, canOverscroll:Z
    :cond_2c
    :goto_2c
    sub-int v1, v12, v3

    #@2e
    sub-int v2, v13, v4

    #@30
    iget v7, p0, Landroid/widget/HorizontalScrollView;->mOverflingDistance:I

    #@32
    move-object v0, p0

    #@33
    move v8, v6

    #@34
    move v9, v6

    #@35
    invoke-virtual/range {v0 .. v9}, Landroid/widget/HorizontalScrollView;->overScrollBy(IIIIIIIIZ)Z

    #@38
    .line 1264
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@3a
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@3c
    invoke-virtual {p0, v0, v1, v3, v4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    #@3f
    .line 1266
    if-eqz v10, :cond_51

    #@41
    .line 1267
    if-gez v12, :cond_5d

    #@43
    if-ltz v3, :cond_5d

    #@45
    .line 1268
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@47
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@49
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@4c
    move-result v1

    #@4d
    float-to-int v1, v1

    #@4e
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@51
    .line 1275
    .end local v5           #range:I
    .end local v10           #canOverscroll:Z
    .end local v11           #overscrollMode:I
    :cond_51
    :goto_51
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->awakenScrollBars()Z

    #@54
    move-result v0

    #@55
    if-nez v0, :cond_5a

    #@57
    .line 1276
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@5a
    .line 1279
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    .end local v12           #x:I
    .end local v13           #y:I
    :cond_5a
    return-void

    #@5b
    .restart local v3       #oldX:I
    .restart local v4       #oldY:I
    .restart local v5       #range:I
    .restart local v11       #overscrollMode:I
    .restart local v12       #x:I
    .restart local v13       #y:I
    :cond_5b
    move v10, v6

    #@5c
    .line 1259
    goto :goto_2c

    #@5d
    .line 1269
    .restart local v10       #canOverscroll:Z
    :cond_5d
    if-le v12, v5, :cond_51

    #@5f
    if-gt v3, v5, :cond_51

    #@61
    .line 1270
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@63
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@65
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@68
    move-result v1

    #@69
    float-to-int v1, v1

    #@6a
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@6d
    goto :goto_51
.end method

.method protected computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I
    .registers 12
    .parameter "rect"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1329
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@4
    move-result v8

    #@5
    if-nez v8, :cond_9

    #@7
    move v5, v7

    #@8
    .line 1383
    :cond_8
    :goto_8
    return v5

    #@9
    .line 1331
    :cond_9
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@c
    move-result v6

    #@d
    .line 1332
    .local v6, width:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@10
    move-result v3

    #@11
    .line 1333
    .local v3, screenLeft:I
    add-int v4, v3, v6

    #@13
    .line 1335
    .local v4, screenRight:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    #@16
    move-result v1

    #@17
    .line 1338
    .local v1, fadingEdge:I
    iget v8, p1, Landroid/graphics/Rect;->left:I

    #@19
    if-lez v8, :cond_1c

    #@1b
    .line 1339
    add-int/2addr v3, v1

    #@1c
    .line 1343
    :cond_1c
    iget v8, p1, Landroid/graphics/Rect;->right:I

    #@1e
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    #@25
    move-result v9

    #@26
    if-ge v8, v9, :cond_29

    #@28
    .line 1344
    sub-int/2addr v4, v1

    #@29
    .line 1347
    :cond_29
    const/4 v5, 0x0

    #@2a
    .line 1349
    .local v5, scrollXDelta:I
    iget v8, p1, Landroid/graphics/Rect;->right:I

    #@2c
    if-le v8, v4, :cond_50

    #@2e
    iget v8, p1, Landroid/graphics/Rect;->left:I

    #@30
    if-le v8, v3, :cond_50

    #@32
    .line 1354
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@35
    move-result v8

    #@36
    if-le v8, v6, :cond_4b

    #@38
    .line 1356
    iget v8, p1, Landroid/graphics/Rect;->left:I

    #@3a
    sub-int/2addr v8, v3

    #@3b
    add-int/2addr v5, v8

    #@3c
    .line 1363
    :goto_3c
    invoke-virtual {p0, v7}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    #@43
    move-result v2

    #@44
    .line 1364
    .local v2, right:I
    sub-int v0, v2, v4

    #@46
    .line 1365
    .local v0, distanceToRight:I
    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    #@49
    move-result v5

    #@4a
    .line 1367
    goto :goto_8

    #@4b
    .line 1359
    .end local v0           #distanceToRight:I
    .end local v2           #right:I
    :cond_4b
    iget v8, p1, Landroid/graphics/Rect;->right:I

    #@4d
    sub-int/2addr v8, v4

    #@4e
    add-int/2addr v5, v8

    #@4f
    goto :goto_3c

    #@50
    .line 1367
    :cond_50
    iget v7, p1, Landroid/graphics/Rect;->left:I

    #@52
    if-ge v7, v3, :cond_8

    #@54
    iget v7, p1, Landroid/graphics/Rect;->right:I

    #@56
    if-ge v7, v4, :cond_8

    #@58
    .line 1372
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@5b
    move-result v7

    #@5c
    if-le v7, v6, :cond_6d

    #@5e
    .line 1374
    iget v7, p1, Landroid/graphics/Rect;->right:I

    #@60
    sub-int v7, v4, v7

    #@62
    sub-int/2addr v5, v7

    #@63
    .line 1381
    :goto_63
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@66
    move-result v7

    #@67
    neg-int v7, v7

    #@68
    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    #@6b
    move-result v5

    #@6c
    goto :goto_8

    #@6d
    .line 1377
    :cond_6d
    iget v7, p1, Landroid/graphics/Rect;->left:I

    #@6f
    sub-int v7, v3, v7

    #@71
    sub-int/2addr v5, v7

    #@72
    goto :goto_63
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 332
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0, p1}, Landroid/widget/HorizontalScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter "canvas"

    #@0
    .prologue
    .line 1562
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    #@3
    .line 1563
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@5
    if-eqz v4, :cond_8f

    #@7
    .line 1564
    iget v2, p0, Landroid/view/View;->mScrollX:I

    #@9
    .line 1565
    .local v2, scrollX:I
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@b
    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_4a

    #@11
    .line 1566
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@14
    move-result v1

    #@15
    .line 1567
    .local v1, restoreCount:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHeight()I

    #@18
    move-result v4

    #@19
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@1b
    sub-int/2addr v4, v5

    #@1c
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@1e
    sub-int v0, v4, v5

    #@20
    .line 1569
    .local v0, height:I
    const/high16 v4, 0x4387

    #@22
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    #@25
    .line 1570
    neg-int v4, v0

    #@26
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@28
    add-int/2addr v4, v5

    #@29
    int-to-float v4, v4

    #@2a
    const/4 v5, 0x0

    #@2b
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    #@2e
    move-result v5

    #@2f
    int-to-float v5, v5

    #@30
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@33
    .line 1571
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@35
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@38
    move-result v5

    #@39
    invoke-virtual {v4, v0, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@3c
    .line 1572
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@3e
    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_47

    #@44
    .line 1573
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@47
    .line 1575
    :cond_47
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@4a
    .line 1577
    .end local v0           #height:I
    .end local v1           #restoreCount:I
    :cond_4a
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@4c
    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@4f
    move-result v4

    #@50
    if-nez v4, :cond_8f

    #@52
    .line 1578
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@55
    move-result v1

    #@56
    .line 1579
    .restart local v1       #restoreCount:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@59
    move-result v3

    #@5a
    .line 1580
    .local v3, width:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHeight()I

    #@5d
    move-result v4

    #@5e
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@60
    sub-int/2addr v4, v5

    #@61
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@63
    sub-int v0, v4, v5

    #@65
    .line 1582
    .restart local v0       #height:I
    const/high16 v4, 0x42b4

    #@67
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    #@6a
    .line 1583
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@6c
    neg-int v4, v4

    #@6d
    int-to-float v4, v4

    #@6e
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@71
    move-result v5

    #@72
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    #@75
    move-result v5

    #@76
    add-int/2addr v5, v3

    #@77
    neg-int v5, v5

    #@78
    int-to-float v5, v5

    #@79
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@7c
    .line 1585
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@7e
    invoke-virtual {v4, v0, v3}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@81
    .line 1586
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@83
    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@86
    move-result v4

    #@87
    if-eqz v4, :cond_8c

    #@89
    .line 1587
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@8c
    .line 1589
    :cond_8c
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@8f
    .line 1592
    .end local v0           #height:I
    .end local v1           #restoreCount:I
    .end local v2           #scrollX:I
    .end local v3           #width:I
    :cond_8f
    return-void
.end method

.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v4, 0x11

    #@3
    const/16 v5, 0x42

    #@5
    .line 344
    iget-object v6, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {v6}, Landroid/graphics/Rect;->setEmpty()V

    #@a
    .line 346
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->canScroll()Z

    #@d
    move-result v6

    #@e
    if-nez v6, :cond_31

    #@10
    .line 347
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->isFocused()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_30

    #@16
    .line 348
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->findFocus()Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    .line 349
    .local v0, currentFocused:Landroid/view/View;
    if-ne v0, p0, :cond_1d

    #@1c
    const/4 v0, 0x0

    #@1d
    .line 350
    :cond_1d
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p0, v0, v5}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@24
    move-result-object v2

    #@25
    .line 352
    .local v2, nextFocused:Landroid/view/View;
    if-eqz v2, :cond_30

    #@27
    if-eq v2, p0, :cond_30

    #@29
    invoke-virtual {v2, v5}, Landroid/view/View;->requestFocus(I)Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_30

    #@2f
    const/4 v3, 0x1

    #@30
    .line 381
    .end local v0           #currentFocused:Landroid/view/View;
    .end local v2           #nextFocused:Landroid/view/View;
    :cond_30
    :goto_30
    return v3

    #@31
    .line 358
    :cond_31
    const/4 v1, 0x0

    #@32
    .line 359
    .local v1, handled:Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_3f

    #@38
    .line 360
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3b
    move-result v3

    #@3c
    sparse-switch v3, :sswitch_data_6e

    #@3f
    :cond_3f
    :goto_3f
    move v3, v1

    #@40
    .line 381
    goto :goto_30

    #@41
    .line 362
    :sswitch_41
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@44
    move-result v3

    #@45
    if-nez v3, :cond_4c

    #@47
    .line 363
    invoke-virtual {p0, v4}, Landroid/widget/HorizontalScrollView;->arrowScroll(I)Z

    #@4a
    move-result v1

    #@4b
    goto :goto_3f

    #@4c
    .line 365
    :cond_4c
    invoke-virtual {p0, v4}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    #@4f
    move-result v1

    #@50
    .line 367
    goto :goto_3f

    #@51
    .line 369
    :sswitch_51
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@54
    move-result v3

    #@55
    if-nez v3, :cond_5c

    #@57
    .line 370
    invoke-virtual {p0, v5}, Landroid/widget/HorizontalScrollView;->arrowScroll(I)Z

    #@5a
    move-result v1

    #@5b
    goto :goto_3f

    #@5c
    .line 372
    :cond_5c
    invoke-virtual {p0, v5}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    #@5f
    move-result v1

    #@60
    .line 374
    goto :goto_3f

    #@61
    .line 376
    :sswitch_61
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@64
    move-result v3

    #@65
    if-eqz v3, :cond_6c

    #@67
    move v3, v4

    #@68
    :goto_68
    invoke-virtual {p0, v3}, Landroid/widget/HorizontalScrollView;->pageScroll(I)Z

    #@6b
    goto :goto_3f

    #@6c
    :cond_6c
    move v3, v5

    #@6d
    goto :goto_68

    #@6e
    .line 360
    :sswitch_data_6e
    .sparse-switch
        0x15 -> :sswitch_41
        0x16 -> :sswitch_51
        0x3e -> :sswitch_61
    .end sparse-switch
.end method

.method public fling(I)V
    .registers 19
    .parameter "velocityX"

    #@0
    .prologue
    .line 1501
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v1

    #@4
    if-lez v1, :cond_64

    #@6
    .line 1502
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@9
    move-result v1

    #@a
    move-object/from16 v0, p0

    #@c
    iget v2, v0, Landroid/view/View;->mPaddingRight:I

    #@e
    sub-int/2addr v1, v2

    #@f
    move-object/from16 v0, p0

    #@11
    iget v2, v0, Landroid/view/View;->mPaddingLeft:I

    #@13
    sub-int v16, v1, v2

    #@15
    .line 1503
    .local v16, width:I
    const/4 v1, 0x0

    #@16
    move-object/from16 v0, p0

    #@18
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    #@1f
    move-result v15

    #@20
    .line 1505
    .local v15, right:I
    move-object/from16 v0, p0

    #@22
    iget-object v1, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@24
    move-object/from16 v0, p0

    #@26
    iget v2, v0, Landroid/view/View;->mScrollX:I

    #@28
    move-object/from16 v0, p0

    #@2a
    iget v3, v0, Landroid/view/View;->mScrollY:I

    #@2c
    const/4 v5, 0x0

    #@2d
    const/4 v6, 0x0

    #@2e
    const/4 v4, 0x0

    #@2f
    sub-int v7, v15, v16

    #@31
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    #@34
    move-result v7

    #@35
    const/4 v8, 0x0

    #@36
    const/4 v9, 0x0

    #@37
    div-int/lit8 v10, v16, 0x2

    #@39
    const/4 v11, 0x0

    #@3a
    move/from16 v4, p1

    #@3c
    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@3f
    .line 1508
    if-lez p1, :cond_65

    #@41
    const/4 v13, 0x1

    #@42
    .line 1510
    .local v13, movingRight:Z
    :goto_42
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->findFocus()Landroid/view/View;

    #@45
    move-result-object v12

    #@46
    .line 1511
    .local v12, currentFocused:Landroid/view/View;
    move-object/from16 v0, p0

    #@48
    iget-object v1, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@4a
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalX()I

    #@4d
    move-result v1

    #@4e
    move-object/from16 v0, p0

    #@50
    invoke-direct {v0, v13, v1, v12}, Landroid/widget/HorizontalScrollView;->findFocusableViewInMyBounds(ZILandroid/view/View;)Landroid/view/View;

    #@53
    move-result-object v14

    #@54
    .line 1514
    .local v14, newFocused:Landroid/view/View;
    if-nez v14, :cond_58

    #@56
    .line 1515
    move-object/from16 v14, p0

    #@58
    .line 1518
    :cond_58
    if-eq v14, v12, :cond_61

    #@5a
    .line 1519
    if-eqz v13, :cond_67

    #@5c
    const/16 v1, 0x42

    #@5e
    :goto_5e
    invoke-virtual {v14, v1}, Landroid/view/View;->requestFocus(I)Z

    #@61
    .line 1522
    :cond_61
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@64
    .line 1524
    .end local v12           #currentFocused:Landroid/view/View;
    .end local v13           #movingRight:Z
    .end local v14           #newFocused:Landroid/view/View;
    .end local v15           #right:I
    .end local v16           #width:I
    :cond_64
    return-void

    #@65
    .line 1508
    .restart local v15       #right:I
    .restart local v16       #width:I
    :cond_65
    const/4 v13, 0x0

    #@66
    goto :goto_42

    #@67
    .line 1519
    .restart local v12       #currentFocused:Landroid/view/View;
    .restart local v13       #movingRight:Z
    .restart local v14       #newFocused:Landroid/view/View;
    :cond_67
    const/16 v1, 0x11

    #@69
    goto :goto_5e
.end method

.method public fullScroll(I)Z
    .registers 8
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 985
    const/16 v5, 0x42

    #@3
    if-ne p1, v5, :cond_3c

    #@5
    const/4 v1, 0x1

    #@6
    .line 986
    .local v1, right:Z
    :goto_6
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@9
    move-result v3

    #@a
    .line 988
    .local v3, width:I
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@c
    iput v4, v5, Landroid/graphics/Rect;->left:I

    #@e
    .line 989
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@10
    iput v3, v5, Landroid/graphics/Rect;->right:I

    #@12
    .line 991
    if-eqz v1, :cond_2f

    #@14
    .line 992
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@17
    move-result v0

    #@18
    .line 993
    .local v0, count:I
    if-lez v0, :cond_2f

    #@1a
    .line 994
    invoke-virtual {p0, v4}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@1d
    move-result-object v2

    #@1e
    .line 995
    .local v2, view:Landroid/view/View;
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@20
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    #@23
    move-result v5

    #@24
    iput v5, v4, Landroid/graphics/Rect;->right:I

    #@26
    .line 996
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@28
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2a
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@2c
    sub-int/2addr v5, v3

    #@2d
    iput v5, v4, Landroid/graphics/Rect;->left:I

    #@2f
    .line 1000
    .end local v0           #count:I
    .end local v2           #view:Landroid/view/View;
    :cond_2f
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@31
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@33
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@35
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@37
    invoke-direct {p0, p1, v4, v5}, Landroid/widget/HorizontalScrollView;->scrollAndFocus(III)Z

    #@3a
    move-result v4

    #@3b
    return v4

    #@3c
    .end local v1           #right:Z
    .end local v3           #width:I
    :cond_3c
    move v1, v4

    #@3d
    .line 985
    goto :goto_6
.end method

.method protected getLeftFadingEdgeStrength()F
    .registers 4

    #@0
    .prologue
    .line 158
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    .line 159
    const/4 v1, 0x0

    #@7
    .line 167
    :goto_7
    return v1

    #@8
    .line 162
    :cond_8
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    #@b
    move-result v0

    #@c
    .line 163
    .local v0, length:I
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@e
    if-ge v1, v0, :cond_16

    #@10
    .line 164
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@12
    int-to-float v1, v1

    #@13
    int-to-float v2, v0

    #@14
    div-float/2addr v1, v2

    #@15
    goto :goto_7

    #@16
    .line 167
    :cond_16
    const/high16 v1, 0x3f80

    #@18
    goto :goto_7
.end method

.method public getMaxScrollAmount()I
    .registers 4

    #@0
    .prologue
    .line 191
    const/high16 v0, 0x3f00

    #@2
    iget v1, p0, Landroid/view/View;->mRight:I

    #@4
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@6
    sub-int/2addr v1, v2

    #@7
    int-to-float v1, v1

    #@8
    mul-float/2addr v0, v1

    #@9
    float-to-int v0, v0

    #@a
    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .registers 6

    #@0
    .prologue
    .line 172
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_8

    #@6
    .line 173
    const/4 v3, 0x0

    #@7
    .line 183
    :goto_7
    return v3

    #@8
    .line 176
    :cond_8
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    #@b
    move-result v0

    #@c
    .line 177
    .local v0, length:I
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@f
    move-result v3

    #@10
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@12
    sub-int v1, v3, v4

    #@14
    .line 178
    .local v1, rightEdge:I
    const/4 v3, 0x0

    #@15
    invoke-virtual {p0, v3}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    #@1c
    move-result v3

    #@1d
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@1f
    sub-int/2addr v3, v4

    #@20
    sub-int v2, v3, v1

    #@22
    .line 179
    .local v2, span:I
    if-ge v2, v0, :cond_28

    #@24
    .line 180
    int-to-float v3, v2

    #@25
    int-to-float v4, v0

    #@26
    div-float/2addr v3, v4

    #@27
    goto :goto_7

    #@28
    .line 183
    :cond_28
    const/high16 v3, 0x3f80

    #@2a
    goto :goto_7
.end method

.method public isFillViewport()Z
    .registers 2

    #@0
    .prologue
    .line 265
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mFillViewport:Z

    #@2
    return v0
.end method

.method public isSmoothScrollingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 288
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mSmoothScrollingEnabled:Z

    #@2
    return v0
.end method

.method protected measureChild(Landroid/view/View;II)V
    .registers 10
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "parentHeightMeasureSpec"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1205
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v2

    #@5
    .line 1210
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@7
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@9
    add-int/2addr v3, v4

    #@a
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@c
    invoke-static {p3, v3, v4}, Landroid/widget/HorizontalScrollView;->getChildMeasureSpec(III)I

    #@f
    move-result v0

    #@10
    .line 1213
    .local v0, childHeightMeasureSpec:I
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result v1

    #@14
    .line 1215
    .local v1, childWidthMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@17
    .line 1216
    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .registers 11
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "widthUsed"
    .parameter "parentHeightMeasureSpec"
    .parameter "heightUsed"

    #@0
    .prologue
    .line 1221
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v2

    #@4
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    #@6
    .line 1223
    .local v2, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@8
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@a
    add-int/2addr v3, v4

    #@b
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@d
    add-int/2addr v3, v4

    #@e
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@10
    add-int/2addr v3, v4

    #@11
    add-int/2addr v3, p5

    #@12
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@14
    invoke-static {p4, v3, v4}, Landroid/widget/HorizontalScrollView;->getChildMeasureSpec(III)I

    #@17
    move-result v0

    #@18
    .line 1226
    .local v0, childHeightMeasureSpec:I
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1a
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1c
    add-int/2addr v3, v4

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@21
    move-result v1

    #@22
    .line 1229
    .local v1, childWidthMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@25
    .line 1230
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 693
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3
    move-result v5

    #@4
    and-int/lit8 v5, v5, 0x2

    #@6
    if-eqz v5, :cond_f

    #@8
    .line 694
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@b
    move-result v5

    #@c
    packed-switch v5, :pswitch_data_52

    #@f
    .line 722
    :cond_f
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v5

    #@13
    :goto_13
    return v5

    #@14
    .line 696
    :pswitch_14
    iget-boolean v5, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@16
    if-nez v5, :cond_f

    #@18
    .line 698
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@1b
    move-result v5

    #@1c
    and-int/lit8 v5, v5, 0x1

    #@1e
    if-eqz v5, :cond_46

    #@20
    .line 699
    const/16 v5, 0x9

    #@22
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@25
    move-result v5

    #@26
    neg-float v1, v5

    #@27
    .line 703
    .local v1, hscroll:F
    :goto_27
    const/4 v5, 0x0

    #@28
    cmpl-float v5, v1, v5

    #@2a
    if-eqz v5, :cond_f

    #@2c
    .line 704
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHorizontalScrollFactor()F

    #@2f
    move-result v5

    #@30
    mul-float/2addr v5, v1

    #@31
    float-to-int v0, v5

    #@32
    .line 705
    .local v0, delta:I
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@35
    move-result v4

    #@36
    .line 706
    .local v4, range:I
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@38
    .line 707
    .local v3, oldScrollX:I
    add-int v2, v3, v0

    #@3a
    .line 708
    .local v2, newScrollX:I
    if-gez v2, :cond_4d

    #@3c
    .line 709
    const/4 v2, 0x0

    #@3d
    .line 713
    :cond_3d
    :goto_3d
    if-eq v2, v3, :cond_f

    #@3f
    .line 714
    iget v5, p0, Landroid/view/View;->mScrollY:I

    #@41
    invoke-super {p0, v2, v5}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@44
    .line 715
    const/4 v5, 0x1

    #@45
    goto :goto_13

    #@46
    .line 701
    .end local v0           #delta:I
    .end local v1           #hscroll:F
    .end local v2           #newScrollX:I
    .end local v3           #oldScrollX:I
    .end local v4           #range:I
    :cond_46
    const/16 v5, 0xa

    #@48
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@4b
    move-result v1

    #@4c
    .restart local v1       #hscroll:F
    goto :goto_27

    #@4d
    .line 710
    .restart local v0       #delta:I
    .restart local v2       #newScrollX:I
    .restart local v3       #oldScrollX:I
    .restart local v4       #range:I
    :cond_4d
    if-le v2, v4, :cond_3d

    #@4f
    .line 711
    move v2, v4

    #@50
    goto :goto_3d

    #@51
    .line 694
    nop

    #@52
    :pswitch_data_52
    .packed-switch 0x8
        :pswitch_14
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 798
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 799
    const-class v0, Landroid/widget/HorizontalScrollView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 800
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@f
    move-result v0

    #@10
    if-lez v0, :cond_2d

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@16
    .line 801
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@18
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    #@1b
    .line 802
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@1d
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    #@20
    .line 803
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@23
    move-result v0

    #@24
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    #@27
    .line 804
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@29
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    #@2c
    .line 805
    return-void

    #@2d
    .line 800
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_13
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 782
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 783
    const-class v1, Landroid/widget/HorizontalScrollView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 784
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@f
    move-result v0

    #@10
    .line 785
    .local v0, scrollRange:I
    if-lez v0, :cond_34

    #@12
    .line 786
    const/4 v1, 0x1

    #@13
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@16
    .line 787
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->isEnabled()Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_25

    #@1c
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@1e
    if-lez v1, :cond_25

    #@20
    .line 788
    const/16 v1, 0x2000

    #@22
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@25
    .line 790
    :cond_25
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->isEnabled()Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_34

    #@2b
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@2d
    if-ge v1, v0, :cond_34

    #@2f
    .line 791
    const/16 v1, 0x1000

    #@31
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@34
    .line 794
    :cond_34
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 438
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@6
    move-result v7

    #@7
    .line 439
    .local v7, action:I
    const/4 v1, 0x2

    #@8
    if-ne v7, v1, :cond_f

    #@a
    iget-boolean v1, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 531
    :goto_e
    return v0

    #@f
    .line 443
    :cond_f
    and-int/lit16 v1, v7, 0xff

    #@11
    packed-switch v1, :pswitch_data_da

    #@14
    .line 531
    :cond_14
    :goto_14
    :pswitch_14
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@16
    goto :goto_e

    #@17
    .line 454
    :pswitch_17
    iget v8, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@19
    .line 455
    .local v8, activePointerId:I
    if-eq v8, v2, :cond_14

    #@1b
    .line 460
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@1e
    move-result v10

    #@1f
    .line 461
    .local v10, pointerIndex:I
    if-ne v10, v2, :cond_40

    #@21
    .line 462
    const-string v0, "HorizontalScrollView"

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "Invalid pointerId="

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, " in onInterceptTouchEvent"

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_14

    #@40
    .line 467
    :cond_40
    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->getX(I)F

    #@43
    move-result v1

    #@44
    float-to-int v11, v1

    #@45
    .line 468
    .local v11, x:I
    iget v1, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@47
    sub-int v1, v11, v1

    #@49
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@4c
    move-result v12

    #@4d
    .line 469
    .local v12, xDiff:I
    iget v1, p0, Landroid/widget/HorizontalScrollView;->mTouchSlop:I

    #@4f
    if-le v12, v1, :cond_14

    #@51
    .line 470
    iput-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@53
    .line 471
    iput v11, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@55
    .line 472
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->initVelocityTrackerIfNotExists()V

    #@58
    .line 473
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5a
    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@5d
    .line 474
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@5f
    if-eqz v1, :cond_14

    #@61
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@63
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@66
    goto :goto_14

    #@67
    .line 480
    .end local v8           #activePointerId:I
    .end local v10           #pointerIndex:I
    .end local v11           #x:I
    .end local v12           #xDiff:I
    :pswitch_67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@6a
    move-result v1

    #@6b
    float-to-int v11, v1

    #@6c
    .line 481
    .restart local v11       #x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@6f
    move-result v1

    #@70
    float-to-int v1, v1

    #@71
    invoke-direct {p0, v11, v1}, Landroid/widget/HorizontalScrollView;->inChild(II)Z

    #@74
    move-result v1

    #@75
    if-nez v1, :cond_7d

    #@77
    .line 482
    iput-boolean v3, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@79
    .line 483
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->recycleVelocityTracker()V

    #@7c
    goto :goto_14

    #@7d
    .line 491
    :cond_7d
    iput v11, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@7f
    .line 492
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@82
    move-result v1

    #@83
    iput v1, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@85
    .line 494
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->initOrResetVelocityTracker()V

    #@88
    .line 495
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@8a
    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@8d
    .line 502
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@8f
    invoke-virtual {v1}, Landroid/widget/OverScroller;->isFinished()Z

    #@92
    move-result v1

    #@93
    if-nez v1, :cond_96

    #@95
    move v3, v0

    #@96
    :cond_96
    iput-boolean v3, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@98
    goto/16 :goto_14

    #@9a
    .line 509
    .end local v11           #x:I
    :pswitch_9a
    iput-boolean v3, p0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@9c
    .line 510
    iput v2, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@9e
    .line 511
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@a0
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@a2
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@a4
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@a7
    move-result v4

    #@a8
    move v5, v3

    #@a9
    move v6, v3

    #@aa
    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@ad
    move-result v0

    #@ae
    if-eqz v0, :cond_14

    #@b0
    .line 512
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@b3
    goto/16 :goto_14

    #@b5
    .line 516
    :pswitch_b5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@b8
    move-result v9

    #@b9
    .line 517
    .local v9, index:I
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    #@bc
    move-result v0

    #@bd
    float-to-int v0, v0

    #@be
    iput v0, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@c0
    .line 518
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@c3
    move-result v0

    #@c4
    iput v0, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@c6
    goto/16 :goto_14

    #@c8
    .line 522
    .end local v9           #index:I
    :pswitch_c8
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@cb
    .line 523
    iget v0, p0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@cd
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@d0
    move-result v0

    #@d1
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@d4
    move-result v0

    #@d5
    float-to-int v0, v0

    #@d6
    iput v0, p0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@d8
    goto/16 :goto_14

    #@da
    .line 443
    :pswitch_data_da
    .packed-switch 0x0
        :pswitch_67
        :pswitch_9a
        :pswitch_17
        :pswitch_9a
        :pswitch_14
        :pswitch_b5
        :pswitch_c8
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1451
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    #@3
    .line 1452
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mIsLayoutDirty:Z

    #@6
    .line 1454
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@8
    if-eqz v0, :cond_17

    #@a
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@c
    invoke-static {v0, p0}, Landroid/widget/HorizontalScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 1455
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@14
    invoke-direct {p0, v0}, Landroid/widget/HorizontalScrollView;->scrollToChild(Landroid/view/View;)V

    #@17
    .line 1457
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@1a
    .line 1460
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@1c
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@1e
    invoke-virtual {p0, v0, v1}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    #@21
    .line 1461
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 301
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@3
    .line 303
    iget-boolean v6, p0, Landroid/widget/HorizontalScrollView;->mFillViewport:Z

    #@5
    if-nez v6, :cond_8

    #@7
    .line 327
    :cond_7
    :goto_7
    return-void

    #@8
    .line 307
    :cond_8
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v5

    #@c
    .line 308
    .local v5, widthMode:I
    if-eqz v5, :cond_7

    #@e
    .line 312
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@11
    move-result v6

    #@12
    if-lez v6, :cond_7

    #@14
    .line 313
    const/4 v6, 0x0

    #@15
    invoke-virtual {p0, v6}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    .line 314
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    #@1c
    move-result v4

    #@1d
    .line 315
    .local v4, width:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@20
    move-result v6

    #@21
    if-ge v6, v4, :cond_7

    #@23
    .line 316
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    #@29
    .line 318
    .local v3, lp:Landroid/widget/FrameLayout$LayoutParams;
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@2b
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@2d
    add-int/2addr v6, v7

    #@2e
    iget v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@30
    invoke-static {p2, v6, v7}, Landroid/widget/HorizontalScrollView;->getChildMeasureSpec(III)I

    #@33
    move-result v1

    #@34
    .line 320
    .local v1, childHeightMeasureSpec:I
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@36
    sub-int/2addr v4, v6

    #@37
    .line 321
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@39
    sub-int/2addr v4, v6

    #@3a
    .line 322
    const/high16 v6, 0x4000

    #@3c
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3f
    move-result v2

    #@40
    .line 324
    .local v2, childWidthMeasureSpec:I
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    #@43
    goto :goto_7
.end method

.method protected onOverScrolled(IIZZ)V
    .registers 12
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 734
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@3
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_25

    #@9
    .line 735
    iput p1, p0, Landroid/view/View;->mScrollX:I

    #@b
    .line 736
    iput p2, p0, Landroid/view/View;->mScrollY:I

    #@d
    .line 737
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->invalidateParentIfNeeded()V

    #@10
    .line 738
    if-eqz p3, :cond_21

    #@12
    .line 739
    iget-object v0, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@14
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@16
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@18
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@1b
    move-result v4

    #@1c
    move v5, v3

    #@1d
    move v6, v3

    #@1e
    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@21
    .line 745
    :cond_21
    :goto_21
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->awakenScrollBars()Z

    #@24
    .line 746
    return-void

    #@25
    .line 742
    :cond_25
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@28
    goto :goto_21
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 7
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1411
    const/4 v2, 0x2

    #@2
    if-ne p1, v2, :cond_14

    #@4
    .line 1412
    const/16 p1, 0x42

    #@6
    .line 1417
    :cond_6
    :goto_6
    if-nez p2, :cond_1a

    #@8
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@b
    move-result-object v2

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v2, p0, v3, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 1422
    .local v0, nextFocus:Landroid/view/View;
    :goto_11
    if-nez v0, :cond_23

    #@13
    .line 1430
    :cond_13
    :goto_13
    return v1

    #@14
    .line 1413
    .end local v0           #nextFocus:Landroid/view/View;
    :cond_14
    const/4 v2, 0x1

    #@15
    if-ne p1, v2, :cond_6

    #@17
    .line 1414
    const/16 p1, 0x11

    #@19
    goto :goto_6

    #@1a
    .line 1417
    :cond_1a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    goto :goto_11

    #@23
    .line 1426
    .restart local v0       #nextFocus:Landroid/view/View;
    :cond_23
    invoke-direct {p0, v0}, Landroid/widget/HorizontalScrollView;->isOffScreen(Landroid/view/View;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_13

    #@29
    .line 1430
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@2c
    move-result v1

    #@2d
    goto :goto_13
.end method

.method protected onSizeChanged(IIII)V
    .registers 10
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 1465
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    #@3
    .line 1467
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->findFocus()Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    .line 1468
    .local v0, currentFocused:Landroid/view/View;
    if-eqz v0, :cond_b

    #@9
    if-ne p0, v0, :cond_c

    #@b
    .line 1479
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1471
    :cond_c
    iget v3, p0, Landroid/view/View;->mRight:I

    #@e
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@10
    sub-int v1, v3, v4

    #@12
    .line 1473
    .local v1, maxJump:I
    invoke-direct {p0, v0, v1}, Landroid/widget/HorizontalScrollView;->isWithinDeltaOfScreen(Landroid/view/View;I)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_b

    #@18
    .line 1474
    iget-object v3, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@1a
    invoke-virtual {v0, v3}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@1d
    .line 1475
    iget-object v3, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@1f
    invoke-virtual {p0, v0, v3}, Landroid/widget/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@22
    .line 1476
    iget-object v3, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@24
    invoke-virtual {p0, v3}, Landroid/widget/HorizontalScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    #@27
    move-result v2

    #@28
    .line 1477
    .local v2, scrollDelta:I
    invoke-direct {p0, v2}, Landroid/widget/HorizontalScrollView;->doScrollX(I)V

    #@2b
    goto :goto_b
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 29
    .parameter "ev"

    #@0
    .prologue
    .line 536
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->initVelocityTrackerIfNotExists()V

    #@3
    .line 537
    move-object/from16 v0, p0

    #@5
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@7
    move-object/from16 v0, p1

    #@9
    invoke-virtual {v3, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@c
    .line 539
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v16

    #@10
    .line 541
    .local v16, action:I
    move/from16 v0, v16

    #@12
    and-int/lit16 v3, v0, 0xff

    #@14
    packed-switch v3, :pswitch_data_268

    #@17
    .line 671
    :cond_17
    :goto_17
    :pswitch_17
    const/4 v3, 0x1

    #@18
    :goto_18
    return v3

    #@19
    .line 543
    :pswitch_19
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_21

    #@1f
    .line 544
    const/4 v3, 0x0

    #@20
    goto :goto_18

    #@21
    .line 546
    :cond_21
    move-object/from16 v0, p0

    #@23
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@25
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@28
    move-result v3

    #@29
    if-nez v3, :cond_64

    #@2b
    const/4 v3, 0x1

    #@2c
    :goto_2c
    move-object/from16 v0, p0

    #@2e
    iput-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@30
    if-eqz v3, :cond_3e

    #@32
    .line 547
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    #@35
    move-result-object v23

    #@36
    .line 548
    .local v23, parent:Landroid/view/ViewParent;
    if-eqz v23, :cond_3e

    #@38
    .line 549
    const/4 v3, 0x1

    #@39
    move-object/from16 v0, v23

    #@3b
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@3e
    .line 557
    .end local v23           #parent:Landroid/view/ViewParent;
    :cond_3e
    move-object/from16 v0, p0

    #@40
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@42
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_4f

    #@48
    .line 558
    move-object/from16 v0, p0

    #@4a
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@4c
    invoke-virtual {v3}, Landroid/widget/OverScroller;->abortAnimation()V

    #@4f
    .line 562
    :cond_4f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@52
    move-result v3

    #@53
    float-to-int v3, v3

    #@54
    move-object/from16 v0, p0

    #@56
    iput v3, v0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@58
    .line 563
    const/4 v3, 0x0

    #@59
    move-object/from16 v0, p1

    #@5b
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@5e
    move-result v3

    #@5f
    move-object/from16 v0, p0

    #@61
    iput v3, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@63
    goto :goto_17

    #@64
    .line 546
    :cond_64
    const/4 v3, 0x0

    #@65
    goto :goto_2c

    #@66
    .line 567
    :pswitch_66
    move-object/from16 v0, p0

    #@68
    iget v3, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@6a
    move-object/from16 v0, p1

    #@6c
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@6f
    move-result v17

    #@70
    .line 568
    .local v17, activePointerIndex:I
    const/4 v3, -0x1

    #@71
    move/from16 v0, v17

    #@73
    if-ne v0, v3, :cond_98

    #@75
    .line 569
    const-string v3, "HorizontalScrollView"

    #@77
    new-instance v5, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v6, "Invalid pointerId="

    #@7e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    move-object/from16 v0, p0

    #@84
    iget v6, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@86
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v5

    #@8a
    const-string v6, " in onTouchEvent"

    #@8c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v5

    #@90
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v5

    #@94
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_17

    #@98
    .line 573
    :cond_98
    move-object/from16 v0, p1

    #@9a
    move/from16 v1, v17

    #@9c
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@9f
    move-result v3

    #@a0
    float-to-int v0, v3

    #@a1
    move/from16 v26, v0

    #@a3
    .line 574
    .local v26, x:I
    move-object/from16 v0, p0

    #@a5
    iget v3, v0, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@a7
    sub-int v4, v3, v26

    #@a9
    .line 575
    .local v4, deltaX:I
    move-object/from16 v0, p0

    #@ab
    iget-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@ad
    if-nez v3, :cond_d1

    #@af
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@b2
    move-result v3

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget v5, v0, Landroid/widget/HorizontalScrollView;->mTouchSlop:I

    #@b7
    if-le v3, v5, :cond_d1

    #@b9
    .line 576
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    #@bc
    move-result-object v23

    #@bd
    .line 577
    .restart local v23       #parent:Landroid/view/ViewParent;
    if-eqz v23, :cond_c5

    #@bf
    .line 578
    const/4 v3, 0x1

    #@c0
    move-object/from16 v0, v23

    #@c2
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@c5
    .line 580
    :cond_c5
    const/4 v3, 0x1

    #@c6
    move-object/from16 v0, p0

    #@c8
    iput-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@ca
    .line 581
    if-lez v4, :cond_16d

    #@cc
    .line 582
    move-object/from16 v0, p0

    #@ce
    iget v3, v0, Landroid/widget/HorizontalScrollView;->mTouchSlop:I

    #@d0
    sub-int/2addr v4, v3

    #@d1
    .line 587
    .end local v23           #parent:Landroid/view/ViewParent;
    :cond_d1
    :goto_d1
    move-object/from16 v0, p0

    #@d3
    iget-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@d5
    if-eqz v3, :cond_17

    #@d7
    .line 589
    move/from16 v0, v26

    #@d9
    move-object/from16 v1, p0

    #@db
    iput v0, v1, Landroid/widget/HorizontalScrollView;->mLastMotionX:I

    #@dd
    .line 591
    move-object/from16 v0, p0

    #@df
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@e1
    move/from16 v20, v0

    #@e3
    .line 592
    .local v20, oldX:I
    move-object/from16 v0, p0

    #@e5
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@e7
    move/from16 v21, v0

    #@e9
    .line 593
    .local v21, oldY:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@ec
    move-result v8

    #@ed
    .line 594
    .local v8, range:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getOverScrollMode()I

    #@f0
    move-result v22

    #@f1
    .line 595
    .local v22, overscrollMode:I
    if-eqz v22, :cond_fa

    #@f3
    const/4 v3, 0x1

    #@f4
    move/from16 v0, v22

    #@f6
    if-ne v0, v3, :cond_174

    #@f8
    if-lez v8, :cond_174

    #@fa
    :cond_fa
    const/16 v18, 0x1

    #@fc
    .line 598
    .local v18, canOverscroll:Z
    :goto_fc
    const/4 v5, 0x0

    #@fd
    move-object/from16 v0, p0

    #@ff
    iget v6, v0, Landroid/view/View;->mScrollX:I

    #@101
    const/4 v7, 0x0

    #@102
    const/4 v9, 0x0

    #@103
    move-object/from16 v0, p0

    #@105
    iget v10, v0, Landroid/widget/HorizontalScrollView;->mOverscrollDistance:I

    #@107
    const/4 v11, 0x0

    #@108
    const/4 v12, 0x1

    #@109
    move-object/from16 v3, p0

    #@10b
    invoke-virtual/range {v3 .. v12}, Landroid/widget/HorizontalScrollView;->overScrollBy(IIIIIIIIZ)Z

    #@10e
    move-result v3

    #@10f
    if-eqz v3, :cond_118

    #@111
    .line 601
    move-object/from16 v0, p0

    #@113
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@115
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@118
    .line 603
    :cond_118
    move-object/from16 v0, p0

    #@11a
    iget v3, v0, Landroid/view/View;->mScrollX:I

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget v5, v0, Landroid/view/View;->mScrollY:I

    #@120
    move-object/from16 v0, p0

    #@122
    move/from16 v1, v20

    #@124
    move/from16 v2, v21

    #@126
    invoke-virtual {v0, v3, v5, v1, v2}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    #@129
    .line 605
    if-eqz v18, :cond_17

    #@12b
    .line 606
    add-int v24, v20, v4

    #@12d
    .line 607
    .local v24, pulledToX:I
    if-gez v24, :cond_177

    #@12f
    .line 608
    move-object/from16 v0, p0

    #@131
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@133
    int-to-float v5, v4

    #@134
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@137
    move-result v6

    #@138
    int-to-float v6, v6

    #@139
    div-float/2addr v5, v6

    #@13a
    invoke-virtual {v3, v5}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@13d
    .line 609
    move-object/from16 v0, p0

    #@13f
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@141
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@144
    move-result v3

    #@145
    if-nez v3, :cond_14e

    #@147
    .line 610
    move-object/from16 v0, p0

    #@149
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@14b
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@14e
    .line 618
    :cond_14e
    :goto_14e
    move-object/from16 v0, p0

    #@150
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@152
    if-eqz v3, :cond_17

    #@154
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@158
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@15b
    move-result v3

    #@15c
    if-eqz v3, :cond_168

    #@15e
    move-object/from16 v0, p0

    #@160
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@162
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@165
    move-result v3

    #@166
    if-nez v3, :cond_17

    #@168
    .line 620
    :cond_168
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@16b
    goto/16 :goto_17

    #@16d
    .line 584
    .end local v8           #range:I
    .end local v18           #canOverscroll:Z
    .end local v20           #oldX:I
    .end local v21           #oldY:I
    .end local v22           #overscrollMode:I
    .end local v24           #pulledToX:I
    .restart local v23       #parent:Landroid/view/ViewParent;
    :cond_16d
    move-object/from16 v0, p0

    #@16f
    iget v3, v0, Landroid/widget/HorizontalScrollView;->mTouchSlop:I

    #@171
    add-int/2addr v4, v3

    #@172
    goto/16 :goto_d1

    #@174
    .line 595
    .end local v23           #parent:Landroid/view/ViewParent;
    .restart local v8       #range:I
    .restart local v20       #oldX:I
    .restart local v21       #oldY:I
    .restart local v22       #overscrollMode:I
    :cond_174
    const/16 v18, 0x0

    #@176
    goto :goto_fc

    #@177
    .line 612
    .restart local v18       #canOverscroll:Z
    .restart local v24       #pulledToX:I
    :cond_177
    move/from16 v0, v24

    #@179
    if-le v0, v8, :cond_14e

    #@17b
    .line 613
    move-object/from16 v0, p0

    #@17d
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@17f
    int-to-float v5, v4

    #@180
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@183
    move-result v6

    #@184
    int-to-float v6, v6

    #@185
    div-float/2addr v5, v6

    #@186
    invoke-virtual {v3, v5}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@189
    .line 614
    move-object/from16 v0, p0

    #@18b
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@18d
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@190
    move-result v3

    #@191
    if-nez v3, :cond_14e

    #@193
    .line 615
    move-object/from16 v0, p0

    #@195
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@197
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@19a
    goto :goto_14e

    #@19b
    .line 626
    .end local v4           #deltaX:I
    .end local v8           #range:I
    .end local v17           #activePointerIndex:I
    .end local v18           #canOverscroll:Z
    .end local v20           #oldX:I
    .end local v21           #oldY:I
    .end local v22           #overscrollMode:I
    .end local v24           #pulledToX:I
    .end local v26           #x:I
    :pswitch_19b
    move-object/from16 v0, p0

    #@19d
    iget-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@19f
    if-eqz v3, :cond_17

    #@1a1
    .line 627
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Landroid/widget/HorizontalScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1a5
    move-object/from16 v25, v0

    #@1a7
    .line 628
    .local v25, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v3, 0x3e8

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    iget v5, v0, Landroid/widget/HorizontalScrollView;->mMaximumVelocity:I

    #@1ad
    int-to-float v5, v5

    #@1ae
    move-object/from16 v0, v25

    #@1b0
    invoke-virtual {v0, v3, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@1b3
    .line 629
    move-object/from16 v0, p0

    #@1b5
    iget v3, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@1b7
    move-object/from16 v0, v25

    #@1b9
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@1bc
    move-result v3

    #@1bd
    float-to-int v0, v3

    #@1be
    move/from16 v19, v0

    #@1c0
    .line 631
    .local v19, initialVelocity:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@1c3
    move-result v3

    #@1c4
    if-lez v3, :cond_1d8

    #@1c6
    .line 632
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(I)I

    #@1c9
    move-result v3

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget v5, v0, Landroid/widget/HorizontalScrollView;->mMinimumVelocity:I

    #@1ce
    if-le v3, v5, :cond_1fb

    #@1d0
    .line 633
    move/from16 v0, v19

    #@1d2
    neg-int v3, v0

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    invoke-virtual {v0, v3}, Landroid/widget/HorizontalScrollView;->fling(I)V

    #@1d8
    .line 642
    :cond_1d8
    :goto_1d8
    const/4 v3, -0x1

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iput v3, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@1dd
    .line 643
    const/4 v3, 0x0

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iput-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@1e2
    .line 644
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->recycleVelocityTracker()V

    #@1e5
    .line 646
    move-object/from16 v0, p0

    #@1e7
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@1e9
    if-eqz v3, :cond_17

    #@1eb
    .line 647
    move-object/from16 v0, p0

    #@1ed
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@1ef
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@1f2
    .line 648
    move-object/from16 v0, p0

    #@1f4
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@1f6
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@1f9
    goto/16 :goto_17

    #@1fb
    .line 635
    :cond_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v9, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget v10, v0, Landroid/view/View;->mScrollX:I

    #@203
    move-object/from16 v0, p0

    #@205
    iget v11, v0, Landroid/view/View;->mScrollY:I

    #@207
    const/4 v12, 0x0

    #@208
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@20b
    move-result v13

    #@20c
    const/4 v14, 0x0

    #@20d
    const/4 v15, 0x0

    #@20e
    invoke-virtual/range {v9 .. v15}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@211
    move-result v3

    #@212
    if-eqz v3, :cond_1d8

    #@214
    .line 637
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@217
    goto :goto_1d8

    #@218
    .line 653
    .end local v19           #initialVelocity:I
    .end local v25           #velocityTracker:Landroid/view/VelocityTracker;
    :pswitch_218
    move-object/from16 v0, p0

    #@21a
    iget-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@21c
    if-eqz v3, :cond_17

    #@21e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@221
    move-result v3

    #@222
    if-lez v3, :cond_17

    #@224
    .line 654
    move-object/from16 v0, p0

    #@226
    iget-object v9, v0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@228
    move-object/from16 v0, p0

    #@22a
    iget v10, v0, Landroid/view/View;->mScrollX:I

    #@22c
    move-object/from16 v0, p0

    #@22e
    iget v11, v0, Landroid/view/View;->mScrollY:I

    #@230
    const/4 v12, 0x0

    #@231
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@234
    move-result v13

    #@235
    const/4 v14, 0x0

    #@236
    const/4 v15, 0x0

    #@237
    invoke-virtual/range {v9 .. v15}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@23a
    move-result v3

    #@23b
    if-eqz v3, :cond_240

    #@23d
    .line 655
    invoke-virtual/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@240
    .line 657
    :cond_240
    const/4 v3, -0x1

    #@241
    move-object/from16 v0, p0

    #@243
    iput v3, v0, Landroid/widget/HorizontalScrollView;->mActivePointerId:I

    #@245
    .line 658
    const/4 v3, 0x0

    #@246
    move-object/from16 v0, p0

    #@248
    iput-boolean v3, v0, Landroid/widget/HorizontalScrollView;->mIsBeingDragged:Z

    #@24a
    .line 659
    invoke-direct/range {p0 .. p0}, Landroid/widget/HorizontalScrollView;->recycleVelocityTracker()V

    #@24d
    .line 661
    move-object/from16 v0, p0

    #@24f
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@251
    if-eqz v3, :cond_17

    #@253
    .line 662
    move-object/from16 v0, p0

    #@255
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@257
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@25a
    .line 663
    move-object/from16 v0, p0

    #@25c
    iget-object v3, v0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@25e
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@261
    goto/16 :goto_17

    #@263
    .line 668
    :pswitch_263
    invoke-direct/range {p0 .. p1}, Landroid/widget/HorizontalScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@266
    goto/16 :goto_17

    #@268
    .line 541
    :pswitch_data_268
    .packed-switch 0x0
        :pswitch_19
        :pswitch_19b
        :pswitch_66
        :pswitch_218
        :pswitch_17
        :pswitch_17
        :pswitch_263
    .end packed-switch
.end method

.method public pageScroll(I)Z
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 949
    const/16 v5, 0x42

    #@3
    if-ne p1, v5, :cond_49

    #@5
    const/4 v1, 0x1

    #@6
    .line 950
    .local v1, right:Z
    :goto_6
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@9
    move-result v3

    #@a
    .line 952
    .local v3, width:I
    if-eqz v1, :cond_4b

    #@c
    .line 953
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@e
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@11
    move-result v6

    #@12
    add-int/2addr v6, v3

    #@13
    iput v6, v5, Landroid/graphics/Rect;->left:I

    #@15
    .line 954
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@18
    move-result v0

    #@19
    .line 955
    .local v0, count:I
    if-lez v0, :cond_33

    #@1b
    .line 956
    invoke-virtual {p0, v4}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v2

    #@1f
    .line 957
    .local v2, view:Landroid/view/View;
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@21
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@23
    add-int/2addr v4, v3

    #@24
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    #@27
    move-result v5

    #@28
    if-le v4, v5, :cond_33

    #@2a
    .line 958
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@2c
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    #@2f
    move-result v5

    #@30
    sub-int/2addr v5, v3

    #@31
    iput v5, v4, Landroid/graphics/Rect;->left:I

    #@33
    .line 967
    .end local v0           #count:I
    .end local v2           #view:Landroid/view/View;
    :cond_33
    :goto_33
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@35
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@37
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@39
    add-int/2addr v5, v3

    #@3a
    iput v5, v4, Landroid/graphics/Rect;->right:I

    #@3c
    .line 969
    iget-object v4, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@3e
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@40
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@42
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@44
    invoke-direct {p0, p1, v4, v5}, Landroid/widget/HorizontalScrollView;->scrollAndFocus(III)Z

    #@47
    move-result v4

    #@48
    return v4

    #@49
    .end local v1           #right:Z
    .end local v3           #width:I
    :cond_49
    move v1, v4

    #@4a
    .line 949
    goto :goto_6

    #@4b
    .line 962
    .restart local v1       #right:Z
    .restart local v3       #width:I
    :cond_4b
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@4d
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    #@50
    move-result v6

    #@51
    sub-int/2addr v6, v3

    #@52
    iput v6, v5, Landroid/graphics/Rect;->left:I

    #@54
    .line 963
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@56
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@58
    if-gez v5, :cond_33

    #@5a
    .line 964
    iget-object v5, p0, Landroid/widget/HorizontalScrollView;->mTempRect:Landroid/graphics/Rect;

    #@5c
    iput v4, v5, Landroid/graphics/Rect;->left:I

    #@5e
    goto :goto_33
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 9
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 750
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_9

    #@8
    .line 777
    :goto_8
    return v2

    #@9
    .line 753
    :cond_9
    sparse-switch p1, :sswitch_data_5a

    #@c
    move v2, v3

    #@d
    .line 777
    goto :goto_8

    #@e
    .line 755
    :sswitch_e
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->isEnabled()Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_16

    #@14
    move v2, v3

    #@15
    .line 756
    goto :goto_8

    #@16
    .line 758
    :cond_16
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@19
    move-result v4

    #@1a
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@1c
    sub-int/2addr v4, v5

    #@1d
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@1f
    sub-int v1, v4, v5

    #@21
    .line 759
    .local v1, viewportWidth:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@23
    add-int/2addr v4, v1

    #@24
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->getScrollRange()I

    #@27
    move-result v5

    #@28
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result v0

    #@2c
    .line 760
    .local v0, targetScrollX:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@2e
    if-eq v0, v4, :cond_34

    #@30
    .line 761
    invoke-virtual {p0, v0, v3}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    #@33
    goto :goto_8

    #@34
    :cond_34
    move v2, v3

    #@35
    .line 764
    goto :goto_8

    #@36
    .line 766
    .end local v0           #targetScrollX:I
    .end local v1           #viewportWidth:I
    :sswitch_36
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->isEnabled()Z

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_3e

    #@3c
    move v2, v3

    #@3d
    .line 767
    goto :goto_8

    #@3e
    .line 769
    :cond_3e
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@41
    move-result v4

    #@42
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@44
    sub-int/2addr v4, v5

    #@45
    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    #@47
    sub-int v1, v4, v5

    #@49
    .line 770
    .restart local v1       #viewportWidth:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@4b
    sub-int/2addr v4, v1

    #@4c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@4f
    move-result v0

    #@50
    .line 771
    .restart local v0       #targetScrollX:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@52
    if-eq v0, v4, :cond_58

    #@54
    .line 772
    invoke-virtual {p0, v0, v3}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    #@57
    goto :goto_8

    #@58
    :cond_58
    move v2, v3

    #@59
    .line 775
    goto :goto_8

    #@5a
    .line 753
    :sswitch_data_5a
    .sparse-switch
        0x1000 -> :sswitch_e
        0x2000 -> :sswitch_36
    .end sparse-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 1388
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mIsLayoutDirty:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1389
    invoke-direct {p0, p2}, Landroid/widget/HorizontalScrollView;->scrollToChild(Landroid/view/View;)V

    #@7
    .line 1394
    :goto_7
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@a
    .line 1395
    return-void

    #@b
    .line 1392
    :cond_b
    iput-object p2, p0, Landroid/widget/HorizontalScrollView;->mChildToScrollTo:Landroid/view/View;

    #@d
    goto :goto_7
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 7
    .parameter "child"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 1437
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    #@10
    move-result v2

    #@11
    sub-int/2addr v1, v2

    #@12
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    #@15
    .line 1440
    invoke-direct {p0, p2, p3}, Landroid/widget/HorizontalScrollView;->scrollToChildRect(Landroid/graphics/Rect;Z)Z

    #@18
    move-result v0

    #@19
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "disallowIntercept"

    #@0
    .prologue
    .line 419
    if-eqz p1, :cond_5

    #@2
    .line 420
    invoke-direct {p0}, Landroid/widget/HorizontalScrollView;->recycleVelocityTracker()V

    #@5
    .line 422
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    #@8
    .line 423
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 1445
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mIsLayoutDirty:Z

    #@3
    .line 1446
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    #@6
    .line 1447
    return-void
.end method

.method public scrollTo(II)V
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1534
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@3
    move-result v1

    #@4
    if-lez v1, :cond_3a

    #@6
    .line 1535
    const/4 v1, 0x0

    #@7
    invoke-virtual {p0, v1}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 1536
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@e
    move-result v1

    #@f
    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    #@11
    sub-int/2addr v1, v2

    #@12
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@14
    sub-int/2addr v1, v2

    #@15
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@18
    move-result v2

    #@19
    invoke-static {p1, v1, v2}, Landroid/widget/HorizontalScrollView;->clamp(III)I

    #@1c
    move-result p1

    #@1d
    .line 1537
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getHeight()I

    #@20
    move-result v1

    #@21
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@23
    sub-int/2addr v1, v2

    #@24
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@26
    sub-int/2addr v1, v2

    #@27
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@2a
    move-result v2

    #@2b
    invoke-static {p2, v1, v2}, Landroid/widget/HorizontalScrollView;->clamp(III)I

    #@2e
    move-result p2

    #@2f
    .line 1538
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@31
    if-ne p1, v1, :cond_37

    #@33
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@35
    if-eq p2, v1, :cond_3a

    #@37
    .line 1539
    :cond_37
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    #@3a
    .line 1542
    .end local v0           #child:Landroid/view/View;
    :cond_3a
    return-void
.end method

.method public setFillViewport(Z)V
    .registers 3
    .parameter "fillViewport"

    #@0
    .prologue
    .line 278
    iget-boolean v0, p0, Landroid/widget/HorizontalScrollView;->mFillViewport:Z

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 279
    iput-boolean p1, p0, Landroid/widget/HorizontalScrollView;->mFillViewport:Z

    #@6
    .line 280
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->requestLayout()V

    #@9
    .line 282
    :cond_9
    return-void
.end method

.method public setOverScrollMode(I)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1546
    const/4 v1, 0x2

    #@2
    if-eq p1, v1, :cond_1e

    #@4
    .line 1547
    iget-object v1, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@6
    if-nez v1, :cond_1a

    #@8
    .line 1548
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    .line 1549
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/widget/EdgeEffect;

    #@e
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@11
    iput-object v1, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@13
    .line 1550
    new-instance v1, Landroid/widget/EdgeEffect;

    #@15
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@18
    iput-object v1, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@1a
    .line 1556
    .end local v0           #context:Landroid/content/Context;
    :cond_1a
    :goto_1a
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOverScrollMode(I)V

    #@1d
    .line 1557
    return-void

    #@1e
    .line 1553
    :cond_1e
    iput-object v2, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@20
    .line 1554
    iput-object v2, p0, Landroid/widget/HorizontalScrollView;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@22
    goto :goto_1a
.end method

.method public setSmoothScrollingEnabled(Z)V
    .registers 2
    .parameter "smoothScrollingEnabled"

    #@0
    .prologue
    .line 296
    iput-boolean p1, p0, Landroid/widget/HorizontalScrollView;->mSmoothScrollingEnabled:Z

    #@2
    .line 297
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 727
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public final smoothScrollBy(II)V
    .registers 14
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 1141
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    #@4
    move-result v6

    #@5
    if-nez v6, :cond_8

    #@7
    .line 1162
    :goto_7
    return-void

    #@8
    .line 1145
    :cond_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@b
    move-result-wide v6

    #@c
    iget-wide v8, p0, Landroid/widget/HorizontalScrollView;->mLastScroll:J

    #@e
    sub-long v0, v6, v8

    #@10
    .line 1146
    .local v0, duration:J
    const-wide/16 v6, 0xfa

    #@12
    cmp-long v6, v0, v6

    #@14
    if-lez v6, :cond_4e

    #@16
    .line 1147
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    #@19
    move-result v6

    #@1a
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@1c
    sub-int/2addr v6, v7

    #@1d
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    #@1f
    sub-int v5, v6, v7

    #@21
    .line 1148
    .local v5, width:I
    invoke-virtual {p0, v10}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    #@28
    move-result v3

    #@29
    .line 1149
    .local v3, right:I
    sub-int v6, v3, v5

    #@2b
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    #@2e
    move-result v2

    #@2f
    .line 1150
    .local v2, maxX:I
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@31
    .line 1151
    .local v4, scrollX:I
    add-int v6, v4, p1

    #@33
    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    #@36
    move-result v6

    #@37
    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    #@3a
    move-result v6

    #@3b
    sub-int p1, v6, v4

    #@3d
    .line 1153
    iget-object v6, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@3f
    iget v7, p0, Landroid/view/View;->mScrollY:I

    #@41
    invoke-virtual {v6, v4, v7, p1, v10}, Landroid/widget/OverScroller;->startScroll(IIII)V

    #@44
    .line 1154
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->postInvalidateOnAnimation()V

    #@47
    .line 1161
    .end local v2           #maxX:I
    .end local v3           #right:I
    .end local v4           #scrollX:I
    .end local v5           #width:I
    :goto_47
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@4a
    move-result-wide v6

    #@4b
    iput-wide v6, p0, Landroid/widget/HorizontalScrollView;->mLastScroll:J

    #@4d
    goto :goto_7

    #@4e
    .line 1156
    :cond_4e
    iget-object v6, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@50
    invoke-virtual {v6}, Landroid/widget/OverScroller;->isFinished()Z

    #@53
    move-result v6

    #@54
    if-nez v6, :cond_5b

    #@56
    .line 1157
    iget-object v6, p0, Landroid/widget/HorizontalScrollView;->mScroller:Landroid/widget/OverScroller;

    #@58
    invoke-virtual {v6}, Landroid/widget/OverScroller;->abortAnimation()V

    #@5b
    .line 1159
    :cond_5b
    invoke-virtual {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    #@5e
    goto :goto_47
.end method

.method public final smoothScrollTo(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1171
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    sub-int v0, p1, v0

    #@4
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@6
    sub-int v1, p2, v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    #@b
    .line 1172
    return-void
.end method
