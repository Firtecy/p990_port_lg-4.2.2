.class Landroid/widget/CalendarView$1;
.super Landroid/database/DataSetObserver;
.source "CalendarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/CalendarView;->setUpAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/CalendarView;


# direct methods
.method constructor <init>(Landroid/widget/CalendarView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1008
    iput-object p1, p0, Landroid/widget/CalendarView$1;->this$0:Landroid/widget/CalendarView;

    #@2
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onChanged()V
    .registers 7

    #@0
    .prologue
    .line 1011
    iget-object v1, p0, Landroid/widget/CalendarView$1;->this$0:Landroid/widget/CalendarView;

    #@2
    invoke-static {v1}, Landroid/widget/CalendarView;->access$600(Landroid/widget/CalendarView;)Landroid/widget/CalendarView$OnDateChangeListener;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_2c

    #@8
    .line 1012
    iget-object v1, p0, Landroid/widget/CalendarView$1;->this$0:Landroid/widget/CalendarView;

    #@a
    invoke-static {v1}, Landroid/widget/CalendarView;->access$700(Landroid/widget/CalendarView;)Landroid/widget/CalendarView$WeeksAdapter;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/widget/CalendarView$WeeksAdapter;->getSelectedDay()Ljava/util/Calendar;

    #@11
    move-result-object v0

    #@12
    .line 1013
    .local v0, selectedDay:Ljava/util/Calendar;
    iget-object v1, p0, Landroid/widget/CalendarView$1;->this$0:Landroid/widget/CalendarView;

    #@14
    invoke-static {v1}, Landroid/widget/CalendarView;->access$600(Landroid/widget/CalendarView;)Landroid/widget/CalendarView$OnDateChangeListener;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/widget/CalendarView$1;->this$0:Landroid/widget/CalendarView;

    #@1a
    const/4 v3, 0x1

    #@1b
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    #@1e
    move-result v3

    #@1f
    const/4 v4, 0x2

    #@20
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    #@23
    move-result v4

    #@24
    const/4 v5, 0x5

    #@25
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    #@28
    move-result v5

    #@29
    invoke-interface {v1, v2, v3, v4, v5}, Landroid/widget/CalendarView$OnDateChangeListener;->onSelectedDayChange(Landroid/widget/CalendarView;III)V

    #@2c
    .line 1018
    .end local v0           #selectedDay:Ljava/util/Calendar;
    :cond_2c
    return-void
.end method
