.class Landroid/widget/ListView$FocusSelector;
.super Ljava/lang/Object;
.source "ListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusSelector"
.end annotation


# instance fields
.field private mPosition:I

.field private mPositionTop:I

.field final synthetic this$0:Landroid/widget/ListView;


# direct methods
.method private constructor <init>(Landroid/widget/ListView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1080
    iput-object p1, p0, Landroid/widget/ListView$FocusSelector;->this$0:Landroid/widget/ListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ListView;Landroid/widget/ListView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1080
    invoke-direct {p0, p1}, Landroid/widget/ListView$FocusSelector;-><init>(Landroid/widget/ListView;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 1091
    iget-object v0, p0, Landroid/widget/ListView$FocusSelector;->this$0:Landroid/widget/ListView;

    #@2
    iget v1, p0, Landroid/widget/ListView$FocusSelector;->mPosition:I

    #@4
    iget v2, p0, Landroid/widget/ListView$FocusSelector;->mPositionTop:I

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@9
    .line 1092
    return-void
.end method

.method public setup(II)Landroid/widget/ListView$FocusSelector;
    .registers 3
    .parameter "position"
    .parameter "top"

    #@0
    .prologue
    .line 1085
    iput p1, p0, Landroid/widget/ListView$FocusSelector;->mPosition:I

    #@2
    .line 1086
    iput p2, p0, Landroid/widget/ListView$FocusSelector;->mPositionTop:I

    #@4
    .line 1087
    return-object p0
.end method
