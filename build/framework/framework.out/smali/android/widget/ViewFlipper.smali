.class public Landroid/widget/ViewFlipper;
.super Landroid/widget/ViewAnimator;
.source "ViewFlipper.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final DEFAULT_INTERVAL:I = 0xbb8

.field private static final LOGD:Z = false

.field private static final TAG:Ljava/lang/String; = "ViewFlipper"


# instance fields
.field private final FLIP_MSG:I

.field private mAutoStart:Z

.field private mFlipInterval:I

.field private final mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRunning:Z

.field private mStarted:Z

.field private mUserPresent:Z

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;)V

    #@5
    .line 47
    const/16 v0, 0xbb8

    #@7
    iput v0, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@9
    .line 48
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@b
    .line 50
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mRunning:Z

    #@d
    .line 51
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@f
    .line 52
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mVisible:Z

    #@11
    .line 53
    iput-boolean v2, p0, Landroid/widget/ViewFlipper;->mUserPresent:Z

    #@13
    .line 71
    new-instance v0, Landroid/widget/ViewFlipper$1;

    #@15
    invoke-direct {v0, p0}, Landroid/widget/ViewFlipper$1;-><init>(Landroid/widget/ViewFlipper;)V

    #@18
    iput-object v0, p0, Landroid/widget/ViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    .line 213
    iput v2, p0, Landroid/widget/ViewFlipper;->FLIP_MSG:I

    #@1c
    .line 215
    new-instance v0, Landroid/widget/ViewFlipper$2;

    #@1e
    invoke-direct {v0, p0}, Landroid/widget/ViewFlipper$2;-><init>(Landroid/widget/ViewFlipper;)V

    #@21
    iput-object v0, p0, Landroid/widget/ViewFlipper;->mHandler:Landroid/os/Handler;

    #@23
    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v4, 0xbb8

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@7
    .line 47
    iput v4, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@9
    .line 48
    iput-boolean v2, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@b
    .line 50
    iput-boolean v2, p0, Landroid/widget/ViewFlipper;->mRunning:Z

    #@d
    .line 51
    iput-boolean v2, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@f
    .line 52
    iput-boolean v2, p0, Landroid/widget/ViewFlipper;->mVisible:Z

    #@11
    .line 53
    iput-boolean v3, p0, Landroid/widget/ViewFlipper;->mUserPresent:Z

    #@13
    .line 71
    new-instance v1, Landroid/widget/ViewFlipper$1;

    #@15
    invoke-direct {v1, p0}, Landroid/widget/ViewFlipper$1;-><init>(Landroid/widget/ViewFlipper;)V

    #@18
    iput-object v1, p0, Landroid/widget/ViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1a
    .line 213
    iput v3, p0, Landroid/widget/ViewFlipper;->FLIP_MSG:I

    #@1c
    .line 215
    new-instance v1, Landroid/widget/ViewFlipper$2;

    #@1e
    invoke-direct {v1, p0}, Landroid/widget/ViewFlipper$2;-><init>(Landroid/widget/ViewFlipper;)V

    #@21
    iput-object v1, p0, Landroid/widget/ViewFlipper;->mHandler:Landroid/os/Handler;

    #@23
    .line 62
    sget-object v1, Lcom/android/internal/R$styleable;->ViewFlipper:[I

    #@25
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@28
    move-result-object v0

    #@29
    .line 64
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@2f
    .line 66
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@32
    move-result v1

    #@33
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@35
    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@38
    .line 69
    return-void
.end method

.method static synthetic access$002(Landroid/widget/ViewFlipper;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-boolean p1, p0, Landroid/widget/ViewFlipper;->mUserPresent:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/widget/ViewFlipper;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Landroid/widget/ViewFlipper;->updateRunning()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/ViewFlipper;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/ViewFlipper;->updateRunning(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/ViewFlipper;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-boolean v0, p0, Landroid/widget/ViewFlipper;->mRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/widget/ViewFlipper;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget v0, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@2
    return v0
.end method

.method private updateRunning()V
    .registers 2

    #@0
    .prologue
    .line 161
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/widget/ViewFlipper;->updateRunning(Z)V

    #@4
    .line 162
    return-void
.end method

.method private updateRunning(Z)V
    .registers 7
    .parameter "flipNow"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 173
    iget-boolean v3, p0, Landroid/widget/ViewFlipper;->mVisible:Z

    #@3
    if-eqz v3, :cond_2a

    #@5
    iget-boolean v3, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@7
    if-eqz v3, :cond_2a

    #@9
    iget-boolean v3, p0, Landroid/widget/ViewFlipper;->mUserPresent:Z

    #@b
    if-eqz v3, :cond_2a

    #@d
    move v1, v2

    #@e
    .line 174
    .local v1, running:Z
    :goto_e
    iget-boolean v3, p0, Landroid/widget/ViewFlipper;->mRunning:Z

    #@10
    if-eq v1, v3, :cond_29

    #@12
    .line 175
    if-eqz v1, :cond_2c

    #@14
    .line 176
    iget v3, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@16
    invoke-virtual {p0, v3, p1}, Landroid/widget/ViewFlipper;->showOnly(IZ)V

    #@19
    .line 177
    iget-object v3, p0, Landroid/widget/ViewFlipper;->mHandler:Landroid/os/Handler;

    #@1b
    invoke-virtual {v3, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v0

    #@1f
    .line 178
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/widget/ViewFlipper;->mHandler:Landroid/os/Handler;

    #@21
    iget v3, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@23
    int-to-long v3, v3

    #@24
    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@27
    .line 182
    .end local v0           #msg:Landroid/os/Message;
    :goto_27
    iput-boolean v1, p0, Landroid/widget/ViewFlipper;->mRunning:Z

    #@29
    .line 188
    :cond_29
    return-void

    #@2a
    .line 173
    .end local v1           #running:Z
    :cond_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_e

    #@2c
    .line 180
    .restart local v1       #running:Z
    :cond_2c
    iget-object v3, p0, Landroid/widget/ViewFlipper;->mHandler:Landroid/os/Handler;

    #@2e
    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@31
    goto :goto_27
.end method


# virtual methods
.method public isAutoStart()Z
    .registers 2

    #@0
    .prologue
    .line 210
    iget-boolean v0, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@2
    return v0
.end method

.method public isFlipping()Z
    .registers 2

    #@0
    .prologue
    .line 194
    iget-boolean v0, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@2
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/ViewAnimator;->onAttachedToWindow()V

    #@3
    .line 90
    new-instance v0, Landroid/content/IntentFilter;

    #@5
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@8
    .line 91
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d
    .line 92
    const-string v1, "android.intent.action.USER_PRESENT"

    #@f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12
    .line 93
    invoke-virtual {p0}, Landroid/widget/ViewFlipper;->getContext()Landroid/content/Context;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/widget/ViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@18
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1b
    .line 95
    iget-boolean v1, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@1d
    if-eqz v1, :cond_22

    #@1f
    .line 97
    invoke-virtual {p0}, Landroid/widget/ViewFlipper;->startFlipping()V

    #@22
    .line 99
    :cond_22
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/ViewAnimator;->onDetachedFromWindow()V

    #@3
    .line 104
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/ViewFlipper;->mVisible:Z

    #@6
    .line 106
    invoke-virtual {p0}, Landroid/widget/ViewFlipper;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/widget/ViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 107
    invoke-direct {p0}, Landroid/widget/ViewFlipper;->updateRunning()V

    #@12
    .line 108
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 147
    const-class v0, Landroid/widget/ViewFlipper;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 148
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 153
    const-class v0, Landroid/widget/ViewFlipper;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 154
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 4
    .parameter "visibility"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 112
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->onWindowVisibilityChanged(I)V

    #@4
    .line 113
    if-nez p1, :cond_d

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    iput-boolean v0, p0, Landroid/widget/ViewFlipper;->mVisible:Z

    #@9
    .line 114
    invoke-direct {p0, v1}, Landroid/widget/ViewFlipper;->updateRunning(Z)V

    #@c
    .line 115
    return-void

    #@d
    :cond_d
    move v0, v1

    #@e
    .line 113
    goto :goto_7
.end method

.method public setAutoStart(Z)V
    .registers 2
    .parameter "autoStart"

    #@0
    .prologue
    .line 202
    iput-boolean p1, p0, Landroid/widget/ViewFlipper;->mAutoStart:Z

    #@2
    .line 203
    return-void
.end method

.method public setFlipInterval(I)V
    .registers 2
    .parameter "milliseconds"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 125
    iput p1, p0, Landroid/widget/ViewFlipper;->mFlipInterval:I

    #@2
    .line 126
    return-void
.end method

.method public startFlipping()V
    .registers 2

    #@0
    .prologue
    .line 132
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@3
    .line 133
    invoke-direct {p0}, Landroid/widget/ViewFlipper;->updateRunning()V

    #@6
    .line 134
    return-void
.end method

.method public stopFlipping()V
    .registers 2

    #@0
    .prologue
    .line 140
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/ViewFlipper;->mStarted:Z

    #@3
    .line 141
    invoke-direct {p0}, Landroid/widget/ViewFlipper;->updateRunning()V

    #@6
    .line 142
    return-void
.end method
