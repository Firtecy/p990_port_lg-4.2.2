.class public Landroid/widget/GridLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/GridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field private static final BOTTOM_MARGIN:I = 0x6

.field private static final COLUMN:I = 0x1

.field private static final COLUMN_SPAN:I = 0x4

.field private static final DEFAULT_COLUMN:I = -0x80000000

.field private static final DEFAULT_HEIGHT:I = -0x2

.field private static final DEFAULT_MARGIN:I = -0x80000000

.field private static final DEFAULT_ROW:I = -0x80000000

.field private static final DEFAULT_SPAN:Landroid/widget/GridLayout$Interval; = null

#the value of this static final field might be set in the static constructor
.field private static final DEFAULT_SPAN_SIZE:I = 0x0

.field private static final DEFAULT_WIDTH:I = -0x2

.field private static final GRAVITY:I = 0x0

.field private static final LEFT_MARGIN:I = 0x3

.field private static final MARGIN:I = 0x2

.field private static final RIGHT_MARGIN:I = 0x5

.field private static final ROW:I = 0x2

.field private static final ROW_SPAN:I = 0x3

.field private static final TOP_MARGIN:I = 0x4


# instance fields
.field public columnSpec:Landroid/widget/GridLayout$Spec;

.field public rowSpec:Landroid/widget/GridLayout$Spec;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 1820
    new-instance v0, Landroid/widget/GridLayout$Interval;

    #@2
    const/high16 v1, -0x8000

    #@4
    const v2, -0x7fffffff

    #@7
    invoke-direct {v0, v1, v2}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@a
    sput-object v0, Landroid/widget/GridLayout$LayoutParams;->DEFAULT_SPAN:Landroid/widget/GridLayout$Interval;

    #@c
    .line 1821
    sget-object v0, Landroid/widget/GridLayout$LayoutParams;->DEFAULT_SPAN:Landroid/widget/GridLayout$Interval;

    #@e
    invoke-virtual {v0}, Landroid/widget/GridLayout$Interval;->size()I

    #@11
    move-result v0

    #@12
    sput v0, Landroid/widget/GridLayout$LayoutParams;->DEFAULT_SPAN_SIZE:I

    #@14
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 1901
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@2
    sget-object v1, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@4
    invoke-direct {p0, v0, v1}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    #@7
    .line 1902
    return-void
.end method

.method private constructor <init>(IIIIIILandroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V
    .registers 10
    .parameter "width"
    .parameter "height"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "rowSpec"
    .parameter "columnSpec"

    #@0
    .prologue
    .line 1877
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@3
    .line 1855
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@5
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@7
    .line 1869
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 1878
    invoke-virtual {p0, p3, p4, p5, p6}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    #@e
    .line 1879
    iput-object p7, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@10
    .line 1880
    iput-object p8, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@12
    .line 1881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 1938
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 1855
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@5
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@7
    .line 1869
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 1939
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout$LayoutParams;->reInitSuper(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@e
    .line 1940
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout$LayoutParams;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@11
    .line 1941
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 1910
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 1855
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@5
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@7
    .line 1869
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 1911
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 1917
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 1855
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@5
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@7
    .line 1869
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 1918
    return-void
.end method

.method public constructor <init>(Landroid/widget/GridLayout$LayoutParams;)V
    .registers 3
    .parameter "that"

    #@0
    .prologue
    .line 1924
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 1855
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@5
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@7
    .line 1869
    sget-object v0, Landroid/widget/GridLayout$Spec;->UNDEFINED:Landroid/widget/GridLayout$Spec;

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@b
    .line 1925
    iget-object v0, p1, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@d
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@f
    .line 1926
    iget-object v0, p1, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@11
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@13
    .line 1927
    return-void
.end method

.method public constructor <init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V
    .registers 12
    .parameter "rowSpec"
    .parameter "columnSpec"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    const/high16 v3, -0x8000

    #@3
    .line 1892
    move-object v0, p0

    #@4
    move v2, v1

    #@5
    move v4, v3

    #@6
    move v5, v3

    #@7
    move v6, v3

    #@8
    move-object v7, p1

    #@9
    move-object v8, p2

    #@a
    invoke-direct/range {v0 .. v8}, Landroid/widget/GridLayout$LayoutParams;-><init>(IIIIIILandroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    #@d
    .line 1895
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 1970
    sget-object v6, Lcom/android/internal/R$styleable;->GridLayout_Layout:[I

    #@2
    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v0

    #@6
    .line 1972
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v6, 0x0

    #@7
    const/4 v7, 0x0

    #@8
    :try_start_8
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@b
    move-result v3

    #@c
    .line 1974
    .local v3, gravity:I
    const/4 v6, 0x1

    #@d
    const/high16 v7, -0x8000

    #@f
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@12
    move-result v2

    #@13
    .line 1975
    .local v2, column:I
    const/4 v6, 0x4

    #@14
    sget v7, Landroid/widget/GridLayout$LayoutParams;->DEFAULT_SPAN_SIZE:I

    #@16
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@19
    move-result v1

    #@1a
    .line 1976
    .local v1, colSpan:I
    const/4 v6, 0x1

    #@1b
    invoke-static {v3, v6}, Landroid/widget/GridLayout;->getAlignment(IZ)Landroid/widget/GridLayout$Alignment;

    #@1e
    move-result-object v6

    #@1f
    invoke-static {v2, v1, v6}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@22
    move-result-object v6

    #@23
    iput-object v6, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@25
    .line 1978
    const/4 v6, 0x2

    #@26
    const/high16 v7, -0x8000

    #@28
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2b
    move-result v4

    #@2c
    .line 1979
    .local v4, row:I
    const/4 v6, 0x3

    #@2d
    sget v7, Landroid/widget/GridLayout$LayoutParams;->DEFAULT_SPAN_SIZE:I

    #@2f
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@32
    move-result v5

    #@33
    .line 1980
    .local v5, rowSpan:I
    const/4 v6, 0x0

    #@34
    invoke-static {v3, v6}, Landroid/widget/GridLayout;->getAlignment(IZ)Landroid/widget/GridLayout$Alignment;

    #@37
    move-result-object v6

    #@38
    invoke-static {v4, v5, v6}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@3b
    move-result-object v6

    #@3c
    iput-object v6, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;
    :try_end_3e
    .catchall {:try_start_8 .. :try_end_3e} :catchall_42

    #@3e
    .line 1982
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@41
    .line 1984
    return-void

    #@42
    .line 1982
    .end local v1           #colSpan:I
    .end local v2           #column:I
    .end local v3           #gravity:I
    .end local v4           #row:I
    .end local v5           #rowSpan:I
    :catchall_42
    move-exception v6

    #@43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@46
    throw v6
.end method

.method private reInitSuper(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 1955
    sget-object v2, Lcom/android/internal/R$styleable;->ViewGroup_MarginLayout:[I

    #@2
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v0

    #@6
    .line 1958
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x2

    #@7
    const/high16 v3, -0x8000

    #@9
    :try_start_9
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@c
    move-result v1

    #@d
    .line 1960
    .local v1, margin:I
    const/4 v2, 0x3

    #@e
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@11
    move-result v2

    #@12
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@14
    .line 1961
    const/4 v2, 0x4

    #@15
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@18
    move-result v2

    #@19
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1b
    .line 1962
    const/4 v2, 0x5

    #@1c
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1f
    move-result v2

    #@20
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@22
    .line 1963
    const/4 v2, 0x6

    #@23
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@26
    move-result v2

    #@27
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I
    :try_end_29
    .catchall {:try_start_9 .. :try_end_29} :catchall_2d

    #@29
    .line 1965
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2c
    .line 1967
    return-void

    #@2d
    .line 1965
    .end local v1           #margin:I
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@31
    throw v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2015
    if-ne p0, p1, :cond_5

    #@4
    .line 2023
    :cond_4
    :goto_4
    return v1

    #@5
    .line 2016
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 2018
    check-cast v0, Landroid/widget/GridLayout$LayoutParams;

    #@16
    .line 2020
    .local v0, that:Landroid/widget/GridLayout$LayoutParams;
    iget-object v3, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@18
    iget-object v4, v0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@1a
    invoke-virtual {v3, v4}, Landroid/widget/GridLayout$Spec;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_22

    #@20
    move v1, v2

    #@21
    goto :goto_4

    #@22
    .line 2021
    :cond_22
    iget-object v3, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@24
    iget-object v4, v0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@26
    invoke-virtual {v3, v4}, Landroid/widget/GridLayout$Spec;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_4

    #@2c
    move v1, v2

    #@2d
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 2028
    iget-object v1, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@2
    invoke-virtual {v1}, Landroid/widget/GridLayout$Spec;->hashCode()I

    #@5
    move-result v0

    #@6
    .line 2029
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget-object v2, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@a
    invoke-virtual {v2}, Landroid/widget/GridLayout$Spec;->hashCode()I

    #@d
    move-result v2

    #@e
    add-int v0, v1, v2

    #@10
    .line 2030
    return v0
.end method

.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .registers 6
    .parameter "attributes"
    .parameter "widthAttr"
    .parameter "heightAttr"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 2001
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@4
    move-result v0

    #@5
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@7
    .line 2002
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@d
    .line 2003
    return-void
.end method

.method final setColumnSpecSpan(Landroid/widget/GridLayout$Interval;)V
    .registers 3
    .parameter "span"

    #@0
    .prologue
    .line 2010
    iget-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Spec;->copyWriteSpan(Landroid/widget/GridLayout$Interval;)Landroid/widget/GridLayout$Spec;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@8
    .line 2011
    return-void
.end method

.method public setGravity(I)V
    .registers 4
    .parameter "gravity"

    #@0
    .prologue
    .line 1995
    iget-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {p1, v1}, Landroid/widget/GridLayout;->getAlignment(IZ)Landroid/widget/GridLayout$Alignment;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout$Spec;->copyWriteAlignment(Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@d
    .line 1996
    iget-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@f
    const/4 v1, 0x1

    #@10
    invoke-static {p1, v1}, Landroid/widget/GridLayout;->getAlignment(IZ)Landroid/widget/GridLayout$Alignment;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Landroid/widget/GridLayout$Spec;->copyWriteAlignment(Landroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@1a
    .line 1997
    return-void
.end method

.method final setRowSpecSpan(Landroid/widget/GridLayout$Interval;)V
    .registers 3
    .parameter "span"

    #@0
    .prologue
    .line 2006
    iget-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/GridLayout$Spec;->copyWriteSpan(Landroid/widget/GridLayout$Interval;)Landroid/widget/GridLayout$Spec;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@8
    .line 2007
    return-void
.end method
