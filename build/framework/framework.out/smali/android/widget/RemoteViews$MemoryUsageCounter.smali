.class Landroid/widget/RemoteViews$MemoryUsageCounter;
.super Ljava/lang/Object;
.source "RemoteViews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoryUsageCounter"
.end annotation


# instance fields
.field mMemoryUsage:I

.field final synthetic this$0:Landroid/widget/RemoteViews;


# direct methods
.method private constructor <init>(Landroid/widget/RemoteViews;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1408
    iput-object p1, p0, Landroid/widget/RemoteViews$MemoryUsageCounter;->this$0:Landroid/widget/RemoteViews;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1408
    invoke-direct {p0, p1}, Landroid/widget/RemoteViews$MemoryUsageCounter;-><init>(Landroid/widget/RemoteViews;)V

    #@3
    return-void
.end method


# virtual methods
.method public addBitmapMemory(Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "b"

    #@0
    .prologue
    .line 1422
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@3
    move-result-object v1

    #@4
    .line 1424
    .local v1, c:Landroid/graphics/Bitmap$Config;
    const/4 v0, 0x4

    #@5
    .line 1425
    .local v0, bpp:I
    if-eqz v1, :cond_12

    #@7
    .line 1426
    sget-object v2, Landroid/widget/RemoteViews$2;->$SwitchMap$android$graphics$Bitmap$Config:[I

    #@9
    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->ordinal()I

    #@c
    move-result v3

    #@d
    aget v2, v2, v3

    #@f
    packed-switch v2, :pswitch_data_26

    #@12
    .line 1439
    :cond_12
    :goto_12
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@15
    move-result v2

    #@16
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@19
    move-result v3

    #@1a
    mul-int/2addr v2, v3

    #@1b
    mul-int/2addr v2, v0

    #@1c
    invoke-virtual {p0, v2}, Landroid/widget/RemoteViews$MemoryUsageCounter;->increment(I)V

    #@1f
    .line 1440
    return-void

    #@20
    .line 1428
    :pswitch_20
    const/4 v0, 0x1

    #@21
    .line 1429
    goto :goto_12

    #@22
    .line 1432
    :pswitch_22
    const/4 v0, 0x2

    #@23
    .line 1433
    goto :goto_12

    #@24
    .line 1435
    :pswitch_24
    const/4 v0, 0x4

    #@25
    goto :goto_12

    #@26
    .line 1426
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_20
        :pswitch_22
        :pswitch_22
        :pswitch_24
    .end packed-switch
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 1410
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/widget/RemoteViews$MemoryUsageCounter;->mMemoryUsage:I

    #@3
    .line 1411
    return-void
.end method

.method public getMemoryUsage()I
    .registers 2

    #@0
    .prologue
    .line 1418
    iget v0, p0, Landroid/widget/RemoteViews$MemoryUsageCounter;->mMemoryUsage:I

    #@2
    return v0
.end method

.method public increment(I)V
    .registers 3
    .parameter "numBytes"

    #@0
    .prologue
    .line 1414
    iget v0, p0, Landroid/widget/RemoteViews$MemoryUsageCounter;->mMemoryUsage:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/widget/RemoteViews$MemoryUsageCounter;->mMemoryUsage:I

    #@5
    .line 1415
    return-void
.end method
