.class public Landroid/widget/ListView;
.super Landroid/widget/AbsListView;
.source "ListView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ListView$1;,
        Landroid/widget/ListView$ArrowScrollFocusResult;,
        Landroid/widget/ListView$FocusSelector;,
        Landroid/widget/ListView$FixedViewInfo;
    }
.end annotation


# static fields
.field private static final MAX_SCROLL_FACTOR:F = 0.33f

.field private static final MIN_SCROLL_PREVIEW_PIXELS:I = 0x2

.field static final NO_POSITION:I = -0x1


# instance fields
.field private mAreAllItemsSelectable:Z

.field private final mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

.field mDivider:Landroid/graphics/drawable/Drawable;

.field mDividerHeight:I

.field private mDividerIsOpaque:Z

.field private mDividerPaint:Landroid/graphics/Paint;

.field private mFocusSelector:Landroid/widget/ListView$FocusSelector;

.field private mFooterDividersEnabled:Z

.field private mFooterViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderDividersEnabled:Z

.field private mHeaderViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCacheColorOpaque:Z

.field private mItemsCanFocus:Z

.field mOverScrollFooter:Landroid/graphics/drawable/Drawable;

.field mOverScrollHeader:Landroid/graphics/drawable/Drawable;

.field private final mTempRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 139
    const v0, 0x1010074

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 143
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 104
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@8
    move-result-object v6

    #@9
    iput-object v6, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@b
    .line 105
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@e
    move-result-object v6

    #@f
    iput-object v6, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@11
    .line 119
    iput-boolean v9, p0, Landroid/widget/ListView;->mAreAllItemsSelectable:Z

    #@13
    .line 121
    iput-boolean v8, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@15
    .line 124
    new-instance v6, Landroid/graphics/Rect;

    #@17
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@1a
    iput-object v6, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@1c
    .line 129
    new-instance v6, Landroid/widget/ListView$ArrowScrollFocusResult;

    #@1e
    const/4 v7, 0x0

    #@1f
    invoke-direct {v6, v7}, Landroid/widget/ListView$ArrowScrollFocusResult;-><init>(Landroid/widget/ListView$1;)V

    #@22
    iput-object v6, p0, Landroid/widget/ListView;->mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

    #@24
    .line 145
    sget-object v6, Lcom/android/internal/R$styleable;->ListView:[I

    #@26
    invoke-virtual {p1, p2, v6, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@29
    move-result-object v0

    #@2a
    .line 148
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@2d
    move-result-object v3

    #@2e
    .line 150
    .local v3, entries:[Ljava/lang/CharSequence;
    if-eqz v3, :cond_3b

    #@30
    .line 151
    new-instance v6, Landroid/widget/ArrayAdapter;

    #@32
    const v7, 0x1090003

    #@35
    invoke-direct {v6, p1, v7, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    #@38
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@3b
    .line 155
    :cond_3b
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v1

    #@3f
    .line 156
    .local v1, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_44

    #@41
    .line 158
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    #@44
    .line 161
    :cond_44
    const/4 v6, 0x5

    #@45
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@48
    move-result-object v5

    #@49
    .line 163
    .local v5, osHeader:Landroid/graphics/drawable/Drawable;
    if-eqz v5, :cond_4e

    #@4b
    .line 164
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V

    #@4e
    .line 167
    :cond_4e
    const/4 v6, 0x6

    #@4f
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@52
    move-result-object v4

    #@53
    .line 169
    .local v4, osFooter:Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_58

    #@55
    .line 170
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V

    #@58
    .line 174
    :cond_58
    const/4 v6, 0x2

    #@59
    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@5c
    move-result v2

    #@5d
    .line 176
    .local v2, dividerHeight:I
    if-eqz v2, :cond_62

    #@5f
    .line 177
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    #@62
    .line 180
    :cond_62
    const/4 v6, 0x3

    #@63
    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@66
    move-result v6

    #@67
    iput-boolean v6, p0, Landroid/widget/ListView;->mHeaderDividersEnabled:Z

    #@69
    .line 181
    const/4 v6, 0x4

    #@6a
    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6d
    move-result v6

    #@6e
    iput-boolean v6, p0, Landroid/widget/ListView;->mFooterDividersEnabled:Z

    #@70
    .line 183
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@73
    .line 184
    return-void
.end method

.method private addViewAbove(Landroid/view/View;I)Landroid/view/View;
    .registers 11
    .parameter "theView"
    .parameter "position"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3026
    add-int/lit8 v2, p2, -0x1

    #@3
    .line 3027
    .local v2, abovePosition:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@5
    invoke-virtual {p0, v2, v0}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    .line 3028
    .local v1, view:Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@c
    move-result v0

    #@d
    iget v5, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@f
    sub-int v3, v0, v5

    #@11
    .line 3029
    .local v3, edgeOfNewChild:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@13
    iget v5, v0, Landroid/graphics/Rect;->left:I

    #@15
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@17
    aget-boolean v7, v0, v4

    #@19
    move-object v0, p0

    #@1a
    move v6, v4

    #@1b
    invoke-direct/range {v0 .. v7}, Landroid/widget/ListView;->setupChild(Landroid/view/View;IIZIZZ)V

    #@1e
    .line 3031
    return-object v1
.end method

.method private addViewBelow(Landroid/view/View;I)Landroid/view/View;
    .registers 11
    .parameter "theView"
    .parameter "position"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 3035
    add-int/lit8 v2, p2, 0x1

    #@3
    .line 3036
    .local v2, belowPosition:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@5
    invoke-virtual {p0, v2, v0}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    .line 3037
    .local v1, view:Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@c
    move-result v0

    #@d
    iget v4, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@f
    add-int v3, v0, v4

    #@11
    .line 3038
    .local v3, edgeOfNewChild:I
    const/4 v4, 0x1

    #@12
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@14
    iget v5, v0, Landroid/graphics/Rect;->left:I

    #@16
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@18
    aget-boolean v7, v0, v6

    #@1a
    move-object v0, p0

    #@1b
    invoke-direct/range {v0 .. v7}, Landroid/widget/ListView;->setupChild(Landroid/view/View;IIZIZZ)V

    #@1e
    .line 3040
    return-object v1
.end method

.method private adjustViewsUpOrDown()V
    .registers 7

    #@0
    .prologue
    .line 199
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 202
    .local v1, childCount:I
    if-lez v1, :cond_29

    #@6
    .line 205
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@8
    if-nez v3, :cond_2a

    #@a
    .line 208
    const/4 v3, 0x0

    #@b
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    .line 209
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@12
    move-result v3

    #@13
    iget-object v4, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@15
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@17
    sub-int v2, v3, v4

    #@19
    .line 210
    .local v2, delta:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 213
    iget v3, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@1f
    sub-int/2addr v2, v3

    #@20
    .line 215
    :cond_20
    if-gez v2, :cond_23

    #@22
    .line 217
    const/4 v2, 0x0

    #@23
    .line 235
    :cond_23
    :goto_23
    if-eqz v2, :cond_29

    #@25
    .line 236
    neg-int v3, v2

    #@26
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@29
    .line 239
    .end local v0           #child:Landroid/view/View;
    .end local v2           #delta:I
    :cond_29
    return-void

    #@2a
    .line 221
    :cond_2a
    add-int/lit8 v3, v1, -0x1

    #@2c
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@2f
    move-result-object v0

    #@30
    .line 222
    .restart local v0       #child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@33
    move-result v3

    #@34
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@37
    move-result v4

    #@38
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3a
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@3c
    sub-int/2addr v4, v5

    #@3d
    sub-int v2, v3, v4

    #@3f
    .line 224
    .restart local v2       #delta:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@41
    add-int/2addr v3, v1

    #@42
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@44
    if-ge v3, v4, :cond_49

    #@46
    .line 227
    iget v3, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@48
    add-int/2addr v2, v3

    #@49
    .line 230
    :cond_49
    if-lez v2, :cond_23

    #@4b
    .line 231
    const/4 v2, 0x0

    #@4c
    goto :goto_23
.end method

.method private amountToScroll(II)I
    .registers 15
    .parameter "direction"
    .parameter "nextSelectedPosition"

    #@0
    .prologue
    .line 2641
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@3
    move-result v10

    #@4
    iget-object v11, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@6
    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    #@8
    sub-int v4, v10, v11

    #@a
    .line 2642
    .local v4, listBottom:I
    iget-object v10, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@c
    iget v5, v10, Landroid/graphics/Rect;->top:I

    #@e
    .line 2644
    .local v5, listTop:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@11
    move-result v7

    #@12
    .line 2646
    .local v7, numChildren:I
    const/16 v10, 0x82

    #@14
    if-ne p1, v10, :cond_72

    #@16
    .line 2647
    add-int/lit8 v3, v7, -0x1

    #@18
    .line 2648
    .local v3, indexToMakeVisible:I
    const/4 v10, -0x1

    #@19
    if-eq p2, v10, :cond_1f

    #@1b
    .line 2649
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1d
    sub-int v3, p2, v10

    #@1f
    .line 2652
    :cond_1f
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@21
    add-int v8, v10, v3

    #@23
    .line 2653
    .local v8, positionToMakeVisible:I
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@26
    move-result-object v9

    #@27
    .line 2655
    .local v9, viewToMakeVisible:Landroid/view/View;
    move v1, v4

    #@28
    .line 2656
    .local v1, goalBottom:I
    iget v10, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2a
    add-int/lit8 v10, v10, -0x1

    #@2c
    if-ge v8, v10, :cond_33

    #@2e
    .line 2657
    invoke-direct {p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@31
    move-result v10

    #@32
    sub-int/2addr v1, v10

    #@33
    .line 2660
    :cond_33
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    #@36
    move-result v10

    #@37
    if-gt v10, v1, :cond_3b

    #@39
    .line 2662
    const/4 v10, 0x0

    #@3a
    .line 2708
    .end local v1           #goalBottom:I
    :goto_3a
    return v10

    #@3b
    .line 2665
    .restart local v1       #goalBottom:I
    :cond_3b
    const/4 v10, -0x1

    #@3c
    if-eq p2, v10, :cond_4c

    #@3e
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    #@41
    move-result v10

    #@42
    sub-int v10, v1, v10

    #@44
    invoke-virtual {p0}, Landroid/widget/ListView;->getMaxScrollAmount()I

    #@47
    move-result v11

    #@48
    if-lt v10, v11, :cond_4c

    #@4a
    .line 2668
    const/4 v10, 0x0

    #@4b
    goto :goto_3a

    #@4c
    .line 2671
    :cond_4c
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    #@4f
    move-result v10

    #@50
    sub-int v0, v10, v1

    #@52
    .line 2673
    .local v0, amountToScroll:I
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@54
    add-int/2addr v10, v7

    #@55
    iget v11, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@57
    if-ne v10, v11, :cond_69

    #@59
    .line 2675
    add-int/lit8 v10, v7, -0x1

    #@5b
    invoke-virtual {p0, v10}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@5e
    move-result-object v10

    #@5f
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@62
    move-result v10

    #@63
    sub-int v6, v10, v4

    #@65
    .line 2676
    .local v6, max:I
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    #@68
    move-result v0

    #@69
    .line 2679
    .end local v6           #max:I
    :cond_69
    invoke-virtual {p0}, Landroid/widget/ListView;->getMaxScrollAmount()I

    #@6c
    move-result v10

    #@6d
    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    #@70
    move-result v10

    #@71
    goto :goto_3a

    #@72
    .line 2681
    .end local v0           #amountToScroll:I
    .end local v1           #goalBottom:I
    .end local v3           #indexToMakeVisible:I
    .end local v8           #positionToMakeVisible:I
    .end local v9           #viewToMakeVisible:Landroid/view/View;
    :cond_72
    const/4 v3, 0x0

    #@73
    .line 2682
    .restart local v3       #indexToMakeVisible:I
    const/4 v10, -0x1

    #@74
    if-eq p2, v10, :cond_7a

    #@76
    .line 2683
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@78
    sub-int v3, p2, v10

    #@7a
    .line 2685
    :cond_7a
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@7c
    add-int v8, v10, v3

    #@7e
    .line 2686
    .restart local v8       #positionToMakeVisible:I
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@81
    move-result-object v9

    #@82
    .line 2687
    .restart local v9       #viewToMakeVisible:Landroid/view/View;
    move v2, v5

    #@83
    .line 2688
    .local v2, goalTop:I
    if-lez v8, :cond_8a

    #@85
    .line 2689
    invoke-direct {p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@88
    move-result v10

    #@89
    add-int/2addr v2, v10

    #@8a
    .line 2691
    :cond_8a
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    #@8d
    move-result v10

    #@8e
    if-lt v10, v2, :cond_92

    #@90
    .line 2693
    const/4 v10, 0x0

    #@91
    goto :goto_3a

    #@92
    .line 2696
    :cond_92
    const/4 v10, -0x1

    #@93
    if-eq p2, v10, :cond_a2

    #@95
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    #@98
    move-result v10

    #@99
    sub-int/2addr v10, v2

    #@9a
    invoke-virtual {p0}, Landroid/widget/ListView;->getMaxScrollAmount()I

    #@9d
    move-result v11

    #@9e
    if-lt v10, v11, :cond_a2

    #@a0
    .line 2699
    const/4 v10, 0x0

    #@a1
    goto :goto_3a

    #@a2
    .line 2702
    :cond_a2
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    #@a5
    move-result v10

    #@a6
    sub-int v0, v2, v10

    #@a8
    .line 2703
    .restart local v0       #amountToScroll:I
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@aa
    if-nez v10, :cond_bb

    #@ac
    .line 2705
    const/4 v10, 0x0

    #@ad
    invoke-virtual {p0, v10}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@b0
    move-result-object v10

    #@b1
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@b4
    move-result v10

    #@b5
    sub-int v6, v5, v10

    #@b7
    .line 2706
    .restart local v6       #max:I
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    #@ba
    move-result v0

    #@bb
    .line 2708
    .end local v6           #max:I
    :cond_bb
    invoke-virtual {p0}, Landroid/widget/ListView;->getMaxScrollAmount()I

    #@be
    move-result v10

    #@bf
    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    #@c2
    move-result v10

    #@c3
    goto/16 :goto_3a
.end method

.method private amountToScrollToNewFocus(ILandroid/view/View;I)I
    .registers 8
    .parameter "direction"
    .parameter "newFocus"
    .parameter "positionOfNewFocus"

    #@0
    .prologue
    .line 2900
    const/4 v0, 0x0

    #@1
    .line 2901
    .local v0, amountToScroll:I
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {p2, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@6
    .line 2902
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@8
    invoke-virtual {p0, p2, v2}, Landroid/widget/ListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@b
    .line 2903
    const/16 v2, 0x21

    #@d
    if-ne p1, v2, :cond_2b

    #@f
    .line 2904
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@11
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@13
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@15
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@17
    if-ge v2, v3, :cond_2a

    #@19
    .line 2905
    iget-object v2, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1b
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@1d
    iget-object v3, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@1f
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@21
    sub-int v0, v2, v3

    #@23
    .line 2906
    if-lez p3, :cond_2a

    #@25
    .line 2907
    invoke-direct {p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@28
    move-result v2

    #@29
    add-int/2addr v0, v2

    #@2a
    .line 2919
    :cond_2a
    :goto_2a
    return v0

    #@2b
    .line 2911
    :cond_2b
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@2e
    move-result v2

    #@2f
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@31
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@33
    sub-int v1, v2, v3

    #@35
    .line 2912
    .local v1, listBottom:I
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@37
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    #@39
    if-le v2, v1, :cond_2a

    #@3b
    .line 2913
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@3d
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    #@3f
    sub-int v0, v2, v1

    #@41
    .line 2914
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@43
    add-int/lit8 v2, v2, -0x1

    #@45
    if-ge p3, v2, :cond_2a

    #@47
    .line 2915
    invoke-direct {p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@4a
    move-result v2

    #@4b
    add-int/2addr v0, v2

    #@4c
    goto :goto_2a
.end method

.method private arrowScrollFocused(I)Landroid/widget/ListView$ArrowScrollFocusResult;
    .registers 19
    .parameter "direction"

    #@0
    .prologue
    .line 2798
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    #@3
    move-result-object v11

    #@4
    .line 2800
    .local v11, selectedView:Landroid/view/View;
    if-eqz v11, :cond_4a

    #@6
    invoke-virtual {v11}, Landroid/view/View;->hasFocus()Z

    #@9
    move-result v14

    #@a
    if-eqz v14, :cond_4a

    #@c
    .line 2801
    invoke-virtual {v11}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@f
    move-result-object v8

    #@10
    .line 2802
    .local v8, oldFocus:Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@13
    move-result-object v14

    #@14
    move-object/from16 v0, p0

    #@16
    move/from16 v1, p1

    #@18
    invoke-virtual {v14, v0, v8, v1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@1b
    move-result-object v7

    #@1c
    .line 2827
    .end local v8           #oldFocus:Landroid/view/View;
    .local v7, newFocus:Landroid/view/View;
    :goto_1c
    if-eqz v7, :cond_113

    #@1e
    .line 2828
    move-object/from16 v0, p0

    #@20
    invoke-direct {v0, v7}, Landroid/widget/ListView;->positionOfNewFocus(Landroid/view/View;)I

    #@23
    move-result v9

    #@24
    .line 2832
    .local v9, positionOfNewFocus:I
    move-object/from16 v0, p0

    #@26
    iget v14, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@28
    const/4 v15, -0x1

    #@29
    if-eq v14, v15, :cond_d9

    #@2b
    move-object/from16 v0, p0

    #@2d
    iget v14, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2f
    if-eq v9, v14, :cond_d9

    #@31
    .line 2833
    invoke-direct/range {p0 .. p1}, Landroid/widget/ListView;->lookForSelectablePositionOnScreen(I)I

    #@34
    move-result v10

    #@35
    .line 2834
    .local v10, selectablePosition:I
    const/4 v14, -0x1

    #@36
    if-eq v10, v14, :cond_d9

    #@38
    const/16 v14, 0x82

    #@3a
    move/from16 v0, p1

    #@3c
    if-ne v0, v14, :cond_40

    #@3e
    if-lt v10, v9, :cond_48

    #@40
    :cond_40
    const/16 v14, 0x21

    #@42
    move/from16 v0, p1

    #@44
    if-ne v0, v14, :cond_d9

    #@46
    if-le v10, v9, :cond_d9

    #@48
    .line 2837
    :cond_48
    const/4 v14, 0x0

    #@49
    .line 2859
    .end local v9           #positionOfNewFocus:I
    .end local v10           #selectablePosition:I
    :goto_49
    return-object v14

    #@4a
    .line 2804
    .end local v7           #newFocus:Landroid/view/View;
    :cond_4a
    const/16 v14, 0x82

    #@4c
    move/from16 v0, p1

    #@4e
    if-ne v0, v14, :cond_94

    #@50
    .line 2805
    move-object/from16 v0, p0

    #@52
    iget v14, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@54
    if-lez v14, :cond_8e

    #@56
    const/4 v12, 0x1

    #@57
    .line 2806
    .local v12, topFadingEdgeShowing:Z
    :goto_57
    move-object/from16 v0, p0

    #@59
    iget-object v14, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@5b
    iget v15, v14, Landroid/graphics/Rect;->top:I

    #@5d
    if-eqz v12, :cond_90

    #@5f
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@62
    move-result v14

    #@63
    :goto_63
    add-int v5, v15, v14

    #@65
    .line 2808
    .local v5, listTop:I
    if-eqz v11, :cond_92

    #@67
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    #@6a
    move-result v14

    #@6b
    if-le v14, v5, :cond_92

    #@6d
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    #@70
    move-result v13

    #@71
    .line 2812
    .local v13, ySearchPoint:I
    :goto_71
    move-object/from16 v0, p0

    #@73
    iget-object v14, v0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@75
    const/4 v15, 0x0

    #@76
    const/16 v16, 0x0

    #@78
    move/from16 v0, v16

    #@7a
    invoke-virtual {v14, v15, v13, v0, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@7d
    .line 2824
    .end local v5           #listTop:I
    .end local v12           #topFadingEdgeShowing:Z
    :goto_7d
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@80
    move-result-object v14

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v15, v0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@85
    move-object/from16 v0, p0

    #@87
    move/from16 v1, p1

    #@89
    invoke-virtual {v14, v0, v15, v1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    #@8c
    move-result-object v7

    #@8d
    .restart local v7       #newFocus:Landroid/view/View;
    goto :goto_1c

    #@8e
    .line 2805
    .end local v7           #newFocus:Landroid/view/View;
    .end local v13           #ySearchPoint:I
    :cond_8e
    const/4 v12, 0x0

    #@8f
    goto :goto_57

    #@90
    .line 2806
    .restart local v12       #topFadingEdgeShowing:Z
    :cond_90
    const/4 v14, 0x0

    #@91
    goto :goto_63

    #@92
    .restart local v5       #listTop:I
    :cond_92
    move v13, v5

    #@93
    .line 2808
    goto :goto_71

    #@94
    .line 2814
    .end local v5           #listTop:I
    .end local v12           #topFadingEdgeShowing:Z
    :cond_94
    move-object/from16 v0, p0

    #@96
    iget v14, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@98
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getChildCount()I

    #@9b
    move-result v15

    #@9c
    add-int/2addr v14, v15

    #@9d
    add-int/lit8 v14, v14, -0x1

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v15, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@a3
    if-ge v14, v15, :cond_d3

    #@a5
    const/4 v2, 0x1

    #@a6
    .line 2816
    .local v2, bottomFadingEdgeShowing:Z
    :goto_a6
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getHeight()I

    #@a9
    move-result v14

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget-object v15, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@ae
    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    #@b0
    sub-int v15, v14, v15

    #@b2
    if-eqz v2, :cond_d5

    #@b4
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->getArrowScrollPreviewLength()I

    #@b7
    move-result v14

    #@b8
    :goto_b8
    sub-int v4, v15, v14

    #@ba
    .line 2818
    .local v4, listBottom:I
    if-eqz v11, :cond_d7

    #@bc
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    #@bf
    move-result v14

    #@c0
    if-ge v14, v4, :cond_d7

    #@c2
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    #@c5
    move-result v13

    #@c6
    .line 2822
    .restart local v13       #ySearchPoint:I
    :goto_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v14, v0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@ca
    const/4 v15, 0x0

    #@cb
    const/16 v16, 0x0

    #@cd
    move/from16 v0, v16

    #@cf
    invoke-virtual {v14, v15, v13, v0, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@d2
    goto :goto_7d

    #@d3
    .line 2814
    .end local v2           #bottomFadingEdgeShowing:Z
    .end local v4           #listBottom:I
    .end local v13           #ySearchPoint:I
    :cond_d3
    const/4 v2, 0x0

    #@d4
    goto :goto_a6

    #@d5
    .line 2816
    .restart local v2       #bottomFadingEdgeShowing:Z
    :cond_d5
    const/4 v14, 0x0

    #@d6
    goto :goto_b8

    #@d7
    .restart local v4       #listBottom:I
    :cond_d7
    move v13, v4

    #@d8
    .line 2818
    goto :goto_c6

    #@d9
    .line 2841
    .end local v2           #bottomFadingEdgeShowing:Z
    .end local v4           #listBottom:I
    .restart local v7       #newFocus:Landroid/view/View;
    .restart local v9       #positionOfNewFocus:I
    :cond_d9
    move-object/from16 v0, p0

    #@db
    move/from16 v1, p1

    #@dd
    invoke-direct {v0, v1, v7, v9}, Landroid/widget/ListView;->amountToScrollToNewFocus(ILandroid/view/View;I)I

    #@e0
    move-result v3

    #@e1
    .line 2843
    .local v3, focusScroll:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getMaxScrollAmount()I

    #@e4
    move-result v6

    #@e5
    .line 2844
    .local v6, maxScrollAmount:I
    if-ge v3, v6, :cond_f9

    #@e7
    .line 2846
    move/from16 v0, p1

    #@e9
    invoke-virtual {v7, v0}, Landroid/view/View;->requestFocus(I)Z

    #@ec
    .line 2847
    move-object/from16 v0, p0

    #@ee
    iget-object v14, v0, Landroid/widget/ListView;->mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

    #@f0
    invoke-virtual {v14, v9, v3}, Landroid/widget/ListView$ArrowScrollFocusResult;->populate(II)V

    #@f3
    .line 2848
    move-object/from16 v0, p0

    #@f5
    iget-object v14, v0, Landroid/widget/ListView;->mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

    #@f7
    goto/16 :goto_49

    #@f9
    .line 2849
    :cond_f9
    move-object/from16 v0, p0

    #@fb
    invoke-direct {v0, v7}, Landroid/widget/ListView;->distanceToView(Landroid/view/View;)I

    #@fe
    move-result v14

    #@ff
    if-ge v14, v6, :cond_113

    #@101
    .line 2854
    move/from16 v0, p1

    #@103
    invoke-virtual {v7, v0}, Landroid/view/View;->requestFocus(I)Z

    #@106
    .line 2855
    move-object/from16 v0, p0

    #@108
    iget-object v14, v0, Landroid/widget/ListView;->mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

    #@10a
    invoke-virtual {v14, v9, v6}, Landroid/widget/ListView$ArrowScrollFocusResult;->populate(II)V

    #@10d
    .line 2856
    move-object/from16 v0, p0

    #@10f
    iget-object v14, v0, Landroid/widget/ListView;->mArrowScrollFocusResult:Landroid/widget/ListView$ArrowScrollFocusResult;

    #@111
    goto/16 :goto_49

    #@113
    .line 2859
    .end local v3           #focusScroll:I
    .end local v6           #maxScrollAmount:I
    .end local v9           #positionOfNewFocus:I
    :cond_113
    const/4 v14, 0x0

    #@114
    goto/16 :goto_49
.end method

.method private arrowScrollImpl(I)Z
    .registers 13
    .parameter "direction"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v10, -0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 2425
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@6
    move-result v7

    #@7
    if-gtz v7, :cond_a

    #@9
    .line 2499
    :cond_9
    :goto_9
    return v9

    #@a
    .line 2429
    :cond_a
    invoke-virtual {p0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    #@d
    move-result-object v6

    #@e
    .line 2430
    .local v6, selectedView:Landroid/view/View;
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@10
    .line 2432
    .local v5, selectedPos:I
    invoke-direct {p0, p1}, Landroid/widget/ListView;->lookForSelectablePositionOnScreen(I)I

    #@13
    move-result v4

    #@14
    .line 2433
    .local v4, nextSelectedPosition:I
    invoke-direct {p0, p1, v4}, Landroid/widget/ListView;->amountToScroll(II)I

    #@17
    move-result v0

    #@18
    .line 2436
    .local v0, amountToScroll:I
    iget-boolean v7, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@1a
    if-eqz v7, :cond_aa

    #@1c
    invoke-direct {p0, p1}, Landroid/widget/ListView;->arrowScrollFocused(I)Landroid/widget/ListView$ArrowScrollFocusResult;

    #@1f
    move-result-object v1

    #@20
    .line 2437
    .local v1, focusResult:Landroid/widget/ListView$ArrowScrollFocusResult;
    :goto_20
    if-eqz v1, :cond_2a

    #@22
    .line 2438
    invoke-virtual {v1}, Landroid/widget/ListView$ArrowScrollFocusResult;->getSelectedPosition()I

    #@25
    move-result v4

    #@26
    .line 2439
    invoke-virtual {v1}, Landroid/widget/ListView$ArrowScrollFocusResult;->getAmountToScroll()I

    #@29
    move-result v0

    #@2a
    .line 2442
    :cond_2a
    if-eqz v1, :cond_ad

    #@2c
    move v3, v8

    #@2d
    .line 2443
    .local v3, needToRedraw:Z
    :goto_2d
    if-eq v4, v10, :cond_53

    #@2f
    .line 2444
    if-eqz v1, :cond_b0

    #@31
    move v7, v8

    #@32
    :goto_32
    invoke-direct {p0, v6, p1, v4, v7}, Landroid/widget/ListView;->handleNewSelectionChange(Landroid/view/View;IIZ)V

    #@35
    .line 2445
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setSelectedPositionInt(I)V

    #@38
    .line 2446
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setNextSelectedPositionInt(I)V

    #@3b
    .line 2447
    invoke-virtual {p0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    #@3e
    move-result-object v6

    #@3f
    .line 2448
    move v5, v4

    #@40
    .line 2449
    iget-boolean v7, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@42
    if-eqz v7, :cond_4f

    #@44
    if-nez v1, :cond_4f

    #@46
    .line 2452
    invoke-virtual {p0}, Landroid/widget/ListView;->getFocusedChild()Landroid/view/View;

    #@49
    move-result-object v2

    #@4a
    .line 2453
    .local v2, focused:Landroid/view/View;
    if-eqz v2, :cond_4f

    #@4c
    .line 2454
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    #@4f
    .line 2457
    .end local v2           #focused:Landroid/view/View;
    :cond_4f
    const/4 v3, 0x1

    #@50
    .line 2458
    invoke-virtual {p0}, Landroid/widget/ListView;->checkSelectionChanged()V

    #@53
    .line 2461
    :cond_53
    if-lez v0, :cond_5d

    #@55
    .line 2462
    const/16 v7, 0x21

    #@57
    if-ne p1, v7, :cond_b2

    #@59
    .end local v0           #amountToScroll:I
    :goto_59
    invoke-direct {p0, v0}, Landroid/widget/ListView;->scrollListItemsBy(I)V

    #@5c
    .line 2463
    const/4 v3, 0x1

    #@5d
    .line 2468
    :cond_5d
    iget-boolean v7, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@5f
    if-eqz v7, :cond_7e

    #@61
    if-nez v1, :cond_7e

    #@63
    if-eqz v6, :cond_7e

    #@65
    invoke-virtual {v6}, Landroid/view/View;->hasFocus()Z

    #@68
    move-result v7

    #@69
    if-eqz v7, :cond_7e

    #@6b
    .line 2470
    invoke-virtual {v6}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@6e
    move-result-object v2

    #@6f
    .line 2471
    .restart local v2       #focused:Landroid/view/View;
    invoke-direct {p0, v2, p0}, Landroid/widget/ListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    #@72
    move-result v7

    #@73
    if-eqz v7, :cond_7b

    #@75
    invoke-direct {p0, v2}, Landroid/widget/ListView;->distanceToView(Landroid/view/View;)I

    #@78
    move-result v7

    #@79
    if-lez v7, :cond_7e

    #@7b
    .line 2472
    :cond_7b
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    #@7e
    .line 2477
    .end local v2           #focused:Landroid/view/View;
    :cond_7e
    if-ne v4, v10, :cond_8e

    #@80
    if-eqz v6, :cond_8e

    #@82
    invoke-direct {p0, v6, p0}, Landroid/widget/ListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    #@85
    move-result v7

    #@86
    if-nez v7, :cond_8e

    #@88
    .line 2479
    const/4 v6, 0x0

    #@89
    .line 2480
    invoke-virtual {p0}, Landroid/widget/ListView;->hideSelector()V

    #@8c
    .line 2484
    iput v10, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@8e
    .line 2487
    :cond_8e
    if-eqz v3, :cond_9

    #@90
    .line 2488
    if-eqz v6, :cond_9b

    #@92
    .line 2489
    invoke-virtual {p0, v5, v6}, Landroid/widget/ListView;->positionSelector(ILandroid/view/View;)V

    #@95
    .line 2490
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@98
    move-result v7

    #@99
    iput v7, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@9b
    .line 2492
    :cond_9b
    invoke-virtual {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    #@9e
    move-result v7

    #@9f
    if-nez v7, :cond_a4

    #@a1
    .line 2493
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@a4
    .line 2495
    :cond_a4
    invoke-virtual {p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@a7
    move v9, v8

    #@a8
    .line 2496
    goto/16 :goto_9

    #@aa
    .line 2436
    .end local v1           #focusResult:Landroid/widget/ListView$ArrowScrollFocusResult;
    .end local v3           #needToRedraw:Z
    .restart local v0       #amountToScroll:I
    :cond_aa
    const/4 v1, 0x0

    #@ab
    goto/16 :goto_20

    #@ad
    .restart local v1       #focusResult:Landroid/widget/ListView$ArrowScrollFocusResult;
    :cond_ad
    move v3, v9

    #@ae
    .line 2442
    goto/16 :goto_2d

    #@b0
    .restart local v3       #needToRedraw:Z
    :cond_b0
    move v7, v9

    #@b1
    .line 2444
    goto :goto_32

    #@b2
    .line 2462
    :cond_b2
    neg-int v0, v0

    #@b3
    goto :goto_59
.end method

.method private clearRecycledState(Ljava/util/ArrayList;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 510
    .local p1, infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    if-eqz p1, :cond_1f

    #@2
    .line 511
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 513
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_1f

    #@9
    .line 514
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v4

    #@d
    check-cast v4, Landroid/widget/ListView$FixedViewInfo;

    #@f
    iget-object v0, v4, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@11
    .line 515
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Landroid/widget/AbsListView$LayoutParams;

    #@17
    .line 516
    .local v3, p:Landroid/widget/AbsListView$LayoutParams;
    if-eqz v3, :cond_1c

    #@19
    .line 517
    const/4 v4, 0x0

    #@1a
    iput-boolean v4, v3, Landroid/widget/AbsListView$LayoutParams;->recycledHeaderFooter:Z

    #@1c
    .line 513
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 521
    .end local v0           #child:Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_1f
    return-void
.end method

.method private commonKey(IILandroid/view/KeyEvent;)Z
    .registers 13
    .parameter "keyCode"
    .parameter "count"
    .parameter "event"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/16 v7, 0x82

    #@3
    const/16 v6, 0x21

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v4, 0x1

    #@7
    .line 2117
    iget-object v5, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@9
    if-eqz v5, :cond_f

    #@b
    iget-boolean v5, p0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@d
    if-nez v5, :cond_11

    #@f
    :cond_f
    move v4, v3

    #@10
    .line 2264
    :cond_10
    :goto_10
    return v4

    #@11
    .line 2121
    :cond_11
    iget-boolean v5, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@13
    if-eqz v5, :cond_18

    #@15
    .line 2122
    invoke-virtual {p0}, Landroid/widget/ListView;->layoutChildren()V

    #@18
    .line 2125
    :cond_18
    const/4 v2, 0x0

    #@19
    .line 2126
    .local v2, handled:Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@1c
    move-result v0

    #@1d
    .line 2128
    .local v0, action:I
    if-eq v0, v4, :cond_22

    #@1f
    .line 2129
    sparse-switch p1, :sswitch_data_19e

    #@22
    .line 2245
    :cond_22
    :goto_22
    if-nez v2, :cond_10

    #@24
    .line 2249
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/ListView;->sendToTextFilter(IILandroid/view/KeyEvent;)Z

    #@27
    move-result v5

    #@28
    if-nez v5, :cond_10

    #@2a
    .line 2253
    packed-switch v0, :pswitch_data_1cc

    #@2d
    move v4, v3

    #@2e
    .line 2264
    goto :goto_10

    #@2f
    .line 2131
    :sswitch_2f
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@32
    move-result v5

    #@33
    if-eqz v5, :cond_49

    #@35
    .line 2132
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@38
    move-result v2

    #@39
    .line 2133
    if-nez v2, :cond_22

    #@3b
    move v1, p2

    #@3c
    .line 2134
    .end local p2
    .local v1, count:I
    :goto_3c
    add-int/lit8 p2, v1, -0x1

    #@3e
    .end local v1           #count:I
    .restart local p2
    if-lez v1, :cond_22

    #@40
    .line 2135
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->arrowScroll(I)Z

    #@43
    move-result v5

    #@44
    if-eqz v5, :cond_22

    #@46
    .line 2136
    const/4 v2, 0x1

    #@47
    move v1, p2

    #@48
    .end local p2
    .restart local v1       #count:I
    goto :goto_3c

    #@49
    .line 2142
    .end local v1           #count:I
    .restart local p2
    :cond_49
    invoke-virtual {p3, v8}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_22

    #@4f
    .line 2143
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@52
    move-result v5

    #@53
    if-nez v5, :cond_5b

    #@55
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->fullScroll(I)Z

    #@58
    move-result v5

    #@59
    if-eqz v5, :cond_5d

    #@5b
    :cond_5b
    move v2, v4

    #@5c
    :goto_5c
    goto :goto_22

    #@5d
    :cond_5d
    move v2, v3

    #@5e
    goto :goto_5c

    #@5f
    .line 2148
    :sswitch_5f
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@62
    move-result v5

    #@63
    if-eqz v5, :cond_79

    #@65
    .line 2149
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@68
    move-result v2

    #@69
    .line 2150
    if-nez v2, :cond_22

    #@6b
    move v1, p2

    #@6c
    .line 2151
    .end local p2
    .restart local v1       #count:I
    :goto_6c
    add-int/lit8 p2, v1, -0x1

    #@6e
    .end local v1           #count:I
    .restart local p2
    if-lez v1, :cond_22

    #@70
    .line 2152
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->arrowScroll(I)Z

    #@73
    move-result v5

    #@74
    if-eqz v5, :cond_22

    #@76
    .line 2153
    const/4 v2, 0x1

    #@77
    move v1, p2

    #@78
    .end local p2
    .restart local v1       #count:I
    goto :goto_6c

    #@79
    .line 2159
    .end local v1           #count:I
    .restart local p2
    :cond_79
    invoke-virtual {p3, v8}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@7c
    move-result v5

    #@7d
    if-eqz v5, :cond_22

    #@7f
    .line 2160
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@82
    move-result v5

    #@83
    if-nez v5, :cond_8b

    #@85
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->fullScroll(I)Z

    #@88
    move-result v5

    #@89
    if-eqz v5, :cond_8d

    #@8b
    :cond_8b
    move v2, v4

    #@8c
    :goto_8c
    goto :goto_22

    #@8d
    :cond_8d
    move v2, v3

    #@8e
    goto :goto_8c

    #@8f
    .line 2165
    :sswitch_8f
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@92
    move-result v5

    #@93
    if-eqz v5, :cond_22

    #@95
    .line 2166
    const/16 v5, 0x11

    #@97
    invoke-direct {p0, v5}, Landroid/widget/ListView;->handleHorizontalFocusWithinListItem(I)Z

    #@9a
    move-result v2

    #@9b
    goto :goto_22

    #@9c
    .line 2171
    :sswitch_9c
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@9f
    move-result v5

    #@a0
    if-eqz v5, :cond_22

    #@a2
    .line 2172
    const/16 v5, 0x42

    #@a4
    invoke-direct {p0, v5}, Landroid/widget/ListView;->handleHorizontalFocusWithinListItem(I)Z

    #@a7
    move-result v2

    #@a8
    goto/16 :goto_22

    #@aa
    .line 2178
    :sswitch_aa
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@ad
    move-result v5

    #@ae
    if-eqz v5, :cond_22

    #@b0
    .line 2179
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@b3
    move-result v2

    #@b4
    .line 2180
    if-nez v2, :cond_22

    #@b6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@b9
    move-result v5

    #@ba
    if-nez v5, :cond_22

    #@bc
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@bf
    move-result v5

    #@c0
    if-lez v5, :cond_22

    #@c2
    .line 2182
    invoke-virtual {p0}, Landroid/widget/ListView;->keyPressed()V

    #@c5
    .line 2183
    const/4 v2, 0x1

    #@c6
    goto/16 :goto_22

    #@c8
    .line 2189
    :sswitch_c8
    iget-object v5, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@ca
    if-eqz v5, :cond_d4

    #@cc
    iget-object v5, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@ce
    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    #@d1
    move-result v5

    #@d2
    if-nez v5, :cond_22

    #@d4
    .line 2190
    :cond_d4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@d7
    move-result v5

    #@d8
    if-eqz v5, :cond_ec

    #@da
    .line 2191
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@dd
    move-result v5

    #@de
    if-nez v5, :cond_e6

    #@e0
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->pageScroll(I)Z

    #@e3
    move-result v5

    #@e4
    if-eqz v5, :cond_ea

    #@e6
    :cond_e6
    move v2, v4

    #@e7
    .line 2195
    :cond_e7
    :goto_e7
    const/4 v2, 0x1

    #@e8
    goto/16 :goto_22

    #@ea
    :cond_ea
    move v2, v3

    #@eb
    .line 2191
    goto :goto_e7

    #@ec
    .line 2192
    :cond_ec
    invoke-virtual {p3, v4}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@ef
    move-result v5

    #@f0
    if-eqz v5, :cond_e7

    #@f2
    .line 2193
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@f5
    move-result v5

    #@f6
    if-nez v5, :cond_fe

    #@f8
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->pageScroll(I)Z

    #@fb
    move-result v5

    #@fc
    if-eqz v5, :cond_100

    #@fe
    :cond_fe
    move v2, v4

    #@ff
    :goto_ff
    goto :goto_e7

    #@100
    :cond_100
    move v2, v3

    #@101
    goto :goto_ff

    #@102
    .line 2200
    :sswitch_102
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@105
    move-result v5

    #@106
    if-eqz v5, :cond_119

    #@108
    .line 2201
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@10b
    move-result v5

    #@10c
    if-nez v5, :cond_114

    #@10e
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->pageScroll(I)Z

    #@111
    move-result v5

    #@112
    if-eqz v5, :cond_117

    #@114
    :cond_114
    move v2, v4

    #@115
    :goto_115
    goto/16 :goto_22

    #@117
    :cond_117
    move v2, v3

    #@118
    goto :goto_115

    #@119
    .line 2202
    :cond_119
    invoke-virtual {p3, v8}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@11c
    move-result v5

    #@11d
    if-eqz v5, :cond_22

    #@11f
    .line 2203
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@122
    move-result v5

    #@123
    if-nez v5, :cond_12b

    #@125
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->fullScroll(I)Z

    #@128
    move-result v5

    #@129
    if-eqz v5, :cond_12e

    #@12b
    :cond_12b
    move v2, v4

    #@12c
    :goto_12c
    goto/16 :goto_22

    #@12e
    :cond_12e
    move v2, v3

    #@12f
    goto :goto_12c

    #@130
    .line 2208
    :sswitch_130
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@133
    move-result v5

    #@134
    if-eqz v5, :cond_147

    #@136
    .line 2209
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@139
    move-result v5

    #@13a
    if-nez v5, :cond_142

    #@13c
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->pageScroll(I)Z

    #@13f
    move-result v5

    #@140
    if-eqz v5, :cond_145

    #@142
    :cond_142
    move v2, v4

    #@143
    :goto_143
    goto/16 :goto_22

    #@145
    :cond_145
    move v2, v3

    #@146
    goto :goto_143

    #@147
    .line 2210
    :cond_147
    invoke-virtual {p3, v8}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@14a
    move-result v5

    #@14b
    if-eqz v5, :cond_22

    #@14d
    .line 2211
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@150
    move-result v5

    #@151
    if-nez v5, :cond_159

    #@153
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->fullScroll(I)Z

    #@156
    move-result v5

    #@157
    if-eqz v5, :cond_15c

    #@159
    :cond_159
    move v2, v4

    #@15a
    :goto_15a
    goto/16 :goto_22

    #@15c
    :cond_15c
    move v2, v3

    #@15d
    goto :goto_15a

    #@15e
    .line 2216
    :sswitch_15e
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@161
    move-result v5

    #@162
    if-eqz v5, :cond_22

    #@164
    .line 2217
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@167
    move-result v5

    #@168
    if-nez v5, :cond_170

    #@16a
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->fullScroll(I)Z

    #@16d
    move-result v5

    #@16e
    if-eqz v5, :cond_173

    #@170
    :cond_170
    move v2, v4

    #@171
    :goto_171
    goto/16 :goto_22

    #@173
    :cond_173
    move v2, v3

    #@174
    goto :goto_171

    #@175
    .line 2222
    :sswitch_175
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@178
    move-result v5

    #@179
    if-eqz v5, :cond_22

    #@17b
    .line 2223
    invoke-virtual {p0}, Landroid/widget/ListView;->resurrectSelectionIfNeeded()Z

    #@17e
    move-result v5

    #@17f
    if-nez v5, :cond_187

    #@181
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->fullScroll(I)Z

    #@184
    move-result v5

    #@185
    if-eqz v5, :cond_18a

    #@187
    :cond_187
    move v2, v4

    #@188
    :goto_188
    goto/16 :goto_22

    #@18a
    :cond_18a
    move v2, v3

    #@18b
    goto :goto_188

    #@18c
    .line 2255
    :pswitch_18c
    invoke-super {p0, p1, p3}, Landroid/widget/AbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@18f
    move-result v4

    #@190
    goto/16 :goto_10

    #@192
    .line 2258
    :pswitch_192
    invoke-super {p0, p1, p3}, Landroid/widget/AbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@195
    move-result v4

    #@196
    goto/16 :goto_10

    #@198
    .line 2261
    :pswitch_198
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsListView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@19b
    move-result v4

    #@19c
    goto/16 :goto_10

    #@19e
    .line 2129
    :sswitch_data_19e
    .sparse-switch
        0x13 -> :sswitch_2f
        0x14 -> :sswitch_5f
        0x15 -> :sswitch_8f
        0x16 -> :sswitch_9c
        0x17 -> :sswitch_aa
        0x3e -> :sswitch_c8
        0x42 -> :sswitch_aa
        0x5c -> :sswitch_102
        0x5d -> :sswitch_130
        0x7a -> :sswitch_15e
        0x7b -> :sswitch_175
    .end sparse-switch

    #@1cc
    .line 2253
    :pswitch_data_1cc
    .packed-switch 0x0
        :pswitch_18c
        :pswitch_192
        :pswitch_198
    .end packed-switch
.end method

.method private correctTooHigh(I)V
    .registers 12
    .parameter "childCount"

    #@0
    .prologue
    .line 1367
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    add-int/2addr v7, p1

    #@3
    add-int/lit8 v6, v7, -0x1

    #@5
    .line 1368
    .local v6, lastPosition:I
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    add-int/lit8 v7, v7, -0x1

    #@9
    if-ne v6, v7, :cond_5e

    #@b
    if-lez p1, :cond_5e

    #@d
    .line 1371
    add-int/lit8 v7, p1, -0x1

    #@f
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v5

    #@13
    .line 1374
    .local v5, lastChild:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@16
    move-result v4

    #@17
    .line 1377
    .local v4, lastBottom:I
    iget v7, p0, Landroid/view/View;->mBottom:I

    #@19
    iget v8, p0, Landroid/view/View;->mTop:I

    #@1b
    sub-int/2addr v7, v8

    #@1c
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1e
    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    #@20
    sub-int v1, v7, v8

    #@22
    .line 1381
    .local v1, end:I
    sub-int v0, v1, v4

    #@24
    .line 1382
    .local v0, bottomOffset:I
    const/4 v7, 0x0

    #@25
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    .line 1383
    .local v2, firstChild:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@2c
    move-result v3

    #@2d
    .line 1387
    .local v3, firstTop:I
    if-lez v0, :cond_5e

    #@2f
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@31
    if-gtz v7, :cond_39

    #@33
    iget-object v7, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@35
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@37
    if-ge v3, v7, :cond_5e

    #@39
    .line 1388
    :cond_39
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3b
    if-nez v7, :cond_46

    #@3d
    .line 1390
    iget-object v7, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3f
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@41
    sub-int/2addr v7, v3

    #@42
    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    #@45
    move-result v0

    #@46
    .line 1393
    :cond_46
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@49
    .line 1394
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4b
    if-lez v7, :cond_5e

    #@4d
    .line 1397
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4f
    add-int/lit8 v7, v7, -0x1

    #@51
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@54
    move-result v8

    #@55
    iget v9, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@57
    sub-int/2addr v8, v9

    #@58
    invoke-direct {p0, v7, v8}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@5b
    .line 1399
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@5e
    .line 1404
    .end local v0           #bottomOffset:I
    .end local v1           #end:I
    .end local v2           #firstChild:Landroid/view/View;
    .end local v3           #firstTop:I
    .end local v4           #lastBottom:I
    .end local v5           #lastChild:Landroid/view/View;
    :cond_5e
    return-void
.end method

.method private correctTooLow(I)V
    .registers 13
    .parameter "childCount"

    #@0
    .prologue
    .line 1416
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    if-nez v8, :cond_5e

    #@4
    if-lez p1, :cond_5e

    #@6
    .line 1419
    const/4 v8, 0x0

    #@7
    invoke-virtual {p0, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 1422
    .local v1, firstChild:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@e
    move-result v2

    #@f
    .line 1425
    .local v2, firstTop:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@11
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@13
    .line 1428
    .local v6, start:I
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@15
    iget v9, p0, Landroid/view/View;->mTop:I

    #@17
    sub-int/2addr v8, v9

    #@18
    iget-object v9, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1a
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@1c
    sub-int v0, v8, v9

    #@1e
    .line 1432
    .local v0, end:I
    sub-int v7, v2, v6

    #@20
    .line 1433
    .local v7, topOffset:I
    add-int/lit8 v8, p1, -0x1

    #@22
    invoke-virtual {p0, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@25
    move-result-object v4

    #@26
    .line 1434
    .local v4, lastChild:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@29
    move-result v3

    #@2a
    .line 1435
    .local v3, lastBottom:I
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2c
    add-int/2addr v8, p1

    #@2d
    add-int/lit8 v5, v8, -0x1

    #@2f
    .line 1439
    .local v5, lastPosition:I
    if-lez v7, :cond_5e

    #@31
    .line 1440
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@33
    add-int/lit8 v8, v8, -0x1

    #@35
    if-lt v5, v8, :cond_39

    #@37
    if-le v3, v0, :cond_5f

    #@39
    .line 1441
    :cond_39
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3b
    add-int/lit8 v8, v8, -0x1

    #@3d
    if-ne v5, v8, :cond_45

    #@3f
    .line 1443
    sub-int v8, v3, v0

    #@41
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    #@44
    move-result v7

    #@45
    .line 1446
    :cond_45
    neg-int v8, v7

    #@46
    invoke-virtual {p0, v8}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@49
    .line 1447
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@4b
    add-int/lit8 v8, v8, -0x1

    #@4d
    if-ge v5, v8, :cond_5e

    #@4f
    .line 1450
    add-int/lit8 v8, v5, 0x1

    #@51
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@54
    move-result v9

    #@55
    iget v10, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@57
    add-int/2addr v9, v10

    #@58
    invoke-direct {p0, v8, v9}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@5b
    .line 1452
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@5e
    .line 1459
    .end local v0           #end:I
    .end local v1           #firstChild:Landroid/view/View;
    .end local v2           #firstTop:I
    .end local v3           #lastBottom:I
    .end local v4           #lastChild:Landroid/view/View;
    .end local v5           #lastPosition:I
    .end local v6           #start:I
    .end local v7           #topOffset:I
    :cond_5e
    :goto_5e
    return-void

    #@5f
    .line 1454
    .restart local v0       #end:I
    .restart local v1       #firstChild:Landroid/view/View;
    .restart local v2       #firstTop:I
    .restart local v3       #lastBottom:I
    .restart local v4       #lastChild:Landroid/view/View;
    .restart local v5       #lastPosition:I
    .restart local v6       #start:I
    .restart local v7       #topOffset:I
    :cond_5f
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@61
    add-int/lit8 v8, v8, -0x1

    #@63
    if-ne v5, v8, :cond_5e

    #@65
    .line 1455
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@68
    goto :goto_5e
.end method

.method private distanceToView(Landroid/view/View;)I
    .registers 6
    .parameter "descendant"

    #@0
    .prologue
    .line 2930
    const/4 v0, 0x0

    #@1
    .line 2931
    .local v0, distance:I
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {p1, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@6
    .line 2932
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@8
    invoke-virtual {p0, p1, v2}, Landroid/widget/ListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@b
    .line 2933
    iget v2, p0, Landroid/view/View;->mBottom:I

    #@d
    iget v3, p0, Landroid/view/View;->mTop:I

    #@f
    sub-int/2addr v2, v3

    #@10
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@12
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@14
    sub-int v1, v2, v3

    #@16
    .line 2934
    .local v1, listBottom:I
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@18
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    #@1a
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1c
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@1e
    if-ge v2, v3, :cond_2b

    #@20
    .line 2935
    iget-object v2, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@22
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@24
    iget-object v3, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@26
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@28
    sub-int v0, v2, v3

    #@2a
    .line 2939
    :cond_2a
    :goto_2a
    return v0

    #@2b
    .line 2936
    :cond_2b
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@2d
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@2f
    if-le v2, v1, :cond_2a

    #@31
    .line 2937
    iget-object v2, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@33
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@35
    sub-int v0, v2, v1

    #@37
    goto :goto_2a
.end method

.method private fillAboveAndBelow(Landroid/view/View;I)V
    .registers 6
    .parameter "sel"
    .parameter "position"

    #@0
    .prologue
    .line 782
    iget v0, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@2
    .line 783
    .local v0, dividerHeight:I
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@4
    if-nez v1, :cond_1e

    #@6
    .line 784
    add-int/lit8 v1, p2, -0x1

    #@8
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@b
    move-result v2

    #@c
    sub-int/2addr v2, v0

    #@d
    invoke-direct {p0, v1, v2}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@10
    .line 785
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@13
    .line 786
    add-int/lit8 v1, p2, 0x1

    #@15
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@18
    move-result v2

    #@19
    add-int/2addr v2, v0

    #@1a
    invoke-direct {p0, v1, v2}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@1d
    .line 792
    :goto_1d
    return-void

    #@1e
    .line 788
    :cond_1e
    add-int/lit8 v1, p2, 0x1

    #@20
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@23
    move-result v2

    #@24
    add-int/2addr v2, v0

    #@25
    invoke-direct {p0, v1, v2}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@28
    .line 789
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@2b
    .line 790
    add-int/lit8 v1, p2, -0x1

    #@2d
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@30
    move-result v2

    #@31
    sub-int/2addr v2, v0

    #@32
    invoke-direct {p0, v1, v2}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@35
    goto :goto_1d
.end method

.method private fillDown(II)Landroid/view/View;
    .registers 12
    .parameter "pos"
    .parameter "nextTop"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 664
    const/4 v8, 0x0

    #@2
    .line 666
    .local v8, selectedView:Landroid/view/View;
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@4
    iget v1, p0, Landroid/view/View;->mTop:I

    #@6
    sub-int v7, v0, v1

    #@8
    .line 667
    .local v7, end:I
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@a
    and-int/lit8 v0, v0, 0x22

    #@c
    const/16 v1, 0x22

    #@e
    if-ne v0, v1, :cond_15

    #@10
    .line 668
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@12
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@14
    sub-int/2addr v7, v0

    #@15
    .line 671
    :cond_15
    :goto_15
    if-gt p2, v7, :cond_3b

    #@17
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@19
    if-ge p1, v0, :cond_3b

    #@1b
    .line 673
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1d
    if-ne p1, v0, :cond_39

    #@1f
    move v5, v3

    #@20
    .line 674
    .local v5, selected:Z
    :goto_20
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@22
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@24
    move-object v0, p0

    #@25
    move v1, p1

    #@26
    move v2, p2

    #@27
    invoke-direct/range {v0 .. v5}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@2a
    move-result-object v6

    #@2b
    .line 676
    .local v6, child:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@2e
    move-result v0

    #@2f
    iget v1, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@31
    add-int p2, v0, v1

    #@33
    .line 677
    if-eqz v5, :cond_36

    #@35
    .line 678
    move-object v8, v6

    #@36
    .line 680
    :cond_36
    add-int/lit8 p1, p1, 0x1

    #@38
    .line 681
    goto :goto_15

    #@39
    .line 673
    .end local v5           #selected:Z
    .end local v6           #child:Landroid/view/View;
    :cond_39
    const/4 v5, 0x0

    #@3a
    goto :goto_20

    #@3b
    .line 683
    :cond_3b
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3d
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3f
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@42
    move-result v2

    #@43
    add-int/2addr v1, v2

    #@44
    add-int/lit8 v1, v1, -0x1

    #@46
    invoke-virtual {p0, v0, v1}, Landroid/widget/ListView;->setVisibleRangeHint(II)V

    #@49
    .line 684
    return-object v8
.end method

.method private fillFromMiddle(II)Landroid/view/View;
    .registers 12
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 750
    sub-int v6, p2, p1

    #@3
    .line 752
    .local v6, height:I
    invoke-virtual {p0}, Landroid/widget/ListView;->reconcileSelectedPosition()I

    #@6
    move-result v1

    #@7
    .line 754
    .local v1, position:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@9
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@b
    move-object v0, p0

    #@c
    move v2, p1

    #@d
    move v5, v3

    #@e
    invoke-direct/range {v0 .. v5}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@11
    move-result-object v7

    #@12
    .line 756
    .local v7, sel:Landroid/view/View;
    iput v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@14
    .line 758
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    #@17
    move-result v8

    #@18
    .line 759
    .local v8, selHeight:I
    if-gt v8, v6, :cond_21

    #@1a
    .line 760
    sub-int v0, v6, v8

    #@1c
    div-int/lit8 v0, v0, 0x2

    #@1e
    invoke-virtual {v7, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@21
    .line 763
    :cond_21
    invoke-direct {p0, v7, v1}, Landroid/widget/ListView;->fillAboveAndBelow(Landroid/view/View;I)V

    #@24
    .line 765
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@26
    if-nez v0, :cond_30

    #@28
    .line 766
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@2b
    move-result v0

    #@2c
    invoke-direct {p0, v0}, Landroid/widget/ListView;->correctTooHigh(I)V

    #@2f
    .line 771
    :goto_2f
    return-object v7

    #@30
    .line 768
    :cond_30
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@33
    move-result v0

    #@34
    invoke-direct {p0, v0}, Landroid/widget/ListView;->correctTooLow(I)V

    #@37
    goto :goto_2f
.end method

.method private fillFromSelection(III)Landroid/view/View;
    .registers 18
    .parameter "selectedTop"
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 806
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@3
    move-result v8

    #@4
    .line 807
    .local v8, fadingEdgeLength:I
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    .line 811
    .local v2, selectedPosition:I
    move/from16 v0, p2

    #@8
    invoke-direct {p0, v0, v8, v2}, Landroid/widget/ListView;->getTopSelectionPixel(III)I

    #@b
    move-result v13

    #@c
    .line 813
    .local v13, topSelectionPixel:I
    move/from16 v0, p3

    #@e
    invoke-direct {p0, v0, v8, v2}, Landroid/widget/ListView;->getBottomSelectionPixel(III)I

    #@11
    move-result v7

    #@12
    .line 816
    .local v7, bottomSelectionPixel:I
    const/4 v4, 0x1

    #@13
    iget-object v1, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@15
    iget v5, v1, Landroid/graphics/Rect;->left:I

    #@17
    const/4 v6, 0x1

    #@18
    move-object v1, p0

    #@19
    move v3, p1

    #@1a
    invoke-direct/range {v1 .. v6}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@1d
    move-result-object v10

    #@1e
    .line 820
    .local v10, sel:Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@21
    move-result v1

    #@22
    if-le v1, v7, :cond_47

    #@24
    .line 823
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@27
    move-result v1

    #@28
    sub-int v11, v1, v13

    #@2a
    .line 827
    .local v11, spaceAbove:I
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@2d
    move-result v1

    #@2e
    sub-int v12, v1, v7

    #@30
    .line 828
    .local v12, spaceBelow:I
    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    #@33
    move-result v9

    #@34
    .line 831
    .local v9, offset:I
    neg-int v1, v9

    #@35
    invoke-virtual {v10, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@38
    .line 847
    .end local v9           #offset:I
    .end local v11           #spaceAbove:I
    .end local v12           #spaceBelow:I
    :cond_38
    :goto_38
    invoke-direct {p0, v10, v2}, Landroid/widget/ListView;->fillAboveAndBelow(Landroid/view/View;I)V

    #@3b
    .line 849
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@3d
    if-nez v1, :cond_61

    #@3f
    .line 850
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@42
    move-result v1

    #@43
    invoke-direct {p0, v1}, Landroid/widget/ListView;->correctTooHigh(I)V

    #@46
    .line 855
    :goto_46
    return-object v10

    #@47
    .line 832
    :cond_47
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@4a
    move-result v1

    #@4b
    if-ge v1, v13, :cond_38

    #@4d
    .line 835
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@50
    move-result v1

    #@51
    sub-int v11, v13, v1

    #@53
    .line 839
    .restart local v11       #spaceAbove:I
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@56
    move-result v1

    #@57
    sub-int v12, v7, v1

    #@59
    .line 840
    .restart local v12       #spaceBelow:I
    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    #@5c
    move-result v9

    #@5d
    .line 843
    .restart local v9       #offset:I
    invoke-virtual {v10, v9}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@60
    goto :goto_38

    #@61
    .line 852
    .end local v9           #offset:I
    .end local v11           #spaceAbove:I
    .end local v12           #spaceBelow:I
    :cond_61
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@64
    move-result v1

    #@65
    invoke-direct {p0, v1}, Landroid/widget/ListView;->correctTooLow(I)V

    #@68
    goto :goto_46
.end method

.method private fillFromTop(I)Landroid/view/View;
    .registers 4
    .parameter "nextTop"

    #@0
    .prologue
    .line 730
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@4
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@a
    .line 731
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@e
    add-int/lit8 v1, v1, -0x1

    #@10
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@16
    .line 732
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@18
    if-gez v0, :cond_1d

    #@1a
    .line 733
    const/4 v0, 0x0

    #@1b
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1d
    .line 735
    :cond_1d
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1f
    invoke-direct {p0, v0, p1}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@22
    move-result-object v0

    #@23
    return-object v0
.end method

.method private fillSpecific(II)Landroid/view/View;
    .registers 14
    .parameter "position"
    .parameter "top"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1319
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3
    if-ne p1, v0, :cond_3e

    #@5
    move v5, v3

    #@6
    .line 1320
    .local v5, tempIsSelected:Z
    :goto_6
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@8
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@a
    move-object v0, p0

    #@b
    move v1, p1

    #@c
    move v2, p2

    #@d
    invoke-direct/range {v0 .. v5}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@10
    move-result-object v10

    #@11
    .line 1322
    .local v10, temp:Landroid/view/View;
    iput p1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@13
    .line 1327
    iget v9, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@15
    .line 1328
    .local v9, dividerHeight:I
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@17
    if-nez v0, :cond_40

    #@19
    .line 1329
    add-int/lit8 v0, p1, -0x1

    #@1b
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@1e
    move-result v1

    #@1f
    sub-int/2addr v1, v9

    #@20
    invoke-direct {p0, v0, v1}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@23
    move-result-object v6

    #@24
    .line 1331
    .local v6, above:Landroid/view/View;
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@27
    .line 1332
    add-int/lit8 v0, p1, 0x1

    #@29
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@2c
    move-result v1

    #@2d
    add-int/2addr v1, v9

    #@2e
    invoke-direct {p0, v0, v1}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@31
    move-result-object v7

    #@32
    .line 1333
    .local v7, below:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@35
    move-result v8

    #@36
    .line 1334
    .local v8, childCount:I
    if-lez v8, :cond_3b

    #@38
    .line 1335
    invoke-direct {p0, v8}, Landroid/widget/ListView;->correctTooHigh(I)V

    #@3b
    .line 1348
    :cond_3b
    :goto_3b
    if-eqz v5, :cond_63

    #@3d
    .line 1353
    .end local v10           #temp:Landroid/view/View;
    :goto_3d
    return-object v10

    #@3e
    .line 1319
    .end local v5           #tempIsSelected:Z
    .end local v6           #above:Landroid/view/View;
    .end local v7           #below:Landroid/view/View;
    .end local v8           #childCount:I
    .end local v9           #dividerHeight:I
    :cond_3e
    const/4 v5, 0x0

    #@3f
    goto :goto_6

    #@40
    .line 1338
    .restart local v5       #tempIsSelected:Z
    .restart local v9       #dividerHeight:I
    .restart local v10       #temp:Landroid/view/View;
    :cond_40
    add-int/lit8 v0, p1, 0x1

    #@42
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@45
    move-result v1

    #@46
    add-int/2addr v1, v9

    #@47
    invoke-direct {p0, v0, v1}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@4a
    move-result-object v7

    #@4b
    .line 1340
    .restart local v7       #below:Landroid/view/View;
    invoke-direct {p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@4e
    .line 1341
    add-int/lit8 v0, p1, -0x1

    #@50
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@53
    move-result v1

    #@54
    sub-int/2addr v1, v9

    #@55
    invoke-direct {p0, v0, v1}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@58
    move-result-object v6

    #@59
    .line 1342
    .restart local v6       #above:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@5c
    move-result v8

    #@5d
    .line 1343
    .restart local v8       #childCount:I
    if-lez v8, :cond_3b

    #@5f
    .line 1344
    invoke-direct {p0, v8}, Landroid/widget/ListView;->correctTooLow(I)V

    #@62
    goto :goto_3b

    #@63
    .line 1350
    :cond_63
    if-eqz v6, :cond_67

    #@65
    move-object v10, v6

    #@66
    .line 1351
    goto :goto_3d

    #@67
    :cond_67
    move-object v10, v7

    #@68
    .line 1353
    goto :goto_3d
.end method

.method private fillUp(II)Landroid/view/View;
    .registers 12
    .parameter "pos"
    .parameter "nextBottom"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 698
    const/4 v8, 0x0

    #@2
    .line 700
    .local v8, selectedView:Landroid/view/View;
    const/4 v7, 0x0

    #@3
    .line 701
    .local v7, end:I
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5
    and-int/lit8 v0, v0, 0x22

    #@7
    const/16 v1, 0x22

    #@9
    if-ne v0, v1, :cond_f

    #@b
    .line 702
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@d
    iget v7, v0, Landroid/graphics/Rect;->top:I

    #@f
    .line 705
    :cond_f
    :goto_f
    if-lt p2, v7, :cond_33

    #@11
    if-ltz p1, :cond_33

    #@13
    .line 707
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@15
    if-ne p1, v0, :cond_31

    #@17
    const/4 v5, 0x1

    #@18
    .line 708
    .local v5, selected:Z
    :goto_18
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1a
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@1c
    move-object v0, p0

    #@1d
    move v1, p1

    #@1e
    move v2, p2

    #@1f
    invoke-direct/range {v0 .. v5}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@22
    move-result-object v6

    #@23
    .line 709
    .local v6, child:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@26
    move-result v0

    #@27
    iget v1, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@29
    sub-int p2, v0, v1

    #@2b
    .line 710
    if-eqz v5, :cond_2e

    #@2d
    .line 711
    move-object v8, v6

    #@2e
    .line 713
    :cond_2e
    add-int/lit8 p1, p1, -0x1

    #@30
    .line 714
    goto :goto_f

    #@31
    .end local v5           #selected:Z
    .end local v6           #child:Landroid/view/View;
    :cond_31
    move v5, v3

    #@32
    .line 707
    goto :goto_18

    #@33
    .line 716
    :cond_33
    add-int/lit8 v0, p1, 0x1

    #@35
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@37
    .line 717
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@39
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3b
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3e
    move-result v2

    #@3f
    add-int/2addr v1, v2

    #@40
    add-int/lit8 v1, v1, -0x1

    #@42
    invoke-virtual {p0, v0, v1}, Landroid/widget/ListView;->setVisibleRangeHint(II)V

    #@45
    .line 718
    return-object v8
.end method

.method private findAccessibilityFocusedChild(Landroid/view/View;)Landroid/view/View;
    .registers 4
    .parameter "focusedView"

    #@0
    .prologue
    .line 1766
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v0

    #@4
    .line 1767
    .local v0, viewParent:Landroid/view/ViewParent;
    :goto_4
    instance-of v1, v0, Landroid/view/View;

    #@6
    if-eqz v1, :cond_12

    #@8
    if-eq v0, p0, :cond_12

    #@a
    move-object p1, v0

    #@b
    .line 1768
    check-cast p1, Landroid/view/View;

    #@d
    .line 1769
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    #@10
    move-result-object v0

    #@11
    goto :goto_4

    #@12
    .line 1771
    :cond_12
    instance-of v1, v0, Landroid/view/View;

    #@14
    if-nez v1, :cond_17

    #@16
    .line 1772
    const/4 p1, 0x0

    #@17
    .line 1774
    .end local p1
    :cond_17
    return-object p1
.end method

.method private getArrowScrollPreviewLength()I
    .registers 3

    #@0
    .prologue
    .line 2625
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@4
    move-result v1

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method private getBottomSelectionPixel(III)I
    .registers 6
    .parameter "childrenBottom"
    .parameter "fadingEdgeLength"
    .parameter "selectedPosition"

    #@0
    .prologue
    .line 868
    move v0, p1

    #@1
    .line 869
    .local v0, bottomSelectionPixel:I
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3
    add-int/lit8 v1, v1, -0x1

    #@5
    if-eq p3, v1, :cond_8

    #@7
    .line 870
    sub-int/2addr v0, p2

    #@8
    .line 872
    :cond_8
    return v0
.end method

.method private getTopSelectionPixel(III)I
    .registers 5
    .parameter "childrenTop"
    .parameter "fadingEdgeLength"
    .parameter "selectedPosition"

    #@0
    .prologue
    .line 885
    move v0, p1

    #@1
    .line 886
    .local v0, topSelectionPixel:I
    if-lez p3, :cond_4

    #@3
    .line 887
    add-int/2addr v0, p2

    #@4
    .line 889
    :cond_4
    return v0
.end method

.method private handleHorizontalFocusWithinListItem(I)Z
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    .line 2360
    const/16 v5, 0x11

    #@2
    if-eq p1, v5, :cond_10

    #@4
    const/16 v5, 0x42

    #@6
    if-eq p1, v5, :cond_10

    #@8
    .line 2361
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v6, "direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT}"

    #@c
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v5

    #@10
    .line 2365
    :cond_10
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@13
    move-result v3

    #@14
    .line 2366
    .local v3, numChildren:I
    iget-boolean v5, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@16
    if-eqz v5, :cond_6d

    #@18
    if-lez v3, :cond_6d

    #@1a
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1c
    const/4 v6, -0x1

    #@1d
    if-eq v5, v6, :cond_6d

    #@1f
    .line 2367
    invoke-virtual {p0}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    #@22
    move-result-object v4

    #@23
    .line 2368
    .local v4, selectedView:Landroid/view/View;
    if-eqz v4, :cond_6d

    #@25
    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_6d

    #@2b
    instance-of v5, v4, Landroid/view/ViewGroup;

    #@2d
    if-eqz v5, :cond_6d

    #@2f
    .line 2371
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@32
    move-result-object v0

    #@33
    .line 2372
    .local v0, currentFocus:Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@36
    move-result-object v5

    #@37
    check-cast v4, Landroid/view/ViewGroup;

    #@39
    .end local v4           #selectedView:Landroid/view/View;
    invoke-virtual {v5, v4, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@3c
    move-result-object v2

    #@3d
    .line 2374
    .local v2, nextFocus:Landroid/view/View;
    if-eqz v2, :cond_58

    #@3f
    .line 2376
    iget-object v5, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@41
    invoke-virtual {v0, v5}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@44
    .line 2377
    iget-object v5, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@46
    invoke-virtual {p0, v0, v5}, Landroid/widget/ListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@49
    .line 2378
    iget-object v5, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@4b
    invoke-virtual {p0, v2, v5}, Landroid/widget/ListView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@4e
    .line 2379
    iget-object v5, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@50
    invoke-virtual {v2, p1, v5}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@53
    move-result v5

    #@54
    if-eqz v5, :cond_58

    #@56
    .line 2380
    const/4 v5, 0x1

    #@57
    .line 2394
    .end local v0           #currentFocus:Landroid/view/View;
    .end local v2           #nextFocus:Landroid/view/View;
    :goto_57
    return v5

    #@58
    .line 2387
    .restart local v0       #currentFocus:Landroid/view/View;
    .restart local v2       #nextFocus:Landroid/view/View;
    :cond_58
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {p0}, Landroid/widget/ListView;->getRootView()Landroid/view/View;

    #@5f
    move-result-object v5

    #@60
    check-cast v5, Landroid/view/ViewGroup;

    #@62
    invoke-virtual {v6, v5, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@65
    move-result-object v1

    #@66
    .line 2389
    .local v1, globalNextFocus:Landroid/view/View;
    if-eqz v1, :cond_6d

    #@68
    .line 2390
    invoke-direct {p0, v1, p0}, Landroid/widget/ListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    #@6b
    move-result v5

    #@6c
    goto :goto_57

    #@6d
    .line 2394
    .end local v0           #currentFocus:Landroid/view/View;
    .end local v1           #globalNextFocus:Landroid/view/View;
    .end local v2           #nextFocus:Landroid/view/View;
    :cond_6d
    const/4 v5, 0x0

    #@6e
    goto :goto_57
.end method

.method private handleNewSelectionChange(Landroid/view/View;IIZ)V
    .registers 15
    .parameter "selectedView"
    .parameter "direction"
    .parameter "newSelectedPosition"
    .parameter "newFocusAssigned"

    #@0
    .prologue
    .line 2517
    const/4 v8, -0x1

    #@1
    if-ne p3, v8, :cond_c

    #@3
    .line 2518
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v9, "newSelectedPosition needs to be valid"

    #@8
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v8

    #@c
    .line 2528
    :cond_c
    const/4 v5, 0x0

    #@d
    .line 2529
    .local v5, topSelected:Z
    iget v8, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@f
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@11
    sub-int v4, v8, v9

    #@13
    .line 2530
    .local v4, selectedIndex:I
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@15
    sub-int v2, p3, v8

    #@17
    .line 2531
    .local v2, nextSelectedIndex:I
    const/16 v8, 0x21

    #@19
    if-ne p2, v8, :cond_42

    #@1b
    .line 2532
    move v7, v2

    #@1c
    .line 2533
    .local v7, topViewIndex:I
    move v1, v4

    #@1d
    .line 2534
    .local v1, bottomViewIndex:I
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v6

    #@21
    .line 2535
    .local v6, topView:Landroid/view/View;
    move-object v0, p1

    #@22
    .line 2536
    .local v0, bottomView:Landroid/view/View;
    const/4 v5, 0x1

    #@23
    .line 2544
    :goto_23
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@26
    move-result v3

    #@27
    .line 2547
    .local v3, numChildren:I
    if-eqz v6, :cond_34

    #@29
    .line 2548
    if-nez p4, :cond_4a

    #@2b
    if-eqz v5, :cond_4a

    #@2d
    const/4 v8, 0x1

    #@2e
    :goto_2e
    invoke-virtual {v6, v8}, Landroid/view/View;->setSelected(Z)V

    #@31
    .line 2549
    invoke-direct {p0, v6, v7, v3}, Landroid/widget/ListView;->measureAndAdjustDown(Landroid/view/View;II)V

    #@34
    .line 2553
    :cond_34
    if-eqz v0, :cond_41

    #@36
    .line 2554
    if-nez p4, :cond_4c

    #@38
    if-nez v5, :cond_4c

    #@3a
    const/4 v8, 0x1

    #@3b
    :goto_3b
    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    #@3e
    .line 2555
    invoke-direct {p0, v0, v1, v3}, Landroid/widget/ListView;->measureAndAdjustDown(Landroid/view/View;II)V

    #@41
    .line 2557
    :cond_41
    return-void

    #@42
    .line 2538
    .end local v0           #bottomView:Landroid/view/View;
    .end local v1           #bottomViewIndex:I
    .end local v3           #numChildren:I
    .end local v6           #topView:Landroid/view/View;
    .end local v7           #topViewIndex:I
    :cond_42
    move v7, v4

    #@43
    .line 2539
    .restart local v7       #topViewIndex:I
    move v1, v2

    #@44
    .line 2540
    .restart local v1       #bottomViewIndex:I
    move-object v6, p1

    #@45
    .line 2541
    .restart local v6       #topView:Landroid/view/View;
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@48
    move-result-object v0

    #@49
    .restart local v0       #bottomView:Landroid/view/View;
    goto :goto_23

    #@4a
    .line 2548
    .restart local v3       #numChildren:I
    :cond_4a
    const/4 v8, 0x0

    #@4b
    goto :goto_2e

    #@4c
    .line 2554
    :cond_4c
    const/4 v8, 0x0

    #@4d
    goto :goto_3b
.end method

.method private isDirectChildHeaderOrFooter(Landroid/view/View;)Z
    .registers 9
    .parameter "child"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1783
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@3
    .line 1784
    .local v1, headers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v4

    #@7
    .line 1785
    .local v4, numHeaders:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v4, :cond_19

    #@a
    .line 1786
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v5

    #@e
    check-cast v5, Landroid/widget/ListView$FixedViewInfo;

    #@10
    iget-object v5, v5, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@12
    if-ne p1, v5, :cond_16

    #@14
    move v5, v6

    #@15
    .line 1797
    :goto_15
    return v5

    #@16
    .line 1785
    :cond_16
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_8

    #@19
    .line 1790
    :cond_19
    iget-object v0, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@1b
    .line 1791
    .local v0, footers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v3

    #@1f
    .line 1792
    .local v3, numFooters:I
    const/4 v2, 0x0

    #@20
    :goto_20
    if-ge v2, v3, :cond_31

    #@22
    .line 1793
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/widget/ListView$FixedViewInfo;

    #@28
    iget-object v5, v5, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@2a
    if-ne p1, v5, :cond_2e

    #@2c
    move v5, v6

    #@2d
    .line 1794
    goto :goto_15

    #@2e
    .line 1792
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_20

    #@31
    .line 1797
    :cond_31
    const/4 v5, 0x0

    #@32
    goto :goto_15
.end method

.method private isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z
    .registers 6
    .parameter "child"
    .parameter "parent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2882
    if-ne p1, p2, :cond_4

    #@3
    .line 2887
    :cond_3
    :goto_3
    return v1

    #@4
    .line 2886
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@7
    move-result-object v0

    #@8
    .line 2887
    .local v0, theParent:Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@a
    if-eqz v2, :cond_14

    #@c
    check-cast v0, Landroid/view/View;

    #@e
    .end local v0           #theParent:Landroid/view/ViewParent;
    invoke-direct {p0, v0, p2}, Landroid/widget/ListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_3

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_3
.end method

.method private lookForSelectablePositionOnScreen(I)I
    .registers 10
    .parameter "direction"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 2745
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3
    .line 2746
    .local v1, firstPosition:I
    const/16 v7, 0x82

    #@5
    if-ne p1, v7, :cond_3e

    #@7
    .line 2747
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@9
    if-eq v7, v6, :cond_19

    #@b
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@d
    add-int/lit8 v5, v7, 0x1

    #@f
    .line 2750
    .local v5, startPos:I
    :goto_f
    iget-object v7, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@11
    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    #@14
    move-result v7

    #@15
    if-lt v5, v7, :cond_1b

    #@17
    move v4, v6

    #@18
    .line 2785
    :cond_18
    :goto_18
    return v4

    #@19
    .end local v5           #startPos:I
    :cond_19
    move v5, v1

    #@1a
    .line 2747
    goto :goto_f

    #@1b
    .line 2753
    .restart local v5       #startPos:I
    :cond_1b
    if-ge v5, v1, :cond_1e

    #@1d
    .line 2754
    move v5, v1

    #@1e
    .line 2757
    :cond_1e
    invoke-virtual {p0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    #@21
    move-result v3

    #@22
    .line 2758
    .local v3, lastVisiblePos:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    #@25
    move-result-object v0

    #@26
    .line 2759
    .local v0, adapter:Landroid/widget/ListAdapter;
    move v4, v5

    #@27
    .local v4, pos:I
    :goto_27
    if-gt v4, v3, :cond_80

    #@29
    .line 2760
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@2c
    move-result v7

    #@2d
    if-eqz v7, :cond_3b

    #@2f
    sub-int v7, v4, v1

    #@31
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    #@38
    move-result v7

    #@39
    if-eqz v7, :cond_18

    #@3b
    .line 2759
    :cond_3b
    add-int/lit8 v4, v4, 0x1

    #@3d
    goto :goto_27

    #@3e
    .line 2766
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v3           #lastVisiblePos:I
    .end local v4           #pos:I
    .end local v5           #startPos:I
    :cond_3e
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@41
    move-result v7

    #@42
    add-int/2addr v7, v1

    #@43
    add-int/lit8 v2, v7, -0x1

    #@45
    .line 2767
    .local v2, last:I
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@47
    if-eq v7, v6, :cond_59

    #@49
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@4b
    add-int/lit8 v5, v7, -0x1

    #@4d
    .line 2770
    .restart local v5       #startPos:I
    :goto_4d
    if-ltz v5, :cond_57

    #@4f
    iget-object v7, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@51
    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    #@54
    move-result v7

    #@55
    if-lt v5, v7, :cond_61

    #@57
    :cond_57
    move v4, v6

    #@58
    .line 2771
    goto :goto_18

    #@59
    .line 2767
    .end local v5           #startPos:I
    :cond_59
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@5c
    move-result v7

    #@5d
    add-int/2addr v7, v1

    #@5e
    add-int/lit8 v5, v7, -0x1

    #@60
    goto :goto_4d

    #@61
    .line 2773
    .restart local v5       #startPos:I
    :cond_61
    if-le v5, v2, :cond_64

    #@63
    .line 2774
    move v5, v2

    #@64
    .line 2777
    :cond_64
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    #@67
    move-result-object v0

    #@68
    .line 2778
    .restart local v0       #adapter:Landroid/widget/ListAdapter;
    move v4, v5

    #@69
    .restart local v4       #pos:I
    :goto_69
    if-lt v4, v1, :cond_80

    #@6b
    .line 2779
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@6e
    move-result v7

    #@6f
    if-eqz v7, :cond_7d

    #@71
    sub-int v7, v4, v1

    #@73
    invoke-virtual {p0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    #@7a
    move-result v7

    #@7b
    if-eqz v7, :cond_18

    #@7d
    .line 2778
    :cond_7d
    add-int/lit8 v4, v4, -0x1

    #@7f
    goto :goto_69

    #@80
    .end local v2           #last:I
    :cond_80
    move v4, v6

    #@81
    .line 2785
    goto :goto_18
.end method

.method private makeAndAddView(IIZIZ)Landroid/view/View;
    .registers 15
    .parameter "position"
    .parameter "y"
    .parameter "flow"
    .parameter "childrenLeft"
    .parameter "selected"

    #@0
    .prologue
    .line 1818
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2
    if-nez v0, :cond_18

    #@4
    .line 1820
    iget-object v0, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView$RecycleBin;->getActiveView(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    .line 1821
    .local v1, child:Landroid/view/View;
    if-eqz v1, :cond_18

    #@c
    .line 1824
    const/4 v7, 0x1

    #@d
    move-object v0, p0

    #@e
    move v2, p1

    #@f
    move v3, p2

    #@10
    move v4, p3

    #@11
    move v5, p4

    #@12
    move v6, p5

    #@13
    invoke-direct/range {v0 .. v7}, Landroid/widget/ListView;->setupChild(Landroid/view/View;IIZIZZ)V

    #@16
    move-object v8, v1

    #@17
    .line 1836
    .end local v1           #child:Landroid/view/View;
    .local v8, child:Landroid/view/View;
    :goto_17
    return-object v8

    #@18
    .line 1831
    .end local v8           #child:Landroid/view/View;
    :cond_18
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@1a
    invoke-virtual {p0, p1, v0}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@1d
    move-result-object v1

    #@1e
    .line 1834
    .restart local v1       #child:Landroid/view/View;
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@20
    const/4 v2, 0x0

    #@21
    aget-boolean v7, v0, v2

    #@23
    move-object v0, p0

    #@24
    move v2, p1

    #@25
    move v3, p2

    #@26
    move v4, p3

    #@27
    move v5, p4

    #@28
    move v6, p5

    #@29
    invoke-direct/range {v0 .. v7}, Landroid/widget/ListView;->setupChild(Landroid/view/View;IIZIZZ)V

    #@2c
    move-object v8, v1

    #@2d
    .line 1836
    .end local v1           #child:Landroid/view/View;
    .restart local v8       #child:Landroid/view/View;
    goto :goto_17
.end method

.method private measureAndAdjustDown(Landroid/view/View;II)V
    .registers 8
    .parameter "child"
    .parameter "childIndex"
    .parameter "numChildren"

    #@0
    .prologue
    .line 2567
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@3
    move-result v2

    #@4
    .line 2568
    .local v2, oldHeight:I
    invoke-direct {p0, p1}, Landroid/widget/ListView;->measureItem(Landroid/view/View;)V

    #@7
    .line 2569
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@a
    move-result v3

    #@b
    if-eq v3, v2, :cond_24

    #@d
    .line 2571
    invoke-direct {p0, p1}, Landroid/widget/ListView;->relayoutMeasuredItem(Landroid/view/View;)V

    #@10
    .line 2574
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@13
    move-result v3

    #@14
    sub-int v0, v3, v2

    #@16
    .line 2575
    .local v0, heightDelta:I
    add-int/lit8 v1, p2, 0x1

    #@18
    .local v1, i:I
    :goto_18
    if-ge v1, p3, :cond_24

    #@1a
    .line 2576
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@21
    .line 2575
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_18

    #@24
    .line 2579
    .end local v0           #heightDelta:I
    .end local v1           #i:I
    :cond_24
    return-void
.end method

.method private measureItem(Landroid/view/View;)V
    .registers 10
    .parameter "child"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2587
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v3

    #@5
    .line 2588
    .local v3, p:Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_e

    #@7
    .line 2589
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    #@9
    .end local v3           #p:Landroid/view/ViewGroup$LayoutParams;
    const/4 v4, -0x1

    #@a
    const/4 v5, -0x2

    #@b
    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@e
    .line 2594
    .restart local v3       #p:Landroid/view/ViewGroup$LayoutParams;
    :cond_e
    iget v4, p0, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@10
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@12
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@14
    iget-object v6, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@16
    iget v6, v6, Landroid/graphics/Rect;->right:I

    #@18
    add-int/2addr v5, v6

    #@19
    iget v6, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1b
    invoke-static {v4, v5, v6}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@1e
    move-result v1

    #@1f
    .line 2596
    .local v1, childWidthSpec:I
    iget v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@21
    .line 2598
    .local v2, lpHeight:I
    if-lez v2, :cond_2d

    #@23
    .line 2599
    const/high16 v4, 0x4000

    #@25
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@28
    move-result v0

    #@29
    .line 2603
    .local v0, childHeightSpec:I
    :goto_29
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@2c
    .line 2604
    return-void

    #@2d
    .line 2601
    .end local v0           #childHeightSpec:I
    :cond_2d
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@30
    move-result v0

    #@31
    .restart local v0       #childHeightSpec:I
    goto :goto_29
.end method

.method private measureScrapChild(Landroid/view/View;II)V
    .registers 11
    .parameter "child"
    .parameter "position"
    .parameter "widthMeasureSpec"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1166
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v3

    #@5
    check-cast v3, Landroid/widget/AbsListView$LayoutParams;

    #@7
    .line 1167
    .local v3, p:Landroid/widget/AbsListView$LayoutParams;
    if-nez v3, :cond_12

    #@9
    .line 1168
    invoke-virtual {p0}, Landroid/widget/ListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@c
    move-result-object v3

    #@d
    .end local v3           #p:Landroid/widget/AbsListView$LayoutParams;
    check-cast v3, Landroid/widget/AbsListView$LayoutParams;

    #@f
    .line 1169
    .restart local v3       #p:Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@12
    .line 1171
    :cond_12
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@14
    invoke-interface {v4, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@17
    move-result v4

    #@18
    iput v4, v3, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@1a
    .line 1172
    const/4 v4, 0x1

    #@1b
    iput-boolean v4, v3, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@1d
    .line 1174
    iget-object v4, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1f
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@21
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@23
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@25
    add-int/2addr v4, v5

    #@26
    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@28
    invoke-static {p3, v4, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@2b
    move-result v1

    #@2c
    .line 1176
    .local v1, childWidthSpec:I
    iget v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2e
    .line 1178
    .local v2, lpHeight:I
    if-lez v2, :cond_3a

    #@30
    .line 1179
    const/high16 v4, 0x4000

    #@32
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@35
    move-result v0

    #@36
    .line 1183
    .local v0, childHeightSpec:I
    :goto_36
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@39
    .line 1184
    return-void

    #@3a
    .line 1181
    .end local v0           #childHeightSpec:I
    :cond_3a
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3d
    move-result v0

    #@3e
    .restart local v0       #childHeightSpec:I
    goto :goto_36
.end method

.method private moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    .registers 25
    .parameter "oldSel"
    .parameter "newSel"
    .parameter "delta"
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 931
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@3
    move-result v10

    #@4
    .line 932
    .local v10, fadingEdgeLength:I
    move-object/from16 v0, p0

    #@6
    iget v15, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8
    .line 936
    .local v15, selectedPosition:I
    move-object/from16 v0, p0

    #@a
    move/from16 v1, p4

    #@c
    invoke-direct {v0, v1, v10, v15}, Landroid/widget/ListView;->getTopSelectionPixel(III)I

    #@f
    move-result v18

    #@10
    .line 938
    .local v18, topSelectionPixel:I
    move-object/from16 v0, p0

    #@12
    move/from16 v1, p4

    #@14
    invoke-direct {v0, v1, v10, v15}, Landroid/widget/ListView;->getBottomSelectionPixel(III)I

    #@17
    move-result v8

    #@18
    .line 941
    .local v8, bottomSelectionPixel:I
    if-lez p3, :cond_bd

    #@1a
    .line 963
    add-int/lit8 v3, v15, -0x1

    #@1c
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@1f
    move-result v4

    #@20
    const/4 v5, 0x1

    #@21
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@25
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@27
    const/4 v7, 0x0

    #@28
    move-object/from16 v2, p0

    #@2a
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@2d
    move-result-object p1

    #@2e
    .line 966
    move-object/from16 v0, p0

    #@30
    iget v9, v0, Landroid/widget/ListView;->mDividerHeight:I

    #@32
    .line 969
    .local v9, dividerHeight:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getBottom()I

    #@35
    move-result v2

    #@36
    add-int v4, v2, v9

    #@38
    const/4 v5, 0x1

    #@39
    move-object/from16 v0, p0

    #@3b
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3d
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@3f
    const/4 v7, 0x1

    #@40
    move-object/from16 v2, p0

    #@42
    move v3, v15

    #@43
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@46
    move-result-object v14

    #@47
    .line 973
    .local v14, sel:Landroid/view/View;
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@4a
    move-result v2

    #@4b
    if-le v2, v8, :cond_6f

    #@4d
    .line 976
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@50
    move-result v2

    #@51
    sub-int v16, v2, v18

    #@53
    .line 979
    .local v16, spaceAbove:I
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@56
    move-result v2

    #@57
    sub-int v17, v2, v8

    #@59
    .line 982
    .local v17, spaceBelow:I
    sub-int v2, p5, p4

    #@5b
    div-int/lit8 v11, v2, 0x2

    #@5d
    .line 983
    .local v11, halfVerticalSpace:I
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    #@60
    move-result v13

    #@61
    .line 984
    .local v13, offset:I
    invoke-static {v13, v11}, Ljava/lang/Math;->min(II)I

    #@64
    move-result v13

    #@65
    .line 987
    neg-int v2, v13

    #@66
    move-object/from16 v0, p1

    #@68
    invoke-virtual {v0, v2}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@6b
    .line 989
    neg-int v2, v13

    #@6c
    invoke-virtual {v14, v2}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@6f
    .line 993
    .end local v11           #halfVerticalSpace:I
    .end local v13           #offset:I
    .end local v16           #spaceAbove:I
    .end local v17           #spaceBelow:I
    :cond_6f
    move-object/from16 v0, p0

    #@71
    iget-boolean v2, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@73
    if-nez v2, :cond_99

    #@75
    .line 994
    move-object/from16 v0, p0

    #@77
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@79
    add-int/lit8 v2, v2, -0x2

    #@7b
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@7e
    move-result v3

    #@7f
    sub-int/2addr v3, v9

    #@80
    move-object/from16 v0, p0

    #@82
    invoke-direct {v0, v2, v3}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@85
    .line 995
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@88
    .line 996
    move-object/from16 v0, p0

    #@8a
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8c
    add-int/lit8 v2, v2, 0x1

    #@8e
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@91
    move-result v3

    #@92
    add-int/2addr v3, v9

    #@93
    move-object/from16 v0, p0

    #@95
    invoke-direct {v0, v2, v3}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@98
    .line 1077
    .end local v9           #dividerHeight:I
    :goto_98
    return-object v14

    #@99
    .line 998
    .restart local v9       #dividerHeight:I
    :cond_99
    move-object/from16 v0, p0

    #@9b
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@9d
    add-int/lit8 v2, v2, 0x1

    #@9f
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@a2
    move-result v3

    #@a3
    add-int/2addr v3, v9

    #@a4
    move-object/from16 v0, p0

    #@a6
    invoke-direct {v0, v2, v3}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@a9
    .line 999
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@ac
    .line 1000
    move-object/from16 v0, p0

    #@ae
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@b0
    add-int/lit8 v2, v2, -0x2

    #@b2
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@b5
    move-result v3

    #@b6
    sub-int/2addr v3, v9

    #@b7
    move-object/from16 v0, p0

    #@b9
    invoke-direct {v0, v2, v3}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@bc
    goto :goto_98

    #@bd
    .line 1002
    .end local v9           #dividerHeight:I
    .end local v14           #sel:Landroid/view/View;
    :cond_bd
    if-gez p3, :cond_111

    #@bf
    .line 1023
    if-eqz p2, :cond_fd

    #@c1
    .line 1025
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    #@c4
    move-result v4

    #@c5
    const/4 v5, 0x1

    #@c6
    move-object/from16 v0, p0

    #@c8
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@ca
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@cc
    const/4 v7, 0x1

    #@cd
    move-object/from16 v2, p0

    #@cf
    move v3, v15

    #@d0
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@d3
    move-result-object v14

    #@d4
    .line 1035
    .restart local v14       #sel:Landroid/view/View;
    :goto_d4
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@d7
    move-result v2

    #@d8
    move/from16 v0, v18

    #@da
    if-ge v2, v0, :cond_f7

    #@dc
    .line 1037
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@df
    move-result v2

    #@e0
    sub-int v16, v18, v2

    #@e2
    .line 1040
    .restart local v16       #spaceAbove:I
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@e5
    move-result v2

    #@e6
    sub-int v17, v8, v2

    #@e8
    .line 1043
    .restart local v17       #spaceBelow:I
    sub-int v2, p5, p4

    #@ea
    div-int/lit8 v11, v2, 0x2

    #@ec
    .line 1044
    .restart local v11       #halfVerticalSpace:I
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    #@ef
    move-result v13

    #@f0
    .line 1045
    .restart local v13       #offset:I
    invoke-static {v13, v11}, Ljava/lang/Math;->min(II)I

    #@f3
    move-result v13

    #@f4
    .line 1048
    invoke-virtual {v14, v13}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@f7
    .line 1052
    .end local v11           #halfVerticalSpace:I
    .end local v13           #offset:I
    .end local v16           #spaceAbove:I
    .end local v17           #spaceBelow:I
    :cond_f7
    move-object/from16 v0, p0

    #@f9
    invoke-direct {v0, v14, v15}, Landroid/widget/ListView;->fillAboveAndBelow(Landroid/view/View;I)V

    #@fc
    goto :goto_98

    #@fd
    .line 1030
    .end local v14           #sel:Landroid/view/View;
    :cond_fd
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@100
    move-result v4

    #@101
    const/4 v5, 0x0

    #@102
    move-object/from16 v0, p0

    #@104
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@106
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@108
    const/4 v7, 0x1

    #@109
    move-object/from16 v2, p0

    #@10b
    move v3, v15

    #@10c
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@10f
    move-result-object v14

    #@110
    .restart local v14       #sel:Landroid/view/View;
    goto :goto_d4

    #@111
    .line 1055
    .end local v14           #sel:Landroid/view/View;
    :cond_111
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@114
    move-result v4

    #@115
    .line 1060
    .local v4, oldTop:I
    const/4 v5, 0x1

    #@116
    move-object/from16 v0, p0

    #@118
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@11a
    iget v6, v2, Landroid/graphics/Rect;->left:I

    #@11c
    const/4 v7, 0x1

    #@11d
    move-object/from16 v2, p0

    #@11f
    move v3, v15

    #@120
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    #@123
    move-result-object v14

    #@124
    .line 1063
    .restart local v14       #sel:Landroid/view/View;
    move/from16 v0, p4

    #@126
    if-ge v4, v0, :cond_139

    #@128
    .line 1066
    invoke-virtual {v14}, Landroid/view/View;->getBottom()I

    #@12b
    move-result v12

    #@12c
    .line 1067
    .local v12, newBottom:I
    add-int/lit8 v2, p4, 0x14

    #@12e
    if-ge v12, v2, :cond_139

    #@130
    .line 1069
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@133
    move-result v2

    #@134
    sub-int v2, p4, v2

    #@136
    invoke-virtual {v14, v2}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@139
    .line 1074
    .end local v12           #newBottom:I
    :cond_139
    move-object/from16 v0, p0

    #@13b
    invoke-direct {v0, v14, v15}, Landroid/widget/ListView;->fillAboveAndBelow(Landroid/view/View;I)V

    #@13e
    goto/16 :goto_98
.end method

.method private positionOfNewFocus(Landroid/view/View;)I
    .registers 7
    .parameter "newFocus"

    #@0
    .prologue
    .line 2867
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3
    move-result v2

    #@4
    .line 2868
    .local v2, numChildren:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v2, :cond_18

    #@7
    .line 2869
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 2870
    .local v0, child:Landroid/view/View;
    invoke-direct {p0, p1, v0}, Landroid/widget/ListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_15

    #@11
    .line 2871
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@13
    add-int/2addr v3, v1

    #@14
    return v3

    #@15
    .line 2868
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_5

    #@18
    .line 2874
    .end local v0           #child:Landroid/view/View;
    :cond_18
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string/jumbo v4, "newFocus is not a child of any of the children of the list!"

    #@1d
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v3
.end method

.method private relayoutMeasuredItem(Landroid/view/View;)V
    .registers 9
    .parameter "child"

    #@0
    .prologue
    .line 2612
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3
    move-result v5

    #@4
    .line 2613
    .local v5, w:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@7
    move-result v4

    #@8
    .line 2614
    .local v4, h:I
    iget-object v6, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@a
    iget v1, v6, Landroid/graphics/Rect;->left:I

    #@c
    .line 2615
    .local v1, childLeft:I
    add-int v2, v1, v5

    #@e
    .line 2616
    .local v2, childRight:I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@11
    move-result v3

    #@12
    .line 2617
    .local v3, childTop:I
    add-int v0, v3, v4

    #@14
    .line 2618
    .local v0, childBottom:I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/view/View;->layout(IIII)V

    #@17
    .line 2619
    return-void
.end method

.method private removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V
    .registers 7
    .parameter "v"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 317
    .local p2, where:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    .line 318
    .local v2, len:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_14

    #@7
    .line 319
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Landroid/widget/ListView$FixedViewInfo;

    #@d
    .line 320
    .local v1, info:Landroid/widget/ListView$FixedViewInfo;
    iget-object v3, v1, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@f
    if-ne v3, p1, :cond_15

    #@11
    .line 321
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@14
    .line 325
    .end local v1           #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_14
    return-void

    #@15
    .line 318
    .restart local v1       #info:Landroid/widget/ListView$FixedViewInfo;
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_5
.end method

.method private scrollListItemsBy(I)V
    .registers 14
    .parameter "amount"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 2950
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@4
    .line 2952
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@7
    move-result v9

    #@8
    iget-object v10, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@a
    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    #@c
    sub-int v5, v9, v10

    #@e
    .line 2953
    .local v5, listBottom:I
    iget-object v9, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@10
    iget v6, v9, Landroid/graphics/Rect;->top:I

    #@12
    .line 2954
    .local v6, listTop:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@14
    .line 2956
    .local v8, recycleBin:Landroid/widget/AbsListView$RecycleBin;
    if-gez p1, :cond_76

    #@16
    .line 2960
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@19
    move-result v7

    #@1a
    .line 2961
    .local v7, numChildren:I
    add-int/lit8 v9, v7, -0x1

    #@1c
    invoke-virtual {p0, v9}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@1f
    move-result-object v1

    #@20
    .line 2962
    .local v1, last:Landroid/view/View;
    :goto_20
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@23
    move-result v9

    #@24
    if-ge v9, v5, :cond_38

    #@26
    .line 2963
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@28
    add-int/2addr v9, v7

    #@29
    add-int/lit8 v3, v9, -0x1

    #@2b
    .line 2964
    .local v3, lastVisiblePosition:I
    iget v9, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2d
    add-int/lit8 v9, v9, -0x1

    #@2f
    if-ge v3, v9, :cond_38

    #@31
    .line 2965
    invoke-direct {p0, v1, v3}, Landroid/widget/ListView;->addViewBelow(Landroid/view/View;I)Landroid/view/View;

    #@34
    move-result-object v1

    #@35
    .line 2966
    add-int/lit8 v7, v7, 0x1

    #@37
    .line 2970
    goto :goto_20

    #@38
    .line 2975
    .end local v3           #lastVisiblePosition:I
    :cond_38
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@3b
    move-result v9

    #@3c
    if-ge v9, v5, :cond_47

    #@3e
    .line 2976
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@41
    move-result v9

    #@42
    sub-int v9, v5, v9

    #@44
    invoke-virtual {p0, v9}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@47
    .line 2980
    :cond_47
    invoke-virtual {p0, v11}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@4a
    move-result-object v0

    #@4b
    .line 2981
    .local v0, first:Landroid/view/View;
    :goto_4b
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@4e
    move-result v9

    #@4f
    if-ge v9, v6, :cond_d2

    #@51
    .line 2982
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@54
    move-result-object v4

    #@55
    check-cast v4, Landroid/widget/AbsListView$LayoutParams;

    #@57
    .line 2983
    .local v4, layoutParams:Landroid/widget/AbsListView$LayoutParams;
    iget v9, v4, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@59
    invoke-virtual {v8, v9}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@5c
    move-result v9

    #@5d
    if-eqz v9, :cond_72

    #@5f
    .line 2984
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->detachViewFromParent(Landroid/view/View;)V

    #@62
    .line 2985
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@64
    invoke-virtual {v8, v0, v9}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@67
    .line 2989
    :goto_67
    invoke-virtual {p0, v11}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@6a
    move-result-object v0

    #@6b
    .line 2990
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6d
    add-int/lit8 v9, v9, 0x1

    #@6f
    iput v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@71
    goto :goto_4b

    #@72
    .line 2987
    :cond_72
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->removeViewInLayout(Landroid/view/View;)V

    #@75
    goto :goto_67

    #@76
    .line 2994
    .end local v0           #first:Landroid/view/View;
    .end local v1           #last:Landroid/view/View;
    .end local v4           #layoutParams:Landroid/widget/AbsListView$LayoutParams;
    .end local v7           #numChildren:I
    :cond_76
    invoke-virtual {p0, v11}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@79
    move-result-object v0

    #@7a
    .line 2997
    .restart local v0       #first:Landroid/view/View;
    :goto_7a
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@7d
    move-result v9

    #@7e
    if-le v9, v6, :cond_91

    #@80
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@82
    if-lez v9, :cond_91

    #@84
    .line 2998
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@86
    invoke-direct {p0, v0, v9}, Landroid/widget/ListView;->addViewAbove(Landroid/view/View;I)Landroid/view/View;

    #@89
    move-result-object v0

    #@8a
    .line 2999
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8c
    add-int/lit8 v9, v9, -0x1

    #@8e
    iput v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@90
    goto :goto_7a

    #@91
    .line 3004
    :cond_91
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@94
    move-result v9

    #@95
    if-le v9, v6, :cond_a0

    #@97
    .line 3005
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@9a
    move-result v9

    #@9b
    sub-int v9, v6, v9

    #@9d
    invoke-virtual {p0, v9}, Landroid/widget/ListView;->offsetChildrenTopAndBottom(I)V

    #@a0
    .line 3008
    :cond_a0
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@a3
    move-result v9

    #@a4
    add-int/lit8 v2, v9, -0x1

    #@a6
    .line 3009
    .local v2, lastIndex:I
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@a9
    move-result-object v1

    #@aa
    .line 3012
    .restart local v1       #last:Landroid/view/View;
    :goto_aa
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@ad
    move-result v9

    #@ae
    if-le v9, v5, :cond_d2

    #@b0
    .line 3013
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@b3
    move-result-object v4

    #@b4
    check-cast v4, Landroid/widget/AbsListView$LayoutParams;

    #@b6
    .line 3014
    .restart local v4       #layoutParams:Landroid/widget/AbsListView$LayoutParams;
    iget v9, v4, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@b8
    invoke-virtual {v8, v9}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@bb
    move-result v9

    #@bc
    if-eqz v9, :cond_ce

    #@be
    .line 3015
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->detachViewFromParent(Landroid/view/View;)V

    #@c1
    .line 3016
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c3
    add-int/2addr v9, v2

    #@c4
    invoke-virtual {v8, v1, v9}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@c7
    .line 3020
    :goto_c7
    add-int/lit8 v2, v2, -0x1

    #@c9
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@cc
    move-result-object v1

    #@cd
    .line 3021
    goto :goto_aa

    #@ce
    .line 3018
    :cond_ce
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->removeViewInLayout(Landroid/view/View;)V

    #@d1
    goto :goto_c7

    #@d2
    .line 3023
    .end local v2           #lastIndex:I
    .end local v4           #layoutParams:Landroid/widget/AbsListView$LayoutParams;
    :cond_d2
    return-void
.end method

.method private setupChild(Landroid/view/View;IIZIZZ)V
    .registers 30
    .parameter "child"
    .parameter "position"
    .parameter "y"
    .parameter "flowDown"
    .parameter "childrenLeft"
    .parameter "selected"
    .parameter "recycled"

    #@0
    .prologue
    .line 1855
    if-eqz p6, :cond_146

    #@2
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->shouldShowSelector()Z

    #@5
    move-result v19

    #@6
    if-eqz v19, :cond_146

    #@8
    const/4 v11, 0x1

    #@9
    .line 1856
    .local v11, isSelected:Z
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    #@c
    move-result v19

    #@d
    move/from16 v0, v19

    #@f
    if-eq v11, v0, :cond_149

    #@11
    const/16 v17, 0x1

    #@13
    .line 1857
    .local v17, updateChildSelected:Z
    :goto_13
    move-object/from16 v0, p0

    #@15
    iget v13, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@17
    .line 1858
    .local v13, mode:I
    if-lez v13, :cond_14d

    #@19
    const/16 v19, 0x3

    #@1b
    move/from16 v0, v19

    #@1d
    if-ge v13, v0, :cond_14d

    #@1f
    move-object/from16 v0, p0

    #@21
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@23
    move/from16 v19, v0

    #@25
    move/from16 v0, v19

    #@27
    move/from16 v1, p2

    #@29
    if-ne v0, v1, :cond_14d

    #@2b
    const/4 v10, 0x1

    #@2c
    .line 1860
    .local v10, isPressed:Z
    :goto_2c
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isPressed()Z

    #@2f
    move-result v19

    #@30
    move/from16 v0, v19

    #@32
    if-eq v10, v0, :cond_150

    #@34
    const/16 v16, 0x1

    #@36
    .line 1861
    .local v16, updateChildPressed:Z
    :goto_36
    if-eqz p7, :cond_40

    #@38
    if-nez v17, :cond_40

    #@3a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isLayoutRequested()Z

    #@3d
    move-result v19

    #@3e
    if-eqz v19, :cond_154

    #@40
    :cond_40
    const/4 v14, 0x1

    #@41
    .line 1865
    .local v14, needToMeasure:Z
    :goto_41
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@44
    move-result-object v15

    #@45
    check-cast v15, Landroid/widget/AbsListView$LayoutParams;

    #@47
    .line 1866
    .local v15, p:Landroid/widget/AbsListView$LayoutParams;
    if-nez v15, :cond_4f

    #@49
    .line 1867
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4c
    move-result-object v15

    #@4d
    .end local v15           #p:Landroid/widget/AbsListView$LayoutParams;
    check-cast v15, Landroid/widget/AbsListView$LayoutParams;

    #@4f
    .line 1869
    .restart local v15       #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_4f
    move-object/from16 v0, p0

    #@51
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@53
    move-object/from16 v19, v0

    #@55
    move-object/from16 v0, v19

    #@57
    move/from16 v1, p2

    #@59
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@5c
    move-result v19

    #@5d
    move/from16 v0, v19

    #@5f
    iput v0, v15, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@61
    .line 1871
    if-eqz p7, :cond_69

    #@63
    iget-boolean v0, v15, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@65
    move/from16 v19, v0

    #@67
    if-eqz v19, :cond_7b

    #@69
    :cond_69
    iget-boolean v0, v15, Landroid/widget/AbsListView$LayoutParams;->recycledHeaderFooter:Z

    #@6b
    move/from16 v19, v0

    #@6d
    if-eqz v19, :cond_15b

    #@6f
    iget v0, v15, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@71
    move/from16 v19, v0

    #@73
    const/16 v20, -0x2

    #@75
    move/from16 v0, v19

    #@77
    move/from16 v1, v20

    #@79
    if-ne v0, v1, :cond_15b

    #@7b
    .line 1873
    :cond_7b
    if-eqz p4, :cond_157

    #@7d
    const/16 v19, -0x1

    #@7f
    :goto_7f
    move-object/from16 v0, p0

    #@81
    move-object/from16 v1, p1

    #@83
    move/from16 v2, v19

    #@85
    invoke-virtual {v0, v1, v2, v15}, Landroid/widget/ListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@88
    .line 1882
    :goto_88
    if-eqz v17, :cond_8f

    #@8a
    .line 1883
    move-object/from16 v0, p1

    #@8c
    invoke-virtual {v0, v11}, Landroid/view/View;->setSelected(Z)V

    #@8f
    .line 1886
    :cond_8f
    if-eqz v16, :cond_96

    #@91
    .line 1887
    move-object/from16 v0, p1

    #@93
    invoke-virtual {v0, v10}, Landroid/view/View;->setPressed(Z)V

    #@96
    .line 1890
    :cond_96
    move-object/from16 v0, p0

    #@98
    iget v0, v0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@9a
    move/from16 v19, v0

    #@9c
    if-eqz v19, :cond_c3

    #@9e
    move-object/from16 v0, p0

    #@a0
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@a2
    move-object/from16 v19, v0

    #@a4
    if-eqz v19, :cond_c3

    #@a6
    .line 1891
    move-object/from16 v0, p1

    #@a8
    instance-of v0, v0, Landroid/widget/Checkable;

    #@aa
    move/from16 v19, v0

    #@ac
    if-eqz v19, :cond_189

    #@ae
    move-object/from16 v19, p1

    #@b0
    .line 1892
    check-cast v19, Landroid/widget/Checkable;

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@b6
    move-object/from16 v20, v0

    #@b8
    move-object/from16 v0, v20

    #@ba
    move/from16 v1, p2

    #@bc
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@bf
    move-result v20

    #@c0
    invoke-interface/range {v19 .. v20}, Landroid/widget/Checkable;->setChecked(Z)V

    #@c3
    .line 1899
    :cond_c3
    :goto_c3
    if-eqz v14, :cond_1c0

    #@c5
    .line 1900
    move-object/from16 v0, p0

    #@c7
    iget v0, v0, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@c9
    move/from16 v19, v0

    #@cb
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@cf
    move-object/from16 v20, v0

    #@d1
    move-object/from16 v0, v20

    #@d3
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@d5
    move/from16 v20, v0

    #@d7
    move-object/from16 v0, p0

    #@d9
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@db
    move-object/from16 v21, v0

    #@dd
    move-object/from16 v0, v21

    #@df
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@e1
    move/from16 v21, v0

    #@e3
    add-int v20, v20, v21

    #@e5
    iget v0, v15, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@e7
    move/from16 v21, v0

    #@e9
    invoke-static/range {v19 .. v21}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@ec
    move-result v8

    #@ed
    .line 1902
    .local v8, childWidthSpec:I
    iget v12, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@ef
    .line 1904
    .local v12, lpHeight:I
    if-lez v12, :cond_1b6

    #@f1
    .line 1905
    const/high16 v19, 0x4000

    #@f3
    move/from16 v0, v19

    #@f5
    invoke-static {v12, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@f8
    move-result v5

    #@f9
    .line 1909
    .local v5, childHeightSpec:I
    :goto_f9
    move-object/from16 v0, p1

    #@fb
    invoke-virtual {v0, v8, v5}, Landroid/view/View;->measure(II)V

    #@fe
    .line 1914
    .end local v5           #childHeightSpec:I
    .end local v8           #childWidthSpec:I
    .end local v12           #lpHeight:I
    :goto_fe
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    #@101
    move-result v18

    #@102
    .line 1915
    .local v18, w:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    #@105
    move-result v9

    #@106
    .line 1916
    .local v9, h:I
    if-eqz p4, :cond_1c5

    #@108
    move/from16 v7, p3

    #@10a
    .line 1918
    .local v7, childTop:I
    :goto_10a
    if-eqz v14, :cond_1c9

    #@10c
    .line 1919
    add-int v6, p5, v18

    #@10e
    .line 1920
    .local v6, childRight:I
    add-int v4, v7, v9

    #@110
    .line 1921
    .local v4, childBottom:I
    move-object/from16 v0, p1

    #@112
    move/from16 v1, p5

    #@114
    invoke-virtual {v0, v1, v7, v6, v4}, Landroid/view/View;->layout(IIII)V

    #@117
    .line 1927
    .end local v4           #childBottom:I
    .end local v6           #childRight:I
    :goto_117
    move-object/from16 v0, p0

    #@119
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@11b
    move/from16 v19, v0

    #@11d
    if-eqz v19, :cond_12e

    #@11f
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isDrawingCacheEnabled()Z

    #@122
    move-result v19

    #@123
    if-nez v19, :cond_12e

    #@125
    .line 1928
    const/16 v19, 0x1

    #@127
    move-object/from16 v0, p1

    #@129
    move/from16 v1, v19

    #@12b
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    #@12e
    .line 1931
    :cond_12e
    if-eqz p7, :cond_145

    #@130
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@133
    move-result-object v19

    #@134
    check-cast v19, Landroid/widget/AbsListView$LayoutParams;

    #@136
    move-object/from16 v0, v19

    #@138
    iget v0, v0, Landroid/widget/AbsListView$LayoutParams;->scrappedFromPosition:I

    #@13a
    move/from16 v19, v0

    #@13c
    move/from16 v0, v19

    #@13e
    move/from16 v1, p2

    #@140
    if-eq v0, v1, :cond_145

    #@142
    .line 1933
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@145
    .line 1935
    :cond_145
    return-void

    #@146
    .line 1855
    .end local v7           #childTop:I
    .end local v9           #h:I
    .end local v10           #isPressed:Z
    .end local v11           #isSelected:Z
    .end local v13           #mode:I
    .end local v14           #needToMeasure:Z
    .end local v15           #p:Landroid/widget/AbsListView$LayoutParams;
    .end local v16           #updateChildPressed:Z
    .end local v17           #updateChildSelected:Z
    .end local v18           #w:I
    :cond_146
    const/4 v11, 0x0

    #@147
    goto/16 :goto_9

    #@149
    .line 1856
    .restart local v11       #isSelected:Z
    :cond_149
    const/16 v17, 0x0

    #@14b
    goto/16 :goto_13

    #@14d
    .line 1858
    .restart local v13       #mode:I
    .restart local v17       #updateChildSelected:Z
    :cond_14d
    const/4 v10, 0x0

    #@14e
    goto/16 :goto_2c

    #@150
    .line 1860
    .restart local v10       #isPressed:Z
    :cond_150
    const/16 v16, 0x0

    #@152
    goto/16 :goto_36

    #@154
    .line 1861
    .restart local v16       #updateChildPressed:Z
    :cond_154
    const/4 v14, 0x0

    #@155
    goto/16 :goto_41

    #@157
    .line 1873
    .restart local v14       #needToMeasure:Z
    .restart local v15       #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_157
    const/16 v19, 0x0

    #@159
    goto/16 :goto_7f

    #@15b
    .line 1875
    :cond_15b
    const/16 v19, 0x0

    #@15d
    move/from16 v0, v19

    #@15f
    iput-boolean v0, v15, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@161
    .line 1876
    iget v0, v15, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@163
    move/from16 v19, v0

    #@165
    const/16 v20, -0x2

    #@167
    move/from16 v0, v19

    #@169
    move/from16 v1, v20

    #@16b
    if-ne v0, v1, :cond_173

    #@16d
    .line 1877
    const/16 v19, 0x1

    #@16f
    move/from16 v0, v19

    #@171
    iput-boolean v0, v15, Landroid/widget/AbsListView$LayoutParams;->recycledHeaderFooter:Z

    #@173
    .line 1879
    :cond_173
    if-eqz p4, :cond_186

    #@175
    const/16 v19, -0x1

    #@177
    :goto_177
    const/16 v20, 0x1

    #@179
    move-object/from16 v0, p0

    #@17b
    move-object/from16 v1, p1

    #@17d
    move/from16 v2, v19

    #@17f
    move/from16 v3, v20

    #@181
    invoke-virtual {v0, v1, v2, v15, v3}, Landroid/widget/ListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    #@184
    goto/16 :goto_88

    #@186
    :cond_186
    const/16 v19, 0x0

    #@188
    goto :goto_177

    #@189
    .line 1893
    :cond_189
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    #@18c
    move-result-object v19

    #@18d
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@190
    move-result-object v19

    #@191
    move-object/from16 v0, v19

    #@193
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@195
    move/from16 v19, v0

    #@197
    const/16 v20, 0xb

    #@199
    move/from16 v0, v19

    #@19b
    move/from16 v1, v20

    #@19d
    if-lt v0, v1, :cond_c3

    #@19f
    .line 1895
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@1a3
    move-object/from16 v19, v0

    #@1a5
    move-object/from16 v0, v19

    #@1a7
    move/from16 v1, p2

    #@1a9
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@1ac
    move-result v19

    #@1ad
    move-object/from16 v0, p1

    #@1af
    move/from16 v1, v19

    #@1b1
    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    #@1b4
    goto/16 :goto_c3

    #@1b6
    .line 1907
    .restart local v8       #childWidthSpec:I
    .restart local v12       #lpHeight:I
    :cond_1b6
    const/16 v19, 0x0

    #@1b8
    const/16 v20, 0x0

    #@1ba
    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1bd
    move-result v5

    #@1be
    .restart local v5       #childHeightSpec:I
    goto/16 :goto_f9

    #@1c0
    .line 1911
    .end local v5           #childHeightSpec:I
    .end local v8           #childWidthSpec:I
    .end local v12           #lpHeight:I
    :cond_1c0
    invoke-virtual/range {p0 .. p1}, Landroid/widget/ListView;->cleanupLayoutState(Landroid/view/View;)V

    #@1c3
    goto/16 :goto_fe

    #@1c5
    .line 1916
    .restart local v9       #h:I
    .restart local v18       #w:I
    :cond_1c5
    sub-int v7, p3, v9

    #@1c7
    goto/16 :goto_10a

    #@1c9
    .line 1923
    .restart local v7       #childTop:I
    :cond_1c9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    #@1cc
    move-result v19

    #@1cd
    sub-int v19, p5, v19

    #@1cf
    move-object/from16 v0, p1

    #@1d1
    move/from16 v1, v19

    #@1d3
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@1d6
    .line 1924
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@1d9
    move-result v19

    #@1da
    sub-int v19, v7, v19

    #@1dc
    move-object/from16 v0, p1

    #@1de
    move/from16 v1, v19

    #@1e0
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@1e3
    goto/16 :goto_117
.end method

.method private showingBottomFadingEdge()Z
    .registers 7

    #@0
    .prologue
    .line 535
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 536
    .local v1, childCount:I
    add-int/lit8 v4, v1, -0x1

    #@6
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@9
    move-result-object v4

    #@a
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@d
    move-result v0

    #@e
    .line 537
    .local v0, bottomOfBottomChild:I
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@10
    add-int/2addr v4, v1

    #@11
    add-int/lit8 v2, v4, -0x1

    #@13
    .line 539
    .local v2, lastVisiblePosition:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@15
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@18
    move-result v5

    #@19
    add-int/2addr v4, v5

    #@1a
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1c
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@1e
    sub-int v3, v4, v5

    #@20
    .line 541
    .local v3, listBottom:I
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@22
    add-int/lit8 v4, v4, -0x1

    #@24
    if-lt v2, v4, :cond_28

    #@26
    if-ge v0, v3, :cond_2a

    #@28
    :cond_28
    const/4 v4, 0x1

    #@29
    :goto_29
    return v4

    #@2a
    :cond_2a
    const/4 v4, 0x0

    #@2b
    goto :goto_29
.end method

.method private showingTopFadingEdge()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 527
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@3
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@5
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@7
    add-int v0, v2, v3

    #@9
    .line 528
    .local v0, listTop:I
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@b
    if-gtz v2, :cond_17

    #@d
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@14
    move-result v2

    #@15
    if-le v2, v0, :cond_18

    #@17
    :cond_17
    const/4 v1, 0x1

    #@18
    :cond_18
    return v1
.end method


# virtual methods
.method public addFooterView(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 371
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    #@5
    .line 372
    return-void
.end method

.method public addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .registers 6
    .parameter "v"
    .parameter "data"
    .parameter "isSelectable"

    #@0
    .prologue
    .line 347
    new-instance v0, Landroid/widget/ListView$FixedViewInfo;

    #@2
    invoke-direct {v0, p0}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    #@5
    .line 348
    .local v0, info:Landroid/widget/ListView$FixedViewInfo;
    iput-object p1, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@7
    .line 349
    iput-object p2, v0, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    #@9
    .line 350
    iput-boolean p3, v0, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    #@b
    .line 351
    iget-object v1, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 355
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@12
    if-eqz v1, :cond_1d

    #@14
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@16
    if-eqz v1, :cond_1d

    #@18
    .line 356
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@1a
    invoke-virtual {v1}, Landroid/widget/AbsListView$AdapterDataSetObserver;->onChanged()V

    #@1d
    .line 358
    :cond_1d
    return-void
.end method

.method public addHeaderView(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 286
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    #@5
    .line 287
    return-void
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .registers 7
    .parameter "v"
    .parameter "data"
    .parameter "isSelectable"

    #@0
    .prologue
    .line 256
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-eqz v1, :cond_12

    #@4
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    instance-of v1, v1, Landroid/widget/HeaderViewListAdapter;

    #@8
    if-nez v1, :cond_12

    #@a
    .line 257
    new-instance v1, Ljava/lang/IllegalStateException;

    #@c
    const-string v2, "Cannot add header view to list -- setAdapter has already been called."

    #@e
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 261
    :cond_12
    new-instance v0, Landroid/widget/ListView$FixedViewInfo;

    #@14
    invoke-direct {v0, p0}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    #@17
    .line 262
    .local v0, info:Landroid/widget/ListView$FixedViewInfo;
    iput-object p1, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@19
    .line 263
    iput-object p2, v0, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    #@1b
    .line 264
    iput-boolean p3, v0, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    #@1d
    .line 265
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22
    .line 269
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@24
    if-eqz v1, :cond_2f

    #@26
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@28
    if-eqz v1, :cond_2f

    #@2a
    .line 270
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@2c
    invoke-virtual {v1}, Landroid/widget/AbsListView$AdapterDataSetObserver;->onChanged()V

    #@2f
    .line 272
    :cond_2f
    return-void
.end method

.method arrowScroll(I)Z
    .registers 5
    .parameter "direction"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2406
    const/4 v1, 0x1

    #@2
    :try_start_2
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@4
    .line 2407
    invoke-direct {p0, p1}, Landroid/widget/ListView;->arrowScrollImpl(I)Z

    #@7
    move-result v0

    #@8
    .line 2408
    .local v0, handled:Z
    if-eqz v0, :cond_11

    #@a
    .line 2409
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    #@d
    move-result v1

    #@e
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->playSoundEffect(I)V
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_14

    #@11
    .line 2413
    :cond_11
    iput-boolean v2, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@13
    .line 2411
    return v0

    #@14
    .line 2413
    .end local v0           #handled:Z
    :catchall_14
    move-exception v1

    #@15
    iput-boolean v2, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@17
    throw v1
.end method

.method protected canAnimate()Z
    .registers 2

    #@0
    .prologue
    .line 1939
    invoke-super {p0}, Landroid/widget/AbsListView;->canAnimate()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@8
    if-lez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 36
    .parameter "canvas"

    #@0
    .prologue
    .line 3134
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@4
    move/from16 v32, v0

    #@6
    if-eqz v32, :cond_10

    #@8
    .line 3135
    const/16 v32, 0x1

    #@a
    move/from16 v0, v32

    #@c
    move-object/from16 v1, p0

    #@e
    iput-boolean v0, v1, Landroid/widget/AbsListView;->mCachingActive:Z

    #@10
    .line 3139
    :cond_10
    move-object/from16 v0, p0

    #@12
    iget v10, v0, Landroid/widget/ListView;->mDividerHeight:I

    #@14
    .line 3140
    .local v10, dividerHeight:I
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/widget/ListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    #@18
    move-object/from16 v27, v0

    #@1a
    .line 3141
    .local v27, overscrollHeader:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Landroid/widget/ListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    #@1e
    move-object/from16 v26, v0

    #@20
    .line 3142
    .local v26, overscrollFooter:Landroid/graphics/drawable/Drawable;
    if-eqz v27, :cond_1a9

    #@22
    const/4 v13, 0x1

    #@23
    .line 3143
    .local v13, drawOverscrollHeader:Z
    :goto_23
    if-eqz v26, :cond_1ac

    #@25
    const/4 v12, 0x1

    #@26
    .line 3144
    .local v12, drawOverscrollFooter:Z
    :goto_26
    if-lez v10, :cond_1af

    #@28
    move-object/from16 v0, p0

    #@2a
    iget-object v0, v0, Landroid/widget/ListView;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2c
    move-object/from16 v32, v0

    #@2e
    if-eqz v32, :cond_1af

    #@30
    const/4 v11, 0x1

    #@31
    .line 3146
    .local v11, drawDividers:Z
    :goto_31
    if-nez v11, :cond_37

    #@33
    if-nez v13, :cond_37

    #@35
    if-eqz v12, :cond_20f

    #@37
    .line 3148
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v7, v0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@3b
    .line 3149
    .local v7, bounds:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@3d
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@3f
    move/from16 v32, v0

    #@41
    move/from16 v0, v32

    #@43
    iput v0, v7, Landroid/graphics/Rect;->left:I

    #@45
    .line 3150
    move-object/from16 v0, p0

    #@47
    iget v0, v0, Landroid/view/View;->mRight:I

    #@49
    move/from16 v32, v0

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@4f
    move/from16 v33, v0

    #@51
    sub-int v32, v32, v33

    #@53
    move-object/from16 v0, p0

    #@55
    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    #@57
    move/from16 v33, v0

    #@59
    sub-int v32, v32, v33

    #@5b
    move/from16 v0, v32

    #@5d
    iput v0, v7, Landroid/graphics/Rect;->right:I

    #@5f
    .line 3152
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getChildCount()I

    #@62
    move-result v9

    #@63
    .line 3153
    .local v9, count:I
    move-object/from16 v0, p0

    #@65
    iget-object v0, v0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@67
    move-object/from16 v32, v0

    #@69
    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    #@6c
    move-result v20

    #@6d
    .line 3154
    .local v20, headerCount:I
    move-object/from16 v0, p0

    #@6f
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@71
    move/from16 v23, v0

    #@73
    .line 3155
    .local v23, itemCount:I
    move-object/from16 v0, p0

    #@75
    iget-object v0, v0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@77
    move-object/from16 v32, v0

    #@79
    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    #@7c
    move-result v32

    #@7d
    sub-int v32, v23, v32

    #@7f
    add-int/lit8 v19, v32, -0x1

    #@81
    .line 3156
    .local v19, footerLimit:I
    move-object/from16 v0, p0

    #@83
    iget-boolean v0, v0, Landroid/widget/ListView;->mHeaderDividersEnabled:Z

    #@85
    move/from16 v21, v0

    #@87
    .line 3157
    .local v21, headerDividers:Z
    move-object/from16 v0, p0

    #@89
    iget-boolean v0, v0, Landroid/widget/ListView;->mFooterDividersEnabled:Z

    #@8b
    move/from16 v18, v0

    #@8d
    .line 3158
    .local v18, footerDividers:Z
    move-object/from16 v0, p0

    #@8f
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@91
    move/from16 v17, v0

    #@93
    .line 3159
    .local v17, first:I
    move-object/from16 v0, p0

    #@95
    iget-boolean v5, v0, Landroid/widget/ListView;->mAreAllItemsSelectable:Z

    #@97
    .line 3160
    .local v5, areAllItemsSelectable:Z
    move-object/from16 v0, p0

    #@99
    iget-object v4, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@9b
    .line 3165
    .local v4, adapter:Landroid/widget/ListAdapter;
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->isOpaque()Z

    #@9e
    move-result v32

    #@9f
    if-eqz v32, :cond_1b2

    #@a1
    invoke-super/range {p0 .. p0}, Landroid/widget/AbsListView;->isOpaque()Z

    #@a4
    move-result v32

    #@a5
    if-nez v32, :cond_1b2

    #@a7
    const/16 v16, 0x1

    #@a9
    .line 3167
    .local v16, fillForMissingDividers:Z
    :goto_a9
    if-eqz v16, :cond_d3

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v0, v0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@af
    move-object/from16 v32, v0

    #@b1
    if-nez v32, :cond_d3

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget-boolean v0, v0, Landroid/widget/ListView;->mIsCacheColorOpaque:Z

    #@b7
    move/from16 v32, v0

    #@b9
    if-eqz v32, :cond_d3

    #@bb
    .line 3168
    new-instance v32, Landroid/graphics/Paint;

    #@bd
    invoke-direct/range {v32 .. v32}, Landroid/graphics/Paint;-><init>()V

    #@c0
    move-object/from16 v0, v32

    #@c2
    move-object/from16 v1, p0

    #@c4
    iput-object v0, v1, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@c6
    .line 3169
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@ca
    move-object/from16 v32, v0

    #@cc
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getCacheColorHint()I

    #@cf
    move-result v33

    #@d0
    invoke-virtual/range {v32 .. v33}, Landroid/graphics/Paint;->setColor(I)V

    #@d3
    .line 3171
    :cond_d3
    move-object/from16 v0, p0

    #@d5
    iget-object v0, v0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@d7
    move-object/from16 v28, v0

    #@d9
    .line 3173
    .local v28, paint:Landroid/graphics/Paint;
    const/4 v15, 0x0

    #@da
    .line 3174
    .local v15, effectivePaddingTop:I
    const/4 v14, 0x0

    #@db
    .line 3175
    .local v14, effectivePaddingBottom:I
    move-object/from16 v0, p0

    #@dd
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@df
    move/from16 v32, v0

    #@e1
    and-int/lit8 v32, v32, 0x22

    #@e3
    const/16 v33, 0x22

    #@e5
    move/from16 v0, v32

    #@e7
    move/from16 v1, v33

    #@e9
    if-ne v0, v1, :cond_ff

    #@eb
    .line 3176
    move-object/from16 v0, p0

    #@ed
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@ef
    move-object/from16 v32, v0

    #@f1
    move-object/from16 v0, v32

    #@f3
    iget v15, v0, Landroid/graphics/Rect;->top:I

    #@f5
    .line 3177
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@f9
    move-object/from16 v32, v0

    #@fb
    move-object/from16 v0, v32

    #@fd
    iget v14, v0, Landroid/graphics/Rect;->bottom:I

    #@ff
    .line 3180
    :cond_ff
    move-object/from16 v0, p0

    #@101
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@103
    move/from16 v32, v0

    #@105
    move-object/from16 v0, p0

    #@107
    iget v0, v0, Landroid/view/View;->mTop:I

    #@109
    move/from16 v33, v0

    #@10b
    sub-int v32, v32, v33

    #@10d
    sub-int v32, v32, v14

    #@10f
    move-object/from16 v0, p0

    #@111
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@113
    move/from16 v33, v0

    #@115
    add-int v24, v32, v33

    #@117
    .line 3181
    .local v24, listBottom:I
    move-object/from16 v0, p0

    #@119
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@11b
    move/from16 v32, v0

    #@11d
    if-nez v32, :cond_213

    #@11f
    .line 3182
    const/4 v6, 0x0

    #@120
    .line 3185
    .local v6, bottom:I
    move-object/from16 v0, p0

    #@122
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@124
    move/from16 v29, v0

    #@126
    .line 3186
    .local v29, scrollY:I
    if-lez v9, :cond_13f

    #@128
    if-gez v29, :cond_13f

    #@12a
    .line 3187
    if-eqz v13, :cond_1b6

    #@12c
    .line 3188
    const/16 v32, 0x0

    #@12e
    move/from16 v0, v32

    #@130
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@132
    .line 3189
    move/from16 v0, v29

    #@134
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@136
    .line 3190
    move-object/from16 v0, p0

    #@138
    move-object/from16 v1, p1

    #@13a
    move-object/from16 v2, v27

    #@13c
    invoke-virtual {v0, v1, v2, v7}, Landroid/widget/ListView;->drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    #@13f
    .line 3198
    :cond_13f
    :goto_13f
    const/16 v22, 0x0

    #@141
    .local v22, i:I
    :goto_141
    move/from16 v0, v22

    #@143
    if-ge v0, v9, :cond_1e4

    #@145
    .line 3199
    if-nez v21, :cond_14f

    #@147
    add-int v32, v17, v22

    #@149
    move/from16 v0, v32

    #@14b
    move/from16 v1, v20

    #@14d
    if-lt v0, v1, :cond_1a6

    #@14f
    :cond_14f
    if-nez v18, :cond_159

    #@151
    add-int v32, v17, v22

    #@153
    move/from16 v0, v32

    #@155
    move/from16 v1, v19

    #@157
    if-ge v0, v1, :cond_1a6

    #@159
    .line 3201
    :cond_159
    move-object/from16 v0, p0

    #@15b
    move/from16 v1, v22

    #@15d
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@160
    move-result-object v8

    #@161
    .line 3202
    .local v8, child:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    #@164
    move-result v6

    #@165
    .line 3205
    if-eqz v11, :cond_1a6

    #@167
    move/from16 v0, v24

    #@169
    if-ge v6, v0, :cond_1a6

    #@16b
    if-eqz v12, :cond_175

    #@16d
    add-int/lit8 v32, v9, -0x1

    #@16f
    move/from16 v0, v22

    #@171
    move/from16 v1, v32

    #@173
    if-eq v0, v1, :cond_1a6

    #@175
    .line 3207
    :cond_175
    if-nez v5, :cond_195

    #@177
    add-int v32, v17, v22

    #@179
    move/from16 v0, v32

    #@17b
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@17e
    move-result v32

    #@17f
    if-eqz v32, :cond_1d2

    #@181
    add-int/lit8 v32, v9, -0x1

    #@183
    move/from16 v0, v22

    #@185
    move/from16 v1, v32

    #@187
    if-eq v0, v1, :cond_195

    #@189
    add-int v32, v17, v22

    #@18b
    add-int/lit8 v32, v32, 0x1

    #@18d
    move/from16 v0, v32

    #@18f
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@192
    move-result v32

    #@193
    if-eqz v32, :cond_1d2

    #@195
    .line 3210
    :cond_195
    iput v6, v7, Landroid/graphics/Rect;->top:I

    #@197
    .line 3211
    add-int v32, v6, v10

    #@199
    move/from16 v0, v32

    #@19b
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@19d
    .line 3212
    move-object/from16 v0, p0

    #@19f
    move-object/from16 v1, p1

    #@1a1
    move/from16 v2, v22

    #@1a3
    invoke-virtual {v0, v1, v7, v2}, Landroid/widget/ListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    #@1a6
    .line 3198
    .end local v8           #child:Landroid/view/View;
    :cond_1a6
    :goto_1a6
    add-int/lit8 v22, v22, 0x1

    #@1a8
    goto :goto_141

    #@1a9
    .line 3142
    .end local v4           #adapter:Landroid/widget/ListAdapter;
    .end local v5           #areAllItemsSelectable:Z
    .end local v6           #bottom:I
    .end local v7           #bounds:Landroid/graphics/Rect;
    .end local v9           #count:I
    .end local v11           #drawDividers:Z
    .end local v12           #drawOverscrollFooter:Z
    .end local v13           #drawOverscrollHeader:Z
    .end local v14           #effectivePaddingBottom:I
    .end local v15           #effectivePaddingTop:I
    .end local v16           #fillForMissingDividers:Z
    .end local v17           #first:I
    .end local v18           #footerDividers:Z
    .end local v19           #footerLimit:I
    .end local v20           #headerCount:I
    .end local v21           #headerDividers:Z
    .end local v22           #i:I
    .end local v23           #itemCount:I
    .end local v24           #listBottom:I
    .end local v28           #paint:Landroid/graphics/Paint;
    .end local v29           #scrollY:I
    :cond_1a9
    const/4 v13, 0x0

    #@1aa
    goto/16 :goto_23

    #@1ac
    .line 3143
    .restart local v13       #drawOverscrollHeader:Z
    :cond_1ac
    const/4 v12, 0x0

    #@1ad
    goto/16 :goto_26

    #@1af
    .line 3144
    .restart local v12       #drawOverscrollFooter:Z
    :cond_1af
    const/4 v11, 0x0

    #@1b0
    goto/16 :goto_31

    #@1b2
    .line 3165
    .restart local v4       #adapter:Landroid/widget/ListAdapter;
    .restart local v5       #areAllItemsSelectable:Z
    .restart local v7       #bounds:Landroid/graphics/Rect;
    .restart local v9       #count:I
    .restart local v11       #drawDividers:Z
    .restart local v17       #first:I
    .restart local v18       #footerDividers:Z
    .restart local v19       #footerLimit:I
    .restart local v20       #headerCount:I
    .restart local v21       #headerDividers:Z
    .restart local v23       #itemCount:I
    :cond_1b2
    const/16 v16, 0x0

    #@1b4
    goto/16 :goto_a9

    #@1b6
    .line 3191
    .restart local v6       #bottom:I
    .restart local v14       #effectivePaddingBottom:I
    .restart local v15       #effectivePaddingTop:I
    .restart local v16       #fillForMissingDividers:Z
    .restart local v24       #listBottom:I
    .restart local v28       #paint:Landroid/graphics/Paint;
    .restart local v29       #scrollY:I
    :cond_1b6
    if-eqz v11, :cond_13f

    #@1b8
    .line 3192
    const/16 v32, 0x0

    #@1ba
    move/from16 v0, v32

    #@1bc
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@1be
    .line 3193
    neg-int v0, v10

    #@1bf
    move/from16 v32, v0

    #@1c1
    move/from16 v0, v32

    #@1c3
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@1c5
    .line 3194
    const/16 v32, -0x1

    #@1c7
    move-object/from16 v0, p0

    #@1c9
    move-object/from16 v1, p1

    #@1cb
    move/from16 v2, v32

    #@1cd
    invoke-virtual {v0, v1, v7, v2}, Landroid/widget/ListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    #@1d0
    goto/16 :goto_13f

    #@1d2
    .line 3213
    .restart local v8       #child:Landroid/view/View;
    .restart local v22       #i:I
    :cond_1d2
    if-eqz v16, :cond_1a6

    #@1d4
    .line 3214
    iput v6, v7, Landroid/graphics/Rect;->top:I

    #@1d6
    .line 3215
    add-int v32, v6, v10

    #@1d8
    move/from16 v0, v32

    #@1da
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@1dc
    .line 3216
    move-object/from16 v0, p1

    #@1de
    move-object/from16 v1, v28

    #@1e0
    invoke-virtual {v0, v7, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@1e3
    goto :goto_1a6

    #@1e4
    .line 3222
    .end local v8           #child:Landroid/view/View;
    :cond_1e4
    move-object/from16 v0, p0

    #@1e6
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@1e8
    move/from16 v32, v0

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@1ee
    move/from16 v33, v0

    #@1f0
    add-int v25, v32, v33

    #@1f2
    .line 3223
    .local v25, overFooterBottom:I
    if-eqz v12, :cond_20f

    #@1f4
    add-int v32, v17, v9

    #@1f6
    move/from16 v0, v32

    #@1f8
    move/from16 v1, v23

    #@1fa
    if-ne v0, v1, :cond_20f

    #@1fc
    move/from16 v0, v25

    #@1fe
    if-le v0, v6, :cond_20f

    #@200
    .line 3225
    iput v6, v7, Landroid/graphics/Rect;->top:I

    #@202
    .line 3226
    move/from16 v0, v25

    #@204
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@206
    .line 3227
    move-object/from16 v0, p0

    #@208
    move-object/from16 v1, p1

    #@20a
    move-object/from16 v2, v26

    #@20c
    invoke-virtual {v0, v1, v2, v7}, Landroid/widget/ListView;->drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    #@20f
    .line 3283
    .end local v4           #adapter:Landroid/widget/ListAdapter;
    .end local v5           #areAllItemsSelectable:Z
    .end local v6           #bottom:I
    .end local v7           #bounds:Landroid/graphics/Rect;
    .end local v9           #count:I
    .end local v14           #effectivePaddingBottom:I
    .end local v15           #effectivePaddingTop:I
    .end local v16           #fillForMissingDividers:Z
    .end local v17           #first:I
    .end local v18           #footerDividers:Z
    .end local v19           #footerLimit:I
    .end local v20           #headerCount:I
    .end local v21           #headerDividers:Z
    .end local v22           #i:I
    .end local v23           #itemCount:I
    .end local v24           #listBottom:I
    .end local v25           #overFooterBottom:I
    .end local v28           #paint:Landroid/graphics/Paint;
    .end local v29           #scrollY:I
    :cond_20f
    :goto_20f
    invoke-super/range {p0 .. p1}, Landroid/widget/AbsListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@212
    .line 3284
    return-void

    #@213
    .line 3232
    .restart local v4       #adapter:Landroid/widget/ListAdapter;
    .restart local v5       #areAllItemsSelectable:Z
    .restart local v7       #bounds:Landroid/graphics/Rect;
    .restart local v9       #count:I
    .restart local v14       #effectivePaddingBottom:I
    .restart local v15       #effectivePaddingTop:I
    .restart local v16       #fillForMissingDividers:Z
    .restart local v17       #first:I
    .restart local v18       #footerDividers:Z
    .restart local v19       #footerLimit:I
    .restart local v20       #headerCount:I
    .restart local v21       #headerDividers:Z
    .restart local v23       #itemCount:I
    .restart local v24       #listBottom:I
    .restart local v28       #paint:Landroid/graphics/Paint;
    :cond_213
    move-object/from16 v0, p0

    #@215
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@217
    move/from16 v29, v0

    #@219
    .line 3234
    .restart local v29       #scrollY:I
    if-lez v9, :cond_23c

    #@21b
    if-eqz v13, :cond_23c

    #@21d
    .line 3235
    move/from16 v0, v29

    #@21f
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@221
    .line 3236
    const/16 v32, 0x0

    #@223
    move-object/from16 v0, p0

    #@225
    move/from16 v1, v32

    #@227
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@22a
    move-result-object v32

    #@22b
    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getTop()I

    #@22e
    move-result v32

    #@22f
    move/from16 v0, v32

    #@231
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@233
    .line 3237
    move-object/from16 v0, p0

    #@235
    move-object/from16 v1, p1

    #@237
    move-object/from16 v2, v27

    #@239
    invoke-virtual {v0, v1, v2, v7}, Landroid/widget/ListView;->drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    #@23c
    .line 3240
    :cond_23c
    if-eqz v13, :cond_2a2

    #@23e
    const/16 v30, 0x1

    #@240
    .line 3241
    .local v30, start:I
    :goto_240
    move/from16 v22, v30

    #@242
    .restart local v22       #i:I
    :goto_242
    move/from16 v0, v22

    #@244
    if-ge v0, v9, :cond_2b9

    #@246
    .line 3242
    if-nez v21, :cond_250

    #@248
    add-int v32, v17, v22

    #@24a
    move/from16 v0, v32

    #@24c
    move/from16 v1, v20

    #@24e
    if-lt v0, v1, :cond_29f

    #@250
    :cond_250
    if-nez v18, :cond_25a

    #@252
    add-int v32, v17, v22

    #@254
    move/from16 v0, v32

    #@256
    move/from16 v1, v19

    #@258
    if-ge v0, v1, :cond_29f

    #@25a
    .line 3244
    :cond_25a
    move-object/from16 v0, p0

    #@25c
    move/from16 v1, v22

    #@25e
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@261
    move-result-object v8

    #@262
    .line 3245
    .restart local v8       #child:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    #@265
    move-result v31

    #@266
    .line 3247
    .local v31, top:I
    move/from16 v0, v31

    #@268
    if-le v0, v15, :cond_29f

    #@26a
    .line 3248
    if-nez v5, :cond_28a

    #@26c
    add-int v32, v17, v22

    #@26e
    move/from16 v0, v32

    #@270
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@273
    move-result v32

    #@274
    if-eqz v32, :cond_2a5

    #@276
    add-int/lit8 v32, v9, -0x1

    #@278
    move/from16 v0, v22

    #@27a
    move/from16 v1, v32

    #@27c
    if-eq v0, v1, :cond_28a

    #@27e
    add-int v32, v17, v22

    #@280
    add-int/lit8 v32, v32, 0x1

    #@282
    move/from16 v0, v32

    #@284
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@287
    move-result v32

    #@288
    if-eqz v32, :cond_2a5

    #@28a
    .line 3251
    :cond_28a
    sub-int v32, v31, v10

    #@28c
    move/from16 v0, v32

    #@28e
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@290
    .line 3252
    move/from16 v0, v31

    #@292
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@294
    .line 3257
    add-int/lit8 v32, v22, -0x1

    #@296
    move-object/from16 v0, p0

    #@298
    move-object/from16 v1, p1

    #@29a
    move/from16 v2, v32

    #@29c
    invoke-virtual {v0, v1, v7, v2}, Landroid/widget/ListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    #@29f
    .line 3241
    .end local v8           #child:Landroid/view/View;
    .end local v31           #top:I
    :cond_29f
    :goto_29f
    add-int/lit8 v22, v22, 0x1

    #@2a1
    goto :goto_242

    #@2a2
    .line 3240
    .end local v22           #i:I
    .end local v30           #start:I
    :cond_2a2
    const/16 v30, 0x0

    #@2a4
    goto :goto_240

    #@2a5
    .line 3258
    .restart local v8       #child:Landroid/view/View;
    .restart local v22       #i:I
    .restart local v30       #start:I
    .restart local v31       #top:I
    :cond_2a5
    if-eqz v16, :cond_29f

    #@2a7
    .line 3259
    sub-int v32, v31, v10

    #@2a9
    move/from16 v0, v32

    #@2ab
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@2ad
    .line 3260
    move/from16 v0, v31

    #@2af
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@2b1
    .line 3261
    move-object/from16 v0, p1

    #@2b3
    move-object/from16 v1, v28

    #@2b5
    invoke-virtual {v0, v7, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@2b8
    goto :goto_29f

    #@2b9
    .line 3267
    .end local v8           #child:Landroid/view/View;
    .end local v31           #top:I
    :cond_2b9
    if-lez v9, :cond_20f

    #@2bb
    if-lez v29, :cond_20f

    #@2bd
    .line 3268
    if-eqz v12, :cond_2d6

    #@2bf
    .line 3269
    move-object/from16 v0, p0

    #@2c1
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@2c3
    .line 3270
    .local v3, absListBottom:I
    iput v3, v7, Landroid/graphics/Rect;->top:I

    #@2c5
    .line 3271
    add-int v32, v3, v29

    #@2c7
    move/from16 v0, v32

    #@2c9
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@2cb
    .line 3272
    move-object/from16 v0, p0

    #@2cd
    move-object/from16 v1, p1

    #@2cf
    move-object/from16 v2, v26

    #@2d1
    invoke-virtual {v0, v1, v2, v7}, Landroid/widget/ListView;->drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    #@2d4
    goto/16 :goto_20f

    #@2d6
    .line 3273
    .end local v3           #absListBottom:I
    :cond_2d6
    if-eqz v11, :cond_20f

    #@2d8
    .line 3274
    move/from16 v0, v24

    #@2da
    iput v0, v7, Landroid/graphics/Rect;->top:I

    #@2dc
    .line 3275
    add-int v32, v24, v10

    #@2de
    move/from16 v0, v32

    #@2e0
    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    #@2e2
    .line 3276
    const/16 v32, -0x1

    #@2e4
    move-object/from16 v0, p0

    #@2e6
    move-object/from16 v1, p1

    #@2e8
    move/from16 v2, v32

    #@2ea
    invoke-virtual {v0, v1, v7, v2}, Landroid/widget/ListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    #@2ed
    goto/16 :goto_20f
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 2088
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v1

    #@4
    .line 2089
    .local v1, handled:Z
    if-nez v1, :cond_1a

    #@6
    .line 2091
    invoke-virtual {p0}, Landroid/widget/ListView;->getFocusedChild()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 2092
    .local v0, focused:Landroid/view/View;
    if-eqz v0, :cond_1a

    #@c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_1a

    #@12
    .line 2095
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@15
    move-result v2

    #@16
    invoke-virtual {p0, v2, p1}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@19
    move-result v1

    #@1a
    .line 2098
    .end local v0           #focused:Landroid/view/View;
    :cond_1a
    return v1
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 7
    .parameter "canvas"
    .parameter "child"
    .parameter "drawingTime"

    #@0
    .prologue
    .line 3288
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AbsListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@3
    move-result v0

    #@4
    .line 3289
    .local v0, more:Z
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mCachingActive:Z

    #@6
    if-eqz v1, :cond_f

    #@8
    iget-boolean v1, p2, Landroid/view/View;->mCachingFailed:Z

    #@a
    if-eqz v1, :cond_f

    #@c
    .line 3290
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Landroid/widget/AbsListView;->mCachingActive:Z

    #@f
    .line 3292
    :cond_f
    return v0
.end method

.method drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .registers 5
    .parameter "canvas"
    .parameter "bounds"
    .parameter "childIndex"

    #@0
    .prologue
    .line 3306
    iget-object v0, p0, Landroid/widget/ListView;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    .line 3308
    .local v0, divider:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@5
    .line 3309
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@8
    .line 3310
    return-void
.end method

.method drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .registers 8
    .parameter "canvas"
    .parameter "drawable"
    .parameter "bounds"

    #@0
    .prologue
    .line 3116
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@3
    move-result v0

    #@4
    .line 3118
    .local v0, height:I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@7
    .line 3119
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    #@a
    .line 3121
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@c
    iget v3, p3, Landroid/graphics/Rect;->top:I

    #@e
    sub-int v1, v2, v3

    #@10
    .line 3122
    .local v1, span:I
    if-ge v1, v0, :cond_17

    #@12
    .line 3123
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@14
    add-int/2addr v2, v0

    #@15
    iput v2, p3, Landroid/graphics/Rect;->bottom:I

    #@17
    .line 3126
    :cond_17
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@1a
    .line 3127
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@1d
    .line 3129
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@20
    .line 3130
    return-void
.end method

.method drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .registers 8
    .parameter "canvas"
    .parameter "drawable"
    .parameter "bounds"

    #@0
    .prologue
    .line 3099
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@3
    move-result v0

    #@4
    .line 3101
    .local v0, height:I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@7
    .line 3102
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    #@a
    .line 3104
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@c
    iget v3, p3, Landroid/graphics/Rect;->top:I

    #@e
    sub-int v1, v2, v3

    #@10
    .line 3105
    .local v1, span:I
    if-ge v1, v0, :cond_17

    #@12
    .line 3106
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@14
    sub-int/2addr v2, v0

    #@15
    iput v2, p3, Landroid/graphics/Rect;->top:I

    #@17
    .line 3109
    :cond_17
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@1a
    .line 3110
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@1d
    .line 3112
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@20
    .line 3113
    return-void
.end method

.method fillGap(Z)V
    .registers 8
    .parameter "down"

    #@0
    .prologue
    const/16 v5, 0x22

    #@2
    .line 630
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 631
    .local v0, count:I
    if-eqz p1, :cond_33

    #@8
    .line 632
    const/4 v2, 0x0

    #@9
    .line 633
    .local v2, paddingTop:I
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@b
    and-int/lit8 v4, v4, 0x22

    #@d
    if-ne v4, v5, :cond_13

    #@f
    .line 634
    invoke-virtual {p0}, Landroid/widget/ListView;->getListPaddingTop()I

    #@12
    move-result v2

    #@13
    .line 636
    :cond_13
    if-lez v0, :cond_31

    #@15
    add-int/lit8 v4, v0, -0x1

    #@17
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@1e
    move-result v4

    #@1f
    iget v5, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@21
    add-int v3, v4, v5

    #@23
    .line 638
    .local v3, startOffset:I
    :goto_23
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@25
    add-int/2addr v4, v0

    #@26
    invoke-direct {p0, v4, v3}, Landroid/widget/ListView;->fillDown(II)Landroid/view/View;

    #@29
    .line 639
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@2c
    move-result v4

    #@2d
    invoke-direct {p0, v4}, Landroid/widget/ListView;->correctTooHigh(I)V

    #@30
    .line 650
    .end local v2           #paddingTop:I
    :goto_30
    return-void

    #@31
    .end local v3           #startOffset:I
    .restart local v2       #paddingTop:I
    :cond_31
    move v3, v2

    #@32
    .line 636
    goto :goto_23

    #@33
    .line 641
    .end local v2           #paddingTop:I
    :cond_33
    const/4 v1, 0x0

    #@34
    .line 642
    .local v1, paddingBottom:I
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@36
    and-int/lit8 v4, v4, 0x22

    #@38
    if-ne v4, v5, :cond_3e

    #@3a
    .line 643
    invoke-virtual {p0}, Landroid/widget/ListView;->getListPaddingBottom()I

    #@3d
    move-result v1

    #@3e
    .line 645
    :cond_3e
    if-lez v0, :cond_5c

    #@40
    const/4 v4, 0x0

    #@41
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    #@48
    move-result v4

    #@49
    iget v5, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@4b
    sub-int v3, v4, v5

    #@4d
    .line 647
    .restart local v3       #startOffset:I
    :goto_4d
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4f
    add-int/lit8 v4, v4, -0x1

    #@51
    invoke-direct {p0, v4, v3}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@54
    .line 648
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@57
    move-result v4

    #@58
    invoke-direct {p0, v4}, Landroid/widget/ListView;->correctTooLow(I)V

    #@5b
    goto :goto_30

    #@5c
    .line 645
    .end local v3           #startOffset:I
    :cond_5c
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@5f
    move-result v4

    #@60
    sub-int v3, v4, v1

    #@62
    goto :goto_4d
.end method

.method findMotionRow(I)I
    .registers 6
    .parameter "y"

    #@0
    .prologue
    .line 1286
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 1287
    .local v0, childCount:I
    if-lez v0, :cond_33

    #@6
    .line 1288
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@8
    if-nez v3, :cond_1e

    #@a
    .line 1289
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_33

    #@d
    .line 1290
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v2

    #@11
    .line 1291
    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    #@14
    move-result v3

    #@15
    if-gt p1, v3, :cond_1b

    #@17
    .line 1292
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@19
    add-int/2addr v3, v1

    #@1a
    .line 1304
    .end local v1           #i:I
    .end local v2           #v:Landroid/view/View;
    :goto_1a
    return v3

    #@1b
    .line 1289
    .restart local v1       #i:I
    .restart local v2       #v:Landroid/view/View;
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_b

    #@1e
    .line 1296
    .end local v1           #i:I
    .end local v2           #v:Landroid/view/View;
    :cond_1e
    add-int/lit8 v1, v0, -0x1

    #@20
    .restart local v1       #i:I
    :goto_20
    if-ltz v1, :cond_33

    #@22
    .line 1297
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@25
    move-result-object v2

    #@26
    .line 1298
    .restart local v2       #v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@29
    move-result v3

    #@2a
    if-lt p1, v3, :cond_30

    #@2c
    .line 1299
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2e
    add-int/2addr v3, v1

    #@2f
    goto :goto_1a

    #@30
    .line 1296
    :cond_30
    add-int/lit8 v1, v1, -0x1

    #@32
    goto :goto_20

    #@33
    .line 1304
    .end local v1           #i:I
    .end local v2           #v:Landroid/view/View;
    :cond_33
    const/4 v3, -0x1

    #@34
    goto :goto_1a
.end method

.method findViewByPredicateInHeadersOrFooters(Ljava/util/ArrayList;Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter "childToSkip"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 3614
    .local p1, where:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    .local p2, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    if-eqz p1, :cond_23

    #@2
    .line 3615
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 3618
    .local v1, len:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_23

    #@9
    .line 3619
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@f
    iget-object v2, v3, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@11
    .line 3621
    .local v2, v:Landroid/view/View;
    if-eq v2, p3, :cond_20

    #@13
    invoke-virtual {v2}, Landroid/view/View;->isRootNamespace()Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_20

    #@19
    .line 3622
    invoke-virtual {v2, p2}, Landroid/view/View;->findViewByPredicate(Lcom/android/internal/util/Predicate;)Landroid/view/View;

    #@1c
    move-result-object v2

    #@1d
    .line 3624
    if-eqz v2, :cond_20

    #@1f
    .line 3630
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :goto_1f
    return-object v2

    #@20
    .line 3618
    .restart local v0       #i:I
    .restart local v1       #len:I
    .restart local v2       #v:Landroid/view/View;
    :cond_20
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_7

    #@23
    .line 3630
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_1f
.end method

.method protected findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter "childToSkip"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 3592
    .local p1, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    invoke-super {p0, p1, p2}, Landroid/widget/AbsListView;->findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 3593
    .local v0, v:Landroid/view/View;
    if-nez v0, :cond_1a

    #@6
    .line 3594
    iget-object v2, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {p0, v2, p1, p2}, Landroid/widget/ListView;->findViewByPredicateInHeadersOrFooters(Ljava/util/ArrayList;Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 3595
    if-eqz v0, :cond_10

    #@e
    move-object v1, v0

    #@f
    .line 3604
    .end local v0           #v:Landroid/view/View;
    .local v1, v:Landroid/view/View;
    :goto_f
    return-object v1

    #@10
    .line 3599
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_10
    iget-object v2, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@12
    invoke-virtual {p0, v2, p1, p2}, Landroid/widget/ListView;->findViewByPredicateInHeadersOrFooters(Ljava/util/ArrayList;Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 3600
    if-eqz v0, :cond_1a

    #@18
    move-object v1, v0

    #@19
    .line 3601
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f

    #@1a
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_1a
    move-object v1, v0

    #@1b
    .line 3604
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f
.end method

.method findViewInHeadersOrFooters(Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 7
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 3519
    .local p1, where:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    if-eqz p1, :cond_21

    #@2
    .line 3520
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 3523
    .local v1, len:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_21

    #@9
    .line 3524
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@f
    iget-object v2, v3, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@11
    .line 3526
    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->isRootNamespace()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_1e

    #@17
    .line 3527
    invoke-virtual {v2, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v2

    #@1b
    .line 3529
    if-eqz v2, :cond_1e

    #@1d
    .line 3535
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :goto_1d
    return-object v2

    #@1e
    .line 3523
    .restart local v0       #i:I
    .restart local v1       #len:I
    .restart local v2       #v:Landroid/view/View;
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_7

    #@21
    .line 3535
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_1d
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 3500
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->findViewTraversal(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 3501
    .local v0, v:Landroid/view/View;
    if-nez v0, :cond_1a

    #@6
    .line 3502
    iget-object v2, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {p0, v2, p1}, Landroid/widget/ListView;->findViewInHeadersOrFooters(Ljava/util/ArrayList;I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 3503
    if-eqz v0, :cond_10

    #@e
    move-object v1, v0

    #@f
    .line 3511
    .end local v0           #v:Landroid/view/View;
    .local v1, v:Landroid/view/View;
    :goto_f
    return-object v1

    #@10
    .line 3506
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_10
    iget-object v2, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@12
    invoke-virtual {p0, v2, p1}, Landroid/widget/ListView;->findViewInHeadersOrFooters(Ljava/util/ArrayList;I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 3507
    if-eqz v0, :cond_1a

    #@18
    move-object v1, v0

    #@19
    .line 3508
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f

    #@1a
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_1a
    move-object v1, v0

    #@1b
    .line 3511
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f
.end method

.method findViewWithTagInHeadersOrFooters(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter "tag"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ListView$FixedViewInfo;",
            ">;",
            "Ljava/lang/Object;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 3565
    .local p1, where:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/ListView$FixedViewInfo;>;"
    if-eqz p1, :cond_21

    #@2
    .line 3566
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 3569
    .local v1, len:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_21

    #@9
    .line 3570
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/widget/ListView$FixedViewInfo;

    #@f
    iget-object v2, v3, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    #@11
    .line 3572
    .local v2, v:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->isRootNamespace()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_1e

    #@17
    .line 3573
    invoke-virtual {v2, p2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@1a
    move-result-object v2

    #@1b
    .line 3575
    if-eqz v2, :cond_1e

    #@1d
    .line 3581
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :goto_1d
    return-object v2

    #@1e
    .line 3569
    .restart local v0       #i:I
    .restart local v1       #len:I
    .restart local v2       #v:Landroid/view/View;
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_7

    #@21
    .line 3581
    .end local v0           #i:I
    .end local v1           #len:I
    .end local v2           #v:Landroid/view/View;
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_1d
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .registers 5
    .parameter "tag"

    #@0
    .prologue
    .line 3545
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 3546
    .local v0, v:Landroid/view/View;
    if-nez v0, :cond_1a

    #@6
    .line 3547
    iget-object v2, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@8
    invoke-virtual {p0, v2, p1}, Landroid/widget/ListView;->findViewWithTagInHeadersOrFooters(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 3548
    if-eqz v0, :cond_10

    #@e
    move-object v1, v0

    #@f
    .line 3557
    .end local v0           #v:Landroid/view/View;
    .local v1, v:Landroid/view/View;
    :goto_f
    return-object v1

    #@10
    .line 3552
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_10
    iget-object v2, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@12
    invoke-virtual {p0, v2, p1}, Landroid/widget/ListView;->findViewWithTagInHeadersOrFooters(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 3553
    if-eqz v0, :cond_1a

    #@18
    move-object v1, v0

    #@19
    .line 3554
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f

    #@1a
    .end local v1           #v:Landroid/view/View;
    .restart local v0       #v:Landroid/view/View;
    :cond_1a
    move-object v1, v0

    #@1b
    .line 3557
    .end local v0           #v:Landroid/view/View;
    .restart local v1       #v:Landroid/view/View;
    goto :goto_f
.end method

.method fullScroll(I)Z
    .registers 7
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 2321
    const/4 v0, 0x0

    #@2
    .line 2322
    .local v0, moved:Z
    const/16 v2, 0x21

    #@4
    if-ne p1, v2, :cond_29

    #@6
    .line 2323
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8
    if-eqz v2, :cond_1a

    #@a
    .line 2324
    const/4 v2, 0x0

    #@b
    invoke-virtual {p0, v2, v4}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@e
    move-result v1

    #@f
    .line 2325
    .local v1, position:I
    if-ltz v1, :cond_19

    #@11
    .line 2326
    iput v4, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@13
    .line 2327
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setSelectionInt(I)V

    #@16
    .line 2328
    invoke-virtual {p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@19
    .line 2330
    :cond_19
    const/4 v0, 0x1

    #@1a
    .line 2344
    .end local v1           #position:I
    :cond_1a
    :goto_1a
    if-eqz v0, :cond_28

    #@1c
    invoke-virtual {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_28

    #@22
    .line 2345
    invoke-virtual {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    #@25
    .line 2346
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@28
    .line 2349
    :cond_28
    return v0

    #@29
    .line 2332
    :cond_29
    const/16 v2, 0x82

    #@2b
    if-ne p1, v2, :cond_1a

    #@2d
    .line 2333
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2f
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@31
    add-int/lit8 v3, v3, -0x1

    #@33
    if-ge v2, v3, :cond_1a

    #@35
    .line 2334
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@37
    add-int/lit8 v2, v2, -0x1

    #@39
    invoke-virtual {p0, v2, v4}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@3c
    move-result v1

    #@3d
    .line 2335
    .restart local v1       #position:I
    if-ltz v1, :cond_48

    #@3f
    .line 2336
    const/4 v2, 0x3

    #@40
    iput v2, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@42
    .line 2337
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setSelectionInt(I)V

    #@45
    .line 2338
    invoke-virtual {p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@48
    .line 2340
    :cond_48
    const/4 v0, 0x1

    #@49
    goto :goto_1a
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .registers 2

    #@0
    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 412
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method public getCheckItemIds()[J
    .registers 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 3645
    iget-object v8, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    if-eqz v8, :cond_12

    #@5
    iget-object v8, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@7
    invoke-interface {v8}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@a
    move-result v8

    #@b
    if-eqz v8, :cond_12

    #@d
    .line 3646
    invoke-virtual {p0}, Landroid/widget/ListView;->getCheckedItemIds()[J

    #@10
    move-result-object v5

    #@11
    .line 3675
    :cond_11
    :goto_11
    return-object v5

    #@12
    .line 3651
    :cond_12
    iget v8, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@14
    if-eqz v8, :cond_4c

    #@16
    iget-object v8, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@18
    if-eqz v8, :cond_4c

    #@1a
    iget-object v8, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1c
    if-eqz v8, :cond_4c

    #@1e
    .line 3652
    iget-object v7, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@20
    .line 3653
    .local v7, states:Landroid/util/SparseBooleanArray;
    invoke-virtual {v7}, Landroid/util/SparseBooleanArray;->size()I

    #@23
    move-result v3

    #@24
    .line 3654
    .local v3, count:I
    new-array v5, v3, [J

    #@26
    .line 3655
    .local v5, ids:[J
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@28
    .line 3657
    .local v0, adapter:Landroid/widget/ListAdapter;
    const/4 v1, 0x0

    #@29
    .line 3658
    .local v1, checkedCount:I
    const/4 v4, 0x0

    #@2a
    .local v4, i:I
    move v2, v1

    #@2b
    .end local v1           #checkedCount:I
    .local v2, checkedCount:I
    :goto_2b
    if-ge v4, v3, :cond_43

    #@2d
    .line 3659
    invoke-virtual {v7, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@30
    move-result v8

    #@31
    if-eqz v8, :cond_4f

    #@33
    .line 3660
    add-int/lit8 v1, v2, 0x1

    #@35
    .end local v2           #checkedCount:I
    .restart local v1       #checkedCount:I
    invoke-virtual {v7, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@38
    move-result v8

    #@39
    invoke-interface {v0, v8}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@3c
    move-result-wide v8

    #@3d
    aput-wide v8, v5, v2

    #@3f
    .line 3658
    :goto_3f
    add-int/lit8 v4, v4, 0x1

    #@41
    move v2, v1

    #@42
    .end local v1           #checkedCount:I
    .restart local v2       #checkedCount:I
    goto :goto_2b

    #@43
    .line 3666
    :cond_43
    if-eq v2, v3, :cond_11

    #@45
    .line 3669
    new-array v6, v2, [J

    #@47
    .line 3670
    .local v6, result:[J
    invoke-static {v5, v10, v6, v10, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4a
    move-object v5, v6

    #@4b
    .line 3672
    goto :goto_11

    #@4c
    .line 3675
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v2           #checkedCount:I
    .end local v3           #count:I
    .end local v4           #i:I
    .end local v5           #ids:[J
    .end local v6           #result:[J
    .end local v7           #states:Landroid/util/SparseBooleanArray;
    :cond_4c
    new-array v5, v10, [J

    #@4e
    goto :goto_11

    #@4f
    .restart local v0       #adapter:Landroid/widget/ListAdapter;
    .restart local v2       #checkedCount:I
    .restart local v3       #count:I
    .restart local v4       #i:I
    .restart local v5       #ids:[J
    .restart local v7       #states:Landroid/util/SparseBooleanArray;
    :cond_4f
    move v1, v2

    #@50
    .end local v2           #checkedCount:I
    .restart local v1       #checkedCount:I
    goto :goto_3f
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 3318
    iget-object v0, p0, Landroid/widget/ListView;->mDivider:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getDividerHeight()I
    .registers 2

    #@0
    .prologue
    .line 3343
    iget v0, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@2
    return v0
.end method

.method public getFooterViewsCount()I
    .registers 2

    #@0
    .prologue
    .line 376
    iget-object v0, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getHeaderViewsCount()I
    .registers 2

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getItemsCanFocus()Z
    .registers 2

    #@0
    .prologue
    .line 3061
    iget-boolean v0, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@2
    return v0
.end method

.method public getMaxScrollAmount()I
    .registers 4

    #@0
    .prologue
    .line 191
    const v0, 0x3ea8f5c3

    #@3
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@5
    iget v2, p0, Landroid/view/View;->mTop:I

    #@7
    sub-int/2addr v1, v2

    #@8
    int-to-float v1, v1

    #@9
    mul-float/2addr v0, v1

    #@a
    float-to-int v0, v0

    #@b
    return v0
.end method

.method public getOverscrollFooter()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 3420
    iget-object v0, p0, Landroid/widget/ListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getOverscrollHeader()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 3401
    iget-object v0, p0, Landroid/widget/ListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public isOpaque()Z
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 3066
    iget-boolean v5, p0, Landroid/widget/AbsListView;->mCachingActive:Z

    #@3
    if-eqz v5, :cond_13

    #@5
    iget-boolean v5, p0, Landroid/widget/ListView;->mIsCacheColorOpaque:Z

    #@7
    if-eqz v5, :cond_13

    #@9
    iget-boolean v5, p0, Landroid/widget/ListView;->mDividerIsOpaque:Z

    #@b
    if-eqz v5, :cond_13

    #@d
    invoke-virtual {p0}, Landroid/widget/ListView;->hasOpaqueScrollbars()Z

    #@10
    move-result v5

    #@11
    if-nez v5, :cond_19

    #@13
    :cond_13
    invoke-super {p0}, Landroid/widget/AbsListView;->isOpaque()Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_32

    #@19
    :cond_19
    const/4 v4, 0x1

    #@1a
    .line 3068
    .local v4, retValue:Z
    :goto_1a
    if-eqz v4, :cond_31

    #@1c
    .line 3070
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1e
    if-eqz v5, :cond_34

    #@20
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@22
    iget v3, v5, Landroid/graphics/Rect;->top:I

    #@24
    .line 3071
    .local v3, listTop:I
    :goto_24
    invoke-virtual {p0, v6}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@27
    move-result-object v0

    #@28
    .line 3072
    .local v0, first:Landroid/view/View;
    if-eqz v0, :cond_30

    #@2a
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@2d
    move-result v5

    #@2e
    if-le v5, v3, :cond_37

    #@30
    :cond_30
    move v4, v6

    #@31
    .line 3082
    .end local v0           #first:Landroid/view/View;
    .end local v3           #listTop:I
    .end local v4           #retValue:Z
    :cond_31
    :goto_31
    return v4

    #@32
    :cond_32
    move v4, v6

    #@33
    .line 3066
    goto :goto_1a

    #@34
    .line 3070
    .restart local v4       #retValue:Z
    :cond_34
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@36
    goto :goto_24

    #@37
    .line 3075
    .restart local v0       #first:Landroid/view/View;
    .restart local v3       #listTop:I
    :cond_37
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@3a
    move-result v7

    #@3b
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3d
    if-eqz v5, :cond_59

    #@3f
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@41
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@43
    :goto_43
    sub-int v2, v7, v5

    #@45
    .line 3077
    .local v2, listBottom:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@48
    move-result v5

    #@49
    add-int/lit8 v5, v5, -0x1

    #@4b
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@4e
    move-result-object v1

    #@4f
    .line 3078
    .local v1, last:Landroid/view/View;
    if-eqz v1, :cond_57

    #@51
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    #@54
    move-result v5

    #@55
    if-ge v5, v2, :cond_31

    #@57
    :cond_57
    move v4, v6

    #@58
    .line 3079
    goto :goto_31

    #@59
    .line 3075
    .end local v1           #last:Landroid/view/View;
    .end local v2           #listBottom:I
    :cond_59
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@5b
    goto :goto_43
.end method

.method protected layoutChildren()V
    .registers 34

    #@0
    .prologue
    .line 1463
    move-object/from16 v0, p0

    #@2
    iget-boolean v13, v0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@4
    .line 1464
    .local v13, blockLayoutRequests:Z
    if-nez v13, :cond_24

    #@6
    .line 1465
    const/4 v2, 0x1

    #@7
    move-object/from16 v0, p0

    #@9
    iput-boolean v2, v0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@b
    .line 1471
    :try_start_b
    invoke-super/range {p0 .. p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@e
    .line 1473
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->invalidate()V

    #@11
    .line 1475
    move-object/from16 v0, p0

    #@13
    iget-object v2, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@15
    if-nez v2, :cond_25

    #@17
    .line 1476
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->resetList()V

    #@1a
    .line 1477
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V
    :try_end_1d
    .catchall {:try_start_b .. :try_end_1d} :catchall_126

    #@1d
    .line 1755
    if-nez v13, :cond_24

    #@1f
    .line 1756
    :goto_1f
    const/4 v2, 0x0

    #@20
    move-object/from16 v0, p0

    #@22
    iput-boolean v2, v0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@24
    .line 1759
    :cond_24
    return-void

    #@25
    .line 1481
    :cond_25
    :try_start_25
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@29
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@2b
    .line 1482
    .local v6, childrenTop:I
    move-object/from16 v0, p0

    #@2d
    iget v2, v0, Landroid/view/View;->mBottom:I

    #@2f
    move-object/from16 v0, p0

    #@31
    iget v0, v0, Landroid/view/View;->mTop:I

    #@33
    move/from16 v31, v0

    #@35
    sub-int v2, v2, v31

    #@37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3b
    move-object/from16 v31, v0

    #@3d
    move-object/from16 v0, v31

    #@3f
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@41
    move/from16 v31, v0

    #@43
    sub-int v7, v2, v31

    #@45
    .line 1484
    .local v7, childrenBottom:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getChildCount()I

    #@48
    move-result v15

    #@49
    .line 1485
    .local v15, childCount:I
    const/16 v24, 0x0

    #@4b
    .line 1486
    .local v24, index:I
    const/4 v5, 0x0

    #@4c
    .line 1489
    .local v5, delta:I
    const/4 v3, 0x0

    #@4d
    .line 1490
    .local v3, oldSel:Landroid/view/View;
    const/16 v25, 0x0

    #@4f
    .line 1491
    .local v25, oldFirst:Landroid/view/View;
    const/4 v4, 0x0

    #@50
    .line 1493
    .local v4, newSel:Landroid/view/View;
    const/16 v19, 0x0

    #@52
    .line 1495
    .local v19, focusLayoutRestoreView:Landroid/view/View;
    const/4 v10, 0x0

    #@53
    .line 1496
    .local v10, accessibilityFocusLayoutRestoreNode:Landroid/view/accessibility/AccessibilityNodeInfo;
    const/4 v11, 0x0

    #@54
    .line 1497
    .local v11, accessibilityFocusLayoutRestoreView:Landroid/view/View;
    const/4 v12, -0x1

    #@55
    .line 1500
    .local v12, accessibilityFocusPosition:I
    move-object/from16 v0, p0

    #@57
    iget v2, v0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@59
    packed-switch v2, :pswitch_data_3ee

    #@5c
    .line 1515
    move-object/from16 v0, p0

    #@5e
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@60
    move-object/from16 v0, p0

    #@62
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@64
    move/from16 v31, v0

    #@66
    sub-int v24, v2, v31

    #@68
    .line 1516
    if-ltz v24, :cond_76

    #@6a
    move/from16 v0, v24

    #@6c
    if-ge v0, v15, :cond_76

    #@6e
    .line 1517
    move-object/from16 v0, p0

    #@70
    move/from16 v1, v24

    #@72
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@75
    move-result-object v3

    #@76
    .line 1521
    :cond_76
    const/4 v2, 0x0

    #@77
    move-object/from16 v0, p0

    #@79
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@7c
    move-result-object v25

    #@7d
    .line 1523
    move-object/from16 v0, p0

    #@7f
    iget v2, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@81
    if-ltz v2, :cond_8f

    #@83
    .line 1524
    move-object/from16 v0, p0

    #@85
    iget v2, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@87
    move-object/from16 v0, p0

    #@89
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8b
    move/from16 v31, v0

    #@8d
    sub-int v5, v2, v31

    #@8f
    .line 1528
    :cond_8f
    add-int v2, v24, v5

    #@91
    move-object/from16 v0, p0

    #@93
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@96
    move-result-object v4

    #@97
    .line 1532
    :cond_97
    :goto_97
    :pswitch_97
    move-object/from16 v0, p0

    #@99
    iget-boolean v0, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@9b
    move/from16 v16, v0

    #@9d
    .line 1533
    .local v16, dataChanged:Z
    if-eqz v16, :cond_a2

    #@9f
    .line 1534
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->handleDataChanged()V

    #@a2
    .line 1539
    :cond_a2
    move-object/from16 v0, p0

    #@a4
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@a6
    if-nez v2, :cond_cd

    #@a8
    .line 1540
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->resetList()V

    #@ab
    .line 1541
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@ae
    .line 1755
    if-nez v13, :cond_24

    #@b0
    goto/16 :goto_1f

    #@b2
    .line 1502
    .end local v16           #dataChanged:Z
    :pswitch_b2
    move-object/from16 v0, p0

    #@b4
    iget v2, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@ba
    move/from16 v31, v0

    #@bc
    sub-int v24, v2, v31

    #@be
    .line 1503
    if-ltz v24, :cond_97

    #@c0
    move/from16 v0, v24

    #@c2
    if-ge v0, v15, :cond_97

    #@c4
    .line 1504
    move-object/from16 v0, p0

    #@c6
    move/from16 v1, v24

    #@c8
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@cb
    move-result-object v4

    #@cc
    goto :goto_97

    #@cd
    .line 1543
    .restart local v16       #dataChanged:Z
    :cond_cd
    move-object/from16 v0, p0

    #@cf
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@d5
    move-object/from16 v31, v0

    #@d7
    invoke-interface/range {v31 .. v31}, Landroid/widget/ListAdapter;->getCount()I

    #@da
    move-result v31

    #@db
    move/from16 v0, v31

    #@dd
    if-eq v2, v0, :cond_132

    #@df
    .line 1544
    new-instance v2, Ljava/lang/IllegalStateException;

    #@e1
    new-instance v31, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v32, "The content of the adapter has changed but ListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in ListView("

    #@e8
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v31

    #@ec
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getId()I

    #@ef
    move-result v32

    #@f0
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v31

    #@f4
    const-string v32, ", "

    #@f6
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v31

    #@fa
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@fd
    move-result-object v32

    #@fe
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v31

    #@102
    const-string v32, ") with Adapter("

    #@104
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v31

    #@108
    move-object/from16 v0, p0

    #@10a
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@10c
    move-object/from16 v32, v0

    #@10e
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@111
    move-result-object v32

    #@112
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v31

    #@116
    const-string v32, ")]"

    #@118
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v31

    #@11c
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v31

    #@120
    move-object/from16 v0, v31

    #@122
    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@125
    throw v2
    :try_end_126
    .catchall {:try_start_25 .. :try_end_126} :catchall_126

    #@126
    .line 1755
    .end local v3           #oldSel:Landroid/view/View;
    .end local v4           #newSel:Landroid/view/View;
    .end local v5           #delta:I
    .end local v6           #childrenTop:I
    .end local v7           #childrenBottom:I
    .end local v10           #accessibilityFocusLayoutRestoreNode:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v11           #accessibilityFocusLayoutRestoreView:Landroid/view/View;
    .end local v12           #accessibilityFocusPosition:I
    .end local v15           #childCount:I
    .end local v16           #dataChanged:Z
    .end local v19           #focusLayoutRestoreView:Landroid/view/View;
    .end local v24           #index:I
    .end local v25           #oldFirst:Landroid/view/View;
    :catchall_126
    move-exception v2

    #@127
    if-nez v13, :cond_131

    #@129
    .line 1756
    const/16 v31, 0x0

    #@12b
    move/from16 v0, v31

    #@12d
    move-object/from16 v1, p0

    #@12f
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@131
    .line 1755
    :cond_131
    throw v2

    #@132
    .line 1551
    .restart local v3       #oldSel:Landroid/view/View;
    .restart local v4       #newSel:Landroid/view/View;
    .restart local v5       #delta:I
    .restart local v6       #childrenTop:I
    .restart local v7       #childrenBottom:I
    .restart local v10       #accessibilityFocusLayoutRestoreNode:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v11       #accessibilityFocusLayoutRestoreView:Landroid/view/View;
    .restart local v12       #accessibilityFocusPosition:I
    .restart local v15       #childCount:I
    .restart local v16       #dataChanged:Z
    .restart local v19       #focusLayoutRestoreView:Landroid/view/View;
    .restart local v24       #index:I
    .restart local v25       #oldFirst:Landroid/view/View;
    :cond_132
    :try_start_132
    move-object/from16 v0, p0

    #@134
    iget v2, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@136
    move-object/from16 v0, p0

    #@138
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelectedPositionInt(I)V

    #@13b
    .line 1555
    move-object/from16 v0, p0

    #@13d
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@13f
    move/from16 v17, v0

    #@141
    .line 1556
    .local v17, firstPosition:I
    move-object/from16 v0, p0

    #@143
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@145
    move-object/from16 v27, v0

    #@147
    .line 1559
    .local v27, recycleBin:Landroid/widget/AbsListView$RecycleBin;
    const/16 v18, 0x0

    #@149
    .line 1563
    .local v18, focusLayoutRestoreDirectChild:Landroid/view/View;
    if-eqz v16, :cond_165

    #@14b
    .line 1564
    const/16 v23, 0x0

    #@14d
    .local v23, i:I
    :goto_14d
    move/from16 v0, v23

    #@14f
    if-ge v0, v15, :cond_16c

    #@151
    .line 1565
    move-object/from16 v0, p0

    #@153
    move/from16 v1, v23

    #@155
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@158
    move-result-object v2

    #@159
    add-int v31, v17, v23

    #@15b
    move-object/from16 v0, v27

    #@15d
    move/from16 v1, v31

    #@15f
    invoke-virtual {v0, v2, v1}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@162
    .line 1564
    add-int/lit8 v23, v23, 0x1

    #@164
    goto :goto_14d

    #@165
    .line 1568
    .end local v23           #i:I
    :cond_165
    move-object/from16 v0, v27

    #@167
    move/from16 v1, v17

    #@169
    invoke-virtual {v0, v15, v1}, Landroid/widget/AbsListView$RecycleBin;->fillActiveViews(II)V

    #@16c
    .line 1575
    :cond_16c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getFocusedChild()Landroid/view/View;

    #@16f
    move-result-object v22

    #@170
    .line 1576
    .local v22, focusedChild:Landroid/view/View;
    if-eqz v22, :cond_18c

    #@172
    .line 1581
    if-eqz v16, :cond_17e

    #@174
    move-object/from16 v0, p0

    #@176
    move-object/from16 v1, v22

    #@178
    invoke-direct {v0, v1}, Landroid/widget/ListView;->isDirectChildHeaderOrFooter(Landroid/view/View;)Z

    #@17b
    move-result v2

    #@17c
    if-eqz v2, :cond_189

    #@17e
    .line 1582
    :cond_17e
    move-object/from16 v18, v22

    #@180
    .line 1584
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->findFocus()Landroid/view/View;

    #@183
    move-result-object v19

    #@184
    .line 1585
    if-eqz v19, :cond_189

    #@186
    .line 1587
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->onStartTemporaryDetach()V

    #@189
    .line 1590
    :cond_189
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->requestFocus()Z

    #@18c
    .line 1594
    :cond_18c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@18f
    move-result-object v30

    #@190
    .line 1595
    .local v30, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v30, :cond_1af

    #@192
    .line 1596
    invoke-virtual/range {v30 .. v30}, Landroid/view/ViewRootImpl;->getAccessibilityFocusedHost()Landroid/view/View;

    #@195
    move-result-object v9

    #@196
    .line 1597
    .local v9, accessFocusedView:Landroid/view/View;
    if-eqz v9, :cond_1af

    #@198
    .line 1598
    move-object/from16 v0, p0

    #@19a
    invoke-direct {v0, v9}, Landroid/widget/ListView;->findAccessibilityFocusedChild(Landroid/view/View;)Landroid/view/View;

    #@19d
    move-result-object v8

    #@19e
    .line 1600
    .local v8, accessFocusedChild:Landroid/view/View;
    if-eqz v8, :cond_1af

    #@1a0
    .line 1601
    if-eqz v16, :cond_1aa

    #@1a2
    move-object/from16 v0, p0

    #@1a4
    invoke-direct {v0, v8}, Landroid/widget/ListView;->isDirectChildHeaderOrFooter(Landroid/view/View;)Z

    #@1a7
    move-result v2

    #@1a8
    if-eqz v2, :cond_275

    #@1aa
    .line 1605
    :cond_1aa
    move-object v11, v9

    #@1ab
    .line 1606
    invoke-virtual/range {v30 .. v30}, Landroid/view/ViewRootImpl;->getAccessibilityFocusedVirtualView()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1ae
    move-result-object v10

    #@1af
    .line 1618
    .end local v8           #accessFocusedChild:Landroid/view/View;
    .end local v9           #accessFocusedView:Landroid/view/View;
    :cond_1af
    :goto_1af
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->detachAllViewsFromParent()V

    #@1b2
    .line 1619
    invoke-virtual/range {v27 .. v27}, Landroid/widget/AbsListView$RecycleBin;->removeSkippedScrap()V

    #@1b5
    .line 1621
    move-object/from16 v0, p0

    #@1b7
    iget v2, v0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@1b9
    packed-switch v2, :pswitch_data_3fc

    #@1bc
    .line 1648
    if-nez v15, :cond_309

    #@1be
    .line 1649
    move-object/from16 v0, p0

    #@1c0
    iget-boolean v2, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@1c2
    if-nez v2, :cond_2e4

    #@1c4
    .line 1650
    const/4 v2, 0x0

    #@1c5
    const/16 v31, 0x1

    #@1c7
    move-object/from16 v0, p0

    #@1c9
    move/from16 v1, v31

    #@1cb
    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@1ce
    move-result v26

    #@1cf
    .line 1651
    .local v26, position:I
    move-object/from16 v0, p0

    #@1d1
    move/from16 v1, v26

    #@1d3
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelectedPositionInt(I)V

    #@1d6
    .line 1652
    move-object/from16 v0, p0

    #@1d8
    invoke-direct {v0, v6}, Landroid/widget/ListView;->fillFromTop(I)Landroid/view/View;

    #@1db
    move-result-object v29

    #@1dc
    .line 1673
    .end local v6           #childrenTop:I
    .end local v26           #position:I
    .local v29, sel:Landroid/view/View;
    :goto_1dc
    invoke-virtual/range {v27 .. v27}, Landroid/widget/AbsListView$RecycleBin;->scrapActiveViews()V

    #@1df
    .line 1675
    if-eqz v29, :cond_377

    #@1e1
    .line 1678
    move-object/from16 v0, p0

    #@1e3
    iget-boolean v2, v0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@1e5
    if-eqz v2, :cond_36d

    #@1e7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->hasFocus()Z

    #@1ea
    move-result v2

    #@1eb
    if-eqz v2, :cond_36d

    #@1ed
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->hasFocus()Z

    #@1f0
    move-result v2

    #@1f1
    if-nez v2, :cond_36d

    #@1f3
    .line 1679
    move-object/from16 v0, v29

    #@1f5
    move-object/from16 v1, v18

    #@1f7
    if-ne v0, v1, :cond_201

    #@1f9
    if-eqz v19, :cond_201

    #@1fb
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->requestFocus()Z

    #@1fe
    move-result v2

    #@1ff
    if-nez v2, :cond_207

    #@201
    :cond_201
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->requestFocus()Z

    #@204
    move-result v2

    #@205
    if-eqz v2, :cond_35a

    #@207
    :cond_207
    const/16 v20, 0x1

    #@209
    .line 1682
    .local v20, focusWasTaken:Z
    :goto_209
    if-nez v20, :cond_35e

    #@20b
    .line 1686
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getFocusedChild()Landroid/view/View;

    #@20e
    move-result-object v21

    #@20f
    .line 1687
    .local v21, focused:Landroid/view/View;
    if-eqz v21, :cond_214

    #@211
    .line 1688
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->clearFocus()V

    #@214
    .line 1690
    :cond_214
    const/4 v2, -0x1

    #@215
    move-object/from16 v0, p0

    #@217
    move-object/from16 v1, v29

    #@219
    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->positionSelector(ILandroid/view/View;)V

    #@21c
    .line 1698
    .end local v20           #focusWasTaken:Z
    .end local v21           #focused:Landroid/view/View;
    :goto_21c
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getTop()I

    #@21f
    move-result v2

    #@220
    move-object/from16 v0, p0

    #@222
    iput v2, v0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@224
    .line 1716
    :cond_224
    :goto_224
    if-eqz v10, :cond_3be

    #@226
    .line 1717
    const/16 v2, 0x40

    #@228
    invoke-virtual {v10, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(I)Z

    #@22b
    .line 1733
    :cond_22b
    :goto_22b
    if-eqz v19, :cond_236

    #@22d
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@230
    move-result-object v2

    #@231
    if-eqz v2, :cond_236

    #@233
    .line 1735
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->onFinishTemporaryDetach()V

    #@236
    .line 1738
    :cond_236
    const/4 v2, 0x0

    #@237
    move-object/from16 v0, p0

    #@239
    iput v2, v0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@23b
    .line 1739
    const/4 v2, 0x0

    #@23c
    move-object/from16 v0, p0

    #@23e
    iput-boolean v2, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@240
    .line 1740
    move-object/from16 v0, p0

    #@242
    iget-object v2, v0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@244
    if-eqz v2, :cond_254

    #@246
    .line 1741
    move-object/from16 v0, p0

    #@248
    iget-object v2, v0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@24a
    move-object/from16 v0, p0

    #@24c
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    #@24f
    .line 1742
    const/4 v2, 0x0

    #@250
    move-object/from16 v0, p0

    #@252
    iput-object v2, v0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@254
    .line 1744
    :cond_254
    const/4 v2, 0x0

    #@255
    move-object/from16 v0, p0

    #@257
    iput-boolean v2, v0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@259
    .line 1745
    move-object/from16 v0, p0

    #@25b
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@25d
    move-object/from16 v0, p0

    #@25f
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setNextSelectedPositionInt(I)V

    #@262
    .line 1747
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->updateScrollIndicators()V

    #@265
    .line 1749
    move-object/from16 v0, p0

    #@267
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@269
    if-lez v2, :cond_26e

    #@26b
    .line 1750
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->checkSelectionChanged()V

    #@26e
    .line 1753
    :cond_26e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@271
    .line 1755
    if-nez v13, :cond_24

    #@273
    goto/16 :goto_1f

    #@275
    .line 1611
    .end local v29           #sel:Landroid/view/View;
    .restart local v6       #childrenTop:I
    .restart local v8       #accessFocusedChild:Landroid/view/View;
    .restart local v9       #accessFocusedView:Landroid/view/View;
    :cond_275
    move-object/from16 v0, p0

    #@277
    invoke-virtual {v0, v8}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    #@27a
    move-result v12

    #@27b
    goto/16 :goto_1af

    #@27d
    .line 1623
    .end local v8           #accessFocusedChild:Landroid/view/View;
    .end local v9           #accessFocusedView:Landroid/view/View;
    :pswitch_27d
    if-eqz v4, :cond_28b

    #@27f
    .line 1624
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    #@282
    move-result v2

    #@283
    move-object/from16 v0, p0

    #@285
    invoke-direct {v0, v2, v6, v7}, Landroid/widget/ListView;->fillFromSelection(III)Landroid/view/View;

    #@288
    move-result-object v29

    #@289
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@28b
    .line 1626
    .end local v29           #sel:Landroid/view/View;
    :cond_28b
    move-object/from16 v0, p0

    #@28d
    invoke-direct {v0, v6, v7}, Landroid/widget/ListView;->fillFromMiddle(II)Landroid/view/View;

    #@290
    move-result-object v29

    #@291
    .line 1628
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@293
    .line 1630
    .end local v29           #sel:Landroid/view/View;
    :pswitch_293
    move-object/from16 v0, p0

    #@295
    iget v2, v0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@297
    move-object/from16 v0, p0

    #@299
    iget v0, v0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@29b
    move/from16 v31, v0

    #@29d
    move-object/from16 v0, p0

    #@29f
    move/from16 v1, v31

    #@2a1
    invoke-direct {v0, v2, v1}, Landroid/widget/ListView;->fillSpecific(II)Landroid/view/View;

    #@2a4
    move-result-object v29

    #@2a5
    .line 1631
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@2a7
    .line 1633
    .end local v29           #sel:Landroid/view/View;
    :pswitch_2a7
    move-object/from16 v0, p0

    #@2a9
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@2ab
    add-int/lit8 v2, v2, -0x1

    #@2ad
    move-object/from16 v0, p0

    #@2af
    invoke-direct {v0, v2, v7}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@2b2
    move-result-object v29

    #@2b3
    .line 1634
    .restart local v29       #sel:Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@2b6
    goto/16 :goto_1dc

    #@2b8
    .line 1637
    .end local v29           #sel:Landroid/view/View;
    :pswitch_2b8
    const/4 v2, 0x0

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iput v2, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2bd
    .line 1638
    move-object/from16 v0, p0

    #@2bf
    invoke-direct {v0, v6}, Landroid/widget/ListView;->fillFromTop(I)Landroid/view/View;

    #@2c2
    move-result-object v29

    #@2c3
    .line 1639
    .restart local v29       #sel:Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Landroid/widget/ListView;->adjustViewsUpOrDown()V

    #@2c6
    goto/16 :goto_1dc

    #@2c8
    .line 1642
    .end local v29           #sel:Landroid/view/View;
    :pswitch_2c8
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->reconcileSelectedPosition()I

    #@2cb
    move-result v2

    #@2cc
    move-object/from16 v0, p0

    #@2ce
    iget v0, v0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@2d0
    move/from16 v31, v0

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    move/from16 v1, v31

    #@2d6
    invoke-direct {v0, v2, v1}, Landroid/widget/ListView;->fillSpecific(II)Landroid/view/View;

    #@2d9
    move-result-object v29

    #@2da
    .line 1643
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@2dc
    .end local v29           #sel:Landroid/view/View;
    :pswitch_2dc
    move-object/from16 v2, p0

    #@2de
    .line 1645
    invoke-direct/range {v2 .. v7}, Landroid/widget/ListView;->moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;

    #@2e1
    move-result-object v29

    #@2e2
    .line 1646
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@2e4
    .line 1654
    .end local v29           #sel:Landroid/view/View;
    :cond_2e4
    move-object/from16 v0, p0

    #@2e6
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@2e8
    add-int/lit8 v2, v2, -0x1

    #@2ea
    const/16 v31, 0x0

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    move/from16 v1, v31

    #@2f0
    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@2f3
    move-result v26

    #@2f4
    .line 1655
    .restart local v26       #position:I
    move-object/from16 v0, p0

    #@2f6
    move/from16 v1, v26

    #@2f8
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelectedPositionInt(I)V

    #@2fb
    .line 1656
    move-object/from16 v0, p0

    #@2fd
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@2ff
    add-int/lit8 v2, v2, -0x1

    #@301
    move-object/from16 v0, p0

    #@303
    invoke-direct {v0, v2, v7}, Landroid/widget/ListView;->fillUp(II)Landroid/view/View;

    #@306
    move-result-object v29

    #@307
    .line 1657
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@309
    .line 1659
    .end local v26           #position:I
    .end local v29           #sel:Landroid/view/View;
    :cond_309
    move-object/from16 v0, p0

    #@30b
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@30d
    if-ltz v2, :cond_330

    #@30f
    move-object/from16 v0, p0

    #@311
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@313
    move-object/from16 v0, p0

    #@315
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@317
    move/from16 v31, v0

    #@319
    move/from16 v0, v31

    #@31b
    if-ge v2, v0, :cond_330

    #@31d
    .line 1660
    move-object/from16 v0, p0

    #@31f
    iget v2, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@321
    if-nez v3, :cond_32b

    #@323
    .end local v6           #childrenTop:I
    :goto_323
    move-object/from16 v0, p0

    #@325
    invoke-direct {v0, v2, v6}, Landroid/widget/ListView;->fillSpecific(II)Landroid/view/View;

    #@328
    move-result-object v29

    #@329
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@32b
    .end local v29           #sel:Landroid/view/View;
    .restart local v6       #childrenTop:I
    :cond_32b
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@32e
    move-result v6

    #@32f
    goto :goto_323

    #@330
    .line 1662
    :cond_330
    move-object/from16 v0, p0

    #@332
    iget v2, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@334
    move-object/from16 v0, p0

    #@336
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@338
    move/from16 v31, v0

    #@33a
    move/from16 v0, v31

    #@33c
    if-ge v2, v0, :cond_351

    #@33e
    .line 1663
    move-object/from16 v0, p0

    #@340
    iget v2, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@342
    if-nez v25, :cond_34c

    #@344
    .end local v6           #childrenTop:I
    :goto_344
    move-object/from16 v0, p0

    #@346
    invoke-direct {v0, v2, v6}, Landroid/widget/ListView;->fillSpecific(II)Landroid/view/View;

    #@349
    move-result-object v29

    #@34a
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@34c
    .end local v29           #sel:Landroid/view/View;
    .restart local v6       #childrenTop:I
    :cond_34c
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getTop()I

    #@34f
    move-result v6

    #@350
    goto :goto_344

    #@351
    .line 1666
    :cond_351
    const/4 v2, 0x0

    #@352
    move-object/from16 v0, p0

    #@354
    invoke-direct {v0, v2, v6}, Landroid/widget/ListView;->fillSpecific(II)Landroid/view/View;

    #@357
    move-result-object v29

    #@358
    .restart local v29       #sel:Landroid/view/View;
    goto/16 :goto_1dc

    #@35a
    .line 1679
    .end local v6           #childrenTop:I
    :cond_35a
    const/16 v20, 0x0

    #@35c
    goto/16 :goto_209

    #@35e
    .line 1692
    .restart local v20       #focusWasTaken:Z
    :cond_35e
    const/4 v2, 0x0

    #@35f
    move-object/from16 v0, v29

    #@361
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    #@364
    .line 1693
    move-object/from16 v0, p0

    #@366
    iget-object v2, v0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@368
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@36b
    goto/16 :goto_21c

    #@36d
    .line 1696
    .end local v20           #focusWasTaken:Z
    :cond_36d
    const/4 v2, -0x1

    #@36e
    move-object/from16 v0, p0

    #@370
    move-object/from16 v1, v29

    #@372
    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->positionSelector(ILandroid/view/View;)V

    #@375
    goto/16 :goto_21c

    #@377
    .line 1700
    :cond_377
    move-object/from16 v0, p0

    #@379
    iget v2, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@37b
    if-lez v2, :cond_3b1

    #@37d
    move-object/from16 v0, p0

    #@37f
    iget v2, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@381
    const/16 v31, 0x3

    #@383
    move/from16 v0, v31

    #@385
    if-ge v2, v0, :cond_3b1

    #@387
    .line 1701
    move-object/from16 v0, p0

    #@389
    iget v2, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@38b
    move-object/from16 v0, p0

    #@38d
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@38f
    move/from16 v31, v0

    #@391
    sub-int v2, v2, v31

    #@393
    move-object/from16 v0, p0

    #@395
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@398
    move-result-object v14

    #@399
    .line 1702
    .local v14, child:Landroid/view/View;
    if-eqz v14, :cond_3a4

    #@39b
    move-object/from16 v0, p0

    #@39d
    iget v2, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@39f
    move-object/from16 v0, p0

    #@3a1
    invoke-virtual {v0, v2, v14}, Landroid/widget/ListView;->positionSelector(ILandroid/view/View;)V

    #@3a4
    .line 1710
    .end local v14           #child:Landroid/view/View;
    :cond_3a4
    :goto_3a4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->hasFocus()Z

    #@3a7
    move-result v2

    #@3a8
    if-eqz v2, :cond_224

    #@3aa
    if-eqz v19, :cond_224

    #@3ac
    .line 1711
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->requestFocus()Z

    #@3af
    goto/16 :goto_224

    #@3b1
    .line 1704
    :cond_3b1
    const/4 v2, 0x0

    #@3b2
    move-object/from16 v0, p0

    #@3b4
    iput v2, v0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@3b6
    .line 1705
    move-object/from16 v0, p0

    #@3b8
    iget-object v2, v0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@3ba
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@3bd
    goto :goto_3a4

    #@3be
    .line 1719
    :cond_3be
    if-eqz v11, :cond_3c5

    #@3c0
    .line 1720
    invoke-virtual {v11}, Landroid/view/View;->requestAccessibilityFocus()Z

    #@3c3
    goto/16 :goto_22b

    #@3c5
    .line 1721
    :cond_3c5
    const/4 v2, -0x1

    #@3c6
    if-eq v12, v2, :cond_22b

    #@3c8
    .line 1723
    move-object/from16 v0, p0

    #@3ca
    iget v2, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3cc
    sub-int v2, v12, v2

    #@3ce
    const/16 v31, 0x0

    #@3d0
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ListView;->getChildCount()I

    #@3d3
    move-result v32

    #@3d4
    add-int/lit8 v32, v32, -0x1

    #@3d6
    move/from16 v0, v31

    #@3d8
    move/from16 v1, v32

    #@3da
    invoke-static {v2, v0, v1}, Landroid/util/MathUtils;->constrain(III)I

    #@3dd
    move-result v26

    #@3de
    .line 1725
    .restart local v26       #position:I
    move-object/from16 v0, p0

    #@3e0
    move/from16 v1, v26

    #@3e2
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@3e5
    move-result-object v28

    #@3e6
    .line 1726
    .local v28, restoreView:Landroid/view/View;
    if-eqz v28, :cond_22b

    #@3e8
    .line 1727
    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->requestAccessibilityFocus()Z
    :try_end_3eb
    .catchall {:try_start_132 .. :try_end_3eb} :catchall_126

    #@3eb
    goto/16 :goto_22b

    #@3ed
    .line 1500
    nop

    #@3ee
    :pswitch_data_3ee
    .packed-switch 0x1
        :pswitch_97
        :pswitch_b2
        :pswitch_97
        :pswitch_97
        :pswitch_97
    .end packed-switch

    #@3fc
    .line 1621
    :pswitch_data_3fc
    .packed-switch 0x1
        :pswitch_2b8
        :pswitch_27d
        :pswitch_2a7
        :pswitch_2c8
        :pswitch_293
        :pswitch_2dc
    .end packed-switch
.end method

.method lookForSelectablePosition(IZ)I
    .registers 7
    .parameter "position"
    .parameter "lookDown"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 2034
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    .line 2035
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-eqz v0, :cond_b

    #@5
    invoke-virtual {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_c

    #@b
    .line 2061
    :cond_b
    :goto_b
    return v2

    #@c
    .line 2039
    :cond_c
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@f
    move-result v1

    #@10
    .line 2040
    .local v1, count:I
    iget-boolean v3, p0, Landroid/widget/ListView;->mAreAllItemsSelectable:Z

    #@12
    if-nez v3, :cond_3d

    #@14
    .line 2041
    if-eqz p2, :cond_26

    #@16
    .line 2042
    const/4 v3, 0x0

    #@17
    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    #@1a
    move-result p1

    #@1b
    .line 2043
    :goto_1b
    if-ge p1, v1, :cond_37

    #@1d
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@20
    move-result v3

    #@21
    if-nez v3, :cond_37

    #@23
    .line 2044
    add-int/lit8 p1, p1, 0x1

    #@25
    goto :goto_1b

    #@26
    .line 2047
    :cond_26
    add-int/lit8 v3, v1, -0x1

    #@28
    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result p1

    #@2c
    .line 2048
    :goto_2c
    if-ltz p1, :cond_37

    #@2e
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@31
    move-result v3

    #@32
    if-nez v3, :cond_37

    #@34
    .line 2049
    add-int/lit8 p1, p1, -0x1

    #@36
    goto :goto_2c

    #@37
    .line 2053
    :cond_37
    if-ltz p1, :cond_b

    #@39
    if-ge p1, v1, :cond_b

    #@3b
    move v2, p1

    #@3c
    .line 2056
    goto :goto_b

    #@3d
    .line 2058
    :cond_3d
    if-ltz p1, :cond_b

    #@3f
    if-ge p1, v1, :cond_b

    #@41
    move v2, p1

    #@42
    .line 2061
    goto :goto_b
.end method

.method final measureHeightOfChildren(IIIII)I
    .registers 18
    .parameter "widthMeasureSpec"
    .parameter "startPosition"
    .parameter "endPosition"
    .parameter "maxHeight"
    .parameter "disallowPartialChildPosition"

    #@0
    .prologue
    .line 1225
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    .line 1226
    .local v1, adapter:Landroid/widget/ListAdapter;
    if-nez v1, :cond_f

    #@4
    .line 1227
    iget-object v10, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@6
    iget v10, v10, Landroid/graphics/Rect;->top:I

    #@8
    iget-object v11, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@a
    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    #@c
    add-int v6, v10, v11

    #@e
    .line 1281
    :cond_e
    :goto_e
    return v6

    #@f
    .line 1231
    :cond_f
    iget-object v10, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@11
    iget v10, v10, Landroid/graphics/Rect;->top:I

    #@13
    iget-object v11, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@15
    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    #@17
    add-int v9, v10, v11

    #@19
    .line 1232
    .local v9, returnedHeight:I
    iget v10, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@1b
    if-lez v10, :cond_6e

    #@1d
    iget-object v10, p0, Landroid/widget/ListView;->mDivider:Landroid/graphics/drawable/Drawable;

    #@1f
    if-eqz v10, :cond_6e

    #@21
    iget v3, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@23
    .line 1235
    .local v3, dividerHeight:I
    :goto_23
    const/4 v6, 0x0

    #@24
    .line 1240
    .local v6, prevHeightWithoutPartialChild:I
    const/4 v10, -0x1

    #@25
    if-ne p3, v10, :cond_2d

    #@27
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    #@2a
    move-result v10

    #@2b
    add-int/lit8 p3, v10, -0x1

    #@2d
    .line 1241
    :cond_2d
    iget-object v7, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@2f
    .line 1242
    .local v7, recycleBin:Landroid/widget/AbsListView$RecycleBin;
    invoke-virtual {p0}, Landroid/widget/ListView;->recycleOnMeasure()Z

    #@32
    move-result v8

    #@33
    .line 1243
    .local v8, recyle:Z
    iget-object v5, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@35
    .line 1245
    .local v5, isScrap:[Z
    move v4, p2

    #@36
    .local v4, i:I
    :goto_36
    if-gt v4, p3, :cond_7a

    #@38
    .line 1246
    invoke-virtual {p0, v4, v5}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@3b
    move-result-object v2

    #@3c
    .line 1248
    .local v2, child:Landroid/view/View;
    invoke-direct {p0, v2, v4, p1}, Landroid/widget/ListView;->measureScrapChild(Landroid/view/View;II)V

    #@3f
    .line 1250
    if-lez v4, :cond_42

    #@41
    .line 1252
    add-int/2addr v9, v3

    #@42
    .line 1256
    :cond_42
    if-eqz v8, :cond_56

    #@44
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@47
    move-result-object v10

    #@48
    check-cast v10, Landroid/widget/AbsListView$LayoutParams;

    #@4a
    iget v10, v10, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@4c
    invoke-virtual {v7, v10}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@4f
    move-result v10

    #@50
    if-eqz v10, :cond_56

    #@52
    .line 1258
    const/4 v10, -0x1

    #@53
    invoke-virtual {v7, v2, v10}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@56
    .line 1261
    :cond_56
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    #@59
    move-result v10

    #@5a
    add-int/2addr v9, v10

    #@5b
    .line 1263
    move/from16 v0, p4

    #@5d
    if-lt v9, v0, :cond_70

    #@5f
    .line 1266
    if-ltz p5, :cond_6b

    #@61
    move/from16 v0, p5

    #@63
    if-le v4, v0, :cond_6b

    #@65
    if-lez v6, :cond_6b

    #@67
    move/from16 v0, p4

    #@69
    if-ne v9, v0, :cond_e

    #@6b
    :cond_6b
    move/from16 v6, p4

    #@6d
    goto :goto_e

    #@6e
    .line 1232
    .end local v2           #child:Landroid/view/View;
    .end local v3           #dividerHeight:I
    .end local v4           #i:I
    .end local v5           #isScrap:[Z
    .end local v6           #prevHeightWithoutPartialChild:I
    .end local v7           #recycleBin:Landroid/widget/AbsListView$RecycleBin;
    .end local v8           #recyle:Z
    :cond_6e
    const/4 v3, 0x0

    #@6f
    goto :goto_23

    #@70
    .line 1274
    .restart local v2       #child:Landroid/view/View;
    .restart local v3       #dividerHeight:I
    .restart local v4       #i:I
    .restart local v5       #isScrap:[Z
    .restart local v6       #prevHeightWithoutPartialChild:I
    .restart local v7       #recycleBin:Landroid/widget/AbsListView$RecycleBin;
    .restart local v8       #recyle:Z
    :cond_70
    if-ltz p5, :cond_77

    #@72
    move/from16 v0, p5

    #@74
    if-lt v4, v0, :cond_77

    #@76
    .line 1275
    move v6, v9

    #@77
    .line 1245
    :cond_77
    add-int/lit8 v4, v4, 0x1

    #@79
    goto :goto_36

    #@7a
    .end local v2           #child:Landroid/view/View;
    :cond_7a
    move v6, v9

    #@7b
    .line 1281
    goto :goto_e
.end method

.method protected onFinishInflate()V
    .registers 4

    #@0
    .prologue
    .line 3482
    invoke-super {p0}, Landroid/widget/AbsListView;->onFinishInflate()V

    #@3
    .line 3484
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@6
    move-result v0

    #@7
    .line 3485
    .local v0, count:I
    if-lez v0, :cond_19

    #@9
    .line 3486
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_16

    #@c
    .line 3487
    invoke-virtual {p0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    #@13
    .line 3486
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_a

    #@16
    .line 3489
    :cond_16
    invoke-virtual {p0}, Landroid/widget/ListView;->removeAllViews()V

    #@19
    .line 3491
    .end local v1           #i:I
    :cond_19
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 19
    .parameter "gainFocus"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 3425
    invoke-super/range {p0 .. p3}, Landroid/widget/AbsListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 3427
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@5
    .line 3428
    .local v2, adapter:Landroid/widget/ListAdapter;
    const/4 v5, -0x1

    #@6
    .line 3429
    .local v5, closetChildIndex:I
    const/4 v4, 0x0

    #@7
    .line 3430
    .local v4, closestChildTop:I
    if-eqz v2, :cond_5d

    #@9
    if-eqz p1, :cond_5d

    #@b
    if-eqz p3, :cond_5d

    #@d
    .line 3431
    iget v12, p0, Landroid/view/View;->mScrollX:I

    #@f
    iget v13, p0, Landroid/view/View;->mScrollY:I

    #@11
    move-object/from16 v0, p3

    #@13
    invoke-virtual {v0, v12, v13}, Landroid/graphics/Rect;->offset(II)V

    #@16
    .line 3435
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    #@19
    move-result v12

    #@1a
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@1d
    move-result v13

    #@1e
    iget v14, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@20
    add-int/2addr v13, v14

    #@21
    if-ge v12, v13, :cond_29

    #@23
    .line 3436
    const/4 v12, 0x0

    #@24
    iput v12, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@26
    .line 3437
    invoke-virtual {p0}, Landroid/widget/ListView;->layoutChildren()V

    #@29
    .line 3442
    :cond_29
    iget-object v11, p0, Landroid/widget/ListView;->mTempRect:Landroid/graphics/Rect;

    #@2b
    .line 3443
    .local v11, otherRect:Landroid/graphics/Rect;
    const v9, 0x7fffffff

    #@2e
    .line 3444
    .local v9, minDistance:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@31
    move-result v3

    #@32
    .line 3445
    .local v3, childCount:I
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@34
    .line 3447
    .local v7, firstPosition:I
    const/4 v8, 0x0

    #@35
    .local v8, i:I
    :goto_35
    if-ge v8, v3, :cond_5d

    #@37
    .line 3449
    add-int v12, v7, v8

    #@39
    invoke-interface {v2, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@3c
    move-result v12

    #@3d
    if-nez v12, :cond_42

    #@3f
    .line 3447
    :cond_3f
    :goto_3f
    add-int/lit8 v8, v8, 0x1

    #@41
    goto :goto_35

    #@42
    .line 3453
    :cond_42
    invoke-virtual {p0, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@45
    move-result-object v10

    #@46
    .line 3454
    .local v10, other:Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@49
    .line 3455
    invoke-virtual {p0, v10, v11}, Landroid/widget/ListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@4c
    .line 3456
    move-object/from16 v0, p3

    #@4e
    move/from16 v1, p2

    #@50
    invoke-static {v0, v11, v1}, Landroid/widget/ListView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    #@53
    move-result v6

    #@54
    .line 3458
    .local v6, distance:I
    if-ge v6, v9, :cond_3f

    #@56
    .line 3459
    move v9, v6

    #@57
    .line 3460
    move v5, v8

    #@58
    .line 3461
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@5b
    move-result v4

    #@5c
    goto :goto_3f

    #@5d
    .line 3466
    .end local v3           #childCount:I
    .end local v6           #distance:I
    .end local v7           #firstPosition:I
    .end local v8           #i:I
    .end local v9           #minDistance:I
    .end local v10           #other:Landroid/view/View;
    .end local v11           #otherRect:Landroid/graphics/Rect;
    :cond_5d
    if-ltz v5, :cond_66

    #@5f
    .line 3467
    iget v12, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@61
    add-int/2addr v12, v5

    #@62
    invoke-virtual {p0, v12, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@65
    .line 3471
    :goto_65
    return-void

    #@66
    .line 3469
    :cond_66
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    #@69
    goto :goto_65
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 3680
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 3681
    const-class v0, Landroid/widget/ListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 3682
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 3686
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 3687
    const-class v0, Landroid/widget/ListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 3688
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2103
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ListView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 2108
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2113
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ListView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1116
    invoke-super {p0, p1, p2}, Landroid/widget/AbsListView;->onMeasure(II)V

    #@5
    .line 1118
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@8
    move-result v11

    #@9
    .line 1119
    .local v11, widthMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@c
    move-result v10

    #@d
    .line 1120
    .local v10, heightMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@10
    move-result v12

    #@11
    .line 1121
    .local v12, widthSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@14
    move-result v4

    #@15
    .line 1123
    .local v4, heightSize:I
    const/4 v9, 0x0

    #@16
    .line 1124
    .local v9, childWidth:I
    const/4 v7, 0x0

    #@17
    .line 1125
    .local v7, childHeight:I
    const/4 v8, 0x0

    #@18
    .line 1127
    .local v8, childState:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1a
    if-nez v0, :cond_92

    #@1c
    move v0, v2

    #@1d
    :goto_1d
    iput v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1f
    .line 1128
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@21
    if-lez v0, :cond_5b

    #@23
    if-eqz v11, :cond_27

    #@25
    if-nez v10, :cond_5b

    #@27
    .line 1130
    :cond_27
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@29
    invoke-virtual {p0, v2, v0}, Landroid/widget/ListView;->obtainView(I[Z)Landroid/view/View;

    #@2c
    move-result-object v6

    #@2d
    .line 1132
    .local v6, child:Landroid/view/View;
    invoke-direct {p0, v6, v2, p1}, Landroid/widget/ListView;->measureScrapChild(Landroid/view/View;II)V

    #@30
    .line 1134
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    #@33
    move-result v9

    #@34
    .line 1135
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    #@37
    move-result v7

    #@38
    .line 1136
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredState()I

    #@3b
    move-result v0

    #@3c
    invoke-static {v8, v0}, Landroid/widget/ListView;->combineMeasuredStates(II)I

    #@3f
    move-result v8

    #@40
    .line 1138
    invoke-virtual {p0}, Landroid/widget/ListView;->recycleOnMeasure()Z

    #@43
    move-result v0

    #@44
    if-eqz v0, :cond_5b

    #@46
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@48
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, Landroid/widget/AbsListView$LayoutParams;

    #@4e
    iget v0, v0, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@50
    invoke-virtual {v1, v0}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@53
    move-result v0

    #@54
    if-eqz v0, :cond_5b

    #@56
    .line 1140
    iget-object v0, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@58
    invoke-virtual {v0, v6, v3}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@5b
    .line 1144
    .end local v6           #child:Landroid/view/View;
    :cond_5b
    if-nez v11, :cond_99

    #@5d
    .line 1145
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@5f
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@61
    iget-object v1, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@63
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@65
    add-int/2addr v0, v1

    #@66
    add-int/2addr v0, v9

    #@67
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalScrollbarWidth()I

    #@6a
    move-result v1

    #@6b
    add-int v12, v0, v1

    #@6d
    .line 1151
    :goto_6d
    if-nez v10, :cond_81

    #@6f
    .line 1152
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@71
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@73
    iget-object v1, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@75
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@77
    add-int/2addr v0, v1

    #@78
    add-int/2addr v0, v7

    #@79
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@7c
    move-result v1

    #@7d
    mul-int/lit8 v1, v1, 0x2

    #@7f
    add-int v4, v0, v1

    #@81
    .line 1156
    :cond_81
    const/high16 v0, -0x8000

    #@83
    if-ne v10, v0, :cond_8c

    #@85
    move-object v0, p0

    #@86
    move v1, p1

    #@87
    move v5, v3

    #@88
    .line 1158
    invoke-virtual/range {v0 .. v5}, Landroid/widget/ListView;->measureHeightOfChildren(IIIII)I

    #@8b
    move-result v4

    #@8c
    .line 1161
    :cond_8c
    invoke-virtual {p0, v12, v4}, Landroid/widget/ListView;->setMeasuredDimension(II)V

    #@8f
    .line 1162
    iput p1, p0, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@91
    .line 1163
    return-void

    #@92
    .line 1127
    :cond_92
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@94
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@97
    move-result v0

    #@98
    goto :goto_1d

    #@99
    .line 1148
    :cond_99
    const/high16 v0, -0x100

    #@9b
    and-int/2addr v0, v8

    #@9c
    or-int/2addr v12, v0

    #@9d
    goto :goto_6d
.end method

.method protected onSizeChanged(IIII)V
    .registers 12
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 1097
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3
    move-result v5

    #@4
    if-lez v5, :cond_3e

    #@6
    .line 1098
    invoke-virtual {p0}, Landroid/widget/ListView;->getFocusedChild()Landroid/view/View;

    #@9
    move-result-object v2

    #@a
    .line 1099
    .local v2, focusedChild:Landroid/view/View;
    if-eqz v2, :cond_3e

    #@c
    .line 1100
    iget v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@e
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->indexOfChild(Landroid/view/View;)I

    #@11
    move-result v6

    #@12
    add-int v1, v5, v6

    #@14
    .line 1101
    .local v1, childPosition:I
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    #@17
    move-result v0

    #@18
    .line 1102
    .local v0, childBottom:I
    const/4 v5, 0x0

    #@19
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@1b
    sub-int v6, p2, v6

    #@1d
    sub-int v6, v0, v6

    #@1f
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@22
    move-result v3

    #@23
    .line 1103
    .local v3, offset:I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@26
    move-result v5

    #@27
    sub-int v4, v5, v3

    #@29
    .line 1104
    .local v4, top:I
    iget-object v5, p0, Landroid/widget/ListView;->mFocusSelector:Landroid/widget/ListView$FocusSelector;

    #@2b
    if-nez v5, :cond_35

    #@2d
    .line 1105
    new-instance v5, Landroid/widget/ListView$FocusSelector;

    #@2f
    const/4 v6, 0x0

    #@30
    invoke-direct {v5, p0, v6}, Landroid/widget/ListView$FocusSelector;-><init>(Landroid/widget/ListView;Landroid/widget/ListView$1;)V

    #@33
    iput-object v5, p0, Landroid/widget/ListView;->mFocusSelector:Landroid/widget/ListView$FocusSelector;

    #@35
    .line 1107
    :cond_35
    iget-object v5, p0, Landroid/widget/ListView;->mFocusSelector:Landroid/widget/ListView$FocusSelector;

    #@37
    invoke-virtual {v5, v1, v4}, Landroid/widget/ListView$FocusSelector;->setup(II)Landroid/widget/ListView$FocusSelector;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    #@3e
    .line 1110
    .end local v0           #childBottom:I
    .end local v1           #childPosition:I
    .end local v2           #focusedChild:Landroid/view/View;
    .end local v3           #offset:I
    .end local v4           #top:I
    :cond_3e
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AbsListView;->onSizeChanged(IIII)V

    #@41
    .line 1111
    return-void
.end method

.method pageScroll(I)Z
    .registers 10
    .parameter "direction"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2275
    const/4 v1, -0x1

    #@3
    .line 2276
    .local v1, nextPage:I
    const/4 v0, 0x0

    #@4
    .line 2278
    .local v0, down:Z
    const/16 v5, 0x21

    #@6
    if-ne p1, v5, :cond_51

    #@8
    .line 2279
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@a
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@d
    move-result v6

    #@e
    sub-int/2addr v5, v6

    #@f
    add-int/lit8 v5, v5, -0x1

    #@11
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@14
    move-result v1

    #@15
    .line 2285
    :cond_15
    :goto_15
    if-ltz v1, :cond_68

    #@17
    .line 2286
    invoke-virtual {p0, v1, v0}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@1a
    move-result v2

    #@1b
    .line 2287
    .local v2, position:I
    if-ltz v2, :cond_68

    #@1d
    .line 2288
    const/4 v4, 0x4

    #@1e
    iput v4, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@20
    .line 2289
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@22
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@25
    move-result v5

    #@26
    add-int/2addr v4, v5

    #@27
    iput v4, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@29
    .line 2291
    if-eqz v0, :cond_37

    #@2b
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2d
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@30
    move-result v5

    #@31
    sub-int/2addr v4, v5

    #@32
    if-le v2, v4, :cond_37

    #@34
    .line 2292
    const/4 v4, 0x3

    #@35
    iput v4, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@37
    .line 2295
    :cond_37
    if-nez v0, :cond_41

    #@39
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3c
    move-result v4

    #@3d
    if-ge v2, v4, :cond_41

    #@3f
    .line 2296
    iput v3, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@41
    .line 2299
    :cond_41
    invoke-virtual {p0, v2}, Landroid/widget/ListView;->setSelectionInt(I)V

    #@44
    .line 2300
    invoke-virtual {p0}, Landroid/widget/ListView;->invokeOnItemScrollListener()V

    #@47
    .line 2301
    invoke-virtual {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    #@4a
    move-result v4

    #@4b
    if-nez v4, :cond_50

    #@4d
    .line 2302
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@50
    .line 2309
    .end local v2           #position:I
    :cond_50
    :goto_50
    return v3

    #@51
    .line 2280
    :cond_51
    const/16 v5, 0x82

    #@53
    if-ne p1, v5, :cond_15

    #@55
    .line 2281
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@57
    add-int/lit8 v5, v5, -0x1

    #@59
    iget v6, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@5b
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@5e
    move-result v7

    #@5f
    add-int/2addr v6, v7

    #@60
    add-int/lit8 v6, v6, -0x1

    #@62
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@65
    move-result v1

    #@66
    .line 2282
    const/4 v0, 0x1

    #@67
    goto :goto_15

    #@68
    :cond_68
    move v3, v4

    #@69
    .line 2309
    goto :goto_50
.end method

.method protected recycleOnMeasure()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation

    #@0
    .prologue
    .line 1193
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public removeFooterView(Landroid/view/View;)Z
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 387
    iget-object v1, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    if-lez v1, :cond_27

    #@8
    .line 388
    const/4 v0, 0x0

    #@9
    .line 389
    .local v0, result:Z
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@b
    if-eqz v1, :cond_21

    #@d
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@f
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    #@11
    invoke-virtual {v1, p1}, Landroid/widget/HeaderViewListAdapter;->removeFooter(Landroid/view/View;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_21

    #@17
    .line 390
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@19
    if-eqz v1, :cond_20

    #@1b
    .line 391
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@1d
    invoke-virtual {v1}, Landroid/widget/AbsListView$AdapterDataSetObserver;->onChanged()V

    #@20
    .line 393
    :cond_20
    const/4 v0, 0x1

    #@21
    .line 395
    :cond_21
    iget-object v1, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@23
    invoke-direct {p0, p1, v1}, Landroid/widget/ListView;->removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V

    #@26
    .line 398
    .end local v0           #result:Z
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_26
.end method

.method public removeHeaderView(Landroid/view/View;)Z
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 302
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    if-lez v1, :cond_27

    #@8
    .line 303
    const/4 v0, 0x0

    #@9
    .line 304
    .local v0, result:Z
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@b
    if-eqz v1, :cond_21

    #@d
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@f
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    #@11
    invoke-virtual {v1, p1}, Landroid/widget/HeaderViewListAdapter;->removeHeader(Landroid/view/View;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_21

    #@17
    .line 305
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@19
    if-eqz v1, :cond_20

    #@1b
    .line 306
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@1d
    invoke-virtual {v1}, Landroid/widget/AbsListView$AdapterDataSetObserver;->onChanged()V

    #@20
    .line 308
    :cond_20
    const/4 v0, 0x1

    #@21
    .line 310
    :cond_21
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@23
    invoke-direct {p0, p1, v1}, Landroid/widget/ListView;->removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V

    #@26
    .line 313
    .end local v0           #result:Z
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_26
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 19
    .parameter "child"
    .parameter "rect"
    .parameter "immediate"

    #@0
    .prologue
    .line 549
    move-object/from16 v0, p2

    #@2
    iget v9, v0, Landroid/graphics/Rect;->top:I

    #@4
    .line 552
    .local v9, rectTopWithinChild:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    #@7
    move-result v13

    #@8
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@b
    move-result v14

    #@c
    move-object/from16 v0, p2

    #@e
    invoke-virtual {v0, v13, v14}, Landroid/graphics/Rect;->offset(II)V

    #@11
    .line 553
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollX()I

    #@14
    move-result v13

    #@15
    neg-int v13, v13

    #@16
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollY()I

    #@19
    move-result v14

    #@1a
    neg-int v14, v14

    #@1b
    move-object/from16 v0, p2

    #@1d
    invoke-virtual {v0, v13, v14}, Landroid/graphics/Rect;->offset(II)V

    #@20
    .line 555
    invoke-virtual {p0}, Landroid/widget/ListView;->getHeight()I

    #@23
    move-result v6

    #@24
    .line 556
    .local v6, height:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getScrollY()I

    #@27
    move-result v8

    #@28
    .line 557
    .local v8, listUnfadedTop:I
    add-int v7, v8, v6

    #@2a
    .line 558
    .local v7, listUnfadedBottom:I
    invoke-virtual {p0}, Landroid/widget/ListView;->getVerticalFadingEdgeLength()I

    #@2d
    move-result v5

    #@2e
    .line 560
    .local v5, fadingEdge:I
    invoke-direct {p0}, Landroid/widget/ListView;->showingTopFadingEdge()Z

    #@31
    move-result v13

    #@32
    if-eqz v13, :cond_3b

    #@34
    .line 562
    iget v13, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@36
    if-gtz v13, :cond_3a

    #@38
    if-le v9, v5, :cond_3b

    #@3a
    .line 563
    :cond_3a
    add-int/2addr v8, v5

    #@3b
    .line 567
    :cond_3b
    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    #@3e
    move-result v2

    #@3f
    .line 568
    .local v2, childCount:I
    add-int/lit8 v13, v2, -0x1

    #@41
    invoke-virtual {p0, v13}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@44
    move-result-object v13

    #@45
    invoke-virtual {v13}, Landroid/view/View;->getBottom()I

    #@48
    move-result v1

    #@49
    .line 570
    .local v1, bottomOfBottomChild:I
    invoke-direct {p0}, Landroid/widget/ListView;->showingBottomFadingEdge()Z

    #@4c
    move-result v13

    #@4d
    if-eqz v13, :cond_60

    #@4f
    .line 572
    iget v13, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@51
    iget v14, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@53
    add-int/lit8 v14, v14, -0x1

    #@55
    if-lt v13, v14, :cond_5f

    #@57
    move-object/from16 v0, p2

    #@59
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@5b
    sub-int v14, v1, v5

    #@5d
    if-ge v13, v14, :cond_60

    #@5f
    .line 574
    :cond_5f
    sub-int/2addr v7, v5

    #@60
    .line 578
    :cond_60
    const/4 v11, 0x0

    #@61
    .line 580
    .local v11, scrollYDelta:I
    move-object/from16 v0, p2

    #@63
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@65
    if-le v13, v7, :cond_9f

    #@67
    move-object/from16 v0, p2

    #@69
    iget v13, v0, Landroid/graphics/Rect;->top:I

    #@6b
    if-le v13, v8, :cond_9f

    #@6d
    .line 585
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    #@70
    move-result v13

    #@71
    if-le v13, v6, :cond_98

    #@73
    .line 587
    move-object/from16 v0, p2

    #@75
    iget v13, v0, Landroid/graphics/Rect;->top:I

    #@77
    sub-int/2addr v13, v8

    #@78
    add-int/2addr v11, v13

    #@79
    .line 594
    :goto_79
    sub-int v4, v1, v7

    #@7b
    .line 595
    .local v4, distanceToBottom:I
    invoke-static {v11, v4}, Ljava/lang/Math;->min(II)I

    #@7e
    move-result v11

    #@7f
    .line 615
    .end local v4           #distanceToBottom:I
    :cond_7f
    :goto_7f
    if-eqz v11, :cond_d0

    #@81
    const/4 v10, 0x1

    #@82
    .line 616
    .local v10, scroll:Z
    :goto_82
    if-eqz v10, :cond_97

    #@84
    .line 617
    neg-int v13, v11

    #@85
    invoke-direct {p0, v13}, Landroid/widget/ListView;->scrollListItemsBy(I)V

    #@88
    .line 618
    const/4 v13, -0x1

    #@89
    move-object/from16 v0, p1

    #@8b
    invoke-virtual {p0, v13, v0}, Landroid/widget/ListView;->positionSelector(ILandroid/view/View;)V

    #@8e
    .line 619
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@91
    move-result v13

    #@92
    iput v13, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@94
    .line 620
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@97
    .line 622
    :cond_97
    return v10

    #@98
    .line 590
    .end local v10           #scroll:Z
    :cond_98
    move-object/from16 v0, p2

    #@9a
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@9c
    sub-int/2addr v13, v7

    #@9d
    add-int/2addr v11, v13

    #@9e
    goto :goto_79

    #@9f
    .line 596
    :cond_9f
    move-object/from16 v0, p2

    #@a1
    iget v13, v0, Landroid/graphics/Rect;->top:I

    #@a3
    if-ge v13, v8, :cond_7f

    #@a5
    move-object/from16 v0, p2

    #@a7
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@a9
    if-ge v13, v7, :cond_7f

    #@ab
    .line 601
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    #@ae
    move-result v13

    #@af
    if-le v13, v6, :cond_c8

    #@b1
    .line 603
    move-object/from16 v0, p2

    #@b3
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@b5
    sub-int v13, v7, v13

    #@b7
    sub-int/2addr v11, v13

    #@b8
    .line 610
    :goto_b8
    const/4 v13, 0x0

    #@b9
    invoke-virtual {p0, v13}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@bc
    move-result-object v13

    #@bd
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    #@c0
    move-result v12

    #@c1
    .line 611
    .local v12, top:I
    sub-int v3, v12, v8

    #@c3
    .line 612
    .local v3, deltaToTop:I
    invoke-static {v11, v3}, Ljava/lang/Math;->max(II)I

    #@c6
    move-result v11

    #@c7
    goto :goto_7f

    #@c8
    .line 606
    .end local v3           #deltaToTop:I
    .end local v12           #top:I
    :cond_c8
    move-object/from16 v0, p2

    #@ca
    iget v13, v0, Landroid/graphics/Rect;->top:I

    #@cc
    sub-int v13, v8, v13

    #@ce
    sub-int/2addr v11, v13

    #@cf
    goto :goto_b8

    #@d0
    .line 615
    :cond_d0
    const/4 v10, 0x0

    #@d1
    goto :goto_82
.end method

.method resetList()V
    .registers 2

    #@0
    .prologue
    .line 501
    iget-object v0, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0}, Landroid/widget/ListView;->clearRecycledState(Ljava/util/ArrayList;)V

    #@5
    .line 502
    iget-object v0, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@7
    invoke-direct {p0, v0}, Landroid/widget/ListView;->clearRecycledState(Ljava/util/ArrayList;)V

    #@a
    .line 504
    invoke-super {p0}, Landroid/widget/AbsListView;->resetList()V

    #@d
    .line 506
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@10
    .line 507
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    check-cast p1, Landroid/widget/ListAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 8
    .parameter "adapter"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 440
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@4
    if-eqz v1, :cond_11

    #@6
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 441
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@c
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@e
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@11
    .line 444
    :cond_11
    invoke-virtual {p0}, Landroid/widget/ListView;->resetList()V

    #@14
    .line 445
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@16
    invoke-virtual {v1}, Landroid/widget/AbsListView$RecycleBin;->clear()V

    #@19
    .line 447
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v1

    #@1f
    if-gtz v1, :cond_29

    #@21
    iget-object v1, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v1

    #@27
    if-lez v1, :cond_8f

    #@29
    .line 448
    :cond_29
    new-instance v1, Landroid/widget/HeaderViewListAdapter;

    #@2b
    iget-object v2, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2d
    iget-object v3, p0, Landroid/widget/ListView;->mFooterViewInfos:Ljava/util/ArrayList;

    #@2f
    invoke-direct {v1, v2, v3, p1}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    #@32
    iput-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@34
    .line 453
    :goto_34
    const/4 v1, -0x1

    #@35
    iput v1, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@37
    .line 454
    const-wide/high16 v1, -0x8000

    #@39
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@3b
    .line 457
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@3e
    .line 459
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@40
    if-eqz v1, :cond_97

    #@42
    .line 460
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@44
    invoke-interface {v1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    #@47
    move-result v1

    #@48
    iput-boolean v1, p0, Landroid/widget/ListView;->mAreAllItemsSelectable:Z

    #@4a
    .line 461
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@4c
    iput v1, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@4e
    .line 462
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@50
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    #@53
    move-result v1

    #@54
    iput v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@56
    .line 463
    invoke-virtual {p0}, Landroid/widget/ListView;->checkFocus()V

    #@59
    .line 465
    new-instance v1, Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@5b
    invoke-direct {v1, p0}, Landroid/widget/AbsListView$AdapterDataSetObserver;-><init>(Landroid/widget/AbsListView;)V

    #@5e
    iput-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@60
    .line 466
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@62
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@64
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@67
    .line 468
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@69
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@6b
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    #@6e
    move-result v2

    #@6f
    invoke-virtual {v1, v2}, Landroid/widget/AbsListView$RecycleBin;->setViewTypeCount(I)V

    #@72
    .line 471
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@74
    if-eqz v1, :cond_92

    #@76
    .line 472
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@78
    add-int/lit8 v1, v1, -0x1

    #@7a
    invoke-virtual {p0, v1, v4}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@7d
    move-result v0

    #@7e
    .line 476
    .local v0, position:I
    :goto_7e
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setSelectedPositionInt(I)V

    #@81
    .line 477
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setNextSelectedPositionInt(I)V

    #@84
    .line 479
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@86
    if-nez v1, :cond_8b

    #@88
    .line 481
    invoke-virtual {p0}, Landroid/widget/ListView;->checkSelectionChanged()V

    #@8b
    .line 490
    .end local v0           #position:I
    :cond_8b
    :goto_8b
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    #@8e
    .line 491
    return-void

    #@8f
    .line 450
    :cond_8f
    iput-object p1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@91
    goto :goto_34

    #@92
    .line 474
    :cond_92
    invoke-virtual {p0, v4, v5}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@95
    move-result v0

    #@96
    .restart local v0       #position:I
    goto :goto_7e

    #@97
    .line 484
    .end local v0           #position:I
    :cond_97
    iput-boolean v5, p0, Landroid/widget/ListView;->mAreAllItemsSelectable:Z

    #@99
    .line 485
    invoke-virtual {p0}, Landroid/widget/ListView;->checkFocus()V

    #@9c
    .line 487
    invoke-virtual {p0}, Landroid/widget/ListView;->checkSelectionChanged()V

    #@9f
    goto :goto_8b
.end method

.method public setCacheColorHint(I)V
    .registers 5
    .parameter "color"

    #@0
    .prologue
    .line 3087
    ushr-int/lit8 v1, p1, 0x18

    #@2
    const/16 v2, 0xff

    #@4
    if-ne v1, v2, :cond_1f

    #@6
    const/4 v0, 0x1

    #@7
    .line 3088
    .local v0, opaque:Z
    :goto_7
    iput-boolean v0, p0, Landroid/widget/ListView;->mIsCacheColorOpaque:Z

    #@9
    .line 3089
    if-eqz v0, :cond_1b

    #@b
    .line 3090
    iget-object v1, p0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@d
    if-nez v1, :cond_16

    #@f
    .line 3091
    new-instance v1, Landroid/graphics/Paint;

    #@11
    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    #@14
    iput-object v1, p0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@16
    .line 3093
    :cond_16
    iget-object v1, p0, Landroid/widget/ListView;->mDividerPaint:Landroid/graphics/Paint;

    #@18
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColor(I)V

    #@1b
    .line 3095
    :cond_1b
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setCacheColorHint(I)V

    #@1e
    .line 3096
    return-void

    #@1f
    .line 3087
    .end local v0           #opaque:Z
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_7
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "divider"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3328
    if-eqz p1, :cond_1e

    #@3
    .line 3329
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@6
    move-result v1

    #@7
    iput v1, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@9
    .line 3333
    :goto_9
    iput-object p1, p0, Landroid/widget/ListView;->mDivider:Landroid/graphics/drawable/Drawable;

    #@b
    .line 3334
    if-eqz p1, :cond_14

    #@d
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@10
    move-result v1

    #@11
    const/4 v2, -0x1

    #@12
    if-ne v1, v2, :cond_15

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :cond_15
    iput-boolean v0, p0, Landroid/widget/ListView;->mDividerIsOpaque:Z

    #@17
    .line 3335
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    #@1a
    .line 3336
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@1d
    .line 3337
    return-void

    #@1e
    .line 3331
    :cond_1e
    iput v0, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@20
    goto :goto_9
.end method

.method public setDividerHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 3353
    iput p1, p0, Landroid/widget/ListView;->mDividerHeight:I

    #@2
    .line 3354
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    #@5
    .line 3355
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@8
    .line 3356
    return-void
.end method

.method public setFooterDividersEnabled(Z)V
    .registers 2
    .parameter "footerDividersEnabled"

    #@0
    .prologue
    .line 3380
    iput-boolean p1, p0, Landroid/widget/ListView;->mFooterDividersEnabled:Z

    #@2
    .line 3381
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@5
    .line 3382
    return-void
.end method

.method public setHeaderDividersEnabled(Z)V
    .registers 2
    .parameter "headerDividersEnabled"

    #@0
    .prologue
    .line 3367
    iput-boolean p1, p0, Landroid/widget/ListView;->mHeaderDividersEnabled:Z

    #@2
    .line 3368
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@5
    .line 3369
    return-void
.end method

.method public setItemsCanFocus(Z)V
    .registers 3
    .parameter "itemsCanFocus"

    #@0
    .prologue
    .line 3050
    iput-boolean p1, p0, Landroid/widget/ListView;->mItemsCanFocus:Z

    #@2
    .line 3051
    if-nez p1, :cond_9

    #@4
    .line 3052
    const/high16 v0, 0x6

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setDescendantFocusability(I)V

    #@9
    .line 3054
    :cond_9
    return-void
.end method

.method public setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "footer"

    #@0
    .prologue
    .line 3412
    iput-object p1, p0, Landroid/widget/ListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    #@2
    .line 3413
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@5
    .line 3414
    return-void
.end method

.method public setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "header"

    #@0
    .prologue
    .line 3391
    iput-object p1, p0, Landroid/widget/ListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    #@2
    .line 3392
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@4
    if-gez v0, :cond_9

    #@6
    .line 3393
    invoke-virtual {p0}, Landroid/widget/ListView;->invalidate()V

    #@9
    .line 3395
    :cond_9
    return-void
.end method

.method public setRemoteViewsAdapter(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 422
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setRemoteViewsAdapter(Landroid/content/Intent;)V

    #@3
    .line 423
    return-void
.end method

.method public setSelection(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1951
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@4
    .line 1952
    return-void
.end method

.method public setSelectionAfterHeaderView()V
    .registers 3

    #@0
    .prologue
    .line 2070
    iget-object v1, p0, Landroid/widget/ListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 2071
    .local v0, count:I
    if-lez v0, :cond_c

    #@8
    .line 2072
    const/4 v1, 0x0

    #@9
    iput v1, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@b
    .line 2083
    :goto_b
    return-void

    #@c
    .line 2076
    :cond_c
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@e
    if-eqz v1, :cond_14

    #@10
    .line 2077
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setSelection(I)V

    #@13
    goto :goto_b

    #@14
    .line 2079
    :cond_14
    iput v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@16
    .line 2080
    const/4 v1, 0x2

    #@17
    iput v1, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@19
    goto :goto_b
.end method

.method public setSelectionFromTop(II)V
    .registers 5
    .parameter "position"
    .parameter "y"

    #@0
    .prologue
    .line 1964
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1991
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1968
    :cond_5
    invoke-virtual {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_3c

    #@b
    .line 1969
    const/4 v0, 0x1

    #@c
    invoke-virtual {p0, p1, v0}, Landroid/widget/ListView;->lookForSelectablePosition(IZ)I

    #@f
    move-result p1

    #@10
    .line 1970
    if-ltz p1, :cond_15

    #@12
    .line 1971
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setNextSelectedPositionInt(I)V

    #@15
    .line 1977
    :cond_15
    :goto_15
    if-ltz p1, :cond_4

    #@17
    .line 1978
    const/4 v0, 0x4

    #@18
    iput v0, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@1a
    .line 1979
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1c
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@1e
    add-int/2addr v0, p2

    #@1f
    iput v0, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@21
    .line 1981
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@23
    if-eqz v0, :cond_2f

    #@25
    .line 1982
    iput p1, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@27
    .line 1983
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@29
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@2c
    move-result-wide v0

    #@2d
    iput-wide v0, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@2f
    .line 1986
    :cond_2f
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@31
    if-eqz v0, :cond_38

    #@33
    .line 1987
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@35
    invoke-virtual {v0}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@38
    .line 1989
    :cond_38
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    #@3b
    goto :goto_4

    #@3c
    .line 1974
    :cond_3c
    iput p1, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@3e
    goto :goto_15
.end method

.method setSelectionInt(I)V
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 2000
    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setNextSelectedPositionInt(I)V

    #@3
    .line 2001
    const/4 v0, 0x0

    #@4
    .line 2003
    .local v0, awakeScrollbars:Z
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    .line 2005
    .local v1, selectedPosition:I
    if-ltz v1, :cond_d

    #@8
    .line 2006
    add-int/lit8 v2, v1, -0x1

    #@a
    if-ne p1, v2, :cond_1f

    #@c
    .line 2007
    const/4 v0, 0x1

    #@d
    .line 2013
    :cond_d
    :goto_d
    iget-object v2, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@f
    if-eqz v2, :cond_16

    #@11
    .line 2014
    iget-object v2, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@13
    invoke-virtual {v2}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@16
    .line 2017
    :cond_16
    invoke-virtual {p0}, Landroid/widget/ListView;->layoutChildren()V

    #@19
    .line 2019
    if-eqz v0, :cond_1e

    #@1b
    .line 2020
    invoke-virtual {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    #@1e
    .line 2022
    :cond_1e
    return-void

    #@1f
    .line 2008
    :cond_1f
    add-int/lit8 v2, v1, 0x1

    #@21
    if-ne p1, v2, :cond_d

    #@23
    .line 2009
    const/4 v0, 0x1

    #@24
    goto :goto_d
.end method

.method public smoothScrollByOffset(I)V
    .registers 2
    .parameter "offset"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 909
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->smoothScrollByOffset(I)V

    #@3
    .line 910
    return-void
.end method

.method public smoothScrollToPosition(I)V
    .registers 2
    .parameter "position"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 899
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->smoothScrollToPosition(I)V

    #@3
    .line 900
    return-void
.end method
