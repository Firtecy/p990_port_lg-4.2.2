.class Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;
.super Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;
.source "RemoteViewsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RemoteViewsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RemoteViewsAdapterServiceConnection"
.end annotation


# instance fields
.field private mAdapter:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/RemoteViewsAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mIsConnected:Z

.field private mIsConnecting:Z

.field private mRemoteViewsFactory:Lcom/android/internal/widget/IRemoteViewsFactory;


# direct methods
.method public constructor <init>(Landroid/widget/RemoteViewsAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    .line 149
    invoke-direct {p0}, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;-><init>()V

    #@3
    .line 150
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mAdapter:Ljava/lang/ref/WeakReference;

    #@a
    .line 151
    return-void
.end method

.method static synthetic access$802(Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 142
    iput-boolean p1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$902(Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 142
    iput-boolean p1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z

    #@2
    return p1
.end method


# virtual methods
.method public declared-synchronized bind(Landroid/content/Context;ILandroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "intent"

    #@0
    .prologue
    .line 154
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5e

    #@3
    if-nez v3, :cond_2c

    #@5
    .line 157
    :try_start_5
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@8
    move-result-object v2

    #@9
    .line 158
    .local v2, mgr:Landroid/appwidget/AppWidgetManager;
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@c
    move-result v3

    #@d
    const/16 v4, 0x3e8

    #@f
    if-ne v3, v4, :cond_2e

    #@11
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mAdapter:Ljava/lang/ref/WeakReference;

    #@13
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/RemoteViewsAdapter;

    #@19
    .local v0, adapter:Landroid/widget/RemoteViewsAdapter;
    if-eqz v0, :cond_2e

    #@1b
    .line 160
    invoke-virtual {p0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v3

    #@1f
    new-instance v4, Landroid/os/UserHandle;

    #@21
    iget v5, v0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@23
    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@26
    invoke-virtual {v2, p2, p3, v3, v4}, Landroid/appwidget/AppWidgetManager;->bindRemoteViewsService(ILandroid/content/Intent;Landroid/os/IBinder;Landroid/os/UserHandle;)V

    #@29
    .line 166
    .end local v0           #adapter:Landroid/widget/RemoteViewsAdapter;
    :goto_29
    const/4 v3, 0x1

    #@2a
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z
    :try_end_2c
    .catchall {:try_start_5 .. :try_end_2c} :catchall_5e
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_2c} :catch_3a

    #@2c
    .line 173
    .end local v2           #mgr:Landroid/appwidget/AppWidgetManager;
    :cond_2c
    :goto_2c
    monitor-exit p0

    #@2d
    return-void

    #@2e
    .line 163
    .restart local v2       #mgr:Landroid/appwidget/AppWidgetManager;
    :cond_2e
    :try_start_2e
    invoke-virtual {p0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->asBinder()Landroid/os/IBinder;

    #@31
    move-result-object v3

    #@32
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v2, p2, p3, v3, v4}, Landroid/appwidget/AppWidgetManager;->bindRemoteViewsService(ILandroid/content/Intent;Landroid/os/IBinder;Landroid/os/UserHandle;)V
    :try_end_39
    .catchall {:try_start_2e .. :try_end_39} :catchall_5e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_39} :catch_3a

    #@39
    goto :goto_29

    #@3a
    .line 167
    .end local v2           #mgr:Landroid/appwidget/AppWidgetManager;
    :catch_3a
    move-exception v1

    #@3b
    .line 168
    .local v1, e:Ljava/lang/Exception;
    :try_start_3b
    const-string v3, "RemoteViewsAdapterServiceConnection"

    #@3d
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "bind(): "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 169
    const/4 v3, 0x0

    #@58
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z

    #@5a
    .line 170
    const/4 v3, 0x0

    #@5b
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnected:Z
    :try_end_5d
    .catchall {:try_start_3b .. :try_end_5d} :catchall_5e

    #@5d
    goto :goto_2c

    #@5e
    .line 154
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_5e
    move-exception v3

    #@5f
    monitor-exit p0

    #@60
    throw v3
.end method

.method public declared-synchronized getRemoteViewsFactory()Lcom/android/internal/widget/IRemoteViewsFactory;
    .registers 2

    #@0
    .prologue
    .line 282
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mRemoteViewsFactory:Lcom/android/internal/widget/IRemoteViewsFactory;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 286
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnected:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized onServiceConnected(Landroid/os/IBinder;)V
    .registers 5
    .parameter "service"

    #@0
    .prologue
    .line 195
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/IRemoteViewsFactory;

    #@4
    move-result-object v1

    #@5
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mRemoteViewsFactory:Lcom/android/internal/widget/IRemoteViewsFactory;

    #@7
    .line 198
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mAdapter:Ljava/lang/ref/WeakReference;

    #@9
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/widget/RemoteViewsAdapter;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_20

    #@f
    .line 199
    .local v0, adapter:Landroid/widget/RemoteViewsAdapter;
    if-nez v0, :cond_13

    #@11
    .line 256
    :goto_11
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 202
    :cond_13
    :try_start_13
    #getter for: Landroid/widget/RemoteViewsAdapter;->mWorkerQueue:Landroid/os/Handler;
    invoke-static {v0}, Landroid/widget/RemoteViewsAdapter;->access$1000(Landroid/widget/RemoteViewsAdapter;)Landroid/os/Handler;

    #@16
    move-result-object v1

    #@17
    new-instance v2, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection$1;

    #@19
    invoke-direct {v2, p0, v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection$1;-><init>(Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;Landroid/widget/RemoteViewsAdapter;)V

    #@1c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1f
    .catchall {:try_start_13 .. :try_end_1f} :catchall_20

    #@1f
    goto :goto_11

    #@20
    .line 195
    .end local v0           #adapter:Landroid/widget/RemoteViewsAdapter;
    :catchall_20
    move-exception v1

    #@21
    monitor-exit p0

    #@22
    throw v1
.end method

.method public declared-synchronized onServiceDisconnected()V
    .registers 4

    #@0
    .prologue
    .line 259
    monitor-enter p0

    #@1
    const/4 v1, 0x0

    #@2
    :try_start_2
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnected:Z

    #@4
    .line 260
    const/4 v1, 0x0

    #@5
    iput-boolean v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z

    #@7
    .line 261
    const/4 v1, 0x0

    #@8
    iput-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mRemoteViewsFactory:Lcom/android/internal/widget/IRemoteViewsFactory;

    #@a
    .line 264
    iget-object v1, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mAdapter:Ljava/lang/ref/WeakReference;

    #@c
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/widget/RemoteViewsAdapter;
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_23

    #@12
    .line 265
    .local v0, adapter:Landroid/widget/RemoteViewsAdapter;
    if-nez v0, :cond_16

    #@14
    .line 279
    :goto_14
    monitor-exit p0

    #@15
    return-void

    #@16
    .line 267
    :cond_16
    :try_start_16
    #getter for: Landroid/widget/RemoteViewsAdapter;->mMainQueue:Landroid/os/Handler;
    invoke-static {v0}, Landroid/widget/RemoteViewsAdapter;->access$600(Landroid/widget/RemoteViewsAdapter;)Landroid/os/Handler;

    #@19
    move-result-object v1

    #@1a
    new-instance v2, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection$2;

    #@1c
    invoke-direct {v2, p0, v0}, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection$2;-><init>(Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;Landroid/widget/RemoteViewsAdapter;)V

    #@1f
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_22
    .catchall {:try_start_16 .. :try_end_22} :catchall_23

    #@22
    goto :goto_14

    #@23
    .line 259
    .end local v0           #adapter:Landroid/widget/RemoteViewsAdapter;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit p0

    #@25
    throw v1
.end method

.method public declared-synchronized unbind(Landroid/content/Context;ILandroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "intent"

    #@0
    .prologue
    .line 178
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@4
    move-result-object v2

    #@5
    .line 179
    .local v2, mgr:Landroid/appwidget/AppWidgetManager;
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@8
    move-result v3

    #@9
    const/16 v4, 0x3e8

    #@b
    if-ne v3, v4, :cond_26

    #@d
    iget-object v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mAdapter:Ljava/lang/ref/WeakReference;

    #@f
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/widget/RemoteViewsAdapter;

    #@15
    .local v0, adapter:Landroid/widget/RemoteViewsAdapter;
    if-eqz v0, :cond_26

    #@17
    .line 181
    new-instance v3, Landroid/os/UserHandle;

    #@19
    iget v4, v0, Landroid/widget/RemoteViewsAdapter;->mUserId:I

    #@1b
    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    #@1e
    invoke-virtual {v2, p2, p3, v3}, Landroid/appwidget/AppWidgetManager;->unbindRemoteViewsService(ILandroid/content/Intent;Landroid/os/UserHandle;)V

    #@21
    .line 186
    .end local v0           #adapter:Landroid/widget/RemoteViewsAdapter;
    :goto_21
    const/4 v3, 0x0

    #@22
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_53
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_24} :catch_2e

    #@24
    .line 192
    .end local v2           #mgr:Landroid/appwidget/AppWidgetManager;
    :goto_24
    monitor-exit p0

    #@25
    return-void

    #@26
    .line 184
    .restart local v2       #mgr:Landroid/appwidget/AppWidgetManager;
    :cond_26
    :try_start_26
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, p2, p3, v3}, Landroid/appwidget/AppWidgetManager;->unbindRemoteViewsService(ILandroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_2d
    .catchall {:try_start_26 .. :try_end_2d} :catchall_53
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_21

    #@2e
    .line 187
    .end local v2           #mgr:Landroid/appwidget/AppWidgetManager;
    :catch_2e
    move-exception v1

    #@2f
    .line 188
    .local v1, e:Ljava/lang/Exception;
    :try_start_2f
    const-string v3, "RemoteViewsAdapterServiceConnection"

    #@31
    new-instance v4, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string/jumbo v5, "unbind(): "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 189
    const/4 v3, 0x0

    #@4d
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnecting:Z

    #@4f
    .line 190
    const/4 v3, 0x0

    #@50
    iput-boolean v3, p0, Landroid/widget/RemoteViewsAdapter$RemoteViewsAdapterServiceConnection;->mIsConnected:Z
    :try_end_52
    .catchall {:try_start_2f .. :try_end_52} :catchall_53

    #@52
    goto :goto_24

    #@53
    .line 178
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_53
    move-exception v3

    #@54
    monitor-exit p0

    #@55
    throw v3
.end method
