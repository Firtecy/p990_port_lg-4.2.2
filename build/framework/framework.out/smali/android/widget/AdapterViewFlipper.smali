.class public Landroid/widget/AdapterViewFlipper;
.super Landroid/widget/AdapterViewAnimator;
.source "AdapterViewFlipper.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final DEFAULT_INTERVAL:I = 0x2710

.field private static final LOGD:Z = false

.field private static final TAG:Ljava/lang/String; = "ViewFlipper"


# instance fields
.field private final FLIP_MSG:I

.field private mAdvancedByHost:Z

.field private mAutoStart:Z

.field private mFlipInterval:I

.field private final mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRunning:Z

.field private mStarted:Z

.field private mUserPresent:Z

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/AdapterViewAnimator;-><init>(Landroid/content/Context;)V

    #@5
    .line 48
    const/16 v0, 0x2710

    #@7
    iput v0, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@9
    .line 49
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@b
    .line 51
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@d
    .line 52
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@f
    .line 53
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mVisible:Z

    #@11
    .line 54
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mUserPresent:Z

    #@13
    .line 55
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mAdvancedByHost:Z

    #@15
    .line 77
    new-instance v0, Landroid/widget/AdapterViewFlipper$1;

    #@17
    invoke-direct {v0, p0}, Landroid/widget/AdapterViewFlipper$1;-><init>(Landroid/widget/AdapterViewFlipper;)V

    #@1a
    iput-object v0, p0, Landroid/widget/AdapterViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    .line 263
    iput v2, p0, Landroid/widget/AdapterViewFlipper;->FLIP_MSG:I

    #@1e
    .line 265
    new-instance v0, Landroid/widget/AdapterViewFlipper$2;

    #@20
    invoke-direct {v0, p0}, Landroid/widget/AdapterViewFlipper$2;-><init>(Landroid/widget/AdapterViewFlipper;)V

    #@23
    iput-object v0, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@25
    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v4, 0x2710

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@7
    .line 48
    iput v4, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@9
    .line 49
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@b
    .line 51
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@d
    .line 52
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@f
    .line 53
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mVisible:Z

    #@11
    .line 54
    iput-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mUserPresent:Z

    #@13
    .line 55
    iput-boolean v2, p0, Landroid/widget/AdapterViewFlipper;->mAdvancedByHost:Z

    #@15
    .line 77
    new-instance v1, Landroid/widget/AdapterViewFlipper$1;

    #@17
    invoke-direct {v1, p0}, Landroid/widget/AdapterViewFlipper$1;-><init>(Landroid/widget/AdapterViewFlipper;)V

    #@1a
    iput-object v1, p0, Landroid/widget/AdapterViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    .line 263
    iput v3, p0, Landroid/widget/AdapterViewFlipper;->FLIP_MSG:I

    #@1e
    .line 265
    new-instance v1, Landroid/widget/AdapterViewFlipper$2;

    #@20
    invoke-direct {v1, p0}, Landroid/widget/AdapterViewFlipper$2;-><init>(Landroid/widget/AdapterViewFlipper;)V

    #@23
    iput-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@25
    .line 64
    sget-object v1, Lcom/android/internal/R$styleable;->AdapterViewFlipper:[I

    #@27
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@2a
    move-result-object v0

    #@2b
    .line 66
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2e
    move-result v1

    #@2f
    iput v1, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@31
    .line 68
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@34
    move-result v1

    #@35
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@37
    .line 72
    iput-boolean v3, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@39
    .line 74
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3c
    .line 75
    return-void
.end method

.method static synthetic access$002(Landroid/widget/AdapterViewFlipper;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput-boolean p1, p0, Landroid/widget/AdapterViewFlipper;->mUserPresent:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/widget/AdapterViewFlipper;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/widget/AdapterViewFlipper;->updateRunning()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/AdapterViewFlipper;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/AdapterViewFlipper;->updateRunning(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/AdapterViewFlipper;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@2
    return v0
.end method

.method private updateRunning()V
    .registers 2

    #@0
    .prologue
    .line 210
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/widget/AdapterViewFlipper;->updateRunning(Z)V

    #@4
    .line 211
    return-void
.end method

.method private updateRunning(Z)V
    .registers 7
    .parameter "flipNow"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 222
    iget-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mAdvancedByHost:Z

    #@3
    if-nez v3, :cond_32

    #@5
    iget-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mVisible:Z

    #@7
    if-eqz v3, :cond_32

    #@9
    iget-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@b
    if-eqz v3, :cond_32

    #@d
    iget-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mUserPresent:Z

    #@f
    if-eqz v3, :cond_32

    #@11
    iget-object v3, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@13
    if-eqz v3, :cond_32

    #@15
    move v1, v2

    #@16
    .line 224
    .local v1, running:Z
    :goto_16
    iget-boolean v3, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@18
    if-eq v1, v3, :cond_31

    #@1a
    .line 225
    if-eqz v1, :cond_34

    #@1c
    .line 226
    iget v3, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@1e
    invoke-virtual {p0, v3, p1}, Landroid/widget/AdapterViewFlipper;->showOnly(IZ)V

    #@21
    .line 227
    iget-object v3, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@23
    invoke-virtual {v3, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v0

    #@27
    .line 228
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@29
    iget v3, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@2b
    int-to-long v3, v3

    #@2c
    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2f
    .line 232
    .end local v0           #msg:Landroid/os/Message;
    :goto_2f
    iput-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@31
    .line 238
    :cond_31
    return-void

    #@32
    .line 222
    .end local v1           #running:Z
    :cond_32
    const/4 v1, 0x0

    #@33
    goto :goto_16

    #@34
    .line 230
    .restart local v1       #running:Z
    :cond_34
    iget-object v3, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@36
    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@39
    goto :goto_2f
.end method


# virtual methods
.method public fyiWillBeAdvancedByHostKThx()V
    .registers 2

    #@0
    .prologue
    .line 284
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mAdvancedByHost:Z

    #@3
    .line 285
    const/4 v0, 0x0

    #@4
    invoke-direct {p0, v0}, Landroid/widget/AdapterViewFlipper;->updateRunning(Z)V

    #@7
    .line 286
    return-void
.end method

.method public getFlipInterval()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget v0, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@2
    return v0
.end method

.method public isAutoStart()Z
    .registers 2

    #@0
    .prologue
    .line 260
    iget-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@2
    return v0
.end method

.method public isFlipping()Z
    .registers 2

    #@0
    .prologue
    .line 244
    iget-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@2
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 93
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->onAttachedToWindow()V

    #@3
    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    #@5
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@8
    .line 97
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d
    .line 98
    const-string v1, "android.intent.action.USER_PRESENT"

    #@f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12
    .line 99
    invoke-virtual {p0}, Landroid/widget/AdapterViewFlipper;->getContext()Landroid/content/Context;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/widget/AdapterViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@18
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1b
    .line 101
    iget-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@1d
    if-eqz v1, :cond_22

    #@1f
    .line 103
    invoke-virtual {p0}, Landroid/widget/AdapterViewFlipper;->startFlipping()V

    #@22
    .line 105
    :cond_22
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 109
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->onDetachedFromWindow()V

    #@3
    .line 110
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mVisible:Z

    #@6
    .line 112
    invoke-virtual {p0}, Landroid/widget/AdapterViewFlipper;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mReceiver:Landroid/content/BroadcastReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 113
    invoke-direct {p0}, Landroid/widget/AdapterViewFlipper;->updateRunning()V

    #@12
    .line 114
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 290
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 291
    const-class v0, Landroid/widget/AdapterViewFlipper;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 292
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 296
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 297
    const-class v0, Landroid/widget/AdapterViewFlipper;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 298
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 4
    .parameter "visibility"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 118
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onWindowVisibilityChanged(I)V

    #@4
    .line 119
    if-nez p1, :cond_d

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    iput-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mVisible:Z

    #@9
    .line 120
    invoke-direct {p0, v1}, Landroid/widget/AdapterViewFlipper;->updateRunning(Z)V

    #@c
    .line 121
    return-void

    #@d
    :cond_d
    move v0, v1

    #@e
    .line 119
    goto :goto_7
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "adapter"

    #@0
    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->setAdapter(Landroid/widget/Adapter;)V

    #@3
    .line 126
    invoke-direct {p0}, Landroid/widget/AdapterViewFlipper;->updateRunning()V

    #@6
    .line 127
    return-void
.end method

.method public setAutoStart(Z)V
    .registers 2
    .parameter "autoStart"

    #@0
    .prologue
    .line 252
    iput-boolean p1, p0, Landroid/widget/AdapterViewFlipper;->mAutoStart:Z

    #@2
    .line 253
    return-void
.end method

.method public setFlipInterval(I)V
    .registers 2
    .parameter "flipInterval"

    #@0
    .prologue
    .line 152
    iput p1, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@2
    .line 153
    return-void
.end method

.method public showNext()V
    .registers 5
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 179
    iget-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@3
    if-eqz v1, :cond_18

    #@5
    .line 180
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@7
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@a
    .line 181
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 182
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@12
    iget v2, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@14
    int-to-long v2, v2

    #@15
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@18
    .line 184
    .end local v0           #msg:Landroid/os/Message;
    :cond_18
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->showNext()V

    #@1b
    .line 185
    return-void
.end method

.method public showPrevious()V
    .registers 5
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 195
    iget-boolean v1, p0, Landroid/widget/AdapterViewFlipper;->mRunning:Z

    #@3
    if-eqz v1, :cond_18

    #@5
    .line 196
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@7
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@a
    .line 197
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 198
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/widget/AdapterViewFlipper;->mHandler:Landroid/os/Handler;

    #@12
    iget v2, p0, Landroid/widget/AdapterViewFlipper;->mFlipInterval:I

    #@14
    int-to-long v2, v2

    #@15
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@18
    .line 200
    .end local v0           #msg:Landroid/os/Message;
    :cond_18
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->showPrevious()V

    #@1b
    .line 201
    return-void
.end method

.method public startFlipping()V
    .registers 2

    #@0
    .prologue
    .line 159
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@3
    .line 160
    invoke-direct {p0}, Landroid/widget/AdapterViewFlipper;->updateRunning()V

    #@6
    .line 161
    return-void
.end method

.method public stopFlipping()V
    .registers 2

    #@0
    .prologue
    .line 167
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/AdapterViewFlipper;->mStarted:Z

    #@3
    .line 168
    invoke-direct {p0}, Landroid/widget/AdapterViewFlipper;->updateRunning()V

    #@6
    .line 169
    return-void
.end method
