.class abstract Landroid/widget/Editor$HandleView;
.super Landroid/view/View;
.source "Editor.java"

# interfaces
.implements Landroid/widget/Editor$TextViewPositionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "HandleView"
.end annotation


# static fields
.field private static final HISTORY_SIZE:I = 0x5

.field private static final TOUCH_UP_FILTER_DELAY_AFTER:I = 0x96

.field private static final TOUCH_UP_FILTER_DELAY_BEFORE:I = 0x15e


# instance fields
.field private mActionPopupShower:Ljava/lang/Runnable;

.field protected mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

.field private final mContainer:Landroid/widget/PopupWindow;

.field protected mDrawable:Landroid/graphics/drawable/Drawable;

.field protected mDrawableLtr:Landroid/graphics/drawable/Drawable;

.field protected mDrawableRtl:Landroid/graphics/drawable/Drawable;

.field protected mHotspotX:I

.field private mIdealVerticalOffset:F

.field protected mIsCrossed:Z

.field private mIsDragging:Z

.field private mLastParentX:I

.field private mLastParentY:I

.field protected mMoveLeftToRight:Z

.field private mNumberPreviousOffsets:I

.field private mPositionHasChanged:Z

.field private mPositionX:I

.field private mPositionY:I

.field protected mPreviousOffset:I

.field private mPreviousOffsetIndex:I

.field private final mPreviousOffsets:[I

.field private final mPreviousOffsetsTimes:[J

.field private mPreviousX:F

.field private mTouchOffsetY:F

.field private mTouchToWindowOffsetX:F

.field private mTouchToWindowOffsetY:F

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 11
    .parameter
    .parameter "drawableLtr"
    .parameter "drawableRtl"

    #@0
    .prologue
    const/4 v2, 0x5

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 3822
    iput-object p1, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@5
    .line 3823
    invoke-static {p1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    invoke-direct {p0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@10
    .line 3816
    const/4 v1, -0x1

    #@11
    iput v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@13
    .line 3818
    iput-boolean v6, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@15
    .line 3851
    const/high16 v1, -0x3100

    #@17
    iput v1, p0, Landroid/widget/Editor$HandleView;->mPreviousX:F

    #@19
    .line 3891
    new-array v1, v2, [J

    #@1b
    iput-object v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetsTimes:[J

    #@1d
    .line 3892
    new-array v1, v2, [I

    #@1f
    iput-object v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsets:[I

    #@21
    .line 3893
    iput v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@23
    .line 3894
    iput v5, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@25
    .line 3824
    new-instance v1, Landroid/widget/PopupWindow;

    #@27
    invoke-static {p1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v2

    #@2f
    const/4 v3, 0x0

    #@30
    const v4, 0x10102c8

    #@33
    invoke-direct {v1, v2, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@36
    iput-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@38
    .line 3826
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@3a
    invoke-virtual {v1, v6}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    #@3d
    .line 3827
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@3f
    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    #@42
    .line 3829
    invoke-static {p1}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_7d

    #@48
    instance-of v1, p0, Landroid/widget/Editor$SelectionStartHandleView;

    #@4a
    if-nez v1, :cond_50

    #@4c
    instance-of v1, p0, Landroid/widget/Editor$SelectionEndHandleView;

    #@4e
    if-eqz v1, :cond_7d

    #@50
    .line 3830
    :cond_50
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@52
    const/16 v2, 0x7d2

    #@54
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@57
    .line 3831
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@59
    invoke-virtual {v1, v6}, Landroid/widget/PopupWindow;->setBaseAppType(I)V

    #@5c
    .line 3836
    :goto_5c
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@5e
    invoke-virtual {v1, p0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@61
    .line 3838
    iput-object p2, p0, Landroid/widget/Editor$HandleView;->mDrawableLtr:Landroid/graphics/drawable/Drawable;

    #@63
    .line 3839
    iput-object p3, p0, Landroid/widget/Editor$HandleView;->mDrawableRtl:Landroid/graphics/drawable/Drawable;

    #@65
    .line 3841
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->updateDrawable()V

    #@68
    .line 3843
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6a
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@6d
    move-result v0

    #@6e
    .line 3844
    .local v0, handleHeight:I
    const v1, -0x41666666

    #@71
    int-to-float v2, v0

    #@72
    mul-float/2addr v1, v2

    #@73
    iput v1, p0, Landroid/widget/Editor$HandleView;->mTouchOffsetY:F

    #@75
    .line 3845
    const v1, 0x3f333333

    #@78
    int-to-float v2, v0

    #@79
    mul-float/2addr v1, v2

    #@7a
    iput v1, p0, Landroid/widget/Editor$HandleView;->mIdealVerticalOffset:F

    #@7c
    .line 3846
    return-void

    #@7d
    .line 3833
    .end local v0           #handleHeight:I
    :cond_7d
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@7f
    const/16 v2, 0x3ea

    #@81
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@84
    goto :goto_5c
.end method

.method private addPositionToTouchUpFilter(I)V
    .registers 6
    .parameter "offset"

    #@0
    .prologue
    .line 3902
    iget v0, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    rem-int/lit8 v0, v0, 0x5

    #@6
    iput v0, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@8
    .line 3903
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsets:[I

    #@a
    iget v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@c
    aput p1, v0, v1

    #@e
    .line 3904
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetsTimes:[J

    #@10
    iget v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@12
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@15
    move-result-wide v2

    #@16
    aput-wide v2, v0, v1

    #@18
    .line 3905
    iget v0, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    iput v0, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@1e
    .line 3906
    return-void
.end method

.method private filterOnTouchUp()V
    .registers 10

    #@0
    .prologue
    .line 3909
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v3

    #@4
    .line 3910
    .local v3, now:J
    const/4 v0, 0x0

    #@5
    .line 3911
    .local v0, i:I
    iget v2, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@7
    .line 3912
    .local v2, index:I
    iget v5, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@9
    const/4 v6, 0x5

    #@a
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@d
    move-result v1

    #@e
    .line 3913
    .local v1, iMax:I
    :goto_e
    if-ge v0, v1, :cond_26

    #@10
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetsTimes:[J

    #@12
    aget-wide v5, v5, v2

    #@14
    sub-long v5, v3, v5

    #@16
    const-wide/16 v7, 0x96

    #@18
    cmp-long v5, v5, v7

    #@1a
    if-gez v5, :cond_26

    #@1c
    .line 3914
    add-int/lit8 v0, v0, 0x1

    #@1e
    .line 3915
    iget v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetIndex:I

    #@20
    sub-int/2addr v5, v0

    #@21
    add-int/lit8 v5, v5, 0x5

    #@23
    rem-int/lit8 v2, v5, 0x5

    #@25
    goto :goto_e

    #@26
    .line 3918
    :cond_26
    if-lez v0, :cond_3e

    #@28
    if-ge v0, v1, :cond_3e

    #@2a
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsetsTimes:[J

    #@2c
    aget-wide v5, v5, v2

    #@2e
    sub-long v5, v3, v5

    #@30
    const-wide/16 v7, 0x15e

    #@32
    cmp-long v5, v5, v7

    #@34
    if-lez v5, :cond_3e

    #@36
    .line 3920
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffsets:[I

    #@38
    aget v5, v5, v2

    #@3a
    const/4 v6, 0x0

    #@3b
    invoke-virtual {p0, v5, v6}, Landroid/widget/Editor$HandleView;->positionAtCursorOffset(IZ)V

    #@3e
    .line 3922
    :cond_3e
    return-void
.end method

.method private isVisible()Z
    .registers 4

    #@0
    .prologue
    .line 4021
    iget-boolean v0, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 4022
    const/4 v0, 0x1

    #@5
    .line 4029
    :goto_5
    return v0

    #@6
    .line 4025
    :cond_6
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@8
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/widget/TextView;->isInBatchEditMode()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    .line 4026
    const/4 v0, 0x0

    #@13
    goto :goto_5

    #@14
    .line 4029
    :cond_14
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@16
    iget v1, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@18
    iget v2, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@1a
    add-int/2addr v1, v2

    #@1b
    iget v2, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@1d
    invoke-static {v0, v1, v2}, Landroid/widget/Editor;->access$3300(Landroid/widget/Editor;II)Z

    #@20
    move-result v0

    #@21
    goto :goto_5
.end method

.method private startTouchUpFilter(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 3897
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@3
    .line 3898
    invoke-direct {p0, p1}, Landroid/widget/Editor$HandleView;->addPositionToTouchUpFilter(I)V

    #@6
    .line 3899
    return-void
.end method


# virtual methods
.method public crossHandle(Z)V
    .registers 8
    .parameter "crossing"

    #@0
    .prologue
    .line 3854
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@2
    if-ne v4, p1, :cond_5

    #@4
    .line 3876
    :goto_4
    return-void

    #@5
    .line 3855
    :cond_5
    iput-boolean p1, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@7
    .line 3856
    iget-object v3, p0, Landroid/widget/Editor$HandleView;->mDrawableLtr:Landroid/graphics/drawable/Drawable;

    #@9
    .line 3857
    .local v3, temp:Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Landroid/widget/Editor$HandleView;->mDrawableRtl:Landroid/graphics/drawable/Drawable;

    #@b
    iput-object v4, p0, Landroid/widget/Editor$HandleView;->mDrawableLtr:Landroid/graphics/drawable/Drawable;

    #@d
    .line 3858
    iput-object v3, p0, Landroid/widget/Editor$HandleView;->mDrawableRtl:Landroid/graphics/drawable/Drawable;

    #@f
    .line 3859
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->updateDrawable()V

    #@12
    .line 3861
    iget-object v4, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@14
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@1b
    move-result-object v0

    #@1c
    .line 3862
    .local v0, layout:Landroid/text/Layout;
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@1f
    move-result v2

    #@20
    .line 3863
    .local v2, offset:I
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    #@23
    move-result v1

    #@24
    .line 3865
    .local v1, line:I
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@27
    move-result v4

    #@28
    const/high16 v5, 0x3f00

    #@2a
    sub-float/2addr v4, v5

    #@2b
    iget v5, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@2d
    int-to-float v5, v5

    #@2e
    sub-float/2addr v4, v5

    #@2f
    float-to-int v4, v4

    #@30
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@32
    .line 3866
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@35
    move-result v4

    #@36
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@38
    .line 3869
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@3a
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@3c
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@43
    move-result v5

    #@44
    add-int/2addr v4, v5

    #@45
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@47
    .line 3870
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@49
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@4b
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@52
    move-result v5

    #@53
    add-int/2addr v4, v5

    #@54
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@56
    .line 3872
    iput v2, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@58
    .line 3873
    const/4 v4, 0x1

    #@59
    iput-boolean v4, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@5b
    .line 3875
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->postInvalidate()V

    #@5e
    goto :goto_4
.end method

.method protected dismiss()V
    .registers 2

    #@0
    .prologue
    .line 3952
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@3
    .line 3953
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@5
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@8
    .line 3954
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->onDetached()V

    #@b
    .line 3955
    return-void
.end method

.method public abstract getCurrentCursorOffset()I
.end method

.method protected abstract getHotspotX(Landroid/graphics/drawable/Drawable;Z)I
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    .line 3958
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->dismiss()V

    #@3
    .line 3960
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@5
    invoke-static {v0}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0, p0}, Landroid/widget/Editor$PositionListener;->removeSubscriber(Landroid/widget/Editor$TextViewPositionListener;)V

    #@c
    .line 3961
    return-void
.end method

.method protected hideActionPopupWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3993
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 3994
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@7
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@10
    .line 3996
    :cond_10
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@12
    if-eqz v0, :cond_7d

    #@14
    .line 3998
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@16
    if-eqz v0, :cond_5e

    #@18
    .line 3999
    const-string v0, "Cliptray_editer"

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "HandleView, hideActionPopupWindow()isHideCliptrayOpenCueAfterDelay: "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@27
    invoke-static {v2}, Landroid/widget/Editor;->access$3000(Landroid/widget/Editor;)Z

    #@2a
    move-result v2

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 4000
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@38
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@3a
    if-eqz v0, :cond_5e

    #@3c
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@3e
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@40
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@43
    move-result v0

    #@44
    const/16 v1, 0x8

    #@46
    if-ne v0, v1, :cond_5e

    #@48
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@4a
    invoke-static {v0}, Landroid/widget/Editor;->access$3000(Landroid/widget/Editor;)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_5e

    #@50
    .line 4002
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@52
    iget-object v0, v0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@54
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptraycue()V

    #@57
    .line 4003
    const-string v0, "Cliptray_editer"

    #@59
    const-string v1, "Editor::HandleView,  hideActionPopupWindow() , hideCliptraycue()"

    #@5b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 4006
    :cond_5e
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@60
    invoke-virtual {v0}, Landroid/widget/Editor$ActionPopupWindow;->hide()V

    #@63
    .line 4007
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@65
    if-eqz v0, :cond_7d

    #@67
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->isShowing()Z

    #@6a
    move-result v0

    #@6b
    if-nez v0, :cond_7d

    #@6d
    .line 4008
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@6f
    iget-object v0, v0, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@71
    if-eqz v0, :cond_7b

    #@73
    instance-of v0, p0, Landroid/widget/Editor$InsertionHandleView;

    #@75
    if-nez v0, :cond_7b

    #@77
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@79
    iput-object v3, v0, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@7b
    .line 4009
    :cond_7b
    iput-object v3, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@7d
    .line 4013
    :cond_7d
    return-void
.end method

.method public isDragging()Z
    .registers 2

    #@0
    .prologue
    .line 4235
    iget-boolean v0, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@2
    return v0
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 4016
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public offsetHasBeenChanged()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3925
    iget v1, p0, Landroid/widget/Editor$HandleView;->mNumberPreviousOffsets:I

    #@3
    if-le v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onDetached()V
    .registers 1

    #@0
    .prologue
    .line 4243
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->hideActionPopupWindow()V

    #@3
    .line 4244
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "c"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4159
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    iget v1, p0, Landroid/view/View;->mRight:I

    #@5
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@7
    sub-int/2addr v1, v2

    #@8
    iget v2, p0, Landroid/view/View;->mBottom:I

    #@a
    iget v3, p0, Landroid/view/View;->mTop:I

    #@c
    sub-int/2addr v2, v3

    #@d
    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@10
    .line 4160
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@15
    .line 4161
    return-void
.end method

.method onHandleMoved()V
    .registers 1

    #@0
    .prologue
    .line 4239
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->hideActionPopupWindow()V

    #@3
    .line 4240
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 3930
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@b
    move-result v1

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/widget/Editor$HandleView;->setMeasuredDimension(II)V

    #@f
    .line 3931
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 4165
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@5
    move-result v9

    #@6
    packed-switch v9, :pswitch_data_e8

    #@9
    .line 4231
    :cond_9
    :goto_9
    return v12

    #@a
    .line 4167
    :pswitch_a
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@d
    move-result v9

    #@e
    invoke-direct {p0, v9}, Landroid/widget/Editor$HandleView;->startTouchUpFilter(I)V

    #@11
    .line 4168
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    #@14
    move-result v9

    #@15
    iget v10, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@17
    int-to-float v10, v10

    #@18
    sub-float/2addr v9, v10

    #@19
    iput v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetX:F

    #@1b
    .line 4169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    #@1e
    move-result v9

    #@1f
    iget v10, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@21
    int-to-float v10, v10

    #@22
    sub-float/2addr v9, v10

    #@23
    iput v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@25
    .line 4171
    iget-object v9, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@27
    invoke-static {v9}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@2a
    move-result-object v5

    #@2b
    .line 4172
    .local v5, positionListener:Landroid/widget/Editor$PositionListener;
    invoke-virtual {v5}, Landroid/widget/Editor$PositionListener;->getPositionX()I

    #@2e
    move-result v9

    #@2f
    iput v9, p0, Landroid/widget/Editor$HandleView;->mLastParentX:I

    #@31
    .line 4173
    invoke-virtual {v5}, Landroid/widget/Editor$PositionListener;->getPositionY()I

    #@34
    move-result v9

    #@35
    iput v9, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@37
    .line 4174
    iput-boolean v12, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@39
    .line 4176
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3b
    if-eqz v9, :cond_9

    #@3d
    .line 4177
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    #@40
    move-result v9

    #@41
    iput v9, p0, Landroid/widget/Editor$HandleView;->mPreviousX:F

    #@43
    .line 4178
    instance-of v9, p0, Landroid/widget/Editor$SelectionStartHandleView;

    #@45
    if-eqz v9, :cond_49

    #@47
    iput-boolean v12, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@49
    .line 4179
    :cond_49
    instance-of v9, p0, Landroid/widget/Editor$SelectionEndHandleView;

    #@4b
    if-eqz v9, :cond_9

    #@4d
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@4f
    goto :goto_9

    #@50
    .line 4185
    .end local v5           #positionListener:Landroid/widget/Editor$PositionListener;
    :pswitch_50
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    #@53
    move-result v7

    #@54
    .line 4186
    .local v7, rawX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    #@57
    move-result v8

    #@58
    .line 4189
    .local v8, rawY:F
    iget v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@5a
    iget v10, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@5c
    int-to-float v10, v10

    #@5d
    sub-float v6, v9, v10

    #@5f
    .line 4190
    .local v6, previousVerticalOffset:F
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@61
    int-to-float v9, v9

    #@62
    sub-float v9, v8, v9

    #@64
    iget v10, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@66
    int-to-float v10, v10

    #@67
    sub-float v0, v9, v10

    #@69
    .line 4192
    .local v0, currentVerticalOffset:F
    iget v9, p0, Landroid/widget/Editor$HandleView;->mIdealVerticalOffset:F

    #@6b
    cmpg-float v9, v6, v9

    #@6d
    if-gez v9, :cond_b5

    #@6f
    .line 4193
    iget v9, p0, Landroid/widget/Editor$HandleView;->mIdealVerticalOffset:F

    #@71
    invoke-static {v0, v9}, Ljava/lang/Math;->min(FF)F

    #@74
    move-result v3

    #@75
    .line 4194
    .local v3, newVerticalOffset:F
    invoke-static {v3, v6}, Ljava/lang/Math;->max(FF)F

    #@78
    move-result v3

    #@79
    .line 4199
    :goto_79
    iget v9, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@7b
    int-to-float v9, v9

    #@7c
    add-float/2addr v9, v3

    #@7d
    iput v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@7f
    .line 4201
    iget v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetX:F

    #@81
    sub-float v9, v7, v9

    #@83
    iget v10, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@85
    int-to-float v10, v10

    #@86
    add-float v1, v9, v10

    #@88
    .line 4202
    .local v1, newPosX:F
    iget v9, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@8a
    sub-float v9, v8, v9

    #@8c
    iget v10, p0, Landroid/widget/Editor$HandleView;->mTouchOffsetY:F

    #@8e
    add-float v2, v9, v10

    #@90
    .line 4204
    .local v2, newPosY:F
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@92
    if-eqz v9, :cond_b0

    #@94
    .line 4205
    iget-object v9, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@96
    invoke-static {v9}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@99
    move-result-object v9

    #@9a
    invoke-virtual {v9, v1, v2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@9d
    move-result v4

    #@9e
    .line 4206
    .local v4, offset:I
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@a0
    if-eq v4, v9, :cond_b0

    #@a2
    .line 4207
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPreviousX:F

    #@a4
    cmpg-float v9, v9, v7

    #@a6
    if-gez v9, :cond_c0

    #@a8
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@aa
    if-ge v9, v4, :cond_c0

    #@ac
    iput-boolean v12, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@ae
    .line 4209
    :cond_ae
    :goto_ae
    iput v7, p0, Landroid/widget/Editor$HandleView;->mPreviousX:F

    #@b0
    .line 4213
    .end local v4           #offset:I
    :cond_b0
    invoke-virtual {p0, v1, v2}, Landroid/widget/Editor$HandleView;->updatePosition(FF)V

    #@b3
    goto/16 :goto_9

    #@b5
    .line 4196
    .end local v1           #newPosX:F
    .end local v2           #newPosY:F
    .end local v3           #newVerticalOffset:F
    :cond_b5
    iget v9, p0, Landroid/widget/Editor$HandleView;->mIdealVerticalOffset:F

    #@b7
    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    #@ba
    move-result v3

    #@bb
    .line 4197
    .restart local v3       #newVerticalOffset:F
    invoke-static {v3, v6}, Ljava/lang/Math;->min(FF)F

    #@be
    move-result v3

    #@bf
    goto :goto_79

    #@c0
    .line 4208
    .restart local v1       #newPosX:F
    .restart local v2       #newPosY:F
    .restart local v4       #offset:I
    :cond_c0
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPreviousX:F

    #@c2
    cmpl-float v9, v9, v7

    #@c4
    if-lez v9, :cond_ae

    #@c6
    iget v9, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@c8
    if-le v9, v4, :cond_ae

    #@ca
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@cc
    goto :goto_ae

    #@cd
    .line 4218
    .end local v0           #currentVerticalOffset:F
    .end local v1           #newPosX:F
    .end local v2           #newPosY:F
    .end local v3           #newVerticalOffset:F
    .end local v4           #offset:I
    .end local v6           #previousVerticalOffset:F
    .end local v7           #rawX:F
    .end local v8           #rawY:F
    :pswitch_cd
    invoke-direct {p0}, Landroid/widget/Editor$HandleView;->filterOnTouchUp()V

    #@d0
    .line 4219
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@d2
    .line 4220
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@d4
    if-eqz v9, :cond_9

    #@d6
    instance-of v9, p0, Landroid/widget/Editor$SelectionStartHandleView;

    #@d8
    if-nez v9, :cond_de

    #@da
    instance-of v9, p0, Landroid/widget/Editor$SelectionEndHandleView;

    #@dc
    if-eqz v9, :cond_9

    #@de
    .line 4223
    :cond_de
    invoke-virtual {p0, v11}, Landroid/widget/Editor$HandleView;->showActionPopupWindow(I)V

    #@e1
    goto/16 :goto_9

    #@e3
    .line 4228
    :pswitch_e3
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@e5
    goto/16 :goto_9

    #@e7
    .line 4165
    nop

    #@e8
    :pswitch_data_e8
    .packed-switch 0x0
        :pswitch_a
        :pswitch_cd
        :pswitch_50
        :pswitch_e3
    .end packed-switch
.end method

.method protected positionAtCursorOffset(IZ)V
    .registers 9
    .parameter "offset"
    .parameter "parentScrolled"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 4040
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v5

    #@8
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@b
    move-result-object v0

    #@c
    .line 4041
    .local v0, layout:Landroid/text/Layout;
    if-nez v0, :cond_14

    #@e
    .line 4043
    iget-object v3, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@10
    invoke-virtual {v3}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@13
    .line 4072
    :cond_13
    :goto_13
    return-void

    #@14
    .line 4047
    :cond_14
    iget v5, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@16
    if-eq p1, v5, :cond_44

    #@18
    move v2, v3

    #@19
    .line 4048
    .local v2, offsetChanged:Z
    :goto_19
    if-nez v2, :cond_1d

    #@1b
    if-eqz p2, :cond_13

    #@1d
    .line 4049
    :cond_1d
    if-eqz v2, :cond_4c

    #@1f
    .line 4050
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@21
    if-eqz v5, :cond_46

    #@23
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@25
    iget-boolean v5, v5, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@27
    if-eqz v5, :cond_46

    #@29
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@2b
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Landroid/widget/TextView;->hasSelection()Z

    #@32
    move-result v5

    #@33
    if-nez v5, :cond_46

    #@35
    iget-boolean v5, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@37
    if-nez v5, :cond_46

    #@39
    .line 4052
    iget-object v3, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@3b
    invoke-virtual {v3, v4}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@3e
    .line 4053
    iget-object v3, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@40
    invoke-virtual {v3}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@43
    goto :goto_13

    #@44
    .end local v2           #offsetChanged:Z
    :cond_44
    move v2, v4

    #@45
    .line 4047
    goto :goto_19

    #@46
    .line 4057
    .restart local v2       #offsetChanged:Z
    :cond_46
    invoke-virtual {p0, p1}, Landroid/widget/Editor$HandleView;->updateSelection(I)V

    #@49
    .line 4058
    invoke-direct {p0, p1}, Landroid/widget/Editor$HandleView;->addPositionToTouchUpFilter(I)V

    #@4c
    .line 4060
    :cond_4c
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@4f
    move-result v1

    #@50
    .line 4062
    .local v1, line:I
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@53
    move-result v4

    #@54
    const/high16 v5, 0x3f00

    #@56
    sub-float/2addr v4, v5

    #@57
    iget v5, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@59
    int-to-float v5, v5

    #@5a
    sub-float/2addr v4, v5

    #@5b
    float-to-int v4, v4

    #@5c
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@5e
    .line 4063
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@61
    move-result v4

    #@62
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@64
    .line 4066
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@66
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@68
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@6f
    move-result v5

    #@70
    add-int/2addr v4, v5

    #@71
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@73
    .line 4067
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@75
    iget-object v5, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@77
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@7e
    move-result v5

    #@7f
    add-int/2addr v4, v5

    #@80
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@82
    .line 4069
    iput p1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@84
    .line 4070
    iput-boolean v3, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@86
    goto :goto_13
.end method

.method public show()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3934
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->isShowing()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_9

    #@8
    .line 3949
    :goto_8
    return-void

    #@9
    .line 3936
    :cond_9
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@b
    if-eqz v2, :cond_32

    #@d
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@f
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@12
    move-result-object v2

    #@13
    if-eqz v2, :cond_32

    #@15
    .line 3937
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@17
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Landroid/widget/TextView;->getSelectionStart()I

    #@1e
    move-result v1

    #@1f
    .line 3938
    .local v1, selectionStart:I
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@21
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Landroid/widget/TextView;->getSelectionEnd()I

    #@28
    move-result v0

    #@29
    .line 3939
    .local v0, selectionEnd:I
    iget-boolean v2, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@2b
    if-eqz v2, :cond_49

    #@2d
    if-ge v1, v0, :cond_49

    #@2f
    invoke-virtual {p0, v3}, Landroid/widget/Editor$HandleView;->crossHandle(Z)V

    #@32
    .line 3942
    .end local v0           #selectionEnd:I
    .end local v1           #selectionStart:I
    :cond_32
    :goto_32
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@34
    invoke-static {v2}, Landroid/widget/Editor;->access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, p0, v4}, Landroid/widget/Editor$PositionListener;->addSubscriber(Landroid/widget/Editor$TextViewPositionListener;Z)V

    #@3b
    .line 3945
    const/4 v2, -0x1

    #@3c
    iput v2, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@3e
    .line 3946
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@41
    move-result v2

    #@42
    invoke-virtual {p0, v2, v3}, Landroid/widget/Editor$HandleView;->positionAtCursorOffset(IZ)V

    #@45
    .line 3948
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->hideActionPopupWindow()V

    #@48
    goto :goto_8

    #@49
    .line 3940
    .restart local v0       #selectionEnd:I
    .restart local v1       #selectionStart:I
    :cond_49
    iget-boolean v2, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@4b
    if-nez v2, :cond_32

    #@4d
    if-le v1, v0, :cond_32

    #@4f
    invoke-virtual {p0, v4}, Landroid/widget/Editor$HandleView;->crossHandle(Z)V

    #@52
    goto :goto_32
.end method

.method showActionPopupWindow(I)V
    .registers 6
    .parameter "delay"

    #@0
    .prologue
    .line 3964
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@6
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v0

    #@a
    iget-boolean v0, v0, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@c
    if-nez v0, :cond_f

    #@e
    .line 3990
    :goto_e
    return-void

    #@f
    .line 3965
    :cond_f
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@11
    if-nez v0, :cond_1c

    #@13
    .line 3966
    new-instance v0, Landroid/widget/Editor$ActionPopupWindow;

    #@15
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@17
    invoke-direct {v0, v1}, Landroid/widget/Editor$ActionPopupWindow;-><init>(Landroid/widget/Editor;)V

    #@1a
    iput-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@1c
    .line 3968
    :cond_1c
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@1e
    if-nez v0, :cond_34

    #@20
    .line 3969
    new-instance v0, Landroid/widget/Editor$HandleView$1;

    #@22
    invoke-direct {v0, p0}, Landroid/widget/Editor$HandleView$1;-><init>(Landroid/widget/Editor$HandleView;)V

    #@25
    iput-object v0, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@27
    .line 3989
    :goto_27
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@29
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2c
    move-result-object v0

    #@2d
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@2f
    int-to-long v2, p1

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@33
    goto :goto_e

    #@34
    .line 3987
    :cond_34
    iget-object v0, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@36
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@39
    move-result-object v0

    #@3a
    iget-object v1, p0, Landroid/widget/Editor$HandleView;->mActionPopupShower:Ljava/lang/Runnable;

    #@3c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@3f
    goto :goto_27
.end method

.method protected updateDrawable()V
    .registers 4

    #@0
    .prologue
    .line 3879
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@3
    move-result v1

    #@4
    .line 3880
    .local v1, offset:I
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@6
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, v1}, Landroid/text/Layout;->isRtlCharAt(I)Z

    #@11
    move-result v0

    #@12
    .line 3881
    .local v0, isRtlCharAtOffset:Z
    if-eqz v0, :cond_21

    #@14
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->mDrawableRtl:Landroid/graphics/drawable/Drawable;

    #@16
    :goto_16
    iput-object v2, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@18
    .line 3882
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1a
    invoke-virtual {p0, v2, v0}, Landroid/widget/Editor$HandleView;->getHotspotX(Landroid/graphics/drawable/Drawable;Z)I

    #@1d
    move-result v2

    #@1e
    iput v2, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@20
    .line 3883
    return-void

    #@21
    .line 3881
    :cond_21
    iget-object v2, p0, Landroid/widget/Editor$HandleView;->mDrawableLtr:Landroid/graphics/drawable/Drawable;

    #@23
    goto :goto_16
.end method

.method public abstract updatePosition(FF)V
.end method

.method public updatePosition(IIZZ)V
    .registers 19
    .parameter "parentPositionX"
    .parameter "parentPositionY"
    .parameter "parentPositionChanged"
    .parameter "parentScrolled"

    #@0
    .prologue
    .line 4076
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@3
    move-result v11

    #@4
    move/from16 v0, p4

    #@6
    invoke-virtual {p0, v11, v0}, Landroid/widget/Editor$HandleView;->positionAtCursorOffset(IZ)V

    #@9
    .line 4077
    if-nez p3, :cond_f

    #@b
    iget-boolean v11, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@d
    if-eqz v11, :cond_120

    #@f
    .line 4079
    :cond_f
    const/4 v11, 0x2

    #@10
    new-array v7, v11, [I

    #@12
    .line 4080
    .local v7, screenLocation:[I
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@14
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@17
    move-result-object v11

    #@18
    invoke-virtual {v11, v7}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    #@1b
    .line 4081
    const/4 v11, 0x2

    #@1c
    new-array v1, v11, [I

    #@1e
    .line 4082
    .local v1, drawingLocation:[I
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@20
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@23
    move-result-object v11

    #@24
    invoke-virtual {v11, v1}, Landroid/widget/TextView;->getLocationInWindow([I)V

    #@27
    .line 4083
    const/4 v11, 0x0

    #@28
    aget v11, v7, v11

    #@2a
    const/4 v12, 0x0

    #@2b
    aget v12, v1, v12

    #@2d
    sub-int v9, v11, v12

    #@2f
    .line 4084
    .local v9, winX:I
    const/4 v11, 0x1

    #@30
    aget v11, v7, v11

    #@32
    const/4 v12, 0x1

    #@33
    aget v12, v1, v12

    #@35
    sub-int v10, v11, v12

    #@37
    .line 4085
    .local v10, winY:I
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@39
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@3c
    move-result-object v11

    #@3d
    invoke-virtual {v11}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@40
    move-result-object v11

    #@41
    const v12, 0x105000c

    #@44
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@47
    move-result v8

    #@48
    .line 4087
    .local v8, statusBarHeight:I
    iget-boolean v11, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@4a
    if-eqz v11, :cond_8d

    #@4c
    .line 4089
    iget v11, p0, Landroid/widget/Editor$HandleView;->mLastParentX:I

    #@4e
    if-ne p1, v11, :cond_56

    #@50
    iget v11, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@52
    move/from16 v0, p2

    #@54
    if-eq v0, v11, :cond_8a

    #@56
    .line 4090
    :cond_56
    iget v11, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetX:F

    #@58
    iget v12, p0, Landroid/widget/Editor$HandleView;->mLastParentX:I

    #@5a
    sub-int v12, p1, v12

    #@5c
    int-to-float v12, v12

    #@5d
    add-float/2addr v11, v12

    #@5e
    iput v11, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetX:F

    #@60
    .line 4091
    iget v11, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@62
    iget v12, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@64
    sub-int v12, p2, v12

    #@66
    int-to-float v12, v12

    #@67
    add-float/2addr v11, v12

    #@68
    iput v11, p0, Landroid/widget/Editor$HandleView;->mTouchToWindowOffsetY:F

    #@6a
    .line 4092
    iput p1, p0, Landroid/widget/Editor$HandleView;->mLastParentX:I

    #@6c
    .line 4093
    move/from16 v0, p2

    #@6e
    iput v0, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@70
    .line 4095
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@72
    invoke-static {v11}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@75
    move-result v11

    #@76
    if-eqz v11, :cond_8a

    #@78
    instance-of v11, p0, Landroid/widget/Editor$SelectionStartHandleView;

    #@7a
    if-nez v11, :cond_80

    #@7c
    instance-of v11, p0, Landroid/widget/Editor$SelectionEndHandleView;

    #@7e
    if-eqz v11, :cond_8a

    #@80
    .line 4096
    :cond_80
    iget v11, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@82
    add-int/2addr v11, v10

    #@83
    iput v11, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@85
    .line 4097
    iget v11, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@87
    sub-int/2addr v11, v8

    #@88
    iput v11, p0, Landroid/widget/Editor$HandleView;->mLastParentY:I

    #@8a
    .line 4102
    :cond_8a
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->onHandleMoved()V

    #@8d
    .line 4106
    :cond_8d
    if-nez p3, :cond_99

    #@8f
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@91
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@94
    move-result-object v11

    #@95
    instance-of v11, v11, Landroid/inputmethodservice/ExtractEditText;

    #@97
    if-eqz v11, :cond_e2

    #@99
    .line 4107
    :cond_99
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@9b
    invoke-static {v11}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@9e
    move-result-object v11

    #@9f
    invoke-virtual {v11}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@a2
    move-result-object v2

    #@a3
    .line 4108
    .local v2, layout:Landroid/text/Layout;
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->getCurrentCursorOffset()I

    #@a6
    move-result v4

    #@a7
    .line 4109
    .local v4, offset:I
    invoke-virtual {v2, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    #@aa
    move-result v3

    #@ab
    .line 4111
    .local v3, line:I
    invoke-virtual {v2, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@ae
    move-result v11

    #@af
    const/high16 v12, 0x3f00

    #@b1
    sub-float/2addr v11, v12

    #@b2
    iget v12, p0, Landroid/widget/Editor$HandleView;->mHotspotX:I

    #@b4
    int-to-float v12, v12

    #@b5
    sub-float/2addr v11, v12

    #@b6
    float-to-int v11, v11

    #@b7
    iput v11, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@b9
    .line 4112
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBottom(I)I

    #@bc
    move-result v11

    #@bd
    iput v11, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@bf
    .line 4115
    iget v11, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@c1
    iget-object v12, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@c3
    invoke-static {v12}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c6
    move-result-object v12

    #@c7
    invoke-virtual {v12}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@ca
    move-result v12

    #@cb
    add-int/2addr v11, v12

    #@cc
    iput v11, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@ce
    .line 4116
    iget v11, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@d0
    iget-object v12, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@d2
    invoke-static {v12}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@d5
    move-result-object v12

    #@d6
    invoke-virtual {v12}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@d9
    move-result v12

    #@da
    add-int/2addr v11, v12

    #@db
    iput v11, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@dd
    .line 4117
    iput v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@df
    .line 4118
    const/4 v11, 0x1

    #@e0
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@e2
    .line 4121
    .end local v2           #layout:Landroid/text/Layout;
    .end local v3           #line:I
    .end local v4           #offset:I
    :cond_e2
    invoke-direct {p0}, Landroid/widget/Editor$HandleView;->isVisible()Z

    #@e5
    move-result v11

    #@e6
    if-eqz v11, :cond_136

    #@e8
    .line 4123
    iget v11, p0, Landroid/widget/Editor$HandleView;->mPositionX:I

    #@ea
    add-int v5, p1, v11

    #@ec
    .line 4124
    .local v5, positionX:I
    iget v11, p0, Landroid/widget/Editor$HandleView;->mPositionY:I

    #@ee
    add-int v6, p2, v11

    #@f0
    .line 4125
    .local v6, positionY:I
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@f2
    invoke-static {v11}, Landroid/widget/Editor;->access$1200(Landroid/widget/Editor;)Z

    #@f5
    move-result v11

    #@f6
    if-eqz v11, :cond_121

    #@f8
    instance-of v11, p0, Landroid/widget/Editor$SelectionStartHandleView;

    #@fa
    if-nez v11, :cond_100

    #@fc
    instance-of v11, p0, Landroid/widget/Editor$SelectionEndHandleView;

    #@fe
    if-eqz v11, :cond_121

    #@100
    .line 4126
    :cond_100
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@102
    const/16 v12, 0x7d2

    #@104
    invoke-virtual {v11, v12}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@107
    .line 4127
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@109
    const/4 v12, 0x1

    #@10a
    invoke-virtual {v11, v12}, Landroid/widget/PopupWindow;->setBaseAppType(I)V

    #@10d
    .line 4128
    add-int/2addr v5, v9

    #@10e
    .line 4129
    add-int/2addr v6, v10

    #@10f
    .line 4130
    sub-int/2addr v6, v8

    #@110
    .line 4135
    :goto_110
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->isShowing()Z

    #@113
    move-result v11

    #@114
    if-eqz v11, :cond_129

    #@116
    .line 4136
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@118
    const/4 v12, -0x1

    #@119
    const/4 v13, -0x1

    #@11a
    invoke-virtual {v11, v5, v6, v12, v13}, Landroid/widget/PopupWindow;->update(IIII)V

    #@11d
    .line 4153
    .end local v5           #positionX:I
    .end local v6           #positionY:I
    :cond_11d
    :goto_11d
    const/4 v11, 0x0

    #@11e
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mPositionHasChanged:Z

    #@120
    .line 4155
    .end local v1           #drawingLocation:[I
    .end local v7           #screenLocation:[I
    .end local v8           #statusBarHeight:I
    .end local v9           #winX:I
    .end local v10           #winY:I
    :cond_120
    return-void

    #@121
    .line 4132
    .restart local v1       #drawingLocation:[I
    .restart local v5       #positionX:I
    .restart local v6       #positionY:I
    .restart local v7       #screenLocation:[I
    .restart local v8       #statusBarHeight:I
    .restart local v9       #winX:I
    .restart local v10       #winY:I
    :cond_121
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@123
    const/16 v12, 0x3e8

    #@125
    invoke-virtual {v11, v12}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@128
    goto :goto_110

    #@129
    .line 4138
    :cond_129
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@12b
    iget-object v12, p0, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@12d
    invoke-static {v12}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@130
    move-result-object v12

    #@131
    const/4 v13, 0x0

    #@132
    invoke-virtual {v11, v12, v13, v5, v6}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@135
    goto :goto_11d

    #@136
    .line 4142
    .end local v5           #positionX:I
    .end local v6           #positionY:I
    :cond_136
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->isShowing()Z

    #@139
    move-result v11

    #@13a
    if-eqz v11, :cond_11d

    #@13c
    .line 4143
    sget-boolean v11, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@13e
    if-nez v11, :cond_144

    #@140
    .line 4144
    invoke-virtual {p0}, Landroid/widget/Editor$HandleView;->dismiss()V

    #@143
    goto :goto_11d

    #@144
    .line 4147
    :cond_144
    const/4 v11, 0x0

    #@145
    iput-boolean v11, p0, Landroid/widget/Editor$HandleView;->mIsDragging:Z

    #@147
    .line 4148
    iget-object v11, p0, Landroid/widget/Editor$HandleView;->mContainer:Landroid/widget/PopupWindow;

    #@149
    invoke-virtual {v11}, Landroid/widget/PopupWindow;->dismiss()V

    #@14c
    goto :goto_11d
.end method

.method protected abstract updateSelection(I)V
.end method
