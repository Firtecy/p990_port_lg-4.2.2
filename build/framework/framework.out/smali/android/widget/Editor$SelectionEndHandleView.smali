.class Landroid/widget/Editor$SelectionEndHandleView;
.super Landroid/widget/Editor$HandleView;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionEndHandleView"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter
    .parameter "drawableLtr"
    .parameter "drawableRtl"

    #@0
    .prologue
    .line 4452
    iput-object p1, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@2
    .line 4453
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Editor$HandleView;-><init>(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 4454
    return-void
.end method


# virtual methods
.method public getCurrentCursorOffset()I
    .registers 2

    #@0
    .prologue
    .line 4475
    iget-object v0, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected getHotspotX(Landroid/graphics/drawable/Drawable;Z)I
    .registers 4
    .parameter "drawable"
    .parameter "isRtlRun"

    #@0
    .prologue
    .line 4458
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    iget-boolean v0, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 4459
    if-eqz p2, :cond_11

    #@a
    .line 4460
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@d
    move-result v0

    #@e
    div-int/lit8 v0, v0, 0x4

    #@10
    .line 4469
    :goto_10
    return v0

    #@11
    .line 4462
    :cond_11
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@14
    move-result v0

    #@15
    mul-int/lit8 v0, v0, 0x3

    #@17
    div-int/lit8 v0, v0, 0x4

    #@19
    goto :goto_10

    #@1a
    .line 4466
    :cond_1a
    if-eqz p2, :cond_25

    #@1c
    .line 4467
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1f
    move-result v0

    #@20
    mul-int/lit8 v0, v0, 0x3

    #@22
    div-int/lit8 v0, v0, 0x4

    #@24
    goto :goto_10

    #@25
    .line 4469
    :cond_25
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@28
    move-result v0

    #@29
    div-int/lit8 v0, v0, 0x4

    #@2b
    goto :goto_10
.end method

.method public setActionPopupWindow(Landroid/widget/Editor$ActionPopupWindow;)V
    .registers 2
    .parameter "actionPopupWindow"

    #@0
    .prologue
    .line 4527
    iput-object p1, p0, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@2
    .line 4528
    return-void
.end method

.method public updatePosition(FF)V
    .registers 10
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 4487
    iget-object v4, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@4
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4, p1, p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@b
    move-result v1

    #@c
    .line 4490
    .local v1, offset:I
    iget-object v4, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@e
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionStart()I

    #@15
    move-result v3

    #@16
    .line 4492
    .local v3, selectionStart:I
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@18
    if-nez v4, :cond_34

    #@1a
    .line 4493
    if-gt v1, v3, :cond_30

    #@1c
    .line 4494
    add-int/lit8 v4, v3, 0x1

    #@1e
    iget-object v5, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@20
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@27
    move-result-object v5

    #@28
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@2b
    move-result v5

    #@2c
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@2f
    move-result v1

    #@30
    .line 4523
    :cond_30
    :goto_30
    invoke-virtual {p0, v1, v6}, Landroid/widget/Editor$SelectionEndHandleView;->positionAtCursorOffset(IZ)V

    #@33
    .line 4524
    :cond_33
    return-void

    #@34
    .line 4497
    :cond_34
    iget-object v4, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@36
    invoke-virtual {v4}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@39
    move-result-object v0

    #@3a
    .line 4498
    .local v0, controller:Landroid/widget/Editor$SelectionModifierCursorController;
    if-eqz v0, :cond_33

    #@3c
    .line 4500
    iget-object v4, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@3e
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionEnd()I

    #@45
    move-result v2

    #@46
    .line 4501
    .local v2, selectionEnd:I
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@48
    if-ne v2, v4, :cond_4c

    #@4a
    if-eq v3, v1, :cond_52

    #@4c
    :cond_4c
    if-ne v3, v2, :cond_82

    #@4e
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@50
    if-ne v4, v1, :cond_82

    #@52
    .line 4503
    :cond_52
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@54
    if-nez v4, :cond_65

    #@56
    .line 4504
    add-int/lit8 v1, v3, -0x1

    #@58
    .line 4505
    if-gtz v1, :cond_5d

    #@5a
    add-int/lit8 v1, v3, 0x1

    #@5c
    goto :goto_30

    #@5d
    .line 4506
    :cond_5d
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@5f
    if-nez v4, :cond_30

    #@61
    invoke-virtual {v0, v5}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@64
    goto :goto_30

    #@65
    .line 4508
    :cond_65
    add-int/lit8 v1, v3, 0x1

    #@67
    .line 4509
    iget-object v4, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@69
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@70
    move-result-object v4

    #@71
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@74
    move-result v4

    #@75
    if-lt v1, v4, :cond_7a

    #@77
    add-int/lit8 v1, v3, -0x1

    #@79
    goto :goto_30

    #@7a
    .line 4510
    :cond_7a
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@7c
    if-eqz v4, :cond_30

    #@7e
    invoke-virtual {v0, v6}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@81
    goto :goto_30

    #@82
    .line 4512
    :cond_82
    if-le v3, v2, :cond_90

    #@84
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@86
    if-lt v4, v1, :cond_90

    #@88
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@8a
    if-nez v4, :cond_90

    #@8c
    .line 4513
    invoke-virtual {v0, v5}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@8f
    goto :goto_30

    #@90
    .line 4514
    :cond_90
    if-ge v3, v2, :cond_9e

    #@92
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@94
    if-gt v4, v1, :cond_9e

    #@96
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@98
    if-eqz v4, :cond_9e

    #@9a
    .line 4515
    invoke-virtual {v0, v6}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@9d
    goto :goto_30

    #@9e
    .line 4516
    :cond_9e
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@a0
    if-ne v2, v4, :cond_b2

    #@a2
    if-ge v3, v1, :cond_b2

    #@a4
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@a6
    if-eqz v4, :cond_b2

    #@a8
    if-le v3, v2, :cond_b2

    #@aa
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@ac
    if-ge v4, v1, :cond_b2

    #@ae
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@b0
    if-eqz v4, :cond_c6

    #@b2
    :cond_b2
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@b4
    if-ne v2, v4, :cond_30

    #@b6
    if-le v3, v1, :cond_30

    #@b8
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mIsCrossed:Z

    #@ba
    if-nez v4, :cond_30

    #@bc
    if-ge v3, v2, :cond_30

    #@be
    iget v4, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@c0
    if-le v4, v1, :cond_30

    #@c2
    iget-boolean v4, p0, Landroid/widget/Editor$HandleView;->mMoveLeftToRight:Z

    #@c4
    if-eqz v4, :cond_30

    #@c6
    .line 4520
    :cond_c6
    iget v1, p0, Landroid/widget/Editor$HandleView;->mPreviousOffset:I

    #@c8
    goto/16 :goto_30
.end method

.method public updateSelection(I)V
    .registers 4
    .parameter "offset"

    #@0
    .prologue
    .line 4480
    iget-object v0, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v0}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/text/Spannable;

    #@c
    iget-object v1, p0, Landroid/widget/Editor$SelectionEndHandleView;->this$0:Landroid/widget/Editor;

    #@e
    invoke-static {v1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionStart()I

    #@15
    move-result v1

    #@16
    invoke-static {v0, v1, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@19
    .line 4482
    invoke-virtual {p0}, Landroid/widget/Editor$SelectionEndHandleView;->updateDrawable()V

    #@1c
    .line 4483
    return-void
.end method
