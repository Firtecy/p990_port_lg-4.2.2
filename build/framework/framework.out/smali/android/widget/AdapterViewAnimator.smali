.class public abstract Landroid/widget/AdapterViewAnimator;
.super Landroid/widget/AdapterView;
.source "AdapterViewAnimator.java"

# interfaces
.implements Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;
.implements Landroid/widget/Advanceable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AdapterViewAnimator$SavedState;,
        Landroid/widget/AdapterViewAnimator$CheckForTap;,
        Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/Adapter;",
        ">;",
        "Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;",
        "Landroid/widget/Advanceable;"
    }
.end annotation


# static fields
.field private static final DEFAULT_ANIMATION_DURATION:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "RemoteViewAnimator"

.field static final TOUCH_MODE_DOWN_IN_CURRENT_VIEW:I = 0x1

.field static final TOUCH_MODE_HANDLED:I = 0x2

.field static final TOUCH_MODE_NONE:I


# instance fields
.field mActiveOffset:I

.field mAdapter:Landroid/widget/Adapter;

.field mAnimateFirstTime:Z

.field mCurrentWindowEnd:I

.field mCurrentWindowStart:I

.field mCurrentWindowStartUnbounded:I

.field mDataSetObserver:Landroid/widget/AdapterView$AdapterDataSetObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/AdapterView",
            "<",
            "Landroid/widget/Adapter;",
            ">.AdapterDataSetObserver;"
        }
    .end annotation
.end field

.field mDeferNotifyDataSetChanged:Z

.field mFirstTime:Z

.field mInAnimation:Landroid/animation/ObjectAnimator;

.field mLoopViews:Z

.field mMaxNumActiveViews:I

.field mOutAnimation:Landroid/animation/ObjectAnimator;

.field private mPendingCheckForTap:Ljava/lang/Runnable;

.field mPreviousViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mReferenceChildHeight:I

.field mReferenceChildWidth:I

.field mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

.field private mRestoreWhichChild:I

.field private mTouchMode:I

.field mViewsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/AdapterViewAnimator$ViewAndMetaData;",
            ">;"
        }
    .end annotation
.end field

.field mWhichChild:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 168
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/AdapterViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 172
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AdapterViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 176
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 56
    iput v4, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@8
    .line 62
    iput v6, p0, Landroid/widget/AdapterViewAnimator;->mRestoreWhichChild:I

    #@a
    .line 67
    iput-boolean v5, p0, Landroid/widget/AdapterViewAnimator;->mAnimateFirstTime:Z

    #@c
    .line 73
    iput v4, p0, Landroid/widget/AdapterViewAnimator;->mActiveOffset:I

    #@e
    .line 79
    iput v5, p0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@10
    .line 84
    new-instance v3, Ljava/util/HashMap;

    #@12
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@15
    iput-object v3, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@17
    .line 94
    iput v4, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@19
    .line 99
    iput v6, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@1b
    .line 105
    iput v4, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@1d
    .line 125
    iput-boolean v4, p0, Landroid/widget/AdapterViewAnimator;->mDeferNotifyDataSetChanged:Z

    #@1f
    .line 130
    iput-boolean v5, p0, Landroid/widget/AdapterViewAnimator;->mFirstTime:Z

    #@21
    .line 136
    iput-boolean v5, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@23
    .line 142
    iput v6, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@25
    .line 143
    iput v6, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@27
    .line 154
    iput v4, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@29
    .line 178
    sget-object v3, Lcom/android/internal/R$styleable;->AdapterViewAnimator:[I

    #@2b
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@2e
    move-result-object v0

    #@2f
    .line 180
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@32
    move-result v2

    #@33
    .line 182
    .local v2, resource:I
    if-lez v2, :cond_57

    #@35
    .line 183
    invoke-virtual {p0, p1, v2}, Landroid/widget/AdapterViewAnimator;->setInAnimation(Landroid/content/Context;I)V

    #@38
    .line 188
    :goto_38
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3b
    move-result v2

    #@3c
    .line 189
    if-lez v2, :cond_5f

    #@3e
    .line 190
    invoke-virtual {p0, p1, v2}, Landroid/widget/AdapterViewAnimator;->setOutAnimation(Landroid/content/Context;I)V

    #@41
    .line 195
    :goto_41
    const/4 v3, 0x2

    #@42
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@45
    move-result v1

    #@46
    .line 197
    .local v1, flag:Z
    invoke-virtual {p0, v1}, Landroid/widget/AdapterViewAnimator;->setAnimateFirstView(Z)V

    #@49
    .line 199
    const/4 v3, 0x3

    #@4a
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4d
    move-result v3

    #@4e
    iput-boolean v3, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@50
    .line 202
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@53
    .line 204
    invoke-direct {p0}, Landroid/widget/AdapterViewAnimator;->initViewAnimator()V

    #@56
    .line 205
    return-void

    #@57
    .line 185
    .end local v1           #flag:Z
    :cond_57
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getDefaultInAnimation()Landroid/animation/ObjectAnimator;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {p0, v3}, Landroid/widget/AdapterViewAnimator;->setInAnimation(Landroid/animation/ObjectAnimator;)V

    #@5e
    goto :goto_38

    #@5f
    .line 192
    :cond_5f
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getDefaultOutAnimation()Landroid/animation/ObjectAnimator;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {p0, v3}, Landroid/widget/AdapterViewAnimator;->setOutAnimation(Landroid/animation/ObjectAnimator;)V

    #@66
    goto :goto_41
.end method

.method static synthetic access$000(Landroid/widget/AdapterViewAnimator;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@2
    return v0
.end method

.method private addChild(Landroid/view/View;)V
    .registers 6
    .parameter "child"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 576
    invoke-virtual {p0, p1}, Landroid/widget/AdapterViewAnimator;->createOrReuseLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, p1, v2, v1}, Landroid/widget/AdapterViewAnimator;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    #@9
    .line 581
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@b
    if-eq v1, v2, :cond_11

    #@d
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@f
    if-ne v1, v2, :cond_24

    #@11
    .line 582
    :cond_11
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@14
    move-result v0

    #@15
    .line 583
    .local v0, measureSpec:I
    invoke-virtual {p1, v0, v0}, Landroid/view/View;->measure(II)V

    #@18
    .line 584
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@1b
    move-result v1

    #@1c
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@1e
    .line 585
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@21
    move-result v1

    #@22
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@24
    .line 587
    .end local v0           #measureSpec:I
    :cond_24
    return-void
.end method

.method private getMetaDataForChild(Landroid/view/View;)Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    .registers 5
    .parameter "child"

    #@0
    .prologue
    .line 392
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1b

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@16
    .line 393
    .local v1, vm:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    iget-object v2, v1, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@18
    if-ne v2, p1, :cond_a

    #@1a
    .line 397
    .end local v1           #vm:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    :goto_1a
    return-object v1

    #@1b
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_1a
.end method

.method private initViewAnimator()V
    .registers 2

    #@0
    .prologue
    .line 211
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    iput-object v0, p0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@7
    .line 212
    return-void
.end method

.method private measureChildren()V
    .registers 9

    #@0
    .prologue
    const/high16 v7, 0x4000

    #@2
    .line 679
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getChildCount()I

    #@5
    move-result v3

    #@6
    .line 680
    .local v3, count:I
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getMeasuredWidth()I

    #@9
    move-result v5

    #@a
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@c
    sub-int/2addr v5, v6

    #@d
    iget v6, p0, Landroid/view/View;->mPaddingRight:I

    #@f
    sub-int v2, v5, v6

    #@11
    .line 681
    .local v2, childWidth:I
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getMeasuredHeight()I

    #@14
    move-result v5

    #@15
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@17
    sub-int/2addr v5, v6

    #@18
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@1a
    sub-int v1, v5, v6

    #@1c
    .line 683
    .local v1, childHeight:I
    const/4 v4, 0x0

    #@1d
    .local v4, i:I
    :goto_1d
    if-ge v4, v3, :cond_31

    #@1f
    .line 684
    invoke-virtual {p0, v4}, Landroid/widget/AdapterViewAnimator;->getChildAt(I)Landroid/view/View;

    #@22
    move-result-object v0

    #@23
    .line 685
    .local v0, child:Landroid/view/View;
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@26
    move-result v5

    #@27
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2a
    move-result v6

    #@2b
    invoke-virtual {v0, v5, v6}, Landroid/view/View;->measure(II)V

    #@2e
    .line 683
    add-int/lit8 v4, v4, 0x1

    #@30
    goto :goto_1d

    #@31
    .line 688
    .end local v0           #child:Landroid/view/View;
    :cond_31
    return-void
.end method

.method private setDisplayedChild(IZ)V
    .registers 6
    .parameter "whichChild"
    .parameter "animate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 297
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@3
    if-eqz v1, :cond_26

    #@5
    .line 298
    iput p1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@7
    .line 299
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@a
    move-result v1

    #@b
    if-lt p1, v1, :cond_2e

    #@d
    .line 300
    iget-boolean v1, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@f
    if-eqz v1, :cond_27

    #@11
    move v1, v2

    #@12
    :goto_12
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@14
    .line 305
    :cond_14
    :goto_14
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getFocusedChild()Landroid/view/View;

    #@17
    move-result-object v1

    #@18
    if-eqz v1, :cond_3f

    #@1a
    const/4 v0, 0x1

    #@1b
    .line 307
    .local v0, hasFocus:Z
    :goto_1b
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@1d
    invoke-virtual {p0, v1, p2}, Landroid/widget/AdapterViewAnimator;->showOnly(IZ)V

    #@20
    .line 308
    if-eqz v0, :cond_26

    #@22
    .line 310
    const/4 v1, 0x2

    #@23
    invoke-virtual {p0, v1}, Landroid/widget/AdapterViewAnimator;->requestFocus(I)Z

    #@26
    .line 313
    .end local v0           #hasFocus:Z
    :cond_26
    return-void

    #@27
    .line 300
    :cond_27
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@2a
    move-result v1

    #@2b
    add-int/lit8 v1, v1, -0x1

    #@2d
    goto :goto_12

    #@2e
    .line 301
    :cond_2e
    if-gez p1, :cond_14

    #@30
    .line 302
    iget-boolean v1, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@32
    if-eqz v1, :cond_3d

    #@34
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@37
    move-result v1

    #@38
    add-int/lit8 v1, v1, -0x1

    #@3a
    :goto_3a
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@3c
    goto :goto_14

    #@3d
    :cond_3d
    move v1, v2

    #@3e
    goto :goto_3a

    #@3f
    :cond_3f
    move v0, v2

    #@40
    .line 305
    goto :goto_1b
.end method


# virtual methods
.method public advance()V
    .registers 1

    #@0
    .prologue
    .line 1070
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->showNext()V

    #@3
    .line 1071
    return-void
.end method

.method applyTransformForChildAtIndex(Landroid/view/View;I)V
    .registers 3
    .parameter "child"
    .parameter "relativeIndex"

    #@0
    .prologue
    .line 323
    return-void
.end method

.method cancelHandleClick()V
    .registers 3

    #@0
    .prologue
    .line 598
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 599
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_9

    #@6
    .line 600
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->hideTapFeedback(Landroid/view/View;)V

    #@9
    .line 602
    :cond_9
    const/4 v1, 0x0

    #@a
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@c
    .line 603
    return-void
.end method

.method checkForAndHandleDataChanged()V
    .registers 3

    #@0
    .prologue
    .line 735
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2
    .line 736
    .local v0, dataChanged:Z
    if-eqz v0, :cond_c

    #@4
    .line 737
    new-instance v1, Landroid/widget/AdapterViewAnimator$2;

    #@6
    invoke-direct {v1, p0}, Landroid/widget/AdapterViewAnimator$2;-><init>(Landroid/widget/AdapterViewAnimator;)V

    #@9
    invoke-virtual {p0, v1}, Landroid/widget/AdapterViewAnimator;->post(Ljava/lang/Runnable;)Z

    #@c
    .line 754
    :cond_c
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@f
    .line 755
    return-void
.end method

.method configureViewAnimator(II)V
    .registers 4
    .parameter "numVisibleViews"
    .parameter "activeOffset"

    #@0
    .prologue
    .line 242
    add-int/lit8 v0, p1, -0x1

    #@2
    if-le p2, v0, :cond_4

    #@4
    .line 245
    :cond_4
    iput p1, p0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@6
    .line 246
    iput p2, p0, Landroid/widget/AdapterViewAnimator;->mActiveOffset:I

    #@8
    .line 247
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@d
    .line 248
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@12
    .line 249
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->removeAllViewsInLayout()V

    #@15
    .line 250
    const/4 v0, 0x0

    #@16
    iput v0, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@18
    .line 251
    const/4 v0, -0x1

    #@19
    iput v0, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@1b
    .line 252
    return-void
.end method

.method createOrReuseLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;
    .registers 6
    .parameter "v"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 401
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v0

    #@5
    .line 402
    .local v0, currentLp:Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Landroid/view/ViewGroup$LayoutParams;

    #@7
    if-eqz v2, :cond_b

    #@9
    .line 403
    move-object v1, v0

    #@a
    .line 406
    :goto_a
    return-object v1

    #@b
    :cond_b
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@d
    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@10
    goto :goto_a
.end method

.method public deferNotifyDataSetChanged()V
    .registers 2

    #@0
    .prologue
    .line 1026
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AdapterViewAnimator;->mDeferNotifyDataSetChanged:Z

    #@3
    .line 1027
    return-void
.end method

.method public fyiWillBeAdvancedByHostKThx()V
    .registers 1

    #@0
    .prologue
    .line 1080
    return-void
.end method

.method public getAdapter()Landroid/widget/Adapter;
    .registers 2

    #@0
    .prologue
    .line 947
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@2
    return-object v0
.end method

.method public getBaseline()I
    .registers 2

    #@0
    .prologue
    .line 942
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_f

    #@6
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@d
    move-result v0

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-super {p0}, Landroid/widget/AdapterView;->getBaseline()I

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public getCurrentView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 852
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mActiveOffset:I

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getDefaultInAnimation()Landroid/animation/ObjectAnimator;
    .registers 5

    #@0
    .prologue
    .line 275
    const/4 v1, 0x0

    #@1
    const-string v2, "alpha"

    #@3
    const/4 v3, 0x2

    #@4
    new-array v3, v3, [F

    #@6
    fill-array-data v3, :array_14

    #@9
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@c
    move-result-object v0

    #@d
    .line 276
    .local v0, anim:Landroid/animation/ObjectAnimator;
    const-wide/16 v1, 0xc8

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@12
    .line 277
    return-object v0

    #@13
    .line 275
    nop

    #@14
    :array_14
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method getDefaultOutAnimation()Landroid/animation/ObjectAnimator;
    .registers 5

    #@0
    .prologue
    .line 281
    const/4 v1, 0x0

    #@1
    const-string v2, "alpha"

    #@3
    const/4 v3, 0x2

    #@4
    new-array v3, v3, [F

    #@6
    fill-array-data v3, :array_14

    #@9
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@c
    move-result-object v0

    #@d
    .line 282
    .local v0, anim:Landroid/animation/ObjectAnimator;
    const-wide/16 v1, 0xc8

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@12
    .line 283
    return-object v0

    #@13
    .line 281
    nop

    #@14
    :array_14
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public getDisplayedChild()I
    .registers 2

    #@0
    .prologue
    .line 329
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@2
    return v0
.end method

.method getFrameForChild()Landroid/widget/FrameLayout;
    .registers 3

    #@0
    .prologue
    .line 442
    new-instance v0, Landroid/widget/FrameLayout;

    #@2
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@7
    return-object v0
.end method

.method public getInAnimation()Landroid/animation/ObjectAnimator;
    .registers 2

    #@0
    .prologue
    .line 864
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mInAnimation:Landroid/animation/ObjectAnimator;

    #@2
    return-object v0
.end method

.method getNumActiveViews()I
    .registers 3

    #@0
    .prologue
    .line 371
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 372
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCount()I

    #@7
    move-result v0

    #@8
    add-int/lit8 v0, v0, 0x1

    #@a
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v0

    #@10
    .line 374
    :goto_10
    return v0

    #@11
    :cond_11
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@13
    goto :goto_10
.end method

.method public getOutAnimation()Landroid/animation/ObjectAnimator;
    .registers 2

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mOutAnimation:Landroid/animation/ObjectAnimator;

    #@2
    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1018
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mActiveOffset:I

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getViewAtRelativeIndex(I)Landroid/view/View;
    .registers 5
    .parameter "relativeIndex"

    #@0
    .prologue
    .line 361
    if-ltz p1, :cond_34

    #@2
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getNumActiveViews()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v1, v1, -0x1

    #@8
    if-gt p1, v1, :cond_34

    #@a
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@c
    if-eqz v1, :cond_34

    #@e
    .line 362
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@10
    add-int/2addr v1, p1

    #@11
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@14
    move-result v2

    #@15
    invoke-virtual {p0, v1, v2}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@18
    move-result v0

    #@19
    .line 363
    .local v0, i:I
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    if-eqz v1, :cond_34

    #@25
    .line 364
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@31
    iget-object v1, v1, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@33
    .line 367
    .end local v0           #i:I
    :goto_33
    return-object v1

    #@34
    :cond_34
    const/4 v1, 0x0

    #@35
    goto :goto_33
.end method

.method getWindowSize()I
    .registers 3

    #@0
    .prologue
    .line 379
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@2
    if-eqz v1, :cond_16

    #@4
    .line 380
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCount()I

    #@7
    move-result v0

    #@8
    .line 381
    .local v0, adapterCount:I
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getNumActiveViews()I

    #@b
    move-result v1

    #@c
    if-gt v0, v1, :cond_15

    #@e
    iget-boolean v1, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@10
    if-eqz v1, :cond_15

    #@12
    .line 382
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@14
    mul-int/2addr v0, v1

    #@15
    .line 387
    .end local v0           #adapterCount:I
    :cond_15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method hideTapFeedback(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 594
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    #@4
    .line 595
    return-void
.end method

.method modulo(II)I
    .registers 4
    .parameter "pos"
    .parameter "size"

    #@0
    .prologue
    .line 347
    if-lez p2, :cond_7

    #@2
    .line 348
    rem-int v0, p1, p2

    #@4
    add-int/2addr v0, p2

    #@5
    rem-int/2addr v0, p2

    #@6
    .line 350
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1084
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1085
    const-class v0, Landroid/widget/AdapterViewAnimator;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1086
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1090
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1091
    const-class v0, Landroid/widget/AdapterViewAnimator;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1092
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 13
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 759
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->checkForAndHandleDataChanged()V

    #@3
    .line 761
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getChildCount()I

    #@6
    move-result v2

    #@7
    .line 762
    .local v2, childCount:I
    const/4 v4, 0x0

    #@8
    .local v4, i:I
    :goto_8
    if-ge v4, v2, :cond_28

    #@a
    .line 763
    invoke-virtual {p0, v4}, Landroid/widget/AdapterViewAnimator;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 765
    .local v0, child:Landroid/view/View;
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@10
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@13
    move-result v6

    #@14
    add-int v3, v5, v6

    #@16
    .line 766
    .local v3, childRight:I
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@18
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@1b
    move-result v6

    #@1c
    add-int v1, v5, v6

    #@1e
    .line 768
    .local v1, childBottom:I
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@20
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@22
    invoke-virtual {v0, v5, v6, v3, v1}, Landroid/view/View;->layout(IIII)V

    #@25
    .line 762
    add-int/lit8 v4, v4, 0x1

    #@27
    goto :goto_8

    #@28
    .line 770
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childBottom:I
    .end local v3           #childRight:I
    :cond_28
    return-void
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v11, 0x100

    #@2
    const/4 v9, -0x1

    #@3
    const/high16 v10, -0x8000

    #@5
    const/4 v7, 0x0

    #@6
    .line 692
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v6

    #@a
    .line 693
    .local v6, widthSpecSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@d
    move-result v3

    #@e
    .line 694
    .local v3, heightSpecSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@11
    move-result v5

    #@12
    .line 695
    .local v5, widthSpecMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@15
    move-result v2

    #@16
    .line 697
    .local v2, heightSpecMode:I
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@18
    if-eq v8, v9, :cond_40

    #@1a
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@1c
    if-eq v8, v9, :cond_40

    #@1e
    const/4 v0, 0x1

    #@1f
    .line 702
    .local v0, haveChildRefSize:Z
    :goto_1f
    if-nez v2, :cond_44

    #@21
    .line 703
    if-eqz v0, :cond_42

    #@23
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@25
    iget v9, p0, Landroid/view/View;->mPaddingTop:I

    #@27
    add-int/2addr v8, v9

    #@28
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@2a
    add-int v3, v8, v9

    #@2c
    .line 716
    :cond_2c
    :goto_2c
    if-nez v5, :cond_59

    #@2e
    .line 717
    if-eqz v0, :cond_57

    #@30
    iget v7, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@32
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@34
    add-int/2addr v7, v8

    #@35
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@37
    add-int v6, v7, v8

    #@39
    .line 730
    :cond_39
    :goto_39
    invoke-virtual {p0, v6, v3}, Landroid/widget/AdapterViewAnimator;->setMeasuredDimension(II)V

    #@3c
    .line 731
    invoke-direct {p0}, Landroid/widget/AdapterViewAnimator;->measureChildren()V

    #@3f
    .line 732
    return-void

    #@40
    .end local v0           #haveChildRefSize:Z
    :cond_40
    move v0, v7

    #@41
    .line 697
    goto :goto_1f

    #@42
    .restart local v0       #haveChildRefSize:Z
    :cond_42
    move v3, v7

    #@43
    .line 703
    goto :goto_2c

    #@44
    .line 705
    :cond_44
    if-ne v2, v10, :cond_2c

    #@46
    .line 706
    if-eqz v0, :cond_2c

    #@48
    .line 707
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@4a
    iget v9, p0, Landroid/view/View;->mPaddingTop:I

    #@4c
    add-int/2addr v8, v9

    #@4d
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@4f
    add-int v1, v8, v9

    #@51
    .line 708
    .local v1, height:I
    if-le v1, v3, :cond_55

    #@53
    .line 709
    or-int/2addr v3, v11

    #@54
    goto :goto_2c

    #@55
    .line 711
    :cond_55
    move v3, v1

    #@56
    goto :goto_2c

    #@57
    .end local v1           #height:I
    :cond_57
    move v6, v7

    #@58
    .line 717
    goto :goto_39

    #@59
    .line 719
    :cond_59
    if-ne v2, v10, :cond_39

    #@5b
    .line 720
    if-eqz v0, :cond_39

    #@5d
    .line 721
    iget v7, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@5f
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@61
    add-int/2addr v7, v8

    #@62
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@64
    add-int v4, v7, v8

    #@66
    .line 722
    .local v4, width:I
    if-le v4, v6, :cond_6a

    #@68
    .line 723
    or-int/2addr v6, v11

    #@69
    goto :goto_39

    #@6a
    .line 725
    :cond_6a
    move v6, v4

    #@6b
    goto :goto_39
.end method

.method public onRemoteAdapterConnected()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1033
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@4
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@6
    if-eq v1, v2, :cond_24

    #@8
    .line 1034
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@a
    invoke-virtual {p0, v1}, Landroid/widget/AdapterViewAnimator;->setAdapter(Landroid/widget/Adapter;)V

    #@d
    .line 1036
    iget-boolean v1, p0, Landroid/widget/AdapterViewAnimator;->mDeferNotifyDataSetChanged:Z

    #@f
    if-eqz v1, :cond_18

    #@11
    .line 1037
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@13
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter;->notifyDataSetChanged()V

    #@16
    .line 1038
    iput-boolean v0, p0, Landroid/widget/AdapterViewAnimator;->mDeferNotifyDataSetChanged:Z

    #@18
    .line 1042
    :cond_18
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mRestoreWhichChild:I

    #@1a
    if-le v1, v3, :cond_23

    #@1c
    .line 1043
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mRestoreWhichChild:I

    #@1e
    invoke-direct {p0, v1, v0}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(IZ)V

    #@21
    .line 1044
    iput v3, p0, Landroid/widget/AdapterViewAnimator;->mRestoreWhichChild:I

    #@23
    .line 1051
    :cond_23
    :goto_23
    return v0

    #@24
    .line 1047
    :cond_24
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@26
    if-eqz v1, :cond_23

    #@28
    .line 1048
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2a
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter;->superNotifyDataSetChanged()V

    #@2d
    .line 1049
    const/4 v0, 0x1

    #@2e
    goto :goto_23
.end method

.method public onRemoteAdapterDisconnected()V
    .registers 1

    #@0
    .prologue
    .line 1063
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 825
    move-object v0, p1

    #@1
    check-cast v0, Landroid/widget/AdapterViewAnimator$SavedState;

    #@3
    .line 826
    .local v0, ss:Landroid/widget/AdapterViewAnimator$SavedState;
    invoke-virtual {v0}, Landroid/widget/AdapterViewAnimator$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 831
    iget v1, v0, Landroid/widget/AdapterViewAnimator$SavedState;->whichChild:I

    #@c
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@e
    .line 837
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@10
    if-eqz v1, :cond_1b

    #@12
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@14
    if-nez v1, :cond_1b

    #@16
    .line 838
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@18
    iput v1, p0, Landroid/widget/AdapterViewAnimator;->mRestoreWhichChild:I

    #@1a
    .line 842
    :goto_1a
    return-void

    #@1b
    .line 840
    :cond_1b
    iget v1, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@1d
    const/4 v2, 0x0

    #@1e
    invoke-direct {p0, v1, v2}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(IZ)V

    #@21
    goto :goto_1a
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 816
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v0

    #@4
    .line 817
    .local v0, superState:Landroid/os/Parcelable;
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@6
    if-eqz v1, :cond_d

    #@8
    .line 818
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@a
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter;->saveRemoteViewsCache()V

    #@d
    .line 820
    :cond_d
    new-instance v1, Landroid/widget/AdapterViewAnimator$SavedState;

    #@f
    iget v2, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@11
    invoke-direct {v1, v0, v2}, Landroid/widget/AdapterViewAnimator$SavedState;-><init>(Landroid/os/Parcelable;I)V

    #@14
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter "ev"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 616
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@6
    move-result v0

    #@7
    .line 617
    .local v0, action:I
    const/4 v1, 0x0

    #@8
    .line 618
    .local v1, handled:Z
    packed-switch v0, :pswitch_data_80

    #@b
    .line 675
    :cond_b
    :goto_b
    :pswitch_b
    return v1

    #@c
    .line 620
    :pswitch_c
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@f
    move-result-object v3

    #@10
    .line 621
    .local v3, v:Landroid/view/View;
    if-eqz v3, :cond_b

    #@12
    .line 622
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@15
    move-result v5

    #@16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@19
    move-result v6

    #@1a
    invoke-virtual {p0, v5, v6, v3, v9}, Landroid/widget/AdapterViewAnimator;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_b

    #@20
    .line 623
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@22
    if-nez v5, :cond_2b

    #@24
    .line 624
    new-instance v5, Landroid/widget/AdapterViewAnimator$CheckForTap;

    #@26
    invoke-direct {v5, p0}, Landroid/widget/AdapterViewAnimator$CheckForTap;-><init>(Landroid/widget/AdapterViewAnimator;)V

    #@29
    iput-object v5, p0, Landroid/widget/AdapterViewAnimator;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@2b
    .line 626
    :cond_2b
    iput v7, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@2d
    .line 627
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@2f
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@32
    move-result v6

    #@33
    int-to-long v6, v6

    #@34
    invoke-virtual {p0, v5, v6, v7}, Landroid/widget/AdapterViewAnimator;->postDelayed(Ljava/lang/Runnable;J)Z

    #@37
    goto :goto_b

    #@38
    .line 635
    .end local v3           #v:Landroid/view/View;
    :pswitch_38
    iget v5, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@3a
    if-ne v5, v7, :cond_70

    #@3c
    .line 636
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@3f
    move-result-object v3

    #@40
    .line 637
    .restart local v3       #v:Landroid/view/View;
    invoke-direct {p0, v3}, Landroid/widget/AdapterViewAnimator;->getMetaDataForChild(Landroid/view/View;)Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@43
    move-result-object v4

    #@44
    .line 638
    .local v4, viewData:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    if-eqz v3, :cond_70

    #@46
    .line 639
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@49
    move-result v5

    #@4a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@4d
    move-result v6

    #@4e
    invoke-virtual {p0, v5, v6, v3, v9}, Landroid/widget/AdapterViewAnimator;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@51
    move-result v5

    #@52
    if-eqz v5, :cond_70

    #@54
    .line 640
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getHandler()Landroid/os/Handler;

    #@57
    move-result-object v2

    #@58
    .line 641
    .local v2, handler:Landroid/os/Handler;
    if-eqz v2, :cond_5f

    #@5a
    .line 642
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@5c
    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@5f
    .line 644
    :cond_5f
    invoke-virtual {p0, v3}, Landroid/widget/AdapterViewAnimator;->showTapFeedback(Landroid/view/View;)V

    #@62
    .line 645
    new-instance v5, Landroid/widget/AdapterViewAnimator$1;

    #@64
    invoke-direct {v5, p0, v3, v4}, Landroid/widget/AdapterViewAnimator$1;-><init>(Landroid/widget/AdapterViewAnimator;Landroid/view/View;Landroid/widget/AdapterViewAnimator$ViewAndMetaData;)V

    #@67
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    #@6a
    move-result v6

    #@6b
    int-to-long v6, v6

    #@6c
    invoke-virtual {p0, v5, v6, v7}, Landroid/widget/AdapterViewAnimator;->postDelayed(Ljava/lang/Runnable;J)Z

    #@6f
    .line 660
    const/4 v1, 0x1

    #@70
    .line 664
    .end local v2           #handler:Landroid/os/Handler;
    .end local v3           #v:Landroid/view/View;
    .end local v4           #viewData:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    :cond_70
    iput v8, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@72
    goto :goto_b

    #@73
    .line 668
    :pswitch_73
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCurrentView()Landroid/view/View;

    #@76
    move-result-object v3

    #@77
    .line 669
    .restart local v3       #v:Landroid/view/View;
    if-eqz v3, :cond_7c

    #@79
    .line 670
    invoke-virtual {p0, v3}, Landroid/widget/AdapterViewAnimator;->hideTapFeedback(Landroid/view/View;)V

    #@7c
    .line 672
    :cond_7c
    iput v8, p0, Landroid/widget/AdapterViewAnimator;->mTouchMode:I

    #@7e
    goto :goto_b

    #@7f
    .line 618
    nop

    #@80
    :pswitch_data_80
    .packed-switch 0x0
        :pswitch_c
        :pswitch_38
        :pswitch_b
        :pswitch_73
        :pswitch_b
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method refreshChildren()V
    .registers 9

    #@0
    .prologue
    .line 410
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@2
    if-nez v5, :cond_5

    #@4
    .line 432
    :cond_4
    return-void

    #@5
    .line 411
    :cond_5
    iget v2, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@7
    .local v2, i:I
    :goto_7
    iget v5, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@9
    if-gt v2, v5, :cond_4

    #@b
    .line 412
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@e
    move-result v5

    #@f
    invoke-virtual {p0, v2, v5}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@12
    move-result v3

    #@13
    .line 414
    .local v3, index:I
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getCount()I

    #@16
    move-result v0

    #@17
    .line 416
    .local v0, adapterCount:I
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@19
    invoke-virtual {p0, v2, v0}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@1c
    move-result v6

    #@1d
    const/4 v7, 0x0

    #@1e
    invoke-interface {v5, v6, v7, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@21
    move-result-object v4

    #@22
    .line 418
    .local v4, updatedChild:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getImportantForAccessibility()I

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_2c

    #@28
    .line 419
    const/4 v5, 0x1

    #@29
    invoke-virtual {v4, v5}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@2c
    .line 422
    :cond_2c
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@2e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@35
    move-result v5

    #@36
    if-eqz v5, :cond_50

    #@38
    .line 423
    iget-object v5, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@3a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v5

    #@42
    check-cast v5, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@44
    iget-object v1, v5, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@46
    check-cast v1, Landroid/widget/FrameLayout;

    #@48
    .line 425
    .local v1, fl:Landroid/widget/FrameLayout;
    if-eqz v4, :cond_50

    #@4a
    .line 427
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    #@4d
    .line 428
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@50
    .line 411
    .end local v1           #fl:Landroid/widget/FrameLayout;
    :cond_50
    add-int/lit8 v2, v2, 0x1

    #@52
    goto :goto_7
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .registers 5
    .parameter "adapter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 952
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@3
    if-eqz v0, :cond_10

    #@5
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mDataSetObserver:Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 953
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@b
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mDataSetObserver:Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@d
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@10
    .line 956
    :cond_10
    iput-object p1, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@12
    .line 957
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->checkFocus()V

    #@15
    .line 959
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@17
    if-eqz v0, :cond_2f

    #@19
    .line 960
    new-instance v0, Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@1b
    invoke-direct {v0, p0}, Landroid/widget/AdapterView$AdapterDataSetObserver;-><init>(Landroid/widget/AdapterView;)V

    #@1e
    iput-object v0, p0, Landroid/widget/AdapterViewAnimator;->mDataSetObserver:Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@20
    .line 961
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@22
    iget-object v1, p0, Landroid/widget/AdapterViewAnimator;->mDataSetObserver:Landroid/widget/AdapterView$AdapterDataSetObserver;

    #@24
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@27
    .line 962
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@29
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    #@2c
    move-result v0

    #@2d
    iput v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2f
    .line 964
    :cond_2f
    const/4 v0, 0x1

    #@30
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->setFocusable(Z)V

    #@33
    .line 965
    iput v2, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@35
    .line 966
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@37
    invoke-virtual {p0, v0, v2}, Landroid/widget/AdapterViewAnimator;->showOnly(IZ)V

    #@3a
    .line 967
    return-void
.end method

.method public setAnimateFirstView(Z)V
    .registers 2
    .parameter "animate"

    #@0
    .prologue
    .line 937
    iput-boolean p1, p0, Landroid/widget/AdapterViewAnimator;->mAnimateFirstTime:Z

    #@2
    .line 938
    return-void
.end method

.method public setDisplayedChild(I)V
    .registers 3
    .parameter "whichChild"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 293
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(IZ)V

    #@4
    .line 294
    return-void
.end method

.method public setInAnimation(Landroid/animation/ObjectAnimator;)V
    .registers 2
    .parameter "inAnimation"

    #@0
    .prologue
    .line 876
    iput-object p1, p0, Landroid/widget/AdapterViewAnimator;->mInAnimation:Landroid/animation/ObjectAnimator;

    #@2
    .line 877
    return-void
.end method

.method public setInAnimation(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 913
    invoke-static {p1, p2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->setInAnimation(Landroid/animation/ObjectAnimator;)V

    #@9
    .line 914
    return-void
.end method

.method public setOutAnimation(Landroid/animation/ObjectAnimator;)V
    .registers 2
    .parameter "outAnimation"

    #@0
    .prologue
    .line 900
    iput-object p1, p0, Landroid/widget/AdapterViewAnimator;->mOutAnimation:Landroid/animation/ObjectAnimator;

    #@2
    .line 901
    return-void
.end method

.method public setOutAnimation(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 926
    invoke-static {p1, p2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->setOutAnimation(Landroid/animation/ObjectAnimator;)V

    #@9
    .line 927
    return-void
.end method

.method public setRemoteViewsAdapter(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 980
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2
    if-eqz v2, :cond_1b

    #@4
    .line 981
    new-instance v0, Landroid/content/Intent$FilterComparison;

    #@6
    invoke-direct {v0, p1}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@9
    .line 982
    .local v0, fcNew:Landroid/content/Intent$FilterComparison;
    new-instance v1, Landroid/content/Intent$FilterComparison;

    #@b
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@d
    invoke-virtual {v2}, Landroid/widget/RemoteViewsAdapter;->getRemoteViewsServiceIntent()Landroid/content/Intent;

    #@10
    move-result-object v2

    #@11
    invoke-direct {v1, v2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@14
    .line 984
    .local v1, fcOld:Landroid/content/Intent$FilterComparison;
    invoke-virtual {v0, v1}, Landroid/content/Intent$FilterComparison;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1b

    #@1a
    .line 994
    .end local v0           #fcNew:Landroid/content/Intent$FilterComparison;
    .end local v1           #fcOld:Landroid/content/Intent$FilterComparison;
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 988
    :cond_1b
    const/4 v2, 0x0

    #@1c
    iput-boolean v2, p0, Landroid/widget/AdapterViewAnimator;->mDeferNotifyDataSetChanged:Z

    #@1e
    .line 990
    new-instance v2, Landroid/widget/RemoteViewsAdapter;

    #@20
    invoke-virtual {p0}, Landroid/widget/AdapterViewAnimator;->getContext()Landroid/content/Context;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v2, v3, p1, p0}, Landroid/widget/RemoteViewsAdapter;-><init>(Landroid/content/Context;Landroid/content/Intent;Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;)V

    #@27
    iput-object v2, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@29
    .line 991
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2b
    invoke-virtual {v2}, Landroid/widget/RemoteViewsAdapter;->isDataReady()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_1a

    #@31
    .line 992
    iget-object v2, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@33
    invoke-virtual {p0, v2}, Landroid/widget/AdapterViewAnimator;->setAdapter(Landroid/widget/Adapter;)V

    #@36
    goto :goto_1a
.end method

.method public setRemoteViewsOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 1006
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1007
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViewsAdapter;->setRemoteViewsOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V

    #@9
    .line 1009
    :cond_9
    return-void
.end method

.method public setSelection(I)V
    .registers 2
    .parameter "position"

    #@0
    .prologue
    .line 1013
    invoke-virtual {p0, p1}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(I)V

    #@3
    .line 1014
    return-void
.end method

.method public showNext()V
    .registers 2

    #@0
    .prologue
    .line 336
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(I)V

    #@7
    .line 337
    return-void
.end method

.method showOnly(IZ)V
    .registers 37
    .parameter "childIndex"
    .parameter "animate"

    #@0
    .prologue
    .line 456
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@4
    if-nez v4, :cond_7

    #@6
    .line 573
    :cond_6
    :goto_6
    return-void

    #@7
    .line 457
    :cond_7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getCount()I

    #@a
    move-result v11

    #@b
    .line 458
    .local v11, adapterCount:I
    if-eqz v11, :cond_6

    #@d
    .line 460
    const/4 v14, 0x0

    #@e
    .local v14, i:I
    :goto_e
    move-object/from16 v0, p0

    #@10
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v4

    #@16
    if-ge v14, v4, :cond_5f

    #@18
    .line 461
    move-object/from16 v0, p0

    #@1a
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget-object v5, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v4

    #@28
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@2a
    iget-object v0, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@2c
    move-object/from16 v30, v0

    #@2e
    .line 462
    .local v30, viewToRemove:Landroid/view/View;
    move-object/from16 v0, p0

    #@30
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@32
    move-object/from16 v0, p0

    #@34
    iget-object v5, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 463
    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->clearAnimation()V

    #@40
    .line 464
    move-object/from16 v0, v30

    #@42
    instance-of v4, v0, Landroid/view/ViewGroup;

    #@44
    if-eqz v4, :cond_4d

    #@46
    move-object/from16 v28, v30

    #@48
    .line 465
    check-cast v28, Landroid/view/ViewGroup;

    #@4a
    .line 466
    .local v28, vg:Landroid/view/ViewGroup;
    invoke-virtual/range {v28 .. v28}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    #@4d
    .line 470
    .end local v28           #vg:Landroid/view/ViewGroup;
    :cond_4d
    const/4 v4, -0x1

    #@4e
    move-object/from16 v0, p0

    #@50
    move-object/from16 v1, v30

    #@52
    invoke-virtual {v0, v1, v4}, Landroid/widget/AdapterViewAnimator;->applyTransformForChildAtIndex(Landroid/view/View;I)V

    #@55
    .line 472
    move-object/from16 v0, p0

    #@57
    move-object/from16 v1, v30

    #@59
    invoke-virtual {v0, v1}, Landroid/widget/AdapterViewAnimator;->removeViewInLayout(Landroid/view/View;)V

    #@5c
    .line 460
    add-int/lit8 v14, v14, 0x1

    #@5e
    goto :goto_e

    #@5f
    .line 474
    .end local v30           #viewToRemove:Landroid/view/View;
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@63
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@66
    .line 475
    move-object/from16 v0, p0

    #@68
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mActiveOffset:I

    #@6a
    sub-int v22, p1, v4

    #@6c
    .line 476
    .local v22, newWindowStartUnbounded:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getNumActiveViews()I

    #@6f
    move-result v4

    #@70
    add-int v4, v4, v22

    #@72
    add-int/lit8 v20, v4, -0x1

    #@74
    .line 477
    .local v20, newWindowEndUnbounded:I
    const/4 v4, 0x0

    #@75
    move/from16 v0, v22

    #@77
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    #@7a
    move-result v21

    #@7b
    .line 478
    .local v21, newWindowStart:I
    add-int/lit8 v4, v11, -0x1

    #@7d
    move/from16 v0, v20

    #@7f
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    #@82
    move-result v19

    #@83
    .line 480
    .local v19, newWindowEnd:I
    move-object/from16 v0, p0

    #@85
    iget-boolean v4, v0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@87
    if-eqz v4, :cond_8d

    #@89
    .line 481
    move/from16 v21, v22

    #@8b
    .line 482
    move/from16 v19, v20

    #@8d
    .line 484
    :cond_8d
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@90
    move-result v4

    #@91
    move-object/from16 v0, p0

    #@93
    move/from16 v1, v21

    #@95
    invoke-virtual {v0, v1, v4}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@98
    move-result v26

    #@99
    .line 485
    .local v26, rangeStart:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@9c
    move-result v4

    #@9d
    move-object/from16 v0, p0

    #@9f
    move/from16 v1, v19

    #@a1
    invoke-virtual {v0, v1, v4}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@a4
    move-result v25

    #@a5
    .line 487
    .local v25, rangeEnd:I
    const/16 v31, 0x0

    #@a7
    .line 488
    .local v31, wrap:Z
    move/from16 v0, v26

    #@a9
    move/from16 v1, v25

    #@ab
    if-le v0, v1, :cond_af

    #@ad
    .line 489
    const/16 v31, 0x1

    #@af
    .line 496
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@b3
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@b6
    move-result-object v4

    #@b7
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@ba
    move-result-object v15

    #@bb
    .local v15, i$:Ljava/util/Iterator;
    :cond_bb
    :goto_bb
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@be
    move-result v4

    #@bf
    if-eqz v4, :cond_12a

    #@c1
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c4
    move-result-object v17

    #@c5
    check-cast v17, Ljava/lang/Integer;

    #@c7
    .line 497
    .local v17, index:Ljava/lang/Integer;
    const/16 v27, 0x0

    #@c9
    .line 498
    .local v27, remove:Z
    if-nez v31, :cond_115

    #@cb
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@ce
    move-result v4

    #@cf
    move/from16 v0, v26

    #@d1
    if-lt v4, v0, :cond_db

    #@d3
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@d6
    move-result v4

    #@d7
    move/from16 v0, v25

    #@d9
    if-le v4, v0, :cond_115

    #@db
    .line 499
    :cond_db
    const/16 v27, 0x1

    #@dd
    .line 504
    :cond_dd
    :goto_dd
    if-eqz v27, :cond_bb

    #@df
    .line 505
    move-object/from16 v0, p0

    #@e1
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@e3
    move-object/from16 v0, v17

    #@e5
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e8
    move-result-object v4

    #@e9
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@eb
    iget-object v0, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@ed
    move-object/from16 v24, v0

    #@ef
    .line 506
    .local v24, previousView:Landroid/view/View;
    move-object/from16 v0, p0

    #@f1
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@f3
    move-object/from16 v0, v17

    #@f5
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f8
    move-result-object v4

    #@f9
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@fb
    iget v0, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->relativeIndex:I

    #@fd
    move/from16 v23, v0

    #@ff
    .line 508
    .local v23, oldRelativeIndex:I
    move-object/from16 v0, p0

    #@101
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@103
    move-object/from16 v0, v17

    #@105
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@108
    .line 509
    const/4 v4, -0x1

    #@109
    move-object/from16 v0, p0

    #@10b
    move/from16 v1, v23

    #@10d
    move-object/from16 v2, v24

    #@10f
    move/from16 v3, p2

    #@111
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/AdapterViewAnimator;->transformViewForTransition(IILandroid/view/View;Z)V

    #@114
    goto :goto_bb

    #@115
    .line 500
    .end local v23           #oldRelativeIndex:I
    .end local v24           #previousView:Landroid/view/View;
    :cond_115
    if-eqz v31, :cond_dd

    #@117
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@11a
    move-result v4

    #@11b
    move/from16 v0, v25

    #@11d
    if-le v4, v0, :cond_dd

    #@11f
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@122
    move-result v4

    #@123
    move/from16 v0, v26

    #@125
    if-ge v4, v0, :cond_dd

    #@127
    .line 501
    const/16 v27, 0x1

    #@129
    goto :goto_dd

    #@12a
    .line 514
    .end local v17           #index:Ljava/lang/Integer;
    .end local v27           #remove:Z
    :cond_12a
    move-object/from16 v0, p0

    #@12c
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@12e
    move/from16 v0, v21

    #@130
    if-ne v0, v4, :cond_142

    #@132
    move-object/from16 v0, p0

    #@134
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@136
    move/from16 v0, v19

    #@138
    if-ne v0, v4, :cond_142

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@13e
    move/from16 v0, v22

    #@140
    if-eq v0, v4, :cond_267

    #@142
    .line 517
    :cond_142
    move/from16 v14, v21

    #@144
    :goto_144
    move/from16 v0, v19

    #@146
    if-gt v14, v0, :cond_234

    #@148
    .line 519
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getWindowSize()I

    #@14b
    move-result v4

    #@14c
    move-object/from16 v0, p0

    #@14e
    invoke-virtual {v0, v14, v4}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@151
    move-result v17

    #@152
    .line 521
    .local v17, index:I
    move-object/from16 v0, p0

    #@154
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@156
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@159
    move-result-object v5

    #@15a
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@15d
    move-result v4

    #@15e
    if-eqz v4, :cond_1df

    #@160
    .line 522
    move-object/from16 v0, p0

    #@162
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@164
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@167
    move-result-object v5

    #@168
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16b
    move-result-object v4

    #@16c
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@16e
    iget v0, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->relativeIndex:I

    #@170
    move/from16 v23, v0

    #@172
    .line 526
    .restart local v23       #oldRelativeIndex:I
    :goto_172
    sub-int v7, v14, v22

    #@174
    .line 531
    .local v7, newRelativeIndex:I
    move-object/from16 v0, p0

    #@176
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@178
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17b
    move-result-object v5

    #@17c
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@17f
    move-result v4

    #@180
    if-eqz v4, :cond_1e2

    #@182
    move-object/from16 v0, p0

    #@184
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mPreviousViews:Ljava/util/ArrayList;

    #@186
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@189
    move-result-object v5

    #@18a
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@18d
    move-result v4

    #@18e
    if-nez v4, :cond_1e2

    #@190
    const/16 v16, 0x1

    #@192
    .line 533
    .local v16, inOldRange:Z
    :goto_192
    if-eqz v16, :cond_1e5

    #@194
    .line 534
    move-object/from16 v0, p0

    #@196
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@198
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19b
    move-result-object v5

    #@19c
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19f
    move-result-object v4

    #@1a0
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@1a2
    iget-object v0, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@1a4
    move-object/from16 v29, v0

    #@1a6
    .line 535
    .local v29, view:Landroid/view/View;
    move-object/from16 v0, p0

    #@1a8
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@1aa
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ad
    move-result-object v5

    #@1ae
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b1
    move-result-object v4

    #@1b2
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@1b4
    iput v7, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->relativeIndex:I

    #@1b6
    .line 536
    move-object/from16 v0, p0

    #@1b8
    move-object/from16 v1, v29

    #@1ba
    invoke-virtual {v0, v1, v7}, Landroid/widget/AdapterViewAnimator;->applyTransformForChildAtIndex(Landroid/view/View;I)V

    #@1bd
    .line 537
    move-object/from16 v0, p0

    #@1bf
    move/from16 v1, v23

    #@1c1
    move-object/from16 v2, v29

    #@1c3
    move/from16 v3, p2

    #@1c5
    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/widget/AdapterViewAnimator;->transformViewForTransition(IILandroid/view/View;Z)V

    #@1c8
    .line 560
    .end local v29           #view:Landroid/view/View;
    :goto_1c8
    move-object/from16 v0, p0

    #@1ca
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@1cc
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1cf
    move-result-object v5

    #@1d0
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d3
    move-result-object v4

    #@1d4
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@1d6
    iget-object v4, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@1d8
    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    #@1db
    .line 517
    add-int/lit8 v14, v14, 0x1

    #@1dd
    goto/16 :goto_144

    #@1df
    .line 524
    .end local v7           #newRelativeIndex:I
    .end local v16           #inOldRange:Z
    .end local v23           #oldRelativeIndex:I
    :cond_1df
    const/16 v23, -0x1

    #@1e1
    .restart local v23       #oldRelativeIndex:I
    goto :goto_172

    #@1e2
    .line 531
    .restart local v7       #newRelativeIndex:I
    :cond_1e2
    const/16 v16, 0x0

    #@1e4
    goto :goto_192

    #@1e5
    .line 542
    .restart local v16       #inOldRange:Z
    :cond_1e5
    move-object/from16 v0, p0

    #@1e7
    invoke-virtual {v0, v14, v11}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@1ea
    move-result v8

    #@1eb
    .line 543
    .local v8, adapterPosition:I
    move-object/from16 v0, p0

    #@1ed
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@1ef
    const/4 v5, 0x0

    #@1f0
    move-object/from16 v0, p0

    #@1f2
    invoke-interface {v4, v8, v5, v0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@1f5
    move-result-object v18

    #@1f6
    .line 544
    .local v18, newView:Landroid/view/View;
    move-object/from16 v0, p0

    #@1f8
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@1fa
    invoke-interface {v4, v8}, Landroid/widget/Adapter;->getItemId(I)J

    #@1fd
    move-result-wide v9

    #@1fe
    .line 548
    .local v9, itemId:J
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->getFrameForChild()Landroid/widget/FrameLayout;

    #@201
    move-result-object v6

    #@202
    .line 551
    .local v6, fl:Landroid/widget/FrameLayout;
    if-eqz v18, :cond_209

    #@204
    .line 552
    move-object/from16 v0, v18

    #@206
    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@209
    .line 554
    :cond_209
    move-object/from16 v0, p0

    #@20b
    iget-object v0, v0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@20d
    move-object/from16 v32, v0

    #@20f
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@212
    move-result-object v33

    #@213
    new-instance v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@215
    move-object/from16 v5, p0

    #@217
    invoke-direct/range {v4 .. v10}, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;-><init>(Landroid/widget/AdapterViewAnimator;Landroid/view/View;IIJ)V

    #@21a
    move-object/from16 v0, v32

    #@21c
    move-object/from16 v1, v33

    #@21e
    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@221
    .line 556
    move-object/from16 v0, p0

    #@223
    invoke-direct {v0, v6}, Landroid/widget/AdapterViewAnimator;->addChild(Landroid/view/View;)V

    #@226
    .line 557
    move-object/from16 v0, p0

    #@228
    invoke-virtual {v0, v6, v7}, Landroid/widget/AdapterViewAnimator;->applyTransformForChildAtIndex(Landroid/view/View;I)V

    #@22b
    .line 558
    const/4 v4, -0x1

    #@22c
    move-object/from16 v0, p0

    #@22e
    move/from16 v1, p2

    #@230
    invoke-virtual {v0, v4, v7, v6, v1}, Landroid/widget/AdapterViewAnimator;->transformViewForTransition(IILandroid/view/View;Z)V

    #@233
    goto :goto_1c8

    #@234
    .line 562
    .end local v6           #fl:Landroid/widget/FrameLayout;
    .end local v7           #newRelativeIndex:I
    .end local v8           #adapterPosition:I
    .end local v9           #itemId:J
    .end local v16           #inOldRange:Z
    .end local v17           #index:I
    .end local v18           #newView:Landroid/view/View;
    .end local v23           #oldRelativeIndex:I
    :cond_234
    move/from16 v0, v21

    #@236
    move-object/from16 v1, p0

    #@238
    iput v0, v1, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@23a
    .line 563
    move/from16 v0, v19

    #@23c
    move-object/from16 v1, p0

    #@23e
    iput v0, v1, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@240
    .line 564
    move/from16 v0, v22

    #@242
    move-object/from16 v1, p0

    #@244
    iput v0, v1, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@246
    .line 565
    move-object/from16 v0, p0

    #@248
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@24a
    if-eqz v4, :cond_267

    #@24c
    .line 566
    move-object/from16 v0, p0

    #@24e
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@250
    move-object/from16 v0, p0

    #@252
    invoke-virtual {v0, v4, v11}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@255
    move-result v13

    #@256
    .line 567
    .local v13, adapterStart:I
    move-object/from16 v0, p0

    #@258
    iget v4, v0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@25a
    move-object/from16 v0, p0

    #@25c
    invoke-virtual {v0, v4, v11}, Landroid/widget/AdapterViewAnimator;->modulo(II)I

    #@25f
    move-result v12

    #@260
    .line 568
    .local v12, adapterEnd:I
    move-object/from16 v0, p0

    #@262
    iget-object v4, v0, Landroid/widget/AdapterViewAnimator;->mRemoteViewsAdapter:Landroid/widget/RemoteViewsAdapter;

    #@264
    invoke-virtual {v4, v13, v12}, Landroid/widget/RemoteViewsAdapter;->setVisibleRangeHint(II)V

    #@267
    .line 571
    .end local v12           #adapterEnd:I
    .end local v13           #adapterStart:I
    :cond_267
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->requestLayout()V

    #@26a
    .line 572
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AdapterViewAnimator;->invalidate()V

    #@26d
    goto/16 :goto_6
.end method

.method public showPrevious()V
    .registers 2

    #@0
    .prologue
    .line 343
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/AdapterViewAnimator;->setDisplayedChild(I)V

    #@7
    .line 344
    return-void
.end method

.method showTapFeedback(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 590
    const/4 v0, 0x1

    #@1
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    #@4
    .line 591
    return-void
.end method

.method transformViewForTransition(IILandroid/view/View;Z)V
    .registers 6
    .parameter "fromIndex"
    .parameter "toIndex"
    .parameter "view"
    .parameter "animate"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 265
    if-ne p1, v0, :cond_e

    #@3
    .line 266
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mInAnimation:Landroid/animation/ObjectAnimator;

    #@5
    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    #@8
    .line 267
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mInAnimation:Landroid/animation/ObjectAnimator;

    #@a
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@d
    .line 272
    :cond_d
    :goto_d
    return-void

    #@e
    .line 268
    :cond_e
    if-ne p2, v0, :cond_d

    #@10
    .line 269
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mOutAnimation:Landroid/animation/ObjectAnimator;

    #@12
    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    #@15
    .line 270
    iget-object v0, p0, Landroid/widget/AdapterViewAnimator;->mOutAnimation:Landroid/animation/ObjectAnimator;

    #@17
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@1a
    goto :goto_d
.end method
