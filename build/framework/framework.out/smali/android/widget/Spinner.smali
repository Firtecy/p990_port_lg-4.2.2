.class public Landroid/widget/Spinner;
.super Landroid/widget/AbsSpinner;
.source "Spinner.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Spinner$1;,
        Landroid/widget/Spinner$DropdownPopup;,
        Landroid/widget/Spinner$DialogPopup;,
        Landroid/widget/Spinner$SpinnerPopup;,
        Landroid/widget/Spinner$DropDownAdapter;
    }
.end annotation


# static fields
.field private static final MAX_ITEMS_MEASURED:I = 0xf

.field public static final MODE_DIALOG:I = 0x0

.field public static final MODE_DROPDOWN:I = 0x1

.field private static final MODE_THEME:I = -0x1

.field private static final TAG:Ljava/lang/String; = "Spinner"


# instance fields
.field private mDisableChildrenWhenDisabled:Z

.field mDropDownWidth:I

.field private mGravity:I

.field private mPopup:Landroid/widget/Spinner$SpinnerPopup;

.field private mTempAdapter:Landroid/widget/Spinner$DropDownAdapter;

.field private mTempRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 95
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "mode"

    #@0
    .prologue
    .line 111
    const/4 v0, 0x0

    #@1
    const v1, 0x1010081

    #@4
    invoke-direct {p0, p1, v0, v1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@7
    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 122
    const v0, 0x1010081

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 138
    const/4 v0, -0x1

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@4
    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 13
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"
    .parameter "mode"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 159
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 86
    new-instance v4, Landroid/graphics/Rect;

    #@7
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v4, p0, Landroid/widget/Spinner;->mTempRect:Landroid/graphics/Rect;

    #@c
    .line 161
    sget-object v4, Lcom/android/internal/R$styleable;->Spinner:[I

    #@e
    invoke-virtual {p1, p2, v4, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@11
    move-result-object v0

    #@12
    .line 164
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, -0x1

    #@13
    if-ne p4, v4, :cond_1a

    #@15
    .line 165
    const/4 v4, 0x7

    #@16
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@19
    move-result p4

    #@1a
    .line 168
    :cond_1a
    packed-switch p4, :pswitch_data_7c

    #@1d
    .line 199
    :goto_1d
    const/16 v4, 0x11

    #@1f
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@22
    move-result v4

    #@23
    iput v4, p0, Landroid/widget/Spinner;->mGravity:I

    #@25
    .line 201
    iget-object v4, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@27
    const/4 v5, 0x3

    #@28
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-interface {v4, v5}, Landroid/widget/Spinner$SpinnerPopup;->setPromptText(Ljava/lang/CharSequence;)V

    #@2f
    .line 203
    const/16 v4, 0x9

    #@31
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@34
    move-result v4

    #@35
    iput-boolean v4, p0, Landroid/widget/Spinner;->mDisableChildrenWhenDisabled:Z

    #@37
    .line 206
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3a
    .line 210
    iget-object v4, p0, Landroid/widget/Spinner;->mTempAdapter:Landroid/widget/Spinner$DropDownAdapter;

    #@3c
    if-eqz v4, :cond_47

    #@3e
    .line 211
    iget-object v4, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@40
    iget-object v5, p0, Landroid/widget/Spinner;->mTempAdapter:Landroid/widget/Spinner$DropDownAdapter;

    #@42
    invoke-interface {v4, v5}, Landroid/widget/Spinner$SpinnerPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    #@45
    .line 212
    iput-object v7, p0, Landroid/widget/Spinner;->mTempAdapter:Landroid/widget/Spinner$DropDownAdapter;

    #@47
    .line 214
    :cond_47
    return-void

    #@48
    .line 170
    :pswitch_48
    new-instance v4, Landroid/widget/Spinner$DialogPopup;

    #@4a
    invoke-direct {v4, p0, v7}, Landroid/widget/Spinner$DialogPopup;-><init>(Landroid/widget/Spinner;Landroid/widget/Spinner$1;)V

    #@4d
    iput-object v4, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@4f
    goto :goto_1d

    #@50
    .line 175
    :pswitch_50
    new-instance v2, Landroid/widget/Spinner$DropdownPopup;

    #@52
    invoke-direct {v2, p0, p1, p2, p3}, Landroid/widget/Spinner$DropdownPopup;-><init>(Landroid/widget/Spinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@55
    .line 177
    .local v2, popup:Landroid/widget/Spinner$DropdownPopup;
    const/4 v4, 0x4

    #@56
    const/4 v5, -0x2

    #@57
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@5a
    move-result v4

    #@5b
    iput v4, p0, Landroid/widget/Spinner;->mDropDownWidth:I

    #@5d
    .line 180
    const/4 v4, 0x2

    #@5e
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v2, v4}, Landroid/widget/Spinner$DropdownPopup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@65
    .line 182
    const/4 v4, 0x6

    #@66
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@69
    move-result v3

    #@6a
    .line 184
    .local v3, verticalOffset:I
    if-eqz v3, :cond_6f

    #@6c
    .line 185
    invoke-virtual {v2, v3}, Landroid/widget/Spinner$DropdownPopup;->setVerticalOffset(I)V

    #@6f
    .line 188
    :cond_6f
    const/4 v4, 0x5

    #@70
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@73
    move-result v1

    #@74
    .line 190
    .local v1, horizontalOffset:I
    if-eqz v1, :cond_79

    #@76
    .line 191
    invoke-virtual {v2, v1}, Landroid/widget/Spinner$DropdownPopup;->setHorizontalOffset(I)V

    #@79
    .line 194
    :cond_79
    iput-object v2, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@7b
    goto :goto_1d

    #@7c
    .line 168
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_48
        :pswitch_50
    .end packed-switch
.end method

.method static synthetic access$200(Landroid/widget/Spinner;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/widget/Spinner;->mTempRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/Spinner;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/Spinner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/widget/Spinner;->isVisibleToUser()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Landroid/widget/Spinner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/widget/Spinner;->isVisibleToUser()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private makeAndAddView(I)Landroid/view/View;
    .registers 6
    .parameter "position"

    #@0
    .prologue
    .line 536
    iget-boolean v2, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2
    if-nez v2, :cond_11

    #@4
    .line 537
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@6
    invoke-virtual {v2, p1}, Landroid/widget/AbsSpinner$RecycleBin;->get(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 538
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_11

    #@c
    .line 540
    invoke-direct {p0, v0}, Landroid/widget/Spinner;->setUpChild(Landroid/view/View;)V

    #@f
    move-object v1, v0

    #@10
    .line 552
    .end local v0           #child:Landroid/view/View;
    .local v1, child:Landroid/view/View;
    :goto_10
    return-object v1

    #@11
    .line 547
    .end local v1           #child:Landroid/view/View;
    :cond_11
    iget-object v2, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@13
    const/4 v3, 0x0

    #@14
    invoke-interface {v2, p1, v3, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@17
    move-result-object v0

    #@18
    .line 550
    .restart local v0       #child:Landroid/view/View;
    invoke-direct {p0, v0}, Landroid/widget/Spinner;->setUpChild(Landroid/view/View;)V

    #@1b
    move-object v1, v0

    #@1c
    .line 552
    .end local v0           #child:Landroid/view/View;
    .restart local v1       #child:Landroid/view/View;
    goto :goto_10
.end method

.method private setUpChild(Landroid/view/View;)V
    .registers 13
    .parameter "child"

    #@0
    .prologue
    .line 565
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v6

    #@4
    .line 566
    .local v6, lp:Landroid/view/ViewGroup$LayoutParams;
    if-nez v6, :cond_a

    #@6
    .line 567
    invoke-virtual {p0}, Landroid/widget/Spinner;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9
    move-result-object v6

    #@a
    .line 570
    :cond_a
    const/4 v8, 0x0

    #@b
    invoke-virtual {p0, p1, v8, v6}, Landroid/widget/Spinner;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    #@e
    .line 572
    invoke-virtual {p0}, Landroid/widget/Spinner;->hasFocus()Z

    #@11
    move-result v8

    #@12
    invoke-virtual {p1, v8}, Landroid/view/View;->setSelected(Z)V

    #@15
    .line 573
    iget-boolean v8, p0, Landroid/widget/Spinner;->mDisableChildrenWhenDisabled:Z

    #@17
    if-eqz v8, :cond_20

    #@19
    .line 574
    invoke-virtual {p0}, Landroid/widget/Spinner;->isEnabled()Z

    #@1c
    move-result v8

    #@1d
    invoke-virtual {p1, v8}, Landroid/view/View;->setEnabled(Z)V

    #@20
    .line 578
    :cond_20
    iget v8, p0, Landroid/widget/AbsSpinner;->mHeightMeasureSpec:I

    #@22
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@24
    iget v9, v9, Landroid/graphics/Rect;->top:I

    #@26
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@28
    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    #@2a
    add-int/2addr v9, v10

    #@2b
    iget v10, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2d
    invoke-static {v8, v9, v10}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@30
    move-result v1

    #@31
    .line 580
    .local v1, childHeightSpec:I
    iget v8, p0, Landroid/widget/AbsSpinner;->mWidthMeasureSpec:I

    #@33
    iget-object v9, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@35
    iget v9, v9, Landroid/graphics/Rect;->left:I

    #@37
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@39
    iget v10, v10, Landroid/graphics/Rect;->right:I

    #@3b
    add-int/2addr v9, v10

    #@3c
    iget v10, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3e
    invoke-static {v8, v9, v10}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@41
    move-result v5

    #@42
    .line 584
    .local v5, childWidthSpec:I
    invoke-virtual {p1, v5, v1}, Landroid/view/View;->measure(II)V

    #@45
    .line 590
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@47
    iget v8, v8, Landroid/graphics/Rect;->top:I

    #@49
    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredHeight()I

    #@4c
    move-result v9

    #@4d
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@4f
    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    #@51
    sub-int/2addr v9, v10

    #@52
    iget-object v10, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@54
    iget v10, v10, Landroid/graphics/Rect;->top:I

    #@56
    sub-int/2addr v9, v10

    #@57
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@5a
    move-result v10

    #@5b
    sub-int/2addr v9, v10

    #@5c
    div-int/lit8 v9, v9, 0x2

    #@5e
    add-int v4, v8, v9

    #@60
    .line 593
    .local v4, childTop:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@63
    move-result v8

    #@64
    add-int v0, v4, v8

    #@66
    .line 595
    .local v0, childBottom:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@69
    move-result v7

    #@6a
    .line 596
    .local v7, width:I
    const/4 v2, 0x0

    #@6b
    .line 597
    .local v2, childLeft:I
    add-int v3, v2, v7

    #@6d
    .line 599
    .local v3, childRight:I
    invoke-virtual {p1, v2, v4, v3, v0}, Landroid/view/View;->layout(IIII)V

    #@70
    .line 600
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .registers 6

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 392
    const/4 v0, 0x0

    #@3
    .line 394
    .local v0, child:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/Spinner;->getChildCount()I

    #@6
    move-result v3

    #@7
    if-lez v3, :cond_1b

    #@9
    .line 395
    invoke-virtual {p0, v4}, Landroid/widget/Spinner;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 402
    :cond_d
    :goto_d
    if-eqz v0, :cond_1a

    #@f
    .line 403
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    #@12
    move-result v1

    #@13
    .line 404
    .local v1, childBaseline:I
    if-ltz v1, :cond_1a

    #@15
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@18
    move-result v2

    #@19
    add-int/2addr v2, v1

    #@1a
    .line 406
    .end local v1           #childBaseline:I
    :cond_1a
    return v2

    #@1b
    .line 396
    :cond_1b
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@1d
    if-eqz v3, :cond_d

    #@1f
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mAdapter:Landroid/widget/SpinnerAdapter;

    #@21
    invoke-interface {v3}, Landroid/widget/SpinnerAdapter;->getCount()I

    #@24
    move-result v3

    #@25
    if-lez v3, :cond_d

    #@27
    .line 397
    invoke-direct {p0, v4}, Landroid/widget/Spinner;->makeAndAddView(I)Landroid/view/View;

    #@2a
    move-result-object v0

    #@2b
    .line 398
    iget-object v3, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@2d
    invoke-virtual {v3, v4, v0}, Landroid/widget/AbsSpinner$RecycleBin;->put(ILandroid/view/View;)V

    #@30
    .line 399
    invoke-virtual {p0}, Landroid/widget/Spinner;->removeAllViewsInLayout()V

    #@33
    goto :goto_d
.end method

.method public getDropDownHorizontalOffset()I
    .registers 2

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->getHorizontalOffset()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownVerticalOffset()I
    .registers 2

    #@0
    .prologue
    .line 277
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->getVerticalOffset()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDropDownWidth()I
    .registers 2

    #@0
    .prologue
    .line 337
    iget v0, p0, Landroid/widget/Spinner;->mDropDownWidth:I

    #@2
    return v0
.end method

.method public getGravity()I
    .registers 2

    #@0
    .prologue
    .line 376
    iget v0, p0, Landroid/widget/Spinner;->mGravity:I

    #@2
    return v0
.end method

.method public getPopupBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 253
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPrompt()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 654
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->getHintText()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method layout(IZ)V
    .registers 13
    .parameter "delta"
    .parameter "animate"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 472
    iget-object v7, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@3
    iget v1, v7, Landroid/graphics/Rect;->left:I

    #@5
    .line 473
    .local v1, childrenLeft:I
    iget v7, p0, Landroid/view/View;->mRight:I

    #@7
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@9
    sub-int/2addr v7, v8

    #@a
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@c
    iget v8, v8, Landroid/graphics/Rect;->left:I

    #@e
    sub-int/2addr v7, v8

    #@f
    iget-object v8, p0, Landroid/widget/AbsSpinner;->mSpinnerPadding:Landroid/graphics/Rect;

    #@11
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@13
    sub-int v2, v7, v8

    #@15
    .line 475
    .local v2, childrenWidth:I
    iget-boolean v7, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@17
    if-eqz v7, :cond_1c

    #@19
    .line 476
    invoke-virtual {p0}, Landroid/widget/Spinner;->handleDataChanged()V

    #@1c
    .line 480
    :cond_1c
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1e
    if-nez v7, :cond_24

    #@20
    .line 481
    invoke-virtual {p0}, Landroid/widget/Spinner;->resetList()V

    #@23
    .line 521
    :goto_23
    return-void

    #@24
    .line 485
    :cond_24
    iget v7, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@26
    if-ltz v7, :cond_2d

    #@28
    .line 486
    iget v7, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@2a
    invoke-virtual {p0, v7}, Landroid/widget/Spinner;->setSelectedPositionInt(I)V

    #@2d
    .line 489
    :cond_2d
    invoke-virtual {p0}, Landroid/widget/Spinner;->recycleAllViews()V

    #@30
    .line 492
    invoke-virtual {p0}, Landroid/widget/Spinner;->removeAllViewsInLayout()V

    #@33
    .line 495
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@35
    iput v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@37
    .line 496
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@39
    invoke-direct {p0, v7}, Landroid/widget/Spinner;->makeAndAddView(I)Landroid/view/View;

    #@3c
    move-result-object v4

    #@3d
    .line 497
    .local v4, sel:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@40
    move-result v6

    #@41
    .line 498
    .local v6, width:I
    move v5, v1

    #@42
    .line 499
    .local v5, selectedOffset:I
    invoke-virtual {p0}, Landroid/widget/Spinner;->getLayoutDirection()I

    #@45
    move-result v3

    #@46
    .line 500
    .local v3, layoutDirection:I
    iget v7, p0, Landroid/widget/Spinner;->mGravity:I

    #@48
    invoke-static {v7, v3}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@4b
    move-result v0

    #@4c
    .line 501
    .local v0, absoluteGravity:I
    and-int/lit8 v7, v0, 0x7

    #@4e
    sparse-switch v7, :sswitch_data_76

    #@51
    .line 509
    :goto_51
    invoke-virtual {v4, v5}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@54
    .line 512
    iget-object v7, p0, Landroid/widget/AbsSpinner;->mRecycler:Landroid/widget/AbsSpinner$RecycleBin;

    #@56
    invoke-virtual {v7}, Landroid/widget/AbsSpinner$RecycleBin;->clear()V

    #@59
    .line 514
    invoke-virtual {p0}, Landroid/widget/Spinner;->invalidate()V

    #@5c
    .line 516
    invoke-virtual {p0}, Landroid/widget/Spinner;->checkSelectionChanged()V

    #@5f
    .line 518
    iput-boolean v9, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@61
    .line 519
    iput-boolean v9, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@63
    .line 520
    iget v7, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@65
    invoke-virtual {p0, v7}, Landroid/widget/Spinner;->setNextSelectedPositionInt(I)V

    #@68
    goto :goto_23

    #@69
    .line 503
    :sswitch_69
    div-int/lit8 v7, v2, 0x2

    #@6b
    add-int/2addr v7, v1

    #@6c
    div-int/lit8 v8, v6, 0x2

    #@6e
    sub-int v5, v7, v8

    #@70
    .line 504
    goto :goto_51

    #@71
    .line 506
    :sswitch_71
    add-int v7, v1, v2

    #@73
    sub-int v5, v7, v6

    #@75
    goto :goto_51

    #@76
    .line 501
    :sswitch_data_76
    .sparse-switch
        0x1 -> :sswitch_69
        0x5 -> :sswitch_71
    .end sparse-switch
.end method

.method measureContentWidth(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
    .registers 16
    .parameter "adapter"
    .parameter "background"

    #@0
    .prologue
    .line 658
    if-nez p1, :cond_4

    #@2
    .line 659
    const/4 v8, 0x0

    #@3
    .line 698
    :cond_3
    :goto_3
    return v8

    #@4
    .line 662
    :cond_4
    const/4 v8, 0x0

    #@5
    .line 663
    .local v8, width:I
    const/4 v5, 0x0

    #@6
    .line 664
    .local v5, itemView:Landroid/view/View;
    const/4 v4, 0x0

    #@7
    .line 665
    .local v4, itemType:I
    const/4 v10, 0x0

    #@8
    const/4 v11, 0x0

    #@9
    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c
    move-result v9

    #@d
    .line 667
    .local v9, widthMeasureSpec:I
    const/4 v10, 0x0

    #@e
    const/4 v11, 0x0

    #@f
    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@12
    move-result v2

    #@13
    .line 672
    .local v2, heightMeasureSpec:I
    const/4 v10, 0x0

    #@14
    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@17
    move-result v11

    #@18
    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    #@1b
    move-result v7

    #@1c
    .line 673
    .local v7, start:I
    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    #@1f
    move-result v10

    #@20
    add-int/lit8 v11, v7, 0xf

    #@22
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@25
    move-result v1

    #@26
    .line 674
    .local v1, end:I
    sub-int v0, v1, v7

    #@28
    .line 675
    .local v0, count:I
    const/4 v10, 0x0

    #@29
    rsub-int/lit8 v11, v0, 0xf

    #@2b
    sub-int v11, v7, v11

    #@2d
    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    #@30
    move-result v7

    #@31
    .line 676
    move v3, v7

    #@32
    .local v3, i:I
    :goto_32
    if-ge v3, v1, :cond_5e

    #@34
    .line 677
    invoke-interface {p1, v3}, Landroid/widget/SpinnerAdapter;->getItemViewType(I)I

    #@37
    move-result v6

    #@38
    .line 678
    .local v6, positionType:I
    if-eq v6, v4, :cond_3c

    #@3a
    .line 679
    move v4, v6

    #@3b
    .line 680
    const/4 v5, 0x0

    #@3c
    .line 682
    :cond_3c
    invoke-interface {p1, v3, v5, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@3f
    move-result-object v5

    #@40
    .line 683
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@43
    move-result-object v10

    #@44
    if-nez v10, :cond_50

    #@46
    .line 684
    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    #@48
    const/4 v11, -0x2

    #@49
    const/4 v12, -0x2

    #@4a
    invoke-direct {v10, v11, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@4d
    invoke-virtual {v5, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@50
    .line 688
    :cond_50
    invoke-virtual {v5, v9, v2}, Landroid/view/View;->measure(II)V

    #@53
    .line 689
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    #@56
    move-result v10

    #@57
    invoke-static {v8, v10}, Ljava/lang/Math;->max(II)I

    #@5a
    move-result v8

    #@5b
    .line 676
    add-int/lit8 v3, v3, 0x1

    #@5d
    goto :goto_32

    #@5e
    .line 693
    .end local v6           #positionType:I
    :cond_5e
    if-eqz p2, :cond_3

    #@60
    .line 694
    iget-object v10, p0, Landroid/widget/Spinner;->mTempRect:Landroid/graphics/Rect;

    #@62
    invoke-virtual {p2, v10}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@65
    .line 695
    iget-object v10, p0, Landroid/widget/Spinner;->mTempRect:Landroid/graphics/Rect;

    #@67
    iget v10, v10, Landroid/graphics/Rect;->left:I

    #@69
    iget-object v11, p0, Landroid/widget/Spinner;->mTempRect:Landroid/graphics/Rect;

    #@6b
    iget v11, v11, Landroid/graphics/Rect;->right:I

    #@6d
    add-int/2addr v10, v11

    #@6e
    add-int/2addr v8, v10

    #@6f
    goto :goto_3
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 618
    invoke-virtual {p0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    #@3
    .line 619
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@6
    .line 620
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 412
    invoke-super {p0}, Landroid/widget/AbsSpinner;->onDetachedFromWindow()V

    #@3
    .line 414
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@5
    if-eqz v0, :cond_14

    #@7
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@9
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->isShowing()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 415
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@11
    invoke-interface {v0}, Landroid/widget/Spinner$SpinnerPopup;->dismiss()V

    #@14
    .line 417
    :cond_14
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 624
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 625
    const-class v0, Landroid/widget/Spinner;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 626
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 630
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 631
    const-class v0, Landroid/widget/Spinner;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 632
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 458
    invoke-super/range {p0 .. p5}, Landroid/widget/AbsSpinner;->onLayout(ZIIII)V

    #@4
    .line 459
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@7
    .line 460
    invoke-virtual {p0, v1, v1}, Landroid/widget/Spinner;->layout(IZ)V

    #@a
    .line 461
    iput-boolean v1, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@c
    .line 462
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 440
    invoke-super {p0, p1, p2}, Landroid/widget/AbsSpinner;->onMeasure(II)V

    #@3
    .line 441
    iget-object v1, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@5
    if-eqz v1, :cond_32

    #@7
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@a
    move-result v1

    #@b
    const/high16 v2, -0x8000

    #@d
    if-ne v1, v2, :cond_32

    #@f
    .line 442
    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredWidth()I

    #@12
    move-result v0

    #@13
    .line 443
    .local v0, measuredWidth:I
    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p0, v1, v2}, Landroid/widget/Spinner;->measureContentWidth(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    #@1e
    move-result v1

    #@1f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@22
    move-result v1

    #@23
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@26
    move-result v2

    #@27
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@2a
    move-result v1

    #@2b
    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredHeight()I

    #@2e
    move-result v2

    #@2f
    invoke-virtual {p0, v1, v2}, Landroid/widget/Spinner;->setMeasuredDimension(II)V

    #@32
    .line 448
    .end local v0           #measuredWidth:I
    :cond_32
    return-void
.end method

.method public performClick()Z
    .registers 3

    #@0
    .prologue
    .line 604
    invoke-super {p0}, Landroid/widget/AbsSpinner;->performClick()Z

    #@3
    move-result v0

    #@4
    .line 606
    .local v0, handled:Z
    if-nez v0, :cond_14

    #@6
    .line 607
    const/4 v0, 0x1

    #@7
    .line 609
    iget-object v1, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@9
    invoke-interface {v1}, Landroid/widget/Spinner$SpinnerPopup;->isShowing()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_14

    #@f
    .line 610
    iget-object v1, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@11
    invoke-interface {v1}, Landroid/widget/Spinner$SpinnerPopup;->show()V

    #@14
    .line 614
    :cond_14
    return v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    check-cast p1, Landroid/widget/SpinnerAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .registers 4
    .parameter "adapter"

    #@0
    .prologue
    .line 381
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@3
    .line 383
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 384
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@9
    new-instance v1, Landroid/widget/Spinner$DropDownAdapter;

    #@b
    invoke-direct {v1, p1}, Landroid/widget/Spinner$DropDownAdapter;-><init>(Landroid/widget/SpinnerAdapter;)V

    #@e
    invoke-interface {v0, v1}, Landroid/widget/Spinner$SpinnerPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    #@11
    .line 388
    :goto_11
    return-void

    #@12
    .line 386
    :cond_12
    new-instance v0, Landroid/widget/Spinner$DropDownAdapter;

    #@14
    invoke-direct {v0, p1}, Landroid/widget/Spinner$DropDownAdapter;-><init>(Landroid/widget/SpinnerAdapter;)V

    #@17
    iput-object v0, p0, Landroid/widget/Spinner;->mTempAdapter:Landroid/widget/Spinner$DropDownAdapter;

    #@19
    goto :goto_11
.end method

.method public setDropDownHorizontalOffset(I)V
    .registers 3
    .parameter "pixels"

    #@0
    .prologue
    .line 289
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0, p1}, Landroid/widget/Spinner$SpinnerPopup;->setHorizontalOffset(I)V

    #@5
    .line 290
    return-void
.end method

.method public setDropDownVerticalOffset(I)V
    .registers 3
    .parameter "pixels"

    #@0
    .prologue
    .line 265
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0, p1}, Landroid/widget/Spinner$SpinnerPopup;->setVerticalOffset(I)V

    #@5
    .line 266
    return-void
.end method

.method public setDropDownWidth(I)V
    .registers 4
    .parameter "pixels"

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    instance-of v0, v0, Landroid/widget/Spinner$DropdownPopup;

    #@4
    if-nez v0, :cond_e

    #@6
    .line 319
    const-string v0, "Spinner"

    #@8
    const-string v1, "Cannot set dropdown width for MODE_DIALOG, ignoring"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 323
    :goto_d
    return-void

    #@e
    .line 322
    :cond_e
    iput p1, p0, Landroid/widget/Spinner;->mDropDownWidth:I

    #@10
    goto :goto_d
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setEnabled(Z)V

    #@3
    .line 343
    iget-boolean v2, p0, Landroid/widget/Spinner;->mDisableChildrenWhenDisabled:Z

    #@5
    if-eqz v2, :cond_18

    #@7
    .line 344
    invoke-virtual {p0}, Landroid/widget/Spinner;->getChildCount()I

    #@a
    move-result v0

    #@b
    .line 345
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_18

    #@e
    .line 346
    invoke-virtual {p0, v1}, Landroid/widget/Spinner;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    #@15
    .line 345
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_c

    #@18
    .line 349
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_18
    return-void
.end method

.method public setGravity(I)V
    .registers 3
    .parameter "gravity"

    #@0
    .prologue
    .line 360
    iget v0, p0, Landroid/widget/Spinner;->mGravity:I

    #@2
    if-eq v0, p1, :cond_11

    #@4
    .line 361
    and-int/lit8 v0, p1, 0x7

    #@6
    if-nez v0, :cond_c

    #@8
    .line 362
    const v0, 0x800003

    #@b
    or-int/2addr p1, v0

    #@c
    .line 364
    :cond_c
    iput p1, p0, Landroid/widget/Spinner;->mGravity:I

    #@e
    .line 365
    invoke-virtual {p0}, Landroid/widget/Spinner;->requestLayout()V

    #@11
    .line 367
    :cond_11
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 4
    .parameter "l"

    #@0
    .prologue
    .line 428
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string/jumbo v1, "setOnItemClickListener cannot be used with a spinner."

    #@5
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public setOnItemClickListenerInt(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 435
    invoke-super {p0, p1}, Landroid/widget/AbsSpinner;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@3
    .line 436
    return-void
.end method

.method public setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "background"

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    instance-of v0, v0, Landroid/widget/Spinner$DropdownPopup;

    #@4
    if-nez v0, :cond_f

    #@6
    .line 226
    const-string v0, "Spinner"

    #@8
    const-string/jumbo v1, "setPopupBackgroundDrawable: incompatible spinner mode; ignoring..."

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 230
    :goto_e
    return-void

    #@f
    .line 229
    :cond_f
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@11
    check-cast v0, Landroid/widget/Spinner$DropdownPopup;

    #@13
    invoke-virtual {v0, p1}, Landroid/widget/Spinner$DropdownPopup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@16
    goto :goto_e
.end method

.method public setPopupBackgroundResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 241
    invoke-virtual {p0}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 242
    return-void
.end method

.method public setPrompt(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "prompt"

    #@0
    .prologue
    .line 639
    iget-object v0, p0, Landroid/widget/Spinner;->mPopup:Landroid/widget/Spinner$SpinnerPopup;

    #@2
    invoke-interface {v0, p1}, Landroid/widget/Spinner$SpinnerPopup;->setPromptText(Ljava/lang/CharSequence;)V

    #@5
    .line 640
    return-void
.end method

.method public setPromptId(I)V
    .registers 3
    .parameter "promptId"

    #@0
    .prologue
    .line 647
    invoke-virtual {p0}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    #@b
    .line 648
    return-void
.end method
