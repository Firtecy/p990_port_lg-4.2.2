.class public Landroid/widget/Toast;
.super Ljava/lang/Object;
.source "Toast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Toast$TN;
    }
.end annotation


# static fields
.field public static final LENGTH_LONG:I = 0x1

.field public static final LENGTH_SHORT:I = 0x0

.field static final TAG:Ljava/lang/String; = "Toast"

.field static final localLOGV:Z

.field private static mToastCallerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/widget/Toast$TN;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private static sService:Landroid/app/INotificationManager;


# instance fields
.field final mContext:Landroid/content/Context;

.field mDuration:I

.field mNextView:Landroid/view/View;

.field final mTN:Landroid/widget/Toast$TN;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 88
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/widget/Toast;->mToastCallerMap:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 97
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 98
    iput-object p1, p0, Landroid/widget/Toast;->mContext:Landroid/content/Context;

    #@5
    .line 99
    new-instance v0, Landroid/widget/Toast$TN;

    #@7
    invoke-direct {v0}, Landroid/widget/Toast$TN;-><init>()V

    #@a
    iput-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@c
    .line 100
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@11
    move-result-object v1

    #@12
    const v2, 0x105000b

    #@15
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@18
    move-result v1

    #@19
    iput v1, v0, Landroid/widget/Toast$TN;->mY:I

    #@1b
    .line 104
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@1d
    if-eqz v0, :cond_28

    #@1f
    .line 106
    sget-object v0, Landroid/widget/Toast;->mToastCallerMap:Ljava/util/HashMap;

    #@21
    iget-object v1, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@23
    iget-object v2, p0, Landroid/widget/Toast;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    .line 108
    :cond_28
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 64
    sget-object v0, Landroid/widget/Toast;->mToastCallerMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method private static getService()Landroid/app/INotificationManager;
    .registers 1

    #@0
    .prologue
    .line 313
    sget-object v0, Landroid/widget/Toast;->sService:Landroid/app/INotificationManager;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 314
    sget-object v0, Landroid/widget/Toast;->sService:Landroid/app/INotificationManager;

    #@6
    .line 317
    :goto_6
    return-object v0

    #@7
    .line 316
    :cond_7
    const-string/jumbo v0, "notification"

    #@a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/widget/Toast;->sService:Landroid/app/INotificationManager;

    #@14
    .line 317
    sget-object v0, Landroid/widget/Toast;->sService:Landroid/app/INotificationManager;

    #@16
    goto :goto_6
.end method

.method public static makeText(Landroid/content/Context;II)Landroid/widget/Toast;
    .registers 4
    .parameter "context"
    .parameter "resId"
    .parameter "duration"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 279
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public static makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    .registers 9
    .parameter "context"
    .parameter "text"
    .parameter "duration"

    #@0
    .prologue
    .line 252
    new-instance v1, Landroid/widget/Toast;

    #@2
    invoke-direct {v1, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    #@5
    .line 254
    .local v1, result:Landroid/widget/Toast;
    const-string/jumbo v4, "layout_inflater"

    #@8
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/view/LayoutInflater;

    #@e
    .line 256
    .local v0, inflate:Landroid/view/LayoutInflater;
    const v4, 0x10900e2

    #@11
    const/4 v5, 0x0

    #@12
    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@15
    move-result-object v3

    #@16
    .line 257
    .local v3, v:Landroid/view/View;
    const v4, 0x102000b

    #@19
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Landroid/widget/TextView;

    #@1f
    .line 258
    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@22
    .line 260
    iput-object v3, v1, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@24
    .line 261
    iput p2, v1, Landroid/widget/Toast;->mDuration:I

    #@26
    .line 263
    return-object v1
.end method


# virtual methods
.method public cancel()V
    .registers 4

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    invoke-virtual {v0}, Landroid/widget/Toast$TN;->hide()V

    #@5
    .line 139
    :try_start_5
    invoke-static {}, Landroid/widget/Toast;->getService()Landroid/app/INotificationManager;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Landroid/widget/Toast;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    iget-object v2, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@11
    invoke-interface {v0, v1, v2}, Landroid/app/INotificationManager;->cancelToast(Ljava/lang/String;Landroid/app/ITransientNotification;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_14} :catch_15

    #@14
    .line 143
    :goto_14
    return-void

    #@15
    .line 140
    :catch_15
    move-exception v0

    #@16
    goto :goto_14
.end method

.method public getDuration()I
    .registers 2

    #@0
    .prologue
    .line 175
    iget v0, p0, Landroid/widget/Toast;->mDuration:I

    #@2
    return v0
.end method

.method public getGravity()I
    .registers 2

    #@0
    .prologue
    .line 224
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iget v0, v0, Landroid/widget/Toast$TN;->mGravity:I

    #@4
    return v0
.end method

.method public getHorizontalMargin()F
    .registers 2

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iget v0, v0, Landroid/widget/Toast$TN;->mHorizontalMargin:F

    #@4
    return v0
.end method

.method public getVerticalMargin()F
    .registers 2

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iget v0, v0, Landroid/widget/Toast$TN;->mVerticalMargin:F

    #@4
    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getXOffset()I
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iget v0, v0, Landroid/widget/Toast$TN;->mX:I

    #@4
    return v0
.end method

.method public getYOffset()I
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iget v0, v0, Landroid/widget/Toast$TN;->mY:I

    #@4
    return v0
.end method

.method public setDuration(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 167
    iput p1, p0, Landroid/widget/Toast;->mDuration:I

    #@2
    .line 168
    return-void
.end method

.method public setGravity(III)V
    .registers 5
    .parameter "gravity"
    .parameter "xOffset"
    .parameter "yOffset"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iput p1, v0, Landroid/widget/Toast$TN;->mGravity:I

    #@4
    .line 214
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@6
    iput p2, v0, Landroid/widget/Toast$TN;->mX:I

    #@8
    .line 215
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@a
    iput p3, v0, Landroid/widget/Toast$TN;->mY:I

    #@c
    .line 216
    return-void
.end method

.method public setMargin(FF)V
    .registers 4
    .parameter "horizontalMargin"
    .parameter "verticalMargin"

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@2
    iput p1, v0, Landroid/widget/Toast$TN;->mHorizontalMargin:F

    #@4
    .line 190
    iget-object v0, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@6
    iput p2, v0, Landroid/widget/Toast$TN;->mVerticalMargin:F

    #@8
    .line 191
    return-void
.end method

.method public setText(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 287
    iget-object v0, p0, Landroid/widget/Toast;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    #@9
    .line 288
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 295
    iget-object v1, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 296
    new-instance v1, Ljava/lang/RuntimeException;

    #@6
    const-string v2, "This Toast was not created with Toast.makeText()"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 298
    :cond_c
    iget-object v1, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@e
    const v2, 0x102000b

    #@11
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/widget/TextView;

    #@17
    .line 299
    .local v0, tv:Landroid/widget/TextView;
    if-nez v0, :cond_21

    #@19
    .line 300
    new-instance v1, Ljava/lang/RuntimeException;

    #@1b
    const-string v2, "This Toast was not created with Toast.makeText()"

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1

    #@21
    .line 302
    :cond_21
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@24
    .line 303
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 150
    iput-object p1, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@2
    .line 151
    return-void
.end method

.method public show()V
    .registers 6

    #@0
    .prologue
    .line 114
    iget-object v3, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@2
    if-nez v3, :cond_d

    #@4
    .line 115
    new-instance v3, Ljava/lang/RuntimeException;

    #@6
    const-string/jumbo v4, "setView must have been called"

    #@9
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v3

    #@d
    .line 118
    :cond_d
    invoke-static {}, Landroid/widget/Toast;->getService()Landroid/app/INotificationManager;

    #@10
    move-result-object v1

    #@11
    .line 119
    .local v1, service:Landroid/app/INotificationManager;
    iget-object v3, p0, Landroid/widget/Toast;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 120
    .local v0, pkg:Ljava/lang/String;
    iget-object v2, p0, Landroid/widget/Toast;->mTN:Landroid/widget/Toast$TN;

    #@19
    .line 121
    .local v2, tn:Landroid/widget/Toast$TN;
    iget-object v3, p0, Landroid/widget/Toast;->mNextView:Landroid/view/View;

    #@1b
    iput-object v3, v2, Landroid/widget/Toast$TN;->mNextView:Landroid/view/View;

    #@1d
    .line 124
    :try_start_1d
    iget v3, p0, Landroid/widget/Toast;->mDuration:I

    #@1f
    invoke-interface {v1, v0, v2, v3}, Landroid/app/INotificationManager;->enqueueToast(Ljava/lang/String;Landroid/app/ITransientNotification;I)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_22} :catch_23

    #@22
    .line 128
    :goto_22
    return-void

    #@23
    .line 125
    :catch_23
    move-exception v3

    #@24
    goto :goto_22
.end method
