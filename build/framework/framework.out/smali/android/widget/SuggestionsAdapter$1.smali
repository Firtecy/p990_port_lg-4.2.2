.class Landroid/widget/SuggestionsAdapter$1;
.super Ljava/lang/Object;
.source "SuggestionsAdapter.java"

# interfaces
.implements Landroid/widget/Filter$Delayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/SuggestionsAdapter;-><init>(Landroid/content/Context;Landroid/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPreviousLength:I

.field final synthetic this$0:Landroid/widget/SuggestionsAdapter;


# direct methods
.method constructor <init>(Landroid/widget/SuggestionsAdapter;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 139
    iput-object p1, p0, Landroid/widget/SuggestionsAdapter$1;->this$0:Landroid/widget/SuggestionsAdapter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 141
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/widget/SuggestionsAdapter$1;->mPreviousLength:I

    #@8
    return-void
.end method


# virtual methods
.method public getPostingDelay(Ljava/lang/CharSequence;)J
    .registers 6
    .parameter "constraint"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 144
    if-nez p1, :cond_5

    #@4
    .line 148
    :goto_4
    return-wide v0

    #@5
    .line 146
    :cond_5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v2

    #@9
    iget v3, p0, Landroid/widget/SuggestionsAdapter$1;->mPreviousLength:I

    #@b
    if-ge v2, v3, :cond_f

    #@d
    const-wide/16 v0, 0x1f4

    #@f
    .line 147
    .local v0, delay:J
    :cond_f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v2

    #@13
    iput v2, p0, Landroid/widget/SuggestionsAdapter$1;->mPreviousLength:I

    #@15
    goto :goto_4
.end method
