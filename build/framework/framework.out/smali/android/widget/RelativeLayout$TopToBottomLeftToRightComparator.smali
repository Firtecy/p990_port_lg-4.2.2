.class Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;
.super Ljava/lang/Object;
.source "RelativeLayout.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/RelativeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TopToBottomLeftToRightComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/RelativeLayout;


# direct methods
.method private constructor <init>(Landroid/widget/RelativeLayout;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1100
    iput-object p1, p0, Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;->this$0:Landroid/widget/RelativeLayout;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/RelativeLayout;Landroid/widget/RelativeLayout$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1100
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;-><init>(Landroid/widget/RelativeLayout;)V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/view/View;Landroid/view/View;)I
    .registers 9
    .parameter "first"
    .parameter "second"

    #@0
    .prologue
    .line 1103
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@3
    move-result v4

    #@4
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    #@7
    move-result v5

    #@8
    sub-int v2, v4, v5

    #@a
    .line 1104
    .local v2, topDifference:I
    if-eqz v2, :cond_d

    #@c
    .line 1122
    .end local v2           #topDifference:I
    :goto_c
    return v2

    #@d
    .line 1108
    .restart local v2       #topDifference:I
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@10
    move-result v4

    #@11
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    #@14
    move-result v5

    #@15
    sub-int v1, v4, v5

    #@17
    .line 1109
    .local v1, leftDifference:I
    if-eqz v1, :cond_1b

    #@19
    move v2, v1

    #@1a
    .line 1110
    goto :goto_c

    #@1b
    .line 1113
    :cond_1b
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@1e
    move-result v4

    #@1f
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    #@22
    move-result v5

    #@23
    sub-int v0, v4, v5

    #@25
    .line 1114
    .local v0, heightDiference:I
    if-eqz v0, :cond_29

    #@27
    move v2, v0

    #@28
    .line 1115
    goto :goto_c

    #@29
    .line 1118
    :cond_29
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@2c
    move-result v4

    #@2d
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    #@30
    move-result v5

    #@31
    sub-int v3, v4, v5

    #@33
    .line 1119
    .local v3, widthDiference:I
    if-eqz v3, :cond_37

    #@35
    move v2, v3

    #@36
    .line 1120
    goto :goto_c

    #@37
    .line 1122
    :cond_37
    const/4 v2, 0x0

    #@38
    goto :goto_c
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1100
    check-cast p1, Landroid/view/View;

    #@2
    .end local p1
    check-cast p2, Landroid/view/View;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/widget/RelativeLayout$TopToBottomLeftToRightComparator;->compare(Landroid/view/View;Landroid/view/View;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
