.class public Landroid/widget/GridView;
.super Landroid/widget/AbsListView;
.source "GridView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field public static final AUTO_FIT:I = -0x1

.field public static final NO_STRETCH:I = 0x0

.field public static final STRETCH_COLUMN_WIDTH:I = 0x2

.field public static final STRETCH_SPACING:I = 0x1

.field public static final STRETCH_SPACING_UNIFORM:I = 0x3


# instance fields
.field private mColumnWidth:I

.field private mGravity:I

.field private mHorizontalSpacing:I

.field private mNumColumns:I

.field private mReferenceView:Landroid/view/View;

.field private mReferenceViewInSelectedRow:Landroid/view/View;

.field private mRequestedColumnWidth:I

.field private mRequestedHorizontalSpacing:I

.field private mRequestedNumColumns:I

.field private mStretchMode:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 102
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 106
    const v0, 0x1010071

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 15
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x2

    #@3
    const/4 v8, -0x1

    #@4
    const/4 v7, 0x0

    #@5
    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8
    .line 84
    iput v8, p0, Landroid/widget/GridView;->mNumColumns:I

    #@a
    .line 86
    iput v7, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@c
    .line 88
    iput v7, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@e
    .line 89
    iput v9, p0, Landroid/widget/GridView;->mStretchMode:I

    #@10
    .line 94
    iput-object v6, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@12
    .line 95
    iput-object v6, p0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@14
    .line 97
    const v6, 0x800003

    #@17
    iput v6, p0, Landroid/widget/GridView;->mGravity:I

    #@19
    .line 99
    new-instance v6, Landroid/graphics/Rect;

    #@1b
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@1e
    iput-object v6, p0, Landroid/widget/GridView;->mTempRect:Landroid/graphics/Rect;

    #@20
    .line 112
    sget-object v6, Lcom/android/internal/R$styleable;->GridView:[I

    #@22
    invoke-virtual {p1, p2, v6, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@25
    move-result-object v0

    #@26
    .line 115
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v10, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@29
    move-result v2

    #@2a
    .line 117
    .local v2, hSpacing:I
    invoke-virtual {p0, v2}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    #@2d
    .line 119
    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@30
    move-result v5

    #@31
    .line 121
    .local v5, vSpacing:I
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    #@34
    .line 123
    const/4 v6, 0x3

    #@35
    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@38
    move-result v3

    #@39
    .line 124
    .local v3, index:I
    if-ltz v3, :cond_3e

    #@3b
    .line 125
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->setStretchMode(I)V

    #@3e
    .line 128
    :cond_3e
    const/4 v6, 0x4

    #@3f
    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@42
    move-result v1

    #@43
    .line 129
    .local v1, columnWidth:I
    if-lez v1, :cond_48

    #@45
    .line 130
    invoke-virtual {p0, v1}, Landroid/widget/GridView;->setColumnWidth(I)V

    #@48
    .line 133
    :cond_48
    const/4 v6, 0x5

    #@49
    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4c
    move-result v4

    #@4d
    .line 134
    .local v4, numColumns:I
    invoke-virtual {p0, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    #@50
    .line 136
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@53
    move-result v3

    #@54
    .line 137
    if-ltz v3, :cond_59

    #@56
    .line 138
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->setGravity(I)V

    #@59
    .line 141
    :cond_59
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@5c
    .line 142
    return-void
.end method

.method private adjustForBottomFadingEdge(Landroid/view/View;II)V
    .registers 8
    .parameter "childInSelectedRow"
    .parameter "topSelectionPixel"
    .parameter "bottomSelectionPixel"

    #@0
    .prologue
    .line 778
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@3
    move-result v3

    #@4
    if-le v3, p3, :cond_1a

    #@6
    .line 782
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@9
    move-result v3

    #@a
    sub-int v1, v3, p2

    #@c
    .line 786
    .local v1, spaceAbove:I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@f
    move-result v3

    #@10
    sub-int v2, v3, p3

    #@12
    .line 787
    .local v2, spaceBelow:I
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v0

    #@16
    .line 790
    .local v0, offset:I
    neg-int v3, v0

    #@17
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@1a
    .line 792
    .end local v0           #offset:I
    .end local v1           #spaceAbove:I
    .end local v2           #spaceBelow:I
    :cond_1a
    return-void
.end method

.method private adjustForTopFadingEdge(Landroid/view/View;II)V
    .registers 8
    .parameter "childInSelectedRow"
    .parameter "topSelectionPixel"
    .parameter "bottomSelectionPixel"

    #@0
    .prologue
    .line 806
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@3
    move-result v3

    #@4
    if-ge v3, p2, :cond_19

    #@6
    .line 809
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@9
    move-result v3

    #@a
    sub-int v1, p2, v3

    #@c
    .line 813
    .local v1, spaceAbove:I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@f
    move-result v3

    #@10
    sub-int v2, p3, v3

    #@12
    .line 814
    .local v2, spaceBelow:I
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v0

    #@16
    .line 817
    .local v0, offset:I
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@19
    .line 819
    .end local v0           #offset:I
    .end local v1           #spaceAbove:I
    .end local v2           #spaceBelow:I
    :cond_19
    return-void
.end method

.method private adjustViewsUpOrDown()V
    .registers 7

    #@0
    .prologue
    .line 2141
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 2143
    .local v1, childCount:I
    if-lez v1, :cond_29

    #@6
    .line 2147
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@8
    if-nez v3, :cond_2a

    #@a
    .line 2150
    const/4 v3, 0x0

    #@b
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    .line 2151
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@12
    move-result v3

    #@13
    iget-object v4, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@15
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@17
    sub-int v2, v3, v4

    #@19
    .line 2152
    .local v2, delta:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 2155
    iget v3, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@1f
    sub-int/2addr v2, v3

    #@20
    .line 2157
    :cond_20
    if-gez v2, :cond_23

    #@22
    .line 2159
    const/4 v2, 0x0

    #@23
    .line 2178
    :cond_23
    :goto_23
    if-eqz v2, :cond_29

    #@25
    .line 2179
    neg-int v3, v2

    #@26
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@29
    .line 2182
    .end local v0           #child:Landroid/view/View;
    .end local v2           #delta:I
    :cond_29
    return-void

    #@2a
    .line 2163
    :cond_2a
    add-int/lit8 v3, v1, -0x1

    #@2c
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@2f
    move-result-object v0

    #@30
    .line 2164
    .restart local v0       #child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    #@33
    move-result v3

    #@34
    invoke-virtual {p0}, Landroid/widget/GridView;->getHeight()I

    #@37
    move-result v4

    #@38
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3a
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@3c
    sub-int/2addr v4, v5

    #@3d
    sub-int v2, v3, v4

    #@3f
    .line 2166
    .restart local v2       #delta:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@41
    add-int/2addr v3, v1

    #@42
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@44
    if-ge v3, v4, :cond_49

    #@46
    .line 2169
    iget v3, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@48
    add-int/2addr v2, v3

    #@49
    .line 2172
    :cond_49
    if-lez v2, :cond_23

    #@4b
    .line 2174
    const/4 v2, 0x0

    #@4c
    goto :goto_23
.end method

.method private commonKey(IILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "keyCode"
    .parameter "count"
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/16 v6, 0x82

    #@3
    const/16 v5, 0x21

    #@5
    const/4 v2, 0x0

    #@6
    const/4 v3, 0x1

    #@7
    .line 1532
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@9
    if-nez v4, :cond_c

    #@b
    .line 1659
    :goto_b
    return v2

    #@c
    .line 1536
    :cond_c
    iget-boolean v4, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@e
    if-eqz v4, :cond_13

    #@10
    .line 1537
    invoke-virtual {p0}, Landroid/widget/GridView;->layoutChildren()V

    #@13
    .line 1540
    :cond_13
    const/4 v1, 0x0

    #@14
    .line 1541
    .local v1, handled:Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@17
    move-result v0

    #@18
    .line 1543
    .local v0, action:I
    if-eq v0, v3, :cond_1d

    #@1a
    .line 1544
    sparse-switch p1, :sswitch_data_1ac

    #@1d
    .line 1643
    :cond_1d
    :goto_1d
    if-eqz v1, :cond_18c

    #@1f
    move v2, v3

    #@20
    .line 1644
    goto :goto_b

    #@21
    .line 1546
    :sswitch_21
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_1d

    #@27
    .line 1547
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@2a
    move-result v4

    #@2b
    if-nez v4, :cond_35

    #@2d
    const/16 v4, 0x11

    #@2f
    invoke-virtual {p0, v4}, Landroid/widget/GridView;->arrowScroll(I)Z

    #@32
    move-result v4

    #@33
    if-eqz v4, :cond_37

    #@35
    :cond_35
    move v1, v3

    #@36
    :goto_36
    goto :goto_1d

    #@37
    :cond_37
    move v1, v2

    #@38
    goto :goto_36

    #@39
    .line 1552
    :sswitch_39
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@3c
    move-result v4

    #@3d
    if-eqz v4, :cond_1d

    #@3f
    .line 1553
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@42
    move-result v4

    #@43
    if-nez v4, :cond_4d

    #@45
    const/16 v4, 0x42

    #@47
    invoke-virtual {p0, v4}, Landroid/widget/GridView;->arrowScroll(I)Z

    #@4a
    move-result v4

    #@4b
    if-eqz v4, :cond_4f

    #@4d
    :cond_4d
    move v1, v3

    #@4e
    :goto_4e
    goto :goto_1d

    #@4f
    :cond_4f
    move v1, v2

    #@50
    goto :goto_4e

    #@51
    .line 1558
    :sswitch_51
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@54
    move-result v4

    #@55
    if-eqz v4, :cond_67

    #@57
    .line 1559
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@5a
    move-result v4

    #@5b
    if-nez v4, :cond_63

    #@5d
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->arrowScroll(I)Z

    #@60
    move-result v4

    #@61
    if-eqz v4, :cond_65

    #@63
    :cond_63
    move v1, v3

    #@64
    :goto_64
    goto :goto_1d

    #@65
    :cond_65
    move v1, v2

    #@66
    goto :goto_64

    #@67
    .line 1560
    :cond_67
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@6a
    move-result v4

    #@6b
    if-eqz v4, :cond_1d

    #@6d
    .line 1561
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@70
    move-result v4

    #@71
    if-nez v4, :cond_79

    #@73
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->fullScroll(I)Z

    #@76
    move-result v4

    #@77
    if-eqz v4, :cond_7b

    #@79
    :cond_79
    move v1, v3

    #@7a
    :goto_7a
    goto :goto_1d

    #@7b
    :cond_7b
    move v1, v2

    #@7c
    goto :goto_7a

    #@7d
    .line 1566
    :sswitch_7d
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@80
    move-result v4

    #@81
    if-eqz v4, :cond_93

    #@83
    .line 1567
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@86
    move-result v4

    #@87
    if-nez v4, :cond_8f

    #@89
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->arrowScroll(I)Z

    #@8c
    move-result v4

    #@8d
    if-eqz v4, :cond_91

    #@8f
    :cond_8f
    move v1, v3

    #@90
    :goto_90
    goto :goto_1d

    #@91
    :cond_91
    move v1, v2

    #@92
    goto :goto_90

    #@93
    .line 1568
    :cond_93
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@96
    move-result v4

    #@97
    if-eqz v4, :cond_1d

    #@99
    .line 1569
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@9c
    move-result v4

    #@9d
    if-nez v4, :cond_a5

    #@9f
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->fullScroll(I)Z

    #@a2
    move-result v4

    #@a3
    if-eqz v4, :cond_a8

    #@a5
    :cond_a5
    move v1, v3

    #@a6
    :goto_a6
    goto/16 :goto_1d

    #@a8
    :cond_a8
    move v1, v2

    #@a9
    goto :goto_a6

    #@aa
    .line 1575
    :sswitch_aa
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@ad
    move-result v4

    #@ae
    if-eqz v4, :cond_1d

    #@b0
    .line 1576
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@b3
    move-result v1

    #@b4
    .line 1577
    if-nez v1, :cond_1d

    #@b6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@b9
    move-result v4

    #@ba
    if-nez v4, :cond_1d

    #@bc
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@bf
    move-result v4

    #@c0
    if-lez v4, :cond_1d

    #@c2
    .line 1579
    invoke-virtual {p0}, Landroid/widget/GridView;->keyPressed()V

    #@c5
    .line 1580
    const/4 v1, 0x1

    #@c6
    goto/16 :goto_1d

    #@c8
    .line 1586
    :sswitch_c8
    iget-object v4, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@ca
    if-eqz v4, :cond_d4

    #@cc
    iget-object v4, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@ce
    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    #@d1
    move-result v4

    #@d2
    if-nez v4, :cond_1d

    #@d4
    .line 1587
    :cond_d4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@d7
    move-result v4

    #@d8
    if-eqz v4, :cond_eb

    #@da
    .line 1588
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@dd
    move-result v4

    #@de
    if-nez v4, :cond_e6

    #@e0
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->pageScroll(I)Z

    #@e3
    move-result v4

    #@e4
    if-eqz v4, :cond_e9

    #@e6
    :cond_e6
    move v1, v3

    #@e7
    :goto_e7
    goto/16 :goto_1d

    #@e9
    :cond_e9
    move v1, v2

    #@ea
    goto :goto_e7

    #@eb
    .line 1589
    :cond_eb
    invoke-virtual {p3, v3}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@ee
    move-result v4

    #@ef
    if-eqz v4, :cond_1d

    #@f1
    .line 1590
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@f4
    move-result v4

    #@f5
    if-nez v4, :cond_fd

    #@f7
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->pageScroll(I)Z

    #@fa
    move-result v4

    #@fb
    if-eqz v4, :cond_100

    #@fd
    :cond_fd
    move v1, v3

    #@fe
    :goto_fe
    goto/16 :goto_1d

    #@100
    :cond_100
    move v1, v2

    #@101
    goto :goto_fe

    #@102
    .line 1596
    :sswitch_102
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@105
    move-result v4

    #@106
    if-eqz v4, :cond_119

    #@108
    .line 1597
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@10b
    move-result v4

    #@10c
    if-nez v4, :cond_114

    #@10e
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->pageScroll(I)Z

    #@111
    move-result v4

    #@112
    if-eqz v4, :cond_117

    #@114
    :cond_114
    move v1, v3

    #@115
    :goto_115
    goto/16 :goto_1d

    #@117
    :cond_117
    move v1, v2

    #@118
    goto :goto_115

    #@119
    .line 1598
    :cond_119
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@11c
    move-result v4

    #@11d
    if-eqz v4, :cond_1d

    #@11f
    .line 1599
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@122
    move-result v4

    #@123
    if-nez v4, :cond_12b

    #@125
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->fullScroll(I)Z

    #@128
    move-result v4

    #@129
    if-eqz v4, :cond_12e

    #@12b
    :cond_12b
    move v1, v3

    #@12c
    :goto_12c
    goto/16 :goto_1d

    #@12e
    :cond_12e
    move v1, v2

    #@12f
    goto :goto_12c

    #@130
    .line 1604
    :sswitch_130
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@133
    move-result v4

    #@134
    if-eqz v4, :cond_147

    #@136
    .line 1605
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@139
    move-result v4

    #@13a
    if-nez v4, :cond_142

    #@13c
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->pageScroll(I)Z

    #@13f
    move-result v4

    #@140
    if-eqz v4, :cond_145

    #@142
    :cond_142
    move v1, v3

    #@143
    :goto_143
    goto/16 :goto_1d

    #@145
    :cond_145
    move v1, v2

    #@146
    goto :goto_143

    #@147
    .line 1606
    :cond_147
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@14a
    move-result v4

    #@14b
    if-eqz v4, :cond_1d

    #@14d
    .line 1607
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@150
    move-result v4

    #@151
    if-nez v4, :cond_159

    #@153
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->fullScroll(I)Z

    #@156
    move-result v4

    #@157
    if-eqz v4, :cond_15c

    #@159
    :cond_159
    move v1, v3

    #@15a
    :goto_15a
    goto/16 :goto_1d

    #@15c
    :cond_15c
    move v1, v2

    #@15d
    goto :goto_15a

    #@15e
    .line 1612
    :sswitch_15e
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@161
    move-result v4

    #@162
    if-eqz v4, :cond_1d

    #@164
    .line 1613
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@167
    move-result v4

    #@168
    if-nez v4, :cond_170

    #@16a
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->fullScroll(I)Z

    #@16d
    move-result v4

    #@16e
    if-eqz v4, :cond_173

    #@170
    :cond_170
    move v1, v3

    #@171
    :goto_171
    goto/16 :goto_1d

    #@173
    :cond_173
    move v1, v2

    #@174
    goto :goto_171

    #@175
    .line 1618
    :sswitch_175
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@178
    move-result v4

    #@179
    if-eqz v4, :cond_1d

    #@17b
    .line 1619
    invoke-virtual {p0}, Landroid/widget/GridView;->resurrectSelectionIfNeeded()Z

    #@17e
    move-result v4

    #@17f
    if-nez v4, :cond_187

    #@181
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->fullScroll(I)Z

    #@184
    move-result v4

    #@185
    if-eqz v4, :cond_18a

    #@187
    :cond_187
    move v1, v3

    #@188
    :goto_188
    goto/16 :goto_1d

    #@18a
    :cond_18a
    move v1, v2

    #@18b
    goto :goto_188

    #@18c
    .line 1647
    :cond_18c
    invoke-virtual {p0, p1, p2, p3}, Landroid/widget/GridView;->sendToTextFilter(IILandroid/view/KeyEvent;)Z

    #@18f
    move-result v4

    #@190
    if-eqz v4, :cond_195

    #@192
    move v2, v3

    #@193
    .line 1648
    goto/16 :goto_b

    #@195
    .line 1651
    :cond_195
    packed-switch v0, :pswitch_data_1da

    #@198
    goto/16 :goto_b

    #@19a
    .line 1653
    :pswitch_19a
    invoke-super {p0, p1, p3}, Landroid/widget/AbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@19d
    move-result v2

    #@19e
    goto/16 :goto_b

    #@1a0
    .line 1655
    :pswitch_1a0
    invoke-super {p0, p1, p3}, Landroid/widget/AbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@1a3
    move-result v2

    #@1a4
    goto/16 :goto_b

    #@1a6
    .line 1657
    :pswitch_1a6
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsListView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@1a9
    move-result v2

    #@1aa
    goto/16 :goto_b

    #@1ac
    .line 1544
    :sswitch_data_1ac
    .sparse-switch
        0x13 -> :sswitch_51
        0x14 -> :sswitch_7d
        0x15 -> :sswitch_21
        0x16 -> :sswitch_39
        0x17 -> :sswitch_aa
        0x3e -> :sswitch_c8
        0x42 -> :sswitch_aa
        0x5c -> :sswitch_102
        0x5d -> :sswitch_130
        0x7a -> :sswitch_15e
        0x7b -> :sswitch_175
    .end sparse-switch

    #@1da
    .line 1651
    :pswitch_data_1da
    .packed-switch 0x0
        :pswitch_19a
        :pswitch_1a0
        :pswitch_1a6
    .end packed-switch
.end method

.method private correctTooHigh(III)V
    .registers 13
    .parameter "numColumns"
    .parameter "verticalSpacing"
    .parameter "childCount"

    #@0
    .prologue
    .line 590
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    add-int/2addr v7, p3

    #@3
    add-int/lit8 v6, v7, -0x1

    #@5
    .line 591
    .local v6, lastPosition:I
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    add-int/lit8 v7, v7, -0x1

    #@9
    if-ne v6, v7, :cond_60

    #@b
    if-lez p3, :cond_60

    #@d
    .line 593
    add-int/lit8 v7, p3, -0x1

    #@f
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v5

    #@13
    .line 596
    .local v5, lastChild:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@16
    move-result v4

    #@17
    .line 598
    .local v4, lastBottom:I
    iget v7, p0, Landroid/view/View;->mBottom:I

    #@19
    iget v8, p0, Landroid/view/View;->mTop:I

    #@1b
    sub-int/2addr v7, v8

    #@1c
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1e
    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    #@20
    sub-int v1, v7, v8

    #@22
    .line 602
    .local v1, end:I
    sub-int v0, v1, v4

    #@24
    .line 604
    .local v0, bottomOffset:I
    const/4 v7, 0x0

    #@25
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    .line 605
    .local v2, firstChild:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@2c
    move-result v3

    #@2d
    .line 609
    .local v3, firstTop:I
    if-lez v0, :cond_60

    #@2f
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@31
    if-gtz v7, :cond_39

    #@33
    iget-object v7, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@35
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@37
    if-ge v3, v7, :cond_60

    #@39
    .line 610
    :cond_39
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3b
    if-nez v7, :cond_46

    #@3d
    .line 612
    iget-object v7, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3f
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@41
    sub-int/2addr v7, v3

    #@42
    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    #@45
    move-result v0

    #@46
    .line 616
    :cond_46
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@49
    .line 617
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4b
    if-lez v7, :cond_60

    #@4d
    .line 620
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@4f
    iget-boolean v8, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@51
    if-eqz v8, :cond_54

    #@53
    const/4 p1, 0x1

    #@54
    .end local p1
    :cond_54
    sub-int/2addr v7, p1

    #@55
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@58
    move-result v8

    #@59
    sub-int/2addr v8, p2

    #@5a
    invoke-direct {p0, v7, v8}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@5d
    .line 623
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@60
    .line 627
    .end local v0           #bottomOffset:I
    .end local v1           #end:I
    .end local v2           #firstChild:Landroid/view/View;
    .end local v3           #firstTop:I
    .end local v4           #lastBottom:I
    .end local v5           #lastChild:Landroid/view/View;
    :cond_60
    return-void
.end method

.method private correctTooLow(III)V
    .registers 14
    .parameter "numColumns"
    .parameter "verticalSpacing"
    .parameter "childCount"

    #@0
    .prologue
    .line 630
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    if-nez v8, :cond_61

    #@4
    if-lez p3, :cond_61

    #@6
    .line 632
    const/4 v8, 0x0

    #@7
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 635
    .local v1, firstChild:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@e
    move-result v2

    #@f
    .line 638
    .local v2, firstTop:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@11
    iget v6, v8, Landroid/graphics/Rect;->top:I

    #@13
    .line 641
    .local v6, start:I
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@15
    iget v9, p0, Landroid/view/View;->mTop:I

    #@17
    sub-int/2addr v8, v9

    #@18
    iget-object v9, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1a
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@1c
    sub-int v0, v8, v9

    #@1e
    .line 645
    .local v0, end:I
    sub-int v7, v2, v6

    #@20
    .line 646
    .local v7, topOffset:I
    add-int/lit8 v8, p3, -0x1

    #@22
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@25
    move-result-object v4

    #@26
    .line 647
    .local v4, lastChild:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@29
    move-result v3

    #@2a
    .line 648
    .local v3, lastBottom:I
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2c
    add-int/2addr v8, p3

    #@2d
    add-int/lit8 v5, v8, -0x1

    #@2f
    .line 652
    .local v5, lastPosition:I
    if-lez v7, :cond_61

    #@31
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@33
    add-int/lit8 v8, v8, -0x1

    #@35
    if-lt v5, v8, :cond_39

    #@37
    if-le v3, v0, :cond_61

    #@39
    .line 653
    :cond_39
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3b
    add-int/lit8 v8, v8, -0x1

    #@3d
    if-ne v5, v8, :cond_45

    #@3f
    .line 655
    sub-int v8, v3, v0

    #@41
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    #@44
    move-result v7

    #@45
    .line 659
    :cond_45
    neg-int v8, v7

    #@46
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@49
    .line 660
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@4b
    add-int/lit8 v8, v8, -0x1

    #@4d
    if-ge v5, v8, :cond_61

    #@4f
    .line 663
    iget-boolean v8, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@51
    if-nez v8, :cond_54

    #@53
    const/4 p1, 0x1

    #@54
    .end local p1
    :cond_54
    add-int v8, v5, p1

    #@56
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    #@59
    move-result v9

    #@5a
    add-int/2addr v9, p2

    #@5b
    invoke-direct {p0, v8, v9}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@5e
    .line 666
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@61
    .line 670
    .end local v0           #end:I
    .end local v1           #firstChild:Landroid/view/View;
    .end local v2           #firstTop:I
    .end local v3           #lastBottom:I
    .end local v4           #lastChild:Landroid/view/View;
    .end local v5           #lastPosition:I
    .end local v6           #start:I
    .end local v7           #topOffset:I
    :cond_61
    return-void
.end method

.method private determineColumns(I)Z
    .registers 10
    .parameter "availableSpace"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 939
    iget v2, p0, Landroid/widget/GridView;->mRequestedHorizontalSpacing:I

    #@3
    .line 940
    .local v2, requestedHorizontalSpacing:I
    iget v4, p0, Landroid/widget/GridView;->mStretchMode:I

    #@5
    .line 941
    .local v4, stretchMode:I
    iget v1, p0, Landroid/widget/GridView;->mRequestedColumnWidth:I

    #@7
    .line 942
    .local v1, requestedColumnWidth:I
    const/4 v0, 0x0

    #@8
    .line 944
    .local v0, didNotInitiallyFit:Z
    iget v5, p0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@a
    const/4 v6, -0x1

    #@b
    if-ne v5, v6, :cond_36

    #@d
    .line 945
    if-lez v1, :cond_32

    #@f
    .line 947
    add-int v5, p1, v2

    #@11
    add-int v6, v1, v2

    #@13
    div-int/2addr v5, v6

    #@14
    iput v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@16
    .line 958
    :goto_16
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@18
    if-gtz v5, :cond_1c

    #@1a
    .line 959
    iput v7, p0, Landroid/widget/GridView;->mNumColumns:I

    #@1c
    .line 962
    :cond_1c
    packed-switch v4, :pswitch_data_74

    #@1f
    .line 970
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@21
    mul-int/2addr v5, v1

    #@22
    sub-int v5, p1, v5

    #@24
    iget v6, p0, Landroid/widget/GridView;->mNumColumns:I

    #@26
    add-int/lit8 v6, v6, -0x1

    #@28
    mul-int/2addr v6, v2

    #@29
    sub-int v3, v5, v6

    #@2b
    .line 973
    .local v3, spaceLeftOver:I
    if-gez v3, :cond_2e

    #@2d
    .line 974
    const/4 v0, 0x1

    #@2e
    .line 977
    :cond_2e
    packed-switch v4, :pswitch_data_7a

    #@31
    .line 1009
    .end local v3           #spaceLeftOver:I
    :goto_31
    return v0

    #@32
    .line 951
    :cond_32
    const/4 v5, 0x2

    #@33
    iput v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@35
    goto :goto_16

    #@36
    .line 955
    :cond_36
    iget v5, p0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@38
    iput v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@3a
    goto :goto_16

    #@3b
    .line 965
    :pswitch_3b
    iput v1, p0, Landroid/widget/GridView;->mColumnWidth:I

    #@3d
    .line 966
    iput v2, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@3f
    goto :goto_31

    #@40
    .line 980
    .restart local v3       #spaceLeftOver:I
    :pswitch_40
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@42
    div-int v5, v3, v5

    #@44
    add-int/2addr v5, v1

    #@45
    iput v5, p0, Landroid/widget/GridView;->mColumnWidth:I

    #@47
    .line 981
    iput v2, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@49
    goto :goto_31

    #@4a
    .line 986
    :pswitch_4a
    iput v1, p0, Landroid/widget/GridView;->mColumnWidth:I

    #@4c
    .line 987
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@4e
    if-le v5, v7, :cond_5a

    #@50
    .line 988
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@52
    add-int/lit8 v5, v5, -0x1

    #@54
    div-int v5, v3, v5

    #@56
    add-int/2addr v5, v2

    #@57
    iput v5, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@59
    goto :goto_31

    #@5a
    .line 991
    :cond_5a
    add-int v5, v2, v3

    #@5c
    iput v5, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@5e
    goto :goto_31

    #@5f
    .line 997
    :pswitch_5f
    iput v1, p0, Landroid/widget/GridView;->mColumnWidth:I

    #@61
    .line 998
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@63
    if-le v5, v7, :cond_6f

    #@65
    .line 999
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@67
    add-int/lit8 v5, v5, 0x1

    #@69
    div-int v5, v3, v5

    #@6b
    add-int/2addr v5, v2

    #@6c
    iput v5, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@6e
    goto :goto_31

    #@6f
    .line 1002
    :cond_6f
    add-int v5, v2, v3

    #@71
    iput v5, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@73
    goto :goto_31

    #@74
    .line 962
    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_3b
    .end packed-switch

    #@7a
    .line 977
    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_40
        :pswitch_5f
    .end packed-switch
.end method

.method private fillDown(II)Landroid/view/View;
    .registers 9
    .parameter "pos"
    .parameter "nextTop"

    #@0
    .prologue
    .line 275
    const/4 v1, 0x0

    #@1
    .line 277
    .local v1, selectedView:Landroid/view/View;
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@3
    iget v4, p0, Landroid/view/View;->mTop:I

    #@5
    sub-int v0, v3, v4

    #@7
    .line 278
    .local v0, end:I
    iget v3, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@9
    and-int/lit8 v3, v3, 0x22

    #@b
    const/16 v4, 0x22

    #@d
    if-ne v3, v4, :cond_14

    #@f
    .line 279
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@11
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@13
    sub-int/2addr v0, v3

    #@14
    .line 282
    :cond_14
    :goto_14
    if-ge p2, v0, :cond_30

    #@16
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@18
    if-ge p1, v3, :cond_30

    #@1a
    .line 283
    const/4 v3, 0x1

    #@1b
    invoke-direct {p0, p1, p2, v3}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@1e
    move-result-object v2

    #@1f
    .line 284
    .local v2, temp:Landroid/view/View;
    if-eqz v2, :cond_22

    #@21
    .line 285
    move-object v1, v2

    #@22
    .line 290
    :cond_22
    iget-object v3, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@24
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@27
    move-result v3

    #@28
    iget v4, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@2a
    add-int p2, v3, v4

    #@2c
    .line 292
    iget v3, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2e
    add-int/2addr p1, v3

    #@2f
    .line 293
    goto :goto_14

    #@30
    .line 295
    .end local v2           #temp:Landroid/view/View;
    :cond_30
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@32
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@34
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@37
    move-result v5

    #@38
    add-int/2addr v4, v5

    #@39
    add-int/lit8 v4, v4, -0x1

    #@3b
    invoke-virtual {p0, v3, v4}, Landroid/widget/GridView;->setVisibleRangeHint(II)V

    #@3e
    .line 296
    return-object v1
.end method

.method private fillFromBottom(II)Landroid/view/View;
    .registers 6
    .parameter "lastPosition"
    .parameter "nextBottom"

    #@0
    .prologue
    .line 421
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    #@5
    move-result p1

    #@6
    .line 422
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    #@d
    move-result p1

    #@e
    .line 424
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@10
    add-int/lit8 v1, v1, -0x1

    #@12
    sub-int v0, v1, p1

    #@14
    .line 425
    .local v0, invertedPosition:I
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@16
    add-int/lit8 v1, v1, -0x1

    #@18
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@1a
    rem-int v2, v0, v2

    #@1c
    sub-int v2, v0, v2

    #@1e
    sub-int p1, v1, v2

    #@20
    .line 427
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@23
    move-result-object v1

    #@24
    return-object v1
.end method

.method private fillFromSelection(III)Landroid/view/View;
    .registers 18
    .parameter "selectedTop"
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 683
    invoke-virtual {p0}, Landroid/widget/GridView;->getVerticalFadingEdgeLength()I

    #@3
    move-result v2

    #@4
    .line 684
    .local v2, fadingEdgeLength:I
    iget v9, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    .line 685
    .local v9, selectedPosition:I
    iget v4, p0, Landroid/widget/GridView;->mNumColumns:I

    #@8
    .line 686
    .local v4, numColumns:I
    iget v11, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@a
    .line 689
    .local v11, verticalSpacing:I
    const/4 v6, -0x1

    #@b
    .line 691
    .local v6, rowEnd:I
    iget-boolean v12, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@d
    if-nez v12, :cond_4f

    #@f
    .line 692
    rem-int v12, v9, v4

    #@11
    sub-int v7, v9, v12

    #@13
    .line 703
    .local v7, rowStart:I
    :goto_13
    move/from16 v0, p2

    #@15
    invoke-direct {p0, v0, v2, v7}, Landroid/widget/GridView;->getTopSelectionPixel(III)I

    #@18
    move-result v10

    #@19
    .line 704
    .local v10, topSelectionPixel:I
    move/from16 v0, p3

    #@1b
    invoke-direct {p0, v0, v2, v4, v7}, Landroid/widget/GridView;->getBottomSelectionPixel(IIII)I

    #@1e
    move-result v1

    #@1f
    .line 707
    .local v1, bottomSelectionPixel:I
    iget-boolean v12, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@21
    if-eqz v12, :cond_69

    #@23
    move v12, v6

    #@24
    :goto_24
    const/4 v13, 0x1

    #@25
    invoke-direct {p0, v12, p1, v13}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@28
    move-result-object v8

    #@29
    .line 709
    .local v8, sel:Landroid/view/View;
    iput v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2b
    .line 711
    iget-object v5, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@2d
    .line 712
    .local v5, referenceView:Landroid/view/View;
    invoke-direct {p0, v5, v10, v1}, Landroid/widget/GridView;->adjustForTopFadingEdge(Landroid/view/View;II)V

    #@30
    .line 713
    invoke-direct {p0, v5, v10, v1}, Landroid/widget/GridView;->adjustForBottomFadingEdge(Landroid/view/View;II)V

    #@33
    .line 715
    iget-boolean v12, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@35
    if-nez v12, :cond_6b

    #@37
    .line 716
    sub-int v12, v7, v4

    #@39
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@3c
    move-result v13

    #@3d
    sub-int/2addr v13, v11

    #@3e
    invoke-direct {p0, v12, v13}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@41
    .line 717
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@44
    .line 718
    add-int v12, v7, v4

    #@46
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@49
    move-result v13

    #@4a
    add-int/2addr v13, v11

    #@4b
    invoke-direct {p0, v12, v13}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@4e
    .line 726
    :goto_4e
    return-object v8

    #@4f
    .line 694
    .end local v1           #bottomSelectionPixel:I
    .end local v5           #referenceView:Landroid/view/View;
    .end local v7           #rowStart:I
    .end local v8           #sel:Landroid/view/View;
    .end local v10           #topSelectionPixel:I
    :cond_4f
    iget v12, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@51
    add-int/lit8 v12, v12, -0x1

    #@53
    sub-int v3, v12, v9

    #@55
    .line 696
    .local v3, invertedSelection:I
    iget v12, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@57
    add-int/lit8 v12, v12, -0x1

    #@59
    rem-int v13, v3, v4

    #@5b
    sub-int v13, v3, v13

    #@5d
    sub-int v6, v12, v13

    #@5f
    .line 697
    const/4 v12, 0x0

    #@60
    sub-int v13, v6, v4

    #@62
    add-int/lit8 v13, v13, 0x1

    #@64
    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    #@67
    move-result v7

    #@68
    .restart local v7       #rowStart:I
    goto :goto_13

    #@69
    .end local v3           #invertedSelection:I
    .restart local v1       #bottomSelectionPixel:I
    .restart local v10       #topSelectionPixel:I
    :cond_69
    move v12, v7

    #@6a
    .line 707
    goto :goto_24

    #@6b
    .line 720
    .restart local v5       #referenceView:Landroid/view/View;
    .restart local v8       #sel:Landroid/view/View;
    :cond_6b
    add-int v12, v6, v4

    #@6d
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@70
    move-result v13

    #@71
    add-int/2addr v13, v11

    #@72
    invoke-direct {p0, v12, v13}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@75
    .line 721
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@78
    .line 722
    add-int/lit8 v12, v7, -0x1

    #@7a
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@7d
    move-result v13

    #@7e
    sub-int/2addr v13, v11

    #@7f
    invoke-direct {p0, v12, v13}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@82
    goto :goto_4e
.end method

.method private fillFromTop(I)Landroid/view/View;
    .registers 5
    .parameter "nextTop"

    #@0
    .prologue
    .line 411
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@4
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@a
    .line 412
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@e
    add-int/lit8 v1, v1, -0x1

    #@10
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@16
    .line 413
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@18
    if-gez v0, :cond_1d

    #@1a
    .line 414
    const/4 v0, 0x0

    #@1b
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1d
    .line 416
    :cond_1d
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1f
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@21
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@23
    rem-int/2addr v1, v2

    #@24
    sub-int/2addr v0, v1

    #@25
    iput v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@27
    .line 417
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@29
    invoke-direct {p0, v0, p1}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method

.method private fillSelection(II)Landroid/view/View;
    .registers 18
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 431
    invoke-virtual {p0}, Landroid/widget/GridView;->reconcileSelectedPosition()I

    #@3
    move-result v10

    #@4
    .line 432
    .local v10, selectedPosition:I
    iget v4, p0, Landroid/widget/GridView;->mNumColumns:I

    #@6
    .line 433
    .local v4, numColumns:I
    iget v12, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@8
    .line 436
    .local v12, verticalSpacing:I
    const/4 v7, -0x1

    #@9
    .line 438
    .local v7, rowEnd:I
    iget-boolean v13, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@b
    if-nez v13, :cond_4a

    #@d
    .line 439
    rem-int v13, v10, v4

    #@f
    sub-int v8, v10, v13

    #@11
    .line 447
    .local v8, rowStart:I
    :goto_11
    invoke-virtual {p0}, Landroid/widget/GridView;->getVerticalFadingEdgeLength()I

    #@14
    move-result v2

    #@15
    .line 448
    .local v2, fadingEdgeLength:I
    move/from16 v0, p1

    #@17
    invoke-direct {p0, v0, v2, v8}, Landroid/widget/GridView;->getTopSelectionPixel(III)I

    #@1a
    move-result v11

    #@1b
    .line 450
    .local v11, topSelectionPixel:I
    iget-boolean v13, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@1d
    if-eqz v13, :cond_64

    #@1f
    move v13, v7

    #@20
    :goto_20
    const/4 v14, 0x1

    #@21
    invoke-direct {p0, v13, v11, v14}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@24
    move-result-object v9

    #@25
    .line 451
    .local v9, sel:Landroid/view/View;
    iput v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@27
    .line 453
    iget-object v6, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@29
    .line 455
    .local v6, referenceView:Landroid/view/View;
    iget-boolean v13, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@2b
    if-nez v13, :cond_66

    #@2d
    .line 456
    add-int v13, v8, v4

    #@2f
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@32
    move-result v14

    #@33
    add-int/2addr v14, v12

    #@34
    invoke-direct {p0, v13, v14}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@37
    .line 457
    move/from16 v0, p2

    #@39
    invoke-direct {p0, v0}, Landroid/widget/GridView;->pinToBottom(I)V

    #@3c
    .line 458
    sub-int v13, v8, v4

    #@3e
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@41
    move-result v14

    #@42
    sub-int/2addr v14, v12

    #@43
    invoke-direct {p0, v13, v14}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@46
    .line 459
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@49
    .line 471
    :goto_49
    return-object v9

    #@4a
    .line 441
    .end local v2           #fadingEdgeLength:I
    .end local v6           #referenceView:Landroid/view/View;
    .end local v8           #rowStart:I
    .end local v9           #sel:Landroid/view/View;
    .end local v11           #topSelectionPixel:I
    :cond_4a
    iget v13, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@4c
    add-int/lit8 v13, v13, -0x1

    #@4e
    sub-int v3, v13, v10

    #@50
    .line 443
    .local v3, invertedSelection:I
    iget v13, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@52
    add-int/lit8 v13, v13, -0x1

    #@54
    rem-int v14, v3, v4

    #@56
    sub-int v14, v3, v14

    #@58
    sub-int v7, v13, v14

    #@5a
    .line 444
    const/4 v13, 0x0

    #@5b
    sub-int v14, v7, v4

    #@5d
    add-int/lit8 v14, v14, 0x1

    #@5f
    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    #@62
    move-result v8

    #@63
    .restart local v8       #rowStart:I
    goto :goto_11

    #@64
    .end local v3           #invertedSelection:I
    .restart local v2       #fadingEdgeLength:I
    .restart local v11       #topSelectionPixel:I
    :cond_64
    move v13, v8

    #@65
    .line 450
    goto :goto_20

    #@66
    .line 461
    .restart local v6       #referenceView:Landroid/view/View;
    .restart local v9       #sel:Landroid/view/View;
    :cond_66
    move/from16 v0, p2

    #@68
    invoke-direct {p0, v0, v2, v4, v8}, Landroid/widget/GridView;->getBottomSelectionPixel(IIII)I

    #@6b
    move-result v1

    #@6c
    .line 463
    .local v1, bottomSelectionPixel:I
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@6f
    move-result v13

    #@70
    sub-int v5, v1, v13

    #@72
    .line 464
    .local v5, offset:I
    invoke-virtual {p0, v5}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@75
    .line 465
    add-int/lit8 v13, v8, -0x1

    #@77
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@7a
    move-result v14

    #@7b
    sub-int/2addr v14, v12

    #@7c
    invoke-direct {p0, v13, v14}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@7f
    .line 466
    invoke-direct/range {p0 .. p1}, Landroid/widget/GridView;->pinToTop(I)V

    #@82
    .line 467
    add-int v13, v7, v4

    #@84
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@87
    move-result v14

    #@88
    add-int/2addr v14, v12

    #@89
    invoke-direct {p0, v13, v14}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@8c
    .line 468
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@8f
    goto :goto_49
.end method

.method private fillSpecific(II)Landroid/view/View;
    .registers 15
    .parameter "position"
    .parameter "top"

    #@0
    .prologue
    .line 529
    iget v6, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2
    .line 532
    .local v6, numColumns:I
    const/4 v4, -0x1

    #@3
    .line 534
    .local v4, motionRowEnd:I
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@5
    if-nez v10, :cond_1d

    #@7
    .line 535
    rem-int v10, p1, v6

    #@9
    sub-int v5, p1, v10

    #@b
    .line 543
    .local v5, motionRowStart:I
    :goto_b
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@d
    if-eqz v10, :cond_37

    #@f
    move v10, v4

    #@10
    :goto_10
    const/4 v11, 0x1

    #@11
    invoke-direct {p0, v10, p2, v11}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@14
    move-result-object v8

    #@15
    .line 546
    .local v8, temp:Landroid/view/View;
    iput v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@17
    .line 548
    iget-object v7, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@19
    .line 550
    .local v7, referenceView:Landroid/view/View;
    if-nez v7, :cond_39

    #@1b
    .line 551
    const/4 v8, 0x0

    #@1c
    .line 584
    .end local v8           #temp:Landroid/view/View;
    :cond_1c
    :goto_1c
    return-object v8

    #@1d
    .line 537
    .end local v5           #motionRowStart:I
    .end local v7           #referenceView:Landroid/view/View;
    :cond_1d
    iget v10, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1f
    add-int/lit8 v10, v10, -0x1

    #@21
    sub-int v3, v10, p1

    #@23
    .line 539
    .local v3, invertedSelection:I
    iget v10, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@25
    add-int/lit8 v10, v10, -0x1

    #@27
    rem-int v11, v3, v6

    #@29
    sub-int v11, v3, v11

    #@2b
    sub-int v4, v10, v11

    #@2d
    .line 540
    const/4 v10, 0x0

    #@2e
    sub-int v11, v4, v6

    #@30
    add-int/lit8 v11, v11, 0x1

    #@32
    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    #@35
    move-result v5

    #@36
    .restart local v5       #motionRowStart:I
    goto :goto_b

    #@37
    .end local v3           #invertedSelection:I
    :cond_37
    move v10, v5

    #@38
    .line 543
    goto :goto_10

    #@39
    .line 554
    .restart local v7       #referenceView:Landroid/view/View;
    .restart local v8       #temp:Landroid/view/View;
    :cond_39
    iget v9, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@3b
    .line 559
    .local v9, verticalSpacing:I
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@3d
    if-nez v10, :cond_67

    #@3f
    .line 560
    sub-int v10, v5, v6

    #@41
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@44
    move-result v11

    #@45
    sub-int/2addr v11, v9

    #@46
    invoke-direct {p0, v10, v11}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@49
    move-result-object v0

    #@4a
    .line 561
    .local v0, above:Landroid/view/View;
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@4d
    .line 562
    add-int v10, v5, v6

    #@4f
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@52
    move-result v11

    #@53
    add-int/2addr v11, v9

    #@54
    invoke-direct {p0, v10, v11}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@57
    move-result-object v1

    #@58
    .line 564
    .local v1, below:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@5b
    move-result v2

    #@5c
    .line 565
    .local v2, childCount:I
    if-lez v2, :cond_61

    #@5e
    .line 566
    invoke-direct {p0, v6, v9, v2}, Landroid/widget/GridView;->correctTooHigh(III)V

    #@61
    .line 579
    :cond_61
    :goto_61
    if-nez v8, :cond_1c

    #@63
    .line 581
    if-eqz v0, :cond_8a

    #@65
    move-object v8, v0

    #@66
    .line 582
    goto :goto_1c

    #@67
    .line 569
    .end local v0           #above:Landroid/view/View;
    .end local v1           #below:Landroid/view/View;
    .end local v2           #childCount:I
    :cond_67
    add-int v10, v4, v6

    #@69
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@6c
    move-result v11

    #@6d
    add-int/2addr v11, v9

    #@6e
    invoke-direct {p0, v10, v11}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@71
    move-result-object v1

    #@72
    .line 570
    .restart local v1       #below:Landroid/view/View;
    invoke-direct {p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@75
    .line 571
    add-int/lit8 v10, v5, -0x1

    #@77
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@7a
    move-result v11

    #@7b
    sub-int/2addr v11, v9

    #@7c
    invoke-direct {p0, v10, v11}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@7f
    move-result-object v0

    #@80
    .line 573
    .restart local v0       #above:Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@83
    move-result v2

    #@84
    .line 574
    .restart local v2       #childCount:I
    if-lez v2, :cond_61

    #@86
    .line 575
    invoke-direct {p0, v6, v9, v2}, Landroid/widget/GridView;->correctTooLow(III)V

    #@89
    goto :goto_61

    #@8a
    :cond_8a
    move-object v8, v1

    #@8b
    .line 584
    goto :goto_1c
.end method

.method private fillUp(II)Landroid/view/View;
    .registers 9
    .parameter "pos"
    .parameter "nextBottom"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 373
    const/4 v1, 0x0

    #@2
    .line 375
    .local v1, selectedView:Landroid/view/View;
    const/4 v0, 0x0

    #@3
    .line 376
    .local v0, end:I
    iget v3, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5
    and-int/lit8 v3, v3, 0x22

    #@7
    const/16 v4, 0x22

    #@9
    if-ne v3, v4, :cond_f

    #@b
    .line 377
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@d
    iget v0, v3, Landroid/graphics/Rect;->top:I

    #@f
    .line 380
    :cond_f
    :goto_f
    if-le p2, v0, :cond_2a

    #@11
    if-ltz p1, :cond_2a

    #@13
    .line 382
    invoke-direct {p0, p1, p2, v5}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@16
    move-result-object v2

    #@17
    .line 383
    .local v2, temp:Landroid/view/View;
    if-eqz v2, :cond_1a

    #@19
    .line 384
    move-object v1, v2

    #@1a
    .line 387
    :cond_1a
    iget-object v3, p0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@1c
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@1f
    move-result v3

    #@20
    iget v4, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@22
    sub-int p2, v3, v4

    #@24
    .line 389
    iput p1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@26
    .line 391
    iget v3, p0, Landroid/widget/GridView;->mNumColumns:I

    #@28
    sub-int/2addr p1, v3

    #@29
    .line 392
    goto :goto_f

    #@2a
    .line 394
    .end local v2           #temp:Landroid/view/View;
    :cond_2a
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@2c
    if-eqz v3, :cond_36

    #@2e
    .line 395
    add-int/lit8 v3, p1, 0x1

    #@30
    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    #@33
    move-result v3

    #@34
    iput v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@36
    .line 398
    :cond_36
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@38
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3a
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@3d
    move-result v5

    #@3e
    add-int/2addr v4, v5

    #@3f
    add-int/lit8 v4, v4, -0x1

    #@41
    invoke-virtual {p0, v3, v4}, Landroid/widget/GridView;->setVisibleRangeHint(II)V

    #@44
    .line 399
    return-object v1
.end method

.method private getBottomSelectionPixel(IIII)I
    .registers 8
    .parameter "childrenBottom"
    .parameter "fadingEdgeLength"
    .parameter "numColumns"
    .parameter "rowStart"

    #@0
    .prologue
    .line 741
    move v0, p1

    #@1
    .line 742
    .local v0, bottomSelectionPixel:I
    add-int v1, p4, p3

    #@3
    add-int/lit8 v1, v1, -0x1

    #@5
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    add-int/lit8 v2, v2, -0x1

    #@9
    if-ge v1, v2, :cond_c

    #@b
    .line 743
    sub-int/2addr v0, p2

    #@c
    .line 745
    :cond_c
    return v0
.end method

.method private getTopSelectionPixel(III)I
    .registers 5
    .parameter "childrenTop"
    .parameter "fadingEdgeLength"
    .parameter "rowStart"

    #@0
    .prologue
    .line 758
    move v0, p1

    #@1
    .line 759
    .local v0, topSelectionPixel:I
    if-lez p3, :cond_4

    #@3
    .line 760
    add-int/2addr v0, p2

    #@4
    .line 762
    :cond_4
    return v0
.end method

.method private isCandidateSelection(II)Z
    .registers 11
    .parameter "childIndex"
    .parameter "direction"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1890
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 1891
    .local v0, count:I
    add-int/lit8 v6, v0, -0x1

    #@8
    sub-int v1, v6, p1

    #@a
    .line 1896
    .local v1, invertedIndex:I
    iget-boolean v6, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@c
    if-nez v6, :cond_28

    #@e
    .line 1897
    iget v6, p0, Landroid/widget/GridView;->mNumColumns:I

    #@10
    rem-int v6, p1, v6

    #@12
    sub-int v3, p1, v6

    #@14
    .line 1898
    .local v3, rowStart:I
    iget v6, p0, Landroid/widget/GridView;->mNumColumns:I

    #@16
    add-int/2addr v6, v3

    #@17
    add-int/lit8 v6, v6, -0x1

    #@19
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    #@1c
    move-result v2

    #@1d
    .line 1904
    .local v2, rowEnd:I
    :goto_1d
    sparse-switch p2, :sswitch_data_5e

    #@20
    .line 1925
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@22
    const-string v5, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    #@24
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v4

    #@28
    .line 1900
    .end local v2           #rowEnd:I
    .end local v3           #rowStart:I
    :cond_28
    add-int/lit8 v6, v0, -0x1

    #@2a
    iget v7, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2c
    rem-int v7, v1, v7

    #@2e
    sub-int v7, v1, v7

    #@30
    sub-int v2, v6, v7

    #@32
    .line 1901
    .restart local v2       #rowEnd:I
    iget v6, p0, Landroid/widget/GridView;->mNumColumns:I

    #@34
    sub-int v6, v2, v6

    #@36
    add-int/lit8 v6, v6, 0x1

    #@38
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@3b
    move-result v3

    #@3c
    .restart local v3       #rowStart:I
    goto :goto_1d

    #@3d
    .line 1908
    :sswitch_3d
    if-ne p1, v3, :cond_40

    #@3f
    .line 1923
    :cond_3f
    :goto_3f
    return v4

    #@40
    :cond_40
    move v4, v5

    #@41
    .line 1908
    goto :goto_3f

    #@42
    .line 1911
    :sswitch_42
    if-eqz v3, :cond_3f

    #@44
    move v4, v5

    #@45
    goto :goto_3f

    #@46
    .line 1914
    :sswitch_46
    if-eq p1, v2, :cond_3f

    #@48
    move v4, v5

    #@49
    goto :goto_3f

    #@4a
    .line 1917
    :sswitch_4a
    add-int/lit8 v6, v0, -0x1

    #@4c
    if-eq v2, v6, :cond_3f

    #@4e
    move v4, v5

    #@4f
    goto :goto_3f

    #@50
    .line 1920
    :sswitch_50
    if-ne p1, v3, :cond_54

    #@52
    if-eqz v3, :cond_3f

    #@54
    :cond_54
    move v4, v5

    #@55
    goto :goto_3f

    #@56
    .line 1923
    :sswitch_56
    if-ne p1, v2, :cond_5c

    #@58
    add-int/lit8 v6, v0, -0x1

    #@5a
    if-eq v2, v6, :cond_3f

    #@5c
    :cond_5c
    move v4, v5

    #@5d
    goto :goto_3f

    #@5e
    .line 1904
    :sswitch_data_5e
    .sparse-switch
        0x1 -> :sswitch_56
        0x2 -> :sswitch_50
        0x11 -> :sswitch_46
        0x21 -> :sswitch_4a
        0x42 -> :sswitch_3d
        0x82 -> :sswitch_42
    .end sparse-switch
.end method

.method private makeAndAddView(IIZIZI)Landroid/view/View;
    .registers 17
    .parameter "position"
    .parameter "y"
    .parameter "flow"
    .parameter "childrenLeft"
    .parameter "selected"
    .parameter "where"

    #@0
    .prologue
    .line 1328
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    .line 1330
    iget-object v0, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView$RecycleBin;->getActiveView(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    .line 1331
    .local v1, child:Landroid/view/View;
    if-eqz v1, :cond_1a

    #@c
    .line 1334
    const/4 v7, 0x1

    #@d
    move-object v0, p0

    #@e
    move v2, p1

    #@f
    move v3, p2

    #@10
    move v4, p3

    #@11
    move v5, p4

    #@12
    move v6, p5

    #@13
    move/from16 v8, p6

    #@15
    invoke-direct/range {v0 .. v8}, Landroid/widget/GridView;->setupChild(Landroid/view/View;IIZIZZI)V

    #@18
    move-object v9, v1

    #@19
    .line 1346
    .end local v1           #child:Landroid/view/View;
    .local v9, child:Landroid/view/View;
    :goto_19
    return-object v9

    #@1a
    .line 1341
    .end local v9           #child:Landroid/view/View;
    :cond_1a
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@1c
    invoke-virtual {p0, p1, v0}, Landroid/widget/GridView;->obtainView(I[Z)Landroid/view/View;

    #@1f
    move-result-object v1

    #@20
    .line 1344
    .restart local v1       #child:Landroid/view/View;
    iget-object v0, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@22
    const/4 v2, 0x0

    #@23
    aget-boolean v7, v0, v2

    #@25
    move-object v0, p0

    #@26
    move v2, p1

    #@27
    move v3, p2

    #@28
    move v4, p3

    #@29
    move v5, p4

    #@2a
    move v6, p5

    #@2b
    move/from16 v8, p6

    #@2d
    invoke-direct/range {v0 .. v8}, Landroid/widget/GridView;->setupChild(Landroid/view/View;IIZIZZI)V

    #@30
    move-object v9, v1

    #@31
    .line 1346
    .end local v1           #child:Landroid/view/View;
    .restart local v9       #child:Landroid/view/View;
    goto :goto_19
.end method

.method private makeRow(IIZ)Landroid/view/View;
    .registers 22
    .parameter "startPos"
    .parameter "y"
    .parameter "flow"

    #@0
    .prologue
    .line 300
    move-object/from16 v0, p0

    #@2
    iget v9, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@4
    .line 301
    .local v9, columnWidth:I
    move-object/from16 v0, p0

    #@6
    iget v12, v0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@8
    .line 303
    .local v12, horizontalSpacing:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->isLayoutRtl()Z

    #@b
    move-result v14

    #@c
    .line 308
    .local v14, isLayoutRtl:Z
    if-eqz v14, :cond_7b

    #@e
    .line 309
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getWidth()I

    #@11
    move-result v1

    #@12
    move-object/from16 v0, p0

    #@14
    iget-object v3, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@16
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@18
    sub-int/2addr v1, v3

    #@19
    sub-int v3, v1, v9

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget v1, v0, Landroid/widget/GridView;->mStretchMode:I

    #@1f
    const/4 v4, 0x3

    #@20
    if-ne v1, v4, :cond_79

    #@22
    move v1, v12

    #@23
    :goto_23
    sub-int v5, v3, v1

    #@25
    .line 316
    .local v5, nextLeft:I
    :goto_25
    move-object/from16 v0, p0

    #@27
    iget-boolean v1, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@29
    if-nez v1, :cond_8e

    #@2b
    .line 317
    move-object/from16 v0, p0

    #@2d
    iget v1, v0, Landroid/widget/GridView;->mNumColumns:I

    #@2f
    add-int v1, v1, p1

    #@31
    move-object/from16 v0, p0

    #@33
    iget v3, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@35
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    #@38
    move-result v15

    #@39
    .line 328
    .local v15, last:I
    :cond_39
    :goto_39
    const/16 v17, 0x0

    #@3b
    .line 330
    .local v17, selectedView:Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->shouldShowSelector()Z

    #@3e
    move-result v11

    #@3f
    .line 331
    .local v11, hasFocus:Z
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->touchModeDrawsInPressedState()Z

    #@42
    move-result v13

    #@43
    .line 332
    .local v13, inClick:Z
    move-object/from16 v0, p0

    #@45
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@47
    move/from16 v16, v0

    #@49
    .line 334
    .local v16, selectedPosition:I
    const/4 v8, 0x0

    #@4a
    .line 335
    .local v8, child:Landroid/view/View;
    move/from16 v2, p1

    #@4c
    .local v2, pos:I
    :goto_4c
    if-ge v2, v15, :cond_c1

    #@4e
    .line 337
    move/from16 v0, v16

    #@50
    if-ne v2, v0, :cond_b8

    #@52
    const/4 v6, 0x1

    #@53
    .line 340
    .local v6, selected:Z
    :goto_53
    if-eqz p3, :cond_ba

    #@55
    const/4 v7, -0x1

    #@56
    .local v7, where:I
    :goto_56
    move-object/from16 v1, p0

    #@58
    move/from16 v3, p2

    #@5a
    move/from16 v4, p3

    #@5c
    .line 341
    invoke-direct/range {v1 .. v7}, Landroid/widget/GridView;->makeAndAddView(IIZIZI)Landroid/view/View;

    #@5f
    move-result-object v8

    #@60
    .line 343
    if-eqz v14, :cond_bd

    #@62
    const/4 v1, -0x1

    #@63
    :goto_63
    mul-int/2addr v1, v9

    #@64
    add-int/2addr v5, v1

    #@65
    .line 344
    add-int/lit8 v1, v15, -0x1

    #@67
    if-ge v2, v1, :cond_6e

    #@69
    .line 345
    if-eqz v14, :cond_bf

    #@6b
    const/4 v1, -0x1

    #@6c
    :goto_6c
    mul-int/2addr v1, v12

    #@6d
    add-int/2addr v5, v1

    #@6e
    .line 348
    :cond_6e
    if-eqz v6, :cond_76

    #@70
    if-nez v11, :cond_74

    #@72
    if-eqz v13, :cond_76

    #@74
    .line 349
    :cond_74
    move-object/from16 v17, v8

    #@76
    .line 335
    :cond_76
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_4c

    #@79
    .line 309
    .end local v2           #pos:I
    .end local v5           #nextLeft:I
    .end local v6           #selected:Z
    .end local v7           #where:I
    .end local v8           #child:Landroid/view/View;
    .end local v11           #hasFocus:Z
    .end local v13           #inClick:Z
    .end local v15           #last:I
    .end local v16           #selectedPosition:I
    .end local v17           #selectedView:Landroid/view/View;
    :cond_79
    const/4 v1, 0x0

    #@7a
    goto :goto_23

    #@7b
    .line 312
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v1, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@7f
    iget v3, v1, Landroid/graphics/Rect;->left:I

    #@81
    move-object/from16 v0, p0

    #@83
    iget v1, v0, Landroid/widget/GridView;->mStretchMode:I

    #@85
    const/4 v4, 0x3

    #@86
    if-ne v1, v4, :cond_8c

    #@88
    move v1, v12

    #@89
    :goto_89
    add-int v5, v3, v1

    #@8b
    .restart local v5       #nextLeft:I
    goto :goto_25

    #@8c
    .end local v5           #nextLeft:I
    :cond_8c
    const/4 v1, 0x0

    #@8d
    goto :goto_89

    #@8e
    .line 319
    .restart local v5       #nextLeft:I
    :cond_8e
    add-int/lit8 v15, p1, 0x1

    #@90
    .line 320
    .restart local v15       #last:I
    const/4 v1, 0x0

    #@91
    move-object/from16 v0, p0

    #@93
    iget v3, v0, Landroid/widget/GridView;->mNumColumns:I

    #@95
    sub-int v3, p1, v3

    #@97
    add-int/lit8 v3, v3, 0x1

    #@99
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    #@9c
    move-result p1

    #@9d
    .line 322
    sub-int v1, v15, p1

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v3, v0, Landroid/widget/GridView;->mNumColumns:I

    #@a3
    if-ge v1, v3, :cond_39

    #@a5
    .line 323
    move-object/from16 v0, p0

    #@a7
    iget v1, v0, Landroid/widget/GridView;->mNumColumns:I

    #@a9
    sub-int v3, v15, p1

    #@ab
    sub-int/2addr v1, v3

    #@ac
    add-int v3, v9, v12

    #@ae
    mul-int v10, v1, v3

    #@b0
    .line 324
    .local v10, deltaLeft:I
    if-eqz v14, :cond_b6

    #@b2
    const/4 v1, -0x1

    #@b3
    :goto_b3
    mul-int/2addr v1, v10

    #@b4
    add-int/2addr v5, v1

    #@b5
    goto :goto_39

    #@b6
    :cond_b6
    const/4 v1, 0x1

    #@b7
    goto :goto_b3

    #@b8
    .line 337
    .end local v10           #deltaLeft:I
    .restart local v2       #pos:I
    .restart local v8       #child:Landroid/view/View;
    .restart local v11       #hasFocus:Z
    .restart local v13       #inClick:Z
    .restart local v16       #selectedPosition:I
    .restart local v17       #selectedView:Landroid/view/View;
    :cond_b8
    const/4 v6, 0x0

    #@b9
    goto :goto_53

    #@ba
    .line 340
    .restart local v6       #selected:Z
    :cond_ba
    sub-int v7, v2, p1

    #@bc
    goto :goto_56

    #@bd
    .line 343
    .restart local v7       #where:I
    :cond_bd
    const/4 v1, 0x1

    #@be
    goto :goto_63

    #@bf
    .line 345
    :cond_bf
    const/4 v1, 0x1

    #@c0
    goto :goto_6c

    #@c1
    .line 353
    .end local v6           #selected:Z
    .end local v7           #where:I
    :cond_c1
    move-object/from16 v0, p0

    #@c3
    iput-object v8, v0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@c5
    .line 355
    if-eqz v17, :cond_cf

    #@c7
    .line 356
    move-object/from16 v0, p0

    #@c9
    iget-object v1, v0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@cb
    move-object/from16 v0, p0

    #@cd
    iput-object v1, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@cf
    .line 359
    :cond_cf
    return-object v17
.end method

.method private moveSelection(III)Landroid/view/View;
    .registers 26
    .parameter "delta"
    .parameter "childrenTop"
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 855
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getVerticalFadingEdgeLength()I

    #@3
    move-result v5

    #@4
    .line 856
    .local v5, fadingEdgeLength:I
    move-object/from16 v0, p0

    #@6
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8
    move/from16 v16, v0

    #@a
    .line 857
    .local v16, selectedPosition:I
    move-object/from16 v0, p0

    #@c
    iget v7, v0, Landroid/widget/GridView;->mNumColumns:I

    #@e
    .line 858
    .local v7, numColumns:I
    move-object/from16 v0, p0

    #@10
    iget v0, v0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@12
    move/from16 v18, v0

    #@14
    .line 862
    .local v18, verticalSpacing:I
    const/4 v13, -0x1

    #@15
    .line 864
    .local v13, rowEnd:I
    move-object/from16 v0, p0

    #@17
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@19
    move/from16 v19, v0

    #@1b
    if-nez v19, :cond_9d

    #@1d
    .line 865
    sub-int v19, v16, p1

    #@1f
    sub-int v20, v16, p1

    #@21
    rem-int v20, v20, v7

    #@23
    sub-int v9, v19, v20

    #@25
    .line 867
    .local v9, oldRowStart:I
    rem-int v19, v16, v7

    #@27
    sub-int v14, v16, v19

    #@29
    .line 879
    .local v14, rowStart:I
    :goto_29
    sub-int v12, v14, v9

    #@2b
    .line 881
    .local v12, rowDelta:I
    move-object/from16 v0, p0

    #@2d
    move/from16 v1, p2

    #@2f
    invoke-direct {v0, v1, v5, v14}, Landroid/widget/GridView;->getTopSelectionPixel(III)I

    #@32
    move-result v17

    #@33
    .line 882
    .local v17, topSelectionPixel:I
    move-object/from16 v0, p0

    #@35
    move/from16 v1, p3

    #@37
    invoke-direct {v0, v1, v5, v7, v14}, Landroid/widget/GridView;->getBottomSelectionPixel(IIII)I

    #@3a
    move-result v4

    #@3b
    .line 886
    .local v4, bottomSelectionPixel:I
    move-object/from16 v0, p0

    #@3d
    iput v14, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3f
    .line 891
    if-lez v12, :cond_f5

    #@41
    .line 896
    move-object/from16 v0, p0

    #@43
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@45
    move-object/from16 v19, v0

    #@47
    if-nez v19, :cond_e5

    #@49
    const/4 v8, 0x0

    #@4a
    .line 899
    .local v8, oldBottom:I
    :goto_4a
    move-object/from16 v0, p0

    #@4c
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@4e
    move/from16 v19, v0

    #@50
    if-eqz v19, :cond_f1

    #@52
    move/from16 v19, v13

    #@54
    :goto_54
    add-int v20, v8, v18

    #@56
    const/16 v21, 0x1

    #@58
    move-object/from16 v0, p0

    #@5a
    move/from16 v1, v19

    #@5c
    move/from16 v2, v20

    #@5e
    move/from16 v3, v21

    #@60
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@63
    move-result-object v15

    #@64
    .line 900
    .local v15, sel:Landroid/view/View;
    move-object/from16 v0, p0

    #@66
    iget-object v11, v0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@68
    .line 902
    .local v11, referenceView:Landroid/view/View;
    move-object/from16 v0, p0

    #@6a
    move/from16 v1, v17

    #@6c
    invoke-direct {v0, v11, v1, v4}, Landroid/widget/GridView;->adjustForBottomFadingEdge(Landroid/view/View;II)V

    #@6f
    .line 925
    .end local v8           #oldBottom:I
    :goto_6f
    move-object/from16 v0, p0

    #@71
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@73
    move/from16 v19, v0

    #@75
    if-nez v19, :cond_168

    #@77
    .line 926
    sub-int v19, v14, v7

    #@79
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    #@7c
    move-result v20

    #@7d
    sub-int v20, v20, v18

    #@7f
    move-object/from16 v0, p0

    #@81
    move/from16 v1, v19

    #@83
    move/from16 v2, v20

    #@85
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@88
    .line 927
    invoke-direct/range {p0 .. p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@8b
    .line 928
    add-int v19, v14, v7

    #@8d
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    #@90
    move-result v20

    #@91
    add-int v20, v20, v18

    #@93
    move-object/from16 v0, p0

    #@95
    move/from16 v1, v19

    #@97
    move/from16 v2, v20

    #@99
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@9c
    .line 935
    :goto_9c
    return-object v15

    #@9d
    .line 869
    .end local v4           #bottomSelectionPixel:I
    .end local v9           #oldRowStart:I
    .end local v11           #referenceView:Landroid/view/View;
    .end local v12           #rowDelta:I
    .end local v14           #rowStart:I
    .end local v15           #sel:Landroid/view/View;
    .end local v17           #topSelectionPixel:I
    :cond_9d
    move-object/from16 v0, p0

    #@9f
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@a1
    move/from16 v19, v0

    #@a3
    add-int/lit8 v19, v19, -0x1

    #@a5
    sub-int v6, v19, v16

    #@a7
    .line 871
    .local v6, invertedSelection:I
    move-object/from16 v0, p0

    #@a9
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@ab
    move/from16 v19, v0

    #@ad
    add-int/lit8 v19, v19, -0x1

    #@af
    rem-int v20, v6, v7

    #@b1
    sub-int v20, v6, v20

    #@b3
    sub-int v13, v19, v20

    #@b5
    .line 872
    const/16 v19, 0x0

    #@b7
    sub-int v20, v13, v7

    #@b9
    add-int/lit8 v20, v20, 0x1

    #@bb
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    #@be
    move-result v14

    #@bf
    .line 874
    .restart local v14       #rowStart:I
    move-object/from16 v0, p0

    #@c1
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@c3
    move/from16 v19, v0

    #@c5
    add-int/lit8 v19, v19, -0x1

    #@c7
    sub-int v20, v16, p1

    #@c9
    sub-int v6, v19, v20

    #@cb
    .line 875
    move-object/from16 v0, p0

    #@cd
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@cf
    move/from16 v19, v0

    #@d1
    add-int/lit8 v19, v19, -0x1

    #@d3
    rem-int v20, v6, v7

    #@d5
    sub-int v20, v6, v20

    #@d7
    sub-int v9, v19, v20

    #@d9
    .line 876
    .restart local v9       #oldRowStart:I
    const/16 v19, 0x0

    #@db
    sub-int v20, v9, v7

    #@dd
    add-int/lit8 v20, v20, 0x1

    #@df
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    #@e2
    move-result v9

    #@e3
    goto/16 :goto_29

    #@e5
    .line 896
    .end local v6           #invertedSelection:I
    .restart local v4       #bottomSelectionPixel:I
    .restart local v12       #rowDelta:I
    .restart local v17       #topSelectionPixel:I
    :cond_e5
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@e9
    move-object/from16 v19, v0

    #@eb
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getBottom()I

    #@ee
    move-result v8

    #@ef
    goto/16 :goto_4a

    #@f1
    .restart local v8       #oldBottom:I
    :cond_f1
    move/from16 v19, v14

    #@f3
    .line 899
    goto/16 :goto_54

    #@f5
    .line 903
    .end local v8           #oldBottom:I
    :cond_f5
    if-gez v12, :cond_135

    #@f7
    .line 907
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@fb
    move-object/from16 v19, v0

    #@fd
    if-nez v19, :cond_127

    #@ff
    const/4 v10, 0x0

    #@100
    .line 910
    .local v10, oldTop:I
    :goto_100
    move-object/from16 v0, p0

    #@102
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@104
    move/from16 v19, v0

    #@106
    if-eqz v19, :cond_132

    #@108
    move/from16 v19, v13

    #@10a
    :goto_10a
    sub-int v20, v10, v18

    #@10c
    const/16 v21, 0x0

    #@10e
    move-object/from16 v0, p0

    #@110
    move/from16 v1, v19

    #@112
    move/from16 v2, v20

    #@114
    move/from16 v3, v21

    #@116
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@119
    move-result-object v15

    #@11a
    .line 911
    .restart local v15       #sel:Landroid/view/View;
    move-object/from16 v0, p0

    #@11c
    iget-object v11, v0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@11e
    .line 913
    .restart local v11       #referenceView:Landroid/view/View;
    move-object/from16 v0, p0

    #@120
    move/from16 v1, v17

    #@122
    invoke-direct {v0, v11, v1, v4}, Landroid/widget/GridView;->adjustForTopFadingEdge(Landroid/view/View;II)V

    #@125
    goto/16 :goto_6f

    #@127
    .line 907
    .end local v10           #oldTop:I
    .end local v11           #referenceView:Landroid/view/View;
    .end local v15           #sel:Landroid/view/View;
    :cond_127
    move-object/from16 v0, p0

    #@129
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@12b
    move-object/from16 v19, v0

    #@12d
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getTop()I

    #@130
    move-result v10

    #@131
    goto :goto_100

    #@132
    .restart local v10       #oldTop:I
    :cond_132
    move/from16 v19, v14

    #@134
    .line 910
    goto :goto_10a

    #@135
    .line 918
    .end local v10           #oldTop:I
    :cond_135
    move-object/from16 v0, p0

    #@137
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@139
    move-object/from16 v19, v0

    #@13b
    if-nez v19, :cond_15a

    #@13d
    const/4 v10, 0x0

    #@13e
    .line 921
    .restart local v10       #oldTop:I
    :goto_13e
    move-object/from16 v0, p0

    #@140
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@142
    move/from16 v19, v0

    #@144
    if-eqz v19, :cond_165

    #@146
    move/from16 v19, v13

    #@148
    :goto_148
    const/16 v20, 0x1

    #@14a
    move-object/from16 v0, p0

    #@14c
    move/from16 v1, v19

    #@14e
    move/from16 v2, v20

    #@150
    invoke-direct {v0, v1, v10, v2}, Landroid/widget/GridView;->makeRow(IIZ)Landroid/view/View;

    #@153
    move-result-object v15

    #@154
    .line 922
    .restart local v15       #sel:Landroid/view/View;
    move-object/from16 v0, p0

    #@156
    iget-object v11, v0, Landroid/widget/GridView;->mReferenceView:Landroid/view/View;

    #@158
    .restart local v11       #referenceView:Landroid/view/View;
    goto/16 :goto_6f

    #@15a
    .line 918
    .end local v10           #oldTop:I
    .end local v11           #referenceView:Landroid/view/View;
    .end local v15           #sel:Landroid/view/View;
    :cond_15a
    move-object/from16 v0, p0

    #@15c
    iget-object v0, v0, Landroid/widget/GridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    #@15e
    move-object/from16 v19, v0

    #@160
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getTop()I

    #@163
    move-result v10

    #@164
    goto :goto_13e

    #@165
    .restart local v10       #oldTop:I
    :cond_165
    move/from16 v19, v14

    #@167
    .line 921
    goto :goto_148

    #@168
    .line 930
    .end local v10           #oldTop:I
    .restart local v11       #referenceView:Landroid/view/View;
    .restart local v15       #sel:Landroid/view/View;
    :cond_168
    add-int v19, v13, v7

    #@16a
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    #@16d
    move-result v20

    #@16e
    add-int v20, v20, v18

    #@170
    move-object/from16 v0, p0

    #@172
    move/from16 v1, v19

    #@174
    move/from16 v2, v20

    #@176
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@179
    .line 931
    invoke-direct/range {p0 .. p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@17c
    .line 932
    add-int/lit8 v19, v14, -0x1

    #@17e
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    #@181
    move-result v20

    #@182
    sub-int v20, v20, v18

    #@184
    move-object/from16 v0, p0

    #@186
    move/from16 v1, v19

    #@188
    move/from16 v2, v20

    #@18a
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@18d
    goto/16 :goto_9c
.end method

.method private pinToBottom(I)V
    .registers 7
    .parameter "childrenBottom"

    #@0
    .prologue
    .line 485
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 486
    .local v1, count:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6
    add-int/2addr v3, v1

    #@7
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@9
    if-ne v3, v4, :cond_1c

    #@b
    .line 487
    add-int/lit8 v3, v1, -0x1

    #@d
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@14
    move-result v0

    #@15
    .line 488
    .local v0, bottom:I
    sub-int v2, p1, v0

    #@17
    .line 489
    .local v2, offset:I
    if-lez v2, :cond_1c

    #@19
    .line 490
    invoke-virtual {p0, v2}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@1c
    .line 493
    .end local v0           #bottom:I
    .end local v2           #offset:I
    :cond_1c
    return-void
.end method

.method private pinToTop(I)V
    .registers 5
    .parameter "childrenTop"

    #@0
    .prologue
    .line 475
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    if-nez v2, :cond_14

    #@4
    .line 476
    const/4 v2, 0x0

    #@5
    invoke-virtual {p0, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@c
    move-result v1

    #@d
    .line 477
    .local v1, top:I
    sub-int v0, p1, v1

    #@f
    .line 478
    .local v0, offset:I
    if-gez v0, :cond_14

    #@11
    .line 479
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->offsetChildrenTopAndBottom(I)V

    #@14
    .line 482
    .end local v0           #offset:I
    .end local v1           #top:I
    :cond_14
    return-void
.end method

.method private setupChild(Landroid/view/View;IIZIZZI)V
    .registers 34
    .parameter "child"
    .parameter "position"
    .parameter "y"
    .parameter "flow"
    .parameter "childrenLeft"
    .parameter "selected"
    .parameter "recycled"
    .parameter "where"

    #@0
    .prologue
    .line 1367
    if-eqz p6, :cond_147

    #@2
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->shouldShowSelector()Z

    #@5
    move-result v22

    #@6
    if-eqz v22, :cond_147

    #@8
    const/4 v14, 0x1

    #@9
    .line 1368
    .local v14, isSelected:Z
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    #@c
    move-result v22

    #@d
    move/from16 v0, v22

    #@f
    if-eq v14, v0, :cond_14a

    #@11
    const/16 v20, 0x1

    #@13
    .line 1369
    .local v20, updateChildSelected:Z
    :goto_13
    move-object/from16 v0, p0

    #@15
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@17
    move/from16 v16, v0

    #@19
    .line 1370
    .local v16, mode:I
    if-lez v16, :cond_14e

    #@1b
    const/16 v22, 0x3

    #@1d
    move/from16 v0, v16

    #@1f
    move/from16 v1, v22

    #@21
    if-ge v0, v1, :cond_14e

    #@23
    move-object/from16 v0, p0

    #@25
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@27
    move/from16 v22, v0

    #@29
    move/from16 v0, v22

    #@2b
    move/from16 v1, p2

    #@2d
    if-ne v0, v1, :cond_14e

    #@2f
    const/4 v13, 0x1

    #@30
    .line 1372
    .local v13, isPressed:Z
    :goto_30
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isPressed()Z

    #@33
    move-result v22

    #@34
    move/from16 v0, v22

    #@36
    if-eq v13, v0, :cond_151

    #@38
    const/16 v19, 0x1

    #@3a
    .line 1374
    .local v19, updateChildPressed:Z
    :goto_3a
    if-eqz p7, :cond_44

    #@3c
    if-nez v20, :cond_44

    #@3e
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isLayoutRequested()Z

    #@41
    move-result v22

    #@42
    if-eqz v22, :cond_155

    #@44
    :cond_44
    const/16 v17, 0x1

    #@46
    .line 1378
    .local v17, needToMeasure:Z
    :goto_46
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@49
    move-result-object v18

    #@4a
    check-cast v18, Landroid/widget/AbsListView$LayoutParams;

    #@4c
    .line 1379
    .local v18, p:Landroid/widget/AbsListView$LayoutParams;
    if-nez v18, :cond_54

    #@4e
    .line 1380
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@51
    move-result-object v18

    #@52
    .end local v18           #p:Landroid/widget/AbsListView$LayoutParams;
    check-cast v18, Landroid/widget/AbsListView$LayoutParams;

    #@54
    .line 1382
    .restart local v18       #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_54
    move-object/from16 v0, p0

    #@56
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@58
    move-object/from16 v22, v0

    #@5a
    move-object/from16 v0, v22

    #@5c
    move/from16 v1, p2

    #@5e
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@61
    move-result v22

    #@62
    move/from16 v0, v22

    #@64
    move-object/from16 v1, v18

    #@66
    iput v0, v1, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@68
    .line 1384
    if-eqz p7, :cond_159

    #@6a
    move-object/from16 v0, v18

    #@6c
    iget-boolean v0, v0, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@6e
    move/from16 v22, v0

    #@70
    if-nez v22, :cond_159

    #@72
    .line 1385
    move-object/from16 v0, p0

    #@74
    move-object/from16 v1, p1

    #@76
    move/from16 v2, p8

    #@78
    move-object/from16 v3, v18

    #@7a
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/GridView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@7d
    .line 1391
    :goto_7d
    if-eqz v20, :cond_89

    #@7f
    .line 1392
    move-object/from16 v0, p1

    #@81
    invoke-virtual {v0, v14}, Landroid/view/View;->setSelected(Z)V

    #@84
    .line 1393
    if-eqz v14, :cond_89

    #@86
    .line 1394
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->requestFocus()Z

    #@89
    .line 1398
    :cond_89
    if-eqz v19, :cond_90

    #@8b
    .line 1399
    move-object/from16 v0, p1

    #@8d
    invoke-virtual {v0, v13}, Landroid/view/View;->setPressed(Z)V

    #@90
    .line 1402
    :cond_90
    move-object/from16 v0, p0

    #@92
    iget v0, v0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@94
    move/from16 v22, v0

    #@96
    if-eqz v22, :cond_bd

    #@98
    move-object/from16 v0, p0

    #@9a
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@9c
    move-object/from16 v22, v0

    #@9e
    if-eqz v22, :cond_bd

    #@a0
    .line 1403
    move-object/from16 v0, p1

    #@a2
    instance-of v0, v0, Landroid/widget/Checkable;

    #@a4
    move/from16 v22, v0

    #@a6
    if-eqz v22, :cond_172

    #@a8
    move-object/from16 v22, p1

    #@aa
    .line 1404
    check-cast v22, Landroid/widget/Checkable;

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@b0
    move-object/from16 v23, v0

    #@b2
    move-object/from16 v0, v23

    #@b4
    move/from16 v1, p2

    #@b6
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@b9
    move-result v23

    #@ba
    invoke-interface/range {v22 .. v23}, Landroid/widget/Checkable;->setChecked(Z)V

    #@bd
    .line 1411
    :cond_bd
    :goto_bd
    if-eqz v17, :cond_19f

    #@bf
    .line 1412
    const/16 v22, 0x0

    #@c1
    const/16 v23, 0x0

    #@c3
    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c6
    move-result v22

    #@c7
    const/16 v23, 0x0

    #@c9
    move-object/from16 v0, v18

    #@cb
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@cd
    move/from16 v24, v0

    #@cf
    invoke-static/range {v22 .. v24}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@d2
    move-result v7

    #@d3
    .line 1415
    .local v7, childHeightSpec:I
    move-object/from16 v0, p0

    #@d5
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@d7
    move/from16 v22, v0

    #@d9
    const/high16 v23, 0x4000

    #@db
    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@de
    move-result v22

    #@df
    const/16 v23, 0x0

    #@e1
    move-object/from16 v0, v18

    #@e3
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@e5
    move/from16 v24, v0

    #@e7
    invoke-static/range {v22 .. v24}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@ea
    move-result v11

    #@eb
    .line 1417
    .local v11, childWidthSpec:I
    move-object/from16 v0, p1

    #@ed
    invoke-virtual {v0, v11, v7}, Landroid/view/View;->measure(II)V

    #@f0
    .line 1422
    .end local v7           #childHeightSpec:I
    .end local v11           #childWidthSpec:I
    :goto_f0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    #@f3
    move-result v21

    #@f4
    .line 1423
    .local v21, w:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    #@f7
    move-result v12

    #@f8
    .line 1426
    .local v12, h:I
    if-eqz p4, :cond_1a4

    #@fa
    move/from16 v10, p3

    #@fc
    .line 1428
    .local v10, childTop:I
    :goto_fc
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getLayoutDirection()I

    #@ff
    move-result v15

    #@100
    .line 1429
    .local v15, layoutDirection:I
    move-object/from16 v0, p0

    #@102
    iget v0, v0, Landroid/widget/GridView;->mGravity:I

    #@104
    move/from16 v22, v0

    #@106
    move/from16 v0, v22

    #@108
    invoke-static {v0, v15}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@10b
    move-result v5

    #@10c
    .line 1430
    .local v5, absoluteGravity:I
    and-int/lit8 v22, v5, 0x7

    #@10e
    packed-switch v22, :pswitch_data_1e2

    #@111
    .line 1441
    :pswitch_111
    move/from16 v8, p5

    #@113
    .line 1445
    .local v8, childLeft:I
    :goto_113
    if-eqz v17, :cond_1c6

    #@115
    .line 1446
    add-int v9, v8, v21

    #@117
    .line 1447
    .local v9, childRight:I
    add-int v6, v10, v12

    #@119
    .line 1448
    .local v6, childBottom:I
    move-object/from16 v0, p1

    #@11b
    invoke-virtual {v0, v8, v10, v9, v6}, Landroid/view/View;->layout(IIII)V

    #@11e
    .line 1454
    .end local v6           #childBottom:I
    .end local v9           #childRight:I
    :goto_11e
    move-object/from16 v0, p0

    #@120
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@122
    move/from16 v22, v0

    #@124
    if-eqz v22, :cond_12f

    #@126
    .line 1455
    const/16 v22, 0x1

    #@128
    move-object/from16 v0, p1

    #@12a
    move/from16 v1, v22

    #@12c
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    #@12f
    .line 1458
    :cond_12f
    if-eqz p7, :cond_146

    #@131
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@134
    move-result-object v22

    #@135
    check-cast v22, Landroid/widget/AbsListView$LayoutParams;

    #@137
    move-object/from16 v0, v22

    #@139
    iget v0, v0, Landroid/widget/AbsListView$LayoutParams;->scrappedFromPosition:I

    #@13b
    move/from16 v22, v0

    #@13d
    move/from16 v0, v22

    #@13f
    move/from16 v1, p2

    #@141
    if-eq v0, v1, :cond_146

    #@143
    .line 1460
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@146
    .line 1462
    :cond_146
    return-void

    #@147
    .line 1367
    .end local v5           #absoluteGravity:I
    .end local v8           #childLeft:I
    .end local v10           #childTop:I
    .end local v12           #h:I
    .end local v13           #isPressed:Z
    .end local v14           #isSelected:Z
    .end local v15           #layoutDirection:I
    .end local v16           #mode:I
    .end local v17           #needToMeasure:Z
    .end local v18           #p:Landroid/widget/AbsListView$LayoutParams;
    .end local v19           #updateChildPressed:Z
    .end local v20           #updateChildSelected:Z
    .end local v21           #w:I
    :cond_147
    const/4 v14, 0x0

    #@148
    goto/16 :goto_9

    #@14a
    .line 1368
    .restart local v14       #isSelected:Z
    :cond_14a
    const/16 v20, 0x0

    #@14c
    goto/16 :goto_13

    #@14e
    .line 1370
    .restart local v16       #mode:I
    .restart local v20       #updateChildSelected:Z
    :cond_14e
    const/4 v13, 0x0

    #@14f
    goto/16 :goto_30

    #@151
    .line 1372
    .restart local v13       #isPressed:Z
    :cond_151
    const/16 v19, 0x0

    #@153
    goto/16 :goto_3a

    #@155
    .line 1374
    .restart local v19       #updateChildPressed:Z
    :cond_155
    const/16 v17, 0x0

    #@157
    goto/16 :goto_46

    #@159
    .line 1387
    .restart local v17       #needToMeasure:Z
    .restart local v18       #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_159
    const/16 v22, 0x0

    #@15b
    move/from16 v0, v22

    #@15d
    move-object/from16 v1, v18

    #@15f
    iput-boolean v0, v1, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@161
    .line 1388
    const/16 v22, 0x1

    #@163
    move-object/from16 v0, p0

    #@165
    move-object/from16 v1, p1

    #@167
    move/from16 v2, p8

    #@169
    move-object/from16 v3, v18

    #@16b
    move/from16 v4, v22

    #@16d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/GridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    #@170
    goto/16 :goto_7d

    #@172
    .line 1405
    :cond_172
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getContext()Landroid/content/Context;

    #@175
    move-result-object v22

    #@176
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@179
    move-result-object v22

    #@17a
    move-object/from16 v0, v22

    #@17c
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@17e
    move/from16 v22, v0

    #@180
    const/16 v23, 0xb

    #@182
    move/from16 v0, v22

    #@184
    move/from16 v1, v23

    #@186
    if-lt v0, v1, :cond_bd

    #@188
    .line 1407
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@18c
    move-object/from16 v22, v0

    #@18e
    move-object/from16 v0, v22

    #@190
    move/from16 v1, p2

    #@192
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@195
    move-result v22

    #@196
    move-object/from16 v0, p1

    #@198
    move/from16 v1, v22

    #@19a
    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    #@19d
    goto/16 :goto_bd

    #@19f
    .line 1419
    :cond_19f
    invoke-virtual/range {p0 .. p1}, Landroid/widget/GridView;->cleanupLayoutState(Landroid/view/View;)V

    #@1a2
    goto/16 :goto_f0

    #@1a4
    .line 1426
    .restart local v12       #h:I
    .restart local v21       #w:I
    :cond_1a4
    sub-int v10, p3, v12

    #@1a6
    goto/16 :goto_fc

    #@1a8
    .line 1432
    .restart local v5       #absoluteGravity:I
    .restart local v10       #childTop:I
    .restart local v15       #layoutDirection:I
    :pswitch_1a8
    move/from16 v8, p5

    #@1aa
    .line 1433
    .restart local v8       #childLeft:I
    goto/16 :goto_113

    #@1ac
    .line 1435
    .end local v8           #childLeft:I
    :pswitch_1ac
    move-object/from16 v0, p0

    #@1ae
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@1b0
    move/from16 v22, v0

    #@1b2
    sub-int v22, v22, v21

    #@1b4
    div-int/lit8 v22, v22, 0x2

    #@1b6
    add-int v8, p5, v22

    #@1b8
    .line 1436
    .restart local v8       #childLeft:I
    goto/16 :goto_113

    #@1ba
    .line 1438
    .end local v8           #childLeft:I
    :pswitch_1ba
    move-object/from16 v0, p0

    #@1bc
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@1be
    move/from16 v22, v0

    #@1c0
    add-int v22, v22, p5

    #@1c2
    sub-int v8, v22, v21

    #@1c4
    .line 1439
    .restart local v8       #childLeft:I
    goto/16 :goto_113

    #@1c6
    .line 1450
    :cond_1c6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    #@1c9
    move-result v22

    #@1ca
    sub-int v22, v8, v22

    #@1cc
    move-object/from16 v0, p1

    #@1ce
    move/from16 v1, v22

    #@1d0
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@1d3
    .line 1451
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@1d6
    move-result v22

    #@1d7
    sub-int v22, v10, v22

    #@1d9
    move-object/from16 v0, p1

    #@1db
    move/from16 v1, v22

    #@1dd
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@1e0
    goto/16 :goto_11e

    #@1e2
    .line 1430
    :pswitch_data_1e2
    .packed-switch 0x1
        :pswitch_1ac
        :pswitch_111
        :pswitch_1a8
        :pswitch_111
        :pswitch_1ba
    .end packed-switch
.end method


# virtual methods
.method arrowScroll(I)Z
    .registers 12
    .parameter "direction"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x6

    #@2
    .line 1725
    iget v4, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@4
    .line 1726
    .local v4, selectedPosition:I
    iget v3, p0, Landroid/widget/GridView;->mNumColumns:I

    #@6
    .line 1731
    .local v3, numColumns:I
    const/4 v2, 0x0

    #@7
    .line 1733
    .local v2, moved:Z
    iget-boolean v6, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@9
    if-nez v6, :cond_30

    #@b
    .line 1734
    div-int v6, v4, v3

    #@d
    mul-int v5, v6, v3

    #@f
    .line 1735
    .local v5, startOfRowPos:I
    add-int v6, v5, v3

    #@11
    add-int/lit8 v6, v6, -0x1

    #@13
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@15
    add-int/lit8 v7, v7, -0x1

    #@17
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    #@1a
    move-result v0

    #@1b
    .line 1742
    .local v0, endOfRowPos:I
    :goto_1b
    sparse-switch p1, :sswitch_data_90

    #@1e
    .line 1773
    :cond_1e
    :goto_1e
    if-eqz v2, :cond_2a

    #@20
    .line 1774
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    #@23
    move-result v6

    #@24
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->playSoundEffect(I)V

    #@27
    .line 1775
    invoke-virtual {p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@2a
    .line 1778
    :cond_2a
    if-eqz v2, :cond_2f

    #@2c
    .line 1779
    invoke-virtual {p0}, Landroid/widget/GridView;->awakenScrollBars()Z

    #@2f
    .line 1782
    :cond_2f
    return v2

    #@30
    .line 1737
    .end local v0           #endOfRowPos:I
    .end local v5           #startOfRowPos:I
    :cond_30
    iget v6, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@32
    add-int/lit8 v6, v6, -0x1

    #@34
    sub-int v1, v6, v4

    #@36
    .line 1738
    .local v1, invertedSelection:I
    iget v6, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@38
    add-int/lit8 v6, v6, -0x1

    #@3a
    div-int v7, v1, v3

    #@3c
    mul-int/2addr v7, v3

    #@3d
    sub-int v0, v6, v7

    #@3f
    .line 1739
    .restart local v0       #endOfRowPos:I
    sub-int v6, v0, v3

    #@41
    add-int/lit8 v6, v6, 0x1

    #@43
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    #@46
    move-result v5

    #@47
    .restart local v5       #startOfRowPos:I
    goto :goto_1b

    #@48
    .line 1744
    .end local v1           #invertedSelection:I
    :sswitch_48
    if-lez v5, :cond_1e

    #@4a
    .line 1745
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@4c
    .line 1746
    sub-int v6, v4, v3

    #@4e
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    #@51
    move-result v6

    #@52
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@55
    .line 1747
    const/4 v2, 0x1

    #@56
    goto :goto_1e

    #@57
    .line 1751
    :sswitch_57
    iget v6, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@59
    add-int/lit8 v6, v6, -0x1

    #@5b
    if-ge v0, v6, :cond_1e

    #@5d
    .line 1752
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@5f
    .line 1753
    add-int v6, v4, v3

    #@61
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@63
    add-int/lit8 v7, v7, -0x1

    #@65
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    #@68
    move-result v6

    #@69
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@6c
    .line 1754
    const/4 v2, 0x1

    #@6d
    goto :goto_1e

    #@6e
    .line 1758
    :sswitch_6e
    if-le v4, v5, :cond_1e

    #@70
    .line 1759
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@72
    .line 1760
    add-int/lit8 v6, v4, -0x1

    #@74
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    #@77
    move-result v6

    #@78
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@7b
    .line 1761
    const/4 v2, 0x1

    #@7c
    goto :goto_1e

    #@7d
    .line 1765
    :sswitch_7d
    if-ge v4, v0, :cond_1e

    #@7f
    .line 1766
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@81
    .line 1767
    add-int/lit8 v6, v4, 0x1

    #@83
    iget v7, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@85
    add-int/lit8 v7, v7, -0x1

    #@87
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    #@8a
    move-result v6

    #@8b
    invoke-virtual {p0, v6}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@8e
    .line 1768
    const/4 v2, 0x1

    #@8f
    goto :goto_1e

    #@90
    .line 1742
    :sswitch_data_90
    .sparse-switch
        0x11 -> :sswitch_6e
        0x21 -> :sswitch_48
        0x42 -> :sswitch_7d
        0x82 -> :sswitch_57
    .end sparse-switch
.end method

.method protected attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    .registers 9
    .parameter "child"
    .parameter "params"
    .parameter "index"
    .parameter "count"

    #@0
    .prologue
    .line 1103
    iget-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@2
    check-cast v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    #@4
    .line 1106
    .local v0, animationParams:Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    if-nez v0, :cond_d

    #@6
    .line 1107
    new-instance v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    #@8
    .end local v0           #animationParams:Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    invoke-direct {v0}, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;-><init>()V

    #@b
    .line 1108
    .restart local v0       #animationParams:Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    iput-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@d
    .line 1111
    :cond_d
    iput p4, v0, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->count:I

    #@f
    .line 1112
    iput p3, v0, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->index:I

    #@11
    .line 1113
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@13
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    #@15
    .line 1114
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@17
    div-int v2, p4, v2

    #@19
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@1b
    .line 1116
    iget-boolean v2, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@1d
    if-nez v2, :cond_2c

    #@1f
    .line 1117
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@21
    rem-int v2, p3, v2

    #@23
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    #@25
    .line 1118
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@27
    div-int v2, p3, v2

    #@29
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    #@2b
    .line 1125
    :goto_2b
    return-void

    #@2c
    .line 1120
    :cond_2c
    add-int/lit8 v2, p4, -0x1

    #@2e
    sub-int v1, v2, p3

    #@30
    .line 1122
    .local v1, invertedIndex:I
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@32
    add-int/lit8 v2, v2, -0x1

    #@34
    iget v3, p0, Landroid/widget/GridView;->mNumColumns:I

    #@36
    rem-int v3, v1, v3

    #@38
    sub-int/2addr v2, v3

    #@39
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    #@3b
    .line 1123
    iget v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@3d
    add-int/lit8 v2, v2, -0x1

    #@3f
    iget v3, p0, Landroid/widget/GridView;->mNumColumns:I

    #@41
    div-int v3, v1, v3

    #@43
    sub-int/2addr v2, v3

    #@44
    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    #@46
    goto :goto_2b
.end method

.method protected computeVerticalScrollExtent()I
    .registers 11

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2186
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@4
    move-result v1

    #@5
    .line 2187
    .local v1, count:I
    if-lez v1, :cond_3e

    #@7
    .line 2188
    iget v4, p0, Landroid/widget/GridView;->mNumColumns:I

    #@9
    .line 2189
    .local v4, numColumns:I
    add-int v9, v1, v4

    #@b
    add-int/lit8 v9, v9, -0x1

    #@d
    div-int v5, v9, v4

    #@f
    .line 2191
    .local v5, rowCount:I
    mul-int/lit8 v2, v5, 0x64

    #@11
    .line 2193
    .local v2, extent:I
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v7

    #@15
    .line 2194
    .local v7, view:Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@18
    move-result v6

    #@19
    .line 2195
    .local v6, top:I
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    #@1c
    move-result v3

    #@1d
    .line 2196
    .local v3, height:I
    if-lez v3, :cond_23

    #@1f
    .line 2197
    mul-int/lit8 v8, v6, 0x64

    #@21
    div-int/2addr v8, v3

    #@22
    add-int/2addr v2, v8

    #@23
    .line 2200
    :cond_23
    add-int/lit8 v8, v1, -0x1

    #@25
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@28
    move-result-object v7

    #@29
    .line 2201
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@2c
    move-result v0

    #@2d
    .line 2202
    .local v0, bottom:I
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    #@30
    move-result v3

    #@31
    .line 2203
    if-lez v3, :cond_3d

    #@33
    .line 2204
    invoke-virtual {p0}, Landroid/widget/GridView;->getHeight()I

    #@36
    move-result v8

    #@37
    sub-int v8, v0, v8

    #@39
    mul-int/lit8 v8, v8, 0x64

    #@3b
    div-int/2addr v8, v3

    #@3c
    sub-int/2addr v2, v8

    #@3d
    .line 2209
    .end local v0           #bottom:I
    .end local v2           #extent:I
    .end local v3           #height:I
    .end local v4           #numColumns:I
    .end local v5           #rowCount:I
    .end local v6           #top:I
    .end local v7           #view:Landroid/view/View;
    :cond_3d
    :goto_3d
    return v2

    #@3e
    :cond_3e
    move v2, v8

    #@3f
    goto :goto_3d
.end method

.method protected computeVerticalScrollOffset()I
    .registers 12

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2214
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3
    if-ltz v8, :cond_4d

    #@5
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@8
    move-result v8

    #@9
    if-lez v8, :cond_4d

    #@b
    .line 2215
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v5

    #@f
    .line 2216
    .local v5, view:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@12
    move-result v4

    #@13
    .line 2217
    .local v4, top:I
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    #@16
    move-result v0

    #@17
    .line 2218
    .local v0, height:I
    if-lez v0, :cond_4d

    #@19
    .line 2219
    iget v1, p0, Landroid/widget/GridView;->mNumColumns:I

    #@1b
    .line 2220
    .local v1, numColumns:I
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1d
    add-int/2addr v8, v1

    #@1e
    add-int/lit8 v8, v8, -0x1

    #@20
    div-int v3, v8, v1

    #@22
    .line 2224
    .local v3, rowCount:I
    invoke-virtual {p0}, Landroid/widget/GridView;->isStackFromBottom()Z

    #@25
    move-result v8

    #@26
    if-eqz v8, :cond_4e

    #@28
    mul-int v8, v3, v1

    #@2a
    iget v9, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2c
    sub-int v2, v8, v9

    #@2e
    .line 2226
    .local v2, oddItemsOnFirstRow:I
    :goto_2e
    iget v8, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@30
    add-int/2addr v8, v2

    #@31
    div-int v6, v8, v1

    #@33
    .line 2227
    .local v6, whichRow:I
    mul-int/lit8 v8, v6, 0x64

    #@35
    mul-int/lit8 v9, v4, 0x64

    #@37
    div-int/2addr v9, v0

    #@38
    sub-int/2addr v8, v9

    #@39
    iget v9, p0, Landroid/view/View;->mScrollY:I

    #@3b
    int-to-float v9, v9

    #@3c
    invoke-virtual {p0}, Landroid/widget/GridView;->getHeight()I

    #@3f
    move-result v10

    #@40
    int-to-float v10, v10

    #@41
    div-float/2addr v9, v10

    #@42
    int-to-float v10, v3

    #@43
    mul-float/2addr v9, v10

    #@44
    const/high16 v10, 0x42c8

    #@46
    mul-float/2addr v9, v10

    #@47
    float-to-int v9, v9

    #@48
    add-int/2addr v8, v9

    #@49
    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    #@4c
    move-result v7

    #@4d
    .line 2231
    .end local v0           #height:I
    .end local v1           #numColumns:I
    .end local v2           #oddItemsOnFirstRow:I
    .end local v3           #rowCount:I
    .end local v4           #top:I
    .end local v5           #view:Landroid/view/View;
    .end local v6           #whichRow:I
    :cond_4d
    return v7

    #@4e
    .restart local v0       #height:I
    .restart local v1       #numColumns:I
    .restart local v3       #rowCount:I
    .restart local v4       #top:I
    .restart local v5       #view:Landroid/view/View;
    :cond_4e
    move v2, v7

    #@4f
    .line 2224
    goto :goto_2e
.end method

.method protected computeVerticalScrollRange()I
    .registers 6

    #@0
    .prologue
    .line 2237
    iget v0, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2
    .line 2238
    .local v0, numColumns:I
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@4
    add-int/2addr v3, v0

    #@5
    add-int/lit8 v3, v3, -0x1

    #@7
    div-int v2, v3, v0

    #@9
    .line 2239
    .local v2, rowCount:I
    mul-int/lit8 v3, v2, 0x64

    #@b
    const/4 v4, 0x0

    #@c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@f
    move-result v1

    #@10
    .line 2240
    .local v1, result:I
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@12
    if-eqz v3, :cond_28

    #@14
    .line 2242
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@16
    int-to-float v3, v3

    #@17
    invoke-virtual {p0}, Landroid/widget/GridView;->getHeight()I

    #@1a
    move-result v4

    #@1b
    int-to-float v4, v4

    #@1c
    div-float/2addr v3, v4

    #@1d
    int-to-float v4, v2

    #@1e
    mul-float/2addr v3, v4

    #@1f
    const/high16 v4, 0x42c8

    #@21
    mul-float/2addr v3, v4

    #@22
    float-to-int v3, v3

    #@23
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    #@26
    move-result v3

    #@27
    add-int/2addr v1, v3

    #@28
    .line 2244
    :cond_28
    return v1
.end method

.method fillGap(Z)V
    .registers 11
    .parameter "down"

    #@0
    .prologue
    const/16 v8, 0x22

    #@2
    .line 227
    iget v1, p0, Landroid/widget/GridView;->mNumColumns:I

    #@4
    .line 228
    .local v1, numColumns:I
    iget v6, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@6
    .line 230
    .local v6, verticalSpacing:I
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@9
    move-result v0

    #@a
    .line 232
    .local v0, count:I
    if-eqz p1, :cond_3d

    #@c
    .line 233
    const/4 v3, 0x0

    #@d
    .line 234
    .local v3, paddingTop:I
    iget v7, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@f
    and-int/lit8 v7, v7, 0x22

    #@11
    if-ne v7, v8, :cond_17

    #@13
    .line 235
    invoke-virtual {p0}, Landroid/widget/GridView;->getListPaddingTop()I

    #@16
    move-result v3

    #@17
    .line 237
    :cond_17
    if-lez v0, :cond_3b

    #@19
    add-int/lit8 v7, v0, -0x1

    #@1b
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@22
    move-result v7

    #@23
    add-int v5, v7, v6

    #@25
    .line 239
    .local v5, startOffset:I
    :goto_25
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@27
    add-int v4, v7, v0

    #@29
    .line 240
    .local v4, position:I
    iget-boolean v7, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@2b
    if-eqz v7, :cond_30

    #@2d
    .line 241
    add-int/lit8 v7, v1, -0x1

    #@2f
    add-int/2addr v4, v7

    #@30
    .line 243
    :cond_30
    invoke-direct {p0, v4, v5}, Landroid/widget/GridView;->fillDown(II)Landroid/view/View;

    #@33
    .line 244
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@36
    move-result v7

    #@37
    invoke-direct {p0, v1, v6, v7}, Landroid/widget/GridView;->correctTooHigh(III)V

    #@3a
    .line 261
    .end local v3           #paddingTop:I
    :goto_3a
    return-void

    #@3b
    .end local v4           #position:I
    .end local v5           #startOffset:I
    .restart local v3       #paddingTop:I
    :cond_3b
    move v5, v3

    #@3c
    .line 237
    goto :goto_25

    #@3d
    .line 246
    .end local v3           #paddingTop:I
    :cond_3d
    const/4 v2, 0x0

    #@3e
    .line 247
    .local v2, paddingBottom:I
    iget v7, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@40
    and-int/lit8 v7, v7, 0x22

    #@42
    if-ne v7, v8, :cond_48

    #@44
    .line 248
    invoke-virtual {p0}, Landroid/widget/GridView;->getListPaddingBottom()I

    #@47
    move-result v2

    #@48
    .line 250
    :cond_48
    if-lez v0, :cond_67

    #@4a
    const/4 v7, 0x0

    #@4b
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@52
    move-result v7

    #@53
    sub-int v5, v7, v6

    #@55
    .line 252
    .restart local v5       #startOffset:I
    :goto_55
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@57
    .line 253
    .restart local v4       #position:I
    iget-boolean v7, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@59
    if-nez v7, :cond_6e

    #@5b
    .line 254
    sub-int/2addr v4, v1

    #@5c
    .line 258
    :goto_5c
    invoke-direct {p0, v4, v5}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@5f
    .line 259
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@62
    move-result v7

    #@63
    invoke-direct {p0, v1, v6, v7}, Landroid/widget/GridView;->correctTooLow(III)V

    #@66
    goto :goto_3a

    #@67
    .line 250
    .end local v4           #position:I
    .end local v5           #startOffset:I
    :cond_67
    invoke-virtual {p0}, Landroid/widget/GridView;->getHeight()I

    #@6a
    move-result v7

    #@6b
    sub-int v5, v7, v2

    #@6d
    goto :goto_55

    #@6e
    .line 256
    .restart local v4       #position:I
    .restart local v5       #startOffset:I
    :cond_6e
    add-int/lit8 v4, v4, -0x1

    #@70
    goto :goto_5c
.end method

.method findMotionRow(I)I
    .registers 6
    .parameter "y"

    #@0
    .prologue
    .line 497
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 498
    .local v0, childCount:I
    if-lez v0, :cond_33

    #@6
    .line 500
    iget v2, p0, Landroid/widget/GridView;->mNumColumns:I

    #@8
    .line 501
    .local v2, numColumns:I
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@a
    if-nez v3, :cond_1f

    #@c
    .line 502
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_33

    #@f
    .line 503
    invoke-virtual {p0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@16
    move-result v3

    #@17
    if-gt p1, v3, :cond_1d

    #@19
    .line 504
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1b
    add-int/2addr v3, v1

    #@1c
    .line 515
    .end local v1           #i:I
    .end local v2           #numColumns:I
    :goto_1c
    return v3

    #@1d
    .line 502
    .restart local v1       #i:I
    .restart local v2       #numColumns:I
    :cond_1d
    add-int/2addr v1, v2

    #@1e
    goto :goto_d

    #@1f
    .line 508
    .end local v1           #i:I
    :cond_1f
    add-int/lit8 v1, v0, -0x1

    #@21
    .restart local v1       #i:I
    :goto_21
    if-ltz v1, :cond_33

    #@23
    .line 509
    invoke-virtual {p0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@2a
    move-result v3

    #@2b
    if-lt p1, v3, :cond_31

    #@2d
    .line 510
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2f
    add-int/2addr v3, v1

    #@30
    goto :goto_1c

    #@31
    .line 508
    :cond_31
    sub-int/2addr v1, v2

    #@32
    goto :goto_21

    #@33
    .line 515
    .end local v1           #i:I
    .end local v2           #numColumns:I
    :cond_33
    const/4 v3, -0x1

    #@34
    goto :goto_1c
.end method

.method fullScroll(I)Z
    .registers 5
    .parameter "direction"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 1696
    const/4 v0, 0x0

    #@2
    .line 1697
    .local v0, moved:Z
    const/16 v1, 0x21

    #@4
    if-ne p1, v1, :cond_16

    #@6
    .line 1698
    iput v2, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@8
    .line 1699
    const/4 v1, 0x0

    #@9
    invoke-virtual {p0, v1}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@c
    .line 1700
    invoke-virtual {p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@f
    .line 1701
    const/4 v0, 0x1

    #@10
    .line 1709
    :cond_10
    :goto_10
    if-eqz v0, :cond_15

    #@12
    .line 1710
    invoke-virtual {p0}, Landroid/widget/GridView;->awakenScrollBars()Z

    #@15
    .line 1713
    :cond_15
    return v0

    #@16
    .line 1702
    :cond_16
    const/16 v1, 0x82

    #@18
    if-ne p1, v1, :cond_10

    #@1a
    .line 1703
    iput v2, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@1c
    .line 1704
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1e
    add-int/lit8 v1, v1, -0x1

    #@20
    invoke-virtual {p0, v1}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@23
    .line 1705
    invoke-virtual {p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@26
    .line 1706
    const/4 v0, 0x1

    #@27
    goto :goto_10
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .registers 2

    #@0
    .prologue
    .line 50
    invoke-virtual {p0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method public getColumnWidth()I
    .registers 2

    #@0
    .prologue
    .line 2089
    iget v0, p0, Landroid/widget/GridView;->mColumnWidth:I

    #@2
    return v0
.end method

.method public getGravity()I
    .registers 2

    #@0
    .prologue
    .line 1954
    iget v0, p0, Landroid/widget/GridView;->mGravity:I

    #@2
    return v0
.end method

.method public getHorizontalSpacing()I
    .registers 2

    #@0
    .prologue
    .line 1989
    iget v0, p0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@2
    return v0
.end method

.method public getNumColumns()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 2133
    iget v0, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2
    return v0
.end method

.method public getRequestedColumnWidth()I
    .registers 2

    #@0
    .prologue
    .line 2106
    iget v0, p0, Landroid/widget/GridView;->mRequestedColumnWidth:I

    #@2
    return v0
.end method

.method public getRequestedHorizontalSpacing()I
    .registers 2

    #@0
    .prologue
    .line 2009
    iget v0, p0, Landroid/widget/GridView;->mRequestedHorizontalSpacing:I

    #@2
    return v0
.end method

.method public getStretchMode()I
    .registers 2

    #@0
    .prologue
    .line 2059
    iget v0, p0, Landroid/widget/GridView;->mStretchMode:I

    #@2
    return v0
.end method

.method public getVerticalSpacing()I
    .registers 2

    #@0
    .prologue
    .line 2040
    iget v0, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@2
    return v0
.end method

.method protected layoutChildren()V
    .registers 22

    #@0
    .prologue
    .line 1129
    move-object/from16 v0, p0

    #@2
    iget-boolean v3, v0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@4
    .line 1130
    .local v3, blockLayoutRequests:Z
    if-nez v3, :cond_e

    #@6
    .line 1131
    const/16 v19, 0x1

    #@8
    move/from16 v0, v19

    #@a
    move-object/from16 v1, p0

    #@c
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@e
    .line 1135
    :cond_e
    :try_start_e
    invoke-super/range {p0 .. p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@11
    .line 1137
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->invalidate()V

    #@14
    .line 1139
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@18
    move-object/from16 v19, v0

    #@1a
    if-nez v19, :cond_2d

    #@1c
    .line 1140
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->resetList()V

    #@1f
    .line 1141
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V
    :try_end_22
    .catchall {:try_start_e .. :try_end_22} :catchall_1d7

    #@22
    .line 1303
    if-nez v3, :cond_2c

    #@24
    .line 1304
    :goto_24
    const/16 v19, 0x0

    #@26
    move/from16 v0, v19

    #@28
    move-object/from16 v1, p0

    #@2a
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@2c
    .line 1307
    :cond_2c
    return-void

    #@2d
    .line 1145
    :cond_2d
    :try_start_2d
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@31
    move-object/from16 v19, v0

    #@33
    move-object/from16 v0, v19

    #@35
    iget v7, v0, Landroid/graphics/Rect;->top:I

    #@37
    .line 1146
    .local v7, childrenTop:I
    move-object/from16 v0, p0

    #@39
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@3b
    move/from16 v19, v0

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget v0, v0, Landroid/view/View;->mTop:I

    #@41
    move/from16 v20, v0

    #@43
    sub-int v19, v19, v20

    #@45
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@49
    move-object/from16 v20, v0

    #@4b
    move-object/from16 v0, v20

    #@4d
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@4f
    move/from16 v20, v0

    #@51
    sub-int v6, v19, v20

    #@53
    .line 1148
    .local v6, childrenBottom:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getChildCount()I

    #@56
    move-result v5

    #@57
    .line 1150
    .local v5, childCount:I
    const/4 v9, 0x0

    #@58
    .line 1153
    .local v9, delta:I
    const/16 v16, 0x0

    #@5a
    .line 1154
    .local v16, oldSel:Landroid/view/View;
    const/4 v15, 0x0

    #@5b
    .line 1155
    .local v15, oldFirst:Landroid/view/View;
    const/4 v14, 0x0

    #@5c
    .line 1158
    .local v14, newSel:Landroid/view/View;
    move-object/from16 v0, p0

    #@5e
    iget v0, v0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@60
    move/from16 v19, v0

    #@62
    packed-switch v19, :pswitch_data_314

    #@65
    .line 1177
    move-object/from16 v0, p0

    #@67
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@69
    move/from16 v19, v0

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6f
    move/from16 v20, v0

    #@71
    sub-int v12, v19, v20

    #@73
    .line 1178
    .local v12, index:I
    if-ltz v12, :cond_7d

    #@75
    if-ge v12, v5, :cond_7d

    #@77
    .line 1179
    move-object/from16 v0, p0

    #@79
    invoke-virtual {v0, v12}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@7c
    move-result-object v16

    #@7d
    .line 1183
    :cond_7d
    const/16 v19, 0x0

    #@7f
    move-object/from16 v0, p0

    #@81
    move/from16 v1, v19

    #@83
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@86
    move-result-object v15

    #@87
    .line 1186
    .end local v12           #index:I
    :cond_87
    :goto_87
    :pswitch_87
    move-object/from16 v0, p0

    #@89
    iget-boolean v8, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@8b
    .line 1187
    .local v8, dataChanged:Z
    if-eqz v8, :cond_90

    #@8d
    .line 1188
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->handleDataChanged()V

    #@90
    .line 1193
    :cond_90
    move-object/from16 v0, p0

    #@92
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@94
    move/from16 v19, v0

    #@96
    if-nez v19, :cond_d1

    #@98
    .line 1194
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->resetList()V

    #@9b
    .line 1195
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@9e
    .line 1303
    if-nez v3, :cond_2c

    #@a0
    goto :goto_24

    #@a1
    .line 1160
    .end local v8           #dataChanged:Z
    :pswitch_a1
    move-object/from16 v0, p0

    #@a3
    iget v0, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@a5
    move/from16 v19, v0

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@ab
    move/from16 v20, v0

    #@ad
    sub-int v12, v19, v20

    #@af
    .line 1161
    .restart local v12       #index:I
    if-ltz v12, :cond_87

    #@b1
    if-ge v12, v5, :cond_87

    #@b3
    .line 1162
    move-object/from16 v0, p0

    #@b5
    invoke-virtual {v0, v12}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@b8
    move-result-object v14

    #@b9
    goto :goto_87

    #@ba
    .line 1171
    .end local v12           #index:I
    :pswitch_ba
    move-object/from16 v0, p0

    #@bc
    iget v0, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@be
    move/from16 v19, v0

    #@c0
    if-ltz v19, :cond_87

    #@c2
    .line 1172
    move-object/from16 v0, p0

    #@c4
    iget v0, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@c6
    move/from16 v19, v0

    #@c8
    move-object/from16 v0, p0

    #@ca
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@cc
    move/from16 v20, v0

    #@ce
    sub-int v9, v19, v20

    #@d0
    goto :goto_87

    #@d1
    .line 1199
    .restart local v8       #dataChanged:Z
    :cond_d1
    move-object/from16 v0, p0

    #@d3
    iget v0, v0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@d5
    move/from16 v19, v0

    #@d7
    move-object/from16 v0, p0

    #@d9
    move/from16 v1, v19

    #@db
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelectedPositionInt(I)V

    #@de
    .line 1203
    move-object/from16 v0, p0

    #@e0
    iget v10, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@e2
    .line 1204
    .local v10, firstPosition:I
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@e6
    move-object/from16 v17, v0

    #@e8
    .line 1206
    .local v17, recycleBin:Landroid/widget/AbsListView$RecycleBin;
    if-eqz v8, :cond_101

    #@ea
    .line 1207
    const/4 v11, 0x0

    #@eb
    .local v11, i:I
    :goto_eb
    if-ge v11, v5, :cond_106

    #@ed
    .line 1208
    move-object/from16 v0, p0

    #@ef
    invoke-virtual {v0, v11}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@f2
    move-result-object v19

    #@f3
    add-int v20, v10, v11

    #@f5
    move-object/from16 v0, v17

    #@f7
    move-object/from16 v1, v19

    #@f9
    move/from16 v2, v20

    #@fb
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@fe
    .line 1207
    add-int/lit8 v11, v11, 0x1

    #@100
    goto :goto_eb

    #@101
    .line 1211
    .end local v11           #i:I
    :cond_101
    move-object/from16 v0, v17

    #@103
    invoke-virtual {v0, v5, v10}, Landroid/widget/AbsListView$RecycleBin;->fillActiveViews(II)V

    #@106
    .line 1216
    :cond_106
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->detachAllViewsFromParent()V

    #@109
    .line 1217
    invoke-virtual/range {v17 .. v17}, Landroid/widget/AbsListView$RecycleBin;->removeSkippedScrap()V

    #@10c
    .line 1219
    move-object/from16 v0, p0

    #@10e
    iget v0, v0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@110
    move/from16 v19, v0

    #@112
    packed-switch v19, :pswitch_data_324

    #@115
    .line 1247
    if-nez v5, :cond_25e

    #@117
    .line 1248
    move-object/from16 v0, p0

    #@119
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@11b
    move/from16 v19, v0

    #@11d
    if-nez v19, :cond_234

    #@11f
    .line 1249
    move-object/from16 v0, p0

    #@121
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@123
    move-object/from16 v19, v0

    #@125
    if-eqz v19, :cond_12d

    #@127
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->isInTouchMode()Z

    #@12a
    move-result v19

    #@12b
    if-eqz v19, :cond_230

    #@12d
    :cond_12d
    const/16 v19, -0x1

    #@12f
    :goto_12f
    move-object/from16 v0, p0

    #@131
    move/from16 v1, v19

    #@133
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelectedPositionInt(I)V

    #@136
    .line 1251
    move-object/from16 v0, p0

    #@138
    invoke-direct {v0, v7}, Landroid/widget/GridView;->fillFromTop(I)Landroid/view/View;

    #@13b
    move-result-object v18

    #@13c
    .line 1273
    .end local v7           #childrenTop:I
    .local v18, sel:Landroid/view/View;
    :goto_13c
    invoke-virtual/range {v17 .. v17}, Landroid/widget/AbsListView$RecycleBin;->scrapActiveViews()V

    #@13f
    .line 1275
    if-eqz v18, :cond_2c4

    #@141
    .line 1276
    const/16 v19, -0x1

    #@143
    move-object/from16 v0, p0

    #@145
    move/from16 v1, v19

    #@147
    move-object/from16 v2, v18

    #@149
    invoke-virtual {v0, v1, v2}, Landroid/widget/GridView;->positionSelector(ILandroid/view/View;)V

    #@14c
    .line 1277
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getTop()I

    #@14f
    move-result v19

    #@150
    move/from16 v0, v19

    #@152
    move-object/from16 v1, p0

    #@154
    iput v0, v1, Landroid/widget/AbsListView;->mSelectedTop:I

    #@156
    .line 1286
    :cond_156
    :goto_156
    const/16 v19, 0x0

    #@158
    move/from16 v0, v19

    #@15a
    move-object/from16 v1, p0

    #@15c
    iput v0, v1, Landroid/widget/AbsListView;->mLayoutMode:I

    #@15e
    .line 1287
    const/16 v19, 0x0

    #@160
    move/from16 v0, v19

    #@162
    move-object/from16 v1, p0

    #@164
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mDataChanged:Z

    #@166
    .line 1288
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@16a
    move-object/from16 v19, v0

    #@16c
    if-eqz v19, :cond_183

    #@16e
    .line 1289
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@172
    move-object/from16 v19, v0

    #@174
    move-object/from16 v0, p0

    #@176
    move-object/from16 v1, v19

    #@178
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    #@17b
    .line 1290
    const/16 v19, 0x0

    #@17d
    move-object/from16 v0, v19

    #@17f
    move-object/from16 v1, p0

    #@181
    iput-object v0, v1, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@183
    .line 1292
    :cond_183
    const/16 v19, 0x0

    #@185
    move/from16 v0, v19

    #@187
    move-object/from16 v1, p0

    #@189
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mNeedSync:Z

    #@18b
    .line 1293
    move-object/from16 v0, p0

    #@18d
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@18f
    move/from16 v19, v0

    #@191
    move-object/from16 v0, p0

    #@193
    move/from16 v1, v19

    #@195
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNextSelectedPositionInt(I)V

    #@198
    .line 1295
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->updateScrollIndicators()V

    #@19b
    .line 1297
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@19f
    move/from16 v19, v0

    #@1a1
    if-lez v19, :cond_1a6

    #@1a3
    .line 1298
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->checkSelectionChanged()V

    #@1a6
    .line 1301
    :cond_1a6
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@1a9
    .line 1303
    if-nez v3, :cond_2c

    #@1ab
    goto/16 :goto_24

    #@1ad
    .line 1221
    .end local v18           #sel:Landroid/view/View;
    .restart local v7       #childrenTop:I
    :pswitch_1ad
    if-eqz v14, :cond_1bc

    #@1af
    .line 1222
    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    #@1b2
    move-result v19

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    move/from16 v1, v19

    #@1b7
    invoke-direct {v0, v1, v7, v6}, Landroid/widget/GridView;->fillFromSelection(III)Landroid/view/View;

    #@1ba
    move-result-object v18

    #@1bb
    .restart local v18       #sel:Landroid/view/View;
    goto :goto_13c

    #@1bc
    .line 1224
    .end local v18           #sel:Landroid/view/View;
    :cond_1bc
    move-object/from16 v0, p0

    #@1be
    invoke-direct {v0, v7, v6}, Landroid/widget/GridView;->fillSelection(II)Landroid/view/View;

    #@1c1
    move-result-object v18

    #@1c2
    .line 1226
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@1c4
    .line 1228
    .end local v18           #sel:Landroid/view/View;
    :pswitch_1c4
    const/16 v19, 0x0

    #@1c6
    move/from16 v0, v19

    #@1c8
    move-object/from16 v1, p0

    #@1ca
    iput v0, v1, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1cc
    .line 1229
    move-object/from16 v0, p0

    #@1ce
    invoke-direct {v0, v7}, Landroid/widget/GridView;->fillFromTop(I)Landroid/view/View;

    #@1d1
    move-result-object v18

    #@1d2
    .line 1230
    .restart local v18       #sel:Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V
    :try_end_1d5
    .catchall {:try_start_2d .. :try_end_1d5} :catchall_1d7

    #@1d5
    goto/16 :goto_13c

    #@1d7
    .line 1303
    .end local v5           #childCount:I
    .end local v6           #childrenBottom:I
    .end local v7           #childrenTop:I
    .end local v8           #dataChanged:Z
    .end local v9           #delta:I
    .end local v10           #firstPosition:I
    .end local v14           #newSel:Landroid/view/View;
    .end local v15           #oldFirst:Landroid/view/View;
    .end local v16           #oldSel:Landroid/view/View;
    .end local v17           #recycleBin:Landroid/widget/AbsListView$RecycleBin;
    .end local v18           #sel:Landroid/view/View;
    :catchall_1d7
    move-exception v19

    #@1d8
    if-nez v3, :cond_1e2

    #@1da
    .line 1304
    const/16 v20, 0x0

    #@1dc
    move/from16 v0, v20

    #@1de
    move-object/from16 v1, p0

    #@1e0
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@1e2
    .line 1303
    :cond_1e2
    throw v19

    #@1e3
    .line 1233
    .restart local v5       #childCount:I
    .restart local v6       #childrenBottom:I
    .restart local v7       #childrenTop:I
    .restart local v8       #dataChanged:Z
    .restart local v9       #delta:I
    .restart local v10       #firstPosition:I
    .restart local v14       #newSel:Landroid/view/View;
    .restart local v15       #oldFirst:Landroid/view/View;
    .restart local v16       #oldSel:Landroid/view/View;
    .restart local v17       #recycleBin:Landroid/widget/AbsListView$RecycleBin;
    :pswitch_1e3
    :try_start_1e3
    move-object/from16 v0, p0

    #@1e5
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@1e7
    move/from16 v19, v0

    #@1e9
    add-int/lit8 v19, v19, -0x1

    #@1eb
    move-object/from16 v0, p0

    #@1ed
    move/from16 v1, v19

    #@1ef
    invoke-direct {v0, v1, v6}, Landroid/widget/GridView;->fillUp(II)Landroid/view/View;

    #@1f2
    move-result-object v18

    #@1f3
    .line 1234
    .restart local v18       #sel:Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Landroid/widget/GridView;->adjustViewsUpOrDown()V

    #@1f6
    goto/16 :goto_13c

    #@1f8
    .line 1237
    .end local v18           #sel:Landroid/view/View;
    :pswitch_1f8
    move-object/from16 v0, p0

    #@1fa
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1fc
    move/from16 v19, v0

    #@1fe
    move-object/from16 v0, p0

    #@200
    iget v0, v0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@202
    move/from16 v20, v0

    #@204
    move-object/from16 v0, p0

    #@206
    move/from16 v1, v19

    #@208
    move/from16 v2, v20

    #@20a
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillSpecific(II)Landroid/view/View;

    #@20d
    move-result-object v18

    #@20e
    .line 1238
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@210
    .line 1240
    .end local v18           #sel:Landroid/view/View;
    :pswitch_210
    move-object/from16 v0, p0

    #@212
    iget v0, v0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@214
    move/from16 v19, v0

    #@216
    move-object/from16 v0, p0

    #@218
    iget v0, v0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@21a
    move/from16 v20, v0

    #@21c
    move-object/from16 v0, p0

    #@21e
    move/from16 v1, v19

    #@220
    move/from16 v2, v20

    #@222
    invoke-direct {v0, v1, v2}, Landroid/widget/GridView;->fillSpecific(II)Landroid/view/View;

    #@225
    move-result-object v18

    #@226
    .line 1241
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@228
    .line 1244
    .end local v18           #sel:Landroid/view/View;
    :pswitch_228
    move-object/from16 v0, p0

    #@22a
    invoke-direct {v0, v9, v7, v6}, Landroid/widget/GridView;->moveSelection(III)Landroid/view/View;

    #@22d
    move-result-object v18

    #@22e
    .line 1245
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@230
    .line 1249
    .end local v18           #sel:Landroid/view/View;
    :cond_230
    const/16 v19, 0x0

    #@232
    goto/16 :goto_12f

    #@234
    .line 1253
    :cond_234
    move-object/from16 v0, p0

    #@236
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@238
    move/from16 v19, v0

    #@23a
    add-int/lit8 v13, v19, -0x1

    #@23c
    .line 1254
    .local v13, last:I
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@240
    move-object/from16 v19, v0

    #@242
    if-eqz v19, :cond_24a

    #@244
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->isInTouchMode()Z

    #@247
    move-result v19

    #@248
    if-eqz v19, :cond_25b

    #@24a
    :cond_24a
    const/16 v19, -0x1

    #@24c
    :goto_24c
    move-object/from16 v0, p0

    #@24e
    move/from16 v1, v19

    #@250
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelectedPositionInt(I)V

    #@253
    .line 1256
    move-object/from16 v0, p0

    #@255
    invoke-direct {v0, v13, v6}, Landroid/widget/GridView;->fillFromBottom(II)Landroid/view/View;

    #@258
    move-result-object v18

    #@259
    .line 1257
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@25b
    .end local v18           #sel:Landroid/view/View;
    :cond_25b
    move/from16 v19, v13

    #@25d
    .line 1254
    goto :goto_24c

    #@25e
    .line 1259
    .end local v13           #last:I
    :cond_25e
    move-object/from16 v0, p0

    #@260
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@262
    move/from16 v19, v0

    #@264
    if-ltz v19, :cond_28f

    #@266
    move-object/from16 v0, p0

    #@268
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@26a
    move/from16 v19, v0

    #@26c
    move-object/from16 v0, p0

    #@26e
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@270
    move/from16 v20, v0

    #@272
    move/from16 v0, v19

    #@274
    move/from16 v1, v20

    #@276
    if-ge v0, v1, :cond_28f

    #@278
    .line 1260
    move-object/from16 v0, p0

    #@27a
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@27c
    move/from16 v19, v0

    #@27e
    if-nez v16, :cond_28a

    #@280
    .end local v7           #childrenTop:I
    :goto_280
    move-object/from16 v0, p0

    #@282
    move/from16 v1, v19

    #@284
    invoke-direct {v0, v1, v7}, Landroid/widget/GridView;->fillSpecific(II)Landroid/view/View;

    #@287
    move-result-object v18

    #@288
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@28a
    .end local v18           #sel:Landroid/view/View;
    .restart local v7       #childrenTop:I
    :cond_28a
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getTop()I

    #@28d
    move-result v7

    #@28e
    goto :goto_280

    #@28f
    .line 1262
    :cond_28f
    move-object/from16 v0, p0

    #@291
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@293
    move/from16 v19, v0

    #@295
    move-object/from16 v0, p0

    #@297
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@299
    move/from16 v20, v0

    #@29b
    move/from16 v0, v19

    #@29d
    move/from16 v1, v20

    #@29f
    if-ge v0, v1, :cond_2b8

    #@2a1
    .line 1263
    move-object/from16 v0, p0

    #@2a3
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2a5
    move/from16 v19, v0

    #@2a7
    if-nez v15, :cond_2b3

    #@2a9
    .end local v7           #childrenTop:I
    :goto_2a9
    move-object/from16 v0, p0

    #@2ab
    move/from16 v1, v19

    #@2ad
    invoke-direct {v0, v1, v7}, Landroid/widget/GridView;->fillSpecific(II)Landroid/view/View;

    #@2b0
    move-result-object v18

    #@2b1
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@2b3
    .end local v18           #sel:Landroid/view/View;
    .restart local v7       #childrenTop:I
    :cond_2b3
    invoke-virtual {v15}, Landroid/view/View;->getTop()I

    #@2b6
    move-result v7

    #@2b7
    goto :goto_2a9

    #@2b8
    .line 1266
    :cond_2b8
    const/16 v19, 0x0

    #@2ba
    move-object/from16 v0, p0

    #@2bc
    move/from16 v1, v19

    #@2be
    invoke-direct {v0, v1, v7}, Landroid/widget/GridView;->fillSpecific(II)Landroid/view/View;

    #@2c1
    move-result-object v18

    #@2c2
    .restart local v18       #sel:Landroid/view/View;
    goto/16 :goto_13c

    #@2c4
    .line 1278
    .end local v7           #childrenTop:I
    :cond_2c4
    move-object/from16 v0, p0

    #@2c6
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@2c8
    move/from16 v19, v0

    #@2ca
    if-lez v19, :cond_301

    #@2cc
    move-object/from16 v0, p0

    #@2ce
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@2d0
    move/from16 v19, v0

    #@2d2
    const/16 v20, 0x3

    #@2d4
    move/from16 v0, v19

    #@2d6
    move/from16 v1, v20

    #@2d8
    if-ge v0, v1, :cond_301

    #@2da
    .line 1279
    move-object/from16 v0, p0

    #@2dc
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@2de
    move/from16 v19, v0

    #@2e0
    move-object/from16 v0, p0

    #@2e2
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2e4
    move/from16 v20, v0

    #@2e6
    sub-int v19, v19, v20

    #@2e8
    move-object/from16 v0, p0

    #@2ea
    move/from16 v1, v19

    #@2ec
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@2ef
    move-result-object v4

    #@2f0
    .line 1280
    .local v4, child:Landroid/view/View;
    if-eqz v4, :cond_156

    #@2f2
    move-object/from16 v0, p0

    #@2f4
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@2f6
    move/from16 v19, v0

    #@2f8
    move-object/from16 v0, p0

    #@2fa
    move/from16 v1, v19

    #@2fc
    invoke-virtual {v0, v1, v4}, Landroid/widget/GridView;->positionSelector(ILandroid/view/View;)V

    #@2ff
    goto/16 :goto_156

    #@301
    .line 1282
    .end local v4           #child:Landroid/view/View;
    :cond_301
    const/16 v19, 0x0

    #@303
    move/from16 v0, v19

    #@305
    move-object/from16 v1, p0

    #@307
    iput v0, v1, Landroid/widget/AbsListView;->mSelectedTop:I

    #@309
    .line 1283
    move-object/from16 v0, p0

    #@30b
    iget-object v0, v0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@30d
    move-object/from16 v19, v0

    #@30f
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->setEmpty()V
    :try_end_312
    .catchall {:try_start_1e3 .. :try_end_312} :catchall_1d7

    #@312
    goto/16 :goto_156

    #@314
    .line 1158
    :pswitch_data_314
    .packed-switch 0x1
        :pswitch_87
        :pswitch_a1
        :pswitch_87
        :pswitch_87
        :pswitch_87
        :pswitch_ba
    .end packed-switch

    #@324
    .line 1219
    :pswitch_data_324
    .packed-switch 0x1
        :pswitch_1c4
        :pswitch_1ad
        :pswitch_1e3
        :pswitch_1f8
        :pswitch_210
        :pswitch_228
    .end packed-switch
.end method

.method lookForSelectablePosition(IZ)I
    .registers 6
    .parameter "position"
    .parameter "lookDown"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 211
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    .line 212
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-eqz v0, :cond_b

    #@5
    invoke-virtual {p0}, Landroid/widget/GridView;->isInTouchMode()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_d

    #@b
    :cond_b
    move p1, v1

    #@c
    .line 219
    .end local p1
    :cond_c
    :goto_c
    return p1

    #@d
    .line 216
    .restart local p1
    :cond_d
    if-ltz p1, :cond_13

    #@f
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@11
    if-lt p1, v2, :cond_c

    #@13
    :cond_13
    move p1, v1

    #@14
    .line 217
    goto :goto_c
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 13
    .parameter "gainFocus"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 1845
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 1847
    const/4 v1, -0x1

    #@4
    .line 1848
    .local v1, closestChildIndex:I
    if-eqz p1, :cond_37

    #@6
    if-eqz p3, :cond_37

    #@8
    .line 1849
    iget v7, p0, Landroid/view/View;->mScrollX:I

    #@a
    iget v8, p0, Landroid/view/View;->mScrollY:I

    #@c
    invoke-virtual {p3, v7, v8}, Landroid/graphics/Rect;->offset(II)V

    #@f
    .line 1853
    iget-object v6, p0, Landroid/widget/GridView;->mTempRect:Landroid/graphics/Rect;

    #@11
    .line 1854
    .local v6, otherRect:Landroid/graphics/Rect;
    const v4, 0x7fffffff

    #@14
    .line 1855
    .local v4, minDistance:I
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@17
    move-result v0

    #@18
    .line 1856
    .local v0, childCount:I
    const/4 v3, 0x0

    #@19
    .local v3, i:I
    :goto_19
    if-ge v3, v0, :cond_37

    #@1b
    .line 1858
    invoke-direct {p0, v3, p2}, Landroid/widget/GridView;->isCandidateSelection(II)Z

    #@1e
    move-result v7

    #@1f
    if-nez v7, :cond_24

    #@21
    .line 1856
    :cond_21
    :goto_21
    add-int/lit8 v3, v3, 0x1

    #@23
    goto :goto_19

    #@24
    .line 1862
    :cond_24
    invoke-virtual {p0, v3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    #@27
    move-result-object v5

    #@28
    .line 1863
    .local v5, other:Landroid/view/View;
    invoke-virtual {v5, v6}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@2b
    .line 1864
    invoke-virtual {p0, v5, v6}, Landroid/widget/GridView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@2e
    .line 1865
    invoke-static {p3, v6, p2}, Landroid/widget/GridView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    #@31
    move-result v2

    #@32
    .line 1867
    .local v2, distance:I
    if-ge v2, v4, :cond_21

    #@34
    .line 1868
    move v4, v2

    #@35
    .line 1869
    move v1, v3

    #@36
    goto :goto_21

    #@37
    .line 1874
    .end local v0           #childCount:I
    .end local v2           #distance:I
    .end local v3           #i:I
    .end local v4           #minDistance:I
    .end local v5           #other:Landroid/view/View;
    .end local v6           #otherRect:Landroid/graphics/Rect;
    :cond_37
    if-ltz v1, :cond_40

    #@39
    .line 1875
    iget v7, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3b
    add-int/2addr v7, v1

    #@3c
    invoke-virtual {p0, v7}, Landroid/widget/GridView;->setSelection(I)V

    #@3f
    .line 1879
    :goto_3f
    return-void

    #@40
    .line 1877
    :cond_40
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayout()V

    #@43
    goto :goto_3f
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2249
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 2250
    const-class v0, Landroid/widget/GridView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 2251
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 2255
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 2256
    const-class v0, Landroid/widget/GridView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 2257
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1518
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/GridView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 1523
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1528
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0, p2}, Landroid/widget/GridView;->commonKey(IILandroid/view/KeyEvent;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected onMeasure(II)V
    .registers 25
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 1015
    invoke-super/range {p0 .. p2}, Landroid/widget/AbsListView;->onMeasure(II)V

    #@3
    .line 1017
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@6
    move-result v17

    #@7
    .line 1018
    .local v17, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@a
    move-result v11

    #@b
    .line 1019
    .local v11, heightMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@e
    move-result v18

    #@f
    .line 1020
    .local v18, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@12
    move-result v12

    #@13
    .line 1022
    .local v12, heightSize:I
    if-nez v17, :cond_45

    #@15
    .line 1023
    move-object/from16 v0, p0

    #@17
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@19
    move/from16 v19, v0

    #@1b
    if-lez v19, :cond_1e6

    #@1d
    .line 1024
    move-object/from16 v0, p0

    #@1f
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@21
    move/from16 v19, v0

    #@23
    move-object/from16 v0, p0

    #@25
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@27
    move-object/from16 v20, v0

    #@29
    move-object/from16 v0, v20

    #@2b
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@2d
    move/from16 v20, v0

    #@2f
    add-int v19, v19, v20

    #@31
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@35
    move-object/from16 v20, v0

    #@37
    move-object/from16 v0, v20

    #@39
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@3b
    move/from16 v20, v0

    #@3d
    add-int v18, v19, v20

    #@3f
    .line 1028
    :goto_3f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getVerticalScrollbarWidth()I

    #@42
    move-result v19

    #@43
    add-int v18, v18, v19

    #@45
    .line 1031
    :cond_45
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@49
    move-object/from16 v19, v0

    #@4b
    move-object/from16 v0, v19

    #@4d
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4f
    move/from16 v19, v0

    #@51
    sub-int v19, v18, v19

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@57
    move-object/from16 v20, v0

    #@59
    move-object/from16 v0, v20

    #@5b
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@5d
    move/from16 v20, v0

    #@5f
    sub-int v7, v19, v20

    #@61
    .line 1032
    .local v7, childWidth:I
    move-object/from16 v0, p0

    #@63
    invoke-direct {v0, v7}, Landroid/widget/GridView;->determineColumns(I)Z

    #@66
    move-result v10

    #@67
    .line 1034
    .local v10, didNotInitiallyFit:Z
    const/4 v4, 0x0

    #@68
    .line 1035
    .local v4, childHeight:I
    const/4 v6, 0x0

    #@69
    .line 1037
    .local v6, childState:I
    move-object/from16 v0, p0

    #@6b
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@6d
    move-object/from16 v19, v0

    #@6f
    if-nez v19, :cond_202

    #@71
    const/16 v19, 0x0

    #@73
    :goto_73
    move/from16 v0, v19

    #@75
    move-object/from16 v1, p0

    #@77
    iput v0, v1, Landroid/widget/AdapterView;->mItemCount:I

    #@79
    .line 1038
    move-object/from16 v0, p0

    #@7b
    iget v9, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@7d
    .line 1039
    .local v9, count:I
    if-lez v9, :cond_11c

    #@7f
    .line 1040
    const/16 v19, 0x0

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@85
    move-object/from16 v20, v0

    #@87
    move-object/from16 v0, p0

    #@89
    move/from16 v1, v19

    #@8b
    move-object/from16 v2, v20

    #@8d
    invoke-virtual {v0, v1, v2}, Landroid/widget/GridView;->obtainView(I[Z)Landroid/view/View;

    #@90
    move-result-object v3

    #@91
    .line 1042
    .local v3, child:Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@94
    move-result-object v16

    #@95
    check-cast v16, Landroid/widget/AbsListView$LayoutParams;

    #@97
    .line 1043
    .local v16, p:Landroid/widget/AbsListView$LayoutParams;
    if-nez v16, :cond_a4

    #@99
    .line 1044
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9c
    move-result-object v16

    #@9d
    .end local v16           #p:Landroid/widget/AbsListView$LayoutParams;
    check-cast v16, Landroid/widget/AbsListView$LayoutParams;

    #@9f
    .line 1045
    .restart local v16       #p:Landroid/widget/AbsListView$LayoutParams;
    move-object/from16 v0, v16

    #@a1
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@a4
    .line 1047
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@a8
    move-object/from16 v19, v0

    #@aa
    const/16 v20, 0x0

    #@ac
    invoke-interface/range {v19 .. v20}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@af
    move-result v19

    #@b0
    move/from16 v0, v19

    #@b2
    move-object/from16 v1, v16

    #@b4
    iput v0, v1, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@b6
    .line 1048
    const/16 v19, 0x1

    #@b8
    move/from16 v0, v19

    #@ba
    move-object/from16 v1, v16

    #@bc
    iput-boolean v0, v1, Landroid/widget/AbsListView$LayoutParams;->forceAdd:Z

    #@be
    .line 1050
    const/16 v19, 0x0

    #@c0
    const/16 v20, 0x0

    #@c2
    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@c5
    move-result v19

    #@c6
    const/16 v20, 0x0

    #@c8
    move-object/from16 v0, v16

    #@ca
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@cc
    move/from16 v21, v0

    #@ce
    invoke-static/range {v19 .. v21}, Landroid/widget/GridView;->getChildMeasureSpec(III)I

    #@d1
    move-result v5

    #@d2
    .line 1052
    .local v5, childHeightSpec:I
    move-object/from16 v0, p0

    #@d4
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@d6
    move/from16 v19, v0

    #@d8
    const/high16 v20, 0x4000

    #@da
    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@dd
    move-result v19

    #@de
    const/16 v20, 0x0

    #@e0
    move-object/from16 v0, v16

    #@e2
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@e4
    move/from16 v21, v0

    #@e6
    invoke-static/range {v19 .. v21}, Landroid/widget/GridView;->getChildMeasureSpec(III)I

    #@e9
    move-result v8

    #@ea
    .line 1054
    .local v8, childWidthSpec:I
    invoke-virtual {v3, v8, v5}, Landroid/view/View;->measure(II)V

    #@ed
    .line 1056
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@f0
    move-result v4

    #@f1
    .line 1057
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    #@f4
    move-result v19

    #@f5
    move/from16 v0, v19

    #@f7
    invoke-static {v6, v0}, Landroid/widget/GridView;->combineMeasuredStates(II)I

    #@fa
    move-result v6

    #@fb
    .line 1059
    move-object/from16 v0, p0

    #@fd
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@ff
    move-object/from16 v19, v0

    #@101
    move-object/from16 v0, v16

    #@103
    iget v0, v0, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@105
    move/from16 v20, v0

    #@107
    invoke-virtual/range {v19 .. v20}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@10a
    move-result v19

    #@10b
    if-eqz v19, :cond_11c

    #@10d
    .line 1060
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@111
    move-object/from16 v19, v0

    #@113
    const/16 v20, -0x1

    #@115
    move-object/from16 v0, v19

    #@117
    move/from16 v1, v20

    #@119
    invoke-virtual {v0, v3, v1}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@11c
    .line 1064
    .end local v3           #child:Landroid/view/View;
    .end local v5           #childHeightSpec:I
    .end local v8           #childWidthSpec:I
    .end local v16           #p:Landroid/widget/AbsListView$LayoutParams;
    :cond_11c
    if-nez v11, :cond_142

    #@11e
    .line 1065
    move-object/from16 v0, p0

    #@120
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@122
    move-object/from16 v19, v0

    #@124
    move-object/from16 v0, v19

    #@126
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@128
    move/from16 v19, v0

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@12e
    move-object/from16 v20, v0

    #@130
    move-object/from16 v0, v20

    #@132
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@134
    move/from16 v20, v0

    #@136
    add-int v19, v19, v20

    #@138
    add-int v19, v19, v4

    #@13a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/GridView;->getVerticalFadingEdgeLength()I

    #@13d
    move-result v20

    #@13e
    mul-int/lit8 v20, v20, 0x2

    #@140
    add-int v12, v19, v20

    #@142
    .line 1069
    :cond_142
    const/high16 v19, -0x8000

    #@144
    move/from16 v0, v19

    #@146
    if-ne v11, v0, :cond_17c

    #@148
    .line 1070
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@14c
    move-object/from16 v19, v0

    #@14e
    move-object/from16 v0, v19

    #@150
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@152
    move/from16 v19, v0

    #@154
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@158
    move-object/from16 v20, v0

    #@15a
    move-object/from16 v0, v20

    #@15c
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@15e
    move/from16 v20, v0

    #@160
    add-int v15, v19, v20

    #@162
    .line 1072
    .local v15, ourSize:I
    move-object/from16 v0, p0

    #@164
    iget v14, v0, Landroid/widget/GridView;->mNumColumns:I

    #@166
    .line 1073
    .local v14, numColumns:I
    const/4 v13, 0x0

    #@167
    .local v13, i:I
    :goto_167
    if-ge v13, v9, :cond_17b

    #@169
    .line 1074
    add-int/2addr v15, v4

    #@16a
    .line 1075
    add-int v19, v13, v14

    #@16c
    move/from16 v0, v19

    #@16e
    if-ge v0, v9, :cond_178

    #@170
    .line 1076
    move-object/from16 v0, p0

    #@172
    iget v0, v0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@174
    move/from16 v19, v0

    #@176
    add-int v15, v15, v19

    #@178
    .line 1078
    :cond_178
    if-lt v15, v12, :cond_20e

    #@17a
    .line 1079
    move v15, v12

    #@17b
    .line 1083
    :cond_17b
    move v12, v15

    #@17c
    .line 1086
    .end local v13           #i:I
    .end local v14           #numColumns:I
    .end local v15           #ourSize:I
    :cond_17c
    const/high16 v19, -0x8000

    #@17e
    move/from16 v0, v17

    #@180
    move/from16 v1, v19

    #@182
    if-ne v0, v1, :cond_1d8

    #@184
    move-object/from16 v0, p0

    #@186
    iget v0, v0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@188
    move/from16 v19, v0

    #@18a
    const/16 v20, -0x1

    #@18c
    move/from16 v0, v19

    #@18e
    move/from16 v1, v20

    #@190
    if-eq v0, v1, :cond_1d8

    #@192
    .line 1087
    move-object/from16 v0, p0

    #@194
    iget v0, v0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@196
    move/from16 v19, v0

    #@198
    move-object/from16 v0, p0

    #@19a
    iget v0, v0, Landroid/widget/GridView;->mColumnWidth:I

    #@19c
    move/from16 v20, v0

    #@19e
    mul-int v19, v19, v20

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    iget v0, v0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@1a4
    move/from16 v20, v0

    #@1a6
    add-int/lit8 v20, v20, -0x1

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget v0, v0, Landroid/widget/GridView;->mHorizontalSpacing:I

    #@1ac
    move/from16 v21, v0

    #@1ae
    mul-int v20, v20, v21

    #@1b0
    add-int v19, v19, v20

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1b6
    move-object/from16 v20, v0

    #@1b8
    move-object/from16 v0, v20

    #@1ba
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@1bc
    move/from16 v20, v0

    #@1be
    add-int v19, v19, v20

    #@1c0
    move-object/from16 v0, p0

    #@1c2
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1c4
    move-object/from16 v20, v0

    #@1c6
    move-object/from16 v0, v20

    #@1c8
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@1ca
    move/from16 v20, v0

    #@1cc
    add-int v15, v19, v20

    #@1ce
    .line 1090
    .restart local v15       #ourSize:I
    move/from16 v0, v18

    #@1d0
    if-gt v15, v0, :cond_1d4

    #@1d2
    if-eqz v10, :cond_1d8

    #@1d4
    .line 1091
    :cond_1d4
    const/high16 v19, 0x100

    #@1d6
    or-int v18, v18, v19

    #@1d8
    .line 1095
    .end local v15           #ourSize:I
    :cond_1d8
    move-object/from16 v0, p0

    #@1da
    move/from16 v1, v18

    #@1dc
    invoke-virtual {v0, v1, v12}, Landroid/widget/GridView;->setMeasuredDimension(II)V

    #@1df
    .line 1096
    move/from16 v0, p1

    #@1e1
    move-object/from16 v1, p0

    #@1e3
    iput v0, v1, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@1e5
    .line 1097
    return-void

    #@1e6
    .line 1026
    .end local v4           #childHeight:I
    .end local v6           #childState:I
    .end local v7           #childWidth:I
    .end local v9           #count:I
    .end local v10           #didNotInitiallyFit:Z
    :cond_1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1ea
    move-object/from16 v19, v0

    #@1ec
    move-object/from16 v0, v19

    #@1ee
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@1f0
    move/from16 v19, v0

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1f6
    move-object/from16 v20, v0

    #@1f8
    move-object/from16 v0, v20

    #@1fa
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@1fc
    move/from16 v20, v0

    #@1fe
    add-int v18, v19, v20

    #@200
    goto/16 :goto_3f

    #@202
    .line 1037
    .restart local v4       #childHeight:I
    .restart local v6       #childState:I
    .restart local v7       #childWidth:I
    .restart local v10       #didNotInitiallyFit:Z
    :cond_202
    move-object/from16 v0, p0

    #@204
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@206
    move-object/from16 v19, v0

    #@208
    invoke-interface/range {v19 .. v19}, Landroid/widget/ListAdapter;->getCount()I

    #@20b
    move-result v19

    #@20c
    goto/16 :goto_73

    #@20e
    .line 1073
    .restart local v9       #count:I
    .restart local v13       #i:I
    .restart local v14       #numColumns:I
    .restart local v15       #ourSize:I
    :cond_20e
    add-int/2addr v13, v14

    #@20f
    goto/16 :goto_167
.end method

.method pageScroll(I)Z
    .registers 7
    .parameter "direction"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1670
    const/4 v0, -0x1

    #@2
    .line 1672
    .local v0, nextPage:I
    const/16 v2, 0x21

    #@4
    if-ne p1, v2, :cond_1e

    #@6
    .line 1673
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@8
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@b
    move-result v3

    #@c
    sub-int/2addr v2, v3

    #@d
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v0

    #@11
    .line 1678
    :cond_11
    :goto_11
    if-ltz v0, :cond_1d

    #@13
    .line 1679
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@16
    .line 1680
    invoke-virtual {p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@19
    .line 1681
    invoke-virtual {p0}, Landroid/widget/GridView;->awakenScrollBars()Z

    #@1c
    .line 1682
    const/4 v1, 0x1

    #@1d
    .line 1685
    :cond_1d
    return v1

    #@1e
    .line 1674
    :cond_1e
    const/16 v2, 0x82

    #@20
    if-ne p1, v2, :cond_11

    #@22
    .line 1675
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@24
    add-int/lit8 v2, v2, -0x1

    #@26
    iget v3, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@28
    invoke-virtual {p0}, Landroid/widget/GridView;->getChildCount()I

    #@2b
    move-result v4

    #@2c
    add-int/2addr v3, v4

    #@2d
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@30
    move-result v0

    #@31
    goto :goto_11
.end method

.method sequenceScroll(I)Z
    .registers 15
    .parameter "direction"

    #@0
    .prologue
    const/4 v12, 0x6

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 1790
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@5
    .line 1791
    .local v5, selectedPosition:I
    iget v4, p0, Landroid/widget/GridView;->mNumColumns:I

    #@7
    .line 1792
    .local v4, numColumns:I
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@9
    .line 1796
    .local v0, count:I
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@b
    if-nez v10, :cond_32

    #@d
    .line 1797
    div-int v10, v5, v4

    #@f
    mul-int v7, v10, v4

    #@11
    .line 1798
    .local v7, startOfRow:I
    add-int v10, v7, v4

    #@13
    add-int/lit8 v10, v10, -0x1

    #@15
    add-int/lit8 v11, v0, -0x1

    #@17
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    #@1a
    move-result v1

    #@1b
    .line 1805
    .local v1, endOfRow:I
    :goto_1b
    const/4 v3, 0x0

    #@1c
    .line 1806
    .local v3, moved:Z
    const/4 v6, 0x0

    #@1d
    .line 1807
    .local v6, showScroll:Z
    packed-switch p1, :pswitch_data_68

    #@20
    .line 1831
    :cond_20
    :goto_20
    if-eqz v3, :cond_2c

    #@22
    .line 1832
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    #@25
    move-result v8

    #@26
    invoke-virtual {p0, v8}, Landroid/widget/GridView;->playSoundEffect(I)V

    #@29
    .line 1833
    invoke-virtual {p0}, Landroid/widget/GridView;->invokeOnItemScrollListener()V

    #@2c
    .line 1836
    :cond_2c
    if-eqz v6, :cond_31

    #@2e
    .line 1837
    invoke-virtual {p0}, Landroid/widget/GridView;->awakenScrollBars()Z

    #@31
    .line 1840
    :cond_31
    return v3

    #@32
    .line 1800
    .end local v1           #endOfRow:I
    .end local v3           #moved:Z
    .end local v6           #showScroll:Z
    .end local v7           #startOfRow:I
    :cond_32
    add-int/lit8 v10, v0, -0x1

    #@34
    sub-int v2, v10, v5

    #@36
    .line 1801
    .local v2, invertedSelection:I
    add-int/lit8 v10, v0, -0x1

    #@38
    div-int v11, v2, v4

    #@3a
    mul-int/2addr v11, v4

    #@3b
    sub-int v1, v10, v11

    #@3d
    .line 1802
    .restart local v1       #endOfRow:I
    sub-int v10, v1, v4

    #@3f
    add-int/lit8 v10, v10, 0x1

    #@41
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@44
    move-result v7

    #@45
    .restart local v7       #startOfRow:I
    goto :goto_1b

    #@46
    .line 1809
    .end local v2           #invertedSelection:I
    .restart local v3       #moved:Z
    .restart local v6       #showScroll:Z
    :pswitch_46
    add-int/lit8 v10, v0, -0x1

    #@48
    if-ge v5, v10, :cond_20

    #@4a
    .line 1811
    iput v12, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@4c
    .line 1812
    add-int/lit8 v10, v5, 0x1

    #@4e
    invoke-virtual {p0, v10}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@51
    .line 1813
    const/4 v3, 0x1

    #@52
    .line 1815
    if-ne v5, v1, :cond_56

    #@54
    move v6, v8

    #@55
    :goto_55
    goto :goto_20

    #@56
    :cond_56
    move v6, v9

    #@57
    goto :goto_55

    #@58
    .line 1820
    :pswitch_58
    if-lez v5, :cond_20

    #@5a
    .line 1822
    iput v12, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@5c
    .line 1823
    add-int/lit8 v10, v5, -0x1

    #@5e
    invoke-virtual {p0, v10}, Landroid/widget/GridView;->setSelectionInt(I)V

    #@61
    .line 1824
    const/4 v3, 0x1

    #@62
    .line 1826
    if-ne v5, v7, :cond_66

    #@64
    move v6, v8

    #@65
    :goto_65
    goto :goto_20

    #@66
    :cond_66
    move v6, v9

    #@67
    goto :goto_65

    #@68
    .line 1807
    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_58
        :pswitch_46
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    check-cast p1, Landroid/widget/ListAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 7
    .parameter "adapter"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 166
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@4
    if-eqz v1, :cond_11

    #@6
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 167
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@c
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@e
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@11
    .line 170
    :cond_11
    invoke-virtual {p0}, Landroid/widget/GridView;->resetList()V

    #@14
    .line 171
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@16
    invoke-virtual {v1}, Landroid/widget/AbsListView$RecycleBin;->clear()V

    #@19
    .line 172
    iput-object p1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1b
    .line 174
    const/4 v1, -0x1

    #@1c
    iput v1, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@1e
    .line 175
    const-wide/high16 v1, -0x8000

    #@20
    iput-wide v1, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@22
    .line 178
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@25
    .line 180
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@27
    if-eqz v1, :cond_71

    #@29
    .line 181
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2b
    iput v1, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@2d
    .line 182
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2f
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    #@32
    move-result v1

    #@33
    iput v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@35
    .line 183
    iput-boolean v4, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@37
    .line 184
    invoke-virtual {p0}, Landroid/widget/GridView;->checkFocus()V

    #@3a
    .line 186
    new-instance v1, Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@3c
    invoke-direct {v1, p0}, Landroid/widget/AbsListView$AdapterDataSetObserver;-><init>(Landroid/widget/AbsListView;)V

    #@3f
    iput-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@41
    .line 187
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@43
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@45
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@48
    .line 189
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@4a
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@4c
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    #@4f
    move-result v2

    #@50
    invoke-virtual {v1, v2}, Landroid/widget/AbsListView$RecycleBin;->setViewTypeCount(I)V

    #@53
    .line 192
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@55
    if-eqz v1, :cond_6c

    #@57
    .line 193
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@59
    add-int/lit8 v1, v1, -0x1

    #@5b
    invoke-virtual {p0, v1, v3}, Landroid/widget/GridView;->lookForSelectablePosition(IZ)I

    #@5e
    move-result v0

    #@5f
    .line 197
    .local v0, position:I
    :goto_5f
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->setSelectedPositionInt(I)V

    #@62
    .line 198
    invoke-virtual {p0, v0}, Landroid/widget/GridView;->setNextSelectedPositionInt(I)V

    #@65
    .line 199
    invoke-virtual {p0}, Landroid/widget/GridView;->checkSelectionChanged()V

    #@68
    .line 206
    .end local v0           #position:I
    :goto_68
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayout()V

    #@6b
    .line 207
    return-void

    #@6c
    .line 195
    :cond_6c
    invoke-virtual {p0, v3, v4}, Landroid/widget/GridView;->lookForSelectablePosition(IZ)I

    #@6f
    move-result v0

    #@70
    .restart local v0       #position:I
    goto :goto_5f

    #@71
    .line 201
    .end local v0           #position:I
    :cond_71
    invoke-virtual {p0}, Landroid/widget/GridView;->checkFocus()V

    #@74
    .line 203
    invoke-virtual {p0}, Landroid/widget/GridView;->checkSelectionChanged()V

    #@77
    goto :goto_68
.end method

.method public setColumnWidth(I)V
    .registers 3
    .parameter "columnWidth"

    #@0
    .prologue
    .line 2070
    iget v0, p0, Landroid/widget/GridView;->mRequestedColumnWidth:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 2071
    iput p1, p0, Landroid/widget/GridView;->mRequestedColumnWidth:I

    #@6
    .line 2072
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 2074
    :cond_9
    return-void
.end method

.method public setGravity(I)V
    .registers 3
    .parameter "gravity"

    #@0
    .prologue
    .line 1940
    iget v0, p0, Landroid/widget/GridView;->mGravity:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1941
    iput p1, p0, Landroid/widget/GridView;->mGravity:I

    #@6
    .line 1942
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 1944
    :cond_9
    return-void
.end method

.method public setHorizontalSpacing(I)V
    .registers 3
    .parameter "horizontalSpacing"

    #@0
    .prologue
    .line 1967
    iget v0, p0, Landroid/widget/GridView;->mRequestedHorizontalSpacing:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 1968
    iput p1, p0, Landroid/widget/GridView;->mRequestedHorizontalSpacing:I

    #@6
    .line 1969
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 1971
    :cond_9
    return-void
.end method

.method public setNumColumns(I)V
    .registers 3
    .parameter "numColumns"

    #@0
    .prologue
    .line 2117
    iget v0, p0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 2118
    iput p1, p0, Landroid/widget/GridView;->mRequestedNumColumns:I

    #@6
    .line 2119
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 2121
    :cond_9
    return-void
.end method

.method public setRemoteViewsAdapter(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->setRemoteViewsAdapter(Landroid/content/Intent;)V

    #@3
    .line 157
    return-void
.end method

.method public setSelection(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1474
    invoke-virtual {p0}, Landroid/widget/GridView;->isInTouchMode()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_19

    #@6
    .line 1475
    invoke-virtual {p0, p1}, Landroid/widget/GridView;->setNextSelectedPositionInt(I)V

    #@9
    .line 1479
    :goto_9
    const/4 v0, 0x2

    #@a
    iput v0, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@c
    .line 1480
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 1481
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@12
    invoke-virtual {v0}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@15
    .line 1483
    :cond_15
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayout()V

    #@18
    .line 1484
    return-void

    #@19
    .line 1477
    :cond_19
    iput p1, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@1b
    goto :goto_9
.end method

.method setSelectionInt(I)V
    .registers 9
    .parameter "position"

    #@0
    .prologue
    .line 1493
    iget v4, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@2
    .line 1495
    .local v4, previousSelectedPosition:I
    iget-object v5, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@4
    if-eqz v5, :cond_b

    #@6
    .line 1496
    iget-object v5, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@8
    invoke-virtual {v5}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@b
    .line 1499
    :cond_b
    invoke-virtual {p0, p1}, Landroid/widget/GridView;->setNextSelectedPositionInt(I)V

    #@e
    .line 1500
    invoke-virtual {p0}, Landroid/widget/GridView;->layoutChildren()V

    #@11
    .line 1502
    iget-boolean v5, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@13
    if-eqz v5, :cond_35

    #@15
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@17
    add-int/lit8 v5, v5, -0x1

    #@19
    iget v6, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@1b
    sub-int v0, v5, v6

    #@1d
    .line 1504
    .local v0, next:I
    :goto_1d
    iget-boolean v5, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@1f
    if-eqz v5, :cond_38

    #@21
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@23
    add-int/lit8 v5, v5, -0x1

    #@25
    sub-int v2, v5, v4

    #@27
    .line 1507
    .local v2, previous:I
    :goto_27
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@29
    div-int v1, v0, v5

    #@2b
    .line 1508
    .local v1, nextRow:I
    iget v5, p0, Landroid/widget/GridView;->mNumColumns:I

    #@2d
    div-int v3, v2, v5

    #@2f
    .line 1510
    .local v3, previousRow:I
    if-eq v1, v3, :cond_34

    #@31
    .line 1511
    invoke-virtual {p0}, Landroid/widget/GridView;->awakenScrollBars()Z

    #@34
    .line 1514
    :cond_34
    return-void

    #@35
    .line 1502
    .end local v0           #next:I
    .end local v1           #nextRow:I
    .end local v2           #previous:I
    .end local v3           #previousRow:I
    :cond_35
    iget v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@37
    goto :goto_1d

    #@38
    .restart local v0       #next:I
    :cond_38
    move v2, v4

    #@39
    .line 1504
    goto :goto_27
.end method

.method public setStretchMode(I)V
    .registers 3
    .parameter "stretchMode"

    #@0
    .prologue
    .line 2052
    iget v0, p0, Landroid/widget/GridView;->mStretchMode:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 2053
    iput p1, p0, Landroid/widget/GridView;->mStretchMode:I

    #@6
    .line 2054
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 2056
    :cond_9
    return-void
.end method

.method public setVerticalSpacing(I)V
    .registers 3
    .parameter "verticalSpacing"

    #@0
    .prologue
    .line 2024
    iget v0, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 2025
    iput p1, p0, Landroid/widget/GridView;->mVerticalSpacing:I

    #@6
    .line 2026
    invoke-virtual {p0}, Landroid/widget/GridView;->requestLayoutIfNecessary()V

    #@9
    .line 2028
    :cond_9
    return-void
.end method

.method public smoothScrollByOffset(I)V
    .registers 2
    .parameter "offset"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 838
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->smoothScrollByOffset(I)V

    #@3
    .line 839
    return-void
.end method

.method public smoothScrollToPosition(I)V
    .registers 2
    .parameter "position"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 828
    invoke-super {p0, p1}, Landroid/widget/AbsListView;->smoothScrollToPosition(I)V

    #@3
    .line 829
    return-void
.end method
